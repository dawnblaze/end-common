param(
	[string] $packageFile,
	[string] $apiKey
)

if ([string]::IsNullOrEmpty($apiKey)) 
{
	dotnet.exe nuget push $packageFile -s https://www.myget.org/F/corebridgeshared/api/v2/package -ss https://www.myget.org/F/corebridgeshared/symbols/api/v2/package
}
else
{
	dotnet.exe nuget push $packageFile -s https://www.myget.org/F/corebridgeshared/api/v2/package -ss https://www.myget.org/F/corebridgeshared/symbols/api/v2/package -k $apiKey -sk $apiKey
}
