using Microsoft.AspNetCore.Mvc;
using System;

namespace Endor.Tenant.Cache
{
    [Route("api/[controller]")]
    public class CacheController : ControllerBase
    {
        private static ITenantDataCache _cacheObject;
        private static Func<ITenantDataCache> _cacheGetter;

        private static ITenantDataCache Cache
        {
            get
            {
                if (_cacheObject == null)
                {
                    if (_cacheGetter != null)
                        _cacheObject = _cacheGetter();
                }

                return _cacheObject;
            }
        }
        public static void SetCache(ITenantDataCache cache)
        {
            _cacheObject = cache;
        }
        public static void SetCacheEvent(Func<ITenantDataCache> cacheGetter)
        {
            _cacheGetter = cacheGetter;
        }

        [HttpPost("invalidate")]
        public IActionResult InvalidateCache([FromQuery]short? bid = null)
        {
            if (bid.HasValue)
                Cache.InvalidateCache(bid.Value);
            else
                Cache.InvalidateCache();

            return new OkResult();
        }
        
    }
}
