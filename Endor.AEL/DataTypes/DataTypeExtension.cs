﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Endor.AEL
{
    public static class DataTypeExtension
    {
        public static DataTypeInfo MetaDataInfo(this DataType dt) 
        {
            var dict = DataTypeDictionary.Instance;

            if (dict.ContainsKey(dt))
                return DataTypeDictionary.Instance[dt];

            return null;
        }

        public static short ClassTypeID(this DataType dt)
        {
            if ((dt == DataType.Order) || (dt == DataType.Estimate))
            {
                return 10000;
            }

            if ((dt == DataType.OrderList) || (dt == DataType.EstimateList))
            {
                return -10000;
            }


            // else return the collection of the datatype as the ClassType
            return ((short)dt);
        }

        public static IMemberInfo FindMember(this Type dt, IDataProvider dataProvider, string memberText)
        {
            List<IMemberInfo> bml = BusinessMemberDictionary.MembersByType(dataProvider, dt);

            IMemberInfo result = bml?.FirstOrDefault(t => t.TextMatches(memberText));

            if (result == null && dt.BaseType != null)
                return dt.BaseType.FindMember(dataProvider, memberText);

            return result;
        }

        public static IMemberInfo FindMember(this DataType dt, IDataProvider dataProvider, string memberText)
        {
            List<IMemberInfo> bml = BusinessMemberDictionary.MembersByDataType(dataProvider, dt);

            return bml?.FirstOrDefault(t => t.Text.ToLower() == memberText.ToLower());
        }

        public static IMemberInfo FindMember(this DataType dt, IDataProvider dataProvider, AELMember member)
        {
            List<IMemberInfo> bml = BusinessMemberDictionary.MembersByDataType(dataProvider, dt);

            if (bml == null)
                return null;

            if (member.ID.GetValueOrDefault(0) == 0)
                return bml.FirstOrDefault(t => t.Text.ToLower() == member.Text.ToLower());

            if (member.RelatedID.GetValueOrDefault(0) != 0)
                return bml.FirstOrDefault(t => t.ID == member.ID.Value && t.RelatedID == member.RelatedID.Value);

            return bml.FirstOrDefault(t => t.ID == member.ID.Value && t.RelatedID == 0);
        }
    }
}
