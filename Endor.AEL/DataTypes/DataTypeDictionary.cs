﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Endor.AEL
{
    public class DataTypeDictionary : Dictionary<DataType, DataTypeInfo>
    {
        // Use a private constructor and a singleton pattern with Lazy instantiation
        private DataTypeDictionary()
        {
            CreateTypeDictionary();
        }

        private static readonly Lazy<DataTypeDictionary> lazy = new Lazy<DataTypeDictionary>(() => new DataTypeDictionary());

        public static DataTypeDictionary Instance => lazy.Value;

        private void CreateTypeDictionary()
        {
            // Fill it with the primary types
            new List<DataTypeInfo>()
            {
                new DataTypeInfo
                {
                    DataType = DataType.None,
                    Text = "Unassigned",
                    PluralText = "Unassigneds",
                },
                new DataTypeInfo
                {
                    DataType = DataType.Boolean,
                    Text = "Boolean",
                    PluralText = "Booleans",
                    SystemType = typeof(bool)
                },
                new DataTypeInfo
                {
                    DataType = DataType.Number,
                    Text = "Number",
                    PluralText = "Numbers",
                    SystemType = typeof(decimal)
                },
                new DataTypeInfo
                {
                    DataType = DataType.String,
                    Text = "String",
                    PluralText = "Strings",
                    SystemType = typeof(string)
                },
                new DataTypeInfo
                {
                    DataType = DataType.DateTime,
                    Text = "Date Time",
                    PluralText = "Date Times",
                    SystemType = typeof(DateTime)
                },
                new DataTypeInfo
                {
                    DataType = DataType.Location,
                    Text = "Location",
                    PluralText = "Locations"
                },
                new DataTypeInfo
                {
                    DataType = DataType.LocationCategory,
                    Text = "Location",
                    PluralText = "Locations"
                },
                new DataTypeInfo
                {
                    DataType = DataType.Company,
                    Text = "Company",
                    PluralText = "Companies"
                },
                new DataTypeInfo
                {
                    DataType = DataType.CompanyCategory,
                    Text = "Company",
                    PluralText = "Companies"
                },
                new DataTypeInfo
                {
                    DataType = DataType.CompanyCustomField,
                    Text = "Custom Field",
                    PluralText = "Custom Field Values"
                },
                new DataTypeInfo
                {
                    DataType = DataType.Industry,
                    Text = "",
                    PluralText = ""
                },
                new DataTypeInfo
                {
                    DataType = DataType.Origin,
                    Text = "",
                    PluralText = ""
                },
                new DataTypeInfo
                {
                    DataType = DataType.Contact,
                    Text = "Contact",
                    PluralText = "Contacts"
                },
                new DataTypeInfo
                {
                    DataType = DataType.ContactCategory,
                    Text = "Contact",
                    PluralText = "Contacts"
                },
                new DataTypeInfo
                {
                    DataType = DataType.ContactCustomField,
                    Text = "Custom Field",
                    PluralText = "Custom Field Values"
                },
                new DataTypeInfo
                {
                    DataType = DataType.Employee,
                    Text = "Employee",
                    PluralText = "Employees"
                },
                new DataTypeInfo
                {
                    DataType = DataType.EmployeeCategory,
                    Text = "Employee",
                    PluralText = "Employees"
                },
                new DataTypeInfo
                {
                    DataType = DataType.EmployeeCustomField,
                    Text = "Custom Field",
                    PluralText = "Custom Field Values"
                },
                new DataTypeInfo
                {
                    DataType = DataType.EmployeeRole,
                    Text = "Employee Role ?",
                    PluralText = "Employee Roles"
                },
                new DataTypeInfo
                {
                    DataType = DataType.Team,
                    Text = "",
                    PluralText = ""
                },
                new DataTypeInfo
                {
                    DataType = DataType.Opportunity,
                    Text = "Opportunity",
                    PluralText = "Opportunities"
                },
                new DataTypeInfo
                {
                    DataType = DataType.EstimateCustomField,
                    Text = "Custom Field",
                    PluralText = "Custom Field Values"
                },
                new DataTypeInfo
                {
                    DataType = DataType.OrderCustomField,
                    Text = "Custom Field",
                    PluralText = "Custom Field Values"
                },
                new DataTypeInfo
                {
                    DataType = DataType.Order,
                    Text = "Order",
                    PluralText = "Orders"
                },
                new DataTypeInfo
                {
                    DataType = DataType.OrderCategory,
                    Text = "Order",
                    PluralText = "Orders"
                },
                new DataTypeInfo
                {
                    DataType = DataType.Estimate,
                    Text = "Estimate",
                    PluralText = "Estimates"
                },
                new DataTypeInfo
                {
                    DataType = DataType.EstimateCategory,
                    Text = "Estimate",
                    PluralText = "Estimates"
                },
                new DataTypeInfo
                {
                    DataType = DataType.PurchaseOrder,
                    Text = "PO",
                    PluralText = "PO"
                },
                new DataTypeInfo
                {
                    DataType = DataType.OrderContactRole,
                    Text = "Contact ?",
                    PluralText = "Contacts"
                },
                new DataTypeInfo
                {
                    DataType = DataType.OrderEmployeeRole,
                    Text = "Employee ?",
                    PluralText = "Employees"
                },
                new DataTypeInfo
                {
                    DataType = DataType.OrderNotes,
                    Text = "Notes",
                    PluralText = "Notes"
                },
                new DataTypeInfo
                {
                    DataType = DataType.LineItem,
                    Text = "Line Item",
                    PluralText = "Line Items"
                },
                new DataTypeInfo
                {
                    DataType = DataType.LineItemCategory,
                    Text = "Line Item",
                    PluralText = "Line Items"
                },
                new DataTypeInfo
                {
                    DataType = DataType.ItemStatus,
                    Text = "Item Status",
                    PluralText = "Item Statuses"
                },
                new DataTypeInfo
                {
                    DataType = DataType.Substatus,
                    Text = "SubStatus",
                    PluralText = "SubStatuses"
                },
                new DataTypeInfo
                {
                    DataType = DataType.Destination,
                    Text = "Destination",
                    PluralText = "Destinations"
                },
                new DataTypeInfo
                {
                    DataType = DataType.DestinationCategory,
                    Text = "Destination",
                    PluralText = "Destinations"
                },
                new DataTypeInfo
                {
                    DataType = DataType.DestinationStatus,
                    Text = "Dest Status",
                    PluralText = "Dest Statuses"
                },
                new DataTypeInfo
                {
                    DataType = DataType.TaxGroup,
                    Text = "",
                    PluralText = ""
                },
                new DataTypeInfo
                {
                    DataType = DataType.UserLink,
                    Text = "User",
                    PluralText = "Users"
                },

            }.ForEach((dt) => {
                Add(dt.DataType, dt);
                if (dt.DataType != DataType.None)
                {
                    DataType negativeKey = (DataType)(-(int)dt.DataType);

                    if (ContainsKey(negativeKey))
                    {
                        throw new InvalidOperationException($"Attempted to add negative key of {negativeKey} but it already exists");
                    }
                    else
                    {
                        Add(negativeKey,
                            new DataTypeInfo
                            {
                                DataType = negativeKey,
                                Text = dt.Text + "Collection",
                                PluralText = dt.PluralText + "Collection",
                                SystemType = dt.SystemType
                            }
                            );
                    }
                }
            });
        }
    }    
}
