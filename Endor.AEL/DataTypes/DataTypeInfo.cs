﻿using Endor.Models;
using Newtonsoft.Json;
using System;

namespace Endor.AEL
{
    public class DataTypeInfo
    {
        public DataType DataType { get; set; }
        public string Text { get; set; }
        public string PluralText { get; set; }
        [JsonIgnore]
        public Type SystemType { get; set; }
        public bool IsList => (DataType < DataType.None);
    }
}
