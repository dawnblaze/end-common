﻿using Endor.Models;
using System.Collections.Generic;
using System.Linq;

namespace Endor.AEL
{
    public interface IAELDataProvider
    {
        short BID { get; }
        IQueryable<T> GetData<T>() where T : class, IAtom;
    }
}
