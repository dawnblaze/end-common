﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.AEL
{
    public interface IMergeLookup
    {
        string MergeField { get; }
        bool IsValid { get; }
        bool IsNull { get; }
        dynamic Result { get; }
        string ToString();
    }
}
