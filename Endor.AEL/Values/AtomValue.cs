﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.AEL
{
    public class AtomValue<I> : IAtom<I>
        where I : struct, IConvertible
    {
        public DataType DataType { get; set; }

        public short BID { get; set; }

        public I ID { get; set; }

        public int ClassTypeID
        {
            get
            {
                return this.DataType.ClassTypeID();
            }
            set
            {

            }
        }

        public DateTime ModifiedDT { get; set; }
    }
}
