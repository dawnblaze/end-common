﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class TransactionHeaderTagsCategoryMemberInitializer : BaseMemberInitializer
    {
        public TransactionHeaderTagsCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<TransactionHeaderTagsCategoryMemberInitializer> lazy = new Lazy<TransactionHeaderTagsCategoryMemberInitializer>(() => new TransactionHeaderTagsCategoryMemberInitializer());

        public static TransactionHeaderTagsCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.TransactionHeaderTagsCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20311,
                        Text = "Has Tags",
                        DataType = DataType.Boolean,
                        LinqFx = TH => ((TransactionHeaderData)TH)?.TagLinks?.Any(),
                        Expression = (TH, forSQL) => {
                            Expression tagLinks = Expression.Property(TH, "TagLinks");

                            var callAny = Expression.Call(
                                typeof(Enumerable)
                                , "Any"
                                , new Type[]{
                                    typeof(OrderTagLink)
                                }
                                , tagLinks
                                );

                            if (forSQL)
                                return callAny;

                            var expCheckTags = Expression.Condition(
                                Expression.Equal(tagLinks, Expression.Constant(null, typeof(ICollection<OrderTagLink>)))
                                , AELHelper.Expressions.BooleanFalse
                                , callAny
                                );

                            return Expression.Condition(
                                Expression.Equal(TH, Expression.Constant(null, typeof(TransactionHeaderData)))
                                , AELHelper.Expressions.NullBoolean
                                , AELHelper.Expressions.ConvertToBoolean(expCheckTags)
                                );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20312,
                        Text = "Tag Name",
                        DataType = DataType.StringList,
                        LinqFx = TH => ((TransactionHeaderData)TH)?.TagLinks?.Select(t => t.Tag?.Name).ToList(),
                        Expression = (TH, forSQL) => AELHelper.Expressions.ObjectListPropertyWithSelect<TransactionHeaderData, OrderTagLink, string>(
                            TH
                            , nameof(TransactionHeaderData.TagLinks)
                            , $"{nameof(OrderTagLink.Tag)}.{nameof(ListTag.Name)}"
                            , forSQL
                            ),
                   },
                   new PropertyInfo()
                   {
                        ID = 20313,
                        Text = "Tag Color",
                        DataType = DataType.String,
                        LinqFx = TH => ((TransactionHeaderData)TH)?.TagLinks?.Select(t => t.Tag?.RGB).ToList(),
                        Expression = (TH, forSQL) => AELHelper.Expressions.ObjectListPropertyWithSelect<TransactionHeaderData, OrderTagLink, string>(
                            TH
                            , nameof(TransactionHeaderData.TagLinks)
                            , $"{nameof(OrderTagLink.Tag)}.{nameof(ListTag.RGB)}"
                            , forSQL
                            ),
                   },
               };
       }
}
