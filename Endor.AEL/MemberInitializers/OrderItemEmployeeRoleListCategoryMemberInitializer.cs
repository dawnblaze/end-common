﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemEmployeeRoleListCategoryMemberInitializer : BaseMemberInitializer
    {
        public OrderItemEmployeeRoleListCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemEmployeeRoleListCategoryMemberInitializer> lazy = new Lazy<OrderItemEmployeeRoleListCategoryMemberInitializer>(() => new OrderItemEmployeeRoleListCategoryMemberInitializer());

        public static OrderItemEmployeeRoleListCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemEmployeeRoleListCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                       ID = 20441,
                        Text = "Roles",
                        DataType = DataType.OrderItemEmployeeRoleList,
                        LinqFx = O => O,
                        Expression = (O, forSQL) => O,
                   },
                   new PropertyInfo()
                   {
                        ID = 20442,
                        Text = "Assigned To",
                        DataType = DataType.EmployeeCategory,
                        LinqFx = O => ((OrderItemData)O)?.EmployeeRoles?
                            .Where(r => r.RoleID == SystemIDs.EmployeeRole.AssignedTo)?.Select(x => x.Employee).ToList(),
                        Expression = (O, forSQL) => AELHelper.Expressions
                                       .ObjectListPropertyWithWhereSelect<OrderItemData, OrderEmployeeRole, EmployeeData>(
                                             O
                                           , "EmployeeRoles"
                                           , "Employee"
                                           , forSQL
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "RoleID", WhereValue = Expression.Constant(SystemIDs.EmployeeRole.AssignedTo, typeof(short)) }
                                       ),
                   },
                    new PropertyInfo()
                   {
                        ID = 20443,
                        Text = "Entered By",
                        DataType = DataType.EmployeeCategory,
                        LinqFx = O => ((OrderItemData)O)?.EmployeeRoles
                            .Where(r => r.RoleID == SystemIDs.EmployeeRole.EnteredBy)?.Select(x => x.Employee).ToList(),
                        Expression = (O, forSQL) => AELHelper.Expressions
                                       .ObjectListPropertyWithWhereSelect<OrderItemData, OrderEmployeeRole, EmployeeData>(
                                             O
                                           , "EmployeeRoles"
                                           , "Employee"
                                           , forSQL
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "RoleID", WhereValue = Expression.Constant(SystemIDs.EmployeeRole.EnteredBy, typeof(short)) }
                                       ),
                    },
               };
    }
}
