﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class TransactionHeaderContactRoleCategory : BaseMemberInitializer
    {
        public TransactionHeaderContactRoleCategory() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<TransactionHeaderContactRoleCategory> lazy = new Lazy<TransactionHeaderContactRoleCategory>(() => new TransactionHeaderContactRoleCategory());

        public static TransactionHeaderContactRoleCategory Instance => lazy.Value;

        public override DataType DataType => DataType.TransactionHeaderContactRoleCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                       ID = 20191,
                        Text = "Primary",
                        DataType = DataType.ContactCategory,
                        LinqFx = O => ((TransactionHeaderData)O).ContactRoles?
                            .FirstOrDefault(r => r.RoleType == OrderContactRoleType.Primary)?.Contact,
                        Expression = (O, forSQL) =>
                        {
                            //var contactRole = 
                            //    AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<TransactionHeaderData, OrderContactRole>(
                            //              O
                            //            , "ContactRoles"
                            //            , forSQL
                            //            , new AELHelper.Expressions.WhereCondition{ WhereProperty = "RoleType", WhereValue = Expression.Constant(OrderContactRoleType.Primary, typeof(OrderContactRoleType)) }
                            //            );

                            //return AELHelper.Expressions.ObjectProperty<OrderContactRole, ContactData>(contactRole, "Contact", forSQL);

                            var contactRole =
                                AELHelper.Expressions.ObjectListPropertyWithWhereSelect<TransactionHeaderData, OrderContactRole, ContactData>(
                                          O
                                        , "ContactRoles"
                                        , "Contact"
                                        , forSQL
                                        , new AELHelper.Expressions.WhereCondition{ WhereProperty = "RoleType", WhereValue = Expression.Constant(OrderContactRoleType.Primary, typeof(OrderContactRoleType)) }
                                        );

                            return contactRole;
                        },
                   },
                   new PropertyInfo()
                   {
                       ID = 20192,
                        Text = "Shipping",
                        DataType = DataType.ContactCategory,
                        LinqFx = O => ((TransactionHeaderData)O).ContactRoles?
                            .FirstOrDefault(r => r.RoleType == OrderContactRoleType.ShipTo)?.Contact,
                        Expression = (O, forSQL) =>
                        {
                            //var contactRole =
                            //    AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault
                            //        <TransactionHeaderData, OrderContactRole>(
                            //              O
                            //            , "ContactRoles"
                            //            , forSQL
                            //            , new AELHelper.Expressions.WhereCondition{ WhereProperty = "RoleType", WhereValue = Expression.Constant(OrderContactRoleType.ShipTo, typeof(OrderContactRoleType)) }
                            //            );

                            //return AELHelper.Expressions.ObjectProperty<OrderContactRole, ContactData>(contactRole, "Contact", forSQL);

                            var contactRole =
                                AELHelper.Expressions.ObjectListPropertyWithWhereSelect<TransactionHeaderData, OrderContactRole, ContactData>(
                                          O
                                        , "ContactRoles"
                                        , "Contact"
                                        , forSQL
                                        , new AELHelper.Expressions.WhereCondition{ WhereProperty = "RoleType", WhereValue = Expression.Constant(OrderContactRoleType.ShipTo, typeof(OrderContactRoleType)) }
                                        );

                            return contactRole;

                        },
                   },
               };
        }
}
