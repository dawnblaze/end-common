﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class PONumberCategoryMemberInitializer : BaseMemberInitializer
    {
        public PONumberCategoryMemberInitializer() { }

        private static readonly Lazy<PONumberCategoryMemberInitializer> lazy = new Lazy<PONumberCategoryMemberInitializer>(() => new PONumberCategoryMemberInitializer());

        public static PONumberCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.PONumberCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo()
                {
                    ID = 20180,
                    Text = "Default PO Number",
                    DataType = DataType.String,
                    LinqFx = C => ((CompanyData)C)?.DefaultPONumber,
                    Expression = (C, forSQL) => AELHelper.Expressions.StringProperty<CompanyData>(C, nameof(CompanyData.DefaultPONumber), forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20181,
                    Text = "PO Required",
                    DataType = DataType.Boolean,
                    LinqFx = C => ((CompanyData)C)?.IsPORequired,
                    Expression = (C, forSQL) => AELHelper.Expressions.BooleanProperty<CompanyData>(C, nameof(CompanyData.IsPORequired), forSQL),
                },
            };
    }
}
