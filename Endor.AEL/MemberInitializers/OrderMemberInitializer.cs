﻿using System;
using System.Collections.Generic;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class OrderMemberInitializer : BaseMemberInitializer
    {
        public OrderMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderMemberInitializer> lazy = new Lazy<OrderMemberInitializer>(() => new OrderMemberInitializer());

        public static OrderMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderCategory;
        public override Type ParentType => typeof(OrderData);

        public override List<PropertyInfo> GlobalProperties()
        {
            return new List<PropertyInfo>()
            {
                //new PropertyInfo()
                //{
                //    ID = 10000,
                //    Text = "Line Items",
                //    DataType = DataType.LineItemCategory,
                //    LinqFx = O => ((OrderData)O)?.Items?.ToList(),
                //    Expression = (O, forSQL) => AELHelper.Expressions.ObjectListProperty<OrderData, OrderItemData>(O, nameof(OrderData.Items), forSQL),
                //},
                new PropertyInfo()
                {
                    ID = 10001,
                    Text = "Contacts",
                    DataType = DataType.TransactionHeaderContactRoleCategory,
                    LinqFx = O => ((OrderData)O),
                    Expression = (O, forSQL) => O,
                },
                new PropertyInfo()
                {
                    ID = 10002,
                    Text = "Employees",
                    DataType = DataType.TransactionHeaderEmployeeRoleListCategory,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O,
                },
                new PropertyInfo()
                {
                    ID = 10003,
                    Text = "Locations",
                    DataType = DataType.LocationCategory,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O,
                },
                new PropertyInfo()
                {
                    ID = 10004,
                    Text = "Order Origin",
                    DataType = DataType.Origin,
                    LinqFx = O => ((OrderData)O)?.Origin,
                    Expression = (O, forSQL) => {
                        var exp = Expression.Property(O, "Origin");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(O, AELHelper.Expressions.NullOrder)
                            , Expression.Constant(null, typeof(CrmOrigin))
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10005,
                    Text = "Destinations",
                    DataType = DataType.DestinationCategory,
                    LinqFx = O => ((OrderData)O)?.Destinations?.ToList(),
                    Expression = (O, forSQL) => {
                        var destinationProperty = Expression.Property(O, "Destinations");
                        var callToList = Expression.Call(
                              typeof(Enumerable)
                            , "ToList"
                            , new Type[]
                            {
                                typeof(OrderDestinationData)
                            }
                            , destinationProperty
                        );

                        if (forSQL)
                            return callToList;

                        var itemNullCheck = Expression.Condition(
                              Expression.Equal(destinationProperty, Expression.Constant(null, typeof(ICollection<OrderDestinationData>)))
                            , Expression.Constant(null, typeof(List<OrderDestinationData>))
                            , callToList
                        );

                        return Expression.Condition(
                              Expression.Equal(O, AELHelper.Expressions.NullOrder)
                            , Expression.Constant(null, typeof(List<OrderDestinationData>))
                            , itemNullCheck
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10006,
                    Text = "Order Number",
                    DataType = DataType.Number,
                    NumberType =  NumberType.Integer,
                    LinqFx = O => ((OrderData)O)?.Number,
                    Expression = (O, forSQL) => {
                        var exp = AELHelper.Expressions.ConvertToNumber(Expression.Property(O, "Number"));

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(O, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullNumber
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10007,
                    Text = "Description",
                    DataType = DataType.String,
                    LinqFx = O => ((OrderData)O)?.Description,
                    Expression = (O, forSQL) => {
                        var exp = Expression.Property(O, "Description");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(O, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullString
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10008,
                    Text = "Company",
                    DataType = DataType.CompanyCategory,
                    LinqFx = O => ((OrderData)O)?.Company,
                    Expression = (O, forSQL) => {
                        var exp = Expression.Property(O, "Company");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(O, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullCompany
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10009,
                    Text = "Status",
                    DataType = DataType.OrderStatus,
                    LinqFx = O => (decimal?)((OrderData)O)?.OrderStatusID,
                    Expression = (O, forSQL) => {
                        var exp = Expression.Convert(
                              Expression.Convert(
                                  Expression.Property(O, "OrderStatusID")
                                , typeof(byte?))
                            , typeof(decimal?));

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(O, AELHelper.Expressions.NullOrder)
                            , Expression.Constant(null, typeof(decimal?))
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10010,
                    Text = "Dates",
                    DataType = DataType.OrderDateCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10011,
                    Text = "Taxes",
                    DataType = DataType.TaxesCategory,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O,
                },
                new PropertyInfo()
                {
                    ID = 20060,
                    Text = "Prices",
                    DataType = DataType.PricesCategory,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O
                },
                new PropertyInfo()
                {
                    ID = 20090,
                    Text = "Payments",
                    DataType = DataType.PaymentCategory,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O
                    
                    //LinqFx = O => ((OrderData)O)?.PaymentPaid,
                    //Expression = (O, forSQL) => {
                    //    var exp = Expression.Property(O, "PaymentPaid");

                    //    if (forSQL)
                    //        return exp;

                    //    return Expression.Condition(
                    //          Expression.Equal(O, AELHelper.Expressions.NullOrder)
                    //        , AELHelper.Expressions.NullNumber
                    //        , exp
                    //    );
                    //},
                },
                new PropertyInfo()
                {
                    ID = 20070,
                    Text = "Costs",
                    DataType = DataType.CostCategory,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O
                    
                },
                new PropertyInfo()
                {
                    ID = 10015,
                    Text = "PO Number",
                    DataType = DataType.String,
                    LinqFx = O => ((OrderData)O)?.OrderPONumber,
                    Expression = (O, forSQL) => {
                        var exp = Expression.Property(O, "OrderPONumber");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(O, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullString
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10017,
                    Text = "Tags",
                    DataType = DataType.TransactionHeaderTagsCategory,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O,
                },
                new PropertyInfo()
                {
                    ID = 10018,
                    Text = "Has Documents",
                    DataType = DataType.Boolean,
                    LinqFx = O => ((OrderData)O)?.HasDocuments,
                    Expression = (O, forSQL) => {
                        var exp = AELHelper.Expressions.ConvertToBoolean(Expression.Property(O, "HasDocuments"));

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(O, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullBoolean
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10019,
                    Text = "Notes",
                    DataType = DataType.TransactionHeaderNotesCategory,
                    LinqFx = O => ((OrderData)O)?.Notes?.ToList(),
                    Expression = (O, forSQL) => AELHelper.Expressions.ObjectListProperty<OrderData, OrderNote>(O, "Notes", forSQL),
                },
                new PropertyInfo
                {
                    ID = 10040,
                    Text = "Custom Fields",
                    DataType = DataType.OrderCustomField,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O,
                },
            };

        }

        public OrderCustomFieldMemberInitializer CompanyCustomField => OrderCustomFieldMemberInitializer.Instance;
    }
}
