﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class CompanyEmployeeListCategoryMemberInitializer : BaseMemberInitializer
    {
        public CompanyEmployeeListCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<CompanyEmployeeListCategoryMemberInitializer> lazy = new Lazy<CompanyEmployeeListCategoryMemberInitializer>(() => new CompanyEmployeeListCategoryMemberInitializer());

        public static CompanyEmployeeListCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyEmployeeListCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                       ID = 10100,
                        Text = "Roles",
                        DataType = DataType.CompanyEmployeeRoleListCategory,
                        LinqFx = C => C,
                        Expression = (C, forSQL) => C,
                   },
                   new PropertyInfo()
                   {
                       ID = 10101,
                        Text = "Assigned To",
                        DataType = DataType.EmployeeCategory,
                        LinqFx = C => ((CompanyData)C)?.EmployeeTeam?.EmployeeTeamLinks?
                                        .Where(r => r.RoleID == SystemIDs.EmployeeRole.AssignedTo)?.Select(p => p.Employee).ToList(),
                        Expression = (C, forSQL) => AELHelper.Expressions
                                       .ObjectListPropertyWithWhereSelect<CompanyData, EmployeeTeamLink, EmployeeData>(
                                             C
                                           , "EmployeeTeam.EmployeeTeamLinks"
                                           , "Employee"
                                           , forSQL
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "RoleID", WhereValue = Expression.Constant(SystemIDs.EmployeeRole.AssignedTo, typeof(short)) }
                                       ),
                   },
               };

    }
}
