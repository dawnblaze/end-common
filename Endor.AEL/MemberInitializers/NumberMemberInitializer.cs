﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.AEL
{
    public class NumberMemberInitializer : BaseMemberInitializer
    {
        public NumberMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<NumberMemberInitializer> lazy = new Lazy<NumberMemberInitializer>(() => new NumberMemberInitializer());

        public static NumberMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.Number;
        public override Type ParentType => typeof(decimal);
    }
}
