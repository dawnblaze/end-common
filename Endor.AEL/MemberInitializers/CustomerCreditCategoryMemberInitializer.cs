﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class CustomerCreditCategoryMemberInitializer : BaseMemberInitializer
    {
        public CustomerCreditCategoryMemberInitializer() { }

        private static readonly Lazy<CustomerCreditCategoryMemberInitializer> lazy = new Lazy<CustomerCreditCategoryMemberInitializer>(() => new CustomerCreditCategoryMemberInitializer());

        public static CustomerCreditCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CustomerCreditCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo()
                {
                    ID = 20170,
                    Text = "Customer Credit Limit",
                    DataType = DataType.Number,
                    NumberType = NumberType.Currency,
                    LinqFx = C => ((CompanyData)C)?.CreditLimit,
                    Expression = (C, forSQL) => AELHelper.Expressions.NumberProperty<CompanyData>(C, "CreditLimit", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20171,
                    Text = "Has Customer Credit",
                    DataType = DataType.Boolean,
                    LinqFx = C => ((CompanyData)C)?.HasCreditAccount,
                    Expression = (C, forSQL) => AELHelper.Expressions.BooleanProperty<CompanyData>(C, "HasCreditAccount", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20172,
                    Text = "Credit Available",
                    DataType = DataType.Number,
                    NumberType = NumberType.Currency,
                    LinqFx = C => ((CompanyData)C)?.CreditLimit - (((CompanyData)C)?.NonRefundableCredit + ((CompanyData)C)?.RefundableCredit),
                    Expression = (C, forSQL) =>
                    {
                        return Expression.Subtract(
                            AELHelper.Expressions.NumberProperty<CompanyData>(C, "CreditLimit", forSQL),
                            Expression.Add(
                                AELHelper.Expressions.NumberProperty<CompanyData>(C, "NonRefundableCredit", forSQL),
                                AELHelper.Expressions.NumberProperty<CompanyData>(C, "RefundableCredit", forSQL)
                            ));
                    },
                },
            };
    }
}
