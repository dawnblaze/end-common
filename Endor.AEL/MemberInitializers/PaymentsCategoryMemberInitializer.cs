﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class PaymentsCategoryMemberInitializer : BaseMemberInitializer
    {
        public PaymentsCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<PaymentsCategoryMemberInitializer> lazy = new Lazy<PaymentsCategoryMemberInitializer>(() => new PaymentsCategoryMemberInitializer());

        public static PaymentsCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.PaymentCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20091,
                        Text = "Payment Amount",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.PaymentTotal,
                        Expression = (O, forSQL) =>
                            AELHelper.Expressions.NumberProperty<TransactionHeaderData>(O,"PaymentTotal",forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20092,
                        Text = "Balance Due",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.PaymentBalanceDue,
                        Expression = (O, forSQL) =>
                            AELHelper.Expressions.NumberProperty<TransactionHeaderData>(O,"PaymentBalanceDue",forSQL),
                   },
               };
       };
}
