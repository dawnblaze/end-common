﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class CostCategoryMemberInitializer : BaseMemberInitializer
    {
        public CostCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<CostCategoryMemberInitializer> lazy = new Lazy<CostCategoryMemberInitializer>(() => new CostCategoryMemberInitializer());

        public static CostCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CostCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20301,
                        Text = "Total Cost",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.CostTotal,
                        Expression = (O, forSQL) => AELHelper.Expressions.NumberProperty<TransactionHeaderData>(O,"CostTotal",forSQL)
                   },
                   new PropertyInfo()
                   {
                        ID = 20302,
                        Text = "Material Cost",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.CostMaterial,
                        Expression = (O, forSQL) => AELHelper.Expressions.NumberProperty<TransactionHeaderData>(O,"CostMaterial",forSQL)
                   },
                   new PropertyInfo()
                   {
                        ID = 20303,
                        Text = "Labor Cost",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.CostLabor,
                        Expression = (O, forSQL) => AELHelper.Expressions.NumberProperty<TransactionHeaderData>(O,"CostLabor",forSQL)
                   },
                   new PropertyInfo()
                   {
                        ID = 20304,
                        Text = "Machine Cost",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.CostMachine,
                        Expression = (O, forSQL) => AELHelper.Expressions.NumberProperty<TransactionHeaderData>(O,"CostMachine",forSQL)
                   },
               };
    }
}
