﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemEmployeeRoleMemberInitializer : BaseMemberInitializer
    {
        public OrderItemEmployeeRoleMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemEmployeeRoleMemberInitializer> lazy = new Lazy<OrderItemEmployeeRoleMemberInitializer>(() => new OrderItemEmployeeRoleMemberInitializer());

        public static OrderItemEmployeeRoleMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemEmployeeRoleList;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 11106,
                        Text = "Employees in Any Role",
                        DataType = DataType.EmployeeCategory,
                        LinqFx = O => ((OrderItemData)O)?.EmployeeRoles
                                        .Select(r => r.Employee)
                                        .Distinct()
                                        .ToList(),
                        Expression = (O, forSQL) => AELHelper.Expressions.ObjectListPropertyWithSelect<OrderItemData, OrderEmployeeRole, EmployeeData>(O, "EmployeeRoles", "Employee", forSQL),
                   },
               };

        public override List<PropertyInfo> BIDProperties(IDataProvider dataProvider)
        {
            List<PropertyInfo> result = new List<PropertyInfo>();

            IQueryable<EmployeeRole> roles = dataProvider.GetData<EmployeeRole>()
                                                         .Where(x => x.ID != SystemIDs.EmployeeRole.AssignedTo && x.ID != SystemIDs.EmployeeRole.EnteredBy)
                                                         .OrderBy(x => x.Name);

            if (roles != null)
            {
                foreach (EmployeeRole role in roles)
                {
                    result.Add(
                           new PropertyInfo()
                           {
                               ID = 11109,
                               RelatedID = role.ID,
                               Text = role.Name,
                               DataType = DataType.EmployeeCategory,
                               LinqFx = O => ((OrderItemData)O)?.EmployeeRoles?
                                   .Where(r => r.RoleID == role.ID)?.Select(x => x.Employee).ToList(),
                               Expression = (O, forSQL) => AELHelper.Expressions
                                       .ObjectListPropertyWithWhereSelect<OrderItemData, OrderEmployeeRole, EmployeeData>(
                                             O
                                           , "EmployeeRoles"
                                           , "Employee"
                                           , forSQL
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "RoleID", WhereValue = Expression.Constant(role.ID, typeof(short)) }
                                       ),
                           }
                        );

                }
            }

            return result;
        }
    }
}
