﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemTagsCategoryMemberInitializer : BaseMemberInitializer
    {
        public OrderItemTagsCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemTagsCategoryMemberInitializer> lazy = new Lazy<OrderItemTagsCategoryMemberInitializer>(() => new OrderItemTagsCategoryMemberInitializer());

        public static OrderItemTagsCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemTagsCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20211,
                        Text = "Has Tags",
                        DataType = DataType.Boolean,
                        LinqFx = OI => ((OrderItemData)OI)?.TagLinks?.Any(),
                        Expression = (OI, forSQL) => {
                            Expression tagLinks = Expression.Property(OI, "TagLinks");

                            var callAny = Expression.Call(
                                typeof(Enumerable)
                                , "Any"
                                , new Type[]{
                                    typeof(OrderItemTagLink)
                                }
                                , tagLinks
                                );

                            if (forSQL)
                                return callAny;

                            var expCheckTags = Expression.Condition(
                                Expression.Equal(tagLinks, Expression.Constant(null, typeof(ICollection<OrderItemTagLink>)))
                                , AELHelper.Expressions.BooleanFalse
                                , callAny
                                );

                            return Expression.Condition(
                                Expression.Equal(OI, Expression.Constant(null, typeof(OrderItemData)))
                                , AELHelper.Expressions.NullBoolean
                                , AELHelper.Expressions.ConvertToBoolean(expCheckTags)
                                );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20212,
                        Text = "Tag Name",
                        DataType = DataType.StringList,
                        LinqFx = OI => ((OrderItemData)OI)?.TagLinks?.Select(t => t.Tag?.Name).ToList(),
                        Expression = (OI, forSQL) =>
                        {
                            var expTagLinks = Expression.Property(OI, "TagLinks");

                            var expTags = AELHelper.Expressions.SelectFromObjectList<OrderItemTagLink, ListTag>(expTagLinks, "Tag", forSQL);
                            var expTagNames = AELHelper.Expressions.SelectFromObjectList<ListTag, string>(expTags, "Name", forSQL);

                            if (forSQL)
                                return expTagNames;

                            return Expression.Condition(
                                  Expression.Equal(OI, Expression.Constant(null, typeof(OrderItemData)))
                                , Expression.Constant(null, typeof(List<string>))
                                , expTagNames
                            );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20213,
                        Text = "Tag Color",
                        DataType = DataType.String,
                        LinqFx = OI => ((OrderItemData)OI)?.TagLinks?.Select(t => t.Tag?.RGB).ToList(),
                        Expression = (OI, forSQL) =>
                        {
                            var expTagLinks = Expression.Property(OI, "TagLinks");

                            var expTags = AELHelper.Expressions.SelectFromObjectList<OrderItemTagLink, ListTag>(expTagLinks, "Tag", forSQL);
                            var expTagNames = AELHelper.Expressions.SelectFromObjectList<ListTag, string>(expTags, "RGB", forSQL);

                            return Expression.Condition(
                                  Expression.Equal(OI, Expression.Constant(null, typeof(OrderItemData)))
                                , Expression.Constant(null, typeof(List<string>))
                                , expTagNames
                            );
                        },
                   },
               };
       }
}
