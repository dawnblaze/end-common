﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class PricesCategoryMemberInitializer : BaseMemberInitializer
    {
        public PricesCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<PricesCategoryMemberInitializer> lazy = new Lazy<PricesCategoryMemberInitializer>(() => new PricesCategoryMemberInitializer());

        public static PricesCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.PricesCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20071,
                        Text = "Price (Pre Tax)",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.PricePreTax,
                        Expression = (O, forSQL) => {
                            var exp = Expression.Property(O, "PricePreTax");

                            if (forSQL)
                                return exp;

                            return Expression.Condition(
                                Expression.Equal(O, AELHelper.Expressions.NullOrder)
                                , Expression.Constant(null,typeof(decimal?))
                                , exp
                                );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20072,
                        Text = "Price (After Tax)",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.PriceTotal,
                        Expression = (O, forSQL) => {
                            var exp = Expression.Property(O, "PriceTotal");

                            if (forSQL)
                                return exp;

                            return Expression.Condition(
                                Expression.Equal(O, AELHelper.Expressions.NullOrder)
                                , Expression.Constant(null,typeof(decimal?))
                                , exp
                                );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20073,
                        Text = "Line Item Total",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.PriceProductTotal,
                        Expression = (O, forSQL) => {
                            var exp = Expression.Property(O, "PriceProductTotal");

                            if (forSQL)
                                return exp;

                            return Expression.Condition(
                                Expression.Equal(O, AELHelper.Expressions.NullOrder)
                                , Expression.Constant(null,typeof(decimal?))
                                , exp
                                );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20074,
                        Text = "Destination Total",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.PriceDestinationTotal,
                        Expression = (O, forSQL) => {
                            var exp = Expression.Property(O, "PriceDestinationTotal");

                            if (forSQL)
                                return exp;

                            return Expression.Condition(
                                Expression.Equal(O, AELHelper.Expressions.NullOrder)
                                , Expression.Constant(null,typeof(decimal?))
                                , exp
                                );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20075,
                        Text = "Finance Charge",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.PriceFinanceCharge,
                        Expression = (O, forSQL) => {
                            var exp = Expression.Property(O, "PriceFinanceCharge");

                            if (forSQL)
                                return exp;

                            return Expression.Condition(
                                Expression.Equal(O, AELHelper.Expressions.NullOrder)
                                , Expression.Constant(null,typeof(decimal?))
                                , exp
                                );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20076,
                        Text = "Discount Amount",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = O => ((TransactionHeaderData)O)?.PriceDiscount,
                        Expression = (O, forSQL) => {
                            var exp = Expression.Property(O, "PriceDiscount");

                            if (forSQL)
                                return exp;

                            return Expression.Condition(
                                Expression.Equal(O, AELHelper.Expressions.NullOrder)
                                , Expression.Constant(null,typeof(decimal?))
                                , exp
                                );
                        },
                   },

               };
       };
}
