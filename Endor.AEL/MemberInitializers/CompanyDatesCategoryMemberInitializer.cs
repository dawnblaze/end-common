﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class CompanyDatesCategoryMemberInitializer : BaseMemberInitializer
    {
        public CompanyDatesCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<CompanyDatesCategoryMemberInitializer> lazy = new Lazy<CompanyDatesCategoryMemberInitializer>(() => new CompanyDatesCategoryMemberInitializer());

        public static CompanyDatesCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyDatesCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>
            {
                new PropertyInfo
                {
                    ID = 20360,
                    Text = "Created Date",
                    DataType = DataType.DateTime,
                    LinqFx = C => ((CompanyData)C)?.CreatedDate,
                    Expression = (C, forSQL) => AELHelper.Expressions.DateTimeProperty<CompanyData>(C, "CreatedDate", forSQL),
                },
                new PropertyInfo
                {
                    ID = 20361,
                    Text = "Last Order Date",
                    DataType = DataType.DateTime,
                    LinqFx = C => ((CompanyData)C)?.Orders?.SelectMany(x => x.Dates.Where(d => d.KeyDateType == OrderKeyDateType.Created && d.OrderItemID == null)).Max(d => d.KeyDate),
                    Expression = (C, forSQL) => {
                        var expOrders = Expression.Property(C, "Orders");

                        var selectManyParam = Expression.Parameter(typeof(TransactionHeaderData), "x");

                        var selectManyProp = Expression.Property(selectManyParam, "Dates");

                        var callSelectManyDates = Expression.Call(
                              typeof(Enumerable)
                            , "SelectMany"
                            , new Type[]
                            {
                                  typeof(TransactionHeaderData)
                                , typeof(OrderKeyDate)
                            }
                            , expOrders
                            , Expression.Lambda(
                                  selectManyProp
                                , selectManyParam
                            )
                        );

                        var datesWhereParam = Expression.Parameter(typeof(OrderKeyDate), "d");
                        var keyDateTypeProp = Expression.Property(datesWhereParam, "KeyDateType");
                        var orderItemIDProp = Expression.Property(datesWhereParam, "OrderItemID");

                        var whereClause = Expression.AndAlso(
                              Expression.Equal(keyDateTypeProp, Expression.Constant(OrderKeyDateType.Created, typeof(OrderKeyDateType)))
                            , Expression.Equal(orderItemIDProp, Expression.Constant(null, typeof(int?)))
                        );

                        var callWhere = Expression.Call(
                              typeof(Enumerable)
                            , "Where"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , callSelectManyDates
                            , Expression.Lambda(
                                  whereClause
                                , datesWhereParam
                            )
                        );


                        var keyDateParam = Expression.Parameter(typeof(OrderKeyDate), "d");
                        var keyDateProp = Expression.Property(keyDateParam, "KeyDate");

                        var callMaxDate = Expression.Call(
                              typeof(Enumerable)
                            , "Max"
                            , new Type[]
                            {
                                  typeof(OrderKeyDate)
                                , typeof(DateTime?)
                            }
                            , callWhere
                            , Expression.Lambda(
                                  keyDateProp
                                , keyDateParam
                            )
                        );


                        if (forSQL)
                            return callMaxDate;

                        return Expression.Condition(
                              Expression.Equal(C, Expression.Constant(null, typeof(CompanyData)))
                            , AELHelper.Expressions.NullDateTime
                            , callMaxDate
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 20362,
                    Text = "First Order Date",
                    DataType = DataType.DateTime,
                    LinqFx = C => ((CompanyData)C)?.Orders?.SelectMany(x => x.Dates.Where(d => d.KeyDateType == OrderKeyDateType.Created && d.OrderItemID == null)).Min(d => d.KeyDate),
                    Expression = (C, forSQL) => {
                        var expOrders = Expression.Property(C, "Orders");

                        var selectManyParam = Expression.Parameter(typeof(TransactionHeaderData), "x");

                        var selectManyProp = Expression.Property(selectManyParam, "Dates");

                        var callSelectManyDates = Expression.Call(
                              typeof(Enumerable)
                            , "SelectMany"
                            , new Type[]
                            {
                                  typeof(TransactionHeaderData)
                                , typeof(OrderKeyDate)
                            }
                            , expOrders
                            , Expression.Lambda(
                                  selectManyProp
                                , selectManyParam
                            )
                        );

                        var datesWhereParam = Expression.Parameter(typeof(OrderKeyDate), "d");
                        var keyDateTypeProp = Expression.Property(datesWhereParam, "KeyDateType");
                        var orderItemIDProp = Expression.Property(datesWhereParam, "OrderItemID");

                        var whereClause = Expression.AndAlso(
                              Expression.Equal(keyDateTypeProp, Expression.Constant(OrderKeyDateType.Created, typeof(OrderKeyDateType)))
                            , Expression.Equal(orderItemIDProp, Expression.Constant(null, typeof(int?)))
                        );

                        var callWhere = Expression.Call(
                              typeof(Enumerable)
                            , "Where"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , callSelectManyDates
                            , Expression.Lambda(
                                  whereClause
                                , datesWhereParam
                            )
                        );


                        var keyDateParam = Expression.Parameter(typeof(OrderKeyDate), "d");
                        var keyDateProp = Expression.Property(keyDateParam, "KeyDate");

                        var callMaxDate = Expression.Call(
                              typeof(Enumerable)
                            , "Min"
                            , new Type[]
                            {
                                  typeof(OrderKeyDate)
                                , typeof(DateTime?)
                            }
                            , callWhere
                            , Expression.Lambda(
                                  keyDateProp
                                , keyDateParam
                            )
                        );


                        if (forSQL)
                            return callMaxDate;

                        return Expression.Condition(
                              Expression.Equal(C, Expression.Constant(null, typeof(CompanyData)))
                            , AELHelper.Expressions.NullDateTime
                            , callMaxDate
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 20363,
                    Text = "Last Payment Date",
                    DataType = DataType.DateTime,
                    LinqFx = C => ((CompanyData)C)?.Payments.Max(p => p.AccountingDT),
                    Expression = (C, forSQL) => {
                        var paymentsProp = Expression.Property(C, "Payments");
                        var paymentsParam = Expression.Parameter(typeof(PaymentMaster), "p");
                        var paymentsDateProp = Expression.Property(paymentsParam, "AccountingDT");
                        var callPaymentsMax = Expression.Call(
                              typeof(Enumerable)
                            , "Max"
                            , new Type[]
                            {
                                  typeof(PaymentMaster)
                                , typeof(DateTime)
                            }
                            , paymentsProp
                            , Expression.Lambda(
                                  paymentsDateProp
                                , paymentsParam
                            )
                        );

                        if (forSQL)
                            return callPaymentsMax;

                        var paymentsEqualNull = Expression.Equal(
                              paymentsProp
                            , Expression.Constant(null, typeof(ICollection<PaymentMaster>))
                        );

                        var expPaymentsMaxCheck = Expression.Condition(
                              paymentsEqualNull
                            , AELHelper.Expressions.NullDateTime
                            , AELHelper.Expressions.ConvertToDateTime(callPaymentsMax)
                        );

                        return Expression.Condition(
                              Expression.Equal(C, Expression.Constant(null, typeof(CompanyData)))
                            , AELHelper.Expressions.NullDateTime
                            , AELHelper.Expressions.ConvertToDateTime(expPaymentsMaxCheck)
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 20364,
                    Text = "Last Invoiced Date",
                    DataType = DataType.DateTime,
                    LinqFx = C => ((CompanyData)C)?.Orders?.SelectMany(x => x.Dates.Where(d => d.KeyDateType == OrderKeyDateType.Invoiced && d.OrderItemID == null)).Max(d => d.KeyDate),
                    Expression = (C, forSQL) => {
                        var expOrders = Expression.Property(C, "Orders");

                        var selectManyParam = Expression.Parameter(typeof(TransactionHeaderData), "x");

                        var selectManyProp = Expression.Property(selectManyParam, "Dates");

                        var callSelectManyDates = Expression.Call(
                              typeof(Enumerable)
                            , "SelectMany"
                            , new Type[]
                            {
                                  typeof(TransactionHeaderData)
                                , typeof(OrderKeyDate)
                            }
                            , expOrders
                            , Expression.Lambda(
                                  selectManyProp
                                , selectManyParam
                            )
                        );

                        var datesWhereParam = Expression.Parameter(typeof(OrderKeyDate), "d");
                        var keyDateTypeProp = Expression.Property(datesWhereParam, "KeyDateType");
                        var orderItemIDProp = Expression.Property(datesWhereParam, "OrderItemID");

                        var whereClause = Expression.AndAlso(
                              Expression.Equal(keyDateTypeProp, Expression.Constant(OrderKeyDateType.Invoiced, typeof(OrderKeyDateType)))
                            , Expression.Equal(orderItemIDProp, Expression.Constant(null, typeof(int?)))
                        );

                        var callWhere = Expression.Call(
                              typeof(Enumerable)
                            , "Where"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , callSelectManyDates
                            , Expression.Lambda(
                                  whereClause
                                , datesWhereParam
                            )
                        );


                        var keyDateParam = Expression.Parameter(typeof(OrderKeyDate), "d");
                        var keyDateProp = Expression.Property(keyDateParam, "KeyDate");

                        var callMaxDate = Expression.Call(
                              typeof(Enumerable)
                            , "Min"
                            , new Type[]
                            {
                                  typeof(OrderKeyDate)
                                , typeof(DateTime?)
                            }
                            , callWhere
                            , Expression.Lambda(
                                  keyDateProp
                                , keyDateParam
                            )
                        );


                        if (forSQL)
                            return callMaxDate;

                        return Expression.Condition(
                              Expression.Equal(C, Expression.Constant(null, typeof(CompanyData)))
                            , AELHelper.Expressions.NullDateTime
                            , callMaxDate
                        );
                    }
                },
            };
    }
}
