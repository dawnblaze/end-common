﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemStatusMemberInitializer : BaseMemberInitializer
    {
        public OrderItemStatusMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemStatusMemberInitializer> lazy = new Lazy<OrderItemStatusMemberInitializer>(() => new OrderItemStatusMemberInitializer());

        public static OrderItemStatusMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.LineItemStatusCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20130,
                        Text = "Item Status",
                        DataType = DataType.ItemStatus,
                        LinqFx = OI => ((OrderItemData)OI)?.ItemStatusID,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "ItemStatusID", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20131,
                        Text = "Sub status",
                        DataType = DataType.Substatus,
                        LinqFx = OI => ((OrderItemData)OI)?.SubStatusID,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "SubStatusID", forSQL),
                   },
               };
       }
}
