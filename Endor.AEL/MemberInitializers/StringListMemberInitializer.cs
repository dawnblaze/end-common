﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL
{
    public class StringListMemberInitializer : BaseMemberInitializer
    {
        public StringListMemberInitializer() { }

        private static readonly Lazy<StringListMemberInitializer> lazy = new Lazy<StringListMemberInitializer>(() => new StringListMemberInitializer());

        public static StringListMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.StringList;
        public override Type ParentType => typeof(string);
    }
}
