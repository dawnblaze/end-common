﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class CompanyMemberInitializer : BaseMemberInitializer
    {
        public CompanyMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<CompanyMemberInitializer> lazy = new Lazy<CompanyMemberInitializer>(() => new CompanyMemberInitializer());

        public static CompanyMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>
            {
                new PropertyInfo
                {
                    ID = 2000,
                    Text = "Contacts",
                    DataType = DataType.CompanyContactListCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C
                },
                new PropertyInfo
                {
                    ID = 2001,
                    Text = "Employees",
                    DataType = DataType.CompanyEmployeeListCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
                new PropertyInfo
                {
                    ID = 2002,
                    Text = "Dates",
                    DataType = DataType.CompanyDatesCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
                new PropertyInfo
                {
                    ID = 2003,
                    Text = "Time Zone",
                    DataType = DataType.TimeZone,
                    LinqFx = C => ((CompanyData)C)?.TimeZone,
                    Expression = (C, forSQL) =>
                    {
                        var exp = Expression.Property(C, "TimeZone");
                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , Expression.Constant(null, typeof(EnumTimeZone))
                            , exp
                        );
                    },
                },
                new PropertyInfo
                {
                    ID = 2004,
                    Text = "Default Location",
                    DataType = DataType.Location,
                    LinqFx = C => ((CompanyData)C)?.Location,
                    Expression = (C, forSQL) =>
                    {
                        var exp = Expression.Property(C, "Location");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , Expression.Constant(null, typeof(LocationData))
                            , exp
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 2005,
                    Text = "Origin",
                    DataType = DataType.Origin,
                    LinqFx = C => ((CompanyData)C)?.CrmOrigin,
                    Expression = (C, forSQL) =>
                    {
                        var exp = Expression.Property(C, "CrmOrigin");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , Expression.Constant(null, typeof(CrmOrigin))
                            , exp
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 2006,
                    Text = "Industry",
                    DataType = DataType.Industry,
                    LinqFx = C => ((CompanyData) C)?.CrmIndustry,
                    Expression = (C, forSQL) => 
                    {
                        var exp = Expression.Property(C, "CrmIndustry");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , Expression.Constant(null, typeof(CrmIndustry))
                            , exp
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 2007,
                    Text = "Name",
                    DataType = DataType.CompanyCategory,
                    IsDefault = true,
                    LinqFx = C => ((CompanyData)C)?.ID,
                    Expression = (C, forSQL) =>
                    {
                        return AELHelper.Expressions.NumberProperty<CompanyData>(C,"ID",forSQL);
                    },
                },
                new PropertyInfo
                {
                    ID = 2008,
                    Text = "Company Name",
                    DataType = DataType.String,
                    LinqFx = C => ((CompanyData)C)?.Name,
                    Expression = (C, forSQL) =>
                    {
                        var exp = Expression.Property(C, "Name");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , AELHelper.Expressions.NullString
                            , exp
                        );
                    },
                },
                new PropertyInfo
                {
                    ID = 2009,
                    Text = "Type",
                    DataType = DataType.CompanyTypeCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
                new PropertyInfo
                {
                    ID = 2010,
                    Text = "Has Credit Account",
                    DataType = DataType.Boolean,
                    LinqFx = C => ((CompanyData)C)?.HasCreditAccount,
                    Expression = (C, forSQL) => 
                    {
                        var exp = AELHelper.Expressions.ConvertToBoolean(Expression.Property(C, "HasCreditAccount"));
                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , AELHelper.Expressions.NullBoolean
                            , exp
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 2011,
                    Text = "Tax",
                    DataType = DataType.CompanyTaxCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
                new PropertyInfo
                {
                    ID = 2012,
                    Text = "Has Image",
                    DataType = DataType.String,
                    LinqFx = C => ((CompanyData)C)?.HasImage,
                    Expression = (C, forSQL) => 
                    {
                        var exp = AELHelper.Expressions.ConvertToBoolean(Expression.Property(C, "HasImage"));

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , AELHelper.Expressions.NullBoolean
                            , exp
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 2013,
                    Text = "Payment Terms",
                    DataType = DataType.PaymentTerm,
                    LinqFx = C => ((CompanyData)C)?.DefaultPaymentTerms,
                    Expression = (C, forSQL) => 
                    {
                        if (forSQL)
                            return Expression.Property(C, "DefaultPaymentTerms");

                        return Expression.Condition(
                              Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , Expression.Constant(null, typeof(PaymentTerm))
                            , Expression.Property(C, "DefaultPaymentTerms")
                        );
                    }
                },
                new PropertyInfo
                {
                    ID = 2014,
                    Text = "Subsidiaries",
                    DataType = DataType.SubsidiariesCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
                new PropertyInfo
                {
                    ID = 2015,
                    Text = "Customer Credit",
                    DataType = DataType.CustomerCreditCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
                new PropertyInfo
                {
                    ID = 2016,
                    Text = "PO Number",
                    DataType = DataType.PONumberCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
                new PropertyInfo
                {
                    ID = 2017,
                    Text = "Tags",
                    DataType = DataType.CompanyTagsCategory,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
                new PropertyInfo
                {
                    ID = 2018,
                    Text = "Custom Fields",
                    DataType = DataType.CompanyCustomField,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
            };

        public CompanyCustomFieldMemberInitializer CompanyCustomField => CompanyCustomFieldMemberInitializer.Instance;
    }
}
