﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class LocationCategoryMemberInitializer : BaseMemberInitializer
    {
        public LocationCategoryMemberInitializer() { }

        private static readonly Lazy<LocationCategoryMemberInitializer> lazy = new Lazy<LocationCategoryMemberInitializer>(() => new LocationCategoryMemberInitializer());

        public static LocationCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.LocationCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>
            {
                new PropertyInfo
                {
                    ID = 20020,
                    Text = "Sales Location",
                    DataType = DataType.Location,
                    LinqFx = T => ((TransactionHeaderData)T)?.PickupLocationID,
                    Expression = (T, forSQL) =>
                    {
                        return AELHelper.Expressions.NumberProperty<TransactionHeaderData>(T,"PickupLocationID",forSQL);
                    },
                },
                new PropertyInfo
                {
                    ID = 20021,
                    Text = "Production Location",
                    DataType = DataType.Location,
                    LinqFx = T => ((TransactionHeaderData)T)?.ProductionLocationID,
                    Expression = (T, forSQL) =>
                    {
                        return AELHelper.Expressions.NumberProperty<TransactionHeaderData>(T,"ProductionLocationID",forSQL);
                    },
                },
            };
    }
}
