﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class CompanySubsidiaryCategoryMemberInitializer : BaseMemberInitializer
    {
        public CompanySubsidiaryCategoryMemberInitializer() { }

        private static readonly Lazy<CompanySubsidiaryCategoryMemberInitializer> lazy = new Lazy<CompanySubsidiaryCategoryMemberInitializer>(() => new CompanySubsidiaryCategoryMemberInitializer());

        public static CompanySubsidiaryCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.SubsidiariesCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo()
                {
                    ID = 20160,
                    Text = "Has Parent Company",
                    DataType = DataType.Boolean,
                    LinqFx = C => ((CompanyData)C)?.ParentID.HasValue,
                    Expression = (C, forSQL) =>
                    {
                        var expParentID = AELHelper.Expressions.ObjectProperty<CompanyData, int?>(C, "ParentID", forSQL);
                        var expCompareIds = Expression.NotEqual(expParentID, Expression.Constant(null, typeof(int?)));

                        return Expression.Condition(
                            Expression.Equal(C, AELHelper.Expressions.NullCompany)
                            , AELHelper.Expressions.NullBoolean
                            , AELHelper.Expressions.ConvertToBoolean(expCompareIds)
                            );
                    },
                },
                new PropertyInfo()
                {
                    ID = 20161,
                    Text = "Parent Company",
                    DataType = DataType.CompanyCategory,
                    LinqFx = C => ((CompanyData)C)?.ParentCompany,
                    Expression = (C, forSQL) => AELHelper.Expressions.ObjectProperty<CompanyData, CompanyData>(C, "ParentCompany", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20162,
                    Text = "Parent Company Name",
                    DataType = DataType.String,
                    LinqFx = C => ((CompanyData)C)?.ParentCompany?.Name,
                    Expression = (C, forSQL) =>
                    {
                        var expParentCompany = AELHelper.Expressions.ObjectProperty<CompanyData, CompanyData>(C, "ParentCompany", forSQL);

                        return AELHelper.Expressions.ObjectProperty<CompanyData, string>(expParentCompany, "Name", forSQL);
                    },
                },
                new PropertyInfo()
                {
                    ID = 20163,
                    Text = "Subsidiary",
                    DataType = DataType.CompanyList,
                    LinqFx = C => ((CompanyData)C)?.ChildCompanies.ToList(),
                    Expression = (C, forSQL) => AELHelper.Expressions.ObjectListProperty<CompanyData, CompanyData>(C, "ChildCompanies", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20164,
                    Text = "Subsidiary Name",
                    DataType = DataType.StringList,
                    LinqFx = C => ((CompanyData)C)?.ChildCompanies.Select(c => c.Name).ToList(),
                    Expression = (C, forSQL) => AELHelper.Expressions.ObjectListPropertyWithSelect<CompanyData, CompanyData, string>(C, "ChildCompanies", "Name", forSQL),
                },
            };
    }
}
