﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class CompanyTaxCategoryMemberInitializer: BaseMemberInitializer
    {
        public CompanyTaxCategoryMemberInitializer() { }

        private static readonly Lazy<CompanyTaxCategoryMemberInitializer> lazy = new Lazy<CompanyTaxCategoryMemberInitializer>(() => new CompanyTaxCategoryMemberInitializer());

        public static CompanyTaxCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyTaxCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo()
                {
                    ID = 20150,
                    Text = "Tax Exempt",
                    DataType = DataType.Boolean,
                    LinqFx = C => ((CompanyData)C)?.IsTaxExempt,
                    Expression = (C, forSQL) => AELHelper.Expressions.BooleanProperty<CompanyData>(C, "IsTaxExempt", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20151,
                    Text = "Tax Exempt Reason",
                    DataType = DataType.TaxExemptReason,
                    LinqFx = C => ((CompanyData)C)?.TaxExemptReason,
                    Expression = (C, forSQL) => AELHelper.Expressions.ObjectProperty<CompanyData, FlatListItem>(C, "TaxExemptReason", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20152,
                    Text = "Tax Group",
                    DataType = DataType.TaxGroup,
                    LinqFx = C => ((CompanyData)C)?.TaxGroup,
                    Expression = (C, forSQL) => AELHelper.Expressions.ObjectProperty<CompanyData, TaxGroup>(C, "TaxGroup", forSQL),
                },
            };
    }
}
