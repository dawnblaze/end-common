﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Endor.AEL.MemberInitializers
{
    public class ContactCustomFieldMemberInitializer : BaseMemberInitializer
    {
        public ContactCustomFieldMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<ContactCustomFieldMemberInitializer> lazy = new Lazy<ContactCustomFieldMemberInitializer>(() => new ContactCustomFieldMemberInitializer());

        public static ContactCustomFieldMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.ContactCustomField;

        public override Type ParentType => typeof(ContactCustomData);

        public override List<PropertyInfo> BIDProperties(IDataProvider dataProvider)
        {
            List<PropertyInfo> result = new List<PropertyInfo>();

            List<CustomFieldDefinition> customFields = dataProvider.GetData<CustomFieldDefinition>()
                                                                   .Where(c => c.AppliesToClassTypeID == ClassType.Contact.ID())
                                                                   .Where(c => c.DataType == DataType.Boolean || c.DataType == DataType.String || c.DataType == DataType.Number)
                                                                   .OrderBy(c => c.Name.ToLower())
                                                                   .ToList();

            foreach (CustomFieldDefinition customField in customFields)
            {
                Func<dynamic, dynamic> linqFx;
                Func<Expression, bool, Expression> expression;

                switch (customField.DataType)
                {
                    case DataType.Number:
                        linqFx = C => ((ContactData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.ValueAsNumber;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<ContactData, ContactCustomDataValue>(
                                C
                                , nameof(ContactData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(ContactCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(ContactCustomDataValue.ValueAsNumber));
                        };
                        break;

                    case DataType.Boolean:
                        linqFx = C => ((ContactData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.ValueAsBoolean;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<ContactData, ContactCustomDataValue>(
                                C
                                , nameof(ContactData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(ContactCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(ContactCustomDataValue.ValueAsBoolean));
                        };
                        break;

                    case DataType.String:
                    default:
                        linqFx = C => ((ContactData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.Value;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<ContactData, ContactCustomDataValue>(
                                C
                                , nameof(ContactData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(ContactCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(ContactCustomDataValue.Value));
                        };
                        break;
                }

                result.Add(
                    new PropertyInfo()
                    {
                        ID = 20350,
                        RelatedID = customField.ID,
                        Text = customField.Name,
                        DataType = customField.DataType,
                        LinqFx = linqFx,
                        Expression = expression,
                    }
                );
            }

            return result;
        }
    }
}
