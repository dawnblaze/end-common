﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Endor.AEL
{
    public class NumberListMemberInitializer : BaseMemberInitializer
    {
        public NumberListMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<NumberListMemberInitializer> lazy = new Lazy<NumberListMemberInitializer>(() => new NumberListMemberInitializer());

        public static NumberListMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.NumberList;
        public override Type ParentType => typeof(decimal);
    }
}
