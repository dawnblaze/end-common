﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml.Linq;

namespace Endor.AEL.MemberInitializers
{
    public class EstimateCustomFieldMemberInitializer : BaseMemberInitializer
    {
        public EstimateCustomFieldMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<EstimateCustomFieldMemberInitializer> lazy = new Lazy<EstimateCustomFieldMemberInitializer>(() => new EstimateCustomFieldMemberInitializer());

        public static EstimateCustomFieldMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.EstimateCustomField;

        public override Type ParentType => typeof(TransactionHeaderCustomDataValue);

        public override List<PropertyInfo> BIDProperties(IDataProvider dataProvider)
        {
            List<PropertyInfo> result = new List<PropertyInfo>();

            List<CustomFieldDefinition> customFields = dataProvider.GetData<CustomFieldDefinition>()
                                                                   .Where(c => c.AppliesToClassTypeID == ClassType.Estimate.ID())
                                                                   .Where(c => c.DataType == DataType.Boolean || c.DataType == DataType.String || c.DataType == DataType.Number)
                                                                   .OrderBy(c => c.Name.ToLower())
                                                                   .ToList();

            foreach (CustomFieldDefinition customField in customFields)
            {
                Func<dynamic, dynamic> linqFx;
                Func<Expression, bool, Expression> expression;

                switch (customField.DataType)
                {
                    case DataType.Number:
                        linqFx = C => ((EstimateData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.ValueAsNumber;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<EstimateData, TransactionHeaderCustomDataValue>(
                                C
                                , nameof(EstimateData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(TransactionHeaderCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(TransactionHeaderCustomDataValue.ValueAsNumber));
                        };
                        break;

                    case DataType.Boolean:
                        linqFx = C => ((EstimateData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.ValueAsBoolean;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<EstimateData, TransactionHeaderCustomDataValue>(
                                C
                                , nameof(EstimateData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(TransactionHeaderCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(TransactionHeaderCustomDataValue.ValueAsBoolean));
                        };
                        break;

                    case DataType.String:
                    default:
                        linqFx = C => ((EstimateData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.Value;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<EstimateData, TransactionHeaderCustomDataValue>(
                                C
                                , nameof(EstimateData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(TransactionHeaderCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(TransactionHeaderCustomDataValue.Value));
                        };
                        break;
                }

                result.Add(
                    new PropertyInfo()
                    {
                        ID = 20310,
                        RelatedID = customField.ID,
                        Text = customField.Name,
                        DataType = customField.DataType,
                        LinqFx = linqFx,
                        Expression = expression,
                    }
                );
            }

            return result;
        }
    }
}
