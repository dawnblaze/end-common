﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Endor.AEL
{
    public class DateTimeMemberInitializer : BaseMemberInitializer
    {
        public DateTimeMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<DateTimeMemberInitializer> lazy = new Lazy<DateTimeMemberInitializer>(() => new DateTimeMemberInitializer());

        public static DateTimeMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.DateTime;
        public override Type ParentType => typeof(DateTime);
    }
}
