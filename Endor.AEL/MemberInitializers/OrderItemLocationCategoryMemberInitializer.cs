﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemLocationCategoryMemberInitializer : BaseMemberInitializer
    {
        public OrderItemLocationCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemLocationCategoryMemberInitializer> lazy = new Lazy<OrderItemLocationCategoryMemberInitializer>(() => new OrderItemLocationCategoryMemberInitializer());

        public static OrderItemLocationCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemLocationCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20380,
                        Text = "Sales Location",
                        DataType = DataType.Location,
                        LinqFx = OI => ((OrderItemData)OI)?.Order?.PickupLocationID,
                        Expression = (OI, forSQL) =>
                        {
                            var expOrder = AELHelper.Expressions.ObjectProperty<OrderItemData, TransactionHeaderData>(OI, "Order", forSQL);
                            return AELHelper.Expressions.NumberProperty<TransactionHeaderData>(expOrder, "PickupLocationID", forSQL);
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20381,
                        Text = "Production Location",
                        DataType = DataType.Location,
                        LinqFx = OI => ((OrderItemData)OI)?.ProductionLocationID,
                        Expression = (OI, forSQL) =>
                        {
                            return AELHelper.Expressions.NumberProperty<OrderItemData>(OI,"ProductionLocationID",forSQL);
                        },
                   },
               };
    }
}
