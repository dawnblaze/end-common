﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class EstimateMemberInitializer : BaseMemberInitializer
    {
        public EstimateMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<EstimateMemberInitializer> lazy = new Lazy<EstimateMemberInitializer>(() => new EstimateMemberInitializer());

        public static EstimateMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.EstimateCategory;
        public override Type ParentType => typeof(EstimateData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo()
                {
                    ID = 10200,
                    Text = "Line Items",
                    DataType = DataType.LineItemCategory,
                    LinqFx = E => ((EstimateData)E)?.Items?.ToList(),
                    Expression = (E, forSQL) => AELHelper.Expressions.ObjectListProperty<EstimateData, OrderItemData>(E, "Items", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 10201,
                    Text = "Contacts",
                    DataType = DataType.TransactionHeaderContactRoleCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10202,
                    Text = "Employees",
                    DataType = DataType.TransactionHeaderEmployeeRoleListCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10203,
                    Text = "Locations",
                    DataType = DataType.LocationCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10204,
                    Text = "Estimate Origin",
                    DataType = DataType.Origin,
                    LinqFx = E => ((EstimateData)E)?.Origin,
                    Expression = (E, forSQL) => {
                        var exp = Expression.Property(E, "Origin");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(E, AELHelper.Expressions.NullOrder)
                            , Expression.Constant(null, typeof(CrmOrigin))
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10205,
                    Text = "Estimate Number",
                    DataType = DataType.Number,
                    NumberType = NumberType.Integer,
                    LinqFx = E => ((EstimateData)E)?.Number,
                    Expression = (E, forSQL) => {
                        var exp = AELHelper.Expressions.ConvertToNumber(Expression.Property(E, "Number"));

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(E, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullNumber
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10206,
                    Text = "Description",
                    DataType = DataType.String,
                    LinqFx = E => ((EstimateData)E)?.Description,
                    Expression = (E, forSQL) => {
                        var exp = Expression.Property(E, "Description");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(E, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullString
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10207,
                    Text = "Company",
                    DataType = DataType.CompanyCategory,
                    LinqFx = E => ((EstimateData)E)?.Company,
                    Expression = (E, forSQL) => {
                        var exp = Expression.Property(E, "Company");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(E, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullCompany
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10208,
                    Text = "Status",
                    DataType = DataType.OrderStatus,
                    LinqFx = E => (decimal?)((EstimateData)E)?.OrderStatusID,
                    Expression = (E, forSQL) => {
                        var exp = Expression.Convert(
                              Expression.Convert(
                                  Expression.Property(E, "OrderStatusID")
                                , typeof(byte?))
                            , typeof(decimal?));

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(E, AELHelper.Expressions.NullOrder)
                            , Expression.Constant(null, typeof(decimal?))
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10209,
                    Text = "Dates",
                    DataType = DataType.EstimateDateCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10210,
                    Text = "Taxes",
                    DataType = DataType.TaxesCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10211,
                    Text = "Prices",
                    DataType = DataType.PricesCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10212,
                    Text = "Costs",
                    DataType = DataType.CostCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10213,
                    Text = "PO Number",
                    DataType = DataType.String,
                    LinqFx = E => ((EstimateData)E)?.OrderPONumber,
                    Expression = (E, forSQL) => {
                        var exp = Expression.Property(E, "OrderPONumber");

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(E, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullString
                            , exp
                        );
                    },
                },
                new PropertyInfo()
                {
                    ID = 10214,
                    Text = "Tags",
                    DataType = DataType.TransactionHeaderTagsCategory,
                    LinqFx = E => E,
                    Expression = (E, forSQL) => E,
                },
                new PropertyInfo()
                {
                    ID = 10215,
                    Text = "Notes",
                    DataType = DataType.TransactionHeaderNotesCategory,
                    LinqFx = E => ((EstimateData)E)?.Notes?.ToList(),
                    Expression = (E, forSQL) => AELHelper.Expressions.ObjectListProperty<EstimateData, OrderNote>(E, "Notes", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 10216,
                    Text = "Has Documents",
                    DataType = DataType.Boolean,
                    LinqFx = E => ((EstimateData)E)?.HasDocuments,
                    Expression = (E, forSQL) => {
                        var exp = AELHelper.Expressions.ConvertToBoolean(Expression.Property(E, "HasDocuments"));

                        if (forSQL)
                            return exp;

                        return Expression.Condition(
                              Expression.Equal(E, AELHelper.Expressions.NullOrder)
                            , AELHelper.Expressions.NullBoolean
                            , exp
                        );
                    },
                },
                new PropertyInfo
                {
                    ID = 10217,
                    Text = "Custom Fields",
                    DataType = DataType.EstimateCustomField,
                    LinqFx = O => O,
                    Expression = (O, forSQL) => O,
                },
            };

        public EstimateCustomFieldMemberInitializer CompanyCustomField => EstimateCustomFieldMemberInitializer.Instance;
    }
}
