﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class CompanyTagsCategoryMemberInitializer : BaseMemberInitializer
    {
        public CompanyTagsCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<CompanyTagsCategoryMemberInitializer> lazy = new Lazy<CompanyTagsCategoryMemberInitializer>(() => new CompanyTagsCategoryMemberInitializer());

        public static CompanyTagsCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyTagsCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20111,
                        Text = "Has Tags",
                        DataType = DataType.Boolean,
                        LinqFx = C => ((CompanyData)C)?.TagLinks?.Any(),
                        Expression = (C, forSQL) => {
                            Expression tagLinks = Expression.Property(C, "TagLinks");

                            var callAny = Expression.Call(
                                typeof(Enumerable)
                                , "Any"
                                , new Type[]{
                                    typeof(CompanyTagLink)
                                }
                                , tagLinks
                                );

                            if (forSQL)
                                return callAny;

                            var expCheckTags = Expression.Condition(
                                Expression.Equal(tagLinks, Expression.Constant(null, typeof(ICollection<CompanyTagLink>)))
                                , AELHelper.Expressions.BooleanFalse
                                , callAny
                                );

                            return Expression.Condition(
                                Expression.Equal(C, Expression.Constant(null, typeof(CompanyData)))
                                , AELHelper.Expressions.NullBoolean
                                , AELHelper.Expressions.ConvertToBoolean(expCheckTags)
                                );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20112,
                        Text = "Tag Name",
                        DataType = DataType.StringList,
                        LinqFx = C => ((CompanyData)C)?.TagLinks?.Select(t => t.Tag?.Name).ToList(),
                        Expression = (C, forSQL) =>
                        {
                            var expTagLinks = Expression.Property(C, "TagLinks");

                            var expTags = AELHelper.Expressions.SelectFromObjectList<CompanyTagLink, ListTag>(expTagLinks, "Tag", forSQL);
                            var expTagNames = AELHelper.Expressions.SelectFromObjectList<ListTag, string>(expTags, "Name", forSQL);

                            return Expression.Condition(
                                  Expression.Equal(C, Expression.Constant(null, typeof(CompanyData)))
                                , Expression.Constant(null, typeof(List<string>))
                                , expTagNames
                            );
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20113,
                        Text = "Tag Color",
                        DataType = DataType.String,
                        LinqFx = C => ((CompanyData)C)?.TagLinks?.Select(t => t.Tag?.RGB).ToList(),
                        Expression = (C, forSQL) =>
                        {
                            var expTagLinks = Expression.Property(C, "TagLinks");

                            var expTags = AELHelper.Expressions.SelectFromObjectList<CompanyTagLink, ListTag>(expTagLinks, "Tag", forSQL);
                            var expTagNames = AELHelper.Expressions.SelectFromObjectList<ListTag, string>(expTags, "RGB", forSQL);

                            return Expression.Condition(
                                  Expression.Equal(C, Expression.Constant(null, typeof(CompanyData)))
                                , Expression.Constant(null, typeof(List<string>))
                                , expTagNames
                            );
                        },
                   },
               };
       }
}
