﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class OrderDateCategoryMemberInitializer : BaseMemberInitializer
    {
        public OrderDateCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderDateCategoryMemberInitializer> lazy = new Lazy<OrderDateCategoryMemberInitializer>(() => new OrderDateCategoryMemberInitializer());

        public static OrderDateCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderDateCategory;
        public override Type ParentType => typeof(OrderData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>
            {
                new PropertyInfo
                {
                    ID = 21260,
                    Text = "Current Status Date",
                    DataType = DataType.DateTime,
                    LinqFx = O => ((OrderData)O)?.OrderStatusStartDT,
                    Expression = (O, forSQL) => AELHelper.Expressions.DateTimeProperty<OrderData>(O, "OrderStatusStartDT", forSQL),
                },
                new PropertyInfo
                {
                    ID = 21261,
                    Text = "Created Date",
                    DataType = DataType.DateTime,
                    LinqFx = O => ((OrderData)O)?.Dates?.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.Created).KeyDate,
                    Expression = (O, forSQL) => {
                        var paramFirstOrDefault = Expression.Parameter(typeof(OrderKeyDate), "k");
                        var conditionWhere = Expression.Equal(
                              Expression.Property(paramFirstOrDefault, "KeyDateType")
                            , Expression.Constant(OrderKeyDateType.Created, typeof(OrderKeyDateType))
                        );

                        var datesProp = Expression.Property(O, "Dates");
                        var callFirstOrDefault = Expression.Call(
                              typeof(Enumerable)
                            , "FirstOrDefault"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , datesProp
                            , Expression.Lambda(
                                  conditionWhere
                                , paramFirstOrDefault
                            )
                        );

                        var keyDateProp = Expression.Property(callFirstOrDefault, "KeyDate");

                        if (forSQL)
                            return keyDateProp;

                        var resultEqualNull = Expression.Equal(
                              callFirstOrDefault
                            , Expression.Constant(null, typeof(OrderKeyDate))
                        );

                        var resultNullCheck = Expression.Condition(
                              resultEqualNull
                            , AELHelper.Expressions.NullDateTime
                            , keyDateProp
                        );

                        var equalNull = Expression.Equal(
                              datesProp
                            , Expression.Constant(null, typeof(List<OrderKeyDate>))
                        );

                        var keyDatesNullCheck = Expression.Condition(
                              equalNull
                            , AELHelper.Expressions.NullDateTime
                            , resultNullCheck
                        );

                        return Expression.Condition(
                              Expression.Equal(O, Expression.Constant(null, typeof(OrderData)))
                            , AELHelper.Expressions.NullDateTime
                            , keyDatesNullCheck
                        );
                    },
                },
                new PropertyInfo
                {
                    ID = 21262,
                    Text = "Order Send Date",
                    DataType = DataType.DateTime,
                    LinqFx = O => ((OrderData)O)?.Dates?.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.ProposalDue).KeyDate,
                    Expression = (O, forSQL) => {
                        var paramFirstOrDefault = Expression.Parameter(typeof(OrderKeyDate), "k");
                        var conditionWhere = Expression.Equal(
                              Expression.Property(paramFirstOrDefault, "KeyDateType")
                            , Expression.Constant(OrderKeyDateType.ProposalDue, typeof(OrderKeyDateType))
                        );

                        var datesProp = Expression.Property(O, "Dates");
                        var callFirstOrDefault = Expression.Call(
                              typeof(Enumerable)
                            , "FirstOrDefault"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , datesProp
                            , Expression.Lambda(
                                  conditionWhere
                                , paramFirstOrDefault
                            )
                        );

                        var keyDateProp = Expression.Property(callFirstOrDefault, "KeyDate");

                        if (forSQL)
                            return keyDateProp;

                        var resultEqualNull = Expression.Equal(
                              callFirstOrDefault
                            , Expression.Constant(null, typeof(OrderKeyDate))
                        );

                        var resultNullCheck = Expression.Condition(
                              resultEqualNull
                            , AELHelper.Expressions.NullDateTime
                            , keyDateProp
                        );

                        var equalNull = Expression.Equal(
                              datesProp
                            , Expression.Constant(null, typeof(List<OrderKeyDate>))
                        );

                        var keyDatesNullCheck = Expression.Condition(
                              equalNull
                            , AELHelper.Expressions.NullDateTime
                            , resultNullCheck
                        );

                        return Expression.Condition(
                              Expression.Equal(O, Expression.Constant(null, typeof(OrderData)))
                            , AELHelper.Expressions.NullDateTime
                            , keyDatesNullCheck
                        );
                    },
                },
                new PropertyInfo
                {
                    ID = 21263,
                    Text = "Voided Date",
                    DataType = DataType.DateTime,
                    LinqFx = O => ((OrderData)O)?.Dates?.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.Voided).KeyDate,
                    Expression = (O, forSQL) => {
                        var paramFirstOrDefault = Expression.Parameter(typeof(OrderKeyDate), "k");
                        var conditionWhere = Expression.Equal(
                              Expression.Property(paramFirstOrDefault, "KeyDateType")
                            , Expression.Constant(OrderKeyDateType.Voided, typeof(OrderKeyDateType))
                        );

                        var datesProp = Expression.Property(O, "Dates");
                        var callFirstOrDefault = Expression.Call(
                              typeof(Enumerable)
                            , "FirstOrDefault"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , datesProp
                            , Expression.Lambda(
                                  conditionWhere
                                , paramFirstOrDefault
                            )
                        );

                        var keyDateProp = Expression.Property(callFirstOrDefault, "KeyDate");

                        if (forSQL)
                            return keyDateProp;

                        var resultEqualNull = Expression.Equal(
                              callFirstOrDefault
                            , Expression.Constant(null, typeof(OrderKeyDate))
                        );

                        var resultNullCheck = Expression.Condition(
                              resultEqualNull
                            , AELHelper.Expressions.NullDateTime
                            , keyDateProp
                        );

                        var equalNull = Expression.Equal(
                              datesProp
                            , Expression.Constant(null, typeof(List<OrderKeyDate>))
                        );

                        var keyDatesNullCheck = Expression.Condition(
                              equalNull
                            , AELHelper.Expressions.NullDateTime
                            , resultNullCheck
                        );

                        return Expression.Condition(
                              Expression.Equal(O, Expression.Constant(null, typeof(OrderData)))
                            , AELHelper.Expressions.NullDateTime
                            , keyDatesNullCheck
                        );
                    },
                },
            };

    }
}
