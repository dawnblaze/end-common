﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class EstimateDateCategoryMemberInitializer : BaseMemberInitializer
    {
        public EstimateDateCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<EstimateDateCategoryMemberInitializer> lazy = new Lazy<EstimateDateCategoryMemberInitializer>(() => new EstimateDateCategoryMemberInitializer());

        public static EstimateDateCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.EstimateDateCategory;
        public override Type ParentType => typeof(EstimateData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>
            {
                new PropertyInfo
                {
                    ID = 20260,
                    Text = "Current Status Date",
                    DataType = DataType.DateTime,
                    LinqFx = E => ((EstimateData)E)?.OrderStatusStartDT,
                    Expression = (E, forSQL) => AELHelper.Expressions.DateTimeProperty<EstimateData>(E, "OrderStatusStartDT", forSQL),
                },
                new PropertyInfo
                {
                    ID = 20261,
                    Text = "Created Date",
                    DataType = DataType.DateTime,
                    LinqFx = E => ((EstimateData)E)?.Dates?.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.Created).KeyDate,
                    Expression = (E, forSQL) => {
                        var paramFirstOrDefault = Expression.Parameter(typeof(OrderKeyDate), "k");
                        var conditionWhere = Expression.Equal(
                              Expression.Property(paramFirstOrDefault, "KeyDateType")
                            , Expression.Constant(OrderKeyDateType.Created, typeof(OrderKeyDateType))
                        );

                        var datesProp = Expression.Property(E, "Dates");
                        var callFirstOrDefault = Expression.Call(
                              typeof(Enumerable)
                            , "FirstOrDefault"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , datesProp
                            , Expression.Lambda(
                                  conditionWhere
                                , paramFirstOrDefault
                            )
                        );

                        var keyDateProp = Expression.Property(callFirstOrDefault, "KeyDate");

                        if (forSQL)
                            return keyDateProp;

                        var resultEqualNull = Expression.Equal(
                              callFirstOrDefault
                            , Expression.Constant(null, typeof(OrderKeyDate))
                        );

                        var resultNullCheck = Expression.Condition(
                              resultEqualNull
                            , AELHelper.Expressions.NullDateTime
                            , keyDateProp
                        );

                        var equalNull = Expression.Equal(
                              datesProp
                            , Expression.Constant(null, typeof(List<OrderKeyDate>))
                        );

                        var keyDatesNullCheck = Expression.Condition(
                              equalNull
                            , AELHelper.Expressions.NullDateTime
                            , resultNullCheck
                        );

                        return Expression.Condition(
                              Expression.Equal(E, Expression.Constant(null, typeof(EstimateData)))
                            , AELHelper.Expressions.NullDateTime
                            , keyDatesNullCheck
                        );
                    },
                },
                new PropertyInfo
                {
                    ID = 20262,
                    Text = "Estimate Send Date",
                    DataType = DataType.DateTime,
                    LinqFx = E => ((EstimateData)E)?.Dates?.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.ProposalDue).KeyDate,
                    Expression = (E, forSQL) => {
                        var paramFirstOrDefault = Expression.Parameter(typeof(OrderKeyDate), "k");
                        var conditionWhere = Expression.Equal(
                              Expression.Property(paramFirstOrDefault, "KeyDateType")
                            , Expression.Constant(OrderKeyDateType.ProposalDue, typeof(OrderKeyDateType))
                        );

                        var datesProp = Expression.Property(E, "Dates");
                        var callFirstOrDefault = Expression.Call(
                              typeof(Enumerable)
                            , "FirstOrDefault"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , datesProp
                            , Expression.Lambda(
                                  conditionWhere
                                , paramFirstOrDefault
                            )
                        );

                        var keyDateProp = Expression.Property(callFirstOrDefault, "KeyDate");

                        if (forSQL)
                            return keyDateProp;

                        var resultEqualNull = Expression.Equal(
                              callFirstOrDefault
                            , Expression.Constant(null, typeof(OrderKeyDate))
                        );

                        var resultNullCheck = Expression.Condition(
                              resultEqualNull
                            , AELHelper.Expressions.NullDateTime
                            , keyDateProp
                        );

                        var equalNull = Expression.Equal(
                              datesProp
                            , Expression.Constant(null, typeof(List<OrderKeyDate>))
                        );

                        var keyDatesNullCheck = Expression.Condition(
                              equalNull
                            , AELHelper.Expressions.NullDateTime
                            , resultNullCheck
                        );

                        return Expression.Condition(
                              Expression.Equal(E, Expression.Constant(null, typeof(EstimateData)))
                            , AELHelper.Expressions.NullDateTime
                            , keyDatesNullCheck
                        );
                    },
                },
                new PropertyInfo
                {
                    ID = 20263,
                    Text = "Voided Date",
                    DataType = DataType.DateTime,
                    LinqFx = E => ((EstimateData)E)?.Dates?.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.Voided).KeyDate,
                    Expression = (E, forSQL) => {
                        var paramFirstOrDefault = Expression.Parameter(typeof(OrderKeyDate), "k");
                        var conditionWhere = Expression.Equal(
                              Expression.Property(paramFirstOrDefault, "KeyDateType")
                            , Expression.Constant(OrderKeyDateType.Voided, typeof(OrderKeyDateType))
                        );

                        var datesProp = Expression.Property(E, "Dates");
                        var callFirstOrDefault = Expression.Call(
                              typeof(Enumerable)
                            , "FirstOrDefault"
                            , new Type[]
                            {
                                typeof(OrderKeyDate)
                            }
                            , datesProp
                            , Expression.Lambda(
                                  conditionWhere
                                , paramFirstOrDefault
                            )
                        );

                        var keyDateProp = Expression.Property(callFirstOrDefault, "KeyDate");

                        if (forSQL)
                            return keyDateProp;

                        var resultEqualNull = Expression.Equal(
                              callFirstOrDefault
                            , Expression.Constant(null, typeof(OrderKeyDate))
                        );

                        var resultNullCheck = Expression.Condition(
                              resultEqualNull
                            , AELHelper.Expressions.NullDateTime
                            , keyDateProp
                        );

                        var equalNull = Expression.Equal(
                              datesProp
                            , Expression.Constant(null, typeof(List<OrderKeyDate>))
                        );

                        var keyDatesNullCheck = Expression.Condition(
                              equalNull
                            , AELHelper.Expressions.NullDateTime
                            , resultNullCheck
                        );

                        return Expression.Condition(
                              Expression.Equal(E, Expression.Constant(null, typeof(EstimateData)))
                            , AELHelper.Expressions.NullDateTime
                            , keyDatesNullCheck
                        );
                    },
                },
            };

    }
}
