﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.AEL.MemberInitializers
{
    public class UserMemberInitializer : BaseMemberInitializer
    {
        private static readonly Lazy<UserMemberInitializer> lazy = new Lazy<UserMemberInitializer>(() => new UserMemberInitializer());

        public static UserMemberInitializer Instance => lazy.Value;
        public override DataType DataType => DataType.UserLink;

        public override Type ParentType => typeof(UserLink);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo
                {
                    ID = 1010,
                    Text = "Name",
                    DataType = DataType.String,
                    LinqFx = C => ((UserLink)C)?.DisplayName,
                    Expression = (U, forSQL) => AELHelper.Expressions.StringProperty<UserLink>(U, "DisplayName", forSQL),                                        
                },
                new PropertyInfo
                {
                    ID = 1011,
                    Text = "UserName",
                    DataType = DataType.String,
                    LinqFx = C => ((UserLink)C)?.UserName,
                    Expression = (U, forSQL) => AELHelper.Expressions.StringProperty<UserLink>(U, "UserName", forSQL),
                }
            };
    }
}
