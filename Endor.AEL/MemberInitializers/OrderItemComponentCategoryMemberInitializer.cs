﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.ObjectModel;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemComponentCategoryMemberInitializer : BaseMemberInitializer
    {
        public OrderItemComponentCategoryMemberInitializer() { }

        private static readonly Lazy<OrderItemComponentCategoryMemberInitializer> lazy = new Lazy<OrderItemComponentCategoryMemberInitializer>(() => new OrderItemComponentCategoryMemberInitializer());

        public static OrderItemComponentCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemComponentCategory;
        public override Type ParentType => typeof(OrderItemComponent);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>
            {
                new PropertyInfo
                {
                    ID = 20280,
                    Text = "Component Name",
                    DataType = DataType.String,
                    LinqFx = OIC => ((OrderItemComponent)OIC)?.Name,
                    Expression = (OIC, forSQL) => AELHelper.Expressions.StringProperty<OrderItemComponent>(OIC, "Name", forSQL),
                },
                new PropertyInfo
                {
                    ID = 20281,
                    Text = "Quantity",
                    DataType = DataType.Number,
                    NumberType = NumberType.Decimal,
                    LinqFx = OIC => ((OrderItemComponent)OIC)?.TotalQuantity,
                    Expression = (OIC, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemComponent>(OIC, "Quantity", forSQL),
                },
                new PropertyInfo
                {
                    ID = 20282,
                    Text = "Unit Price",
                    DataType = DataType.Number,
                    NumberType = NumberType.Currency,
                    LinqFx = OIC => ((OrderItemComponent)OIC)?.PriceUnit,
                    Expression = (OIC, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemComponent>(OIC, "PriceUnit", forSQL),
                },
                new PropertyInfo
                {
                    ID = 20283,
                    Text = "Price",
                    DataType = DataType.Number,
                    NumberType = NumberType.Currency,
                    LinqFx = OIC => ((OrderItemComponent)OIC)?.PricePreTax,
                    Expression = (OIC, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemComponent>(OIC, "PricePreTax", forSQL),
                },
                new PropertyInfo
                {
                    ID = 20284,
                    Text = "Is Overridden",
                    DataType = DataType.Boolean,
                    LinqFx = OIC => ((OrderItemComponent)OIC)?.PriceUnitOV,
                    Expression = (OIC, forSQL) => AELHelper.Expressions.BooleanProperty<OrderItemComponent>(OIC, "PriceUnitOV", forSQL),
                },
                new PropertyInfo
                {
                    ID = 20285,
                    Text = "Component Type",
                    DataType = DataType.String,
                    LinqFx = OIC => ((OrderItemComponent)OIC)?.Description,
                    Expression = (OIC, forSQL) => AELHelper.Expressions.StringProperty<OrderItemComponent>(OIC, "Description", forSQL),
                },

            };
    }
}
