﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Xml.Linq;

namespace Endor.AEL.MemberInitializers
{
    public class CompanyCustomFieldMemberInitializer : BaseMemberInitializer
    {
        public CompanyCustomFieldMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<CompanyCustomFieldMemberInitializer> lazy = new Lazy<CompanyCustomFieldMemberInitializer>(() => new CompanyCustomFieldMemberInitializer());

        public static CompanyCustomFieldMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyCustomField;

        public override Type ParentType => typeof(CompanyCustomData);

        public override List<PropertyInfo> BIDProperties(IDataProvider dataProvider)
        {
            List<PropertyInfo> result = new List<PropertyInfo>();

            List<CustomFieldDefinition> customFields = dataProvider.GetData<CustomFieldDefinition>()
                                                                   .Where(c => c.AppliesToClassTypeID == ClassType.Company.ID())
                                                                   .Where(c => c.DataType == DataType.Boolean || c.DataType == DataType.String || c.DataType == DataType.Number)
                                                                   .OrderBy(c => c.Name.ToLower())
                                                                   .ToList();

            foreach (CustomFieldDefinition customField in customFields)
            {
                Func<dynamic, dynamic> linqFx;
                Func<Expression, bool, Expression> expression;

                switch (customField.DataType)
                {
                    case DataType.Number:
                        linqFx = C => ((CompanyData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.ValueAsNumber;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<CompanyData, CompanyCustomDataValue>(
                                C
                                , nameof(CompanyData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(CompanyCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(CompanyCustomDataValue.ValueAsNumber));
                        };
                        break;

                    case DataType.Boolean:
                        linqFx = C => ((CompanyData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.ValueAsBoolean;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<CompanyData, CompanyCustomDataValue>(
                                C
                                , nameof(CompanyData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(CompanyCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(CompanyCustomDataValue.ValueAsBoolean));
                        };
                        break;

                    case DataType.String:
                    default:
                        linqFx = C => ((CompanyData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.Value;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<CompanyData, CompanyCustomDataValue>(
                                C
                                , nameof(CompanyData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(CompanyCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(CompanyCustomDataValue.Value));
                        };
                        break;
                }

                result.Add(
                    new PropertyInfo()
                    {
                        ID = 20330,
                        RelatedID = customField.ID,
                        Text = customField.Name,
                        DataType = customField.DataType,
                        LinqFx = linqFx,
                        Expression = expression,
                    }
                );
            }
            
            return result;
        }
    }
}
