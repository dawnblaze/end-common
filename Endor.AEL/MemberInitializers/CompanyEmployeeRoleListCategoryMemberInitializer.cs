﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;
namespace Endor.AEL.MemberInitializers
{
    public class CompanyEmployeeRoleListCategoryMemberInitializer : BaseMemberInitializer
    {
        public CompanyEmployeeRoleListCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<CompanyEmployeeRoleListCategoryMemberInitializer> lazy = new Lazy<CompanyEmployeeRoleListCategoryMemberInitializer>(() => new CompanyEmployeeRoleListCategoryMemberInitializer());

        public static CompanyEmployeeRoleListCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyEmployeeRoleListCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 11110,
                        Text = "Employees in Any Role",
                        DataType = DataType.EmployeeCategory,
                        LinqFx = C => ((CompanyData)C)?.EmployeeTeam?.EmployeeTeamLinks?
                                        .Select(r => r.Employee)
                                        .Distinct()
                                        .ToList(),
                        Expression = (C, forSQL) => AELHelper.Expressions.ObjectListPropertyWithSelect<CompanyData, EmployeeTeamLink, EmployeeData>(C, "EmployeeTeam.EmployeeTeamLinks", "Employee", forSQL),
                   },
               };

        public override List<PropertyInfo> BIDProperties(IDataProvider dataProvider)
        {
            List<PropertyInfo> result = new List<PropertyInfo>();

            IQueryable<EmployeeRole> roles = dataProvider.GetData<EmployeeRole>()
                                                         .Where(x => x.ID != SystemIDs.EmployeeRole.AssignedTo)
                                                         .OrderBy(x => x.ID != SystemIDs.EmployeeRole.Salesperson)
                                                         .ThenBy(x => x.Name);

            if (roles != null)
            {
                foreach (EmployeeRole role in roles)
                {
                    result.Add(
                           new PropertyInfo()
                           {
                               ID = 11111,
                               RelatedID = role.ID,
                               Text = role.Name,
                               DataType = DataType.EmployeeCategory,
                               LinqFx = C => ((CompanyData)C)?.EmployeeTeam?.EmployeeTeamLinks?
                                   .Where(r => r.RoleID == role.ID)?.Select(x => x.Employee).ToList(),
                               Expression = (C, forSQL) => AELHelper.Expressions
                                       .ObjectListPropertyWithWhereSelect<CompanyData, EmployeeTeamLink, EmployeeData>(
                                             C
                                           , "EmployeeTeam.EmployeeTeamLinks"
                                           , "Employee"
                                           , forSQL
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "RoleID", WhereValue = Expression.Constant(role.ID, typeof(short)) }
                                       ),
                           }
                        );

                }
            }

            return result;
        }
    }
}
