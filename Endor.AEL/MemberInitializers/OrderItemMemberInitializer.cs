﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemMemberInitializer : BaseMemberInitializer
    {
        public OrderItemMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemMemberInitializer> lazy = new Lazy<OrderItemMemberInitializer>(() => new OrderItemMemberInitializer());

        public static OrderItemMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.LineItemCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 10020,
                        Text = "Setup Fees",
                        DataType = DataType.NumberList,
                        LinqFx = OI => ((OrderItemData)OI)?.Surcharges?.ToList(),
                        //Expression = (OI, forSQL) => AELHelper.Expressions.ObjectListProperty<OrderItemData, OrderItemSurcharge>(OI, "Surcharges", forSQL),
                        Expression = (OI, forSQL) => {
                            var orderItemSurcharge =
                                AELHelper.Expressions.ObjectListPropertyWithWhereSelect
                                    <OrderItemData, OrderItemSurcharge, Decimal?>(
                                          OI
                                        , "Surcharges"
                                        , "PricePreTax"
                                        , forSQL
                                        );

                            return orderItemSurcharge;
                       }
                   },
                   new PropertyInfo()
                   {
                       ID = 10021,
                        Text = "Order",
                        DataType = DataType.OrderCategory,
                        LinqFx = OI => ((OrderItemData)OI)?.Order,
                        Expression = (OI, forSQL) => AELHelper.Expressions.ObjectProperty<OrderItemData, TransactionHeaderData>(OI, "Order", forSQL),
                   },
                   new PropertyInfo()
                   {
                       ID = 10022,
                        Text = "Estimate",
                        DataType = DataType.EstimateCategory,
                        LinqFx = OI => ((OrderItemData)OI)?.Order,
                        Expression = (OI, forSQL) => AELHelper.Expressions.ObjectProperty<OrderItemData, TransactionHeaderData>(OI, "Order", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10023,
                        Text = "Employees",
                        DataType = DataType.OrderItemEmployeeRoleListCategory,
                        LinqFx = OI => OI,
                        Expression = (OI, forSQL) => OI,
                   },
                   new PropertyInfo()
                   {
                        ID = 10024,
                        Text = "Contacts",
                        DataType = DataType.OrderItemContactRoleCategory,
                        LinqFx = OI => OI,
                        Expression = (OI, forSQL) => OI,
                   },
                   new PropertyInfo()
                   {
                        ID = 10025,
                        Text = "Notes",
                        DataType = DataType.LineItemNotesCategory,
                        LinqFx = OI => ((OrderItemData)OI)?.Notes?.ToList(),
                        Expression = (OI, forSQL) => AELHelper.Expressions.ObjectListProperty<OrderItemData, OrderNote>(OI, "Notes", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10026,
                        Text = "Locations",
                        DataType = DataType.OrderItemLocationCategory,
                        LinqFx = OI => ((OrderItemData)OI),
                        Expression = (OI, forSQL) => OI,
                   },
                   new PropertyInfo()
                   {
                        ID = 10027,
                        Text = "Line Item Number",
                        DataType = DataType.Number,
                        NumberType = NumberType.Integer,
                        LinqFx = OI => ((OrderItemData)OI)?.ItemNumber,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "ItemNumber", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10028,
                        Text = "Description",
                        DataType = DataType.String,
                        LinqFx = OI => ((OrderItemData)OI)?.Description,
                        Expression = (OI, forSQL) => AELHelper.Expressions.StringProperty<OrderItemData>(OI, "Description", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10029,
                        Text = "Quantity",
                        DataType = DataType.Number,
                        NumberType = NumberType.Decimal,
                        LinqFx = OI => ((OrderItemData)OI)?.Quantity,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "Quantity", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10030,
                        Text = "Name",
                        DataType = DataType.String,
                        LinqFx = OI => ((OrderItemData)OI)?.Name,
                        Expression = (OI, forSQL) => AELHelper.Expressions.StringProperty<OrderItemData>(OI, "Name", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10031,
                        Text = "Status",
                        DataType = DataType.LineItemStatusCategory,
                        LinqFx = OI => OI,
                        Expression = (OI, forSQL) => OI,
                   },
                   new PropertyInfo()
                   {
                        ID = 10032,
                        Text = "Taxes",
                        DataType = DataType.OrderItemTaxesCategory,
                        LinqFx = OI => ((OrderItemData)OI),
                        Expression = (OI, forSQL) => OI,
                   },
                   new PropertyInfo()
                   {
                        ID = 10033,
                        Text = "Prices",
                        DataType = DataType.OrderItemPricesCategory, 
                        LinqFx = OI => ((OrderItemData)OI),
                        Expression = (OI, forSQL) => OI,
                   },
                   
                   new PropertyInfo()
                   {
                        ID = 10034,
                        Text = "Cost",
                        DataType = DataType.OrderItemCostsCategory,
                        LinqFx = OI => ((OrderItemData)OI),
                        Expression = (OI, forSQL) => OI,
                   },
                   new PropertyInfo()
                   {
                        ID = 10035,
                        Text = "Has Documents",
                        DataType = DataType.Boolean,
                        LinqFx = OI => ((OrderItemData)OI)?.HasDocuments,
                        Expression = (OI, forSQL) => AELHelper.Expressions.BooleanProperty<OrderItemData>(OI, "HasDocuments", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10036,
                        Text = "Has Proof",
                        DataType = DataType.Boolean,
                        LinqFx = OI => ((OrderItemData)OI)?.HasProof,
                        Expression = (OI, forSQL) => AELHelper.Expressions.BooleanProperty<OrderItemData>(OI, "HasProof", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10037,
                        Text = "Has Custom Image",
                        DataType = DataType.Boolean,
                        LinqFx = OI => ((OrderItemData)OI)?.HasCustomImage,
                        Expression = (OI, forSQL) => AELHelper.Expressions.BooleanProperty<OrderItemData>(OI, "HasCustomImage", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 10038,
                        Text = "Tags",
                        DataType = DataType.OrderItemTagsCategory,
                        LinqFx = OI => OI,
                        Expression = (OI, forSQL) => OI,
                   },
                   new PropertyInfo()
                   {
                        ID = 10039,
                        Text = "Components",
                        DataType = DataType.OrderItemComponentCategory,
                        LinqFx = OI => ((OrderItemData)OI)?.Components?.ToList(),
                        Expression = (OI, forSQL) => AELHelper.Expressions.ObjectListProperty<OrderItemData, OrderItemComponent>(OI, "Components", forSQL),
                   }
               };
    }
}
