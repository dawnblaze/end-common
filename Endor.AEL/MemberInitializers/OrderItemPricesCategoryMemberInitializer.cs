﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemPricesCategoryMemberInitializer : BaseMemberInitializer
    {
        public OrderItemPricesCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemPricesCategoryMemberInitializer> lazy = new Lazy<OrderItemPricesCategoryMemberInitializer>(() => new OrderItemPricesCategoryMemberInitializer());

        public static OrderItemPricesCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemPricesCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20370,
                        Text = "Price (Pre Tax)",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.PricePreTax,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "PricePreTax", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20371,
                        Text = "Price (After Tax)",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.PriceTotal,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "PriceTotal", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20372,
                        Text = "Unit Price (Pre Tax)",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.PriceUnitPreTax,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "PriceUnitPreTax", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20373,
                        Text = "Line Item Discount Percent",
                        DataType = DataType.Number,
                        NumberType = NumberType.Decimal,
                        LinqFx = OI => ((OrderItemData)OI)?.PriceDiscountPercent,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "PriceDiscountPercent", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20374,
                        Text = "Line Item Discount Amount",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.PriceDiscount,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "PriceDiscount", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20375,
                        Text = "Setup Fees",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.PriceSurcharge,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "PriceSurcharge", forSQL),
                   },
               };

    }
}
