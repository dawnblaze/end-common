﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL
{
    public class BooleanListMemberInitializer : BaseMemberInitializer
    {
        public BooleanListMemberInitializer() {}

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<BooleanListMemberInitializer> lazy = new Lazy<BooleanListMemberInitializer>(() => new BooleanListMemberInitializer());

        public static BooleanListMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.BooleanList;
        public override Type ParentType => typeof(bool);
    }
}


