﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemTaxesCategoryMemberInitializer : BaseMemberInitializer
    {
        public OrderItemTaxesCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemTaxesCategoryMemberInitializer> lazy = new Lazy<OrderItemTaxesCategoryMemberInitializer>(() => new OrderItemTaxesCategoryMemberInitializer());

        public static OrderItemTaxesCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemTaxesCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20390,
                        Text = "Tax Rate",
                        DataType = DataType.Number,
                        NumberType = NumberType.Decimal,
                        LinqFx = OI => ((OrderItemData)OI)?.Order?.PriceTaxRate,
                        Expression = (OI, forSQL) =>
                        {
                            var expOrder = AELHelper.Expressions.ObjectProperty<OrderItemData, TransactionHeaderData>(OI, "Order", forSQL);

                            return AELHelper.Expressions.NumberProperty<TransactionHeaderData>(expOrder, "PriceTaxRate", forSQL);
                        },
                   },
                   new PropertyInfo()
                   {
                        ID = 20391,
                        Text = "Tax Exempt",
                        DataType = DataType.Boolean,
                        LinqFx = OI => ((OrderItemData)OI)?.IsTaxExempt,
                        Expression = (OI, forSQL) => AELHelper.Expressions.BooleanProperty<OrderItemData>(OI, "IsTaxExempt", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20392,
                        Text = "Tax Amount",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.PriceTax,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "PriceTax", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20393,
                        Text = "Tax Group",
                        DataType = DataType.TaxGroup,
                        LinqFx = OI => ((OrderItemData)OI)?.TaxGroup,
                        Expression = (OI, forSQL) => AELHelper.Expressions.ObjectProperty<OrderItemData, TaxGroup>(OI, "TaxGroup", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20394,
                        Text = "Tax Exempt Reason",
                        DataType = DataType.TaxExemptReason,
                        LinqFx = OI => ((OrderItemData)OI)?.TaxExemptReason,
                        Expression = (OI, forSQL) => AELHelper.Expressions.ObjectProperty<OrderItemData, FlatListItem>(OI, "TaxExemptReason", forSQL),
                   },
               };
    }
}
