﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemCostsCategoryMemberInitializer : BaseMemberInitializer
    {
        public OrderItemCostsCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemCostsCategoryMemberInitializer> lazy = new Lazy<OrderItemCostsCategoryMemberInitializer>(() => new OrderItemCostsCategoryMemberInitializer());

        public static OrderItemCostsCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemCostsCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 20400,
                        Text = "Total Cost",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.CostTotal,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "CostTotal", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20401,
                        Text = "Material Cost",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.CostMaterial,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "CostMaterial", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20402,
                        Text = "Labor Cost",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.CostLabor,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "CostLabor", forSQL),
                   },
                   new PropertyInfo()
                   {
                        ID = 20403,
                        Text = "Machine Cost",
                        DataType = DataType.Number,
                        NumberType = NumberType.Currency,
                        LinqFx = OI => ((OrderItemData)OI)?.CostMachine,
                        Expression = (OI, forSQL) => AELHelper.Expressions.NumberProperty<OrderItemData>(OI, "CostMachine", forSQL),
                   },
               };
       }
}
