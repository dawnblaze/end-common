﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class CompanyTypeCategoryMemberInitializer : BaseMemberInitializer
    {
        public CompanyTypeCategoryMemberInitializer() { }

        private static readonly Lazy<CompanyMemberInitializer> lazy = new Lazy<CompanyMemberInitializer>(() => new CompanyMemberInitializer());

        public static CompanyMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyTypeCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>
            {
                new PropertyInfo
                {
                    ID = 20140,
                    Text = "Is Prospect",
                    DataType = DataType.Boolean,
                    LinqFx = C => ((CompanyData)C)?.IsProspect,
                    Expression = (C, forSQL) => AELHelper.Expressions.BooleanProperty<CompanyData>(C, nameof(CompanyData.IsProspect), forSQL),
                },
                new PropertyInfo
                {
                    ID = 20141,
                    Text = "Is Customer",
                    DataType = DataType.Boolean,
                    LinqFx = C => ((CompanyData)C)?.Orders?.Any(),
                    Expression = (C, forSQL) =>
                    {
                        Expression expOrders = Expression.Property(C, nameof(CompanyData.Orders));

                        var callAny = Expression.Call(
                            typeof(Enumerable)
                            , "Any"
                            , new Type[]{
                                typeof(TransactionHeaderData)
                            }
                            , expOrders
                            );

                        if (forSQL)
                            return callAny;

                        var checkOrders = Expression.Condition(
                            Expression.Equal(expOrders, Expression.Constant(null, typeof(ICollection<TransactionHeaderData>)))
                            , AELHelper.Expressions.BooleanFalse
                            , callAny
                            );

                        return Expression.Condition(
                            Expression.Equal(C, Expression.Constant(null, typeof(CompanyData)))
                            , AELHelper.Expressions.NullBoolean
                            , AELHelper.Expressions.ConvertToBoolean(checkOrders)
                            );
                    },
                },
                new PropertyInfo
                {
                    ID = 20142,
                    Text = "Is Vendor",
                    DataType = DataType.Boolean,
                    LinqFx = C => ((CompanyData)C)?.IsVendor,
                    Expression = (C, forSQL) => AELHelper.Expressions.BooleanProperty<CompanyData>(C, nameof(CompanyData.IsVendor), forSQL),
                },
            };
    }
}
