﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class LineItemNotesCategoryMemberInitializer : BaseMemberInitializer
    {
        public LineItemNotesCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<LineItemNotesCategoryMemberInitializer> lazy = new Lazy<LineItemNotesCategoryMemberInitializer>(() => new LineItemNotesCategoryMemberInitializer());

        public static LineItemNotesCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.LineItemNotesCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                       ID = 11103,
                       Text = "Notes (Any)",
                       DataType = DataType.StringList,
                       LinqFx = oi => ((OrderItemData)oi).Notes?
                                      .Select(x => x.Note)?
                                      .ToList(),
                       Expression = (OI, forSQL) => AELHelper.Expressions.ObjectListPropertyWithSelect<OrderItemData, OrderNote, string>(OI, nameof(OrderItemData.Notes), nameof(OrderNote.Note), forSQL)
                   },
                   new PropertyInfo()
                   {
                       ID = 11104,
                       Text = "Internal Notes",
                       DataType = DataType.StringList,
                       LinqFx = oi => ((OrderItemData)oi).Notes?
                                      .Where(n => n.NoteType != OrderNoteType.Customer)
                                      .Select(x => x.Note)?
                                      .ToList(),
                       Expression = (OI, forSQL) => AELHelper.Expressions.ObjectListPropertyWithWhereSelect<OrderItemData, OrderNote, string>(
                           OI
                           , nameof(OrderItemData.Notes)
                           , nameof(OrderNote.Note)
                           , forSQL
                           , new AELHelper.Expressions.WhereCondition { WhereProperty = nameof(OrderNote.NoteType), WhereValue = Expression.Constant(OrderNoteType.Customer, typeof(OrderNoteType)), TestFunction = Expression.NotEqual }
                           )
                   },
                   new PropertyInfo()
                   {
                       ID = 11105,
                       Text = "Customer Notes",
                       DataType = DataType.StringList,
                       LinqFx = oi => ((OrderItemData)oi).Notes?
                                      .Where(n => n.NoteType == OrderNoteType.Customer)
                                      .Select(x => x.Note)?
                                      .ToList(),
                       Expression = (OI, forSQL) => AELHelper.Expressions.ObjectListPropertyWithWhereSelect<OrderItemData, OrderNote, string>(
                           OI
                           , nameof(OrderItemData.Notes)
                           , nameof(OrderNote.Note)
                           , forSQL
                           , new AELHelper.Expressions.WhereCondition { WhereProperty = nameof(OrderNote.NoteType), WhereValue = Expression.Constant(OrderNoteType.Customer, typeof(OrderNoteType)) }
                           )
                   },
               };
       }
}
