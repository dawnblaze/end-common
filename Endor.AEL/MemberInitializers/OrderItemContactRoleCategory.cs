﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class OrderItemContactRoleCategory : BaseMemberInitializer
    {
        public OrderItemContactRoleCategory() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderItemContactRoleCategory> lazy = new Lazy<OrderItemContactRoleCategory>(() => new OrderItemContactRoleCategory());

        public static OrderItemContactRoleCategory Instance => lazy.Value;

        public override DataType DataType => DataType.OrderItemContactRoleCategory;
        public override Type ParentType => typeof(OrderItemData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                       ID = 20431,
                        Text = "Primary",
                        DataType = DataType.ContactCategory,
                        LinqFx = O => ((OrderItemData)O).ContactRoles?
                            .FirstOrDefault(r => r.RoleType == OrderContactRoleType.Primary)?.Contact,
                        Expression = (O, forSQL) =>
                        {
                            var contactRole = 
                                AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault
                                    <OrderItemData, OrderContactRole>(
                                          O
                                        , "ContactRoles"
                                        , forSQL
                                        , new AELHelper.Expressions.WhereCondition{ WhereProperty = "RoleType", WhereValue = Expression.Constant(OrderContactRoleType.Primary, typeof(OrderContactRoleType)) }
                                        );

                            return AELHelper.Expressions.ObjectProperty<OrderContactRole, ContactData>(contactRole, "Contact", forSQL);
                        },
                   },
                   new PropertyInfo()
                   {
                       ID = 20432,
                        Text = "Shipping",
                        DataType = DataType.ContactCategory,
                        LinqFx = O => ((OrderItemData)O).ContactRoles?
                            .FirstOrDefault(r => r.RoleType == OrderContactRoleType.ShipTo)?.Contact,
                        Expression = (O, forSQL) =>
                        {
                            var contactRole =
                                AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault
                                    <OrderItemData, OrderContactRole>(
                                          O
                                        , "ContactRoles"
                                        , forSQL
                                        , new AELHelper.Expressions.WhereCondition{ WhereProperty = "RoleType", WhereValue = Expression.Constant(OrderContactRoleType.ShipTo, typeof(OrderContactRoleType)) }
                                        );

                            return AELHelper.Expressions.ObjectProperty<OrderContactRole, ContactData>(contactRole, "Contact", forSQL);
                        },
                   },
               };
        }
}
