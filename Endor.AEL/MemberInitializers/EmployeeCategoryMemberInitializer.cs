﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Diagnostics;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class EmployeeCategoryMemberInitializer : BaseMemberInitializer
    {
        public EmployeeCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<EmployeeCategoryMemberInitializer> lazy = new Lazy<EmployeeCategoryMemberInitializer>(() => new EmployeeCategoryMemberInitializer());

        public static EmployeeCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.EmployeeCategory;
        public override Type ParentType => typeof(EmployeeData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo()
                {
                    ID = 20220,
                    Text = "Name",
                    IsDefault = true,
                    DataType = DataType.Employee,
                    LinqFx = E => E,
                    Expression = (E, forSQL) =>
                    {
                        return AELHelper.Expressions.NumberProperty<EmployeeData>(E,"ID",forSQL);
                    },
                },
                new PropertyInfo()
                {
                    ID = 20221,
                    Text = "Employee Name",
                    DataType = DataType.String,
                    LinqFx = E => ((EmployeeData)E)?.ShortName,
                    Expression = (E, forSQL) => AELHelper.Expressions.StringProperty<EmployeeData>(E, nameof(EmployeeData.ShortName), forSQL), 
                },
                new PropertyInfo()
                {
                    ID = 20222,
                    Text = "First Name",
                    DataType = DataType.String,
                    LinqFx = E => ((EmployeeData)E)?.First,
                    Expression = (E, forSQL) => AELHelper.Expressions.StringProperty<EmployeeData>(E, nameof(EmployeeData.First), forSQL), 
                },
                new PropertyInfo()
                {
                    ID = 20223,
                    Text = "Last Name",
                    DataType = DataType.String,
                    LinqFx = E => ((EmployeeData)E)?.Last,
                    Expression = (E, forSQL) => AELHelper.Expressions.StringProperty<EmployeeData>(E, nameof(EmployeeData.Last), forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20224,
                    Text = "Email",
                    //DataType = DataType.StringList,
                    DataType = DataType.String,
                    LinqFx = E => ((EmployeeData)E)?.EmployeeLocators?
                        .Where(l => l.LocatorType == (byte)LocatorType.Email )
                        .Select(l => l.RawInput),
                    Expression = (E, forSQL) =>
                    {
                        //Expression expEmployeeLocators = AELHelper.Expressions.ObjectListProperty<EmployeeData, EmployeeLocator>
                        //    (
                        //        E
                        //        , "EmployeeLocators"
                        //        , forSQL
                        //    );

                        //return AELHelper.Expressions.ObjectListWithWhereSelect<EmployeeLocator, string>(
                        //    expEmployeeLocators
                        //    , "RawInput"
                        //    , forSQL
                        //    , new AELHelper.Expressions.WhereCondition { WhereProperty = "LocatorType", WhereValue = Expression.Constant((byte)LocatorType.Email, typeof(byte)) }
                        //    );

                        var expEmployeeLocator = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<EmployeeData, EmployeeLocator>(
                              E
                            , "EmployeeLocators"
                            , forSQL
                            , new AELHelper.Expressions.WhereCondition { WhereProperty = "LocatorType", WhereValue = Expression.Constant((byte)LocatorType.Email, typeof(byte)) }
                            );

                        return AELHelper.Expressions.StringProperty<EmployeeLocator>(expEmployeeLocator, "RawInput", forSQL);

                    },
                },
                new PropertyInfo()
                {
                    ID = 20225,
                    Text = "Phone Number",
                    DataType = DataType.String,
                    LinqFx = E => ((EmployeeData)E)?.EmployeeLocators?
                        .Where(l => l.LocatorType == (byte)LocatorType.Phone)
                        .Select(l => l.RawInput),
                    Expression = (E, forSQL) =>
                    {
                        //Expression expEmployeeLocators = AELHelper.Expressions.ObjectListProperty<EmployeeData, EmployeeLocator>
                        //    (
                        //        E
                        //        , "EmployeeLocators"
                        //        , forSQL
                        //    );

                        //return AELHelper.Expressions.ObjectListWithWhereSelect<EmployeeLocator, string>(
                        //    expEmployeeLocators
                        //    , "RawInput"
                        //    , forSQL
                        //    , new AELHelper.Expressions.WhereCondition { WhereProperty = "LocatorType", WhereValue = Expression.Constant((byte)LocatorType.Phone, typeof(byte)) }
                        //    );


                        var expEmployeeLocator = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<EmployeeData, EmployeeLocator>(
                              E
                            , "EmployeeLocators"
                            , forSQL
                            , new AELHelper.Expressions.WhereCondition { WhereProperty = "LocatorType", WhereValue = Expression.Constant((byte)LocatorType.Phone, typeof(byte)) }
                            );

                        return AELHelper.Expressions.StringProperty<EmployeeLocator>(expEmployeeLocator, "RawInput", forSQL);
                    },
                },
            };
    }
}
