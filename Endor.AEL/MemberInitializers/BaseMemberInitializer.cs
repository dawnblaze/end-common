﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL
{
    /// <summary>
    /// Parent Class for all Member Initializers. 
    /// Upon start-up, all classes that inherit from this class should be instantiated
    /// 
    /// The member methods are initialized as Delegates that will construct the Members
    /// upon access.  This is required for the BID specific properties since they may
    /// vary for each Business.  It is also done for the global members (for symmetry).
    /// </summary>
    public abstract class BaseMemberInitializer
    {
        public BaseMemberInitializer()
        {
        }

        public abstract DataType DataType { get; }
        public abstract Type ParentType { get; }

        public virtual List<PropertyInfo> GlobalProperties() => null;
        public virtual List<MethodInfo> GlobalMethods() => null;

        public virtual List<PropertyInfo> BIDProperties(IDataProvider dataProvider) => null;
        public virtual List<MethodInfo> BIDMethods(IDataProvider dataProvider) => null;

        public IEnumerable<IMemberInfo> GetAllMembers(IDataProvider dataProvider)
        {
            IEnumerable<IMemberInfo> result = null;

            void AddMembersToResult<T>(List<T> memberList)
                where T : IMemberInfo
            {
                if (memberList != null)
                {
                    if (result == null)
                        result = memberList.Cast<IMemberInfo>();
                    else
                        result = result.Concat(memberList.Cast<IMemberInfo>());
                }
            }

            AddMembersToResult(GlobalProperties());
            AddMembersToResult(BIDProperties(dataProvider));
            AddMembersToResult(GlobalMethods());
            AddMembersToResult(BIDMethods(dataProvider));

            return result;
        }
    }
}