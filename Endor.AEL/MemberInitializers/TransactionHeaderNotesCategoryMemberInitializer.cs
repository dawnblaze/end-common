﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class TransactionHeaderNotesCategoryMemberInitializer : BaseMemberInitializer
    {
        public TransactionHeaderNotesCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<TransactionHeaderNotesCategoryMemberInitializer> lazy = new Lazy<TransactionHeaderNotesCategoryMemberInitializer>(() => new TransactionHeaderNotesCategoryMemberInitializer());

        public static TransactionHeaderNotesCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.TransactionHeaderNotesCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                       ID = 11100,
                       Text = "Notes (Any)",
                       DataType = DataType.StringList,
                       LinqFx = o => ((TransactionHeaderData)o).Notes?
                                      .Select(x => x.Note)?
                                      .ToList(),
                       Expression = (O, forSQL) => AELHelper.Expressions.ObjectListPropertyWithSelect<TransactionHeaderData, OrderNote, string>(O, nameof(TransactionHeaderData.Notes), nameof(OrderNote.Note), forSQL)
                   },
                   new PropertyInfo()
                   {
                       ID = 11101,
                       Text = "Internal Notes",
                       DataType = DataType.StringList,
                       LinqFx = o => ((TransactionHeaderData)o).Notes?
                                      .Where(n => n.NoteType != OrderNoteType.Customer)
                                      .Select(x => x.Note)?
                                      .ToList(),
                       Expression = (O, forSQL) => AELHelper.Expressions.ObjectListPropertyWithWhereSelect<TransactionHeaderData, OrderNote, string>(
                           O
                           , nameof(TransactionHeaderData.Notes)
                           , nameof(OrderNote.Note)
                           , forSQL
                           , new AELHelper.Expressions.WhereCondition { WhereProperty = nameof(OrderNote.NoteType), WhereValue = Expression.Constant(OrderNoteType.Customer, typeof(OrderNoteType)), TestFunction = Expression.NotEqual }
                           )
                   },
                   new PropertyInfo()
                   {
                       ID = 11102,
                       Text = "Customer Notes",
                       DataType = DataType.StringList,
                       LinqFx = o => ((TransactionHeaderData)o).Notes?
                                      .Where(n => n.NoteType == OrderNoteType.Customer)
                                      .Select(x => x.Note)?
                                      .ToList(),
                       Expression = (O, forSQL) => AELHelper.Expressions.ObjectListPropertyWithWhereSelect<TransactionHeaderData, OrderNote, string>(
                           O
                           , nameof(TransactionHeaderData.Notes)
                           , nameof(OrderNote.Note)
                           , forSQL
                           , new AELHelper.Expressions.WhereCondition { WhereProperty = nameof(OrderNote.NoteType), WhereValue = Expression.Constant(OrderNoteType.Customer, typeof(OrderNoteType)) }
                           )
                   },
               };
    }
}
