﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Endor.AEL.MemberInitializers
{
    public class OrderCustomFieldMemberInitializer : BaseMemberInitializer
    {
        public OrderCustomFieldMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<OrderCustomFieldMemberInitializer> lazy = new Lazy<OrderCustomFieldMemberInitializer>(() => new OrderCustomFieldMemberInitializer());

        public static OrderCustomFieldMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.OrderCustomField;

        public override Type ParentType => typeof(TransactionHeaderCustomDataValue);

        public override List<PropertyInfo> BIDProperties(IDataProvider dataProvider)
        {
            List<PropertyInfo> result = new List<PropertyInfo>();

            List<CustomFieldDefinition> customFields = dataProvider.GetData<CustomFieldDefinition>()
                                                                   .Where(c => c.AppliesToClassTypeID == ClassType.Order.ID())
                                                                   .Where(c => c.DataType == DataType.Boolean || c.DataType == DataType.String || c.DataType == DataType.Number)
                                                                   .OrderBy(c => c.Name.ToLower())
                                                                   .ToList();

            foreach (CustomFieldDefinition customField in customFields)
            {
                Func<dynamic, dynamic> linqFx;
                Func<Expression, bool, Expression> expression;

                switch (customField.DataType)
                {
                    case DataType.Number:
                        linqFx = C => ((OrderData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.ValueAsNumber;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<OrderData, TransactionHeaderCustomDataValue>(
                                C
                                , nameof(OrderData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(TransactionHeaderCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(TransactionHeaderCustomDataValue.ValueAsNumber));
                        };
                        break;

                    case DataType.Boolean:
                        linqFx = C => ((OrderData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.ValueAsBoolean;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<OrderData, TransactionHeaderCustomDataValue>(
                                C
                                , nameof(OrderData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(TransactionHeaderCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(TransactionHeaderCustomDataValue.ValueAsBoolean));
                        };
                        break;

                    case DataType.String:
                    default:
                        linqFx = C => ((OrderData)C).CustomDataValues.FirstOrDefault(x => x.ClassTypeID == customField.ID)?.Value;
                        expression = (C, forSQL) =>
                        {
                            Expression customDataValue = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<OrderData, TransactionHeaderCustomDataValue>(
                                C
                                , nameof(OrderData.CustomDataValues)
                                , forSQL
                                , new AELHelper.Expressions.WhereCondition() { WhereProperty = nameof(TransactionHeaderCustomDataValue.CustomFieldDefID), WhereValue = Expression.Constant(customField.ID, typeof(short)) }
                                );

                            return Expression.Property(customDataValue, nameof(TransactionHeaderCustomDataValue.Value));
                        };
                        break;
                }

                result.Add(
                    new PropertyInfo()
                    {
                        ID = 20340,
                        RelatedID = customField.ID,
                        Text = customField.Name,
                        DataType = customField.DataType,
                        LinqFx = linqFx,
                        Expression = expression,
                    }
                );
            }

            return result;
        }
    }
}
