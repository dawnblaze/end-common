﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Endor.AEL
{
    public class StringMemberInitializer : BaseMemberInitializer
    {
        public StringMemberInitializer() { }

        private static readonly Lazy<StringMemberInitializer> lazy = new Lazy<StringMemberInitializer>(() => new StringMemberInitializer());

        public static StringMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.String;
        public override Type ParentType => typeof(string);
    }
}
