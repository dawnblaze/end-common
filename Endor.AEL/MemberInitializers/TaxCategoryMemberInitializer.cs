﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class TaxCategoryMemberInitializer : BaseMemberInitializer
    {
        public TaxCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<TaxCategoryMemberInitializer> lazy = new Lazy<TaxCategoryMemberInitializer>(() => new TaxCategoryMemberInitializer());

        public static TaxCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.TaxesCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo()
                {
                    ID = 20050,
                    Text = "Tax Rate",
                    DataType = DataType.Number,
                    NumberType = NumberType.Decimal,
                    LinqFx = T => ((TransactionHeaderData)T)?.PriceTaxRate,
                    Expression = (T, forSQL) => AELHelper.Expressions.NumberProperty<TransactionHeaderData>(T, nameof(TransactionHeaderData.PriceTaxRate), forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20051,
                    Text = "Tax Exempt",
                    DataType = DataType.Boolean,
                    LinqFx = T => ((TransactionHeaderData)T)?.IsTaxExempt,
                    Expression = (T, forSQL) => AELHelper.Expressions.BooleanProperty<TransactionHeaderData>(T, nameof(TransactionHeaderData.IsTaxExempt), forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20052,
                    Text = "Tax Amount",
                    DataType = DataType.Number,
                    NumberType = NumberType.Currency,
                    LinqFx = T => ((TransactionHeaderData)T)?.PriceTax,
                    Expression = (T, forSQL) => AELHelper.Expressions.NumberProperty<TransactionHeaderData>(T, nameof(TransactionHeaderData.PriceTax), forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20053,
                    Text = "Tax Group",
                    DataType = DataType.TaxGroup,
                    LinqFx = T => ((TransactionHeaderData)T)?.TaxGroup,
                    Expression = (T, forSQL) => AELHelper.Expressions.ObjectProperty<TransactionHeaderData, TaxGroup>(T, nameof(TransactionHeaderData.TaxGroup), forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20054,
                    Text = "Tax Exempt Reason",
                    DataType = DataType.TaxExemptReason,
                    LinqFx = T => ((TransactionHeaderData)T)?.TaxExemptReason,
                    Expression = (T, forSQL) => AELHelper.Expressions.ObjectProperty<TransactionHeaderData, FlatListItem>(T, nameof(TransactionHeaderData.TaxExemptReason), forSQL),
                },
            };

    }
}
