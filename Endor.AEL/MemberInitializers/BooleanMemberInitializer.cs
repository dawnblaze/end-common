﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Endor.AEL
{
    public class BooleanMemberInitializer : BaseMemberInitializer
    {
        public BooleanMemberInitializer() { }

        private static readonly Lazy<BooleanMemberInitializer> lazy = new Lazy<BooleanMemberInitializer>(() => new BooleanMemberInitializer());

        public static BooleanMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.Boolean;
        public override Type ParentType => typeof(bool);
    }
}
