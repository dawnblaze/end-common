﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.MemberInitializers
{
    public class CompanyContactListCategoryMemberInitializer : BaseMemberInitializer
    {
        public CompanyContactListCategoryMemberInitializer() { }

        private static readonly Lazy<CompanyContactListCategoryMemberInitializer> lazy = new Lazy<CompanyContactListCategoryMemberInitializer>(() => new CompanyContactListCategoryMemberInitializer());

        public static CompanyContactListCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.CompanyContactListCategory;
        public override Type ParentType => typeof(CompanyData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo
                {
                    ID = 20320,
                    Text = "Primary",
                    DataType = DataType.ContactCategory,
                    LinqFx = C => ((CompanyData)C)?.CompanyContactLinks?.FirstOrDefault(c => c.IsPrimary == true)?.Contact,
                    Expression = (C, forSQL) =>
                    {
                        var link = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<CompanyData, CompanyContactLink>(
                              C
                            , "CompanyContactLinks"
                            , forSQL
                            , new AELHelper.Expressions.WhereCondition() { WhereProperty = "IsPrimary", WhereValue = AELHelper.Expressions.BooleanNullableTrue }
                        );

                        return AELHelper.Expressions.ObjectProperty<CompanyContactLink, ContactData>(link, "Contact", forSQL);
                    },
                },
                new PropertyInfo
                {
                    ID = 20321,
                    Text = "Any",
                    DataType = DataType.ContactCategory,
                    LinqFx = C => ((CompanyData)C)?.CompanyContactLinks?.Select(c => c.Contact).ToList(),
                    Expression = (C, forSQL) => AELHelper.Expressions
                        .ObjectListPropertyWithSelect<CompanyData, CompanyContactLink, ContactData>(C, "CompanyContactLinks", "Contact", forSQL),
                },
                new PropertyInfo
                {
                    ID = 20322,
                    Text = "Billing",
                    DataType = DataType.ContactCategory,
                    LinqFx = C => ((CompanyData)C)?.CompanyContactLinks?.FirstOrDefault(c => c.IsBilling == true)?.Contact,
                    Expression = (C, forSQL) =>
                    {
                        var link = AELHelper.Expressions.ObjectListPropertyWithWhereFirstOrDefault<CompanyData, CompanyContactLink>(
                              C
                            , "CompanyContactLinks"
                            , forSQL
                            , new AELHelper.Expressions.WhereCondition() { WhereProperty = "IsBilling", WhereValue = AELHelper.Expressions.BooleanNullableTrue }
                        );

                        return AELHelper.Expressions.ObjectProperty<CompanyContactLink, ContactData>(link, "Contact", forSQL);
                    },
                },
            };
    }
}
