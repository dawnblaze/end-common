﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class TransactionHeaderEmployeeRoleListCategoryMemberInitializer : BaseMemberInitializer
    {
        public TransactionHeaderEmployeeRoleListCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<TransactionHeaderEmployeeRoleListCategoryMemberInitializer> lazy = new Lazy<TransactionHeaderEmployeeRoleListCategoryMemberInitializer>(() => new TransactionHeaderEmployeeRoleListCategoryMemberInitializer());

        public static TransactionHeaderEmployeeRoleListCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.TransactionHeaderEmployeeRoleListCategory;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                       ID = 20200,
                        Text = "Roles",
                        DataType = DataType.TransactionHeaderEmployeeRoleList,
                        LinqFx = O => O,
                        Expression = (O, forSQL) => O,
                   },
                   new PropertyInfo()
                   {
                        ID = 20201,
                        Text = "Assigned To",
                        DataType = DataType.EmployeeCategory,
                        LinqFx = O => ((TransactionHeaderData)O)?.EmployeeRoles?
                            .Where(r => r.RoleID == SystemIDs.EmployeeRole.AssignedTo && r.OrderItemID == null)?.Select(x => x.Employee).ToList(),
                        Expression = (O, forSQL) => AELHelper.Expressions
                                       .ObjectListPropertyWithWhereSelect<TransactionHeaderData, OrderEmployeeRole, EmployeeData>(
                                             O
                                           , "EmployeeRoles"
                                           , "Employee"
                                           , forSQL
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "RoleID", WhereValue = Expression.Constant(SystemIDs.EmployeeRole.AssignedTo, typeof(short)) }
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "OrderItemID", WhereValue = Expression.Constant(null, typeof(int?)) }
                                       ),
                   },
                   new PropertyInfo()
                   {
                        ID = 20202,
                        Text = "Entered By",
                        DataType = DataType.EmployeeCategory,
                        LinqFx = O => ((TransactionHeaderData)O)?.EmployeeRoles
                            .Where(r => r.RoleID == SystemIDs.EmployeeRole.EnteredBy && r.OrderItemID == null)?.Select(x => x.Employee).ToList(),
                        Expression = (O, forSQL) => AELHelper.Expressions
                                       .ObjectListPropertyWithWhereSelect<TransactionHeaderData, OrderEmployeeRole, EmployeeData>(
                                             O
                                           , "EmployeeRoles"
                                           , "Employee"
                                           , forSQL
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "RoleID", WhereValue = Expression.Constant(SystemIDs.EmployeeRole.EnteredBy, typeof(short)) }
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "OrderItemID", WhereValue = Expression.Constant(null, typeof(int?)) }
                                       ),
                    },
               };
    }
}
