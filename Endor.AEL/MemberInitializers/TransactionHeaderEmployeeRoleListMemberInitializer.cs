﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class TransactionHeaderEmployeeRoleListMemberInitializer : BaseMemberInitializer
    {
        public TransactionHeaderEmployeeRoleListMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<TransactionHeaderEmployeeRoleListMemberInitializer> lazy = new Lazy<TransactionHeaderEmployeeRoleListMemberInitializer>(() => new TransactionHeaderEmployeeRoleListMemberInitializer());

        public static TransactionHeaderEmployeeRoleListMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.TransactionHeaderEmployeeRoleList;
        public override Type ParentType => typeof(TransactionHeaderData);

        public override List<PropertyInfo> GlobalProperties()
               => new List<PropertyInfo>()
               {
                   new PropertyInfo()
                   {
                        ID = 11107,
                        Text = "Employees in Any Role",
                        DataType = DataType.EmployeeCategory,
                        LinqFx = O => ((TransactionHeaderData)O)?.EmployeeRoles
                                        .Select(r => r.Employee)
                                        .Distinct()
                                        .ToList(),
                        Expression = (O, forSQL) => AELHelper.Expressions.ObjectListPropertyWithSelect<TransactionHeaderData, OrderEmployeeRole, EmployeeData>(O, "EmployeeRoles", "Employee", forSQL),
                   },
               };

        public override List<PropertyInfo> BIDProperties(IDataProvider dataProvider)
        {
            List<PropertyInfo> result = new List<PropertyInfo>();

            IQueryable<EmployeeRole> roles = dataProvider.GetData<EmployeeRole>()
                                                         .Where(x => x.ID != SystemIDs.EmployeeRole.AssignedTo && x.ID != SystemIDs.EmployeeRole.EnteredBy)
                                                         .OrderBy(x => x.Name);

            if (roles != null)
            {
                foreach (EmployeeRole role in roles)
                {
                    result.Add(
                           new PropertyInfo()
                           {
                               ID = 11108,
                               RelatedID = role.ID,
                               Text = role.Name,
                               DataType = DataType.EmployeeCategory,
                               LinqFx = O => ((TransactionHeaderData)O)?.EmployeeRoles?
                                   .Where(r => r.RoleID == role.ID && r.OrderItemID == null)?.Select(x => x.Employee).ToList(),
                               Expression = (O, forSQL) => AELHelper.Expressions
                                       .ObjectListPropertyWithWhereSelect<TransactionHeaderData, OrderEmployeeRole, EmployeeData>(
                                             O
                                           , "EmployeeRoles"
                                           , "Employee"
                                           , forSQL
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "RoleID", WhereValue = Expression.Constant(role.ID, typeof(short)) }
                                           , new AELHelper.Expressions.WhereCondition { WhereProperty = "OrderItemID", WhereValue = Expression.Constant(null, typeof(int?)) }
                                       ),
                           }
                        );

                }
            }

            return result;
        }
    }
}
