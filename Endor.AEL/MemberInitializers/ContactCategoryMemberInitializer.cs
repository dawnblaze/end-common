﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL.MemberInitializers
{
    public class ContactCategoryMemberInitializer : BaseMemberInitializer
    {
        public ContactCategoryMemberInitializer() { }

        // Use a singleton pattern with Lazy instantiation
        private static readonly Lazy<ContactCategoryMemberInitializer> lazy = new Lazy<ContactCategoryMemberInitializer>(() => new ContactCategoryMemberInitializer());

        public static ContactCategoryMemberInitializer Instance => lazy.Value;

        public override DataType DataType => DataType.ContactCategory;
        public override Type ParentType => typeof(ContactData);

        public override List<PropertyInfo> GlobalProperties()
            => new List<PropertyInfo>()
            {
                new PropertyInfo()
                {
                    ID = 20120,
                    Text = "Name",
                    IsDefault = true,
                    DataType = DataType.Contact,
                    LinqFx = C => ((ContactData)C)?.ID,
                    Expression = (C, forSQL) => AELHelper.Expressions.NumberProperty<ContactData>(C, "ID", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20121,
                    Text = "Contact Name",
                    DataType = DataType.String,
                    LinqFx = C => ((ContactData)C)?.ShortName,
                    Expression = (C, forSQL) => AELHelper.Expressions.StringProperty<ContactData>(C, "ShortName", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20122,
                    Text = "First Name",
                    DataType = DataType.String,
                    LinqFx = C => ((ContactData)C)?.First,
                    Expression = (C, forSQL) => AELHelper.Expressions.StringProperty<ContactData>(C, "First", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20123,
                    Text = "Last Name",
                    DataType = DataType.String,
                    LinqFx = C => ((ContactData)C)?.Last,
                    Expression = (C, forSQL) => AELHelper.Expressions.StringProperty<ContactData>(C, "Last", forSQL),
                },
                new PropertyInfo()
                {
                    ID = 20124,
                    Text = "Email",
                    DataType = DataType.StringList,
                    LinqFx = C => ((ContactData)C)?.ContactLocators
                        .Where(l => l.LocatorType == (byte)LocatorType.Email)
                        .Select(l => l.RawInput),
                    Expression = (C, forSQL) => AELHelper.Expressions.ObjectListPropertyWithWhereSelect<ContactData, ContactLocator, string>(
                        C
                        , "ContactLocators"
                        , "RawInput"
                        , forSQL
                        , new AELHelper.Expressions.WhereCondition { WhereProperty = "LocatorType", WhereValue = Expression.Constant((byte)LocatorType.Email, typeof(byte)) }
                        ),
                },
                new PropertyInfo()
                {
                    ID = 20125,
                    Text = "Phone Number",
                    DataType = DataType.StringList,
                    LinqFx = C => ((ContactData)C)?.ContactLocators
                        .Where(l => l.LocatorType == (byte)LocatorType.Phone)
                        .Select(l => l.RawInput),
                    Expression = (C, forSQL) => AELHelper.Expressions.ObjectListPropertyWithWhereSelect<ContactData, ContactLocator, string>(
                        C
                        , "ContactLocators"
                        , "RawInput"
                        , forSQL
                        , new AELHelper.Expressions.WhereCondition { WhereProperty = "LocatorType", WhereValue = Expression.Constant((byte)LocatorType.Phone, typeof(byte)) }
                        ),
                },
                new PropertyInfo()
                {
                    ID = 20126,
                    Text = "Custom Fields",
                    DataType = DataType.ContactCustomField,
                    LinqFx = C => C,
                    Expression = (C, forSQL) => C,
                },
            };
    }
}
