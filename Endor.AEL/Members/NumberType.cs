﻿namespace Endor.AEL
{
    public enum NumberType 
    {
        Unknown, 
        Decimal, 
        Integer, 
        Currency 
    }
}
