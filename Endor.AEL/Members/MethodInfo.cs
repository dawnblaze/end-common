﻿using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Endor.AEL
{
    /// <summary>
    /// This is the class that describes method members.
    /// </summary>
    public class MethodInfo : IMemberInfo
    {
        public int ID { get; set; }

        public string Text { get; set; }

        public DataType DataType { get; set; }

        public NumberType NumberType { get; set; }

        public MemberType MemberType { get => AEL.MemberType.Method; }

        public int RelatedID { get; set; }

        public DataType[] ParameterDataTypes { get; set; }

        /// <summary>
        /// The LinqFx is the formula (as a linq expression) that takes the input values
        /// and returns the result.
        /// 
        /// The definition of the method is linq expression is:
        ///   Func<dynamic, List<dynamic>, dynamic>
        /// where the parameters are:
        ///   * 1 = the current (starting) value of the expression
        ///   * 2 = a list of parameters, if there are any
        ///   * 3 = the resulting value, which may be a scalar or collection value.
        ///   
        /// Example - the Round function for DataTpe Numeric (scalar) and NumericCollection.  
        ///     (x, p) => Math.Round( (decimal)x )
        ///     (x, p) => ((List<decimal>) x).Select( d => Math.Round(d)).ToList<decimal>()
        ///     
        /// Example with Parameter - the RoundTo method for Numeric and NumericCollection
        ///     (x, p) => Math.Round( (decimal)x, (p!=null&&p.Count>0)?(int)(p[0]):0 )
        ///     (x, p) => (x, p) => ((List<decimal>) x).Select( d => Math.Round(d, (p!=null&&p.Count>0)?(int)(p[0]):0 )).ToList<decimal>()
        ///     
        /// </summary>
        [JsonIgnore]
        public Func<dynamic, List<dynamic>, dynamic> LinqFx { get; set; }

        /// <summary>
        /// The Expression is the query expression that takes the input values
        /// and returns the result.
        /// 
        /// The definition of the method expression is:
        ///   Func<Expression, List<Expression>, Expression>
        /// where the parameters are:
        ///   * 1 = the current (starting) value of the expression
        ///   * 2 = a list of parameters, if there are any
        ///   * 3 = the resulting value, which may be a scalar or collection value.
        ///   
        /// Example - the Round function for DataTpe Numeric (scalar) and NumericCollection.  
        ///     (x, p) => Math.Round( (decimal)x )
        ///     (x, p) => ((List<decimal>) x).Select( d => Math.Round(d)).ToList<decimal>()
        ///     
        /// Example with Parameter - the RoundTo method for Numeric and NumericCollection
        ///     (x, p) => Math.Round( (decimal)x, (p!=null&&p.Count>0)?(int)(p[0]):0 )
        ///     (x, p) => (x, p) => ((List<decimal>) x).Select( d => Math.Round(d, (p!=null&&p.Count>0)?(int)(p[0]):0 )).ToList<decimal>()
        ///     
        /// </summary>
        [JsonIgnore]
        public Func<Expression, List<Expression>, bool, Expression> Expression { get; set; }

        public int ParameterCount => (ParameterDataTypes?.Length ?? 0);
        public bool HasParameters() => (ParameterDataTypes?.Length ?? 0) > 0;
    }

 
}