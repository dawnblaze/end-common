﻿using Endor.Models;
using System;
using System.Collections.Generic;

namespace Endor.AEL
{
    public class DataTypeMemberInfo
    {
        public DataType DataType { get; set; }
        public Type ParentType { get; set; }
        public string Text { get; set; }
        public string PluralText { get; set; }
        public List<IMemberInfo> Members { get; set; }
        public bool TextMatches(string text)
        {
            return text.Equals(this.Text, StringComparison.InvariantCultureIgnoreCase)
                   || text.Equals(this.Text.Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
