﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.AEL
{
    public enum MemberType
    {
        None = 0,
        Property = 1,
        Method = 2
    }

    public struct MemberTypeInfo
    {
        public MemberType MemberType { get; set; }

        public string Text { get; set; }
    }

    public static class MemberMetaData
    {
        public static Dictionary<MemberType, MemberTypeInfo> TypeDictionary()
        {
            if (MyTypeDictionary == null) CreateTypeDictionary();
            return MyTypeDictionary;
        }

        public static MemberTypeInfo MetaDataInfo(this MemberType mt) => TypeDictionary()[mt];

        private static Dictionary<MemberType, MemberTypeInfo> MyTypeDictionary;

        private static void CreateTypeDictionary()
        {
            MyTypeDictionary = new Dictionary<MemberType, MemberTypeInfo>()
            {
                { MemberType.Property,
                    new MemberTypeInfo { MemberType = MemberType.Property,
                                        Text = "Property" } },

                { MemberType.Method,
                    new MemberTypeInfo { MemberType = MemberType.Method,
                                        Text = "Method"  } }
            };
        }
    }
}
