﻿using Endor.Models;
using System;

namespace Endor.AEL
{
    /// <summary>
    /// This is the base interface for Property and Method members.
    /// </summary>
    public interface IMemberInfo
    {
        /// <summary>
        /// The unqiue ID for this member.  For standard (system) methods, this is unique among all members.
        /// Data-Generated Members (e.g., Order.Role.RoleName) share the same ID but use the RelatedID for diffentiation.
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// The string that display to the user for this member.
        /// </summary>
        string Text { get; set; }

        /// <summary>
        /// The output data type this member produces.
        /// </summary>
        DataType DataType { get; set; }

        /// <summary>
        /// The type of number
        /// </summary>
        NumberType NumberType { get; set; }
        
        /// <summary>
        /// The member type of the specific member (property or method).
        /// </summary>
        MemberType MemberType { get; }

        /// <summary>
        /// An additional field used as an additional ID field for data-generated members.
        /// </summary>
        int RelatedID { get; set; }

        /// <summary>
        /// Parameter Data Types .. only used for Methods.  Null for properties.
        /// </summary>
        DataType[] ParameterDataTypes { get; }
    }

    public static class IMemberInfoExt
    {
        public static bool TextMatches(this IMemberInfo memberInfo, string text)
        {
            return text.Equals(memberInfo.Text, StringComparison.InvariantCultureIgnoreCase)
                   || text.Equals(memberInfo.Text.Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase);
        }
    }
}