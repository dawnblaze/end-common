﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Endor.AEL
{
    public static class BusinessMemberDictionary
    {
        private class _BusinessMemberDictionary : Dictionary<short, (MemberDictionaryByDataType byDataType, MemberDictionaryByType byType)> { }

        private static readonly Lazy<_BusinessMemberDictionary> lazy = new Lazy<_BusinessMemberDictionary>(() => new _BusinessMemberDictionary());

        private static _BusinessMemberDictionary Instance => lazy.Value;

        private static (MemberDictionaryByDataType byDataType, MemberDictionaryByType byType) MemberDictionaries(IDataProvider dataProvider)
        {
            if (!Instance.ContainsKey(dataProvider.BID))
                Instance.Add(dataProvider.BID, GetInitializers(dataProvider));

            return Instance[dataProvider.BID];
        }

        public static List<IMemberInfo> MembersByDataType(IDataProvider dataProvider, DataType dt)
        {
            return MemberDictionaryByDataType(dataProvider)?.GetValueOrDefault(dt, null)?.Members;
        }
        /// <summary>
        /// The recommended way to retrieve a BID specific dictionary.  This dictionary is auto-instantiated on first load.
        /// </summary>
        /// <param name="BID"></param>
        /// <returns></returns>
        public static MemberDictionaryByDataType MemberDictionaryByDataType(IDataProvider dataProvider)
        {
            return MemberDictionaries(dataProvider).byDataType;
        }
        public static List<IMemberInfo> MembersByType(IDataProvider dataProvider, Type dt)
        {
            return MemberDictionaryByType(dataProvider)?.GetValueOrDefault(dt, null)?.Members;
        }
        /// <summary>
        /// The recommended way to retrieve a BID specific dictionary.  This dictionary is auto-instantiated on first load.
        /// </summary>
        /// <param name="BID"></param>
        /// <returns></returns>
        public static MemberDictionaryByType MemberDictionaryByType(IDataProvider dataProvider)
        {
            return MemberDictionaries(dataProvider).byType;
        }
        /// <summary>
        /// Clears a particular BID from the dictionary.  This is called when a change may result in new members.
        /// The BID will be recreated upon first access, so there is no need to load it.
        /// </summary>
        /// <param name="BID"></param>
        public static bool ClearBIDDictionary(short BID) => Instance.Remove(BID);

        private static (MemberDictionaryByDataType byDataType, MemberDictionaryByType byType) GetInitializers(IDataProvider dataProvider)
        {
            MemberDictionaryByDataType resultByDataType = new MemberDictionaryByDataType();
            MemberDictionaryByType resultByType = new MemberDictionaryByType();

            foreach (var initializer in GetBaseMemberInitializers())
            {
                DataTypeInfo dtInfo = initializer.DataType.MetaDataInfo();
                List<IMemberInfo> allMembers = initializer.GetAllMembers(dataProvider)?.ToList();

                resultByDataType.Add(initializer.DataType,
                        new DataTypeMemberInfo()
                        {
                            DataType = initializer.DataType,
                            ParentType = initializer.ParentType,
                            PluralText = dtInfo?.PluralText ?? initializer.DataType.ToString(),
                            Text = dtInfo?.Text ?? initializer.DataType.ToString(),
                            Members = allMembers,
                        });

                if (allMembers != null)
                {
                    DataTypeMemberInfo dataTypeMemberInfo;

                    if (resultByType.ContainsKey(initializer.ParentType))
                        dataTypeMemberInfo = resultByType[initializer.ParentType];
                    else
                    {
                        dataTypeMemberInfo = new DataTypeMemberInfo()
                        {
                            DataType = DataType.None,
                            ParentType = initializer.ParentType,
                            Members = new List<IMemberInfo>(),
                        };
                        resultByType.Add(initializer.ParentType, dataTypeMemberInfo);
                    }

                    if (dataTypeMemberInfo.DataType == DataType.None && dtInfo != null)
                    {
                        dataTypeMemberInfo.DataType = dtInfo.DataType;
                        dataTypeMemberInfo.PluralText = dtInfo.PluralText;
                        dataTypeMemberInfo.Text = dtInfo.Text;
                    }

                    dataTypeMemberInfo.Members.AddRange(allMembers);
                }
            }

            return (resultByDataType, resultByType);
        }

        private static IEnumerable<BaseMemberInitializer> GetBaseMemberInitializers()
        {
            List<BaseMemberInitializer> initializers = new List<BaseMemberInitializer>();
            foreach (Type type in
                Assembly.GetAssembly(typeof(BaseMemberInitializer)).GetTypes()
                .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(BaseMemberInitializer))))
            {
                initializers.Add((BaseMemberInitializer)Activator.CreateInstance(type));
            }
            return initializers;
        }
    }
}
