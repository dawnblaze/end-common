﻿using System;
using System.Collections.Generic;
using Endor.Models;

namespace Endor.AEL
{
    /// <summary>
    /// A MemberDictionaryByDataType is a Dictionary of Members for a DataType.
    /// </summary>
    public class MemberDictionaryByDataType : Dictionary<DataType, DataTypeMemberInfo> { }

    /// <summary>
    /// A MemberDictionary is a Dictionary of Members for a DataType.
    /// </summary>
    public class MemberDictionaryByType : Dictionary<Type, DataTypeMemberInfo> { }
}
