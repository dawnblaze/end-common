﻿using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Linq.Expressions;

namespace Endor.AEL
{
    /// <summary>
    /// This is the class that describes property members.
    /// </summary>
    public class PropertyInfo : IMemberInfo
    {
        public int ID { get; set; }

        public string Text { get; set; }

        public bool IsDefault { get; set; } = false;

        public DataType DataType { get; set; }

        public NumberType NumberType { get; set; }

        public MemberType MemberType { get => AEL.MemberType.Property; }

        public int RelatedID { get; set; }

        /// <summary>
        /// Parameters don't apply to properties, so hard code null.
        /// </summary>
        public DataType[] ParameterDataTypes { get => null; }

        /// <summary>
        /// The LinqFx is the formula (as a linq expression) that takes the input values
        /// and returns the result.
        /// 
        /// The definition of the method is linq expression is:
        ///   Func<dynamic, dynamic>
        /// where the parameters are:
        ///   * 1 = the current (starting) value of the expression
        ///   * 2 = the resulting value, which is always of the same datatype
        ///   
        /// Example - the Round function for DataTpe Numeric (scalar) and NumericCollection.  
        ///     (x, p) => Math.Round( (decimal)x )
        ///     (x, p) => ((List<decimal>) x).Select( d => Math.Round(d)).ToList<decimal>()
        ///     
        /// Example with Parameter - the RoundTo method for Numeric and NumericCollection
        ///     (x, p) => Math.Round( (decimal)x, (p!=null&&p.Count>0)?(int)(p[0]):0 )
        ///     (x, p) => (x, p) => ((List<decimal>) x).Select( d => Math.Round(d, (p!=null&&p.Count>0)?(int)(p[0]):0 )).ToList<decimal>()
        ///     
        /// </summary>
        ///
        [JsonIgnore]
        public Func<dynamic, dynamic> LinqFx { get; set; }

        /// <summary>
        /// The Expression is the query expression that takes the input values
        /// and returns the result.
        /// 
        /// The definition of the method is linq expression is:
        ///   Func<Expression, Expression>
        /// where the parameters are:
        ///   * 1 = the current (starting) value of the expression
        ///   * 2 = the resulting value, which is always of the same datatype
        ///   
        /// Example - the Round function for DataTpe Numeric (scalar) and NumericCollection.  
        ///     (x, p) => Math.Round( (decimal)x )
        ///     (x, p) => ((List<decimal>) x).Select( d => Math.Round(d)).ToList<decimal>()
        ///     
        /// Example with Parameter - the RoundTo method for Numeric and NumericCollection
        ///     (x, p) => Math.Round( (decimal)x, (p!=null&&p.Count>0)?(int)(p[0]):0 )
        ///     (x, p) => (x, p) => ((List<decimal>) x).Select( d => Math.Round(d, (p!=null&&p.Count>0)?(int)(p[0]):0 )).ToList<decimal>()
        ///     
        /// </summary>
        ///
        [JsonIgnore]
        public Func<Expression, bool, Expression> Expression { get; set; }
    }


}