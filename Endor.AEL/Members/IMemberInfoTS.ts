﻿namespace Endor.AEL.TypeScript
{

    enum DataType {
        None = 0,

        String = 1,
        Number = 2,
        Boolean = 3,
        DateTime = 9,

        Location = 1005,
        Company = 2000,
        CompanyCustomField = 2001,
        Industry = 2011,
        Origin = 2012,
        Contact = 3000,
        ContactCustomField = 3001,
        Employee = 5000,
        EmployeeCustomField = 5001,
        EmployeeRole = 5004,
        Team = 5020,
        Opportunity = 9000,
        OpportunityCustomField = 9001,
        OrderCustomField = 10001,
        Order = 10002,
        Estimate = 10003,
        PurchaseOrder = 10004,
        OrderContactRole = 10013,
        OrderEmployeeRole = 10014,
        OrderNotes = 10015,
        LineItem = 10020,
        ItemStatus = 10022,
        Substatus = 10023,
        Destination = 10040,
        DestinationStatus = 10042,
        TaxGroup = 11101,

        // Collections are the same, but with a negative ID
        BooleanList = -Boolean,
        NumberList = -Number,
        StringList = -String,
        DateTimeList = -DateTime,

        LocationList = -Location,
        CompanyList = -Company,
        CompanyCustomFieldList = -CompanyCustomField,
        IndustryList = -Industry,
        OriginList = -Origin,
        ContactList = -Contact,
        ContactCustomFieldList = -ContactCustomField,
        EmployeeList = -Employee,
        EmployeeCustomFieldList = -EmployeeCustomField,
        EmployeeRoleList = -EmployeeRole,
        TeamList = -Team,
        OpportunityList = -Opportunity,
        OrderCustomFieldList = -OrderCustomField,
        OrderList = -Order,
        EstimateList = -Estimate,
        PurchaseOrderList = -PurchaseOrder,
        OrderContactRoleList = -OrderContactRole,
        OrderEmployeeRoleList = -OrderEmployeeRole,
        OrderNotesList = -OrderNotes,
        LineItemList = -LineItem,
        ItemStatusList = -ItemStatus,
        SubstatusList = -Substatus,
        DestinationList = -Destination,
        DestinationStatusList = -DestinationStatus,
        TaxGroupList = -TaxGroup,

        // These enum values are used 
        // in order to signify a type that can reflect 
        // any type, any value type, or any object type.
        //
        // They are not real value types and should not be used in expressions
        AnyObjectScalar = 35000,
        AnyObjectList = -35002,
    }

    enum MemberType {
        None = 0,
        Property = 1,
        Method = 2
    }

    enum OperatorType {
        EQ = 0,
        NEQ = 1,
        LT = 2,
        GTE = 3,
        GT = 4,
        LTE = 5,
        AND = 6,
        OR = 8,
        NOT = 10,
        ISNULL = 12,
        ISNOTNULL = 13,
        IN = 14,
        NOTIN = 15,
        INCLUDES = 16,
        NOTINCLUDES = 17,
        CONTAINSTEXT = 18,
        NOTCONTAINSTEXT = 19,
        CONTAINSWORDS = 20,
        NOTCONTAINSWORDS = 21, 
    }

    enum ValueChoices {
        none = 0,
        single = 1,
        multiple = 2
    }

    interface DataTypeInfo {
        DataType : DataType,
        Text : string,
        PluralText: string,
        IsList: Boolean,
    }


    /// <summary>
    /// This is the base interface for Property and Method members.
    /// </summary>
    interface IMemberInfoTS {
        // Natural key = DataType + ID + RelatedID?
        //      DataType = 
        //      ID = identifier for member on this DataType
        //      RelatedID? = only used for data generated Members 
        //                  e.g., EmployeeRoles>Salesperson, EmployeeRoles>CSR, EmployeeRoles>AccountManager => all use the same logic and ID.
        //                  The RelatedID stores the ID of the particular Role we are using.
        //
        DataType: DataType,
        ID: number,
        RelatedID?: number,

        Text: string, //Order,Salesperson
        MemberType: MemberType,
        ParameterDataTypes?: DataType[],

        ChildMembers?: IMemberInfoTS[],
        Operators?: IOperatorInfo[],
        ConditionalOperators?:  IOperatorInfo[]
    }

    interface IOperatorInfo {
        OperatorType: OperatorType,
        DataType: DataType,
        Symbol: String,
        Text: String,
        ASCIICode: String,
        ValidDataTypes: {
            [key: number]:
            {
                lhsOperand: DataType,
                rhsOperand: DataType,
            }[]
        }
    }

    interface IConditionalOp {
        operator: OperatorType,
        valuechoice: ValueChoices
    }

    //there are top level datat types/members that are not linked to the starting/initial datatype
    //ex: SystemDate is a peer of Order on add new Order queue

    //initial view
    // [ Order v]

    //user clicks dropdown
    // [ Order v]
    // [ order       ]
    // [ system date ]

    // user selects order, client loads order child members
    // user waits for network
    // [ Order v]
    // [ order > ]

    //network finishes
    // [ Order v]
    // [ order > ]
    // [   salesperson ]
    // [   order number ]

    // user selects salesperson, client loads order child members
    // [ Order v]
    // [ order > ]
    // [   salesperson ]


    /*
        * { DataType.Order: [{Salesperson}]}
        * { DataType.Salesperson: [{FirstName, Locators}] }
        * */

    /*
        * Order > 
        * Order > Salesperson
        * Order > Salesperson > FN
        * Order > Salesperson > Locators
        * */

    class MemberInfoDictionary {

        // key = DataType ... 
        dict: { [key: number]: IMemberInfoTS[] };

        Add(dt: DataType, info: IMemberInfoTS[]) {
            this.dict[dt] = info;
        }

        LoadOrder() {
            this.Add(DataType.Order,
                [
                    {
                        ID: 1017,
                        Text: "Description",
                        MemberType: MemberType.Property,
                        DataType: DataType.String,
                    },

                    {
                        ID: 1048,
                        Text: "Salesperson",
                        MemberType: MemberType.Property,
                        DataType: DataType.Employee,
                    }

                ]);
        };


        LoadBoolean() {
            this.Add(DataType.Boolean,
                [
                    {
                        ID: 1002,
                        Text: "ToString",
                        MemberType: MemberType.Method,
                        DataType: DataType.String,
                    },

                    {
                        ID: 1003,
                        Text: "IsNotSet",
                        MemberType: MemberType.Property,
                        DataType: DataType.Boolean,
                    }
                ]);
        };
    }
}
