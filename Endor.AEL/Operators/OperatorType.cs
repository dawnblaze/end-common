﻿using System;
using System.Text;

namespace Endor.AEL
{
    public enum OperatorType
    {
        EQ = 0,
        NEQ = 1,
        LT = 2,
        GTE = 3,
        GT = 4,
        LTE = 5,
        AND = 6,
        OR = 8,
        NOT = 10,
        ISNULL = 12,
        ISNOTNULL = 13,
        IN = 14,
        NOTIN = 15,
        INCLUDES = 16,
        NOTINCLUDES = 17,
        CONTAINSTEXT = 18,
        NOTCONTAINSTEXT = 19,
        CONTAINSWORDS = 20,
        NOTCONTAINSWORDS = 21,
        ISTRUE = 22,
        ISFALSE = 23,
        ISFUTURE = 24,
        INPAST = 25,
        ISTODAY = 26,
    }
}
