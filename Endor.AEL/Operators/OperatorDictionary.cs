﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Collections;

namespace Endor.AEL
{
    public class OperatorDictionary : Dictionary<OperatorType, OperatorInfo>
    {
        // Use a private constructor and a singleton pattern with Lazy instantiation
        private OperatorDictionary()
        {
            FillDictionary();
        }

        private static readonly Lazy<OperatorDictionary> lazy = new Lazy<OperatorDictionary>(() => new OperatorDictionary());

        public static OperatorDictionary Instance => lazy.Value;

        private Expression ExpressionStringIsNullOrEmpty(Expression stringExp)
        {
            return Expression.Call(
                  typeof(string)
                , "IsNullOrEmpty"
                , null
                , new[] { stringExp });
        }

        private Expression ExpressionStringAreEqual(Expression left, Expression right, bool forSQL)
        {
            var compare = Expression.Call(
                  typeof(string)
                , "Compare"
                , null
                //, new[] { left, right, AELHelper.Expressions.BooleanTrue }
                , new[] { left, right }
                );

            if (forSQL)
                return Expression.Equal(compare, Expression.Constant(0));

            var leftNullOrEmpty = ExpressionStringIsNullOrEmpty(left);
            var rightNullOrEmpty = ExpressionStringIsNullOrEmpty(right);

            return Expression.Condition(
                Expression.AndAlso(leftNullOrEmpty, rightNullOrEmpty)
                , AELHelper.Expressions.BooleanTrue
                , Expression.Condition(
                    leftNullOrEmpty
                    , AELHelper.Expressions.BooleanFalse
                    , Expression.Condition(
                        rightNullOrEmpty
                        , AELHelper.Expressions.BooleanFalse
                        , Expression.Equal(compare, Expression.Constant(0))
                        )
                    )
                );
        }

        private Expression ExpressionStringIsLessThan(Expression left, Expression right)
        {
            var leftNullOrEmpty = ExpressionStringIsNullOrEmpty(left);
            var rightNullOrEmpty = ExpressionStringIsNullOrEmpty(right);

            var compare = Expression.Call(
                  typeof(string)
                , "Compare"
                , null
                //, new[] { left, right, AELHelper.Expressions.BooleanTrue }
                , new[] { left, right }
                );

            return Expression.Condition(
                Expression.AndAlso(leftNullOrEmpty, rightNullOrEmpty)
                , AELHelper.Expressions.BooleanFalse
                , Expression.Condition(
                    leftNullOrEmpty
                    , AELHelper.Expressions.BooleanTrue
                    , Expression.Condition(
                        rightNullOrEmpty
                        , AELHelper.Expressions.BooleanFalse
                        , Expression.LessThan(compare, Expression.Constant(0))
                        )
                    )
                );
        }

        private Expression ExpressionStringIsGreaterThan(Expression left, Expression right)
        {
            var leftNullOrEmpty = ExpressionStringIsNullOrEmpty(left);
            var rightNullOrEmpty = ExpressionStringIsNullOrEmpty(right);

            var compare = Expression.Call(
                  typeof(string)
                , "Compare"
                , null
                //, new[] { left, right, AELHelper.Expressions.BooleanTrue }
                , new[] { left, right }
                );

            return Expression.Condition(
                Expression.AndAlso(leftNullOrEmpty, rightNullOrEmpty)
                , AELHelper.Expressions.BooleanFalse
                , Expression.Condition(
                    leftNullOrEmpty
                    , AELHelper.Expressions.BooleanFalse
                    , Expression.Condition(
                        rightNullOrEmpty
                        , AELHelper.Expressions.BooleanTrue
                        , Expression.GreaterThan(compare, Expression.Constant(0))
                        )
                    )
                );
        }

        private Expression ExpressionStringIsLessThanOrEqual(Expression left, Expression right)
        {
            var leftNullOrEmpty = ExpressionStringIsNullOrEmpty(left);
            var rightNullOrEmpty = ExpressionStringIsNullOrEmpty(right);

            var compare = Expression.Call(
                  typeof(string)
                , "Compare"
                , null
                //, new[] { left, right, AELHelper.Expressions.BooleanTrue }
                , new[] { left, right }
                );

            return Expression.Condition(
                Expression.AndAlso(leftNullOrEmpty, rightNullOrEmpty)
                , AELHelper.Expressions.BooleanTrue
                , Expression.Condition(
                    leftNullOrEmpty
                    , AELHelper.Expressions.BooleanTrue
                    , Expression.Condition(
                        rightNullOrEmpty
                        , AELHelper.Expressions.BooleanFalse
                        , Expression.LessThanOrEqual(compare, Expression.Constant(0))
                        )
                    )
                );
        }

        private Expression ExpressionStringIsGreaterThanOrEqual(Expression left, Expression right)
        {
            var leftNullOrEmpty = ExpressionStringIsNullOrEmpty(left);
            var rightNullOrEmpty = ExpressionStringIsNullOrEmpty(right);

            var compare = Expression.Call(
                  typeof(string)
                , "Compare"
                , null
                //, new[] { left, right, AELHelper.Expressions.BooleanTrue }
                , new[] { left, right }
                );

            return Expression.Condition(
                Expression.AndAlso(leftNullOrEmpty, rightNullOrEmpty)
                , AELHelper.Expressions.BooleanTrue
                , Expression.Condition(
                    leftNullOrEmpty
                    , AELHelper.Expressions.BooleanFalse
                    , Expression.Condition(
                        rightNullOrEmpty
                        , AELHelper.Expressions.BooleanTrue
                        , Expression.GreaterThanOrEqual(compare, Expression.Constant(0))
                        )
                    )
                );
        }

        private Expression ExpressionAtomValueAreEqual(Expression left, Expression right, bool forSQL)
        {
            var compare = Expression.Equal(Expression.Property(left, "ID"), Expression.Property(right, "ID"));

            /*
            var compare = Expression.AndAlso(
                    Expression.Equal(Expression.Property(left, "BID"), Expression.Property(right, "BID"))
                    , Expression.AndAlso(
                        Expression.Equal(Expression.Convert(Expression.Property(left, "ID"), typeof(int)), Expression.Convert(Expression.Property(right, "ID"), typeof(int)))
                        , Expression.Equal(Expression.Property(left, "ClassTypeID"), Expression.Property(right, "ClassTypeID"))
                      )
                );
            */

            if (forSQL)
                return compare;

            var leftNull = Expression.Equal(left, Expression.Constant(null, left.Type));
            var rightNull = Expression.Equal(right, Expression.Constant(null, right.Type));

            return Expression.Condition(
                Expression.AndAlso(leftNull, rightNull)
                , AELHelper.Expressions.BooleanTrue
                , Expression.Condition(
                    leftNull
                    , AELHelper.Expressions.BooleanFalse
                    , Expression.Condition(
                        rightNull
                        , AELHelper.Expressions.BooleanFalse
                        , compare
                        )
                    )
                );
        }

        private Expression ExpressionAtomValueIsNull(Expression left, bool forSQL)
        {
            var compare = Expression.Equal(Expression.Convert(Expression.Property(left, "ID"), typeof(int?)), Expression.Constant(null, typeof(int?)));

            if (forSQL)
                return compare;

            var leftNull = Expression.Equal(left, Expression.Constant(null, left.Type));

            return Expression.Condition(
                leftNull
                , AELHelper.Expressions.BooleanTrue
                , compare
                );
        }

        private Expression ExpressionAtomValueIsNotNull(Expression left, bool forSQL)
        {
            if (forSQL)
                return Expression.NotEqual(Expression.Convert(Expression.Property(left, "ID"), typeof(int?)), Expression.Constant(null, typeof(int?)));

            var compare = Expression.NotEqual(Expression.Convert(Expression.Property(left, "ID"), typeof(int?)), Expression.Constant(null, typeof(int?)));

            if (forSQL)
                return compare;

            var leftNull = Expression.Equal(left, Expression.Constant(null, left.Type));

            return Expression.Condition(
                leftNull
                , AELHelper.Expressions.BooleanFalse
                , compare
                );
        }

        private bool LinqAtomsAreEqual(dynamic left, dynamic right)
        {
            bool GetReturnValue<I>(IAtom<I> l, IAtom<I> r)
                where I : struct, IConvertible
            {
                return l.BID == r.BID && Convert.ToInt32(l.ID) == Convert.ToInt32(r.ID) && l.ClassTypeID == r.ClassTypeID;
            }

            if (left == null && right == null)
                return true;

            if (left == null)
                return false;

            if (right == null)
                return false;

            Type idType = left.GetType().GetGenericArguments()[0];

            if (idType == typeof(int))
                return GetReturnValue<int>((IAtom<int>)left, (IAtom<int>)right);

            if (idType == typeof(short))
                return GetReturnValue<short>((IAtom<short>)left, (IAtom<short>)right);

            if (idType == typeof(byte))
                return GetReturnValue<byte>((IAtom<byte>)left, (IAtom<byte>)right);

            throw new NotImplementedException($"IAtom<{idType.Name}> not implemented.");
        }

        private bool LinqItemInList(dynamic list, dynamic item)
        {
            bool GetReturnValue<I>(List<IAtom<I>> theList, IAtom<I> theItem)
                where I : struct, IConvertible
            {
                return theList.Any(theListItem =>
                {
                    if (theListItem == null && theItem == null)
                        return true;

                    if (theListItem == null)
                        return false;

                    if (theItem == null)
                        return false;

                    return theListItem.BID == theItem.BID && Convert.ToInt32(theListItem.ID) == Convert.ToInt32(theItem.ID) && theListItem.ClassTypeID == theItem.ClassTypeID;
                }
                );
            }

            Type idType = list.GetType().GetGenericArguments()[0].GetGenericArguments()[0];

            if (idType == typeof(int))
                return GetReturnValue<int>(((IEnumerable)list).Cast<IAtom<int>>().ToList(), (IAtom<int>)item);

            if (idType == typeof(short))
                return GetReturnValue<short>(((IEnumerable)list).Cast<IAtom<short>>().ToList(), (IAtom<short>)item);

            if (idType == typeof(byte))
                return GetReturnValue<byte>(((IEnumerable)list).Cast<IAtom<byte>>().ToList(), (IAtom<byte>)item);

            throw new NotImplementedException($"IAtom<{idType.Name}> not implemented.");
        }

        private bool LinqListInList(dynamic list1, dynamic list2)
        {
            bool GetReturnValue<I>(List<IAtom<I>> l, List<IAtom<I>> r)
                where I : struct, IConvertible
            {
                return l.Any(theListItem1 =>
                {
                    return r.Any(theListItem2 =>
                    {
                        if (theListItem1 == null && theListItem2 == null)
                            return true;

                        if (theListItem1 == null)
                            return false;

                        if (theListItem2 == null)
                            return false;

                        return theListItem1.BID == theListItem2.BID && Convert.ToInt32(theListItem1.ID) == Convert.ToInt32(theListItem2.ID) && theListItem1.ClassTypeID == theListItem2.ClassTypeID;
                    }
                );
                }
                );
            }

            Type idType = list1.GetType().GetGenericArguments()[0].GetGenericArguments()[0];

            if (idType == typeof(int))
                return GetReturnValue<int>(((IEnumerable)list1).Cast<IAtom<int>>().ToList(), ((IEnumerable)list2).Cast<IAtom<int>>().ToList());

            if (idType == typeof(short))
                return GetReturnValue<short>(((IEnumerable)list1).Cast<IAtom<short>>().ToList(), ((IEnumerable)list2).Cast<IAtom<short>>().ToList());

            if (idType == typeof(byte))
                return GetReturnValue<byte>(((IEnumerable)list1).Cast<IAtom<byte>>().ToList(), ((IEnumerable)list2).Cast<IAtom<byte>>().ToList());

            throw new NotImplementedException($"IAtom<{idType.Name}> not implemented.");
        }

        private void FillDictionary()
        {
            // create a number of shared dictionaries showing the valid type-mappings
            new List<OperatorInfo>()
            {
                new OperatorInfo
                {
                    OperatorType = OperatorType.EQ,
                    DataType = DataType.Boolean,
                    Symbol = "is",
                    Text = "is",
                    ASCIICode = (char)61,
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType= DataType.Boolean,
                            RightDataType =  DataType.Boolean,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = (bool?)lhs;
                                bool? r = (bool?)rhs;

                                return l == r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                if (right == null)
                                {
                                    return Expression.Constant(false, typeof(bool?));
                                }

                                var nullCheck = Expression.NotEqual(right, Expression.Constant(null, right.Type));
                                var cond = Expression.Equal(left, Expression.Convert(right, left.Type));
                                var exp = Expression.AndAlso(nullCheck, cond);

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = (decimal?)lhs;
                                decimal? r = (decimal?)rhs;

                                return l == r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.Equal(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) && string.IsNullOrEmpty(r))
                                    return true;

                                if (string.IsNullOrEmpty(l))
                                    return false;

                                if (string.IsNullOrEmpty(r))
                                    return false;

                                return l == r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionStringAreEqual(left, right, forSQL));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.DateTime,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;
                                DateTime? r = (DateTime?)rhs;

                                return l == r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.Equal(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectScalar,
                            RightDataType = DataType.AnyObjectScalar,
                            LinqFx = (lhs, rhs) =>
                            {
                                return LinqAtomsAreEqual(lhs, rhs);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionAtomValueAreEqual(left, right, forSQL));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = (decimal?)lhs;
                                decimal? r = (decimal?)rhs;

                                return l == r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , left
                                    , right
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.NEQ,
                    DataType = DataType.Boolean,
                    Symbol = "is not",
                    Text = "is not",
                    ASCIICode = (char)8800,
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.Boolean,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = ((bool?)lhs);
                                bool? r = ((bool?)rhs);

                                return l != r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                if (right == null)
                                {
                                    return Expression.Constant(false, typeof(bool?));
                                }

                                var nullCheck = Expression.NotEqual(right, Expression.Constant(null, right.Type));
                                var cond = Expression.NotEqual(left, Expression.Convert(right, left.Type));
                                var exp = Expression.AndAlso(nullCheck, cond);

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                return l != r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.NotEqual(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) && string.IsNullOrEmpty(r))
                                    return false;

                                if (string.IsNullOrEmpty(l))
                                    return true;

                                if (string.IsNullOrEmpty(r))
                                    return true;

                                return l != r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.Not(ExpressionStringAreEqual(left, right, forSQL)));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.DateTime,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;
                                DateTime? r = (DateTime?)rhs;

                                return l != r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.NotEqual(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectScalar,
                            RightDataType = DataType.AnyObjectScalar,
                            LinqFx = (lhs, rhs) =>
                            {
                                return !LinqAtomsAreEqual(lhs, rhs);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.Not(ExpressionAtomValueAreEqual(left, right, forSQL)));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                return l != r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(decimal?)
                                        }
                                        , left
                                        , right
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.LT,
                    DataType = DataType.Boolean,
                    Symbol = "<",
                    Text = "less than",
                    ASCIICode = (char)60,
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l.Value < r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.LessThan(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) && string.IsNullOrEmpty(r))
                                    return false;

                                if (string.IsNullOrEmpty(l))
                                    return true;

                                if (string.IsNullOrEmpty(r))
                                    return false;

                                return l.CompareTo(r) < 0;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionStringIsLessThan(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.DateTime,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;
                                DateTime? r = (DateTime?)rhs;

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l < r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.LessThan(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l.Value < r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var param = Expression.Parameter(right.Type, "li");
                                var conditionAny = Expression.LessThan(param, right);

                                Expression exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        right.Type
                                    }
                                    , left
                                    , Expression.Lambda(
                                        conditionAny
                                        , param
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.LTE,
                    DataType = DataType.Boolean,
                    Symbol = "<=",
                    Text = "less than or equal to",
                    ASCIICode = (char)243,
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l.Value <= r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.LessThanOrEqual(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) && string.IsNullOrEmpty(r))
                                    return true;

                                if (string.IsNullOrEmpty(l))
                                    return true;

                                if (string.IsNullOrEmpty(r))
                                    return false;

                                return l.CompareTo(r) <= 0;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionStringIsLessThanOrEqual(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.DateTime,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;
                                DateTime? r = (DateTime?)rhs;

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l <= r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.LessThanOrEqual(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l.Value < r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var param = Expression.Parameter(right.Type, "li");
                                var conditionAny = Expression.LessThanOrEqual(param, right);

                                Expression exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        right.Type
                                    }
                                    , left
                                    , Expression.Lambda(
                                        conditionAny
                                        , param
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.GT,
                    DataType = DataType.Boolean,
                    Symbol = ">",
                    Text = "greater than",
                    ASCIICode = (char)62,
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l.Value > r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.GreaterThan(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) && string.IsNullOrEmpty(r))
                                    return false;

                                if (string.IsNullOrEmpty(l))
                                    return false;

                                if (string.IsNullOrEmpty(r))
                                    return true;

                                return l.CompareTo(r) > 0;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionStringIsGreaterThan(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.DateTime,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;
                                DateTime? r = (DateTime?)rhs;

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l > r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.GreaterThan(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l.Value > r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var param = Expression.Parameter(right.Type, "li");
                                var conditionAny = Expression.GreaterThan(param, right);

                                Expression exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        right.Type
                                    }
                                    , left
                                    , Expression.Lambda(
                                        conditionAny
                                        , param
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.GTE,
                    DataType = DataType.Boolean,
                    Symbol = ">=",
                    Text = "greater than or equal to",
                    ASCIICode = (char)242,
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l.Value >= r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.GreaterThanOrEqual(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) && string.IsNullOrEmpty(r))
                                    return true;

                                if (string.IsNullOrEmpty(l))
                                    return false;

                                if (string.IsNullOrEmpty(r))
                                    return true;

                                return l.CompareTo(r) >= 0;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionStringIsGreaterThanOrEqual(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.DateTime,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;
                                DateTime? r = (DateTime?)rhs;

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l >= r;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.GreaterThanOrEqual(left, right));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                decimal? r = ((decimal?)rhs);

                                if (!l.HasValue || !r.HasValue)
                                    return false;

                                return l.Value >= r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var param = Expression.Parameter(right.Type, "li");
                                var conditionAny = Expression.GreaterThanOrEqual(param, right);

                                Expression exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        right.Type
                                    }
                                    , left
                                    , Expression.Lambda(
                                        conditionAny
                                        , param
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.AND,
                    DataType = DataType.Boolean,
                    Symbol = "and",
                    Text = "and",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.Boolean,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = ((bool?)lhs);
                                bool? r = ((bool?)rhs);

                                if (!l.HasValue && !r.HasValue)
                                    return false;

                                if (!l.HasValue)
                                    return r.Value;

                                if (!r.HasValue)
                                    return l.Value;

                                return l.Value && r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Expression exp = null;

                                if (forSQL)
                                {
                                    if (Nullable.GetUnderlyingType(left.Type) != null && Nullable.GetUnderlyingType(right.Type) != null) // both are nullable
                                    {
                                        var leftValue = Expression.Property(left, "Value");
                                        var rightValue = Expression.Property(right, "Value");
                                        exp = Expression.AndAlso(leftValue, rightValue);
                                    } else
                                    {
                                        if (right == null)
                                        {
                                            return Expression.Constant(false, typeof(bool?));
                                        }

                                        var nullCheck = Expression.NotEqual(right, Expression.Constant(null, right.Type));
                                        var cond = Expression.AndAlso(left, Expression.Convert(right, left.Type));
                                        exp = Expression.AndAlso(nullCheck, cond);
                                    }
                                } else
                                {
                                    var leftValue = Expression.Property(left, "Value");
                                    var rightValue = Expression.Property(right, "Value");

                                    exp = Expression.Condition(
                                        Expression.AndAlso(
                                            Expression.Equal(left, AELHelper.Expressions.NullBoolean)
                                            , Expression.Equal(right, AELHelper.Expressions.NullBoolean)
                                            )
                                        , AELHelper.Expressions.BooleanFalse
                                        , Expression.Condition(
                                            Expression.Equal(left, AELHelper.Expressions.NullBoolean)
                                            , rightValue
                                            , Expression.Condition(
                                                Expression.Equal(right, AELHelper.Expressions.NullBoolean)
                                                , leftValue
                                                , Expression.AndAlso(leftValue, rightValue)
                                                )
                                            )
                                        );
                                }

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.OR,
                    DataType = DataType.Boolean,
                    Symbol = "or",
                    Text = "or",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.Boolean,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = ((bool?)lhs);
                                bool? r = ((bool?)rhs);

                                if (!l.HasValue && !r.HasValue)
                                    return false;

                                if (!l.HasValue)
                                    return r.Value;

                                if (!r.HasValue)
                                    return l.Value;

                                return l.Value || r.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Expression exp = null;

                                if (forSQL)
                                {
                                    if (Nullable.GetUnderlyingType(left.Type) != null && Nullable.GetUnderlyingType(right.Type) != null) // both are nullable
                                    {
                                        var leftValue = Expression.Property(left, "Value");
                                        var rightValue = Expression.Property(right, "Value");
                                        exp = Expression.OrElse(leftValue, rightValue);
                                    } else
                                    {
                                        if (right == null)
                                        {
                                            return Expression.Constant(false, typeof(bool?));
                                        }

                                        var nullCheck = Expression.NotEqual(right, Expression.Constant(null, right.Type));
                                        var cond = Expression.OrElse(left, Expression.Convert(right, left.Type));
                                        exp = Expression.AndAlso(nullCheck, cond);
                                    }
                                } else
                                {
                                    var leftValue = Expression.Property(left, "Value");
                                    var rightValue = Expression.Property(right, "Value");

                                    exp = Expression.Condition(
                                        Expression.AndAlso(
                                            Expression.Equal(left, AELHelper.Expressions.NullBoolean)
                                            , Expression.Equal(right, AELHelper.Expressions.NullBoolean)
                                            )
                                        , AELHelper.Expressions.BooleanFalse
                                        , Expression.Condition(
                                            Expression.Equal(left, AELHelper.Expressions.NullBoolean)
                                            , rightValue
                                            , Expression.Condition(
                                                Expression.Equal(right, AELHelper.Expressions.NullBoolean)
                                                , leftValue
                                                , Expression.OrElse(leftValue, rightValue)
                                                )
                                            )
                                        );
                                }

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.ISNULL,
                    DataType = DataType.Boolean,
                    Symbol = "is blank",
                    Text = "is blank",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => !((bool?)lhs).HasValue,
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.Equal(left, AELHelper.Expressions.BooleanFalse));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => !((decimal?)lhs).HasValue,
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.Equal(left, AELHelper.Expressions.NullNumber));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => string.IsNullOrEmpty((string)lhs),
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionStringIsNullOrEmpty(left));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => !((DateTime?)lhs).HasValue,
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.Equal(left, AELHelper.Expressions.NullDateTime));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectScalar,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => lhs == null,
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionAtomValueIsNull(left, forSQL));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => !((decimal?)lhs).HasValue,
                            Expression = (left, right, forSQL) =>
                            {
                                var param = Expression.Parameter(typeof(decimal?), "li");
                                var conditionAny = Expression.Equal(param, AELHelper.Expressions.NullNumber);

                                Expression exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        conditionAny
                                        , param
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    }
                },
                new OperatorInfo {
                    OperatorType = OperatorType.ISNOTNULL,
                    DataType = DataType.Boolean,
                    Symbol = "has a value",
                    Text = "has a value",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => ((bool?)lhs).HasValue,
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.NotEqual(left, AELHelper.Expressions.BooleanFalse));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => ((decimal?)lhs).HasValue,
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.NotEqual(left, AELHelper.Expressions.NullNumber));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => !string.IsNullOrEmpty((string)lhs),
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.Not(ExpressionStringIsNullOrEmpty(left)));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => ((DateTime?)lhs).HasValue,
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(Expression.NotEqual(left, AELHelper.Expressions.NullDateTime));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectScalar,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => lhs != null,
                            Expression = (left, right, forSQL) =>
                            {
                                return AELHelper.Expressions.ConvertToBoolean(ExpressionAtomValueIsNotNull(left, forSQL));
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) => ((decimal?)lhs).HasValue,
                            Expression = (left, right, forSQL) =>
                            {
                                var param = Expression.Parameter(typeof(decimal?), "li");
                                var conditionAny = Expression.NotEqual(param, AELHelper.Expressions.NullNumber);

                                Expression exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        conditionAny
                                        , param
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    }
                },
                new OperatorInfo {
                    OperatorType = OperatorType.IN,
                    DataType = DataType.Boolean,
                    Symbol = "is in",
                    Text = "is in",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.BooleanList,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = ((bool?)lhs);
                                List<bool?> r = (List<bool?>)rhs;

                                if (r == null)
                                    return false;

                                return r.Contains(l);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(bool?)
                                    }
                                    , right
                                    , left
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.NumberList,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                List<decimal?> r = (List<decimal?>)rhs;

                                if (r == null)
                                    return false;

                                return r.Contains(l);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , right
                                    , left
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.StringList,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                List<string> r = ((List<string>)rhs).ConvertAll(s => s?.ToUpper());

                                if (r == null)
                                    return false;

                                return r.Any(ri =>
                                {
                                    if (string.IsNullOrEmpty(l) && string.IsNullOrEmpty(ri))
                                        return true;

                                    if (string.IsNullOrEmpty(l))
                                        return false;

                                    if (string.IsNullOrEmpty(ri))
                                        return false;

                                    return l == ri;
                                });
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                if (forSQL)
                                {
                                    Expression exp = Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(string)
                                        }
                                        , right
                                        , left
                                        );

                                    return AELHelper.Expressions.ConvertToBoolean(exp);
                                }

                                else
                                {
                                    var param = Expression.Parameter(typeof(string), "ri");
                                    var conditionAny = ExpressionStringAreEqual(left, param, forSQL);

                                    Expression exp = Expression.Call(
                                        typeof(Enumerable)
                                        , "Any"
                                        , new Type[]
                                        {
                                            typeof(string)
                                        }
                                        , right
                                        , Expression.Lambda(
                                            conditionAny
                                            , param
                                            )
                                        );

                                    return AELHelper.Expressions.ConvertToBoolean(exp);
                                }
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.DateTimeList,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = ((DateTime?)lhs);
                                List<DateTime?> r = (List<DateTime?>)rhs;

                                if (r == null)
                                    return false;

                                return r.Contains(l);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(DateTime?)
                                    }
                                    , right
                                    , left
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectScalar,
                            RightDataType = DataType.AnyObjectList,
                            LinqFx = (lhs, rhs) =>
                            {
                                if (rhs == null)
                                    return false;

                                return LinqItemInList(rhs, lhs);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Type rightAtomType = right.Type.GetGenericArguments()[0];

                                var paramAny = Expression.Parameter(rightAtomType, "ri");
                                var conditionAny = ExpressionAtomValueAreEqual(left, paramAny, forSQL);

                                var callAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        rightAtomType
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionAny
                                        , paramAny
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(callAny);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.BooleanList,
                            RightDataType = DataType.BooleanList,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<bool?> l = (List<bool?>)lhs;
                                List<bool?> r = (List<bool?>)rhs;

                                if (l == null || r == null)
                                    return false;

                                return l.All(li => r.Contains(li));
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAll = Expression.Parameter(typeof(bool?), "li");
                                var paramAny = Expression.Parameter(typeof(bool?), "ri");
                                var conditionAny = Expression.Equal(paramAll, paramAny);

                                var callAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(bool?)
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionAny
                                        , paramAny
                                        )
                                    );

                                var callAll = Expression.Call(
                                    typeof(Enumerable)
                                    , "All"
                                    , new Type[]
                                    {
                                        typeof(bool?)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callAny
                                        , paramAll
                                        )
                                    );

                                var exp = Expression.Condition(
                                    Expression.OrElse(
                                        Expression.Equal(left, AELHelper.Expressions.NullBooleanList)
                                        , Expression.Equal(right, AELHelper.Expressions.NullBooleanList)
                                        )
                                    , AELHelper.Expressions.BooleanFalse
                                    , callAll
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.NumberList,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<decimal?> l = (List<decimal?>)lhs;
                                List<decimal?> r = (List<decimal?>)rhs;

                                if (l == null || r == null)
                                    return false;

                                return l.All(li => r.Contains(li));
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAll = Expression.Parameter(typeof(decimal?), "li");

                                var callContains = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , right
                                    , paramAll
                                    );

                                var callAll = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callContains
                                        , paramAll
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(callAll);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.StringList,
                            RightDataType = DataType.StringList,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<string> l = ((List<string>)lhs).ConvertAll(s => s?.ToUpper());
                                List<string> r = ((List<string>)rhs).ConvertAll(s => s?.ToUpper());

                                if (l == null || r == null)
                                    return false;

                                return l.All( li =>
                                {
                                    return r.Any(ri =>
                                    {
                                        if (string.IsNullOrEmpty(li) && string.IsNullOrEmpty(ri))
                                            return true;

                                        if (string.IsNullOrEmpty(li))
                                            return false;

                                        if (string.IsNullOrEmpty(ri))
                                            return false;

                                        return li == ri;
                                    }
                                );
                                }
                                );
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAll = Expression.Parameter(typeof(string), "li");
                                //var paramAny = Expression.Parameter(typeof(string), "ri");
                                //var conditionAny = ExpressionStringAreEqual(paramAll, paramAny, forSQL);

                                //var callAny = Expression.Condition(
                                //    Expression.Equal(right, AELHelper.Expressions.NullStringList),
                                //    AELHelper.Expressions.BooleanFalse,
                                //    Expression.Call(
                                //    typeof(Enumerable)
                                //    , "Any"
                                //    , new Type[]
                                //    {
                                //        typeof(string)
                                //    }
                                //    , right
                                //    , Expression.Lambda(
                                //        conditionAny
                                //        , paramAny
                                //        )
                                //    )
                                //);

                                var callContains = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(string)
                                    }
                                    , right
                                    , paramAll
                                    );

                                var callAll = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(string)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callContains
                                        , paramAll
                                        )
                                    );

                                if (forSQL)
                                    return AELHelper.Expressions.ConvertToBoolean(callAll);

                                var exp = Expression.Condition(
                                    Expression.OrElse(
                                        Expression.Equal(left, AELHelper.Expressions.NullStringList)
                                        , Expression.Equal(right, AELHelper.Expressions.NullStringList)
                                        )
                                    , AELHelper.Expressions.BooleanFalse
                                    , callAll
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTimeList,
                            RightDataType = DataType.DateTimeList,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<DateTime?> l = (List<DateTime?>)lhs;
                                List<DateTime?> r = (List<DateTime?>)rhs;

                                if (l == null || r == null)
                                    return false;

                                return l.All(li => r.Contains(li));
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAll = Expression.Parameter(typeof(DateTime?), "li");
                                var paramAny = Expression.Parameter(typeof(DateTime?), "ri");
                                var conditionAny = Expression.Equal(paramAll, paramAny);

                                var callAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(DateTime?)
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionAny
                                        , paramAny
                                        )
                                    );

                                var callAll = Expression.Call(
                                    typeof(Enumerable)
                                    , "All"
                                    , new Type[]
                                    {
                                        typeof(DateTime?)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callAny
                                        , paramAll
                                        )
                                    );

                                var exp = Expression.Condition(
                                    Expression.OrElse(
                                        Expression.Equal(left, AELHelper.Expressions.NullDateTimeList)
                                        , Expression.Equal(right, AELHelper.Expressions.NullDateTimeList)
                                        )
                                    , AELHelper.Expressions.BooleanFalse
                                    , callAll
                                    )
                                    ;

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectList,
                            RightDataType = DataType.AnyObjectList,
                            LinqFx = (lhs, rhs) =>
                            {
                                if (lhs == null || lhs == null)
                                    return false;

                                return LinqListInList(lhs, rhs);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Type leftAtomType = left.Type.GetGenericArguments()[0];
                                Type rightAtomType = right.Type.GetGenericArguments()[0];

                                var paramLeftAny = Expression.Parameter(leftAtomType, "li");
                                var paramRightAny = Expression.Parameter(rightAtomType, "ri");
                                var conditionAny = ExpressionAtomValueAreEqual(paramLeftAny, paramRightAny, forSQL);

                                var callRightAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        rightAtomType
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionAny
                                        , paramRightAny
                                        )
                                    );

                                var callLeftAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        leftAtomType
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callRightAny
                                        , paramLeftAny
                                        )
                                    );

                                if (forSQL)
                                    return AELHelper.Expressions.ConvertToBoolean(callLeftAny);

                                var exp = Expression.Condition(
                                    Expression.OrElse(
                                        Expression.Equal(left, Expression.Constant(null, left.Type))
                                        , Expression.Equal(right, Expression.Constant(null, right.Type))
                                        )
                                    , AELHelper.Expressions.BooleanFalse
                                    , callLeftAny
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.NOTIN,
                    DataType = DataType.Boolean,
                    Symbol = "is not in",
                    Text = "is not in",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.BooleanList,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = ((bool?)lhs);
                                List<bool?> r = (List<bool?>)rhs;

                                if (r == null)
                                    return true;

                                return !r.Contains(l);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(bool?)
                                        }
                                        , right
                                        , left
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.Number,
                            RightDataType = DataType.NumberList,
                            LinqFx = (lhs, rhs) =>
                            {
                                decimal? l = ((decimal?)lhs);
                                List<decimal?> r = (List<decimal?>)rhs;

                                if (r == null)
                                    return true;

                                return !r.Contains(l);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(decimal?)
                                        }
                                        , right
                                        , left
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.StringList,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                List<string> r = ((List<string>)rhs).ConvertAll(s => s?.ToUpper());

                                if (r == null)
                                    return true;

                                return !r.Any(ri =>
                                {
                                    if (string.IsNullOrEmpty(l) && string.IsNullOrEmpty(ri))
                                        return true;

                                    if (string.IsNullOrEmpty(l))
                                        return false;

                                    if (string.IsNullOrEmpty(ri))
                                        return false;

                                    return l == ri;
                                }
                                );
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                if (forSQL)
                                {
                                    Expression exp = Expression.Not(
                                        Expression.Call(
                                            typeof(Enumerable)
                                            , "Contains"
                                            , new Type[]
                                            {
                                                typeof(string)
                                            }
                                            , right
                                            , left
                                        )
                                    );

                                    return AELHelper.Expressions.ConvertToBoolean(exp);
                                }
                                else
                                {
                                    var paramAny = Expression.Parameter(typeof(string), "ri");
                                    var conditionAny = ExpressionStringAreEqual(left, paramAny, forSQL);

                                    var exp = Expression.Not(
                                        Expression.Call(
                                            typeof(Enumerable)
                                            , "Any"
                                            , new Type[]
                                            {
                                                typeof(string)
                                            }
                                            , right
                                            , Expression.Lambda(
                                                conditionAny
                                                , paramAny
                                                )
                                            )
                                        );

                                    return AELHelper.Expressions.ConvertToBoolean(exp);
                                }
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.DateTimeList,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = ((DateTime?)lhs);
                                List<DateTime?> r = (List<DateTime?>)rhs;

                                if (r == null)
                                    return true;

                                return !r.Contains(l);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(DateTime?)
                                        }
                                        , right
                                        , left
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectScalar,
                            RightDataType = DataType.AnyObjectList,
                            LinqFx = (lhs, rhs) =>
                            {
                                if (rhs == null)
                                    return false;

                                return !LinqItemInList(rhs, lhs);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Type rightAtomType = right.Type.GetGenericArguments()[0];

                                var paramAny = Expression.Parameter(rightAtomType, "ri");
                                var conditionAny = ExpressionAtomValueAreEqual(left, paramAny, forSQL);

                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Any"
                                        , new Type[]
                                        {
                                            rightAtomType
                                        }
                                        , right
                                        , Expression.Lambda(
                                            conditionAny
                                            , paramAny
                                            )
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.BooleanList,
                            RightDataType = DataType.BooleanList,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<bool?> l = (List<bool?>)lhs;
                                List<bool?> r = (List<bool?>)rhs;

                                if (l == null || r == null)
                                    return true;

                                return !l.All(li => r.Contains(li));
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAll = Expression.Parameter(typeof(bool?), "li");
                                var paramAny = Expression.Parameter(typeof(bool?), "ri");
                                var conditionAny = Expression.Equal(paramAll, paramAny);

                                var callAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(bool?)
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionAny
                                        , paramAny
                                        )
                                    );

                                var callAll = Expression.Call(
                                    typeof(Enumerable)
                                    , "All"
                                    , new Type[]
                                    {
                                        typeof(bool?)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callAny
                                        , paramAll
                                        )
                                    );

                                var exp = Expression.Not(
                                    Expression.Condition(
                                        Expression.OrElse(
                                            Expression.Equal(left, AELHelper.Expressions.NullBooleanList)
                                            , Expression.Equal(right, AELHelper.Expressions.NullBooleanList)
                                            )
                                        , AELHelper.Expressions.BooleanFalse
                                        , callAll
                                      )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.NumberList,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<decimal?> l = (List<decimal?>)lhs;
                                List<decimal?> r = (List<decimal?>)rhs;

                                if (l == null || r == null)
                                    return true;

                                return !l.All(li => r.Contains(li));
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAll = Expression.Parameter(typeof(decimal?), "li");

                                var callContains = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , right
                                    , paramAll
                                    );

                                var callAll = Expression.Not(Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callContains
                                        , paramAll
                                        )
                                    ));

                                return AELHelper.Expressions.ConvertToBoolean(callAll);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.StringList,
                            RightDataType = DataType.StringList,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<string> l = ((List<string>)lhs).ConvertAll(s => s?.ToUpper());
                                List<string> r = ((List<string>)rhs).ConvertAll(s => s?.ToUpper());

                                if (l == null || r == null)
                                    return true;

                                return !l.All( li =>
                                {
                                    return r.Any(ri =>
                                    {
                                        if (string.IsNullOrEmpty(li) && string.IsNullOrEmpty(ri))
                                            return true;

                                        if (string.IsNullOrEmpty(li))
                                            return false;

                                        if (string.IsNullOrEmpty(ri))
                                            return false;

                                        return li == ri;
                                    }
                                );
                                }
                                );
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAll = Expression.Parameter(typeof(string), "li");
                                //var paramAny = Expression.Parameter(typeof(string), "ri");
                                //var conditionAny = ExpressionStringAreEqual(paramAll, paramAny, forSQL);

                                //var callAny = Expression.Call(
                                //    typeof(Enumerable)
                                //    , "Any"
                                //    , new Type[]
                                //    {
                                //        typeof(string)
                                //    }
                                //    , right
                                //    , Expression.Lambda(
                                //        conditionAny
                                //        , paramAny
                                //        )
                                //    );

                                var callContains = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(string)
                                    }
                                    , right
                                    , paramAll
                                    );

                                var callAll = Expression.Not(Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(string)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callContains
                                        , paramAll
                                        )
                                    ));

                                if (forSQL)
                                    return AELHelper.Expressions.ConvertToBoolean(callAll);

                                var exp = Expression.Not(
                                    Expression.Condition(
                                        Expression.OrElse(
                                            Expression.Equal(left, AELHelper.Expressions.NullStringList)
                                            , Expression.Equal(right, AELHelper.Expressions.NullStringList)
                                            )
                                        , AELHelper.Expressions.BooleanFalse
                                        , callAll
                                      )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTimeList,
                            RightDataType = DataType.DateTimeList,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<DateTime?> l = (List<DateTime?>)lhs;
                                List<DateTime?> r = (List<DateTime?>)rhs;

                                if (l == null || r == null)
                                    return true;

                                return !l.All(li => r.Contains(li));
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAll = Expression.Parameter(typeof(DateTime?), "li");
                                var paramAny = Expression.Parameter(typeof(DateTime?), "ri");
                                var conditionAny = Expression.Equal(paramAll, paramAny);

                                var callAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(DateTime?)
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionAny
                                        , paramAny
                                        )
                                    );

                                var callAll = Expression.Call(
                                    typeof(Enumerable)
                                    , "All"
                                    , new Type[]
                                    {
                                        typeof(DateTime?)
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callAny
                                        , paramAll
                                        )
                                    );

                                var exp = Expression.Not(
                                    Expression.Condition(
                                        Expression.OrElse(
                                            Expression.Equal(left, AELHelper.Expressions.NullDateTimeList)
                                            , Expression.Equal(right, AELHelper.Expressions.NullDateTimeList)
                                            )
                                        , AELHelper.Expressions.BooleanFalse
                                        , callAll
                                      )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectList,
                            RightDataType = DataType.AnyObjectList,
                            LinqFx = (lhs, rhs) =>
                            {
                                if (lhs == null || lhs == null)
                                    return false;

                                return !LinqListInList(lhs, rhs);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Type leftAtomType = left.Type.GetGenericArguments()[0];
                                Type rightAtomType = right.Type.GetGenericArguments()[0];

                                var paramAll = Expression.Parameter(leftAtomType, "li");
                                var paramAny = Expression.Parameter(rightAtomType, "ri");
                                var conditionAny = ExpressionAtomValueAreEqual(paramAll, paramAny, forSQL);

                                var callAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        rightAtomType
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionAny
                                        , paramAny
                                        )
                                    );

                                var callAll = Expression.Call(
                                    typeof(Enumerable)
                                    , "All"
                                    , new Type[]
                                    {
                                        leftAtomType
                                    }
                                    , left
                                    , Expression.Lambda(
                                        callAny
                                        , paramAll
                                        )
                                    );

                                var exp = Expression.Condition(
                                            Expression.OrElse(
                                                Expression.Equal(left, Expression.Constant(null, left.Type))
                                                , Expression.Equal(right, Expression.Constant(null, right.Type))
                                                )
                                            , AELHelper.Expressions.BooleanFalse
                                        , Expression.Not( callAll )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.INCLUDES,
                    DataType = DataType.Boolean,
                    Symbol = "includes any",
                    Text = "includes any of",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.BooleanList,
                            RightDataType = DataType.Boolean,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<bool?> l = (List<bool?>)lhs;
                                bool? r = (bool?)rhs;

                                if (l == null)
                                    return false;

                                return l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(bool?)
                                    }
                                    , left
                                    , right
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<decimal?> l = (List<decimal?>)lhs;
                                decimal? r = (decimal?)rhs;

                                if (l == null)
                                    return false;

                                return l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(decimal?)
                                    }
                                    , left
                                    , right
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                          new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) || string.IsNullOrEmpty(r))
                                    return false;

                                return l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Expression rightExp = Expression.Call(right, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                Expression leftExp = Expression.Call(left, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                var exp = Expression.Condition(
                                    Expression.OrElse(ExpressionStringIsNullOrEmpty(left), ExpressionStringIsNullOrEmpty(right))
                                    , AELHelper.Expressions.BooleanFalse
                                    , Expression.Call(
                                        leftExp
                                        , "Contains"
                                        , null
                                        , rightExp
                                        //, Expression.Constant(StringComparison.OrdinalIgnoreCase, typeof(StringComparison))
                                      )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) || string.IsNullOrEmpty(r))
                                    return false;

                                return l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Expression rightExp = Expression.Call(right, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                Expression leftExp = Expression.Call(left, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                var exp = Expression.Condition(
                                    Expression.OrElse(ExpressionStringIsNullOrEmpty(left), ExpressionStringIsNullOrEmpty(right))
                                    , AELHelper.Expressions.BooleanFalse
                                    , Expression.Call(
                                        leftExp
                                        , "Contains"
                                        , null
                                        , rightExp
                                        //, Expression.Constant(StringComparison.OrdinalIgnoreCase, typeof(StringComparison))
                                      )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.StringList,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<string> l = ((List<string>)lhs).ConvertAll(s => s?.ToUpper());
                                string r = ((string)rhs)?.ToUpper();

                                if (l == null)
                                    return false;

                                return l.Any(li =>
                                {
                                    if (string.IsNullOrEmpty(r) && string.IsNullOrEmpty(li))
                                        return true;

                                    if (string.IsNullOrEmpty(r))
                                        return false;

                                    if (string.IsNullOrEmpty(li))
                                        return false;

                                    return li == r;
                                }
                                );
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                if (forSQL)
                                {
                                    Expression exp = Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(string)
                                        }
                                        , left
                                        , right
                                        );

                                    return AELHelper.Expressions.ConvertToBoolean(exp);
                                }

                                else
                                {
                                    var param = Expression.Parameter(typeof(string), "li");
                                    var conditionAny = ExpressionStringAreEqual(right, param, forSQL);

                                    Expression exp = Expression.Call(
                                        typeof(Enumerable)
                                        , "Any"
                                        , new Type[]
                                        {
                                            typeof(string)
                                        }
                                        , left
                                        , Expression.Lambda(
                                            conditionAny
                                            , param
                                            )
                                        );

                                    return AELHelper.Expressions.ConvertToBoolean(exp);
                                }
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTimeList,
                            RightDataType = DataType.DateTime,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<DateTime?> l = (List<DateTime?>)lhs;
                                DateTime? r = ((DateTime?)rhs);

                                if (l == null)
                                    return false;

                                return l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Contains"
                                    , new Type[]
                                    {
                                        typeof(DateTime?)
                                    }
                                    , left
                                    , right
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectList,
                            RightDataType = DataType.AnyObjectScalar,
                            LinqFx = (lhs, rhs) =>
                            {
                                if (lhs == null)
                                    return false;

                                return LinqItemInList(lhs, rhs);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Type leftAtomType = left.Type.GetGenericArguments()[0];

                                var paramAny = Expression.Parameter(leftAtomType, "li");
                                var conditionAny = ExpressionAtomValueAreEqual(right, paramAny, forSQL);

                                var exp = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        leftAtomType
                                    }
                                    , left
                                    , Expression.Lambda(
                                        conditionAny
                                        , paramAny
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.NOTINCLUDES,
                    DataType = DataType.Boolean,
                    Symbol = "includes none",
                    Text = "includes none of",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.BooleanList,
                            RightDataType = DataType.Boolean,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<bool?> l = (List<bool?>)lhs;
                                bool? r = (bool?)rhs;

                                if (l == null)
                                    return true;

                                return !l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(bool?)
                                        }
                                        , left
                                        , right
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.NumberList,
                            RightDataType = DataType.Number,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<decimal?> l = (List<decimal?>)lhs;
                                decimal? r = (decimal?)rhs;

                                if (l == null)
                                    return true;

                                return !l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(decimal?)
                                        }
                                        , left
                                        , right
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) || string.IsNullOrEmpty(r))
                                    return false;

                                return l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Expression rightExp = Expression.Call(right, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                Expression leftExp = Expression.Call(left, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                var exp =  //Expression.Condition(
                                    //Expression.OrElse(ExpressionStringIsNullOrEmpty(left), ExpressionStringIsNullOrEmpty(right))
                                    //, AELHelper.Expressions.BooleanFalse
                                    //,
                                    Expression.Not(Expression.Call(
                                        leftExp
                                        , "Contains"
                                        , null
                                        , rightExp
                                        //, Expression.Constant(StringComparison.OrdinalIgnoreCase, typeof(StringComparison))
                                      )
                                    );
                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.StringList,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<string> l = ((List<string>)lhs).ConvertAll(s => s?.ToUpper());
                                string r = ((string)rhs)?.ToUpper();

                                if (l == null)
                                    return false;

                                return !l.Any(li =>
                                {
                                    if (string.IsNullOrEmpty(r) && string.IsNullOrEmpty(li))
                                        return true;

                                    if (string.IsNullOrEmpty(r))
                                        return false;

                                    if (string.IsNullOrEmpty(li))
                                        return false;

                                    return li == r;
                                }
                                );
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var paramAny = Expression.Parameter(typeof(string), "li");
                                var conditionAny = ExpressionStringAreEqual(right, paramAny, forSQL);

                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Any"
                                        , new Type[]
                                        {
                                            typeof(string)
                                        }
                                        , left
                                        , Expression.Lambda(
                                            conditionAny
                                            , paramAny
                                            )
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.DateTimeList,
                            RightDataType = DataType.DateTime,
                            LinqFx = (lhs, rhs) =>
                            {
                                List<DateTime?> l = (List<DateTime?>)lhs;
                                DateTime? r = ((DateTime?)rhs);

                                if (l == null)
                                    return true;

                                return !l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Contains"
                                        , new Type[]
                                        {
                                            typeof(DateTime?)
                                        }
                                        , left
                                        , right
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.AnyObjectList,
                            RightDataType = DataType.AnyObjectScalar,
                            LinqFx = (lhs, rhs) =>
                            {
                                if (lhs == null)
                                    return false;

                                return !LinqItemInList(lhs, rhs);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Type leftAtomType = left.Type.GetGenericArguments()[0];

                                var paramAny = Expression.Parameter(leftAtomType, "li");
                                var conditionAny = ExpressionAtomValueAreEqual(right, paramAny, forSQL);

                                var exp = Expression.Not(
                                    Expression.Call(
                                        typeof(Enumerable)
                                        , "Any"
                                        , new Type[]
                                        {
                                            leftAtomType
                                        }
                                        , left
                                        , Expression.Lambda(
                                            conditionAny
                                            , paramAny
                                            )
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.CONTAINSTEXT,
                    DataType = DataType.Boolean,
                    Symbol = "contains any",
                    Text = "contains any of",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) || string.IsNullOrEmpty(r))
                                    return false;

                                return l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Expression rightExp = Expression.Call(right, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                Expression leftExp = Expression.Call(left, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                var exp = Expression.Condition(
                                    Expression.OrElse(ExpressionStringIsNullOrEmpty(left), ExpressionStringIsNullOrEmpty(right))
                                    , AELHelper.Expressions.BooleanFalse
                                    , Expression.Call(
                                        leftExp
                                        , "Contains"
                                        , null
                                        , rightExp
                                        //, Expression.Constant(StringComparison.OrdinalIgnoreCase, typeof(StringComparison))
                                      )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.StringList,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                List<string> r = ((List<string>)rhs).ConvertAll(s => s?.ToUpper());

                                if (r == null)
                                    return false;

                                if (string.IsNullOrEmpty(l))
                                    return false;

                                return r.Any(ri =>
                                    {
                                        if (string.IsNullOrEmpty(ri))
                                            return false;

                                        return l.Contains(ri);
                                    }
                                );
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var param = Expression.Parameter(typeof(string), "ri");

                                Expression paramExp = Expression.Call(param, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                Expression leftExp = Expression.Call(left, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));


                                var conditionContains = Expression.Condition(
                                    ExpressionStringIsNullOrEmpty(param)
                                    , AELHelper.Expressions.BooleanFalse
                                    ,  Expression.Call(
                                            leftExp
                                            , "Contains"
                                            , null
                                            , paramExp
                                            //, Expression.Constant(StringComparison.OrdinalIgnoreCase, typeof(StringComparison))
                                          )
                                    );


                                var callAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(string)
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionContains
                                        , param
                                        )
                                    );

                                var exp = Expression.Condition(
                                    ExpressionStringIsNullOrEmpty(left)
                                    , AELHelper.Expressions.BooleanFalse
                                    , callAny
                                );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.NOTCONTAINSTEXT,
                    DataType = DataType.Boolean,
                    Symbol = "contains none",
                    Text = "contains none of",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.String,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                string r = ((string)rhs)?.ToUpper();

                                if (string.IsNullOrEmpty(l) || string.IsNullOrEmpty(r))
                                    return true;

                                return !l.Contains(r);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                Expression rightExp = Expression.Call(right, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                Expression leftExp = Expression.Call(left, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));

                                var exp = Expression.Not(
                                    Expression.Condition(
                                        Expression.OrElse(ExpressionStringIsNullOrEmpty(left), ExpressionStringIsNullOrEmpty(right))
                                        , AELHelper.Expressions.BooleanFalse
                                        , Expression.Call(
                                            leftExp
                                            , "Contains"
                                            , null
                                            , rightExp
                                            //, Expression.Constant(StringComparison.OrdinalIgnoreCase, typeof(StringComparison))
                                          )
                                    )
                                );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                        new OperandType
                        {
                            LeftDataType = DataType.String,
                            RightDataType = DataType.StringList,
                            LinqFx = (lhs, rhs) =>
                            {
                                string l = ((string)lhs)?.ToUpper();
                                List<string> r = ((List<string>)rhs).ConvertAll(s => s?.ToUpper());

                                if (r == null)
                                    return true;

                                if (string.IsNullOrEmpty(l))
                                    return true;

                                return !r.Any(ri =>
                                    {
                                        if (string.IsNullOrEmpty(ri))
                                            return false;

                                        return l.Contains(ri);
                                    }
                                );
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var param = Expression.Parameter(typeof(string), "ri");

                                Expression paramExp = Expression.Call(param, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
                                Expression leftExp = Expression.Call(left, typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));

                                var conditionContains = Expression.Condition(
                                    ExpressionStringIsNullOrEmpty(param)
                                    , AELHelper.Expressions.BooleanFalse
                                    ,  Expression.Call(
                                            leftExp
                                            , "Contains"
                                            , null
                                            , paramExp
                                            //, Expression.Constant(StringComparison.OrdinalIgnoreCase, typeof(StringComparison))
                                          )
                                    );


                                var callAny = Expression.Call(
                                    typeof(Enumerable)
                                    , "Any"
                                    , new Type[]
                                    {
                                        typeof(string)
                                    }
                                    , right
                                    , Expression.Lambda(
                                        conditionContains
                                        , param
                                        )
                                    );

                                var exp = Expression.Not(
                                    Expression.Condition(
                                        ExpressionStringIsNullOrEmpty(left)
                                        , AELHelper.Expressions.BooleanFalse
                                        , callAny
                                    )
                                );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.NOT,
                    DataType = DataType.Boolean,
                    Symbol = "not",
                    Text = "not",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = (bool?)lhs;

                                if (!l.HasValue)
                                    return true;

                                return !l.Value;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                if (forSQL)
                                    return AELHelper.Expressions.ConvertToBoolean(Expression.Not(left));

                                var leftValue = Expression.Property(left, "Value");

                                var exp = Expression.Condition(
                                    Expression.Equal(left, AELHelper.Expressions.NullBoolean)
                                    , AELHelper.Expressions.BooleanTrue
                                    , Expression.Not(leftValue)
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.ISTRUE,
                    DataType = DataType.Boolean,
                    Symbol = "is true",
                    Text = "is true",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = (bool?)lhs;

                                if (!l.HasValue)
                                    return false;

                                return l.Value == true;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                if (forSQL)
                                    return AELHelper.Expressions.ConvertToBoolean(Expression.Equal(left, AELHelper.Expressions.BooleanTrue));

                                var leftValue = Expression.Property(left, "Value");

                                var exp = Expression.Condition(
                                    Expression.Equal(left, AELHelper.Expressions.NullBoolean)
                                    , AELHelper.Expressions.BooleanFalse
                                    , leftValue
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.ISFALSE,
                    DataType = DataType.Boolean,
                    Symbol = "is false",
                    Text = "is false",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.Boolean,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) =>
                            {
                                bool? l = (bool?)lhs;

                                if (!l.HasValue)
                                    return true;

                                return l.Value == false;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                if (forSQL)
                                    return AELHelper.Expressions.ConvertToBoolean(Expression.Equal(left, AELHelper.Expressions.BooleanFalse));

                                var leftValue = Expression.Property(left, "Value");

                                var exp = Expression.Condition(
                                    Expression.Equal(left, AELHelper.Expressions.NullBoolean)
                                    , AELHelper.Expressions.BooleanTrue
                                    , Expression.Not(leftValue)
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.ISFUTURE,
                    DataType = DataType.Boolean,
                    Symbol = "is in future",
                    Text = "is in future",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;

                                if (!l.HasValue)
                                    return false;

                                return l.Value > DateTime.UtcNow;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Condition(
                                    Expression.Equal(left, AELHelper.Expressions.NullDateTime)
                                    , AELHelper.Expressions.BooleanFalse
                                    , Expression.GreaterThan(
                                        Expression.Property(left, "Value")
                                        , Expression.Constant(DateTime.UtcNow, typeof(DateTime))
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.INPAST,
                    DataType = DataType.Boolean,
                    Symbol = "is in past",
                    Text = "is in past",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;

                                if (!l.HasValue)
                                    return false;

                                return l.Value < DateTime.UtcNow;
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Condition(
                                    Expression.Equal(left, AELHelper.Expressions.NullDateTime)
                                    , AELHelper.Expressions.BooleanFalse
                                    , Expression.LessThan(
                                        Expression.Property(left, "Value")
                                        , Expression.Constant(DateTime.UtcNow, typeof(DateTime))
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
                new OperatorInfo {
                    OperatorType = OperatorType.ISTODAY,
                    DataType = DataType.Boolean,
                    Symbol = "is today",
                    Text = "is today",
                    OperandTypes = new List<OperandType>
                    {
                        new OperandType
                        {
                            LeftDataType = DataType.DateTime,
                            RightDataType = DataType.None,
                            LinqFx = (lhs, rhs) =>
                            {
                                DateTime? l = (DateTime?)lhs;

                                if (!l.HasValue)
                                    return false;

                                return l.Value >= DateTime.UtcNow.Date && l.Value < DateTime.UtcNow.Date.AddDays(1);
                            },
                            Expression = (left, right, forSQL) =>
                            {
                                var exp = Expression.Condition(
                                    Expression.Equal(left, AELHelper.Expressions.NullDateTime)
                                    , AELHelper.Expressions.BooleanFalse
                                    , Expression.AndAlso(
                                          Expression.GreaterThanOrEqual(
                                            Expression.Property(left, "Value")
                                            , Expression.Constant(DateTime.UtcNow.Date, typeof(DateTime)
                                              )
                                            )
                                        , Expression.LessThan(
                                            Expression.Property(left, "Value")
                                            , Expression.Constant(DateTime.UtcNow.Date.AddDays(1), typeof(DateTime)
                                              )
                                            )
                                        )
                                    );

                                return AELHelper.Expressions.ConvertToBoolean(exp);
                            }
                        },
                    },
                },
            }.ForEach(op => this.Add(op.OperatorType, op));
        }
    }
}

