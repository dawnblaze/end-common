﻿using System.Collections.Generic;
using System.Linq;

namespace Endor.AEL
{
    public static class OperatorExtensions
    {
        public static OperatorInfo MetaDataInfo(this OperatorType ot) => OperatorDictionary.Instance[ot];
    }
}
