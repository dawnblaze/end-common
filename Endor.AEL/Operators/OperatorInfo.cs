﻿using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL
{
    public struct SimpleOperandType
    {
        public DataType loperand { get; set; }
        public DataType roperand { get; set; }
    }

    public class OperandType
    {
        public DataType LeftDataType { get; set; }
        public DataType RightDataType { get; set; }
        public Func<dynamic, dynamic, bool> LinqFx { get; set; }
        public Func<Expression, Expression, bool, Expression> Expression { get; set; }
    }

    public class OperatorInfo
    {
        /// <summary>
        /// The operator.
        /// </summary>
        public OperatorType OperatorType { get; set; }

        /// <summary>
        /// The resulting DataType once the operator is executed
        /// </summary>
        public DataType DataType { get; set; }

        /// <summary>
        /// The symbol to use for the operator
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// The friendly text to display for the operator
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// An alternate ASCII code that can be used to render the symbol.
        /// This is helpful for NEQ, GTE, LTE and a few others
        /// </summary>
        public char ASCIICode { get; set; }

        /// <summary>
        /// The OperandTypes track the valid left and right operands for this operator, 
        /// and the LinqFormula used to process the operation
        /// 
        /// The Expression is the query expression that takes the 
        /// left and right hand operators and returns the result.
        /// 
        /// The definition of the method is linq expression is:
        ///   Func<Expression, Expression, Expression>
        /// where the parameters are:
        ///   * 1 = the left operand, which may be a scalar or a list
        ///   * 2 = the right operand, which may be null, a scalar, or a list
        ///   * 3 = the resulting value, which may be a scalar or list value.
        ///   
        /// Example - the EQ operator for DataTpe Numeric (scalar)
        ///     (lhs, rhs, res) => ( (decimal)lhs == (decimal)rhs )\
        ///     
        /// Example - the IN operator for Employee and EmployeeList.
        ///     (lhs, rhs, res) => ( (List<AtomObj>)rhs.Exists(r => r.ID == (AtomObj)lhs.ID )
        ///     
        /// </summary>
        [JsonIgnore]
        public List<OperandType> OperandTypes;

        /// <summary>
        /// This is used by the client for mapping left and right Operand Types since it cannot use the functions directly.
        /// </summary>
        public List<SimpleOperandType> SimpleOperandTypes
        {
            get => this.OperandTypes
                    ?.Select(op => new SimpleOperandType() { loperand = op.LeftDataType, roperand = op.RightDataType })
                    ?.ToList() ?? new List<SimpleOperandType>();
        }
        
        private DataType VerifyDataType(DataType dataType)
        {
            switch (dataType)
            {
                case DataType.OrderStatus:
                case DataType.TimeZone:
                case DataType.ItemStatus:
                case DataType.Substatus:
                case DataType.Company:
                case DataType.Location:
                case DataType.Employee:
                case DataType.Contact:
                    return DataType.Number;
                case DataType.OrderStatusList:
                case DataType.ItemStatusList:
                case DataType.SubstatusList:
                case DataType.CompanyList:
                case DataType.LocationList:
                case DataType.EmployeeList:
                case DataType.ContactList:
                    return DataType.NumberList;
                //case DataType.Contact:
                case DataType.ContactCustomField:
                case DataType.Destination:
                case DataType.DestinationStatus:
                //case DataType.Employee:
                case DataType.EmployeeCustomField:
                case DataType.EmployeeRole:
                case DataType.Estimate:
                case DataType.FlatList:
                case DataType.Industry:
                //case DataType.ItemStatus:
                case DataType.LaborPart:
                case DataType.LineItem:
                //case DataType.Location:
                case DataType.MachinePart:
                case DataType.MaterialPart:
                case DataType.Opportunity:
                case DataType.Order:
                case DataType.OrderContactRole:
                case DataType.OrderCustomField:
                case DataType.OrderEmployeeRole:
                case DataType.OrderNotes:
                case DataType.Origin:
                case DataType.PaymentTerm:
                case DataType.PurchaseOrder:
                //case DataType.Substatus:
                case DataType.TaxGroup:
                case DataType.Team:
                case DataType.CompanyContactListCategory:
                case DataType.CompanyDatesCategory:
                case DataType.CompanyEmployeeListCategory:
                case DataType.CompanyEmployeeRoleListCategory:
                case DataType.CompanyCustomField:
                case DataType.CompanyCategory:
                case DataType.SubsidiariesCategory:
                case DataType.CompanyTagsCategory:
                case DataType.CompanyTaxCategory:
                case DataType.CompanyTypeCategory:
                case DataType.ContactCategory:
                case DataType.CostCategory:
                case DataType.CustomerCreditCategory:
                case DataType.EstimateDateCategory:
                case DataType.EstimateCategory:
                case DataType.LocationCategory:
                case DataType.TransactionHeaderNotesCategory:
                case DataType.LineItemNotesCategory:
                case DataType.OrderDateCategory:
                case DataType.OrderItemComponentCategory:
                case DataType.OrderItemContactRoleCategory:
                case DataType.OrderItemCostsCategory:
                case DataType.OrderItemEmployeeRoleListCategory:
                case DataType.OrderItemEmployeeRoleList:
                case DataType.OrderItemLocationCategory:
                case DataType.LineItemCategory:
                case DataType.OrderItemPricesCategory:
                case DataType.LineItemStatusCategory:
                case DataType.OrderItemTagsCategory:
                case DataType.OrderItemTaxesCategory:
                case DataType.OrderCategory:
                case DataType.PaymentCategory:
                case DataType.PONumberCategory:
                case DataType.PricesCategory:
                case DataType.TaxesCategory:
                case DataType.TransactionHeaderContactRoleCategory:
                case DataType.TransactionHeaderEmployeeRoleListCategory:
                case DataType.TransactionHeaderEmployeeRoleList:
                case DataType.TransactionHeaderTagsCategory:
                    return DataType.AnyObjectScalar;
                //case DataType.ContactList:
                case DataType.ContactCustomFieldList:
                case DataType.DestinationList:
                case DataType.DestinationStatusList:
                //case DataType.EmployeeList:
                case DataType.EmployeeCustomFieldList:
                case DataType.EmployeeRoleList:
                case DataType.EstimateList:
                case DataType.FlatListList:
                case DataType.IndustryList:
                //case DataType.ItemStatusList:
                case DataType.LaborPartList:
                case DataType.LineItemList:
                //case DataType.LocationList:
                case DataType.MachinePartList:
                case DataType.MaterialPartList:
                case DataType.OpportunityList:
                case DataType.OrderList:
                case DataType.OrderContactRoleList:
                case DataType.OrderCustomFieldList:
                case DataType.OrderEmployeeRoleList:
                case DataType.OrderNotesList:
                case DataType.OriginList:
                case DataType.PurchaseOrderList:
                //case DataType.SubstatusList:
                case DataType.TaxGroupList:
                case DataType.TeamList:
                case DataType.EmployeeCategory:
                    return DataType.AnyObjectList;
                default:
                    return dataType;
            }
        }

        public OperandType FindOperandType(DataType leftDataType, DataType rightDataType, DataType memberBaseDataType)
        {
            if (memberBaseDataType == DataType.EmployeeCategory && leftDataType == DataType.StringList)
            {
                leftDataType = DataType.String;
            }
            else
            {
                leftDataType = VerifyDataType(leftDataType);
            }
            //leftDataType = VerifyDataType(leftDataType);
            rightDataType = VerifyDataType(rightDataType);

            return OperandTypes
                        .Where(x => x.LeftDataType == leftDataType && x.RightDataType == rightDataType)
                        .FirstOrDefault();
        }
    }
}
