﻿using System.Globalization;

namespace Endor.AEL
{
    public class MergeLookup : IMergeLookup
    {
        public string MergeField { get; set; }

        public bool IsValid { get; set; }

        public bool IsNull { get; set; }

        public dynamic Result { get; set; }

        public NumberType NumberType { get; set; }

        public CultureInfo CultureInfo { get; set; }

        public override string ToString()
        {
            if (Result == null)
                return null;

            if (this.NumberType != NumberType.Unknown && Result is decimal?)
            {
                decimal d = ((decimal?)Result).Value;

                switch (this.NumberType)
                {
                    case NumberType.Integer:
                        return decimal.ToInt32(d).ToString();
                    case NumberType.Decimal:
                        return d.ToString("0.####");
                    case NumberType.Currency:
                        return d.ToString("C", CultureInfo);
                }
            }

            return Result.ToString();
        }
    }
}
