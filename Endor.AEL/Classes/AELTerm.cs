﻿using Endor.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace Endor.AEL
{
    /// <summary>
    /// AEL Term Object
    /// </summary>
    public abstract class AELTerm
    {
        /// <summary>
        /// Data Type for the Term
        /// </summary>
        public DataType DataType { get; set; }
    }

    /// <summary>
    /// AEL Operation Object
    /// </summary>
    public class AELOperation : AELTerm
    {
        /// <summary>
        /// Json Serialization Binder class
        /// </summary>
        public class AELSerializationBinder : ISerializationBinder
        {
            public void BindToName(Type serializedType, out string assemblyName, out string typeName)
            {
                assemblyName = null;
                typeName = serializedType.Name;
            }

            public Type BindToType(string assemblyName, string typeName)
            {
                Assembly assem = typeof(AELTerm).Assembly;
                var result = assem.GetType($"Endor.AEL.{typeName}");

                return result;
            }
        }

        /// <summary>
        /// Operator Type
        /// </summary>
        public OperatorType Operator { get; set; }

        /// <summary>
        /// Left-hand side data type
        /// </summary>
        public DataType LhsDataType { get; set; }

        /// <summary>
        /// Right-hand side data type
        /// </summary>
        public DataType RhsDataType { get; set; }

        /// <summary>
        /// Collection of nested Terms
        /// </summary>
        public List<AELTerm> Terms { get; set; }

        public static readonly JsonSerializerSettings jsonSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            TypeNameHandling = TypeNameHandling.Objects,
            SerializationBinder = new AELSerializationBinder(),
        };

        public static AELOperation Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<AELOperation>(json, jsonSettings);
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this, jsonSettings);
        }
    }

    /// <summary>
    /// AEL Member Object
    /// </summary>
    public class AELMember : AELTerm
    {
        /// <summary>
        /// ID
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// Text
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Related ID
        /// </summary>
        public int? RelatedID { get; set; }

        /// <summary>
        /// Collection of parameters
        /// </summary>
        public List<AELTerm> Parameters { get; set; }

        /// <summary>
        /// Member Base
        /// </summary>
        public AELMember MemberBase { get; set; }
    }

    /// <summary>
    /// AEL Values Object
    /// </summary>
    public class AELValues : AELTerm
    {
        /// <summary>
        /// Values
        /// </summary>
        public List<object> Values { get; set; }

        /// <summary>
        /// Display Text
        /// </summary>
        public string DisplayText { get; set; }

        public Expression GetExpression(short bid)
        {
            if (Values.Count == 0)
                return null;

            object v1 = Values[0];

            switch (DataType)
            {
                case DataType.None:
                    return null;
                case DataType.String:
                    {
                        string s = v1.ToString();
                        return Expression.Constant(s, typeof(string));
                    }
                case DataType.StringList:
                    {
                        List<string> sl = Values.ConvertAll(new Converter<object, string>(v => v.ToString()));
                        return Expression.Constant(sl, typeof(List<string>));
                    }

                /*Enums*/
                case DataType.OrderStatus:
                case DataType.TimeZone:
                case DataType.Number:
                case DataType.ItemStatus:
                case DataType.Substatus:
                case DataType.Company:
                case DataType.Location:
                case DataType.Employee:
                case DataType.Contact:
                    {
                        decimal? d;
                        if (v1 == null)
                            d = null;
                        else
                            d = Convert.ToDecimal(v1);

                        return Expression.Constant(d, typeof(decimal?));
                    }
                case DataType.OrderStatusList:
                case DataType.NumberList:
                case DataType.ItemStatusList:
                case DataType.SubstatusList:
                case DataType.CompanyList:
                case DataType.LocationList:
                case DataType.EmployeeList:
                case DataType.ContactList:
                    {
                        List<decimal?> dl = Values.ConvertAll(new Converter<object, decimal?>(v =>
                        {
                            decimal? d;
                            if (v == null)
                                d = null;
                            else
                                d = Convert.ToDecimal(v);

                            return d;
                        }));

                        return Expression.Constant(dl, typeof(List<decimal?>));
                    }

                case DataType.DateTime:
                    {
                        DateTime? dt;
                        if (v1 == null)
                            dt = null;
                        else
                            dt = Convert.ToDateTime(v1);

                        return Expression.Constant(dt, typeof(DateTime?));
                    }
                case DataType.DateTimeList:
                    {
                        List<DateTime?> dtl = Values.ConvertAll(new Converter<object, DateTime?>(v =>
                        {
                            DateTime? dt;
                            if (v == null)
                                dt = null;
                            else
                                dt = Convert.ToDateTime(v);

                            return dt;
                        }));

                        return Expression.Constant(dtl, typeof(List<DateTime?>));
                    }

                case DataType.Boolean:
                    {
                        bool? b;
                        if (v1 == null)
                            b = null;
                        else
                            b = Convert.ToBoolean(v1);

                        return Expression.Constant(b, typeof(bool?));
                    }
                case DataType.BooleanList:
                    {
                        List<bool?> bl = Values.ConvertAll(new Converter<object, bool?>(v =>
                        {
                            bool? b;
                            if (v == null)
                                b = null;
                            else
                                b = Convert.ToBoolean(v);

                            return b;
                        }));

                        return Expression.Constant(bl, typeof(List<bool?>));
                    }

                //case DataType.Contact:
                case DataType.CompanyCustomField:
                case DataType.ContactCustomField:
                case DataType.Destination:
                case DataType.Estimate:
                case DataType.Order:
                case DataType.LaborPart:
                case DataType.MachinePart:
                case DataType.MaterialPart:
                case DataType.LineItem:
                case DataType.Opportunity:
                case DataType.OrderContactRole:
                case DataType.OrderCustomField:
                case DataType.OrderEmployeeRole:
                case DataType.OrderNotes:
                case DataType.PaymentTerm:
                case DataType.PurchaseOrder:
                case DataType.Team:
                    {
                        if (v1 == null)
                            return Expression.Constant(null, typeof(AtomValue<int>));

                        AtomValue<int> av = new AtomValue<int>
                        {
                            BID = bid,
                            ID = Convert.ToInt16(v1),
                            DataType = DataType,
                        };

                        return Expression.Constant(av, typeof(AtomValue<int>));
                    }

                //case DataType.Employee:
                case DataType.EmployeeCustomField:
                case DataType.EmployeeRole:
                case DataType.FlatList:
                case DataType.Industry:
                //case DataType.ItemStatus:
                case DataType.Origin:
                //case DataType.Substatus:
                case DataType.TaxGroup:
                    {
                        if (v1 == null)
                            return Expression.Constant(null, typeof(AtomValue<short>));

                        AtomValue<short> av = new AtomValue<short>
                        {
                            BID = bid,
                            ID = Convert.ToInt16(v1),
                            DataType = DataType,
                        };

                        return Expression.Constant(av, typeof(AtomValue<short>));
                    }

                case DataType.DestinationStatus:
                    {
                        if (v1 == null)
                            return Expression.Constant(null, typeof(AtomValue<byte>));

                        AtomValue<byte> av = new AtomValue<byte>
                        {
                            BID = bid,
                            ID = Convert.ToByte(v1),
                            DataType = DataType,
                        };

                        return Expression.Constant(av, typeof(AtomValue<byte>));
                    }

                //case DataType.ContactList:
                case DataType.ContactCustomFieldList:
                case DataType.DestinationList:
                case DataType.EstimateList:
                case DataType.OrderList:
                case DataType.LaborPartList:
                case DataType.MachinePartList:
                case DataType.MaterialPartList:
                case DataType.LineItemList:
                case DataType.OpportunityList:
                case DataType.OrderContactRoleList:
                case DataType.OrderCustomFieldList:
                case DataType.OrderEmployeeRoleList:
                case DataType.OrderNotesList:
                case DataType.PurchaseOrderList:
                case DataType.TeamList:
                    {
                        List<AtomValue<int>> avl = Values.ConvertAll(new Converter<object, AtomValue<int>>(v =>
                        {
                            if (v == null)
                                return null;

                            AtomValue<int> av = new AtomValue<int>
                            {
                                BID = bid,
                                ID = Convert.ToInt16(v),
                                DataType = (DataType)(-(short)DataType),
                            };

                            return av;
                        }));

                        return Expression.Constant(avl, typeof(List<AtomValue<int>>));
                    }

                //case DataType.EmployeeList:
                case DataType.EmployeeCustomFieldList:
                case DataType.EmployeeRoleList:
                case DataType.FlatListList:
                case DataType.IndustryList:
                //case DataType.ItemStatusList:
                case DataType.OriginList:
                //case DataType.SubstatusList:
                case DataType.TaxGroupList:
                    {
                        List<AtomValue<short>> avl = Values.ConvertAll(new Converter<object, AtomValue<short>>(v =>
                        {
                            if (v == null)
                                return null;

                            AtomValue<short> av = new AtomValue<short>
                            {
                                BID = bid,
                                ID = Convert.ToInt16(v),
                                DataType = (DataType)(-(short)DataType),
                            };

                            return av;
                        }));

                        return Expression.Constant(avl, typeof(List<AtomValue<short>>));
                    }

                case DataType.DestinationStatusList:
                    {
                        List<AtomValue<byte>> avl = Values.ConvertAll(new Converter<object, AtomValue<byte>>(v =>
                        {
                            if (v == null)
                                return null;

                            AtomValue<byte> av = new AtomValue<byte>
                            {
                                BID = bid,
                                ID = Convert.ToByte(v),
                                DataType = (DataType)(-(short)DataType),
                            };

                            return av;
                        }));

                        return Expression.Constant(avl, typeof(List<AtomValue<byte>>));
                    }
            }

            throw new NotImplementedException("Value conversion not implemented.");
        }
    }
}