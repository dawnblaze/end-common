﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Endor.AEL
{
    public class MergeFieldHelper
    {
        private readonly IDataProvider dataProvider;
        private readonly int? classTypeID;
        private readonly int? ID;
        private readonly int? userID;

        /// <summary>
        /// Constructs a MergeFieldHelper for a root object
        /// </summary>
        /// <param name="DataProvider">The AEL Data Provider user to retrieve the BID and data context</param>
        /// <param name="ClassTypeID">The ClassTypeID fo the Root Object.  If null, only User Merge Fields are available.</param>
        /// <param name="ID">The ID of the Root Object.  If null, only User Merge Fields are available.</param>
        /// <param name="UserID">The ID of the User running the merge. Must be supplied if ID or ClassTypeID is not.</param>
        public MergeFieldHelper(IDataProvider DataProvider, int? ClassTypeID, int? ID, int? UserID = null) 
        {
            this.dataProvider = DataProvider;
            this.classTypeID = ClassTypeID;
            this.ID = ID;
            this.userID = UserID;

            Task.FromResult(CheckRootValidity());
        }

        /// <summary>
        /// This procedure uses AEL to look up the value of a single MergeField.
        /// If the MergeField can not be found, the IsValid is set to false.
        /// Errors in the lookup are trapped and not returned.  
        /// </summary>
        /// <param name="mergeField"></param>
        /// <returns>An IMergeField containing the result</returns>
        public IMergeLookup Lookup(string mergeField)
        {
            List<string> mergeFields = new List<string> { mergeField };

            IEnumerable<IMergeLookup> result = Lookup(mergeFields);

            if (result.Any())
                return result.First();

            return new MergeLookup { MergeField = mergeField, IsValid = false, IsNull = true, Result = null };
        }


        private string ReadLocalizationLanguage()
        {
            string resultPart1 = null;
            string resultPart2 = null;

            DbConnection conn = dataProvider.Connection;
                conn.Open();

            try
            {
                DbCommand command = conn.CreateCommand();
                command.CommandText = "[Option.GetValue]";
                command.CommandType = CommandType.StoredProcedure;

                DbParameter dp;

                dp = command.CreateParameter();
                dp.ParameterName = "BID";
                dp.Value = dataProvider.BID;
                command.Parameters.Add(dp);

                dp = command.CreateParameter();
                dp.ParameterName = "OptionName";
                command.Parameters.Add(dp);

                command.Parameters["OptionName"].Value = "Localization.Language";

                using (DbDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        resultPart1 = dr.GetString(1);
                    }
                }

                command.Parameters["OptionName"].Value = "Localization.DefaultCountry";

                using (DbDataReader dr = command.ExecuteReader())
                {
                    if (dr.Read())
                    {
                        resultPart2 = dr.GetString(1);
                    }
                }
            }
            finally
            {
                conn.Close();
            }

            if (string.IsNullOrWhiteSpace(resultPart1))
                return resultPart2;

            if (string.IsNullOrWhiteSpace(resultPart2))
                return resultPart1;

            return $"{resultPart1}-{resultPart2}";
        }

        private CultureInfo _CultureInfo;
        private CultureInfo CultureInfo
        {
            get
            {
                if (_CultureInfo == null)
                {
                    string langCode = ReadLocalizationLanguage();

                    CultureInfo cultureInfo = new CultureInfo(langCode);

                    string currencySymbol = cultureInfo?.NumberFormat?.CurrencySymbol;
                    if (string.IsNullOrWhiteSpace(currencySymbol) || currencySymbol == "\u00A4")
                        cultureInfo = new CultureInfo("en-US");

                    CultureInfo.CurrentCulture = cultureInfo;

                    _CultureInfo = cultureInfo;
                }

                return _CultureInfo;
            }
        }

        /// <summary>
        /// This procedure takes an IEnumerable of MergeFields and returns an IEnumerable or Results.
        /// If the MergeField can not be found, the IsValid is set to false.
        /// Errors in the lookup are trapped and not returned.  
        /// </summary>
        /// <param name="mergeField"></param>
        /// <returns></returns>
        public IEnumerable<IMergeLookup> Lookup(IEnumerable<string> mergeFields)
        {
            List<IMergeLookup> lookups = new List<IMergeLookup>();

            IEnumerable<string> userMergeFields = mergeFields.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.ToLower()).Where(x => x.StartsWith("user."));
            if (userMergeFields.Any())
            {
                IQueryable<IAtom> dataset = null;

                if (userID.HasValue)
                    dataset = dataProvider.GetData(typeof(UserLink));

                if (dataset == null)
                    lookups.AddRange(userMergeFields.Select(x => new MergeLookup { MergeField = x, IsValid = false, IsNull = true, Result = null }));
                else
                    lookups.AddRange(dataset.Lookup(dataProvider, CultureInfo, userID.Value, nameof(UserLink.AuthUserID), userMergeFields, typeof(UserLink)));
            }

            IEnumerable<string> sourceMergeFields = mergeFields.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.ToLower()).Where(x => !x.StartsWith("user."));
            if (sourceMergeFields.Any())
            {
                Type classType = null;
                IQueryable<IAtom> dataset = null;

                if (classTypeID.HasValue && ID.HasValue)
                {
                    classType = ModelToClassType.FromClassTypeID(classTypeID.Value);
                    dataset = dataProvider.GetData(classType);
                }

                if (dataset == null)
                    lookups.AddRange(sourceMergeFields.Select(x => new MergeLookup { MergeField = x, IsValid = false, IsNull = true, Result = null }));
                else
                    lookups.AddRange(dataset.Lookup(dataProvider, CultureInfo, ID.Value, nameof(IAtom<int>.ID), sourceMergeFields, classType));
            }

            return lookups;
        }

        /// <summary>
        /// This procedure all of the fields in a text document.  The document is parsed for { } delimited content.
        /// All content found is aggregated and sent to the Lookup(MergeFields) endpoint.  The looked up values are 
        /// merged back into the text and the results returned.
        /// Any value that was not able to be looked up remains unchanged.
        /// </summary>
        /// <param name="document"></param>
        /// <returns>A string containing the merged document.</returns>
        public async Task<string> Merge(string document)
        {
            await CheckRootValidity();

            return ReplaceBracketValues(document, GetBracketValues(document));
        }

        /// <summary>
        /// Gets all values between curly brackets
        /// Runs a lookup for each value
        /// Stores the valid lookup result to the dictionary
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetBracketValues(string document)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            // gets all keys between curly brackets
            MatchCollection matches = Regex.Matches(document, @"{([^{^}]+)}");

            IEnumerable<string> mergeFields = matches.Select(m => m.Groups[1].Value.ToLower()).Distinct();

            IEnumerable<IMergeLookup> lookups = Lookup(mergeFields);

            foreach (IMergeLookup lookup in lookups)
            {
                // only return valid results
                if (lookup.IsValid)
                    result.Add(lookup.MergeField, lookup.ToString());
            }

            return result;
        }

        /// <summary>
        /// Replaces all values between curly brackets
        /// 
        /// </summary>
        /// <param name="document"></param>
        /// <param name="lookups"></param>
        /// <returns></returns>
        private string ReplaceBracketValues(string document, Dictionary<string, string> lookups)
        {
            string result = document;

            foreach (var lookup in lookups)
                result = result.Replace(
                    $"{{{lookup.Key}}}",
                    lookup.Value,
                    StringComparison.InvariantCultureIgnoreCase);

            return result;
        }

        private Task CheckRootValidity()
        {
            if (!classTypeID.HasValue)
                throw new ArgumentException("ClassTypeID is missing.");
            else if (!ID.HasValue)
                throw new ArgumentException("ID is missing.");
            else
                if (!CheckIfRootExist())
                    throw new KeyNotFoundException("Can't find the root object.");

            return Task.CompletedTask;
        }

        private bool CheckIfRootExist()
        {
            Type classType = ModelToClassType.FromClassTypeID(classTypeID.Value);

            if(classType != null)
            {
                IQueryable<IAtom> dataset = dataProvider.GetData(classType);

                if (classType.GetInterfaces().Contains(typeof(IAtom<int>)))
                    dataset = dataset.Cast<IAtom<int>>().Where(x => x.ID.Equals(ID));
                else if (classType.GetInterfaces().Contains(typeof(IAtom<short>)))
                    dataset = dataset.Cast<IAtom<short>>().Where(x => x.ID.Equals((short)ID));
                else if (classType.GetInterfaces().Contains(typeof(IAtom<byte>)))
                    dataset = dataset.Cast<IAtom<byte>>().Where(x => x.ID.Equals((byte)ID));

                if (dataset.ToList().Count > 0)
                    return true;
            }
            else
                throw new ArgumentException("ClassTypeID is invalid.");

            return false;
        }
    }
}
