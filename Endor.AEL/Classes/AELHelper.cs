﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL
{
    public static class AELHelper
    {
        public static class Expressions
        {
            public class WhereCondition
            {
                public string WhereProperty { get; set; }
                public Expression WhereValue { get; set; }
                public Func<Expression, Expression, BinaryExpression> TestFunction { get; set; }

                public BinaryExpression Condition(Expression objExpression)
                {
                    if (TestFunction == null)
                        return Expression.Equal(GetPropertyExpression(objExpression, WhereProperty), WhereValue);
                    else
                        return TestFunction(GetPropertyExpression(objExpression, WhereProperty), WhereValue);
                }
            }

            private static MemberExpression GetPropertyExpression(Expression exp, string listPropertyName)
            {
                String[] propertyNames = listPropertyName.Split('.');

                MemberExpression result = Expression.Property(exp, propertyNames[0]);

                for (int i = 1; i < propertyNames.Length; i++)
                    result = Expression.Property(result, propertyNames[i]);

                return result;
            }

            private static MethodCallExpression ToList<ItemT>(Expression exp)
            {
                return Expression.Call(
                      typeof(Enumerable)
                    , "ToList"
                    , new Type[]
                    {
                       typeof(ItemT)
                    }
                    , exp
                );
            }

            private static MethodCallExpression Distinct<ItemT>(Expression exp)
            {
                return Expression.Call(
                      typeof(Enumerable)
                    , "Distinct"
                    , new Type[]
                    {
                      typeof(ItemT)
                    }
                    , exp
                );
            }

            public static readonly Expression NullString = Expression.Constant(null, typeof(string));
            public static readonly Expression NullStringList = Expression.Constant(null, typeof(List<string>));
            public static readonly Expression NullNumber = Expression.Constant(null, typeof(decimal?));
            public static readonly Expression NullNumberList = Expression.Constant(null, typeof(List<decimal?>));
            public static readonly Expression NullBoolean = Expression.Constant(null, typeof(bool?));
            public static readonly Expression NullBooleanList = Expression.Constant(null, typeof(List<bool?>));
            public static readonly Expression NullDateTime = Expression.Constant(null, typeof(DateTime?));
            public static readonly Expression NullDateTimeList = Expression.Constant(null, typeof(List<DateTime?>));
            public static readonly Expression NullLocation = Expression.Constant(null, typeof(LocationData));
            public static readonly Expression NullOrderItem = Expression.Constant(null, typeof(OrderItemData));
            public static readonly Expression NullOrderItemStatus = Expression.Constant(null, typeof(OrderItemStatus));
            public static readonly Expression NullOrderItemSubStatus = Expression.Constant(null, typeof(OrderItemSubStatus));
            public static readonly Expression NullOrder = Expression.Constant(null, typeof(TransactionHeaderData));
            public static readonly Expression NullCompany = Expression.Constant(null, typeof(CompanyData));
            public static readonly Expression NullObject = Expression.Constant(null, typeof(object));
            public static readonly Expression NullFlatListItem = Expression.Constant(null, typeof(FlatListItem));
            public static readonly Expression NullEmployee = Expression.Constant(null, typeof(EmployeeData));
            public static readonly Expression NullSurcharge = Expression.Constant(null, typeof(OrderItemSurcharge));
            public static readonly Expression NullSurchargeList = Expression.Constant(null, typeof(ICollection<OrderItemSurcharge>));
            public static readonly Expression NullContact = Expression.Constant(null, typeof(ContactData));
            public static readonly Expression NullOrderContactRoleCollection = Expression.Constant(null, typeof(ICollection<OrderContactRole>));


            public static readonly Expression ZeroDecimal = Expression.Constant(0m, typeof(decimal));
            public static readonly Expression ZeroInt = Expression.Constant(0, typeof(int));

            public static readonly Expression ZeroNumber = Expression.Constant(0m, typeof(decimal?));
            
            public static readonly Expression EmptyString = Expression.Constant("", typeof(string));
            public static readonly Expression NewLineString = Expression.Constant(Environment.NewLine, typeof(string));
            public static readonly Expression SpaceString = Expression.Constant(" ", typeof(string));

            public static readonly Expression BooleanTrue = Expression.Constant(true, typeof(bool));
            public static readonly Expression BooleanFalse = Expression.Constant(false, typeof(bool));
            public static readonly Expression BooleanNullableTrue = Expression.Constant(true, typeof(bool?));
            public static readonly Expression BooleanNullableFalse = Expression.Constant(false, typeof(bool?));

            public static Expression ConvertToBoolean(Expression exp) => Expression.Convert(exp, typeof(bool?));
            public static Expression ConvertToDateTime(Expression exp) => Expression.Convert(exp, typeof(DateTime?));
            public static Expression ConvertToNumber(Expression exp) => Expression.Convert(exp, typeof(decimal?));

            public static Expression ObjectProperty<InputT, OutputT>(Expression exp, string propertyName, bool forSQL)
            {
                var propExp = GetPropertyExpression(exp, propertyName);

                if (forSQL)
                    return propExp;

                return Expression.Condition(
                      Expression.Equal(exp, Expression.Constant(null, typeof(InputT)))
                    , Expression.Constant(null, typeof(OutputT))
                    , propExp
                );
            }
            public static Expression BooleanProperty<InputT>(Expression exp, string propertyName, bool forSQL)
            {
                var propExp = ConvertToBoolean(GetPropertyExpression(exp, propertyName));

                if (forSQL)
                    return propExp;

                return Expression.Condition(
                      Expression.Equal(exp, Expression.Constant(null, typeof(InputT)))
                    , NullBoolean
                    , propExp
                );
            }
            public static Expression NumberProperty<InputT>(Expression exp, string propertyName, bool forSQL)
            {
                var propExp = ConvertToNumber(GetPropertyExpression(exp, propertyName));

                if (forSQL)
                    return propExp;

                return Expression.Condition(
                      Expression.Equal(exp, Expression.Constant(null, typeof(InputT)))
                    , NullNumber
                    , propExp
                );
            }
            public static Expression StringProperty<InputT>(Expression exp, string propertyName, bool forSQL)
            {
                var propExp = GetPropertyExpression(exp, propertyName);

                if (forSQL)
                    return propExp;

                return Expression.Condition(
                      Expression.Equal(exp, Expression.Constant(null, typeof(InputT)))
                    , NullString
                    , propExp
                );
            }
            public static Expression DateTimeProperty<InputT>(Expression exp, string propertyName, bool forSQL)
            {
                var propExp = ConvertToDateTime(GetPropertyExpression(exp, propertyName));

                if (forSQL)
                    return propExp;

                return Expression.Condition(
                      Expression.Equal(exp, Expression.Constant(null, typeof(InputT)))
                    , NullDateTime
                    , propExp
                );
            }

            /// <summary>
            /// Returns an expression representing a list property of InputT as a List<OutputT>
            /// </summary>
            /// <typeparam name="InputT"></typeparam>
            /// <typeparam name="OutputT"></typeparam>
            /// <param name="exp"></param>
            /// <param name="propertyName"></param>
            /// <param name="forSQL"></param>
            /// <returns></returns>
            public static Expression ObjectListProperty<InputT, OutputItemT>(Expression exp, string propertyName, bool forSQL)
            {
                var propExp = GetPropertyExpression(exp, propertyName);

                if (forSQL)
                    return propExp;

                var callDistinct = Distinct<OutputItemT>(propExp);

                var callToList = ToList<OutputItemT>(callDistinct);

                var propEqualNullCheck = Expression.Equal(
                      propExp
                    , Expression.Constant(null, typeof(IEnumerable<OutputItemT>))
                );

                var propNull = Expression.Condition(
                      propEqualNullCheck
                    , Expression.Constant(null, typeof(List<OutputItemT>))
                    , callToList
                );

                var inputEqualNullCheck = Expression.Equal(
                      exp
                    , Expression.Constant(null, typeof(InputT))
                );

                return Expression.Condition(
                      inputEqualNullCheck
                    , Expression.Constant(null, typeof(List<OutputItemT>))
                    , propNull
                );
            }

            public static Expression SelectFromObjectList<ListItemT, OutputT>(Expression listExp, string selectPropertyName, bool forSQL)
            {
                return ObjectListWithWhereSelect<ListItemT, OutputT>(listExp, selectPropertyName, forSQL);
            }

            public static Expression ObjectListSelectMany<ListItemT, OutputItemT>(Expression listExp, string selectManyPropertyName, bool forSQL)
            {
                Expression selectList = listExp;
                var paramSelect = Expression.Parameter(typeof(ListItemT), "l");

                var selectProp = GetPropertyExpression(paramSelect, selectManyPropertyName);

                var callSelect = Expression.Call(
                      typeof(Enumerable)
                    , "SelectMany"
                    , new Type[]
                    {
                        typeof(ListItemT)
                      , typeof(OutputItemT)
                    }
                    , selectList
                    , Expression.Lambda(
                          selectProp
                        , paramSelect
                    )
                );

                if (forSQL)
                    return callSelect;

                var callDistinct = Distinct<OutputItemT>(callSelect);

                var callToList = ToList<OutputItemT>(callDistinct);

                var selectEqualNull = Expression.Equal(
                      callSelect
                    , Expression.Constant(null, typeof(List<ListItemT>))
                );

                var checkToList = Expression.Condition(
                      selectEqualNull
                    , Expression.Constant(null, typeof(List<OutputItemT>))
                    , callToList
                );

                var listEqualNull = Expression.Equal(
                      listExp
                    , Expression.Constant(null, typeof(List<ListItemT>))
                );

                return Expression.Condition(
                      listEqualNull
                    , Expression.Constant(null, typeof(List<OutputItemT>))
                    , checkToList
                );
            }

            /// <summary>
            /// Returns an expression representing a select of a item property of type ListItemT of a list property of InputT as a List<OutputT>
            /// </summary>
            /// <typeparam name="InputT"></typeparam>
            /// <typeparam name="ListItemT"></typeparam>
            /// <typeparam name="OutputT"></typeparam>
            /// <param name="exp"></param>
            /// <param name="listPropertyName"></param>
            /// <param name="selectPropertyName"></param>
            /// <param name="forSQL"></param>
            /// <returns></returns>
            public static Expression ObjectListPropertyWithSelect<InputT, ListItemT, OutputT>(Expression exp, string listPropertyName, string selectPropertyName, bool forSQL)
            {
                return ObjectListPropertyWithWhereSelect<InputT, ListItemT, OutputT>(exp, listPropertyName, selectPropertyName, forSQL);
            }

            public static Expression ObjectListWithWhereSelect<ListItemT, OutputItemT>(Expression listExp, string selectPropertyName, bool forSQL, params WhereCondition[] whereConditions)
            {
                Expression selectList = listExp;
                var paramSelect = Expression.Parameter(typeof(ListItemT), "l");

                if (whereConditions != null)
                {
                    Expression whereCondition = null;

                    Expression currentCondition = null;
                    foreach (WhereCondition wc in whereConditions)
                    {
                        currentCondition = wc.Condition(paramSelect);

                        if (whereCondition == null)
                            whereCondition = currentCondition;
                        else
                            whereCondition = Expression.AndAlso(whereCondition, currentCondition);
                    }

                    if (whereCondition != null)
                        selectList = Expression.Call(
                                          typeof(Enumerable)
                                        , "Where"
                                        , new Type[]
                                        {
                                    typeof(ListItemT)
                                        }
                                        , selectList
                                        , Expression.Lambda(
                                                  whereCondition
                                                , paramSelect
                                            )
                                        );

                }

                var selectProp = GetPropertyExpression(paramSelect, selectPropertyName);

                var callSelect = Expression.Call(
                      typeof(Enumerable)
                    , "Select"
                    , new Type[]
                    {
                        typeof(ListItemT)
                      , typeof(OutputItemT)
                    }
                    , selectList
                    , Expression.Lambda(
                          selectProp
                        , paramSelect
                    )
                );

                if (forSQL)
                    return callSelect;

                var callDistinct = Distinct<OutputItemT>(callSelect);

                var callToList = ToList<OutputItemT>(callDistinct);

                var selectEqualNull = Expression.Equal(
                      callSelect
                    , Expression.Constant(null, typeof(List<OutputItemT>))
                );

                var checkToList = Expression.Condition(
                      selectEqualNull
                    , Expression.Constant(null, typeof(List<OutputItemT>))
                    , callToList
                );

                var listEqualNull = Expression.Equal(
                      listExp
                    , Expression.Constant(null, typeof(List<ListItemT>))
                );

                return Expression.Condition(
                      listEqualNull
                    , Expression.Constant(null, typeof(List<OutputItemT>))
                    , checkToList
                );
            }

            public static Expression ObjectListPropertyWithWhereSelect<InputT, ListItemT, OutputItemT>(Expression exp, string listPropertyName, string selectPropertyName, bool forSQL, params WhereCondition[] whereConditions)
            {
                Expression listExp = GetPropertyExpression(exp, listPropertyName);

                Expression selectExp = ObjectListWithWhereSelect<ListItemT, OutputItemT>(listExp, selectPropertyName, forSQL, whereConditions);

                if (forSQL)
                    return selectExp;

                return Expression.Condition(
                      Expression.Equal(exp, Expression.Constant(null, typeof(InputT)))
                    , Expression.Constant(null, typeof(List<OutputItemT>))
                    , selectExp
                );
            }

            public static Expression ObjectListPropertyWithWhereFirstOrDefault<InputT, OutputT>(Expression exp, string listPropertyName, bool forSQL, params WhereCondition[] whereConditions)
            {
                var paramSelect = Expression.Parameter(typeof(OutputT), "l");
                Expression selectList = GetPropertyExpression(exp, listPropertyName);

                if (whereConditions != null)
                {
                    Expression whereCondition = null;

                    Expression currentCondition = null;
                    foreach (WhereCondition wc in whereConditions)
                    {
                        currentCondition = wc.Condition(paramSelect);

                        if (whereCondition == null)
                            whereCondition = currentCondition;
                        else
                            whereCondition = Expression.AndAlso(whereCondition, currentCondition);
                    }

                    if (whereCondition != null)
                        selectList = Expression.Call(
                                          typeof(Enumerable)
                                        , "Where"
                                        , new Type[]
                                        {
                                          typeof(OutputT)
                                        }
                                        , selectList
                                        , Expression.Lambda(
                                                  whereCondition
                                                , paramSelect
                                            )
                                        );

                }


                var callFirstOrDefault = Expression.Call(
                      typeof(Enumerable)
                    , "FirstOrDefault"
                    , new Type[]
                    {
                                typeof(OutputT)
                    }
                    , selectList
                );

                if (forSQL)
                    return callFirstOrDefault;

                var equalNull = Expression.Equal(
                      selectList
                    , Expression.Constant(null, typeof(List<OutputT>))
                );

                var listNullCheck = Expression.Condition(
                      equalNull
                    , Expression.Constant(null, typeof(OutputT))
                    , callFirstOrDefault
                );

                return Expression.Condition(
                      Expression.Equal(exp, Expression.Constant(null, typeof(InputT)))
                    , Expression.Constant(null, typeof(OutputT))
                    , listNullCheck
                );
            }
        }
    }
}
