﻿using Endor.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL
{
    public static class AELTermExtensions
    {
        private static Expression<Func<T, bool>> GetExpressionFromOperation<T>(IDataProvider dataProvider, AELOperation op)
        {
            ParameterExpression argument = Expression.Parameter(typeof(T));
            Expression body = GetFuncFromOperation(dataProvider, op, argument);

            return Expression.Lambda<Func<T, bool>>(
                     Expression.Property(body, "Value"),
                     new[] { argument }
                    );
        }

        private static Expression GetFuncFromOperation(IDataProvider dataProvider, AELOperation op, Expression arg)
        {
            if (op?.Terms == null || op.Terms.Count == 0)
                return null;

            //https://corebridge.atlassian.net/browse/END-7322
            if (op.Operator == OperatorType.EQ && op?.Terms?.Count == 2 && op.Terms[1].DataType == DataType.None)
            {
                op.Operator = OperatorType.ISNOTNULL;
            }

            if (op.Terms.Count == 1)
            {
                if (op.Terms[0] is AELOperation childOp)
                    return GetFuncFromOperation(dataProvider, childOp, arg);

                return null;
            }

            OperatorInfo opInfo = OperatorDictionary.Instance[op.Operator];

            if (op.Terms[0] is AELMember member && op.Terms[1] is AELValues values)
                return GetFuncFromMembers(dataProvider, opInfo, arg, member, values);

            //OperandType opType = opInfo.FindOperandType(op.LhsDataType, op.RhsDataType);
            OperandType opType = opInfo.FindOperandType(op.LhsDataType, op.RhsDataType, op.DataType);

            Expression result = null;
            foreach (AELTerm t in op.Terms)
            {
                if (t is AELOperation childOp)
                {
                    Expression exp = GetFuncFromOperation(dataProvider, childOp, arg);
                    if (result == null)
                        result = exp;
                    else
                        result = opType.Expression(result, exp, true);
                }
            }

            return result;
        }

        private class FuncFromMemberInfo
        {
            public Expression SingularExp { get; set; }
            public Expression PluralExp { get; set; }
            public ParameterExpression Param { get; set; }

            public FuncFromMemberInfo ChildInfo { get; set; }

            public Expression GetExpression(Expression condition)
            {
                // ctx.OrderData.Where(o => o.Items.Any(i => i.EmployeeRoles.Where()))
                Expression result = Expression.Call(
                        typeof(Enumerable)
                        , "Any"
                        , new Type[]
                        {
                            Param.Type
                        }
                        , PluralExp
                        , Expression.Lambda(
                            condition
                            , Param
                            )
                        );


                if (ChildInfo == null)
                    return result;

                return ChildInfo.GetExpression(result);
            }
        }

        private static Expression GetFuncFromMembers(IDataProvider dataProvider, OperatorInfo opInfo, Expression arg, AELMember member, AELValues values)
        {
            //OperandType opType = opInfo.FindOperandType(member.DataType, values.DataType);
            var memberDataType = member.MemberBase?.DataType ?? DataType.None;
            OperandType opType = opInfo.FindOperandType(member.DataType, values.DataType, memberDataType);

            FuncFromMemberInfo leftInfo = GetFuncFromMember(dataProvider, member, arg);

            Expression right = values.GetExpression(dataProvider.BID);

            Expression condition = opType.Expression(leftInfo.SingularExp, right, true);

            if (leftInfo.PluralExp == null)
                return condition;

            return AELHelper.Expressions.ConvertToBoolean(leftInfo.GetExpression(Expression.Property(condition, "Value")));
        }

        private static FuncFromMemberInfo GetFuncFromMember(IDataProvider dataProvider, AELMember member, Expression arg)
        {
            if (member?.MemberBase == null)
                return new FuncFromMemberInfo() { SingularExp = arg };

            FuncFromMemberInfo subResult = GetFuncFromMember(dataProvider, member.MemberBase, arg);

            Type subResultType = subResult.SingularExp.Type;
            if (subResultType.GetInterfaces().Contains(typeof(IEnumerable)))
            {
                if (subResult.PluralExp != null)
                {
                    subResult.ChildInfo = new FuncFromMemberInfo
                    {
                        PluralExp = subResult.PluralExp,
                        Param = subResult.Param,
                    };
                }

                subResult.PluralExp = subResult.SingularExp;
                subResult.Param = Expression.Parameter(subResultType.GenericTypeArguments[0], "x");
                subResult.SingularExp = subResult.Param;
            }

            IMemberInfo memberInfo = member.MemberBase.DataType.FindMember(dataProvider, member);
            Expression resultExp;
            if (memberInfo.MemberType == MemberType.Method)
                resultExp = ((MethodInfo)memberInfo).Expression(subResult.SingularExp, null, true);
            else
                resultExp = ((PropertyInfo)memberInfo).Expression(subResult.SingularExp, true);

            return new FuncFromMemberInfo()
            {
                SingularExp = resultExp,
                PluralExp = subResult.PluralExp,
                Param = subResult.Param,
                ChildInfo = subResult.ChildInfo,
            };
        }

        public static IQueryable<T> Where<T>(this IQueryable<T> q, IDataProvider dataProvider, string operationString)
            where T : class, IAtom
        {
            AELOperation operation = AELOperation.Deserialize(operationString);

            return Where(q, dataProvider, operation);
        }

        public static IQueryable<T> Where<T>(this IQueryable<T> q, IDataProvider dataProvider, AELOperation operation)
            where T : class, IAtom
        {
            return q
                .Where(x => x.BID.Equals(dataProvider.BID))
                .Where(GetExpressionFromOperation<T>(dataProvider, operation));
        }

        private static (IMemberInfo memberInfo, Expression expression) GetExpressionFromMembers(Type parentType, Expression arg, IDataProvider dataProvider, string[] memberSegments)
        {
            PropertyInfo propertyInfo;

            if (memberSegments.Length == 1)
            {
                propertyInfo = parentType.FindMember(dataProvider, memberSegments[0]) as PropertyInfo;
                return (propertyInfo, propertyInfo?.Expression(arg, true));
            }

            propertyInfo = parentType.FindMember(dataProvider, memberSegments[0]) as PropertyInfo;

            if (propertyInfo == null)
                return (null, null);

            Type childType = BusinessMemberDictionary.MemberDictionaryByDataType(dataProvider)[propertyInfo.DataType]?.ParentType;

            if (childType == null)
                return (null, null);

            Expression parentExp = propertyInfo.Expression(arg, true);

            return GetExpressionFromMembers(childType, parentExp, dataProvider, memberSegments.Skip(1).ToArray());
        }


        private static (IMemberInfo memberInfo, Expression expression) GetExpressionFromSingleMember(Type parentType, Expression arg, IDataProvider dataProvider, string memberText)
        {
            if (string.IsNullOrWhiteSpace(memberText))
                return (null, null);

            string[] segments = memberText.Split(".");

            DataTypeMemberInfo dtMemberInfo = BusinessMemberDictionary.MemberDictionaryByType(dataProvider)[parentType];

            if (dtMemberInfo != null && dtMemberInfo.TextMatches(segments[0]))
                segments = segments.Skip(1).ToArray();

            return GetExpressionFromMembers(parentType, arg, dataProvider, segments);
        }

        public static IEnumerable<IMergeLookup> Lookup(this IQueryable<IAtom> q, IDataProvider dataProvider, CultureInfo cultureInfo, int id, string idFieldName, IEnumerable<string> membersText, Type parentType)
        {
            ParameterExpression inputParam = Expression.Parameter(typeof(IAtom));
            Expression inputParamAsType = Expression.Convert(inputParam, parentType);

            List<Expression> inits = new List<Expression>();
            bool hasValidItem = false;

            List<MergeLookup> result = new List<MergeLookup>();

            foreach (string memberText in membersText)
            {
                (IMemberInfo memberInfo, Expression memberExp) = GetExpressionFromSingleMember(parentType, inputParamAsType, dataProvider, memberText);

                if (memberExp == null)
                    result.Add(new MergeLookup { MergeField = memberText, IsNull = true, IsValid = false, Result = null });
                else
                {
                    result.Add(new MergeLookup { MergeField = memberText, IsNull = true, IsValid = true, Result = null, NumberType = memberInfo.NumberType, CultureInfo = cultureInfo });
                    inits.Add(Expression.Convert(memberExp, typeof(object)));
                    hasValidItem = true;
                }
            }

            if (!hasValidItem)
                return result;

            NewArrayExpression arrayInit = Expression.NewArrayInit(typeof(object), inits);

            Expression<Func<IAtom, object[]>> selectExp = Expression.Lambda<Func<IAtom, object[]>>(arrayInit, inputParam);

            IQueryable<IAtom> query = q;

            if (!string.IsNullOrWhiteSpace(idFieldName))
            {
                var idProp = parentType.GetProperty(idFieldName);

                if (idProp != null)
                {
                    Expression idExp = null;
                    if (idProp.PropertyType == typeof(int))
                        idExp = Expression.Constant(id, typeof(int));
                    else if (idProp.PropertyType == typeof(int?))
                        idExp = Expression.Constant(id, typeof(int?));
                    else if (idProp.PropertyType == typeof(short))
                        idExp = Expression.Constant((short)id, typeof(short));
                    else if (idProp.PropertyType == typeof(short?))
                        idExp = Expression.Constant((short)id, typeof(short?));
                    else if (idProp.PropertyType == typeof(byte))
                        idExp = Expression.Constant((byte)id, typeof(byte));
                    else if (idProp.PropertyType == typeof(byte?))
                        idExp = Expression.Constant((byte)id, typeof(byte?));

                    if (idExp != null)
                    {
                        Expression<Func<IAtom, bool>> expWhere = Expression.Lambda<Func<IAtom, bool>>(
                                                                                   Expression.Equal(Expression.Property(inputParamAsType, idFieldName), idExp),
                                                                                   new[] { inputParam });

                        query = query.Where(expWhere);
                    }
                }
            }

            object[] queryResult = query
                .Select(selectExp)
                .FirstOrDefault();

            int i = 0;
            foreach (MergeLookup mergeLookup in result)
            {
                if (mergeLookup.IsValid)
                {
                    mergeLookup.Result = queryResult[i];
                    mergeLookup.IsNull = mergeLookup.Result == null;
                    i++;
                }
            }

            return result;
        }
    }
}