﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.EF
{
    public abstract class EndorMigration : Migration, IRequiresReindex
    {
        public virtual int? ReindexClasstype()
        {
            return null;
        }

        public virtual List<int> ReindexClasstypes()
        {
            return null;
        }

        public virtual List<(int ctid, int id)> ReindexEntities()
        {
            return null;
        }

        public virtual (int ctid, int id)? ReindexEntity()
        {
            return null;
        }

        public virtual List<(int ctid, int id)> ClearAssemblies()
        {
            return null;
        }
        public virtual bool ClearAllAssemblies()
        {
            return false;
        }
    }
}
