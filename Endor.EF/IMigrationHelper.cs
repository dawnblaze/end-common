﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.EF
{
    public interface IMigrationHelper
    {
        void MigrateDb(ApiContext ctx);
        void ResetCopyAllDefaults(ApiContext ctx);

        void RunCopyAllDefaults(ApiContext ctx);
    }
}
