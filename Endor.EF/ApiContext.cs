﻿﻿using Endor.Models;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Endor.EF
{ 
    public partial class ApiContext : DbContext
    {
        private readonly ITenantDataCache _iTenantCache;
        private readonly IHttpContextAccessor _httpCtxAccess;
        private readonly short? _bid; 

        public virtual DbSet<ActivityAction> ActivityAction { get; set; }
        public virtual DbSet<ActivityEvent> ActivityEvent { get; set; }
        public virtual DbSet<ActivityGlactivity> ActivityGlactivity { get; set; }
        public virtual DbSet<AlertDefinition> AlertDefinition { get; set; }
        public virtual DbSet<AlertDefinitionAction> AlertDefinitionAction { get; set; }
        public virtual DbSet<BoardDefinitionData> BoardDefinitionData { get; set; }
        public virtual DbSet<BoardEmployeeLink> BoardEmployeeLink { get; set; }
        public virtual DbSet<BoardModuleLink> BoardModuleLink { get; set; }
        public virtual DbSet<BoardRoleLink> BoardRoleLink { get; set; }
        public virtual DbSet<BoardView> BoardView { get; set; }
        public virtual DbSet<BoardViewLink> BoardViewLink { get; set; }
        public virtual DbSet<BusinessData> BusinessData { get; set; }
        public virtual DbSet<BusinessLocator> BusinessLocator { get; set; }
        public virtual DbSet<BusinessTimeZoneLink> BusinessTimeZoneLink { get; set; }
        public virtual DbSet<CampaignData> CampaignData { get; set; }
        public virtual DbSet<CompanyCustomData> CompanyCustomData { get; set; }
        public virtual DbSet<CompanyData> CompanyData { get; set; }
        public virtual DbSet<CompanyLocator> CompanyLocator { get; set; }
        public virtual DbSet<ContactData> ContactData { get; set; }
        public virtual DbSet<ContactCustomData> ContactCustomData { get; set; }
        public virtual DbSet<ContactLocator> ContactLocator { get; set; }
        public virtual DbSet<CrmIndustry> CrmIndustry { get; set; }
        public virtual DbSet<CrmOrigin> CrmOrigin { get; set; }
        public virtual DbSet<CustomFieldDefinition> CustomFieldDefinition { get; set; }

        public virtual DbSet<CustomFieldLayoutDefinition> CustomFieldLayoutDefinition { get; set; }
        public virtual DbSet<CustomFieldLayoutElement> CustomFieldLayoutElement { get; set; }

        public virtual DbSet<CustomFieldOtherData> CustomFieldOtherData { get; set; }
        public virtual DbSet<DMAccessToken> DMAccessToken { get; set; }
        public virtual DbSet<EmailAccountData> EmailAccountData { get; set; }
        public virtual DbSet<EmployeeData> EmployeeData { get; set; }
        public virtual DbSet<EmployeeLocator> EmployeeLocator { get; set; }
        public virtual DbSet<EmployeeTeam> EmployeeTeam { get; set; }
        public virtual DbSet<EmployeeTeamLink> EmployeeTeamLink { get; set; }
        public virtual DbSet<EmployeeTeamLocationLink> EmployeeTeamLocationLink { get; set; }
        public virtual DbSet<EnumAccountingCurrencyType> EnumAccountingCurrencyType { get; set; }
        public virtual DbSet<EnumActivityType> EnumActivityType { get; set; }
        public virtual DbSet<EnumCampaignStage> EnumCampaignStage { get; set; }
        public virtual DbSet<EnumCampaignType> EnumCampaignType { get; set; }
        public virtual DbSet<EnumClassType> EnumClassType { get; set; }
        public virtual DbSet<EnumCrmCompanyStatus> EnumCrmCompanyStatus { get; set; }
        public virtual DbSet<EnumDataType> EnumDataType { get; set; }
        public virtual DbSet<EnumCustomFieldNumberDisplayType> EnumCustomFieldNumberDisplayType { get; set; }
        public virtual DbSet<EnumEmailAccountStatus> EnumEmailAccountStatus { get; set; }
        public virtual DbSet<EnumFlatListType> EnumFlatListType { get; set; }
        public virtual DbSet<EnumGLAccountType> EnumGLAccountType { get; set; }
        public virtual DbSet<EnumLeadStage> EnumLeadStage { get; set; }
        public virtual DbSet<EnumLocatorSubType> EnumLocatorSubType { get; set; }
        public virtual DbSet<EnumLocatorType> EnumLocatorType { get; set; }
        public virtual DbSet<EnumMaterialConsumptionMethod> EnumMaterialConsumptionMethod { get; set; }
        public virtual DbSet<EnumMaterialCostingMethod> EnumMaterialCostingMethod { get; set; }
        public virtual DbSet<EnumMaterialPhysicalType> EnumMaterialPhysicalType { get; set; }
        public virtual DbSet<EnumModule> EnumModule { get; set; }
        public virtual DbSet<EnumOpportunityStage> EnumOpportunityStage { get; set; }
        public virtual DbSet<EnumOrderDestinationType> EnumOrderDestinationType { get; set; }
        public virtual DbSet<EnumOrderKeyDateType> EnumOrderKeyDateType { get; set; }
        public virtual DbSet<EnumOrderContactRoleType> EnumOrderContactRoleType { get; set; }
        public virtual DbSet<EmployeeRole> EmployeeRole { get; set; }
        public virtual DbSet<EnumOrderNoteType> EnumOrderNoteType { get; set; }
        public virtual DbSet<EnumOrderOrderStatus> EnumOrderOrderStatus { get; set; }
        public virtual DbSet<EnumOrderTransactionLevel> EnumOrderTransactionLevel { get; set; }
        public virtual DbSet<EnumOrderTransactionType> EnumOrderTransactionType { get; set; }
        public virtual DbSet<EnumPaymentMethodType> EnumPaymentMethodType { get; set; }
        public virtual DbSet<EnumTimeZone> EnumTimeZone { get; set; }
        public virtual DbSet<EnumUserAccessType> EnumUserAccessType { get; set; }
        public virtual DbSet<EnumRoleAccess> EnumRoleAccess { get; set; }
        public virtual DbSet<EnumAssociationType> EnumAssociationType { get; set; }
        public virtual DbSet<EnumPaymentDueBasedOnType> EnumPaymentDueBasedOnType { get; set; }
        public virtual DbSet<EnumTaxEngineType> EnumTaxEngineType { get; set; }        
        public virtual DbSet<EstimateData> EstimateData { get; set; }
        public virtual DbSet<FlatListItem> FlatListItem { get; set; }
        public virtual DbSet<GLAccount> GLAccount { get; set; }
        public virtual DbSet<GLData> GLData { get; set; }
        public virtual DbSet<LaborCategory> LaborCategory { get; set; }
        public virtual DbSet<LaborCategoryLink> LaborCategoryLink { get; set; }
        public virtual DbSet<LaborData> LaborData { get; set; }
        public virtual DbSet<SystemListFilter> ListFilter { get; set; }
        public virtual DbSet<LocationData> LocationData { get; set; }
        public virtual DbSet<LocationLocator> LocationLocator { get; set; }
        public virtual DbSet<MachineCategory> MachineCategory { get; set; }
        public virtual DbSet<MachineCategoryLink> MachineCategoryLink { get; set; }
        public virtual DbSet<MachineData> MachineData { get; set; }
        public virtual DbSet<MachineInstance> MachineInstance { get; set; }
        public virtual DbSet<MachineProfile> MachineProfile { get; set; }
        public virtual DbSet<MachineProfileTable> MachineProfileTable { get; set; }
        public virtual DbSet<MachineProfileVariable> MachineProfileVariable { get; set; }
        public virtual DbSet<DestinationData> DestinationData { get; set; }
        public virtual DbSet<DestinationProfile> DestinationProfile { get; set; }
        public virtual DbSet<DestinationProfileTable> DestinationProfileTable { get; set; }
        public virtual DbSet<DestinationProfileVariable> DestinationProfileVariable { get; set; }
        public virtual DbSet<SimpleDestinationData> SimpleDestinationData { get; set; }
        public virtual DbSet<MaterialCategory> MaterialCategory { get; set; }
        public virtual DbSet<MaterialCategoryLink> MaterialCategoryLink { get; set; }
        public virtual DbSet<MaterialData> MaterialData { get; set; }
        public virtual DbSet<OpportunityCustomData> OpportunityCustomData { get; set; }
        public virtual DbSet<OpportunityData> OpportunityData { get; set; }
        public virtual DbSet<OptionData> OptionData { get; set; }
        public virtual DbSet<OrderData> OrderData { get; set; }
        public virtual DbSet<CreditMemoData> CreditMemoData { get; set; }
        public virtual DbSet<OrderCustomData> OrderCustomData { get; set; }
        public virtual DbSet<OrderItemStatus> OrderItemStatus { get; set; }
        public virtual DbSet<OrderItemSubStatus> OrderItemSubStatus { get; set; }
        public virtual DbSet<OrderItemStatusSubStatusLink> OrderItemStatusSubStatusLink { get; set; }
        public virtual DbSet<OrderKeyDate> OrderKeyDate { get; set; }
        public virtual DbSet<OrderNote> OrderNote { get; set; }
        public virtual DbSet<OrderContactRole> OrderContactRole { get; set; }
        public virtual DbSet<OrderContactRoleLocator> OrderContactRoleLocator { get; set; }
        public virtual DbSet<OrderEmployeeRole> OrderEmployeeRole { get; set; }
        public virtual DbSet<OrderItemData> OrderItemData { get; set; }
        public virtual DbSet<OrderDestinationData> OrderDestinationData { get; set; }
        public virtual DbSet<OrderItemComponent> OrderItemComponent { get; set; }
        public virtual DbSet<OrderItemSurcharge> OrderItemSurcharge { get; set; }
        public virtual DbSet<OrderOrderLink> OrderOrderLink { get; set; }
        public virtual DbSet<OrderItemTaxAssessment> OrderItemTaxAssessment { get; set; }
        //https://corebridge.atlassian.net/browse/END-472
        public virtual DbSet<PaymentMaster> PaymentMaster { get; set; }
        public virtual DbSet<PaymentApplication> PaymentApplication { get; set; }
        public virtual DbSet<PaymentMethod> PaymentMethod { get; set; }
        public virtual DbSet<PaymentTerm> PaymentTerm { get; set; }
        public virtual DbSet<ReportMenu> ReportMenu { get; set; }
        public virtual DbSet<RightsGroup> RightsGroup { get; set; }
        public virtual DbSet<RightsGroupList> RightsGroupList { get; set; }
        public virtual DbSet<RightsGroupListRightsGroupLink> RightsGroupListRightsGroupLink { get; set; }
        public virtual DbSet<RightsGroupChildGroupLink> RightsGroupChildGroupLink { get; set; }
        public virtual DbSet<RightsGroupRightLink> RightsGroupRightLink { get; set; }
        public virtual DbSet<RightsGroupMenuTree> RightsGroupMenuTree { get; set; }
        public virtual DbSet<RightsUserAccessTypeGroupLink> RightsUserAccessTypeGroupLink { get; set; }
        public virtual DbSet<RootData> RootData { get; set; }
        public virtual DbSet<SimpleAlertDefinition> SimpleAlertDefinition { get; set; }
        public virtual DbSet<SimpleBoardView> SimpleBoardView { get; set; }
        public virtual DbSet<SimpleBusinessData> SimpleBusinessData { get; set; }
        public virtual DbSet<SimpleCompanyData> SimpleCompanyData { get; set; }
        public virtual DbSet<SimpleContactData> SimpleContactData { get; set; }
        public virtual DbSet<SimpleCrmIndustry> SimpleCrmIndustry { get; set; }
        public virtual DbSet<SimpleEmailAccountData> SimpleEmailAccountData { get; set; }
        public virtual DbSet<SimpleEmployeeData> SimpleEmployeeData { get; set; }
        public virtual DbSet<SimpleEmployeeTeam> SimpleEmployeeTeam { get; set; }
        public virtual DbSet<SimpleEnumTimeZone> SimpleEnumTimeZone { get; set; }
        public virtual DbSet<SimpleLaborData> SimpleLaborData { get; set; }
        public virtual DbSet<SimpleLocationData> SimpleLocationData { get; set; }
        public virtual DbSet<SimpleMachineData> SimpleMachineData { get; set; }
        public virtual DbSet<SimpleMachineCategory> SimpleMachineCategory { get; set; }
        public virtual DbSet<SimpleLaborCategory> SimpleLaborCategory { get; set; }
        public virtual DbSet<SimpleMaterialData> SimpleMaterialData { get; set; }
        public virtual DbSet<SimpleMaterialCategory> SimpleMaterialCategory { get; set; }
        public virtual DbSet<SimpleOriginData> SimpleOriginData { get; set; }
        public virtual DbSet<SimpleOrderData> SimpleOrderData { get; set; }
        public virtual DbSet<SimpleOrderItemData> SimpleOrderItemData { get; set; }
        public virtual DbSet<SimpleOrderDestinationData> SimpleOrderDestinationData { get; set; }
        public virtual DbSet<SimplePaymentTerm> SimplePaymentTerm { get; set; }
        public virtual DbSet<SimplePaymentMethod> SimplePaymentMethod { get; set; }
        public virtual DbSet<SimpleTaxabilityCode> SimpleTaxabilityCodes { get; set; }
        public virtual DbSet<SimpleTaxGroup> SimpleTaxGroup { get; set; }
        public virtual DbSet<SimpleTaxItem> SimpleTaxItem { get; set; }
        public virtual DbSet<SimpleSurchargeDef> SimpleSurchargeDef { get; set; }
        public virtual DbSet<SurchargeDef> SurchargeDef { get; set; }
        public virtual DbSet<SystemListColumn> SystemListColumn { get; set; }
        public virtual DbSet<SystemListFilterCriteria> SystemListFilterCriteria { get; set; }
        public virtual DbSet<SystemOptionCategory> SystemOptionCategory { get; set; }
        public virtual DbSet<SystemOptionDefinition> SystemOptionDefinition { get; set; }
        public virtual DbSet<SystemOptionSection> SystemOptionSection { get; set; }
        public virtual DbSet<TaxabilityCodeItemExemptionLink> TaxabilityCodeItemExemptionLinks { get; set; }
        public virtual DbSet<TaxabilityCode> TaxabilityCodes { get; set; }
        public virtual DbSet<TaxItem> TaxItem { get; set; }
        public virtual DbSet<TaxGroup> TaxGroup { get; set; }
        public virtual DbSet<TaxGroupItemLink> TaxGroupItemLink { get; set; }
        public virtual DbSet<TaxGroupLocationLink> TaxGroupLocationLink { get; set; }
        public virtual DbSet<UserLink> UserLink { get; set; }
        public virtual DbSet<SystemAutomationActionDefinition> SystemAutomationActionDefinition { get; set; }
        public virtual DbSet<SystemAutomationActionDataTypeLink> SystemAutomationActionDataTypeLink { get; set; }
        public virtual DbSet<SystemAutomationTriggerDefinition> SystemAutomationTriggerDefinition { get; set; }
        public virtual DbSet<EnumAutomationTriggerCategoryType> EnumAutomationTriggerCategoryType { get; set; }
        public virtual DbSet<SystemEmailSMTPConfigurationType> SystemEmailSMTPConfigurationType { get; set; }
        public virtual DbSet<EnumEmailProviderType> EmailProviderType { get; set; }
        public virtual DbSet<EnumEmailSecurityType> EmailSecurityType { get; set; }
        public virtual DbSet<DomainEmailLocationLink> DomainEmailLocationLink { get; set; }
        public virtual DbSet<DomainEmail> DomainEmail { get; set; }
        public virtual DbSet<SimpleDomainEmail> SimpleDomainEmail { get; set; }
        public virtual DbSet<DomainData> DomainData { get; set; }
        public virtual DbSet<EnumDomainAccessType> EnumDomainAccessType { get; set; }
        public virtual DbSet<EnumDomainStatus> EnumDomainStatus { get; set; }
        public virtual DbSet<SSLCertificateData> SSLCertificateData { get; set; }
        public virtual DbSet<LocationGoal> LocationGoal { get; set; }
        public virtual DbSet<BusinessGoal> BusinessGoal { get; set; }
        public virtual DbSet<DashboardData> DashboardData { get; set; }
        public virtual DbSet<DashboardWidgetData> DashboardWidgetData { get; set; }
        public virtual DbSet<DashboardWidgetDefinition> DashboardWidgetDefinition { get; set; }
        public virtual DbSet<SimpleDashboardWidgetData> SimpleDashboardWidgetData { get; set; }
        public virtual DbSet<SimpleDashboardWidgetDefinitionData> SimpleDashboardWidgetDefinitionData { get; set; }
        public virtual DbSet<SystemColor> SystemColor { get; set; }
        public virtual DbSet<ListTag> ListTag { get; set; }
        public virtual DbSet<ListTagOtherLink> ListTagOtherLink { get; set; }
        public virtual DbSet<CompanyTagLink> CompanyTagLink { get; set; }
        public virtual DbSet<ContactTagLink> ContactTagLink { get; set; }
        public virtual DbSet<OrderTagLink> OrderTagLink { get; set; }
        public virtual DbSet<OrderItemTagLink> OrderItemTagLink { get; set; }
        public virtual DbSet<OrderDestinationTagLink> OrderDestinationTagLink { get; set; }
        public virtual DbSet<TimeCard> TimeCard { get; set; }
        public virtual DbSet<TimeCardDetail> TimeCardDetail { get; set; }
        public virtual DbSet<TransactionHeaderData> TransactionHeaderData { get; set; }
        public virtual DbSet<AssemblyData> AssemblyData { get; set; }
        public virtual DbSet<AssemblyCategory> AssemblyCategory { get; set; }
        public virtual DbSet<AssemblyCategoryLink> AssemblyCategoryLink { get; set; }
        public virtual DbSet<SimpleAssemblyData> SimpleAssemblyData { get; set; }
        public virtual DbSet<SimpleAssemblyCategory> SimpleAssemblyCategory { get; set; }
        public virtual DbSet<AssemblyElement> AssemblyElement { get; set; }
        public virtual DbSet<AssemblyLayout> AssemblyLayout { get; set; }
        public virtual DbSet<AssemblyVariable> AssemblyVariable { get; set; }
        public virtual DbSet<AssemblyVariableFormula> AssemblyVariableFormula { get; set; }
        public virtual DbSet<EnumElementType> EnumElementType { get; set; }
        public virtual DbSet<PurchaseOrderData> PurchaseOrderData { get; set; }
        public virtual DbSet<UserDraft> UserDraft { get; set; }
        public virtual DbSet<SystemLanguageType> SystemLanguageTypes { get; set; }
        public virtual DbSet<SystemTranslationDefinition> SystemTranslationDefinitions { get; set; }
        public virtual DbSet<EnumAssemblyElementType> EnumAssemblyElementType { get; set; }
        public virtual DbSet<EnumAssemblyType> EnumAssemblyType { get; set; }
        public virtual DbSet<EnumAssemblyIncomeAllocationType> EnumAssemblyIncomeAllocationType { get; set; }
        public virtual DbSet<EnumAssemblyLabelType> EnumAssemblyLabelType { get; set; }
        public virtual DbSet<EnumAssemblyLayoutType> EnumAssemblyLayoutType { get; set; }
        public virtual DbSet<EnumMachineLayoutType> EnumMachineLayoutType { get; set; }
        public virtual DbSet<EnumAssemblyListDataType> EnumAssemblyListDataType { get; set; }
        public virtual DbSet<EnumAssemblyPricingType> EnumAssemblyPricingType { get; set; }
        public virtual DbSet<EnumPriceFormulaType> EnumPriceFormulaType { get; set; }
        public virtual DbSet<EnumAssemblyTableMatchType> EnumAssemblyTableMatchType { get; set; }
        public virtual DbSet<EnumAssemblyTableType> EnumAssemblyTableType { get; set; }
        public virtual DbSet<EnumAssemblyFormulaEvalType> EnumAssemblyFormulaEvalType { get; set; }
        public virtual DbSet<EnumAssemblyFormulaUseType> EnumAssemblyFormulaUseType { get; set; }

        public virtual DbSet<SystemAssemblyVariable> SystemAssemblyVariable { get; set; }
        public virtual DbSet<SimpleSystemAssemblyVariable> SimpleSystemAssemblyVariable { get; set; }

        public virtual DbSet<QuickItemData> QuickItemData { get; set; }
        public virtual DbSet<QuickItemCategoryLink> QuickItemCategoryLink { get; set; }
        public virtual DbSet<SimpleQuickItemData> SimpleQuickItemData { get; set; }

        public virtual DbSet<URLRegistrationData> URLRegistrationData { get; set; }
        public virtual DbSet<URLRegistrationLookupHistory> URLRegistrationLookupHistory { get; set; }
        public virtual DbSet<AssemblyTable> AssemblyTable { get; set; }
        public virtual DbSet<EnumDashboardWidgetCategoryType> EnumDashboardWidgetCategoryType { get; set; }
        public virtual DbSet<EnumEmbeddedAssemblyType> EnumEmbeddedAssemblyType { get; set; }
        public virtual DbSet<DashboardWidgetCategoryLink> DashboardWidgetCategoryLink { get; set; }
        public virtual DbSet<SystemMessageTemplateType> SystemMessageTemplateType { get; set; }
        public virtual DbSet<MessageBodyTemplate> MessageBodyTemplate { get; set; }
        public virtual DbSet<CompanyContactLink> CompanyContactLink { get; set; }
        public virtual DbSet<EnumContactRoles> EnumContactRoles { get; set; }
        public virtual DbSet<Reconciliation> Reconciliation { get; set; }
        public virtual DbSet<ReconciliationItem> ReconciliationItem { get; set; }
        public virtual DbSet<SimpleReconciliation> SimpleReconciliation { get; set; }
        public virtual DbSet<EmailAccountTeamLink> EmailAccountTeamLink { get; set; }

        public virtual DbSet<EnumOrderItemComponentType> EnumOrderItemComponentType { get; set; }

        public virtual DbSet<CompanyCustomDataValue> CompanyCustomDataValue { get; set; }
        public virtual DbSet<ContactCustomDataValue> ContactCustomDataValue { get; set; }
        public virtual DbSet<OpportunityCustomDataValue> OpportunityCustomDataValue { get; set; }
        public virtual DbSet<TransactionHeaderCustomDataValue> TransactionHeaderCustomDataValue { get; set; }
        public virtual DbSet<AssemblyCustomDataValue> AssemblyCustomDataValue { get; set; }
        public virtual DbSet<EmployeeCustomDataValue> EmployeeCustomDataValue { get; set; }
        public virtual DbSet<LaborCustomDataValue> LaborCustomDataValue { get; set; }
        public virtual DbSet<LocationCustomDataValue> LocationCustomDataValue { get; set; }
        public virtual DbSet<MachineCustomDataValue> MachineCustomDataValue { get; set; }
        public virtual DbSet<MaterialCustomDataValue> MaterialCustomDataValue { get; set; }

        public ApiContext(DbContextOptions<ApiContext> options, ITenantDataCache iTenantCache, IHttpContextAccessor httpCtx) : base(options)
        {
            _iTenantCache = iTenantCache;
            _httpCtxAccess = httpCtx;
            _bid = null;
        }
        public ApiContext(DbContextOptions<ApiContext> options, ITenantDataCache iTenantCache, short bidScope) : base(options)
        {
            _iTenantCache = iTenantCache;
            _httpCtxAccess = null;
            _bid = bidScope;
        }
        public ApiContext(ITenantDataCache iTenantCache, short bidScope)
        {
            _iTenantCache = iTenantCache;
            _httpCtxAccess = null;
            _bid = bidScope;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (_bid == null && _httpCtxAccess == null)
                throw new InvalidOperationException("Context has no way to get BID. Please pass a BID or IHttpContextAccessor at constructor time. Aborting configuration.");

            short bid = _bid ?? _httpCtxAccess.HttpContext.User.BID().Value;
            
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_iTenantCache.Get(bid).Result.BusinessDBConnectionString);
                optionsBuilder.EnableSensitiveDataLogging(false);
            }

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AlertDefinition>(entity =>
            {
                entity.ToTable("Alert.Definition");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14100))");
                entity.Property(e => e.Description).HasColumnType("nvarchar(max)");
                entity.Property(e => e.DataType).HasColumnType("smallint");
                entity.Property(e => e.TriggerConditionFx).HasColumnType("nvarchar(max)");
                entity.Property(e => e.TriggerReadable).HasColumnType("nvarchar(max)");
                entity.Property(e => e.ConditionFx).HasColumnType("nvarchar(max)");
                entity.Property(e => e.ConditionReadable).HasColumnType("nvarchar(max)");
                entity.Property(e => e.ConditionSQL).HasColumnType("nvarchar(max)");
                entity.Property(e => e.LastRunDT).HasColumnType("datetime2(2)");

                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.IsActive })
                    .HasName("IX_Alert.Definition_Employee");
                entity.HasIndex(e => new { e.BID, e.TriggerID, e.IsActive })
                    .HasName("IX_Alert.Definition_Trigger");
                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_Alert.Definition_Name");

                entity.HasOne(e => e.Business).WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Alert.Definition_Business.Data");
                entity.HasOne(e => e.Employee).WithMany()
                    .HasForeignKey(e => new { e.BID, e.EmployeeID })
                    .HasConstraintName("FK_Alert.Definition_Employee.Data");
                entity.HasOne(e => e.Trigger).WithMany()
                    .HasForeignKey(e => e.TriggerID)
                    .HasConstraintName("FK_Alert.Definition_System.Automation.Trigger.Definition");
            });

            modelBuilder.Entity<AlertDefinitionAction>(entity =>
            {
                entity.ToTable("Alert.Definition.Action");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14102))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.DataType).HasColumnType("smallint");
                entity.Property(e => e.Subject).HasColumnType("nvarchar(4000)");
                entity.Property(e => e.Body).HasColumnType("nvarchar(max)");
                entity.Property(e => e.MetaData).HasColumnType("xml");

                entity.HasOne(e => e.AlertDefinition).WithMany(e => e.Actions)
                    .HasForeignKey(e => new { e.BID, e.AlertDefinitionID })
                    .HasConstraintName("FK_Alert.Definition.Action_Alert.Definition");
                entity.HasOne(e => e.Definition).WithMany()
                    .HasForeignKey(e => e.ActionDefinitionID)
                    .HasConstraintName("FK_Alert.Definition.Action_System.Automation.Action.Definition");
            });

            modelBuilder.Entity<ActivityAction>(entity =>
            {
                entity.ToTable("Activity.Action");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((11001))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.StartDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.EndDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.IsComplete).HasComputedColumnSql("(case when [CompletedDT] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.CompletedDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.MetaData).HasColumnType("xml");
            });

            modelBuilder.Entity<ActivityEvent>(entity =>
            {
                entity.ToTable("Activity.Event");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((11002))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.StartDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.EndDT).HasColumnType("datetime2(0)")
                    .HasComputedColumnSql("([StartDT])");
                entity.Property(e => e.CompletedDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.IsComplete).HasComputedColumnSql("(case when [CompletedDT] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.MetaData).HasColumnType("xml");
            });

            modelBuilder.Entity<PaymentMaster>(entity =>
            {
                entity.ToTable("Accounting.Payment.Master");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((8010))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(7)")
                    .HasDefaultValueSql($"SYSUTCDATETIME()").IsRequired().ValueGeneratedOnAddOrUpdate();
                entity.Property(e => e.ValidToDT).HasColumnType("datetime2(7)")
                    .HasDefaultValueSql("(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))");
                entity.Property(e => e.AccountingDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.DisplayNumber).HasColumnType("nvarchar(50)");
                entity.Property(e => e.Notes).HasColumnType("nvarchar(100)");
                entity.Property(e => e.CurrencyType).HasColumnType("tinyint");
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 4)").HasDefaultValue(0);
                entity.Property(e => e.RefBalance).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.NonRefBalance).HasColumnType("decimal(18,4)");
                entity.Property(e => e.ChangeInRefCredit).HasColumnType("decimal(18,4)");
                entity.Property(e => e.ChangeInNonRefCredit).HasColumnType("decimal(18,4)");

                entity.HasIndex(e => new { e.BID, e.CompanyID, e.AccountingDT })
                    .HasName("IX_Accounting.Payment.Master_Company");
                entity.HasIndex(e => new { e.BID, e.AccountingDT, e.LocationID })
                    .HasName("IX_Accounting.Payment.Master_Date");
                entity.HasIndex(e => new { e.BID, e.LocationID, e.AccountingDT })
                    .HasName("IX_Accounting.Payment.Master_Location");

                entity.HasOne(e => e.Company)
                    .WithMany(c => c.Payments)
                    .HasForeignKey(t => new { t.BID, t.CompanyID })
                    .HasConstraintName("FK_Accounting.Payment.Master_Company.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.CurrencyTypeNavigation)
                    .WithMany()
                    .HasForeignKey(t => t.CurrencyType)
                    .HasConstraintName("FK_Accounting.Payment.Data_enum.Accounting.CurrencyType")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.PaymentTransactionTypeNavigation)
                    .WithMany()
                    .HasForeignKey(t => t.PaymentTransactionType)
                    .HasConstraintName("FK_Accounting.Payment.Master_enum.Accounting.PaymentTransactionType")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Location)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.LocationID })
                    .HasConstraintName("FK_Accounting.Payment.Master_Location.Data")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<PaymentApplication>(entity =>
            {
                entity.ToTable("Accounting.Payment.Application");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((8011))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(7)")
                    .HasDefaultValueSql($"SYSUTCDATETIME()").IsRequired().ValueGeneratedOnAddOrUpdate();
                entity.Property(e => e.ValidToDT).HasColumnType("datetime2(7)")
                    .HasDefaultValueSql("(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))");
                entity.Property(e => e.AccountingDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 4)").HasDefaultValue(0);
                entity.Property(e => e.DisplayNumber).HasColumnType("nvarchar(50)");
                entity.Property(e => e.CurrencyType).HasColumnType("tinyint");
                entity.Property(e => e.HasAdjustments).HasDefaultValue(0);
                entity.Property(e => e.RefBalance).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.NonRefBalance).HasColumnType("decimal(18,4)");
                entity.Property(e => e.AdjustedApplicationID).HasColumnType("int");
                entity.Property(e => e.MerchangtAccountID).HasColumnType("tinyint");
                entity.Property(e => e.TokenID).HasColumnType("varchar(64)");
                entity.Property(e => e.EnteredByContactID).HasColumnType("int");
                entity.Property(e => e.Notes).HasColumnType("nvarchar(100)");

                entity.HasIndex(e => new { e.BID, e.CompanyID, e.AccountingDT })
                    .HasName("IX_Accounting.Payment.Application_Company");
                entity.HasIndex(e => new { e.BID, e.AccountingDT, e.LocationID })
                    .HasName("IX_Accounting.Payment.Application_Date");
                entity.HasIndex(e => new { e.BID, e.LocationID, e.AccountingDT })
                    .HasName("IX_Accounting.Payment.Application_Location");
                entity.HasIndex(e => new { e.BID, e.OrderID })
                    .HasName("IX_Accounting.Payment.Application_Order");

                entity.HasOne(e => e.Master)
                    .WithMany(e => e.PaymentApplications)
                    .HasForeignKey(t => new { t.BID, t.MasterID })
                    .HasConstraintName("FK_Accounting.Payment.Application_Accounting.Payment.Application]")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.PaymentMethod)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.PaymentMethodID })
                    .HasConstraintName("FK_Accounting.Payment.Application_Accounting.Payment.Method")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Company)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.CompanyID })
                    .HasConstraintName("FK_Accounting.Payment.Application_Company.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.EnteredByContact)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.EnteredByContactID })
                    .HasConstraintName("FK_Accounting.Payment.Application_Contact.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.EnteredByEmployee)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.EnteredByEmployeeID })
                    .HasConstraintName("FK_Accounting.Payment.Application_Employee.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.CurrencyTypeNavigation)
                    .WithMany()
                    .HasForeignKey(t => t.CurrencyType)
                    .HasConstraintName("FK_Accounting.Payment.Application_enum.Accounting.CurrencyType")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.PaymentTypeNavigation)
                    .WithMany()
                    .HasForeignKey(t => t.PaymentType)
                    .HasConstraintName("FK_Accounting.Payment.Application_enum.Accounting.PaymentMethodType")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.PaymentTransactionTypeNavigation)
                    .WithMany()
                    .HasForeignKey(t => t.PaymentTransactionType)
                    .HasConstraintName("FK_Accounting.Payment.Application_enum.Accounting.PaymentTransactionType")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Location)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.LocationID })
                    .HasConstraintName("FK_Accounting.Payment.Data_Location.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Order)
                    .WithMany(m => m.Payments)
                    .HasForeignKey(t => new { t.BID, t.OrderID })
                    .HasConstraintName("FK_Accounting.Payment.Data_Order.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.AdjustedApplicationNavigation)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.AdjustedApplicationID })
                    .HasConstraintName("FK_Accounting.Payment.Application_Accounting.Payment.Application.AdjustedApplication")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.GLActivity)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.GLActivityID })
                    .HasConstraintName("FK_Accounting.Payment.Application_Activity.GLActivity")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<OrderItemTaxAssessment>(entity =>
            {
                entity.ToTable("Order.Tax.Item.Assessment");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((10004))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(7)")
                    .HasDefaultValueSql($"SYSUTCDATETIME()").IsRequired().ValueGeneratedOnAddOrUpdate();
                entity.Property(e => e.ValidToDT).HasColumnType("datetime2(7)")
                    .HasDefaultValueSql("(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))").IsRequired().ValueGeneratedOnAddOrUpdate();
                entity.Property(e => e.DestinationID).HasColumnType("int");
                entity.Property(e => e.TaxableAmount).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.TaxRate).HasColumnType("decimal(7, 4)");
                entity.Property(e => e.TaxAmount).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ItemComponentID).HasColumnType("INT SPARSE NULL");
                entity.Property(e => e.ItemSurchargeID).HasColumnType("INT SPARSE NULL");
                entity.Property(e => e.TaxGroupID).HasColumnType("SMALLINT SPARSE NULL");

                entity.HasIndex(e => new { e.BID, e.DestinationID })
                    .HasName("IX_Order.Tax.Item.Assessment_Dest");
                entity.HasIndex(e => new { e.BID, e.OrderItemID })
                    .HasName("IX_Order.Tax.Item.Assessment_Item");
                entity.HasIndex(e => new { e.BID, e.OrderID, e.OrderItemID, e.TaxCodeID })
                     .HasName("IX_Order.Tax.Item.Assessment_Order");

                entity.HasOne(e => e.TaxCode)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxCodeID })
                    .HasConstraintName("FK_Order.Tax.Item.Assessment_Accounting.Tax.Code")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.TaxItem)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxItemID })
                    .HasConstraintName("FK_Order.Tax.Item.Assessment_Accounting.Tax.Item")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Order)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.OrderID })
                    .HasConstraintName("FK_Order.Tax.Item.Assessment_Order.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Destination)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.DestinationID })
                    .HasConstraintName("FK_Order.Tax.Item.Assessment_Order.Destination.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.OrderItem)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.OrderItemID })
                    .HasConstraintName("FK_Order.Tax.Item.Assessment_Order.Item.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.ItemComponent)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.ItemComponentID })
                    .HasConstraintName("FK_Order.Tax.Item.Assessment_Order.Item.Component")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.ItemSurcharge)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.ItemSurchargeID })
                    .HasConstraintName("FK_Order.Tax.Item.Assessment_Order.Item.Surcharge")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.TaxGroup)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxGroupID })
                    .HasConstraintName("FK_Order.Tax.Item.Assessment_Accounting.Tax.Group")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<GLData>(entity =>
            {
                entity.ToTable("Accounting.GL.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.BID);
                entity.Property(e => e.ID);
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((8001))");
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(7)").HasDefaultValueSql($"SYSUTCDATETIME()").IsRequired().ValueGeneratedOnAddOrUpdate();

                //shadow properties
                entity.Property<DateTime>("ValidToDT")
                    .HasColumnType("DATETIME2(7)")
                    .IsRequired()
                    .HasDefaultValueSql("CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.AccountingDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.RecordedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.LocationID);
                entity.Property(e => e.GLAccountID);
                entity.Property(e => e.CurrencyType);
                entity.Property(e => e.Amount).HasColumnType("decimal(18, 4)").HasDefaultValue(0);
                entity.Property(e => e.Credit).HasColumnType("decimal(18, 4)").HasComputedColumnSql("(case when Amount < 0 then -Amount else NULL end)");
                entity.Property(e => e.Debit).HasColumnType("decimal(18, 4)").HasComputedColumnSql("(case when Amount > 0 then Amount else NULL end)");
                entity.Property(e => e.IsOffBS).HasDefaultValue(0);
                entity.Property(e => e.OrderItemID).HasColumnType("int");
                entity.Property(e => e.DestinationID).HasColumnType("int");
                entity.Property(e => e.ItemComponentID).HasColumnType("int");
                entity.Property(e => e.MaterialID).HasColumnType("int");
                entity.Property(e => e.LaborID).HasColumnType("int");
                entity.Property(e => e.MachineID).HasColumnType("smallint");
                entity.Property(e => e.PlaceID).HasColumnType("int");
                entity.Property(e => e.AssetID).HasColumnType("int");
                entity.Property(e => e.TaxItemID).HasColumnType("smallint");
                entity.Property(e => e.PaymentMethodID).HasColumnType("int");
                entity.Property(e => e.PaymentID).HasColumnType("int");
                entity.Property(e => e.AssemblyID).HasColumnType("int");
                entity.Property(e => e.MaterialID).HasColumnType("int");
                entity.Property(e => e.ItemSurchargeID).HasColumnType("int sparse null");
                entity.Property(e => e.TaxCodeID).HasColumnType("smallint sparse null");

                entity.HasOne(e => e.Location)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.LocationID })
                    .HasConstraintName("FK_GL.Data_Location.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.GLAccount)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.GLAccountID })
                    .HasConstraintName("FK_Accounting.GL.Data_Accounting.GL.Account_BID_GLAccountID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.CurrencyTypeNavigation)
                    .WithMany()
                    .HasForeignKey(t => t.CurrencyType)
                    .HasConstraintName("FK_GL.Data_enum.Accounting.CurrencyTypet")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.TaxGroup)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxGroupID })
                    .HasConstraintName("FK_GL.Data_Accounting.Tax.Group")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.GLActivity)
                    .WithMany(x => x.GL)
                    .HasForeignKey(t => new { t.BID, t.ActivityID })
                    .HasConstraintName("FK_Accounting.GL.Data_Activity.GL")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Company)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.CompanyID })
                    .HasConstraintName("FK_GL.Data_Company.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Order)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.OrderID })
                    .HasConstraintName("FK_GL.Data_Order.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.OrderItem)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.OrderItemID })
                    .HasConstraintName("FK_GL.Data_OrderItem.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.OrderDestination)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.DestinationID })
                    .HasConstraintName("FK_GL.Data_OrderDestination.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.OrderItemComponent)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.ItemComponentID })
                    .HasConstraintName("FK_GL.Data_OrderItemComponent")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Material)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.MaterialID })
                    .HasConstraintName("FK_GL.Data_Material.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Labor)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.LaborID })
                    .HasConstraintName("FK_GL.Data_Labor.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Machine)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.MachineID })
                    .HasConstraintName("FK_GL.Data_Machine.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Material)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.MaterialID })
                    .HasConstraintName("FK_GL.Data_Material.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Assembly)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.AssemblyID })
                    .HasConstraintName("FK_GL.Data_Assembly.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.TaxItem)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxItemID })
                    .HasConstraintName("FK_GL.Data_TaxItem.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.PaymentMethod)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.PaymentMethodID })
                    .HasConstraintName("FK_GL.Data_PaymentMethod.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.Payment)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.PaymentID })
                    .HasConstraintName("FK_GL.Data_Payment.Application")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.ItemSurcharge)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.ItemSurchargeID })
                    .HasConstraintName("FK_GL.Data_Order.Item.Surcharge")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.TaxCode)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxCodeID })
                    .HasConstraintName("FK_GL.Data_Accounting.Tax.Code")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasIndex(e => new { e.BID, e.ActivityID})
                    .HasName("IX_Accounting.GL.Data_Activity");
                entity.HasIndex(e => new { e.BID, e.CompanyID, e.AccountingDT })
                    .HasName("IX_Accounting.GL.Data_Company")
                    .HasFilter("CompanyID IS NOT NULL");
                entity.HasIndex(e => new { e.BID, e.OrderID, e.AccountingDT })
                    .HasName("IX_Accounting.GL.Data_Order")
                    .HasFilter("OrderID IS NOT NULL");
                entity.HasIndex(e => new { e.BID, e.AccountingDT, e.GLAccountID, e.Amount })
                    .HasName("IX_Accounting.GL.Data_AccountingDT");
                entity.HasIndex(e => new { e.BID, e.GLAccountID, e.AccountingDT, e.Amount })
                    .HasName("IX_Accounting.GL.Data_GLAccount");
            });

            modelBuilder.Entity<ActivityGlactivity>(entity =>
            {
                entity.ToTable("Activity.GLActivity");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((11003))");
                entity.Property(e => e.CreatedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive).HasComputedColumnSql("(isnull(CONVERT([bit],(1)),(1)))");
                entity.Property(e => e.ActivityType).HasComputedColumnSql("(isnull(CONVERT([tinyint],(50)),(50)))");
                entity.Property(e => e.StartDT).HasColumnType("datetime2(0)")
                    .HasComputedColumnSql("([CompletedDT])");
                entity.Property(e => e.EndDT).HasColumnType("datetime2(0)")
                    .HasComputedColumnSql("([CompletedDT])");
                entity.Property(e => e.Notes).IsUnicode(false)
                    .HasComputedColumnSql("(CONVERT([varchar](max),NULL))");
                entity.Property(e => e.TeamID).HasComputedColumnSql("CONVERT([int],NULL)");
                entity.Property(e => e.IsComplete).HasComputedColumnSql("(isnull(CONVERT([bit],(1)),(1)))");
                entity.Property(e => e.CompletedByID).HasColumnType("smallint");
                entity.Property(e => e.CompletedDT).HasColumnType("datetime2(0)");
                //.HasComputedColumnSql("([CompletedDT])");
                entity.Property(e => e.MetaData).HasColumnType("XML");

                entity.Ignore(e => e.GLIDs);

                entity.HasOne(e => e.GLEntryTypeNavigation)
                    .WithMany()
                    .HasForeignKey(t => t.GLEntryType)
                    .HasConstraintName("FK_Activity.GL_enum.Accouting.GLEntryType")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.CompletedBy)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.CompletedByID })
                    .HasConstraintName("FK_Activity.GL_Employee.Data")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.CompletedByContact)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.CompletedByContactID })
                    .HasConstraintName("FK_Activity.GL_Contact.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                //fix for EF462 "Unable to determine the relationship represented by navigation property 'ActivityGlactivity.Company' of type 'CompanyData'. Either manually configure the relationship, or ignore this property from the model."
                entity.HasOne(e => e.Company)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.CompanyID })
                    .HasPrincipalKey(company => new { company.BID, company.ID });

                entity.HasOne(e => e.Order)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.OrderID })
                    .HasConstraintName("FK_Activity.GLActivity_Order.Data_BID_OrderID");
            });

            modelBuilder.Entity<EnumGLEntryType>(entity =>
            {
                entity.ToTable("enum.Activity.GLEntryType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<BoardDefinitionData>(entity =>
            {
                entity.ToTable("Board.Definition.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14000))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.Description).HasColumnType("nvarchar(MAX)");
                entity.Property(e => e.DataType).HasColumnType("smallint");
                entity.Property(e => e.AverageRunDuration).HasComputedColumnSql("(case when [CummRunDurationSec]=(0.0) then CONVERT([float],NULL) else [CummRunDurationSec]/[CummRunCount] end)");
                entity.Property(e => e.ConditionFx).HasColumnType("varchar(MAX)");
                entity.Property(e => e.ConditionSQL).HasColumnType("varchar(MAX)");
                entity.Property(e => e.LastRunDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.LastRunDurationSec).HasColumnType("float");
                entity.Property(e => e.CummCounterDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.CummRunDurationSec).HasColumnType("float");
                entity.Property(e => e.AverageRunDuration).HasColumnType("float");

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive, e.DataType })
                    .HasName("IX_Board.Definition_Name");

                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(e => e.BID)
                    //.OnDelete(DeleteBehavior.NoAction)
                    .HasConstraintName("FK_Board.Definition_Business.Data");
            });

            modelBuilder.Entity<BoardEmployeeLink>(entity =>
            {
                entity.ToTable("Board.Employee.Link");

                entity.HasKey(e => new { e.BID, e.BoardID, e.EmployeeID, e.ModuleType });

                entity.Property(e => e.ModuleType).HasColumnType("smallint not null").HasDefaultValueSql("(1)");

                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.BoardID, e.ModuleType })
                    .HasName("IX_Board.Employee.Link_Employee");

                entity.HasOne(e => e.BoardDefinition)
                    .WithMany(e => e.EmployeeLinks)
                    .HasForeignKey(d => new { d.BID, d.BoardID })
                    .HasConstraintName("FK_Board.Employee.Link_Board.Data");

                entity.HasOne(e => e.Employee)
                    .WithMany(e => e.BoardLinks)
                    .HasForeignKey(d => new { d.BID, d.EmployeeID })
                    .HasConstraintName("FK_Board.Employee.Link_Employee.Data");

                entity.HasOne(e => e.EnumModule)
                    .WithMany()
                    .HasForeignKey(d => new { d.ModuleType })
                    .HasConstraintName("FK_Board.Employee.Link_enum.Module");
            });

            modelBuilder.Entity<BoardModuleLink>(entity =>
            {
                entity.ToTable("Board.Module.Link");

                entity.HasKey(e => new { e.BID, e.BoardID, e.ModuleType });

                entity.Property(e => e.ModuleType).HasColumnType("smallint");

                entity.HasIndex(e => new { e.BID, e.ModuleType, e.BoardID })
                    .HasName("IX_Board.Module.Link_Module");

                entity.HasOne(e => e.BoardDefinition)
                    .WithMany(e => e.ModuleLinks)
                    .HasForeignKey(d => new { d.BID, d.BoardID })
                    .HasConstraintName("FK_Board.Module.Link_Board.Definition");
                entity.HasOne(e => e.EnumModule)
                    .WithMany()
                    .HasForeignKey(d => new { d.ModuleType })
                    .HasConstraintName("FK_Board.Module.Link_enum.Module");
            });

            modelBuilder.Entity<BoardRoleLink>(entity =>
            {
                entity.ToTable("Board.Role.Link");

                entity.HasKey(e => new { e.BID, e.BoardID, e.RoleID });

                entity.HasIndex(e => new { e.BID, e.RoleID, e.BoardID })
                    .HasName("IX_Board.Role.Link_Role");

                entity.HasOne(e => e.BoardDefinition)
                    .WithMany(e => e.RoleLinks)
                    .HasForeignKey(d => new { d.BID, d.BoardID })
                    .HasConstraintName("FK_Board.Role.Link_Board.Definition");

                entity.HasOne(e => e.Role)
                    .WithMany(e => e.BoardLinks)
                    .HasForeignKey(d => new { d.BID, d.RoleID })
                    .HasConstraintName("FK_Board.Role.Link_Link.Data");
            });

            modelBuilder.Entity<BoardView>(entity =>
            {
                entity.ToTable("Board.View");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14040))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsListView).HasComputedColumnSql("(~[IsTileView])");
                entity.Property(e => e.DataType).HasColumnType("smallint");
                entity.Property(e => e.GroupBy).HasColumnType("VARCHAR(MAX)");
                entity.Property(e => e.Columns).HasColumnType("NVARCHAR(MAX)");
                entity.Property(e => e.SortOptions).HasColumnType("NVARCHAR(MAX)");
                entity.Property(e => e.CustomLayout).HasColumnType("XML");

                entity.Ignore(e => e.CustomLayoutJson);
                entity.Ignore(e => e.Boards);

                entity.HasIndex(e => new { e.BID, e.DataType, e.IsActive, e.Name }).HasName("IX_Board.View_DataType");
                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive, e.DataType }).HasName("IX_Board.View_Name");
            });

            modelBuilder.Entity<BoardViewLink>(entity =>
            {
                entity.ToTable("Board.View.Link");

                entity.HasKey(e => new { e.BID, e.BoardID, e.ViewID });

                entity.Property(e => e.IsPrimary).HasComputedColumnSql("(case when [SortIndex]=(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

                entity.HasOne(e => e.BoardDefinitionData)
                    .WithMany(e => e.BoardViewLinks)
                    .HasForeignKey(d => new { d.BID, d.BoardID })
                    .HasConstraintName("FK_Board.View.Link_Board.Definition");
                entity.HasOne(e => e.BoardView)
                    .WithMany(e => e.BoardViewLinks)
                    .HasForeignKey(d => new { d.BID, d.ViewID })
                    .HasConstraintName("FK_Board.View.Link_Link.Definition");
            });

            modelBuilder.Entity<BusinessTimeZoneLink>(entity => 
            {
                entity.HasKey(e => new { e.BID, e.TimeZoneID });
            });

            modelBuilder.Entity<BusinessData>(entity =>
            {
                entity.ToTable("Business.Data");

                entity.HasIndex(e => new { e.AssociationType, e.Name })
                    .HasName("IX_Business.Data_Association");

                entity.HasIndex(e => new { e.Name, e.AssociationType })
                    .HasName("IX_Business.Data_Business");

                entity.Property(e => e.BID).ValueGeneratedNever();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1000))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ID).HasComputedColumnSql("(isnull(CONVERT([smallint],(1)),(1)))");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");

                entity.HasOne(d => d.BillingEmployee)
                    .WithMany(p => p.BillingBusinesses)
                    .HasForeignKey(d => new { d.BID, d.BillingEmployeeID })
                    .HasConstraintName("FK_Business.Data_Employee.Data1");
                entity.HasOne(d => d.OwnerEmployee)
                    .WithMany(p => p.OwnedBusinesses)
                    .HasForeignKey(d => new { d.BID, d.OwnerEmployeeID })
                    .HasConstraintName("FK_Business.Data_Employee.Data");
            });

            modelBuilder.Entity<BusinessLocator>(entity =>
            {
                entity.ToTable("Business.Locator");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1003))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");

                entity.HasIndex(e => e.MetaDataXML)
                    .HasName("XML_IX_Business.Locator");
                entity.HasIndex(e => new { e.BID, e.ParentID })
                    .HasName("IX_Business.Locator_Parent");
                entity.HasIndex(e => new { e.BID, e.LocatorType, e.LocatorSubType })
                    .HasName("IX_Business.Locator_LoactorType");

                MapLocator<BusinessLocator, short, BusinessData>(entity);

                entity.HasOne(d => d.LocatorTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.LocatorType)
                    .HasConstraintName("FK_Business.Locator_enum.LocatorType");
                entity.HasOne(d => d.LocatorSubTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => new { d.LocatorType, d.LocatorSubType })
                    .HasConstraintName("FK_Business.Locator_enum.LocatorSubType");
                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.BusinessLocators)
                    .HasForeignKey(d => new { d.BID })
                    .HasConstraintName("FK_Business.Locator_Business_Data_ParentID");
            });

            modelBuilder.Entity<CampaignData>(entity =>
            {
                entity.ToTable("Campaign.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((9100))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.StartDate).HasColumnType("date");
                entity.Property(e => e.EndDate).HasColumnType("date");
                entity.Property(e => e.Budget).HasColumnType("money");
                entity.Property(e => e.ActualCost).HasColumnType("money");
                entity.Property(e => e.ExceptedRevenue).HasColumnType("money");
                entity.Property(e => e.ActualRevenue).HasColumnType("money");
                entity.Property(e => e.MetaData).HasColumnType("xml");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Campaigns)
                    .HasForeignKey(d => d.BID)
                    .HasConstraintName("FK_Campaign.Data_Business.Data");
                entity.HasOne<EnumCampaignType>(d => d.CampaignTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.CampaignType)
                    .HasConstraintName("FK_Campaign.Data_enum.Campaign.Type");
                entity.HasOne(d => d.EmployeeTeam)
                    .WithMany(p => p.Campaigns)
                    .HasForeignKey(d => new { d.BID, d.TeamID })
                    .HasConstraintName("FK_Campaign.Data_Employee.Team");
            });

            modelBuilder.Entity<CompanyCustomData>(entity =>
            {
                entity.ToTable("Company.Custom.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((2001))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.AppliesToClassTypeID).ValueGeneratedOnAddOrUpdate()
                    .HasDefaultValueSql("((2000))");
                entity.Property(e => e.DataXML).HasColumnType("xml");

                entity.HasIndex(e => e.DataXML).HasName("XML_IX_Company.Custom.Data");

                //fix for EF462 Unable to determine the relationship represented by navigation property 'CompanyCustomData.Company' of type 'CompanyData'. Either manually configure the relationship, or ignore this property from the model.
                entity.HasOne(e => e.Company)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.ID })
                    .HasPrincipalKey(company => new { company.BID, company.ID });
            });

            modelBuilder.Entity<CompanyData>(entity =>
            {
                entity.ToTable("Company.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((2000))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.HasCreditAccount).HasComputedColumnSql("(case when [CreditLimit] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.IsClient).HasComputedColumnSql("(case when ([StatusID]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsLead).HasComputedColumnSql("(case when ([StatusID]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsPersonal).HasComputedColumnSql("(case when ([StatusID]&(16))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsProspect).HasComputedColumnSql("(case when ([StatusID]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsVendor).HasComputedColumnSql("(case when ([StatusID]&(8))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.LocationID).HasDefaultValueSql("((1))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.StatusID).HasDefaultValueSql("((0))");
                entity.Property(e => e.StatusText)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(concat(case when [StatusID]=(1) then 'Lead'  end,case when ([StatusID]&(2))<>(0) then 'Prospect'  end,case when ([StatusID]&(4))<>(0) then 'Client'  end,case when ([StatusID]&(8))<>(4) then 'Vendor'  end))");
                entity.Property(e => e.RefundableCredit).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.NonRefundableCredit).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.CreditLimit).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.CreditApprovalDate).HasColumnType("date");

                entity.Ignore(e => e.CFValuesJSON);
                entity.Ignore(e => e.CFValues);

                entity.HasIndex(e => new { e.BID, e.ParentID })
                    .HasName("IX_Company.Data_Parent");
                entity.HasIndex(e => new { e.BID, e.Name, e.StatusID, e.IsActive })
                    .HasName("IX_Company.Data_Name");
                entity.HasIndex(e => new { e.BID, e.StatusID, e.IsActive, e.Name })
                    .HasName("IX_Company.Data_Status");
                entity.HasIndex(e => new { e.BID, e.TeamID, e.Name, e.IsActive })
                    .HasName("IX_Company.Data_Team");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Companies)
                    .HasForeignKey(d => d.BID)
                    .HasConstraintName("FK_Company.Data_Business.Data")
                    .HasPrincipalKey(business => business.BID);
                entity.HasOne<EnumCrmCompanyStatus>(d => d.Status)
                    .WithMany()
                    .HasForeignKey(d => d.StatusID)
                    .HasConstraintName("FK_Company.Data_enum.CRM.Company.Status");
                entity.HasOne<EnumTimeZone>(d => d.TimeZone)
                    .WithMany()
                    .HasForeignKey(d => d.TimeZoneID)
                    .HasConstraintName("FK_Company.Data_enum.TimeZone");
                entity.HasOne(d => d.CrmIndustry)
                    .WithMany(p => p.Companies)
                    .HasForeignKey(d => new { d.BID, d.IndustryID })
                    .HasConstraintName("FK_Company.Data_CRM.Industry");
                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Companies)
                    .HasForeignKey(d => new { d.BID, d.LocationID })
                    .HasConstraintName("FK_Company.Data_Location.Data");
                entity.HasOne(d => d.ParentCompany)
                    .WithMany(p => p.ChildCompanies)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    .HasConstraintName("FK_Company.Data_Company.Data");
                entity.HasOne(d => d.CrmOrigin)
                    .WithMany(p => p.Companies)
                    .HasForeignKey(d => new { d.BID, d.OriginID })
                    .HasConstraintName("FK_Company.Data_CRM.Origin");
                entity.HasOne(d => d.TaxGroup)
                    .WithMany(p => p.Companies)
                    .HasForeignKey(d => new { d.BID, d.TaxGroupID })
                    .HasConstraintName("FK_Company.Data_Accounting.Tax.Group");
                entity.HasOne(d => d.EmployeeTeam)
                    .WithMany(p => p.Companies)
                    .HasForeignKey(d => new { d.BID, d.TeamID })
                    .HasConstraintName("FK_Company.Data_Employee.Team");
                entity.HasOne(d => d.DefaultPaymentTerms).WithMany()
                    .HasForeignKey(e => new { e.BID, e.PaymentTermID })
                    .HasConstraintName("FK_Company.Data_PaymentTerm");
                entity.HasOne(d => d.VendorPaymentTerms).WithMany()
                    .HasForeignKey(e => new { e.BID, e.VendorPaymentTermID })
                    .HasConstraintName("FK_Company.Data_VendorPaymentTerm");
                entity.HasOne<FlatListItem>(x => x.TaxExemptReason)
                    .WithMany()
                    .HasForeignKey(d => new { d.BID, d.TaxExemptReasonID })
                    .HasConstraintName("FK_Company.Data_TaxExemptReason");
                entity.HasOne<FlatListItem>(x => x.PricingTier)
                    .WithMany()
                    .HasForeignKey(d => new { d.BID, d.PricingTierID })
                    .HasConstraintName("FK_Company.Data_PricingTier");

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });

                //entity.HasOne(d => d.ParentCompany)
                //    .WithMany(p => p.SimpleContacts);
            });

            modelBuilder.Entity<CompanyLocator>(entity =>
            {
                entity.ToTable("Company.Locator");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((2003))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");

                entity.HasIndex(e => e.MetaDataXML)
                    .HasName("XML_IX_Company.Locator");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.LocatorType })
                    .HasName("IX_Company.Locator_Parent");
                entity.HasIndex(e => new { e.BID, e.LocatorType, e.LocatorSubType, e.ParentID })
                    .HasName("IX_Company.Locator_Locator");

                MapLocator<CompanyLocator, int, CompanyData>(entity);

                entity.HasOne(d => d.LocatorTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.LocatorType)
                    .HasConstraintName("FK_Company.Locator_enum.LocatorType");
                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.CompanyLocators)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    .HasConstraintName("FK_Company.Locator_Company.Data");
                entity.HasOne(d => d.LocatorSubTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => new { d.LocatorType, d.LocatorSubType })
                    .HasConstraintName("FK_Company.Locator_enum.LocatorSubType");
            });

            modelBuilder.Entity<ContactData>(entity =>
            {
                entity.ToTable("Contact.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.BirthDate).HasColumnType("date")
                    .HasComputedColumnSql("(datefromparts([BirthYear],[BirthMonth],[BirthDayOfMonth]))");
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((3000))");
                entity.Property(e => e.LongName).HasComputedColumnSql("(ltrim(rtrim(concat([Prefix]+' ',[First]+' ',left([Middle],(1))+'. ',[Last]+' ',[Suffix]))))");
                entity.Property(e => e.ShortName).HasComputedColumnSql("(ltrim(rtrim(concat(coalesce([First],[NickName])+' ',[Last]))))");
                entity.Property(e => e.StatusID).HasDefaultValueSql("((1))");

                entity.HasIndex(e => new { e.BID, e.Last })
                    .HasName("IX_Contact.Data_Last");
                entity.HasIndex(e => new { e.BID, e.ShortName })
                    .HasName("IX_Contact.Data_Short");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.BID)
                    .HasConstraintName("FK_Contact.Data_Business.Data");
                entity.HasOne<EnumTimeZone>(d => d.TimeZone)
                    .WithMany()
                    .HasForeignKey(d => d.TimeZoneID)
                    .HasConstraintName("FK_Contact.Data_enum.TimeZone");                
                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => new { d.BID, d.LocationID })
                    .HasConstraintName("FK_Contact.Data_Location.Data");
                entity.HasOne(c => c.Status)
                    .WithMany()
                    .HasForeignKey(c => c.StatusID)
                    .HasConstraintName("FK_Contact.Data_enum.CRM.Company.Status");

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<ContactCustomData>(entity =>
            {
                entity.ToTable("Contact.Custom.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((3001))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.AppliesToClassTypeID).ValueGeneratedOnAddOrUpdate()
                    .HasDefaultValueSql("((3000))");
                entity.Property(e => e.DataXML).HasColumnType("xml");

                entity.HasIndex(e => e.DataXML).HasName("XML_IX_Contact.Custom.Data");

                entity.HasOne(e => e.Contact).WithMany().HasForeignKey(e => new { e.BID, e.ID });
            });

            modelBuilder.Entity<ContactLocator>(entity =>
            {
                entity.ToTable("Contact.Locator");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((3003))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");

                entity.HasIndex(e => new { e.BID, e.ParentID })
                    .HasName("IX_Contact.Locator_Parent");
                entity.HasIndex(e => new { e.BID, e.LocatorType, e.LocatorSubType, e.ParentID })
                    .HasName("IX_Contact.Locator");

                MapLocator<ContactLocator, int, ContactData>(entity);

                entity.HasOne(d => d.LocatorTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.LocatorType)
                    .HasConstraintName("FK_Contact.Locator_enum.LocatorType");
                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.ContactLocators)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    .HasConstraintName("FK_Contact.Locator_Contact.Data");
                entity.HasOne(d => d.LocatorSubTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => new { d.LocatorType, d.LocatorSubType })
                    .HasConstraintName("FK_Contact.Locator_enum.LocatorSubType");
            });

            modelBuilder.Entity<EnumContactRoles>(entity =>
            {
                entity.ToTable("enum.Contact.Role");
                entity.HasKey(e => e.ID);
                entity.Property(e => e.Name).HasColumnType("varchar(100) not null");
            });

            modelBuilder.Entity<CompanyContactLink>(entity =>
            {
                entity.ToTable("Company.Contact.Link");
                entity.HasKey(e => new { e.BID, e.CompanyID, e.ContactID });

                entity.Property(e => e.Roles).HasDefaultValue(ContactRole.Inactive);
                entity.Property(e => e.IsActive).HasComputedColumnSql("(CONVERT([bit],[Roles]))");
                entity.Property(e => e.IsPrimary).HasComputedColumnSql("(CONVERT([bit],[Roles]&(1)))");
                entity.Property(e => e.IsBilling).HasComputedColumnSql("(CONVERT([bit],[Roles]&(2)))");

                entity.HasIndex(e => new { e.BID, e.ContactID, e.CompanyID, e.Roles })
                    .HasName("IX_Company.Contact.Link_Contact");

                entity.HasOne(e => e.Company)
                    .WithMany(c => c.CompanyContactLinks)
                    .HasForeignKey(e => new { e.BID, e.CompanyID })
                    .HasConstraintName("FK_Company.Contact.Link_Company.Data");
                entity.HasOne(e => e.Contact)
                    .WithMany(c => c.CompanyContactLinks)
                    .HasForeignKey(e => new { e.BID, e.ContactID })
                    .HasConstraintName("FK_Company.Contact.Link_Contact.Data");
                entity.HasOne(e => e.ContactRole)
                    .WithMany()
                    .HasForeignKey(e => e.Roles)
                    .HasConstraintName("FK_Company.Contact.Link_enum.Contact.Role");
            });

            modelBuilder.Entity<CrmIndustry>(entity =>
            {
                entity.ToTable("CRM.Industry");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((2011))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.IsTopLevel).HasComputedColumnSql("(case when [ParentID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

                entity.Ignore(e => e.IsLocked);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_CRM.Industry_Name");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.Name })
                    .HasName("IX_CRM.Industry_Parent");

                entity.HasOne(d => d.ParentIndustry)
                    .WithMany(p => p.ChildIndustries)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    .HasConstraintName("FK_CRM.Industry_CRM.Industry");
            });

            modelBuilder.Entity<CrmOrigin>(entity =>
            {
                entity.ToTable("CRM.Origin");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((2012))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.IsTopLevel).HasComputedColumnSql("(case when [ParentID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

                entity.Ignore(e => e.IsLocked);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_CRM.Origin_Name");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.Name })
                    .HasName("IX_CRM.Origin_Parent");

                entity.HasOne(d => d.ParentOrigin)
                    .WithMany(d => d.ChildOrigins)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    .HasConstraintName("FK_CRM.Origin_CRM.Origin");
            });

            modelBuilder.Entity<CustomFieldDefinition>(entity =>
            {
                entity.ToTable("CustomField.Definition");

                /* END-11708 Fix on => The property 'BID' on entity type 'CustomFieldDefinition' is defined to be read-only after it has been saved, 
                 * but its value has been modified or marked as modified.   */
                entity.Property(e => e.BID).Metadata.SetAfterSaveBehavior(Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Ignore);

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"(({ClassType.CustomFieldDefinition.ID()}))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.AppliesToClass).ValueGeneratedOnAddOrUpdate()
                    .HasComputedColumnSql("(case when [AppliesToID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.Description).HasColumnType("VARCHAR(MAX)");
                entity.Property(e => e.DataType).HasColumnType("smallint");
                //entity.Property(e => e.InputType).HasColumnType("tinyint");
                //entity.Property(e => e.DisplayFormatType).HasColumnType("tinyint");
                entity.Property(e => e.ElementType).HasColumnType("tinyint").IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.DisplayFormatString).HasColumnType("VARCHAR(32)");
                //entity.Property(e => e.MetaData).HasColumnType("xml");
                entity.Property(e => e.AllowCustomValue).HasColumnType("bit");
                entity.Property(e => e.AllowDecimals).HasColumnType("bit");
                entity.Property(e => e.AllowMultiSelect).HasColumnType("bit");
                entity.Property(e => e.AllowNotSetOption).HasColumnType("bit");
                entity.Property(e => e.AllowTextFormatting).HasColumnType("bit");
                entity.Property(e => e.AllowUserTypedOptions).HasColumnType("bit");
                entity.Property(e => e.AltText).HasColumnType("nvarchar(255)");
                entity.Property(e => e.DecimalPlaces).HasColumnType("tinyint");
                entity.Property(e => e.DefaultOption).HasColumnType("nvarchar(1024)");
                entity.Property(e => e.DefaultValue).HasColumnType("nvarchar(max)");
                entity.Property(e => e.DisplayFormat).HasColumnType("nvarchar(255)");
                entity.Property(e => e.DisplaySpinner).HasColumnType("bit");
                entity.Property(e => e.DisplayType).HasColumnType("tinyint");
                entity.Property(e => e.ElementUseCount).HasColumnType("tinyint").IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.GroupOptionsByCategory).HasColumnType("bit");
                entity.Property(e => e.GroupType).HasColumnType("tinyint");
                entity.Property(e => e.HasMaxLength).HasColumnType("smallint");
                entity.Property(e => e.IsDisabled).HasColumnType("bit").IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.LabelType).HasColumnType("tinyint");
                entity.Property(e => e.ListDataType).HasColumnType("smallint");
                entity.Property(e => e.ListOptions).HasColumnType("nvarchar(2048)");
                entity.Property(e => e.ListValuesJSON).HasColumnType("nvarchar(max)");
                entity.Property(e => e.NumberOfDecimals).HasColumnType("tinyint");
                entity.Property(e => e.OptionOneLabel).HasColumnType("nvarchar(255)");
                entity.Property(e => e.OptionTwoLabel).HasColumnType("nvarchar(255)");
                entity.Property(e => e.PickerType).HasColumnType("tinyint");
                entity.Property(e => e.PresetTime).HasColumnType("bit");
                entity.Property(e => e.SpinnerIncrement).HasColumnType("decimal(6, 3)");
                entity.Property(e => e.Time).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.ToggleOptions).HasColumnType("bit");
                entity.Property(e => e.Tooltip).HasColumnType("nvarchar(max)");
                entity.Property(e => e.UnitID).HasColumnType("tinyint");
                entity.Property(e => e.UnitType).HasColumnType("tinyint");
                entity.Property(e => e.CustomUnitText).HasColumnType("NVARCHAR(255)");
                entity.Ignore(e => e.TempID);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive, e.AppliesToClassTypeID, e.AppliesToID })
                    .HasName("IX_CustomField.Definition_AppliesTo");

                entity.HasOne(e => e.Business)
                    .WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_CustomField.Definition_BID");


                entity.HasOne(e => e.EnumCustomFieldDataType)
                    .WithMany()
                    .HasForeignKey(e => new { e.DataType })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CustomField.Definition_DataType");

                entity.HasOne<EnumCustomFieldNumberDisplayType>()
                    .WithMany()
                    .HasForeignKey(e => e.DisplayType)
                    .HasConstraintName("FK_CustomField.Definition_NumberDisplayType")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<CustomFieldLayoutDefinition>(entity =>
            {
                entity.ToTable("CustomField.Layout.Definition");

                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.Name).HasColumnType("NVARCHAR(100)");
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)")
                    .HasDefaultValueSql("(getdate())");
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"(({ClassType.CustomFieldLayoutDefinition.ID()}))");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.IsSystem);
                entity.Property(e => e.AppliesToClassTypeID);
                entity.Property(e => e.IsAllTab);
                entity.Property(e => e.IsSubTab);
                entity.Property(e => e.SortIndex);

                entity.HasIndex(e => new { e.BID, e.AppliesToClassTypeID, e.SortIndex, e.IsSubTab })
                    .HasName("IX_CustomField.Layout.Definition_Type");
            });

            modelBuilder.Entity<CustomFieldLayoutElement>(entity =>
            {
                entity.ToTable("CustomField.Layout.Element");

                /* END-11708: Fix on => The property 'BID' on entity type 'CustomFieldLayoutElement' is defined to be read-only after it has been saved, 
                 * but its value has been modified or marked as modified. */
                entity.Property(e => e.BID).Metadata.SetAfterSaveBehavior(Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Ignore);

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"(({ClassType.CustomFieldLayoutElement.ID()}))");
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)")
                    .HasDefaultValueSql("(getdate())");
                entity.Property(e => e.Name).HasColumnType("NVARCHAR(100)");
                entity.Property(e => e.SortIndex);
                entity.Property(e => e.DefinitionID);
                entity.Property(e => e.DataType);
                entity.Property(e => e.ParentID);
                entity.Property(e => e.Column).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.ColumnsWide).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.ElementType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.IsDisabled).HasColumnType("bit").IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.IsReadOnly).HasColumnType("bit").IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.Row).HasColumnType("tinyint").IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.Tooltip).HasColumnType("nvarchar(512)");
                entity.Property(e => e.LayoutID).HasColumnType("smallint").IsRequired().HasDefaultValueSql("0");
                entity.Ignore(e => e.TempDefinitionID);

                entity.HasOne<CustomFieldLayoutElement>()
                    .WithMany(e => e.Elements)
                    .HasForeignKey(e => new { e.BID, e.ParentID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CustomField.Layout.Element_CustomField.Layout.Element");

                entity.HasOne<CustomFieldLayoutDefinition>()
                    .WithMany(e => e.Elements)
                    .HasForeignKey(e => new { e.BID, e.LayoutID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_CustomField.Layout.Element_CustomField.Layout.Definition");

                entity.HasOne<CustomFieldDefinition>(e => e.Definition)
                    .WithMany(t => t.Elements)
                    .HasForeignKey(c => new { c.BID, c.DefinitionID })
                    .HasConstraintName("FK_CustomField.Layout.Element_CustomField.Definition");
            });

            modelBuilder.Entity<CustomFieldOtherData>(entity =>
            {
                entity.ToTable("CustomField.Other.Data");

                entity.HasKey(e => new { e.BID, e.AppliesToClassTypeID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((15026))");
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.AppliesToClassTypeID);
                entity.Property(e => e.DataXML).HasColumnType("xml");

                entity.HasIndex(e => e.DataXML).HasName("XML_IX_CustomField.Other.Data");
            });

            modelBuilder.Entity<EmployeeData>(entity =>
            {
                entity.ToTable("Employee.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((5000))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.LocationID).HasDefaultValueSql("((1))");
                entity.Property(e => e.LongName).HasComputedColumnSql("(ltrim(rtrim(concat([Prefix]+' ',[First]+' ',left([Middle],(1))+'. ',[Last]+' ',[Suffix]))))");
                entity.Property(e => e.ShortName).HasComputedColumnSql("(ltrim(rtrim(concat(coalesce([First],[NickName])+' ',[Last]))))");
                entity.Property(e => e.HireDate).HasColumnType("date");
                entity.Property(e => e.ReleaseDate).HasColumnType("date");
                entity.Property(e => e.BirthDate).HasColumnType("date")
                    .HasComputedColumnSql("(datefromparts([BirthYear],[BirthMonth],[BirthDayOfMonth]))");
                entity.Property(e => e.DefaultModule).HasColumnType("smallint");

                entity.HasIndex(e => new { e.BID, e.Last, e.First })
                    .HasName("IX_Employee.Data_LastName");
                entity.HasIndex(e => new { e.BID, e.LocationID })
                    .HasName("IX_Employee.DataLocation");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(d => d.BID)
                    .HasConstraintName("FK_Employee.Data_Business.Data");
                entity.HasOne(d => d.ReportsTo)
                    .WithMany(p => p.EmployeesReportingToMe)
                    .HasForeignKey(d => new { d.BID, d.ReportsToID })
                    .HasConstraintName("FK_Employee.Data_ReportsToID");
                entity.HasOne(t => t.Location)
                    .WithMany(p => p.Employees)
                    .HasForeignKey(t => new { t.BID, t.LocationID })
                    .HasConstraintName("FK_Employee.Data_LocationID");
                entity.HasOne(e => e.DefaultEmailAccount)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.DefaultEmailAccountID })
                    .HasConstraintName("FK_Employee.Data_DefaultEmailAccountID");
                entity.HasOne(e => e.DefaultEnumModule)
                    .WithMany()
                    .HasForeignKey(d => new { d.DefaultModule })
                    .HasConstraintName("FK_Employee.Data_enum.Module");

                entity.HasMany(x => x.CustomDataValues)
                    .WithOne()
                    .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<EmployeeLocator>(entity =>
            {
                entity.ToTable("Employee.Locator");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((5003))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");

                entity.HasIndex(e => e.MetaDataXML)
                    .HasName("XML_IX_Employee.Locator");
                entity.HasIndex(e => new { e.BID, e.LocatorType, e.LocatorSubType })
                    .HasName("IX_Employee.Locator_LocatorType");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.LocatorType })
                    .HasName("IX_Employee.Locator_parent");

                MapLocator<EmployeeLocator, short, EmployeeData>(entity);

                entity.HasOne(d => d.LocatorTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.LocatorType)
                    .HasConstraintName("FK_Employee.Locator_enum.LocatorType");
                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.EmployeeLocators)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    .HasConstraintName("FK_Employee.Locator_Employee.Data");
                entity.HasOne(d => d.LocatorSubTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => new { d.LocatorType, d.LocatorSubType })
                    .HasConstraintName("FK_Employee.Locator_enum.LocatorSubType");
            });

            modelBuilder.Entity<EmployeeTeam>(entity =>
            {
                entity.ToTable("Employee.Team");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((5020))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);

                entity.Ignore(e => e.SimpleLocations);
                entity.Ignore(e => e.EmailAccounts);
            });

            modelBuilder.Entity<EmployeeTeamLink>(entity =>
            {
                entity.ToTable("Employee.TeamLink");

                entity.HasKey(e => new { e.BID, e.EmployeeID, e.TeamID, e.RoleID });

                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.TeamID, e.RoleID })
                    .HasName("IX_Employee.TeamLink_Employee");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.EmployeeTeamLinks)
                    .HasForeignKey(d => new { d.BID, d.EmployeeID })
                    .HasConstraintName("FK_Employee.TeamLink_Employee.Data");
                entity.HasOne(d => d.EmployeeTeam)
                    .WithMany(p => p.EmployeeTeamLinks)
                    .HasForeignKey(d => new { d.BID, d.TeamID })
                    .HasConstraintName("FK_Employee.TeamLink_Employee.Team");
                entity.HasOne(d => d.Role)
                    .WithMany()
                    .HasConstraintName("FK_Employee.TeamLink_Employee.Role")
                    .HasForeignKey(t => new { t.BID, t.RoleID })
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<EmployeeTeamLocationLink>(entity =>
            {
                entity.ToTable("Employee.Team.LocationLink");

                entity.HasKey(e => new { e.BID, e.TeamID, e.LocationID });

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.EmployeeTeamLocationLinks)
                    .HasForeignKey(d => new { d.BID, d.LocationID })
                    .HasConstraintName("FK_Employee.Team.LocationLink_Location.Data");
                entity.HasOne(d => d.EmployeeTeam)
                    .WithMany(p => p.EmployeeTeamLocationLinks)
                    .HasForeignKey(d => new { d.BID, d.TeamID })
                    .HasConstraintName("FK_Employee.Team.LocationLink_Employee.Team");
            });

            modelBuilder.Entity<EnumAccountingCurrencyType>(entity =>
            {
                entity.ToTable("enum.Accounting.CurrencyType");

                entity.HasKey(t => t.ID);

                entity.Property(e => e.Name).HasColumnType("VARCHAR(100)");
                entity.Property(e => e.Code).HasColumnType("VARCHAR(3)");
                entity.Property(e => e.Symbol).HasColumnType("NVARCHAR(1)");
            });

            modelBuilder.Entity<EnumActivityType>(entity =>
            {
                entity.ToTable("enum.ActivityType");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.RecordTypeText)
                    .IsUnicode(false)
                    .HasComputedColumnSql("(isnull(case [RecordType] when (1) then 'Action' when (2) then 'Event' when (3) then 'GL Activity' else 'Generic Actvity' end,''))");
            });

            modelBuilder.Entity<EnumCampaignStage>(entity =>
            {
                entity.ToTable("enum.Campaign.Stage");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumCampaignType>(entity =>
            {
                entity.ToTable("enum.Campaign.Type");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumClassType>(entity =>
            {
                entity.ToTable("enum.ClassType");

                entity.Property(e => e.ID).ValueGeneratedNever();
                entity.Property(e => e.Description).IsUnicode(false);
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.TableName).IsUnicode(false);
                entity.Property(e => e.RecordNameFromObjectIDSQL).IsUnicode(false);
            });

            modelBuilder.Entity<EnumCrmCompanyStatus>(entity =>
            {
                entity.ToTable("enum.CRM.Company.Status");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumDataType>(entity =>
            {
                entity.ToTable("enum.DataType");

                entity.Property(e => e.ID).HasColumnType("smallint");
                entity.Property(e => e.Name).IsUnicode(false);
            });
            
            modelBuilder.Entity<EnumCustomFieldNumberDisplayType>(entity =>
            {
                entity.ToTable("enum.CustomField.NumberDisplayType");

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumFlatListType>(entity =>
            {
                entity.ToTable("enum.List.FlatListType");

                entity.HasKey(t => t.ID).HasName("PK_enum.List.ReasonType");

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).HasColumnType("varchar(50)");
            });

            modelBuilder.Entity<EnumLeadStage>(entity =>
            {
                entity.ToTable("enum.Lead.Stage");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumLocatorSubType>(entity =>
            {
                entity.ToTable("enum.Locator.SubType");

                entity.HasKey(e => new { e.LocatorType, e.ID });

                entity.Property(e => e.LinkFormatString).IsUnicode(false);
                entity.Property(e => e.Name).IsUnicode(false);

                entity.HasOne<EnumLocatorType>()
                    .WithMany()
                    .HasForeignKey(d => d.LocatorType)
                    .HasConstraintName("FK_enum.Locator.SubType_enum.Locator.Type")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<EnumLocatorType>(entity =>
            {
                entity.ToTable("enum.Locator.Type");

                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.PossibilityRegEx).IsUnicode(false);
                entity.Property(e => e.ValidityRegEx).IsUnicode(false);
            });

            modelBuilder.Entity<EnumModule>(entity =>
            {
                entity.ToTable("enum.Module");

                entity.Property(e => e.ID).HasColumnType("smallint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumOpportunityStage>(entity =>
            {
                entity.ToTable("enum.Opportunity.Stage");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumTimeZone>(entity =>
            {
                entity.ToTable("enum.TimeZone");

                entity.Property(e => e.ID).ValueGeneratedNever();
                entity.Property(e => e.IsCommon).HasDefaultValueSql("((1))");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.StandardName).IsUnicode(false);
                entity.Property(e => e.UTCOffset).HasColumnType("decimal(6, 2)");

                entity.HasIndex(e => e.Name)
                    .HasName("IX_enum.TimeZone_DisplayName");
                entity.HasIndex(e => e.StandardName);
            });

            modelBuilder.Entity<EnumUserAccessType>(entity =>
            {
                entity.ToTable("enum.User.Access.Type");

                entity.HasKey(e => new { e.ID }).HasName("PK_enum.User.Access.Type");

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).HasColumnType("varchar(100)");
            });

            modelBuilder.Entity<EnumMaterialConsumptionMethod>(entity =>
            {
                entity.ToTable("enum.Part.Material.Consumption.Method");

                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Material.Consumption.Method");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumMaterialCostingMethod>(entity =>
            {
                entity.ToTable("enum.Part.Material.Costing.Method");

                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Material.Costing.Method");

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumMaterialPhysicalType>(entity =>
            {
                entity.ToTable("enum.Part.Material.Physical.Type");

                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Material.Physical.Type");

                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.Dimensions).HasDefaultValueSql("(0)");
                entity.Property(e => e.WidthRequired);
                entity.Property(e => e.LengthRequired);
                entity.Property(e => e.HeightRequired).HasColumnName("HeightRequired");
                entity.Property(e => e.WeightRequired);
            });

            modelBuilder.Entity<FlatListItem>(entity =>
            {
                entity.ToTable("List.FlatList.Data");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_List.FlatList.Data");
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1902))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.FlatListType).HasColumnType("tinyint");
                entity.Property(e => e.SortIndex).HasColumnType("smallint");

                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(t => t.BID)
                    .HasConstraintName("FK_List.FlatList.Data_BID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumFlatListType>()
                    .WithMany()
                    .HasForeignKey(t => t.FlatListType)
                    .HasConstraintName("FK_List.FlatList.Data_enum.List.FlatListType")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasIndex(e => new { e.BID, e.FlatListType, e.IsActive, e.SortIndex, e.Name })
                    .HasName("IX_List.FlatList.Data_FlatListDefault");
                // AdHoc == 0 filter added in migration
                entity.HasIndex(e => new { e.BID, e.FlatListType, e.Name, e.IsActive, e.SortIndex })
                    .HasName("IX_List.FlatList.Data_FlatListByName");
                // AdHoc == 0 filter added in migration
            });

            modelBuilder.Entity<SystemListFilter>(entity =>
            {
                entity.ToTable("System.List.Filter");

                entity.HasKey(e => new { e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1700))");
                entity.Property(e => e.Hint).IsUnicode(false);
                entity.Property(e => e.IsActive);
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.SortIndex).HasDefaultValueSql("((50))");
                entity.Property(e => e.CriteriaXML).HasColumnName("Criteria")
                    .HasColumnType("xml");
                entity.Property(e => e.IDsXML).HasColumnName("IDs")
                    .HasColumnType("xml");

                entity.Ignore(e => e.CriteriaObject);
                entity.Ignore(e => e.IDsObject);
            });

            modelBuilder.Entity<LocationData>(entity =>
            {
                entity.ToTable("Location.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1005))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(d => d.LocationNumber).HasColumnType("varchar(25)");
                entity.Property(d => d.CreditMemoPrefix).HasColumnType("nvarchar(10)")
                    .HasDefaultValue("CM-");
                entity.Property(d => d.Abbreviation).HasColumnType("nvarchar(10)").IsRequired().HasDefaultValueSql("'L01'");

                entity.HasIndex(e => new { e.BID, e.Name })
                    .HasName("IX_Location.Data_Name")
                    .IsUnique();

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Locations)
                    .HasForeignKey(d => d.BID)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Location.Data_Business.Data");
                entity.HasOne<EnumTimeZone>(d => d.TimeZone)
                    .WithMany()
                    .HasForeignKey(d => d.TimeZoneID)
                    .HasConstraintName("FK_Location.Data_enum.TimeZone");
                entity.HasOne(d => d.TaxGroup)
                    .WithMany(p => p.DefaultLocations)
                    .HasForeignKey(d => new { d.BID, d.DefaultTaxGroupID })
                    .HasConstraintName("FK_Location.Data_Accounting.Tax.Group");
                entity.HasOne(e => e.DefaultEmailAccount)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.DefaultEmailAccountID })
                    .HasConstraintName("FK_Employee.Data_DefaultEmailAccountID");

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<LocationLocator>(entity =>
            {
                entity.ToTable("Location.Locator");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1008))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");

                entity.HasIndex(e => new { e.BID, e.ParentID })
                    .HasName("IX_Location.Locator_Parent");
                entity.HasIndex(e => new { e.BID, e.LocatorType, e.LocatorSubType, e.ParentID })
                    .HasName("IX_Location.Locator_LocatorType");

                MapLocator<LocationLocator, byte, LocationData>(entity);

                entity.HasOne(d => d.LocatorTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.LocatorType)
                    .HasConstraintName("FK_Location.Locator_enum.LocatorType");
                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.LocationLocators)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    .HasConstraintName("FK_Location.Locator_Location.Data");
                entity.HasOne(d => d.LocatorSubTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => new { d.LocatorType, d.LocatorSubType })
                    .HasConstraintName("FK_Business.Location.Locator_enum.LocatorSubType");
            });

            modelBuilder.Entity<OpportunityData>(entity =>
            {
                entity.ToTable("Opportunity.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((9000))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.LocationID).HasDefaultValueSql("((1))");
                entity.Property(e => e.StatusID).HasDefaultValueSql("((0))");
                entity.Property(e => e.Amount).HasColumnType("money");
                entity.Property(e => e.Probability).HasColumnType("decimal(9, 4)");
                entity.Property(e => e.WeightedAmount).HasColumnType("money")
                    .HasComputedColumnSql("(CONVERT([money],([Amount]*[Probability])/(100)))");
                entity.Property(e => e.ExpectedDate).HasColumnType("date");
                entity.Property(e => e.ClosedDate).HasColumnType("date");

                entity.HasIndex(e => new { e.BID, e.CompanyID })
                    .HasName("IX_Opportunity_Company");
                entity.HasIndex(e => new { e.BID, e.ContactID })
                    .HasName("IX_Opportunity_Contact");

                entity.HasOne(d => d.Business)
                    .WithMany(p => p.Opportunities)
                    .HasForeignKey(d => d.BID)
                    .HasConstraintName("FK_Opportunity.Data_Business.Data");
                entity.HasOne(d => d.Campaign)
                    .WithMany(p => p.Opportunities)
                    .HasForeignKey(d => new { d.BID, d.CampaignID })
                    .HasConstraintName("FK_Opportunity.Data_Campaign.Data");
                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Opportunities)
                    .HasForeignKey(d => new { d.BID, d.CompanyID })
                    .HasConstraintName("FK_Opportunity.Data_Company.Data");
                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Opportunities)
                    .HasForeignKey(d => new { d.BID, d.ContactID })
                    .HasConstraintName("FK_Opportunity.Data_Contact.Data");
                entity.HasOne(d => d.EmployeeTeam)
                    .WithMany(p => p.Opportunities)
                    .HasForeignKey(d => new { d.BID, d.TeamID })
                    .HasConstraintName("FK_Opportunity.Data_Employee.Team");

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<OptionData>(entity =>
            {
                entity.ToTable("Option.Data");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1800))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.OptionLevel).HasComputedColumnSql("(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (128) when [CompanyID] IS NOT NULL then (64) when [UserID] IS NOT NULL then (32) when [StorefrontID] IS NOT NULL then (16) when [LocationID] IS NOT NULL then (8) when [BID] IS NOT NULL then (4) when [AssociationID] IS NOT NULL then (2) else (0) end),(0)))");

                entity.HasIndex(e => new { e.OptionID, e.OptionLevel })
                    .HasName("IX_Option.Data_OptionID_Level_BIDNull");
                entity.HasIndex(e => new { e.BID, e.OptionID, e.OptionLevel })
                    .HasName("IX_Option.Data_BID_OptionID_Level");

                entity.HasOne(d => d.OptionDefinition)
                    .WithMany(p => p.Options)
                    .HasForeignKey(d => d.OptionID)
                    .HasConstraintName("FK_Option.Data_System.Option.Definition");
            });

            modelBuilder.Entity<ReportMenu>(entity =>
            {
                entity.ToTable("Report.Menu");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((2000))");
                entity.Property(e => e.CreatedDate).HasColumnType("date")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.SortIndex).HasDefaultValueSql("((100))");
                entity.Property(e => e.ReportOptions).HasColumnType("xml");
            });

            modelBuilder.Entity<RightsGroup>(entity =>
            {
                entity.ToTable("System.Rights.Group");

                entity.HasKey(e => new { e.ID }).HasName("PK_System.Rights.Group");

                entity.Property(e => e.Name).HasColumnType("varchar(255)").IsRequired();
            });

            modelBuilder.Entity<RightsGroupList>(entity =>
            {
                entity.ToTable("Rights.Group.List");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Rights.Group.List");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1100))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(GetUTCDate())");
                entity.Property(e => e.Name).HasColumnType("varchar(255)");
                entity.Property(e => e.IsUserSpecific).HasDefaultValue(false).ValueGeneratedNever();
                entity.Property(e => e.RightsArray).HasColumnType("varchar(500)");

                entity.Ignore(e => e.RightsGroups);
                entity.Ignore(e => e.Employees);
                entity.Ignore(e => e.Contacts);
                entity.Ignore(e => e.SimpleRightsGroup);
                entity.Ignore(e => e.SimpleEmployees);
                entity.Ignore(e => e.SimpleContacts);
            });

            modelBuilder.Entity<RightsGroupListRightsGroupLink>(entity =>
            {
                entity.ToTable("Rights.Group.List.RightsGroupLink");

                entity.HasKey(e => new { e.BID, e.ListID, e.GroupID }).HasName("PK_Security.Group.List.GroupLink");

                entity.Property(e => e.CreatedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");

                entity.HasIndex(e => new { e.GroupID, e.BID, e.ListID })
                    .HasName("IX_Security.Group.List.GroupLink_GroupID");

                entity.HasOne(e => e.RightsGroup)
                    .WithMany()
                    .HasForeignKey(e => new { e.GroupID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Rights.Group.List.RightsGroupLink_System.Rights.Group");

                entity.HasOne<RightsGroupList>(t => t.RightsGroupList)
                    .WithMany(t => t.RightsGroupRightLinks)
                    .HasForeignKey(e => new { e.BID, e.ListID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Security.Group.List.GroupLink_Security.Group.List");
            });

            modelBuilder.Entity<RightsGroupChildGroupLink>(entity =>
            {
                entity.ToTable("System.Rights.Group.ChildLink");

                entity.HasKey(e => new { e.ParentID, e.ChildID }).HasName("PK_System.Rights.Group.ChildLink");

                entity.HasOne(t => t.ParentRightsGroup)
                    .WithMany()
                    .HasForeignKey(t => new { t.ParentID })
                    .HasConstraintName("FK_System.Rights.Group.ChildLink_Parent")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.ChildRightsGroup)
                    .WithMany()
                    .HasForeignKey(t => new { t.ChildID })
                    .HasConstraintName("FK_System.Rights.Group.ChildLink_Child")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<RightsGroupRightLink>(entity =>
            {
                entity.ToTable("System.Rights.Group.RightLink");

                entity.HasKey(e => new { e.GroupID, e.RightID }).HasName("PK_System.Rights.Group.RightLink");

                // This FK still exists, we just don't have
                // the unlying EnumUserRight object anymore

                //entity.HasOne<EnumUserRight>()
                //    .WithMany()
                //    .HasForeignKey(e => new { e.RightID })
                //    .OnDelete(DeleteBehavior.Restrict)
                //    .HasConstraintName("FK_System.Rights.Group.RightLink_enum.User.Right");

                entity.HasOne(t => t.RightsGroup)
                    .WithMany()
                    .HasForeignKey(e => new { e.GroupID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Rights.Group.RightLink_System.Rights.Group");
            });

            modelBuilder.Entity<RightsGroupMenuTree>(entity =>
            {
                entity.ToTable("System.Rights.Group.Menu.Tree");

                entity.HasKey(e => new { e.ID }).HasName("PK_System.Rights.Group.Menu.Tree");

                entity.HasIndex(e => new { e.EnabledGroupID })
                    .HasName("IX_System.Rights.Group.Menu.Tree_EnabledID");
                entity.HasIndex(e => new { e.ParentID, e.SortIndex, e.Name })
                    .HasName("IX_System.Rights.Group.Menu.Tree_Parent");

                entity.HasOne(t => t.EnabledGroup)
                    .WithMany()
                    .HasForeignKey(e => new { e.EnabledGroupID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Rights.Group.Menu.Tree_EnabledGroup");
                /*entity.HasOne(t => t.Parent)
                    .WithMany()
                    .HasForeignKey(e => new { e.ParentID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Rights.Group.Menu.Tree_System.Rights.Group.Menu.Tree");*/
                entity.HasOne(t => t.FullAccessGroup)
                    .WithMany()
                    .HasForeignKey(e => new { e.FullAccessGroupID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Rights.Group.Menu.Tree_FullAccessGroup");
                /*entity.HasOne<RightsGroupMenuTree>()
                    .WithMany(e => e.Children)
                    .HasForeignKey(e => e.ID)
                    .OnDelete(DeleteBehavior.Restrict);*/
            });

            modelBuilder.Entity<RightsUserAccessTypeGroupLink>(entity =>
            {
                entity.ToTable("System.Rights.UserAccessType.GroupLink");

                entity.HasKey(e => new { e.ID }).HasName("PK_System.Rights.UserAccessType.GroupLink");

                entity.HasOne<EnumUserAccessType>()
                    .WithMany()
                    .HasForeignKey(e => new { e.UserAccessType })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Rights.UserAccessType.GroupLink_enum.User.Access.Type");
                entity.HasOne(t => t.IncludedRightsGroup)
                    .WithMany()
                    .HasForeignKey(e => new { e.IncludedRightsGroupID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Rights.UserAccessType.GroupLink_System.Rights.Group");
            });

            modelBuilder.Entity<RootData>(entity =>
            {
                entity.ToTable("_Root.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((0))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
            });

            modelBuilder.Entity<SystemListColumn>(entity =>
            {
                entity.ToTable("System.List.Column");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1711))");
                entity.Property(e => e.Field).IsUnicode(false);
                entity.Property(e => e.HeaderText).IsUnicode(false);
                entity.Property(e => e.IsVisible).HasComputedColumnSql("(isnull(case when [SortIndex] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.SortField).IsUnicode(false);
                entity.Property(e => e.SortIndex).HasDefaultValueSql("((50))");
                entity.Property(e => e.StyleClass).IsUnicode(false);
                entity.Property(e => e.IsExpandable).HasColumnName("IsExpander");

                entity.HasIndex(e => new { e.TargetClassTypeID, e.SortIndex, e.Name })
                    .HasName("IX_System.List.Column_TargetCTID");
            });

            modelBuilder.Entity<SystemListFilterCriteria>(entity =>
            {
                entity.ToTable("System.List.Filter.Criteria");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1710))");
                entity.Property(e => e.Field).IsUnicode(false);
                entity.Property(e => e.IsLimitToList).HasDefaultValueSql("((1))");
                entity.Property(e => e.IsVisible).HasComputedColumnSql("(isnull(case when [SortIndex] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.Label).IsUnicode(false);
                entity.Property(e => e.ListValues).IsUnicode(false);
                entity.Property(e => e.ListValuesEndpoint).IsUnicode(false);
                entity.Property(e => e.Name).IsUnicode(false);

                entity.HasIndex(e => new { e.TargetClassTypeID, e.SortIndex, e.Name })
                    .HasName("IX_System.List.Filter.Criteria_TargetCTID");
            });

            modelBuilder.Entity<SystemOptionCategory>(entity =>
            {
                entity.ToTable("System.Option.Category");

                entity.Property(e => e.ID).ValueGeneratedNever();
                entity.Property(e => e.Description).IsUnicode(false);
                entity.Property(e => e.IsAssociationOption).HasComputedColumnSql("(isnull(CONVERT([bit],case when ([OptionLevels]&(1))<>(0) then (1) else (0) end),(0)))");
                entity.Property(e => e.IsBusinessOption).HasComputedColumnSql("(isnull(CONVERT([bit],case when ([OptionLevels]&(2))<>(0) then (1) else (0) end),(0)))");
                entity.Property(e => e.IsCompanyOption).HasComputedColumnSql("(isnull(CONVERT([bit],case when ([OptionLevels]&(32))<>(0) then (1) else (0) end),(0)))");
                entity.Property(e => e.IsContactOption).HasComputedColumnSql("(isnull(CONVERT([bit],case when ([OptionLevels]&(64))<>(0) then (1) else (0) end),(0)))");
                entity.Property(e => e.IsUserOption).HasComputedColumnSql("(isnull(CONVERT([bit],case when ([OptionLevels]&(16))<>(0) then (1) else (0) end),(0)))");
                entity.Property(e => e.IsLocationOption).HasComputedColumnSql("(isnull(CONVERT([bit],case when ([OptionLevels]&(4))<>(0) then (1) else (0) end),(0)))");
                entity.Property(e => e.IsStorefrontOption).HasComputedColumnSql("(isnull(CONVERT([bit],case when ([OptionLevels]&(8))<>(0) then (1) else (0) end),(0)))");
                entity.Property(e => e.IsSystemOption).HasComputedColumnSql("((1))");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.SearchTerms).IsUnicode(false);

                entity.HasIndex(e => new { e.SectionID, e.Name, e.IsHidden, e.OptionLevels })
                    .HasName("[IX_System.Option.Category_SectionID");

                entity.HasOne(d => d.Section)
                    .WithMany(p => p.SystemOptionCategories)
                    .HasForeignKey(d => d.SectionID)
                    .HasConstraintName("FK_System.Option.Category_System.Option.Section");
            });

            modelBuilder.Entity<SystemOptionDefinition>(entity =>
            {
                entity.ToTable("System.Option.Definition");

                entity.Property(e => e.ID).ValueGeneratedNever();
                entity.Property(e => e.DataType).HasColumnType("smallint")
                    .HasDefaultValueSql("((0))");
                entity.Property(e => e.DefaultValue).IsUnicode(false);
                entity.Property(e => e.Description).IsUnicode(false);
                entity.Property(e => e.Label).IsUnicode(false);
                entity.Property(e => e.ListValues).IsUnicode(false);
                entity.Property(e => e.Name).IsUnicode(false);

                entity.HasIndex(e => new { e.CategoryID, e.Name, e.IsHidden })
                    .HasName("IX_System.Option.Definition_CategoryID");
                entity.HasIndex(e => new { e.Name, e.CategoryID, e.IsHidden })
                    .HasName("IX_System.Option.Definition_Name");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.SystemOptionDefinitions)
                    .HasForeignKey(d => d.CategoryID)
                    .HasConstraintName("FK_System.Option.Definition_System.Option.Category");
                entity.HasOne<EnumDataType>(d => d.DataTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.DataType)
                    .HasConstraintName("FK_System.Option.Definition_enum.DataType");
            });

            modelBuilder.Entity<SystemOptionSection>(entity =>
            {
                entity.ToTable("System.Option.Section");

                entity.Property(e => e.ID).ValueGeneratedNever();
                entity.Property(e => e.Depth).HasDefaultValueSql("((0))");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.SearchTerms).IsUnicode(false);

                entity.HasIndex(e => new { e.ParentID, e.Name })
                    .HasName("IX_System.Option.Section_ParentID");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.ChildSections)
                    .HasForeignKey(d => d.ParentID)
                    .HasConstraintName("FK_System.Option.Section_System.Option.Section");
            });

            modelBuilder.Entity<TaxItem>(entity =>
            {
                entity.ToTable("Accounting.Tax.Item");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((11102))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.TaxRate).HasColumnType("decimal(12, 4)");
                entity.Property(e => e.TaxableSalesCap).HasColumnType("decimal(18, 4)");

                entity.Ignore(e => e.SimpleTaxGroups);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_Accounting.Tax.Item_Name_IsActive");
                entity.HasIndex(e => new { e.BID, e.GLAccountID })
                    .HasName("IX_Accounting.Tax.Item_GLAccount");
                entity.HasIndex(e => new { e.BID, e.Name })
                    .HasName("IX_Accounting.Tax.Item_Name").IsUnique();

                entity.HasOne(d => d.GLAccount)
                   .WithMany()
                   .HasForeignKey(d => new { d.BID, d.GLAccountID })
                   .HasConstraintName("FK_Accounting.Tax.Item_Accounting.GL.Account_BID_GLAccountID");
                entity.HasOne<EnumTaxEngineType>()
                    .WithMany()
                    .HasForeignKey(t => t.TaxEngineType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Tax.Item_enum.Accounting.TaxEngineType");
            });

            modelBuilder.Entity<TaxGroup>(entity =>
            {
                entity.ToTable("Accounting.Tax.Group");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((11101))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.IsTaxExempt).HasComputedColumnSql("(case when [TaxRate]=(0.0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.TaxRate).HasColumnType("decimal(12, 4)");

                entity.Ignore(e => e.Locations);
                entity.Ignore(e => e.SimpleLocations);
                entity.Ignore(e => e.TaxItems);
                entity.Ignore(e => e.SimpleTaxItems);
            });

            modelBuilder.Entity<TaxGroupItemLink>(entity =>
            {
                entity.ToTable("Accounting.Tax.Group.ItemLink");

                entity.HasKey(e => new { e.BID, e.GroupID, e.ItemID });

                entity.HasIndex(e => new { e.BID, e.ItemID, e.GroupID })
                    .HasName("IX_Accounting.Tax.Group.ItemLink_Item");

                entity.HasOne(d => d.TaxItem)
                    .WithMany(p => p.TaxGroupItemLinks)
                    .HasForeignKey(d => new { d.BID, d.ItemID })
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Accounting.Tax.Group.ItemLink_Accounting.Tax.Item");
                entity.HasOne(d => d.TaxGroup)
                    .WithMany(p => p.TaxGroupItemLinks)
                    .HasForeignKey(d => new { d.BID, d.GroupID })
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Accounting.Tax.Group.ItemLink_Accounting.Tax.Group");
            });

            modelBuilder.Entity<TaxGroupLocationLink>(entity =>
            {
                entity.ToTable("Accounting.Tax.Group.LocationLink");

                entity.HasKey(e => new { e.BID, e.GroupID, e.LocationID });

                entity.HasIndex(e => new { e.BID, e.LocationID, e.GroupID })
                    .HasName("IX_Accounting.Tax.Group.LocationLink_Location");

                entity.HasOne(d => d.TaxGroup)
                    .WithMany(p => p.TaxGroupLocationLinks)
                    .HasForeignKey(d => new { d.BID, d.GroupID })
                    .HasConstraintName("FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group");
                entity.HasOne(d => d.Location)
                    .WithMany(p => p.TaxGroupLocationLinks)
                    .HasForeignKey(d => new { d.BID, d.LocationID })
                    .HasConstraintName("FK_Accounting.Tax.Group.LocationLink_Location.Data");
            });

            modelBuilder.Entity<UserLink>(entity =>
            {
                entity.ToTable("User.Link");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_User.Link");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1012))");
                entity.Property(e => e.UserAccessType).HasColumnType("tinyint");
                entity.Property(e => e.UserName).HasColumnType("varchar(255)");
                entity.Property(e => e.LastConnectionDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.LastDisconnectionDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.LastActivityDT).HasColumnType("datetime2(2)");

                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.UserLinks)
                    .HasForeignKey(d => new { d.BID, d.ContactID })
                    .HasConstraintName("FK_User.Link_Contact.Data");
                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.UserLinks)
                    .HasForeignKey(d => new { d.BID, d.EmployeeID })
                    .HasConstraintName("FK_User.Link_Employee.Data");
                entity.HasOne(e => e.UserAccessTypeEnum)
                    .WithMany()
                    .HasForeignKey(e => new { e.UserAccessType })
                    .HasConstraintName("FK_User.Link_enum.User.Access.Type");
                entity.HasOne(t => t.RightsGroupList)
                    .WithMany(e => e.UserLinks)
                    .IsRequired(false)
                    .HasForeignKey(e => new { e.BID, e.RightsGroupListID })
                    .HasConstraintName("FK_User.Link_Rights.Group.List");
            });

            modelBuilder.Entity<UtilNextID>(entity =>
            {
                entity.ToTable("Util.NextID");

                entity.HasKey(e => new { e.BID, e.ClassTypeID });
            });

            modelBuilder.Entity<SimpleAlertDefinition>(entity =>
            {
                entity.ToTable("Alert.Definition.SimpleList");

                MapSimpleListItem<SimpleAlertDefinition, short>(entity, ClassType.AlertDefinition.AsString());
            });

            modelBuilder.Entity<SimpleBoardView>(entity =>
            {
                entity.ToTable("Board.View.SimpleList");

                MapSimpleListItem<SimpleBoardView, short>(entity, ClassType.BoardView.AsString());
            });

            modelBuilder.Entity<SimpleBusinessData>(entity =>
            {
                entity.ToTable("Business.SimpleList");
                MapSimpleListItem<SimpleBusinessData, short>(entity, ClassType.Business.ToString());
            });

            modelBuilder.Entity<SimpleLocationData>(entity =>
            {
                entity.ToTable("Business.Location.SimpleList");

                MapSimpleListItem<SimpleLocationData, byte>(entity, ClassType.Location.AsString());
            });

            modelBuilder.Entity<SimpleCompanyData>(entity =>
            {
                entity.ToTable("Company.SimpleList");

                MapSimpleListItem<SimpleCompanyData, int>(entity, ClassType.Company.AsString());
            });

            modelBuilder.Entity<SimpleContactData>(entity =>
            {
                entity.ToTable("Contact.SimpleList");

                MapSimpleListItem<SimpleContactData, int>(entity, ClassType.Contact.AsString());

                entity.HasOne(d => d.ParentCompany)
                    .WithMany(d => d.SimpleContacts)
                    .HasForeignKey(d => new { d.BID, d.ParentID });
            });

            modelBuilder.Entity<SimpleEmployeeData>(entity =>
            {
                entity.ToTable("Employee.SimpleList");

                MapSimpleListItem<SimpleEmployeeData, short>(entity, ClassType.Employee.AsString());
            });

            modelBuilder.Entity<SimpleEmployeeTeam>(entity =>
            {
                entity.ToTable("Employee.Team.SimpleList");

                MapSimpleListItem<SimpleEmployeeTeam, int>(entity, ClassType.EmployeeTeam.AsString());
            });

            modelBuilder.Entity<SimpleOriginData>(entity =>
            {
                entity.ToTable("CRM.Origin.SimpleList");

                MapSimpleListItem<SimpleOriginData, short>(entity, ClassType.CrmOrigin.AsString());
            });

            modelBuilder.Entity<SimpleCrmIndustry>(entity =>
            {
                entity.ToTable("CRM.Industry.SimpleList");
                MapSimpleListItem<SimpleCrmIndustry, short>(entity, ClassType.CrmIndustry.AsString());
            });

            modelBuilder.Entity<SimpleMachineData>(entity =>
            {
                entity.ToTable("Part.Machine.SimpleList");

                MapSimpleListItem<SimpleMachineData, short>(entity, ClassType.Machine.AsString());
            });

            modelBuilder.Entity<SimpleMachineCategory>(entity =>
            {
                entity.ToTable("Part.Machine.Category.SimpleList");

                MapSimpleListItem<SimpleMachineCategory, short>(entity, ClassType.MachineCategory.AsString());
            });

            modelBuilder.Entity<SimpleTaxabilityCode>(entity =>
            {
                entity.ToTable("Accounting.Tax.Code.SimpleList");
                MapSimpleListItem<SimpleTaxabilityCode, short>(entity, ClassType.TaxabilityCode.AsString());
            });

            modelBuilder.Entity<SimpleTaxGroup>(entity =>
            {
                entity.ToTable("Accounting.Tax.Group.SimpleList");

                entity.Property(e => e.TaxRate).HasColumnType("decimal(12, 4)");

                MapSimpleListItem<SimpleTaxGroup, short>(entity, ClassType.TaxGroup.AsString());
            });

            modelBuilder.Entity<SimpleTaxItem>(entity =>
            {
                entity.ToTable("Accounting.Tax.Item.SimpleList");
                MapSimpleListItem<SimpleTaxItem, short>(entity, ClassType.TaxItem.AsString());
            });

            modelBuilder.Entity<SimpleEnumTimeZone>(entity =>
            {
                entity.ToTable("enum.TimeZone.SimpleList");

                MapSimpleEnumListItem<SimpleEnumTimeZone, short>(entity);
            });

            modelBuilder.Entity<SimpleLaborData>(entity =>
            {
                entity.ToTable("Part.Labor.SimpleList");
            });

            modelBuilder.Entity<SimpleLaborCategory>(entity =>
            {
                entity.ToTable("Part.Labor.Category.SimpleList");
            });

            modelBuilder.Entity<SimpleMaterialData>(entity =>
            {
                entity.ToTable("Part.Material.SimpleList");
            });

            modelBuilder.Entity<SimpleMaterialCategory>(entity =>
            {
                entity.ToTable("Part.Material.Category.SimpleList");
            });

            modelBuilder.Entity<SimpleSurchargeDef>(entity =>
            {
                entity.ToTable("Part.Surcharge.SimpleList");
                MapSimpleListItem<SimpleSurchargeDef, short>(entity, ClassType.SurchargeDef.AsString());
            });

            modelBuilder.Entity<GLAccount>(entity =>
            {
                entity.ToTable("Accounting.GL.Account");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((8000))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.NumberedName).HasComputedColumnSql("(concat(CONVERT([varchar](6),[Number])+' ',[Name]))");
                entity.Property(e => e.IsAsset).HasComputedColumnSql("(case when [GLAccountType]>=(10) AND [GLAccountType]<=(19) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsLiability).HasComputedColumnSql("(case when [GLAccountType]>=(20) AND [GLAccountType]<=(29) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsEquity).HasComputedColumnSql("(case when [GLAccountType]>=(30) AND [GLAccountType]<=(39) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsIncome).HasComputedColumnSql("(case when [GLAccountType]>=(40) AND [GLAccountType]<=(49) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsCOGS).HasComputedColumnSql("(case when [GLAccountType]>=(50) AND [GLAccountType]<=(59) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsExpense).HasComputedColumnSql("(case when [GLAccountType]>=(60) AND [GLAccountType]<=(69) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

                entity.HasIndex(e => new { e.BID, e.Name }).IsUnique()
                    .HasName("IX_Accounting.GL.Account_Name");
                entity.HasIndex(e => new { e.BID, e.NumberedName })
                    .HasName("IX_Accounting.GL.Account_NumberedName");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.IsActive })
                    .HasName("IX_Accounting.GL.Account_ParentID");

                entity.HasOne(t => t.GLAccountTypeNavigation)
                    .WithMany()
                    .HasForeignKey(e => e.GLAccountType)
                    .HasConstraintName("FK_Accounting.GL.Account_enum.Accounting.GLAccountType_GLAccountType");

                entity.HasOne(t => t.ExportGLAccountTypeNavigation)
                    .WithMany()
                    .HasForeignKey(e => e.ExportGLAccountType)
                    .HasConstraintName("FK_Accounting.GL.Account_enum.Accounting.GLAccountType_ExportGLAccountType");

                entity.HasOne(t => t.ParentGLAccount)
                    .WithMany(m => m.Subaccounts)
                    .HasForeignKey(e => new { e.BID, e.ParentID })
                    .HasConstraintName("FK_Accounting.GL.Account_Accounting.GL.Account_BID_ParentID");

                entity.Ignore("TaxItem");
            });

            modelBuilder.Entity<PaymentMethod>(entity =>
            {
                entity.ToTable("Accounting.Payment.Method");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Accounting.Payment.Method");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("8002");
                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsRequired();
                entity.Property(e => e.PaymentMethodType).HasColumnType("tinyint")
                    .HasDefaultValueSql("0").IsRequired();
                entity.Property(e => e.DepositGLAccountID).IsRequired();
                entity.Property(e => e.IsIntegrated).IsRequired();
                entity.Property(e => e.IsACH).HasComputedColumnSql("(CASE WHEN PaymentMethodType = 3 THEN CONVERT(BIT, 1) ELSE CONVERT([BIT], 0) END)");
                entity.Property(e => e.IsCreditCard).HasComputedColumnSql("(CASE WHEN PaymentMethodType in (5, 6, 7, 8, 9) THEN CONVERT([BIT], 1) ELSE CONVERT([BIT], 0) END)");
                entity.Property(e => e.IsCustom).IsRequired();

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_Accounting.Payment.Method_Name");

                entity.HasOne<GLAccount>(t => t.DepositGLAccount)
                    .WithMany(m => m.PaymentMethods)
                    .HasForeignKey(e => new { e.BID, e.DepositGLAccountID })
                    .HasConstraintName("FK_Accounting.Payment.Method_Accounting.GL.Account");
                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Accounting.Payment.Method_Business.Data");
                entity.HasOne<EnumPaymentMethodType>()
                    .WithMany()
                    .HasForeignKey(e => e.PaymentMethodType)
                    .HasConstraintName("FK_Accounting.Payment.Method_enum.Accounting.PaymentMethodType");

            });

            modelBuilder.Entity<SimplePaymentMethod>(entity =>
            {
                entity.ToTable("Accounting.Payment.Method.SimpleList");
                MapSimpleListItem<SimplePaymentMethod, int>(entity, "8002");
            });

            modelBuilder.Entity<SimplePaymentTerm>(entity =>
            {
                entity.ToTable("Accounting.Payment.Term.SimpleList");
                MapSimpleListItem<SimplePaymentTerm, int>(entity, "11105");
            });

            modelBuilder.Entity<EnumPaymentMethodType>(entity =>
            {
                entity.ToTable("enum.Accounting.PaymentMethodType");
                entity.HasKey(e => e.ID).HasName("PK_enum.Accounting.PaymentMethodType");
            });

            modelBuilder.Entity<EnumPaymentDueBasedOnType>(entity =>
            {
                entity.ToTable("enum.Accounting.PaymentDueBasedOnType");
                entity.HasKey(e => e.ID).HasName("PK_enum.Accounting.PaymentDueBasedOnType");
            });

            modelBuilder.Entity<EnumTaxEngineType>(entity =>
            {
                entity.ToTable("enum.Accounting.TaxEngineType");
                entity.HasKey(e => e.ID).HasName("PK_enum.Accounting.TaxEngineType");
            });

            modelBuilder.Entity<PaymentTerm>(entity =>
            {
                entity.ToTable("Accounting.Payment.Term");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Accounting.Payment.Term");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("11105");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsRequired();
                entity.Property(e => e.DaysDue).HasDefaultValueSql("0").IsRequired();
                entity.Property(e => e.PaymentDueBasedOnType).HasColumnType("tinyint")
                    .HasDefaultValueSql("0").IsRequired();
                entity.Property(e => e.PaymentDueBasedOnText).HasComputedColumnSql("(case [PaymentDueBasedOnType] when (0) then 'Days after Invoice' when (1) then 'Days after Built' when (2) then 'Days after Ordered' when (3) then 'Days After End of Invoice Month' when (4) then 'Day of the Month' when (5) then 'Day of the Following Month' end)");
                entity.Property(e => e.DownPaymentRequired).HasComputedColumnSql(" (case when [DownPaymentPercent]>(0.00) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)0");
                entity.Property(e => e.EarlyPaymentPercent).HasColumnType("decimal(12,4)");
                entity.Property(e => e.DownPaymentPercent).HasColumnType("decimal(12,4)");
                entity.Property(e => e.DownPaymentThreshold).HasColumnType("decimal(18,4)");
                entity.Property(e => e.DownPaymentPercentBelow).HasColumnType("decimal(12,4)");

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_Accounting.Payment.Term_Name");

                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Accounting.Payment.Term_Business.Data");
                entity.HasOne<EnumPaymentDueBasedOnType>()
                    .WithMany()
                    .HasForeignKey(e => e.PaymentDueBasedOnType)
                    .HasConstraintName("FK_Accounting.Payment.Method_enum.Accounting.PaymentDueBasedOnType");
            });

            modelBuilder.Entity<LaborData>(entity =>
            {
                entity.ToTable("Part.Labor.Data");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Labor.Data");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((12020))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsRequired();
                entity.Property(e => e.InvoiceText).HasMaxLength(255).IsRequired();
                entity.Property(e => e.Description).HasMaxLength(255);
                entity.Property(e => e.HasImage);
                entity.Property(e => e.SKU).HasMaxLength(512);
                entity.Property(e => e.IncomeAccountID).IsRequired();
                entity.Property(e => e.ExpenseAccountID).IsRequired();
                entity.Property(e => e.TaxCodeID).HasColumnType("smallint");

                entity.Ignore(e => e.LaborCategories);
                entity.Ignore(e => e.SimpleLaborCategories);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_Part.Labor.Data_Name");

                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Part.Labor.Data_Business.Data");
                entity.HasOne<GLAccount>(t => t.IncomeAccount)
                    .WithMany(m => m.IncomeLabors)
                    .HasForeignKey(e => new { e.BID, e.IncomeAccountID })
                    .HasConstraintName("FK_Part.Labor.Data_IncomeAccountID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<GLAccount>(t => t.ExpenseAccount)
                    .WithMany(m => m.ExpenseLabors)
                    .HasForeignKey(e => new { e.BID, e.ExpenseAccountID })
                    .HasConstraintName("FK_Part.Labor.Data_ExpenseAccountID	")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.TaxabilityCode)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.TaxCodeID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Labor.Data_TaxCodeID");

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<LaborCategory>(entity =>
            {
                entity.ToTable("Part.Labor.Category");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Labor.Category");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("12022");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsRequired();
                entity.Property(e => e.Description).HasMaxLength(255);
                entity.Property(e => e.ParentID);

                entity.Ignore(e => e.Labors);
                entity.Ignore(e => e.SimpleLabors);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive, e.ParentID })
                    .HasName("IX_Part.Labor.Data_Name_IsActive");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.Name, e.IsActive })
                    .HasName("IX_Part.Labor.Data_ParentID_Name");

                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Part.Labor.Category_Business.Data");
                entity.HasOne(d => d.ParentCategory)
                    .WithMany(p => p.ChildCategories)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Labor.Category_Part.Labor.Category");
            });

            modelBuilder.Entity<LaborCategoryLink>(entity =>
            {
                entity.ToTable("Part.Labor.CategoryLink");

                entity.HasKey(e => new { e.BID, e.PartID, e.CategoryID }).HasName("PK_Part.Labor.CategoryLink");

                entity.HasIndex(e => new { e.BID, e.CategoryID, e.PartID })
                    .HasName("IX_Part.Labor.CategoryLink_Category");

                entity.HasOne(d => d.Labor)
                    .WithMany(p => p.LaborCategoryLinks)
                    .HasForeignKey(d => new { d.BID, d.PartID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Labor.CategoryLink_Part.Labor.Data");
                entity.HasOne(d => d.LaborCategory)
                    .WithMany(p => p.LaborCategoryLinks)
                    .HasForeignKey(d => new { d.BID, d.CategoryID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Labor.CategoryLink_Part.Labor.Category");
            });

            modelBuilder.Entity<DestinationData>(entity => 
            {
                entity.ToTable("Destination.Data");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Destination.Data");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.Destination.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsRequired();
                entity.Property(e => e.InvoiceText).HasMaxLength(255);
                entity.Property(e => e.Description).HasMaxLength(255);
                entity.Property(e => e.HasImage);
                entity.Property(e => e.SKU).HasMaxLength(512);
                entity.Property(e => e.IncomeAccountID);
                entity.Property(e => e.ExpenseAccountID);
                entity.Property(e => e.TaxCodeID).HasColumnType("smallint");
                entity.Property(e => e.ActiveProfileCount).IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.TemplateID);

                entity.HasIndex(e => new { e.BID, e.Name })
                    .HasName("IX_Destination.Data_Name");
                entity.HasIndex(e => new { e.BID, e.TemplateID })
                    .HasName("IX_Destination.Data_Template");

                entity.HasOne(t => t.IncomeAccount).WithMany()
                    .HasForeignKey(e => new { e.BID, e.IncomeAccountID })
                    .HasConstraintName("FK_Destination.Data_Accounting.GL.Account_Income")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.ExpenseAccount).WithMany()
                    .HasForeignKey(e => new { e.BID, e.ExpenseAccountID })
                    .HasConstraintName("FK_Destination.Data_Accounting.GL.Account_Expense")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.TaxabilityCode)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.TaxCodeID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Destination.Data_Accounting.Tax.Code");
                entity.HasOne(e => e.Template)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.TemplateID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Destination.Data_Part.Subassembly.Data");
            });

            modelBuilder.Entity<DestinationProfile>(entity =>
            {
                entity.ToTable("Destination.Profile");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Destination.Profile");

                entity.Property(e => e.BID).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.DestinationProfile.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.IsDefault).IsRequired();
                entity.Property(e => e.Name).HasColumnType("nvarchar(100)").IsRequired();
                entity.Property(e => e.DestinationID).IsRequired();
                entity.Property(e => e.TemplateID).IsRequired();
                entity.Property(e => e.AssemblyOVDataJSON).HasColumnType("varchar(max)").IsRequired();

                entity.HasIndex(e => new { e.BID, e.DestinationID, e.IsActive, e.Name, e.IsDefault })
                 .HasName("IX_Destination.Profile_Destination");

                entity.HasOne(e => e.Destination)
                    .WithMany(e => e.Profiles)
                    .HasForeignKey(e => new { e.BID, e.DestinationID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Destination.Profile_Destination.Data");
                entity.HasOne(e => e.Template)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.TemplateID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Destination.Profile_Part.Subassembly.Data");
            });

            modelBuilder.Entity<DestinationProfileTable>(entity =>
            {
                entity.ToTable("Destination.Profile.Table");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Destination.Profile.Table");

                entity.Property(e => e.BID).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.DestinationProfileTable.AsString()).IsRequired();
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.ProfileID).IsRequired();
                entity.Property(e => e.TableID).IsRequired();
                entity.Property(e => e.Label).HasColumnType("nvarchar(255)").IsRequired();
                entity.Property(e => e.OverrideDefault).IsRequired();
                entity.Property(e => e.RowCount);
                entity.Property(e => e.RowValuesJSON).HasColumnType("nvarchar(max)");
                entity.Property(e => e.ColumnCount).HasColumnType("smallint");
                entity.Property(e => e.ColumnValuesJSON).HasColumnType("nvarchar(max)");
                entity.Property(e => e.CellDataJSON).HasColumnType("nvarchar(max)");

                entity.HasOne(e => e.Table).WithMany()
                   .HasForeignKey(e => new { e.BID, e.TableID })
                   .OnDelete(DeleteBehavior.Restrict)
                   .HasConstraintName("FK_Destination.Profile.Table_Part.Assembly.Table");
                entity.HasOne<DestinationProfile>()
                    .WithMany(e => e.DestinationProfileTables)
                    .HasForeignKey(e => new { e.BID, e.ProfileID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Destination.Profile.Table_Destination.Profile");
            });

            modelBuilder.Entity<DestinationProfileVariable>(entity =>
            {
                entity.ToTable("Destination.Profile.Variable");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Destination.Profile.Variable");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.DestinationProfileVariable.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.ProfileID).IsRequired();
                entity.Property(e => e.VariableID).IsRequired();
                entity.Property(e => e.OverrideDefault).IsRequired();
                entity.Property(e => e.IsRequired);
                entity.Property(e => e.IsDisabled);
                entity.Property(e => e.AssemblyID).IsRequired();
                entity.Property(e => e.ListValuesJSON).HasColumnType("nvarchar(max)");
                entity.Property(e => e.UnitID).HasColumnType("tinyint");

                entity.HasIndex(e => new { e.BID, e.ProfileID, e.VariableID })
                    .HasName("IX_Destination.Profile.Variable_Profile");
                entity.HasIndex(e => new { e.BID, e.VariableID, e.ProfileID })
                    .HasName("IX_Destination.Profile.Variable_Variable");

                entity.HasOne<DestinationProfile>()
                    .WithMany(e => e.DestinationProfileVariables)
                    .HasForeignKey(e => new { e.BID, e.ProfileID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Destination.Profile.Variable_Destination.Profile");
                entity.HasOne(e => e.Variable)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.VariableID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Destination.Profile.Variable_Part.Subassembly.Variable");
               
            });

            modelBuilder.Entity<SimpleDestinationData>(entity =>
            {
                entity.ToTable("Destination.SimpleList");

                MapSimpleListItem<SimpleDestinationData, short>(entity, ClassType.Destination.AsString());
            });

            modelBuilder.Entity<MachineData>(entity =>
            {
                entity.ToTable("Part.Machine.Data");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Machine.Data");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("12030");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsRequired();
                entity.Property(e => e.InvoiceText).HasMaxLength(255);
                entity.Property(e => e.Description).HasMaxLength(255);
                entity.Property(e => e.HasImage);
                entity.Property(e => e.SKU).HasMaxLength(512);
                entity.Property(e => e.IncomeAccountID);
                entity.Property(e => e.ExpenseAccountID);
                entity.Property(e => e.TaxCodeID).HasColumnType("smallint");
                entity.Property(e => e.ActiveInstanceCount).IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.ActiveProfileCount).IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.MachineTemplateID);
                entity.Property(e => e.WorksheetData).HasColumnType("xml");
                entity.Property(e => e.EstimatingCostPerHourFormula).HasColumnType("varchar(max)");
                entity.Property(e => e.EstimatingPricePerHourFormula).HasColumnType("varchar(max)");
                entity.Property(e => e.AssemblyOVDataJSON).HasColumnType("varchar(max)");

                entity.Ignore(e => e.MachineCategories);
                entity.Ignore(e => e.SimpleMachineCategories);

                entity.HasIndex(e => new { e.BID, e.Name })
                    .HasName("IX_Part.Machine.Data_Name");
                entity.HasIndex(e => new { e.BID, e.MachineTemplateID })
                    .HasName("IX_Part.Machine.Data_Template");

                entity.HasOne<GLAccount>(t => t.IncomeAccount).WithMany(m => m.IncomeMachines)
                    .HasForeignKey(e => new { e.BID, e.IncomeAccountID })
                    .HasConstraintName("FK_Part.Machine.Data_IncomeAccountID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<GLAccount>(t => t.ExpenseAccount).WithMany(m => m.ExpenseMachines)
                    .HasForeignKey(e => new { e.BID, e.ExpenseAccountID })
                    .HasConstraintName("FK_Part.Machine.Data_ExpenseAccountID	")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.TaxabilityCode)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.TaxCodeID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Data_TaxCodeID");
                entity.HasOne(e => e.MachineTemplate)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.MachineTemplateID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Data_Part.Subassembly.Data");

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<MachineInstance>(entity =>
            {
                entity.ToTable("Part.Machine.Instance");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Machine.Instance");

                entity.Property(e => e.BID).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.MachineInstance.AsString()).IsRequired();
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(100).IsRequired();
                entity.Property(e => e.MachineID).IsRequired();
                entity.Property(e => e.Manufacturer).HasMaxLength(100);
                entity.Property(e => e.Model).HasMaxLength(100);
                entity.Property(e => e.SerialNumber).HasMaxLength(100);
                entity.Property(e => e.IPAddress).HasMaxLength(100);
                entity.Property(e => e.PurchaseDate).HasColumnType("date");
                entity.Property(e => e.HasServiceContract).IsRequired();
                entity.Property(e => e.ContractNumber).HasColumnType("nvarchar(100)");
                entity.Property(e => e.ContractStartDate).HasColumnType("date");
                entity.Property(e => e.ContractEndDate).HasColumnType("date");
                entity.Property(e => e.ContractInfo).HasColumnType("nvarchar(MAX)");
                entity.Property(e => e.LocationID).HasColumnType("tinyint");

                entity.HasIndex(e => new { e.BID, e.MachineID, e.IsActive, e.Name })
                    .HasName("IX_Part.Machine.Instance_Machine");

                entity.HasOne(e => e.Machine)
                    .WithMany(e => e.Instances)
                    .HasForeignKey(e => new { e.BID, e.MachineID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Instance_Part.Machine.Data");
                entity.HasOne<LocationData>()
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.LocationID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Instance_Location.Data")
                    .IsRequired(false);
            });

            modelBuilder.Entity<MachineProfile>(entity =>
            {
                entity.ToTable("Part.Machine.Profile");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Machine.Profile");

                entity.Property(e => e.BID).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.MachineProfile.AsString()).IsRequired();
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.IsDefault).IsRequired();
                entity.Property(e => e.Name).HasColumnType("nvarchar(100)").IsRequired();
                entity.Property(e => e.MachineID).IsRequired();
                entity.Property(e => e.MachineTemplateID).IsRequired();
                entity.Property(e => e.AssemblyOVDataJSON).HasColumnType("varchar(max)").IsRequired();
                entity.Property(e => e.MachineLayoutType).HasColumnType("tinyint").HasDefaultValueSql("0").IsRequired();

                entity.HasIndex(e => new { e.BID, e.MachineID, e.IsActive, e.Name, e.IsDefault })
                    .HasName("IX_Part.Machine.Profile_Machine");

                entity.HasOne(e => e.Machine)
                    .WithMany(e => e.Profiles)
                    .HasForeignKey(e => new { e.BID, e.MachineID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile_Part.Machine.Data");
                entity.HasOne(e => e.MachineTemplate)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.MachineTemplateID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile_Part.Subassembly.Data");
                entity.HasOne<EnumMachineLayoutType>()
                    .WithMany()
                    .HasForeignKey(e => e.MachineLayoutType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile_enum.Part.Subassembly.MachineLayoutType");
            });

            modelBuilder.Entity<MachineProfileTable>(entity =>
            {
                entity.ToTable("Part.Machine.Profile.Table");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Machine.Profile.Table");

                entity.Property(e => e.BID).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.MachineProfileTable.AsString()).IsRequired();
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.ProfileID).IsRequired();
                entity.Property(e => e.TableID).IsRequired();
                entity.Property(e => e.Label).HasColumnType("nvarchar(255)").IsRequired();
                entity.Property(e => e.OverrideDefault).IsRequired();
                entity.Property(e => e.RowCount);
                entity.Property(e => e.RowValuesJSON).HasColumnType("nvarchar(max)");
                entity.Property(e => e.ColumnCount).HasColumnType("smallint");
                entity.Property(e => e.ColumnValuesJSON).HasColumnType("nvarchar(max)");
                entity.Property(e => e.CellDataJSON).HasColumnType("nvarchar(max)");

                entity.HasIndex(e => new { e.BID, e.ProfileID, e.Label })
                    .HasName("IX_Part.Machine.Profile.Table_Profile");
                entity.HasIndex(e => new { e.BID, e.TableID })
                    .HasName("IX_Part.Machine.Profile.Table_Table");

                entity.HasOne<AssemblyTable>(e => e.Table)
                    .WithMany(e => e.MachineProfileTables)
                    .HasForeignKey(e => new { e.BID, e.TableID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Table_Part.Assembly.Table");
                entity.HasOne<MachineProfile>()
                    .WithMany(e => e.MachineProfileTables)
                    .HasForeignKey(e => new { e.BID, e.ProfileID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Table_Part.Machine.Profile");
            });

            modelBuilder.Entity<MachineProfileVariable>(entity =>
            {
                entity.ToTable("Part.Machine.Profile.Variable");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Machine.Profile.Variable");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.MachineProfileVariable.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.ProfileID).IsRequired();
                entity.Property(e => e.VariableID).IsRequired();
                entity.Property(e => e.OverrideDefault).IsRequired();
                entity.Property(e => e.IsRequired);
                entity.Property(e => e.IsDisabled);
                entity.Property(e => e.AssemblyID).IsRequired();
                entity.Property(e => e.ListValuesJSON).HasColumnType("nvarchar(max)");
                entity.Property(e => e.UnitID).HasColumnType("tinyint");
                entity.Property(e => e.GroupOptionByCategory);
                entity.Property(e => e.DataType).HasColumnType("smallint");
                entity.Property(e => e.ListDataType).HasColumnType("smallint");
                entity.Property(e => e.LinkedMaterialID);
                entity.Property(e => e.LinkedLaborID);


                entity.HasIndex(e => new { e.BID, e.ProfileID, e.VariableID })
                    .HasName("IX_Part.Machine.Profile.Variable_Profile");
                entity.HasIndex(e => new { e.BID, e.VariableID, e.ProfileID })
                    .HasName("IX_Part.Machine.Profile.Variable_Variable");

                entity.HasOne<MachineProfile>()
                    .WithMany(e => e.MachineProfileVariables)
                    .HasForeignKey(e => new { e.BID, e.ProfileID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Variable_Part.Machine.Profile");
                entity.HasOne<AssemblyVariable>(e => e.Variable)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.VariableID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Variable_Part.Subassembly.Variable");
                entity.HasOne<AssemblyData>()
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.AssemblyID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Variable_Part.Subassembly.Data");
                entity.HasOne<MaterialData>()
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.LinkedMaterialID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Variable_Part.Material.Data");
                entity.HasOne<LaborData>()
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.LinkedLaborID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Variable_Part.Labor.Data");
                entity.HasOne<EnumDataType>()
                    .WithMany()
                    .HasForeignKey(e => e.DataType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Variable_enum.DataType");
                entity.HasOne<EnumDataType>()
                    .WithMany()
                    .HasForeignKey(e => e.ListDataType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Profile.Variable.ListDataType_enum.DataType");//had to alter the ConstraintName a bit to avoid conflict with .DataType

            });

            modelBuilder.Entity<MachineCategory>(entity =>
            {
                entity.ToTable("Part.Machine.Category");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Machine.Category");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.MachineCategory.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsRequired();
                entity.Property(e => e.Description).HasMaxLength(255);
                entity.Property(e => e.ParentID);

                entity.Ignore(e => e.Machines);
                entity.Ignore(e => e.SimpleMachines);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive, e.ParentID })
                    .HasName("IX_Part.Machine.Category_Name_IsActive");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.Name, e.IsActive })
                    .HasName("IX_Part.Machine.Category_ParentID_Name");

                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Part.Machine.Category_Business.Data");
                entity.HasOne(e => e.ParentCategory)
                    .WithMany(e => e.ChildCategories)
                    .HasForeignKey(e => new { e.BID, e.ParentID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.Category_Part.Machine.Category");
            });

            modelBuilder.Entity<MachineCategoryLink>(entity =>
            {
                entity.ToTable("Part.Machine.CategoryLink");

                entity.HasKey(e => new { e.BID, e.PartID, e.CategoryID })
                    .HasName("PK_Part.Machine.CategoryLink");
                entity.HasIndex(e => new { e.BID, e.CategoryID, e.PartID })
                    .HasName("IX_Part.Machine.CategoryLink_Category");

                entity.HasOne(e => e.Machine)
                    .WithMany(e => e.MachineCategoryLinks)
                    .HasForeignKey(e => new { e.BID, e.PartID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.CategoryLink_Part.Machine.Data");
                entity.HasOne(e => e.MachineCategory)
                    .WithMany(e => e.MachineCategoryLinks)
                    .HasForeignKey(e => new { e.BID, e.CategoryID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Machine.CategoryLink_Part.Machine.Category");
            });

            modelBuilder.Entity<MaterialData>(entity =>
            {
                entity.ToTable("Part.Material.Data");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Material.Data");

                entity.Property(e => e.Name).HasColumnType("nvarchar(255)").IsRequired();
                entity.Property(e => e.InvoiceText).HasColumnType("varchar(255)").IsRequired();
                entity.Property(e => e.Description).HasColumnType("varchar(255)");
                entity.Property(e => e.SKU).HasColumnType("varchar(512)");
                entity.Property(e => e.Color).HasColumnType("varchar(255)");
                entity.Property(e => e.EstimatingConsumptionMethod)
                    .HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.ConsumptionUnit)
                    .HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.EstimatingCostingMethod)
                    .HasColumnType("tinyint").IsRequired();

                entity.Ignore(e => e.Height);
                entity.Property(e => e._HeightValue).HasColumnName("Height").HasColumnType("decimal(18,4)");
                entity.Property(e => e._HeightUnit).HasColumnName("HeightUnit").HasColumnType("tinyint");

                entity.Ignore(e => e.Width);
                entity.Property(e => e._WidthValue).HasColumnName("Width").HasColumnType("decimal(18,4)");
                entity.Property(e => e._WidthUnit).HasColumnName("WidthUnit").HasColumnType("tinyint");

                entity.Ignore(e => e.Length);
                entity.Property(e => e._LengthValue).HasColumnName("Length").HasColumnType("decimal(18,4)");
                entity.Property(e => e._LengthUnit).HasColumnName("LengthUnit").HasColumnType("tinyint");

                entity.Ignore(e => e.Weight);
                entity.Property(e => e._WeightValue).HasColumnName("Weight").HasColumnType("decimal(18,4)");
                entity.Property(e => e._WeightUnit).HasColumnName("WeightUnit").HasColumnType("tinyint");

                entity.Ignore(e => e.Volume);
                entity.Property(e => e._VolumeValue).HasColumnName("Volume").HasColumnType("decimal(18,4)");
                entity.Property(e => e._VolumeUnit).HasColumnName("VolumeUnit").HasColumnType("tinyint");

                entity.Property(e => e.PhysicalMaterialType).HasColumnType("tinyint")
                    .IsRequired().HasDefaultValueSql("0");
                entity.Property(e => e.EstimatingPrice).HasColumnType("decimal(18,8)");
                entity.Property(e => e.EstimatingCost).HasColumnType("decimal(18,8)");
                entity.Property(e => e.UnitType).HasColumnType("tinyint")
                    .IsRequired().HasDefaultValueSql("9");
                entity.Property(e => e.RoundingFactor).HasColumnType("decimal(18,8)")
                    .IsRequired().HasDefaultValueSql("0.000001");
                entity.Property(e => e.MinimumQuantity).HasColumnType("decimal(18,4)")
                    .IsRequired().HasDefaultValueSql("0");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((12000))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                    .HasDefaultValueSql("GetUTCDate()");
                entity.Property(e => e.IsActive);
                entity.Property(e => e.HasImage);
                entity.Property(e => e.TrackInventory);
                entity.Property(e => e.TaxCodeID).HasColumnType("smallint");

                entity.Ignore(e => e.MaterialCategories);
                entity.Ignore(e => e.SimpleMaterialCategories);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_Part.Material.Data_Name");

                entity.HasOne(e => e.IncomeAccount)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.IncomeAccountID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Material.Data_Accounting.GL.Account");
                entity.HasOne(e => e.ExpenseAccount)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.ExpenseAccountID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Material.Data_Accounting.GL.Account1");
                entity.HasOne(e => e.InventoryAccount)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.InventoryAccountID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Material.Data_Accounting.GL.Account2");
                entity.HasOne<EnumMaterialConsumptionMethod>()
                    .WithMany()
                    .HasForeignKey(e => e.EstimatingConsumptionMethod)
                    .HasConstraintName("FK_Part.Material.Data_enum.Part.Material.Consumption.Method");
                entity.HasOne<EnumMaterialCostingMethod>()
                    .WithMany()
                    .HasForeignKey(e => e.EstimatingCostingMethod)
                    .HasConstraintName("FK_Part.Material.Data_enum.Part.Material.Costing.Method");
                entity.HasOne<EnumMaterialPhysicalType>()
                    .WithMany()
                    .HasForeignKey(e => e.PhysicalMaterialType)
                    .HasConstraintName("FK_Part.Material.Data_enum.Part.Physical.Material.Type");
                entity.HasOne(e => e.TaxabilityCode)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.TaxCodeID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Material.Data_Account.Tax.Code");

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<MaterialCategory>(entity =>
            {
                entity.ToTable("Part.Material.Category");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Material.Category");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.MaterialCategory.AsString());
                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsRequired();
                entity.Property(e => e.Description).HasMaxLength(255);
                entity.Property(e => e.ParentID);

                entity.Ignore(e => e.Materials);
                entity.Ignore(e => e.SimpleMaterials);

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive, e.ParentID })
                    .HasName("IX_Part.Material.Category_Name_IsActive");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.Name, e.IsActive })
                    .HasName("IX_Part.Material.Category_ParentID_Name");

                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Part.Material.Category_Business.Data");
                entity.HasOne(e => e.ParentCategory)
                    .WithMany(e => e.ChildCategories)
                    .HasForeignKey(e => new { e.BID, e.ParentID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Material.Category_Part.Machine.Category");
            });

            modelBuilder.Entity<MaterialCategoryLink>(entity =>
            {
                entity.ToTable("Part.Material.CategoryLink");

                entity.HasKey(e => new { e.BID, e.PartID, e.CategoryID }).HasName("PK_Part.Material.CategoryLink");

                entity.HasIndex(e => new { e.BID, e.CategoryID, e.PartID })
                    .HasName("IX_Part.Material.CategoryLink_Category");

                entity.HasOne(e => e.Material)
                    .WithMany(e => e.MaterialCategoryLinks)
                    .HasForeignKey(e => new { e.BID, e.PartID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Material.CategoryLink_Part.Material.Data");
                entity.HasOne(e => e.MaterialCategory)
                    .WithMany(e => e.MaterialCategoryLinks)
                    .HasForeignKey(e => new { e.BID, e.CategoryID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Material.CategoryLink_Part.Material.Category");
            });

            modelBuilder.Entity<EnumOrderOrderStatus>(entity =>
            {
                entity.ToTable("enum.Order.OrderStatus");

                entity.HasKey(e => new { e.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.TransactionTypeText).IsUnicode(false)
                    .HasComputedColumnSql("(case [TransactionType] when(1) then 'Estimate' when(2) then 'Order' when(4) then 'Purchase Order' when(8) then 'Credit Memo' when(32) then 'Opportunity' when(64) then 'Destination' else 'Unknown' end)");
                entity.Property(e => e.EnableCustomItemStatus)
                    .HasComputedColumnSql("(isnull(case when [FixedItemStatusID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,CONVERT([bit],(0))))");
                entity.Property(e => e.FixedItemStatusID).HasColumnType("tinyint");

                entity.HasOne<EnumOrderOrderStatus>()
                    .WithMany()
                    .HasForeignKey(t => t.FixedItemStatusID)
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumOrderTransactionType>()
                    .WithMany()
                    .HasForeignKey(t => t.TransactionType)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<EnumOrderDestinationType>(entity =>
            {
                entity.ToTable("enum.Order.DestinationType");

                entity.HasKey(e => new { e.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.AppliesToEstimate).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.AppliesToOrder).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.AppliesToPO).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsHeaderLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsDestinationLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsItemLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.TransactionTypeSet).HasColumnType("tinyint");
                entity.Property(e => e.TransactionLevelSet).HasColumnType("tinyint");
            });

            modelBuilder.Entity<EnumOrderTransactionType>(entity =>
            {
                entity.ToTable("enum.Order.TransactionType");

                entity.HasKey(e => new { e.ID });

                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumOrderTransactionLevel>(entity =>
            {
                entity.ToTable("enum.Order.TransactionLevel");

                entity.HasKey(e => new { e.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumOrderKeyDateType>(entity =>
            {
                entity.ToTable("enum.Order.KeyDateType");

                entity.HasKey(e => new { e.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.TransactionTypeSet).HasColumnType("tinyint");
                entity.Property(e => e.TransactionLevelSet).HasColumnType("tinyint");
                entity.Property(e => e.AppliesToEstimate).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.AppliesToOrder).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.AppliesToPO).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsHeaderLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsDestinationLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsItemLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
            });

            modelBuilder.Entity<EnumOrderNoteType>(entity =>
            {
                entity.ToTable("enum.Order.NoteType");

                entity.HasKey(e => new { e.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
                entity.Property(e => e.TransactionTypeSet).HasColumnType("tinyint");
                entity.Property(e => e.TransactionLevelSet).HasColumnType("tinyint");
                entity.Property(e => e.AppliesToEstimate).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.AppliesToOrder).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.AppliesToPO).HasComputedColumnSql("(isnull(case when([TransactionTypeSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsHeaderLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsDestinationLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsItemLevel).HasComputedColumnSql("(isnull(case when([TransactionLevelSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))");
            });

            modelBuilder.Entity<TransactionHeaderData>(entity =>
            {
                entity.ToTable("Order.Data");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("(case [TransactionType] when (2) then (10000) when (1) then (10200) when (8) then (10300) when (4) then (10100) else (0) end)");

                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(7)").HasDefaultValueSql($"SYSUTCDATETIME()").IsRequired().ValueGeneratedOnAddOrUpdate();
                entity.Property(e => e.CostTotal).HasColumnName("Cost.Total")
                    .HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("(isnull(((isnull([Cost.Material],(0.0))+isnull([Cost.Labor],(0.0)))+isnull([Cost.Machine],(0.0)))+isnull([Cost.Other],(0.0)),(0.0)))");
                entity.Property(e => e.OrderPONumber).IsUnicode(false).HasColumnType("varchar(32)");
                entity.Property(e => e.CreditMemoBalance).HasColumnName("CreditMemo.Balance")
                    .HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("([CreditMemo.Credit]-coalesce([CreditMemo.Used],(0.0)))");
                entity.Property(e => e.CreditMemoHasBalance).HasColumnName("CreditMemo.HasBalance")
                    .HasComputedColumnSql("(isnull(CONVERT([bit],  abs(sign(coalesce( [CreditMemo.Credit], (0)) - coalesce([CreditMemo.Used], (0))))),(0)))");
                entity.Property(e => e.FormattedNumber).HasColumnType("varchar(32)");
                entity.Property(e => e.InvoiceNumber).HasColumnType("int");
                entity.Property(e => e.OrderStatusStartDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql($"GetUTCDate()").IsRequired();
                entity.Property(e => e.OrderStatusID).HasColumnType("tinyint");
                entity.Property(e => e.PaymentBalanceDue).HasColumnName("Payment.BalanceDue")
                    .HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("(((((((([Price.ProductTotal] - isnull([Price.Discount], (0))) + isnull([Price.DestinationTotal], (0))) + isnull([Price.FinanceCharge], (0))) + [Price.Tax]) - isnull([Payment.Paid],(0))) - isnull([Payment.WriteOff],(0))) - isnull([CreditMemo.Applied], (0))) + isnull([CreditMemo.Credit],(0)))");
                entity.Property(e => e.PriceProductTotal).HasColumnName("Price.ProductTotal")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceDestinationTotal).HasColumnName("Price.DestinationTotal")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceFinanceCharge).HasColumnName("Price.FinanceCharge")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceDiscountPercent).HasColumnName("Price.DiscountPercent")
                    .HasColumnType("DECIMAL(9,4)");
                entity.Property(e => e.PriceTaxableOV).HasColumnName("Price.TaxableOV");
                entity.Property(e => e.DestinationType).HasColumnType("tinyint");

                entity.Property(e => e.PaymentPaid).HasColumnName("Payment.Paid")
                    .HasColumnType("DECIMAL(18,4)").HasDefaultValue(0m);
                entity.Property(e => e.PaymentAuthorized).HasColumnName("Payment.Authorized")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PaymentWriteOff).HasColumnName("Payment.WriteOff")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CreditMemoApplied).HasColumnName("CreditMemo.Applied")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CreditMemoCredit).HasColumnName("CreditMemo.Credit")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CreditMemoUsed).HasColumnName("CreditMemo.Used")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PaymentTotal).HasColumnName("Payment.Total")
                    .HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("(isnull([Payment.Paid] + isnull([Payment.WriteOff],(0)) + IsNull([CreditMemo.Applied],0.0) ,(0.0)))");

                entity.Property(e => e.PriceIsLocked).HasColumnName("Price.IsLocked");
                entity.Property(e => e.PriceNet).HasColumnName("Price.Net")
                    .HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("[Price.ProductTotal] + IsNull([Price.DestinationTotal],(0)) + IsNull([Price.FinanceCharge],(0))");
                entity.Property(e => e.PriceDiscount).HasColumnName("Price.Discount")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PricePreTax).HasColumnName("Price.PreTax")
                    .HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("[Price.ProductTotal] + IsNull([Price.DestinationTotal],(0)) + IsNull([Price.FinanceCharge],(0)) - IsNull([Price.Discount],0)");
                entity.Property(e => e.PriceTaxable).HasColumnName("Price.Taxable")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceTaxRate).HasColumnName("Price.TaxRate")
                    .HasColumnType("DECIMAL(9,4)");
                entity.Property(e => e.PriceTax).HasColumnName("Price.Tax")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceTotal).HasColumnName("Price.Total")
                    .HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("[Price.ProductTotal] + IsNull([Price.DestinationTotal],(0)) + IsNull([Price.FinanceCharge],(0)) - IsNull([Price.Discount],0) + [Price.Tax]");
                entity.Property(t => t.TransactionType).HasColumnType("tinyint").HasColumnName("TransactionType");

                entity.Property(e => e.CostLabor).HasColumnName("Cost.Labor")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostMachine).HasColumnName("Cost.Machine")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostMaterial).HasColumnName("Cost.Material")
                    .HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostOther).HasColumnName("Cost.Other")
                    .HasColumnType("DECIMAL(18,4)");

                entity.Property(e => e.Priority).HasColumnType("smallint");
                entity.Property(e => e.IsUrgent).HasColumnType("bit");
                entity.Property(e => e.ItemCount).HasColumnType("smallint");

                entity.Ignore(e => e.SimpleDestinations);
                entity.Ignore(e => e.SimpleItems);
                entity.Ignore(e => e.FromQuickItemID);

                entity.HasOne(t => t.Location)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.LocationID })
                    .HasConstraintName("FK_Order.Data_LocationID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Opportunity)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.OpportunityID })
                    .HasConstraintName("FK_Order.Data_OpportunityID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.ProductionLocation)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.ProductionLocationID })
                    .HasConstraintName("FK_Order.Data_ProdLocationID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.PickupLocation)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.PickupLocationID })
                    .HasConstraintName("FK_Order.Data_PickupLocationID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Company)
                    .WithMany(t => t.Orders).IsRequired()
                    .HasForeignKey(t => new { t.BID, t.CompanyID })
                    .HasConstraintName("FK_Order.Data_CompanyID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.TaxGroup)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxGroupID })
                    .HasConstraintName("FK_Order.Data_TaxGroupID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Origin)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.OriginID })
                    .HasConstraintName("FK_Order.Data_OriginID")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.EnumOrderDestinationType)
                    .WithMany()
                    .HasForeignKey(t => t.DestinationType)
                    .HasConstraintName("FK_Order.Data_DestinationType")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.EnumOrderOrderStatus)
                    .WithMany()
                    .HasForeignKey(t => t.OrderStatusID)
                    .HasConstraintName("FK_Order.Data_OrderStatus")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumOrderTransactionType>()
                    .WithMany()
                    .HasForeignKey(t => t.TransactionType)
                    .HasConstraintName("FK_Order.Data_TransactionType")
                    .OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<FlatListItem>(x => x.TaxExemptReason)
                    .WithMany()
                    .HasForeignKey(d => new { d.BID, d.TaxExemptReasonID })
                    .HasConstraintName("FK_Order.Data_TaxExemptReason");

                entity.HasIndex(t => new { t.BID, t.Number, t.TransactionType })
                    .HasName("IX_Order.Data_Number");
                entity.HasIndex(t => new { t.BID, t.InvoiceNumber })
                    .HasName("IX_Order.Data_InvoiceNumber");
                entity.HasIndex(t => new { t.BID, t.CompanyID })
                    .HasName("IX_Order.Data_CompanyID");
                entity.HasIndex(t => new { t.BID, t.OpportunityID })
                    .HasName("IX_Order.Data_OpportunityID");
                entity.HasIndex(t => new { t.BID, t.OrderStatusID })
                    .HasName("IX_Order.Data_OrderStatusID");
                entity.HasIndex(t => new { t.BID, t.LocationID, t.OrderStatusID, t.Number })
                    .HasName("IX_Order.Data_LocationID");
                entity.HasIndex(t => new { t.BID, t.ProductionLocationID, t.OrderStatusID, t.Number })
                    .HasName("IX_Order.Data_ProdLocationID");
                entity.HasIndex(t => new { t.BID, t.TaxExemptReasonID })
                    .HasName("IX_Order.Data_TaxExemptReasonID");

                entity.HasDiscriminator(t => t.TransactionType)
                    .HasValue<EstimateData>((byte)OrderTransactionType.Estimate)
                    .HasValue<OrderData>((byte)OrderTransactionType.Order)
                    .HasValue<PurchaseOrderData>((byte)OrderTransactionType.PurchaseOrder)
                    .HasValue<CreditMemoData>((byte)OrderTransactionType.Memo);

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<OpportunityCustomData>(entity =>
            {
                entity.ToTable("Opportunity.Custom.Data");
                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((9001))");

                entity.Property(e => e.ModifiedDT)
                    .HasColumnType("datetime2(7)")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.AppliesToClassTypeID).HasDefaultValueSql("((9000))");

                entity.Property(e => e.DataXML)
                    .HasColumnType("xml");

                entity.HasIndex(e => e.DataXML).HasName("XML_IX_Opportunity.Custom.Data");

                entity.HasOne(t => t.Opportunity)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.ID })
                    .HasConstraintName("FK_Opportunity.Custom.Data_Opportunity.Data_BID_ID")
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<OrderCustomData>(entity =>
            {
                entity.ToTable("Order.Custom.Data");
                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((10001))");

                entity.Property(e => e.ModifiedDT)
                    .HasColumnType("DATETIME2(7)")
                    .HasDefaultValueSql($"SYSUTCDATETIME()");

                entity.Property(e => e.AppliesToClassTypeID).HasDefaultValueSql("((10000))");

                entity.Property(e => e.DataXML)
                    .HasColumnType("xml");

                entity.HasIndex(e => e.DataXML)
                    .HasName("XML_IX_Order.Custom.Data");

                entity.HasOne(t => t.Order)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.ID })
                    .HasConstraintName("FK_Order.Custom.Data_Order.Data_BID_ID");

            });

            modelBuilder.Entity<OrderItemStatus>(entity =>
            {
                entity.ToTable("Order.Item.Status");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.OrderItemStatus.ID()}");

                entity.Property(e => e.ModifiedDT)
                    .HasColumnType("DATETIME2(7)")
                    .HasDefaultValueSql("SYSUTCDATETIME()"); //DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL
                                                             //entity.Property(e => e.ValidToDT   DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL
                entity.Property(e => e.IsActive);//.HasComputedColumnSql("(isnull(CONVERT([bit],(1)),(1)))");
                entity.Property(e => e.IsSystem); //.HasComputedColumnSql("(isnull(CONVERT([bit],(1)),(1)))");
                entity.Property(e => e.IsDefault); //.HasComputedColumnSql("(isnull(CONVERT([bit],(1)),(1)))");

                entity.Property(e => e.StatusIndex);
                entity.Property(e => e.Name)
                    .HasColumnType("NVARCHAR(100)");
                entity.Property(e => e.CustomerPortalText)
                    .HasColumnType("NVARCHAR(255)");
                entity.Property(e => e.Description)
                    .HasColumnType("NVARCHAR(255)");
                entity.Property(e => e.TransactionType)
                    .HasColumnType("tinyint");
                entity.Property(e => e.OrderStatusID)
                    .HasColumnType("tinyint");

                //primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Order.Item.Status");

                //indexes
                entity.HasIndex(e => new { e.BID, e.OrderStatusID, e.StatusIndex, e.Name }).HasName("IX_Order.Item.Status_OrderStatusID");
                entity.HasIndex(e => new { e.BID, e.OrderStatusID, e.Name }).HasName("IX_Order.Item.Status_Name");

                //foreign keys //review
                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Order.Item.Status_BID");

                entity.HasOne<EnumOrderTransactionType>().WithMany()
                    .HasForeignKey(e => e.TransactionType)
                    .HasConstraintName("FK_Order.Item.Status_TransactionType");

                entity.HasOne<EnumOrderOrderStatus>().WithMany()
                    .HasForeignKey(e => e.OrderStatusID)
                    .HasConstraintName("FK_Order.Item.Status_OrderStatus");

                entity.Ignore("OrderItemSubStatuses");
                entity.Ignore("SimpleOrderItemSubStatuses");
            });

            modelBuilder.Entity<OrderItemSubStatus>(entity =>
            {
                entity.ToTable("Order.Item.SubStatus");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((10023))");

                entity.Property(e => e.ModifiedDT)
                    .HasColumnType("DATETIME2(7)")
                    .HasDefaultValueSql("SYSUTCDATETIME()")
                    .IsRequired()
                    .ValueGeneratedOnAddOrUpdate();

                //shadow properties
                entity.Property<DateTime>("ValidToDT")
                    .HasColumnType("DATETIME2(7)")
                    .IsRequired()
                    .HasDefaultValueSql("CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property("Name")
                    .HasColumnType("NVARCHAR(100)");
                entity.Property("Description")
                    .HasColumnType("NVARCHAR(255)");

                //primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Order.Item.SubStatus");

                //indexes
                entity.HasIndex(e => new { e.BID, e.Name }).HasName("X_Order.Item.SubStatus_Name").IsUnique();

                //foreign keys
                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Order.Item.SubStatus_BID");

                entity.Ignore("OrderItemStatuses");
                entity.Ignore("SimpleOrderItemStatuses");
            });

            modelBuilder.Entity<OrderItemStatusSubStatusLink>(entity =>
            {
                entity.ToTable("Order.Item.Status.SubStatusLink");

                //primary key
                entity.HasKey(e => new { e.BID, e.StatusID, e.SubStatusID }).HasName("PK_Order.Item.Status.SubStatusLink");

                //indexes
                entity.HasIndex(e => new { e.BID, e.SubStatusID, e.StatusID }).HasName("IX_Order.Item.Status.SubStatusLink_SubStatus");

                //foreign keys
                entity.HasOne(e => e.OrderItemStatus)
                    .WithMany(OrderItemStatus => OrderItemStatus.OrderItemStatusSubStatusLinks)
                    .HasPrincipalKey(OrderItemStatus => new { OrderItemStatus.BID, OrderItemStatus.ID })
                    .HasForeignKey(e => new { e.BID, e.StatusID })
                    .HasConstraintName("FK_Order.Item.Status.SubStatusLink_Status");

                entity.HasOne(e => e.OrderItemSubStatus)
                    .WithMany(OrderItemSubStatus => OrderItemSubStatus.OrderItemStatusSubStatusLinks)
                    .HasPrincipalKey(OrderItemSubStatus => new { OrderItemSubStatus.BID, OrderItemSubStatus.ID })
                    .HasForeignKey(e => new { e.BID, e.SubStatusID })
                    .HasConstraintName("FK_Order.Item.Status.SubStatusLink_SubStatus");
            });

            modelBuilder.Entity<OrderKeyDate>(entity =>
            {
                entity.ToTable("Order.KeyDate");
                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.OrderItemTempID)
                    .HasColumnType("nvarchar(max)");
                entity.Property(e => e.OrderID)
                    .HasColumnType("int");
                entity.Property(e => e.KeyDateType)
                    .HasColumnType("tinyint");
                entity.Property(e => e.KeyDT)
                    .HasColumnType("datetime2(2)");
                entity.Property(e => e.IsFirmDate)
                    .HasColumnType("bit");
                entity.Property(e => e.DestinationID)
                    .HasColumnType("int");
                entity.Property(e => e.OrderItemID)
                    .HasColumnType("int");
                entity.Property(e => e.ClassTypeID)
                    .HasComputedColumnSql($"{ClassType.OrderKeyDate.ID()}");
                entity.Property(e => e.ModifiedDT)
                    .HasColumnType("DATETIME2(7)")
                    .HasDefaultValueSql($"SYSUTCDATETIME()")
                    .IsRequired()
                    .ValueGeneratedOnAddOrUpdate();
                entity.Property(e => e.IsDestinationDate)
                    .HasColumnType("bit")
                    .HasComputedColumnSql("(isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.IsOrderItemDate)
                    .HasColumnType("bit")
                    .HasComputedColumnSql("(isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.KeyDate)
                    .HasColumnType("date")
                    .HasComputedColumnSql("(CONVERT([date],[KeyDT]))");
                entity.Property(e => e.KeyTime)
                    .HasColumnType("time")
                    .HasComputedColumnSql("(CONVERT([time],[KeyDT]))");

                entity.HasOne(t => t.Order).WithMany(t => t.Dates).HasForeignKey(t => new { t.BID, t.OrderID }).HasConstraintName("FK_Order.KeyDate_OrderID").OnDelete(DeleteBehavior.Restrict);
                // Not implemented yet
                entity.HasOne(t => t.OrderItem).WithMany(t => t.Dates).HasForeignKey(t => new { t.BID, t.OrderItemID }).HasConstraintName("FK_Order.KeyDate_OrderItemID").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Destination).WithMany(t => t.Dates).HasForeignKey(t => new { t.BID, t.DestinationID }).HasConstraintName("FK_Order.KeyDate_DestinationID").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumOrderKeyDateType>().WithMany().HasForeignKey(t => t.KeyDateType).HasConstraintName("FK_Order.KeyDate_DateType").OnDelete(DeleteBehavior.Restrict);

                entity.HasIndex(t => new { t.BID, t.OrderID, t.KeyDateType, t.KeyDT }).HasName("IX_Order.KeyDate_OrderID");
                entity.HasIndex(t => new { t.BID, t.OrderItemID, t.KeyDateType, t.KeyDT }).HasName("IX_Order.KeyDate_OrderItemID");
                entity.HasIndex(t => new { t.BID, t.OrderItemID, t.KeyDateType, t.KeyDT }).HasName("IX_Order.KeyDate_DestinationID");
                entity.HasIndex(t => new { t.BID, t.KeyDateType, t.KeyDT, t.OrderID }).HasName("IX_Order.KeyDate_KeyDateID");
            });

            modelBuilder.Entity<OrderNote>(entity =>
            {
                entity.ToTable("Order.Note");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID)
                    .HasComputedColumnSql($"{ClassType.OrderNote.ID()}");
                entity.Property(e => e.ModifiedDT)
                    .HasColumnType("DATETIME2(7)")
                    .HasDefaultValueSql($"SYSUTCDATETIME()")
                    .IsRequired()
                    .ValueGeneratedOnAddOrUpdate();
                entity.Property(e => e.Note)
                    .HasColumnType("VARCHAR(MAX)");
                entity.Property(e => e.IsOrderNote).HasComputedColumnSql("(isnull(case when [OrderItemID] IS NULL AND [DestinationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsDestinationNote).HasComputedColumnSql("(isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.IsOrderItemNote).HasComputedColumnSql("(isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

                entity.HasOne(t => t.Order).WithMany(t => t.Notes).HasForeignKey(t => new { t.BID, t.OrderID }).HasConstraintName("FK_Order.Note_OrderID").OnDelete(DeleteBehavior.Restrict);
                // Not implemented yet
                entity.HasOne(t => t.OrderItem).WithMany(t => t.Notes).HasForeignKey(t => new { t.BID, t.OrderItemID }).HasConstraintName("FK_Order.Note_OrderItemID").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Destination).WithMany(t => t.Notes).HasForeignKey(t => new { t.BID, t.DestinationID }).HasConstraintName("FK_Order.Note_DestinationID").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumOrderNoteType>().WithMany().HasForeignKey(t => t.NoteType).HasConstraintName("FK_Order.Note_NoteType").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Employee).WithMany().HasForeignKey(t => new { t.BID, t.EmployeeID }).HasConstraintName("FK_Order.Note_EmployeeID").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Contact).WithMany().HasForeignKey(t => new { t.BID, t.ContactID }).HasConstraintName("FK_Order.Note_ContactID").OnDelete(DeleteBehavior.Restrict);

                entity.HasIndex(t => new { t.BID, t.OrderID, t.NoteType }).HasName("IX_Order.Note_OrderID");
                entity.HasIndex(t => new { t.BID, t.OrderItemID, t.NoteType }).HasName("IX_Order.Note_OrderItemID");
                entity.HasIndex(t => new { t.BID, t.DestinationID, t.NoteType }).HasName("IX_Order.Note_DestinationID");
            });

            modelBuilder.Entity<SimpleOrderData>(entity =>
            {
                entity.ToTable("Order.SimpleList");

                MapSimpleListItem<SimpleOrderData, int>(entity, ClassType.Order.AsString());
            });

            modelBuilder.Entity<OrderOrderLink>(entity =>
            {
                entity.ToTable("Order.OrderLink");
                entity.HasKey(t => new { t.BID, t.OrderID, t.LinkType, t.LinkedOrderID });

                entity.Property(e => e.ModifiedDT)
                    .HasColumnType("DATETIME2(7)")
                    .HasDefaultValueSql("SYSUTCDATETIME()"); //DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL
                                                             //entity.Property(e => e.ValidToDT   DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL

                entity.Property(e => e.Amount)
                    .HasColumnType("decimal(18,4)");
                entity.Property(e => e.LinkedFormattedNumber).IsRequired();// VARCHAR(12) NOT NULL
                entity.Property(e => e.Description).IsRequired();// VARCHAR(255) NOT NULL
                entity.HasOne(t => t.Order).WithMany(t => t.Links).HasConstraintName("FK_Order.OrderLink_OrderID").IsRequired().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.LinkedOrder).WithMany().HasConstraintName("FK_Order.OrderLink_LinkedOrderID").IsRequired().OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Employee).WithMany().HasConstraintName("FK_Order.OrderLink_EmployeeID").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumOrderOrderLinkType>()
                    .WithMany()
                    .HasConstraintName("FK_Order.OrderLink_LinkType")
                    .HasForeignKey(t => t.LinkType)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.Order)
                    .WithMany(t => t.Links)
                    .HasForeignKey(t => new { t.BID, t.OrderID })
                    .HasConstraintName("FK_Order.OrderLink_OrderID");

                entity.HasOne(t => t.LinkedOrder)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.LinkedOrderID })
                    .HasConstraintName("FK_Order.OrderLink_LinkedOrderID");

                entity.HasOne(t => t.Employee)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.EmployeeID })
                    .HasConstraintName("FK_Order.OrderLink_EmployeeID");
            });

            modelBuilder.Entity<EnumOrderOrderLinkType>(entity =>
            {
                entity.ToTable("enum.Order.OrderLinkType");
                entity.HasKey(e => e.ID);

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(t => t.Name).IsRequired().IsUnicode(false);
                entity.Property(e => e.SymmetricLinkType).HasColumnType("tinyint");
                entity.Property(e => e.TransactionTypeSet).HasColumnType("tinyint");
                entity.Property(e => e.TransactionLevelSet).HasColumnType("tinyint");
                entity.Property(t => t.AppliesToEstimate).HasComputedColumnSql("(isnull(case when ([TransactionTypeSet]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.AppliesToOrder).HasComputedColumnSql("(isnull(case when ([TransactionTypeSet]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.AppliesToPO).HasComputedColumnSql("(isnull(case when ([TransactionTypeSet]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.IsHeaderLevel).HasComputedColumnSql("(isnull(case when ([TransactionLevelSet]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.IsDestinationLevel).HasComputedColumnSql("(isnull(case when ([TransactionLevelSet]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.IsItemLevel).HasComputedColumnSql("(isnull(case when ([TransactionLevelSet]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
            });

            modelBuilder.Entity<EnumOrderContactRoleType>(entity =>
            {
                entity.ToTable("enum.Order.ContactRoleType");
                entity.HasKey(e => e.ID);

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(t => t.Name).IsRequired().IsUnicode(false);
                entity.Property(e => e.TransactionTypeSet).HasColumnType("tinyint");
                entity.Property(e => e.TransactionLevelSet).HasColumnType("tinyint");
                entity.Property(t => t.AppliesToEstimate).HasColumnType("bit").HasComputedColumnSql("(isnull(case when ([TransactionTypeSet]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.AppliesToOrder).HasColumnType("bit").HasComputedColumnSql("(isnull(case when ([TransactionTypeSet]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.AppliesToPO).HasColumnType("bit").HasComputedColumnSql("(isnull(case when ([TransactionTypeSet]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.IsHeaderLevel).HasColumnType("bit").HasComputedColumnSql("(isnull(case when ([TransactionLevelSet]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.IsDestinationLevel).HasColumnType("bit").HasComputedColumnSql("(isnull(case when ([TransactionLevelSet]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(t => t.IsItemLevel).HasColumnType("bit").HasComputedColumnSql("(isnull(case when ([TransactionLevelSet]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
            });

            modelBuilder.Entity<OrderContactRoleLocator>(entity =>
            {
                entity.ToTable("Order.Contact.Locator");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ModifiedDT)
                    .HasColumnType("DATETIME2(7)")
                    .HasDefaultValueSql($"SYSUTCDATETIME()")
                    .IsRequired()
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasIndex(e => new { e.BID, e.ParentID })
                    .HasName("IX_OrderContactRole.Locator_Parent");

                entity.HasIndex(e => new { e.BID, e.LocatorType, e.LocatorSubType, e.ParentID })
                    .HasName("IX_OrderContactRole.Locator");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((10016))");

                MapLocator<OrderContactRoleLocator, int, OrderContactRole>(entity);

                entity.HasOne(d => d.LocatorTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.LocatorType)
                    //  .OnDelete(DeleteBehavior.NoAction)
                    .HasConstraintName("FK_OrderContactRole.Locator_enum.LocatorType");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.OrderContactRoleLocators)
                    .HasForeignKey(d => new { d.BID, d.ParentID })
                    //  .OnDelete(DeleteBehavior.NoAction)
                    .HasConstraintName("FK_OrderContactRole.Locator_OrderContactRole.Data");

                entity.HasOne(d => d.LocatorSubTypeNavigation)
                    .WithMany()
                    .HasForeignKey(d => new { d.LocatorType, d.LocatorSubType })
                    .HasConstraintName("FK_OrderContactRole.Locator_enum.LocatorSubType");
            });

            modelBuilder.Entity<OrderContactRole>(entity =>
            {
                entity.ToTable("Order.Contact.Role");
                entity.HasKey(t => new { t.BID, t.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.OrderContactRole.ID()}");

                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("SYSUTCDATETIME()"); //DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL
                                                                                           //entity.Property(e => e.ValidToDT   DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL

                entity.Property(e => e.IsOrderRole).HasComputedColumnSql($"(isnull(case when [OrderItemID] IS NULL AND [DestinationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsOrderItemRole).HasComputedColumnSql($"(isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.IsDestinationRole).HasComputedColumnSql($"(isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

                entity.HasIndex(e => new { e.BID, e.OrderID, e.RoleType, e.ContactID, e.IsOrderRole })
                    .HasName("IX_Order.Contact.Role_OrderID");
                entity.HasIndex(e => new { e.BID, e.OrderItemID, e.RoleType, e.ContactID })
                    .HasName("IX_Order.Contact.Role_OrderItemID");
                //.HasFilter("IsOrderItemRole = 1");
                entity.HasIndex(e => new { e.BID, e.DestinationID, e.RoleType, e.ContactID })
                    .HasName("IX_Order.Contact.Role_DestinationID");
                //.HasFilter("IsDestinationRole = 1");
                entity.HasIndex(e => new { e.BID, e.ContactID, e.OrderID, e.RoleType })
                    .HasName("IX_Order.Contact.Role_ContactID");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.ContactRoles)
                    .HasForeignKey(d => new { d.BID, d.OrderID })
                    .HasConstraintName("FK_Order.Contact.Role_OrderID");

                entity.HasOne(d => d.OrderItem)
                    .WithMany(p => p.ContactRoles)
                    .HasForeignKey(d => new { d.BID, d.OrderItemID })
                    .HasConstraintName("FK_Order.Contact.Role_OrderItemID");

                entity.HasOne(d => d.Destination)
                    .WithMany(p => p.ContactRoles)
                    .HasForeignKey(d => new { d.BID, d.DestinationID })
                    .HasConstraintName("FK_Order.Contact.Role_DestinationID");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.OrderRoles)
                    .HasForeignKey(d => new { d.BID, d.ContactID })
                    .HasConstraintName("FK_Order.Contact.Role_ContactID");

                entity.HasOne<EnumOrderContactRoleType>()
                    .WithMany()
                    .HasConstraintName("FK_Order.Contact.Role_RoleType")
                    .HasForeignKey(t => t.RoleType)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<OrderEmployeeRole>(entity =>
            {
                entity.ToTable("Order.Employee.Role");
                entity.HasKey(t => new { t.BID, t.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.OrderEmployeeRole.ID()}");

                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("SYSUTCDATETIME()"); //DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL
                                                                                           //entity.Property(e => e.ValidToDT   DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL

                entity.Property(e => e.IsOrderRole).HasComputedColumnSql($"(isnull(case when [OrderItemID] IS NULL AND [DestinationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsOrderItemRole).HasComputedColumnSql($"(isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.IsDestinationRole).HasComputedColumnSql($"(isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

                entity.HasIndex(e => new { e.BID, e.OrderID, e.RoleID, e.EmployeeID, e.IsOrderRole })
                    .HasName("IX_Order.Employee.Role_OrderID");
                entity.HasIndex(e => new { e.BID, e.OrderItemID, e.RoleID, e.EmployeeID })
                    .HasName("IX_Order.Employee.Role_OrderItemID");
                //.HasFilter("IsOrderItemRole = 1");
                entity.HasIndex(e => new { e.BID, e.DestinationID, e.RoleID, e.EmployeeID })
                    .HasName("IX_Order.Employee.Role_DestinationID");
                //.HasFilter("IsDestinationRole = 1");
                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.OrderID, e.RoleID })
                    .HasName("IX_Order.Employee.Role_EmployeeID");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.EmployeeRoles)
                    .HasForeignKey(d => new { d.BID, d.OrderID })
                    .HasConstraintName("FK_Order.Employee.Role_OrderID");

                entity.HasOne(d => d.OrderItem)
                    .WithMany(p => p.EmployeeRoles)
                    .HasForeignKey(d => new { d.BID, d.OrderItemID })
                    .HasConstraintName("FK_Order.Employee.Role_OrderItemID");

                entity.HasOne(d => d.Destination)
                    .WithMany(p => p.EmployeeRoles)
                    .HasForeignKey(d => new { d.BID, d.DestinationID })
                    .HasConstraintName("FK_Order.Employee.Role_DestinationID");

                entity.HasOne(d => d.Employee)
                    .WithMany(p => p.OrderRoles)
                    .HasForeignKey(d => new { d.BID, d.EmployeeID })
                    .HasConstraintName("FK_Order.Employee.Role_EmployeeID");

                entity.HasOne(d => d.Role)
                    .WithMany()
                    .HasConstraintName("FK_Order.Employee.Role_Employee.Role")
                    .HasForeignKey(t => new { t.BID, t.RoleID })
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<OrderItemData>(entity =>
            {
                entity.ToTable("Order.Item.Data");
                entity.HasKey(t => new { t.BID, t.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.OrderItem.ID()}");
                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("SYSUTCDATETIME()");
                entity.Property(e => e.PriceUnitOV).HasColumnName("Price.UnitOV").HasDefaultValue(false).IsRequired();
                entity.Property(e => e.Quantity).HasColumnType("decimal(18,6)");

                entity.Property(e => e.PriceList).HasColumnName("Price.List").HasColumnType("DECIMAL(9,4)").HasComputedColumnSql("([Price.Component]+[Price.Surcharge])");
                entity.Property(e => e.PriceDiscountPercent).HasColumnName("Price.DiscountPercent").HasColumnType("DECIMAL(9,4)").HasComputedColumnSql("CASE WHEN IsNull( [Price.ItemDiscountPercent],  [Price.AppliedOrderDiscountPercent]) IS NOT NULL THEN 100.0 - ( 100.0 - ISNULL( [Price.ItemDiscountPercent], 0.00) ) * ( 100.0 - ISNULL( [Price.AppliedOrderDiscountPercent], 0.00) ) / 100.0 ELSE NULL END");
                entity.Property(e => e.PriceDiscount).HasColumnName("Price.Discount").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("(isnull( [Price.ItemDiscount], (0.0)) + isnull( [Price.AppliedOrderDiscount], (0.0)))");
                entity.Property(e => e.PricePreTax).HasColumnName("Price.PreTax").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("((([Price.Component]+[Price.Surcharge])-isnull([Price.ItemDiscount],(0.0)))-isnull([Price.AppliedOrderDiscount],(0.0)))");
                entity.Property(e => e.PriceUnitPreTax).HasColumnName("Price.UnitPreTax").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("(((([Price.Component]+[Price.Surcharge])-isnull([Price.ItemDiscount],(0.0)))-isnull([Price.AppliedOrderDiscount],(0.0)))/case when [Quantity]=(0.0) then NULL else [Quantity] end)");
                entity.Property(e => e.PriceTotal).HasColumnName("Price.Total").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("(((([Price.Component]+[Price.Surcharge])-isnull([Price.ItemDiscount],(0.0)))-isnull([Price.AppliedOrderDiscount],(0.0)))+[Price.Tax])");
                entity.Property(e => e.CostTotal).HasColumnName("Cost.Total").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql("(isnull(((isnull( [Cost.Material],(0.0)) + isnull([Cost.Labor],(0.0))) + isnull([Cost.Machine],(0.0))) + isnull([Cost.Other],(0.0)),(0.0)))");
                entity.Property(e => e.OrderStatusID).HasColumnType("tinyint");
                entity.Property(e => e.ItemStatusID).HasColumnType("smallint");
                entity.Property(e => e.SubStatusID).HasColumnType("smallint");
                entity.Property(e => e.ProductionLocationID).HasColumnType("tinyint");
                entity.Property(e => e.PriceIsLocked).HasColumnName("Price.IsLocked").HasColumnType("bit");
                entity.Property(e => e.PriceComponent).HasColumnName("Price.Component").HasColumnType("DECIMAL(9,4)");
                entity.Property(e => e.PriceSurcharge).HasColumnName("Price.Surcharge").HasColumnType("DECIMAL(9,4)");
                entity.Property(e => e.PriceItemDiscountPercent).HasColumnName("Price.ItemDiscountPercent").HasColumnType("DECIMAL(9,4)");
                entity.Property(e => e.PriceAppliedOrderDiscountPercent).HasColumnName("Price.AppliedOrderDiscountPercent").HasColumnType("DECIMAL(9,4)");
                entity.Property(e => e.PriceItemDiscount).HasColumnName("Price.ItemDiscount").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceAppliedOrderDiscount).HasColumnName("Price.AppliedOrderDiscount").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceTaxable).HasColumnName("Price.Taxable").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceTaxableOV).HasColumnName("Price.TaxableOV").HasColumnType("bit");
                entity.Property(e => e.PriceTax).HasColumnName("Price.Tax").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.TaxGroupID).HasColumnType("smallint");
                entity.Property(e => e.TaxExemptReasonID).HasColumnType("smallint");
                entity.Property(e => e.CostMaterial).HasColumnName("Cost.Material").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostLabor).HasColumnName("Cost.Labor").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostMachine).HasColumnName("Cost.Machine").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostOther).HasColumnName("Cost.Other").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.TransactionType).HasColumnType("tinyint");
                entity.Property(e => e.CategoryID);

                entity.HasIndex(e => new { e.BID, e.OrderID, e.ItemNumber })
                    .HasName("IX_Order.Item.Data_OrderID");
                entity.HasIndex(e => new { e.BID, e.ItemStatusID, e.SubStatusID })
                    .HasName("IX_Order.Item.Data_Status");
                entity.HasIndex(e => new { e.BID, e.ProductionLocationID, e.OrderStatusID, e.ItemStatusID })
                    .HasName("IX_Order.Item.Data_ProductionLocation");

                entity.HasOne(t => t.Order).WithMany(p => p.Items)
                    .HasForeignKey(t => new { t.BID, t.OrderID })
                    .HasConstraintName("FK_Order.Item.Data_Order").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.EnumOrderOrderStatus).WithMany()
                    .HasForeignKey(t => new { t.OrderStatusID })
                    .HasConstraintName("FK_Order.Item.Data_OrderStatus").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.OrderItemStatus).WithMany()
                    .HasForeignKey(t => new { t.BID, t.ItemStatusID })
                    .HasConstraintName("FK_Order.Item.Data_ItemStatus").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.SubStatus).WithMany()
                    .HasForeignKey(t => new { t.BID, t.SubStatusID })
                    .HasConstraintName("FK_Order.Item.Data_SubStatus").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.TaxGroup).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxGroupID })
                    .HasConstraintName("FK_Order.Item.Data_TaxGroup").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.ProductionLocation).WithMany()
                    .HasForeignKey(t => new { t.BID, t.ProductionLocationID })
                    .HasConstraintName("FK_Order.Item.Data_ProdLoc").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.Category).WithMany()
                    .HasForeignKey(t => new { t.BID, t.CategoryID })
                    .HasConstraintName("FK_Order.Item.Data_CategoryID").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne<FlatListItem>(x => x.TaxExemptReason)
                    .WithMany()
                    .HasForeignKey(d => new { d.BID, d.TaxExemptReasonID })
                    .HasConstraintName("FK_Order.Item.Data_TaxExemptReason");

                entity.Ignore("TempID");
                entity.Ignore("FromQuickItemID");
                entity.Ignore("FromQuickItemLineItemNumber");

            });

            modelBuilder.Entity<OrderItemComponent>(b =>
            {
                b.ToTable("Order.Item.Component");

                b.HasKey(t => new { t.BID, t.ID });
                b.Property(e => e.ClassTypeID).HasComputedColumnSql($"(({ClassType.OrderItemComponent.ID()}))");
                b.Property(e => e.ModifiedDT).HasDefaultValueSql("SYSUTCDATETIME()");
                b.Property(e => e.QuantityUnit).HasColumnType("tinyint");

                b.Property(e => e.BID)
                 .HasColumnType("smallint");

                b.Property(e => e.ID)
                 .HasColumnType("int");

                b.Property(e => e.ParentComponentID)
                    .HasColumnType("INT");

                b.Property(e => e.Name)
                    .HasColumnType("NVARCHAR(255)");

                b.Property(e => e.VariableName)
                    .HasColumnType("NVARCHAR(255)");

                b.Property(e => e.TotalQuantity)
                    .HasColumnName("Quantity.Total")
                    .HasColumnType("DECIMAL(18,2)");

                b.Property(e => e.TotalQuantityOV)
                    .HasColumnName("Quantity.Total.OV")
                    .HasColumnType("BIT");

                b.Property(e => e.AssemblyQuantity)
                    .HasColumnName("Quantity.Assembly")
                    .HasColumnType("DECIMAL(18,2) NULL");

                b.Property(e => e.AssemblyQuantityOV)
                    .HasColumnName("Quantity.Assembly.OV")
                    .HasColumnType("BIT");

                b.Property(e => e.QuantityUnit)
                    .HasColumnName("Quantity.UnitID")
                    .HasColumnType("TINYINT");

                b.Property(e => e.Description)
                    .HasColumnType("NVARCHAR(2048)");

                b.Property(e => e.MaterialID)
                    .HasColumnType("INT")
                    .HasComputedColumnSql("(CASE WHEN ComponentType=12000 THEN ComponentID ELSE NULL END)")
                    .ValueGeneratedOnAddOrUpdate();

                b.Property(e => e.LaborID)
                    .HasColumnType("INT")
                    .HasComputedColumnSql("(CASE WHEN ComponentType=12020 THEN ComponentID ELSE NULL END)")
                    .ValueGeneratedOnAddOrUpdate();

                b.Property(e => e.MachineID)
                    .HasColumnType("SMALLINT")
                    .HasComputedColumnSql("(CASE WHEN ComponentType=12030 THEN Cast(ComponentID as SMALLINT) ELSE NULL END)")
                    .ValueGeneratedOnAddOrUpdate();

                b.Property(e => e.AssemblyID)
                    .HasColumnType("INT")
                    .HasComputedColumnSql("(CASE WHEN ComponentType=12040 THEN ComponentID ELSE NULL END)")
                    .ValueGeneratedOnAddOrUpdate();

                b.Property(e => e.DestinationID)
                    .HasColumnType("SMALLINT")
                    .HasComputedColumnSql("(CASE WHEN ComponentType=12080 THEN Cast(ComponentID as SMALLINT) ELSE NULL END)")
                    .ValueGeneratedOnAddOrUpdate();

                b.Property(e => e.AssemblyVariableID)
                    .HasColumnType("INT");

                b.Property(e => e.AssemblyDataJSON)
                    .HasColumnType("VARCHAR(MAX)");

                b.Property(e => e.ComponentType)
                    .HasColumnType("SMALLINT");

                b.Property(e => e.ComponentID)
                    .HasColumnType("INT");

                b.Property(e => e.PriceUnit)
                    .HasColumnName("Price.Unit")
                    .HasColumnType("DECIMAL(18,2)");

                b.Property(e => e.PriceUnitOV)
                    .HasColumnName("Price.UnitOV")
                    .HasColumnType("BIT");

                b.Property(e => e.PricePreTax)
                    .HasColumnName("Price.PreTax")
                    .HasColumnType("DECIMAL(18,2) NULL")
                    .HasComputedColumnSql("([Quantity.Total] * [Price.Unit])");

                b.Property(e => e.PricePerItem)
                    .HasColumnName("Price.PerItem")
                    .HasColumnType("DECIMAL(18,2) NULL")
                    .HasComputedColumnSql("([Price.Unit]*[Quantity.Assembly])");

                b.Property(e => e.PriceComputed)
                    .HasColumnName("Price.Computed")
                    .HasColumnType("DECIMAL(18,2)");

                b.Property(e => e.CostUnit)
                    .HasColumnName("Cost.Unit")
                    .HasColumnType("DECIMAL(18,2) NULL");

                b.Property(e => e.CostOV)
                    .HasColumnName("Cost.OV")
                    .HasColumnType("BIT");

                b.Property(e => e.CostNet)
                    .HasColumnName("Cost.Net")
                    .HasColumnType("DECIMAL(18,2) NULL")
                    .HasComputedColumnSql("([Quantity.Total] * [Cost.Unit])");

                b.Property(e => e.CostPerItem)
                    .HasColumnName("Cost.PerItem")
                    .HasColumnType("DECIMAL(18,2) NULL")
                    .HasComputedColumnSql("([Cost.Unit]*[Quantity.Assembly])");

                b.Property(e => e.PriceTaxable)
                    .HasColumnName("Price.Taxable")
                    .HasColumnType("DECIMAL(18,2) NULL");

                b.Property(e => e.PriceTax)
                    .HasColumnName("Price.Tax")
                    .HasColumnType("DECIMAL(18,2) NULL");
                
                b.Property(e => e.PriceTotal)
                    .HasColumnName("Price.Total")
                    .HasColumnType("DECIMAL(18,2) NULL")
                    .HasComputedColumnSql("(([Price.Unit]*[Quantity.Total]) + [Price.Tax])");

                b.HasIndex("BID", "AssemblyID")
                    .HasName("IX_Order.Item.Component_Assembly");

                b.HasIndex("BID", "LaborID")
                    .HasName("IX_Order.Item.Component_Labor");

                b.HasIndex("BID", "MachineID")
                    .HasName("IX_Order.Item.Component_Machine");

                b.HasIndex("BID", "MaterialID")
                    .HasName("IX_Order.Item.Component_Material");

                b.HasIndex("BID", "OrderID")
                    .HasName("IX_Order.Item.Component_Order");

                b.HasIndex("BID", "DestinationID")
                    .HasName("IX_Order.Item.Component_Destination");

                b.HasIndex("BID", "OrderItemID", "ParentComponentID", "Number")
                    .HasName("IX_Order.Item.Component_OrderItem");

                b.HasOne<GLAccount>(e => e.IncomeAccount)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.IncomeAccountID })
                    .HasConstraintName("FK_Order.Item.Component_Accounting.GL.Account")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<GLAccount>(e => e.ExpenseAccount)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.ExpenseAccountID })
                    .HasConstraintName("FK_Order.Item.Component_Accounting.GL.Account1")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<TransactionHeaderData>(e => e.Order)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.OrderID })
                    .HasConstraintName("FK_Order.Item.Component_Order.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<OrderItemComponent>(e => e.ParentComponent)
                    .WithMany(e => e.ChildComponents)
                    .HasPrincipalKey(e => new { e.BID, e.ID })
                    .HasForeignKey(e => new { e.BID, e.ParentComponentID })
                    .HasConstraintName("FK_Order.Item.Component_Order.Item.Component")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<OrderItemData>(e => e.OrderItem)
                    .WithMany(e => e.Components)
                    .HasForeignKey(e => new { e.BID, e.OrderItemID })
                    .HasConstraintName("FK_Order.Item.Component_Order.Item.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<LaborData>(e => e.Labor)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.LaborID })
                    .HasConstraintName("FK_Order.Item.Component_Part.Labor.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<MachineData>(e => e.Machine)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.MachineID })
                    .HasConstraintName("FK_Order.Item.Component_Part.Machine.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<MaterialData>(x => x.Material)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.MaterialID })
                    .HasConstraintName("FK_Order.Item.Component_Part.Material.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<AssemblyData>(e => e.Assembly)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.AssemblyID })
                    .HasConstraintName("FK_Order.Item.Component_Part.Subassembly.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<DestinationData>(e => e.Destination)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.DestinationID })
                    .HasConstraintName("FK_Order.Item.Component_Part.Destination.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                b.HasOne<AssemblyVariable>(x => x.AssemblyVariable)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.AssemblyVariableID })
                    .HasConstraintName("FK_Order.Item.Component_Part.Subassembly.Variable")
                    .OnDelete(DeleteBehavior.Restrict);

                b.Property(e => e.IncomeAllocationType).HasColumnType("tinyint");

                b.HasOne<EnumAssemblyIncomeAllocationType>().WithMany().HasForeignKey(t => new { t.IncomeAllocationType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Order.Item.Component_enum.Part.Subassembly.IncomeAllocationType");
                
                b.Property(e => e.CostComponentFixed)
                    .HasColumnName("Cost.Component.Fixed")
                    .HasColumnType("DECIMAL(18,6)");

                b.Property(e => e.CostComponentUnit)
                    .HasColumnName("Cost.Component.Unit")
                    .HasColumnType("DECIMAL(18,6)");

                b.Ignore(e => e.TaxInfoList);
                b.Ignore(e => e.TempID);
                b.Ignore(e => e.Variables);

                b.Property(e => e.RollupLinkedPriceAndCost);
            });

            modelBuilder.Entity<SimpleOrderItemData>(entity =>
            {
                entity.ToTable("Order.Item.Data.SimpleList");

                MapSimpleListItem<SimpleOrderItemData, int>(entity, ClassType.OrderItem.AsString());
            });

            modelBuilder.Entity<SystemEmailSMTPConfigurationType>(entity =>
            {
                entity.ToTable("System.Email.SMTP.ConfigurationType");

                entity.HasKey(t => new { t.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.SMTPPort).HasColumnType("smallint");
                entity.Property(e => e.SMTPSecurityType).HasColumnType("tinyint");

            });

            modelBuilder.Entity<EnumEmailProviderType>(entity =>
            {
                entity.ToTable("enum.Email.SMTP.ProviderType");
                entity.HasKey(t => new { t.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");

            });

            modelBuilder.Entity<EnumEmailSecurityType>(entity =>
            {
                entity.ToTable("enum.Email.SMTP.SecurityType");
                entity.HasKey(t => new { t.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");

            });

            modelBuilder.Entity<DomainEmailLocationLink>(entity =>
            {
                entity.ToTable("Domain.Email.LocationLink");

                entity.HasKey(t => new { t.BID, t.DomainID, t.LocationID });

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.DomainID).HasColumnType("smallint");
                entity.Property(e => e.LocationID).HasColumnType("tinyint");

                entity.HasOne(t => t.Location).WithMany(x => x.DomainEmailLinks)
                    .HasForeignKey(t => new { t.BID, t.LocationID })
                    .HasConstraintName("FK_Domain.Email.LocationLink_Location.Data").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.DomainEmail).WithMany(x => x.LocationLinks)
                    .HasForeignKey(t => new { t.BID, t.DomainID })
                    .HasConstraintName("FK_Domain.Email.LocationLink_Domain.Email.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<DomainEmail>(entity =>
            {
                entity.ToTable("Domain.Email.Data");
                entity.HasKey(t => new { t.BID, t.ID });

                entity.HasIndex(e => new { e.BID, e.DomainName, e.IsActive })
                    .HasName("IX_Domain.Email.Data_Domain");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("smallint");
                entity.Property(e => e.CreatedDate).HasColumnType("date");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.ProviderType).HasColumnType("tinyint");
                entity.Property(e => e.SMTPConfigurationType).HasColumnType("tinyint");
                entity.Property(e => e.SMTPPort).HasColumnType("smallint");
                entity.Property(e => e.SMTPSecurityType).HasColumnType("tinyint");
                entity.Property(e => e.LastVerificationAttemptDT).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.LastVerifiedDT).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((1022))");
                entity.Property(e => e.IsForAllLocations).HasDefaultValueSql("((0))");
                entity.Ignore(e => e.SimpleLocations);

                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(e => e.BID)
                    //.OnDelete(DeleteBehavior.NoAction)
                    .HasConstraintName("FK_Domain.Email.Data_Business.Data");

                entity.HasOne(t => t.SecurityType).WithMany(x => x.DomainEmails)
                    .HasForeignKey(t => new { t.SMTPSecurityType })
                    .HasConstraintName("FK_Domain.Email.Data_enum.Email.SMTP.SecurityType").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.Provider).WithMany(x => x.DomainEmails)
                    .HasForeignKey(t => new { t.ProviderType })
                    .HasConstraintName("FK_Domain.Email.Data_enum.Email.SMTP.ProviderType").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.ConfigurationType).WithMany(x => x.DomainEmails)
                    .HasForeignKey(t => new { t.SMTPConfigurationType })
                    .HasConstraintName("FK_Domain.Email.Data_System.Email.SMTP.ConfigurationType").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SimpleDomainEmail>(entity =>
            {
                entity.ToTable("Domain.Email.SimpleList");
            });

            modelBuilder.Entity<SystemAutomationActionDefinition>(entity =>
            {
                entity.ToTable("System.Automation.Action.Definition");
                entity.HasKey(t => t.ID);

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(t => t.Name).IsRequired();
            });

            modelBuilder.Entity<SystemAutomationActionDataTypeLink>(entity =>
            {
                entity.ToTable("System.Automation.Action.DataTypeLink");
                entity.HasKey(t => new { t.ActionID, t.DataType });

                entity.Property(e => e.ActionID).HasColumnType("tinyint");
                entity.Property(e => e.DataType).HasColumnType("smallint");

                entity.HasOne(t => t.ActionDefinition).WithMany(x => x.DataTypeLinks)
                    .HasForeignKey(t => new { t.ActionID })
                    .HasConstraintName("FK_System.Automation.Action.DataTypeLink_System.Automation.Action.Definition").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SystemAutomationTriggerDefinition>(entity =>
            {
                entity.ToTable("System.Automation.Trigger.Definition");

                entity.Property(t => t.ID).HasColumnType("smallint").ValueGeneratedNever();
                entity.HasKey(t => t.ID);

                entity.Property(e => e.TriggerCategoryType).HasColumnType("tinyint");
                entity.Property(e => e.DataType).HasColumnType("smallint");
                entity.Property(e => e.ConditionDataType).HasColumnType("smallint");

                entity.HasOne<EnumAutomationTriggerCategoryType>(t => t.AutomationTriggerCategoryType).WithMany().HasForeignKey(d => d.TriggerCategoryType).HasConstraintName("FK_System.Automation.Trigger.Definition_enum.Automation.Trigger.CategoryType").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<EnumAutomationTriggerCategoryType>(entity =>
            {
                entity.ToTable("enum.Automation.Trigger.CategoryType");
                entity.Property(t => t.ID).HasColumnType("tinyint").ValueGeneratedNever();
                entity.HasKey(t => t.ID);
            });

            modelBuilder.Entity<OrderDestinationData>(entity =>
            {
                entity.ToTable("Order.Destination.Data");

                entity.HasKey(t => new { t.BID, t.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.OrderDestination.ID()}");
                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("SYSUTCDATETIME()");

                entity.Property(e => e.PricePreTax).HasColumnName("Price.PreTax").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql($"(([Price.Net] - isnull( [Price.ItemDiscount], (0.0)))");
                entity.Property(e => e.PriceTotal).HasColumnName("Price.Total").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql($"(([Price.Net] - isnull([Price.Discount], (0.0))) + [Price.Tax])");
                entity.Property(e => e.ComputedPreTax).HasColumnName("Computed.PreTax").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql($"([Computed.Net] - isnull([Price.Discount],(0.0)))");
                entity.Property(e => e.CostTotal).HasColumnName("Cost.Total").HasColumnType("DECIMAL(18,4)").HasComputedColumnSql($"(isnull(((isnull( [Cost.Material],(0.0)) + isnull([Cost.Labor],(0.0))) + isnull([Cost.Machine],(0.0))) + isnull([Cost.Other],(0.0)),(0.0)))");

                entity.Property(e => e.Priority);
                entity.Property(e => e.IsUrgent);

                entity.Property(e => e.OrderStatusID).HasColumnType("tinyint");
                entity.Property(e => e.ItemStatusID).HasColumnType("smallint");
                entity.Property(e => e.SubStatusID).HasColumnType("smallint");
                entity.Property(e => e.Variables).HasColumnType("xml");
                entity.Property(e => e.TaxGroupID).HasColumnType("smallint");
                entity.Property(e => e.TaxExemptReasonID).HasColumnType("smallint");
                entity.Property(e => e.TransactionType).HasColumnType("tinyint");

                entity.Property(e => e.PriceIsLocked).HasColumnName("Price.IsLocked").HasColumnType("bit");
                entity.Property(e => e.PriceNet).HasColumnName("Price.Net").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceNetOV).HasColumnName("Price.NetOV").HasColumnType("bit");
                entity.Property(e => e.PriceDiscount).HasColumnName("Price.Discount").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceTaxable).HasColumnName("Price.Taxable").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.PriceTaxableOV).HasColumnName("Price.TaxableOV").HasColumnType("bit");
                entity.Property(e => e.PriceTax).HasColumnName("Price.Tax").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.ComputedNet).HasColumnName("Computed.Net").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostMaterial).HasColumnName("Cost.Material").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostLabor).HasColumnName("Cost.Labor").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostMachine).HasColumnName("Cost.Machine").HasColumnType("DECIMAL(18,4)");
                entity.Property(e => e.CostOther).HasColumnName("Cost.Other").HasColumnType("DECIMAL(18,4)");

                entity.HasIndex(e => new { e.BID, e.OrderID, e.DestinationNumber })
                    .HasName("IX_Order.Destination.Data_OrderID");
                entity.HasIndex(e => new { e.BID, e.ItemStatusID })
                    .HasName("IX_Order.Destination.Data_ItemStatus");
                entity.HasIndex(e => new { e.BID, e.SubStatusID })
                    .HasName("IX_Order.Destination.Data_ItemSubStatus");

                entity.HasOne(t => t.Order).WithMany(p => p.Destinations)
                    .HasForeignKey(t => new { t.BID, t.OrderID })
                    .HasConstraintName("FK_Order.Destination.Data_OrderID").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.TaxGroup).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxGroupID })
                    .HasConstraintName("FK_Order.Destination.Data_TaxGroupID").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.OrderItemStatus).WithMany()
                    .HasForeignKey(t => new { t.BID, t.ItemStatusID })
                    .HasConstraintName("FK_Order.Destination.Data_ItemStatusID").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.SubStatus).WithMany()
                    .HasForeignKey(t => new { t.BID, t.SubStatusID })
                    .HasConstraintName("FK_Order.Destination.Data_SubstatusID").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.EnumOrderOrderStatus).WithMany()
                    .HasForeignKey(t => new { t.OrderStatusID })
                    .HasConstraintName("FK_Order.Destination.Data_OrderStatusID").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.EnumOrderDestinationType).WithMany()
                    .HasForeignKey(t => new { t.DestinationType })
                    .HasConstraintName("FK_Order.Destination.Data_DestinationType").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne<FlatListItem>(x => x.TaxExemptReason)
                    .WithMany()
                    .HasForeignKey(d => new { d.BID, d.TaxExemptReasonID })
                    .HasConstraintName("FK_Order.Destination.Data_TaxExemptReason");
            });

            modelBuilder.Entity<SimpleOrderDestinationData>(entity =>
            {
                entity.ToTable("Order.Destination.Data.SimpleList");
                MapSimpleListItem<SimpleOrderDestinationData, int>(entity, ClassType.OrderDestination.AsString());
            });

            modelBuilder.Entity<EnumRoleAccess>(entity =>
            {
                entity.ToTable("enum.Role.Access");
                entity.HasKey(e => new { e.ID });

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false).HasMaxLength(100);
            });

            modelBuilder.Entity<EmployeeRole>(entity =>
            {
                entity.ToTable("Employee.Role");

                entity.HasKey(t => new { t.BID, t.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"(({ClassType.EmployeeRole.ID()}))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("SYSUTCDATETIME()");
                entity.Property(e => e.Name).HasMaxLength(200);
                entity.Property(e => e.OrderRestriction).HasColumnType("tinyint");
                entity.Property(e => e.OrderItemRestriction).HasColumnType("tinyint");
                entity.Property(e => e.OrderDestinationRestriction).HasColumnType("tinyint");
                entity.Property(e => e.EstimateRestriction).HasColumnType("tinyint");
                entity.Property(e => e.EstimateItemRestriction).HasColumnType("tinyint");
                entity.Property(e => e.EstimateDestinationRestriction).HasColumnType("tinyint");
                entity.Property(e => e.OpportunityRestriction).HasColumnType("tinyint");
                entity.Property(e => e.PORestriction).HasColumnType("tinyint");

                entity.HasOne<EnumRoleAccess>().WithMany().HasForeignKey(t => t.OrderRestriction).OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumRoleAccess>().WithMany().HasForeignKey(t => t.OrderItemRestriction).OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumRoleAccess>().WithMany().HasForeignKey(t => t.OrderDestinationRestriction).OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumRoleAccess>().WithMany().HasForeignKey(t => t.EstimateRestriction).OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumRoleAccess>().WithMany().HasForeignKey(t => t.EstimateItemRestriction).OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumRoleAccess>().WithMany().HasForeignKey(t => t.EstimateDestinationRestriction).OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumRoleAccess>().WithMany().HasForeignKey(t => t.OpportunityRestriction).OnDelete(DeleteBehavior.Restrict);
                entity.HasOne<EnumRoleAccess>().WithMany().HasForeignKey(t => t.PORestriction).OnDelete(DeleteBehavior.Restrict);

                entity.HasOne<BusinessData>().WithMany().HasForeignKey(e => e.BID).HasConstraintName("FK_Employee.Role_BID").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<EnumDomainStatus>(entity =>
            {
                entity.ToTable("enum.DomainStatus");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumDomainAccessType>(entity =>
            {
                entity.ToTable("enum.DomainAccessType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssociationType>(entity =>
            {
                entity.ToTable("enum.AssociationType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<DomainData>(entity =>
            {
                entity.ToTable("Domain.Data");

                entity.HasKey(t => new { t.BID, t.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"(({ClassType.Domain.ID()}))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("SYSUTCDATETIME()");
                entity.Property(e => e.Domain).HasColumnType("varchar(255)");
                entity.Property(e => e.Status).HasColumnType("tinyint");
                entity.Property(e => e.AccessType).HasColumnType("tinyint");
                entity.Property(e => e.DNSLastAttemptDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.DNSLastAttemptResult).HasColumnType("varchar(100)");
                entity.Property(e => e.DNSVerifiedDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.SSLLastAttemptDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.SSLLastAttemptResult).HasColumnType("varchar(100)");

                entity.HasIndex(e => new { e.BID, e.Domain })
                    .HasName("IX_Domain.Data_Domain");

                entity.HasOne(t => t.EnumDomainStatus).WithMany()
                    .HasForeignKey(t => new { t.Status })
                    .HasConstraintName("FK_Domain.Data_enum.DomainStatus").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.EnumDomainAccessType).WithMany()
                    .HasForeignKey(t => new { t.AccessType })
                    .HasConstraintName("FK_Domain.Data_enum.DomainAccessType").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.Location).WithMany()
                    .HasForeignKey(t => new { t.BID, t.LocationID })
                    .HasConstraintName("FK_Domain.Data_Location").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(t => t.SSLCertificate).WithMany()
                    .HasForeignKey(t => new { t.BID, t.SSLCertificateID })
                    .HasConstraintName("FK_Domain.Data_SSLCertificate.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SSLCertificateData>(entity =>
            {
                entity.ToTable("SSLCertificate.Data");

                entity.HasKey(t => new { t.BID, t.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.SSLCertificate.ID()}");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("SYSUTCDATETIME()");
                entity.Property(e => e.CommonName).HasColumnType("varchar(255)");
                entity.Property(e => e.InstalledDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.FileName).HasColumnType("varchar(255)");
                entity.Property(e => e.ValidFromDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.ValidToDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.Thumbprint).HasColumnType("varchar(255)");
                entity.Property(e => e.CertificateName).HasColumnType("varchar(255)");

                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(t => new { t.BID })
                    .HasConstraintName("FK_SSLCertificate.Data_Business.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<LocationGoal>(entity =>
            {
                entity.ToTable("Location.Goal");

                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.LocationGoal.ID()}");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("SYSUTCDATETIME()");

                entity.Property(e => e.PercentOfGoal).HasColumnType("numeric(38,16)").HasComputedColumnSql("(case when [Budgeted]<>(0) then ([Actual]/[Budgeted])*(100.0)  end)");
                entity.Property(e => e.IsYearlyTotal).HasComputedColumnSql("(case when [Month] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
                entity.Property(e => e.IsBusinessTotal).HasComputedColumnSql("(case when [LocationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

                entity.HasIndex(e => new { e.BID, e.Year, e.Month, e.LocationID })
                    .HasName("IX_Location.Goal_Date_Location");
                entity.HasIndex(e => new { e.BID, e.LocationID, e.Year, e.Month })
                    .HasName("IX_Location.Goal_Location_Date");

                entity.HasOne(e => e.Business).WithMany()
                    .HasForeignKey(e => new { e.BID })
                    .HasConstraintName("FK_Location.Goal_Business.Data").OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.Location).WithMany()
                    .HasForeignKey(e => new { e.BID, e.LocationID })
                    .HasConstraintName("FK_Location.Goal_Location.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<BusinessGoal>(entity =>
            {
                entity.ToTable("Business.Goal");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.PercentOfGoal).HasColumnType("numeric(38,6)");
                entity.Property(e => e.CalendarMonth).HasColumnType("tinyint").IsRequired(false);
                entity.Property(e => e.CalendarYear).HasColumnType("smallint").IsRequired(false);
                entity.Property(e => e.FiscalMonth).HasColumnType("tinyint").IsRequired(false);
                entity.Property(e => e.FiscalYear).HasColumnType("smallint");
            });

            modelBuilder.Entity<DashboardData>(entity =>
            {
                entity.ToTable("Dashboard.Data");

                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.Dashboard.ID()}");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("SYSUTCDATETIME()");
                entity.Property(e => e.Description).HasColumnType("nvarchar(MAX)");
                entity.Property(e => e.Module).HasColumnType("smallint");
                entity.Property(e => e.LastUpdatedDT).HasColumnType("datetime2(2)");

                entity.HasIndex(e => new { e.BID, e.UserLinkID, e.Module, e.IsActive })
                    .HasName("IX_Dashboard.Data_User");
                entity.HasIndex(e => new { e.BID, e.Module })
                    .HasName("IX_Dashboard.Data_Module");

                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(t => new { t.BID })
                    .HasConstraintName("FK_Dashboard.Data_Business.Data").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(e => e.UserLink).WithMany()
                    .HasForeignKey(t => new { t.BID, t.UserLinkID })
                    .HasConstraintName("FK_Dashboard.Data_User.Link").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<DashboardWidgetData>(entity =>
            {
                entity.ToTable("Dashboard.Widget.Data");

                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.DashboardWidget.ID()}");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("SYSUTCDATETIME()");
                entity.Property(e => e.Options).HasColumnType("nvarchar(MAX)");
                entity.Property(e => e.LastUpdatedDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.LastResults).HasColumnType("nvarchar(MAX)");

                entity.HasIndex(e => new { e.BID, e.DashboardID })
                    .HasName("IX_Dashboard.Widget.Data_Dashboard");

                entity.HasOne(t => t.Dashboard).WithMany(t => t.Widgets)
                    .HasForeignKey(t => new { t.BID, t.DashboardID })
                    .HasConstraintName("FK_Dashboard.Widget.Data_Dashboard.Data").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.WidgetDefinition).WithMany(t => t.Widgets)
                    .HasForeignKey(t => new { t.WidgetDefinitionID })
                    .HasConstraintName("FK_Dashboard.Widget.Data_System.Dashboard.Widget.Definition").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<DashboardWidgetDefinition>(entity =>
            {
                entity.ToTable("System.Dashboard.Widget.Definition");

                entity.HasKey(e => new { e.ID });
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.DashboardWidgetDefinition.ID()}");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("SYSUTCDATETIME()");
                entity.Property(e => e.DefaultOptions).HasColumnType("nvarchar(MAX)");
                entity.Property(e => e.Description).HasColumnType("nvarchar(MAX)");
                entity.Property(e => e.Modules).HasColumnType("smallint");

                entity.Ignore("BID");
                entity.HasIndex(e => new { e.Modules })
                    .HasName("IX_System.Dashboard.Widget.Definition_Module");
            });

            modelBuilder.Entity<SystemColor>(entity =>
            {
                entity.ToTable("System.Color");

                entity.HasKey(e => new { e.ID });
            });

            modelBuilder.Entity<ListTag>(entity =>
            {
                entity.ToTable("List.Tag");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql($"{ClassType.ListTag.ID()}");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("SYSUTCDATETIME()");
            });

            modelBuilder.Entity<ListTagOtherLink>(entity =>
            {
                entity.ToTable("List.Tag.OtherLink");

                entity.HasKey(e => new { e.BID, e.TagID, e.AppliesToClassTypeID, e.AppliesToID });

                entity.HasIndex(e => new { e.BID, e.TagID, e.AppliesToClassTypeID, e.AppliesToID })
                    .HasName("IX_List.Tag.OtherLink");
                entity.HasIndex(e => new { e.BID, e.AppliesToClassTypeID, e.AppliesToID, e.TagID, })
                    .HasName("IX_List.Tag.OtherLink_AppliesTo");

                entity.HasOne(t => t.Tag).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TagID })
                    .HasConstraintName("FK_List.Tag.OtherLink_List.Tag").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<ContactTagLink>(entity =>
            {
                entity.ToTable("Contact.TagLink");

                entity.HasKey(e => new { e.BID, e.TagID, e.ContactID });

                entity.HasIndex(e => new { e.BID, e.TagID })
                    .HasName("IX_Contact.TagsLink");
                entity.HasIndex(e => new { e.BID, e.ContactID })
                    .HasName("IX_Contact.TagsLink_Contact");

                entity.HasOne(t => t.Tag).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TagID })
                    .HasConstraintName("FK_Contact.TagLink_List.Tag").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Contact).WithMany(x => x.TagLinks)
                    .HasForeignKey(t => new { t.BID, t.ContactID })
                    .HasConstraintName("FK_Contact.TagLink_Contact.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<CompanyTagLink>(entity =>
            {
                entity.ToTable("Company.TagLink");

                entity.HasKey(e => new { e.BID, e.TagID, e.CompanyID });

                entity.HasIndex(e => new { e.BID, e.TagID })
                    .HasName("IX_Company.TagsLink");
                entity.HasIndex(e => new { e.BID, e.CompanyID })
                    .HasName("IX_Company.TagsLink_Company");

                entity.HasOne(t => t.Tag).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TagID })
                    .HasConstraintName("FK_Company.TagLink_List.Tag").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Company).WithMany(x => x.TagLinks)
                    .HasForeignKey(t => new { t.BID, t.CompanyID })
                    .HasConstraintName("FK_Company.TagLink_Company.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<OrderTagLink>(entity =>
            {
                entity.ToTable("Order.TagLink");

                entity.HasKey(e => new { e.BID, e.TagID, e.OrderID });

                entity.HasIndex(e => new { e.BID, e.TagID })
                    .HasName("IX_Order.TagsLink");
                entity.HasIndex(e => new { e.BID, e.OrderID })
                    .HasName("IX_Order.TagsLink_Order");

                entity.HasOne(t => t.Tag).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TagID })
                    .HasConstraintName("FK_Order.TagLink_List.Tag").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Order).WithMany(x => x.TagLinks)
                    .HasForeignKey(t => new { t.BID, t.OrderID })
                    .HasConstraintName("FK_Order.TagLink_Order.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<TimeCard>(entity =>
            {
                entity.ToTable("Employee.TimeCard");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((5030))");
                entity.Property(e => e.IsClosed).HasComputedColumnSql("(case when [EndDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end) PERSISTED");
                entity.Property(e => e.IsAdjusted).HasComputedColumnSql("(isnull(case when [AdjustedByEmployeeID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,CONVERT([bit],(0))))");
                entity.Property(e => e.TimeInMin).HasColumnType("decimal(18,4)").HasComputedColumnSql("((datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.StartDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.EndDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.PaidTimeInMin).HasColumnType("decimal(18,4)");
                entity.Property(e => e.LatStart).HasColumnType("decimal(18,11)");
                entity.Property(e => e.LongStart).HasColumnType("decimal(18,11)");
                entity.Property(e => e.LatEnd).HasColumnType("decimal(18,11)");
                entity.Property(e => e.LongEnd).HasColumnType("decimal(18,11)");
                entity.Property(e => e.AdjustedDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.MetaData).HasColumnType("xml");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.StartDT, e.IsClosed })
                    .HasName("IX_Employee.TimeCard_Employee");

                entity.HasOne(t => t.Employee).WithMany()
                    .HasForeignKey(t => new { t.BID, t.EmployeeID })
                    .HasConstraintName("FK_Employee.TimeCard_Employee.Data").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.AdjustedByEmployee).WithMany()
                    .HasForeignKey(t => new { t.BID, t.AdjustedByEmployeeID })
                    .HasConstraintName("FK_Employee.TimeCard_AdjustedBy").OnDelete(DeleteBehavior.Restrict);

            });

            modelBuilder.Entity<TimeCardDetail>(entity =>
            {
                entity.ToTable("Employee.TimeCard.Detail");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((5031))");

                entity.Property(e => e.SimultaneousDetailCards).HasDefaultValueSql("((1)");
                entity.Property(e => e.IsPaid);
                entity.Property(e => e.IsClosed).HasComputedColumnSql("(case when [EndDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end) PERSISTED");
                entity.Property(e => e.PaidTimeInMin).HasColumnType("decimal(18,4)").HasComputedColumnSql("(case when [IsPaid]=(1) then (datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0) else (0.0) end)");
                entity.Property(e => e.TimeInMin).HasColumnType("decimal(18,4)").HasComputedColumnSql("((datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0))");
                entity.Property(e => e.IsAdjusted).HasComputedColumnSql("(isnull(case when [AdjustedByEmployeeID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,CONVERT([bit],(0))))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.StartDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.EndDT).HasColumnType("datetime2(0)");
                entity.Property(e => e.PaidTimeInMin).HasColumnType("decimal(18,4)");
                entity.Property(e => e.LatStart).HasColumnType("decimal(18,11)");
                entity.Property(e => e.LongStart).HasColumnType("decimal(18,11)");
                entity.Property(e => e.LatEnd).HasColumnType("decimal(18,11)");
                entity.Property(e => e.LongEnd).HasColumnType("decimal(18,11)");
                entity.Property(e => e.AdjustedDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.MetaData).HasColumnType("xml");
                entity.HasKey(e => new { e.BID, e.ID });

                entity.HasIndex(e => new { e.BID, e.TimeCardID })
                    .HasName("IX_Employee.TimeCard.Detail_TimeCard");
                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.StartDT, e.IsClosed })
                    .HasName("IX_Employee.TimeCard.Detail_Employee");

                entity.HasOne(t => t.Employee).WithMany()
                    .HasForeignKey(t => new { t.BID, t.EmployeeID })
                    .HasConstraintName("FK_Employee.TimeCard.Detail_Employee.Data").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.AdjustedByEmployee).WithMany()
                    .HasForeignKey(t => new { t.BID, t.AdjustedByEmployeeID })
                    .HasConstraintName("FK_Employee.TimeCard.Detail_AdjustedBy").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.TimeCard).WithMany(t => t.TimeCardDetails)
                    .HasPrincipalKey(TimeCard => new { TimeCard.BID, TimeCard.ID })
                    .HasForeignKey(t => new { t.BID, t.TimeCardID })
                    .HasConstraintName("FK_Employee.TimeCard.Detail_Employee.TimeCard").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.TimeClockActivity).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TimeClockActivityID })
                    .HasConstraintName("FK_Employee.TimeCard.Detail_List.FlatList.Data").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.TimeClockBreak).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TimeClockBreakID })
                    .HasConstraintName("FK_Employee.TimeCard.Detail_TimeClockBreak").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.OrderItemStatus).WithMany()
                    .HasForeignKey(t => new { t.BID, t.OrderItemStatusID })
                    .HasConstraintName("FK_Employee.TimeCard.Detail_Order.Item.Status").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<OrderItemTagLink>(entity =>
            {
                entity.ToTable("Order.Item.TagLink");

                entity.HasKey(e => new { e.BID, e.TagID, e.OrderItemID });

                entity.HasIndex(e => new { e.BID, e.TagID })
                    .HasName("IX_Order.Item.TagsLink");
                entity.HasIndex(e => new { e.BID, e.OrderItemID })
                    .HasName("IX_Order.Item.TagsLink_Order");

                entity.HasOne(t => t.Tag).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TagID })
                    .HasConstraintName("FK_Order.Item.TagLink_List.Tag").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.OrderItem).WithMany(x => x.TagLinks)
                    .HasForeignKey(t => new { t.BID, t.OrderItemID })
                    .HasConstraintName("FK_Order.Item.TagLink_Order.Item.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<OrderDestinationTagLink>(entity =>
            {
                entity.ToTable("Order.Destination.TagLink");

                entity.HasKey(e => new { e.BID, e.TagID, e.OrderDestinationID });

                entity.HasIndex(e => new { e.BID, e.TagID })
                    .HasName("IX_Order.Destination.TagsLink");
                entity.HasIndex(e => new { e.BID, e.OrderDestinationID })
                    .HasName("IX_Order.Destination.TagsLink_Order");

                entity.HasOne(t => t.Tag).WithMany()
                    .HasForeignKey(t => new { t.BID, t.TagID })
                    .HasConstraintName("FK_Order.Destination.TagLink_List.Tag").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.Destination).WithMany(x => x.TagLinks)
                    .HasForeignKey(t => new { t.BID, t.OrderDestinationID })
                    .HasConstraintName("FK_Order.Destination.TagLink_Order.Destination.Data").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<AssemblyData>(entity =>
            {
                entity.ToTable("Part.Subassembly.Data");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.Assembly.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsUnicode(false).IsRequired();
                entity.Property(e => e.InvoiceText).HasMaxLength(255);
                entity.Property(e => e.Description).HasMaxLength(255).IsUnicode(false);
                entity.Property(e => e.HasImage);
                entity.Property(e => e.SKU).HasMaxLength(512);

                entity.Property(e => e.PricingType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.IncomeAllocationType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.TaxabilityCodeID).HasColumnType("smallint");
                entity.Property(e => e.IncomeAccountID).HasColumnType("int");

                entity.Property(e => e.FixedPrice).HasColumnType("decimal(18,4)");
                entity.Property(e => e.FixedMargin).HasColumnType("decimal(18,4)");
                entity.Property(e => e.FixedMarkup).HasColumnType("decimal(18,4)");
                entity.Property(e => e.HasTierTable);
                entity.Property(e => e.PriceFormulaType).HasDefaultValueSql("0");
                entity.Property(e => e.AssemblyType).HasColumnType("tinyint").HasDefaultValueSql("0").IsRequired();
                entity.Property(e => e.MachineLayoutTypes).HasColumnType("tinyint").HasDefaultValueSql("0").IsRequired();

                entity.Property(e => e.EnableShipByDate);
                entity.Property(e => e.EnableArriveByDate);
                entity.Property(e => e.EnableFromAddress);
                entity.Property(e => e.EnableToAddress);
                entity.Property(e => e.EnableBlindShipping);
                entity.Property(e => e.EnablePackagesTab);

                entity.Property(e => e.ShipByDateLabel).HasColumnType("nvarchar(50)");
                entity.Property(e => e.ArriveByDateLabel).HasColumnType("nvarchar(50)");
                entity.Property(e => e.FromAddressLabel).HasColumnType("nvarchar(50)");
                entity.Property(e => e.ToAddressLabel).HasColumnType("nvarchar(50)");
                entity.Property(e => e.BlindShippingLabel).HasColumnType("nvarchar(50)");

                // primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Subassembly.Data");
                // indexes
                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive }).HasName("IX_Part.Subassembly.Data_Name");

                entity.HasIndex(e => new { e.BID, e.AssemblyType, e.Name, e.IsActive }).HasName("IX_Part.Subassembly.Data_Type");

                // foreign keys
                entity.HasOne<BusinessData>().WithMany().HasForeignKey(e => e.BID).HasConstraintName("FK_Part.Subassembly.Data_Business.Data");

                entity.HasOne<EnumAssemblyPricingType>().WithMany().HasForeignKey(t => new { t.PricingType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Data_enum.Part.Subassembly.PricingType");

                entity.HasOne<EnumPriceFormulaType>().WithMany().HasForeignKey(t => new { t.PriceFormulaType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Data_enum.Assembly.PriceFormula");

                entity.HasOne<EnumAssemblyIncomeAllocationType>().WithMany().HasForeignKey(t => new { t.IncomeAllocationType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Data_enum.Part.Subassembly.IncomeAllocationType");

                entity.HasOne<EnumAssemblyType>().WithMany().HasForeignKey(t => new { t.AssemblyType })
                .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Data_enum.Part.Subassembly.AssemblyType");

                //entity.HasOne<TaxCode>().WithMany().HasForeignKey(t => new { t.BID, t.TaxabilityCodeID })
                //    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Data_Accounting.Tax.Code");

                entity.HasOne<GLAccount>().WithMany().HasForeignKey(t => new { t.BID, t.IncomeAccountID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Data_Accounting.GL.Account");

                entity.Ignore(e => e.AssemblyCategories);
                entity.Ignore(e => e.SimpleAssemblyCategories);

                entity.HasMany(x => x.CustomDataValues)
                      .WithOne()
                      .HasForeignKey(x => new { x.BID, x.ID });
            });

            modelBuilder.Entity<SimpleAssemblyData>(entity =>
            {
                entity.ToTable("Part.Subassembly.SimpleList");

                MapSimpleListItem<SimpleAssemblyData, int>(entity, ClassType.Assembly.AsString());
            });

            modelBuilder.Entity<AssemblyCategory>(entity =>
            {
                entity.ToTable("Part.Subassembly.Category");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.AssemblyCategory.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(255).IsUnicode(false).IsRequired();
                entity.Property(e => e.Description).HasMaxLength(255).IsUnicode(false);
                entity.Property(e => e.ParentID);

                // primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Subassembly.Category");
                // indexes
                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive, e.ParentID }).HasName("IX_Part.Subassembly.Category_Name_IsActive");
                entity.HasIndex(e => new { e.BID, e.ParentID, e.Name, e.IsActive }).HasName("IX_Part.Subassembly.Category_ParentID_Name");
                // foreign keys
                entity.HasOne<BusinessData>().WithMany().HasForeignKey(e => e.BID).HasConstraintName("FK_Part.Subassembly.Category_Business.Data");
                entity.HasOne(e => e.ParentCategory)
                    .WithMany(e => e.ChildCategories)
                    .HasForeignKey(e => new { e.BID, e.ParentID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Subassembly.Category_Part.Subassembly.Category");

                entity.Ignore(e => e.Assemblies);
                entity.Ignore(e => e.SimpleAssemblies);
            });

            modelBuilder.Entity<SimpleAssemblyCategory>(entity =>
            {
                entity.ToTable("Part.Subassembly.Category.SimpleList");

                MapSimpleListItem<SimpleAssemblyCategory, short>(entity, ClassType.AssemblyCategory.AsString());
            });

            modelBuilder.Entity<AssemblyCategoryLink>(entity =>
            {
                entity.ToTable("Part.Subassembly.CategoryLink");

                //primary key
                entity.HasKey(e => new { e.BID, e.PartID, e.CategoryID }).HasName("PK_Part.Subassembly.CategoryLink");

                //indexes
                entity.HasIndex(e => new { e.BID, e.CategoryID, e.PartID }).HasName("IX_Part.Subassembly.CategoryLink_Category");
                //foreign keys
                entity.HasOne(e => e.Assembly)
                    .WithMany(e => e.AssemblyCategoryLinks)
                    .HasForeignKey(e => new { e.BID, e.PartID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Subassembly.CategoryLink_Part.Subassembly.Data");
                entity.HasOne(e => e.AssemblyCategory)
                    .WithMany(e => e.AssemblyCategoryLinks)
                    .HasForeignKey(e => new { e.BID, e.CategoryID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Subassembly.CategoryLink_Part.Subassembly.Category");
            });

            modelBuilder.Entity<EnumElementType>(entity =>
            {
                entity.ToTable("enum.ElementType");

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyElementType>(entity =>
            {
                entity.ToTable("enum.Part.Subassembly.ElementType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Subassembly.ElementType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyIncomeAllocationType>(entity =>
            {
                entity.ToTable("enum.Part.Subassembly.IncomeAllocationType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Subassembly.IncomeAllocationType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyLabelType>(entity =>
            {
                entity.ToTable("enum.Part.Subassembly.LabelType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Subassembly.LabelType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyLayoutType>(entity =>
            {
                entity.ToTable("enum.Part.Subassembly.LayoutType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Subassembly.LayoutType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumMachineLayoutType>(entity =>
            {
                entity.ToTable("enum.Machine.LayoutType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Machine.LayoutType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyListDataType>(entity =>
            {
                entity.ToTable("enum.Part.Subassembly.List.DataType");
                entity.Property(e => e.ID).HasColumnType("smallint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Subassembly.List.DataType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyPricingType>(entity =>
            {
                entity.ToTable("enum.Part.Subassembly.PricingType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Subassembly.PricingType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumPriceFormulaType>(entity =>
            {
                entity.ToTable("enum.Assembly.PriceFormula");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Assembly.PriceFormula");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyType>(entity =>
            {
                entity.ToTable("enum.Assembly.AssemblyType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Assembly.AssemblyType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumEmbeddedAssemblyType>(entity =>
            {
                entity.ToTable("enum.Assembly.EmbeddedType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Assembly.EmbeddedType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyTableMatchType>(entity =>
            {
                entity.ToTable("enum.Part.Subassembly.Table.MatchType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Subassembly.Table.MatchType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EnumAssemblyTableType>(entity =>
            {
                entity.ToTable("enum.Part.Subassembly.TableType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.HasKey(e => new { e.ID }).HasName("PK_enum.Part.Subassembly.TableType");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<AssemblyTable>(entity =>
            {
                entity.ToTable("Part.Assembly.Table");
                entity.Property(e => e.CellDataType).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.CellDataJSON).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.AssemblyTable.AsString());
                entity.Property(e => e.ColumnCount).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.ColumnDataType).HasColumnType("smallint");
                entity.Property(e => e.ColumnLabel).HasColumnType("varchar(255)");
                entity.Property(e => e.ColumnMatchType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.ColumnUnitID).HasColumnType("tinyint");
                entity.Property(e => e.Description).HasColumnType("varchar(4000)");
                entity.Property(e => e.IsTierTable).HasComputedColumnSql("(isnull(case when [TableType]>=(1) AND [TableType]<=(4) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.Label).HasColumnType("nvarchar(255)").IsRequired();
                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.RowCount).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.RowDataType).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.RowLabel).HasColumnType("varchar(255)");
                entity.Property(e => e.RowMatchType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.RowUnitID).HasColumnType("tinyint");
                entity.Property(e => e.TableType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.VariableName).HasColumnType("varchar(255)").IsRequired();

                entity.Ignore("RowVariableTempID");
                entity.Ignore("ColumnVariableTempID");

                //primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Assembly.Table");

                //indexes
                entity.HasIndex(e => new { e.BID, e.AssemblyID, e.Label }).HasName("IX_Part.Assembly.Table_Subassembly");

                //foreign keys
                entity.HasOne<EnumAssemblyTableMatchType>().WithMany().HasForeignKey(e => new { e.RowMatchType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType");
                entity.HasOne<EnumAssemblyTableMatchType>().WithMany().HasForeignKey(e => new { e.ColumnMatchType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType1");
                entity.HasOne<EnumAssemblyTableType>().WithMany().HasForeignKey(e => new { e.TableType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Assembly.Table_enum.Part.Subassembly.TableType");
                entity.HasOne<AssemblyData>().WithMany(t => t.Tables).HasForeignKey(e => new { e.BID, e.AssemblyID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Assembly.Table_Part.Subassembly.Data");
                entity.HasOne<AssemblyVariable>().WithMany().HasForeignKey(e => new { e.BID, e.RowVariableID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Assembly.Table_Part.Subassembly.Variable");
                entity.HasOne<AssemblyVariable>().WithMany().HasForeignKey(e => new { e.BID, e.ColumnVariableID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Assembly.Table_Part.Subassembly.Variable1");
            });

            modelBuilder.Entity<AssemblyElement>(entity =>
            {
                entity.ToTable("Part.Subassembly.Element");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.LayoutID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.AssemblyElement.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.VariableName).HasMaxLength(255).IsRequired();
                entity.Property(e => e.Tooltip).HasColumnType("nvarchar(512)");

                entity.Property(e => e.AssemblyID).HasColumnName("SubassemblyID");

                entity.Ignore(e => e.TempVariableID);
                //primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Subassembly.Element.Data");

                //indexes
                entity.HasIndex(e => new { e.BID, e.AssemblyID, e.ParentID, e.Column, e.Row }).HasName("IX_Subassembly.Element.Data_SubassemblyID");

                //foreign keys
                entity.HasOne<AssemblyElement>().WithMany(e => e.Elements).HasForeignKey(e => new { e.BID, e.ParentID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Subassembly.Element.Data_Subassembly.Element.Data");

                entity.HasOne<AssemblyLayout>().WithMany(e => e.Elements).HasForeignKey(e => new { e.BID, e.LayoutID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Subassembly.Element.Data_Subassembly.Layout");

                entity.HasOne<EnumAssemblyElementType>().WithMany().HasForeignKey(e => new { e.ElementType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Subassembly.Element.Data_ElementType");

                entity.HasOne<EnumDataType>().WithMany().HasForeignKey(e => new { e.DataType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Subassembly.Element.Data_DataType");
            });

            modelBuilder.Entity<AssemblyVariable>(entity =>
            {
                entity.ToTable("Part.Subassembly.Variable");

                entity.HasKey(e => new { e.BID, e.ID });

                entity.Property(e => e.AssemblyID).HasColumnName("SubassemblyID");

                entity.Property(e => e.LinkedAssemblyID).HasColumnName("LinkedSubassemblyID");
                entity.Property(e => e.LinkedLaborID).HasColumnName("LinkedLaborID");
                entity.Property(e => e.LinkedMachineID).HasColumnName("LinkedMachineID");
                entity.Property(e => e.LinkedMaterialID).HasColumnName("LinkedMaterialID");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.AssemblyVariable.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(7)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.ElementType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.IsDisabled).IsRequired();
                entity.Property(e => e.SystemVariableID).HasColumnType("smallint");
                entity.Property(e => e.Label).HasMaxLength(255);
                entity.Property(e => e.AltText).HasMaxLength(255);
                entity.Property(e => e.IsAltTextFormula).HasColumnType("bit");
                entity.Property(e => e.ElementUseCount).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.ConsumptionDefaultValue).HasColumnType("VARCHAR(MAX)");
                entity.Property(e => e.IsConsumptionFormula);
                entity.Property(e => e.EnableProfileOV).HasColumnType("bit").IsRequired().HasDefaultValueSql("0").ValueGeneratedNever();
                entity.Property(e => e.ProfileSetupLabel).HasColumnType("NVARCHAR(255)");
                entity.Property(e => e.ProfileSetupHint).HasColumnType("NVARCHAR(255)");
                entity.Property(e => e.InclusionFormula).HasColumnType("NVARCHAR(MAX)");

                entity.Property(e => e.UnitID).HasColumnType("TINYINT");
                entity.Property(e => e.UnitType).HasColumnType("TINYINT");
                entity.Property(e => e.CustomUnitText).HasColumnType("NVARCHAR(255)");

                entity.Ignore(e => e.TempID);
                entity.HasIndex(e => new { e.BID, e.AssemblyID, e.Name }).HasName("IX_Part.Subassembly.Variable_Subassembly");

                entity.HasMany<AssemblyElement>(e => e.Elements).WithOne(m => m.Variable).HasForeignKey(t => new { t.BID, t.VariableID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Variable._Part.Subassembly.Element.Data");

                entity.HasOne<EnumAssemblyElementType>().WithMany().HasForeignKey(t => t.ElementType)
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Variable_enum.Part.Subassembly.ElementType");

                entity.HasOne<AssemblyData>(e => e.Assembly).WithMany(m => m.Variables).HasForeignKey(t => new { t.BID, t.AssemblyID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Variable_Part.Subassembly.Data");

                entity.HasOne<EnumDataType>().WithMany().HasForeignKey(t => t.DataType)
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Variable_enum.DataType");

                entity.HasOne<EnumAssemblyLabelType>().WithMany().HasForeignKey(t => new { t.LabelType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Variable_enum.Part.Subassembly.LabelType");

                entity.HasOne<EnumCustomFieldNumberDisplayType>().WithMany().HasForeignKey(t => new { t.DisplayType })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_Part.Subassembly.Variable_enum.CustomField.NumberDisplayType");

            });

            modelBuilder.Entity<AssemblyVariableFormula>(entity =>
            {
                entity.ToTable("Part.Assembly.Variable.Formula");

                entity.Property(e => e.BID);
                entity.Property(e => e.ID);
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.AssemblyVariableFormula.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.VariableID);
                entity.Property(e => e.FormulaUseType).HasColumnType("tinyint");
                entity.Property(e => e.FormulaText).HasColumnType("nvarchar(max)");
                entity.Property(e => e.DataType).HasColumnType("smallint");
                entity.Property(e => e.FormulaEvalType).HasColumnType("tinyint");
                entity.Property(e => e.IsFormula).HasComputedColumnSql("(isnull(case when [FormulaEvalType] <= 1 then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.ChildVariableName).HasColumnType("varchar(255)");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Assembly.Variable.Formula");
                entity.HasIndex(e => new { e.BID, e.VariableID, e.FormulaUseType });

                entity.HasOne<EnumAssemblyFormulaEvalType>()
                    .WithMany()
                    .HasForeignKey(t => t.FormulaEvalType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Assembly.Variable.Formula_enum.Assembly.Formula.EvalType");

                entity.HasOne<EnumAssemblyFormulaUseType>()
                    .WithMany()
                    .HasForeignKey(t => t.FormulaUseType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Assembly.Variable.Formula_enum.Assembly.Formula.UseType");

                entity.HasOne<AssemblyVariable>(t => t.Variable)
                    .WithMany(t => t.Formulas)
                    .HasForeignKey(t => new { t.BID, t.VariableID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Assembly.Variable.Formula_Part.Subassembly.Variable");
            });

            modelBuilder.Entity<EnumAssemblyFormulaEvalType>(entity =>
            {
                entity.ToTable("enum.Assembly.Formula.EvalType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).HasColumnType("varchar(100)").IsRequired();

                entity.HasKey(t => t.ID);
            });

            modelBuilder.Entity<EnumAssemblyFormulaUseType>(entity =>
            {
                entity.ToTable("enum.Assembly.Formula.UseType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).HasColumnType("varchar(100)").IsRequired();
                entity.HasKey(t => t.ID);
            });

            modelBuilder.Entity<AssemblyLayout>(entity =>
            {
                entity.ToTable("Part.Subassembly.Layout");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.AssemblyLayout.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.AssemblyID).HasColumnName("SubassemblyID").IsRequired();
                entity.Property(e => e.LayoutType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.MachineLayoutType).HasColumnType("tinyint").HasDefaultValueSql("0").IsRequired();
                entity.Property(e => e.Name).HasColumnType("varchar(255)").IsRequired();

                //primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.Subassembly.Layout");
                //indexes
                entity.HasIndex(e => new { e.BID, e.AssemblyID, e.LayoutType }).HasName("IX_Part.Subassembly.Layout_Subassembly");
                //foreign keys
                entity.HasOne<EnumAssemblyLayoutType>()
                    .WithMany()
                    .HasForeignKey(e => e.LayoutType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Subassembly.Layout_enum.Part.Subassembly.LayoutType");
                entity.HasOne<EnumMachineLayoutType>()
                    .WithMany()
                    .HasForeignKey(e => e.MachineLayoutType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Subassembly.Layout_enum.Part.Subassembly.MachineLayoutType");
                entity.HasOne<AssemblyData>()
                    .WithMany(e => e.Layouts)
                    .HasForeignKey(e => new { e.BID, e.AssemblyID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.Subassembly.Layout_Part.Subassembly.Data");
                // ignore property
                entity.Ignore(e => e.TempLayoutID);
            });

            modelBuilder.Entity<UserDraft>(entity =>
            {
                entity.ToTable("User.Draft");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.UserDraft.AsString());
                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.ExpirationDT).HasDefaultValueSql("(dateadd(month,(1),getutcdate()))").IsRequired();
                entity.Property(e => e.UserLinkID).IsRequired();
                entity.Property(e => e.ObjectCTID).IsRequired();
                entity.Property(e => e.Description).HasMaxLength(255);
                entity.Property(e => e.Description).HasMaxLength(500);
                entity.Property(e => e.IsExpired).HasComputedColumnSql("(case when [ExpirationDT]<getutcdate() then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

                //primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_User.Draft");

                //indexes
                entity.HasIndex(e => new { e.BID, e.ObjectCTID, e.ObjectID, e.UserLinkID }).HasName("IX_User.Draft_Object");
                entity.HasIndex(e => new { e.BID, e.UserLinkID, e.ObjectCTID, e.ExpirationDT, e.ObjectID }).HasName("IX_User.Draft_User");

                //foreign keys
                entity.HasOne(e => e.UserLink).WithMany().HasForeignKey(e => new { e.BID, e.UserLinkID })
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_User.Draft_User.Link");

                entity.Ignore("ObjectJSON");
                entity.Ignore("Object");
            });

            modelBuilder.Entity<SystemLanguageType>(entity =>
            {
                entity.ToTable("System.Language.Type");

                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.Code).HasColumnType("VARCHAR(10)").HasMaxLength(10).IsRequired();
                entity.Property(e => e.GoogleCode).HasColumnType("VARCHAR(10)").HasMaxLength(10).IsRequired();
                entity.Property(e => e.Name).IsRequired();
                entity.Property(e => e.DisplayName).IsRequired();

                //primary key
                entity.HasKey(e => new { e.ID }).HasName("PK_System.Language.Type");

            });

            modelBuilder.Entity<SystemTranslationDefinition>(entity =>
            {
                entity.ToTable("System.Translation.Definition");

                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsVerified).IsRequired();
                entity.Property(e => e.SourceText).HasMaxLength(int.MaxValue).IsRequired();
                entity.Property(e => e.AltMeaning).HasMaxLength(int.MaxValue);
                entity.Property(e => e.TranslatedText).HasColumnType("NVARCHAR(MAX)").IsRequired();
                entity.Property(e => e.LanguageTypeId).IsRequired();

                //primary key
                entity.HasKey(e => new { e.ID }).HasName("PK_System.Translation.Definition");

                //indexes
                entity.HasIndex(e => new { e.LanguageTypeId, e.SourceText, e.AltMeaning }).HasName("IX_System.Language.SourceText");
                entity.HasIndex(e => new { e.LanguageTypeId, e.IsVerified }).HasName("IX_System.Language.Verified");

                //foreign keys
                entity.HasOne(e => e.LanguageType).WithMany().HasForeignKey(e => e.LanguageTypeId)
                    .OnDelete(DeleteBehavior.Restrict).HasConstraintName("FK_System.Language.Definition.LanguageType");
            });

            modelBuilder.Entity<SystemAssemblyVariable>(entity =>
            {
                entity.ToTable("System.Part.Subassembly.Variable");

                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.SystemAssemblyVariable.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.ElementType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.IsDisabled).IsRequired();
                entity.Property(e => e.SystemVariableID).HasColumnType("smallint");
                entity.Property(e => e.Label).HasMaxLength(255);
                entity.Property(e => e.AltText).HasMaxLength(255);
                entity.Property(e => e.ElementUseCount).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.UnitID).HasColumnType("tinyint");

                //primary key
                entity.HasKey(e => e.ID).HasName("PK_System.Subassembly.Part.Variable");

                //foreign keys
                entity.HasOne<EnumDataType>()
                    .WithMany()
                    .HasForeignKey(t => t.DataType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Part.Subassembly.Variable_enum.Part.Subassembly.List.DataType");

                entity.HasOne<EnumAssemblyElementType>()
                    .WithMany()
                    .HasForeignKey(t => t.ElementType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Part.Subassembly.Variable_enum.Part.Subassembly.ElementType");

                entity.HasOne<EnumAssemblyLabelType>()
                    .WithMany()
                    .HasForeignKey(t => t.LabelType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_System.Part.Subassembly.Variable_enum.Part.Subassembly.LabelType");
            });

            modelBuilder.Entity<SimpleSystemAssemblyVariable>(entity =>
            {
                entity.ToTable("System.Part.Subassembly.Variable.SimpleList");

                MapSimpleListItem<SimpleSystemAssemblyVariable, short>(entity, ClassType.SystemAssemblyVariable.AsString());
            });

            modelBuilder.Entity<TaxabilityCode>(entity =>
            {
                entity.ToTable("Accounting.Tax.Code");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Accounting.Tax.Code");
                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive, e.IsSystem }).HasName("IX_Accounting.Tax.Code_Name");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.TaxabilityCode.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.IsSystem).IsRequired();
                entity.Property(e => e.Name).HasMaxLength(100).IsRequired();
                entity.Property(e => e.Description).HasMaxLength(200);
                entity.Property(e => e.TaxCode).HasMaxLength(100);
                entity.Property(e => e.IsTaxExempt).IsRequired();
                entity.Property(e => e.HasExcludedTaxItems).IsRequired();

                entity.Ignore("TaxItems");

                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(e => e.BID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Tax.Code_Business.Data");
            });

            modelBuilder.Entity<TaxabilityCodeItemExemptionLink>(entity =>
            {
                entity.ToTable("Accounting.Tax.Code.ItemExemptionLink");

                entity.HasKey(e => new { e.BID, e.TaxCodeID, e.TaxItemID }).HasName("PK_Accounting.Tax.Code.ItemExemptionLink");
                entity.HasIndex(e => new { e.BID, e.TaxItemID }).HasName("IX_Accounting.Tax.Code.ItemExemptionLink_TaxItem");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.TaxCodeID).IsRequired();
                entity.Property(e => e.TaxItemID).IsRequired();

                entity.HasOne(e => e.TaxabilityCode)
                    .WithMany(e => e.TaxabilityCodeItemExemptionLinks)
                    .HasForeignKey(e => new { e.BID, e.TaxCodeID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Tax.Code.ItemExemptionLink_Accounting.Tax.Code");

                entity.HasOne(e => e.TaxItem)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.TaxItemID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Tax.Code.ItemExemptionLink_Accounting.Tax.Item");

            });

            modelBuilder.Entity<QuickItemData>(entity =>
            {
                entity.ToTable("Part.QuickItem.Data");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.QuickItem.AsString());
                entity.Property(e => e.ModifiedDT).HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).IsRequired();
                entity.Property(e => e.Name).HasColumnType("varchar(255)").IsRequired();

                entity.Property(e => e.IsGlobalItem).HasComputedColumnSql("(isnull(case when [CompanyID] IS NULL AND ([EmployeeID] IS NULL OR [IsShared]=(1)) then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.IsCompanyItem).HasComputedColumnSql("(isnull(case when [CompanyID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Part.QuickItem.Data");
                entity.HasIndex(e => new { e.BID, e.IsGlobalItem, e.Name, e.IsActive }).HasName("IX_Part.QuickItem.Data_Global");
                entity.HasIndex(e => new { e.BID, e.CompanyID, e.Name, e.IsActive }).HasName("IX_Part.QuickItem.Data_Company");
                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.Name, e.IsActive }).HasName("IX_Part.QuickItem.Data_Employee");

                entity.Ignore("DataContent");
                entity.Ignore("Categories");

                entity.HasOne(e => e.Company)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.CompanyID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.QuickItem.Data_Company.Data");

                entity.HasOne(e => e.Employee)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.EmployeeID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.QuickItem.Data_Employee.Data");

                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Part.QuickItem.Data_Business.Data");

            });

            modelBuilder.Entity<QuickItemCategoryLink>(entity =>
            {
                entity.ToTable("Part.QuickItem.CategoryLink");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.QuickItemID).IsRequired();
                entity.Property(e => e.CategoryID).IsRequired();

                entity.HasKey(e => new { e.BID, e.QuickItemID, e.CategoryID }).HasName("PK_Part.QuickItem.CategoryLink");
                entity.HasIndex(e => new { e.BID, e.CategoryID }).HasName("IX_Part.QuickItem.CategoryLink_Category");

                entity.HasOne<FlatListItem>(e => e.Category)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.CategoryID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.QuickItem.CategoryLink_List.FlatList.Data");

                entity.HasOne<QuickItemData>(e => e.QuickItem)
                    .WithMany(i => i.CategoryLinks)
                    .HasForeignKey(e => new { e.BID, e.QuickItemID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Part.QuickItem.CategoryLink_Part.QuickItem.Data");
            });

            modelBuilder.Entity<SimpleQuickItemData>(entity =>
            {
                entity.ToTable("Part.QuickItem.SimpleList");

                MapSimpleListItem<SimpleQuickItemData, int>(entity, ClassType.QuickItem.AsString());
            });

            modelBuilder.Entity<SurchargeDef>(entity =>
            {
                entity.ToTable("Part.Surcharge.Data");

                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.SurchargeDef.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.Name).HasColumnType("nvarchar(255)").IsRequired();
                entity.Property(e => e.CompanyID).HasColumnType("INT");
                entity.Property(e => e.DefaultFixedFee).HasColumnType("decimal(18,6)");
                entity.Property(e => e.DefaultPerUnitFee).HasColumnType("decimal(18,6)");


                entity.HasIndex(e => new { e.BID, e.CompanyID, e.SortIndex, e.IsActive })
                    .HasName("IX_Part.Surcharge.Data_Company")
                    //.HasFilter("[CompanyID] IS NOT NULL")
                    ;

                entity.HasIndex(e => new { e.BID, e.Name, e.IsActive })
                    .HasName("IX_Part.Surcharge.Data_Name");

                entity.HasIndex(e => new { e.BID, e.SortIndex, e.Name, e.IsActive })
                    .HasName("IX_Part.Surcharge.Data_Sort");

                entity.HasOne(e => e.TaxCode)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxCodeID })
                    .HasConstraintName("FK_Part.Surcharge.Data_Accounting.Tax.Code")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.Company)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.CompanyID })
                    .HasConstraintName("FK_Part.Surcharge.Data_Company.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.IncomeAccount)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.IncomeAccountID })
                    .HasConstraintName("FK_Part.Surcharge.Data_Accounting.GL.Account")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<OrderItemSurcharge>(entity =>
            {
                entity.ToTable("Order.Item.Surcharge");

                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.OrderItemSurcharge.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(7)").HasDefaultValueSql("(sysutcdatetime())");
                //entity.Property(e => e.ValidToDT).HasDefaultValueSql("(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))");
                entity.Property(e => e.Name).HasColumnType("nvarchar(255)").IsRequired();
                entity.Property(e => e.PricePreTax).HasColumnName("Price.PreTax").HasColumnType("decimal(18,6)");
                entity.Property(e => e.PriceTaxable).HasColumnName("Price.Taxable").HasColumnType("decimal(18,6)");
                entity.Property(e => e.PriceTax).HasColumnName("Price.Tax").HasColumnType("decimal(18,6)");
                entity.Property(e => e.PriceTotal).HasColumnName("Price.Total").HasColumnType("decimal(18,6)").HasComputedColumnSql("([Price.PreTax] + [Price.Tax])");
                entity.Property(e => e.PriceIsOV).IsRequired();
                entity.Property(e => e.DefaultFixedFee).HasColumnType("decimal(18,6)");
                entity.Property(e => e.DefaultPerUnitFee).HasColumnType("decimal(18,6)");


                entity.HasIndex(e => new { e.BID, e.OrderItemID, e.Number })
                    .HasName("IX_Order.Item.Surcharge_LineItem");

                entity.HasOne(e => e.IncomeAccount)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.IncomeAccountID })
                    .HasConstraintName("FK_Order.Item.Surcharge_Accounting.GL.Account")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.SurchargeDef)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.SurchargeDefID })
                    .HasConstraintName("FK_Order.Item.Surcharge_Part.Surcharge.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.TaxCode)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.TaxCodeID })
                    .HasConstraintName("FK_Order.Item.Surcharge_Accounting.Tax.Code")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.OrderItem)
                    .WithMany(e => e.Surcharges)
                    .HasForeignKey(t => new { t.BID, t.OrderItemID })
                    .HasConstraintName("FK_Order.Item.Surcharge_Order.Item.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.Ignore(e => e.TaxInfoList);
            });

            modelBuilder.Entity<EnumURLRegistrationType>(entity =>
            {
                entity.ToTable("enum.URL.RegistrationType");

                entity.HasKey(e => new { e.ID }).HasName("PK_enum.URL.RegistrationType");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<URLRegistrationData>(entity =>
            {
                entity.ToTable("URL.Registration.Data");

                entity.Property(e => e.BID).IsRequired();
                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_URL.Registration.Data_Business.Data");
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.URLRegistration.AsString()).IsRequired();
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())").IsRequired();
                entity.Property(e => e.CreatedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())").IsRequired();
                entity.Property(e => e.IsActive).HasComputedColumnSql(@"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                AND [UseCount] < coalesce([MaxUseCount], (1000000))
                                    AND ([UseCount] = (0)
                                            OR[ExpireAfterUseDays] IS NULL
                                            OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                then CONVERT([bit], (1))
                                else CONVERT([bit],(0)) end,(0)))");
                entity.Property(e => e.IsRevoked).IsRequired();
                entity.Property(e => e.PublishedURLRoot).HasColumnType("varchar(1024)").IsRequired();
                entity.Property(e => e.PublishedURLBase).HasColumnType("varchar(1024)");
                entity.Property(e => e.PublishedURLPath).HasDefaultValueSql("(newid())").IsRequired();
                entity.Property(e => e.PublishedURLFull).HasColumnType("varchar(1024)").HasComputedColumnSql(@"(concat([PublishedURLRoot]
                                        + case when [PublishedURLRoot] like '%/'
                                            then ''
                                            else '/' end
                                    ,[PublishedURLBase]
                                        + case when [PublishedURLBase] like '%/'
                                            then ''
                                            else '/' end
                                    ,lower(replace([PublishedURLPath],'-',''))))").IsRequired();
                entity.Property(e => e.PublishedShortenedURL).HasColumnType("varchar(50)");
                entity.Property(e => e.RegistrationType).IsRequired();
                entity.HasOne<EnumURLRegistrationType>()
                    .WithMany()
                    .HasForeignKey(e => new { e.RegistrationType })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_URL.Registration.Data_enum.URL.RegistrationType");

                entity.Property(e => e.TargetClassTypeID);
                entity.Property(e => e.TargetID);
                entity.Property(e => e.TargetURL).HasColumnType("varchar(2048)");
                entity.Property(e => e.TargetURLParameters).HasColumnType("varchar(max)");

                entity.Property(e => e.TargetCompanyID).HasColumnType("int");
                entity.Property(e => e.TargetContactID).HasColumnType("int");
                entity.Property(e => e.TargetEmployeeID).HasColumnType("smallint");

                entity.Property(e => e.ExpirationDT).HasColumnType("DATETIME2(2)").IsRequired();
                entity.Property(e => e.UseCount).HasDefaultValueSql("((0))").IsRequired();
                entity.Property(e => e.FirstUseDT).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.LastUseDT).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.MaxUseCount).HasDefaultValueSql("((1000000))").IsRequired();
                entity.Property(e => e.ExpireAfterUseDays);
                entity.Property(e => e.MetaDataJSON).HasColumnType("nvarchar(max)");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_URL.Registration.Data");
                entity.HasIndex(e => new { e.BID, e.TargetClassTypeID, e.TargetID }).HasName("IX_URL.Registration.Data_TargetObject");
                entity.HasIndex(e => new { e.PublishedURLPath }).HasName("IX_URL.Registration.Data_URLPath");
                entity.HasIndex(e => new { e.BID, e.TargetEmployeeID }).HasName("IX_URL.Registration.Data_Employee");
                entity.HasIndex(e => new { e.BID, e.TargetContactID }).HasName("IX_URL.Registration.Data_Contact");
                entity.HasIndex(e => new { e.BID, e.TargetCompanyID }).HasName("IX_URL.Registration.Data_Company");

            });

            modelBuilder.Entity<URLRegistrationLookupHistory>(entity =>
            {
                entity.ToTable("URL.Registration.Lookup.History");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.URLRegistrationLookupHistory.AsString()).IsRequired();
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())").IsRequired();
                entity.Property(e => e.RegistrationID).IsRequired();
                entity.HasOne(e => e.Registration)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.RegistrationID })
                    .HasPrincipalKey(x => new { x.BID, x.ID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_URL.Registration.Lookup.History_URL.Registration.Data");
                entity.Property(e => e.WasSuccessful).IsRequired();
                entity.Property(e => e.IP).HasColumnType("varchar(20)");
                entity.Property(e => e.BrowserTypeName).HasColumnType("varchar(50)");
                entity.Property(e => e.BrowserVersion).HasColumnType("varchar(50)");
                entity.Property(e => e.Platform).HasColumnType("varchar(50)");
                entity.Property(e => e.DeviceName).HasColumnType("varchar(50)");
                entity.Property(e => e.DeviceInfo).HasColumnType("varchar(1024)");
                entity.Property(e => e.MaxWidth);
                entity.Property(e => e.MaxHeight);

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_URL.Registration.Lookup.History");
                entity.HasIndex(e => new { e.BID, e.RegistrationID }).HasName("IX_URL.Registration.Lookup.History_Registration");

            });

            modelBuilder.Entity<DMAccessToken>(entity =>
            {
                entity.ToTable("DM.Access.Token");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.DMAccessToken.AsString()).IsRequired();
                entity.Property(e => e.ExpirationDate).HasColumnType("DATETIME2(2)").IsRequired();
                entity.Property(e => e.URL).HasColumnType("varchar(1024)").IsRequired();
                entity.Property(e => e.SASToken).HasColumnType("varchar(1024)").IsRequired();
                entity.Property(e => e.ViewedCount).HasDefaultValueSql("((0))").IsRequired();
                entity.Property(e => e.ViewsRemaining);
                entity.Property(e => e.ExpirationDate).HasColumnType("DATETIME2(2)");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_DM.Access.Token");
                entity.HasIndex(e => new { e.BID, e.URL }).HasName("IX_DM.Access.Token_URL");

            });

            modelBuilder.Entity<EnumEmailAccountStatus>(entity =>
            {
                entity.ToTable("enum.EmailAccountStatus");

                entity.HasKey(e => new { e.ID }).HasName("PK_enum.EmailAccountStatus");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<EmailAccountData>(entity =>
            {
                entity.ToTable("Email.Account.Data");

                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Email.Account.Data");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.EmailAccount.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("GetUTCDate()").IsRequired();
                entity.Property(e => e.IsActive).HasComputedColumnSql("(isnull(case when [StatusType]=(2) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))").IsRequired();
                entity.Property(e => e.StatusType).IsRequired();
                entity.Property(e => e.DomainEmailID).IsRequired();
                entity.Property(e => e.UserName).HasColumnType("varchar(255)").IsRequired();
                entity.Property(e => e.DomainName).HasColumnType("varchar(255)").IsRequired();
                entity.Property(e => e.IsPrivate).HasComputedColumnSql("(isnull(case when [EmployeeID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))").IsRequired();
                entity.Property(e => e.EmployeeID);
                entity.Property(e => e.Credentials).HasColumnType("varchar(max)").IsRequired();
                entity.Property(e => e.CredentialsExpDT).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.LastEmailSuccessDT).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.LastEmailFailureDT).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.EmailAddress).HasComputedColumnSql("(concat([UserName],'@',[DomainName]))");
                entity.Property(e => e.AliasUserNames).HasColumnType("varchar(1024)");
                entity.Property(e => e.DisplayName).HasColumnType("varchar(255)");

                entity.Ignore(e => e.Teams);

                entity.HasIndex(e => new { e.BID, e.EmployeeID })
                    .HasName("IX_Email.Account.Data_Employee");

                entity.HasIndex(e => new { e.BID, e.EmailAddress, e.IsActive, e.IsPrivate })
                    .HasName("IX_Email.Account.Data_Email");

                entity.HasOne(e => e.Business).WithMany()
                    .HasForeignKey(e => e.BID)
                    //.OnDelete(DeleteBehavior.NoAction)
                    .HasConstraintName("FK_Email.Account.Data_Business.Data");

                entity.HasOne(e => e.Employee)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.EmployeeID })
                    .HasConstraintName("FK_Email.Account.Data_Employee.Data")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.EnumEmailAccountStatus)
                    .WithMany()
                    .HasForeignKey(t => new { t.StatusType })
                    .HasConstraintName("FK_Email.Account.Data_enum.Email.AccountStatus")
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(e => e.DomainEmail)
                    .WithMany()
                    .HasForeignKey(t => new { t.BID, t.DomainEmailID })
                    .HasConstraintName("FK_Email.Account.Data_Domain.Email.Data")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SimpleEmailAccountData>(entity =>
            {
                entity.ToTable("EmailAccountData.SimpleList");

                MapSimpleListItem<SimpleEmailAccountData, short>(entity, ClassType.EmailAccount.AsString());
            });

            modelBuilder.Entity<EmailAccountTeamLink>(entity =>
            {
                entity.ToTable("Email.Account.TeamLink");
                entity.HasKey(e => new { e.BID, e.EmailAccountID, e.TeamID })
                    .HasName("PK_Email.Account.TeamLink");

                entity.HasIndex(e => new { e.BID, e.TeamID })
                    .HasName("IX_Email.Account.TeamLink_Team");

                entity.HasOne(e => e.EmailAccount)
                    .WithMany(c => c.EmailAccountTeamLinks)
                    .HasForeignKey(e => new { e.BID, e.EmailAccountID })
                    .HasConstraintName("FK_Email.Account.TeamLink_Email.Account.Data");
                entity.HasOne(e => e.Team)
                    .WithMany(c => c.EmailAccountTeamLinks)
                    .HasForeignKey(e => new { e.BID, e.TeamID })
                    .HasConstraintName("FK_Email.Account.TeamLink_Employee.Team");
            });

            modelBuilder.Entity<EnumDashboardWidgetCategoryType>(entity =>
            {
                entity.ToTable("enum.Dashboard.Widget.CategoryType");

                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).IsUnicode(false);
            });

            modelBuilder.Entity<DashboardWidgetCategoryLink>(entity =>
            {
                entity.ToTable("System.Dashboard.Widget.CategoryLink");

                entity.HasKey(e => new { e.CategoryType, e.WidgetDefID });
                entity.HasIndex(e => new { e.WidgetDefID, e.CategoryType })
                    .HasName("IX_System.Dashboard.Widget.CategoryLink_WidgetDefID");

                entity.HasOne(t => t.WidgetDefinition)
                    .WithMany(e => e.WidgetCategoryLinks)
                    .HasForeignKey(t => new { t.WidgetDefID })
                    .HasConstraintName("FK_System.Dashboard.Widget.CategoryLink_System.Dashboard.Widget.Definition").OnDelete(DeleteBehavior.Restrict);
                entity.HasOne(t => t.EnumDashboardWidgetCategoryType)
                    .WithMany()
                    .HasForeignKey(t => new { t.CategoryType })
                    .HasConstraintName("FK_System.Dashboard.Widget.CategoryLink_enum.Dashboard.Widget.CategoryType").OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SystemMessageTemplateType>(entity =>
            {
                entity.ToTable("System.Message.TemplateType");

                //Columns
                entity.Property(e => e.AppliesToClassTypeID).HasColumnType("int").IsRequired();
                entity.Property(e => e.ID).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.Name).HasColumnType("varchar(100)").IsRequired();
                entity.Property(e => e.IsSystem).HasColumnType("bit").IsRequired();
                entity.Property(e => e.ChannelType).HasColumnType("tinyint").IsRequired();
                
                //primary key
                entity.HasKey(e => new { e.AppliesToClassTypeID, e.ID }).HasName("PK_System.Message.TemplateType");

                //foreign keys
                entity.HasOne<EnumMessageChannelType>().WithMany()
                    .HasForeignKey(e => e.ChannelType)
                    .HasConstraintName("FK_System.Message.TemplateType_enum.Message.ChannelType");
            });


            modelBuilder.Entity<MessageBodyTemplate>(entity =>
            {
                entity.ToTable("Message.Body.Template");

                // primary keys
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Message.Body.Template");

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14220))");
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(GETUTCDATE())");

                entity.Property(e => e.LocationID).HasColumnType("TINYINT");
                entity.Property(e => e.CompanyID).HasColumnType("INT");
                entity.Property(e => e.EmployeeID).HasColumnType("SMALLINT");
                entity.Property(e => e.AttachedFileNames).HasColumnType("VARCHAR(MAX)");

                entity.Ignore(e =>  e.Participants);

                // computed columns
                entity.Property(e => e.HasAttachment).HasComputedColumnSql("(ISNULL(CASE WHEN [AttachedFileCount]>(0) THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))");
                entity.Property(e => e.HasMergeFields).HasComputedColumnSql("(ISNULL(CASE WHEN LEN(MergeFieldList)>0 THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))");
                entity.Property(e => e.HasBody).HasComputedColumnSql("(ISNULL(CASE WHEN LEN(Body)>0 THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))");

                // foreign keys
                entity.HasOne(e => e.Business).WithMany().HasForeignKey(e => e.BID).HasConstraintName("FK_Message.Body.Template_Business.Data");
                entity.HasOne(e => e.Location).WithMany().HasForeignKey(e => new { e.BID, e.LocationID }).HasConstraintName("FK_Message.Body.Template_Location.Data");
                entity.HasOne(e => e.Company).WithMany().HasForeignKey(e => new { e.BID, e.CompanyID }).HasConstraintName("FK_Message.Body.Template_Company.Data");
                entity.HasOne(e => e.Employee).WithMany().HasForeignKey(e => new { e.BID, e.EmployeeID }).HasConstraintName("FK_Message.Body.Template_Employee.Data");
                entity.HasOne(e => e.TemplateType).WithMany().HasForeignKey(e => new { e.AppliesToClassTypeID, e.MessageTemplateType }).HasPrincipalKey(e => new { e.AppliesToClassTypeID, e.ID }).HasConstraintName("FK_Message.Body.Template_System.Message.TemplateType");

                // indexes
                entity.HasIndex(e => new { e.BID, e.AppliesToClassTypeID, e.MessageTemplateType, e.Name, e.IsActive }).HasName("IX_Message.Body.Template_Name");
                entity.HasIndex(e => new { e.BID, e.AppliesToClassTypeID, e.MessageTemplateType, e.SortIndex, e.IsActive }).HasName("IX_Message.Body.Template_Type");

            });

            modelBuilder.Entity<Reconciliation>(entity =>
            {
                entity.ToTable("Accounting.Reconciliation.Data");

                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.Reconciliation.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.CreatedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.LastAccountingDT).HasColumnType("DATETIME2(0)");
                entity.Property(e => e.AccountingDT).HasColumnType("DATETIME2(0)");
                entity.Property(e => e.IsAdjustmentEntry).HasColumnType("BIT");
                entity.Property(e => e.LocationID).HasColumnType("TINYINT");
                entity.Property(e => e.GLActivityID).HasColumnType("INT");
                entity.Property(e => e.StartingGLID).HasColumnType("INT");
                entity.Property(e => e.EndingGLID).HasColumnType("INT");
                entity.Property(e => e.EnteredByID).HasColumnType("SMALLINT");
                entity.Property(e => e.Description).HasColumnType("NVARCHAR(100)");
                entity.Property(e => e.Notes).HasColumnType("NVARCHAR(500)");
                entity.Property(e => e.ExportedDT).HasColumnType("DATETIME2(2) SPARSE");
                entity.Property(e => e.WasExported).HasColumnType("BIT").HasComputedColumnSql("(isnull(case when [ExportedDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.FormattedNumber).HasColumnType("NVARCHAR(32)");
                entity.Property(e => e.AdjustedReconciliationID).HasColumnType("INT SPARSE");
                entity.Property(e => e.ExportedByID).HasColumnType("SMALLINT");
                entity.Property(e => e.SyncedByID).HasColumnType("SMALLINT");
                entity.Property(e => e.SyncedDT).HasColumnType("DATETIME2(2)");
                entity.Property(e => e.TotalIncome).HasColumnType("DECIMAL(18,6)").HasDefaultValueSql("0");
                entity.Property(e => e.TotalPayments).HasColumnType("DECIMAL(18,6)").HasDefaultValueSql("0");
                entity.Property(e => e.IsEmpty).HasColumnType("BIT").HasDefaultValueSql("0");

                entity.HasKey(e => new { e.BID, e.ID })
                    .HasName("PK_Accounting.Reconciliation.Data");

                entity.HasOne<BusinessData>()
                    .WithMany()
                    .HasForeignKey(i => i.BID)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Business.Data");
                entity.HasOne<GLData>()
                    .WithMany()
                    .HasForeignKey(i => new { i.BID, i.StartingGLID})
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Accounting.GL.Data_First");
                entity.HasOne<GLData>()
                    .WithMany()
                    .HasForeignKey(i => new { i.BID, i.EndingGLID})
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Accounting.GL.Data_Last");
                entity.HasOne<EmployeeData>()
                    .WithMany()
                    .HasForeignKey(i => new { i.BID, i.EnteredByID})
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Employee.Data");
                entity.HasOne<LocationData>()
                    .WithMany()
                    .HasForeignKey(i => new { i.BID, i.LocationID})
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Location.Data");
                entity.HasOne(e => e.ExportedByEmployee)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.ExportedByID })
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Employee.Exported");
                entity.HasOne(e => e.SyncedByEmployee)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.SyncedByID})
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Employee.Synced");
                entity.HasOne(e => e.AdjustedReconciliation)
                    .WithMany()
                    .HasForeignKey(e => new { e.BID, e.AdjustedReconciliationID})
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Reconciliation.Data");
            });

            modelBuilder.Entity<ReconciliationItem>(entity =>
            {
                entity.ToTable("Accounting.Reconciliation.Item");

                entity.HasKey(e => new { e.BID, e.ID });
                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();

                entity.Property(e => e.ClassTypeID).HasComputedColumnSql(ClassType.ReconciliationItem.AsString());
                entity.Property(e => e.ModifiedDT).HasColumnType("DATETIME2(2)").HasDefaultValueSql("(getutcdate())");
                entity.Property(e => e.ReconciliationID).HasColumnType("INT");
                entity.Property(e => e.GLAccountID).HasColumnType("INT");
                entity.Property(e => e.PaymentMethodID).HasColumnType("TINYINT SPARSE");
                entity.Property(e => e.PaymentMasterCount).HasColumnType("SMALLINT SPARSE");
                entity.Property(e => e.PaymentApplicationCount).HasColumnType("SMALLINT SPARSE");
                entity.Property(e => e.Amount).HasColumnType("DECIMAL");
                entity.Property(e => e.Balance).HasColumnType("DECIMAL");
                entity.Property(e => e.IsPaymentSummary).HasColumnType("BIT").HasComputedColumnSql("(isnull(case when [PaymentMethodID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");
                entity.Property(e => e.CurrencyType).HasColumnType("TINYINT SPARSE");

                entity.HasKey(e => new { e.BID, e.ID })
                    .HasName("PK_Accounting.Reconciliation.Item");
                entity.HasIndex(e => new { e.BID, e.ReconciliationID, e.GLAccountID, e.PaymentMethodID })
                    .HasName("IX_Accounting.Reconciliation.Item_Master");

                entity.HasOne(i => i.Reconciliation)
                    .WithMany(t => t.Items)
                    .HasForeignKey(i => new { i.BID, i.ReconciliationID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Data_Accounting.Reconciliation.Item");
                entity.HasOne(i => i.GLAccount)
                    .WithMany()
                    .HasForeignKey(i => new { i.BID, i.GLAccountID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Item_Accounting.GL.Account");
                entity.HasOne(i => i.PaymentMethodNavigation)
                    .WithMany()
                    .HasForeignKey(i => new { i.BID, i.PaymentMethodID })
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Item_Accounting.Payment.Method");
                entity.HasOne(i => i.CurrencyTypeNavigation)
                    .WithMany()
                    .HasForeignKey(i => i.CurrencyType)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Accounting.Reconciliation.Iteml_enum.Accounting.CurrencyType");
            });

            modelBuilder.Entity<EnumOrderItemComponentType>(entity =>
            {
                entity.ToTable("enum.Order.Item.Component.Type");

                entity.Property(e => e.ID).HasColumnType("smallint");
                entity.Property(e => e.Name).HasColumnType("VARCHAR(100)");

            });

            modelBuilder.Entity<CompanyCustomDataValue>(entity =>
            {
                entity.ToView("Company.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("int");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<ContactCustomDataValue>(entity =>
            {
                entity.ToView("Contact.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("int");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<OpportunityCustomDataValue>(entity =>
            {
                entity.ToView("Opportunity.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("int");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<TransactionHeaderCustomDataValue>(entity =>
            {
                entity.ToView("Order.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("int");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<AssemblyCustomDataValue>(entity =>
            {
                entity.ToView("Assembly.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("int");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<EmployeeCustomDataValue>(entity =>
            {
                entity.ToView("Employee.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("smallint");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<LaborCustomDataValue>(entity =>
            {
                entity.ToView("Labor.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("int");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<LocationCustomDataValue>(entity =>
            {
                entity.ToView("Location.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<MachineCustomDataValue>(entity =>
            {
                entity.ToView("Machine.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("smallint");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            modelBuilder.Entity<MaterialCustomDataValue>(entity =>
            {
                entity.ToView("Material.Custom.Data.Value");

                entity.Property(e => e.BID).HasColumnType("smallint");
                entity.Property(e => e.ID).HasColumnType("int");
                entity.Property(e => e.ClassTypeID).HasColumnType("int");
                entity.Property(e => e.CustomFieldDefID).HasColumnType("smallint");
                entity.Property(e => e.Value).HasColumnType("string");
                entity.Property(e => e.ValueAsNumber).HasColumnType("decimal(18, 4)");
                entity.Property(e => e.ValueAsBoolean).HasColumnType("bit");
            });

            //call model creation from partial classes
            this.CreateMessageModels(modelBuilder);
        }

        private void MapSimpleListItem<T, I>(EntityTypeBuilder<T> entity, string classTypeID) where T : SimpleListItem<I> where I : struct, IConvertible
        {
            entity.Property(e => e.ClassTypeID)
                .HasColumnName("ClassTypeID")
                .HasComputedColumnSql(classTypeID)
                .ValueGeneratedOnAddOrUpdate();

            entity.Property(e => e.IsActive);
        }

        private void MapSimpleEnumListItem<T, I>(EntityTypeBuilder<T> entity) where T : SimpleEnumListItem<I>
        {
            entity.Property(e => e.IsActive);
        }

        private void MapLocator<T, LocatorParentID, LocatorParent>(EntityTypeBuilder<T> entity)
            where T : BaseLocator<LocatorParentID, LocatorParent>
            where LocatorParentID : struct, IConvertible
            where LocatorParent : IAtom<LocatorParentID>
        {
            entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)")
                .HasDefaultValueSql("(getutcdate())");
            entity.Property(e => e.IsValid);
            entity.Property(e => e.IsVerified);
            entity.Property(e => e.SortIndex).HasDefaultValueSql("50").HasColumnType("smallint");
            entity.Property(e => e.HasImage);
            entity.Property(e => e.MetaDataXML).HasColumnName("MetaData")
                .HasColumnType("xml");

            entity.Ignore(e => e.MetaDataObject);
        }
    }
}
