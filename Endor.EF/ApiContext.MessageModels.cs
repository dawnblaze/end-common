using System;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Endor.Models;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Endor.EF
{
    public partial class ApiContext
    {
        public virtual DbSet<EnumMessageParticipantRoleType> EnumMessageParticipantRoleType { get; set; }
        public virtual DbSet<EnumMessageChannelType> EnumMessageChannelType { get; set; }
        public virtual DbSet<MessageHeader> MessageHeader { get; set; }
        public virtual DbSet<MessageObjectLink> MessageObjectLink { get; set; }
        public virtual DbSet<MessageDeliveryRecord> MessageDeliveryRecord { get; set; }
        public virtual DbSet<MessageParticipantInfo> MessageParticipantInfo { get; set; }
        public virtual DbSet<MessageBody> MessageBody { get; set; }

        protected void CreateMessageModels(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EnumMessageParticipantRoleType>(entity =>
            {
                entity.ToTable("enum.Message.Participant.RoleType");

                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).HasMaxLength(100).IsRequired();
            });

            modelBuilder.Entity<EnumMessageChannelType>(entity =>
            {
                entity.ToTable("enum.Message.ChannelType");

                entity.HasKey(e => e.ID);
                entity.Property(e => e.ID).HasColumnType("tinyint");
                entity.Property(e => e.Name).HasMaxLength(100).IsRequired();
            });

            modelBuilder.Entity<MessageBody>(entity =>
            {
                entity.ToTable("Message.Body");

                //Columns
                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14110))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()");
                entity.Property(e => e.BodyFirstLine).HasColumnType("nvarchar(100)");
                entity.Property(e => e.Subject).HasColumnType("nvarchar(250)"); ;
                entity.Property(e => e.HasBody).IsRequired();
                entity.Property(e => e.AttachedFileCount).HasColumnType("tinyint").IsRequired().HasDefaultValue("0");
                entity.Property(e => e.AttachedFileNames).HasColumnType("varchar(max)");
                entity.Property(e => e.HasAttachment)
                    .HasComputedColumnSql(
                        "(isnull(case when [AttachedFileCount]>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,CONVERT([bit],(0))))");

                entity.Property(e => e.MetaData).HasColumnType("xml");
                entity.Property(e => e.WasModified).IsRequired();
                entity.Property(e => e.SizeInKB).HasColumnType("int");

                //primary key
                entity.HasKey(e => new {e.BID, e.ID}).HasName("PK_Message.Body");

                //foreign keys
                entity.HasOne<BusinessData>().WithMany()
                    .HasForeignKey(e => e.BID)
                    .HasConstraintName("FK_Message.Content_Business.Data");

            });
            
            modelBuilder.Entity<MessageDeliveryRecord>(entity =>
            {
                entity.ToTable("Message.Delivery.Record");

                //Columns
                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ParticipantID).HasColumnType("int").IsRequired();
                entity.Property(e => e.AttemptNumber).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14114))");
                entity.Property(e => e.IsPending)
                    .HasComputedColumnSql(@"(IsNull(case when [AttemptedDT] IS NOT NULL OR [ScheduledDT] IS NULL 
                                                        then CONVERT([bit],(0)) 
                                                        else CONVERT([bit],(1)) end, 
                                            CONVERT([bit],(1))))");
                entity.Property(e => e.ScheduledDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.AttemptedDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.WasSuccessful).HasColumnType("bit").IsRequired();
                entity.Property(e => e.FailureMessage).HasColumnType("nvarchar(max)");
                entity.Property(e => e.MetaData).HasColumnType("xml");

                //primary key
                entity.HasKey(e => new { e.BID, e.ParticipantID, e.AttemptNumber}).HasName("PK_Message.Delivery.Record");

                //foreign keys
                entity.HasOne<MessageParticipantInfo>().WithMany(participant => participant.MessageDeliveryRecords)
                    .HasForeignKey(e => new { e.BID, e.ParticipantID })
                    .HasConstraintName("FK_Message.Delivery.Record_Message.Participant");
            });

            modelBuilder.Entity<MessageParticipantInfo>(entity =>
            {
                entity.ToTable("Message.Participant.Info");

                //Columns
                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14112))");
                entity.Property(e => e.BodyID).HasColumnType("int").IsRequired();
                entity.Property(e => e.ParticipantRoleType).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.Channel).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.UserName).HasColumnType("nvarchar(512)");
                entity.Property(e => e.IsMergeField).HasColumnType("bit").IsRequired();
                entity.Property(e => e.EmployeeID).HasColumnType("smallint");
                entity.Property(e => e.TeamID).HasColumnType("int");
                entity.Property(e => e.ContactID).HasColumnType("int");
                entity.Property(e => e.DeliveredDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.IsDelivered)
                    .HasComputedColumnSql("(case when [DeliveredDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)");


                //primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Message.Participant.Info");

                // indexes
                entity.HasIndex(e => new { e.BID, e.BodyID }).HasName("IX_Message.Participant.Info_Body");
                entity.HasIndex(e => new { e.BID, e.ContactID }).HasName("IX_Message.Participant.Info_Contact");
                entity.HasIndex(e => new { e.BID, e.EmployeeID }).HasName("IX_Message.Participant.Info_Employee");
                entity.HasIndex(e => new { e.BID, e.TeamID }).HasName("IX_Message.Participant.Info_Team");

                //foreign keys
                entity.HasOne(e=>e.EmployeeData).WithMany()
                    .HasForeignKey(e => new { e.BID, e.EmployeeID })
                    .HasConstraintName("FK_Message.Participant.Info_Employee");

                entity.HasOne(e => e.EmployeeTeam).WithMany()
                    .HasForeignKey(e => new { e.BID, e.TeamID })
                    .HasConstraintName("FK_Message.Participant.Info_Team");

                entity.HasOne<EnumMessageChannelType>().WithMany()
                    .HasForeignKey(e => e.Channel)
                    .HasConstraintName("FK_Message.Participant.Info_enum.Message.ChannelType");

                entity.HasOne<EnumMessageParticipantRoleType>().WithMany()
                    .HasForeignKey(e => e.ParticipantRoleType)
                    .HasConstraintName("FK_Message.Participant.Info_enum.Message.Participant.RoleType");

                entity.HasOne(e => e.MesssageBody).WithMany(body => body.MessageParticipantInfos)
                    .HasForeignKey(e => new { e.BID, e.BodyID })
                    .HasConstraintName("FK_Message.Participant.Info_Message.Body");

                entity.HasOne<ContactData>().WithMany()
                    .HasForeignKey(e => new { e.BID, e.ContactID })
                    .HasConstraintName("FK_Notification.Participant.Info_Contact.Data");

            });

            modelBuilder.Entity<MessageHeader>(entity =>
            {
                entity.ToTable("Message.Header");
                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).HasColumnType("int").IsRequired();
                entity.Property(e => e.ClassTypeID).HasComputedColumnSql("((14111))");
                entity.Property(e => e.ModifiedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()");
                entity.Property(e => e.ReceivedDT).HasColumnType("datetime2(2)").HasDefaultValueSql("GetUTCDate()");
                entity.Property(e => e.EmployeeID).HasColumnType("smallint").IsRequired();
                entity.Property(e => e.ParticipantID).HasColumnType("int").IsRequired();
                entity.Property(e => e.BodyID).HasColumnType("int").IsRequired();
                entity.Property(e => e.IsRead).HasColumnType("bit").IsRequired();
                entity.Property(e => e.ReadDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.IsDeleted).HasColumnType("bit").IsRequired();
                entity.Property(e => e.IsExpired).HasColumnType("bit").IsRequired();
                entity.Property(e => e.DeletedOrExpiredDT).HasColumnType("datetime2(2)");
                entity.Property(e => e.Channels).HasColumnType("tinyint").IsRequired();
                entity.Property(e => e.InSentFolder).HasColumnType("bit").IsRequired();
 
                //primary key
                entity.HasKey(e => new { e.BID, e.ID }).HasName("PK_Message.Header");

                // indexes
                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.ReceivedDT }).HasName("IX_Message.Header_Employee");
                entity.HasIndex(e => new { e.BID, e.ParticipantID }).HasName("IX_Message.Header_Participant");
                entity.HasIndex(e => new { e.BID, e.EmployeeID, e.Channels, e.IsRead }).HasName("IX_Message.Header_EmployeeChannels");

                
                //foreign keys
                entity.HasOne<MessageParticipantInfo>()
                    .WithMany(mpi => mpi.MessageHeaders)
                    .HasForeignKey(e => new { e.BID, e.ParticipantID })
                    .HasConstraintName("FK_Message.Header_Message.Participant.Info");

                entity.HasOne<EmployeeData>().WithMany()
                    .HasForeignKey(e => new { e.BID, e.EmployeeID })
                    .HasConstraintName("FK_Message.Inbox_Employee.Data");

                entity.HasOne(e => e.MessageBody).WithMany()
                    .HasForeignKey(e => new { e.BID, e.BodyID })
                    .HasConstraintName("FK_Message.Inbox_Message.Content");
                //[NotMapped]
                entity.Ignore(e => e._messageBodyString);

            });

            modelBuilder.Entity<MessageObjectLink>(entity =>
            {
                entity.ToTable("Message.Object.Link");

                entity.Property(e => e.BID).IsRequired();
                entity.Property(e => e.ID).HasColumnType("int").IsRequired();
                entity.Property(e => e.BodyID).HasColumnType("int").IsRequired();
                entity.Property(e => e.LinkedObjectClassTypeID).HasColumnType("int").IsRequired();
                entity.Property(e => e.LinkedObjectID).HasColumnType("int").IsRequired();
                entity.Property(e => e.IsSourceOfMessage).HasColumnName("SourceMessage").HasColumnType("bit").IsRequired().HasDefaultValue(false);
                entity.Property(e => e.DisplayText).HasColumnType("nvarchar(100)").IsRequired();

                //primary key
                entity.HasKey(e => new { e.BID, e.BodyID, e.LinkedObjectClassTypeID, e.LinkedObjectID }).HasName("PK_Message.Object.Link");

                // indexes
                entity.HasIndex(e => new { e.BID, e.LinkedObjectClassTypeID, e.LinkedObjectID }).HasName("IX_Message.Object.Link_LinkedObject");

                //foreign keys
                entity.HasOne<MessageBody>().WithMany(body => body.MessageObjectLinks)
                    .HasForeignKey(e => new { e.BID, e.BodyID })
                    .HasConstraintName("FK_Message.Related.Records_Message.Data");

            });

        }
    }
}