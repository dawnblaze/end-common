﻿using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Migrations.Operations.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.EF
{
    public static class MigrationBuilderExtensions
    {
        /// <summary>
        /// Creates a simplelist view
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="name"></param>
        /// <param name="viewSql"></param>
        /// <param name="tableName"></param>
        public static void CreateSimpleListView(this MigrationBuilder migrationBuilder, string name, string viewSql, string tableName)
        {
            migrationBuilder.Sql($@"CREATE VIEW [{name}]
                    AS
                SELECT	{viewSql}
                FROM [{tableName}];");
        }

        /// <summary>
        /// Drops a simplelist view
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="name"></param>
        public static void DropSimpleListView(this MigrationBuilder migrationBuilder, string name)
        {
            migrationBuilder.Sql($"DROP VIEW [{name}]");
        }

        /// <summary>
        /// Drops a Default Value Constraint if it exists
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        /// <param name="columnName">Name of the column</param>
        public static void DropDefaultConstraintIfExists(this MigrationBuilder migrationBuilder, string tableName, string columnName)
        {
            migrationBuilder.Sql(
                $@"
                    DECLARE @var1 sysname;
                    SELECT @var1 = [d].[name]
                    FROM[sys].[default_constraints][d]
                    INNER JOIN[sys].[columns][c] ON[d].[parent_column_id] = [c].[column_id] AND[d].[parent_object_id] = [c].[object_id]
                    WHERE([d].[parent_object_id] = OBJECT_ID(N'[{tableName}]') AND[c].[name] = N'{columnName}');
                    IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [{tableName}] DROP CONSTRAINT [' + @var1 + '];');
                ");
        }


        /// <summary>
        /// Disables System Versioning on a table if it is already enabled
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        public static void DisableSystemVersioningIfEnabled(this MigrationBuilder migrationBuilder, string tableName)
        {
            migrationBuilder.Sql(
                $@"
                    IF EXISTS (SELECT temporal_type FROM sys.tables WHERE object_id = OBJECT_ID('dbo.[{tableName}]', 'u') AND temporal_type = 2)
                    BEGIN
                        ALTER TABLE dbo.[{tableName}] SET(SYSTEM_VERSIONING = OFF);
                    END
                ");
        }

        /// <summary>
        /// Enables System Versioning on a table
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        /// <param name="historyTableName">Name of the history table without square brackets</param>
        public static void EnableSystemVersioning(this MigrationBuilder migrationBuilder, string tableName, string historyTableName = null)
        {
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentException($"A value for parameter {nameof(tableName)} must be supplied!");

            if (string.IsNullOrWhiteSpace(historyTableName))
                historyTableName = $"Historic.{tableName}";

            migrationBuilder.Sql($@"ALTER TABLE dbo.[{tableName}] SET(SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[{historyTableName}]));");
        }
    }
}
