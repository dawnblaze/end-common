﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.EF
{
    public interface IRequiresReindex
    {
        int? ReindexClasstype();
        List<int> ReindexClasstypes();
        (int ctid, int id)? ReindexEntity();
        List<(int ctid, int id)> ReindexEntities();
    }
}
