using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180405115242_MaterialCategory_MaterialCategoryLink")]
    public partial class MaterialCategory_MaterialCategoryLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Material.Category",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12002"),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    ParentID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Material.Category", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Material.Category_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.Material.Category_Part.Machine.Category",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Part.Material.Category",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Part.Material.CategoryLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    PartID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Material.CategoryLink", x => new { x.BID, x.PartID, x.CategoryID });
                    table.ForeignKey(
                        name: "FK_Part.Material.CategoryLink_Part.Material.Category",
                        columns: x => new { x.BID, x.CategoryID },
                        principalTable: "Part.Material.Category",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Material.CategoryLink_Part.Material.Data",
                        columns: x => new { x.BID, x.PartID },
                        principalTable: "Part.Material.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Category_Name_IsActive",
                table: "Part.Material.Category",
                columns: new[] { "BID", "Name", "IsActive", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Category_ParentID_Name",
                table: "Part.Material.Category",
                columns: new[] { "BID", "ParentID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.CategoryLink_Category",
                table: "Part.Material.CategoryLink",
                columns: new[] { "BID", "CategoryID", "PartID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Material.CategoryLink");

            migrationBuilder.DropTable(
                name: "Part.Material.Category");
        }
    }
}

