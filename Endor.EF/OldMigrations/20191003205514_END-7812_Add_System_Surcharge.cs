﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END7812_Add_System_Surcharge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT [Accounting.GL.Account] ([BID], [ID], [ModifiedDT], [IsActive], [CanEdit], [Name], [Number], [GLAccountType], [ParentID], [ExportAccountName], [ExportAccountNumber], [ExportGLAccountType])
VALUES (-1, 1000, '2018-01-01', 1, 0, N'Assets', 1000, 10, NULL, NULL, NULL, 10)
     , (-1, 1100, '2018-01-01', 1, 1, N'Bank Account', 1100, 11, 1000, NULL, NULL, 11)
     , (-1, 1200, '2018-01-01', 1, 0, N'Accounts Receivable', 1200, 12, 1000, NULL, NULL, 12)
     , (-1, 1300, '2018-01-01', 1, 0, N'WIP', 1300, 10, 1000, NULL, NULL, 10)
     , (-1, 1400, '2018-01-01', 1, 0, N'Built', 1400, 10, 1000, NULL, NULL, 10)
     , (-1, 1500, '2018-01-01', 1, 0, N'Inventory', 1500, 10, 1000, NULL, NULL, 10)
     , (-1, 2000, '2018-01-01', 1, 0, N'Liabilities', 2000, 20, NULL, NULL, NULL, 20)
     , (-1, 2100, '2018-01-01', 1, 0, N'Customer Deposits', 2100, 20, 2000, NULL, NULL, 20)
     , (-1, 2200, '2018-01-01', 1, 0, N'Customer Credits', 2200, 20, 2000, NULL, NULL, 20)
     , (-1, 2300, '2018-01-01', 1, 0, N'Production Deliverables', 2300, 20, 2000, NULL, NULL, 20)
     , (-1, 2400, '2018-01-01', 1, 0, N'Sales Tax Payable', 2400, 20, 2000, NULL, NULL, 20)
     , (-1, 3000, '2018-01-01', 0, 0, N'Equity', 3000, 30, NULL, NULL, NULL, 30)
     , (-1, 3100, '2018-01-01', 0, 0, N'Retained Earnings', 3100, 30, 3000, NULL, NULL, 30)
     , (-1, 4000, '2018-01-01', 1, 0, N'Income', 4000, 40, NULL, NULL, NULL, 40)
     , (-1, 4800, '2018-01-01', 1, 0, N'Adjustment Income', 4000, 40, NULL, NULL, NULL, 40)
     , (-1, 4900, '2018-01-01', 1, 0, N'Finance Charges', 4900, 40, 4000, NULL, NULL, 40)
     , (-1, 5000, '2018-01-01', 1, 0, N'COGS', 5000, 50, NULL, NULL, NULL, 50)
     , (-1, 5100, '2018-01-01', 1, 1, N'Printing COGS', 5100, 50, 5000, NULL, NULL, 50)
     , (-1, 5110, '2018-01-01', 1, 1, N'Paper', 5110, 50, 5100, NULL, NULL, 50)
     , (-1, 5120, '2018-01-01', 1, 1, N'Ink', 5120, 50, 5100, NULL, NULL, 50)
     , (-1, 5130, '2018-01-01', 1, 1, N'Lamination', 5130, 50, 5100, NULL, NULL, 50)
     , (-1, 5200, '2018-01-01', 1, 1, N'Installation COGS', 5200, 50, 5000, NULL, NULL, 50)
     , (-1, 5210, '2018-01-01', 1, 1, N'Installation Equipment Rental', 5210, 50, 5200, NULL, NULL, 50)
     , (-1, 5220, '2018-01-01', 1, 1, N'Installation Supplies', 5220, 50, 5200, NULL, NULL, 50)
     , (-1, 6000, '2018-01-01', 1, 0, N'Expenses', 6000, 60, NULL, NULL, NULL, 60)
     , (-1, 6100, '2018-01-01', 1, 0, N'Credit Adjustments', 6100, 60, 6000, NULL, NULL, 60)
;

INSERT INTO [Accounting.Tax.Code] ([BID],[ID],[ModifiedDT],[IsActive],[IsSystem],[Name],[Description],[TaxCode],[IsTaxExempt],[HasExcludedTaxItems])
VALUES (-1,0,'Nov 14 2018',1,1,'Goods','(Default) Used for taxable goods and services.​','',0,0)
     , (-1,1,'Nov 14 2018',1,0,'Service','Miscellaneous services which are not subject to a service-specific tax levy. This category will only treat services as taxable if the jurisdiction taxes services generally.','19000',0,0)
     , (-1,2,'Nov 14 2018',1,0,'Printing Service','Services provided to apply graphics and/or text to paper or other substrates which do not involve an exchange of tangible personal property','19009',0,0)
     , (-1,3,'Nov 14 2018',1,0,'Installation Service','Installation services separately stated from sales of tangible personal property','19001',0,0)
     , (-1,4,'Nov 14 2018',1,0,'Training Service','Services provided to educate users on the proper use of a product','19004',0,0)
     , (-1,5,'Nov 14 2018',1,0,'Advertising Service','Services rendered for advertising which do not include the exchange of tangible personal property','19001',0,0)
     , (-1,6,'Nov 14 2018',1,0,'Professional Service','Professional services which are not subject to a service-specific tax levy','19005',0,0)
     , (-1,7,'Nov 14 2018',1,0,'Repair Service','Services provided to restore tangible personal property to working order or optimal functionality','19007',0,0)
     , (-1,8,'Nov 14 2018',1,0,'Clothing','All human wearing apparel suitable for general use','20010',0,0)
     , (-1,9,'Nov 14 2018',1,0,'Digital Goods','Digital products transferred electronically','31000',0,0)
     , (-1,10,'Nov 14 2018',1,0,'Books','Books, printed','81100',0,0)
     , (-1,11,'Nov 14 2018',1,0,'Textbooks','Textbooks, printed','81110',0,0)
     , (-1,12,'Nov 14 2018',1,0,'Religious books','Religious books and manuals, printed','81120',0,0)
     , (-1,91,'Nov 14 2018',1,1,'FinanceCharge','Used for Finance Charges','99999',0,0)
     , (-1,92,'Nov 14 2018',1,0,'Handling','Used for Handling','',0,0)
     , (-1,98,'Nov 14 2018',1,0,'Shipping','Used for Shipping and Freight','',0,0)
     , (-1,99,'Nov 14 2018',1,1,'ForcedExempt','Used to force an Item as TaxExempt.','99999',0,0)
;

INSERT INTO [Part.Surcharge.Data]
(BID, ID, Name, IsActive, SortIndex, AppliedByDefault, IncomeAccountID, TaxCodeID)
VALUES
(-1, 1, 'Adjustment', 1, 999, 0, 4800, 0)
;

exec [Util.Table.CopyDefaultRecords] 'Accounting.GL.Account';
exec [Util.Table.CopyDefaultRecords] 'Accounting.Tax.Code';
exec [Util.Table.CopyDefaultRecords] 'Part.Surcharge.Data';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
