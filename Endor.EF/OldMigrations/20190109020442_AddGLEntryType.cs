using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddGLEntryType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            IF (OBJECT_ID('DF_Activity_GLActivity_CreatedDate', 'D') IS NOT NULL)
                BEGIN
                    ALTER TABLE [Activity.GLActivity]
                DROP CONSTRAINT[DF_Activity_GLActivity_CreatedDate]
            END
            ");

            migrationBuilder.Sql(@"
                ALTER TABLE[dbo].[Activity.GLActivity]
                DROP CONSTRAINT[FK_Activity.GLActivity_Enum_Activity]
            ");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "Activity.GLActivity");
            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDT",
                table: "Activity.GLActivity",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "(getutcdate())");

            /*migrationBuilder.RenameColumn("CreatedDate", "[Activity.GLActivity]", "CreatedDT");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedDT",
                table: "Activity.GLActivity",
                type: "datetime(2)",
                nullable: false,
                defaultValueSql: "(getutcdate())");*/

            migrationBuilder.AlterColumn<string>(
                name: "MetaData",
                table: "Activity.GLActivity",
                type: "XML SPARSE",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "xml",
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "CompletedByID",
                table: "Activity.GLActivity",
                type: "smallint SPARSE",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "Activity.GLActivity",
                nullable: true);



            migrationBuilder.AddColumn<byte>(
                name: "GLEntryType",
                table: "Activity.GLActivity",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Activity.GLActivity",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "OrderID",
                table: "Activity.GLActivity",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TeamID",
                table: "Activity.GLActivity",
                nullable: true,
                computedColumnSql: "CONVERT([int],NULL)",
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<byte>(
                name: "ActivityType",
                table: "Activity.GLActivity",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([tinyint],(50)),(50)))",
                oldClrType: typeof(byte));

            migrationBuilder.CreateTable(
                name: "enum.Activity.GLEntryType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Accounting.GLEntryType", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activity.GLActivity_GLEntryType",
                table: "Activity.GLActivity",
                column: "GLEntryType");

            migrationBuilder.CreateIndex(
                name: "IX_Activity.GLActivity_BID_CompanyID",
                table: "Activity.GLActivity",
                columns: new[] { "BID", "CompanyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Activity.GLActivity_BID_CompletedByID",
                table: "Activity.GLActivity",
                columns: new[] { "BID", "CompletedByID" });

            migrationBuilder.CreateIndex(
                name: "IX_Activity.GLActivity_BID_OrderID",
                table: "Activity.GLActivity",
                columns: new[] { "BID", "OrderID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Activity.GL_enum.Activity.GLEntryType",
                table: "Activity.GLActivity",
                column: "GLEntryType",
                principalTable: "enum.Activity.GLEntryType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Activity.GLActivity_Company.Data_BID_CompanyID",
                table: "Activity.GLActivity",
                columns: new[] { "BID", "CompanyID" },
                principalTable: "Company.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Activity.GL_Employee.Data",
                table: "Activity.GLActivity",
                columns: new[] { "BID", "CompletedByID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Activity.GLActivity_Order.Data_BID_OrderID",
                table: "Activity.GLActivity",
                columns: new[] { "BID", "OrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
                  Delete [enum.ActivityType] where ID in (51,52,53,54,55,61,62,63,64)
            ");

            migrationBuilder.Sql(@"
                INSERT[enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(50, N'Accounting Entry',3)
            ");

            migrationBuilder.Sql(@"
                INSERT [enum.Activity.GLEntryType] ([ID], [Name]) VALUES (0, N'Manual GL Entry')
    , (1, N'Order New')
    , (2, N'Order Edited')
    , (3, N'Order Status Change')
    , (4, N'Order Part Usage Applied')
    , (5, N'Order Tax Computation')
    , (10, N'Order Maintenance Recompute')
    , (11, N'Payment Applied')
    , (12, N'Payment Edited')
    , (13, N'Payment Refunded')
    , (14, N'Payment Voided')
    , (15, N'Credit Applied')
    , (16, N'Credit Adjusted')
    , (17, N'Credit Refunded')
    , (18, N'Credit Voided')
    , (30, N'Starting Balance Summary')
    , (31, N'Daily Summary')
    , (32, N'Monthly Summary')
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activity.GL_enum.Accouting.GLEntryType",
                table: "Activity.GLActivity");

            migrationBuilder.DropForeignKey(
                name: "FK_Activity.GLActivity_Company.Data_BID_CompanyID",
                table: "Activity.GLActivity");

            migrationBuilder.DropForeignKey(
                name: "FK_Activity.GL_Employee.Data",
                table: "Activity.GLActivity");

            migrationBuilder.DropForeignKey(
                name: "FK_Activity.GLActivity_Order.Data_BID_OrderID",
                table: "Activity.GLActivity");

            migrationBuilder.DropTable(
                name: "enum.Activity.GLEntryType");

            migrationBuilder.DropIndex(
                name: "IX_Activity.GLActivity_GLEntryType",
                table: "Activity.GLActivity");

            migrationBuilder.DropIndex(
                name: "IX_Activity.GLActivity_BID_CompanyID",
                table: "Activity.GLActivity");

            migrationBuilder.DropIndex(
                name: "IX_Activity.GLActivity_BID_CompletedByID",
                table: "Activity.GLActivity");

            migrationBuilder.DropIndex(
                name: "IX_Activity.GLActivity_BID_OrderID",
                table: "Activity.GLActivity");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "Activity.GLActivity");

            migrationBuilder.DropColumn(
                name: "CreatedDT",
                table: "Activity.GLActivity");

            migrationBuilder.DropColumn(
                name: "GLEntryType",
                table: "Activity.GLActivity");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Activity.GLActivity");

            migrationBuilder.DropColumn(
                name: "OrderID",
                table: "Activity.GLActivity");

            migrationBuilder.AlterColumn<int>(
                name: "TeamID",
                table: "Activity.GLActivity",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true,
                oldComputedColumnSql: "CONVERT([int],NULL)");

            migrationBuilder.AlterColumn<string>(
                name: "MetaData",
                table: "Activity.GLActivity",
                type: "xml",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "XML SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CompletedByID",
                table: "Activity.GLActivity",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint SPARSE");

            migrationBuilder.AlterColumn<byte>(
                name: "ActivityType",
                table: "Activity.GLActivity",
                nullable: false,
                oldClrType: typeof(byte),
                oldComputedColumnSql: "(isnull(CONVERT([tinyint],(50)),(50)))");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "Activity.GLActivity",
                type: "date",
                nullable: false,
                defaultValueSql: "(getutcdate())");
        }
    }
}
