using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180129201228_SourceToOriginWithSprocs")]
    public partial class SourceToOriginWithSprocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_CRM.Source",
                table: "Company.Data");

            migrationBuilder.Sql(@"
DROP VIEW [dbo].[CRM.Source.SimpleList]
                ");

            migrationBuilder.DropTable(
                name: "CRM.Source");

            migrationBuilder.RenameColumn(
                name: "SourceID",
                table: "[Opportunity.Data]",
                newName: "OriginID");

            migrationBuilder.RenameColumn(
                name: "SourceID",
                table: "[Company.Data]",
                newName: "OriginID");

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_BID_OriginID",
                table: "Company.Data",
                columns: new[] { "BID", "OriginID" });

            //migrationBuilder.RenameIndex(
            //    name: "[IX_Company.Data_BID_SourceID]",
            //    table: "[Company.Data]",
            //    newName: "IX_Company.Data_BID_OriginID");

            migrationBuilder.CreateTable(
                name: "CRM.Origin",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2012))"),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsLocked = table.Column<bool>(nullable: false),
                    IsTopLevel = table.Column<bool>(nullable: true, computedColumnSql: "(case when [ParentID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    ParentID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.Origin", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CRM.Origin_CRM.Origin",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "CRM.Origin",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Origin_Name",
                table: "CRM.Origin",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Origin_Parent",
                table: "CRM.Origin",
                columns: new[] { "BID", "ParentID", "Name" });

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_CRM.Origin",
                table: "Company.Data",
                columns: new[] { "BID", "OriginID" },
                principalTable: "CRM.Origin",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[CRM.Origin.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [CRM.Origin]
GO
");

            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.SetParent')
  DROP PROCEDURE [Origin.Action.SetParent];
GO

-- ========================================================
-- Name: [Origin.Action.SetParent]
--
-- Description: This procedure sets the referenced Origin's parent ID
--
-- Sample Use:   EXEC dbo.[Origin.Action.SetParent] @BID=1, @OriginID=1, @ParentID=NULL
-- ========================================================
CREATE PROCEDURE [Origin.Action.SetParent]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @OriginID       INT     -- = 2

        , @ParentID       INT     = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @OriginID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Origin Specified. OriginID='+CONVERT(VARCHAR(12),@OriginID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update it
    UPDATE O
    SET   
        ParentID = @ParentID
      , ModifiedDT = GetUTCDate()
    FROM [CRM.Origin] O
    WHERE BID = @BID and ID = @OriginID
      AND COALESCE(ParentID,~@ParentID) != @ParentID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.SetActive')
  DROP PROCEDURE [Origin.Action.SetActive];
GO

-- ========================================================
-- Name: [Origin.Action.SetActive]
--
-- Description: This procedure sets the referenced Origin's active status
--
-- Sample Use:   EXEC dbo.[Origin.Action.SetActive] @BID=1, @OriginID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [Origin.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @OriginID       INT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @OriginID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Origin Specified. OriginID='+CONVERT(VARCHAR(12),@OriginID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update it
    UPDATE O
    SET   
        IsActive = @IsActive
      , ModifiedDT = GetUTCDate()
    FROM [CRM.Origin] O
    WHERE BID = @BID and ID = @OriginID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.CanDelete')
  DROP PROCEDURE [Origin.Action.CanDelete];
GO

-- ========================================================
-- Name: [Origin.Action.CanDelete]
--
-- Description: This procedure checks if the origin is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Origin.Action.CanDelete] @BID=1, @OriginID=1
-- ========================================================
CREATE PROCEDURE [Origin.Action.CanDelete]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @OriginID       INT     -- = 2

        , @Result         BIT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @OriginID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Origin Specified. OriginID='+CONVERT(VARCHAR(12),@OriginID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    SET @Result = 
    CASE WHEN 
        NOT EXISTS(SELECT * FROM [Customer.Data] WHERE OriginID = @OriginID)
            --AND NOT EXISTS(SELECT * FROM [Order.Data] WHERE OriginID = @OriginID)
        THEN 1 
        ELSE 0 
    END;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP VIEW [dbo].[CRM.Origin.SimpleList]
                ");

            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.CanDelete')
  DROP PROCEDURE [Origin.Action.CanDelete];
GO
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.SetActive')
  DROP PROCEDURE [Origin.Action.SetActive];
GO
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.SetParent')
  DROP PROCEDURE [Origin.Action.SetParent];
GO
");
            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_CRM.Origin",
                table: "Company.Data");

            migrationBuilder.DropTable(
                name: "CRM.Origin");

            migrationBuilder.RenameColumn(
                name: "OriginID",
                table: "Opportunity.Data",
                newName: "SourceID");

            migrationBuilder.RenameColumn(
                name: "OriginID",
                table: "[Company.Data]",
                newName: "SourceID");

            migrationBuilder.DropIndex(
                name: "IX_Company.Data_BID_OriginID",
                table: "[Company.Data]");

            //migrationBuilder.RenameIndex(
            //    name: "IX_Company.Data_BID_OriginID",
            //    table: "[Company.Data]",
            //    newName: "IX_Company.Data_BID_SourceID");

            migrationBuilder.CreateTable(
                name: "CRM.Source",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2012))"),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsLocked = table.Column<bool>(nullable: false),
                    IsTopLevel = table.Column<bool>(nullable: true, computedColumnSql: "(case when [ParentID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    ParentID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.Source", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CRM.Source_CRM.Source",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "CRM.Source",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Source_Name",
                table: "CRM.Source",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Source_Parent",
                table: "CRM.Source",
                columns: new[] { "BID", "ParentID", "Name" });

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_CRM.Source",
                table: "Company.Data",
                columns: new[] { "BID", "SourceID" },
                principalTable: "CRM.Source",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[CRM.Source.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [CRM.Source]
GO");
        }
    }
}

