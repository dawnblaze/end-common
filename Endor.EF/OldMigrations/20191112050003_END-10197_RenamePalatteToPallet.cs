﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10197_RenamePalatteToPallet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [enum.Unit]
SET [Name] = 'Pallet'
WHERE ID = 7");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [enum.Unit]
SET [Name] = 'Palatte'
WHERE ID = 7");
        }
    }
}
