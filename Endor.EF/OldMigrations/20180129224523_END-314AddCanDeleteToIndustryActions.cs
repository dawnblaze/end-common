using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180129224523_END-314AddCanDeleteToIndustryActions")]
    public partial class END314AddCanDeleteToIndustryActions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/****** Object:  StoredProcedure [dbo].[Industry.Action.CanDelete]    Script Date: 1/29/2018 4:44:14 PM ******/
IF EXISTS(select * from sys.objects where name = 'Industry.Action.CanDelete' and type = 'P')
  DROP PROCEDURE [dbo].[Industry.Action.CanDelete]
GO

/****** Object:  StoredProcedure [dbo].[Industry.Action.CanDelete]    Script Date: 1/29/2018 4:44:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Industry.Action.CanDelete]
--
-- Description: This procedure checks if the Industry is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Industry.Action.CanDelete] @BID=1, @IndustryID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Industry.Action.CanDelete]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @IndustryID       INT     -- = 2

        , @Result         BIT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Industry specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @IndustryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Industry Specified. IndustryID='+CONVERT(VARCHAR(12),@IndustryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    SET @Result = 
    CASE WHEN 
        NOT EXISTS(SELECT * FROM [Company.Data] WHERE IndustryID = @IndustryID)
            --AND NOT EXISTS(SELECT * FROM [Order.Data] WHERE IndustryID = @IndustryID)
        THEN 1 
        ELSE 0 
    END;

    SELECT @Result as Result;
END

GO

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/****** Object:  StoredProcedure [dbo].[Industry.Action.CanDelete]    Script Date: 1/29/2018 4:44:14 PM ******/
If Exists(select * from sys.objects where name = 'Industry.Action.CanDelete')
  DROP PROCEDURE[dbo].[Industry.Action.CanDelete]
GO
");
        }
    }
}

