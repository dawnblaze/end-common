using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180117011351_InitialMigrationCleanup")]
    public partial class InitialMigrationCleanup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Location.Data",
                table: "Accounting.Payment.Term.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term",
                table: "Accounting.Payment.Term.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment",
                table: "Accounting.Tax.Group.AssessmentLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group",
                table: "Accounting.Tax.Group.AssessmentLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group",
                table: "Accounting.Tax.Group.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Location.Data",
                table: "Accounting.Tax.Group.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Locator_enum.LocatorType",
                table: "Business.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Campaign.Data_Business.Data",
                table: "Campaign.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Campaign.Data_enum.Campaign.Type",
                table: "Campaign.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Campaign.Data_Campaign.Custom",
                table: "Campaign.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_Business.Data",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_enum.CRM.Company.Status",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_Company.Custom",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_Location.Data",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Locator_enum.LocatorType",
                table: "Company.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Locator_Company.Data",
                table: "Company.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_Business.Data",
                table: "Contact.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_Company.Data",
                table: "Contact.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_Contact.Custom",
                table: "Contact.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Locator_enum.LocatorType",
                table: "Contact.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Locator_Contact.Data",
                table: "Contact.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField_Business.Data",
                table: "CRM.CustomField.Def");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField_enum.CustomField.DataType",
                table: "CRM.CustomField.Def");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField_enum.CustomField.InputType",
                table: "CRM.CustomField.Def");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_Business.Data",
                table: "CRM.CustomField.Helper");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType",
                table: "CRM.CustomField.Helper");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType",
                table: "CRM.CustomField.Helper");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField",
                table: "CRM.CustomField.HelperLink");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper",
                table: "CRM.CustomField.HelperLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Data_Business.Data",
                table: "Employee.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Locator_enum.LocatorType",
                table: "Employee.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Locator_Employee.Data",
                table: "Employee.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Team.LocationLink_Location.Data",
                table: "Employee.Team.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Team.LocationLink_Employee.Team",
                table: "Employee.Team.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.TeamLink_Employee.Data",
                table: "Employee.TeamLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.TeamLink_Employee.Team",
                table: "Employee.TeamLink");

            migrationBuilder.DropForeignKey(
                name: "FK_enum.CustomField.InputType_enum.CustomField.DataType",
                table: "enum.CustomField.InputType");

            migrationBuilder.DropForeignKey(
                name: "FK_enum.Locator.SubType_enum.Locator.Type",
                table: "enum.Locator.SubType");

            migrationBuilder.DropForeignKey(
                name: "FK_List.Filter.EmployeeSubscription_Employee.Data",
                table: "List.Filter.EmployeeSubscription");

            migrationBuilder.DropForeignKey(
                name: "FK_List.Filter.EmployeeSubscription_List.Filter",
                table: "List.Filter.EmployeeSubscription");

            migrationBuilder.DropForeignKey(
                name: "FK_Location.Data_Business.Data",
                table: "Location.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Location.Locator_enum.LocatorType",
                table: "Location.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Location.Locator_Location.Data",
                table: "Location.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Opportunity.Data_Business.Data",
                table: "Opportunity.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Opportunity.Data_Opportunity.Custom",
                table: "Opportunity.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Option.Data_System.Option.Definition",
                table: "Option.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Right.Group.Link_Security.Right.Group1",
                table: "Security.Right.Group.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Right.Group.Link_Security.Right.Group",
                table: "Security.Right.Group.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Right.Link_Security.Right.Collection",
                table: "Security.Right.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Role.Link_enum.User.Role.Type",
                table: "Security.Role.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Role.Link_Security.Right.Group",
                table: "Security.Role.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_System.Option.Definition_System.Option.Category",
                table: "System.Option.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_System.Option.Definition_enum.CustomField.DataType",
                table: "System.Option.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_User.Link_enum.User.Role.Type",
                table: "User.Link");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Location.Data",
                table: "Accounting.Payment.Term.LocationLink",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term",
                table: "Accounting.Payment.Term.LocationLink",
                columns: new[] { "BID", "TermID" },
                principalTable: "Accounting.Payment.Term",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment",
                table: "Accounting.Tax.Group.AssessmentLink",
                columns: new[] { "BID", "AssessmentID" },
                principalTable: "Accounting.Tax.Assessment",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group",
                table: "Accounting.Tax.Group.AssessmentLink",
                columns: new[] { "BID", "GroupID" },
                principalTable: "Accounting.Tax.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group",
                table: "Accounting.Tax.Group.LocationLink",
                columns: new[] { "BID", "GroupID" },
                principalTable: "Accounting.Tax.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Location.Data",
                table: "Accounting.Tax.Group.LocationLink",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Locator_enum.LocatorType",
                table: "Business.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Data_Business.Data",
                table: "Campaign.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Data_enum.Campaign.Type",
                table: "Campaign.Data",
                column: "CampaignType",
                principalTable: "enum.Campaign.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Data_Campaign.Custom",
                table: "Campaign.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Campaign.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Business.Data",
                table: "Company.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_enum.CRM.Company.Status",
                table: "Company.Data",
                column: "StatusID",
                principalTable: "enum.CRM.Company.Status",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Company.Custom",
                table: "Company.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Company.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Location.Data",
                table: "Company.Data",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Locator_enum.LocatorType",
                table: "Company.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Locator_Company.Data",
                table: "Company.Locator",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Company.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Business.Data",
                table: "Contact.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Company.Data",
                table: "Contact.Data",
                columns: new[] { "BID", "CompanyID" },
                principalTable: "Company.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Contact.Custom",
                table: "Contact.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Contact.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Locator_enum.LocatorType",
                table: "Contact.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Locator_Contact.Data",
                table: "Contact.Locator",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField_Business.Data",
                table: "CRM.CustomField.Def",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField_enum.CustomField.DataType",
                table: "CRM.CustomField.Def",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField_enum.CustomField.InputType",
                table: "CRM.CustomField.Def",
                column: "InputType",
                principalTable: "enum.CustomField.InputType",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_Business.Data",
                table: "CRM.CustomField.Helper",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType",
                table: "CRM.CustomField.Helper",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType",
                table: "CRM.CustomField.Helper",
                column: "HelperType",
                principalTable: "enum.CustomField.HelperType",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField",
                table: "CRM.CustomField.HelperLink",
                columns: new[] { "BID", "CustomFieldID" },
                principalTable: "CRM.CustomField.Def",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper",
                table: "CRM.CustomField.HelperLink",
                columns: new[] { "BID", "HelperID" },
                principalTable: "CRM.CustomField.Helper",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Data_Business.Data",
                table: "Employee.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Locator_enum.LocatorType",
                table: "Employee.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Locator_Employee.Data",
                table: "Employee.Locator",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Team.LocationLink_Location.Data",
                table: "Employee.Team.LocationLink",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Team.LocationLink_Employee.Team",
                table: "Employee.Team.LocationLink",
                columns: new[] { "BID", "TeamID" },
                principalTable: "Employee.Team",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.TeamLink_Employee.Data",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.TeamLink_Employee.Team",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "TeamID" },
                principalTable: "Employee.Team",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_enum.CustomField.InputType_enum.CustomField.DataType",
                table: "enum.CustomField.InputType",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_enum.Locator.SubType_enum.Locator.Type",
                table: "enum.Locator.SubType",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_List.Filter.EmployeeSubscription_Employee.Data",
                table: "List.Filter.EmployeeSubscription",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_List.Filter.EmployeeSubscription_List.Filter",
                table: "List.Filter.EmployeeSubscription",
                columns: new[] { "BID", "FilterID" },
                principalTable: "List.Filter",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Location.Data_Business.Data",
                table: "Location.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Location.Locator_enum.LocatorType",
                table: "Location.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Location.Locator_Location.Data",
                table: "Location.Locator",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Opportunity.Data_Business.Data",
                table: "Opportunity.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Opportunity.Data_Opportunity.Custom",
                table: "Opportunity.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Opportunity.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Option.Data_System.Option.Definition",
                table: "Option.Data",
                column: "OptionID",
                principalTable: "System.Option.Definition",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Right.Group.Link_Security.Right.Group1",
                table: "Security.Right.Group.Link",
                columns: new[] { "BID", "ChildGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Right.Group.Link_Security.Right.Group",
                table: "Security.Right.Group.Link",
                columns: new[] { "BID", "ParentGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Right.Link_Security.Right.Collection",
                table: "Security.Right.Link",
                columns: new[] { "BID", "RightGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Role.Link_enum.User.Role.Type",
                table: "Security.Role.Link",
                column: "RoleType",
                principalTable: "enum.User.Role.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Role.Link_Security.Right.Group",
                table: "Security.Role.Link",
                columns: new[] { "BID", "RightGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_System.Option.Definition_System.Option.Category",
                table: "System.Option.Definition",
                column: "CategoryID",
                principalTable: "System.Option.Category",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_System.Option.Definition_enum.CustomField.DataType",
                table: "System.Option.Definition",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_User.Link_enum.User.Role.Type",
                table: "User.Link",
                column: "RoleType",
                principalTable: "enum.User.Role.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Location.Data",
                table: "Accounting.Payment.Term.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term",
                table: "Accounting.Payment.Term.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment",
                table: "Accounting.Tax.Group.AssessmentLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group",
                table: "Accounting.Tax.Group.AssessmentLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group",
                table: "Accounting.Tax.Group.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Location.Data",
                table: "Accounting.Tax.Group.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Locator_enum.LocatorType",
                table: "Business.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Campaign.Data_Business.Data",
                table: "Campaign.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Campaign.Data_enum.Campaign.Type",
                table: "Campaign.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Campaign.Data_Campaign.Custom",
                table: "Campaign.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_Business.Data",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_enum.CRM.Company.Status",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_Company.Custom",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_Location.Data",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Locator_enum.LocatorType",
                table: "Company.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Locator_Company.Data",
                table: "Company.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_Business.Data",
                table: "Contact.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_Company.Data",
                table: "Contact.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_Contact.Custom",
                table: "Contact.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Locator_enum.LocatorType",
                table: "Contact.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Locator_Contact.Data",
                table: "Contact.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField_Business.Data",
                table: "CRM.CustomField.Def");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField_enum.CustomField.DataType",
                table: "CRM.CustomField.Def");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField_enum.CustomField.InputType",
                table: "CRM.CustomField.Def");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_Business.Data",
                table: "CRM.CustomField.Helper");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType",
                table: "CRM.CustomField.Helper");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType",
                table: "CRM.CustomField.Helper");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField",
                table: "CRM.CustomField.HelperLink");

            migrationBuilder.DropForeignKey(
                name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper",
                table: "CRM.CustomField.HelperLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Data_Business.Data",
                table: "Employee.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Locator_enum.LocatorType",
                table: "Employee.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Locator_Employee.Data",
                table: "Employee.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Team.LocationLink_Location.Data",
                table: "Employee.Team.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Team.LocationLink_Employee.Team",
                table: "Employee.Team.LocationLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.TeamLink_Employee.Data",
                table: "Employee.TeamLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.TeamLink_Employee.Team",
                table: "Employee.TeamLink");

            migrationBuilder.DropForeignKey(
                name: "FK_enum.CustomField.InputType_enum.CustomField.DataType",
                table: "enum.CustomField.InputType");

            migrationBuilder.DropForeignKey(
                name: "FK_enum.Locator.SubType_enum.Locator.Type",
                table: "enum.Locator.SubType");

            migrationBuilder.DropForeignKey(
                name: "FK_List.Filter.EmployeeSubscription_Employee.Data",
                table: "List.Filter.EmployeeSubscription");

            migrationBuilder.DropForeignKey(
                name: "FK_List.Filter.EmployeeSubscription_List.Filter",
                table: "List.Filter.EmployeeSubscription");

            migrationBuilder.DropForeignKey(
                name: "FK_Location.Data_Business.Data",
                table: "Location.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Location.Locator_enum.LocatorType",
                table: "Location.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Location.Locator_Location.Data",
                table: "Location.Locator");

            migrationBuilder.DropForeignKey(
                name: "FK_Opportunity.Data_Business.Data",
                table: "Opportunity.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Opportunity.Data_Opportunity.Custom",
                table: "Opportunity.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Option.Data_System.Option.Definition",
                table: "Option.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Right.Group.Link_Security.Right.Group1",
                table: "Security.Right.Group.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Right.Group.Link_Security.Right.Group",
                table: "Security.Right.Group.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Right.Link_Security.Right.Collection",
                table: "Security.Right.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Role.Link_enum.User.Role.Type",
                table: "Security.Role.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_Security.Role.Link_Security.Right.Group",
                table: "Security.Role.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_System.Option.Definition_System.Option.Category",
                table: "System.Option.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_System.Option.Definition_enum.CustomField.DataType",
                table: "System.Option.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_User.Link_enum.User.Role.Type",
                table: "User.Link");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Location.Data",
                table: "Accounting.Payment.Term.LocationLink",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term",
                table: "Accounting.Payment.Term.LocationLink",
                columns: new[] { "BID", "TermID" },
                principalTable: "Accounting.Payment.Term",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment",
                table: "Accounting.Tax.Group.AssessmentLink",
                columns: new[] { "BID", "AssessmentID" },
                principalTable: "Accounting.Tax.Assessment",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group",
                table: "Accounting.Tax.Group.AssessmentLink",
                columns: new[] { "BID", "GroupID" },
                principalTable: "Accounting.Tax.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group",
                table: "Accounting.Tax.Group.LocationLink",
                columns: new[] { "BID", "GroupID" },
                principalTable: "Accounting.Tax.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Location.Data",
                table: "Accounting.Tax.Group.LocationLink",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Locator_enum.LocatorType",
                table: "Business.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Data_Business.Data",
                table: "Campaign.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Data_enum.Campaign.Type",
                table: "Campaign.Data",
                column: "CampaignType",
                principalTable: "enum.Campaign.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Data_Campaign.Custom",
                table: "Campaign.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Campaign.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Business.Data",
                table: "Company.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_enum.CRM.Company.Status",
                table: "Company.Data",
                column: "StatusID",
                principalTable: "enum.CRM.Company.Status",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Company.Custom",
                table: "Company.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Company.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Location.Data",
                table: "Company.Data",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Locator_enum.LocatorType",
                table: "Company.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Locator_Company.Data",
                table: "Company.Locator",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Company.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Business.Data",
                table: "Contact.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Company.Data",
                table: "Contact.Data",
                columns: new[] { "BID", "CompanyID" },
                principalTable: "Company.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Contact.Custom",
                table: "Contact.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Contact.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Locator_enum.LocatorType",
                table: "Contact.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Locator_Contact.Data",
                table: "Contact.Locator",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField_Business.Data",
                table: "CRM.CustomField.Def",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField_enum.CustomField.DataType",
                table: "CRM.CustomField.Def",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField_enum.CustomField.InputType",
                table: "CRM.CustomField.Def",
                column: "InputType",
                principalTable: "enum.CustomField.InputType",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_Business.Data",
                table: "CRM.CustomField.Helper",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType",
                table: "CRM.CustomField.Helper",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType",
                table: "CRM.CustomField.Helper",
                column: "HelperType",
                principalTable: "enum.CustomField.HelperType",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField",
                table: "CRM.CustomField.HelperLink",
                columns: new[] { "BID", "CustomFieldID" },
                principalTable: "CRM.CustomField.Def",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper",
                table: "CRM.CustomField.HelperLink",
                columns: new[] { "BID", "HelperID" },
                principalTable: "CRM.CustomField.Helper",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Data_Business.Data",
                table: "Employee.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Locator_enum.LocatorType",
                table: "Employee.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Locator_Employee.Data",
                table: "Employee.Locator",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Team.LocationLink_Location.Data",
                table: "Employee.Team.LocationLink",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Team.LocationLink_Employee.Team",
                table: "Employee.Team.LocationLink",
                columns: new[] { "BID", "TeamID" },
                principalTable: "Employee.Team",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.TeamLink_Employee.Data",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.TeamLink_Employee.Team",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "TeamID" },
                principalTable: "Employee.Team",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_enum.CustomField.InputType_enum.CustomField.DataType",
                table: "enum.CustomField.InputType",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_enum.Locator.SubType_enum.Locator.Type",
                table: "enum.Locator.SubType",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_List.Filter.EmployeeSubscription_Employee.Data",
                table: "List.Filter.EmployeeSubscription",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_List.Filter.EmployeeSubscription_List.Filter",
                table: "List.Filter.EmployeeSubscription",
                columns: new[] { "BID", "FilterID" },
                principalTable: "List.Filter",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Location.Data_Business.Data",
                table: "Location.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Location.Locator_enum.LocatorType",
                table: "Location.Locator",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Location.Locator_Location.Data",
                table: "Location.Locator",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Opportunity.Data_Business.Data",
                table: "Opportunity.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Opportunity.Data_Opportunity.Custom",
                table: "Opportunity.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Opportunity.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Option.Data_System.Option.Definition",
                table: "Option.Data",
                column: "OptionID",
                principalTable: "System.Option.Definition",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Right.Group.Link_Security.Right.Group1",
                table: "Security.Right.Group.Link",
                columns: new[] { "BID", "ChildGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Right.Group.Link_Security.Right.Group",
                table: "Security.Right.Group.Link",
                columns: new[] { "BID", "ParentGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Right.Link_Security.Right.Collection",
                table: "Security.Right.Link",
                columns: new[] { "BID", "RightGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Role.Link_enum.User.Role.Type",
                table: "Security.Role.Link",
                column: "RoleType",
                principalTable: "enum.User.Role.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_Security.Role.Link_Security.Right.Group",
                table: "Security.Role.Link",
                columns: new[] { "BID", "RightGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_System.Option.Definition_System.Option.Category",
                table: "System.Option.Definition",
                column: "CategoryID",
                principalTable: "System.Option.Category",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_System.Option.Definition_enum.CustomField.DataType",
                table: "System.Option.Definition",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_User.Link_enum.User.Role.Type",
                table: "User.Link",
                column: "RoleType",
                principalTable: "enum.User.Role.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.SetNull);
        }
    }
}

