using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Remove_ItemIsSinglePart_OrderItemData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"
                DECLARE @ConstraintName nvarchar(200)
                SELECT @ConstraintName = Name
                FROM SYS.DEFAULT_CONSTRAINTS
                WHERE PARENT_OBJECT_ID = OBJECT_ID('[Order.Item.Data]')
                AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = N'ItemIsSinglePart' AND object_id = OBJECT_ID(N'[Order.Item.Data]'))
                
                IF @ConstraintName IS NOT NULL
                BEGIN
                    EXEC('ALTER TABLE [Order.Item.Data] DROP CONSTRAINT [' + @ConstraintName + ']')
                END
            ");

            migrationBuilder.DropColumn(
                name: "ItemIsSinglePart",
                table: "Order.Item.Data");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
