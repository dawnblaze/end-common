using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180613205838_UpdateEnumCustomFieldDataTypeTable")]
    public partial class UpdateEnumCustomFieldDataTypeTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [enum.CustomField.InputType] WHERE [DataType] IN (4,5);
DELETE FROM [enum.CustomField.DataType] WHERE ID IN (4,5);

INSERT [enum.CustomField.DataType] ([ID], [Name])
    VALUES
    (7, N'Date')
  , (8, N'Time')
  , (9, N'DateTime')
  , (21, N'GLAccount ID')
  , (22, N'Employee ID')
  , (23, N'Product ID')
  , (24, N'Material Part ID')
  , (25, N'Company ID')
  , (26, N'Contact ID')
  , (27, N'Labor Part ID')
  , (28, N'Machine Part ID')
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [enum.CustomField.DataType] WHERE ID IN (7, 8, 9, 21, 22, 23, 24, 25, 26, 27, 28);

INSERT [enum.CustomField.InputType] ([ID],[Name],[DataType])
    VALUES
      (41, 'List of TimeZones', 4)
    , (42, 'Lists of States', 4)
    , (43, 'Lists of Employees', 4)
    , (44, 'Lists of Salespeople', 4)
    , (49, 'Custom List', 4)
    , (59, 'Custom Set', 5)
;

INSERT [enum.CustomField.DataType] ([ID], [Name])
    VALUES
    (4, N'List')
  , (5, N'Set')
;
");
        }
    }
}

