﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9177InsertSectionsForCustomFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
drop table if exists #mytempLayoutDef;
set rowcount 0;
select NULL mykey, * into #mytempLayoutDef from dbo.[CustomField.Layout.Definition];
set rowcount 1;
update #mytempLayoutDef set mykey = 1;
while @@rowcount > 0
begin
    set rowcount 0;
    ---iterate on layouts | start
    declare @bid smallint,@layoutID smallint, @rootContainters int = 0;
    select @bid=tld.BID,@layoutID=tld.ID from #mytempLayoutDef tld where mykey = 1;
    select @rootContainters = count(*) from dbo.[CustomField.Layout.Container] lc where lc.BID=@bid and lc.LayoutID=@layoutID and lc.ParentID is null and lc.ElementType!=121;
    
    if @rootContainters > 0
    begin
        declare @containerCTID int = 15028, @nextID int = null;
        exec @nextID = [dbo].[Util.ID.GetID] @BID = @bid, @ClassTypeID = @containerCTID

        --insert section | start
        insert into dbo.[CustomField.Layout.Container] 
            (BID, ID, ModifiedDT, LayoutID, Name, Hint, ColumnNo, SortIndex, ElementType, ParentID)
            values (@bid, @nextID, GETUTCDATE(), @layoutID, 'Section', '', 0, 0, 121, null);--where section is 121

        update dbo.[CustomField.Layout.Container] set ParentID = @nextID
            where BID=@bid and LayoutID=@layoutID and ParentID is null and ElementType!=121 and ID!=@nextID;
        --insert section | end
    end
    ---iterate on layouts | end

    delete #mytempLayoutDef where mykey = 1;
    set rowcount 1;
    update #mytempLayoutDef set mykey = 1;
end
set rowcount 0;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
