using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8563UpdatePK_CustomFieldOtherData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP INDEX [XML_IX_CustomField.Other.Data] ON [dbo].[CustomField.Other.Data]
                ;

                ALTER TABLE [CustomField.Other.Data]
                    DROP CONSTRAINT [PK_CustomField.Other.Data]
                ;

                GO
                ;
                ALTER TABLE [CustomField.Other.Data]
                    ADD CONSTRAINT [PK_CustomField.Other.Data] PRIMARY KEY (BID, AppliesToClassTypeID, ID)
                ;

                CREATE PRIMARY XML INDEX [XML_IX_CustomField.Other.Data] ON [dbo].[CustomField.Other.Data] ([DataXML])
                ;
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
