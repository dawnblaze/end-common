using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Correct_Hours_Of_Operation_Options : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Hours of Operation', [Description] = 'Hours of operation' WHERE ID = 5;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Operations', [Description] = 'Operations' WHERE ID = 5;
");
        }
    }
}
