using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4046_Refactor_Payment_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_Payment.Data",
                table: "Accounting.GLData");

            migrationBuilder.DropTable(
                name: "Accounting.Payment.Data");

            migrationBuilder.CreateTable(
                name: "enum.Accounting.PaymentTransactionType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Accounting.PaymentTransactionType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Order.Tax.Item.Assessment",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((10004))"),
                    DestinationID = table.Column<int>(type: "int sparse", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    OrderID = table.Column<int>(nullable: false),
                    OrderItemID = table.Column<int>(nullable: true),
                    TaxAmount = table.Column<decimal>(type: "decimal(18, 4)", nullable: false),
                    TaxCodeID = table.Column<short>(nullable: false),
                    TaxItemID = table.Column<short>(nullable: true),
                    TaxRate = table.Column<decimal>(type: "decimal(7, 4)", nullable: false),
                    TaxableAmount = table.Column<decimal>(type: "decimal(18, 4)", nullable: false),
                    ValidToDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Tax.Item.Assessment", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Order.Tax.Item.Assessment_Order.Destination.Data",
                        columns: x => new { x.BID, x.DestinationID },
                        principalTable: "Order.Destination.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Tax.Item.Assessment_Order.Data",
                        columns: x => new { x.BID, x.OrderID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Tax.Item.Assessment_Order.Item.Data",
                        columns: x => new { x.BID, x.OrderItemID },
                        principalTable: "Order.Item.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Tax.Item.Assessment_Accounting.Tax.Code",
                        columns: x => new { x.BID, x.TaxCodeID },
                        principalTable: "Accounting.Tax.Code",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Tax.Item.Assessment_Accounting.Tax.Item",
                        columns: x => new { x.BID, x.TaxItemID },
                        principalTable: "Accounting.Tax.Item",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Payment.Master",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AccountingDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Amount = table.Column<decimal>(type: "decimal(18, 4)", nullable: false, defaultValue: 0m),
                    ChangeInNonRefCredit = table.Column<decimal>(type: "decimal(18,4) sparse", nullable: true),
                    ChangeInRefCredit = table.Column<decimal>(type: "decimal(18,4) sparse", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((8010))"),
                    CompanyID = table.Column<int>(nullable: false),
                    CurrencyType = table.Column<byte>(type: "tinyint sparse", nullable: true),
                    DisplayNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    GLActivityID = table.Column<int>(nullable: true),
                    LocationID = table.Column<byte>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    NonRefBalance = table.Column<decimal>(type: "decimal(18,4) sparse", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PaymentTransactionType = table.Column<byte>(nullable: false),
                    RefBalance = table.Column<decimal>(type: "decimal(18, 4)", nullable: false),
                    ValidToDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Payment.Master", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_enum.Accounting.CurrencyType",
                        column: x => x.CurrencyType,
                        principalTable: "enum.Accounting.CurrencyType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Master_enum.Accounting.PaymentTransactionType",
                        column: x => x.PaymentTransactionType,
                        principalTable: "enum.Accounting.PaymentTransactionType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Master_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Master_Activity.GLActivity",
                        columns: x => new { x.BID, x.GLActivityID },
                        principalTable: "Activity.GLActivity",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Master_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Payment.Application",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AccountingDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    AdjustedApplicationID = table.Column<int>(type: "int sparse", nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18, 4)", nullable: false, defaultValue: 0m),
                    ApplicationGroupID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((8011))"),
                    CompanyID = table.Column<int>(nullable: true),
                    CurrencyType = table.Column<byte>(type: "tinyint sparse", nullable: true),
                    DisplayNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    EnteredByContactID = table.Column<int>(type: "int sparse", nullable: true),
                    EnteredByEmployeeID = table.Column<short>(nullable: true),
                    GLActivityID = table.Column<int>(nullable: false),
                    HasAdjustments = table.Column<bool>(nullable: false, defaultValue: false),
                    LocationID = table.Column<byte>(nullable: false),
                    MasterID = table.Column<int>(nullable: false),
                    MerchangtAccountID = table.Column<byte>(type: "tinyint sparse", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    NonRefBalance = table.Column<decimal>(type: "decimal(18,4) sparse", nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    OrderID = table.Column<int>(nullable: true),
                    PaymentMethodId = table.Column<int>(nullable: true),
                    PaymentTransactionType = table.Column<byte>(nullable: false),
                    PaymentType = table.Column<byte>(nullable: false),
                    ReconciliationID = table.Column<int>(nullable: true),
                    RefBalance = table.Column<decimal>(type: "decimal(18, 4)", nullable: false),
                    TokenID = table.Column<string>(type: "varchar(64) sparse", maxLength: 64, nullable: true),
                    ValidToDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Payment.Application", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_enum.Accounting.CurrencyType",
                        column: x => x.CurrencyType,
                        principalTable: "enum.Accounting.CurrencyType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_enum.Accounting.PaymentTransactionType",
                        column: x => x.PaymentTransactionType,
                        principalTable: "enum.Accounting.PaymentTransactionType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_enum.Accounting.PaymentMethodType",
                        column: x => x.PaymentType,
                        principalTable: "enum.Accounting.PaymentMethodType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_Accounting.Payment.Application.AdjustedApplication",
                        columns: x => new { x.BID, x.AdjustedApplicationID },
                        principalTable: "Accounting.Payment.Application",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_Accounting.Payment.Application.ApplicationGroupID",
                        columns: x => new { x.BID, x.ApplicationGroupID },
                        principalTable: "Accounting.Payment.Application",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_Contact.Data",
                        columns: x => new { x.BID, x.EnteredByContactID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_Employee.Data",
                        columns: x => new { x.BID, x.EnteredByEmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_Activity.GLActivity_BID_GLActivityID",
                        columns: x => new { x.BID, x.GLActivityID },
                        principalTable: "Activity.GLActivity",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_Accounting.Payment.Application]",
                        columns: x => new { x.BID, x.MasterID },
                        principalTable: "Accounting.Payment.Master",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Order.Data",
                        columns: x => new { x.BID, x.OrderID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Application_Accounting.Payment.Method",
                        columns: x => new { x.BID, x.PaymentMethodId },
                        principalTable: "Accounting.Payment.Method",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_CurrencyType",
                table: "Accounting.Payment.Application",
                column: "CurrencyType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_PaymentTransactionType",
                table: "Accounting.Payment.Application",
                column: "PaymentTransactionType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_PaymentType",
                table: "Accounting.Payment.Application",
                column: "PaymentType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_BID_AdjustedApplicationID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "AdjustedApplicationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_BID_ApplicationGroupID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "ApplicationGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_BID_EnteredByContactID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "EnteredByContactID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_BID_EnteredByEmployeeID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "EnteredByEmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_BID_GLActivityID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "GLActivityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_BID_MasterID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "MasterID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_Order",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "OrderID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_BID_PaymentMethodId",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "PaymentMethodId" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_Date",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "AccountingDT", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_Company",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "CompanyID", "AccountingDT" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_Location",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "LocationID", "AccountingDT" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Master_CurrencyType",
                table: "Accounting.Payment.Master",
                column: "CurrencyType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Master_PaymentTransactionType",
                table: "Accounting.Payment.Master",
                column: "PaymentTransactionType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Master_BID_GLActivityID",
                table: "Accounting.Payment.Master",
                columns: new[] { "BID", "GLActivityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Master_Date",
                table: "Accounting.Payment.Master",
                columns: new[] { "BID", "AccountingDT", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Master_Company",
                table: "Accounting.Payment.Master",
                columns: new[] { "BID", "CompanyID", "AccountingDT" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Master_Location",
                table: "Accounting.Payment.Master",
                columns: new[] { "BID", "LocationID", "AccountingDT" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Tax.Item.Assessment_Dest",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "DestinationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Tax.Item.Assessment_Item",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "OrderItemID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Tax.Item.Assessment_BID_TaxCodeID",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "TaxCodeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Tax.Item.Assessment_BID_TaxItemID",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "TaxItemID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Tax.Item.Assessment_Order",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "OrderID", "OrderItemID", "TaxCodeID" });

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_Payment.Data",
                table: "Accounting.GLData",
                columns: new[] { "BID", "PaymentId" },
                principalTable: "Accounting.Payment.Master",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
                INSERT [enum.Accounting.PaymentTransactionType] ([ID], [Name]) 
                VALUES(1, N'Payment')
             , (2, N'Refund')
             , (3, N'Payment from Refundable Credit')
             , (4, N'Refund to Refundable Credit')
             , (5, N'Payment from Nonrefundable Credit')
             , (6, N'Refund to Nonrefundable Credit')
             , (7, N'Credit Gift')
             , (8, N'Credit Adjustment')
            ");

            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Tax.Item.Assessment]
                ADD PERIOD FOR SYSTEM_TIME([ModifiedDT], [ValidToDT])
                GO
                ALTER TABLE [Accounting.Payment.Master]
                ADD PERIOD FOR SYSTEM_TIME([ModifiedDT], [ValidToDT])
                GO
                ALTER TABLE [Accounting.Payment.Application]
                ADD PERIOD FOR SYSTEM_TIME([ModifiedDT], [ValidToDT])
                GO
                ");

            migrationBuilder.EnableSystemVersioning("Order.Tax.Item.Assessment");
            migrationBuilder.EnableSystemVersioning("Accounting.Payment.Master");
            migrationBuilder.EnableSystemVersioning("Accounting.Payment.Application");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_Payment.Data",
                table: "Accounting.GLData");

            migrationBuilder.DropTable(
                name: "Accounting.Payment.Application");

            migrationBuilder.DropTable(
                name: "Order.Tax.Item.Assessment");

            migrationBuilder.DropTable(
                name: "Accounting.Payment.Master");

            migrationBuilder.DropTable(
                name: "enum.Accounting.PaymentTransactionType");

            migrationBuilder.CreateTable(
                name: "Accounting.Payment.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AccountingDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    ActivityID = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18, 4)", nullable: false, defaultValue: 0m),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((8010))"),
                    CompanyID = table.Column<int>(nullable: true),
                    CurrencyType = table.Column<byte>(nullable: false),
                    DisplayNumber = table.Column<string>(maxLength: 50, nullable: true),
                    EnteredByContactID = table.Column<int>(nullable: true),
                    EnteredByEmployeeID = table.Column<short>(nullable: true),
                    IsSubPayment = table.Column<bool>(nullable: true, computedColumnSql: "(case when [ParentID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)"),
                    IsVoided = table.Column<bool>(nullable: true, computedColumnSql: "(case when [VoidedPaymentID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsVoidedEntry = table.Column<bool>(nullable: true, computedColumnSql: "(case when [VoidedDT] IS NOT NULL AND [VoidedPaymentID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)"),
                    IsVoidingEntry = table.Column<bool>(nullable: true, computedColumnSql: "(case when [VoidedDT] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    LocationID = table.Column<byte>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Notes = table.Column<string>(maxLength: 100, nullable: true),
                    OrderID = table.Column<int>(nullable: true),
                    ParentID = table.Column<int>(nullable: false),
                    PaymentMethodId = table.Column<int>(nullable: true),
                    PaymentType = table.Column<byte>(nullable: false),
                    TokenID = table.Column<string>(type: "nvarchar(100)", maxLength: 64, nullable: true),
                    ValidToDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))"),
                    VoidedDT = table.Column<DateTime>(type: "datetime2(2) SPARSE", nullable: true),
                    VoidedPaymentID = table.Column<int>(type: "int SPARSE", nullable: true),
                    VoidedReason = table.Column<string>(type: "nvarchar(255) SPARSE", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Payment.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_enum.Accounting.CurrencyType",
                        column: x => x.CurrencyType,
                        principalTable: "enum.Accounting.CurrencyType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_enum.Accounting.PaymentMethodType",
                        column: x => x.PaymentType,
                        principalTable: "enum.Accounting.PaymentMethodType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Activity.GLActivity",
                        columns: x => new { x.BID, x.ActivityID },
                        principalTable: "Activity.GLActivity",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Contact.Data",
                        columns: x => new { x.BID, x.EnteredByContactID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Employee.Data",
                        columns: x => new { x.BID, x.EnteredByEmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Order.Data",
                        columns: x => new { x.BID, x.OrderID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Accounting.Payment.Data",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Accounting.Payment.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Accounting.Payment.Method",
                        columns: x => new { x.BID, x.PaymentMethodId },
                        principalTable: "Accounting.Payment.Method",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Accounting.Payment.Data1",
                        columns: x => new { x.BID, x.VoidedPaymentID },
                        principalTable: "Accounting.Payment.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_CurrencyType",
                table: "Accounting.Payment.Data",
                column: "CurrencyType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_PaymentType",
                table: "Accounting.Payment.Data",
                column: "PaymentType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_ActivityID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "ActivityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_CompanyID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "CompanyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_EnteredByContactID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "EnteredByContactID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_EnteredByEmployeeID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "EnteredByEmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_LocationID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_OrderID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "OrderID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_ParentID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_PaymentMethodId",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "PaymentMethodId" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_VoidedPaymentID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "VoidedPaymentID" });

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_Payment.Data",
                table: "Accounting.GLData",
                columns: new[] { "BID", "PaymentId" },
                principalTable: "Accounting.Payment.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
