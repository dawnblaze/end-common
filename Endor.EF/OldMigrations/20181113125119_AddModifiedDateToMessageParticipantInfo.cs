using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddModifiedDateToMessageParticipantInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDT",
                table: "Message.Participant.Info",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "GetUTCDate()");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModifiedDT",
                table: "Message.Participant.Info");
        }
    }
}
