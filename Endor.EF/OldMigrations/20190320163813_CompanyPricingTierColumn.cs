using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CompanyPricingTierColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "PricingTierID",
                table: "Company.Data",
                type: "smallint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_BID_PricingTierID",
                table: "Company.Data",
                columns: new[] { "BID", "PricingTierID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_PricingTier",
                table: "Company.Data",
                columns: new[] { "BID", "PricingTierID" },
                principalTable: "List.FlatList.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_PricingTier",
                table: "Company.Data");

            migrationBuilder.DropIndex(
                name: "IX_Company.Data_BID_PricingTierID",
                table: "Company.Data");

            migrationBuilder.DropColumn(
                name: "PricingTierID",
                table: "Company.Data");
        }
    }
}
