using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180403161236_PartMachineCategoryActionSPROCs")]
    public partial class PartMachineCategoryActionSPROCs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                /****** Object:  StoredProcedure [dbo].[Part.Machine.Category.Action.CanDelete]    Script Date: 4/3/2018 7:41:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
========================================================
    Name: [Part.Machine.Category.Action.CanDelete]

    Description: This procedure checks if the MachineCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Machine.Category.Action.CanDelete] @BID=1, @ID=1, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Machine.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the MachineCategory can be deleted. The boolean response is returned in the body.  A MachineCategory can be deleted if
	- there are no machines linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Machine.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Machine Category Specified Does not Exist. MachineCategoryID=', @ID)

    -- there are no machines linked to it or if the force option is specified to deletes the links.
	ELSE IF  EXISTS( SELECT * FROM [Part.Machine.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Machine Category is linked to a Machine Data. MachineCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
/****** Object:  StoredProcedure [dbo].[Part.Machine.Category.Action.SetActive]    Script Date: 4/3/2018 7:53:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Part.Machine.Category.Action.SetActive]
--
-- Description: This procedure sets the MachineCategory to active or inactive
--
-- Sample Use:   EXEC dbo.[Part.Machine.Category.Action.SetActive] @BID=1, @MachineCategoryID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Machine.Category.Action.SetActive]
-- DECLARE 
          @BID                  TINYINT  -- = 1
        , @MachineCategoryID    SMALLINT -- = 2

        , @IsActive             BIT     = 1

        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the MachineCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Machine.Category] WHERE BID = @BID and ID = @MachineCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MachineCategory Specified. MachineCategoryID='+CONVERT(VARCHAR(12),@MachineCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Part.Machine.Category] L
    WHERE BID = @BID and ID = @MachineCategoryID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
GO
/****** Object:  StoredProcedure [dbo].[Part.Machine.Category.Action.LinkMachine]    Script Date: 4/3/2018 7:59:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Part.Machine.Category.Action.LinkMachine]
--
-- Description: This procedure links/unlinks the MachineData to the MachineCategory
--
-- Sample Use:   EXEC dbo.[Part.Machine.Category.Action.LinkMachine] @BID=1, @MachineCategoryID=1, @MachineDataID=1, @IsLinked=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Machine.Category.Action.LinkMachine]
--DECLARE 
          @BID                  TINYINT  --= 1
        , @MachineCategoryID    SMALLINT --= 2
		, @MachineDataID        SMALLINT --= 1
        , @IsLinked             BIT     = 1
        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the MachineCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Machine.Category] WHERE BID = @BID and ID = @MachineCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MachineCategory Specified. MachineCategoryID='+CONVERT(VARCHAR(12),@MachineCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the MachineData specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Machine.Data] WHERE BID = @BID and ID = @MachineDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MachineData Specified. MachineDataID='+CONVERT(VARCHAR(12),@MachineDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.MachineDataLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Part.Machine.CategoryLink] WHERE BID = @BID and CategoryID = @MachineCategoryID and PartID = @MachineDataID)
		BEGIN
			INSERT INTO [Part.Machine.CategoryLink] (BID, CategoryID, PartID)
			VALUES (@BID, @MachineCategoryID, @MachineDataID)
		END;

	END
	ELSE
	BEGIN
		-- Remove entry from Part.Machine.CategoryLink
		DELETE FROM [Part.Machine.CategoryLink] WHERE BID = @BID and CategoryID = @MachineCategoryID and PartID = @MachineDataID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Machine.Category.Action.LinkMachine]");
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Machine.Category.Action.SetActive]");
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Machine.Category.Action.CanDelete]");
        }
    }
}

