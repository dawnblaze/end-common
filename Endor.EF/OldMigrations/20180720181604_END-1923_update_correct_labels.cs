using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180720181604_END-1923_update_correct_labels")]
    public partial class END1923_update_correct_labels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE dbo.[System.Option.Definition]
                SET [Label]='Don''t allow new orders to be created.'
                WHERE [Name]='Order.Creation.CreditLimit.Exceeded.Deny' AND CategoryID=603;

                UPDATE dbo.[System.Option.Definition]
                SET [Label]='Don''t allow existing orders to be marked Invoiced.'
                WHERE [Name]='Order.Invoiced.CreditLimit.Exceeded.Deny' AND CategoryID=603;

                UPDATE dbo.[System.Option.Definition]
                SET [Label]='Don''t allow new orders to be saved.'
                WHERE [Name]='Order.Creation.CreditLimit.WillExceed.Deny' AND CategoryID=603;

                UPDATE dbo.[System.Option.Definition]
                SET [Label]='Don''t allow new orders to be created.'
                WHERE [Name]='Order.Creation.CreditAccount.InDefault.Deny' AND CategoryID=603;

                UPDATE dbo.[System.Option.Definition]
                SET [Label]='Don''t allow existing orders to be marked Invoiced.'
                WHERE [Name]='Order.Invoiced.CreditAccountt.InDefault.Deny' AND CategoryID=603;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

