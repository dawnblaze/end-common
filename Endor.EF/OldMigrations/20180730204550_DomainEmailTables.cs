using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180730204550_DomainEmailTables")]
    public partial class DomainEmailTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            DROP TABLE IF EXISTS[enum.Email.ProviderType];
            DROP TABLE IF EXISTS[enum.Email.SMTP.ProviderType];
            DROP TABLE IF EXISTS[enum.Email.SecurityType];
            DROP TABLE IF EXISTS[enum.Email.SMTP.SecurityType];
            DROP TABLE IF EXISTS[System.Email.SMTP.ConfigurationType];
            DROP TABLE IF EXISTS[Domain.Email.Data];
            DROP TABLE IF EXISTS[Domain.Email.LocationLink];");

            migrationBuilder.CreateTable(
                name: "enum.Email.SMTP.ProviderType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Email.SMTP.ProviderType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Email.SMTP.SecurityType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Email.SMTP.SecurityType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "System.Email.SMTP.ConfigurationType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    SMTPAddress = table.Column<string>(maxLength: 255, nullable: true),
                    SMTPAuthenticateFirst = table.Column<bool>(nullable: true),
                    SMTPPort = table.Column<short>(type: "smallint", nullable: true),
                    SMTPSecurityType = table.Column<byte>(type: "tinyint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Email.SMTP.ConfigurationType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Domain.Email.Data",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    ID = table.Column<short>(type: "smallint", nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false),
                    DomainName = table.Column<string>(maxLength: 255, nullable: false),
                    EnumEmailProviderTypeID = table.Column<byte>(nullable: true),
                    EnumEmailSecurityTypeID = table.Column<byte>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    LastVerificationAttemptDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    LastVerificationResult = table.Column<string>(maxLength: 255, nullable: true),
                    LastVerificationSuccess = table.Column<bool>(nullable: true),
                    LastVerifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    ProviderType = table.Column<byte>(type: "tinyint", nullable: false),
                    SMTPAddress = table.Column<string>(maxLength: 255, nullable: true),
                    SMTPAuthenticateFirst = table.Column<bool>(nullable: true),
                    SMTPConfigurationType = table.Column<byte>(type: "tinyint", nullable: true),
                    SMTPPort = table.Column<short>(type: "smallint", nullable: true),
                    SMTPSecurityType = table.Column<byte>(type: "tinyint", nullable: true),
                    SystemEmailSMTPConfigurationTypeID = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domain.Email.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_enum.Email.SMTP.ProviderType_EnumEmailProviderTypeID",
                        column: x => x.EnumEmailProviderTypeID,
                        principalTable: "enum.Email.SMTP.ProviderType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_enum.Email.SMTP.SecurityType_EnumEmailSecurityTypeID",
                        column: x => x.EnumEmailSecurityTypeID,
                        principalTable: "enum.Email.SMTP.SecurityType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_enum.Email.SMTP.ProviderType",
                        column: x => x.ProviderType,
                        principalTable: "enum.Email.SMTP.ProviderType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_System.Email.SMTP.ConfigurationType",
                        column: x => x.SMTPConfigurationType,
                        principalTable: "System.Email.SMTP.ConfigurationType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_enum.Email.SMTP.SecurityType",
                        column: x => x.SMTPSecurityType,
                        principalTable: "enum.Email.SMTP.SecurityType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.Data_System.Email.SMTP.ConfigurationType_SystemEmailSMTPConfigurationTypeID",
                        column: x => x.SystemEmailSMTPConfigurationTypeID,
                        principalTable: "System.Email.SMTP.ConfigurationType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Domain.Email.LocationLink",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    DomainID = table.Column<short>(type: "smallint", nullable: false),
                    LocationID = table.Column<byte>(type: "tinyint", nullable: false),
                    DomainEmailBID = table.Column<short>(nullable: true),
                    DomainEmailID = table.Column<short>(nullable: true),
                    LocationDataBID = table.Column<short>(nullable: true),
                    LocationDataID = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domain.Email.LocationLink", x => new { x.BID, x.DomainID, x.LocationID });
                    table.ForeignKey(
                        name: "FK_Domain.Email.LocationLink_Domain.Email.Data",
                        columns: x => new { x.BID, x.DomainID },
                        principalTable: "Domain.Email.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.LocationLink_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.LocationLink_Domain.Email.Data_DomainEmailBID_DomainEmailID",
                        columns: x => new { x.DomainEmailBID, x.DomainEmailID },
                        principalTable: "Domain.Email.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Email.LocationLink_Location.Data_LocationDataBID_LocationDataID",
                        columns: x => new { x.LocationDataBID, x.LocationDataID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_EnumEmailProviderTypeID",
                table: "Domain.Email.Data",
                column: "EnumEmailProviderTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_EnumEmailSecurityTypeID",
                table: "Domain.Email.Data",
                column: "EnumEmailSecurityTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_ProviderType",
                table: "Domain.Email.Data",
                column: "ProviderType");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_SMTPConfigurationType",
                table: "Domain.Email.Data",
                column: "SMTPConfigurationType");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_SMTPSecurityType",
                table: "Domain.Email.Data",
                column: "SMTPSecurityType");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_SystemEmailSMTPConfigurationTypeID",
                table: "Domain.Email.Data",
                column: "SystemEmailSMTPConfigurationTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.Data_Domain",
                table: "Domain.Email.Data",
                columns: new[] { "BID", "DomainName", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.LocationLink_BID_LocationID",
                table: "Domain.Email.LocationLink",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.LocationLink_DomainEmailBID_DomainEmailID",
                table: "Domain.Email.LocationLink",
                columns: new[] { "DomainEmailBID", "DomainEmailID" });

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Email.LocationLink_LocationDataBID_LocationDataID",
                table: "Domain.Email.LocationLink",
                columns: new[] { "LocationDataBID", "LocationDataID" });

            migrationBuilder.Sql(@"
INSERT [enum.Email.SMTP.SecurityType] ([ID], [Name]) VALUES (0, N'None (Not Secure)')
, (1, N'SSL')
, (2, N'TLS');");
            migrationBuilder.Sql(@"
INSERT [enum.Email.SMTP.ProviderType] ([ID], [Name]) VALUES (0, N'None')
 , (1, N'Google G Suite')
 , (2, N'Microsoft 365')
 , (3, N'Custom SMTP');");
            migrationBuilder.Sql(@"
INSERT INTO [System.Email.SMTP.ConfigurationType] ([ID], [Name], [SMTPAddress], [SMTPPort], [SMTPSecurityType], [SMTPAuthenticateFirst])
VALUES (0, N'None', NULL, NULL, NULL, NULL)
 , (1, N'Yahoo', N'smtp.mail.yahoo.com', 587, 1, 1)
 , (2, N'AOL', N'smtp.aol.com', 587, 2, 1)
 , (3, N'iCloud', N'smtp.mail.me.com', 587, 1, 1);");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Domain.Email.LocationLink");

            migrationBuilder.DropTable(
                name: "Domain.Email.Data");

            migrationBuilder.DropTable(
                name: "enum.Email.SMTP.ProviderType");

            migrationBuilder.DropTable(
                name: "enum.Email.SMTP.SecurityType");

            migrationBuilder.DropTable(
                name: "System.Email.SMTP.ConfigurationType");
        }
    }
}

