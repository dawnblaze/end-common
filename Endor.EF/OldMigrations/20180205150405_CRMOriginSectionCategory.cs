using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180205150405_CRMOriginSectionCategory")]
    public partial class CRMOriginSectionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [System.Option.Section] ([ID],[Name],[ParentID],[IsHidden],[Depth],[SearchTerms])
VALUES (10,'CRM',NULL,0,0,NULL);

INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden, SearchTerms)
VALUES (7,'Origin',10,'CRM Origins Options',4,0,NULL);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.option.Category] WHERE ID = 7;
DELETE FROM [System.Option.Section] WHERE ID = 10;
");
        }
    }
}

