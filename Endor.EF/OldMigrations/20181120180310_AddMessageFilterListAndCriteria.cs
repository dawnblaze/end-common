using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddMessageFilterListAndCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO[dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID]
                       ,[Name]
                       ,[Label]
                       ,[Field]
                       ,[IsHidden]
                       ,[DataType]
                       ,[InputType]
                       ,[AllowMultiple]
                       ,[ListValues]
                       ,[ListValuesEndpoint]
                       ,[IsLimitToList]
                       ,[SortIndex]
                       ,[ID])
                   VALUES
                       (14109 --<TargetClassTypeID, int,>
                        ,'IsRead' --<Name, varchar(255),>
                        ,'Show Read Messages'  --<Label, varchar(255),>
                        ,'IsRead' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,2 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,'Is Not Read,Is Read' --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,0 --<SortIndex, tinyint,>
                        ,(SELECT (MAX(ID)+1) FROM[dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );

                INSERT INTO[dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID]
                       ,[Name]
                       ,[Label]
                       ,[Field]
                       ,[IsHidden]
                       ,[DataType]
                       ,[InputType]
                       ,[AllowMultiple]
                       ,[ListValues]
                       ,[ListValuesEndpoint]
                       ,[IsLimitToList]
                       ,[SortIndex]
                       ,[ID])
                   VALUES
                        (14109 --<TargetClassTypeID, int,>
                        ,'IsSent' --<Name, varchar(255),>
                        ,'Is Sent'  --<Label, varchar(255),>
                        ,'IsSentFolder' --<Field, varchar(255),>
                        ,1 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,2 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,'Is Not Sent,Is Sent' --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,1 --<SortIndex, tinyint,>
                        ,(SELECT (MAX(ID)+1) FROM[dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );

                INSERT INTO[dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID]
                       ,[Name]
                       ,[Label]
                       ,[Field]
                       ,[IsHidden]
                       ,[DataType]
                       ,[InputType]
                       ,[AllowMultiple]
                       ,[ListValues]
                       ,[ListValuesEndpoint]
                       ,[IsLimitToList]
                       ,[SortIndex]
                       ,[ID])
                   VALUES
                        (14109 --<TargetClassTypeID, int,>
                        ,'IsDeleted' --<Name, varchar(255),>
                        ,'Is Deleted'  --<Label, varchar(255),>
                        ,'IsDeleted' --<Field, varchar(255),>
                        ,1 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,2 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,'Is Not Deleted,Is Deleted' --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,2 --<SortIndex, tinyint,>
                        ,(SELECT (MAX(ID)+1) FROM[dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");

            migrationBuilder.Sql(@"
                    INSERT INTO [dbo].[List.Filter]
                        ([BID]
                        ,[ID]
                        ,[CreatedDate]
                        ,[ModifiedDT]
                        ,[IsActive]
                        ,[Name]
                        ,[TargetClassTypeID]
                        ,[IDs]
                        ,[Criteria]
                        ,[OwnerID]
                        ,[IsPublic]
                        ,[IsSystem]
                        ,[Hint]
                        ,[IsDefault]
                        ,[SortIndex])
                    VALUES
                        (1--<BID, smallint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]) --<ID, int,>
                        ,GETUTCDATE()
                        ,GETUTCDATE()
                        ,1--<IsActive, bit,>
                        ,'IsRead'--<Name, varchar(255),>
                        ,14109--<TargetClassTypeID, int,>
                        ,NULL--<IDs, xml,>
                        ,'<ArrayOfListFilterItem>
                            <ListFilterItem>
                                <Label>Show Read</Label>
                                <SearchValue>False</SearchValue>
                                <Field>IsRead</Field>
                                <DisplayText>Show Read</DisplayText>
                                <IsHidden>false</IsHidden>
                            </ListFilterItem>
                        </ArrayOfListFilterItem>'
                        ,NULL--<OwnerID, smallint,>
                        ,0--<IsPublic, bit,>
                        ,1--<IsSystem, bit,>
                        ,NULL--<Hint, varchar(max),>
                        ,1--<IsDefault, bit,>
                        ,0--<SortIndex, tinyint,>
                        );

                INSERT INTO [dbo].[List.Filter]
                        ([BID]
                        ,[ID]
                        ,[CreatedDate]
                        ,[ModifiedDT]
                        ,[IsActive]
                        ,[Name]
                        ,[TargetClassTypeID]
                        ,[IDs]
                        ,[Criteria]
                        ,[OwnerID]
                        ,[IsPublic]
                        ,[IsSystem]
                        ,[Hint]
                        ,[IsDefault]
                        ,[SortIndex])
                    VALUES
                        (1--<BID, smallint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]) --<ID, int,>
                        ,GETUTCDATE()
                        ,GETUTCDATE()
                        ,1--<IsActive, bit,>
                        ,'IsSent'--<Name, varchar(255),>
                        ,14109--<TargetClassTypeID, int,>
                        ,NULL--<IDs, xml,>
                        ,'<ArrayOfListFilterItem>
                            <ListFilterItem>
                                <Label>Show Sent</Label>
                                <SearchValue>True</SearchValue>
                                <Field>IsSentFolder</Field>
                                <DisplayText>Show Sent</DisplayText>
                                <IsHidden>true</IsHidden>
                            </ListFilterItem>
                        </ArrayOfListFilterItem>'
                        ,NULL--<OwnerID, smallint,>
                        ,0--<IsPublic, bit,>
                        ,1--<IsSystem, bit,>
                        ,NULL--<Hint, varchar(max),>
                        ,1--<IsDefault, bit,>
                        ,1--<SortIndex, tinyint,>
                        );

                INSERT INTO [dbo].[List.Filter]
                        ([BID]
                        ,[ID]
                        ,[CreatedDate]
                        ,[ModifiedDT]
                        ,[IsActive]
                        ,[Name]
                        ,[TargetClassTypeID]
                        ,[IDs]
                        ,[Criteria]
                        ,[OwnerID]
                        ,[IsPublic]
                        ,[IsSystem]
                        ,[Hint]
                        ,[IsDefault]
                        ,[SortIndex])
                    VALUES
                        (1--<BID, smallint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]) --<ID, int,>
                        ,GETUTCDATE()
                        ,GETUTCDATE()
                        ,1--<IsActive, bit,>
                        ,'IsDeleted'--<Name, varchar(255),>
                        ,14109--<TargetClassTypeID, int,>
                        ,NULL--<IDs, xml,>
                        ,'<ArrayOfListFilterItem>
                            <ListFilterItem>
                                <Label>Show Deleted</Label>
                                <SearchValue>True</SearchValue>
                                <Field>IsDeleted</Field>
                                <DisplayText>Show Deleted</DisplayText>
                                <IsHidden>true</IsHidden>
                            </ListFilterItem>
                        </ArrayOfListFilterItem>'
                        ,NULL--<OwnerID, smallint,>
                        ,0--<IsPublic, bit,>
                        ,1--<IsSystem, bit,>
                        ,NULL--<Hint, varchar(max),>
                        ,1--<IsDefault, bit,>
                        ,2--<SortIndex, tinyint,>
                        );
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=14109;
            ");

            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE [TargetClassTypeID]=14109;
            ");
        }
    }
}
