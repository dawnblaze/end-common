using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180208200807_ChartOfAccountListFilters")]
    public partial class ChartOfAccountListFilters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[List.Filter]
                ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1012,'2018-2-08','2018-2-08',1,'Assets',8000,null,'<ArrayOfListFilterItem>
	                <ListFilterItem>
		                <SearchValue>1</SearchValue>
		                <Field>IsAsset</Field>
		                <IsHidden>true</IsHidden>
		                <IsSystem>true</IsSystem>
	                </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,0)
                GO

                INSERT INTO [dbo].[List.Filter]
                ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1013,'2018-2-08','2018-2-08',1,'Liabilities',8000,null,'<ArrayOfListFilterItem>
	                <ListFilterItem>
		                <SearchValue>1</SearchValue>
		                <Field>IsLiability</Field>
		                <IsHidden>true</IsHidden>
		                <IsSystem>true</IsSystem>
	                </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,1)
                GO

                INSERT INTO [dbo].[List.Filter]
                ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1014,'2018-2-08','2018-2-08',1,'Income',8000,null,'<ArrayOfListFilterItem>
	                <ListFilterItem>
		                <SearchValue>1</SearchValue>
		                <Field>IsIncome</Field>
		                <IsHidden>true</IsHidden>
		                <IsSystem>true</IsSystem>
	                </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,2)
                GO

                INSERT INTO [dbo].[List.Filter]
                ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1015,'2018-2-08','2018-2-08',1,'COGS',8000,null,'<ArrayOfListFilterItem>
	                <ListFilterItem>
		                <SearchValue>1</SearchValue>
		                <Field>IsCOGS</Field>
		                <IsHidden>true</IsHidden>
		                <IsSystem>true</IsSystem>
	                </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,3)
                GO

                INSERT INTO [dbo].[List.Filter]
                ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1016,'2018-2-08','2018-2-08',1,'Expenses',8000,null,'<ArrayOfListFilterItem>
	                <ListFilterItem>
		                <SearchValue>1</SearchValue>
		                <Field>IsExpense</Field>
		                <IsHidden>true</IsHidden>
		                <IsSystem>true</IsSystem>
	                </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,4)
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID] = 8000 AND IsSystem = 1
            ");
        }
    }
}

