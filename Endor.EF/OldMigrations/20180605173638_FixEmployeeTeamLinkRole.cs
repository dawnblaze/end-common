using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180605173638_FixEmployeeTeamLinkRole")]
    public partial class FixEmployeeTeamLinkRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Employee.TeamLink",
                table: "Employee.TeamLink");

            migrationBuilder.DropIndex(
                name: "IX_Employee.TeamLink_Employee",
                table: "Employee.TeamLink");

            //migrationBuilder.DropColumn(
            //    name: "RoleType",
            //    table: "Employee.TeamLink");

            migrationBuilder.AddColumn<short>(
                name: "RoleID",
                table: "Employee.TeamLink",
                nullable: false,
                defaultValue: (short)1);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Employee.TeamLink",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "EmployeeID", "TeamID", "RoleID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TeamLink_Employee",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "EmployeeID", "TeamID", "RoleID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.TeamLink_Employee.Role",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "RoleID" },
                principalTable: "Employee.Role",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee.TeamLink_Employee.Role",
                table: "Employee.TeamLink");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Employee.TeamLink",
                table: "Employee.TeamLink");

            migrationBuilder.DropIndex(
                name: "IX_Employee.TeamLink_BID_RoleID",
                table: "Employee.TeamLink");

            migrationBuilder.DropIndex(
                name: "IX_Employee.TeamLink_Employee",
                table: "Employee.TeamLink");

            migrationBuilder.DropColumn(
                name: "RoleID",
                table: "Employee.TeamLink");

            //migrationBuilder.AddColumn<byte>(
            //    name: "RoleType",
            //    table: "Employee.TeamLink",
            //    nullable: false,
            //    defaultValueSql: "((0))");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Employee.TeamLink",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "EmployeeID", "TeamID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TeamLink_Employee",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "EmployeeID", "TeamID" });
        }
    }
}

