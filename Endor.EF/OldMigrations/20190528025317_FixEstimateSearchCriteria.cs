using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixEstimateSearchCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.List.Filter.Criteria]
                SET Field = 'Order',
                DataType = 1,
                InputType = 0
                WHERE TargetClassTypeID = 10200 AND Name = 'Estimate #'
            GO

            UPDATE [System.List.Filter.Criteria]
                SET Field = 'StatusID',
                DataType = 32005,
                InputType = 14
                WHERE TargetClassTypeID = 10200 AND Name = 'Estimate Status'
            GO

            UPDATE [System.List.Filter.Criteria]
                SET Field = 'EmployeeRolesIDs',
                DataType = 32005,
                InputType = 9
                WHERE TargetClassTypeID = 10200 AND Name = 'Employees'
            GO

            UPDATE [System.List.Filter.Criteria]
                SET Field = 'CompanyName',
                DataType = 1,
                InputType = 0
                WHERE TargetClassTypeID = 10200 AND Name = 'Company'
            GO

            UPDATE [System.List.Filter.Criteria]
                SET Field = 'ContactRolesNames',
                DataType = 1,
                InputType = 0
                WHERE TargetClassTypeID = 10200 AND Name = 'Contact'
            GO

            UPDATE [System.List.Filter.Criteria]
                SET Name = 'Show Voided Estimates',
                Label = 'Show Voided Estimates',
                Field = 'IsVoided',
                DataType = 3,
                InputType = 11
                WHERE TargetClassTypeID = 10200 AND Name = 'Include Closed Estimates'
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
