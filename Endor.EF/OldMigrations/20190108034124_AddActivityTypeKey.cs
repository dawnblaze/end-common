using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddActivityTypeKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE[dbo].[Activity.GLActivity]
                ADD CONSTRAINT[FK_Activity.GLActivity_Enum_Activity]
                FOREIGN KEY([ActivityType]) REFERENCES[enum.ActivityType] ([ID])
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE[dbo].[Activity.GLActivity]
                DROP CONSTRAINT[FK_Activity.GLActivity_Enum_Activity]
            ");
        }
    }
}
