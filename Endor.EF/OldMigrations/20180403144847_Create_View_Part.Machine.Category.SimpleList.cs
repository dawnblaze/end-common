using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180403144847_Create_View_Part.Machine.Category.SimpleList")]
    public partial class Create_View_PartMachineCategorySimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Machine.Category.SimpleList]
                GO
                CREATE VIEW [dbo].[Part.Machine.Category.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                    , [IsActive]
                    , CONVERT(BIT, 0) AS[HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[Part.Machine.Category];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Machine.Category.SimpleList]
            ");
        }
    }
}

