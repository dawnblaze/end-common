using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180622144653_AddNewCompanyStatuses")]
    public partial class AddNewCompanyStatuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (9, 'Lead and Vendor')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (10, 'Prospect and Vendor')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (12, 'Customer and Vendor')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (17, 'Lead and Personal')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (18, 'Prospect and Personal')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (20, 'Customer and Personal')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (24, 'Vendor and Personal')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (25, 'Lead and Vendor and Personal')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (26, 'Prospect and Vendor and Personal')
INSERT INTO [dbo].[enum.CRM.Company.Status] VALUES (28, 'Customer and Vendor and Personal')
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[enum.CRM.Company.Status] WHERE ID IN (9,10,12,17,18,20,24,25,26,28);
");
        }
    }
}

