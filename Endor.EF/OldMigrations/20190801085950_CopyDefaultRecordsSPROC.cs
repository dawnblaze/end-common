using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CopyDefaultRecordsSPROC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
-- ========================================================

    Name: Util.Table.DependencyDepth( )

    Description: This Function returns the list of tables and their dependency depths.
                    History Tables are excluded.

    Sample Use:   

        SELECT * FROM [dbo].[Util.Table.DependencyDepth]()
        ORDER BY MaxDepth DESC, TableName
        
-- ========================================================
*/
CREATE OR ALTER FUNCTION [dbo].[Util.Table.DependencyDepth]( )
RETURNS 
    @Result TABLE (ID int, TableName sysname, MaxDepth TINYINT)
AS
BEGIN        
    DECLARE @Child TABLE ([Key] INT IDENTITY PRIMARY KEY, ID int, TableName sysname, ParentID int, ParentTableName sysname, Depth tinyint);

    -- We only need to worry about tables with BID in them
    INSERT INTO @Result (ID, TableName)
        SELECT T.TableID, T.TableName
        FROM TablesAndColumns T
        WHERE T.ColumnName = 'BID'
        AND T.IsHistoryTable = 0
        ;

    INSERT INTO @Child (ID, TableName, ParentID, ParentTableName)
        SELECT DISTINCT referenced_object_id, OBJECT_NAME(referenced_object_id), P.ID, P.TableName
        FROM sys.foreign_key_columns
        JOIN @Result P on P.ID = parent_object_id
        WHERE parent_object_id != referenced_object_id -- remove self-referencing for our purposes
            AND constraint_column_id = 1
            AND parent_object_id IN (SELECT ID FROM @Result)
        ;

    UPDATE C
    SET Depth = 1
    FROM @Child C
    WHERE NOT EXISTS( SELECT * FROM @Child C2 WHERE C2.TableName = C.ParentTableName )
    ;

    DECLARE @Depth TINYINT = 1;

    WHILE (@Depth < 9)
    BEGIN
        UPDATE C
        SET Depth = @Depth + 1
        FROM @Child C
        WHERE DEPTH IS NULL
        AND EXISTS( SELECT * FROM @Child C2 WHERE C2.TableName = C.ParentTableName AND Depth = @Depth)
        AND NOT EXISTS( SELECT * FROM @Child C2 WHERE C2.TableName = C.ParentTableName AND Depth IS NULL )
        ;

        SET @Depth += 1;
    END;

    -- Note .. This won't catch all of them because of recursive references  
    -- These include 
    --              Location.Data, Employee.Data, Domain.Email.Data, 
    --              Email.Account.Data, Business.Data
    UPDATE C
    SET Depth = CASE C.ParentTableName 
                WHEN 'Domain.Email.Data' THEN 9
                WHEN 'Email.Account.Data' THEN 9
                WHEN 'Location.Data' THEN 10
                WHEN 'Employee.Data' THEN 10
                WHEN 'Business.Data' THEN 12
                ELSE 10 END
    FROM @Child C
    WHERE Depth IS NULL 
    AND C.ParentTableName in 
        ('Location.Data', 'Employee.Data', 'Domain.Email.Data', 'Email.Account.Data', 'Business.Data')
    ;

    -- Now Update the Table List
    UPDATE P
    SET MaxDepth = COALESCE((SELECT Max(Depth) FROM @Child C WHERE C.ParentID = P.ID), 0)
    FROM @Result P


    -- Now return the results
    -- SELECT * FROM @Child ORDER BY TableName, ParentTableName
    -- SELECT * FROM @Result ORDER BY MaxDepth DESC, TableName

    RETURN;
END;
");

            migrationBuilder.Sql(@"
/* 
-- ========================================================

    Procedure Name: [Util.Table.CopyDefaultRecords] 

    Description: This function copys all of the -1 and -2 records for a table 
                    into all of the BIDs for which they don't exist.
                    It then deletes all the BID=-2 records.

    Sample Use:   

        EXEC [Util.Table.CopyDefaultRecords] @TableName = '_Root.Data';
        
-- ========================================================
*/
CREATE OR ALTER PROCEDURE [Util.Table.CopyDefaultRecords] 
    (@TableName NVARCHAR(200), @IncludeRequired BIT = 1, @IncludeDefault BIT = 1)
AS
BEGIN
    -- DECLARE @TableName NVARCHAR(200) = '_Root.Data';
    -- DECLARE @IncludeRequired BIT = 1;
    -- DECLARE @IncludeDefault BIT = 1

    DECLARE @BIDS NVARCHAR(30) = IIF(@IncludeRequired=1, IIF(@IncludeDefault=1, '(-1, -2)', '(-1)'), IIF(@IncludeDefault=1, '(-2)', '(null)'));
    DECLARE @Columns NVARCHAR(MAX);

    SELECT @Columns = CONCAT(@Columns + ', ', '[',ColumnName,']')
    FROM TablesAndColumns
    WHERE TableName = @TableName
        AND ColumnName != 'BID'
        AND IsComputed = 0
        AND IsIdentity = 0
    ;

    --PRINT @Columns;

    DECLARE @SQL NVARCHAR(MAX) = '

    INSERT INTO [@TABLENAME] ( BID, '+@Columns+' )
        SELECT B.BID, T.*
        FROM [Business.Data] B
        JOIN 
        (   SELECT '+@Columns+'
            FROM [@TABLENAME]
            WHERE BID IN '+@BIDS+'
        ) T ON 1=1
        WHERE NOT EXISTS (SELECT * FROM [@TABLENAME] D WHERE D.BID = B.BID AND D.ID = T.ID)
            AND B.BID > 0
    ;
    ';

    IF (@IncludeDefault=1)
        SET @SQL += '
    DELETE T
    FROM [@TABLENAME] T
    WHERE T.BID = -3
        AND T.ID IN (SELECT T2.ID FROM [@TABLENAME] T2 WHERE T2.BID = -2)
    ;

    UPDATE [@TABLENAME]
    SET BID = -3
    WHERE BID = -2
    ;
    ';

    SET @SQL = REPLACE(@SQL, '@TABLENAME', @TableName);

    -- PRINT @SQL

    EXECUTE(@SQL);
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP PROCEDURE IF EXISTS [Util.Table.CopyDefaultRecords]");
            migrationBuilder.Sql(@"DROP Function IF EXISTS [dbo].[Util.Table.DependencyDepth]");
        }
    }
}
