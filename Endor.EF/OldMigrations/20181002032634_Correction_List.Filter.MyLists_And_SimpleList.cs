using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181002032634_Correction_List.Filter.MyLists_And_SimpleList")]
    public partial class Correction_ListFilterMyLists_And_SimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION IF EXISTS [dbo].[List.Filter.MyLists];
GO

/***********************************************
  Description:
  This function returns a SimpleList of Lists for a particular UserLinkID for a TargetClassTypeID.
  
  Sample Usage:
  SELECT * FROM [List.Filter.SimpleList](7, 10001, 2000)
  
************************************************/
CREATE FUNCTION [dbo].[List.Filter.MyLists] (
-- DECLARE
      @BID SMALLINT
    , @UserLinkID INT
    , @TargetClassTypeID INT
)
RETURNS TABLE 
AS
RETURN 
(
    SELECT L.* 
    FROM [List.Filter] L
    WHERE BID = @BID 
        AND (L.TargetClassTypeID = @TargetClassTypeID)
        AND (L.IsSystem=1 
             OR L.OwnerID = @UserLinkID
             OR EXISTS( SELECT * 
                        FROM [List.Filter.EmployeeSubscription] LS 
                        JOIN [User.Link] UL ON LS.BID = UL.BID AND LS.EmployeeID = UL.EmployeeID
                        WHERE LS.BID = L.BID 
                          AND LS.FilterID = L.ID 
                          AND UL.ID = @UserLinkID
                        )
             )
);
GO

DROP FUNCTION IF EXISTS [dbo].[List.Filter.SimpleList];
GO

/***********************************************
  Description:
  This function returns a SimpleList of Lists for a particular Employee for a TargetClassTypeID.

  Sample Usage:
  SELECT * FROM [List.Filter.SimpleList](7, 10001, 2000)
************************************************/
CREATE FUNCTION [dbo].[List.Filter.SimpleList] (
-- DECLARE
      @BID SMALLINT
    , @UserLinkID INT
    , @TargetClassTypeID INT
)
RETURNS TABLE 
AS
RETURN 
(
    SELECT TOP 100 PERCENT 
           @BID as BID
         , L.[ID]
         , L.[ClassTypeID]
         , L.[Name] as DisplayName
         , L.[IsActive]
         , CONVERT(BIT,0) AS [HasImage]
         , L.TargetClassTypeID
         , (CASE WHEN LS.FilterID IS NOT NULL THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END) AS IsSubscribed
         , LS.IsShownAsTab
         , LS.IsFavorite
         , L.IsSystem
    FROM [List.Filter] L
	LEFT JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = @BID AND LS.FilterID = L.ID
    WHERE L.BID = @BID 
        AND (L.TargetClassTypeID = @TargetClassTypeID)
        AND (L.IsSystem=1 
             OR L.OwnerID = @UserLinkID
             OR EXISTS( SELECT * 
                        FROM [List.Filter.EmployeeSubscription] LS 
                        JOIN [User.Link] UL ON LS.BID = UL.BID AND LS.EmployeeID = UL.EmployeeID
                        WHERE LS.BID = L.BID 
                          AND LS.FilterID = L.ID 
                          AND UL.ID = @UserLinkID
                        )
             )
);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION IF EXISTS [dbo].[List.Filter.MyLists];
GO

--#######################################################################--

-- =============================================
-- This function returns a SimpleList of Lists for a particular EmployeeID for a TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.SimpleList](7, 10001, 2000)
--
-- =============================================

CREATE FUNCTION [dbo].[List.Filter.MyLists] (@BID SMALLINT, @UserLinkID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT L.* 
    FROM [List.Filter] L
    WHERE BID = @BID 
      AND ID IN (
                    SELECT L.ID
                    FROM [List.Filter] L
                    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
                      AND (L.IsSystem=1 OR (L.BID = @BID AND L.OwnerID = @UserLinkID)) 
    
                    UNION

                    SELECT L.ID
                    FROM [List.Filter] L
					JOIN [User.Link] UL ON UL.BID = L.BID AND UL.ID = @UserLinkID
					JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = L.BID AND LS.FilterID = L.ID AND LS.EmployeeID = UL.EmployeeID
					
                    WHERE (L.BID = @BID) 
                        AND (L.TargetClassTypeID = @TargetClassTypeID)
                        AND (UL.ID = @UserLinkID)
                )
);
GO

DROP FUNCTION IF EXISTS [dbo].[List.Filter.SimpleList];
GO

--#######################################################################--

-- =============================================
-- This function returns a SimpleList of Lists for a particular Employee for a TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.SimpleList](7, 10001, 2000)
--
-- =============================================
CREATE FUNCTION [dbo].[List.Filter.SimpleList] (@BID SMALLINT, @EmployeeID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT TOP 100 PERCENT 
           @BID as BID
         , L.[ID]
         , L.[ClassTypeID]
         , L.[Name] as DisplayName
         , L.[IsActive]
         , CONVERT(BIT,0) AS [HasImage]
         , L.TargetClassTypeID
         , (CASE WHEN LS.FilterID IS NOT NULL THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END) AS IsSubscribed
         , LS.IsShownAsTab
         , LS.IsFavorite
         , L.IsSystem
    FROM [List.Filter] L
    LEFT JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = @BID AND LS.FilterID = L.ID
    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
      AND (L.IsSystem=1 
           OR (L.BID = @BID 
               AND (L.IsPublic=1 OR L.OwnerID = @EmployeeID)
               )
          )
    ORDER BY [Name]
)

");
        }
    }
}

