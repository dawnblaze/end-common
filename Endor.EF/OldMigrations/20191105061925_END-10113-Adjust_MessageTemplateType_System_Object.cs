﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10113Adjust_MessageTemplateType_System_Object : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Message.TemplateType]
  SET AppliesToClassTypeID = (CASE WHEN ID IN (11,12) THEN 2000 ELSE 5000 END)
  WHERE AppliesToClassTypeID = 1012
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
