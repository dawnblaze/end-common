﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10222RemoveOICRollupAddVariableName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RollUpLinkedIncome",
                table: "Order.Item.Component");

            migrationBuilder.AddColumn<string>(
                name: "VariableName",
                table: "Order.Item.Component",
                type: "NVARCHAR(255)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VariableName",
                table: "Order.Item.Component");

            migrationBuilder.AddColumn<bool>(
                name: "RollUpLinkedIncome",
                table: "Order.Item.Component",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
