using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180828102206_END-2312_Remove_CanDelete_SPROCs_No_Longer_Used")]
    public partial class END2312_Remove_CanDelete_SPROCs_No_Longer_Used : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.GL.Account.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Accounting.GL.Account.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Industry.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Industry.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Labor.Category.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Part.Labor.Category.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Machine.Category.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Part.Machine.Category.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Machine.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Part.Machine.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Material.Category.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Part.Material.Category.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Material.Data.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Part.Material.Data.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Origin.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Labor.Data.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Part.Labor.Data.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.Payment.Term.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Accounting.Payment.Term.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.Tax.Group.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Accounting.Tax.Group.Action.CanDelete]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.Tax.Item.Action.CanDelete')
                    BEGIN
                        DROP PROCEDURE [Accounting.Tax.Item.Action.CanDelete]
                    END
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Accounting.GL.Account.Action.CanDelete]

    Description: This procedure checks if the GLAccount is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.GL.Account.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.GL.Account.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
    A GL Account can be deleted if:
        It is not used by any GL.Data record.
        It is not the parent of another GLAccount.
        If it is an inventory account, It is not used by an inventory object.
        It is not used by any Part Object as the expense or income account.
*/


    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.GL.Account] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('GL Account Specified Does not Exist. GLAccountID=', @ID)

    -- and that is it can be edited
    ELSE IF ( SELECT CanEdit FROM [Accounting.GL.Account] WHERE BID = @BID AND ID = @ID ) = 0

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('System GL Accounts Can not be deleted . GLAccountID=', @ID)

    -- and that is it not being used by [GL.Data]
    /* ELSE IF  EXISTS( SELECT * FROM [Accounting.GL.Data] WHERE BID = @BID AND GLAccountID = @ID )
             OR EXISTS( SELECT * FROM [Accounting.GL.Archive] WHERE BID = @BID AND GLAccountID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('GL Account is used in the GL.Data. GLAccountID=', @ID)
	*/
    -- It is it doesn't have any child GL Accounts under it
    ELSE IF EXISTS( SELECT * FROM [Accounting.GL.Account] WHERE BID = @BID AND ParentID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('GL Account has sub GL Accounts under it. GLAccountID=', @ID)

        -- If it is an inventory account, It is not used by an inventory object.
        -- FUTURE

        -- It is not used by any Part Object as the expense or income account.
        -- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO

                "
            );

            migrationBuilder.Sql
            (
                @"
-- ========================================================
-- Name: [Industry.Action.CanDelete]
--
-- Description: This procedure checks if the Industry is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Industry.Action.CanDelete] @BID=1, @ID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Industry.Action.CanDelete]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ID       INT     -- = 2

        , @Result         BIT     = NULL  OUTPUT
		, @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Industry specified is valid

	 IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @ID)
		SELECT @Result = 0
             , @CantDeleteReason = 'Industry specified was not found'

	ELSE IF EXISTS(SELECT * FROM [Company.Data] WHERE IndustryID = @ID)
        SELECT @Result = 0
             , @CantDeleteReason = 'The Industry is being used by a Company and can''t be deleted.'

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Part.Labor.Category.Action.CanDelete]

    Description: This procedure checks if the LaborCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Labor.Category.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Labor.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the LaborCategory can be deleted. The boolean response is returned in the body.  A LaborCategory can be deleted if
	- there are no labors linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Labor.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Category Specified Does not Exist. LaborCategoryID=', @ID)

    -- there are no labors linked to it or if the force option is specified to deletes the links.
	ELSE IF  EXISTS( SELECT * FROM [Part.Labor.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Category is linked to a Labor Data. LaborCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Part.Machine.Category.Action.CanDelete]

    Description: This procedure checks if the MachineCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Machine.Category.Action.CanDelete] @BID=1, @ID=1, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Machine.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the MachineCategory can be deleted. The boolean response is returned in the body.  A MachineCategory can be deleted if
	- there are no machines linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Machine.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Machine Category Specified Does not Exist. MachineCategoryID=', @ID)

    -- there are no machines linked to it or if the force option is specified to deletes the links.
	ELSE IF  EXISTS( SELECT * FROM [Part.Machine.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Machine Category is linked to a Machine Data. MachineCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Part.Machine.Action.CanDelete]

    Description: This procedure checks if the Machine.Data is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Machine.Action.CanDelete] @BID=1, @ID=2, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Machine.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
    Machine Object records can be deleted if all of the following is true:
	(future) It is linked to no Machine Categories.
    (future) It is not in use in any Product Template.
    (future) Is it not in use in any Order or Estimate.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Machine.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Machine Specified Does not Exist. MachineID=', @ID)

	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]
    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Part.Material.Category.Action.CanDelete]

    Description: This procedure checks if the MaterialCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Material.Category.Action.CanDelete] @BID=1, @ID=1, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Material.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the MaterialCategory can be deleted. The boolean response is returned in the body.  A MaterialCategory can be deleted if
	- there are no materials linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Material.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Material Category Specified Does not Exist. MaterialCategoryID=', @ID)

    -- there are no materials linked to it or if the force option is specified to deletes the links.
	ELSE IF  EXISTS( SELECT * FROM [Part.Material.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Material Category is linked to a Material Data. MaterialCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Part.Material.Data.Action.CanDelete]

    Description: This procedure checks if the Material is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Material.Data.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Material.Data.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the Material can be deleted. The boolean response is returned in the body.  A Material can be deleted if
	It is not linked to any Material Categories.
	(future) It is not in use in any Product Template.
	(future) Is it not in use in any Order or Estimate.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Material.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Material Specified Does not Exist. MaterialID=', @ID)

    -- It is not linked to any Material Categories.
	ELSE IF EXISTS( SELECT * FROM [Part.Material.CategoryLink] WHERE BID = @BID AND PartID = @ID )

	SELECT @Result = 0
		 	, @CantDeleteReason = CONCAT('Material is linked to a Material Category. MaterialID=', @ID)

	-- (future) It is not in use in any Product Template.
	-- (future) Is it not in use in any Order or Estimate.
	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
-- ========================================================
-- Name: [Origin.Action.CanDelete]
--
-- Description: This procedure checks if the origin is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Origin.Action.CanDelete] @BID=1, @ID=1005, @ShowCantDeleteReason=1
-- ========================================================
CREATE PROCEDURE [dbo].[Origin.Action.CanDelete]
		  @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @ID)
		SELECT @Result = 0
             , @CantDeleteReason = 'Origin specified was not found'

	ELSE IF EXISTS(SELECT * FROM [Company.Data] WHERE OriginID = @ID)
            --AND NOT EXISTS(SELECT * FROM [Order.Data] WHERE OriginID = @OriginID)
        SELECT @Result = 0
             , @CantDeleteReason = 'The Origin is being used by a Company and can''t be deleted.'

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Part.Labor.Data.Action.CanDelete]

    Description: This procedure checks if the Labor Data is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Labor.Data.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Labor.Data.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the Labor Data can be deleted. The boolean response is returned in the body.  A Labor Data can be deleted if
	- there are no categories linked to it.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Labor.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Data Specified Does not Exist. LaborDataID=', @ID)

    -- there are no categories linked to it.
	ELSE IF  EXISTS( SELECT * FROM [Part.Labor.CategoryLink] WHERE BID = @BID AND PartID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Data is linked to a Labor Category. LaborDataID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Accounting.Payment.Term.Action.CanDelete]

    Description: This procedure checks if the PaymentTerm is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Payment.Term.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Payment.Term.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the PaymentTerm can be deleted. The boolean response is returned in the body.  A PaymentTerm can be deleted if
	It is used by no companies. 
	It is used by no order
	It is used by no estimates
	It is used by no purchase orders
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Payment.Term] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term Specified Does not Exist. PaymentTermID=', @ID)

/* --FUTURE
	ELSE IF  EXISTS( SELECT * FROM [Company.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Company Data. PaymentTermID=', @ID)

	ELSE IF EXISTS( SELECT * FROM [Order.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Order Data. PaymentTermID=', @ID)
	ELSE IF EXISTS( SELECT * FROM [Estimate.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Estimate Data. PaymentTermID=', @ID)
	ELSE IF EXISTS( SELECT * FROM [PO.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Purchase Order Data. PaymentTermID=', @ID)
*/
    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Accounting.Tax.Group.Action.CanDelete]

    Description: This procedure checks if the TaxGroup is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Tax.Group.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the TaxGroup can be deleted. The boolean response is returned in the body.  A TaxGroup can be deleted if
	It is not the default for a Location. Location.Data.DefaultTaxGroupID
	It is not the default for a Company. Company.Data.TaxGroupID
	Is it not used by any order. (?)
	It is not used by the GL.Data.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group Specified Does not Exist. TaxGroupID=', @ID)

    -- It is not the default for a Location. Location.Data.DefaultTaxGroupID
	-- It is not the default for a Company. Company.Data.TaxGroupID
	ELSE IF  EXISTS( SELECT * FROM [Location.Data] WHERE BID = @BID AND DefaultTaxGroupID = @ID )
             OR EXISTS( SELECT * FROM [Company.Data] WHERE BID = @BID AND TaxGroupID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group is used in the Location and Company Data. TaxGroupID=', @ID)

	-- Is it not used by any order. (?)
	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );

            migrationBuilder.Sql
            (
                @"
/* 
========================================================
    Name: [Accounting.Tax.Item.Action.CanDelete]

    Description: This procedure checks if the TaxItem is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Tax.Item.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Tax.Item.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the TaxItem can be deleted. The boolean response is returned in the body.  A TaxItem can be deleted if
	It is not linked to a TaxGroup.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group Specified Does not Exist. TaxItemID=', @ID)

    -- It is not linked to a TaxGroup.
	ELSE IF  EXISTS( SELECT * FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID AND ItemID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Item is used in the Tax Group Data. TaxItemID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
                "
            );
        }
    }
}

