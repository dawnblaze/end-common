﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10493_assemblyovdatajson_column_for_machinedata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AssemblyOVDataJSON",
                table: "Part.Machine.Data",
                type: "varchar(max)",
                nullable: true);

            migrationBuilder.Sql(@"
                INSERT INTO [enum.Part.Subassembly.LayoutType] VALUES (14, 'Machine Template Properties');
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssemblyOVDataJSON",
                table: "Part.Machine.Data");

            migrationBuilder.Sql(@"
                DELETE FROM [enum.Part.Subassembly.LayoutType] WHERE ID = 14;         
            ");                
        }
    }
}
