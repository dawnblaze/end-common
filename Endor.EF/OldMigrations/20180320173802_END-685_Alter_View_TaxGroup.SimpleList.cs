using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180320173802_END-685_Alter_View_TaxGroup.SimpleList")]
    public partial class END685_Alter_View_TaxGroupSimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Accounting.Tax.Group.SimpleList] AS
                    SELECT [BID]
                         , [ID]
                         , [ClassTypeID]
                         , [Name] as DisplayName
                         , [IsActive]
                         , CONVERT(BIT, 0) AS [HasImage]
                         , CONVERT(BIT, 0) AS [IsDefault]
		                 , [TaxRate]
                    FROM [Accounting.Tax.Group]
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Accounting.Tax.Group.SimpleList] AS
                    SELECT [BID]
                         , [ID]
                         , [ClassTypeID]
                         , [Name] as DisplayName
                         , [IsActive]
                         , CONVERT(BIT, 0) AS [HasImage]
                         , CONVERT(BIT, 0) AS [IsDefault]
                    FROM [Accounting.Tax.Group]
                GO
            ");
        }
    }
}

