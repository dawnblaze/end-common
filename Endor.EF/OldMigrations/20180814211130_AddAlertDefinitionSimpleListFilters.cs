using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180814211130_AddAlertDefinitionSimpleListFilters")]
    public partial class AddAlertDefinitionSimpleListFilters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Alert.Definition.SimpleList]
                GO
                CREATE VIEW [dbo].[Alert.Definition.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                    , [IsActive]
                    , [DataType]
                    , [EmployeeID]
                    , CONVERT(BIT, 0) AS[HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[Alert.Definition];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Alert.Definition]
            ");
        }
    }
}

