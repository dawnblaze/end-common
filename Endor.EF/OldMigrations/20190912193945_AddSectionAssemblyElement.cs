﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddSectionAssemblyElement : Migration
    {
        private const byte SectionElementType = 121;
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($@"
-- BEGIN TRANSACTION

DECLARE @Debug BIT = 0;

IF NOT EXISTS(SELECT * FROM [enum.Part.Subassembly.ElementType] WHERE ID = 121)
    INSERT INTO [enum.Part.Subassembly.ElementType] (ID, Name)
        SELECT 121, 'Section'
;

-- AddSectionAssemblyElement migration

-- intent:
-- for all existing [Part.Subassembly.Element] records,
-- if they are ParentID = null (top level)
-- grouped by BID/SubassemblyID/LayoutID
-- create a ElementType=Section parent for assembly elements at the top of the form
-- create a ElementType=Section parent for all non-assembly elements
-- create a ElementType=Section parent for assembly elements at the bottom of the form
-- and reparent the existing elements to be children of these new sections

-- Create a Temporary Table to hold our metadata and new section IDs
DECLARE @NewSections TABLE (
    BID SMALLINT, --allows migration to migrate all bids
    SubassemblyID INT, --for grouping
    LayoutID SMALLINT, --for grouping
    ElementType TINYINT, --for matching the right section to the right elements
    [Column] TINYINT, --for matching the right section to the right elements
    NewSectionID INT --to hold our new section IDs
);

-- Fill the Metadata 
INSERT INTO @NewSections (BID, SubassemblyID, LayoutID, ElementType, [Column])
    SELECT DISTINCT BID, SubassemblyID, LayoutID, ElementType, [Column]
    FROM [Part.Subassembly.Element] 
    WHERE (ParentID IS NULL) AND
       (Elementtype IN (0, 106)) -- 0 = top level non-assembly elements, 106 = above-main-section assembly elements

-- IF (@Debug = 1) SELECT * FROM @NewSections;

-- We have to get our IDs for each BID, which unfortunatley means looping through all of them .. :-(
DECLARE @FirstIDTable TABLE (
    BID SMALLINT,
    IDCount INT,
    FirstID INT,
    LastID AS (FirstID + IDCount -1)
);

 INSERT INTO @FirstIDTable (BID, IDCount)
  SELECT DISTINCT BID, COUNT(*)
  FROM @NewSections
  GROUP BY BID
  ;

-- Loop through for each BID and get a starting ID
-- Do you believe how much code we need for a blooming loop!
DECLARE @ThisBID SMALLINT
      , @IDCount INT
      , @FirstSectionID INT
      ;

DECLARE C CURSOR FOR
    SELECT DISTINCT BID FROM @NewSections;

OPEN C;

FETCH NEXT FROM C 
    INTO @ThisBID;

WHILE @@FETCH_STATUS = 0
BEGIN
    -- Retreive the Business Counts
    SELECT @IDCount = IDCount
         , @FirstSectionID = NULL
    FROM @FirstIDTable 
    WHERE BID = @ThisBID
    ;

    -- Get the IDs for that Business
    EXEC @FirstSectionID = [dbo].[Util.ID.GetID] @BID = @ThisBID, @ClassTypeID = 12044, @Count = @IDCount

    -- Now Update the List
    UPDATE @FirstIDTable
    SET FirstID = @FirstSectionID
    WHERE BID = @ThisBID
    ;

    -- And repeat
    FETCH NEXT FROM C 
        INTO @ThisBID;
END;

CLOSE C;
DEALLOCATE C;

IF (@Debug = 1) SELECT * FROM @FirstIDTable;

-- Now Update The Table With their New Values
UPDATE UQ
SET UQ.NewSectionID = UQ.ComputedSectionID
FROM (
        SELECT NewSectionID, FID.FirstID + ROW_NUMBER() OVER(ORDER BY NS.BID, NS.SubassemblyID)-1 AS ComputedSectionID
        FROM @NewSections NS
        JOIN @FirstIDTable FID ON NS.BID = FID.BID
     ) UQ
;

IF (@Debug = 1) SELECT * FROM @NewSections;
;

-- we create our new section elements
-- note parentID is null (making it top level) and column is 0 and columnswide is 1
INSERT INTO [Part.Subassembly.Element] (BID, ID, [Column], ColumnsWide, ElementType, IsDisabled, IsReadOnly, [Row], SubassemblyID, DataType, LayoutID, VariableName)
    SELECT  BID, 
            NewSectionID, --ID
            0, --column
            1, --columnswide
            121, --elementtype
            0, --isdisabled
            0, --isreadonly
            0, --row
            SubassemblyID, 
            0, --datatype
            LayoutID, 
            '' --variablename
    FROM @NewSections;

-- Create a table to capture the changes
DECLARE @ModifiedValues TABLE (
            BID SMALLINT,
            ElementID INT,
            OldParentID INT,
            NewParentID INT
);

-- now reparent our top-level elements to use the new sections
UPDATE EL 
SET ParentID = NS.NewSectionID 
OUTPUT inserted.BID, inserted.ID, deleted.ParentID, inserted.ParentID INTO @ModifiedValues
FROM [Part.Subassembly.Element] EL
JOIN @NewSections NS ON EL.BID = NS.BID 
WHERE EL.ParentID IS NULL
  AND EL.ElementType != 121
  AND EL.LayoutID = NS.LayoutID 
  AND EL.SubassemblyID = NS.SubassemblyID 
  AND EL.ElementType = NS.ElementType
  AND EL.[Column] = NS.[Column]
;

IF (@Debug = 1) SELECT * FROM @ModifiedValues;

-- ROLLBACK TRANSACTION
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql($@"
UPDATE [Part.Subassembly.Element]
SET ParentID = null
WHERE ParentID is not null and EXISTS
(
    SELECT 1
    FROM [Part.Subassembly.Element] as parent
    WHERE 
    parent.BID = BID and
    parent.ElementType = ${SectionElementType} and
    parent.ID = ParentID
);

DELETE FROM [Part.Subassembly.Element] where ElementType = {SectionElementType};
");

            migrationBuilder.Sql($"DELETE FROM [enum.Part.Subassembly.ElementType] WHERE ID = {SectionElementType}");

        }
    }
}
