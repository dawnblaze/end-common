using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_Estimate_Summary_Widget_Data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            INSERT INTO [dbo].[System.Dashboard.Widget.Definition]
             ([ID],[DefaultCols],[DefaultName],[DefaultOptions],[DefaultRefreshIntervalInMin],[DefaultRows],[Description] ,[HasImage] ,[MaxCols],[MaxRows],[MinCols],[MinRows],[ModifiedDT],[Modules],[SecurityRightID])
            VALUES
             (7,2,'Estimate Summary',NULL,15,1 ,'Estimate Summary',1 ,4,2,2,1,'2018-10-01T18:15:14.79',NULL,NULL);
            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
