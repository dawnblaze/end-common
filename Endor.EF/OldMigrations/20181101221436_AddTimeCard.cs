using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddTimeCard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employee.TimeCard",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AdjustedByEmployeeID = table.Column<short>(nullable: true),
                    AdjustedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false),
                    EmployeeID = table.Column<short>(nullable: false),
                    EndDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    IsAdjusted = table.Column<bool>(nullable: false),
                    IsClosed = table.Column<bool>(nullable: true),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    PaidTimeInMin = table.Column<decimal>(type: "decimal(18,4)", nullable: true),
                    StartDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false),
                    TimeInMin = table.Column<decimal>(type: "decimal(18,4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.TimeCard", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Employee.TimeCard_AdjustedBy",
                        columns: x => new { x.BID, x.AdjustedByEmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.TimeCard_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard_BID_AdjustedByEmployeeID",
                table: "Employee.TimeCard",
                columns: new[] { "BID", "AdjustedByEmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard_Employee",
                table: "Employee.TimeCard",
                columns: new[] { "BID", "EmployeeID", "StartDT", "IsClosed" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employee.TimeCard");
        }
    }
}
