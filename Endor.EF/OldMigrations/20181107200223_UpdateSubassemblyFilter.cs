using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSubassemblyFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (12042 --<TargetClassTypeID, int,>
                        ,'Name' --<Name, varchar(255),>
                        ,'Name' --<Label, varchar(255),>
                        ,'Name' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,1 --<DataType, tinyint,>
                        ,0 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,0 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (12042 --<TargetClassTypeID, int,>
                        ,'Include Inactive' --<Name, varchar(255),>
                        ,'Include Inactive' --<Label, varchar(255),>
                        ,'-IsActive' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,13 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,1 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (12040 --<TargetClassTypeID, int,>
                        ,'Name' --<Name, varchar(255),>
                        ,'Name' --<Label, varchar(255),>
                        ,'Name' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,1 --<DataType, tinyint,>
                        ,0 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,0 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (12040 --<TargetClassTypeID, int,>
                        ,'Include Inactive' --<Name, varchar(255),>
                        ,'Include Inactive' --<Label, varchar(255),>
                        ,'-IsActive' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,13 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,1 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[List.Filter]
                (BID,ID,[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],
				[IsPublic],[IsSystem],[IsDefault],[SortIndex])
                VALUES
                (1,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]),
				'2018-11-07',
				'2018-11-07',
				1,
				'All',
				12042,
				null,
				null,
				null,
				0,
				1,
				1,0);
            ");
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[List.Filter]
                (BID,ID,[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],
				[IsPublic],[IsSystem],[IsDefault],[SortIndex])
                VALUES
                (1,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]),
				'2018-11-07',
				'2018-11-07',
				1,
				'All',
				12040,
				null,
				null,
				null,
				0,
				1,
				1,0);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
Delete From [System.List.Filter.Criteria] where TargetClassTypeID='12042'
            ");
            migrationBuilder.Sql(@"
Delete From [List.Filter] where TargetClassTypeID='12042'
            ");
            migrationBuilder.Sql(@"
Delete From [System.List.Filter.Criteria] where TargetClassTypeID='12040'
            ");
            migrationBuilder.Sql(@"
Delete From [List.Filter] where TargetClassTypeID='12040'
            ");
        }
    }
}
