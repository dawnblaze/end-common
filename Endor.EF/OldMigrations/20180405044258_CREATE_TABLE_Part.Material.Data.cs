using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180405044258_CREATE_TABLE_Part.Material.Data")]
    public partial class CREATE_TABLE_PartMaterialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
   

            migrationBuilder.CreateTable(
                name: "Part.Material.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((12000))"),
                    Color = table.Column<string>(type: "varchar(255)", nullable: true),
                    ConsumptionUnit = table.Column<byte>(type: "tinyint", nullable: false),
                    Description = table.Column<string>(type: "varchar(255)", nullable: false),
                    EstimatingConsumptionMethod = table.Column<byte>(type: "tinyint", nullable: false),
                    EstimatingCost = table.Column<decimal>(nullable: true),
                    EstimatingCostingMethod = table.Column<byte>(type: "tinyint", nullable: false),
                    EstimatingPrice = table.Column<decimal>(nullable: true),
                    ExpenseAccountID = table.Column<int>(nullable: false),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    Height = table.Column<decimal>(nullable: true),
                    HeightUnit = table.Column<byte>(type: "tinyint", nullable: true),
                    IncomeAccountID = table.Column<int>(nullable: false),
                    InventoryAccountID = table.Column<int>(nullable: false),
                    InvoiceText = table.Column<string>(type: "varchar(255)", nullable: false),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    Length = table.Column<decimal>(nullable: true),
                    LengthUnit = table.Column<byte>(type: "tinyint", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(type: "nvarchar(255)", nullable: false),
                    PhysicalMaterialType = table.Column<byte>(type: "tinyint", nullable: false, defaultValueSql: "0"),
                    QuantityInSet = table.Column<byte>(nullable: true),
                    SKU = table.Column<string>(type: "varchar(512)", nullable: true),
                    TrackInventory = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    Weight = table.Column<decimal>(nullable: true),
                    WeigthUnit = table.Column<byte>(type: "tinyint", nullable: true),
                    Width = table.Column<decimal>(nullable: true),
                    WidthUnit = table.Column<byte>(type: "tinyint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Material.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_enum.Part.Unit",
                        column: x => x.ConsumptionUnit,
                        principalTable: "enum.Part.Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_enum.Part.Material.Consumption.Method",
                        column: x => x.EstimatingConsumptionMethod,
                        principalTable: "enum.Part.Material.Consumption.Method",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_enum.Part.Material.Costing.Method",
                        column: x => x.EstimatingCostingMethod,
                        principalTable: "enum.Part.Material.Costing.Method",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_enum.Part.Unit1",
                        column: x => x.HeightUnit,
                        principalTable: "enum.Part.Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_enum.Part.Unit3",
                        column: x => x.LengthUnit,
                        principalTable: "enum.Part.Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_enum.Part.Physical.Material.Type",
                        column: x => x.PhysicalMaterialType,
                        principalTable: "enum.Part.Material.Physical.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_enum.Part.Unit4",
                        column: x => x.WeigthUnit,
                        principalTable: "enum.Part.Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_enum.Part.Unit2",
                        column: x => x.WidthUnit,
                        principalTable: "enum.Part.Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_Accounting.GL.Account1",
                        columns: x => new { x.BID, x.ExpenseAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Material.Data_Accounting.GL.Account",
                        columns: x => new { x.BID, x.IncomeAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "[FK_Part.Material.Data_Accounting.GL.Account2]",
                        columns: x => new { x.BID, x.InventoryAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_ConsumptionUnit",
                table: "Part.Material.Data",
                column: "ConsumptionUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_EstimatingConsumptionMethod",
                table: "Part.Material.Data",
                column: "EstimatingConsumptionMethod");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_EstimatingCostingMethod",
                table: "Part.Material.Data",
                column: "EstimatingCostingMethod");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_HeightUnit",
                table: "Part.Material.Data",
                column: "HeightUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_LengthUnit",
                table: "Part.Material.Data",
                column: "LengthUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_PhysicalMaterialType",
                table: "Part.Material.Data",
                column: "PhysicalMaterialType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_WeigthUnit",
                table: "Part.Material.Data",
                column: "WeigthUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_WidthUnit",
                table: "Part.Material.Data",
                column: "WidthUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_BID_ExpenseAccountID",
                table: "Part.Material.Data",
                columns: new[] { "BID", "ExpenseAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_BID_IncomeAccountID",
                table: "Part.Material.Data",
                columns: new[] { "BID", "IncomeAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_BID_InventoryAccountID",
                table: "Part.Material.Data",
                columns: new[] { "BID", "InventoryAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_Name",
                table: "Part.Material.Data",
                columns: new[] { "BID", "Name", "IsActive" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Material.Data");
        }
    }
}

