using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSubassemblyTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subassembly.Element.Data_Part.Subassembly.Data",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "HasFormula",
                table: "Part.Subassembly.Variable");

            migrationBuilder.RenameColumn(
                name: "Options",
                table: "[Part.Subassembly.Variable]",
                newName: "Tooltip");

            migrationBuilder.RenameColumn(
                name: "IsSystem",
                table: "[Part.Subassembly.Variable]",
                newName: "IsFormula");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Part.Subassembly.Variable"
                );

            migrationBuilder.AddColumn<string>(
                name: "ListValuesJSON",
                table: "Part.Subassembly.Variable",
                nullable: true,
                type: "nvarchar(max) sparse");

            migrationBuilder.RenameColumn(
                name: "DefaultValueText",
                table: "[Part.Subassembly.Variable]",
                newName: "DefaultValue");

            migrationBuilder.AddColumn<bool>(
                name: "AllowCustomValue",
                table: "Part.Subassembly.Variable",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AllowDecimals",
                table: "Part.Subassembly.Variable",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AllowMultiSelect",
                table: "Part.Subassembly.Variable",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AltText",
                table: "Part.Subassembly.Variable",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "DecimalPlaces",
                table: "Part.Subassembly.Variable",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "DisplayType",
                table: "Part.Subassembly.Variable",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "ElementType",
                table: "Part.Subassembly.Variable",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "ElementUseCount",
                table: "Part.Subassembly.Variable",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<bool>(
                name: "GroupOptionsByCategory",
                table: "Part.Subassembly.Variable",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Label",
                table: "Part.Subassembly.Variable",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "LabelType",
                table: "Part.Subassembly.Variable",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ListDataType",
                table: "Part.Subassembly.Variable",
                type: "smallint sparse",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "SystemVariableID",
                table: "Part.Subassembly.Variable",
                type: "smallint sparse",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "UnitID",
                table: "Part.Subassembly.Variable",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "LayoutID",
                table: "Part.Subassembly.Element",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<string>(
                name: "VariableName",
                table: "Part.Subassembly.Element",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Variable_DisplayType",
                table: "Part.Subassembly.Variable",
                column: "DisplayType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Variable_LabelType",
                table: "Part.Subassembly.Variable",
                column: "LabelType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Variable_UnitID",
                table: "Part.Subassembly.Variable",
                column: "UnitID");

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.CustomField.DisplayType",
                table: "Part.Subassembly.Variable",
                column: "DisplayType",
                principalTable: "enum.CustomField.DisplayType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.Part.Subassembly.LabelType",
                table: "Part.Subassembly.Variable",
                column: "LabelType",
                principalTable: "enum.Part.Subassembly.LabelType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.Unit",
                table: "Part.Subassembly.Variable",
                column: "UnitID",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.CustomField.DisplayType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.Part.Subassembly.LabelType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.Unit",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Variable_DisplayType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Variable_LabelType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Variable_UnitID",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "AllowCustomValue",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "AllowDecimals",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "AllowMultiSelect",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "AltText",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "DecimalPlaces",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "DisplayType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "ElementType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "ElementUseCount",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "GroupOptionsByCategory",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "Label",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "LabelType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "ListDataType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "SystemVariableID",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "UnitID",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "LayoutID",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "VariableName",
                table: "Part.Subassembly.Element");


            migrationBuilder.DropColumn(
                name: "ListValuesJSON",
                table: "Part.Subassembly.Variable"
                );

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Part.Subassembly.Variable",
                type: "nvarchar(max)");

            migrationBuilder.RenameColumn(
                name: "Tooltip",
                table: "[Part.Subassembly.Variable]",
                newName: "Options");

            migrationBuilder.RenameColumn(
                name: "IsFormula",
                table: "[Part.Subassembly.Variable]",
                newName: "IsSystem");

            migrationBuilder.RenameColumn(
                name: "DefaultValue",
                table: "[Part.Subassembly.Variable]",
                newName: "DefaultValueText");

            migrationBuilder.AddColumn<bool>(
                name: "HasFormula",
                table: "Part.Subassembly.Variable",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_Subassembly.Element.Data_Part.Subassembly.Data",
                table: "Part.Subassembly.Element",
                columns: new[] { "BID", "SubassemblyID" },
                principalTable: "Part.Subassembly.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
