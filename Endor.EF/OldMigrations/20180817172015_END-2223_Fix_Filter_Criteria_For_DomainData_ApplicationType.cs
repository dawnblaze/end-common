using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180817172015_END-2223_Fix_Filter_Criteria_For_DomainData_ApplicationType")]
    public partial class END2223_Fix_Filter_Criteria_For_DomainData_ApplicationType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.List.Filter.Criteria]
                    SET
                    Field = 'ApplicationType',
                    InputType = 12,
                    ListValues = 'Business,CustomerPortal,ECommerce,VendorPortal'
                    WHERE [Name] = 'Application Type'
                    AND TargetClassTypeID = 1021
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.List.Filter.Criteria]
                    SET
                    Field = 'AccessType',
                    InputType = 4,
                    ListValues = NULL
                    WHERE [Name] = 'Application Type'
                    AND TargetClassTypeID = 1021
                "
            );
        }
    }
}

