using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSPROC_UtilIDResetAll : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
DROP PROCEDURE [dbo].[Util.ID.Reset.All]
GO

/*
This stored procedure resets the NextID counters for a Business ID

Usage:
    EXEC dbo.[Util.ID.Reset.All] @BID = 1, @TestOnly = 1

*/
CREATE   PROCEDURE [dbo].[Util.ID.Reset.All] ( @BID SMALLINT, @TestOnly BIT = 0  )
AS 
BEGIN

    BEGIN TRANSACTION [ResetBusinessIDs]
    BEGIN TRY

        DECLARE @SQL VARCHAR(MAX) = '';

        DROP TABLE IF EXISTS ##MaxIDTable;

        -- Create a Global Temp Table - It must be Global so we can reference it via Dynamic SQL.  (yuck)
        CREATE TABLE ##MaxIDTable (BID SMALLINT, TableName VARCHAR(255), ClassTypeID SMALLINT, MaxID INT, SQLText VARCHAR(MAX));

        INSERT INTO ##MaxIDTable
            SELECT @BID, TableName, CONVERT(SMALLINT, REPLACE(REPLACE(ComputedFormula, '(', ''), ')', '')) AS ComputedID, NULL, NULL
            FROM TablesAndColumns T1
            WHERE ColumnName = 'ClassTypeID' AND IsComputed = 1
            AND TableName NOT LIKE '%Histor%'
            AND TableName NOT IN ('Order.Data', 'Util.NextID', '_Root.Data', 'Report.Menu', 'DM.Access.Token')
			AND ColumnType IN ('INT', 'SMALLINT', 'TINYINT')
            ORDER BY TableName
        ;

        -- Since the order table contains both orders and estimates, we have to update it manually
        INSERT INTO ##MaxIDTable SELECT @BID, 'Order.Data', 10000, NULL, NULL;

        -- Create the SQL Update query for each row
        UPDATE ##MaxIDTable
        SET SQLText = CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(ID) FROM [', TableName, '] WHERE BID = ',@BID ,' AND ClassTypeID = ', ClassTypeID, ' ) WHERE ClassTypeID = ', ClassTypeID, '; ')
        ;

        -- Now handle the Faux-IDs, OrderNumber, InvoiceNumber and EstimateNumber
        INSERT INTO ##MaxIDTable  (BID, TableName, ClassTypeID, MaxID, SQLText)
        VALUES ( @BID, 'OrderNumber'   , -10000, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(Number) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10000 ) WHERE ClassTypeID = ', -10000, '; ') )
                , ( @BID, 'InvoiceNumber' , -10003, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(InvoiceNumber) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10000 ) WHERE ClassTypeID = ', -10003, '; ') )
                , ( @BID, 'EstimateNumber', -10200, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(Number) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10200 ) WHERE ClassTypeID = ', -10200, '; ') )
        ;

        -- Combine the SQL Updates into one massive query
        SELECT @SQL = CONCAT(@SQL, SQLText) FROM ##MaxIDTable;

        -- Now update the Table
        EXEC(@SQL);

        -- Set any NULL to 0 to start
        UPDATE ##MaxIDTable SET MaxID = 0 WHERE MaxID IS NULL;

        -- Preview the Results
        IF (@TestOnly = 1) SELECT * FROM ##MaxIDTable ORDER BY BID, ClassTypeID
        IF (@TestOnly = 1) SELECT * FROM [Util.NextID]  WHERE BID = @BID order by BID, ClassTypeID


        -- INSERT Records if they Don't exist
        INSERT INTO [Util.NextID]
            SELECT BID, ClassTypeID, MaxID+1
            FROM ##MaxIDTable T
            WHERE NOT EXISTS(SELECT * FROM [Util.NextID] T2 WHERE T.BID = T2.BID and T.ClassTypeID = T2.ClassTypeID)
        ;

        -- UPDATE Records if they are not correct
        UPDATE U
        SET NextID = T.MaxID + 1
        FROM [Util.NextID] U
        JOIN ##MaxIDTable T ON U.BID = T.BID and U.ClassTypeID = T.ClassTypeID
        WHERE T.MaxID+1 != COALESCE(U.NextID, -1)
        ;

        IF (@TestOnly = 1) SELECT * FROM [Util.NextID] WHERE BID = @BID order by BID, ClassTypeID

        DROP TABLE ##MaxIDTable;

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [ResetBusinessIDs]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [ResetBusinessIDs];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [ResetBusinessIDs]
END;

GO
"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
DROP PROCEDURE [dbo].[Util.ID.Reset.All]
GO

/*
This stored procedure resets the NextID counters for a Business ID

Usage:
    EXEC dbo.[Util.ID.Reset.All] @BID = 1, @TestOnly = 1

*/
CREATE   PROCEDURE [dbo].[Util.ID.Reset.All] ( @BID SMALLINT, @TestOnly BIT = 0  )
AS 
BEGIN

    BEGIN TRANSACTION [ResetBusinessIDs]
    BEGIN TRY

        DECLARE @SQL VARCHAR(MAX) = '';

        DROP TABLE IF EXISTS ##MaxIDTable;

        -- Create a Global Temp Table - It must be Global so we can reference it via Dynamic SQL.  (yuck)
        CREATE TABLE ##MaxIDTable (BID SMALLINT, TableName VARCHAR(255), ClassTypeID SMALLINT, MaxID INT, SQLText VARCHAR(MAX));

        INSERT INTO ##MaxIDTable
            SELECT @BID, TableName, CONVERT(SMALLINT, REPLACE(REPLACE(ComputedFormula, '(', ''), ')', '')) AS ComputedID, NULL, NULL
            FROM TablesAndColumns T1
            WHERE ColumnName = 'ClassTypeID' AND IsComputed = 1
            AND TableName NOT LIKE '%Histor%'
            AND TableName NOT IN ('Order.Data', 'Util.NextID', '_Root.Data', 'Report.Menu')
            ORDER BY TableName
        ;

        -- Since the order table contains both orders and estimates, we have to update it manually
        INSERT INTO ##MaxIDTable SELECT @BID, 'Order.Data', 10000, NULL, NULL;

        -- Create the SQL Update query for each row
        UPDATE ##MaxIDTable
        SET SQLText = CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(ID) FROM [', TableName, '] WHERE BID = ',@BID ,' AND ClassTypeID = ', ClassTypeID, ' ) WHERE ClassTypeID = ', ClassTypeID, '; ')
        ;

        -- Now handle the Faux-IDs, OrderNumber, InvoiceNumber and EstimateNumber
        INSERT INTO ##MaxIDTable  (BID, TableName, ClassTypeID, MaxID, SQLText)
        VALUES ( @BID, 'OrderNumber'   , -10000, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(Number) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10000 ) WHERE ClassTypeID = ', -10000, '; ') )
                , ( @BID, 'InvoiceNumber' , -10003, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(InvoiceNumber) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10000 ) WHERE ClassTypeID = ', -10003, '; ') )
                , ( @BID, 'EstimateNumber', -10200, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(Number) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10200 ) WHERE ClassTypeID = ', -10200, '; ') )
        ;

        -- Combine the SQL Updates into one massive query
        SELECT @SQL = CONCAT(@SQL, SQLText) FROM ##MaxIDTable;

        -- Now update the Table
        EXEC(@SQL);

        -- Set any NULL to 0 to start
        UPDATE ##MaxIDTable SET MaxID = 0 WHERE MaxID IS NULL;

        -- Preview the Results
        IF (@TestOnly = 1) SELECT * FROM ##MaxIDTable ORDER BY BID, ClassTypeID
        IF (@TestOnly = 1) SELECT * FROM [Util.NextID]  WHERE BID = @BID order by BID, ClassTypeID


        -- INSERT Records if they Don't exist
        INSERT INTO [Util.NextID]
            SELECT BID, ClassTypeID, MaxID+1
            FROM ##MaxIDTable T
            WHERE NOT EXISTS(SELECT * FROM [Util.NextID] T2 WHERE T.BID = T2.BID and T.ClassTypeID = T2.ClassTypeID)
        ;

        -- UPDATE Records if they are not correct
        UPDATE U
        SET NextID = T.MaxID + 1
        FROM [Util.NextID] U
        JOIN ##MaxIDTable T ON U.BID = T.BID and U.ClassTypeID = T.ClassTypeID
        WHERE T.MaxID+1 != COALESCE(U.NextID, -1)
        ;

        IF (@TestOnly = 1) SELECT * FROM [Util.NextID] WHERE BID = @BID order by BID, ClassTypeID

        DROP TABLE ##MaxIDTable;

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [ResetBusinessIDs]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [ResetBusinessIDs];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [ResetBusinessIDs]
END;

GO
"
            );
        }
    }
}
