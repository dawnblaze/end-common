using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180410195809_change_LaborData_ClassTypeID_column")]
    public partial class change_LaborData_ClassTypeID_column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Part.Labor.Data",
                nullable: false,
                computedColumnSql: "((12020))",
                oldClrType: typeof(int),
                oldComputedColumnSql: "12000");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Part.Labor.Data",
                nullable: false,
                computedColumnSql: "12000",
                oldClrType: typeof(int),
                oldComputedColumnSql: "((12020))");
        }
    }
}

