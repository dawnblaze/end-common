using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddReportingSectionAndCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms])
VALUES ( 800, 'Reports and Documents', 1000, 0, 1, NULL);
");

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
VALUES
      (801, 'Document Report Templates', 800, 'Document Report Templates', 2, 0, 'Document Documents Report Template Invoice Estimate Statement Contract Quote Quotation Proposal Customize Customization Custom ')
    , (802, 'Document Reports', 800, 'Document Reports', 2, 0, 'Document Documents Report Invoice Estimate Statement Contract Quote Quotation Proposal Customize Customization Custom ')
    , (803, 'Analytic Report Tempaltes', 800, 'Analytic Report Tempaltes', 2, 0, 'Report Reports Template Sales Financial Timeclock Production WIP GL Payments AR A/R Reconciliation Time')
    , (804, 'Analytic Reports', 800, 'Analytic Reports', 2, 0, 'Report Reports Sales Financial Timeclock Production WIP GL Payments AR A/R Reconciliation Time')
    ;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[System.Option.Category] WHERE [ID] IN (801, 802, 803, 804);
");

            migrationBuilder.Sql(@"
DELETE FROM [dbo].[System.Option.Section] WHERE [ID] = 800;
");
        }
    }
}
