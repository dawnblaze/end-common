using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END7263_ContraintsCheckTrust : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"/*
    This procedure resets the Trust indicator on all Indexes and Foreign Keys.
    If indexes or constraints are not trusted, they will not be used in any query
    or optimization.

    Adapted from: https://www.brentozar.com/blitz/foreign-key-trusted/

    Example Usage:
        EXEC dbo.[Util.Constraints.CheckTrust];

        EXEC dbo.[Util.Constraints.CheckTrust] @Repair = 1;
*/
CREATE PROCEDURE [Util.Constraints.CheckTrust] @Repair BIT = 0
AS
BEGIN
    DECLARE @Result TABLE ( SchemaName VARCHAR(255) NOT NULL
                          , TableName VARCHAR(255) NOT NULL
                          , ConstraintName VARCHAR(512) NOT NULL
                          , IsIndex BIT NOT NULL
                          , IsConstraint BIT NOT NULL
                          , SQLFix VARCHAR(1024) NOT NULL
                        );

    INSERT INTO @Result

        SELECT '[' + s.name + ']' as SchemaName
            , '[' + o.name + ']' as TableName
            , '[' + i.name + ']' AS ConstraintName
            , CONVERT(BIT, 0) AS IsIndex
            , CONVERT(BIT, 1) AS IsContstraint
            , 'ALTER TABLE ['+o.name+'] WITH CHECK CHECK CONSTRAINT ['+i.name+']; ' AS SQLFix
        from sys.foreign_keys i
        INNER JOIN sys.objects o ON i.parent_object_id = o.object_id
        INNER JOIN sys.schemas s ON o.schema_id = s.schema_id
        WHERE i.is_not_trusted = 1

        UNION ALL
        
        SELECT '[' + s.name + ']' as SchemaName
            , '[' + o.name + ']' as TableName
            , '[' + i.name + ']' AS ConstraintName
            , CONVERT(BIT, 1) AS IsIndex
            , CONVERT(BIT, 0) AS IsContstraint
            , 'ALTER TABLE ['+o.name+'] WITH CHECK CHECK CONSTRAINT ['+i.name+']; ' AS SQLFix
        from sys.check_constraints i
        INNER JOIN sys.objects o ON i.parent_object_id = o.object_id
        INNER JOIN sys.schemas s ON o.schema_id = s.schema_id
        WHERE i.is_not_trusted = 1 AND i.is_disabled = 0;

    -- See if we have any untrusted constraints
    DECLARE @CNT INT = (SELECT COUNT(*) FROM @Result)
    IF @CNT = 0
    BEGIN
        SELECT 'No untrusted constraints to Repair.'
        RETURN;
    END
    ELSE
        -- Select Results for Output
        SELECT * FROM @Result;

    IF @Repair = 1
    BEGIN

        DECLARE @CMD NVARCHAR(MAX) = '';

        -- Concatenate the SQL to Run
        SELECT @CMD += SQLFix
        FROM @Result;

        -- Now execute it.
        EXEC(@CMD);

        SELECT CONCAT(@CNT, ' Constraints Repaired');
    END;

END;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP PROCEDURE IF EXISTS [Util.Constraints.CheckTrust];");
        }
    }
}
