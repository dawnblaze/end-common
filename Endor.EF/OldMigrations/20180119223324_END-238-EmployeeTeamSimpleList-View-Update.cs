using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180119223324_END-238-EmployeeTeamSimpleList-View-Update")]
    public partial class END238EmployeeTeamSimpleListViewUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
   ALTER VIEW[dbo].[Employee.Team.SimpleList]
        AS
   SELECT[BID]
        , [ID]
        , [ClassTypeID]
        , [Name] as DisplayName
         , [IsActive]
         , [HasImage]
         , CONVERT(BIT, 0) AS[IsDefault]
    FROM[Employee.Team]
    WHERE IsAdHocTeam = 0");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER VIEW[dbo].[Employee.Team.SimpleList]
        AS
   SELECT[BID]
        , [ID]
        , [ClassTypeID]
        , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS[HasImage]
         , CONVERT(BIT, 0) AS[IsDefault]
    FROM[Employee.Team]
    WHERE IsAdHocTeam = 0");
        }
    }
}

