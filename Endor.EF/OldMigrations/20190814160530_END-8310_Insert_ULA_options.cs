using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8310_Insert_ULA_options : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) VALUES (101, 1021, 'MULA.AcceptedDT', 'Date Time that the MULA was accepted', '', 9, '', NULL, 1);
                    INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) VALUES (101, 1022, 'EULA.AcceptedDT', 'Date Time that the EULA was accepted', '', 9, '', NULL, 1);
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    DELETE FROM [dbo].[System.Option.Definition] WHERE ID IN (1021, 1022)
                "
            );
        }
    }
}
