using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixEstimateListValuesListFilterCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.List.Filter.Criteria]
                SET 
                ListValues = 'Hide Voided,Show Voided'
                WHERE ID = 89
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
