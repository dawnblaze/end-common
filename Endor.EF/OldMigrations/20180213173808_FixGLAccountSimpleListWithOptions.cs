using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180213173808_FixGLAccountSimpleListWithOptions")]
    public partial class FixGLAccountSimpleListWithOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Accounting.GLAccount.SimpleList.WithOptions];
GO

CREATE PROCEDURE [Accounting.GLAccount.SimpleList.WithOptions]

         @BID SMALLINT
        , @ActiveFilter        BIT = 0   -- 0 = Inactive Only, 1 = Active Only, NULL = Both Active and Inactive

        , @UseNumberedName     BIT = NULL  -- 0 = Returns Name Only, 1 = Numbered Name, NULL = Use Default Option
        , @GLAccountTypeIDList VARCHAR(MAX) = NULL  -- A list of GLAccountTypes to return.  NULL returns all types.
AS
/*
    Returns a Simple List for GLAccounts that match one or more GLAccountTypes.
    The list of valid GLAccountTypes is passed in a Comma Delimited String.
    
    Sample Usage:

        EXEC [Accounting.GLAccount.SimpleList.WithOptions]
            @BID = 1
            , @GLAccountTypeIDList = '10, 11, 12, 13, 14'
            , @ActiveFilter = NULL
            , @UseNumberedName = NULL

*/
BEGIN
    -- If @UseNumberedName is not provided, look it up from the option settings
    -- ------------------------------------------------------------
    IF (@UseNumberedName IS NULL) 
    BEGIN
        DECLARE @Opt Table(OptionID INT, [Value] VARCHAR(MAX) , [OptionLevel] TINYINT);

        INSERT INTO @Opt
            EXEC [Option.GetValue] @BID = @BID, @OptionName = 'Accounting.GLAccount.ShowNumberedNames';

        SELECT TOP 1 @UseNumberedName = TRY_CONVERT(BIT, [Value])
        FROM @Opt;
    END;

    -- Handle @ActiveFilter 
    DECLARE @IncludeActive   BIT = IIF(@ActiveFilter IS NULL OR @ActiveFilter = 1, 1, 0)
          , @IncludeInActive BIT = IIF(@ActiveFilter IS NULL OR @ActiveFilter = 0, 1, 0)



    -- Return the results desired
    -- ------------------------------------------------------------
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , (CASE WHEN @UseNumberedName = 0 THEN Name ELSE NumberedName END) as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.GL.Account]
    WHERE BID = @BID
      AND (
           (@GLAccountTypeIDList IS NULL) OR
           (LEN(LTRIM(@GLAccountTypeIDList)) = 0) OR
           ([GLAccountType] in (SELECT CONVERT(INT, Value) FROM string_split( @GLAccountTypeIDList, ',' )))
          )
      AND ((IsActive & @IncludeActive) | (~IsActive & @IncludeInActive)) = 1

END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}

