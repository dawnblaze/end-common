using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Revision_SystemSubassemblyVariable_SubassemblyVariable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DisplayFormat",
                table: "[System.Part.Subassembly.Variable]",
                newName: "DisplayType");

            migrationBuilder.AddColumn<bool>(
                name: "IsDisabled",
                table: "System.Part.Subassembly.Variable",
                type: "tinyint",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDisabled",
                table: "Part.Subassembly.Variable",
                type: "tinyint",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "LinkedMaterialID",
                table: "Part.Subassembly.Variable",
                type: "int sparse",
                nullable: true
                );

            migrationBuilder.AddColumn<int>(
                name: "LinkedLaborID",
                table: "Part.Subassembly.Variable",
                type: "int sparse",
                nullable: true
                );

            migrationBuilder.AddColumn<int>(
                name: "LinkedMachineID",
                table: "Part.Subassembly.Variable",
                type: "smallint sparse",
                nullable: true
                );

            migrationBuilder.AddColumn<int>(
                name: "LinkedSubassemblyID",
                table: "Part.Subassembly.Variable",
                type: "int sparse",
                nullable: true
                );

            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Part.Subassembly.Variable] 
                DROP CONSTRAINT IF EXISTS [FK_Part.Subassembly.Variable_Part.Subassembly.Data]
                ;

                ALTER TABLE [dbo].[Part.Subassembly.Variable] 
                DROP CONSTRAINT IF EXISTS [FK_Part.Subassembly.Variable_enum.Unit]
                ;

                ALTER TABLE [dbo].[Part.Subassembly.Variable] 
                DROP CONSTRAINT IF EXISTS [FK_Part.Subassembly.Variable_enum.Part.Subassembly.LabelType]
                ;

                ALTER TABLE [dbo].[Part.Subassembly.Variable] 
                DROP CONSTRAINT IF EXISTS [FK_Part.Subassembly.Variable_enum.DataType]
                ;

                ALTER TABLE [dbo].[Part.Subassembly.Variable] 
                DROP CONSTRAINT IF EXISTS [FK_Part.Subassembly.Variable_enum.CustomField.DisplayType]
                ;                
                
                DROP INDEX IF EXISTS [IX_Part.Subassembly.Variable_DisplayType] ON [dbo].[Part.Subassembly.Variable];

                DROP INDEX IF EXISTS [IX_Part.Subassembly.Variable_LabelType] ON [dbo].[Part.Subassembly.Variable];

                DROP INDEX IF EXISTS [IX_Part.Subassembly.Variable_Subassembly] ON [dbo].[Part.Subassembly.Variable];

                DROP INDEX IF EXISTS [IX_Part.Subassembly.Variable_UnitID] ON [dbo].[Part.Subassembly.Variable];
    
                CREATE INDEX [IX_Part.Subassembly.Variable_LinkedLabor]
                ON [Part.Subassembly.Variable] ( [BID], [LinkedLaborID] )
                WHERE ([LinkedLaborID] IS NOT NULL)
                GO
 
                CREATE INDEX [IX_Part.Subassembly.Variable_LinkedMachine]
                ON [Part.Subassembly.Variable] ( [BID], [LinkedMachineID] )
                WHERE ([LinkedMachineID] IS NOT NULL)
                GO
 
                CREATE INDEX [IX_Part.Subassembly.Variable_LinkedMaterial]
                ON [Part.Subassembly.Variable] ( [BID], [LinkedMaterialID] )
                WHERE ([LinkedMaterialID] IS NOT NULL)
                GO
 
                CREATE INDEX [IX_Part.Subassembly.Variable_LinkedSA]
                ON [Part.Subassembly.Variable] ( [BID], [LinkedSubassemblyID] )
                WHERE ([LinkedSubassemblyID] IS NOT NULL)
                GO
 
                CREATE INDEX [IX_Part.Subassembly.Variable_Subassembly]
                ON [Part.Subassembly.Variable] ( [BID], [SubassemblyID], [Name] )
                GO
 
                ALTER TABLE [Part.Subassembly.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Subassembly.Variable_enum.Part.Subassembly.ElementType]
                FOREIGN KEY([ElementType]) REFERENCES [enum.Part.Subassembly.ElementType] ([ID])
                GO

                ALTER TABLE [Part.Subassembly.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Subassembly.Variable_enum.Part.Subassembly.LabelType]
                FOREIGN KEY([LabelType]) REFERENCES [enum.Part.Subassembly.LabelType] ([ID])
                GO

                ALTER TABLE [Part.Subassembly.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Subassembly.Variable_enum.Part.Subassembly.List.DataType]
                FOREIGN KEY([ListDataType]) REFERENCES [enum.Part.Subassembly.List.DataType] ([ID])
                GO

                ALTER TABLE [Part.Subassembly.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Subassembly.Variable_Part.Labor.Data]
                FOREIGN KEY([BID], [LinkedLaborID]) REFERENCES [Part.Labor.Data] ([BID], [ID])
                GO

                ALTER TABLE [Part.Subassembly.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Subassembly.Variable_Part.Machine.Data]
                FOREIGN KEY([BID], [LinkedMachineID]) REFERENCES [Part.Machine.Data] ([BID], [ID])
                GO

                ALTER TABLE [Part.Subassembly.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Subassembly.Variable_Part.Material.Data]
                FOREIGN KEY([BID], [LinkedMaterialID]) REFERENCES [Part.Material.Data] ([BID], [ID])
                GO

                ALTER TABLE [Part.Subassembly.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Subassembly.Variable_Part.Subassembly.Data]
                FOREIGN KEY([BID], [SubassemblyID]) REFERENCES [Part.Subassembly.Data] ([BID], [ID])
                GO

                ALTER TABLE [Part.Subassembly.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Subassembly.Variable_Part.Subassembly.Data1]
                FOREIGN KEY([BID], [LinkedSubassemblyID]) REFERENCES [Part.Subassembly.Data] ([BID], [ID])
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
