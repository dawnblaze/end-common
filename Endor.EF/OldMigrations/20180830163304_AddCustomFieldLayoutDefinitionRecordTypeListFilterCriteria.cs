using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180830163304_AddCustomFieldLayoutDefinitionRecordTypeListFilterCriteria")]
    public partial class AddCustomFieldLayoutDefinitionRecordTypeListFilterCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (15027 --<TargetClassTypeID, int,>
                        ,'AppliesToClassTypeIDText' --<Name, varchar(255),>
                        ,'Record Type' --<Label, varchar(255),>
                        ,'AppliesToClassTypeIDText' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,0 --<DataType, tinyint,>
                        ,12 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,'Order,Company,Contact,Employee,Location,Opportunity' --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,1 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE [dbo].[System.List.Filter.Criteria] where TargetClassTypeID=15027 and [name]='AppliesToClassTypeIDText'
            ");
        }
    }
}

