using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class partmichinedata_partassemblydata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InvoiceText",
                table: "Part.Subassembly.Data",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvoiceText",
                table: "Part.Machine.Data",
                maxLength: 255,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvoiceText",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "InvoiceText",
                table: "Part.Machine.Data");
        }
    }
}
