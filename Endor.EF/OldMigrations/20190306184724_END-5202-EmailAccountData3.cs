using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END5202EmailAccountData3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Domain.Email.SimpleList]
                GO
                CREATE VIEW [dbo].[Domain.Email.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [DomainName] as DisplayName
                    , [IsActive]
                    , CONVERT(BIT, 0) AS[HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[Domain.Email.Data];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                    DROP VIEW IF EXISTS [dbo].[Domain.Email.SimpleList]
                ");
        }
    }
}
