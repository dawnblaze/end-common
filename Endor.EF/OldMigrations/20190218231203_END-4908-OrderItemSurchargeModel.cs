using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4908OrderItemSurchargeModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Order.Item.Surcharge");

            migrationBuilder.CreateTable(
                name: "Order.Item.Surcharge",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "10024"),
                    DefaultFixedFee = table.Column<decimal>(type: "decimal(18,6)", nullable: true),
                    DefaultPerUnitFee = table.Column<decimal>(type: "decimal(18,6)", nullable: true),
                    IncomeAccountID = table.Column<int>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(7)", nullable: false, defaultValueSql: "(sysutcdatetime())"),
                    ValidToDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))"),
                    Name = table.Column<string>(type: "nvarchar(255)", nullable: false),
                    Number = table.Column<short>(nullable: false),
                    OrderItemID = table.Column<int>(nullable: false),
                    PriceNet = table.Column<decimal>(type: "decimal(18,6)", nullable: false),
                    PriceUnitOV = table.Column<bool>(nullable: false),
                    SurchargeDefID = table.Column<short>(nullable: false),
                    TaxCodeID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Item.Surcharge", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Order.Item.Surcharge_Accounting.GL.Account",
                        columns: x => new { x.BID, x.IncomeAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Item.Surcharge_Order.Item.Data",
                        columns: x => new { x.BID, x.OrderItemID },
                        principalTable: "Order.Item.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Item.Surcharge_Part.Surcharge.Data",
                        columns: x => new { x.BID, x.SurchargeDefID },
                        principalTable: "Part.Surcharge.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Item.Surcharge_Accounting.Tax.Code",
                        columns: x => new { x.BID, x.TaxCodeID },
                        principalTable: "Accounting.Tax.Code",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Surcharge_BID_IncomeAccountID",
                table: "Order.Item.Surcharge",
                columns: new[] { "BID", "IncomeAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Surcharge_BID_SurchargeDefID",
                table: "Order.Item.Surcharge",
                columns: new[] { "BID", "SurchargeDefID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Surcharge_BID_TaxCodeID",
                table: "Order.Item.Surcharge",
                columns: new[] { "BID", "TaxCodeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Surcharge_LineItem",
                table: "Order.Item.Surcharge",
                columns: new[] { "BID", "OrderItemID", "Number" });

            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Item.Surcharge]
                ADD PERIOD FOR SYSTEM_TIME([ModifiedDT], [ValidToDT])
                GO
                ");

            migrationBuilder.EnableSystemVersioning("Order.Item.Surcharge");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Order.Item.Surcharge");
        }
    }
}
