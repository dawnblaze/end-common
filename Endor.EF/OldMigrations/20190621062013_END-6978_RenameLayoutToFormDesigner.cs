using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6978_RenameLayoutToFormDesigner : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.Option.Category]
                SET 
                [Name] = 'Custom Field Form Designers',
                [Description] = 'Custom Field Form Designers',
                [SearchTerms] = 'Custom Field Form Designers User Defined'
                WHERE ID = 203;

            UPDATE [System.Option.Section]
                SET 
                [SearchTerms] = 'Employee Roles Salesperson Designer Job Function Custom Field Form Designers User Defined Custom Field Definitions User Defined Input Entry'
                WHERE ID = 200
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
