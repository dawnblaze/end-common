using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180501152110_CREATE_Order_Item_Status")]
    public partial class CREATE_Order_Item_Status : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

                DROP TABLE IF EXISTS [dbo].[Order.Item.Status]
                GO
                CREATE TABLE [Order.Item.Status](
                    [BID] [smallint] NOT NULL,
                    [ID] [smallint] NOT NULL,
                    [ClassTypeID]  AS ((10022)),
                    [ModifiedDT] [datetime2](7)NOT NULL,
                    [IsActive] [bit] NOT NULL,
                    [IsSystem] [bit] NOT NULL,
                    [IsDefault] [bit] NOT NULL,
                    [TransactionType] [tinyint] NOT NULL,
                    [OrderStatusID] [tinyint] NULL,
                    [StatusIndex] [tinyint] NOT NULL DEFAULT (50),
                    [Name] [nvarchar](100) NOT NULL,
                    [CustomerPortalText] [nvarchar](255) NULL,
                    [Description] [nvarchar](255) NULL,
                    [TransactionTypeText]  AS (case [TransactionType] when (1) then 'Estimate' when (2) then 'Order' when (4) then 'PO' when (32) then 'Opportunityr' when (64) then 'Destination' else 'Unknown' end),
                CONSTRAINT [PK_Order.Item.Status] PRIMARY KEY ( [BID], [ID] )
                );
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP TABLE IF EXISTS [dbo].[Order.Item.Status]
                GO
            ");
        }
    }
}

