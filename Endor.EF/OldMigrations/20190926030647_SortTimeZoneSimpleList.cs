﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class SortTimeZoneSimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER VIEW [dbo].[enum.TimeZone.SimpleList] AS
    SELECT TOP 100 PERCENT
          [ID]
        , [StandardName] as DisplayName
        , [IsCommon] as [IsActive]
        , CONVERT(BIT, 0) AS [HasImage]
    FROM [enum.TimeZone] order by UTCOffset
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER VIEW [dbo].[enum.TimeZone.SimpleList] AS
    SELECT 
          [ID]
        , [Name] as DisplayName
        , [IsCommon] as [IsActive]
        , CONVERT(BIT, 0) AS [HasImage]
    FROM [enum.TimeZone]
");
        }
    }
}
