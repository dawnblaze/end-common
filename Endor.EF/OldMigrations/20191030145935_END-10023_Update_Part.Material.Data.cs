﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10023_Update_PartMaterialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Volume",
                table: "Part.Material.Data",
                type: "decimal(18,4)",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "VolumeUnit",
                table: "Part.Material.Data",
                type: "tinyint",
                nullable: true);

            migrationBuilder.Sql(@"
UPDATE [Part.Material.Data]
SET
	HeightUnit = CASE 
					-- Inch
					WHEN HeightUnit = 21 THEN 31
					-- Centimeter
					WHEN HeightUnit = 22 THEN 37
					-- Foot
					WHEN HeightUnit = 23 THEN 33
					-- Meter
					WHEN HeightUnit = 24 THEN 38
					-- Millimeter
					WHEN HeightUnit = 25 THEN 36
					-- Yard
					WHEN HeightUnit = 26 THEN 34
					-- Kilogram
					WHEN HeightUnit = 32 THEN 78
					-- Pound
					WHEN HeightUnit = 35 THEN 72
					-- Hour
					WHEN HeightUnit = 42 THEN 113
					-- SquareInch
					WHEN HeightUnit = 121 THEN 41
					-- SquareCentimeter
					WHEN HeightUnit = 122 THEN 47
					-- SquareFoot
					WHEN HeightUnit = 123 THEN 43
					-- SquareMeter
					WHEN HeightUnit = 124 THEN 48
					-- SquareMillimeter
					WHEN HeightUnit = 125 THEN 46
					-- SquareYard
					WHEN HeightUnit = 126 THEN 44
					ELSE
						HeightUnit
					END,
	WidthUnit = CASE 
					-- Inch
					WHEN WidthUnit = 21 THEN 31
					-- Centimeter
					WHEN WidthUnit = 22 THEN 37
					-- Foot
					WHEN WidthUnit = 23 THEN 33
					-- Meter
					WHEN WidthUnit = 24 THEN 38
					-- Millimeter
					WHEN WidthUnit = 25 THEN 36
					-- Yard
					WHEN WidthUnit = 26 THEN 34
					-- Kilogram
					WHEN WidthUnit = 32 THEN 78
					-- Pound
					WHEN WidthUnit = 35 THEN 72
					-- Hour
					WHEN WidthUnit = 42 THEN 113
					-- SquareInch
					WHEN WidthUnit = 121 THEN 41
					-- SquareCentimeter
					WHEN WidthUnit = 122 THEN 47
					-- SquareFoot
					WHEN WidthUnit = 123 THEN 43
					-- SquareMeter
					WHEN WidthUnit = 124 THEN 48
					-- SquareMillimeter
					WHEN WidthUnit = 125 THEN 46
					-- SquareYard
					WHEN WidthUnit = 126 THEN 44
					ELSE
						WidthUnit
					END,
	WeightUnit = CASE 
					-- Inch
					WHEN WeightUnit = 21 THEN 31
					-- Centimeter
					WHEN WeightUnit = 22 THEN 37
					-- Foot
					WHEN WeightUnit = 23 THEN 33
					-- Meter
					WHEN WeightUnit = 24 THEN 38
					-- Millimeter
					WHEN WeightUnit = 25 THEN 36
					-- Yard
					WHEN WeightUnit = 26 THEN 34
					-- Kilogram
					WHEN WeightUnit = 32 THEN 78
					-- Pound
					WHEN WeightUnit = 35 THEN 72
					-- Hour
					WHEN WeightUnit = 42 THEN 113
					-- SquareInch
					WHEN WeightUnit = 121 THEN 41
					-- SquareCentimeter
					WHEN WeightUnit = 122 THEN 47
					-- SquareFoot
					WHEN WeightUnit = 123 THEN 43
					-- SquareMeter
					WHEN WeightUnit = 124 THEN 48
					-- SquareMillimeter
					WHEN WeightUnit = 125 THEN 46
					-- SquareYard
					WHEN WeightUnit = 126 THEN 44
					ELSE
						WeightUnit
					END,
	LengthUnit = CASE 
					-- Inch
					WHEN LengthUnit = 21 THEN 31
					-- Centimeter
					WHEN LengthUnit = 22 THEN 37
					-- Foot
					WHEN LengthUnit = 23 THEN 33
					-- Meter
					WHEN LengthUnit = 24 THEN 38
					-- Millimeter
					WHEN LengthUnit = 25 THEN 36
					-- Yard
					WHEN LengthUnit = 26 THEN 34
					-- Kilogram
					WHEN LengthUnit = 32 THEN 78
					-- Pound
					WHEN LengthUnit = 35 THEN 72
					-- Hour
					WHEN LengthUnit = 42 THEN 113
					-- SquareInch
					WHEN LengthUnit = 121 THEN 41
					-- SquareCentimeter
					WHEN LengthUnit = 122 THEN 47
					-- SquareFoot
					WHEN LengthUnit = 123 THEN 43
					-- SquareMeter
					WHEN LengthUnit = 124 THEN 48
					-- SquareMillimeter
					WHEN LengthUnit = 125 THEN 46
					-- SquareYard
					WHEN LengthUnit = 126 THEN 44
					ELSE
						LengthUnit
					END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Volume",
                table: "Part.Material.Data");

            migrationBuilder.DropColumn(
                name: "VolumeUnit",
                table: "Part.Material.Data");
        }
    }
}
