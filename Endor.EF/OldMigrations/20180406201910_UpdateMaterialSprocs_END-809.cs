using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180406201910_UpdateMaterialSprocs_END-809")]
    public partial class UpdateMaterialSprocs_END809 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Part.Material.Data.Action.CanDelete]

    Description: This procedure checks if the Material is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Material.Data.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

ALTER PROCEDURE [dbo].[Part.Material.Data.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the Material can be deleted. The boolean response is returned in the body.  A Material can be deleted if
	It is not linked to any Material Categories.
	(future) It is not in use in any Product Template.
	(future) Is it not in use in any Order or Estimate.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Material.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Material Specified Does not Exist. MaterialID=', @ID)

    -- It is not linked to any Material Categories.
	ELSE IF EXISTS( SELECT * FROM [Part.Material.CategoryLink] WHERE BID = @BID AND PartID = @ID )

	SELECT @Result = 0
		 	, @CantDeleteReason = CONCAT('Material is linked to a Material Category. MaterialID=', @ID)

	-- (future) It is not in use in any Product Template.
	-- (future) Is it not in use in any Order or Estimate.
	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Part.Material.Data.Action.CanDelete]

    Description: This procedure checks if the Material is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Material.Data.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

ALTER PROCEDURE [dbo].[Part.Material.Data.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the Material can be deleted. The boolean response is returned in the body.  A Material can be deleted if
	It is not linked to any Material Categories.
	(future) It is not in use in any Product Template.
	(future) Is it not in use in any Order or Estimate.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Material.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Material Specified Does not Exist. MaterialID=', @ID)

    -- It is not linked to any Material Categories.
		--ELSE IF EXISTS( SELECT * FROM [Part.Material.CategoryLink] WHERE BID = @BID AND PartID = @ID )

		--SELECT @Result = 0
		--	, @CantDeleteReason = CONCAT('Material is linked to a Material Category. MaterialID=', @ID)

	-- (future) It is not in use in any Product Template.
	-- (future) Is it not in use in any Order or Estimate.
	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
            ");
        }
    }
}

