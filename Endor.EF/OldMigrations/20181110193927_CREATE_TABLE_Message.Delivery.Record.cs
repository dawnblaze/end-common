using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CREATE_TABLE_MessageDeliveryRecord : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
               name: "Message.Delivery.Record",
               columns: table => new
               {
                   BID = table.Column<short>(nullable: false),
                   ParticipantID = table.Column<byte>(type: "int", nullable: false, defaultValue: (byte)0),
                   AttempNumber = table.Column<byte>(type: "smallint", nullable: false, defaultValue: (byte)0),
                   ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14114))"),
                   IsPending = table.Column<int>(nullable: false, computedColumnSql: "(case when [ScheduledDT] IS NULL OR AttemptDT IS NOT NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)"),
                   ScheduledDT = table.Column<DateTime>(type: "datetime2(2) sparse", nullable: true),
                   AttemptDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                   WasSuccessful = table.Column<bool>(nullable: false),
                   FailureMessage = table.Column<string>(type: "nvarchar(max) sparse", nullable: true),
                   MetaData = table.Column<string>(type: "xml sparse", nullable: true),
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_Message.Delivery.Record", x => new { x.BID, x.ParticipantID, x.AttempNumber });
               }
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Delivery.Record_Message.Participant",
                table: "Message.Delivery.Record",
                columns: new[] { "BID", "ParticipantID" },
                principalTable: "Message.Participant.Info",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Delivery.Record_Pending",
                table: "Message.Delivery.Record",
                columns: new[] { "BID", "IsPending", "ScheduledDT" });
        }

    

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message.Delivery.Record_Message.Participant",
                table: "Message.Delivery.Record");

            migrationBuilder.DropIndex(
                name: "IX_Message.Delivery.Record_Pending",
                table: "Message.Delivery.Record");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Delivery.Record",
                table: "Message.Delivery.Record");

            migrationBuilder.DropTable(
                name: "Message.Delivery.Record");

        }
    }
}
