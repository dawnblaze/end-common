using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180809174319_AddSystemAutomationTriggerDefinitions")]
    public partial class AddSystemAutomationTriggerDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Automation.Trigger.CategoryType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Automation.Trigger.CategoryType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "System.Automation.Trigger.Definition",
                columns: table => new
                {
                    ID = table.Column<short>(type: "smallint", nullable: false),
                    AppliesToAlerts = table.Column<bool>(nullable: false),
                    AppliesToAutomations = table.Column<bool>(nullable: false),
                    ConditionDataType = table.Column<short>(type: "smallint SPARSE", nullable: true),
                    DataType = table.Column<short>(type: "smallint", nullable: false),
                    HasCondition = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    TriggerCategoryType = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Automation.Trigger.Definition", x => x.ID);
                    table.ForeignKey(
                        name: "FK_System.Automation.Trigger.Definition_enum.Automation.Trigger.CategoryType",
                        column: x => x.TriggerCategoryType,
                        principalTable: "enum.Automation.Trigger.CategoryType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_System.Automation.Trigger.Definition_TriggerCategoryType",
                table: "System.Automation.Trigger.Definition",
                column: "TriggerCategoryType");

            migrationBuilder.Sql(@"
INSERT [enum.Automation.Trigger.CategoryType] ([ID], [Name])
VALUES (0, N'None')
    , (1, N'Estimate')
    , (2, N'Order')
    , (3, N'Purchase Order')
    , (4, N'Order/Estimate Line Item')
    , (6, N'Order Destination')
    , (8, N'Payment')
    , (9, N'Proof File')
    , (10, N'Opportunity')
    , (11, N'Company')
;
");

            migrationBuilder.Sql(@"
INSERT [System.Automation.Trigger.Definition] ([ID], [TriggerCategoryType], [Name], [DataType], [HasCondition], [AppliesToAutomations], [AppliesToAlerts])
VALUES (1, 11, N'Created', 2000, 0, 1, 1)
    , (2, 11, N'Edited', 2000, 0, 1, 1)
    , (3, 1, N'Created', 10000, 0, 1, 1)
    , (4, 1, N'Edited', 10000, 0, 1, 1)
    , (5, 1, N'Converted', 10000, 0, 1, 1)
    , (6, 1, N'Cancelled', 10000, 0, 1, 1)
    , (7, 2, N'Created', 10000, 0, 1, 1)
    , (8, 2, N'Edited', 10000, 0, 1, 1)
    , (9, 2, N'Voided', 10000, 0, 1, 1)
    , (10, 9, N'Uploaded', 10025, 0, 1, 1)
    , (11, 9, N'Approved', 10025, 0, 1, 1)
    , (12, 9, N'Change Request', 10025, 0, 1, 1)
    , (13, 9, N'Uploaded by Customer', 10025, 0, 1, 1)
    , (14, 2, N'Due Date Changed', 10000, 0, 1, 1)
    , (15, 4, N'Design Date Changed', 10020, 0, 1, 1)
    , (16, 4, N'Due Date Changed', 10020, 0, 1, 1)
    , (17, 2, N'Status Changed', 10000, 0, 1, 1)
    , (18, 4, N'Line Item Status Changed', 10020, 0, 1, 1)
    , (19, 2, N'Sales Notes Added', 10000, 0, 1, 1)
    , (20, 4, N'Design Notes Added', 10020, 0, 1, 1)
    , (21, 4, N'Production Notes Added', 10020, 0, 1, 1)
    , (22, 11, N'Customer Notes Added by Customer', 2000, 0, 1, 1)
    , (23, 11, N'Customer Notes Added', 2000, 0, 1, 1)
    , (24, 11, N'Vendor Notes Added', 2000, 0, 1, 1)
    , (25, 8, N'Posted', 8010, 0, 1, 1)
    , (26, 8, N'Posted by Customer', 8010, 0, 1, 1)
    , (27, 2, N'Closed', 10000, 0, 1, 1)
    , (28, 9, N'Uploaded by Employee', 10025, 0, 1, 1)
    , (50, 11, N'Billing Contact Changed', 2000, 0, 1, 1)
    , (51, 11, N'Company Name Changed', 2000, 0, 1, 1)
    , (52, 11, N'Credit Account Level Increased', 2000, 0, 1, 1)
    , (53, 11, N'Credit Given', 2000, 0, 1, 1)
    , (54, 11, N'Custom Field Edited', 2000, 0, 1, 1)
    , (55, 11, N'Customer Notes Added by Employee', 2000, 0, 1, 1)
    , (56, 11, N'Primary Contact Changed', 2000, 0, 1, 1)
    , (57, 11, N'Salesperson Changed', 2000, 0, 1, 1)
    , (58, 1, N'Custom Field Edited', 10000, 0, 1, 1)
    , (59, 1, N'Lost', 10000, 0, 1, 1)
    , (60, 1, N'Status Changed', 10000, 0, 1, 1)
    , (61, 1, N'Status Changed To �', 10000, 1, 1, 1)
    , (62, 1, N'Total Price Changed', 10000, 0, 1, 1)
    , (63, 10, N'Created', 9000, 0, 1, 1)
    , (64, 10, N'Custom Field Edited', 9000, 0, 1, 1)
    , (65, 10, N'Edited', 9000, 0, 1, 1)
    , (66, 10, N'Estimate Created', 9000, 0, 1, 1)
    , (67, 10, N'Order Created', 9000, 0, 1, 1)
    , (68, 10, N'Status Changed', 9000, 0, 1, 1)
    , (69, 10, N'Status Changed To �', 9000, 1, 1, 1)
    , (70, 10, N'Total Price Changed', 9000, 0, 1, 1)
    , (71, 2, N'Built', 10000, 0, 1, 1)
    , (72, 2, N'Custom Field Edited', 10000, 0, 1, 1)
    , (73, 2, N'In Invoicing', 10000, 0, 1, 1)
    , (74, 2, N'in Pre-WIP', 10000, 0, 1, 1)
    , (75, 2, N'in WIP', 10000, 0, 1, 1)
    , (76, 2, N'Invoiced', 10000, 0, 1, 1)
    , (77, 2, N'Status Changed To �', 10000, 1, 1, 1)
    , (78, 2, N'Total Price Changed', 10000, 0, 1, 1)
    , (79, 6, N'Added to Order', 10040, 0, 1, 1)
    , (80, 6, N'Complete', 10040, 0, 1, 1)
    , (81, 6, N'Pending', 10040, 0, 1, 1)
    , (82, 6, N'Ready', 10040, 0, 1, 1)
    , (83, 6, N'Shipment Delivered', 10040, 0, 1, 1)
    , (84, 6, N'Shipment Delivery Problem', 10040, 0, 1, 1)
    , (85, 6, N'Shipment Pickedup By Carrier', 10040, 0, 1, 1)
    , (86, 4, N'Added to Order', 10020, 0, 1, 1)
    , (87, 4, N'Assigned To Changed', 10020, 0, 1, 1)
    , (88, 4, N'Deleted from Order', 10020, 0, 1, 1)
    , (89, 4, N'Line Item Price Changed', 10020, 0, 1, 1)
    , (90, 4, N'Line Item Status Change To �', 10020, 1, 1, 1)
    , (91, 8, N'Paid with �', 8010, 1, 1, 1)
    , (92, 8, N'Paid with Customer Credit', 8010, 0, 1, 1)
    , (93, 8, N'Posted by Employee', 8010, 0, 1, 1)
    , (94, 8, N'Refunded', 8010, 0, 1, 1)
    , (95, 8, N'Refunded to Credit Card', 8010, 0, 1, 1)
    , (96, 8, N'Voided', 8010, 0, 1, 1)
    , (97, 9, N'Approved by Customer', 10025, 0, 1, 1)
    , (98, 9, N'Approved by Employee', 10025, 0, 1, 1)
    , (99, 3, N'Approved', 10000, 0, 1, 1)
    , (100, 3, N'Closed', 10000, 0, 1, 1)
    , (101, 3, N'Created', 10000, 0, 1, 1)
    , (102, 3, N'Custom Field Edited', 10000, 0, 1, 1)
    , (103, 3, N'Edited', 10000, 0, 1, 1)
    , (104, 3, N'Ordered', 10000, 0, 1, 1)
    , (105, 3, N'Received', 10000, 0, 1, 1)
    , (106, 3, N'Status Changed', 10000, 0, 1, 1)
    , (107, 3, N'Status Changed To �', 10000, 1, 1, 1)
    , (108, 3, N'Total Price Changed', 10000, 0, 1, 1)
    , (109, 3, N'Voided', 10000, 0, 1, 1)
    , (110, 3, N'Voided', 10000, 0, 1, 1)
;
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "System.Automation.Trigger.Definition");

            migrationBuilder.DropTable(
                name: "enum.Automation.Trigger.CategoryType");
        }
    }
}

