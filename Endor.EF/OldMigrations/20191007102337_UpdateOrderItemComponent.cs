﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class UpdateOrderItemComponent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Order.Item.Component] SET (SYSTEM_VERSIONING = OFF);
");
            migrationBuilder.Sql(@"
DROP TABLE [Historic.Order.Item.Component];
");

            migrationBuilder.DropColumn(
                name: "Price.Net",
                table: "Order.Item.Component");

            migrationBuilder.AddColumn<decimal>(
                name: "Price.PreTax",
                table: "Order.Item.Component",
                type: "DECIMAL(18,2)",
                nullable: true,
                computedColumnSql: "([Quantity] * [Price.Unit])");

            migrationBuilder.AddColumn<decimal>(
                name: "Price.Tax",
                table: "Order.Item.Component",
                type: "DECIMAL(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price.Taxable",
                table: "Order.Item.Component",
                type: "DECIMAL(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price.Total",
                table: "Order.Item.Component",
                type: "DECIMAL(18,2)",
                nullable: true,
                computedColumnSql: "(([Price.Unit]*[Quantity]) + [Price.Tax])");

            migrationBuilder.Sql(@"
ALTER TABLE [Order.Item.Component] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Item.Component]));
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price.Tax",
                table: "Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "Price.Taxable",
                table: "Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "Price.Total",
                table: "Order.Item.Component");

            migrationBuilder.RenameColumn(
                name: "Price.PreTax",
                table: "Order.Item.Component",
                newName: "Price.Net");
        }
    }
}
