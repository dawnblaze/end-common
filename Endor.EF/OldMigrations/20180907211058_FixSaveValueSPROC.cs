using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180907211058_FixSaveValueSPROC")]
    public partial class FixSaveValueSPROC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
--
--Name: [Option.SaveValue]
--
-- Description: This procedure saves the setting for an option/setting.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.SaveValue] 
        -- Require Fields
          @OptionName       = 'GLAccount.TaxName2'
        , @OptionID         = NULL
        , @Value            = 'NewValue'  -- Pass in NULL to delete the current value (if any)

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @EmployeeID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

    SELECT * FROM [Option.Data]
*/
-- ========================================================

ALTER PROCEDURE[dbo].[Option.SaveValue]
-- DECLARE
            @OptionID       INT          = NULL
          , @OptionName VARCHAR(255) = NULL
          , @Value VARCHAR(MAX) = NULL     -- Pass in NULL to delete the current value(if any)

          , @AssociationID TINYINT = NULL
          , @BID            SMALLINT     = NULL
          , @LocationID SMALLINT = NULL
          , @EmployeeID     SMALLINT     = NULL
          , @CompanyID INT = NULL
          , @ContactID      INT          = NULL
          , @StorefrontID SMALLINT = NULL

AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF((@OptionName IS NULL) AND(@OptionID IS NULL)) OR((@OptionName IS NOT NULL) AND(@OptionID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @OptionID or the @OptionName.', 1;

    IF((@BID IS NULL) AND(@AssociationID IS NULL)) OR((@BID IS NOT NULL) AND(@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID IS NULL, 0, 1)
                                    + IIF(@EmployeeID IS NULL, 0, 1)
                                    + IIF(@CompanyID IS NULL, 0, 1)
                                    + IIF(@ContactID IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

        IF(@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    IF(@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================
    IF(@OptionID IS NULL)
    BEGIN
        SELECT @OptionID = ID
        FROM[System.Option.Definition]
        WHERE Name = @OptionName;

        IF(@OptionID IS NULL)
        BEGIN
            -- If not defined, and NULL, there will be nothing to delete
            -- -------------------------------------------------
            IF(@Value IS NULL)
                RETURN;

            PRINT 'create new - check parameters, save and return id'

			DECLARE @NewID INT
            EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, 1;
			SET @OptionID = @NewID

            INSERT INTO[System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
           SELECT @OptionID AS ID
                 , @OptionName AS Name
                 , 'Custom: '+@OptionName AS Label
                 , NULL AS DefaultValue
                 , 0 AS DataType -- Always string
                 , 10100 AS CategoryID  -- AdHoc Options
                 , 1 AS IsHidden
            ;

        PRINT @OptionID
        END
    END

    -- ======================================
    -- Lookup Current Value if it Exists
    -- ======================================
    DECLARE @InstanceID INT = 

        CASE
            WHEN @AssociationID IS NOT NULL
            THEN(SELECT ID FROM[Option.Data] WHERE OptionID = @OptionID AND AssociationID = @AssociationID)

            WHEN @LocationID IS NOT NULL
            THEN(SELECT ID FROM[Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND LocationID = @LocationID)

            WHEN @EmployeeID IS NOT NULL
            THEN(SELECT ID FROM[Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND EmployeeID = @EmployeeID)

            WHEN @CompanyID  IS NOT NULL
            THEN(SELECT ID FROM[Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND CompanyID = @CompanyID)

            WHEN @ContactID  IS NOT NULL
            THEN(SELECT ID FROM[Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND ContactID = @ContactID)

            WHEN @StorefrontID IS NOT NULL
            THEN(SELECT ID FROM[Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND StorefrontID = @StorefrontID)

            WHEN @BID        IS NOT NULL
            THEN(SELECT ID FROM[Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND COALESCE(LocationID, EmployeeID, CompanyID, ContactID, StorefrontID) IS NULL )

        ELSE NULL
        END;

    -- ======================================
    -- Now Save(Update or Create) Value
    -- ======================================
    IF(@Value IS NOT NULL)
    BEGIN
        IF(@InstanceID IS NOT NULL)
            UPDATE[Option.Data]
            SET ModifiedDT = GetUTCDate()
            , Value = @Value
            WHERE ID = @InstanceID

        ELSE
            INSERT INTO[Option.Data]
                (CreatedDate, ModifiedDT, IsActive, OptionID
                , [Value], AssociationID, BID, LocationID
                , StoreFrontID, EmployeeID, CompanyID, ContactID
                )
            VALUES
                (GetUTCDate(), GetUTCDate(), 1, @OptionID
                , @Value, @AssociationID, @BID, @LocationID
                , @StoreFrontID, @EmployeeID, @CompanyID, @ContactID
                );
        END

    -- ======================================
    -- Else Delete the Options with NULL Values Passed in
    -- ======================================
    ELSE
        DELETE FROM[Option.Data]
        WHERE ID = @InstanceID



END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

