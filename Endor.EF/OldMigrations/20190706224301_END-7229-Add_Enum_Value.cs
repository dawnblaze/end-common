using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END7229Add_Enum_Value : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                Insert [enum.Part.Subassembly.ElementType] ([ID],[Name])
                Values
                        ( 108, N'LayoutComputationAssembly')
                ;
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE [enum.Part.Subassembly.ElementType] WHERE [ID]=108;");
            
        }
    }
}
