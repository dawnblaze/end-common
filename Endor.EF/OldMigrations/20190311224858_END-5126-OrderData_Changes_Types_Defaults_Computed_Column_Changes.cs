using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END5126OrderData_Changes_Types_Defaults_Computed_Column_Changes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Disable System Versioning so that migrations can take place
            migrationBuilder.Sql
            (@"
                IF(
                (
                    SELECT temporal_type
                    FROM   sys.tables
                    WHERE  object_id = OBJECT_ID('dbo.[Order.Data]', 'u')
                ) > 0)
                BEGIN
                   ALTER TABLE[Order.Data] SET(SYSTEM_VERSIONING = OFF); 
                END
            ");

            migrationBuilder.DropColumn(
                name: "VersionText",
                table: "Order.Data");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "Order.Data");

            migrationBuilder.AlterColumn<short>(
                name: "Priority",
                table: "Order.Data",
                type: "smallint SPARSE",
                nullable: true,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Price.TaxRate",
                table: "Order.Data",
                type: "DECIMAL(9,4)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)");

            migrationBuilder.AlterColumn<bool>(
                name: "Price.IsLocked",
                table: "Order.Data",
                nullable: false,
                defaultValueSql: "((0))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<decimal>(
                name: "Price.DiscountPercent",
                table: "Order.Data",
                type: "DECIMAL(9,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(9,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Payment.Authorized",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.DropDefaultConstraintIfExists("Order.Data", "OrderStatusStartDT");

            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderStatusStartDT",
                table: "Order.Data",
                type: "DATETIME2(2)",
                nullable: false,
                defaultValueSql: "GetUTCDate()",
                oldClrType: typeof(DateTime),
                oldType: "DATETIME2(2)",
                oldDefaultValueSql: "SYSUTCDATETIME()");

            migrationBuilder.AlterColumn<string>(
                name: "OrderPONumber",
                table: "Order.Data",
                type: "varchar(32) SPARSE",
                unicode: false,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "ItemCount",
                table: "Order.Data",
                type: "smallint SPARSE",
                nullable: true,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsUrgent",
                table: "Order.Data",
                type: "bit SPARSE",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "InvoiceNumber",
                table: "Order.Data",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.DropColumn("Cost.Total", "Order.Data");
            migrationBuilder.DropColumn("CreditMemo.Balance", "Order.Data");
            migrationBuilder.DropColumn("CreditMemo.HasBalance", "Order.Data");
            migrationBuilder.DropColumn("Price.Net", "Order.Data");
            migrationBuilder.DropColumn("Price.PreTax", "Order.Data");
            migrationBuilder.DropColumn("Price.Total", "Order.Data");
            migrationBuilder.DropColumn("Payment.BalanceDue", "Order.Data");
            migrationBuilder.DropColumn("Payment.Total", "Order.Data");

            /*Columns used in computed columns*/

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Other",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Material",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Machine",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Labor",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CreditMemo.Applied",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CreditMemo.Used",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CreditMemo.Credit",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Price.FinanceCharge",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Payment.WriteOff",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            /*Columns used in computed columns*/
            migrationBuilder.AddColumn<decimal>(
                name: "Payment.Total",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: false,
                computedColumnSql:
                "(isnull([Payment.Paid] + isnull([Payment.WriteOff],(0)) + IsNull([CreditMemo.Applied],0.0) ,(0.0)))");
            migrationBuilder.AddColumn<decimal>(
                name: "Cost.Total",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: false,
                computedColumnSql:
                "(isnull(((isnull([Cost.Material],(0.0))+isnull([Cost.Labor],(0.0)))+isnull([Cost.Machine],(0.0)))+isnull([Cost.Other],(0.0)),(0.0)))");

            migrationBuilder.AddColumn<decimal>(
                name: "CreditMemo.Balance",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                computedColumnSql: "([CreditMemo.Credit]-coalesce([CreditMemo.Used],(0.0)))");
            migrationBuilder.AddColumn<bool>(
                name: "CreditMemo.HasBalance",
                table: "Order.Data",
                type: "BIT PERSISTED",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([bit], abs(sign(coalesce( [CreditMemo.Credit], (0)) - coalesce([CreditMemo.Used], (0))))),(0)))");
            migrationBuilder.AddColumn<decimal>(
                name: "Price.Net",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                computedColumnSql:
                "[Price.ProductTotal] " +
                "+ IsNull([Price.DestinationTotal],(0)) " +
                "+ IsNull([Price.FinanceCharge],(0))");
            migrationBuilder.AddColumn<decimal>(
                name: "Price.PreTax",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                computedColumnSql: 
                "[Price.ProductTotal] " +
                "+ IsNull([Price.DestinationTotal],(0)) " +
                "+ IsNull([Price.FinanceCharge],(0)) " +
                "- IsNull([Price.Discount],0)");
            migrationBuilder.AddColumn<decimal>(
                name: "Price.Total",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                computedColumnSql:
                "[Price.ProductTotal] " +
                "+ IsNull([Price.DestinationTotal],(0)) " +
                "+ IsNull([Price.FinanceCharge],(0)) " +
                "- IsNull([Price.Discount],0) " +
                "+ [Price.Tax]");
            migrationBuilder.AddColumn<decimal>(
                name: "Payment.BalanceDue",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                computedColumnSql: 
                "((((((((isnull([Price.ProductTotal],(0)) " +
                "- isnull([Price.Discount], (0))) " +
                "+ isnull([Price.DestinationTotal], (0))) " +
                "+ isnull([Price.FinanceCharge], (0))) " +
                "+ isnull([Price.Tax],(0))) " +
                "- isnull([Payment.Paid],(0))) " +
                "- isnull([Payment.WriteOff],(0))) " +
                "- isnull([CreditMemo.Applied], (0))) " +
                "+ isnull([CreditMemo.Credit],(0)))");


            //Turn System Versioning back on
            //Start by dropping old history table
            migrationBuilder.Sql("DROP TABLE IF EXISTS dbo.[Historic.Order.Data]");
            migrationBuilder.Sql
            (@"
                ALTER TABLE[Order.Data] SET(SYSTEM_VERSIONING = ON(HISTORY_TABLE = dbo.[Historic.Order.Data]));"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //Disable System Versioning so that migrations can take place
            migrationBuilder.Sql
            (@"
                IF(
                (
                    SELECT temporal_type
                    FROM   sys.tables
                    WHERE  object_id = OBJECT_ID('dbo.[Order.Data]', 'u')
                ) > 0)
                BEGIN
                   ALTER TABLE[Order.Data] SET(SYSTEM_VERSIONING = OFF); 
                END
            ");

            migrationBuilder.AlterColumn<short>(
                name: "Priority",
                table: "Order.Data",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Price.TaxRate",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(9,4)");

            migrationBuilder.AlterColumn<bool>(
                name: "Price.IsLocked",
                table: "Order.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((0))");

            migrationBuilder.AlterColumn<decimal>(
                name: "Price.DiscountPercent",
                table: "Order.Data",
                type: "DECIMAL(9,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(9,4) SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Payment.Authorized",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);

            migrationBuilder.DropDefaultConstraintIfExists("Order.Data", "OrderStatusStartDT");

            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderStatusStartDT",
                table: "Order.Data",
                type: "DATETIME2(2)",
                nullable: false,
                defaultValueSql: "SYSUTCDATETIME()",
                oldClrType: typeof(DateTime),
                oldType: "DATETIME2(2)",
                oldDefaultValueSql: "GetUTCDate()");

            migrationBuilder.AlterColumn<string>(
                name: "OrderPONumber",
                table: "Order.Data",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(32) SPARSE",
                oldUnicode: false,
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "ItemCount",
                table: "Order.Data",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsUrgent",
                table: "Order.Data",
                nullable: true,
                oldClrType: typeof(bool),
                oldType: "bit SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "InvoiceNumber",
                table: "Order.Data",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Other",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Material",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Machine",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Labor",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);
            migrationBuilder.DropColumn("CreditMemo.Balance", "Order.Data");
            migrationBuilder.DropColumn("CreditMemo.HasBalance", "Order.Data");
            migrationBuilder.DropColumn("Price.Net", "Order.Data");
            migrationBuilder.DropColumn("Price.PreTax", "Order.Data");
            migrationBuilder.DropColumn("Price.Total", "Order.Data");
            migrationBuilder.DropColumn("Payment.BalanceDue", "Order.Data");

            /*Columns used in computed columns*/

            migrationBuilder.AlterColumn<decimal>(
                name: "CreditMemo.Applied",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CreditMemo.Used",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CreditMemo.Credit",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Price.FinanceCharge",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Payment.WriteOff",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4) SPARSE",
                oldNullable: true);
            /*Columns used in computed columns*/
            migrationBuilder.AddColumn<decimal>(
                name: "CreditMemo.Balance",
                table: "Order.Data",
                type: "DECIMAL(18,4) SPARSE",
                nullable: true,
                computedColumnSql: "([CreditMemo.Credit]-coalesce([CreditMemo.Used],(0.0)))");
            migrationBuilder.AddColumn<bool>(
                name: "CreditMemo.HasBalance",
                table: "Order.Data",
                type: "BIT PERSISTED",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([bit], abs(sign(coalesce( [CreditMemo.Credit], (0)) - coalesce([CreditMemo.Used], (0))))),(0)))");
            migrationBuilder.AddColumn<decimal>(
                name: "Price.Net",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                computedColumnSql: 
                "[Price.ProductTotal] " +
                "+ IsNull([Price.DestinationTotal],(0)) " +
                "+ IsNull([Price.FinanceCharge],(0))");
            migrationBuilder.AddColumn<decimal>(
                name: "Price.PreTax",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                computedColumnSql: 
                "[Price.ProductTotal] " +
                "+ IsNull([Price.DestinationTotal],(0)) " +
                "+ IsNull([Price.FinanceCharge],(0)) " +
                "- IsNull([Price.Discount],0)");
            migrationBuilder.AddColumn<decimal>(
                name: "Price.Total",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                computedColumnSql: "[Price.ProductTotal] + IsNull([Price.DestinationTotal],(0)) + IsNull([Price.FinanceCharge],(0)) - IsNull([Price.Discount],0) + [Price.Tax]");
            migrationBuilder.AddColumn<decimal>(
                name: "Payment.BalanceDue", 
                table: "Order.Data", 
                type: "DECIMAL(18,4)");



            migrationBuilder.AddColumn<byte>(
                name: "Version",
                table: "Order.Data",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<string>(
                name: "VersionText",
                table: "Order.Data",
                type: "varchar(2)",
                unicode: false,
                maxLength: 2,
                nullable: true,
                computedColumnSql: "(case when [Version]=(0) then '' when [Version]<=(26) then char((64)+[Version]) else char(CONVERT([int],([Version]-(1))/(26))+(64))+char(([Version]-(1))%(26)+(65)) end)");

            //Turn System Versioning back on
            //Start by dropping old history table
            migrationBuilder.Sql("DROP TABLE IF EXISTS dbo.[Historic.Order.Data]");
            migrationBuilder.Sql
            (@"
                ALTER TABLE[Order.Data] SET(SYSTEM_VERSIONING = ON(HISTORY_TABLE = dbo.[Historic.Order.Data]));"
            );
        }
    }
}
