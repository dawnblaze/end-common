using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180508025303_UPDATE_SPROCs_UpdateEmployeeRole_RemoveEmployeeRole")]
    public partial class UPDATE_SPROCs_UpdateEmployeeRole_RemoveEmployeeRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //added @RoleID both Order.Action.UpdateEmployeeRole & Order.Action.RemoveEmployeeRole PROCs...
            migrationBuilder.Sql(@"

DROP PROCEDURE IF EXISTS [dbo].[Order.Action.RemoveEmployeeRole];
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.UpdateEmployeeRole];
GO

-- ========================================================
-- Name: [Order.Action.UpdateEmployeeRole]( @BID tinyint, @OrderID int, @RoleID int, @EmployeeRoleType tinyint, @EmployeeID int )
--
-- Description: This function updates a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateEmployeeRole] @BID=1, @OrderID=1, @RoleID=NULL, @EmployeeRoleType=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.UpdateEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleType TINYINT   -- = 1
        , @EmployeeID       INT       -- = 1
		, @RoleID          INT           = NULL 

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF ISNULL(@EmployeeRoleType,-1) = 255
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to change this employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    IF (@RoleID IS NULL)
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @EmployeeRoleType
    END

    ELSE
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    UPDATE OER
    SET    EmployeeID = @EmployeeID, EmployeeName = employee.ShortName
    FROM   [Order.Employee.Role] OER 
            LEFT JOIN [Employee.Data] employee ON employee.BID = @BID AND employee.ID = @EmployeeID
    WHERE  OER.BID = @BID AND OER.ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO

-- ========================================================
-- Name: [Order.Action.RemoveEmployeeRole]( @BID tinyint, @OrderID int, @RoleID int, @EmployeeRoleType tinyint, @EmployeeID int )
--
-- Description: This function remove a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.RemoveEmployeeRole] @BID=1, @OrderID=1, @RoleID=NULL, @EmployeeRoleType=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.RemoveEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
		, @RoleID          INT           = NULL
        , @EmployeeRoleType TINYINT   -- = 1
        , @EmployeeID       INT       -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    IF @RoleID IS NOT NULL
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
	END
	ELSE
	BEGIN
		SELECT @ID = ID
		FROM [Order.Employee.Role]
		WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @EmployeeRoleType AND EmployeeID =  @EmployeeID
	END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Delete record is exists
    DELETE FROM OER
    FROM   [Order.Employee.Role] OER
    WHERE  BID = @BID AND ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [dbo].[Order.Action.RemoveEmployeeRole];
                DROP PROCEDURE IF EXISTS [dbo].[Order.Action.UpdateEmployeeRole];
            ");
        }
    }
}

