using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8340_AccountingPaymentTermSimpleList_Modify : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE OR ALTER VIEW[dbo].[Accounting.Payment.Term.SimpleList]
AS
SELECT[BID]
	, APT.[ID]
	, APT.[ClassTypeID]
	, APT.[Name] as DisplayName
	, APT.[IsActive]
	, CONVERT(BIT, CASE WHEN SOD.DefaultValue = CONVERT(VARCHAR(MAX), APT.[ID]) THEN 1 ELSE 0 END) AS [IsDefault]
FROM[Accounting.Payment.Term] APT
JOIN [System.Option.Definition] SOD ON SOD.[Name] = 'Accounting.PaymentTerm.DefaultID'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE OR ALTER VIEW[dbo].[Accounting.Payment.Term.SimpleList]
                AS
            SELECT[BID]
                , [ID]
                , [ClassTypeID]
                , [Name] as DisplayName
                , [IsActive]
                , CONVERT(BIT, 0) AS [IsDefault]
            FROM[Accounting.Payment.Term]");
        }
    }
}
