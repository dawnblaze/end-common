using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180215224405_AddTaxItemGLAccountID")]
    public partial class AddTaxItemGLAccountID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.Tax.Item].[IX_Accounting.Tax.Item_Name]', N'IX_Accounting.Tax.Item_Name_IsActive', N'INDEX';");
            //migrationBuilder.RenameIndex(
            //    name: "IX_Accounting.Tax.Item_Name",
            //    table: "Accounting.Tax.Item",
            //    newName: "IX_Accounting.Tax.Item_Name_IsActive");
            
            migrationBuilder.AddColumn<int>(
                name: "GLAccountID",
                table: "Accounting.Tax.Item",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.Sql(@"

IF NOT EXISTS (SELECT ID FROM [Accounting.GL.Account] where Name = 'Sales Tax Payable')
BEGIN
    INSERT INTO [Accounting.GL.Account] (BID, ID, Name, IsActive, CanEdit, GLAccountType) VALUES (
    1,
    2400,
    'Sales Tax Payable',
    1,
    0,
    23)
END;

-- Create Missing Tax Accounts in the GL for a BID
DECLARE @BID TINYINT = 1;

DECLARE @MissingTaxAccounts TABLE (BID SMALLINT
                                    , TaxItemID SMALLINT
                                    , TaxName NVARCHAR(255)
                                    , GLAccountID SMALLINT
                                    )
                                    ;

-- Create a list of missing GL Tax Account for that BID
INSERT INTO @MissingTaxAccounts (BID, TaxItemID, TaxName)
    SELECT BID, ID, Name
    FROM [Accounting.Tax.Item] TI
    WHERE BID = @BID
      AND ( TI.GLAccountID IS NULL 
            OR NOT EXISTS(SELECT * FROM [Accounting.GL.Account] GLA WHERE GLA.BID = TI.BID AND GLA.ID = TI.GLAccountID )
      )
;

-- See how many we have
DECLARE @MTACount INT = (SELECT COUNT(*) FROM @MissingTaxAccounts)

-- And insert them if we need to
IF (@MTACount != 0) 
BEGIN
    DECLARE @FirstID INT;

    -- Reserve the block of IDs
    EXEC @FirstID = [dbo].[Util.ID.GetID] @BID = 1, @ClassTypeID = 8000, @Count = @MTACount, @StartingID = 7000;

    -- Update the Table with the new GLAccountID 

    WITH MyTable AS
    (
        SELECT *
             , (ROW_NUMBER() OVER( PARTITION BY BID ORDER BY BID, TaxItemID ) - 1 ) as RowNumber
        FROM @MissingTaxAccounts M2
    ) 
        UPDATE MyTable
        SET GLAccountID = @FirstID + RowNumber - 1
    ;

    -- Insert the GLAccounts
    INSERT INTO [Accounting.GL.Account] (BID, ID, Name, IsActive, CanEdit, GLAccountType, ParentID) 
        SELECT BID, GLAccountID, TaxName, 1, 0, 23, 2400
        FROM @MissingTaxAccounts

    -- Update the Tax Items
    UPDATE TI
    SET GLAccountID = MTA.GLAccountID
    FROM [Accounting.Tax.Item] TI
    JOIN @MissingTaxAccounts MTA on TI.BID = MTA.BID AND TI.ID = MTA.TaxItemID
    ;

    SELECT * FROM @MissingTaxAccounts

END;
");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Item_GLAccount",
                table: "Accounting.Tax.Item",
                columns: new[] { "BID", "GLAccountID" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Item_Name",
                table: "Accounting.Tax.Item",
                columns: new[] { "BID", "Name" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Item_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.Tax.Item",
                columns: new[] { "BID", "GLAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Item_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.Tax.Item");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Tax.Item_GLAccount",
                table: "Accounting.Tax.Item");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Tax.Item_Name",
                table: "Accounting.Tax.Item");

            migrationBuilder.DropColumn(
                name: "GLAccountID",
                table: "Accounting.Tax.Item");

            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.Tax.Item].[IX_Accounting.Tax.Item_Name_IsActive]', N'IX_Accounting.Tax.Item_Name', N'INDEX';");
            //migrationBuilder.RenameIndex(
            //    name: "IX_Accounting.Tax.Item_Name_IsActive",
            //    table: "Accounting.Tax.Item",
            //    newName: "IX_Accounting.Tax.Item_Name");
        }
    }
}

