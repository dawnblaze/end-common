﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9452_SystemTimezoneSimpleListSorting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

ALTER VIEW [dbo].[enum.TimeZone.SimpleList] AS
SELECT TOP 99.9 PERCENT
      [ID]
    , [StandardName] as DisplayName
    , [IsCommon] as [IsActive]
    , CONVERT(BIT, 0) AS [HasImage]
FROM [enum.TimeZone] order by UTCOffset, StandardName
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER VIEW [dbo].[enum.TimeZone.SimpleList] AS
SELECT TOP 100 PERCENT
      [ID]
    , [StandardName] as DisplayName
    , [IsCommon] as [IsActive]
    , CONVERT(BIT, 0) AS [HasImage]
FROM [enum.TimeZone] order by UTCOffset
            ");
        }
    }
}
