using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddSubassemblyVariable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DefaultValueText",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "HasFormula",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "IsRequired",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "Options",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddColumn<int>(
                name: "VariableID",
                table: "Part.Subassembly.Element",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Part.Subassembly.Variable",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12046"),
                    DataType = table.Column<short>(nullable: false),
                    DefaultValueText = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    HasFormula = table.Column<bool>(nullable: false),
                    IsRequired = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Options = table.Column<string>(nullable: true),
                    SubassemblyID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Subassembly.Variable", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.Variable_enum.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.Variable_Part.Subassembly.Data",
                        columns: x => new { x.BID, x.SubassemblyID },
                        principalTable: "Part.Subassembly.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });
                        
            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Variable_Subassembly",
                table: "Part.Subassembly.Variable",
                columns: new[] { "BID", "SubassemblyID", "Name" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Variable._Part.Subassembly.Element.Data",
                table: "Part.Subassembly.Element",
                columns: new[] { "BID", "VariableID" },
                principalTable: "Part.Subassembly.Variable",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Variable._Part.Subassembly.Element.Data",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropTable(
                name: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "VariableID",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddColumn<string>(
                name: "DefaultValueText",
                table: "Part.Subassembly.Element",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasFormula",
                table: "Part.Subassembly.Element",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsRequired",
                table: "Part.Subassembly.Element",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Part.Subassembly.Element",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Options",
                table: "Part.Subassembly.Element",
                nullable: true);
        }
    }
}
