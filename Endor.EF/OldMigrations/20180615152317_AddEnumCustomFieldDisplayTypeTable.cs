using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180615152317_AddEnumCustomFieldDisplayTypeTable")]
    public partial class AddEnumCustomFieldDisplayTypeTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.CustomField.DisplayType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    DataType = table.Column<byte>(type: "tinyint", nullable: false),
                    FormatString = table.Column<string>(type: "VARCHAR(MAX)", nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.DisplayType", x => x.ID);
                    table.ForeignKey(
                        name: "FK_enum.CustomField.DisplayType_enum.CustomField.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.CustomField.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_enum.CustomField.DisplayType_DataType",
                table: "enum.CustomField.DisplayType",
                column: "DataType");

            migrationBuilder.Sql(
@"INSERT [enum.CustomField.DisplayType]
([ID], [Name], [DataType])
VALUES
  ( 1, N'Unformatted Single Line'              , 0 )
, ( 2, N'Unformatted Memo'                     , 0 )
, ( 3, N'Formatted Line'                       , 0 )
, ( 4, N'Formatted Memo'                       , 0 )
, (11, N'Integer'                              , 1 )
, (21, N'Number'                               , 2 )
, (22, N'Integer (Rounded)'                    , 2 )
, (23, N'2 Decimal Places'                     , 2 )
, (24, N'4 Decimal Places'                     , 2 )
, (25, N'Money'                                , 2 )
, (26, N'Accounting'                           , 2 )
, (31, N'Checkbox'                             , 3 )
, (32, N'Yes-No'                               , 3 )
, (33, N'True-False'                           , 3 )
, (34, N'Enabled-Disabled'                     , 3 )
, (39, N'Custom'                               , 3 )
, (71, N'Date ( Short Format )'                , 7 )
, (72, N'Date ( Long Format )'                 , 7 )
, (73, N'Date ( Mon 6/19 )'                    , 7 )
, (74, N'Days from Today (Late)'               , 7 )
, (75, N'Workdays from Today (Late)'           , 7 )
, (81, N'Time ( Short Format )'                , 8 )
, (82, N'Time ( Long Format )'                 , 8 )
, (91, N'DateTime ( Short Format )'            , 9 )
, (92, N'DateTime (Long Format)'               , 9 )
, (93, N'DateTime ( Mon 6/19 11:32 )'          , 9 )
, (94, N'Days and Hours from Today (Late)'     , 9 )
, (95, N'WorkDays and Hours from Today (Late)' , 9 )
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.CustomField.DisplayType");
        }
    }
}

