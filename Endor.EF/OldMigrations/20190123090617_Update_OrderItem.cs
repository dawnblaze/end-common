using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Update_OrderItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {


            migrationBuilder.DropColumn(
                name: "ItemIsSinglePart",
                table: "Order.Item.Data");

            migrationBuilder.AddColumn<bool>(
                name: "ItemIsSinglePart",
                table: "Order.Item.Data",
                nullable: false,
                defaultValueSql: "1");

            migrationBuilder.Sql(@"

                ALTER TABLE [dbo].[Order.Item.Data] 
                DROP CONSTRAINT IF EXISTS [FK_Order.Item.Data_Material]
                ;

                ALTER TABLE [dbo].[Order.Item.Data] 
                DROP CONSTRAINT IF EXISTS [FK_Order.Item.Data_Machine]
                ;

                ALTER TABLE [dbo].[Order.Item.Data] 
                DROP CONSTRAINT IF EXISTS [FK_Order.Item.Data_Labor]
                ;                
                
                DROP INDEX IF EXISTS [IX_Order.Item.Data_Product] ON [dbo].[Order.Item.Data];

                ALTER TABLE [dbo].[Order.Item.Data] DROP COLUMN ProductID, MaterialID, LaborID, MachineID;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
