using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180227194808_FixPaymentTermSearchCriteria")]
    public partial class FixPaymentTermSearchCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter.Criteria] SET Field = 'IsActive' WHERE TargetClassTypeID = 11105 AND Name = 'Is Active'
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

