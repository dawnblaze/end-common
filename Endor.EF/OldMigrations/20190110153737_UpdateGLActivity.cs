using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateGLActivity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StartDT",
                table: "Activity.GLActivity");
            migrationBuilder.DropColumn(
                name: "EndDT",
                table: "Activity.GLActivity");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CompletedDT",
                table: "Activity.GLActivity",
                type: "datetime2(0)",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(0)");

            migrationBuilder.AlterColumn<short>(
                name: "CompletedByID",
                table: "Activity.GLActivity",
                type: "smallint SPARSE",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint SPARSE");

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDT",
                table: "Activity.GLActivity",
                type: "datetime2(0)",
                nullable: false,
                computedColumnSql: "([CompletedDT])"
                );

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDT",
                table: "Activity.GLActivity",
                type: "datetime2(0)",
                nullable: false,
                computedColumnSql: "([CompletedDT])"
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "CompletedDT",
                table: "Activity.GLActivity",
                type: "datetime2(0)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(0)",
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "CompletedByID",
                table: "Activity.GLActivity",
                type: "smallint SPARSE",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint SPARSE",
                oldNullable: true);
        }
    }
}
