using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180430222737_AddGoogleAndFormattingOptions")]
    public partial class AddGoogleAndFormattingOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [System.Option.Section] 
(
ID, Name, ParentID, IsHidden, Depth
)
VALUES
(
0, 'Business', null, 1, 0
),
(
1100, 'Locator Configuration', 0, 1, 1
);

INSERT INTO [System.Option.Category]
(
ID, Name, SectionID, Description, OptionLevels, IsHidden
)
VALUES
(
1102, 'Locator.Address', 1100, 'Settings affecting Address Input and Display', 4, 1
),
(
1104, 'Locator.Phone', 1100, 'Settings affecting Phone Input and Display', 4, 1
)

INSERT INTO [System.Option.Definition]
(
ID, Name, Label, Description, DataType, CategoryID, DefaultValue, IsHidden
)
VALUES
(
50, 'Locator.Address.GooglePlaces.Enabled', 'Locator Address GooglePlaces Enabled', 'Determine if Google Places is enabled for the user	', 3, 1102, '1', 1
),
(
51, 'Locator.Address.Format	', 'Locator Address GooglePlaces Enabled', 'Determine if Google Places is enabled for the user	', 0, 1102, 'US', 1
),(
52, 'Locator.Phone.Format', 'Locator Address GooglePlaces Enabled', 'Determine if Google Places is enabled for the user	', 0, 1102, 'US', 1
);

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Definition] where ID in (50,51,52);
DELETE FROM [System.Option.Category] where ID in (1102,1104);
DELETE FROM [System.Option.Section] where ID in (0,1100);
");
        }
    }
}

