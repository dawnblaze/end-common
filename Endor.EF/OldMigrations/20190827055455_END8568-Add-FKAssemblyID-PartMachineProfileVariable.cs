using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8568AddFKAssemblyIDPartMachineProfileVariable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE V
                SET AssemblyID = M.MachineTemplateID
                FROM [Part.Machine.Profile.Variable] V
                     JOIN [Part.Machine.Profile] P ON P.BID = V.BID AND P.ID = V.ProfileID
                     JOIN [Part.Machine.Data] M ON M.BID = P.BID AND M.ID = P.MachineID
                WHERE NOT EXISTS(SELECT 1 FROM [Part.Subassembly.Data] A WHERE A.ID = V.AssemblyID)
                ;

                ALTER TABLE [Part.Machine.Profile.Variable] WITH CHECK
                ADD CONSTRAINT [FK_Part.Machine.Profile.Variable_Part.Subassembly.Data]
                FOREIGN KEY([BID], [AssemblyID]) REFERENCES [Part.Subassembly.Data] ([BID], [ID])
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
