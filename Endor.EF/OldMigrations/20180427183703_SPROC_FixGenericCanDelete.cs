using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180427183703_SPROC_FixGenericCanDelete")]
    public partial class SPROC_FixGenericCanDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [enum.ClassType] SET TableName='Order.Note' WHERE ID = 10015;");

            migrationBuilder.Sql(@"
ALTER PROCEDURE [dbo].[IAtom.Action.CanDelete]
--DECLARE
          @BID            TINYINT 
        , @ID             INT     
        , @ClassTypeID    INT     
        
        , @ShowCantDeleteReason BIT          = 1
        , @Result               BIT          = NULL OUTPUT
        , @CantDeleteReason     VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
    -- Find the table Name
    DECLARE @TableName VARCHAR(255) = RTRIM(LTRIM(dbo.[Util.ClassType.GetTableName]( @ClassTypeID )))
          , @CMD       NVARCHAR(MAX)
          , @Message   VARCHAR(1024)
    ;

    -- Check that we found a table name by ClassTypeID
    IF LEN(@TableName) = 0
    BEGIN
        SELECT @Message = CONCAT('Unable to Resolve a Table Name for ClassTypeID ', @ClassTypeID)
             , @Result = 0;

        THROW 50000, @Message, 1;
        -- RETURN @Result;
    END

    -- Now look any FK dependencies on the table, BID, and ID

    SELECT @CMD = CONCAT(@CMD + ' UNION ', ' SELECT ''[', FK_TableName, ']'' AS [TableName] WHERE EXISTS(SELECT * FROM [', FK_TableName, '] WHERE BID=', @BID, ' AND [', FK_ColumnName, ']=', @ID, ') ')
    FROM [ForeignKeyConstraints]
    WHERE PK_TableName = dbo.[Util.ClassType.GetTableName]( @ClassTypeID ) -- 'Accounting.GL.Data'
    AND PK_ColumnName != 'BID'
    ;

    IF (@@ROWCOUNT = 0) -- Check if we have any constraints ... 
    BEGIN
        SET @Result = 1;
        SELECT @Result;
        RETURN @Result;
    END;

    SET @CMD = CONCAT(' SELECT @CantDeleteReason = CONCAT(@CantDeleteReason+'' AND '', [TableName]) FROM ( ',@CMD, ') TEMP ')

    -- SELECT @CMD;
    EXEC sp_executesql @CMD, N'@CantDeleteReason VARCHAR(MAX) OUTPUT', @CantDeleteReason=@CantDeleteReason OUTPUT

    -- SELECT @CantDeleteReason;
    SET @Result = IIF(@CantDeleteReason IS NULL, 1, 0);

    IF (@Result = 0)
    BEGIN
        SET @CantDeleteReason = 'Unable to Delete Record - Reference by the following Tables: '+@CantDeleteReason;

        IF (@ShowCantDeleteReason=1) 
            SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]  --' -- to fix VS Code formatting bug
        ELSE
            SELECT @Result as Result;
    END
    ELSE
        SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [enum.ClassType] SET TableName='Order.Notes' WHERE ID = 10015;");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[IAtom.Action.CanDelete]
GO

-- ========================================================
-- Name: [IAtom.Action.CanDelete]
--
-- Description: This procedure returns a bit value indicating if the record can be deleted.
--              It does this by reviewing the constraints on the particular ID, BID for the 
--              Table identified from the ClassTypeID.
--
-- Fields:      @BID = The BusinessID
--              @ID = The ID of the record
--              @ClassTypeID = The ClassTypeID of the record.  This is used to look up the table.
--              @Result OUTPUT -- set this variable in if you want the result out rather than pulling from the select
--
-- Sample Use:  
/* 
              DECLARE @Result BIT, @FailReason VARCHAR(MAX)
              EXEC [dbo].[IAtom.Action.CanDelete] @BID=1, @ID=1, @ClassTypeID=1005, @ShowCantDeleteReason=1
                        , @Result=@Result OUTPUT, @CantDeleteReason=@FailReason OUTPUT
              SELECT @Result, @FailReason;
*/
-- ========================================================
CREATE PROCEDURE [dbo].[IAtom.Action.CanDelete]
--DECLARE
          @BID            TINYINT 
        , @ID             INT     
        , @ClassTypeID    INT     

        , @ShowCantDeleteReason BIT          = 1
        , @Result               BIT          = NULL OUTPUT
        , @CantDeleteReason     VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
    -- Find the table Name
    DECLARE @TableName VARCHAR(255) = RTRIM(LTRIM(dbo.[Util.ClassType.GetTableName]( @ClassTypeID )))
          , @CMD       NVARCHAR(MAX)
          , @Message   VARCHAR(1024);

    -- Check that we found a table name by ClassTypeID
    IF LEN(@TableName) = 0
    BEGIN
        SELECT @Message = CONCAT('Unable to Resolve a Table Name for ClassTypeID ', @ClassTypeID)
             , @Result = 0;

        THROW 50000, @Message, 1;
        -- RETURN @Result;
    END

    -- Now look any FK dependencies on the table, BID, and ID

    SELECT @CMD = CONCAT(@CMD + ' UNION ', ' SELECT ''[', FK_TableName, ']'' AS [TableName] WHERE EXISTS(SELECT * FROM [', FK_TableName, '] WHERE BID=', @BID, ' AND [', FK_ColumnName, ']=', @ID, ') ')
    FROM [ForeignKeyConstraints]
    WHERE PK_TableName = dbo.[Util.ClassType.GetTableName]( @ClassTypeID ) -- 'Accounting.GL.Data'
    AND PK_ColumnName != 'BID'

    SET @CMD = CONCAT(' SELECT @CantDeleteReason = CONCAT(@CantDeleteReason+'' AND '', [TableName]) FROM ( ',@CMD, ') TEMP ')

    -- SELECT @CMD;
    EXEC sp_executesql @CMD, N'@CantDeleteReason VARCHAR(MAX) OUTPUT', @CantDeleteReason=@CantDeleteReason OUTPUT

    -- SELECT @CantDeleteReason;
    SET @Result = IIF(@CantDeleteReason IS NULL, 1, 0);

    IF (@Result = 0)
    BEGIN
        SET @CantDeleteReason = 'Unable to Delete Record - Reference by the following Tables: '+@CantDeleteReason;

        IF (@ShowCantDeleteReason=1) 
            SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]  --' -- to fix VS Code formatting bug
        ELSE
            SELECT @Result as Result;
    END
    ELSE
        SELECT @Result as Result;
END
");
        }
    }
}

