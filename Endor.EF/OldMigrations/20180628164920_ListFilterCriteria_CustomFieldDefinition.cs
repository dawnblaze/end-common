using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180628164920_ListFilterCriteria_CustomFieldDefinition")]
    public partial class ListFilterCriteria_CustomFieldDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (15025,'Name','Name','Name',0,0,0,0,NULL,NULL,0,1, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)

INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (15025,'AppliesTo','Applies to','AppliesTo',0,1,4,0,NULL,NULL,0,2, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)

INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (15025,'Is Active','Is Active','IsActive',0,3,2,0,'Is Not Active,Is Active',NULL,1,3, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE [dbo].[System.List.Filter.Criteria]  where TargetClassTypeID=15025
            ");
        }
    }
}

