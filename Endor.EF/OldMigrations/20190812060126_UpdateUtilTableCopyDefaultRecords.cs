using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateUtilTableCopyDefaultRecords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
-- ========================================================

    Procedure Name: [Util.Table.CopyDefaultRecords] 

    Description: This function copys all of the -1 and -2 records for a table 
                    into all of the BIDs for which they don't exist.
                    It then deletes all the BID=-2 records.

    Sample Use:   

        EXEC [Util.Table.CopyDefaultRecords] @TableName = '_Root.Data';
        
-- ========================================================
*/
CREATE OR ALTER PROCEDURE [dbo].[Util.Table.CopyDefaultRecords] 
    (@TableName NVARCHAR(200), @IncludeRequired BIT = 1, @IncludeDefault BIT = 1)
AS
BEGIN
    -- DECLARE @TableName NVARCHAR(200) = '_Root.Data';
    -- DECLARE @IncludeRequired BIT = 1;
    -- DECLARE @IncludeDefault BIT = 1

    DECLARE @BIDS NVARCHAR(30) = IIF(@IncludeRequired=1, IIF(@IncludeDefault=1, '(-1, -2)', '(-1)'), IIF(@IncludeDefault=1, '(-2)', '(null)'));
    DECLARE @Columns NVARCHAR(MAX);

    SELECT @Columns = CONCAT(@Columns + ', ', '[',ColumnName,']')
    FROM TablesAndColumns
    WHERE TableName = @TableName
        AND ColumnName NOT IN ('BID', 'ValidToDT', 'ModifiedDT')
        AND IsComputed = 0
        AND IsIdentity = 0
    ;

    --PRINT @Columns;

    DECLARE @SQL NVARCHAR(MAX) = '

    INSERT INTO [@TABLENAME] ( BID, '+@Columns+' )
        SELECT B.BID, T.*
        FROM [Business.Data] B
        JOIN 
        (   SELECT '+@Columns+'
            FROM [@TABLENAME]
            WHERE BID IN '+@BIDS+'
        ) T ON 1=1
        WHERE NOT EXISTS (SELECT * FROM [@TABLENAME] D WHERE D.BID = B.BID AND D.ID = T.ID)
            AND B.BID > 0
    ;
    ';

    IF (@IncludeDefault=1)
        SET @SQL += '
    DELETE T
    FROM [@TABLENAME] T
    WHERE T.BID = -3
        AND T.ID IN (SELECT T2.ID FROM [@TABLENAME] T2 WHERE T2.BID = -2)
    ;

    UPDATE [@TABLENAME]
    SET BID = -3
    WHERE BID = -2
    ;
    ';

    SET @SQL = REPLACE(@SQL, '@TABLENAME', @TableName);

    --PRINT @SQL

    EXECUTE(@SQL);
END;


");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
