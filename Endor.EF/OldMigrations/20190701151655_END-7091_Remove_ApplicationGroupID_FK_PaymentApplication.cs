using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END7091_Remove_ApplicationGroupID_FK_PaymentApplication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Application_Accounting.Payment.Application.ApplicationGroupID",
                table: "Accounting.Payment.Application");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Payment.Application_BID_ApplicationGroupID",
                table: "Accounting.Payment.Application");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Application_BID_ApplicationGroupID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "ApplicationGroupID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Application_Accounting.Payment.Application.ApplicationGroupID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "ApplicationGroupID" },
                principalTable: "Accounting.Payment.Application",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
