﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10039CreditMemoRecordChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreditMemoPrefix",
                table: "Location.Data",
                type: "nvarchar(10)",
                nullable: true,
                defaultValue: "CM-");

            migrationBuilder.Sql(
                @"-- In order to change a computed column, it must be dropped and re-added.
-- In order to drop a column, we have to disable system versioning.
ALTER TABLE[Order.Data]
SET(SYSTEM_VERSIONING = OFF)
;

            ALTER TABLE[Order.Data]
DROP COLUMN ClassTypeID
;
            ALTER TABLE[Order.Data]
ADD ClassTypeID AS(case [TransactionType] when(2) then(10000) when(1) then(10200) when(8) then(10300) when(4) then(10100) else (0) end)
;
            GO

            -- the history table has to match, so we have to make similar changes to the History table
--but not as computed, since the history table is only data.
ALTER TABLE[Historic.Order.Data]
DROP COLUMN ClassTypeID
;
            ALTER TABLE[Historic.Order.Data]
ADD ClassTypeID INT NOT NULL
CONSTRAINT DF_ClassTypeIDDefault DEFAULT 0-- we have to have a default to add a NOT NULL field
;

            --now fix up the historic data using the same computed formula
UPDATE[Historic.Order.Data]
SET ClassTypeID = (case [TransactionType] when(2) then(10000) when(1) then(10200) when(8) then(10300) when(4) then(10100) else (0) end)
;

-- now drop the constraint
ALTER TABLE[Historic.Order.Data]
DROP CONSTRAINT DF_ClassTypeIDDefault
;
GO

-- Re-enable System Versioning
ALTER TABLE[Order.Data]
SET(SYSTEM_VERSIONING = ON(HISTORY_TABLE = dbo.[Historic.Order.Data]))
;  
        ");

            migrationBuilder.Sql(@"
INSERT [enum.Order.TransactionType] ([ID], [Name])
VALUES (8, N'Credit Memo');

INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) 
VALUES (81, N'Unposted', 8, 0, 0, 81, 0)
        , (86, N'Posted', 8, 0, 0, 86, 0)
        , (89, N'Voided', 8, 0, 0, 89, 0)
;

INSERT INTO [Order.Item.Status]
([BID], [ID], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
VALUES (-1, 81, 1, 1, 1, 8, 81, 50, N'Unposted', NULL, NULL)
        , (-1, 86, 1, 1, 0, 8, 86, 50, N'Posted', NULL, NULL)
        , (-1, 89, 1, 1, 0, 8, 89, 50, N'Voided', NULL, NULL)
;

-- Now Copy the records to every BID
EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName = 'Order.Item.Status';
");

            /*migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Order.Data",
                nullable: false,
                computedColumnSql: "(case [TransactionType] when (2) then (10000) when (1) then (10200) when (8) then (10300) when (4) then (10100) else (0) end)",
                oldClrType: typeof(int),
                oldType: "int",
                oldComputedColumnSql: "(CASE TransactionType WHEN 2 THEN 10000 WHEN 1 THEN 10200 WHEN 4 THEN 10100 ELSE 0 END)");*/
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreditMemoPrefix",
                table: "Location.Data");

            /*migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Order.Data",
                type: "int",
                nullable: false,
                computedColumnSql: "(CASE TransactionType WHEN 2 THEN 10000 WHEN 1 THEN 10200 WHEN 4 THEN 10100 ELSE 0 END)",
                oldClrType: typeof(int),
                oldComputedColumnSql: "(case [TransactionType] when (2) then (10000) when (1) then (10200) when (8) then (10300) when (4) then (10100) else (0) end)");*/
        }
    }
}
