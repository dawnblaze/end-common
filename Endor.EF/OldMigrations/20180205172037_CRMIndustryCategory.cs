using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180205172037_CRMIndustryCategory")]
    public partial class CRMIndustryCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden, SearchTerms)
VALUES (8,'Industry',10,'CRM Industries Options',4,0,NULL);
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.option.Category] WHERE ID = 8;
");

        }
    }
}

