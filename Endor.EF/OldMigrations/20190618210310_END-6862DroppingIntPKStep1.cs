using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6862DroppingIntPKStep1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Part.Machine.Profile.Table",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.AlterColumn<short>(
                name: "RowCount",
                table: "Part.Machine.Profile.Table",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<short>(
                name: "ColumnCount",
                table: "Part.Machine.Profile.Table",
                type: "smallint",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AlterColumn<short>(
                name: "ID",
                table: "Part.Machine.Profile.Table",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Part.Machine.Profile.Table",
                table: "Part.Machine.Profile.Table",
                column: "BID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Part.Machine.Profile.Table",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.AlterColumn<short>(
                name: "RowCount",
                table: "Part.Machine.Profile.Table",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "Part.Machine.Profile.Table",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<short>(
                name: "ColumnCount",
                table: "Part.Machine.Profile.Table",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Part.Machine.Profile.Table",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "BID", "ID" });
        }
    }
}
