using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180612204025_END1458_DestinationID_FKs_Added")]
    public partial class END1458_DestinationID_FKs_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {            
            migrationBuilder.AddForeignKey(
                name: "FK_Order.Contact.Role_DestinationID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "DestinationID" },
                principalTable: "Order.Destination.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Employee.Role_DestinationID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "DestinationID" },
                principalTable: "Order.Destination.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.KeyDate_DestinationID",
                table: "Order.KeyDate",
                columns: new[] { "BID", "DestinationID" },
                principalTable: "Order.Destination.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Note_DestinationID",
                table: "Order.Note",
                columns: new[] { "BID", "DestinationID" },
                principalTable: "Order.Destination.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Contact.Role_DestinationID",
                table: "Order.Contact.Role");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Employee.Role_DestinationID",
                table: "Order.Employee.Role");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.KeyDate_DestinationID",
                table: "Order.KeyDate");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Note_DestinationID",
                table: "Order.Note");
        }
    }
}

