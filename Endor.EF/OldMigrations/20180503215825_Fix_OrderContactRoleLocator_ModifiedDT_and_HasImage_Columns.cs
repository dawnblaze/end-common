using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180503215825_Fix_OrderContactRoleLocator_ModifiedDT_and_HasImage_Columns")]
    public partial class Fix_OrderContactRoleLocator_ModifiedDT_and_HasImage_Columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Boolean>(
                name: "HasImage",
                table: "Order.Contact.Locator",
                type: "bit",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(Boolean),
                oldType: "bit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<Boolean>(
                name: "HasImage",
                table: "Order.Contact.Locator",
                type: "bit",
                nullable: false,
                oldClrType: typeof(Boolean),
                oldType: "bit");
        }
    }
}

