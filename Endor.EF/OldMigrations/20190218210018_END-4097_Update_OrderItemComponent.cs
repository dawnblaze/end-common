using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4097_Update_OrderItemComponent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<decimal>(
                name: "Cost.Component.Fixed",
                table: "Order.Item.Component",
                type: "DECIMAL(18,6) SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Cost.Component.Unit",
                table: "Order.Item.Component",
                type: "DECIMAL(18,6)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "Cost.Component.Fixed",
                table: "Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "Cost.Component.Unit",
                table: "Order.Item.Component");
        }
    }
}
