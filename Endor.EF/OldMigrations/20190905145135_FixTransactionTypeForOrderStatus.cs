using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixTransactionTypeForOrderStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [enum.Order.OrderStatus]
SET TransactionType = 32
WHERE ID BETWEEN 31 AND 39
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
