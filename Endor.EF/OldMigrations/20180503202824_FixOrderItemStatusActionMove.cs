using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180503202824_FixOrderItemStatusActionMove")]
    public partial class FixOrderItemStatusActionMove : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [dbo].[Order.Item.Status.Action.Move];
            ");

            migrationBuilder.Sql(@"
                /* 
                    This procedures moves an Item Status in sort before another Item Status.
                    The approach is to take the StatusIndex of the target ID and then increment all other
                    Item Statuses with a StatusIndex >= that.

                    Example Usage:
                        EXEC [Order.Item.Status.Action.Move] @BID=1, @UD=49, @TargetID=19               -- movebefore
		                EXEC [Order.Item.Status.Action.Move] @BID=1, @UD=49, @TargetID=19, @MoveAfter=1 -- moveafter
		                select * from  [Order.Item.Status]

                */
                CREATE PROCEDURE [dbo].[Order.Item.Status.Action.Move]
                -- DECLARE 
                          @BID   TINYINT
                        , @ID    SMALLINT
                        , @TargetID smallint
		                , @MoveAfter BIT = 0

                        , @Result         INT     = NULL  OUTPUT
                AS
                BEGIN
                    DECLARE @TargetStatusIndex TINYINT
                          , @Message VARCHAR(1024)
	                      , @OrderStatusID tinyint
	                      , @TargetOrderStatusID tinyint
                          ;

                    -- Ignore if dropping on itself
                    IF (@ID = @TargetID)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Can''t drop an item on itself.'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END
                    ;

	                SELECT @TargetStatusIndex = StatusIndex + (CASE WHEN @MoveAfter=1 THEN 1 ELSE 0 END)
                    FROM [Order.Item.Status] 
                    WHERE BID = @BID
                      AND ID = @TargetID
                    ;

                    -- Make Sure we Have a Result
                    IF (@TargetStatusIndex IS NULL)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Specified TargetID not Found.'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END
                    ;

                    SELECT @OrderStatusID = OrderStatusID FROM [Order.Item.Status] where BID = @BID AND ID = @ID;
                    SELECT @TargetOrderStatusID = OrderStatusID FROM [Order.Item.Status] where BID = @BID AND ID = @TargetID;
                    IF (@OrderStatusID <> @TargetOrderStatusID)
                    BEGIN
                    SELECT @Result = 0
                            , @Message = 'Can''t move a status before or after status that does not have the same OrderStatusID.'
                            ;

                    THROW 50000, @Message, 1;
                    RETURN @Result;
                    END;

                    SET @Result = 0;

	                UPDATE [Order.Item.Status] 
                    SET StatusIndex = @TargetStatusIndex 
                    WHERE BID = @BID
                      AND ID = @ID;

                    SET @Result = @Result + @@ROWCOUNT;

	                WITH OrderItemStatus AS (
		                  SELECT t1.*, row_number() OVER (ORDER BY StatusIndex) AS seqnum
		                  FROM [Order.Item.Status]  t1 
                          WHERE @BID = BID AND ID != @ID
                            AND StatusIndex >= @TargetStatusIndex 
		                 )
	                    UPDATE OrderItemStatus
	                    SET StatusIndex = @TargetStatusIndex + seqnum;

                    SET @Result = @Result + @@ROWCOUNT;

                    SELECT @Result as Result;
                END;
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

