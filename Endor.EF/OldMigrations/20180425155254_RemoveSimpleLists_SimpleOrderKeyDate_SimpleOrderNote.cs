using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180425155254_RemoveSimpleLists_SimpleOrderKeyDate_SimpleOrderNote")]
    public partial class RemoveSimpleLists_SimpleOrderKeyDate_SimpleOrderNote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("IF EXISTS (SELECT * FROM sys.views WHERE name = 'Order.KeyDate.SimpleList' AND type = 'V') DROP VIEW [dbo].[Order.KeyDate.SimpleList];");
            migrationBuilder.Sql("IF EXISTS (SELECT * FROM sys.views WHERE name = 'Order.Note.SimpleList' AND type = 'V') DROP VIEW [dbo].[Order.Note.SimpleList];");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    IF EXISTS (SELECT * FROM sys.views WHERE name = 'Order.KeyDate.SimpleList' AND type = 'V') DROP VIEW [dbo].[Order.KeyDate.SimpleList];
                    GO
                    CREATE VIEW [dbo].[Order.KeyDate.SimpleList]
                    as
                    SELECT [Order.KeyDate].[BID]
                    , [Order.KeyDate].[ID]
                    , [Order.KeyDate].[ClassTypeID]
                    , OKDT.[Name] as DisplayName
                    , CONVERT(BIT, 1) as [IsActive]
                    , CONVERT(BIT, 0) as [HasImage]
                    , CONVERT(BIT, 0) AS [IsDefault]
                    from [Order.KeyDate] 
                    Left join [enum.Order.KeyDateType] OKDT on OKDT.ID = [Order.KeyDate].KeyDateType;
                ");

            migrationBuilder.Sql(
                @"
                    IF EXISTS (SELECT * FROM sys.views WHERE name = 'Order.Note.SimpleList' AND type = 'V') DROP VIEW [dbo].[Order.Note.SimpleList];
                    GO
                    CREATE VIEW [dbo].[Order.Note.SimpleList] 
                    as
                    SELECT [Order.Note].[BID]
                    , [Order.Note].[ID]
                    , [Order.Note].[ClassTypeID]
                    , ONT.[Name] as DisplayName
                    , CONVERT(BIT, 1) as [IsActive]
                    , CONVERT(BIT, 0) as [HasImage]
                    , CONVERT(BIT, 0) AS [IsDefault]
                    from [Order.Note] 
                    Left join [enum.Order.NoteType] ONT on ONT.ID = [Order.Note].NoteType;
                ");
        }
    }
}

