﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class RemoveColumnDefaultValue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "ElementType",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldDefaultValueSql: "255");

            migrationBuilder.AlterColumn<byte>(
                name: "ColumnsWide",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<byte>(
                name: "Column",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldDefaultValueSql: "1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "ElementType",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "255",
                oldClrType: typeof(byte),
                oldType: "tinyint");

            migrationBuilder.AlterColumn<byte>(
                name: "ColumnsWide",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(byte),
                oldType: "tinyint");

            migrationBuilder.AlterColumn<byte>(
                name: "Column",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(byte),
                oldType: "tinyint");
        }
    }
}
