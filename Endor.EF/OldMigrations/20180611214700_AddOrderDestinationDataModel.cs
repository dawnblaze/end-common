using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180611214700_AddOrderDestinationDataModel")]
    public partial class AddOrderDestinationDataModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE TABLE [dbo].[Order.Destination.Data](
    [BID] [smallint] NOT NULL,
    [ID] [int] NOT NULL,
    [ClassTypeID]  AS ((10040)),
    [ModifiedDT] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT (SYSUTCDATETIME()),
    [ValidToDT] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL DEFAULT (CONVERT([datetime2](7),'9999-12-31 23:59:59.99999999')),
    [OrderID] [int] NOT NULL,
    [DestinationNumber] [smallint] NOT NULL,
    [DestinationType] [tinyint] NOT NULL,
    [IsForAllItems] [bit] NOT NULL,
    [Name] [nvarchar](255) NOT NULL,
    [Description] [nvarchar](max) NULL,
    [OrderStatusID] [tinyint] NOT NULL,
    [ItemStatusID] [smallint] NOT NULL,
    [SubstatusID] [smallint] NULL,
    [Variables] [xml] NULL,
    [Price.IsLocked] [bit] NOT NULL,
    [Price.Net] [decimal](18, 4) NULL,
    [Price.NetOV] [bit] NOT NULL,
    [Price.Discount] [decimal](18, 4) SPARSE  NULL,
    [Price.PreTax]  AS ([Price.Net]-isnull([Price.Discount],(0))),
    [Price.Taxable] [decimal](18, 4) NULL,
    [Price.TaxableOV] [bit] NOT NULL,
    [Price.Tax] [decimal](18, 4) NULL,
    [Price.Total]  AS (([Price.Net]-isnull([Price.Discount],(0)))+[Price.Tax]) PERSISTED,
    [Computed.Net] [decimal](18, 4) SPARSE  NULL,
    [Computed.PreTax]  AS ([Computed.Net]-isnull([Price.Discount],(0.0))),
    [TaxGroupID] [smallint] NOT NULL,
    [TaxGroupOV] [bit] NOT NULL,
    [IsTaxExempt] [bit] NOT NULL,
    [TaxExemptReasonID] [smallint] SPARSE  NULL,
    [Cost.Material] [decimal](18, 4) SPARSE  NULL,
    [Cost.Labor] [decimal](18, 4) SPARSE  NULL,
    [Cost.Machine] [decimal](18, 4) SPARSE  NULL,
    [Cost.Other] [decimal](18, 4) SPARSE  NULL,
    [Cost.Total]  AS (isnull(((isnull([Cost.Material],(0.0))+isnull([Cost.Labor],(0.0)))+isnull([Cost.Machine],(0.0)))+isnull([Cost.Other],(0.0)),(0.0))),
    [HasDocuments] [bit] NOT NULL,
    CONSTRAINT [PK_Order.Destination.Data] PRIMARY KEY ( [BID], [ID] ),
    PERIOD FOR SYSTEM_TIME(ModifiedDT, ValidToDT)
);
 
-- Turn on Temporal Versioning
ALTER TABLE [Order.Destination.Data] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Destination.Data]));
");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Destination.Data_OrderID",
                table: "Order.Destination.Data",
                columns: new[] { "BID", "OrderID", "DestinationNumber" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Destination.Data_ItemStatus",
                table: "Order.Destination.Data",
                columns: new[] { "BID", "ItemStatusID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Destination.Data_ItemSubStatus",
                table: "Order.Destination.Data",
                columns: new[] { "BID", "SubStatusID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE[Order.Destination.Data] SET(SYSTEM_VERSIONING = OFF);
");
            migrationBuilder.DropTable(
                name: "Historic.Order.Destination.Data");

            migrationBuilder.DropTable(
                name: "Order.Destination.Data");
        }
    }
}

