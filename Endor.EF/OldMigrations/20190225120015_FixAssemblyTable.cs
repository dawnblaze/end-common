using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixAssemblyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Part.Assembly.Table",
                table: "Part.Assembly.Table");

            migrationBuilder.AlterColumn<byte>(
                name: "RowUnitID",
                table: "Part.Assembly.Table",
                type: "tinyint SPARSE",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "tinyint SPARSE");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDT",
                table: "Part.Assembly.Table",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "GetUTCDate()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Part.Assembly.Table",
                type: "varchar(4000)",
                maxLength: 4000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(4000)",
                oldMaxLength: 4000,
                oldNullable: true);

            migrationBuilder.AlterColumn<byte>(
                name: "ColumnUnitID",
                table: "Part.Assembly.Table",
                type: "tinyint SPARSE",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "tinyint SPARSE");

            migrationBuilder.AlterColumn<string>(
                name: "CellDataJSON",
                table: "Part.Assembly.Table",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            //Fix bug with EF on removing primary key...
            migrationBuilder.AddColumn<short>(
                name: "ID2",
                table: "Part.Assembly.Table",
                nullable: true);

            migrationBuilder.Sql("update [part.assembly.table] set ID2 = ID");

            migrationBuilder.DropColumn(
                name: "ID",
                table: "Part.Assembly.Table");

            migrationBuilder.RenameColumn(
                name: "ID2",
                table: "[Part.Assembly.Table]",
                newName: "ID"
                );

            migrationBuilder.AlterColumn<short>(
                name: "ID",
                table: "Part.Assembly.Table",
                nullable: false,
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsTierTable",
                table: "Part.Assembly.Table",
                nullable: false,
                computedColumnSql: "(isnull(case when [TableType]>=(1) AND [TableType]<=(4) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))",
                oldClrType: typeof(bool));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Part.Assembly.Table",
                table: "Part.Assembly.Table",
                columns: new[] { "BID", "ID" });


            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_Subassembly",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType1",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_ColumnUnit",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_RowUnit",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.TableType",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Data",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Variable1",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Variable",
                table: "Part.Assembly.Table");


            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_ColumnMatchType",
                table: "Part.Assembly.Table",
                column: "ColumnMatchType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_ColumnUnitID",
                table: "Part.Assembly.Table",
                column: "ColumnUnitID");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_RowMatchType",
                table: "Part.Assembly.Table",
                column: "RowMatchType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_RowUnitID",
                table: "Part.Assembly.Table",
                column: "RowUnitID");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_TableType",
                table: "Part.Assembly.Table",
                column: "TableType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_BID_ColumnVariableID",
                table: "Part.Assembly.Table",
                columns: new[] { "BID", "ColumnVariableID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_BID_RowVariableID",
                table: "Part.Assembly.Table",
                columns: new[] { "BID", "RowVariableID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_Subassembly",
                table: "Part.Assembly.Table",
                columns: new[] { "BID", "AssemblyID", "Label" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType1",
                table: "Part.Assembly.Table",
                column: "ColumnMatchType",
                principalTable: "enum.Part.Subassembly.Table.MatchType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_ColumnUnit",
                table: "Part.Assembly.Table",
                column: "ColumnUnitID",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType",
                table: "Part.Assembly.Table",
                column: "RowMatchType",
                principalTable: "enum.Part.Subassembly.Table.MatchType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_RowUnit",
                table: "Part.Assembly.Table",
                column: "RowUnitID",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.TableType",
                table: "Part.Assembly.Table",
                column: "TableType",
                principalTable: "enum.Part.Subassembly.TableType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Data",
                table: "Part.Assembly.Table",
                columns: new[] { "BID", "AssemblyID" },
                principalTable: "Part.Subassembly.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Variable1",
                table: "Part.Assembly.Table",
                columns: new[] { "BID", "ColumnVariableID" },
                principalTable: "Part.Subassembly.Variable",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Variable",
                table: "Part.Assembly.Table",
                columns: new[] { "BID", "RowVariableID" },
                principalTable: "Part.Subassembly.Variable",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType1",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_ColumnUnit",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_RowUnit",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_enum.Part.Subassembly.TableType",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Data",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Variable1",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_Part.Subassembly.Variable",
                table: "Part.Assembly.Table");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Part.Assembly.Table",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_ColumnMatchType",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_ColumnUnitID",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_RowMatchType",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_RowUnitID",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_TableType",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_BID_ColumnVariableID",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_BID_RowVariableID",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_Subassembly",
                table: "Part.Assembly.Table");

            migrationBuilder.AlterColumn<byte>(
                name: "RowUnitID",
                table: "Part.Assembly.Table",
                type: "tinyint SPARSE",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDT",
                table: "Part.Assembly.Table",
                type: "datetime2(2)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldDefaultValueSql: "GetUTCDate()");

            migrationBuilder.AlterColumn<bool>(
                name: "IsTierTable",
                table: "Part.Assembly.Table",
                nullable: false,
                oldClrType: typeof(bool),
                oldComputedColumnSql: "(isnull(case when [TableType]>=(1) AND [TableType]<=(4) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Part.Assembly.Table",
                type: "nvarchar(4000)",
                maxLength: 4000,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "varchar(4000)",
                oldMaxLength: 4000,
                oldNullable: true);

            migrationBuilder.AlterColumn<byte>(
                name: "ColumnUnitID",
                table: "Part.Assembly.Table",
                type: "tinyint SPARSE",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CellDataJSON",
                table: "Part.Assembly.Table",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<short>(
                name: "ID",
                table: "Part.Assembly.Table",
                nullable: false,
                oldClrType: typeof(short))
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Part.Assembly.Table",
                table: "Part.Assembly.Table",
                column: "ID");
        }
    }
}
