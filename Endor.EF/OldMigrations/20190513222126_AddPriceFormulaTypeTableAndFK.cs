using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddPriceFormulaTypeTableAndFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "PriceFormulaType",
                table: "Part.Subassembly.Data",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.CreateTable(
                name: "enum.Assembly.PriceFormula",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Assembly.PriceFormula", x => x.ID);
                });

            migrationBuilder.Sql(@"
            INSERT [enum.Assembly.PriceFormula] ([ID], [Name])
            VALUES (0, N'Fixed')
                , (1, N'Price Table')
                , (2, N'Discount Table')
                , (3, N'Margin Table')
                , (4, N'Markup Table')
                , (255, N'Custom')
            ;

            UPDATE [Part.Subassembly.Data] SET PriceFormulaType = 255;
");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Data_PriceFormulaType",
                table: "Part.Subassembly.Data",
                column: "PriceFormulaType");

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Data_enum.Assembly.PriceFormula",
                table: "Part.Subassembly.Data",
                column: "PriceFormulaType",
                principalTable: "enum.Assembly.PriceFormula",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Data_enum.Assembly.PriceFormula",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropTable(
                name: "enum.Assembly.PriceFormula");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Data_PriceFormulaType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "PriceFormulaType",
                table: "Part.Subassembly.Data");
        }
    }
}
