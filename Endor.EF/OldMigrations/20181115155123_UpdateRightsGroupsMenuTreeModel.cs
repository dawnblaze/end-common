using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateRightsGroupsMenuTreeModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                IF COL_LENGTH('System.Rights.Group.Menu.Tree', 'RightsGroupMenuTreeID') IS NOT NULL
                BEGIN
                    ALTER TABLE [System.Rights.Group.Menu.Tree] DROP COLUMN RightsGroupMenuTreeID
                END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
