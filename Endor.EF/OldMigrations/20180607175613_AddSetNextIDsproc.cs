using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180607175613_AddSetNextIDsproc")]
    public partial class AddSetNextIDsproc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- 
-- Name: Util.ID.SetID( @BID smallint, @ClassTypeID int, @NextID int, @AvoidLowering bit )
--
-- Description: This Function Sets the Next ID for a ClassType (Table).
--
-- Sample Use:   
--      exec dbo.[Util.ID.SetID] @BID=100, @ClassTypeID=3500, @NextID=1234, @AvoidLowering=0
--
-- ========================================================
CREATE PROCEDURE [Util.ID.SetID] 
        @BID smallint
      , @ClassTypeID int
      , @NextID int
      , @AvoidLowering bit
AS
BEGIN
    IF (@BID IS NULL) SET @BID = -1; -- Use -1 for System

    DECLARE @CanSet BIT = 1;

    IF (ISNULL(@AvoidLowering, 0) = 1)
    BEGIN
        DECLARE @CurrentValue INT = (SELECT NextID FROM [Util.NextID] WHERE BID = @BID AND ClassTypeID = @ClassTypeID);

        SET @CanSet = CASE WHEN ( ISNULL(@CurrentValue, -1) > @NextID ) THEN 0 ELSE 1 END;
    END;

    IF (@CanSet = 1)
    BEGIN
        -- Assume the record exists ... 
        UPDATE [Util.NextID]
        SET NextID = @NextID
        WHERE BID = @BID AND ClassTypeID = @ClassTypeID
        ;

        -- Check if no rows found, in which case use an add
        IF (@@RowCount=0)
        BEGIN
            INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) VALUES(@BID, @ClassTypeID, @NextID);
        END;
    END;
END;
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE [Util.ID.SetID];
");
        }
    }
}

