using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_PaymentMaster_List_Filter_Criteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                VALUES
                    (8010 --<TargetClassTypeID, int,>
                    ,'Company' --<Name, varchar(255),>
                    ,'Company' --<Label, varchar(255),>
                    ,'CompanyName' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,1 --<DataType, tinyint,>
                    ,0 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,NULL --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,0 --<IsLimitToList, bit,>
                    ,1 --<SortIndex, tinyint,>
                    ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                    );
                GO

                INSERT INTO [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                VALUES
                    (8010 --<TargetClassTypeID, int,>
                    ,'Location' --<Name, varchar(255),>
                    ,'Location' --<Label, varchar(255),>
                    ,'LocationID' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,2 --<DataType, tinyint,>
                    ,4 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,NULL --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,0 --<IsLimitToList, bit,>
                    ,2 --<SortIndex, tinyint,>
                    ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                    );
                GO

                INSERT INTO [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                VALUES
                    (8010 --<TargetClassTypeID, int,>
                    ,'OrderNumber' --<Name, varchar(255),>
                    ,'Order/Invoice #' --<Label, varchar(255),>
                    ,'Order' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,1 --<DataType, tinyint,>
                    ,0 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,NULL --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,0 --<IsLimitToList, bit,>
                    ,3 --<SortIndex, tinyint,>
                    ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                    );
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
