using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180223175045_TaxGroupTaxItemOptionCategory")]
    public partial class TaxGroupTaxItemOptionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"  
if not exists (SELECT ID from [System.Option.Category] where ID = 11 and name = 'Payment Type')
	INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
	VALUES (11, 'Payment Type', 1, 'Specify which payment types you accept.', 4, 0)

if not exists (SELECT ID from [System.Option.Category] where ID = 12 and name = 'Payment Terms')
	INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
	VALUES (12, 'Payment Terms', 1, 'Create and manage customer payment terms.', 4, 0)

if not exists (SELECT ID from [System.Option.Category] where ID = 21 and name = 'Tax Groups')
	INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
	VALUES (21, 'Tax Groups', 1, 'Tax Groups Applied to Customers and Orders', 4, 0)

if not exists (SELECT ID from [System.Option.Category] where ID = 22 and name = 'Tax Items')
	INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
	VALUES (22, 'Tax Items', 1, 'Individual Tax Assessments for different Tax Agencies', 4, 0)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Category] where ID = 11 and name = 'Payment Type';
DELETE FROM [System.Option.Category] where ID = 12 and name = 'Payment Terms';
DELETE FROM [System.Option.Category] where ID = 22 and name = 'Tax Items';
DELETE FROM [System.Option.Category] where ID = 21 and name = 'Tax Groups';
");
        }
    }
}

