using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class RemoveAssociationDataTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Association.Data");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Association.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AssociationType = table.Column<byte>(nullable: false),
                    CampaignID = table.Column<int>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1600))"),
                    CompanyID = table.Column<int>(nullable: true),
                    ContactID = table.Column<int>(nullable: true),
                    DocumentID = table.Column<int>(nullable: true),
                    EmployeeID = table.Column<int>(nullable: true),
                    LeadID = table.Column<int>(nullable: true),
                    OpportunityID = table.Column<int>(nullable: true),
                    TeamID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Association.Data", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Contact",
                table: "Association.Data",
                column: "ContactID");

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Campaign",
                table: "Association.Data",
                columns: new[] { "BID", "CampaignID" });

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Company",
                table: "Association.Data",
                columns: new[] { "BID", "CompanyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Document",
                table: "Association.Data",
                columns: new[] { "BID", "DocumentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Employee",
                table: "Association.Data",
                columns: new[] { "BID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Lead",
                table: "Association.Data",
                columns: new[] { "BID", "LeadID" });

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Opportunity",
                table: "Association.Data",
                columns: new[] { "BID", "OpportunityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Team",
                table: "Association.Data",
                columns: new[] { "BID", "TeamID" });
        }
    }
}
