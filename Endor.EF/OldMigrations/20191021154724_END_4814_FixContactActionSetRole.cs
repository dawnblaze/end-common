﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_4814_FixContactActionSetRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
/*
 Name: [Contact.Action.SetRole]( @BID tinyint, @ContactID int, @CompanyID, @RoleID )

 Description: For the specified Contact and Company, this 
 this function sets the specified Role(s) on the Contact on that Company
 and clears the roles on any other contacts for that company.

 Sample Use:   
    -- Set contact to both Primary
    EXEC dbo.[Contact.Action.SetRole] @BID=1, @ContactID=1040, @CompanyID = 1003, @RoleID = 1

    -- Set contact to both Billing and Primary
    EXEC dbo.[Contact.Action.SetRole] @BID=1, @ContactID=1040, @CompanyID = 1003, @RoleID = 3

    -- Set contact Inactive (can't be primary or billing)
    EXEC dbo.[Contact.Action.SetRole] @BID=1, @ContactID=1040, @CompanyID = 1003, @RoleID = 0

 Notes on RoleID
    00000001 = Primary
    00000010 = Billing
    00000011 = Primary | Billing = Both
    01000000 = Observer
    00000000 = Inactive

*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Contact.Action.SetRole]
-- DECLARE 
          @BID            SMALLINT -- = 1
        , @ContactID      INT     -- = 1040
        , @CompanyID      INT     = NULL 
        , @RoleID         TINYINT
        , @Result         INT     = NULL  -- OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- See how many companies are linked to this contact.  If no company is specified, will count all
    DECLARE @CompanyCount INT = 
                    (   SELECT COUNT(*)
                        FROM [Company.Contact.Link] 
                        WHERE BID = @BID 
                            AND CompanyID = COALESCE(@CompanyID, CompanyID)
                            AND ContactID = @ContactID );

    -- Check if the Company is specified is valid, and is associated with the Contact
    IF (@CompanyID IS NOT NULL) AND (@CompanyCount = 0)
    BEGIN
        SELECT @Result = 0
             , @Message = CONCAT('Company or Contact do not exist or are Not Associated with the each other. CompanyID=',@CompanyID, ' ContactID=', @ContactID)
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Company is not specified and there is more than one contact
    IF (@CompanyID IS NULL) AND (@CompanyCount <> 1)
    BEGIN
        SELECT @Result = 0
             , @Message = CONCAT('The Specified Contact does not exist or is associated with multiple Companies.  You must Specify a company when Company when setting the billing contact. ContactID=',@ContactID)
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Load the Company if not specified (since there can only be one at this point if not specified)
    IF (@CompanyID IS NULL) 
        SELECT @CompanyID = CompanyID 
        FROM [Company.Contact.Link] 
        WHERE BID = @BID 
          AND ContactID = @ContactID        
        ;

    -- Check if we are setting the primary (1) or billing (2) contact to inactive
    IF (@RoleID = 0) 
      AND Exists(   SELECT *
                    FROM [Company.Contact.Link] 
                    WHERE BID = @BID 
                      AND ContactID = @ContactID
                      AND CompanyID = @CompanyID
                      AND ((Roles & 3) != 0) -- Check if primary or billing
                    )
    BEGIN
        SELECT @Result = 0
             , @Message = CONCAT('You can''t set the primary or billing contact inactive without first setting another contact to those roles. ContactID=',@ContactID)
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ClearMask TINYINT = 255
          , @ForceActive BIT = 0;

    -- For primary role, we want to need to clear those flags from others.
    IF (@RoleID & 1 != 0)
        SELECT @ClearMask = @ClearMask & (255-1);
        
    -- For billing role, we want to need to clear those flags from others.
    IF (@RoleID & 2 != 0)
        SELECT @ClearMask = @ClearMask & (255-2);

    -- Can't set to Observer plus another Role
    IF (@RoleID & 128 = 128) AND (@RoleID != 128) 
        SELECT @RoleID = @RoleID & (255-128);

    -- Set Role from the appropriate contact
    --      Clear the Observer Role (128) if assigned
    --      Set the new Role
    UPDATE CL
    SET Roles = (CASE 
                    WHEN ContactID = @ContactID 
                    -- if trying to set to observer and have something besides observer, remove observer else clear observer and add new role(s)
                    THEN IIF((@RoleID & 128 !=0) AND ((Roles | @RoleID) & (255-128) != 0), (Roles | @RoleID) & (255-128), ((Roles & (255-128)) | @RoleID))
                    ELSE (CASE 
                            WHEN (Roles & @ClearMask = 0)
                            THEN 128 
                            ELSE (Roles & @ClearMask) 
                          END) 
                 END)
    FROM [Company.Contact.Link] CL
    WHERE BID = @BID 
        AND CompanyID = @CompanyID
    ;

    -- Update ModifiedDT on Contacts for that Company
    UPDATE Con
    SET ModifiedDT = GETUTCDATE()
    FROM [Company.Contact.Link] CL
    JOIN [Contact.Data] Con on CL.BID = Con.BID AND CL.ContactID = Con.ID
    WHERE CL.BID = @BID 
        AND CL.CompanyID = @CompanyID
    ;

	SET @Result = @@ROWCOUNT
    RETURN @Result;
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Contact.Action.SetRole]( @BID tinyint, @ContactID int, @CompanyID, @RoleID )
--
-- Description: For the specified Contact and Company, this 
-- this function sets the specified Role(s) on the Contact on that Company
-- and clears the roles on any other contacts for that company.
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetRole] @BID=1, @ContactID=1040, @CompanyID = 1003, @RoleID = 2
-- ========================================================
ALTER   PROCEDURE [dbo].[Contact.Action.SetRole]
-- DECLARE 
          @BID            SMALLINT -- = 1
        , @ContactID      INT     -- = 1040
        , @CompanyID      INT     = NULL 
        , @RoleID         TINYINT 
        , @Result         INT     = NULL  -- OUTPUT
AS
BEGIN

    DECLARE @Message VARCHAR(1024);

    -- See how many companies are linked to this contact.  If no company is specified, will count all
    DECLARE @CompanyCount INT = 
                    (   SELECT COUNT(*)
                        FROM [Company.Contact.Link] 
                        WHERE BID = @BID 
                            AND CompanyID = COALESCE(@CompanyID, CompanyID)
                            AND ContactID = @ContactID );

    -- Check if the Company is specified is valid, and is associated with the Contact
    IF (@CompanyID IS NOT NULL) AND (@CompanyCount = 0)
    BEGIN
        SELECT @Result = 0
             , @Message = CONCAT('Company or Contact do not exist or are Not Associated with the each other. CompanyID=',@CompanyID, ' ContactID=', @ContactID)
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Company is not specified and there is more than one contact
    IF (@CompanyID IS NULL) AND (@CompanyCount <> 1)
    BEGIN
        SELECT @Result = 0
             , @Message = CONCAT('The Specified Contact does not exist or is associated with multiple Companies.  You must Specify a company when Company when setting the billing contact. ContactID=',@ContactID)
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Load the Company if not specified (since there can only be one at this point if not specified)
    IF (@CompanyID IS NULL) 
        SELECT @CompanyID = CompanyID 
        FROM [Company.Contact.Link] 
        WHERE BID = @BID 
          AND ContactID = @ContactID        
        ;

    -- Set IsPrimary from the appropriate contact
    --      Clear the Observer Role (128) if assigned
    --      Set the new Role
    UPDATE CL
    SET Roles = (CASE 
                    WHEN ContactID = @ContactID 
                    THEN ((Roles & (255-128)) | @RoleID)
                    ELSE (CASE 
                            WHEN Roles = @RoleID
                            THEN 128 
                            ELSE (Roles & (255-@RoleID)) 
                          END) 
                 END)
    FROM [Company.Contact.Link] CL
    WHERE BID = @BID 
        AND CompanyID = @CompanyID
    ;

    -- Update ModifiedDT on Contacts for that Company
    UPDATE Con
    SET ModifiedDT = GETUTCDATE()
    FROM [Company.Contact.Link] CL
    JOIN [Contact.Data] Con on CL.BID = Con.BID AND CL.ContactID = Con.ID
    WHERE CL.BID = @BID 
        AND CL.CompanyID = @CompanyID
    ;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END;
");
        }
    }
}
