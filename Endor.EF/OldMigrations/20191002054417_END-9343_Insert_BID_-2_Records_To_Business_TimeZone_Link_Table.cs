﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9343_Insert_BID_2_Records_To_Business_TimeZone_Link_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*
             * 35 = Eastern Time
             * 20 = Central Time
             * 10 = Mountain Time
             * 4 = Pacific Time
             * 3 = Alaska Time
             * 2 = Hawaii Time
             * 15 = Arizona
             * 40 = Indiana
             */
            migrationBuilder.Sql(@"
                INSERT INTO [Business.Timezone.Link] ([BID], [TimeZoneID]) VALUES
                    (-2, 35),
                    (-2, 20),
                    (-2, 10),
                    (-2, 4),
                    (-2, 3),
                    (-2, 2),
                    (-2, 15),
                    (-2, 40)
                GO
                EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName='Business.TimeZone.Link'
                GO
            ;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            /*
             * 35 = Eastern Time
             * 20 = Central Time
             * 10 = Mountain Time
             * 4 = Pacific Time
             * 3 = Alaska Time
             * 2 = Hawaii Time
             * 15 = Arizona
             * 40 = Indiana
             */
            migrationBuilder.Sql(@"
                DELETE FROM [Business.Timezone.Link] WHERE [TimeZoneID] IN (35, 20, 10, 4, 3, 2, 15, 40);
            ");
        }
    }
}
