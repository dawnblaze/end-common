using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180606234029_ListFilterCriteria_EmployeeTeam")]
    public partial class ListFilterCriteria_EmployeeTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (5020,'Name','Name','Name',0,0,0,0,NULL,NULL,0,1, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)

INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (5020,'Member','Member','EmployeeName',0,0,0,0,NULL,NULL,0,2, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)

INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (5020,'Location','Location','Location',0,0,0,0,NULL,NULL,0,3, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)

INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (5020,'Is Active','Is Active','IsActive',0,3,2,0,'Is Not Active,Is Active',NULL,1,4, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE [Dev.Endor.Business.DB1].[dbo].[System.List.Filter.Criteria]  where TargetClassTypeID=5020
        ");
        }
    }
}

