using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180705183609_UpdateCFDefSearchListCriteria")]
    public partial class UpdateCFDefSearchListCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter.Criteria] SET [Field]='AppliesTo', [DataType]=0, [InputType]=12, [ListValues]='Company,Contact,Employee,Location,Order', IsLimitToList=1
WHERE [TargetClassTypeID]=15025 AND [Name]='AppliesTo';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter.Criteria] SET [Field]='AppliesTo', [DataType]=1, [InputType]=4, [ListValues]=NULL, IsLimitToList=0
WHERE [TargetClassTypeID]=15025 AND [Name]='AppliesTo';
");
        }
    }
}

