using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class SystemSubassemblyVariableInsertData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                SET IDENTITY_INSERT [System.Part.Subassembly.Variable] ON
                INSERT [System.Part.Subassembly.Variable] ([ID], [ModifiedDT], [VariableName], [ElementType], [Datatype], [DefaultValue], [IsFormula], [IsRequired], [UnitID], [ListDataType], [ListValuesJSON], [AllowCustomValue], [AllowMultiSelect], [GroupOptionsByCategory], [Label], [LabelType], [AltText], [ElementUseCount], [Tooltip], [AllowDecimals], [DisplayFormat], [DecimalPlaces])
                VALUES (1, CAST(N'2018-12-20T17:05:20.2300000' AS DateTime2), N'Quantity', 1, 2, N'=Item.Quantity', 1, 1, 21, NULL, NULL, NULL, NULL, NULL, N'Item Quantity', NULL, NULL, 0, N'This is the line item Quantity.', 0, NULL, NULL)
                     , (2, CAST(N'2018-12-20T17:10:50.2300000' AS DateTime2), N'Tier', 1, 1, N'=Order.Tier.Name', 1, 1, 21, NULL, NULL, NULL, NULL, NULL, N'Tier', 0, NULL, 0, N'This is the Pricing Tier being applied to the order for this Company.', NULL, NULL, NULL)
                     , (6, CAST(N'2018-12-20T17:06:26.1300000' AS DateTime2), N'Height', 21, 2, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'Height', NULL, NULL, 0, NULL, 1, 1, 2)
                     , (7, CAST(N'2018-12-20T17:06:26.1300000' AS DateTime2), N'Width', 21, 2, NULL, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'Width', NULL, NULL, 0, NULL, 1, 1, 2)
                     , (8, CAST(N'2018-12-20T17:20:51.7700000' AS DateTime2), N'Area', 1, 2, N'=Height.Value * Width.Value', 1, 1, 121, NULL, NULL, NULL, NULL, NULL, N'Area', NULL, NULL, 0, NULL, NULL, NULL, NULL)
                SET IDENTITY_INSERT [System.Part.Subassembly.Variable] OFF
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
