using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddGLData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounting.GLData",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AccountingDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    ActivityGlactivityBID = table.Column<short>(nullable: true),
                    ActivityGlactivityID = table.Column<int>(nullable: true),
                    ActivityID = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18, 4)", nullable: false, defaultValue: 0m),
                    AssemblyID = table.Column<int>(type: "int SPARSE", nullable: true),
                    AssetID = table.Column<int>(type: "int SPARSE", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((8001))"),
                    CompanyID = table.Column<int>(nullable: true),
                    Credit = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    CurrencyType = table.Column<byte>(nullable: false),
                    Debit = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    DestinationID = table.Column<int>(type: "int SPARSE", nullable: true),
                    GLAccountID = table.Column<int>(nullable: false),
                    IsOffBS = table.Column<bool>(nullable: false, defaultValue: false),
                    IsTaxable = table.Column<bool>(nullable: false, defaultValue: false),
                    ItemComponentID = table.Column<int>(type: "int SPARSE", nullable: true),
                    LaborID = table.Column<int>(type: "int SPARSE", nullable: true),
                    LocationID = table.Column<byte>(nullable: false),
                    MachineID = table.Column<short>(type: "smallint SPARSE", nullable: true),
                    MaterialID = table.Column<int>(type: "int SPARSE", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    OrderID = table.Column<int>(nullable: true),
                    OrderItemID = table.Column<int>(type: "int SPARSE", nullable: true),
                    PlaceID = table.Column<int>(type: "int SPARSE", nullable: true),
                    RecordedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    TaxGroupID = table.Column<short>(nullable: true),
                    ValidToDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.GLData", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_GL.Data_enum.Accounting.CurrencyTypet",
                        column: x => x.CurrencyType,
                        principalTable: "enum.Accounting.CurrencyType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.GLData_Activity.GLActivity_ActivityGlactivityBID_ActivityGlactivityID",
                        columns: x => new { x.ActivityGlactivityBID, x.ActivityGlactivityID },
                        principalTable: "Activity.GLActivity",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.GL.Data_Activity.GL",
                        columns: x => new { x.BID, x.ActivityID },
                        principalTable: "Activity.GLActivity",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_OrderDestination.Data",
                        columns: x => new { x.BID, x.DestinationID },
                        principalTable: "Order.Destination.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.GLData_Accounting.GL.Account_BID_GLAccountID",
                        columns: x => new { x.BID, x.GLAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_Labor.Data",
                        columns: x => new { x.BID, x.LaborID },
                        principalTable: "Part.Labor.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_Machine.Data",
                        columns: x => new { x.BID, x.MachineID },
                        principalTable: "Part.Machine.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_Material.Data",
                        columns: x => new { x.BID, x.MaterialID },
                        principalTable: "Part.Material.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_Order.Data",
                        columns: x => new { x.BID, x.OrderID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_OrderItem.Data",
                        columns: x => new { x.BID, x.OrderItemID },
                        principalTable: "Order.Item.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GL.Data_Accounting.Tax.Group",
                        columns: x => new { x.BID, x.TaxGroupID },
                        principalTable: "Accounting.Tax.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_CurrencyType",
                table: "Accounting.GLData",
                column: "CurrencyType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_ActivityGlactivityBID_ActivityGlactivityID",
                table: "Accounting.GLData",
                columns: new[] { "ActivityGlactivityBID", "ActivityGlactivityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_ActivityID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "ActivityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_CompanyID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "CompanyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_DestinationID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "DestinationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_GLAccountID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "GLAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_LaborID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "LaborID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_LocationID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_MachineID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "MachineID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_MaterialID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "MaterialID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_OrderID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "OrderID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_OrderItemID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "OrderItemID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_TaxGroupID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "TaxGroupID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounting.GLData");
        }
    }
}
