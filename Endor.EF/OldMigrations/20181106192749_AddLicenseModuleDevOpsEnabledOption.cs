using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddLicenseModuleDevOpsEnabledOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    INSERT INTO [dbo].[System.Option.Definition]
                        ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden])
                    VALUES
                        (20027, 'License.Module.DevOps.Enabled', 'Dev Ops Module Enabled', '', 3, 15102, NULL, 0, 1);
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    DELETE [dbo].[System.Option.Definition] WHERE [ID]=20027 and [Name]='License.Module.DevOps.Enabled'
                "
            );
        }
    }
}
