using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class ADD_COLUMN_AssemblyVariable_RollupLinkedPriceAndCost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "RollupLinkedPriceAndCost",
                table: "Part.Subassembly.Variable",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RollupLinkedPriceAndCost",
                table: "Part.Subassembly.Variable");
        }
    }
}
