using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180123233325_enumCRMCompanyStatusDataInsert")]
    public partial class enumCRMCompanyStatusDataInsert : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("enum.CRM.Company.Status", new string[]
            { "ID", "Name" }, new object[,] {
            {1 ,  "Lead"      },  //The Company has been created, but no Opportunities have been created for the Company.
            {2 ,  "Prospect"  },  //The Company has been created and has or had one or more Opportunities, but no Orders.
            {4 ,  "Customer"  },  //The Company has has one or more orders.
            {8 ,  "Vendor"    },  //The Company is a vendor that the Business has or could order from.
            {16,  "Personal"  }   //The Company has non - business uses for one or more user.
            });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData("enum.CRM.Company.Status", "ID", new object[] { 1, 2, 4, 8, 16 });
        }
    }
}

