using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_OrderItemComponent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE TABLE [Order.Item.Component](
    [BID] [smallint] NOT NULL,
    [ID] [int] NOT NULL,
    [ClassTypeID]  AS ((10021)),
    [ModifiedDT] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT (SYSUTCDATETIME()),
    [ValidToDT] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL DEFAULT (CONVERT([datetime2](7),'9999-12-31 23:59:59.99999999')),
    [OrderID] [int] NOT NULL,
    [OrderItemID] [int] NOT NULL,
    [ParentComponentID] [int] SPARSE  NULL,
    [Number] [smallint] NOT NULL,
    [Name] [nvarchar](255) NOT NULL,
    [Quantity.Estimated] [decimal](18, 6) NULL,
    [Quantity.Estimated.OV] [bit] NOT NULL DEFAULT ((0)),
    [Quantity.Actual] [decimal](18, 6) SPARSE  NULL,
    [Quantity.Actual.OV] [bit] NOT NULL DEFAULT ((0)),
    [Quantity.Remaining] [decimal](18, 6) SPARSE  NULL,
    [Quantity]  AS (isnull([Quantity.Actual],[Quantity.Estimated])),
    [Quantity.UnitID] TINYINT NULL,
    [Description] [nvarchar](2048) NULL,
    [MaterialID] [int] SPARSE  NULL,
    [LaborID] [int] SPARSE  NULL,
    [MachineID] [smallint] SPARSE  NULL,
    [AssetID] [int] SPARSE  NULL,
    [PlaceID] [int] SPARSE  NULL,
    [AssemblyID] [int] SPARSE  NULL,
    [AssemblyVariableID] [int] SPARSE  NULL,
    [AssemblyDataJSON] [varchar](max) SPARSE  NULL,
    [Price.Unit] [decimal](18, 6) NULL,
    [Price.UnitOV] [bit] NOT NULL DEFAULT ((0)),
    [Price.Net]  AS ([Price.Unit]*[Quantity.Estimated]),
    [IncomeAccountID] [int] NULL,
    [Cost.Unit] [decimal](18, 6) NULL,
    [Cost.UnitOV] [bit] NOT NULL DEFAULT ((0)),
    [Cost.Net.Estimated]  AS ([Quantity.Estimated]*[Cost.Unit]),
    [Cost.Net.Actual]  AS ([Quantity.Actual]*[Cost.Unit]),
    [Cost.Net]  AS (isnull([Quantity.Actual],[Quantity.Estimated])*[Cost.Unit]),
    [ExpenseAccountID] [int] NULL,
    [IsMaterial]  AS (isnull(case when [MaterialID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsLabor]  AS (isnull(case when [LaborID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsMachine]  AS (isnull(case when [MachineID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsAsset]  AS (isnull(case when [AssetID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsPlace]  AS (isnull(case when [PlaceID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsAssembly]  AS (isnull(case when [AssemblyID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsLinkedAssembly]  AS (isnull(case when [ParentComponentID] IS NULL AND [AssemblyID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsLinkedMachine]  AS (isnull(case when [ParentComponentID] IS NULL AND [MachineID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [ComponentTypeText]  AS (case when [MaterialID] IS NOT NULL then 'Material' when [LaborID] IS NOT NULL then 'Labor' when [MachineID] IS NOT NULL then 'Equipment' when [AssemblyID] IS NOT NULL then 'Assembly' when [AssetID] IS NOT NULL then 'Asset' when [PlaceID] IS NOT NULL then 'Place' else 'Unknown' end),
    CONSTRAINT [PK_Order.Item.Component] PRIMARY KEY ( BID, ID ),
    PERIOD FOR SYSTEM_TIME(ModifiedDT, ValidToDT)
) 
;
CREATE INDEX [IX_Order.Item.Component_Assembly] ON [Order.Item.Component] ( [BID] ASC, [AssemblyID] ASC )
WHERE ([AssemblyID] IS NOT NULL)
;
CREATE INDEX [IX_Order.Item.Component_Asset] ON [Order.Item.Component] ( [BID] ASC, [AssetID] ASC )
WHERE ([AssetID] IS NOT NULL)
;
CREATE INDEX [IX_Order.Item.Component_Labor] ON [Order.Item.Component] ( [BID] ASC, [LaborID] ASC )
WHERE ([LaborID] IS NOT NULL)
;
CREATE INDEX [IX_Order.Item.Component_Machine] ON [Order.Item.Component] ( [BID] ASC, [MachineID] ASC )
WHERE ([MachineID] IS NOT NULL)
;
CREATE INDEX [IX_Order.Item.Component_Material] ON [Order.Item.Component] ( [BID] ASC, [MaterialID] ASC )
WHERE ([MaterialID] IS NOT NULL)
;
CREATE INDEX [IX_Order.Item.Component_Order] ON [Order.Item.Component] ( [BID] ASC, [OrderID] ASC )
;
CREATE INDEX [IX_Order.Item.Component_OrderItem] ON [Order.Item.Component] ( [BID] ASC, [OrderItemID] ASC, [ParentComponentID] ASC, [Number] ASC )
;
CREATE INDEX [IX_Order.Item.Component_Place] ON [Order.Item.Component] ( [BID] ASC, [PlaceID] ASC )
WHERE ([PlaceID] IS NOT NULL)
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Accounting.GL.Account]
FOREIGN KEY([BID], [IncomeAccountID]) REFERENCES [Accounting.GL.Account] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Accounting.GL.Account1]
FOREIGN KEY([BID], [ExpenseAccountID]) REFERENCES [Accounting.GL.Account] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Order.Data]
FOREIGN KEY([BID], [OrderID]) REFERENCES [Order.Data] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Order.Item.Component]
FOREIGN KEY([BID], [ParentComponentID]) REFERENCES [Order.Item.Component] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Order.Item.Data]
FOREIGN KEY([BID], [OrderItemID]) REFERENCES [Order.Item.Data] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Part.Labor.Data]
FOREIGN KEY([BID], [LaborID]) REFERENCES [Part.Labor.Data] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Part.Machine.Data]
FOREIGN KEY([BID], [MachineID]) REFERENCES [Part.Machine.Data] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Part.Material.Data]
FOREIGN KEY([BID], [MaterialID]) REFERENCES [Part.Material.Data] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Part.Subassembly.Data]
FOREIGN KEY([BID], [AssemblyID]) REFERENCES [Part.Subassembly.Data] ([BID], [ID])
;
ALTER TABLE [Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_Part.Subassembly.Variable]
FOREIGN KEY([BID], [AssemblyVariableID]) REFERENCES [Part.Subassembly.Variable] ([BID], [ID])
;
ALTER TABLE [dbo].[Order.Item.Component] WITH CHECK
ADD CONSTRAINT [FK_Order.Item.Component_enum.Part.Unit]
FOREIGN KEY([Quantity.UnitID]) REFERENCES [dbo].[enum.Part.Unit] ([ID])
;
-- Turn on Temporal Versioning
ALTER TABLE [Order.Item.Component] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Item.Component]));
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE[Order.Item.Component] SET(SYSTEM_VERSIONING = OFF);
");
            migrationBuilder.DropTable(
                name: "Historic.Order.Item.Component");

            migrationBuilder.DropTable(
                name: "Order.Item.Component");
        }
    }
}
