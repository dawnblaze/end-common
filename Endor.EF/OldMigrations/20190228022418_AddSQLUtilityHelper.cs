using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddSQLUtilityHelper : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
/* 
    This procedure deletes ALL orders for a BID.  
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.Order.Delete] @BID = 1, @OrderID = 1013, @TestOnly = 1 ;

*/
CREATE OR ALTER PROCEDURE [Util.Order.Delete]( @BID SMALLINT, @OrderID INT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION;
    BEGIN TRY

        DELETE FROM [Accounting.GLData] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Contact.Locator] WHERE BID = @BID AND @OrderID IN (SELECT OrderID FROM [Order.Contact.Role] WHERE BID = @BID AND OrderID = @OrderID)
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Contact.Role] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Employee.Role] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Item.Component] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Item.Surcharge] WHERE BID = @BID AND @OrderID IN (SELECT OrderID FROM [Order.Item.Data] WHERE BID = @BID AND OrderID = @OrderID)
        DELETE FROM [Order.Item.TagLink] WHERE BID = @BID AND @OrderID IN (SELECT OrderID FROM [Order.Item.Data] WHERE BID = @BID AND OrderID = @OrderID)
        DELETE FROM [Order.KeyDate] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Note] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Tax.Item.Assessment] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Activity.GLActivity] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Custom.Data] WHERE BID = @BID AND ID = @OrderID
        DELETE FROM [Order.Item.Data] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.OrderLink] WHERE BID = @BID AND @OrderID IN (OrderID, LinkedOrderID)
        DELETE FROM [Order.TagLink] WHERE BID = @BID AND OrderID = @OrderID
        DELETE FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION;
            RETURN;
        END;

        COMMIT TRANSACTION;
    
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;

        THROW; -- Rethrow the error
    END CATCH

END;
GO
/* 
    This procedure deletes ALL orders for a BID.  
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.Order.Delete.All] @BID = 1, @TestOnly = 1 ;

*/
CREATE OR ALTER PROCEDURE [Util.Order.Delete.All]( @BID SMALLINT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION;
    BEGIN TRY

        DELETE FROM [Accounting.GLData] WHERE BID = @BID


        DELETE FROM [Accounting.GLData] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID 
        DELETE FROM [Accounting.Payment.Master] WHERE BID = @BID

        DELETE FROM [Order.Note] WHERE BID = @BID
        DELETE FROM [Order.KeyDate] WHERE BID = @BID

        DELETE FROM [Order.Employee.Role] WHERE BID = @BID

        DELETE FROM [Order.Contact.Locator] WHERE BID = @BID
        DELETE FROM [Order.Contact.Role] WHERE BID = @BID

        DELETE FROM [Order.OrderLink] WHERE BID = @BID

        DELETE FROM [Order.Custom.Data] WHERE BID = @BID

        DELETE FROM [Order.Item.Component] WHERE BID = @BID
        DELETE FROM [Order.Item.Surcharge] WHERE BID = @BID
        DELETE FROM [Order.Item.TagLink] WHERE BID = @BID
        DELETE FROM [Order.Tax.Item.Assessment] WHERE BID = @BID
        DELETE FROM [Order.Item.Data] WHERE BID = @BID

        DELETE FROM [Order.Data] WHERE BID = @BID

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION;
            RETURN;
        END;

        COMMIT TRANSACTION;
    
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;

        THROW; -- Rethrow the error
    END CATCH

END;
GO

/* 
    This procedure deletes a Location orders for a BID.  
    Any orders or companies that were for that Location are reset to the passed in LocationID
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.Location.Delete] @BID = 1, @LocationID = 41, @ResetToLocationID = 143, @TestOnly = 0 ;

*/
CREATE OR ALTER PROCEDURE [Util.Location.Delete]( @BID SMALLINT, @LocationID TINYINT, @ResetToLocationID INT, @TestOnly BIT = 0 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteLocation];
    BEGIN TRY
        -- Update Location Fields to the new value

        UPDATE [Accounting.GLData]              SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Accounting.Payment.Application] SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Accounting.Payment.Master]      SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Company.Data]                   SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Contact.Data]                   SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Document.Report.Menu]           SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Domain.Data]                    SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Employee.Data]                  SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Location.Goal]                  SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Opportunity.Data]               SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Order.Data]                     SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;
        UPDATE [Order.Data]                     SET PickupLocationID = @ResetToLocationID   WHERE BID = @BID AND PickupLocationID = @LocationID;
        UPDATE [Order.Data]                     SET ProductionLocationID = @ResetToLocationID WHERE BID = @BID AND ProductionLocationID = @LocationID;
        UPDATE [Order.Item.Data]                SET ProductionLocationID = @ResetToLocationID WHERE BID = @BID AND ProductionLocationID = @LocationID;

        -- For option tables, delete it if it is Option Specific else reset it.
        DELETE FROM [Option.Data]               WHERE BID = @BID AND LocationID = @LocationID AND OptionLevel = 8;
        UPDATE [Option.Data]                    SET LocationID = @ResetToLocationID         WHERE BID = @BID AND LocationID = @LocationID;

        -- For Link tables, remove the data.
        DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID AND LocationID = @LocationID;
        DELETE FROM [Domain.Email.LocationLink]         WHERE BID = @BID AND LocationDataBID = @LocationID;
        DELETE FROM [Domain.Email.LocationLink]         WHERE BID = @BID AND LocationDataID = @LocationID;
        DELETE FROM [Domain.Email.LocationLink]         WHERE BID = @BID AND LocationID = @LocationID;
        DELETE FROM [Employee.Team.LocationLink]        WHERE BID = @BID AND LocationID = @LocationID;
        DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID AND LocationID = @LocationID;

        -- Now Delete the Location Data

        DELETE FROM [Location.Locator] WHERE BID = @BID AND ParentID = @LocationID;
        DELETE FROM [Location.Goal] WHERE BID = @BID AND LocationID = @LocationID;
        DELETE FROM [Location.Data] WHERE BID = @BID AND ID = @LocationID;

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteLocation];
            RETURN;
        END;

        COMMIT TRANSACTION [DeleteLocation];
    
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteLocation];

        THROW; -- Rethrow the error
    END CATCH

END;
GO

/* 
    This procedure deletes a Custom Field Definition and layout information.  
    This does not delete the stored data, but it is not accessible by the system since
    the definition is no longer in the system.

    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.CustomFieldDef.Delete] @BID = 1, @CustomFieldDefID = 41, @TestOnly = 0 ;

*/
CREATE OR ALTER PROCEDURE [Util.CustomFieldDef.Delete]( @BID SMALLINT, @CustomFieldDefID SMALLINT, @TestOnly BIT = 0 )
AS
BEGIN
    DECLARE @AppliesToClassTypeID SMALLINT = ( SELECT AppliesToClassTypeID 
                                                FROM [CustomField.Definition]
                                                WHERE BID = @BID AND ID = @CustomFieldDefID );

    -- Check if anything found!
    IF (@AppliesToClassTypeID IS NULL)
        Return;

    BEGIN TRANSACTION [DeleteCustomFieldDef];
    BEGIN TRY

        --DELETE FROM [CustomField.Definition.Validation]
        --            [CustomField.Definition.Validation]
        --WHERE BID = @BID AND ValidatedID = @CustomFieldDefID
        --;

        DELETE FROM [CustomField.Layout.Element]
        WHERE BID = @BID AND DefinitionID = @CustomFieldDefID
        ;

        DELETE FROM [CustomField.Definition]
        WHERE BID = @BID AND ID = @CustomFieldDefID
        ;

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteCustomFieldDef];
            RETURN;
        END;

        COMMIT TRANSACTION [DeleteCustomFieldDef];
    
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteCustomFieldDef];

        THROW; -- Rethrow the error
    END CATCH

END;
GO

/* 
    Returns a hierachy of dependent tables that are dependent on the stipulated table
*/
CREATE OR ALTER FUNCTION dbo.[Util.Table.Dependency] (@TableName VARCHAR(255)) 
RETURNS TABLE 
AS
RETURN 
(
    with Fkeys as (

        select distinct

             OnTable       = OnTable.name
            ,AgainstTable  = AgainstTable.name 

        from 

            sysforeignkeys fk

            inner join sysobjects onTable 
                on fk.fkeyid = onTable.id

            inner join sysobjects againstTable  
                on fk.rkeyid = againstTable.id

        where 1=1
            AND AgainstTable.TYPE = 'U'
            AND OnTable.TYPE = 'U'
            AND OnTable.name NOT LIKE 'Business.Data'
            AND againstTable.name NOT LIKE 'Business.Data'
            -- ignore self joins; they cause an infinite recursion
            AND OnTable.Name <> AgainstTable.Name
        )

    ,MyData as (

        select 
             OnTable = o.name
            ,AgainstTable = FKeys.againstTable

        from 

            sys.objects o

            left join FKeys
                on  o.name = FKeys.onTable

        where 1=1
            and o.type = 'U'
            and o.name not like 'sys%'
        )

    ,MyRecursion as (

        -- base case
        select 
             TableName  = OnTable
            ,Lvl        = 1
            ,DepPath    = convert(varchar(max), OnTable)
            ,RootTable  = OnTable
        from
            MyData
        where 1=1
            and AgainstTable is null

        -- recursive case
        union all select
             TableName    = OnTable
            ,Lvl        = r.Lvl + 1
            ,DepPath    = convert(varchar(max), r.DepPath + '\' + OnTable)
            ,RootTable = r.RootTable
        from 
            MyData d
            inner join MyRecursion r
                on d.AgainstTable = r.TableName
    )
    SELECT TOP 100 PERCENT MAX(LevelNo) Level, TableName
    FROM 
    (
    select 
         lvl as LevelNo
        ,Level = CONCAT(replicate('  ', Lvl), Lvl)
        ,TableName
        ,RootTable
        ,DepPath
    from 
        MyRecursion
     where DepPath like '%'+@TableName+'%'
    ) Temp
    GROUP BY TableName
    ORDER BY Level DESC, TableName
);
GO

/* 
    This procedure deletes an ENTIRE BUSINESS from the database.
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.Business.Delete] @BID = 999, @TestOnly = 1 ;

*/
CREATE OR ALTER PROCEDURE [Util.Business.Delete]( @BID SMALLINT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteBusiness]
    BEGIN TRY

        DELETE FROM [Accounting.GLData] WHERE BID = @BID
        DELETE FROM [Order.Contact.Locator] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID
        DELETE FROM [Order.Contact.Role] WHERE BID = @BID
        DELETE FROM [Order.Employee.Role] WHERE BID = @BID
        DELETE FROM [Order.Item.Component] WHERE BID = @BID
        DELETE FROM [Order.Item.Surcharge] WHERE BID = @BID
        DELETE FROM [Order.Item.TagLink] WHERE BID = @BID
        DELETE FROM [Order.KeyDate] WHERE BID = @BID
        DELETE FROM [Order.Note] WHERE BID = @BID
        DELETE FROM [Order.Tax.Item.Assessment] WHERE BID = @BID
        DELETE FROM [Activity.GLActivity] WHERE BID = @BID
        DELETE FROM [Dashboard.Widget.Data] WHERE BID = @BID
        DELETE FROM [Order.Custom.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Data] WHERE BID = @BID
        DELETE FROM [Order.OrderLink] WHERE BID = @BID
        DELETE FROM [Order.TagLink] WHERE BID = @BID
        DELETE FROM [Dashboard.Data] WHERE BID = @BID
        DELETE FROM [Message.Delivery.Record] WHERE BID = @BID
        DELETE FROM [Message.Header] WHERE BID = @BID
        DELETE FROM [Order.Data] WHERE BID = @BID
        DELETE FROM [User.Draft] WHERE BID = @BID
        DELETE FROM [Alert.Definition.Action] WHERE BID = @BID
        DELETE FROM [Contact.Custom.Data] WHERE BID = @BID
        DELETE FROM [Contact.Locator] WHERE BID = @BID
        DELETE FROM [Contact.TagLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard.Detail] WHERE BID = @BID
        DELETE FROM [Message.Participant.Info] WHERE BID = @BID
        DELETE FROM [Opportunity.Data] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.CategoryLink] WHERE BID = @BID
        DELETE FROM [User.Link] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Master] WHERE BID = @BID
        DELETE FROM [Alert.Definition] WHERE BID = @BID
        DELETE FROM [Board.Employee.Link] WHERE BID = @BID
        DELETE FROM [Company.Custom.Data] WHERE BID = @BID
        DELETE FROM [Company.Locator] WHERE BID = @BID
        DELETE FROM [Company.TagLink] WHERE BID = @BID
        DELETE FROM [Contact.Data] WHERE BID = @BID
        --DELETE FROM [Document.Report.Menu] WHERE BID = @BID
        DELETE FROM [Employee.Locator] WHERE BID = @BID
        DELETE FROM [Employee.TeamLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard] WHERE BID = @BID
        DELETE FROM [List.Filter.EmployeeSubscription] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Element] WHERE BID = @BID
        DELETE FROM [Part.Surcharge.Data] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code.ItemExemptionLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID
        DELETE FROM [Company.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Element] WHERE BID = @BID
        DELETE FROM [Domain.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.LocationLink] WHERE BID = @BID
        DELETE FROM [Employee.Data] WHERE BID = @BID
        DELETE FROM [Employee.Team.LocationLink] WHERE BID = @BID
        DELETE FROM [Location.Goal] WHERE BID = @BID
        DELETE FROM [Location.Locator] WHERE BID = @BID
        DELETE FROM [Order.Destination.TagLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Machine.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Material.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Layout] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Variable] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Method] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Item] WHERE BID = @BID
        DELETE FROM [Board.Module.Link] WHERE BID = @BID
        DELETE FROM [Board.Role.Link] WHERE BID = @BID
        DELETE FROM [Board.View.Link] WHERE BID = @BID
        DELETE FROM [Campaign.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Container] WHERE BID = @BID
        DELETE FROM [List.Tag.OtherLink] WHERE BID = @BID
        DELETE FROM [Location.Data] WHERE BID = @BID
        DELETE FROM [Message.Object.Link] WHERE BID = @BID
        DELETE FROM [Order.Destination.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.Data] WHERE BID = @BID
        DELETE FROM [Part.Machine.Data] WHERE BID = @BID
        DELETE FROM [Part.Material.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Data] WHERE BID = @BID
        DELETE FROM [Rights.Group.List.RightsGroupLink] WHERE BID = @BID
        DELETE FROM [Accounting.GL.Account] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Term] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group] WHERE BID = @BID
        DELETE FROM [Activity.Action] WHERE BID = @BID
        DELETE FROM [Activity.Event] WHERE BID = @BID
        DELETE FROM [Board.Definition.Data] WHERE BID = @BID
        DELETE FROM [Board.View] WHERE BID = @BID
        DELETE FROM [Business.Locator] WHERE BID = @BID
        DELETE FROM [CRM.Industry] WHERE BID = @BID
        DELETE FROM [CRM.Origin] WHERE BID = @BID
        DELETE FROM [CustomField.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Other.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.Data] WHERE BID = @BID
        DELETE FROM [Employee.Role] WHERE BID = @BID
        DELETE FROM [Employee.Team] WHERE BID = @BID
        DELETE FROM [List.Filter] WHERE BID = @BID
        DELETE FROM [List.FlatList.Data] WHERE BID = @BID
        DELETE FROM [List.Tag] WHERE BID = @BID
        DELETE FROM [Message.Body] WHERE BID = @BID
        DELETE FROM [Option.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Status] WHERE BID = @BID
        DELETE FROM [Order.Item.SubStatus] WHERE BID = @BID
        DELETE FROM [Part.Labor.Category] WHERE BID = @BID
        DELETE FROM [Part.Machine.Category] WHERE BID = @BID
        DELETE FROM [Part.Material.Category] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Category] WHERE BID = @BID
        DELETE FROM [Report.Menu] WHERE BID = @BID
        DELETE FROM [Rights.Group.List] WHERE BID = @BID
        DELETE FROM [SSLCertificate.Data] WHERE BID = @BID
        DELETE FROM [Util.NextID] WHERE BID = @BID

        DELETE FROM [Business.Data] WHERE BID = @BID

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteBusiness]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteBusiness];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [DeleteBusiness]

END;
GO

/*
This stored procedure resets the NextID counters for a Business ID

Usage:
    EXEC dbo.[Util.ID.Reset.All] @BID = 1, @TestOnly = 1

*/
CREATE OR ALTER PROCEDURE [Util.ID.Reset.All] ( @BID SMALLINT, @TestOnly BIT = 0  )
AS 
BEGIN

    BEGIN TRANSACTION [ResetBusinessIDs]
    BEGIN TRY

        DECLARE @SQL VARCHAR(MAX) = '';

        DROP TABLE IF EXISTS ##MaxIDTable;

        -- Create a Global Temp Table - It must be Global so we can reference it via Dynamic SQL.  (yuck)
        CREATE TABLE ##MaxIDTable (BID SMALLINT, TableName VARCHAR(255), ClassTypeID SMALLINT, MaxID INT, SQLText VARCHAR(MAX));

        INSERT INTO ##MaxIDTable
            SELECT @BID, TableName, CONVERT(SMALLINT, REPLACE(REPLACE(ComputedFormula, '(', ''), ')', '')) AS ComputedID, NULL, NULL
            FROM TablesAndColumns T1
            WHERE ColumnName = 'ClassTypeID' AND IsComputed = 1
            AND TableName NOT LIKE '%Histor%'
            AND TableName NOT IN ('Order.Data', 'Util.NextID', '_Root.Data', 'Report.Menu')
            ORDER BY TableName
        ;

        -- Since the order table contains both orders and estimates, we have to update it manually
        INSERT INTO ##MaxIDTable SELECT @BID, 'Order.Data', 10000, NULL, NULL;

        -- Create the SQL Update query for each row
        UPDATE ##MaxIDTable
        SET SQLText = CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(ID) FROM [', TableName, '] WHERE BID = ',@BID ,' AND ClassTypeID = ', ClassTypeID, ' ) WHERE ClassTypeID = ', ClassTypeID, '; ')
        ;

        -- Now handle the Faux-IDs, OrderNumber, InvoiceNumber and EstimateNumber
        INSERT INTO ##MaxIDTable  (BID, TableName, ClassTypeID, MaxID, SQLText)
        VALUES ( @BID, 'OrderNumber'   , -10000, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(Number) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10000 ) WHERE ClassTypeID = ', -10000, '; ') )
                , ( @BID, 'InvoiceNumber' , -10003, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(InvoiceNumber) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10000 ) WHERE ClassTypeID = ', -10003, '; ') )
                , ( @BID, 'EstimateNumber', -10200, NULL, CONCAT('UPDATE ##MaxIDTable SET MaxID = (SELECT MAX(Number) FROM [Order.Data] WHERE BID = ',@BID ,' AND ClassTypeID = 10200 ) WHERE ClassTypeID = ', -10200, '; ') )
        ;

        -- Combine the SQL Updates into one massive query
        SELECT @SQL = CONCAT(@SQL, SQLText) FROM ##MaxIDTable;

        -- Now update the Table
        EXEC(@SQL);

        -- Set any NULL to 0 to start
        UPDATE ##MaxIDTable SET MaxID = 0 WHERE MaxID IS NULL;

        -- Preview the Results
        IF (@TestOnly = 1) SELECT * FROM ##MaxIDTable ORDER BY BID, ClassTypeID
        IF (@TestOnly = 1) SELECT * FROM [Util.NextID]  WHERE BID = @BID order by BID, ClassTypeID


        -- INSERT Records if they Don't exist
        INSERT INTO [Util.NextID]
            SELECT BID, ClassTypeID, MaxID+1
            FROM ##MaxIDTable T
            WHERE NOT EXISTS(SELECT * FROM [Util.NextID] T2 WHERE T.BID = T2.BID and T.ClassTypeID = T2.ClassTypeID)
        ;

        -- UPDATE Records if they are not correct
        UPDATE U
        SET NextID = T.MaxID + 1
        FROM [Util.NextID] U
        JOIN ##MaxIDTable T ON U.BID = T.BID and U.ClassTypeID = T.ClassTypeID
        WHERE T.MaxID+1 != COALESCE(U.NextID, -1)
        ;

        IF (@TestOnly = 1) SELECT * FROM [Util.NextID] WHERE BID = @BID order by BID, ClassTypeID

        DROP TABLE ##MaxIDTable;

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [ResetBusinessIDs]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [ResetBusinessIDs];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [ResetBusinessIDs]
END;
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
DROP PROCEDURE IF EXISTS dbo.[Util.Order.Delete];  
GO
DROP PROCEDURE IF EXISTS dbo.[Util.Order.Delete.All];  
GO
DROP PROCEDURE IF EXISTS dbo.[Procedure Util.Location.Delete];  
GO
DROP PROCEDURE IF EXISTS dbo.[Util.CustomFieldDef.Delete];  
GO
DROP FUNCTION IF EXISTS dbo.[Util.Table.Dependency];  
GO
DROP PROCEDURE IF EXISTS dbo.[Util.Business.Delete];  
GO
DROP PROCEDURE IF EXISTS dbo.[ Util.ID.Reset.All];  
GO
                "
            );
        }
    }
}
