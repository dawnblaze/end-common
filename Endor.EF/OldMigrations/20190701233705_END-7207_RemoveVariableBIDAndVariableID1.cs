using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END7207_RemoveVariableBIDAndVariableID1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Variable.Formula_VariableBID_VariableID1",
                table: "Part.Assembly.Variable.Formula");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Variable.Formula_Part.Subassembly.Variable_VariableBID_VariableID1",
                table: "Part.Assembly.Variable.Formula");

            migrationBuilder.DropColumn(
                name: "VariableBID",
                table: "Part.Assembly.Variable.Formula");

            migrationBuilder.DropColumn(
                name: "VariableID1",
                table: "Part.Assembly.Variable.Formula");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "VariableBID",
                table: "Part.Assembly.Variable.Formula",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VariableID1",
                table: "Part.Assembly.Variable.Formula",
                nullable: true);

            migrationBuilder.AddForeignKey(
                table: "Part.Assembly.Variable.Formula",
                name: "FK_Part.Assembly.Variable.Formula_Part.Subassembly.Variable_VariableBID_VariableID1",
                columns: new[] { "VariableBID", "VariableID1" },
                principalTable: "Part.Subassembly.Variable",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Variable.Formula_VariableBID_VariableID1",
                table: "Part.Assembly.Variable.Formula",
                columns: new[] { "VariableBID", "VariableID1" });
        }
    }
}
