using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180514160726_AddOrderItemFKProperties")]
    public partial class AddOrderItemFKProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddForeignKey(
                name: "FK_Order.Contact.Role_OrderItemID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "OrderItemID" },
                principalTable: "Order.Item.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Employee.Role_OrderItemID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "OrderItemID" },
                principalTable: "Order.Item.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Note_OrderItemID",
                table: "Order.Note",
                columns: new[] { "BID", "OrderItemID" },
                principalTable: "Order.Item.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Contact.Role_OrderItemID",
                table: "Order.Contact.Role");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Employee.Role_OrderItemID",
                table: "Order.Employee.Role");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Note_OrderItemID",
                table: "Order.Note");
        }
    }
}

