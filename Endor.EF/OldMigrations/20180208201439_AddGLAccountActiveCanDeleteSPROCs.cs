using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180208201439_AddGLAccountActiveCanDeleteSPROCs")]
    public partial class AddGLAccountActiveCanDeleteSPROCs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP INDEX [Accounting.GL.Account].[IX_Accounting.GL.Account_ParentID];

ALTER TABLE [Accounting.GL.Account]
DROP COLUMN IsActive;

ALTER TABLE [Accounting.GL.Account]
DROP COLUMN CanEdit;
");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Accounting.GL.Account",
                nullable: false,
                defaultValueSql: "1");

            migrationBuilder.AddColumn<bool>(
                name: "CanEdit",
                table: "Accounting.GL.Account",
                nullable: false,
                defaultValueSql: "1");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_ParentID",
                table: "Accounting.GL.Account",
                columns: new[] { "BID", "ParentID", "IsActive" });

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Accounting.GL.Account.Action.CanDelete];
GO

/* 
========================================================
    Name: [Accounting.GL.Account.Action.CanDelete]

    Description: This procedure checks if the GLAccount is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.GL.Account.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.GL.Account.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
    A GL Account can be deleted if:
        It is not used by any GL.Data record.
        It is not the parent of another GLAccount.
        If it is an inventory account, It is not used by an inventory object.
        It is not used by any Part Object as the expense or income account.
*/


    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.GL.Account] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('GL Account Specified Does not Exist. GLAccountID=', @ID)

    -- and that is it can be edited
    ELSE IF ( SELECT CanEdit FROM [Accounting.GL.Account] WHERE BID = @BID AND ID = @ID ) = 0

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('System GL Accounts Can not be deleted . GLAccountID=', @ID)

    -- and that is it not being used by [GL.Data]
    /* ELSE IF  EXISTS( SELECT * FROM [Accounting.GL.Data] WHERE BID = @BID AND GLAccountID = @ID )
             OR EXISTS( SELECT * FROM [Accounting.GL.Archive] WHERE BID = @BID AND GLAccountID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('GL Account is used in the GL.Data. GLAccountID=', @ID)
	*/
    -- It is it doesn't have any child GL Accounts under it
    ELSE IF EXISTS( SELECT * FROM [Accounting.GL.Account] WHERE BID = @BID AND ParentID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('GL Account has sub GL Accounts under it. GLAccountID=', @ID)

        -- If it is an inventory account, It is not used by an inventory object.
        -- FUTURE

        -- It is not used by any Part Object as the expense or income account.
        -- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END

");

            migrationBuilder.Sql(@"
If (exists( Select * from sys.objects where name='Accounting.GL.Account.Action.SetActive' and Type='p'))
   DROP PROCEDURE [dbo].[Accounting.GL.Account.Action.SetActive]

GO
-- ========================================================
-- Name: [Accounting.GL.Account.Action.SetActive]
--
-- Description: This procedure sets the GLAccount to active or inactive
--
-- Sample Use:   EXEC dbo.[Accounting.GL.Account.Action.SetActive] @BID=1, @GLAccountID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.GL.Account.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @GLAccountID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the GLAccount specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.GL.Account] WHERE BID = @BID and ID = @GLAccountID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid GLAccount Specified. GLAccountID='+CONVERT(VARCHAR(12),@GLAccountID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF ( SELECT CanEdit FROM [Accounting.GL.Account] WHERE BID = @BID AND ID = @GLAccountID ) = 0
    BEGIN
        SELECT @Result = 0
             , @Message = 'Uneditable GLAccount Specified.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;
	
    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.GL.Account] L
    WHERE BID = @BID and ID = @GLAccountID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP INDEX  [Accounting.GL.Account].[IX_Accounting.GL.Account_ParentID];
ALTER TABLE [Accounting.GL.Account] DROP CONSTRAINT [DF__Accountin__CanEd__0A888742];
ALTER TABLE [Accounting.GL.Account] DROP CONSTRAINT [DF__Accountin__IsAct__09946309];

ALTER TABLE [Accounting.GL.Account]
DROP COLUMN IsActive;

ALTER TABLE [Accounting.GL.Account]
DROP COLUMN CanEdit;
");
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Accounting.GL.Account",
                nullable: false,
                computedColumnSql: "((1))");

            migrationBuilder.AddColumn<bool>(
                name: "CanEdit",
                table: "Accounting.GL.Account",
                nullable: true,
                computedColumnSql: "((1))");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Accounting.GL.Account.Action.CanDelete];
GO");
            migrationBuilder.Sql(@"
IF EXISTS(select * from sys.objects where name = 'Accounting.GL.Account.Action.SetActive' and type = 'P')
  DROP PROCEDURE [dbo].[Accounting.GL.Account.Action.SetActive]
GO");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_ParentID",
                table: "Accounting.GL.Account",
                columns: new[] { "BID", "ParentID", "IsActive" });
        }
    }
}

