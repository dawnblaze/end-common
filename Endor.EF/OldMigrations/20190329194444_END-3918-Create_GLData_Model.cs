using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END3918Create_GLData_Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.GLData_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.GLData");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accounting.GLData",
                table: "Accounting.GLData");

            migrationBuilder.RenameTable(
                name: "[Accounting.GLData]",
                newName: "Accounting.GL.Data");

            migrationBuilder.RenameColumn(
                name: "PaymentMethodId",
                table: "[Accounting.GL.Data]",
                newName: "PaymentMethodID");

            migrationBuilder.RenameColumn(
                name: "PaymentId",
                table: "[Accounting.GL.Data]",
                newName: "PaymentID");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_TaxItemID]', N'IX_Accounting.GL.Data_BID_TaxItemID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_TaxGroupID]', N'IX_Accounting.GL.Data_BID_TaxGroupID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_PaymentMethodId]', N'IX_Accounting.GL.Data_BID_PaymentMethodID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_PaymentId]', N'IX_Accounting.GL.Data_BID_PaymentID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_OrderItemID]', N'IX_Accounting.GL.Data_BID_OrderItemID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_OrderID]', N'IX_Accounting.GL.Data_BID_OrderID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_MaterialID]', N'IX_Accounting.GL.Data_BID_MaterialID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_MachineID]', N'IX_Accounting.GL.Data_BID_MachineID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_LocationID]', N'IX_Accounting.GL.Data_BID_LocationID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_LaborID]', N'IX_Accounting.GL.Data_BID_LaborID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_GLAccountID]', N'IX_Accounting.GL.Data_BID_GLAccountID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_DestinationID]', N'IX_Accounting.GL.Data_BID_DestinationID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_CompanyID]', N'IX_Accounting.GL.Data_BID_CompanyID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_BID_ActivityID]', N'IX_Accounting.GL.Data_BID_ActivityID', N'INDEX';");
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.GL.Data].[IX_Accounting.GLData_CurrencyType]', N'IX_Accounting.GL.Data_BID_CurrencyType', N'INDEX';");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ValidToDT",
                table: "Accounting.GL.Data",
                type: "DATETIME2(7)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(7)");

            migrationBuilder.AlterColumn<short>(
                name: "TaxItemID",
                table: "Accounting.GL.Data",
                type: "smallint sparse",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RecordedDT",
                table: "Accounting.GL.Data",
                type: "DATETIME2(2)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)");

            migrationBuilder.AlterColumn<int>(
                name: "PlaceID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PaymentID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OrderItemID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDT",
                table: "Accounting.GL.Data",
                type: "DATETIME2(7)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(7)");

            migrationBuilder.AlterColumn<int>(
                name: "MaterialID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "MachineID",
                table: "Accounting.GL.Data",
                type: "smallint sparse",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LaborID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ItemComponentID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DestinationID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AssetID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AssemblyID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int SPARSE",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "AccountingDT",
                table: "Accounting.GL.Data",
                type: "DATETIME2(2)",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)");

            migrationBuilder.AddColumn<int>(
                name: "ReconciliaitonID",
                table: "Accounting.GL.Data",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accounting.GL.Data",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "ID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_BID_AssemblyID",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "AssemblyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_BID_ItemComponentID",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "ItemComponentID" });

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_Assembly.Data",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "AssemblyID" },
                principalTable: "Part.Subassembly.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.GL.Data_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "GLAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_OrderItemComponent",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "ItemComponentID" },
                principalTable: "Order.Item.Component",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_Assembly.Data",
                table: "Accounting.GL.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.GL.Data_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.GL.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_OrderItemComponent",
                table: "Accounting.GL.Data");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accounting.GL.Data",
                table: "Accounting.GL.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_BID_AssemblyID",
                table: "Accounting.GL.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_BID_ItemComponentID",
                table: "Accounting.GL.Data");

            migrationBuilder.DropColumn(
                name: "ReconciliaitonID",
                table: "Accounting.GL.Data");

            migrationBuilder.RenameTable(
                name: "[Accounting.GL.Data]",
                newName: "Accounting.GLData");

            migrationBuilder.RenameColumn(
                name: "PaymentMethodID",
                table: "[Accounting.GLData]",
                newName: "PaymentMethodId");

            migrationBuilder.RenameColumn(
                name: "PaymentID",
                table: "[Accounting.GLData]",
                newName: "PaymentId");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_TaxItemID",
                table: "[Accounting.GLData]",
                newName: "IX_Accounting.GLData_BID_TaxItemID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_TaxGroupID",
                table: "[Accounting.GLData]",
                newName: "IX_Accounting.GLData_BID_TaxGroupID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_PaymentMethodID",
                table: "[Accounting.GLData]",
                newName: "IX_Accounting.GLData_BID_PaymentMethodId");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_PaymentID",
                table: "[Accounting.GLData]",
                newName: "IX_Accounting.GLData_BID_PaymentId");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_OrderItemID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_OrderItemID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_OrderID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_OrderID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_MaterialID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_MaterialID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_MachineID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_MachineID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_LocationID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_LocationID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_LaborID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_LaborID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_GLAccountID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_GLAccountID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_DestinationID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_DestinationID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_CompanyID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_CompanyID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_BID_ActivityID",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_BID_ActivityID");

            migrationBuilder.RenameIndex(
                name: "IX_Accounting.GL.Data_CurrencyType",
                table: "Accounting.GLData",
                newName: "IX_Accounting.GLData_CurrencyType");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ValidToDT",
                table: "Accounting.GLData",
                type: "datetime2(7)",
                nullable: false,
                defaultValueSql: "(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))",
                oldClrType: typeof(DateTime),
                oldType: "DATETIME2(7)",
                oldDefaultValueSql: "CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')");

            migrationBuilder.AlterColumn<short>(
                name: "TaxItemID",
                table: "Accounting.GLData",
                type: "smallint SPARSE",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "RecordedDT",
                table: "Accounting.GLData",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "(getutcdate())",
                oldClrType: typeof(DateTime),
                oldType: "DATETIME2(2)",
                oldDefaultValueSql: "(getutcdate())");

            migrationBuilder.AlterColumn<int>(
                name: "PlaceID",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PaymentMethodId",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "smallint sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PaymentId",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OrderItemID",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDT",
                table: "Accounting.GLData",
                type: "datetime2(7)",
                nullable: false,
                defaultValueSql: "SYSUTCDATETIME()",
                oldClrType: typeof(DateTime),
                oldType: "DATETIME2(7)",
                oldDefaultValueSql: "SYSUTCDATETIME()");

            migrationBuilder.AlterColumn<int>(
                name: "MaterialID",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "MachineID",
                table: "Accounting.GLData",
                type: "smallint SPARSE",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "LaborID",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ItemComponentID",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DestinationID",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AssetID",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AssemblyID",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int sparse",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "AccountingDT",
                table: "Accounting.GLData",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "(getutcdate())",
                oldClrType: typeof(DateTime),
                oldType: "DATETIME2(2)",
                oldDefaultValueSql: "(getutcdate())");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accounting.GLData",
                table: "Accounting.GLData",
                columns: new[] { "BID", "ID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.GLData_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "GLAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
