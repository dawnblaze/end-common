using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180825021431_CustomFieldLayoutListFilter")]
    public partial class CustomFieldLayoutListFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                        (
                            @"
DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=15027;
INSERT INTO [dbo].[List.Filter]
           ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
     VALUES
           (1 --<BID, smallint,>
           ,(SELECT MAX(ID)+1 FROM dbo.[List.Filter]) --<ID, int,>
           ,GETUTCDATE() --<CreatedDate, date,>
           ,GETUTCDATE() --<ModifiedDT, datetime2(2),>
           ,1 --<IsActive, bit,>
           ,'All' --<Name, varchar(255),>
           ,15027 --<TargetClassTypeID, int,>
           ,NULL --<IDs, xml,>
           ,NULL --<Criteria, xml,>
           ,NULL --<OwnerID, smallint,>
           ,1 --<IsPublic, bit,>
           ,1 --<IsSystem, bit,>
           ,NULL --<Hint, varchar(max),>
           ,1 --<IsDefault, bit,>
           ,0);--<SortIndex, tinyint,>
"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                        (
                            @"
DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=15027;
"
            );
        }
    }
}

