using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180604144835_END-1419_add_IsTaxExempt_TaxExemptReasonID")]
    public partial class END1419_add_IsTaxExempt_TaxExemptReasonID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Data] SET (SYSTEM_VERSIONING = OFF);
            ");

            migrationBuilder.DropColumn(
                name: "Price.IsTaxExempt",
                table: "Order.Data");

            migrationBuilder.AddColumn<bool>(
                name: "IsTaxExempt",
                table: "Order.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<short>(
                name: "TaxExemptReasonID",
                table: "Order.Data",
                nullable: true);

            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Order.Data]
                ALTER COLUMN TaxExemptReasonID SMALLINT SPARSE NULL
            ");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_TaxExemptReasonID",
                table: "Order.Data",
                columns: new[] { "BID", "TaxExemptReasonID" });


            //alter history table start---
            migrationBuilder.DropColumn(
                name: "Price.IsTaxExempt",
                table: "Historic.Order.Data");

            migrationBuilder.AddColumn<bool>(
                name: "IsTaxExempt",
                table: "Historic.Order.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<short>(
                name: "TaxExemptReasonID",
                table: "Historic.Order.Data",
                nullable: true);    

            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Historic.Order.Data]
                ALTER COLUMN TaxExemptReasonID SMALLINT SPARSE NULL
            ");    

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_TaxExemptReasonID",
                table: "Historic.Order.Data",
                columns: new[] { "BID", "TaxExemptReasonID" });                                    
            //alter history table end---

            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Data] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Data]));
            ");                
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Data] SET (SYSTEM_VERSIONING = OFF);
            ");

            migrationBuilder.DropIndex(
                name: "IX_Order.Data_TaxExemptReasonID",
                table: "Order.Data");

            migrationBuilder.DropColumn(
                name: "IsTaxExempt",
                table: "Order.Data");

            migrationBuilder.DropColumn(
                name: "TaxExemptReasonID",
                table: "Order.Data");

            migrationBuilder.AddColumn<bool>(
                name: "Price.IsTaxExempt",
                table: "Order.Data",
                nullable: false,
                computedColumnSql: "Case when [Price.TaxRate] = 0 then cast(1 as bit) else cast(0 as bit) end");

            //alter history table start---
            migrationBuilder.DropColumn(
                name: "IsTaxExempt",
                table: "Historic.Order.Data");

            migrationBuilder.DropColumn(
                name: "TaxExemptReasonID",
                table: "Historic.Order.Data");

            migrationBuilder.AddColumn<bool>(
                name: "Price.IsTaxExempt",
                table: "Historic.Order.Data",
                nullable: true);
            //alter history table end---                

            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Data] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Data]));
            ");
        }
    }
}

