using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180328120611_AccountingPaymentMethodActionCanDelete")]
    public partial class AccountingPaymentMethodActionCanDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                IF EXISTS(select * from sys.objects where name = 'Accounting.Payment.Method.Action.CanDelete' and type = 'P')
  DROP PROCEDURE [dbo].[Accounting.Payment.Method.Action.CanDelete]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* 
========================================================
    Name: [Accounting.Payment.Method.Action.CanDelete]

    Description: This procedure checks if the PaymentMethod is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Payment.Method.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Payment.Method.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	A paymentmethod can be deleted if:

	* It exists
	* It is a custom payment method.
	* It is not used by any payments.
	* It is not used by any stored payment methods.
*/

    SET @Result = 1;

    IF NOT EXISTS( SELECT * FROM [Accounting.Payment.Method] WHERE BID = @BID AND ID = @ID ) 
        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Method Specified Does not Exist. PaymentMethod=', @ID)

    -- Check if the record exists
    ELSE IF  0=(SELECT IsCustom FROM [Accounting.Payment.Method] WHERE ID = @ID)
        SELECT @Result = 0
             , @CantDeleteReason = 'Custom Payment Type is in use and can''t be deleted'

    --ELSE IF EXISTS(SELECT * FROM [Accounting.Payment.Data] WHERE PaymentMethodID = @ID)
    --    SELECT @Result = 0
    --         , @CantDeleteReason = 'Payment Data for Payment Method Exists'

    --ELSE IF EXISTS(SELECT * FROM [Accounting.Payment.Credential] WHERE PaymentMethodID = @ID)
    --    SELECT @Result = 0
    --         , @CantDeleteReason = 'Payment Credentials for Payment Method Exists'

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

