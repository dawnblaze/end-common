using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSubassemblyVariable_IsDisabledType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Drop Default Value Constraint, constraint name is random
            migrationBuilder.Sql(@"
                DECLARE @ConstraintName nvarchar(200)
                SELECT @ConstraintName = Name
                FROM SYS.DEFAULT_CONSTRAINTS
                WHERE PARENT_OBJECT_ID = OBJECT_ID('[Part.Subassembly.Variable]')
                AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = N'IsDisabled' AND object_id = OBJECT_ID(N'[Part.Subassembly.Variable]'))
                
                IF @ConstraintName IS NOT NULL
                BEGIN
                    EXEC('ALTER TABLE [Part.Subassembly.Variable] DROP CONSTRAINT [' + @ConstraintName + ']')
                END
            ");

            migrationBuilder.Sql(@"
                DECLARE @SystemConstraintName nvarchar(200)
                SELECT @SystemConstraintName = Name
                FROM SYS.DEFAULT_CONSTRAINTS
                WHERE PARENT_OBJECT_ID = OBJECT_ID('[System.Part.Subassembly.Variable]')
                AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = N'IsDisabled' AND object_id = OBJECT_ID(N'[System.Part.Subassembly.Variable]'))

                IF @SystemConstraintName IS NOT NULL
                BEGIN
                    EXEC('ALTER TABLE [System.Part.Subassembly.Variable] DROP CONSTRAINT [' + @SystemConstraintName + ']')
                END
            ");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDisabled",
                table: "System.Part.Subassembly.Variable",
                type: "bit");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDisabled",
                table: "Part.Subassembly.Variable",
                type: "bit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //Drop Default Value Constraint, constraint name is random
            migrationBuilder.Sql(@"
                DECLARE @ConstraintName nvarchar(200)
                SELECT @ConstraintName = Name
                FROM SYS.DEFAULT_CONSTRAINTS
                WHERE PARENT_OBJECT_ID = OBJECT_ID('[Part.Subassembly.Variable]')
                AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = N'IsDisabled' AND object_id = OBJECT_ID(N'[Part.Subassembly.Variable]'))

                IF @ConstraintName IS NOT NULL
                BEGIN
                    EXEC('ALTER TABLE [Part.Subassembly.Variable] DROP CONSTRAINT [' + @ConstraintName + ']')
                END
            ");

            migrationBuilder.Sql(@"
                DECLARE @SystemConstraintName nvarchar(200)
                SELECT @SystemConstraintName = Name
                FROM SYS.DEFAULT_CONSTRAINTS
                WHERE PARENT_OBJECT_ID = OBJECT_ID('[System.Part.Subassembly.Variable]')
                AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = N'IsDisabled' AND object_id = OBJECT_ID(N'[System.Part.Subassembly.Variable]'))

                IF @SystemConstraintName IS NOT NULL
                BEGIN
                    EXEC('ALTER TABLE [System.Part.Subassembly.Variable] DROP CONSTRAINT [' + @SystemConstraintName + ']')
                END
            ");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDisabled",
                table: "System.Part.Subassembly.Variable",
                type: "tinyint");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDisabled",
                table: "Part.Subassembly.Variable",
                type: "tinyint");
        }
    }
}
