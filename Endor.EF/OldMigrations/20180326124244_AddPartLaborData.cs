using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180326124244_AddPartLaborData")]
    public partial class AddPartLaborData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Labor.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    BillingIcrementInMin = table.Column<byte>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12000"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    EstimatingCostFixed = table.Column<decimal>(type: "decimal(18, 8)", nullable: true),
                    EstimatingCostPerHour = table.Column<decimal>(type: "decimal(18, 8)", nullable: true),
                    EstimatingPriceFixed = table.Column<decimal>(type: "decimal(18, 8)", nullable: true),
                    EstimatingPricePerHour = table.Column<decimal>(type: "decimal(18, 8)", nullable: true),
                    ExpenseAccountID = table.Column<short>(nullable: false),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IncomeAccountID = table.Column<short>(nullable: false),
                    InvoiceText = table.Column<string>(maxLength: 255, nullable: false),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    MinimumTimeInMin = table.Column<byte>(nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    SKU = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Labor.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Labor.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Labor.Data_Name",
                table: "Labor.Data",
                columns: new[] { "BID", "Name", "IsActive" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Labor.Data");
        }
    }
}

