using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CorrectingClassTypeIDOnPartMachineProfileTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Part.Machine.Profile.Table",
                nullable: false,
                computedColumnSql: "12035",
                oldClrType: typeof(int),
                oldComputedColumnSql: "12034");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Part.Machine.Profile.Table",
                nullable: false,
                computedColumnSql: "12034",
                oldClrType: typeof(int),
                oldComputedColumnSql: "12035");
        }
    }
}
