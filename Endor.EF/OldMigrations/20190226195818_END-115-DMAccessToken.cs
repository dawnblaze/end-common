using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END115DMAccessToken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DM.Access.Token",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<Guid>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "7117"),
                    ExpirationDate = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false),
                    LastViewedDT = table.Column<DateTime>(nullable: true),
                    SASToken = table.Column<string>(type: "varchar(1024)", nullable: false),
                    URL = table.Column<string>(type: "varchar(1024)", nullable: false),
                    ViewedCount = table.Column<short>(nullable: false, defaultValueSql: "((0))"),
                    ViewsRemaining = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DM.Access.Token", x => new { x.BID, x.ID });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DM.Access.Token");
        }
    }
}
