using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Updates_For_GLEngine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Debit",
                table: "Accounting.GLData",
                type: "decimal(18, 4)",
                nullable: true,
                computedColumnSql: "(case when Amount > 0 then -Amount else NULL end)",
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "Accounting.GLData",
                type: "decimal(18, 4)",
                nullable: true,
                computedColumnSql: "(case when Amount < 0 then -Amount else NULL end)",
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 4)",
                oldNullable: true);

            migrationBuilder.Sql(@"
DECLARE @BID SMALLINT = 1;
  
INSERT [Accounting.GL.Account] ([BID], [ID], [ModifiedDT], [IsActive], [CanEdit], [Name], [Number], [GLAccountType], [ParentID], [ExportAccountName], [ExportAccountNumber], [ExportGLAccountType])
VALUES (@BID, 4800, '2018-01-01', 1, 0, N'Shipping Income', 4800, 40, 4000, NULL, NULL, 40)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Debit",
                table: "Accounting.GLData",
                type: "decimal(18, 4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 4)",
                oldNullable: true,
                oldComputedColumnSql: "(case when Amount > 0 then -Amount else NULL end)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "Accounting.GLData",
                type: "decimal(18, 4)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 4)",
                oldNullable: true,
                oldComputedColumnSql: "(case when Amount < 0 then -Amount else NULL end)");
        }
    }
}
