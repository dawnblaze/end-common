using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class ChangeClassTypeIdForOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Data]
                SET (SYSTEM_VERSIONING = OFF)
                GO

                ALTER TABLE [Order.Data]
                DROP COLUMN IF EXISTS ClassTypeID
                ;
                ALTER TABLE [Order.Data]
                ADD ClassTypeID AS (CASE TransactionType WHEN 2 THEN 10000 WHEN 1 THEN 10200 WHEN 4 THEN 10100 ELSE 0 END)
                GO

                DROP TABLE [dbo].[Historic.Order.Data]
                GO

                ALTER TABLE [Order.Data]
                SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Data]))
                ;
            ");

            migrationBuilder.Sql(@"
                -- Number Index
                DROP INDEX IF EXISTS [IX_Order.Data_Number] ON [dbo].[Order.Data];
                CREATE INDEX [IX_Order.Data_Number] ON [dbo].[Order.Data] ( BID, TransactionType, Number );

                -- Company Index
                DROP INDEX IF EXISTS [IX_Order.Data_CompanyID] ON [dbo].[Order.Data];
                CREATE INDEX [IX_Order.Data_CompanyID] ON [dbo].[Order.Data] ( BID, CompanyID, TransactionType );

                -- Location
                DROP INDEX IF EXISTS [IX_Order.Data_LocationID] ON [dbo].[Order.Data];
                CREATE INDEX [IX_Order.Data_LocationID] ON [dbo].[Order.Data]
                ( BID, LocationID, TransactionType, OrderStatusID, Number );
            ");

            migrationBuilder.Sql(@"
                -- drop FKs on enum.Order.OrderStatus
                ALTER TABLE [dbo].[Order.Data] 
                DROP CONSTRAINT IF EXISTS [FK_Order.Data_OrderStatus]
                ;
                ALTER TABLE [dbo].[Order.Item.Data] 
                DROP CONSTRAINT IF EXISTS [FK_Order.Item.Data_OrderStatus]
                ;

                ALTER TABLE [dbo].[enum.Order.OrderStatus] 
                DROP CONSTRAINT IF EXISTS [FK_enum.Order.OrderStatus_enum.Order.OrderStatus_FixedItemStatusID]
                ;
                GO

                -- Alter Table to change Primary Key
                ALTER TABLE [dbo].[enum.Order.OrderStatus] 
                DROP CONSTRAINT IF EXISTS [PK_enum.Order.OrderStatus] WITH ( ONLINE = OFF )
                ;

                ALTER TABLE [dbo].[enum.Order.OrderStatus] 
                DROP COLUMN IF EXISTS TransactionTypeText
                ;

                ALTER TABLE [dbo].[enum.Order.OrderStatus] 
                ALTER COLUMN TransactionType TINYINT NOT NULL
                ;

                ALTER TABLE [dbo].[enum.Order.OrderStatus] 
                ADD TransactionTypeText AS
                (case [TransactionType] when (1) then 'Estimate' when (2) then 'Order' when (4) then 'PO' when (32) then 'Opportunity' when (64) then 'Destination' else 'Unknown' end)

                ALTER TABLE [dbo].[enum.Order.OrderStatus] 
                ADD CONSTRAINT [PK_enum.Order.OrderStatus] PRIMARY KEY (ID, TransactionType)
                ;
                GO

                -- We have to fix some bad data in the Order.Data table 
                -- Where the OrderStatusIDs are not valid for the specified TransactionType
                UPDATE OD 
                SET OrderStatusID = TransactionType*10 + 9
                FROM [Order.Data] OD
                WHERE NOT EXISTS(SELECT * FROM [enum.Order.OrderStatus] OS
                                    WHERE OS.ID = OD.OrderStatusID AND OS.TransactionType = OD.TransactionType)
                ;            
                GO

                -- We have to fix some bad data in the Order.Item table 
                -- Where the OrderStatusIDs are not valid for the specified TransactionType
                UPDATE OI
                SET OrderStatusID = TransactionType*10 + 9
                FROM [Order.Item.Data] OI
                WHERE NOT EXISTS(SELECT * FROM [enum.Order.OrderStatus] OS
                                    WHERE OS.ID = OI.OrderStatusID AND OS.TransactionType = OI.TransactionType)
                ;            
                GO

                -- We have to fix some bad data in the Order.Destination table 
                -- Where the OrderStatusIDs are not valid for the specified TransactionType
                UPDATE OD
                SET OrderStatusID = TransactionType*10 + 9
                FROM [Order.Destination.Data] OD
                WHERE NOT EXISTS(SELECT * FROM [enum.Order.OrderStatus] OS
                                    WHERE OS.ID = OD.OrderStatusID AND OS.TransactionType = OD.TransactionType)
                ;            
                GO


                -- re-add FKs
                ALTER TABLE [dbo].[Order.Data] WITH CHECK 
                ADD CONSTRAINT [FK_Order.Data_OrderStatus] 
                FOREIGN KEY(OrderStatusID, TransactionType) REFERENCES [dbo].[enum.Order.OrderStatus] (ID, TransactionType)
                ;

                ALTER TABLE [dbo].[Order.Data] 
                CHECK CONSTRAINT [FK_Order.Data_OrderStatus]
                ;

                ALTER TABLE [dbo].[enum.Order.OrderStatus] WITH CHECK 
                ADD CONSTRAINT [FK_enum.Order.OrderStatus_enum.Order.OrderStatus_FixedItemStatusID] 
                FOREIGN KEY( FixedItemStatusID, TransactionType) REFERENCES [dbo].[enum.Order.OrderStatus] ([ID], [TransactionType] )
                ;

                ALTER TABLE [dbo].[enum.Order.OrderStatus] 
                CHECK CONSTRAINT [FK_enum.Order.OrderStatus_enum.Order.OrderStatus_FixedItemStatusID]
                ;

                ALTER TABLE [dbo].[Order.Item.Data] WITH CHECK 
                ADD CONSTRAINT [FK_Order.Item.Data_OrderStatus] 
                FOREIGN KEY([OrderStatusID], TransactionType) REFERENCES [dbo].[enum.Order.OrderStatus] ([ID], [TransactionType])
                ;

                ALTER TABLE [dbo].[Order.Item.Data] 
                CHECK CONSTRAINT [FK_Order.Item.Data_OrderStatus]
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
