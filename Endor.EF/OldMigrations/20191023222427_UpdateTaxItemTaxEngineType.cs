﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class UpdateTaxItemTaxEngineType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Accounting.TaxEngineType");

            migrationBuilder.CreateTable(
                name: "enum.Accounting.TaxEngineType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Accounting.TaxEngineType", x => x.ID);
                });

            migrationBuilder.Sql(@"SET IDENTITY_INSERT [enum.Accounting.TaxEngineType] ON;");
            migrationBuilder.Sql(@"
INSERT INTO [enum.Accounting.TaxEngineType] ([ID], [Name])
VALUES(0, 'Exempt')
, (1, 'Supplied')
, (2, 'Internal')
, (3, 'TaxJar')
;
            ");
            migrationBuilder.Sql(@"SET IDENTITY_INSERT [enum.Accounting.TaxEngineType] OFF;");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Item_TaxEngineType",
                table: "Accounting.Tax.Item",
                column: "TaxEngineType");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Item_enum.Accounting.TaxEngineType",
                table: "Accounting.Tax.Item",
                column: "TaxEngineType",
                principalTable: "enum.Accounting.TaxEngineType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Tax.Item_enum.Accounting.TaxEngineType",
                table: "Accounting.Tax.Item");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Tax.Item_TaxEngineType",
                table: "Accounting.Tax.Item");

            migrationBuilder.DropTable(
                name: "enum.Accounting.TaxEngineType");
        }
    }
}
