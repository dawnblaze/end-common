using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Create_Options_OrderStatus_id_Enable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [System.Option.Section] (ID, Name, ParentID, IsHidden, Depth, SearchTerms)
VALUES 
(10051,'Workflow', NULL, 0, 0, NULL)
;

INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden, SearchTerms)
VALUES 
(10051,'Item Statuses', 10051, 'Item Statuses',2,0,NULL)
;

INSERT [dbo].[System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
VALUES 
(10001, N'Order.Status.21.Enable', N'Enable Order Status ""Pre - WIP"" ', N'', 3, 10051, N'', N'true', 0),
(10002, N'Order.Status.24.Enable', N'Enable Order Status ""Invoicing"" ', N'', 3, 10051, N'', N'true', 0)
;

INSERT [Option.Data]([CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) 
VALUES
(GetUTCDate(), GetUTCDate(), 1, 10001, N'true', NULL, 1, NULL, NULL, NULL, NULL, NULL),
(GetUTCDate(), GetUTCDate(), 1, 10002, N'true', NULL, 1, NULL, NULL, NULL, NULL, NULL)
;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE [Option.Data] WHERE [OptionID] in (10001,10002)
DELETE [System.Option.Definition] WHERE ID in (10001,10002)
DELETE [System.Option.Category] WHERE ID in (10051)
DELETE [System.Option.Section] WHERE ID in (10051)
            ");
        }
    }
}
