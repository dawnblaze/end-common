using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END5933UpdateAllAssemblyDataTypes2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"Delete dbo.[enum.DataType] where Id in (10002,10003)");
            migrationBuilder.Sql($"Delete dbo.[enum.ClassType] where Id in (10002,10003)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
