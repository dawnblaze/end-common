using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Alter_Company_VendorTaxIDNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            EXEC sp_rename '[Company.Data].VendorTaxIDNumber', 'TaxID', 'COLUMN';
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TaxID",
                table: "Company.Data",
                newName: "VendorTaxIDNumber");
        }
    }
}
