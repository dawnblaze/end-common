using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6580AddingAssemblyVariableFormula : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Assembly.Formula.EvalType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Assembly.Formula.EvalType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Assembly.Formula.UseType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Assembly.Formula.UseType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Part.Assembly.Variable.Formula",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ChildVariableName = table.Column<string>(type: "varchar(255)", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12052"),
                    DataType = table.Column<short>(type: "smallint", nullable: false),
                    FormulaEvalType = table.Column<byte>(type: "tinyint", nullable: false),
                    FormulaText = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FormulaUseType = table.Column<byte>(type: "tinyint", nullable: false),
                    IsFormula = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [FormulaEvalType] <= 1 then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    VariableBID = table.Column<short>(nullable: true),
                    VariableID = table.Column<int>(nullable: false),
                    VariableID1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Assembly.Variable.Formula", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Variable.Formula_enum.Assembly.Formula.EvalType",
                        column: x => x.FormulaEvalType,
                        principalTable: "enum.Assembly.Formula.EvalType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Variable.Formula_enum.Assembly.Formula.UseType",
                        column: x => x.FormulaUseType,
                        principalTable: "enum.Assembly.Formula.UseType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Variable.Formula_Part.Subassembly.Variable",
                        columns: x => new { x.BID, x.VariableID },
                        principalTable: "Part.Subassembly.Variable",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Variable.Formula_Part.Subassembly.Variable_VariableBID_VariableID1",
                        columns: x => new { x.VariableBID, x.VariableID1 },
                        principalTable: "Part.Subassembly.Variable",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Variable.Formula_FormulaEvalType",
                table: "Part.Assembly.Variable.Formula",
                column: "FormulaEvalType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Variable.Formula_FormulaUseType",
                table: "Part.Assembly.Variable.Formula",
                column: "FormulaUseType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Variable.Formula_VariableBID_VariableID1",
                table: "Part.Assembly.Variable.Formula",
                columns: new[] { "VariableBID", "VariableID1" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Variable.Formula_BID_VariableID_FormulaUseType",
                table: "Part.Assembly.Variable.Formula",
                columns: new[] { "BID", "VariableID", "FormulaUseType" });

            migrationBuilder.Sql(
@"INSERT [enum.Assembly.Formula.UseType] ([ID], [Name])
VALUES (0, N'Default Value')
    , (1, N'Consumption')
    , (2, N'Is Enabled')
    , (3, N'Is Required')
    , (4, N'Is Visible')
    , (10, N'Is Linked')
    , (11, N'Linked Variable OV')
;");

            migrationBuilder.Sql(
@"INSERT [enum.Assembly.Formula.EvalType] ([ID], [Name])
VALUES (0, N'Constant')
    , (1, N'Mapped Variable')
    , (2, N'CBEL')
    , (3, N'C#')
    , (4, N'CFL')
;");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Assembly.Variable.Formula");

            migrationBuilder.DropTable(
                name: "enum.Assembly.Formula.EvalType");

            migrationBuilder.DropTable(
                name: "enum.Assembly.Formula.UseType");
        }
    }
}
