using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class SystemSubassemblyVariableTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "System.Part.Subassembly.Variable",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowCustomValue = table.Column<bool>(nullable: true),
                    AllowDecimals = table.Column<bool>(nullable: true),
                    AllowMultiSelect = table.Column<bool>(nullable: true),
                    AltText = table.Column<string>(maxLength: 255, nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12048"),
                    DataType = table.Column<short>(nullable: false),
                    DecimalPlaces = table.Column<byte>(nullable: true),
                    DefaultValue = table.Column<string>(nullable: true),
                    DisplayFormat = table.Column<byte>(nullable: true),
                    ElementType = table.Column<byte>(type: "tinyint", nullable: false),
                    ElementUseCount = table.Column<byte>(type: "tinyint", nullable: false),
                    GroupOptionsByCategory = table.Column<bool>(nullable: true),
                    IsFormula = table.Column<bool>(nullable: false),
                    IsRequired = table.Column<bool>(nullable: false),
                    Label = table.Column<string>(maxLength: 255, nullable: true),
                    LabelType = table.Column<byte>(nullable: true),
                    ListDataType = table.Column<short>(nullable: true),
                    ListValuesJSON = table.Column<string>(nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    SystemVariableID = table.Column<short>(type: "smallint", nullable: true),
                    Tooltip = table.Column<string>(nullable: true),
                    UnitID = table.Column<byte>(nullable: true),
                    VariableName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Subassembly.Part.Variable", x => x.ID);
                    table.ForeignKey(
                        name: "FK_System.Part.Subassembly.Variable_enum.Part.Subassembly.List.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_System.Part.Subassembly.Variable_enum.Part.Subassembly.ElementType",
                        column: x => x.ElementType,
                        principalTable: "enum.Part.Subassembly.ElementType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_System.Part.Subassembly.Variable_enum.Part.Subassembly.LabelType",
                        column: x => x.LabelType,
                        principalTable: "enum.Part.Subassembly.LabelType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_System.Part.Subassembly.Variable_DataType",
                table: "System.Part.Subassembly.Variable",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_System.Part.Subassembly.Variable_ElementType",
                table: "System.Part.Subassembly.Variable",
                column: "ElementType");

            migrationBuilder.CreateIndex(
                name: "IX_System.Part.Subassembly.Variable_LabelType",
                table: "System.Part.Subassembly.Variable",
                column: "LabelType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "System.Part.Subassembly.Variable");
        }
    }
}
