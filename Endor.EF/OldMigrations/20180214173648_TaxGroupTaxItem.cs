using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180214173648_TaxGroupTaxItem")]
    public partial class TaxGroupTaxItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounting.Tax.Group.AssessmentLink");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accounting.Tax.Assessment_1",
                table: "Accounting.Tax.Assessment");

            migrationBuilder.RenameTable(
                name: "[Accounting.Tax.Assessment]",
                newName: "Accounting.Tax.Item");

            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.Tax.Item].[IX_Accounting.Tax.Assessment_Name]', N'IX_Accounting.Tax.Item_Name', N'INDEX';");
            //migrationBuilder.RenameIndex(
            //    name: "[IX_Accounting.Tax.Assessment_Name]",
            //    table: "[Accounting.Tax.]",
            //    newName: "IX_Accounting.Tax.Item_Name");


            migrationBuilder.AddPrimaryKey(
                name: "PK_Accounting.Tax.Item",
                table: "Accounting.Tax.Item",
                columns: new[] { "BID", "ID" });

            migrationBuilder.CreateTable(
                name: "Accounting.Tax.Group.ItemLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    GroupID = table.Column<short>(nullable: false),
                    ItemID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Tax.Group.ItemLink", x => new { x.BID, x.GroupID, x.ItemID });
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Group.ItemLink_Accounting.Tax.Group",
                        columns: x => new { x.BID, x.GroupID },
                        principalTable: "Accounting.Tax.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Group.ItemLink_Accounting.Tax.Item",
                        columns: x => new { x.BID, x.ItemID },
                        principalTable: "Accounting.Tax.Item",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Group.ItemLink_Item",
                table: "Accounting.Tax.Group.ItemLink",
                columns: new[] { "BID", "ItemID", "GroupID" });


            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.Tax.Assessment.SimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.Tax.Assessment.SimpleList]
GO
IF Exists(select * from sys.objects where name = 'Accounting.Tax.Item.SimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.Tax.Item.SimpleList]
GO

CREATE VIEW [dbo].[Accounting.Tax.Item.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
         , (SELECT GroupID from [Accounting.Tax.Group.ItemLink] i WHERE i.BID = BID AND i.ItemID = ID) as ParentID
    FROM [Accounting.Tax.Item]
GO
");

            #region sprocs

            #region Accounting.Tax.Group.Recalc
            migrationBuilder.Sql(@"
ALTER PROCEDURE [dbo].[Accounting.Tax.Group.Recalc]
                                @BID SMALLINT
AS
BEGIN
    DECLARE @DT DateTime2(2) = GetDate();

    DECLARE @T TABLE(
        BID SMALLINT
      , ID SMALLINT
      , ClassTypeID SMALLINT
      , OldRate DECIMAL(12,4)
      , NewRate DECIMAL(12,4)
    );

    -- Insert into the Temp Table
    --   All records where the Group.TaxRate != Sum(Item.TaxRate)
    INSERT INTO @T
        SELECT G.BID, G.ID, G.ClassTypeID, COALESCE(G.TaxRate,0.0000), SUM(A.TaxRate)
        FROM [Accounting.Tax.Group] G
        JOIN [Accounting.Tax.Group.ItemLink] GAL ON (GAL.BID = G.BID AND GAL.GroupID = G.ID)
        JOIN [Accounting.Tax.Item] A             ON (A.BID = GAL.BID AND A.ID = GAL.ItemID)
        WHERE G.BID = @BID
          AND A.IsActive = 1
        GROUP BY G.BID, G.ID, G.ClassTypeID, COALESCE(G.TaxRate,0.0000)
        HAVING COALESCE(G.TaxRate,0.0000) != SUM(A.TaxRate)
        ORDER BY G.BID, G.ID
    ;

    -- Update the Tax Groups
    UPDATE G
    SET ModifiedDT = @DT
      , TaxRate = T.NewRate
    FROM [Accounting.Tax.Group] G
    JOIN @T T ON (T.BID = G.BID AND T.ID = G.ID)
    ;

    -- Output the Result
    SELECT * FROM @T
    ;
END");
            #endregion

            #region Accounting.Tax.Group.Action.SetActive
            migrationBuilder.Sql(@"

-- ========================================================
-- Name: [Accounting.Tax.Group.Action.SetActive]
--
-- Description: This procedure sets the TaxGroup to active or inactive
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.SetActive] @BID=1, @TaxGroupID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @TaxGroupID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.Tax.Group] L
    WHERE BID = @BID and ID = @TaxGroupID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END");
            #endregion

            #region Accounting.Tax.Group.Action.CanDelete
            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Accounting.Tax.Group.Action.CanDelete]

    Description: This procedure checks if the TaxGroup is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Tax.Group.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the TaxGroup can be deleted. The boolean response is returned in the body.  A TaxGroup can be deleted if
	It is not the default for a Location. Location.Data.DefaultTaxGroupID
	It is not the default for a Company. Company.Data.TaxGroupID
	Is it not used by any order. (?)
	It is not used by the GL.Data.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group Specified Does not Exist. TaxGroupID=', @ID)

    -- It is not the default for a Location. Location.Data.DefaultTaxGroupID
	-- It is not the default for a Company. Company.Data.TaxGroupID
	ELSE IF  EXISTS( SELECT * FROM [Location.Data] WHERE BID = @BID AND DefaultTaxGroupID = @ID )
             OR EXISTS( SELECT * FROM [Company.Data] WHERE BID = @BID AND TaxGroupID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group is used in the Location and Company Data. TaxGroupID=', @ID)

	-- Is it not used by any order. (?)
	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END");
            #endregion

            #region Accounting.Tax.Item.Action.CanDelete
            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Accounting.Tax.Item.Action.CanDelete]

    Description: This procedure checks if the TaxItem is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Tax.Item.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Tax.Item.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the TaxItem can be deleted. The boolean response is returned in the body.  A TaxItem can be deleted if
	It is not linked to a TaxGroup.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group Specified Does not Exist. TaxItemID=', @ID)

    -- It is not linked to a TaxGroup.
	ELSE IF  EXISTS( SELECT * FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID AND ItemID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Item is used in the Tax Group Data. TaxItemID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END");
            #endregion

            #region Accounting.Tax.Item.Action.SetActive
            migrationBuilder.Sql(@"

-- ========================================================
-- Name: [Accounting.Tax.Item.Action.SetActive]
--
-- Description: This procedure sets the TaxItem to active or inactive
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Item.Action.SetActive] @BID=1, @TaxItemID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Tax.Item.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @TaxItemID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxItem Specified. TaxItemID='+CONVERT(VARCHAR(12),@TaxItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.Tax.Item] L
    WHERE BID = @BID and ID = @TaxItemID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
            #endregion

            #region Accounting.Tax.Group.Action.LinkLocation
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Tax.Group.Action.LinkLocation]
--
-- Description: This procedure links/unlinks the Location to the TaxGroup
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.LinkLocation] @BID=1, @TaxGroupID=1, @LocationID=1, @IsLinked=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.LinkLocation]
--DECLARE 
          @BID            TINYINT  --= 1
        , @TaxGroupID     SMALLINT --= 2
		, @LocationID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.LocationLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID and GroupID = @TaxGroupID and LocationID = @LocationID)
		BEGIN
			INSERT INTO [Accounting.Tax.Group.LocationLink] (BID, GroupID, LocationID)
			VALUES (@BID, @TaxGroupID, @LocationID)
		END;

		-- Check location default taxgroup and add taxgroup ID if null
		IF EXISTS (SELECT * FROM [Location.Data] WHERE ID = @LocationID and DefaultTaxGroupID IS NULL)
		BEGIN
			UPDATE [Location.Data]
			SET DefaultTaxGroupID = @TaxGroupID
			WHERE ID = @LocationID and DefaultTaxGroupID IS NULL
		END
	END
	ELSE
	BEGIN
		-- Remove entry from Tax.Group.LocationLink
		DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID and GroupID = @TaxGroupID and LocationID = @LocationID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END");
            #endregion

            #region Accounting.Tax.Group.Action.LinkTaxItem
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Tax.Group.Action.LinkTaxItem]
--
-- Description: This procedure links/unlinks the TaxItem to the TaxGroup
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.LinkTaxItem] @BID=1, @TaxGroupID=1, @TaxItemID=1, @IsLinked=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.LinkTaxItem]
--DECLARE 
          @BID            TINYINT  --= 1
        , @TaxGroupID     SMALLINT --= 2
		, @TaxItemID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the TaxItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxItem Specified. TaxItemID='+CONVERT(VARCHAR(12),@TaxItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.ItemLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID and GroupID = @TaxGroupID and ItemID = @TaxItemID)
		BEGIN
			INSERT INTO [Accounting.Tax.Group.ItemLink] (BID, GroupID, ItemID)
			VALUES (@BID, @TaxGroupID, @TaxItemID)
		END;
	END
	ELSE
	BEGIN
		-- Remove entry from Tax.Group.ItemLink
		DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID and GroupID = @TaxGroupID and ItemID = @TaxItemID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END");
            #endregion

#endregion
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Accounting.Tax.Group.Action.LinkTaxItem];
GO
DROP PROCEDURE IF EXISTS [dbo].[Accounting.Tax.Group.Action.LinkLocation];
GO
DROP PROCEDURE IF EXISTS [dbo].[Accounting.Tax.Item.Action.SetActive];
GO
DROP PROCEDURE IF EXISTS [dbo].[Accounting.Tax.Item.Action.CanDelete];
GO
DROP PROCEDURE IF EXISTS [dbo].[Accounting.Tax.Group.Action.CanDelete];
GO

DROP PROCEDURE IF EXISTS [dbo].[Accounting.Tax.Group.Action.SetActive];
GO
");

            migrationBuilder.DropTable(
                name: "Accounting.Tax.Group.ItemLink");

            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.Tax.Item.SimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.Tax.Item.SimpleList]
GO
");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accounting.Tax.Item",
                table: "Accounting.Tax.Item");

            migrationBuilder.RenameTable(
                name: "[Accounting.Tax.Item]",
                newName: "Accounting.Tax.Assessment");

            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.Tax.Assessment].[IX_Accounting.Tax.Item_Name]', N'IX_Accounting.Tax.Assessment_Name', N'INDEX';");
            //migrationBuilder.RenameIndex(
            //    name: "IX_Accounting.Tax.Item_Name",
            //    table: "Accounting.Tax.Assessment",
            //    newName: "IX_Accounting.Tax.Assessment_Name");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accounting.Tax.Assessment",
                table: "Accounting.Tax.Assessment",
                columns: new[] { "BID", "ID" });

            migrationBuilder.CreateTable(
                name: "Accounting.Tax.Group.AssessmentLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    GroupID = table.Column<short>(nullable: false),
                    AssessmentID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Tax.Group.AssessmentLink", x => new { x.BID, x.GroupID, x.AssessmentID });
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment",
                        columns: x => new { x.BID, x.AssessmentID },
                        principalTable: "Accounting.Tax.Assessment",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group",
                        columns: x => new { x.BID, x.GroupID },
                        principalTable: "Accounting.Tax.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Group.AssessmentLink_Assessment",
                table: "Accounting.Tax.Group.AssessmentLink",
                columns: new[] { "BID", "AssessmentID", "GroupID" });

            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[Accounting.Tax.Assessment.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.Tax.Assessment]
GO
");
        }
    }
}

