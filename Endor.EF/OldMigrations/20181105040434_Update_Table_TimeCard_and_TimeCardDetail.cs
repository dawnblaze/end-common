using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Update_Table_TimeCard_and_TimeCardDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP INDEX [IX_Employee.TimeCard_BID_AdjustedByEmployeeID] ON [dbo].[Employee.TimeCard];
                ALTER TABLE [dbo].[Employee.TimeCard]
                ALTER COLUMN [AdjustedByEmployeeID] [smallint] SPARSE  NULL
            ");
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Employee.TimeCard]
                ALTER COLUMN [AdjustedDT] [datetime2](2) SPARSE  NULL
            ");
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Employee.TimeCard]
                ALTER COLUMN [MetaData] [xml] SPARSE  NULL
            ");

            migrationBuilder.AlterColumn<bool>(
                name: "IsClosed",
                table: "Employee.TimeCard",
                computedColumnSql: "(case when [EndDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end) PERSISTED",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard_BID_AdjustedByEmployeeID",
                table: "Employee.TimeCard",
                columns: new[] { "BID", "AdjustedByEmployeeID" });

            migrationBuilder.AlterColumn<bool>(
                name: "IsAdjusted",
                table: "Employee.TimeCard",
                computedColumnSql: "(isnull(case when [AdjustedByEmployeeID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,CONVERT([bit],(0))))",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Employee.TimeCard",
                computedColumnSql: "((5030))",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "TimeInMin",
                table: "Employee.TimeCard",
                computedColumnSql: "((datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0))",
                nullable: true);

            migrationBuilder.Sql(@"
                DROP INDEX [IX_Employee.TimeCard.Detail_BID_AdjustedByEmployeeID] ON [dbo].[Employee.TimeCard.Detail];
                ALTER TABLE [dbo].[Employee.TimeCard.Detail]
                ALTER COLUMN [AdjustedByEmployeeID] [smallint] SPARSE  NULL
            ");

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard.Detail_BID_AdjustedByEmployeeID",
                table: "Employee.TimeCard.Detail",
                columns: new[] { "BID", "AdjustedByEmployeeID" });

            migrationBuilder.AlterColumn<bool>(
                name: "IsClosed",
                table: "Employee.TimeCard.Detail",
                computedColumnSql: "(case when [EndDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end) PERSISTED",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "SimultaneousDetailCards",
                table: "Employee.TimeCard.Detail",
                defaultValueSql: "((1))",
                nullable: false);

            migrationBuilder.AlterColumn<bool>(
                name: "IsPaid",
                table: "Employee.TimeCard.Detail",
                defaultValueSql: "((1))",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "PaidTimeInMin",
                table: "Employee.TimeCard.Detail",
                computedColumnSql: "(case when [IsPaid]=(1) then (datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0) else (0.0) end)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Employee.TimeCard.Detail",
                computedColumnSql: "((5031))",
                nullable: false);

            migrationBuilder.AlterColumn<int>(
                name: "TimeInMin",
                table: "Employee.TimeCard.Detail",
                computedColumnSql: "((datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0))",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsAdjusted",
                table: "Employee.TimeCard.Detail",
                computedColumnSql: "(isnull(case when [AdjustedByEmployeeID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,CONVERT([bit],(0))))",
                nullable: true);

            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Employee.TimeCard.Detail]
                ALTER COLUMN [AdjustedDT] [datetime2](2) SPARSE  NULL
            ");
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Employee.TimeCard.Detail]
                ALTER COLUMN [MetaData] [xml] SPARSE  NULL
            ");
            migrationBuilder.Sql(@"
                -- =============================================
                -- Author:      CoreBridge
                -- Create date: 10/2018
                -- Description: This Trigger Updates the Master TimeCard any time
                --              the TimeCard Detail changes
                -- =============================================
                CREATE TRIGGER [Employee.TimeCard.Detail_UpdateTimeCard]
                ON  [Employee.TimeCard.Detail]
                AFTER INSERT, UPDATE
                AS
                BEGIN
                    SET NOCOUNT ON;
 
                    UPDATE TC
                    SET ModifiedDT = GETUTCDATE()
                    , PaidTimeInMin = TC.TimeInMin - COALESCE(( SELECT SUM(TCD.TimeInMin) FROM [Employee.TimeCard.Detail] TCD WHERE TCD.TimeCardID = TC.ID AND TCD.IsPaid = 0 ), 0)
                    FROM [Employee.TimeCard] TC
                    JOIN INSERTED I on TC.ID = I.TimeCardID
                    ;
                END
 
                GO
 
                ALTER TABLE [dbo].[Employee.TimeCard.Detail]
                ENABLE TRIGGER [Employee.TimeCard.Detail_UpdateTimeCard]
 
                GO
 
                -- =============================================
                -- Author:      CoreBridge
                -- Create date: 10/2018
                -- Description: This Trigger Updates the Master TimeCard any time
                --              the TimeCard Detail changes
                -- =============================================
                CREATE TRIGGER [Employee.TimeCard.Detail_UpdateTimeCardOnDelete]
                ON  [Employee.TimeCard.Detail]
                AFTER INSERT, UPDATE
                AS
                BEGIN
                    SET NOCOUNT ON;
 
                    UPDATE TC
                    SET ModifiedDT = GETUTCDATE()
                    , PaidTimeInMin = TC.TimeInMin - COALESCE(( SELECT SUM(TCD.TimeInMin) FROM [Employee.TimeCard.Detail] TCD WHERE TCD.TimeCardID = TC.ID AND TCD.IsPaid = 0 ), 0)
                    FROM [Employee.TimeCard] TC
                    JOIN DELETED D on TC.ID = D.TimeCardID
                    ;
                END
 
                GO
 
                ALTER TABLE [dbo].[Employee.TimeCard.Detail]
                ENABLE TRIGGER [Employee.TimeCard.Detail_UpdateTimeCardOnDelete]
 
                GO                
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
