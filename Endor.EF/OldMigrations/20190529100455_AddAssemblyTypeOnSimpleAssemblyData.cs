using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddAssemblyTypeOnSimpleAssemblyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Part.Subassembly.SimpleList]
                AS
                    SELECT[BID]
                        , [ID]
                        , [ClassTypeID]
                        , [Name] as DisplayName
                        , [IsActive]
                        , CONVERT(BIT, 0) AS[HasImage]
                        , CONVERT(BIT, 0) AS[IsDefault]
                        , AssemblyType
                    FROM [dbo].[Part.Subassembly.Data];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Part.Subassembly.SimpleList]
                AS
                    SELECT[BID]
                        , [ID]
                        , [ClassTypeID]
                        , [Name] as DisplayName
                        , [IsActive]
                        , CONVERT(BIT, 0) AS[HasImage]
                        , CONVERT(BIT, 0) AS[IsDefault]
                    FROM [dbo].[Part.Subassembly.Data];
                GO
            ");
        }
    }
}
