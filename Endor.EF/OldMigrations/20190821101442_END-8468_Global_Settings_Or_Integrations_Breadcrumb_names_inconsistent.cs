using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8468_Global_Settings_Or_Integrations_Breadcrumb_names_inconsistent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [System.Option.Category] 
                SET Name = 'Document Management Options', Description = 'Document Management Options'
                WHERE ID = 711;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [System.Option.Category] 
                SET Name = 'File Management Options', Description = 'File Management Options'
                WHERE ID = 711;
            ");
        }
    }
}
