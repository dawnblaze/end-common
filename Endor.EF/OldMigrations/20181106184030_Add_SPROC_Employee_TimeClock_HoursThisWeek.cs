using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_SPROC_Employee_TimeClock_HoursThisWeek : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                /* ========================================================
                --Name: [Employee.TimeClock.HoursThisWeek] (@BID tinyint, @EmployeeID int )
                --
                -- Description: This function retrieves the time an employee worked for the day and week.
                --      The starting DateTime for the Day and the Week must be passed in since the server
                --      time is UTC and this won't work for measuring the week and day start!
                --
                -- Sample Use:   EXEC dbo.[Employee.TimeClock.HoursThisWeek] @BID= 1, @EmployeeID= 1
                -- ======================================================== */
                CREATE PROCEDURE [dbo].[Employee.TimeClock.HoursThisWeek]
                -- DECLARE
                          @BID TINYINT -- = 1
                        , @EmployeeID INT -- = 100
                        , @WeekStartDT DateTime2(2)
                        , @DayStartDT DateTime2(2)
                AS
                BEGIN

                    SELECT COALESCE(TotalTimeForWeek, 0)/60.0 AS TotalHoursForWeek
                           , COALESCE(TotalTimeForWeek, 0)/60.0 - COALESCE(UnpaidTimeForWeek, 0)/60.0 AS PaidHoursForWeek
                           , COALESCE(UnpaidTimeForWeek, 0)/60.0 AS UnpaidHoursForWeek
                           , COALESCE(TotalTimeForDay, 0)/60.0 AS TotalHoursForDay
                           , COALESCE(TotalTimeForDay, 0)/60.0 - COALESCE(UnpaidTimeForDay , 0)/60.0 AS PaidHoursForDay
                          , COALESCE(UnpaidTimeForDay, 0)/60.0 AS UnpaidHoursForDay
                    FROM
                        (
                            SELECT SUM(TimeInMin ) as TotalTimeForWeek
                                , SUM(IIF(StartDT BETWEEN @DayStartDT AND GetUTCDate(), TimeInMin, 0  ) ) as TotalTimeForDay
                          FROM[Employee.TimeCard] TC
                         WHERE BID = @BID AND EmployeeID = @EmployeeID
                         AND StartDT BETWEEN @WeekStartDT AND GetUTCDate()
                        ) TotalTime

                    JOIN
                        (
                            SELECT SUM(TimeInMin ) as UnpaidTimeForWeek
                                , SUM(IIF(StartDT BETWEEN @DayStartDT AND GetUTCDate(), TimeInMin, 0  ) ) as UnpaidTimeForDay
                          FROM[Employee.TimeCard.Detail] TCD
                         WHERE BID = @BID AND EmployeeID = @EmployeeID
                         AND StartDT BETWEEN @WeekStartDT AND GetUTCDate()
                            AND IsPaid = 0
                        ) UnpaidTime
                    ON 1=1

                END
                GO
            ");
        }

    protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [dbo].[Employee.TimeClock.HoursThisWeek]
            ");
        }
    }
}
