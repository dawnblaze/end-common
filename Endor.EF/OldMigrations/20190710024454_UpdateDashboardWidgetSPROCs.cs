using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateDashboardWidgetSPROCs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.003]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Order Throughput 
    The Order Throughput Widget displays a graph or a chart of the throughput of orders in each status for the specified period.
    This Widget measures the throughput of  each status, not the measure of what is in it. 
    Each time an order leaves a status it is recorded as throughput for that status.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/990969857/Dashboard+Widget+Order+Throughput
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.003] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.003]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN
    --  --- There are helpful for testing
    --    DECLARE @BID SMALLINT = 1;
    --    DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    --    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    --    DECLARE @AsTable BIT = 1;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    --                   = 26 for Closed                   = 9 for Closed
    --                   = 29 for Voided                   = 2 for Voided
    
    -- ---------------------------------------------
    -- Compute some working Data
    -- ---------------------------------------------
    DECLARE @MidDate DateTime2(2) = (SELECT DATEADD(SECOND, DATEDIFF(SECOND, @StartUTCDT, @EndUTCDT )/2, @StartUTCDT));

    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , OrderStatusID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );

    -- Create a row for each entry.  Otherwise, if no data is found no record is returned.
    INSERT INTO @Results (LocationID, OrderStatusID, Name, [Count])
    VALUES (@LocationID,  0, 'Sales Goal', 0)
         , (@LocationID,  1, 'Created', 0)
         , (@LocationID, 21, 'Pre-WIP', 0)
         , (@LocationID, 22, 'WIP', 0)
         , (@LocationID, 23, 'Built', 0)
         , (@LocationID, 24, 'Invoicing', 0)
         , (@LocationID, 25, 'Invoiced', 0)
         , (@LocationID, 29, 'Volided', 0)
    ;

    -- Pull ID = 0 - Monthly Goal
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN (
        SELECT 1 AS TheCount, SUM(LG.Budgeted) as TheAmount
        FROM [Location.Goal] LG 
        WHERE LG.BID = @BID
            AND (@LocationID = LG.LocationID OR @LocationID IS NULL)
            AND LG.Year = DatePart(Year, @MidDate)
            AND LG.Month = DatePart(Month, @MidDate)
    ) Data ON R.OrderStatusID = 0
    ;

    -- pull orders that were created in the given range
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN ( SELECT COUNT(O.ID) TheCount, SUM([Price.PreTax]) TheAmount
        FROM [Order.Data] O
        WHERE BID = @BID 
        AND (@LocationID = LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 21 -- Created
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ) Data ON R.OrderStatusID = 1
    ;

    -- pull orders that were moved THROUGH Pre-WIP (into WIP) in the current range
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN ( SELECT COUNT(O.ID) TheCount, SUM([Price.PreTax]) TheAmount
        FROM [Order.Data] O
        WHERE BID = @BID 
        AND (@LocationID = LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 5 -- WIP
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ) Data ON R.OrderStatusID = 21
    ;

    -- pull orders that were moved THROUGH WIP (into Built) in the current range
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN ( SELECT COUNT(O.ID) TheCount, SUM([Price.PreTax]) TheAmount
        FROM [Order.Data] O
        WHERE BID = @BID 
        AND (@LocationID = LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 6 -- Built
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ) Data ON R.OrderStatusID = 22
    ;

    -- pull orders that were moved THROUGH Built (into Invoicing) in the current range
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN ( SELECT COUNT(O.ID) TheCount, SUM([Price.PreTax]) TheAmount
        FROM [Order.Data] O
        WHERE BID = @BID 
        AND (@LocationID = LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 7 -- Invoicing
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ) Data ON R.OrderStatusID = 23
    ;

    -- pull orders that were moved THROUGH Invoicing (into Invoiced) in the current range
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN ( SELECT COUNT(O.ID) TheCount, SUM([Price.PreTax]) TheAmount
        FROM [Order.Data] O
        WHERE BID = @BID 
        AND (@LocationID = LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 8 -- Invoiced
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ) Data ON R.OrderStatusID = 24
    ;

    -- pull orders that were moved THROUGH Invoiced (into Closed) in the current range
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN ( SELECT COUNT(O.ID) TheCount, SUM([Price.PreTax]) TheAmount
        FROM [Order.Data] O
        WHERE BID = @BID 
        AND (@LocationID = LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 7 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ) Data ON R.OrderStatusID = 25
    ;

    -- pull orders that were voided in the current range
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN ( SELECT COUNT(O.ID) TheCount, SUM([Price.PreTax]) TheAmount
        FROM [Order.Data] O
        WHERE BID = @BID 
        AND (@LocationID = LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 9 -- Voided
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ) Data ON R.OrderStatusID = 29
    ;

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------
END

            ");

            migrationBuilder.Sql(@"
/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.004]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Current Order Status 
    The Current Order Status Widget displays a graph of the number of orders in each non-closed status.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/997588993/Dashboard+Widget+Current+Order+Status
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StatusID VARCHAR(255) = '21, 22, 23, 24, 25';  -- Pre-WIP and WIP from https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/595329164/OrderOrderStatus+Enum 

    EXEC dbo.[Dashboard.Widget.Definition.004] 
        @BID = @BID, @LocationID = @LocationID,
        @StatusID = @StatusID
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.004]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StatusID VARCHAR(255) = '21, 22, 23, 24, 25'
AS
BEGIN

    --  --- There are helpful for testing
    --   DECLARE @BID SMALLINT = 1;
    --   DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --   DECLARE @StatusID VARCHAR(255) = '21, 22, 23, 24, 25';
    --   DECLARE @AsTable BIT = 0;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    
    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    SELECT @LocationID [LocationID], OSE.ID, OSE.Name, COUNT(O.ID) [Count], ISNULL(SUM(O.[Price.PreTax]),0.0) [Amount]

    FROM [Order.Data] O

    FULL OUTER JOIN 
    ( SELECT CONVERT(TINYINT, value) [ID] from String_Split(@StatusID, ',') ) OS ON O.BID = @BID AND O.OrderStatusID = OS.ID

    JOIN [enum.Order.OrderStatus] OSE ON OS.ID = OSE.ID

    WHERE (@LocationID IS NULL OR O.LocationID IS NULL OR O.LocationID = @LocationID)
    GROUP BY OSE.ID, OSE.Name
    ORDER BY 1 DESC, 2
    FOR JSON PATH
    ;

END

            ");

            migrationBuilder.Sql(@"
/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.006]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Financial Snapshot 
    The Financial Snapshot Widget displays a graph or a chart that summarizes many of the key financial data points for the business or location.  
    The information is most valuable when (visually) compared to the sales goal of the business or location to see how progress for the month is going.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/991657993/Dashboard+Widget+Financial+Snapshot
    for additional information.
        1. Monthly Goal
        2. MTD Sales
        3. MTD Payments
        4. Current A/Rs
        5. MTD Closed
        6. Current Pre-WIP
        7. Current WIP
        8. Current Built
        9. Current Invoicing
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.006] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.006]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
        --  DECLARE @BID SMALLINT = 1;
        --  DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
        --  DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
        --  DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
        --  DECLARE @AsTable BIT = 1;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    --                   = 26 for Closed                   = 9 for Closed
    --                   = 29 for Voided                   = 2 for Voided
    

    -- ---------------------------------------------
    -- Compute some working Data
    -- ---------------------------------------------
    DECLARE @MidDate DateTime2(2) = (SELECT DATEADD(SECOND, DATEDIFF(SECOND, @StartUTCDT, @EndUTCDT )/2, @StartUTCDT));


    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , ID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );

    -- Create a row for each entry.  Otherwise, if no data is found no record is returned.
    INSERT INTO @Results (LocationID, ID, Name, [Count])
    VALUES (@LocationID, 1, 'Monthly Goal', 0)
         , (@LocationID, 2, 'MTD Sales', 0)
         , (@LocationID, 3, 'MTD Payments', 0)
         , (@LocationID, 4, 'Current A/Rs', 0)
         , (@LocationID, 5, 'MTD Closed', 0)
         , (@LocationID, 6, 'Current Pre-WIP', 0)
         , (@LocationID, 7, 'Current WIP', 0)
         , (@LocationID, 8, 'Current Built', 0)
         , (@LocationID, 9, 'Current Invoicing', 0)
    ;

    -- Pull ID = 1 - Monthly Goal
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN (
        SELECT 1 AS TheCount, SUM(LG.Budgeted) as TheAmount
        FROM [Location.Goal] LG 
        WHERE LG.BID = @BID
            AND (@LocationID = LG.LocationID OR @LocationID IS NULL)
            AND LG.Year = DatePart(Year, @MidDate)
            AND LG.Month = DatePart(Month, @MidDate)
    ) Data ON R.ID = 1
    ;

    -- Pull ID = 2 - MTD Sales
    -- Rework to pull from GL .. but for now ... 
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN (
        SELECT Count(*) AS TheCount, SUM(O.[Price.PreTax]) as TheAmount
        FROM [Order.Data] O
        WHERE O.BID = @BID 
            AND (@LocationID = O.LocationID OR @LocationID IS NULL)
            AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 8 -- Invoiced
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                    )
    ) Data ON R.ID = 2
    ;

    -- Pull ID = 3 - MTD Payments
    -- to get this, we can look at all new master payments (money in) and all payment applications refunds (money out)
    DECLARE @MoneyIn DECIMAL(18,4) = 
                    ( SELECT SUM(Amount)
                       FROM [Accounting.Payment.Master] P
                       WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                       AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                       AND P.PaymentTransactionType = 1 
                    )
    ;

    DECLARE @MoneyOut DECIMAL(18,4) = 
                    (   SELECT SUM(Amount)
                        FROM [Accounting.Payment.Application] P
                        WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                        AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                        AND P.PaymentTransactionType = 2
                    )
    ;

    UPDATE R
    SET [Count] = 1
       , Amount = ISNULL(@MoneyIn, 0) + IsNull(@MoneyOut, 0)
    FROM @Results R
    WHERE R.ID = 3
    ;

    -- Pull ID = 4 - Current A/Rs
    -- Pull ID = 6 - Current Pre-WIP
    -- Pull ID = 7 - Current WIP
    -- Pull ID = 8 - Current Built
    -- Pull ID = 9 - Current Invoicing
    UPDATE R
    SET [Count] = Data.TheCount
      , Amount = Data.TheAmount
    FROM @Results R
    JOIN (
        SELECT COUNT(O.ID) AS TheCount
             , SUM(O.[Price.PreTax]) as TheAmount
             , (CASE O.OrderStatusID WHEN 21 THEN 6 WHEN 22 THEN 7 WHEN 23 THEN 8 WHEN 24 THEN 9 WHEN 25 THEN 4 END) AS RowID
            FROM [Order.Data] O
            RIGHT JOIN (Values (21), (22), (23), (24), (25)) AS OS(ID) ON O.OrderStatusID = OS.ID
            WHERE (O.BID IS NULL)
               OR (O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL))
            GROUP BY OS.ID, OrderStatusID
    ) Data ON R.ID = Data.RowID
    ;


    -- Pull ID = 5 - MTD Closed
    UPDATE R
    SET [Count] = Data.TheCount, Amount = Data.TheAmount
    FROM @Results R
    JOIN (
        SELECT COUNT(O.ID) AS TheCount
             , SUM(O.[Price.PreTax]) as TheAmount
        FROM [Order.Data] O
        WHERE O.BID = @BID 
            AND (@LocationID = O.LocationID OR @LocationID IS NULL)
            AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 9 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
                    )
    ) Data ON R.ID = 5
    ;

    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------
END

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
