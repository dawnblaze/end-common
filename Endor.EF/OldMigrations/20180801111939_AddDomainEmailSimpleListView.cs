using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180801111939_AddDomainEmailSimpleListView")]
    public partial class AddDomainEmailSimpleListView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Domain.Email.SimpleList]
                GO
                CREATE VIEW [dbo].[Domain.Email.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                    , [IsActive]
                    , CONVERT(BIT, 0) AS[HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[Part.Material.Category];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Domain.Email.SimpleList]
            ");
        }
    }
}

