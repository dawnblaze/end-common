using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddTranslationOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    INSERT INTO [dbo].[System.Option.Definition]
                        ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden])
                    VALUES
                        (1045, 'Localization.Language', 'Default Language', '', 0, 103, 'en,en-uk,en-au,fr,es', 'en', 0);
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    DELETE [dbo].[System.Option.Definition] WHERE [ID]=1045 and [Name]='Localization.Language'
                "
            );
        }
    }
}
