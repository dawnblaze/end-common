using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180607155339_AddOrderItemDates")]
    public partial class AddOrderItemDates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddForeignKey(
                name: "FK_Order.KeyDate_OrderItemID",
                table: "Order.KeyDate",
                columns: new[] { "BID", "OrderItemID" },
                principalTable: "Order.Item.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.KeyDate_OrderItemID",
                table: "Order.KeyDate");
        }
    }
}

