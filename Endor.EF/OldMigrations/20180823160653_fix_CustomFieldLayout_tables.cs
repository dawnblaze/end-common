using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180823160653_fix_CustomFieldLayout_tables")]
    public partial class fix_CustomFieldLayout_tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

ALTER TABLE dbo.[CustomField.Layout.Definition]
DROP COLUMN IF EXISTS [Name];

ALTER TABLE dbo.[CustomField.Layout.Definition]
ADD [Name] NVARCHAR(100);

UPDATE dbo.[CustomField.Layout.Definition] SET [NAME]=''
WHERE [Name] IS NULL;

ALTER TABLE dbo.[CustomField.Layout.Definition]
ALTER COLUMN [Name] NVARCHAR(100) NOT NULL;

DROP INDEX IF EXISTS [IX_CustomField.Layout.Definition_Type] ON [CustomField.Layout.Definition];

ALTER TABLE dbo.[CustomField.Layout.Definition]
ALTER COLUMN [AppliesToClassTypeID] SMALLINT NOT NULL;

CREATE INDEX [IX_CustomField.Layout.Definition_Type] ON [CustomField.Layout.Definition]
(
    [BID] ASC,
    [AppliesToClassTypeID] ASC,
    [SortIndex] ASC,
    [IsSubTab] ASC
);
--------------------------------------------
--------------------------------------------
ALTER TABLE dbo.[CustomField.Layout.Container]
DROP CONSTRAINT IF EXISTS [FK_CustomField.Layout.Container_CustomField.Layout.Definition];
ALTER TABLE dbo.[CustomField.Layout.Element]
DROP CONSTRAINT IF EXISTS [FK_CustomField.Layout.Element_CustomField.Layout.Container];
ALTER TABLE dbo.[CustomField.Layout.Container]
DROP CONSTRAINT IF EXISTS [PK_CustomField.Layout.Container];


ALTER TABLE dbo.[CustomField.Layout.Container]
ALTER COLUMN [ID] INT NOT NULL;


ALTER TABLE dbo.[CustomField.Layout.Container]---------------
ADD CONSTRAINT [PK_CustomField.Layout.Container] PRIMARY KEY ( [BID] ASC, [ID] ASC );
ALTER TABLE [CustomField.Layout.Element]  WITH CHECK---------------
ADD CONSTRAINT [FK_CustomField.Layout.Element_CustomField.Layout.Container]
FOREIGN KEY([BID], [ContainerID]) REFERENCES [CustomField.Layout.Container] ([BID], [ID]);
ALTER TABLE [CustomField.Layout.Container]  WITH CHECK----------------
ADD CONSTRAINT [FK_CustomField.Layout.Container_CustomField.Layout.Definition]
FOREIGN KEY([BID], [LayoutID]) REFERENCES [CustomField.Layout.Definition] ([BID], [ID]);
ALTER TABLE [CustomField.Layout.Container]
CHECK CONSTRAINT [FK_CustomField.Layout.Container_CustomField.Layout.Definition];
--------------------------------------------
--------------------------------------------
ALTER TABLE dbo.[CustomField.Layout.Element]
DROP CONSTRAINT IF EXISTS [PK_CustomField.Layout.Element];
ALTER TABLE dbo.[CustomField.Layout.Element]
DROP CONSTRAINT IF EXISTS [FK_CustomField.Layout.Element_CustomField.Definition];


ALTER TABLE dbo.[CustomField.Layout.Element]
ALTER COLUMN [ID] INT NOT NULL;

ALTER TABLE dbo.[CustomField.Layout.Element]
ALTER COLUMN [ElementProperties] [xml] SPARSE  NULL;

ALTER TABLE dbo.[CustomField.Layout.Element]---
ADD CONSTRAINT [PK_CustomField.Layout.Element] PRIMARY KEY ( [BID] ASC, [ID] ASC );
ALTER TABLE [CustomField.Layout.Element]  WITH CHECK-----
ADD CONSTRAINT [FK_CustomField.Layout.Element_CustomField.Definition]
FOREIGN KEY([BID], [DefinitionID]) REFERENCES [CustomField.Definition] ([BID], [ID]);
ALTER TABLE [CustomField.Layout.Element]
CHECK CONSTRAINT [FK_CustomField.Layout.Element_CustomField.Definition];

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

