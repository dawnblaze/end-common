using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180802170723_AddDomainEmailComputedClassTypeID")]
    public partial class AddDomainEmailComputedClassTypeID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Domain.Email.Data",
                nullable: false,
                computedColumnSql: "((1022))",
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Domain.Email.Data",
                nullable: false,
                oldClrType: typeof(int),
                oldComputedColumnSql: "((1022))");
        }
    }
}

