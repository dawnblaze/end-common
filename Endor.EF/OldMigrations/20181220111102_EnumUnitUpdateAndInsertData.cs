using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class EnumUnitUpdateAndInsertData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [enum.Part.Unit]
                SET UnitSystem = 1
                WHERE ID = 21
                GO

                INSERT INTO [enum.Part.Unit]
                SELECT ID + 100, 'Sq '+Abbreviation, 'Square '+Name, ToStandardMultiplyBy*ToStandardMultiplyBy, UnitClassification, UnitSystem, 2 as Dimensionality
                FROM [enum.Part.Unit]
                WHERE UnitClassification = 2
                    AND Dimensionality = 1
                GO

                INSERT INTO [enum.Part.Unit]
                SELECT ID + 200, 'Cu '+Abbreviation, 'Cubic '+Name, ToStandardMultiplyBy*ToStandardMultiplyBy, UnitClassification, UnitSystem, 3 as Dimensionality
                FROM [enum.Part.Unit]
                WHERE UnitClassification = 2
                    AND Dimensionality = 1
                GO

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
