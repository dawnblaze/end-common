using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_Profile_MachineLayoutType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "MachineLayoutType",
                table: "Part.Machine.Profile",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Profile_enum.Part.Subassembly.MachineLayoutType",
                table: "Part.Machine.Profile",
                column: "MachineLayoutType",
                principalTable: "enum.Machine.LayoutType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Profile_enum.Part.Subassembly.MachineLayoutType",
                table: "Part.Machine.Profile");

            migrationBuilder.DropColumn(
                name: "MachineLayoutType",
                table: "Part.Machine.Profile");
        }
    }
}
