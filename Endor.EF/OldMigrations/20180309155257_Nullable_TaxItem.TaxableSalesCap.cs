using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180309155257_Nullable_TaxItem.TaxableSalesCap")]
    public partial class Nullable_TaxItemTaxableSalesCap : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Accounting.Tax.Item]
                ALTER COLUMN [TaxableSalesCap] [decimal](18, 4) NULL
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Accounting.Tax.Item]
                ALTER COLUMN [TaxableSalesCap] [decimal](18, 4) NOT NULL
            ");
        }
    }
}

