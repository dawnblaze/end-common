using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class InitDashboardWidgetCategoryLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Categories",
                table: "System.Dashboard.Widget.Definition");

            migrationBuilder.CreateTable(
                name: "System.Dashboard.Widget.CategoryLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    WidgetDefID = table.Column<short>(nullable: false),
                    CategoryType = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Dashboard.Widget.CategoryLink", x => new { x.BID, x.WidgetDefID, x.CategoryType });
                    table.ForeignKey(
                        name: "FK_DashboardWidgetCategoryLink.Data_enum.DashboardWidgetCategoryType",
                        column: x => x.CategoryType,
                        principalTable: "enum.Dashboard.Widget.CategoryType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DashboardWidgetCategoryLink.WidgetDefinition",
                        column: x => x.WidgetDefID,
                        principalTable: "System.Dashboard.Widget.Definition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_System.Dashboard.Widget.CategoryLink_CategoryType",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "CategoryType");

            migrationBuilder.CreateIndex(
                name: "IX_System.Dashboard.Widget.CategoryLink_WidgetDefID",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "WidgetDefID");

            migrationBuilder.CreateIndex(
                name: "IX_DashboardWidgetCategoryLink.DashboardWidgetCategoryType",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] { "BID", "CategoryType" });

            migrationBuilder.CreateIndex(
                name: "IX_DashboardWidgetCategoryLink.WidgetDefinition",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] { "BID", "WidgetDefID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.AddColumn<string>(
                name: "Categories",
                table: "System.Dashboard.Widget.Definition",
                type: "nvarchar(MAX)",
                nullable: true);
        }
    }
}
