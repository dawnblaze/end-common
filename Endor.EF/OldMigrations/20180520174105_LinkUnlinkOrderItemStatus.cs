using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180520174105_LinkUnlinkOrderItemStatus")]
    public partial class LinkUnlinkOrderItemStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Order.Item.Status.SimpleList]
                GO
                CREATE VIEW [dbo].[Order.Item.Status.SimpleList]
                    AS
                SELECT	[BID]
                        , [ID]
                        , [ClassTypeID]
                        , [Name] as DisplayName
                        , [IsActive]
                        , CONVERT(BIT, 0) AS [IsDefault]
                FROM [Order.Item.Status];
                GO
            ");

            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Order.Item.SubStatus.SimpleList]
                GO
                CREATE VIEW [dbo].[Order.Item.SubStatus.SimpleList]
                    AS
                SELECT	[BID]
                        , [ID]
                        , [ClassTypeID]
                        , [Name] as DisplayName
                        , CONVERT(BIT, 0) AS [IsDefault]
                FROM [Order.Item.SubStatus];
                GO
            ");

            migrationBuilder.Sql(@"
                /****** Object:  StoredProcedure [dbo].[Order.Item.Status.Action.LinkSubStatus]    Script Date: 5/19/2018 12:52:19 AM ******/
                SET ANSI_NULLS ON
                GO
                SET QUOTED_IDENTIFIER ON
                GO

                -- ========================================================
                -- Name: [Order.Item.Status.Action.LinkSubStatus]
                --
                -- Description: This procedure links/unlinks the OrderStatus to the SubStatus
                --
                -- Sample Use:   EXEC dbo.[Order.Item.Status.Action.LinkSubStatus] @BID=1, @SubStatusID=1, @OrderStatusID=16, @IsLinked=1
                -- ========================================================
                CREATE PROCEDURE [dbo].[Order.Item.Status.Action.LinkSubStatus]
                --DECLARE 
                          @BID                  TINYINT  --= 1
                        , @SubStatusID          SMALLINT --= 2
                        , @OrderStatusID        SMALLINT --= 1
                        , @IsLinked             BIT     = 1
                        , @Result               INT     = NULL  OUTPUT
                AS
                BEGIN
                    DECLARE @Message VARCHAR(1024);

                    -- Check if the SubStatus specified is valid
                    IF NOT EXISTS(SELECT * FROM [Order.Item.SubStatus] WHERE BID = @BID and ID = @SubStatusID)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Invalid SubStatus Specified. SubStatusID='+CONVERT(VARCHAR(12),@SubStatusID)+' not found'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END;

                    -- Check if the OrderStatus specified is valid
                    IF NOT EXISTS(SELECT * FROM [Order.Item.Status] WHERE BID = @BID and ID = @OrderStatusID)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Invalid OrderStatus Specified. OrderStatusID='+CONVERT(VARCHAR(12),@OrderStatusID)+' not found'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END;

                    IF @IsLinked = 1
                    BEGIN
                        -- Add new entry to Order.Item.Status.SubStatusLink if link is not yet found
                        IF NOT EXISTS(SELECT * FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID and SubStatusID = @SubStatusID and StatusID = @OrderStatusID)
                        BEGIN
                            INSERT INTO [Order.Item.Status.SubStatusLink] (BID, SubStatusID, StatusID)
                            VALUES (@BID, @SubStatusID, @OrderStatusID)
                        END;

                    END
                    ELSE
                    BEGIN
                        -- Remove entry from Order.Item.Status.SubStatusLink
                        DELETE FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID and SubStatusID = @SubStatusID and StatusID = @OrderStatusID
                    END

                    SET @Result = 1;

                    SELECT @Result as Result;
                END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Order.Item.SubStatus.SimpleList]
            ");

            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Order.Item.Status.SimpleList]
            ");

            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [dbo].[Order.Item.Status.Action.LinkSubStatus];
            ");
        }
    }
}

