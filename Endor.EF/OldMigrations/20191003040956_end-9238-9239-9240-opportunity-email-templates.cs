﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class end923892399240opportunityemailtemplates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

                INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
                VALUES (611,'Opportunity Statuses',600, 'Opportunity Statuses', 2, 0, 'Opportunity Option Status Workflow Stage Station Position');

                GO

                INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
                VALUES (610,'Opportunity Options',600, 'Opportunity Options', 2, 0, 'Opportunity Option Sale Status Goal');

                GO

                INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
                VALUES (2101,'Email Templates',2100, 'Email Templates', 2, 0, 'Email Template Marketing Merge Mail Mailmerge Marketing Invoice Estimate Google Gmail Outlook Custom');

                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
