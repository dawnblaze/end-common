using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END5202EmailAccountData4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "DefaultEmailAccountID",
                table: "Location.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "DefaultEmailAccountID",
                table: "Employee.Data",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location.Data_BID_DefaultEmailAccountID",
                table: "Location.Data",
                columns: new[] { "BID", "DefaultEmailAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.Data_BID_DefaultEmailAccountID",
                table: "Employee.Data",
                columns: new[] { "BID", "DefaultEmailAccountID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Data_DefaultEmailAccountID",
                table: "Employee.Data",
                columns: new[] { "BID", "DefaultEmailAccountID" },
                principalTable: "Email.Account.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Location.Data_DefaultEmailAccountID",
                table: "Location.Data",
                columns: new[] { "BID", "DefaultEmailAccountID" },
                principalTable: "Email.Account.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Data_DefaultEmailAccountID",
                table: "Employee.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Data_DefaultEmailAccountID",
                table: "Location.Data");

            migrationBuilder.DropIndex(
                name: "IX_Location.Data_BID_DefaultEmailAccountID",
                table: "Location.Data");

            migrationBuilder.DropIndex(
                name: "IX_Employee.Data_BID_DefaultEmailAccountID",
                table: "Employee.Data");

            migrationBuilder.DropColumn(
                name: "DefaultEmailAccountID",
                table: "Location.Data");

            migrationBuilder.DropColumn(
                name: "DefaultEmailAccountID",
                table: "Employee.Data");
        }
    }
}
