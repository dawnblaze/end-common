﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9534FixForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Order.Item.Component",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Order.Item.Component",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "ItemComponentID" },
                principalTable: "Order.Item.Component",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Order.Item.Component",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Order.Item.Component",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "ItemComponentID" },
                principalTable: "Order.Tax.Item.Assessment",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
