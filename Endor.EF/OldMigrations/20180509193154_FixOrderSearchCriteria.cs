using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180509193154_FixOrderSearchCriteria")]
    public partial class FixOrderSearchCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter.Criteria]
SET Field = 'StatusID',
DataType = 4,
InputType = 10
 WHERE TargetClassTypeID = 10000 AND Name = 'OrderStatus';

UPDATE [System.List.Filter.Criteria]
SET Field = 'EmployeeRolesIDs',
DataType = 4,
InputType = 9
 WHERE TargetClassTypeID = 10000 AND Name = 'Employees';

UPDATE [System.List.Filter.Criteria]
SET Field = 'CompanyName',
DataType = 0,
InputType = 0
 WHERE TargetClassTypeID = 10000 AND Name = 'Company';

UPDATE [System.List.Filter.Criteria]
SET Field = 'ContactRolesNames',
DataType = 0,
InputType = 0
WHERE TargetClassTypeID = 10000 AND Name = 'Contact';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

