using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CREATE_TABLE_MessageBody : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Message.Body",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AttachedFileCount = table.Column<byte>(type: "int", nullable: false, defaultValue: (byte)0),
                    AttachedFileNames = table.Column<string>(type: "varchar(max) sparse", nullable: true),
                    BodyFirstLine = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14110))"),
                    HasAttachment = table.Column<bool>(nullable: false),
                    HasBody = table.Column<bool>(nullable: false),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    SizeInKB = table.Column<int>(type: "int", nullable: true),
                    Subject = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    WasModified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Body", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Message.Content_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Message.Body");
        }
    }
}
