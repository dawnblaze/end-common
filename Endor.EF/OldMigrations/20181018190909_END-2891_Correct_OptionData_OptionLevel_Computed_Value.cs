using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181018190909_END-2891_Correct_OptionData_OptionLevel_Computed_Value")]
    public partial class END2891_Correct_OptionData_OptionLevel_Computed_Value : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP INDEX IF EXISTS [IX_Option.Data_BID_OptionID_Level] ON [dbo].[Option.Data]");
            migrationBuilder.Sql(@"DROP INDEX IF EXISTS [IX_Option.Data_OptionID_Level_BIDNull] ON [dbo].[Option.Data]");

            migrationBuilder.AlterColumn<byte>(
                name: "OptionLevel",
                table: "Option.Data",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (128) when [CompanyID] IS NOT NULL then (64) when [UserID] IS NOT NULL then (32) when [StorefrontID] IS NOT NULL then (16) when [LocationID] IS NOT NULL then (8) when [BID] IS NOT NULL then (4) when [AssociationID] IS NOT NULL then (2) else (0) end),(0)))",
                oldClrType: typeof(byte),
                oldComputedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (64) when [CompanyID] IS NOT NULL then (32) when [UserID] IS NOT NULL then (16) when [StorefrontID] IS NOT NULL then (8) when [LocationID] IS NOT NULL then (4) when [BID] IS NOT NULL then (2) when [AssociationID] IS NOT NULL then (1) else (0) end),(0)))");

            migrationBuilder.CreateIndex(
                name: "IX_Option.Data_BID_OptionID_Level",
                table: "Option.Data",
                columns: new[] { "BID", "OptionID", "OptionLevel" });

            migrationBuilder.CreateIndex(
                name: "IX_Option.Data_OptionID_Level_BIDNull",
                table: "Option.Data",
                columns: new[] { "OptionID", "OptionLevel" },
                filter: "([BID] IS NULL)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "OptionLevel",
                table: "Option.Data",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (64) when [CompanyID] IS NOT NULL then (32) when [UserID] IS NOT NULL then (16) when [StorefrontID] IS NOT NULL then (8) when [LocationID] IS NOT NULL then (4) when [BID] IS NOT NULL then (2) when [AssociationID] IS NOT NULL then (1) else (0) end),(0)))",
                oldClrType: typeof(byte),
                oldComputedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (128) when [CompanyID] IS NOT NULL then (64) when [UserID] IS NOT NULL then (32) when [StorefrontID] IS NOT NULL then (16) when [LocationID] IS NOT NULL then (8) when [BID] IS NOT NULL then (4) when [AssociationID] IS NOT NULL then (2) else (0) end),(0)))");
        }
    }
}

