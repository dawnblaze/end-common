using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181005134310_FixDashboardAPIModels")]
    public partial class FixDashboardAPIModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Dashboard.Widget.Data_Business.Data",
                table: "Dashboard.Widget.Data");

            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data");

            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_Dashboard",
                table: "Dashboard.Data");

            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data");

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data",
                columns: new[] { "BID", "Module", "IsSystem" });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data",
                columns: new[] { "BID", "UserLinkID", "Module", "IsActive", "IsDefault" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data");

            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data");

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data",
                columns: new[] { "BID", "Module", "IsSystem", "ID" });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_Dashboard",
                table: "Dashboard.Data",
                columns: new[] { "BID", "ID", "UserLinkID", "Module", "IsActive", "IsDefault" });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data",
                columns: new[] { "BID", "UserLinkID", "Module", "ID", "IsActive", "IsDefault" });

            migrationBuilder.AddForeignKey(
                name: "FK_Dashboard.Widget.Data_Business.Data",
                table: "Dashboard.Widget.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

