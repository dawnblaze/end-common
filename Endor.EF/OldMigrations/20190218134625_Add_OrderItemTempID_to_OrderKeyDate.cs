using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_OrderItemTempID_to_OrderKeyDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OrderItemTempID",
                table: "Order.KeyDate",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[List.FlatList.Data]
                           ([BID]
                           ,[ID]
                           ,[FlatListType]
                           ,[IsActive]
                           ,[IsAdHoc]
                           ,[IsSystem]
                           ,[Name])
                     VALUES
                           (1
                           ,101
                           ,31
                           ,1
                           ,0
                           ,1
                           ,'Default')
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "OrderItemTempID",
                table: "Order.KeyDate");

            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[List.FlatList.Data] WHERE ID = 101
                GO
            ");
        }
    }
}
