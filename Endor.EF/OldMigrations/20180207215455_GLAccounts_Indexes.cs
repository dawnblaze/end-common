using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180207215455_GLAccounts_Indexes")]
    public partial class GLAccounts_Indexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Account_BID_ParentID",
                table: "Accounting.GL.Account");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_Name",
                table: "Accounting.GL.Account",
                columns: new[] { "BID", "Name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_NumberedName",
                table: "Accounting.GL.Account",
                columns: new[] { "BID", "NumberedName" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_ParentID",
                table: "Accounting.GL.Account",
                columns: new[] { "BID", "ParentID", "IsActive" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Account_Name",
                table: "Accounting.GL.Account");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Account_NumberedName",
                table: "Accounting.GL.Account");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Account_ParentID",
                table: "Accounting.GL.Account");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_BID_ParentID",
                table: "Accounting.GL.Account",
                columns: new[] { "BID", "ParentID" });
        }
    }
}

