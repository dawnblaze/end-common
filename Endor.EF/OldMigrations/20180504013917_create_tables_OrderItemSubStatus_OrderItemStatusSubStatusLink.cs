using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180504013917_create_tables_OrderItemSubStatus_OrderItemStatusSubStatusLink")]
    public partial class create_tables_OrderItemSubStatus_OrderItemStatusSubStatusLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Order.Item.SubStatus",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((10023))"),
                    Description = table.Column<string>(type: "NVARCHAR(255)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(7)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Name = table.Column<string>(type: "NVARCHAR(100)", nullable: false),
                    ValidToDT = table.Column<DateTime>(type: "DATETIME2(7)", nullable: false, defaultValueSql: "CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Item.SubStatus", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Order.Item.SubStatus_BID",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Order.Item.Status.SubStatusLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    StatusID = table.Column<short>(nullable: false),
                    SubStatusID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Item.Status.SubStatusLink", x => new { x.BID, x.StatusID, x.SubStatusID });
                    table.ForeignKey(
                        name: "FK_Order.Item.Status.SubStatusLink_Status",
                        columns: x => new { x.BID, x.StatusID },
                        principalTable: "Order.Item.Status",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order.Item.Status.SubStatusLink_SubStatus",
                        columns: x => new { x.BID, x.SubStatusID },
                        principalTable: "Order.Item.SubStatus",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Status.SubStatusLink_SubStatus",
                table: "Order.Item.Status.SubStatusLink",
                columns: new[] { "BID", "SubStatusID", "StatusID" });

            migrationBuilder.CreateIndex(
                name: "X_Order.Item.SubStatus_Name",
                table: "Order.Item.SubStatus",
                columns: new[] { "BID", "Name" },
                unique: true);

            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Item.SubStatus]
                ADD PERIOD FOR SYSTEM_TIME([ModifiedDT], [ValidToDT])
                GO
                ALTER TABLE [Order.Item.SubStatus]
                ALTER COLUMN [ValidToDT] ADD HIDDEN
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Order.Item.Status.SubStatusLink");

            migrationBuilder.DropTable(
                name: "Order.Item.SubStatus");
        }
    }
}

