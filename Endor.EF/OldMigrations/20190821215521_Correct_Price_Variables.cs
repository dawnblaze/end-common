using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Correct_Price_Variables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [Part.Subassembly.Variable]
                SET Name = 'Price'
                WHERE Name = 'AssemblyPrice'
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
