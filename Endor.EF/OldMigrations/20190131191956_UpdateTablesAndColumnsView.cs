using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateTablesAndColumnsView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [enum.ClassType] SET TableName='Accounting.Tax.Code'
WHERE ID=8005;
");

            migrationBuilder.Sql(@"
-- =================================================
-- This is a Helper View that is used to return a collection
-- of table and view information.  It is not optimized for high
-- volume use and should generally be limited to development and
-- infrequently run queries.
-- =================================================
CREATE OR ALTER VIEW [dbo].[TablesAndColumns]
AS
SELECT TOP 100 PERCENT
       tbl.name as TableName, 
       tbl.object_id as TableID, 
       tbl.type_desc as TableType,
       col.column_id as ColumnID,  
       col.name as ColumnName,
       coltypes.name as ColumnType, 
       col.Is_Nullable as IsNullable,
       IIF(EXISTS(select * from sys.default_constraints D where D.object_id = col.default_object_id), CONVERT(BIT,1), CONVERT(BIT,0)) as HasDefault,
       col.is_computed IsComputed, 
       col.is_sparse IsSparse, 
       col.is_identity IsIdentity,
       (select TOP 1 definition from sys.default_constraints D where D.object_id = col.default_object_id) as DefaultFormula,
       comcol.definition ComputedFormula,
       col.max_length, 
       col.Precision, 
       col.Scale,
       IIF(EXISTS(SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE PK 
                  WHERE PK.TABLE_NAME = tbl.name AND PK.COLUMN_NAME = col.name AND PK.CONSTRAINT_NAME like 'PK_%')
           , CONVERT(BIT,1), CONVERT(BIT,0)) as IsPK,
       IIF(EXISTS(SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE PK 
                  WHERE PK.TABLE_NAME = tbl.name AND PK.COLUMN_NAME = col.name)
           , CONVERT(BIT,1), CONVERT(BIT,0)) as IsIndexed,
       IIF(EXISTS(SELECT * FROM sys.tables ST WHERE tbl.object_id = ST.history_table_id), CONVERT(BIT,1), CONVERT(BIT,0)) AS IsHistoryTable,
       IIF(tbl.history_table_id IS NOT NULL, 1, 0) AS IsTemporalTable,
       HistoryTable.name as HistoryTableName,
       (CASE col.generated_always_type WHEN 1 THEN 'AS_ROW_START' WHEN 2 THEN 'AS_ROW_END' ELSE NULL END) as GeneratedAlwaysType

FROM sys.tables tbl
     JOIN sys.columns col ON tbl.object_id = col.object_id
     JOIN sys.types coltypes ON col.system_type_id = coltypes.system_type_id AND col.user_type_id = coltypes.user_type_id
     --LEFT JOIN ( SELECT CONVERT(BIT, 1) AS IsPK, table_name, column_name
     --            FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
     --            WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1 ) AS PKs ON PKs.table_name = tbl.name AND PKs.column_name = col.name
     LEFT JOIN sys.computed_columns comcol ON tbl.object_id = comcol.object_id AND col.column_id = comcol.column_id
     LEFT JOIN sys.tables HistoryTable on HistoryTable.object_id = tbl.history_table_id

WHERE tbl.type = 'U' AND tbl.is_ms_shipped = 0
ORDER BY TableName, ColumnID
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [dbo].[TablesAndColumns]
AS
SELECT TOP 100 PERCENT
       tbl.name as TableName, tbl.object_id as TableID, tbl.type_desc as TableType,
       col.column_id as ColumnID,  col.name as ColumnName,
       coltypes.name as ColumnType, col.Is_Nullable as IsNullable,
       col.is_computed IsComputed, col.is_sparse IsSparse, col.is_identity IsIdentity,
       comcol.definition ComputedFormula, col.max_length, PKs.IsPK, col.Precision, col.Scale,
       IIF(EXISTS(SELECT * FROM sys.tables ST WHERE tbl.object_id = ST.history_table_id), 1, 0) AS IsHistoryTable,
       IIF(tbl.history_table_id IS NOT NULL, 1, 0) AS IsTemporalTable,
       HistoryTable.name as HistoryTableName

FROM sys.tables tbl
     JOIN sys.columns col ON tbl.object_id = col.object_id
     JOIN sys.types coltypes ON col.system_type_id = coltypes.system_type_id AND col.user_type_id = coltypes.user_type_id
     LEFT JOIN ( SELECT 1 AS IsPK, table_name, column_name
                 FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                 WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1 ) AS PKs ON PKs.table_name = tbl.name AND PKs.column_name = col.name
     LEFT JOIN sys.computed_columns comcol ON tbl.object_id = comcol.object_id AND col.column_id = comcol.column_id
     LEFT JOIN sys.tables HistoryTable on HistoryTable.object_id = tbl.history_table_id

WHERE tbl.type = 'U' AND tbl.is_ms_shipped = 0

ORDER BY TableName, ColumnID
");

            migrationBuilder.Sql(@"
UPDATE [enum.ClassType] SET TableName='Accounting.Taxability.Code'
WHERE ID=8005;
");
        }
    }
}
