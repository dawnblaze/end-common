﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9985_Update_PartMaterialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "MinimumQuantity",
                table: "Part.Material.Data",
                type: "decimal(18,4)",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<decimal>(
                name: "RoundingFactor",
                table: "Part.Material.Data",
                type: "decimal(18,8)",
                nullable: false,
                defaultValueSql: "0.000001");

            migrationBuilder.AddColumn<byte>(
                name: "UnitType",
                table: "Part.Material.Data",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "9");

            migrationBuilder.Sql(@"
                UPDATE [Part.Material.Data]
                SET UnitType = CASE
                    WHEN ConsumptionUnit = 0 THEN 0  -- None/Number
                    WHEN ConsumptionUnit BETWEEN 1 AND 19 THEN 9  -- Discrete
                    WHEN ConsumptionUnit BETWEEN 21 AND 29 THEN 11  -- Length
                    WHEN ConsumptionUnit BETWEEN 31 AND 39 THEN 15  -- Weight
                    WHEN ConsumptionUnit BETWEEN 41 AND 49 THEN 19  -- Time
                    WHEN ConsumptionUnit BETWEEN 121 AND 129 THEN 12  -- Area
                    WHEN ConsumptionUnit BETWEEN 221 AND 329 THEN 13  -- Volume (Solid)
                    ELSE 0 END,
                ConsumptionUnit = CASE
                    WHEN ConsumptionUnit = 0 THEN 0  -- None/Number
                    WHEN ConsumptionUnit BETWEEN 1 AND 19 THEN ConsumptionUnit  -- Discrete
                    WHEN ConsumptionUnit BETWEEN 21 AND 29 THEN 33  -- Foot
                    WHEN ConsumptionUnit BETWEEN 31 AND 39 THEN 72  -- Pound
                    WHEN ConsumptionUnit BETWEEN 41 AND 49 THEN 113  -- Hour
                    WHEN ConsumptionUnit BETWEEN 121 AND 129 THEN 43  -- SqFeet
                    WHEN ConsumptionUnit BETWEEN 221 AND 329 THEN 53  -- CuFeet
                    ELSE 0 END
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MinimumQuantity",
                table: "Part.Material.Data");

            migrationBuilder.DropColumn(
                name: "RoundingFactor",
                table: "Part.Material.Data");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "Part.Material.Data");
        }
    }
}
