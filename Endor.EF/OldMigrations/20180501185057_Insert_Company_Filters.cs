using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180501185057_Insert_Company_Filters")]
    public partial class Insert_Company_Filters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                           ([TargetClassTypeID]
                           ,[Name]
                           ,[Label]
                           ,[Field]
                           ,[IsHidden]
                           ,[DataType]
                           ,[InputType]
                           ,[AllowMultiple]
                           ,[ListValues]
                           ,[ListValuesEndpoint]
                           ,[IsLimitToList]
                           ,[SortIndex])
                     VALUES
                           (2000 --<TargetClassTypeID, int,>
                           ,'Name' --<Name, varchar(255),>
                           ,'Name' --<Label, varchar(255),>
                           ,'Name' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,0 --<DataType, tinyint,>
                           ,0 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,0) --<SortIndex, tinyint,>)

                           ,(2000 --<TargetClassTypeID, int,>
                           ,'Is Active' --<Name, varchar(255),>
                           ,'Is Active' --<Label, varchar(255),>
                           ,'IsActive' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,3 --<DataType, tinyint,>
                           ,2 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,'Is Not Active,Is Active' --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,1 --<IsLimitToList, bit,>
                           ,1) --<SortIndex, tinyint,>)

                           ,(2000 --<TargetClassTypeID, int,>
                           ,'SalesPerson' --<Name, varchar(255),>
                           ,'Sales Person' --<Label, varchar(255),>
                           ,'SalesPerson' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,0 --<DataType, tinyint,>
                           ,0 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,2) --<SortIndex, tinyint,>)

                           ,(2000 --<TargetClassTypeID, int,>
                           ,'Location' --<Name, varchar(255),>
                           ,'Location' --<Label, varchar(255),>
                           ,'Location' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,0 --<DataType, tinyint,>
                           ,0 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,3) --<SortIndex, tinyint,>)

                           ,(2000 --<TargetClassTypeID, int,>
                           ,'ContactName' --<Name, varchar(255),>
                           ,'Contact Name' --<Label, varchar(255),>
                           ,'ContactName' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,0 --<DataType, tinyint,>
                           ,0 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,4) --<SortIndex, tinyint,>)
                GO

                INSERT INTO [dbo].[List.Filter]
                           ([BID]
                           ,[ID]
                           ,[CreatedDate]
                           ,[ModifiedDT]
                           ,[IsActive]
                           ,[Name]
                           ,[TargetClassTypeID]
                           ,[IDs]
                           ,[Criteria]
                           ,[OwnerID]
                           ,[IsPublic]
                           ,[IsSystem]
                           ,[Hint]
                           ,[IsDefault]
                           ,[SortIndex])
                     VALUES
                           (1 --<BID, smallint,>
                           ,1028 --<ID, int,>
                           ,GETUTCDATE() -- <CreatedDate, date,>
                           ,GETUTCDATE() --<ModifiedDT, datetime2(2),>
                           ,1 --<IsActive, bit,>
                           ,'Active' --<Name, varchar(255),>
                           ,2000 --<TargetClassTypeID, int,>
                           ,NULL --<IDs, xml,>
                           ,'<ArrayOfListFilterItem>
			                  <ListFilterItem>
				                <SearchValue>true</SearchValue>
				                <Field>IsActive</Field>
				                <IsHidden>false</IsHidden>
				                <IsSystem>true</IsSystem>
				                <DisplayText>Is Active</DisplayText>
			                  </ListFilterItem>
			                </ArrayOfListFilterItem>'
                           ,NULL --<OwnerID, smallint,>
                           ,0 --<IsPublic, bit,>
                           ,1 --<IsSystem, bit,>
                           ,NULL --<Hint, varchar(max),>
                           ,1 --<IsDefault, bit,>
                           ,0) --<SortIndex, tinyint,>)
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE [TargetClassTypeID]=2000 AND ([FIELD] IN ('NAME','IsActive','SalesPerson','Location','ContactName'))
                GO
                DELETE FROM [dbo].[List.Filter] WHERE ID=1028
                GO
            ");
        }
    }
}

