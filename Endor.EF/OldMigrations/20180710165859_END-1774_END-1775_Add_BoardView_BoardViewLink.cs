using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180710165859_END-1774_END-1775_Add_BoardView_BoardViewLink")]
    public partial class END1774_END1775_Add_BoardView_BoardViewLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Board.View",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14040))"),
                    Columns = table.Column<string>(type: "NVARCHAR(MAX)", nullable: true),
                    CustomLayout = table.Column<string>(type: "XML SPARSE", nullable: true),
                    DataType = table.Column<short>(type: "smallint", nullable: false),
                    GroupBy = table.Column<string>(type: "VARCHAR(MAX)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsListView = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(~[IsTileView])"),
                    IsSystem = table.Column<bool>(nullable: false),
                    IsTileView = table.Column<bool>(type: "bit", nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    SortOptions = table.Column<string>(type: "NVARCHAR(MAX)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Board.View", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Board.View.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    BoardID = table.Column<short>(nullable: false),
                    ViewID = table.Column<short>(nullable: false),
                    IsPrimary = table.Column<bool>(nullable: true, computedColumnSql: "(case when [SortIndex]=(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    SortIndex = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Board.View.Link", x => new { x.BID, x.BoardID, x.ViewID });
                    table.ForeignKey(
                        name: "FK_Board.View.Link_Board.Definition",
                        columns: x => new { x.BID, x.BoardID },
                        principalTable: "Board.Definition.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Board.View.Link_Link.Definition",
                        columns: x => new { x.BID, x.ViewID },
                        principalTable: "Board.View",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Board.View_DataType",
                table: "Board.View",
                columns: new[] { "BID", "DataType", "IsActive", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_Board.View_Name",
                table: "Board.View",
                columns: new[] { "BID", "Name", "IsActive", "DataType" });

            migrationBuilder.CreateIndex(
                name: "IX_Board.View.Link_BID_ViewID",
                table: "Board.View.Link",
                columns: new[] { "BID", "ViewID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Board.View.Link");

            migrationBuilder.DropTable(
                name: "Board.View");
        }
    }
}

