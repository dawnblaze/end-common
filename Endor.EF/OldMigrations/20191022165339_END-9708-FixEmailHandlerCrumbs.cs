﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9708FixEmailHandlerCrumbs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
update [System.Option.Section] set ParentID=1000 WHERE ID=2100
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
update [System.Option.Section] set ParentID=2000 WHERE ID=2100
            ");

        }
    }
}
