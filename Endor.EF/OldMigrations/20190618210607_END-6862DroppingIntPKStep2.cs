using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6862DroppingIntPKStep2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Part.Machine.Profile.Table",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Part.Machine.Profile.Table",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "BID", "ID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Part.Machine.Profile.Table",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Part.Machine.Profile.Table",
                table: "Part.Machine.Profile.Table",
                column: "BID");
        }
    }
}
