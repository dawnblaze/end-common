using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180717164703_END-1887_fix_Accounting.FiscalYear.StartingMonth_values")]
    public partial class END1887_fix_AccountingFiscalYearStartingMonth_values : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DECLARE @listVal NVARCHAR(MAX) = '';

                SET @listVal = (
                  SELECT def.ListValues FROM [System.Option.Definition] def
                  WHERE def.Name = 'Accounting.FiscalYear.StartingMonth'
                );

                SET @listVal = CONCAT('!@#', @listVal);
                SET @listVal = REPLACE(@listVal, '!@#,', '');
                SET @listVal = REPLACE(@listVal, '-', ' - ');

                WHILE (@listVal like '%  %')
                BEGIN
                  SET @listVal = REPLACE(@listVal, '  ', ' ');
                END

                WHILE (@listVal like '%, %' OR @listVal like '% ,%')
                BEGIN
                  SET @listVal = REPLACE(@listVal, ', ', ',');
                  SET @listVal = REPLACE(@listVal, ' ,', ',');
                END

                SET @listVal = LTRIM(RTRIM(@listVal));

                UPDATE [System.Option.Definition]
                SET [ListValues] = @listVal
                WHERE [Name] = 'Accounting.FiscalYear.StartingMonth';
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

