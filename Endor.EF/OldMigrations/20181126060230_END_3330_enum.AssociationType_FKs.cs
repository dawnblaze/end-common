using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END_3330_enumAssociationType_FKs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Business.Data]
                WITH CHECK ADD CONSTRAINT [FK_Business.Data_enum.AssociationType]
                    FOREIGN KEY ([AssociationType])
                    REFERENCES [dbo].[enum.AssociationType] ([ID])

                GO

                ALTER TABLE [dbo].[Option.Data]
                WITH CHECK ADD CONSTRAINT [FK_Option.Data_enum.AssociationType]
                    FOREIGN KEY ([AssociationID])
                    REFERENCES [dbo].[enum.AssociationType] ([ID])
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Business.Data] DROP CONSTRAINT IF EXISTS [FK_Business.Data_enum.AssociationType]
                GO
                ALTER TABLE [dbo].[Option.Data] DROP CONSTRAINT IF EXISTS [FK_Option.Data_enum.AssociationType]
            ");
        }
    }
}
