using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180510161440_FixIsVoidedOrderSearchCriteria")]
    public partial class FixIsVoidedOrderSearchCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
  UPDATE [List.Filter]
  SET Criteria = '<ArrayOfListFilterItem>
  <ListFilterItem>
    <SearchValue>_omit</SearchValue>
    <Field>IsVoided</Field>
    <IsHidden>false</IsHidden>
    <IsSystem>true</IsSystem>
    <DisplayText>Show Voided</DisplayText>
  </ListFilterItem>
</ArrayOfListFilterItem>' WHERE TargetClassTypeID = 10000 AND Name = 'Active';


UPDATE [System.List.Filter.Criteria]
SET Field = 'IsVoided',
ListValues = 'Hide Voided,Show Voided',
InputType = 11
WHERE TargetClassTypeID = 10000 AND Name = 'ShowVoidedOrders';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

