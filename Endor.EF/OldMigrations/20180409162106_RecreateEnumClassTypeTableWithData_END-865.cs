using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180409162106_RecreateEnumClassTypeTableWithData_END-865")]
    public partial class RecreateEnumClassTypeTableWithData_END865 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Delete old records
            migrationBuilder.Sql("DELETE FROM [dbo].[enum.ClassType]");

            //Schema Modification
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "enum.ClassType",
                unicode: false,
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "enum.ClassType",
                unicode: false,
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 2048,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TableName",
                table: "enum.ClassType",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RecordNameFromObjectIDSQL",
                table: "enum.ClassType",
                nullable: true);

            //Insert new records
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[enum.ClassType]
([ID],[Name],[Description],[TableName],[RecordNameFromObjectIDSQL])
VALUES
(1000, 'Business', NULL, 'Business.Data','SELECT Name FROM [Business.Data] TBL'),
(1001, 'BusinessCustom', NULL,'Business.Custom',''),
(1003, 'BusinessLocator', NULL,'Business.Locator','SELECT Name FROM [Business.Data] B JOIN [Business.Location] TBL ON B.BID = TBL.BID AND B.ID = TBL.ParentID'),
(1005, 'Location', NULL,'Location.Data','SELECT Name FROM [Location.Data] TBL'),
(1006, 'LocationCustom', NULL,'',''),
(1008, 'LocationLocator', NULL,'Location.Locator','SELECT Name FROM [Location.Data] B JOIN [Location.Location] TBL ON B.BID = TBL.BID AND B.ID = TBL.ParentID'),
(1100, 'Security Right', NULL,'',''),
(1101, 'Secuirty Group', NULL,'Security.Right.Group',''),
(1500, 'Document', NULL,'',''),
(1600, 'Association', NULL,'Association.Data',''),
(1700, 'ListFilter', NULL,'List.Filter',''),
(1710, 'SystemListFilterCriteria', NULL,'System.List.Filter.Criteria',''),
(1711, 'SystemListColumn', NULL,'System.List.Column',''),
(1800, 'OptionData', NULL,'Option.Data',''),
(2000, 'Company', NULL,'Company.Data','SELECT Name FROM [Company.Data] TBL'),
(2001, 'CompanyCustom', NULL,'Company.Custom',''),
(2003, 'CompanyLocator', NULL,'Company.Locator','SELECT Name FROM [Company.Data] B JOIN [Company.Location] TBL ON B.BID = TBL.BID AND B.ID = TBL.ParentID'),
(2011, 'Industry', NULL,'CRM.Industry','SELECT Name FROM [CRM.Industry] TBL'),
(2012, 'Origin', NULL,'CRM.Origin','SELECT Name FROM [CRM.Origin] TBL'),
(2013, 'TimeZone', NULL,'',''),
(2101, 'CustomFieldDef', NULL,'',''),
(3000, 'Contact', NULL,'Contact.Data','SELECT ShortName FROM [Contact.Data] TBL'),
(3001, 'ContactCustom', NULL,'Contact.Custom',''),
(3003, 'ContactLocator', NULL,'Contact.Locator',''),
(4000, 'Lead', NULL,'',''),
(4001, 'LeadCustom', NULL,'',''),
(4003, 'LeadLocator', NULL,'',''),
(4101, 'LeadSource', NULL,'',''),
(5000, 'Employee', NULL,'Employee.Data',''),
(5001, 'EmployeeCustom', NULL,'Employee.Custom',''),
(5003, 'EmployeeLocator', NULL,'Employee.Locator',''),
(5020, 'EmployeeTeam', NULL,'Employee.Team',''),
(5101, 'EmployeeRole', NULL,'',''),
(8000, 'AccountingGLAccount', NULL,'Accounting.GL.Account',''),
(8001, 'AccountingGLData', NULL,'Accounting.GL.Data',''),
(8002, 'AccountingPaymentMethod', NULL,'Accounting.Payment.Method',''),
(8007, 'AccountingCurrencyConversion', NULL,'Accounting.Currency.Conversion',''),
(8010, 'AccountingPaymentData', NULL,'Accounting.Payment.Data',''),
(9000, 'Opportunity', NULL,'Opportunity.Data',''),
(9001, 'OpportunityCustom', NULL,'Opportunity.Custom',''),
(9100, 'Campaign', NULL,'',''),
(9101, 'CampaignCustom', NULL,'',''),
(10000, 'Order', NULL,'Order.Data',''),
(10002, 'Estimate', NULL,'',''),
(10011, 'OrderKeyDate', 'Tracks Key Dates for Order, Items, Destinations','Order.KeyDate',''),
(10012, 'OrderStatusHistory', 'Tracks OrderStatus, Status, and SubStatus changes','Order.Status.History',''),
(10013, 'OrderContactRole', 'Links Contacts to Order, Items, Destinations','Order.Contact.Role',''),
(10014, 'OrderEmployeeRole', 'Links Employees to Order, Items, Destinations','Order.Employee.Role',''),
(10015, 'OrderNotes', 'Adds notes for Order, Items, Destinations','Order.Notes',''),
(10016, 'OrderContactLocator', 'Locators for the Contact Added','Order.Contact.Locator',''),
(10017, 'OrderOrderLink', 'Links one order to another to show a relationship like Cloned To/From, Converted To/From, Associated Master/Client, etc.','Order.OrderLink',''),
(10020, 'OrderItemData', NULL,'Order.Item.Data',''),
(10021, 'OrderItemPartLink', NULL,'Order.Item.Partlink',''),
(10022, 'OrderItemStatus', 'Use for ItemStatus and ItemSubStatus','Order.Item.Status',''),
(10040, 'OrderDestinationData', NULL,'Order.Destination.Data',''),
(10041, 'OrderDestinationItemLink', NULL,'Order.Destination.ItemLink',''),
(10042, 'OrderDestinationStatus', NULL,'Order.Destination.Status',''),
(10043, 'OrderDestinationSubstatus', NULL,'Order.Destination.Substatus',''),
(10051, 'OrderDestinationMethod', NULL,'Order.Destination.Method',''),
(10052, 'OrderDestinationMethodCostItem', NULL,'Order.Destination.Method.CostItem',''),
(10100, 'OrderLineItem', NULL,'',''),
(10102, 'EstimateLineItem', NULL,'',''),
(10300, 'EstimateVariation', NULL,'',''),
(10800, 'Job', NULL,'',''),
(11001, 'Activity', NULL,'Activity.Action',''),
(11002, 'Event', NULL,'Activity.Event',''),
(11003, 'GLActivity', NULL,'Activity.GLActivity',''),
(11004, 'InventoryActivity', NULL,'Activity.Inventory',''),
(11101, 'AccountingTaxGroup', NULL,'Accounting.Tax.Group',''),
(11102, 'AccountingTaxItem', NULL,'Accounting.Tax.Item',''),
(11105, 'AccountingPaymentTerm', NULL,'Accounting.Payment.Term',''),
(12000, 'MaterialParts', 'Material Parts','Part.Material.Data',''),
(12002, 'MaterialPartCategory', 'Material Part Category','Part.Material.Category',''),
(12004, 'MaterialPartInventory', 'Stores Inventory levels on Parts that are inventories.  (This is a temporal table.)','Part.Material.Inventory',''),
(12006, 'MaterialPartLedger', 'Ledger for all changes in Inventory levels.  Ties to an Inventory Activity.  Automatically adjusts the Inventory table (with triggers).','Part.Material.Ledger',''),
(12010, 'VendorCatalog', 'Vendor Catalog','	Vendor.Catalog.Data',''),
(12012, 'VendorCatalogItem', 'Item in a Vendor Catalog without any pricing.','Vendor.Catalog.Item',''),
(12014, 'VendorCatalogItemQuantityPrice', 'Price and Quantity Entry for a Catalog Item (at a particular pricing level)','Vendor.Catalog.Item.QuantityPrice',''),
(12016, 'MaterialPartCatalogItemLink', 'Link table connecting Materials. to Catalog Items.','Part.Material.Catalog.ItemLink',''),
(12020, 'LaborParts', 'Labor Parts','Part.Labor.Data',''),
(12022, 'LaborPartCategory', 'Labor Part Category','Part.Labor.Category',''),
(12030, 'MachineParts', 'Machine Parts','Part.Machine.Data',''),
(12032, 'MachinePartCategory', 'Machine Part Category','Part.Machine.Category',''),
(13000, 'ReportFile', NULL,'Report.File',''),
(13010, 'ReportMenu', NULL,'Report.Menu','')
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //Delete new records
            migrationBuilder.Sql("DELETE FROM [dbo].[enum.ClassType]");

            //Schema Modification
            migrationBuilder.DropColumn(
                name: "RecordNameFromObjectIDSQL",
                table: "enum.ClassType");

            migrationBuilder.DropColumn(
                name: "TableName",
                table: "enum.ClassType");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "enum.ClassType",
                unicode: false,
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "enum.ClassType",
                unicode: false,
                maxLength: 2048,
                nullable: true,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 255,
                oldNullable: true);

            //Insert old records
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[enum.ClassType]
([ID],[Name],[Description])
VALUES
(1000, 'Business', NULL),
(1001, 'BusinessCustom', NULL),
(1003, 'BusinessLocator', NULL),
(1005, 'Location', NULL),
(1006, 'LocationCustom', NULL),
(1008, 'LocationLocator', NULL),
(1100, 'Security Right', NULL),
(1101, 'Secuirty Group', NULL),
(1500, 'Document', NULL),
(1600, 'Association', NULL),
(2000, 'Company', NULL),
(2001, 'CompanyCustom', NULL),
(2003, 'CompanyLocator', NULL),
(2011, 'Industry', NULL),
(2012, 'Origin', NULL),
(2013, 'TimeZone', NULL),
(2101, 'CustomFieldDef', NULL),
(3000, 'Contact', NULL),
(3001, 'ContactCustom', NULL),
(3003, 'ContactLocator', NULL),
(4000, 'Lead', NULL),
(4001, 'LeadCustom', NULL),
(4003, 'LeadLocator', NULL),
(4101, 'LeadSource', NULL),
(5000, 'Employee', NULL),
(5001, 'EmployeeCustom', NULL),
(5003, 'EmployeeLocator', NULL),
(5020, 'EmployeeTeam', NULL),
(5101, 'EmployeeRole', NULL),
(9000, 'Opportunity', NULL),
(9001, 'OpportunityCustom', NULL),
(9100, 'Campaign', NULL),
(9101, 'CampaignCustom', NULL),
(10000, 'Order', NULL),
(10002, 'Estimate', NULL),
(10100, 'OrderLineItem', NULL),
(10102, 'EstimateLineItem', NULL),
(10300, 'EstimateVariation', NULL),
(10800, 'Job', NULL),
(11001, 'Activity', NULL),
(11002, 'Event', NULL),
(11003, 'GLActivity', NULL)
");
        }
    }
}

