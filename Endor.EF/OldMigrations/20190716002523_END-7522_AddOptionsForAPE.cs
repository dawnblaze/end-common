using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END7522_AddOptionsForAPE : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
VALUES (7020, N'Integration.CreditCard.BusinessKey', N'Business Key for Atomology Payment Engine (APE)', N'Business Key for Atomology Payment Engine (APE)', 0, 702, N'', N'', 1)
     , (7021, N'Integration.CreditCard.Enabled', N'Enable Integration Credit Card Processing', N'Enable Integration Credit Card Processing', 3, 702, N'', N'0', 0)
     , (7022, N'Integration.CreditCard.Provider', N'Integrated Credit Card Provider', N'Integrated Credit Card Provider', 0, 702, N'', N'CardConnect', 0)
     , (7023, N'Integration.CreditCard.MerchantID', N'MerchantID for Integrated Credit Card Processing', N'MerchantID for Integrated Credit Card Processing', 0, 702, N'', N'', 0)
     , (7024, N'Integration.CreditCard.UserName', N'User Name for Integrated Credit Card Processing', N'User Name for Integrated Credit Card Processing', 0, 702, N'', N'', 0)
     , (7025, N'Integration.CreditCard.Password', N'Password for Integrated Credit Card Processing', N'Password for Integrated Credit Card Processing', 0, 702, N'', N'', 0)
     , (7026, N'Integration.CreditCard.Vaulting.Enabled', N'Enable Vaulting for Integrated Credit Card Processing', N'Enable Vaulting for Integrated Credit Card Processing', 3, 702, N'', N'1', 0)
     , (7027, N'Integration.CreditCard.Reader.Enabled', N'Enable Card Reader for Integrated Credit Card Processing', N'Enable Card Reader for Integrated Credit Card Processing', 3, 702, N'', N'0', 0)
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [System.Option.Definition] WHERE ID BETWEEN 7020 AND 7027");
        }
    }
}
