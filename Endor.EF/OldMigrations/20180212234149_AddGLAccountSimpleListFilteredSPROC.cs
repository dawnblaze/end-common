using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180212234149_AddGLAccountSimpleListFilteredSPROC")]
    public partial class AddGLAccountSimpleListFilteredSPROC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.GL.Account.NumberedSimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.GL.Account.NumberedSimpleList]
GO
");
            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.GL.Account.SimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.GL.Account.SimpleList]
GO");
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Accounting.GLAccount.SimpleList.WithOptions];
GO

CREATE PROCEDURE [Accounting.GLAccount.SimpleList.WithOptions]

         @BID SMALLINT
        , @ActiveFilter        BIT = 0   -- 0 = Inactive Only, 1 = Active Only, NULL = Both Active and Inactive

        , @UseNumberedName     BIT = NULL  -- 0 = Returns Name Only, 1 = Numbered Name, NULL = Use Default Option
        , @GLAccountTypeIDList VARCHAR(MAX) = NULL  -- A list of GLAccountTypes to return.  NULL returns all types.
AS
/*
    Returns a Simple List for GLAccounts that match one or more GLAccountTypes.
    The list of valid GLAccountTypes is passed in a Comma Delimited String.
    
    Sample Usage:

        EXEC [Accounting.GLAccount.SimpleList.WithOptions]
            @BID = 1
            , @GLAccountTypeIDList = '10, 11, 12, 13, 14'
            , @ActiveFilter = NULL
            , @UseNumberedName = NULL

*/
BEGIN
    -- If @UseNumberedName is not provided, look it up from the option settings
    -- ------------------------------------------------------------
    IF (@UseNumberedName IS NULL) 
    BEGIN
        DECLARE @Opt Table(OptionID INT, [Value] VARCHAR(MAX) , [OptionLevel] TINYINT);

        INSERT INTO @Opt
            EXEC [Option.GetValue] @BID = @BID, @OptionName = 'Accounting.GLAccount.ShowNumberedNames';

        SELECT TOP 1 @UseNumberedName = TRY_CONVERT(BIT, [Value])
        FROM @Opt;
    END;

    -- Handle @ActiveFilter 
    DECLARE @IncludeActive   BIT = IIF(@ActiveFilter IS NULL OR @ActiveFilter = 1, 1, 0)
          , @IncludeInActive BIT = IIF(@ActiveFilter IS NULL OR @ActiveFilter = 0, 1, 0)



    -- Return the results desired
    -- ------------------------------------------------------------
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , (CASE WHEN @UseNumberedName = 0 THEN Name ELSE NumberedName END) as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.GL.Account]
    WHERE BID = @BID
      AND (LEN(LTRIM(GLAccountType)) = 0 
          OR [GLAccountType] in (SELECT CONVERT(INT, Value) FROM string_split( @GLAccountTypeIDList, ',' )))
      AND ((IsActive & @IncludeActive) | (~IsActive & @IncludeInActive)) = 1

END;

");

            migrationBuilder.AlterColumn<bool>(
                name: "IsIncome",
                table: "Accounting.GL.Account",
                nullable: true,
                computedColumnSql: "(case when [GLAccountType]>=(40) AND [GLAccountType]<=(49) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)",
                oldClrType: typeof(bool),
                oldNullable: true,
                oldComputedColumnSql: "(case when [GLAccountType]>=(50) AND [GLAccountType]<=(59) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.AlterColumn<bool>(
                name: "IsCOGS",
                table: "Accounting.GL.Account",
                nullable: true,
                computedColumnSql: "(case when [GLAccountType]>=(50) AND [GLAccountType]<=(59) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)",
                oldClrType: typeof(bool),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Accounting.GLAccount.SimpleList.WithOptions];");

            migrationBuilder.AlterColumn<bool>(
                name: "IsCOGS",
                table: "Accounting.GL.Account",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true,
                oldComputedColumnSql: "(case when [GLAccountType]>=(50) AND [GLAccountType]<=(59) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.AlterColumn<bool>(
                name: "IsIncome",
                table: "Accounting.GL.Account",
                nullable: true,
                computedColumnSql: "(case when [GLAccountType]>=(50) AND [GLAccountType]<=(59) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)",
                oldClrType: typeof(bool),
                oldNullable: true,
                oldComputedColumnSql: "(case when [GLAccountType]>=(40) AND [GLAccountType]<=(49) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.GL.Account.NumberedSimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.GL.Account.NumberedSimpleList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Accounting.GL.Account.NumberedSimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , NumberedName as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.GL.Account]
GO
");
            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.GL.Account.SimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.GL.Account.SimpleList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Accounting.GL.Account.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.GL.Account]
GO");
        }
    }
}

