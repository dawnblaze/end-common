using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class MigrateDataJSONToDataXML : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"
                UPDATE T
                SET DataXML = (SELECT ID, V FROM OPENJSON( DataJSON ) WITH (ID int '$.ID', V VARCHAR(MAX) '$.V') FOR XML PATH('Data'))
                FROM [Order.Custom.Data] T
                WHERE DataJSON IS NOT NULL AND DataJSON <> '[]'
                ;

                UPDATE T
                SET DataXML = (SELECT ID, V FROM OPENJSON( DataJSON ) WITH (ID int '$.ID', V VARCHAR(MAX) '$.V') FOR XML PATH('Data'))
                FROM [Contact.Custom.Data] T
                WHERE DataJSON IS NOT NULL AND DataJSON <> '[]'
                ;

                UPDATE T
                SET DataXML = (SELECT ID, V FROM OPENJSON( DataJSON ) WITH (ID int '$.ID', V VARCHAR(MAX) '$.V') FOR XML PATH('Data'))
                FROM [Company.Custom.Data] T
                WHERE DataJSON IS NOT NULL AND DataJSON <> '[]'
                ;

                UPDATE T
                SET DataXML = (SELECT ID, V FROM OPENJSON( DataJSON ) WITH (ID int '$.ID', V VARCHAR(MAX) '$.V') FOR XML PATH('Data'))
                FROM [CustomField.Other.Data] T
                WHERE DataJSON IS NOT NULL AND DataJSON <> '[]'
                ;

                ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
