﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class RemoveCRMWidgetInManagementModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 8 AND [CategoryType] != 4;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT [System.Dashboard.Widget.CategoryLink] ([WidgetDefID], [CategoryType]) 
                VALUES (8, 1);   
            ");
        }
    }
}
