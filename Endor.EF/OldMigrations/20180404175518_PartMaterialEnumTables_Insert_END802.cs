using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180404175518_PartMaterialEnumTables_Insert_END802")]
    public partial class PartMaterialEnumTables_Insert_END802 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    INSERT [enum.Part.Unit.Classification] ([ID], [Name])
                    VALUES (0, N'None')
                         , (1, N'Discrete')
                         , (2, N'Dimensional')
                         , (3, N'Weight')
                         , (4, N'Time')
                    ;
                "
            );

            migrationBuilder.Sql(
                @"
                    INSERT [enum.Part.Unit.System] ([ID], [Name])
                    VALUES (0, N'Default')
                        , (1, N'Imperial')
                        , (2, N'Metric')
                        , (3, N'Metric and Imperial')
                    ;
                "
            );

            migrationBuilder.Sql(
                @"
                    INSERT [enum.Part.Material.Consumption.Method] ([ID], [Name])
                    VALUES (0, N'Each')
                        , (1, N'Linear')
                        , (2, N'Area')
                        , (3, N'Volume')
                    ;
                "
            );

            migrationBuilder.Sql(
                @"
                    INSERT [enum.Part.Material.Costing.Method] ([ID], [Name])
                    VALUES (0, N'Manual')
                        , (1, N'Preferred Vendor Cost')
                        , (2, N'Average Inventory Cost')
                        , (3, N'FIFO')
                    ;
                "
            );

            migrationBuilder.Sql(
                @"
                    INSERT [enum.Part.Material.Physical.Type] ([ID], [Name], Dimensions, WidthRequired, LengthRequired, HeightRequired, WeightRequired )
                    VALUES (0, N'None', 0, 0, 0, 0, 0)
                        , (1, N'Discrete', 0, 0, 0, 0, 0)
                        , (2, N'Roll', 1, 1, 0, 0, 0)
                        , (3, N'Sheet', 2, 1, 1, 0, 0)
                        , (4, N'Volume', 3, 1, 1, 1, 0)
                        , (5, N'Weight', 1, 0, 0, 0, 1)
                    ;
                "
            );

            migrationBuilder.InsertData("enum.Part.Unit"
                , new string[]
                { "ID", "Name", "Abbreviation", "UnitSystem", "UnitClassification", "ToStandardMultiplyBy" }
                , new object[,]
            {
                { 0 , "None",       "",      3, 0,    1.000000000000000000m },
                { 1 , "Each",       "ea",    3, 1,    1.000000000000000000m },
                { 2 , "Box" ,       "box",   3, 1,    1.000000000000000000m },
                { 3 , "Cart",       "ctn",   3, 1,    1.000000000000000000m },
                { 4 , "Impression", "imp",   3, 1,    1.000000000000000000m },
                { 5 , "Package",    "pkg",   3, 1,    1.000000000000000000m },
                { 6 , "Page",       "pg",    3, 1,    1.000000000000000000m },
                { 7 , "Palatte",    "sd",    3, 1,    1.000000000000000000m },
                { 8 , "Piece",      "pc",    3, 1,    1.000000000000000000m },
                { 9 , "Ream",       "ea",    3, 1,    1.000000000000000000m },
                { 10, "Roll",       "ea",    3, 1,    1.000000000000000000m },
                { 11, "Set",        "set",   3, 1,    1.000000000000000000m },
                { 12, "Sheet",      "sheet", 3, 1,    1.000000000000000000m },
                { 21, "Inch",       "in",    3, 2,    1.000000000000000000m },
                { 22, "Centimeter", "cm",    2, 2,    2.540000000000000000m },
                { 23, "Foot",       "ft",    1, 2,   12.000000000000000000m },
                { 24, "Meter",      "m",     2, 2,  254.000000000000000000m },
                { 25, "Millimeter", "mm",    2, 2,    0.254000000000000000m },
                { 26, "Yard",       "yd",    1, 2,   36.000000000000000000m },
                { 31, "Gram",       "g",     3, 3,    1.000000000000000000m },
                { 32, "Kilogram",   "kg",    3, 3, 1000.000000000000000000m },
                { 33, "Milligram",  "mg",    3, 3,    0.001000000000000000m },
                { 34, "Ounce",      "oz",    3, 3,   28.349523125000000000m },
                { 35, "Pound",      "lb",    3, 3,  453.592370100000000000m },
                { 41, "Minute",     "min",   3, 4,    1.000000000000000000m },
                { 42, "Hour",       "hr",    3, 4,   60.000000000000000000m },
                { 43, "Second",     "s",     3, 4,    0.016666666666666667m },
            });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData("enum.Part.Unit", "ID", new object[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 21, 22, 23, 24, 25, 26, 31, 32, 33, 34, 35, 41, 42, 43 });
            migrationBuilder.DeleteData("enum.Part.Unit.Classification", "ID", new object[] { 0, 1, 2, 3, 4 });
            migrationBuilder.DeleteData("enum.Part.Unit.System", "ID", new object[] { 0, 1, 2, 3 });
            migrationBuilder.DeleteData("enum.Part.Material.Consumption.Method", "ID", new object[] { 0, 1, 2, 3 });
            migrationBuilder.DeleteData("enum.Part.Material.Costing.Method", "ID", new object[] { 0, 1, 2, 3 });
            migrationBuilder.DeleteData("enum.Part.Material.Physical.Type", "ID", new object[] { 0, 1, 2, 3, 4, 5 });

        }
    }
}

