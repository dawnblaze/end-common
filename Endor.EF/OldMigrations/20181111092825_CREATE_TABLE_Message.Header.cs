using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CREATE_TABLE_MessageHeader : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Message.Header",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14111))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    ReceivedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    EmployeeID = table.Column<short>(type: "smallint", nullable: false),
                    ParticipantID = table.Column<int>(type: "int", nullable: false),
                    BodyID = table.Column<int>(type: "int", nullable: false),
                    IsRead = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    ReadDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    IsExpired = table.Column<bool>(type: "bit", nullable: false),
                    DeletedOrExpiredDT = table.Column<DateTime>(type: "datetime2(2) sparse", nullable: true),
                    Channels = table.Column<byte>(type: "smallint", nullable: false),
                    InSentFolder = table.Column<bool>(type: "bit", nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Header", x => new { x.BID, x.ID });
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Header_Message.Participant.Info",
                table: "Message.Header",
                columns: new[] { "BID", "ParticipantID" },
                principalTable: "Message.Participant.Info",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Inbox_Employee.Data",
                table: "Message.Header",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Inbox_Message.Content",
                table: "Message.Header",
                columns: new[] { "BID", "BodyID" },
                principalTable: "Message.Body",
                principalColumns: new[] { "BID", "ID" });


            migrationBuilder.CreateIndex(
                name: "IX_Message.Header_Employee",
                table: "Message.Header",
                columns: new[] { "BID", "EmployeeID", "ReceivedDT" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Header_Participant",
                table: "Message.Header",
                columns: new[] { "BID", "ParticipantID" });


            migrationBuilder.CreateIndex(
                name: "IX_Message.Header_EmployeeChannels",
                table: "Message.Header",
                columns: new[] { "BID", "EmployeeID", "Channels", "IsRead" });


            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message.Header_Message.Participant.Info",
                table: "Message.Header");

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Inbox_Employee.Data",
                table: "Message.Header");

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Inbox_Message.Content",
                table: "Message.Header");

            migrationBuilder.DropIndex(
                name: "IX_Message.Header_Employee",
                table: "Message.Header");

            migrationBuilder.DropIndex(
                name: "IX_Message.Header_Participant",
                table: "Message.Header");

            migrationBuilder.DropIndex(
                name: "IX_Message.Header_EmployeeChannels",
                table: "Message.Header");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Header",
                table: "Message.Header");

            migrationBuilder.DropTable(
                name: "Message.Header");
        }
    }
}
