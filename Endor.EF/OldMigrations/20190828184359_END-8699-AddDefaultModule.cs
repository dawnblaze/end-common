using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8699AddDefaultModule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<short>(
                name: "DefaultModule",
                table: "Employee.Data",
                type: "smallint",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Data_enum.Module",
                table: "Employee.Data",
                column: "DefaultModule",
                principalTable: "enum.Module",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Data_enum.Module",
                table: "Employee.Data");

            migrationBuilder.DropColumn(
                name: "DefaultModule",
                table: "Employee.Data");


        }
    }
}
