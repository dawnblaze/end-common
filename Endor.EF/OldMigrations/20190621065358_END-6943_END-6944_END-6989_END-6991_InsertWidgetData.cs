using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6943_END6944_END6989_END6991_InsertWidgetData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.Definition] WHERE [ID] = 5)
BEGIN
	INSERT INTO [System.Dashboard.Widget.Definition]
	([ID], [ModifiedDT], [DefaultName], [DefaultRows], [DefaultCols], [DefaultOptions], [DefaultRefreshIntervalInMin], [Description], [Modules], [SecurityRightID], [MinRows], [MaxRows], [MinCols], [MaxCols], [HasImage])
	VALUES (5, CAST(N'2018-10-01T18:15:14.7900000' AS DateTime2), N'Line Item Status Count', 1, 2, NULL, 15, N'', NULL, NULL, 1, 2, 2, 4, 1);
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.Definition] WHERE [ID] = 6)
BEGIN
	INSERT INTO [System.Dashboard.Widget.Definition]
	([ID], [ModifiedDT], [DefaultName], [DefaultRows], [DefaultCols], [DefaultOptions], [DefaultRefreshIntervalInMin], [Description], [Modules], [SecurityRightID], [MinRows], [MaxRows], [MinCols], [MaxCols], [HasImage])
	VALUES (6, CAST(N'2018-10-01T18:15:14.7900000' AS DateTime2), N'Financial Summary', 1, 2, NULL, 15, N'', NULL, NULL, 1, 2, 2, 4, 1);
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.Definition] WHERE [ID] = 3)
BEGIN
	INSERT INTO [System.Dashboard.Widget.Definition]
	([ID], [ModifiedDT], [DefaultName], [DefaultRows], [DefaultCols], [DefaultOptions], [DefaultRefreshIntervalInMin], [Description], [Modules], [SecurityRightID], [MinRows], [MaxRows], [MinCols], [MaxCols], [HasImage])
	VALUES (3, CAST(N'2018-10-01T18:15:14.7900000' AS DateTime2), N'Order Throughput', 1, 2, NULL, 15, N'', NULL, NULL, 1, 2, 2, 4, 1);
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.Definition] WHERE [ID] = 7)
BEGIN
	INSERT INTO [System.Dashboard.Widget.Definition]
	([ID], [ModifiedDT], [DefaultName], [DefaultRows], [DefaultCols], [DefaultOptions], [DefaultRefreshIntervalInMin], [Description], [Modules], [SecurityRightID], [MinRows], [MaxRows], [MinCols], [MaxCols], [HasImage])
	VALUES (7, CAST(N'2018-10-01T18:15:14.7900000' AS DateTime2), N'Estimate Summary', 1, 2, NULL, 15, N'', NULL, NULL, 1, 2, 2, 4, 1);
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 3 AND [CategoryType] = 1)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (3,1)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 3 AND [CategoryType] = 2)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (3,2)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 3 AND [CategoryType] = 4)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (3,4)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 5 AND [CategoryType] = 1)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (5,1)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 5 AND [CategoryType] = 4)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (5,4)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 5 AND [CategoryType] = 8)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (5,8)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 6 AND [CategoryType] = 1)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (6,1)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 6 AND [CategoryType] = 2)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (6,2)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 7 AND [CategoryType] = 1)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (7,1)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 7 AND [CategoryType] = 4)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (7,4)
END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [System.Dashboard.Widget.CategoryLink] WHERE WidgetDefID = 3
DELETE FROM [System.Dashboard.Widget.CategoryLink] WHERE WidgetDefID = 5
DELETE FROM [System.Dashboard.Widget.CategoryLink] WHERE WidgetDefID = 6
DELETE FROM [System.Dashboard.Widget.CategoryLink] WHERE WidgetDefID = 7

DELETE FROM [System.Dashboard.Widget.Definition] WHERE ID=5
DELETE FROM [System.Dashboard.Widget.Definition] WHERE ID=3
DELETE FROM [System.Dashboard.Widget.Definition] WHERE ID=6
DELETE FROM [System.Dashboard.Widget.Definition] WHERE ID=7");
        }
    }
}
