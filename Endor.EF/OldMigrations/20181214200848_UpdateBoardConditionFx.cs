using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateBoardConditionFx : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
update [Board.Definition.Data]
set ConditionFx = REPLACE(ConditionFx, '""$type"":""IAEL', '""$type"":""AEL')
;
            update[Board.Definition.Data]
set ConditionFx = REPLACE(ConditionFx, '{""DataType"":', '{""$type"":""AELValues"",""DataType"":')
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
