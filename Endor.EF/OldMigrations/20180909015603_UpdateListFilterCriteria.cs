using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180909015603_UpdateListFilterCriteria")]
    public partial class UpdateListFilterCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [System.List.Filter.Criteria]
                ALTER COLUMN DataType smallint;

                UPDATE [System.List.Filter.Criteria]
                SET DataType = CASE DataType
                    WHEN 0 THEN 1 -- String
                    WHEN 1 THEN 2 -- Integer
                    WHEN 2 THEN 2 -- Float
                    WHEN 3 THEN 3 -- Boolean
                    WHEN 4 THEN 32005 -- NamedQuark
                ELSE 
                    DataType
                END;

                ALTER TABLE [System.List.Filter.Criteria] WITH CHECK
                ADD CONSTRAINT [FK_System.List.Filter.Criteria_enum.DataType]
                FOREIGN KEY([DataType]) REFERENCES [enum.DataType]([ID])
                ;
                ALTER TABLE [System.List.Filter.Criteria]
                CHECK CONSTRAINT [FK_System.List.Filter.Criteria_enum.DataType]
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

