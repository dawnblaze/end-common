using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180226053457_Insert_System.List.Filter.Criteria_For_TaxGroup")]
    public partial class Insert_SystemListFilterCriteria_For_TaxGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                       ([TargetClassTypeID]
                       ,[Name]
                       ,[Label]
                       ,[Field]
                       ,[IsHidden]
                       ,[DataType]
                       ,[InputType]
                       ,[AllowMultiple]
                       ,[ListValues]
                       ,[ListValuesEndpoint]
                       ,[IsLimitToList]
                       ,[SortIndex])
                 VALUES
                       (11101 --<TargetClassTypeID, int,>
                       ,'Name' --<Name, varchar(255),>
                       ,'Name'  --<Label, varchar(255),>
                       ,'Name' --<Field, varchar(255),>
                       ,0 --<IsHidden, bit,>
                       ,0 --<DataType, tinyint,>
                       ,0 --<InputType, tinyint,>
                       ,0 --<AllowMultiple, bit,>
                       ,NULL --<ListValues, varchar(max),>
                       ,NULL --<ListValuesEndpoint, varchar(255),>
                       ,0 --<IsLimitToList, bit,>
                       ,0) --<SortIndex, tinyint,>

                       ,(11101 --<TargetClassTypeID, int,>
                       ,'Is Active' --<Name, varchar(255),>
                       ,'Is Active'  --<Label, varchar(255),>
                       ,'IsActive' --<Field, varchar(255),>
                       ,0 --<IsHidden, bit,>
                       ,3 --<DataType, tinyint,>
                       ,2 --<InputType, tinyint,>
                       ,0 --<AllowMultiple, bit,>
                       ,'Is Not Active,Is Active' --<ListValues, varchar(max),>
                       ,NULL --<ListValuesEndpoint, varchar(255),>
                       ,1 --<IsLimitToList, bit,>
                       ,1) --<SortIndex, tinyint,>
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria]
                WHERE TargetClassTypeID = 11101 AND (Name='Name' OR NAME='Is Active')
            ");
        }
    }
}

