using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddUserDraft : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User.Draft",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "1050"),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    ExpirationDT = table.Column<DateTime>(nullable: false, defaultValueSql: "(dateadd(month,(1),getutcdate()))"),
                    IsExpired = table.Column<bool>(nullable: true, computedColumnSql: "(case when [ExpirationDT]<getutcdate() then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    MetaData = table.Column<string>(nullable: true),
                    ModifiedDT = table.Column<DateTime>(nullable: false, defaultValueSql: "GetUTCDate()"),
                    ObjectCTID = table.Column<int>(nullable: false),
                    ObjectID = table.Column<int>(nullable: true),
                    UserLinkID = table.Column<short>(nullable: false),
                    VersionBasedOn = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User.Draft", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_User.Draft_User.Link",
                        columns: x => new { x.BID, x.UserLinkID },
                        principalTable: "User.Link",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User.Draft_Object",
                table: "User.Draft",
                columns: new[] { "BID", "ObjectCTID", "ObjectID", "UserLinkID" });

            migrationBuilder.CreateIndex(
                name: "IX_User.Draft_User",
                table: "User.Draft",
                columns: new[] { "BID", "UserLinkID", "ObjectCTID", "ExpirationDT", "ObjectID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User.Draft");
        }
    }
}
