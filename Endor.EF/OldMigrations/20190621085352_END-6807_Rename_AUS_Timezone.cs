using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6807_Rename_AUS_Timezone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [enum.TimeZone]
                SET 
                [Name] = 'AUS Central Standard Time'
                WHERE ID = 245;

            UPDATE [enum.TimeZone]
                SET 
                [Name] = 'AUS Eastern Standard Time'
                WHERE ID = 255
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
