using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class RECREATE_TABLE_MessageBody : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Inbox_Message.Content",
                table: "Message.Header");

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Participant.Info_Message.Body",
                table: "Message.Participant.Info");

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Related.Records_Message.Data",
                table: "Message.Object.Link");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Body",
                table: "Message.Body");

            migrationBuilder.DropTable(
                name: "Message.Body");


            migrationBuilder.CreateTable(
                name: "Message.Body",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AttachedFileCount = table.Column<byte>(type: "tinyint", nullable: false, defaultValue: (byte)0),
                    AttachedFileNames = table.Column<string>(type: "varchar(max) sparse", nullable: true),
                    BodyFirstLine = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14110))"),
                    HasAttachment = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [AttachedFileCount]>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,CONVERT([bit],(0))))"),
                    HasBody = table.Column<bool>(nullable: false),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    SizeInKB = table.Column<int>(type: "int", nullable: true),
                    Subject = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    WasModified = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Body", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Message.Content_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                });


            migrationBuilder.AddForeignKey(
                name: "FK_Message.Inbox_Message.Content",
                table: "Message.Header",
                columns: new[] { "BID", "BodyID" },
                principalTable: "Message.Body",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Participant.Info_Message.Body",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "BodyID" },
                principalTable: "Message.Body",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Related.Records_Message.Data",
                table: "Message.Object.Link",
                columns: new [] {"BID", "BodyID" },
                principalTable: "Message.Body",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Body",
                table: "Message.Body");

            migrationBuilder.DropTable(
                name: "Message.Body");
        }
    }
}
