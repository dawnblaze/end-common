using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180831145605_SortFilterListCriteriaCustomFieldLayoutDefinition")]
    public partial class SortFilterListCriteriaCustomFieldLayoutDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[System.List.Filter.Criteria]
WHERE [TargetClassTypeID] = 15027 AND [Field] = 'AppliesToClassTypeIDText' AND [InputType] = 0;

UPDATE [dbo].[System.List.Filter.Criteria]
SET [SortIndex] = 0 WHERE [TargetClassTypeID] = 15027 AND [Field] = 'AppliesToClassTypeIDText';

UPDATE [dbo].[System.List.Filter.Criteria]
SET [SortIndex] = 1 WHERE [TargetClassTypeID] = 15027 AND [Field] = '-IsActive';
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[System.List.Filter.Criteria]
WHERE [TargetClassTypeID] = 15027 AND [Field] = 'AppliesToClassTypeIDText' AND [Field] = 'AppliesToClassTypeIDText';

DELETE FROM [dbo].[System.List.Filter.Criteria]
WHERE [TargetClassTypeID] = 15027 AND [Field] = 'AppliesToClassTypeIDText' AND [Field] = '-IsActive';

            ");

        }
    }
}

