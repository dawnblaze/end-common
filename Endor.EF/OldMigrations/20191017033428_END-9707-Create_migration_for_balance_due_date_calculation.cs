﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9707Create_migration_for_balance_due_date_calculation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                /*********************************************************
                  Name:
                    [Order.Action.ComputeBalanceDueDate]( @BID tinyint, @OrderID int)
  
                  Description:
                    This function computes balance due date of an order

	                0 - 	Return NULL if the Order Status != Invoiced
			                Start with Invoiced DateTime
			                Convert to the Location's TimeZone and take the Date Only (no time)
			                Add The DaysDue and Return 

	                1 -		Return NULL if the Order Status not in (Invoicing, Invoiced)
			                Start with Invoicing DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                Add The DaysDue and Return

	                2 -		Return NULL if the Order Status is Closed or Voided
			                Start with Created DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                Add The DaysDue and Return
	
	                3 -		Return NULL if the Order Status != Invoiced
			                Start with Invoiced DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                Move to the First Day of the Next Month 
			                If the (DaysDue > Days in that month), return the last day of the month.
			                Otherwise, return the first of the month + (DaysDue - 1 )

	                4 -		Return NULL if the Order Status != Invoiced
			                Start with Invoiced DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                If the DaysDue is > the Specified Day, Add 1 Month
			                If the (DaysDue > Days in that month), return the last day of the month.
			                Otherwise, return the first of the month + (DaysDue - 1 )

	                5 -		Return NULL if the Order Status != Invoiced
			                Start with Invoiced DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                Add 1 Month
			                If the (DaysDue > Days in that month), return the last day of the month.
			                Otherwise, return the first of the month + (DaysDue - 1 )


	                ===================================================================================

	                Ex: 0 - Days After Invoice 
	                Invoice Date        = 10/15/2019 + 10 DaysDue 
	                balance due date    =  10/25/2019

	                Ex. 1 - Days After Built
	                Built Date          = 10/15/2019  + 10 DaysDue 
	                balance due date    = 10/25/2019

	                Ex. 2 - Days After Placed
	                Create Date         = 10/15/2019  + 10 DaysDue 
	                balance due date    = 10/25/2019

	                Ex. 3 - Day of the Month After Built (DaysDue = 10)
	                Built Date          = 10/15/19
	                balance due date    = 11/10/19

	                Ex. 4 - Day of the Month (DaysDue = 10)
	                Invoice Date        = 10/15/19 
	                balance due date    = 11/10/19

	                Ex. 5 - Day of the Month After Invoiced (DaysDue = 10)
	                Invoice Date        = 10/15/19 
	                balance due date    = 11/10/19

  
                  Sample Use:
                    EXEC [dbo].[Order.Action.ComputeBalanceDueDate] @BID=1, @OrderID=2452;

				  

				  Date created:	 10/17/19
				  Date modified: 10/21/19
                *********************************************************/

                CREATE OR ALTER PROCEDURE [dbo].[Order.Action.ComputeBalanceDueDate]
                (
                --DECLARE
                    @BID            TINYINT		--= 1
                  , @OrderID        INT			--= 1049
                )
                AS
                BEGIN
	
	                DECLARE @PaymentDueBasedOnType INT;
	                DECLARE @OrderStatusID INT;
	                DECLARE @BalanceDueDate DATE = null;
	                DECLARE @InvoicedDate DATE;
	                DECLARE @DaysDue INT;

	                /* When the order is saved, delete Balance Due Key Date if it is present and should be NULL */
	                IF(EXISTS(SELECT 1 FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = 32))
	                BEGIN
		                DELETE FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = 32 --Balance Due
	                END

	                /************ Get PaymentDueBasedOnType and OrderStatusID ******/
	                SELECT 
		                @PaymentDueBasedOnType = APT.PaymentDueBasedOnType,
		                @OrderStatusID = O.OrderStatusID
	                FROM
		                [Order.Data] O 
		                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
		                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
	                WHERE O.BID = @BID AND O.ID = @OrderID

	                /************* 0: Days After Invoice **************************/
	                IF(@PaymentDueBasedOnType = 0 AND @OrderStatusID = 25) /* Return NULL if the Order Status != Invoiced*/
	                BEGIN
		                WITH DaysAfterInvoice(InvoicedDate,LocationTimezone,DaysDue)
		                AS (
			                SELECT 
				                InvoicedDate = (SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 8)
				                ,LocationTimezone = Z.Name
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
			                WHERE O.BID = @BID
				                AND O.ID = @OrderID
				                AND EOS.TransactionType = 2 --Order
				                AND EOS.ID = @OrderStatusID
			                )
		                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Return */
		                SELECT
			                @BalanceDueDate = DATEADD(DAY, DaysDue, CONVERT(DATETIME,InvoicedDate) AT TIME ZONE LocationTimezone)
		                FROM DaysAfterInvoice;
	                END

	                /************* 1: Days After Built **************************/
	                ELSE IF(@PaymentDueBasedOnType = 1 AND (@OrderStatusID = 24 OR @OrderStatusID = 25))
	                BEGIN
		                WITH DaysAfterBuilt(BuiltDate,LocationTimezone,DaysDue)
		                AS (
			                SELECT 
				                BuiltDate = (SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 6)
				                ,LocationTimezone = Z.Name
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
			                WHERE O.BID = @BID
				                AND O.ID = @OrderID
				                AND EOS.TransactionType = 2 --Order
				                AND EOS.ID = @OrderStatusID
			                )
		                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Return */
		                SELECT
			                @BalanceDueDate = DATEADD(DAY, DaysDue, CONVERT(DATETIME,BuiltDate) AT TIME ZONE LocationTimezone)
		                FROM DaysAfterBuilt
	                END

	                /************* 2: Days After Placed **************************/
					ELSE IF(@PaymentDueBasedOnType = 2 AND NOT(@OrderStatusID = 26 OR @OrderStatusID = 29))
	                BEGIN
		                WITH DaysAfterPlaced(CreatedDate,LocationTimezone,DaysDue)
		                AS (
			                SELECT 
			                CreatedDate = (SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 1)
			                ,LocationTimezone = Z.Name
			                ,APT.DaysDue
		                FROM
			                [Order.Data] O 
			                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
			                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
			                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
			                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
			                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
			                WHERE O.BID = @BID 
				                AND O.ID = @OrderID
				                AND EOS.TransactionType = 2 --Order
				                AND EOS.ID IN (@OrderStatusID)	
			                )
		                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Return */
		                SELECT
			                @BalanceDueDate = DATEADD(DAY, DaysDue, CONVERT(DATETIME,CreatedDate) AT TIME ZONE LocationTimezone)
		                FROM DaysAfterPlaced
	                END

	                /************* 3: Day of the Month After Built **************************/
	                ELSE IF(@PaymentDueBasedOnType = 3 AND @OrderStatusID = 25)
	                BEGIN
		                WITH DayOfTheMonthAfterBuilt(InvoicedDate,DaysDue)
		                AS (
			
			                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Return */
			                SELECT 
				                InvoicedDate = CONVERT(DATETIME,(SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 6)) AT TIME ZONE Z.Name
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
				                WHERE O.BID = @BID 
					                AND O.ID = @OrderID
					                AND EOS.TransactionType = 2 --Order
					                AND EOS.ID = @OrderStatusID	
				                )			
			                SELECT
				                @BalanceDueDate = DATEFROMPARTS(YEAR(InvoicedDate), MONTH(InvoicedDate), DaysDue),
				                @InvoicedDate = InvoicedDate,
				                @DaysDue = DaysDue
			                FROM DayOfTheMonthAfterBuilt

			                IF DATEDIFF(DAY, @InvoicedDate, @BalanceDueDate) <= 0
			                BEGIN
				                SET @BalanceDueDate = DATEADD(month, 1, @BalanceDueDate)
			                END

	                END

	                /************* 4: Day of the Month **************************/
	                ELSE IF(@PaymentDueBasedOnType = 4 AND @OrderStatusID = 25)
	                BEGIN
		                WITH DayOfTheMonth(InvoicedDate,DaysDue)
		                AS (
			                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue */
			                SELECT 
				                InvoicedDate = CONVERT(DATETIME,(SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 8)) AT TIME ZONE Z.Name
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
			                WHERE O.BID = @BID
				                AND O.ID = @OrderID
				                AND EOS.TransactionType = 2 --Order
				                AND EOS.ID = @OrderStatusID
			                )
		                SELECT
			                @BalanceDueDate = DATEFROMPARTS(YEAR(InvoicedDate), MONTH(InvoicedDate), DaysDue),
			                @InvoicedDate = InvoicedDate,
			                @DaysDue = DaysDue
		                FROM DayOfTheMonth

		                IF DATEDIFF(DAY, @InvoicedDate, @BalanceDueDate) <= 0
		                BEGIN
			                SET @BalanceDueDate = DATEADD(month, 1, @BalanceDueDate)
		                END
	                END

	                /************* 5: Day of the Month After Invoiced **************************/
	                ELSE IF(@PaymentDueBasedOnType = 5 AND @OrderStatusID = 25)
	                BEGIN
		                WITH DayOfTheMonthAfterInvoiced(InvoicedDate,DaysDue)
		                AS (
			                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Add 1 Month */
			                SELECT 
				                InvoicedDate = DATEADD(MONTH, 1, CONVERT(DATETIME,(SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 8)) AT TIME ZONE Z.Name)
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
				                WHERE O.BID = @BID 
					                AND O.ID = @OrderID
					                AND EOS.TransactionType = 2 --Order
					                AND EOS.ID = @OrderStatusID	
				                )
			                SELECT
				                @BalanceDueDate = DATEFROMPARTS(YEAR(InvoicedDate), MONTH(InvoicedDate), DaysDue),
				                @InvoicedDate = InvoicedDate,
				                @DaysDue = DaysDue
			                FROM DayOfTheMonthAfterInvoiced

			                IF DATEDIFF(DAY, @InvoicedDate, @BalanceDueDate) <= 0
			                BEGIN
				                SET @BalanceDueDate = DATEADD(month, 1, @BalanceDueDate)
			                END
	                END


	                SELECT BalanceDueDate = @BalanceDueDate

                END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
