using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180926211400_RemoveUnusedListFilters")]
    public partial class RemoveUnusedListFilters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [List.Filter] WHERE [TargetClassTypeID] = 3000 AND [Name] <> 'All';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [List.Filter] (BID,ID,CreatedDate,ModifiedDT,IsActive,Name,TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,Hint,IsDefault,SortIndex)
VALUES (1,1009,'2018-01-30','2018-01-30 00:00:00.00',1,'Leads',3000,NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>IsLead</Label><SearchValue>1</SearchValue><Field>StatusID</Field><DisplayText>Is Lead</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,1);

INSERT INTO [List.Filter] (BID,ID,CreatedDate,ModifiedDT,IsActive,Name,TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,Hint,IsDefault,SortIndex)
VALUES (1,1010,'2018-01-30','2018-01-30 00:00:00.00',1,'Prospects',3000,NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>IsProspect</Label><SearchValue>2</SearchValue><Field>StatusID</Field><DisplayText>Is Lead</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,1);

INSERT INTO [List.Filter] (BID,ID,CreatedDate,ModifiedDT,IsActive,Name,TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,Hint,IsDefault,SortIndex)
VALUES (1,1011,'2018-01-30','2018-01-30 00:00:00.00',1,'Customers',3000,NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>IsCustomer</Label><SearchValue>4</SearchValue><Field>StatusID</Field><DisplayText>Is Customer</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,1);
");
        }
    }
}

