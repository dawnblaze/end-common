using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180810165141_AddAlertDefinitionAndAction")]
    public partial class AddAlertDefinitionAndAction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Alert.Definition",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(type: "smallint", nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14100))"),
                    ConditionFx = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConditionReadable = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConditionSQL = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CummRunCount = table.Column<int>(nullable: true),
                    DataType = table.Column<short>(type: "smallint", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmployeeID = table.Column<short>(type: "smallint", nullable: false),
                    HasImage = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    LastRunDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    TriggerConditionFx = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TriggerID = table.Column<short>(type: "smallint", nullable: false),
                    TriggerReadable = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alert.Definition", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Alert.Definition_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Alert.Definition_System.Automation.Trigger.Definition",
                        column: x => x.TriggerID,
                        principalTable: "System.Automation.Trigger.Definition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Alert.Definition_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Alert.Definition.Action",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(type: "smallint", nullable: false),
                    ActionDefinitionID = table.Column<byte>(type: "tinyint", nullable: false),
                    AlertDefinitionID = table.Column<short>(type: "smallint", nullable: false),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BodyHasMergeFields = table.Column<bool>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14102))"),
                    DataType = table.Column<short>(type: "smallint", nullable: false),
                    MetaData = table.Column<string>(type: "xml sparse", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    SortIndex = table.Column<byte>(type: "tinyint", nullable: false),
                    Subject = table.Column<string>(type: "nvarchar(4000) sparse", nullable: true),
                    SubjectHasMergeFields = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Alert.Definition.Action", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Alert.Definition.Action_System.Automation.Action.Definition",
                        column: x => x.ActionDefinitionID,
                        principalTable: "System.Automation.Action.Definition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Alert.Definition.Action_Alert.Definition",
                        columns: x => new { x.BID, x.AlertDefinitionID },
                        principalTable: "Alert.Definition",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Alert.Definition_TriggerID",
                table: "Alert.Definition",
                column: "TriggerID");

            migrationBuilder.CreateIndex(
                name: "IX_Alert.Definition_Employee",
                table: "Alert.Definition",
                columns: new[] { "BID", "EmployeeID", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Alert.Definition_Name",
                table: "Alert.Definition",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Alert.Definition_Trigger",
                table: "Alert.Definition",
                columns: new[] { "BID", "TriggerID", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Alert.Definition.Action_ActionDefinitionID",
                table: "Alert.Definition.Action",
                column: "ActionDefinitionID");

            migrationBuilder.CreateIndex(
                name: "IX_Alert.Definition.Action_BID_AlertDefinitionID",
                table: "Alert.Definition.Action",
                columns: new[] { "BID", "AlertDefinitionID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Alert.Definition.Action");

            migrationBuilder.DropTable(
                name: "Alert.Definition");
        }
    }
}

