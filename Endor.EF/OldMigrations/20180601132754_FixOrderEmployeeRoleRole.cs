using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180601132754_FixOrderEmployeeRoleRole")]
    public partial class FixOrderEmployeeRoleRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RoleTypeID",
                table: "[Order.Employee.Role]",
                newName: "RoleID");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.AddEmployeeRole]
GO

-- ========================================================
-- Name: [Order.Action.AddEmployeeRole]( @BID tinyint, @OrderID int, @EmployeeRoleID smallint, @EmployeeID int )
--
-- Description: This function adds a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.AddEmployeeRole] @BID=1, @OrderID=1, @EmployeeRoleID=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.AddEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleID  SMALLINT   -- = 1
        , @EmployeeID      INT        -- = 1

        , @Result          INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the role type does not exist on the order
    IF ISNULL(@EmployeeRoleID, -1) != 255 AND EXISTS(SELECT * FROM [Order.Employee.Role] WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @EmployeeRoleID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Employee role already exist on this order.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @NewID INT;
    EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10014, 1;

    INSERT INTO [Order.Employee.Role]
    (BID, ID, OrderID, RoleID, EmployeeID, EmployeeName)
    SELECT @BID, @NewID, @OrderID, @EmployeeRoleID, @EmployeeID, employee.ShortName
    FROM   [Employee.Data] employee
    WHERE  employee.BID = @BID AND employee.ID = @EmployeeID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.UpdateEmployeeRole]
GO

-- ========================================================
-- Name: [Order.Action.UpdateEmployeeRole]( @BID tinyint, @OrderID int, @RoleID int, @EmployeeRoleID smallint, @EmployeeID int )
--
-- Description: This function updates a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateEmployeeRole] @BID=1, @OrderID=1, @RoleID=NULL, @EmployeeRoleID=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.UpdateEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleID  SMALLINT   -- = 1
        , @EmployeeID      INT        -- = 1
		, @RoleID          INT           = NULL 

        , @Result          INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF ISNULL(@EmployeeRoleID,-1) = 255
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to change this employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    IF (@RoleID IS NULL)
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @EmployeeRoleID
    END

    ELSE
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    UPDATE OER
    SET    EmployeeID = @EmployeeID, EmployeeName = employee.ShortName, RoleID = @EmployeeRoleID
    FROM   [Order.Employee.Role] OER 
            LEFT JOIN [Employee.Data] employee ON employee.BID = @BID AND employee.ID = @EmployeeID
    WHERE  OER.BID = @BID AND OER.ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RoleID",
                table: "Order.Employee.Role",
                newName: "RoleTypeID");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.AddEmployeeRole]
GO

-- ========================================================
-- Name: [Order.Action.AddEmployeeRole]( @BID tinyint, @OrderID int, @EmployeeRoleType tinyint, @EmployeeID int )
--
-- Description: This function adds a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.AddEmployeeRole] @BID=1, @OrderID=1, @EmployeeRoleType=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.AddEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleType TINYINT   -- = 1
        , @EmployeeID      INT        -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the role type does not exist on the order
    IF ISNULL(@EmployeeRoleType, -1) != 255 AND EXISTS(SELECT * FROM [Order.Employee.Role] WHERE BID = @BID AND OrderID = @OrderID AND RoleTypeID = @EmployeeRoleType)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Employee role already exist on this order.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @NewID INT;
    EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10014, 1;

    INSERT INTO [Order.Employee.Role]
    (BID, ID, OrderID, RoleTypeID, EmployeeID, EmployeeName)
    SELECT @BID, @NewID, @OrderID, @EmployeeRoleType, @EmployeeID, employee.ShortName
    FROM   [Employee.Data] employee
    WHERE  employee.BID = @BID AND employee.ID = @EmployeeID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.UpdateEmployeeRole]
GO

-- ========================================================
-- Name: [Order.Action.UpdateEmployeeRole]( @BID tinyint, @OrderID int, @RoleID int, @EmployeeRoleType tinyint, @EmployeeID int )
--
-- Description: This function updates a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateEmployeeRole] @BID=1, @OrderID=1, @RoleID=NULL, @EmployeeRoleType=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.UpdateEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleType TINYINT   -- = 1
        , @EmployeeID       INT       -- = 1
		, @RoleID          INT           = NULL 

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF ISNULL(@EmployeeRoleType,-1) = 255
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to change this employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    IF (@RoleID IS NULL)
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleTypeID = @EmployeeRoleType
    END

    ELSE
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    UPDATE OER
    SET    EmployeeID = @EmployeeID, EmployeeName = employee.ShortName, RoleTypeID = @EmployeeRoleType
    FROM   [Order.Employee.Role] OER 
            LEFT JOIN [Employee.Data] employee ON employee.BID = @BID AND employee.ID = @EmployeeID
    WHERE  OER.BID = @BID AND OER.ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
        }
    }
}

