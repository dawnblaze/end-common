using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class InitDashboardWidgetCategoryType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Dashboard.Widget.CategoryType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Dashboard.Widget.CategoryType", x => x.ID);
                });

            migrationBuilder.Sql(@"
INSERT INTO [enum.Dashboard.Widget.CategoryType] ([ID], [Name]) VALUES (1, 'Management');
INSERT INTO [enum.Dashboard.Widget.CategoryType] ([ID], [Name]) VALUES (2, 'Accounting');
INSERT INTO [enum.Dashboard.Widget.CategoryType] ([ID], [Name]) VALUES (4, 'Sales');
INSERT INTO [enum.Dashboard.Widget.CategoryType] ([ID], [Name]) VALUES (8, 'Production');
INSERT INTO [enum.Dashboard.Widget.CategoryType] ([ID], [Name]) VALUES (16, 'Purchasing');
INSERT INTO [enum.Dashboard.Widget.CategoryType] ([ID], [Name]) VALUES (32, 'Ecommerce');
INSERT INTO [enum.Dashboard.Widget.CategoryType] ([ID], [Name]) VALUES (128, 'User');
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Dashboard.Widget.CategoryType");
        }
    }
}
