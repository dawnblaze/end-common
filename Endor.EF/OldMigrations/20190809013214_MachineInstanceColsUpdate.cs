using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class MachineInstanceColsUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@" 
				
	        ALTER TABLE dbo.[Part.Machine.Instance]
	        DROP CONSTRAINT [FK_Part.Machine.Instance_Part.Machine.Data_MachineDataBID_MachineDataID]; 	

	        DROP INDEX IF EXISTS [IX_Part.Machine.Instance_MachineDataBID_MachineDataID] ON [dbo].[Part.Machine.Instance];
            ALTER TABLE dbo.[Part.Machine.Instance] 
	        DROP COLUMN [MachineDataBID],[MachineDataID];

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
