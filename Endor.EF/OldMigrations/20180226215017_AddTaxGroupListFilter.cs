using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180226215017_AddTaxGroupListFilter")]
    public partial class AddTaxGroupListFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[List.Filter]
                ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1017,'2018-2-26','2018-2-26',1,'Active',11101,null,'<ArrayOfListFilterItem>
	                <ListFilterItem>
		                <SearchValue>true</SearchValue>
		                <Field>IsActive</Field>
		                <IsHidden>true</IsHidden>
		                <IsSystem>true</IsSystem>
		                <DisplayText>Is Active</DisplayText>
	                </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,0)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
        DELETE FROM [dbo].[List.Filter] where ID = 1017;
");
        }
    }
}

