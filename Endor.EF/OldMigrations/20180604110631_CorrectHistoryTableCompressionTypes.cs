using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180604110631_CorrectHistoryTableCompressionTypes")]
    public partial class CorrectHistoryTableCompressionTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // remove incorrectly named history table
            migrationBuilder.DisableSystemVersioningIfEnabled("Order.Contact.Locator");
            
            migrationBuilder.Sql(@"
DROP TABLE IF EXISTS [Historic.Order.Contact.Loctaor];
");
            // remove the correctly named history table, since it is out of sync
            migrationBuilder.Sql(@"
DROP TABLE IF EXISTS [Historic.Order.Contact.Locator];
");
            migrationBuilder.EnableSystemVersioning("Order.Contact.Locator");

            // Correct table compression types
            migrationBuilder.Sql(@"ALTER TABLE [Historic.Order.Data] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE);");
            migrationBuilder.Sql(@"ALTER TABLE [Historic.Order.Item.Data] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE);");
            migrationBuilder.Sql(@"ALTER TABLE [Historic.Order.Contact.Locator] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE);");
            migrationBuilder.Sql(@"ALTER TABLE [Historic.Order.Contact.Role] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE);");
            migrationBuilder.Sql(@"ALTER TABLE [Historic.Order.Employee.Role] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE);");
            migrationBuilder.Sql(@"ALTER TABLE [Historic.Order.KeyDate] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE);");
            migrationBuilder.Sql(@"ALTER TABLE [Historic.Order.Note] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE);");
            migrationBuilder.Sql(@"ALTER TABLE [Historic.Order.OrderLink] REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE);");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

