using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180409204452_Function-Util-ClassType-GetTableName_END-866")]
    public partial class FunctionUtilClassTypeGetTableName_END866 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION IF EXISTS [dbo].[Util.ClassType.GetTableName]
GO

-- ========================================================
-- 
-- Name: Util.ID.GetID( @BID smallint, @ClassTypeID int, @Count int = 1 )
--
-- Description: This Function Gets a New ID for a ClassType (Table).
--  If requesting multiple IDs, the first ID in the series is returned.
--
-- Sample Use:   
--      declare @NewID int;
--      exec @NewID = dbo.[Util.ID.GetID] @BID=100, @ClassTypeID=3500, @Count=1
--
-- for Byte IDs, use
--      exec @NewID = dbo.[Util.ID.GetID] @BID=100, @ClassTypeID=3500, @StartingID=10
-- ========================================================
CREATE FUNCTION [dbo].[Util.ClassType.GetTableName] ( @ClassTypeID int )
RETURNS VARCHAR(255)
AS
BEGIN
    RETURN COALESCE(

        -- First Try from the enum.ClassType table
        (   SELECT TOP(1) TableName 
            FROM [enum.ClassType]
            WHERE ID = @ClassTypeID
        ),

        -- Second Try  from the ClassTypeID Definition in SQL
        (   SELECT TOP(1) TableName
            FROM TablesAndColumns
            WHERE ColumnName = 'ClassTypeID'
            AND TRY_CONVERT(INT, Replace(Replace(ComputedFormula, ')', ''), '(', '')) = @ClassTypeID
        ),

        -- Next, custom handling
        (   CASE @ClassTypeID 
                WHEN 8000 THEN 'Accounting.GL.Account'
                ELSE NULL
            END
        )
    )
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP FUNCTION IF EXISTS [dbo].[Util.ClassType.GetTableName]");
        }
    }
}

