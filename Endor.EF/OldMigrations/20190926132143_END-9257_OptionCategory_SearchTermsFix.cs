﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9257_OptionCategory_SearchTermsFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [System.Option.Category]
SET SearchTerms = 'email gmail outlook smtp internet google microsoft handler yahoo send'
WHERE ID = 104
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [System.Option.Category]
SET SearchTerms = 'email gmail outlook smtp internet'
WHERE ID = 104
;");
        }
    }
}
