using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180427153331_OrderNote_RemoveExtraColumn_AddJsonAttributes_XML-Validation")]
    public partial class OrderNote_RemoveExtraColumn_AddJsonAttributes_XMLValidation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "Order.Note",
                type: "VARCHAR(MAX)",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "enum.Order.OrderLinkType",
                unicode: false,
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 100);

            migrationBuilder.DropForeignKey("FK_Order.Note_Order.Data_OrderDataBID_OrderDataID", "Order.Note");
            migrationBuilder.DropColumn("OrderDataBID", "Order.Note");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "Order.Note",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "VARCHAR(MAX)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "enum.Order.OrderLinkType",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 100);

            migrationBuilder.AddColumn<int>(
                name: "OrderDataBID",
                table: "Order.Note",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Note_Order.Data_OrderDataBID_OrderDataID",
                table: "Order.Data",
                columns: new[] { "OrderDataBID", "OrderDataID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}

