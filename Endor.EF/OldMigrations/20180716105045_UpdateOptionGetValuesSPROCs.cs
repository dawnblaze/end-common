using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180716105045_UpdateOptionGetValuesSPROCs")]
    public partial class UpdateOptionGetValuesSPROCs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Option.GetValues];
GO

-- ========================================================
-- 
-- Name: [Option.GetValues]
--
-- Description: This procedure looks up a option/setting based on a hierarchy of possibilities.
-- 
--
-- Sample Use:   EXEC dbo.[Option.GetValue] @CategoryID = 300, @BID=2, @LocationID = 1
-- Returns: Table with columns
	-- ID INT
	-- [Name] VARCHAR(255),
	-- [Value] VARCHAR(255),
	-- [Level] INT,
	-- CategoryID INT,
	-- CategoryName VARCHAR(255),
	-- SectionName VARCHAR(255)

    -- OPTION LEVELS
    --    System=0
    --    Association=1
    --    Business=2
    --    Location=4
    --    Storefront=8
    --    Employee=16
    --    Company=32
    --    Contact=64
-- ========================================================

CREATE PROCEDURE [dbo].[Option.GetValues]
-- DECLARE
          @CategoryID     smallint     = NULL

		, @AssociationID  tinyint      = NULL
		, @BID            smallint     = NULL
		, @LocationID     smallint     = NULL
		, @StorefrontID   smallint     = NULL
		, @EmployeeID     smallint     = NULL
		, @CompanyID      int          = NULL
		, @ContactID      int          = NULL
AS

BEGIN
    IF (@BID IS NULL) AND COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;

    IF COALESCE(@BID, @AssociationID) IS NULL
    BEGIN;
        THROW 180000, 'You must supply at least one of the BID or the AssociationID' , 1;
        RETURN;
    END;

    -- Define the Results
	DECLARE @Result TABLE (   [OptionID] INT PRIMARY KEY
		                    , [Value] NVARCHAR(255) INDEX IX_Result_Value WHERE [Value] IS NULL
                            , [DefaultValue] VARCHAR(MAX)
		                    , [Name] VARCHAR(255)
                            , [Label] VARCHAR(255)
                            , [Description] VARCHAR(MAX)
                            , [DataType] TINYINT
		                    , [CategoryID] SMALLINT
                            , [ListValues] VARCHAR(MAX)
                            , [IsHidden] BIT
	                      );

    -- Define a list of Categories to JOIN to get the results
    DECLARE @Categories TABLE( ID SMALLINT PRIMARY KEY  );
    IF (@CategoryID IS NULL)
        INSERT INTO @Categories SELECT ID FROM [System.Option.Category]
    ELSE
        INSERT INTO @Categories SELECT @CategoryID
    ;

    -- Now Fill the List with Option Definitions
    INSERT INTO @Result 
        SELECT DEF.[ID] as [OptionID]
            , NULL AS [Value] 
            , DEF.[DefaultValue]
            , DEF.[Name]
            , DEF.[Label]
            , DEF.Description
            , DEF.DataType
            , CAT.ID as CategoryID
            , DEF.ListValues
            , DEF.IsHidden

        FROM @Categories CAT
        JOIN [System.Option.Definition] DEF ON DEF.CategoryID = CAT.ID
    ;

    -- Lookup values in the option hiearchy 
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;
    END;

    -- Not look up option value based on the specific instance
    IF (@ContactID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Value] IS NULL
            AND (OPT.EmployeeID   IS NULL)
            AND (OPT.ContactID    = @ContactID
                    OR OPT.CompanyID    = @CompanyID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ; 
    
    IF (@CompanyID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Value] IS NULL
            AND (OPT.EmployeeID   IS NULL)
            AND (OPT.ContactID    IS NULL)
            AND (OPT.CompanyID    = @CompanyID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@EmployeeID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Value] IS NULL
            AND (OPT.CompanyID    IS NULL)
            AND (OPT.EmployeeID   = @EmployeeID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@StorefrontID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Value] IS NULL
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.EmployeeID   IS NULL )
            AND (OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@LocationID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Value] IS NULL
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.EmployeeID   IS NULL )
            AND (OPT.StorefrontID IS NULL )
            AND (OPT.LocationID   = @LocationID)
    ;

    IF (@BID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Value] IS NULL
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.EmployeeID   IS NULL )
            AND (OPT.StorefrontID IS NULL )
            AND (OPT.LocationID   IS NULL )
    ;

    IF @AssociationID IS NOT NULL
        UPDATE RES
        SET [Value] = OPT.[Value]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID
        WHERE RES.[Value] IS NULL
            AND (OPT.AssociationID = @AssociationID)
    ;

    -- Now return what you found!
    SELECT *
	FROM @Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP PROCEDURE IF EXISTS [dbo].[Option.GetValues];");
        }
    }
}

