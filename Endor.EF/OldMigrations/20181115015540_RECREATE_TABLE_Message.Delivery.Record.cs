using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class RECREATE_TABLE_MessageDeliveryRecord : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP TABLE [Message.Delivery.Record]
                ;
                --
                /* ================= Message.Delivery.Record =============== */
                --
                CREATE TABLE [Message.Delivery.Record](
                    [BID] [smallint] NOT NULL,
                    [ID] [int] NOT NULL,
                    [ParticipantID] [int] NOT NULL,
                    [AttemptNumber] [smallint] NOT NULL,
                    [ClassTypeID]  AS ((14114)),
                    [ModifiedDT] [datetime2](2) NOT NULL DEFAULT GetUTCDate(),
                    [IsPending]  AS (IsNull(case when [AttemptedDT] IS NOT NULL OR [ScheduledDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end, CONVERT([bit],(1)))) PERSISTED,
                    [ScheduledDT] [datetime2](2) SPARSE  NULL,
                    [AttemptedDT] [datetime2](2) NULL,
                    [WasSuccessful] [bit] NOT NULL,
                    [FailureMessage] [nvarchar](max) SPARSE  NULL,
                    [MetaData] [xml] SPARSE  NULL,
                    CONSTRAINT [PK_Message.Delivery.Record] PRIMARY KEY ( [BID], [ParticipantID], [AttemptNumber] )
                );
                
                CREATE INDEX [IX_Message.Delivery.Record_Pending]
                ON [Message.Delivery.Record] ( [BID], [IsPending], [ScheduledDT] )
                ;
                
                ALTER TABLE [Message.Delivery.Record] WITH CHECK
                ADD CONSTRAINT [FK_Message.Delivery.Record_Message.Participant]
                FOREIGN KEY([BID], [ParticipantID]) REFERENCES [Message.Participant.Info] ([BID], [ID])
                ;             
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
