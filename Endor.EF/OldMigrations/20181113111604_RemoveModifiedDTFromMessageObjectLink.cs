using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class RemoveModifiedDTFromMessageObjectLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModifiedDT",
                table: "Message.Object.Link");

            migrationBuilder.DropColumn(
                name: "ClassTypeID",
                table: "Message.Object.Link");

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDT",
                table: "Message.Object.Link",
                type: "datetime2(2)",
                nullable: false);

            migrationBuilder.AddColumn<int>(
                name: "ClassTypeID",
                table: "Message.Object.Link",
                type: "int",
                nullable: false);

        }
    }
}
