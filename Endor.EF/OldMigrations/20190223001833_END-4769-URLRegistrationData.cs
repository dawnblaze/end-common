using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4769URLRegistrationData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.URL.RegistrationType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.URL.RegistrationType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "URL.Registration.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "21001"),
                    CompanyID = table.Column<int>(type: "int sparse", nullable: true),
                    ContactID = table.Column<int>(type: "int sparse", nullable: true),
                    CreatedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    EmployeeID = table.Column<short>(type: "smallint sparse", nullable: true),
                    ExpirationDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false),
                    ExpireAfterUseDays = table.Column<short>(nullable: true),
                    FirstUseDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false, computedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                AND [UseCount] < coalesce([MaxUseCount], (1))
                                    AND ([UseCount] = (0)
                                            OR[ExpireAfterUseDays] IS NULL
                                            OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                then CONVERT([bit], (1))
                                else CONVERT([bit],(0)) end,(0)))"),
                    IsRevoked = table.Column<bool>(nullable: false, defaultValueSql: "((0))"),
                    LastUseDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    MaxUseCount = table.Column<int>(nullable: false, defaultValueSql: "((1))"),
                    MetaDataJSON = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    PublishedShortenedURL = table.Column<string>(type: "varchar(50)", nullable: true),
                    PublishedURLBase = table.Column<string>(type: "varchar(1024)", nullable: true),
                    PublishedURLFull = table.Column<string>(type: "varchar(1024)", nullable: false, computedColumnSql: @"(concat([PublishedURLRoot]
                                        + case when [PublishedURLRoot] like '%/'
                                            then ''
                                            else '/' end
                                    ,[PublishedURLBase]
                                        + case when [PublishedURLBase] like '%/'
                                            then ''
                                            else '/' end
                                    ,lower(replace([PublishedURLPath],'-',''))))"),
                    PublishedURLPath = table.Column<Guid>(nullable: false, defaultValueSql: "(newid())"),
                    PublishedURLRoot = table.Column<string>(type: "varchar(1024)", nullable: false),
                    RegistrationType = table.Column<byte>(nullable: false),
                    TargetClassTypeID = table.Column<short>(nullable: true),
                    TargetID = table.Column<int>(nullable: true),
                    TargetURL = table.Column<string>(type: "varchar(2048)", nullable: true),
                    TargetURLHeaders = table.Column<string>(type: "varchar(max)", nullable: true),
                    TargetURLParameters = table.Column<string>(type: "varchar(max)", nullable: true),
                    UseCount = table.Column<int>(nullable: false, defaultValueSql: "((0))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_URL.Registration.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_URL.Registration.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_URL.Registration.Data_enum.URL.RegistrationType",
                        column: x => x.RegistrationType,
                        principalTable: "enum.URL.RegistrationType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_URL.Registration.Data_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_URL.Registration.Data_Contact.Data",
                        columns: x => new { x.BID, x.ContactID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_URL.Registration.Data_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "URL.Registration.Lookup.History",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    BrowserTypeName = table.Column<string>(type: "varchar(50)", nullable: true),
                    BrowserVersion = table.Column<string>(type: "varchar(50)", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "21002"),
                    DeviceInfo = table.Column<string>(type: "varchar(1024)", nullable: true),
                    DeviceName = table.Column<string>(type: "varchar(50)", nullable: true),
                    IP = table.Column<string>(type: "varchar(20)", nullable: true),
                    MaxHeight = table.Column<short>(nullable: true),
                    MaxWidth = table.Column<short>(nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Platform = table.Column<string>(type: "varchar(50)", nullable: true),
                    RegistrationID = table.Column<int>(nullable: false),
                    WasSuccessful = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_URL.Registration.Lookup.History", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_URL.Registration.Lookup.History_URL.Registration.Data",
                        columns: x => new { x.BID, x.RegistrationID },
                        principalTable: "URL.Registration.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_URLPath",
                table: "URL.Registration.Data",
                column: "PublishedURLPath");

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_RegistrationType",
                table: "URL.Registration.Data",
                column: "RegistrationType");

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_Company",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "CompanyID" });

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_Contact",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "ContactID" });

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_Employee",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_TargetObject",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "TargetClassTypeID", "TargetID" });

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Lookup.History_Registration",
                table: "URL.Registration.Lookup.History",
                columns: new[] { "BID", "RegistrationID" });

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[enum.URL.RegistrationType]([ID], [Name])
VALUES (0, N'Tracking'),
    (1, N'Redirect'),
    (2, N'RedirectAsEndorEmployee'),
    (3, N'RedirectAsEndorContact'),
    (4, N'RedirectAsEcommContact'),
    (5, N'BlobRead'),
    (6, N'BlobReadWrite'),
    (7, N'ArtworkApproval'),
    (8, N'OnlinePayment'),
    (9, N'OnlineInvoice')
;
        ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "URL.Registration.Lookup.History");

            migrationBuilder.DropTable(
                name: "URL.Registration.Data");

            migrationBuilder.DropTable(
                name: "enum.URL.RegistrationType");
        }
    }
}
