﻿using Endor.Models;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_10096_UpdatingContactStatusID : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE C
SET [StatusID] = ISNULL(
	(SELECT
        MAX(CASE 
			WHEN OD.[TransactionType] = 2 AND OCR.[RoleType] = 1 THEN 4
			WHEN OD.[TransactionType] = 1 AND OCR.[RoleType] = 1 THEN 2
			ELSE 1
		END)
	FROM
		[Order.Contact.Role] OCR
		LEFT JOIN [Order.Data] AS OD ON OD.ID = OCR.[OrderID] AND OD.BID = OCR.[BID]
	WHERE OCR.[ContactID] = C.[ID] AND OCR.[BID] = C.[BID])
, 1)
FROM [Contact.Data] AS C
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }

        public override int? ReindexClasstype()
        {
            return ClassType.Contact.ID();
        }
    }
}
