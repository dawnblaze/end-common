﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class ReconciliationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounting.Reconciliation.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "8015"),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    CreatedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    LastAccountingDT = table.Column<DateTime>(type: "DATETIME2(0)", nullable: false),
                    AccountingDT = table.Column<DateTime>(type: "DATETIME2(0)", nullable: false),
                    IsAdjustmentEntry = table.Column<bool>(type: "BIT", nullable: false),
                    LocationID = table.Column<byte>(type: "TINYINT", nullable: false),
                    GLActivityID = table.Column<int>(type: "INT", nullable: false),
                    StartingGLID = table.Column<int>(type: "INT", nullable: true),
                    EndingGLID = table.Column<int>(type: "INT", nullable: true),
                    EnteredByID = table.Column<short>(type: "SMALLINT", nullable: true),
                    Description = table.Column<string>(type: "NVARCHAR(100)", nullable: true),
                    Notes = table.Column<string>(type: "NVARCHAR(500)", nullable: true),
                    ExportedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false),
                    WasExported = table.Column<bool>(type: "BIT", nullable: false, computedColumnSql: "(isnull(case when [ExportedDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Reconciliation.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Data_Accounting.GL.Data_Last",
                        columns: x => new { x.BID, x.EndingGLID },
                        principalTable: "Accounting.GL.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Data_Employee.Data",
                        columns: x => new { x.BID, x.EnteredByID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Data_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Data_Accounting.GL.Data_First",
                        columns: x => new { x.BID, x.StartingGLID },
                        principalTable: "Accounting.GL.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Reconciliation.Item",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "8015"),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    ReconciliationID = table.Column<int>(type: "INT", nullable: false),
                    GLAccountID = table.Column<int>(type: "INT", nullable: true),
                    PaymentMethodID = table.Column<byte>(type: "TINYINT", nullable: false),
                    PaymentMasterCount = table.Column<short>(type: "SMALLINT SPARSE", nullable: true),
                    PaymentApplicationCount = table.Column<short>(type: "SMALLINT SPARSE", nullable: true),
                    Amount = table.Column<decimal>(type: "DECIMAL", nullable: true),
                    Balance = table.Column<decimal>(type: "DECIMAL", nullable: true),
                    CurrencyType = table.Column<byte>(type: "TINYINT SPARSE", nullable: true),
                    IsPaymentSummary = table.Column<bool>(type: "BIT", nullable: false, computedColumnSql: "(isnull(case when [PaymentMethodID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Reconciliation.Item", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Iteml_enum.Accounting.CurrencyType",
                        column: x => x.CurrencyType,
                        principalTable: "enum.Accounting.CurrencyType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Item_Accounting.Payment.Method",
                        column: x => x.PaymentMethodID,
                        principalTable: "enum.Accounting.PaymentMethodType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Item_Accounting.GL.Account",
                        columns: x => new { x.BID, x.GLAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Reconciliation.Data_Accounting.Reconciliation.Item",
                        columns: x => new { x.BID, x.ReconciliationID },
                        principalTable: "Accounting.Reconciliation.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_EndingGLID",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "EndingGLID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_EnteredByID",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "EnteredByID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_LocationID",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_StartingGLID",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "StartingGLID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Item_CurrencyType",
                table: "Accounting.Reconciliation.Item",
                column: "CurrencyType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Item_PaymentMethodID",
                table: "Accounting.Reconciliation.Item",
                column: "PaymentMethodID");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Item_BID_GLAccountID",
                table: "Accounting.Reconciliation.Item",
                columns: new[] { "BID", "GLAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Item_Master",
                table: "Accounting.Reconciliation.Item",
                columns: new[] { "BID", "ReconciliationID", "GLAccountID", "PaymentMethodID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounting.Reconciliation.Item");

            migrationBuilder.DropTable(
                name: "Accounting.Reconciliation.Data");
        }
    }
}
