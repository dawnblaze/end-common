using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180606152109_END-1396_ListFilters_EmployeeTeam")]
    public partial class END1396_ListFilters_EmployeeTeam : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[List.Filter]
([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],
[Criteria],
[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
VALUES
(1,    1030,'2018-6-6',  '2018-6-6',           1,'All',            5020,null,
null,
null,            0,        1,  null,       1,         0)


INSERT INTO [dbo].[List.Filter]
([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],
[Criteria],
[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
VALUES
(1,    1031,'2018-6-6',  '2018-6-6',           1,'Active',            5020,null,
'<ArrayOfListFilterItem>
    <ListFilterItem>
	    <Label>Active</Label>
        <SearchValue>true</SearchValue>
        <Field>IsActive</Field>
        <IsHidden>true</IsHidden>
        <IsSystem>true</IsSystem>
        <DisplayText>Is Active</DisplayText>
    </ListFilterItem>
</ArrayOfListFilterItem>',
null,            0,        1,  null,       1,         0)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [dbo].[List.Filter] where ID = 1030;");
            migrationBuilder.Sql(@"DELETE FROM [dbo].[List.Filter] where ID = 1031;");
        }
    }
}

