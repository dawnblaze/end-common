using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8163_Modify_AccountingPaymentTermActionSetDefault_Sproc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"-- ========================================================
-- Name: [Accounting.Payment.Term.Action.SetDefault]
--
-- Description: This procedure sets the default PaymentTerm in the options definition table.
--
-- Date: 2018-02-22
-- 
-- Sample Use: EXEC dbo.[Accounting.Payment.Term.Action.SetDefault] @BID=1, @PaymentTermID=1, @IsActive=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetDefault]
          @BID           TINYINT           -- = 1
        , @PaymentTermID SMALLINT     -- = 2
        , @IsDefault     BIT           = 1
        , @Result        INT           = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);
    -- Check if the option specified is valid
    IF NOT EXISTS(SELECT * FROM [System.Option.Definition] WHERE Name = 'Accounting.PaymentTerm.DefaultID')
    BEGIN
        SELECT @Result = 0
             , @Message = 'The Accounting.PaymentTerm.DefaultID OPTION is not found.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;
	IF (@PaymentTermID != 0)
	BEGIN
		-- Check if the payment term exists
		IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Term] WHERE ID = @PaymentTermID)
		BEGIN
			SELECT @Result = 0
				 , @Message = 'Accounting.Payment.Term does not exist.'
				 ;

			THROW 50000, @Message, 1;
			RETURN @Result;
		END;

		-- Now update it
		UPDATE L
		SET DefaultValue = @PaymentTermID
		FROM [System.Option.Definition] L
		WHERE Name = 'Accounting.PaymentTerm.DefaultID'
	END
	ELSE
	BEGIN
		UPDATE [System.Option.Definition]
		SET DefaultValue = NULL
		WHERE [Name] = 'Accounting.PaymentTerm.DefaultID'
	END
	SET @Result = @@ROWCOUNT;
	SELECT @Result as Result;
END");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"-- ========================================================
-- Name: [Accounting.Payment.Term.Action.SetDefault]
--
-- Description: This procedure sets the default PaymentTerm in the options definition table.
--
-- Date: 2018-02-22
--
-- Sample Use:   EXEC dbo.[Accounting.Payment.Term.Action.SetDefault] @BID=1, @PaymentTermID=1, @IsActive=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetDefault]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @PaymentTermID     SMALLINT     -- = 2

        , @IsDefault       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the option specified is valid
    IF NOT EXISTS(SELECT * FROM [System.Option.Definition] WHERE Name = 'Accounting.PaymentTerm.DefaultID')
    BEGIN
        SELECT @Result = 0
             , @Message = 'The Accounting.PaymentTerm.DefaultID OPTION is not found.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	-- Check if the payment term exists
    IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Term] WHERE ID = @PaymentTermID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Accounting.Payment.Term does not exist.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET DefaultValue = @PaymentTermID
    FROM [System.Option.Definition] L
    WHERE Name = 'Accounting.PaymentTerm.DefaultID'

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END");
        }
    }
}
