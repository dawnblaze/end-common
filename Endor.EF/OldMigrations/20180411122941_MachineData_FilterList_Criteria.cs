using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180411122941_MachineData_FilterList_Criteria")]
    public partial class MachineData_FilterList_Criteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                    ([TargetClassTypeID]
                    ,[Name]
                    ,[Label]
                    ,[Field]
                    ,[IsHidden]
                    ,[DataType]
                    ,[InputType]
                    ,[AllowMultiple]
                    ,[ListValues]
                    ,[ListValuesEndpoint]
                    ,[IsLimitToList]
                    ,[SortIndex])
                VALUES
                    (12030 --<TargetClassTypeID, int,>
                    ,'Name' --<Name, varchar(255),>
                    ,'Name'  --<Label, varchar(255),>
                    ,'Name' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,0 --<DataType, tinyint,>
                    ,0 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,NULL --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,0 --<IsLimitToList, bit,>
                    ,0) --<SortIndex, tinyint,>

                    ,(12030 --<TargetClassTypeID, int,>
                    ,'Is Active' --<Name, varchar(255),>
                    ,'Is Active'  --<Label, varchar(255),>
                    ,'IsActive' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,3 --<DataType, tinyint,>
                    ,2 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,'Is Not Active,Is Active' --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,1 --<IsLimitToList, bit,>
                    ,1) --<SortIndex, tinyint,>
            ");

            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[List.Filter]
                ([BID],ID,[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1027,'2018-3-14','2018-3-14',1,'Active',12030,null,'<ArrayOfListFilterItem>
	                <ListFilterItem>
		                <SearchValue>true</SearchValue>
		                <Field>IsActive</Field>
		                <IsHidden>true</IsHidden>
		                <IsSystem>true</IsSystem>
		                <DisplayText>Is Active</DisplayText>
	                </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,0)
            ");            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [dbo].[List.Filter] where ID = 1027");

            migrationBuilder.Sql(@"DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE TargetClassTypeID = 12030 AND (Name='Name' OR NAME='Is Active')");
        }
    }
}

