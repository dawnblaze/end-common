using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class InsertSubassemblyEnums : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

                DROP TABLE IF EXISTS [dbo].[enum.Part.Subassembly.ElementType]
                GO
                CREATE TABLE [dbo].[enum.Part.Subassembly.ElementType](
                    [ID] [tinyint] NOT NULL,
                    [Name] [varchar](100) NOT NULL,
                CONSTRAINT [PK_enum.Part.Subassembly.ElementType] PRIMARY KEY ([ID] ASC)
                )
                GO
                INSERT INTO [dbo].[enum.Part.Subassembly.ElementType] 
                ([ID], [Name])
                VALUES (0, N'Group')
                 , (1, N'Single Line Label')
                 , (2, N'Multi-Line Label')
                 , (3, N'URL Label')
                 , (7, N'Spacer')
                 , (11, N'Single Line Text')
                 , (12, N'Multi-Line String')
                 , (13, N'Drop-Down')
                 , (21, N'Number')
                 , (22, N'Measurement')
                 , (31, N'Checkbox')
                 , (102, N'Show/Hide Group')
                 , (106, N'Linked Subassembly')
                 , (107, N'Linked Machine')
                GO

                DROP TABLE IF EXISTS [dbo].[enum.Part.Subassembly.IncomeAllocationType]
                GO
                CREATE TABLE [dbo].[enum.Part.Subassembly.IncomeAllocationType](
                 [ID] [tinyint] NOT NULL,
                 [Name] [varchar](100) NOT NULL,
                 CONSTRAINT [PK_enum.Part.Subassembly.IncomeAllocationType] PRIMARY KEY ( [ID] ASC )
                )
                GO
                INSERT INTO [dbo].[enum.Part.Subassembly.IncomeAllocationType] ([ID], [Name])
                VALUES (0, N'Single Income Allocation')
                 , (1, N'Allocation by Material Income Account')
                 , (2, N'Allocation by Machine Income Account')
                GO

                DROP TABLE IF EXISTS [dbo].[enum.Part.Subassembly.LabelType]
                GO
                CREATE TABLE [dbo].[enum.Part.Subassembly.LabelType](
                 [ID] [tinyint] NOT NULL,
                 [Name] [varchar](100) NOT NULL,
                 CONSTRAINT [PK_enum.Part.Subassembly.LabelType] PRIMARY KEY ( [ID] ASC )
                )
                GO
                INSERT INTO [dbo].[enum.Part.Subassembly.LabelType] ([ID], [Name])
                VALUES (0, N'None')
                 , (1, N'Information')
                 , (2, N'Success')
                 , (3, N'Warning')
                 , (4, N'Error')
                GO

                DROP TABLE IF EXISTS [dbo].[enum.Part.Subassembly.LayoutType]
                GO
                CREATE TABLE [dbo].[enum.Part.Subassembly.LayoutType](
                 [ID] [tinyint] NOT NULL,
                 [Name] [varchar](100) NOT NULL,
                 CONSTRAINT [PK_enum.Part.Subassembly.LayoutType] PRIMARY KEY ( [ID] ASC )
                )
                GO
                INSERT INTO [dbo].[enum.Part.Subassembly.LayoutType] ([ID], [Name])
                VALUES (1, N'SA Order Entry Basic')
                 , (2, N'SA Order Entry Advanced')
                 , (3, N'SA Ecommerce Basic')
                 , (11, N'Machine Order Entry Basic')
                 , (12, N'Machine Order Entry Advanced')
                 , (13, N'Machine Setup')
                GO

                DROP TABLE IF EXISTS [dbo].[enum.Part.Subassembly.List.DataType]
                GO
                CREATE TABLE [dbo].[enum.Part.Subassembly.List.DataType](
                 [ID] [smallint] NOT NULL,
                 [Name] [varchar](100) NOT NULL,
                 CONSTRAINT [PK_enum.Part.Subassembly.List.DataType] PRIMARY KEY ( [ID] ASC )
                )
                GO
                INSERT INTO [dbo].[enum.Part.Subassembly.List.DataType] ([ID], [Name])
                VALUES (1, N'Text')
                 , (2, N'Text with Number Value')
                 , (12000, N'Materials')
                 , (12020, N'Labor')
                 , (12030, N'Machines')
                GO

                DROP TABLE IF EXISTS [dbo].[enum.Part.Subassembly.PricingType]
                GO
                CREATE TABLE [dbo].[enum.Part.Subassembly.PricingType](
                 [ID] [tinyint] NOT NULL,
                 [Name] [varchar](100) NOT NULL,
                 CONSTRAINT [PK_enum.Part.Subassembly.PricingType] PRIMARY KEY ( [ID] ASC )
                )
                GO
                INSERT INTO [dbo].[enum.Part.Subassembly.PricingType] ([ID], [Name])
                VALUES (0, N'Market Based Pricing')
                 , (1, N'Cost Based Pricing')
                GO

                DROP TABLE IF EXISTS [dbo].[enum.Part.Subassembly.Table.MatchType]
                GO
                CREATE TABLE [dbo].[enum.Part.Subassembly.Table.MatchType](
                 [ID] [tinyint] NOT NULL,
                 [Name] [varchar](100) NOT NULL,
                 CONSTRAINT [PK_enum.Part.Subassembly.Table.MatchType] PRIMARY KEY ( [ID] ASC )
                )
                GO
                INSERT INTO [dbo].[enum.Part.Subassembly.Table.MatchType] ([ID], [Name])
                VALUES (0, N'Exact Match')
                 , (1, N'Use Lower Value')
                 , (2, N'Use Higher Value')
                 , (3, N'Interpolate')
                GO

                DROP TABLE IF EXISTS [dbo].[enum.Part.Subassembly.TableType]
                GO
                CREATE TABLE [dbo].[enum.Part.Subassembly.TableType](
                 [ID] [tinyint] NOT NULL,
                 [Name] [varchar](100) NOT NULL,
                 CONSTRAINT [PK_enum.Part.Subassembly.TableType] PRIMARY KEY ( [ID] ASC )
                )
                GO
                INSERT INTO [dbo].[enum.Part.Subassembly.TableType] ([ID], [Name])
                VALUES (0, N'Custom')
                 , (1, N'Tier - Percentage Discount (Market Based)')
                 , (2, N'Tier - Unit Price (Market Based)')
                 , (3, N'Tier - Margin (Cost Based)')
                 , (4, N'Tier - Multiplier (Cost Based)')
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
