using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180730035120_END-2036-Add-TimeClock-Option-Definitions")]
    public partial class END2036AddTimeClockOptionDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
VALUES (4051, N'Timeclock.Enabled', N'Enable Time Clock Functions (Clock In, Out)', N'Enable Time Clock Functions (Clock In, Out)', 3, 405, N'', N'1', 0)
    , (4052, N'Timeclock.BreakTracking.Enabled', N'Enable Break Tracking', N'Enable Break Tracking on Time Cards', 3, 405, N'', N'1', 0)
    , (4053, N'Timeclock.ActivityTracking.Enabled', N'Enable Activity Tracking', N'Enable Activity Tracking on Time Cards', 3, 405, N'', N'1', 0)
    , (4054, N'Timeclock.ActivityTracking.UseItemStatus', N'Use Line Item Status for Time Clock Activities', N'Use Line Item Status for Time Clock Activities', 3, 405, N'', N'1', 0)
    , (4055, N'Timeclock.ActivityTracking.UseCustomList', N'Use Custom List for Time Clock Activities', N'Use Custom List for Time Clock Activities', 3, 405, N'', N'0', 0)
    , (4056, N'Timeclock.Require.ActivityOnClockIn', N'Require Activity when Clocking In', N'Require an Time Clock Activity be selected when Clocking In', 3, 405, N'', N'0', 0)
    , (4057, N'Timeclock.Require.ActivityOnStartJob', N'Require Activity when Starting Job', N'Require an Time Clock Activity be selected when Starting a Job', 3, 405, N'', N'0', 0)
;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE [System.Option.Definition]
WHERE ID in (4051, 4052, 4053, 4054, 4055, 4056, 4057)
");
        }
    }
}

