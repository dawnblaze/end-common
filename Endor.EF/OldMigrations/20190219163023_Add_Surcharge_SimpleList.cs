using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_Surcharge_SimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                IF EXISTS(select 1 from sys.views where name='Part.Surcharge.SimpleList' and type='v')
                DROP VIEW [dbo].[Part.Surcharge.SimpleList];
                GO
                CREATE VIEW[dbo].[Part.Surcharge.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                        , [IsActive]
                        , CONVERT(BIT, 0) AS[HasImage]
                        , CONVERT(BIT, 0) AS[IsDefault]
                FROM[Part.Surcharge.Data]"
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Surcharge.SimpleList");
        }
    }
}
