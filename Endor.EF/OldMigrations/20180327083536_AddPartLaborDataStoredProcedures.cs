using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180327083536_AddPartLaborDataStoredProcedures")]
    public partial class AddPartLaborDataStoredProcedures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Labor.Data.Action.SetActive]
--
-- Description: This procedure sets the Labor Data to active or inactive
--
-- Sample Use:   EXEC dbo.[Part.Labor.Data.Action.SetActive] @BID=1, @TaxGroupID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Labor.Data.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @LaborDataID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Labor Data specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Labor.Data] WHERE BID = @BID and ID = @LaborDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Labor Data Specified. LaborDataID='+CONVERT(VARCHAR(12),@LaborDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Part.Labor.Data] L
    WHERE BID = @BID and ID = @LaborDataID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
            ");

            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Part.Labor.Data.Action.CanDelete]

    Description: This procedure checks if the Labor Data is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Labor.Data.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Labor.Data.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the Labor Data can be deleted. The boolean response is returned in the body.  A Labor Data can be deleted if

*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Labor.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Data Specified Does not Exist. LaborDataID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE [dbo].[Part.Labor.Data.Action.CanDelete]
DROP PROCEDURE [dbo].[Part.Labor.Data.Action.SetActive]
            ");
        }
    }
}

