using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180818031028_END-2223_Domain_Filter_Criteria_AccessType_Add_Spacing")]
    public partial class END2223_Domain_Filter_Criteria_AccessType_Add_Spacing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.List.Filter.Criteria]
                    SET
                    ListValues = 'Business,Customer Portal,ECommerce,Vendor Portal'
                    WHERE [Name] = 'Application Type'
                    AND TargetClassTypeID = 1021
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.List.Filter.Criteria]
                    SET
                    ListValues = 'Business,CustomerPortal,ECommerce,VendorPortal'
                    WHERE [Name] = 'Application Type'
                    AND TargetClassTypeID = 1021
                "
            );
        }
    }
}

