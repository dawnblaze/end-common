﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END7812_Correct_Adjustment_Surcharge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Accounting.GL.Account]
SET Name = N'TO DELETE'
WHERE ID = 4800 AND Name = N'Adjustment Income'
;

INSERT [Accounting.GL.Account] ([BID], [ID], [ModifiedDT], [IsActive], [CanEdit], [Name], [Number], [GLAccountType], [ParentID], [ExportAccountName], [ExportAccountNumber], [ExportGLAccountType])
VALUES (-1, 4100, '2018-01-01', 1, 0, N'Adjustment Income', 4000, 40, NULL, NULL, NULL, 40)
;

exec [Util.Table.CopyDefaultRecords] 'Accounting.GL.Account';

UPDATE [Part.Surcharge.Data]
SET IncomeAccountID = 4100
WHERE ID = 1 AND IncomeAccountID = 4800
;

DELETE FROM [Accounting.GL.Account] 
WHERE ID = 4800 AND Name = N'TO DELETE'
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
