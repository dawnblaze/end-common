using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6285_ClearExistingsBoard : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [Board.Module.Link]", true);
            migrationBuilder.Sql("DELETE FROM [Board.Definition.Data]", true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
