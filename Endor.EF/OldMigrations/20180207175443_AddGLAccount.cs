using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180207175443_AddGLAccount")]
    public partial class AddGLAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "enum.Accounting.GLAccountType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    ParentType = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Accounting.GLAccountType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Accounting.GL.Account",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    CanEdit = table.Column<bool>(nullable: true, computedColumnSql: "((1))"),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((8000))"),
                    ExportAccountName = table.Column<string>(maxLength: 255, nullable: true),
                    ExportAccountNumber = table.Column<string>(maxLength: 255, nullable: true),
                    ExportGLAccountType = table.Column<byte>(nullable: true),
                    GLAccountType = table.Column<byte>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false, computedColumnSql: "((1))"),
                    IsAsset = table.Column<bool>(nullable: true, computedColumnSql: "(case when [GLAccountType]>=(10) AND [GLAccountType]<=(19) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsCOGS = table.Column<bool>(nullable: true),
                    IsEquity = table.Column<bool>(nullable: true, computedColumnSql: "(case when [GLAccountType]>=(30) AND [GLAccountType]<=(39) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsExpense = table.Column<bool>(nullable: true, computedColumnSql: "(case when [GLAccountType]>=(60) AND [GLAccountType]<=(69) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsIncome = table.Column<bool>(nullable: true, computedColumnSql: "(case when [GLAccountType]>=(50) AND [GLAccountType]<=(59) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsLiability = table.Column<bool>(nullable: true, computedColumnSql: "(case when [GLAccountType]>=(20) AND [GLAccountType]<=(29) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Number = table.Column<int>(nullable: true),
                    NumberedName = table.Column<string>(maxLength: 255, nullable: true, computedColumnSql: "(concat(CONVERT([varchar](6),[Number])+' ',[Name]))"),
                    ParentID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.GL.Account", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.GL.Account_enum.Accounting.GLAccountType_ExportGLAccountType",
                        column: x => x.ExportGLAccountType,
                        principalTable: "enum.Accounting.GLAccountType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.GL.Account_enum.Accounting.GLAccountType_GLAccountType",
                        column: x => x.GLAccountType,
                        principalTable: "enum.Accounting.GLAccountType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounting.GL.Account_Accounting.GL.Account_BID_ParentID",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_ExportGLAccountType",
                table: "Accounting.GL.Account",
                column: "ExportGLAccountType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_GLAccountType",
                table: "Accounting.GL.Account",
                column: "GLAccountType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Account_BID_ParentID",
                table: "Accounting.GL.Account",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.GL.Account.SimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.GL.Account.SimpleList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Accounting.GL.Account.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.GL.Account]
GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.GL.Account.SimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.GL.Account.SimpleList]
");

            migrationBuilder.DropTable(
                name: "Accounting.GL.Account");


            migrationBuilder.DropTable(
                name: "enum.Accounting.GLAccountType");
        }
    }
}

