using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180615133555_RepairEnumCustomFieldInputTypeTable")]
    public partial class RepairEnumCustomFieldInputTypeTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [enum.CustomField.InputType]
WHERE ID NOT IN (1,2,3,4,11,21,31,32,39,71,81,91)
;

INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT  1, N'Unformatted Single Line' , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  1);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT  2, N'Unformatted Memo'        , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  2);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT  3, N'Formatted Single Line'   , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  3);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT  4, N'Formatted Memo'          , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  4);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 11, N'Integer'                 , 1 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 11);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 21, N'Number'                  , 2 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 21);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 31, N'Checkbox'                , 3 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 31);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 32, N'Toggle'                  , 3 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 32);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 39, N'Custom Boolean Option'   , 3 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 39);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 71, N'Date Picker'             , 7 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 71);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 81, N'Time Picker'             , 8 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 81);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 91, N'DateTime Picker'         , 9 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 91);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [enum.CustomField.InputType]
WHERE ID NOT IN (5,6,7,12,19,22,23,24,25,29,39,71,81,91,101,102,103,111,112)
;

INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT  5, N'Phone (US&CA)'         , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =   5);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT  6, N'Email'                 , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =   6);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT  7, N'URL'                   , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =   7);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 12, N'Positive Integers'     , 1 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  12);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 19, N'Custom Integer Range'  , 1 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  19);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 22, N'Positive Numbers'      , 2 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  22);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 23, N'Numbers to 1 Decimal'  , 2 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  23);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 24, N'Numbers to 2 Decimals' , 2 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  24);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 25, N'Numbers to 4 Decimals' , 2 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  25);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 29, N'Custom Number Range'   , 2 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  29);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 39, N'Custom Boolean Option' , 3 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  39);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 71, N'Date Picker'           , 7 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  71);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 81, N'Time Picker'           , 8 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  81);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 91, N'DateTime Picker'       , 9 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID =  91);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 101, N'Phone (UK)'           , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 101);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 102, N'Phone (AU Landline)'  , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 102);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 103, N'Phone (AU Mobile)'    , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 103);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 111, N'Facebook'             , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 111);
INSERT INTO [enum.CustomField.InputType] (ID, Name, DataType) SELECT 112, N'Twitter'              , 0 WHERE NOT EXISTS(SELECT 1 FROM [enum.CustomField.InputType] WHERE ID = 112);            ");
        }
    }
}

