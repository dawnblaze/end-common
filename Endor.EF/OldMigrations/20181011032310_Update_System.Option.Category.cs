using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181011032310_Update_System.Option.Category")]
    public partial class Update_SystemOptionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.Option.Category]
                     SET Name = 'Pressaro'
                       , Description = 'Pressero Ecommerce Integration'
                       , SearchTerms = 'Integration Pressaro Pressero Ecommerce'
                     WHERE ID = 705
                "
            );

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.Option.Category]
                     SET Name = 'Pressaro'
                       , Description = 'Pressaro'
                       , SearchTerms = 'Integration Pressaro'
                     WHERE ID = 705
                "
            );

        }
    }
}

