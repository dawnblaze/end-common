﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddDefaultTaxEngineTypeValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"SET IDENTITY_INSERT [enum.Accounting.TaxEngineType] ON;");
            migrationBuilder.Sql(@"
INSERT INTO [enum.Accounting.TaxEngineType] ([ID], [Name])
VALUES(0, 'Exempt')
, (1, 'Supplied')
, (2, 'Internal')
, (3, 'TaxJar')
;
            ");
            migrationBuilder.Sql(@"SET IDENTITY_INSERT [enum.Accounting.TaxEngineType] OFF;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM[enum.Accounting.TaxEngineType] WHERE[ID] IN(0,1,2,3)");
        }
    }
}
