using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Rename_Document_To_File : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

                UPDATE [dbo].[System.Option.Section] SET [Name] = 'File Management' WHERE Name = 'Document Management'
                GO

                UPDATE [dbo].[System.Option.Category] set Name = 'File Management Options', Description = 'File Management Options' where Name = 'Document Management Options'
                GO

                UPDATE [dbo].[System.Option.Definition] set Label = 'Copy All Files to Cloned Order', 
                Description = 'If checked, File Management files from the original order will be duplicated into the folders of the cloned order. If not checked, no File Management files will be copied to the cloned order. Note: Copying Files will be in addition to any files in the template and copied files will overwrite any template files copied into the order with the same name.' 
                where Name = 'Order.Clone.CopyDocuments'
                GO

                UPDATE [dbo].[System.Option.Definition] set Label = 'Copy All Files to New Order/Estimate', 
                Description = 'If checked, File Management files from the original estimate will be duplicated into the folders of the new order/estimate. If not checked, no File Management files will be copied to the new order/estimate. Note: Copying Files will be in addition to any files in the template and copied files will overwrite any template files copied into the order/estimate with the same name.' 
                where Name = 'Estimate.Clone.CopyDocuments'
                GO

                UPDATE [dbo].[System.Option.Definition] set Label = 'File Management Enabled', Description = 'File Management Enabled' where Name = 'DM.Enabled'
                GO

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
