using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CreateAssociationTypeMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.AssociationType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.AssociationType", x => x.ID);
                });

                migrationBuilder.Sql(@"
                    INSERT [enum.AssociationType] ([ID], [Name])
                        VALUES (0, N'Independent')
                        , (1, N'Fastsigns')
                        , (2, N'Alliance Franchise Brands')
                        , (3, N'SignWorld')
                        , (4, N'Sign*A*Rama')
                        , (5, N'Speedpro US')
                        , (6, N'Speedpro CA')
                        , (11, N'AlphaGraphics')
                    ;            
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.AssociationType");
        }
    }
}
