using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180321183152_END-693_UpdateIndustryCanDeleteStoredProcedure")]
    public partial class END693_UpdateIndustryCanDeleteStoredProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Industry.Action.CanDelete]
--
-- Description: This procedure checks if the Industry is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Industry.Action.CanDelete] @BID=1, @ID=1
-- ========================================================
ALTER PROCEDURE [dbo].[Industry.Action.CanDelete]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ID       INT     -- = 2

        , @Result         BIT     = NULL  OUTPUT
		, @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Industry specified is valid

	 IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @ID)
		SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Invalid Industry Specified. IndustryID=', @ID, ' not found')

	ELSE IF EXISTS(SELECT * FROM [Company.Data] WHERE IndustryID = @ID)
        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Industry exist in Company.Data table. IndustryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@" DECLARE @Message VARCHAR(1024);

    -- Check if the Industry specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @IndustryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Industry Specified. IndustryID='+CONVERT(VARCHAR(12),@IndustryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    SET @Result = 
    CASE WHEN 
        NOT EXISTS(SELECT * FROM [Company.Data] WHERE IndustryID = @IndustryID)
            --AND NOT EXISTS(SELECT * FROM [Order.Data] WHERE IndustryID = @IndustryID)
        THEN 1 
        ELSE 0 
    END;

    SELECT @Result as Result;");
        }
    }
}

