using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180507142713_InsertOrderFilter")]
    public partial class InsertOrderFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                           ([TargetClassTypeID]
                           ,[Name]
                           ,[Label]
                           ,[Field]
                           ,[IsHidden]
                           ,[DataType]
                           ,[InputType]
                           ,[AllowMultiple]
                           ,[ListValues]
                           ,[ListValuesEndpoint]
                           ,[IsLimitToList]
                           ,[SortIndex])
                     VALUES
                           (10000 --<TargetClassTypeID, int,>
                           ,'OrderNumber' --<Name, varchar(255),>
                           ,'Order/Invoice #' --<Label, varchar(255),>
                           ,'OrderNumber' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,0 --<DataType, tinyint,>
                           ,0 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,0) --<SortIndex, tinyint,>)

                           ,(10000 --<TargetClassTypeID, int,>
                           ,'OrderStatus' --<Name, varchar(255),>
                           ,'Order Status' --<Label, varchar(255),>
                           ,'OrderStatus' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,1 --<DataType, tinyint,>
                           ,7 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,1) --<SortIndex, tinyint,>)

                           ,(10000 --<TargetClassTypeID, int,>
                           ,'Employees' --<Name, varchar(255),>
                           ,'Employees' --<Label, varchar(255),>
                           ,'Employees' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,1 --<DataType, tinyint,>
                           ,3 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,2) --<SortIndex, tinyint,>)

                           ,(10000 --<TargetClassTypeID, int,>
                           ,'Company' --<Name, varchar(255),>
                           ,'Company' --<Label, varchar(255),>
                           ,'Company' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,1 --<DataType, tinyint,>
                           ,8 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,3) --<SortIndex, tinyint,>)

                           ,(10000 --<TargetClassTypeID, int,>
                           ,'Contact' --<Name, varchar(255),>
                           ,'Contact' --<Label, varchar(255),>
                           ,'Contact' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,1 --<DataType, tinyint,>
                           ,6 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,0 --<IsLimitToList, bit,>
                           ,4) --<SortIndex, tinyint,>)
                            
                           ,(10000 --<TargetClassTypeID, int,>
                           ,'ShowVoidedOrders' --<Name, varchar(255),>
                           ,'Show Voided Orders' --<Label, varchar(255),>
                           ,'ShowVoidedOrders' --<Field, varchar(255),>
                           ,0 --<IsHidden, bit,>
                           ,3 --<DataType, tinyint,>
                           ,2 --<InputType, tinyint,>
                           ,0 --<AllowMultiple, bit,>
                           ,NULL --<ListValues, varchar(max),>
                           ,NULL --<ListValuesEndpoint, varchar(255),>
                           ,1 --<IsLimitToList, bit,>
                           ,5) --<SortIndex, tinyint,>)
                GO

                INSERT INTO [dbo].[List.Filter]
                           ([BID]
                           ,[ID]
                           ,[CreatedDate]
                           ,[ModifiedDT]
                           ,[IsActive]
                           ,[Name]
                           ,[TargetClassTypeID]
                           ,[IDs]
                           ,[Criteria]
                           ,[OwnerID]
                           ,[IsPublic]
                           ,[IsSystem]
                           ,[Hint]
                           ,[IsDefault]
                           ,[SortIndex])
                     VALUES
                           (1 --<BID, smallint,>
                           ,1029 --<ID, int,>
                           ,GETUTCDATE() -- <CreatedDate, date,>
                           ,GETUTCDATE() --<ModifiedDT, datetime2(2),>
                           ,1 --<IsActive, bit,>
                           ,'Active' --<Name, varchar(255),>
                           ,10000 --<TargetClassTypeID, int,>
                           ,NULL --<IDs, xml,>
                           ,'<ArrayOfListFilterItem>
			                  <ListFilterItem>
				                <SearchValue>true</SearchValue>
				                <Field>ShowVoidedOrders</Field>
				                <IsHidden>false</IsHidden>
				                <IsSystem>true</IsSystem>
				                <DisplayText>Show Voided</DisplayText>
			                  </ListFilterItem>
			                </ArrayOfListFilterItem>'
                           ,NULL --<OwnerID, smallint,>
                           ,0 --<IsPublic, bit,>
                           ,1 --<IsSystem, bit,>
                           ,NULL --<Hint, varchar(max),>
                           ,1 --<IsDefault, bit,>
                           ,0) --<SortIndex, tinyint,>)
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE [TargetClassTypeID]=10000 AND ([FIELD] IN ('OrderNumber','OrderStatus','Employees','Company','Contact','ShowVoidedOrders'))
                GO
                DELETE FROM [dbo].[List.Filter] WHERE ID=1029
                GO
            ");
        }
    }
}

