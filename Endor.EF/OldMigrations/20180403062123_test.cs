using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180403062123_test")]
    public partial class test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Migration code deleted due to change in view being a non-change
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // Migration code deleted due to change in view being a non-change
        }
    }
}

