using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180619002442_DeleteDeprecatedCustomTables")]
    public partial class DeleteDeprecatedCustomTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "_Root.Custom");

            migrationBuilder.DropTable(
                name: "Business.Custom");

            migrationBuilder.DropTable(
                name: "Campaign.Custom");

            migrationBuilder.DropTable(
                name: "Company.Custom");

            migrationBuilder.DropTable(
                name: "Contact.Custom");

            migrationBuilder.DropTable(
                name: "Employee.Custom");

            migrationBuilder.DropTable(
                name: "Opportunity.Custom");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "_Root.Custom",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    BID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Root.Custom", x => new { x.ID, x.BID });
                });

            migrationBuilder.CreateTable(
                name: "Business.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business.Custom", x => x.BID);
                });

            migrationBuilder.CreateTable(
                name: "Campaign.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9101))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaign.Custom", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Campaign.Custom_Campaign.Data",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Campaign.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Company.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company.Custom", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Company.Custom.Data_Company",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Contact.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((3001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact.Custom", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Contact.Custom_Contact.Data",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employee.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((5001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.Custom", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Opportunity.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opportunity.Custom", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Opportunity.Custom_Opportunity.Data",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Opportunity.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });
        }
    }
}

