using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180726114211_AddEstimateVariationsLabel")]
    public partial class AddEstimateVariationsLabel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES (6099, N'Estimate.Variations.Label', N'Label for ""Variations"" on Printed Estimates', N'Label for ""Variations"" on Printed Estimates', 0, 14, N'', N'Option', 0);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [System.Option.Definition] WHERE Name in (
                  N'Estimate.Variations.Label'                
                );
            ");

        }
    }
}

