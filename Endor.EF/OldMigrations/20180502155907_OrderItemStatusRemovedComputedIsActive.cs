using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180502155907_OrderItemStatusRemovedComputedIsActive")]
    public partial class OrderItemStatusRemovedComputedIsActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsSystem",
                table: "Order.Item.Status",
                nullable: false,
                oldClrType: typeof(bool),
                oldComputedColumnSql: "(isnull(CONVERT([bit],(1)),(1)))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDefault",
                table: "Order.Item.Status",
                nullable: false,
                oldClrType: typeof(bool),
                oldComputedColumnSql: "(isnull(CONVERT([bit],(1)),(1)))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Order.Item.Status",
                nullable: false,
                oldClrType: typeof(bool),
                oldComputedColumnSql: "(isnull(CONVERT([bit],(1)),(1)))");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsSystem",
                table: "Order.Item.Status",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([bit],(1)),(1)))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsDefault",
                table: "Order.Item.Status",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([bit],(1)),(1)))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Order.Item.Status",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([bit],(1)),(1)))",
                oldClrType: typeof(bool));
        }
    }
}

