using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180502041303_CREATE_INDEX_Order.Item.Status")]
    public partial class CREATE_INDEX_OrderItemStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                CREATE UNIQUE INDEX [IX_Order.Item.Status_Name] ON [Order.Item.Status] ( [BID], [OrderStatusID], [Name] );
                CREATE INDEX [IX_Order.Item.Status_OrderStatus] ON [Order.Item.Status] ( [BID], [OrderStatusID], [StatusIndex], [Name] );
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"");
        }
    }
}

