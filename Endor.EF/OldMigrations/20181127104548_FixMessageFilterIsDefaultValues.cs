using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixMessageFilterIsDefaultValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [dbo].[List.Filter]
                    SET [IsDefault] = 0
                WHERE TargetClassTypeID = 14109 AND Name = 'Sent';

                UPDATE [dbo].[List.Filter]
                    SET [IsDefault] = 0
                WHERE TargetClassTypeID = 14109 AND Name = 'Deleted';
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[List.Filter] TargetClassTypeID = 14109 AND Name = 'Sent';

                DELETE FROM [dbo].[List.Filter] TargetClassTypeID = 14109 AND Name = 'Deleted';
            ");
        }
    }
}
