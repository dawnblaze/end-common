using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180209161945_InsertGLAccountOptionRecords")]
    public partial class InsertGLAccountOptionRecords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"  
if not exists (SELECT ID from [System.Option.Category] where ID = 9 and name = 'Chart Of Accounts')
	INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
	VALUES (9, 'Chart Of Accounts', 1, 'GLAccount Options', 4, 0)

if not exists (SELECT ID from [System.Option.Definition] where ID = 39 and name = 'Accounting.GLAccount.ShowNumberedNames')
  INSERT INTO [System.Option.Definition] (ID, Name, Label, Description, DataType, CategoryID, ListValues, DefaultValue, IsHidden)
  VALUES (39, 'Accounting.GLAccount.ShowNumberedNames', 'Use Numbered Accounts', NULL, 3, 9, NULL, 0, 0)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Definition] where ID = 39 and name = 'Accounting.GLAccount.ShowNumberedNames'

DELETE FROM [System.Option.Category] where ID = 9 and name = 'Chart Of Accounts';
");

        }
    }
}

