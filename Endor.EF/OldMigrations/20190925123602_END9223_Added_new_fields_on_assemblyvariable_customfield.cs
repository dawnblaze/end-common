﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9223_Added_new_fields_on_assemblyvariable_customfield : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /**
             * Create new columns on Part.Subassembly.Variable
             */
            migrationBuilder.AlterColumn<byte>(
                name: "UnitID",
                table: "Part.Subassembly.Variable",
                type: "TINYINT SPARSE",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomUnitText",
                table: "Part.Subassembly.Variable",
                type: "NVARCHAR(255) SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "UnitType",
                table: "Part.Subassembly.Variable",
                type: "TINYINT SPARSE",
                nullable: true);

            /**
             * Create new columns on CustomField.Definition
             */
            migrationBuilder.AddColumn<string>(
                name: "CustomUnitText",
                table: "CustomField.Definition",
                type: "NVARCHAR(255) SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "UnitID",
                table: "CustomField.Definition",
                type: "TINYINT SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "UnitType",
                table: "CustomField.Definition",
                type: "TINYINT SPARSE",
                nullable: true);

            /**
             * Update the assembly ELEMENT rows with Element Type = Measuremenet to Number
             */

            migrationBuilder.Sql(
            @"
                UPDATE [dbo].[Part.Subassembly.Element]
                SET [ElementType] = 21
                WHERE [ElementType] = 22    
            ");
            /**
             * Update the assembly VARIABLE rows with Element Type = Measuremenet to Number
             */
            migrationBuilder.Sql(
            @"
                UPDATE [dbo].[Part.Subassembly.Variable]
                SET [ElementType] = 21
                WHERE [ElementType] = 22
            ");

            migrationBuilder.Sql(
            @"
                DELETE FROM [dbo].[enum.Part.Subassembly.ElementType] WHERE ID = 22
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomUnitText",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "CustomUnitText",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "UnitID",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "CustomField.Definition");

            migrationBuilder.AlterColumn<byte>(
                name: "UnitID",
                table: "Part.Subassembly.Variable",
                type: "tinyint",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "TINYINT SPARSE",
                oldNullable: true);

            migrationBuilder.Sql(
            @"
                UPDATE [dbo].[Part.Subassembly.Element]
                SET [ElementType] = 22
                WHERE [ElementType] = 21    
            ");

            migrationBuilder.Sql(
            @"
                UPDATE [dbo].[Part.Subassembly.Variable]
                SET [ElementType] = 21
                WHERE [ElementType] = 22
            ");
        }
    }
}
