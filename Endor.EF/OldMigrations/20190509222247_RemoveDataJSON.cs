using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class RemoveDataJSON : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataJSON",
                table: "Company.Custom.Data");

            migrationBuilder.DropColumn(
                name: "DataJSON",
                table: "Contact.Custom.Data");

            migrationBuilder.DropColumn(
                name: "DataJSON",
                table: "Opportunity.Custom.Data");

            migrationBuilder.DropColumn(
                name: "DataJSON",
                table: "Order.Custom.Data");

            migrationBuilder.DropColumn(
                name: "DataJSON",
                table: "CustomField.Other.Data");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
