using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CREATE_TABLE_MessageObjectLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Message.Object.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    BodyID = table.Column<int>(type: "int", nullable: false),
                    LinkedObjectClassTypeID = table.Column<int>(type: "int", nullable: false),
                    LinkedObjectID = table.Column<int>(type: "int", nullable: false),
                    IsSourceOfMessage = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    DisplayText = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),

                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Object.Link", x => new { x.BID, x.BodyID, x.LinkedObjectClassTypeID, x.LinkedObjectID });
                    table.ForeignKey(
                        name: "FK_Message.Related.Records_Message.Data",
                        columns: x => new { x.BID, x.BodyID },
                        principalTable: "Message.Body",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);

                });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Object.Link_LinkedObject",
                table: "Message.Object.Link",
                columns: new[] { "BID", "LinkedObjectClassTypeID", "LinkedObjectID" });


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Message.Related.Records_Message.Data",
                table: "Message.Object.Link");

            migrationBuilder.DropIndex(
                name: "IX_Message.Object.Link_LinkedObject",
                table: "Message.Object.Link");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Object.Link",
                table: "Message.Object.Link");

            migrationBuilder.DropTable(
                name: "Message.Object.Link");
        }
    }
}
