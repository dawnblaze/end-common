using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8862_Migration_for_OrderStatus_and_OrderItemStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) 
                    VALUES (31, N'Holding', 1, 0, 0, NULL, 0)
                        ,  (32, N'Active', 1, 0, 0, NULL, 0)
                        ,  (36, N'Approved', 1, 0, 0, 36, 0)
                        ,  (38, N'Lost', 1, 0, 0, 38, 0)
                        ,  (39, N'Voided', 1, 0, 0, 39, 0)
                    ;

                    INSERT INTO [Order.Item.Status] 
                    ([BID], [ID], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (-1, 31, 1, 1, 0, 32, 31, 50, N'Holding', NULL, NULL)
                            , (-1, 32, 1, 1, 0, 32, 32, 50, N'Active', NULL, NULL)
                            , (-1, 36, 1, 1, 0, 32, 36, 50, N'Approved', NULL, NULL)
                            , (-1, 38, 1, 1, 0, 32, 38, 50, N'Lost', NULL, NULL)
                            , (-1, 39, 1, 1, 0, 32, 39, 50, N'Voided', NULL, NULL)
                    ;
                "
            );

            migrationBuilder.Sql(
                @"
                    EXEC [Util.Table.CopyDefaultRecords] @TableName = 'Order.Item.Status'
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    DELETE FROM [enum.Order.OrderStatus] WHERE ID IN (31, 32, 36, 38, 39);
                    DELETE FROM [Order.Item.Status]  WHERE ID IN (31, 32, 36, 38, 39);
                "
            );
        }
    }
}
