using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddCurrencyType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Accounting.CurrencyType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Code = table.Column<string>(type: "VARCHAR(3)", unicode: false, maxLength: 3, nullable: false),
                    Name = table.Column<string>(type: "VARCHAR(100)", unicode: false, maxLength: 100, nullable: false),
                    Symbol = table.Column<string>(type: "NVARCHAR(1)", maxLength: 1, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Accounting.CurrencyType", x => x.ID);
                });
            migrationBuilder.Sql(@"
            INSERT [enum.Accounting.CurrencyType] ([ID], [Name], [Code],[Symbol]) VALUES (0, N'US Dollar', N'USD',N'$')
                , (1, N'Canadian Dollar', N'CAD',N'$')
                , (2, N'Australian Dollar', N'AUD',N'$')
                , (3, N'Euro', N'EUR',N'€')
                , (4, N'Pounds Sterling', N'GBP',N'£')
                , (5, N'Rand', N'ZAR',N'R')
                , (6, N'New Zealand Dollar', N'NZD',N'$')
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Accounting.CurrencyType");
        }
    }
}
