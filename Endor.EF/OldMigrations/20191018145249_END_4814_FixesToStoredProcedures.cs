﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_4814_FixesToStoredProcedures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // remove old actions
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS dbo.[Contact.Action.SetBilling];
DROP PROCEDURE IF EXISTS dbo.[Contact.Action.SetDefault];
");
            // add new action
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Contact.Action.SetRole]( @BID tinyint, @ContactID int, @CompanyID, @RoleID )
--
-- Description: For the specified Contact and Company, this 
-- this function sets the specified Role(s) on the Contact on that Company
-- and clears the roles on any other contacts for that company.
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetRole] @BID=1, @ContactID=1040, @CompanyID = 1003, @RoleID = 2
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Contact.Action.SetRole]
-- DECLARE 
          @BID            SMALLINT -- = 1
        , @ContactID      INT     -- = 1040
        , @CompanyID      INT     = NULL 
        , @RoleID         TINYINT 
        , @Result         INT     = NULL  -- OUTPUT
AS
BEGIN

    DECLARE @Message VARCHAR(1024);

    -- See how many companies are linked to this contact.  If no company is specified, will count all
    DECLARE @CompanyCount INT = 
                    (   SELECT COUNT(*)
                        FROM [Company.Contact.Link] 
                        WHERE BID = @BID 
                            AND CompanyID = COALESCE(@CompanyID, CompanyID)
                            AND ContactID = @ContactID );

    -- Check if the Company is specified is valid, and is associated with the Contact
    IF (@CompanyID IS NOT NULL) AND (@CompanyCount = 0)
    BEGIN
        SELECT @Result = 0
             , @Message = CONCAT('Company or Contact do not exist or are Not Associated with the each other. CompanyID=',@CompanyID, ' ContactID=', @ContactID)
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Company is not specified and there is more than one contact
    IF (@CompanyID IS NULL) AND (@CompanyCount <> 1)
    BEGIN
        SELECT @Result = 0
             , @Message = CONCAT('The Specified Contact does not exist or is associated with multiple Companies.  You must Specify a company when Company when setting the billing contact. ContactID=',@ContactID)
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Load the Company if not specified (since there can only be one at this point if not specified)
    IF (@CompanyID IS NULL) 
        SELECT @CompanyID = CompanyID 
        FROM [Company.Contact.Link] 
        WHERE BID = @BID 
          AND ContactID = @ContactID        
        ;

    -- Set IsPrimary from the appropriate contact
    --      Clear the Observer Role (128) if assigned
    --      Set the new Role
    UPDATE CL
    SET Roles = (CASE 
                    WHEN ContactID = @ContactID 
                    THEN ((Roles & (255-128)) | @RoleID)
                    ELSE (CASE 
                            WHEN Roles = @RoleID
                            THEN 128 
                            ELSE (Roles & (255-@RoleID)) 
                          END) 
                 END)
    FROM [Company.Contact.Link] CL
    WHERE BID = @BID 
        AND CompanyID = @CompanyID
    ;

    -- Update ModifiedDT on Contacts for that Company
    UPDATE Con
    SET ModifiedDT = GETUTCDATE()
    FROM [Company.Contact.Link] CL
    JOIN [Contact.Data] Con on CL.BID = Con.BID AND CL.ContactID = Con.ID
    WHERE CL.BID = @BID 
        AND CL.CompanyID = @CompanyID
    ;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END;
");
            // fix Order.Action.AddContactRole
            migrationBuilder.Sql(@"
-- ========================================================
/* 
     SPROC: [Order.Action.AddContactRole] @BID tinyint, @OrderID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255)
     Description: This function adds a order contact role

     Sample Use:   

        SELECT * FROM [Order.Contact.Role] WHERE BID =1 AND OrderID = 1100
        EXEC dbo.[Order.Action.AddContactRole] @BID=1, @OrderID=1100, @contactRoleType=2, @contactID=1001
        SELECT * FROM [Order.Contact.Role] WHERE BID =1 AND OrderID = 1100

*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.AddContactRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024) = NULL
          , @CompanyID INT
          ;

    SELECT @CompanyID = CompanyID 
    FROM [Order.Data] O 
    WHERE O.BID = @BID AND O.ID = @OrderID
    ;

    -- Check if the Order specified is valid
    IF (@ContactID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'Contact Not Specified.');

    ELSE IF (@CompanyID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'Invalid Order Specified. OrderID=',@OrderID,' not found or Company not Set; ');


    ELSE IF NOT EXISTS( SELECT * FROM [Company.Contact.Link] CL WHERE CL.BID = @BID AND CL.CompanyID = @CompanyID AND CL.ContactID = @ContactID )
        SET @Message = CONCAT('//' + @Message, 'Invalid Contact Specified or Contact is not associated with the Order Company.');

    IF (@Message IS NOT NULL)
    BEGIN
        SET @Result = 0;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @NewID INT;
    EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10013, 1;

    INSERT INTO [Order.Contact.Role] (BID, ID, OrderID, RoleType, IsAdHoc, ContactID, ContactName, ContactCompany)
        SELECT @BID, @NewID, @OrderID, @ContactRoleType, 0, @ContactID
            , (SELECT ShortName FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
            , (SELECT Name FROM [Company.Data] WHERE BID = @BID AND ID = @CompanyID)
    ;

    SET @Result = @@ROWCOUNT;
END
");
            // fix Order.Action.UpdateContactRole
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: 
--
-- Description: This function updates a order contact role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateContactRole] @BID=1, @OrderID=1, @contactRoleType=1, @contactID=1
-- ========================================================
-- ========================================================
/* 
     SPROC: [Order.Action.UpdateContactRole] @BID tinyint, @OrderID int, @RoleID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255) 
     Description: This function update a order contact role

     Sample Use:   

        SELECT * FROM [Order.Contact.Role] WHERE BID =1 AND OrderID = 1100
        EXEC dbo.[Order.Action.UpdateContactRole] @BID=1, @OrderID=1100, @contactRoleType=2, @contactID=1001
        SELECT * FROM [Order.Contact.Role] WHERE BID =1 AND OrderID = 1100

*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.UpdateContactRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1
        , @RoleID          INT           = NULL 

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024) = NULL
          , @CompanyID INT
          , @ExistingID INT
          ;

    SELECT @CompanyID = CompanyID 
    FROM [Order.Data] O 
    WHERE O.BID = @BID AND O.ID = @OrderID
    ;

    -- Pull the Existing ID .... by either RoleType or RoleID
    IF (@RoleID IS NULL)
        SELECT TOP(1) @ExistingID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType

    ELSE
        SELECT TOP(1) @ExistingID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
    ;
 
    -- Check if the supplied information is valid
    IF (@ContactID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'Contact Not Specified.');

    ELSE IF (@CompanyID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'Invalid Order Specified. OrderID=',@OrderID,' not found or Company not Set; ');


    ELSE IF NOT EXISTS( SELECT * FROM [Company.Contact.Link] CL WHERE CL.BID = @BID AND CL.CompanyID = @CompanyID AND CL.ContactID = @ContactID )
        SET @Message = CONCAT('//' + @Message, 'Invalid Contact Specified or Contact is not associated with the Order Company.');

   ELSE IF (@ExistingID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'No Existing Role to Update for OrderID=',@OrderID,'; ');

    IF (@Message IS NOT NULL)
    BEGIN
        SET @Result = 0;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    UPDATE OCR
    SET    RoleType = @ContactRoleType, IsAdHoc = 0, ContactID = @ContactID
            , ContactName = (SELECT ShortName FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
            , ContactCompany = (SELECT Name FROM [Company.Data] WHERE BID = @BID AND ID = @CompanyID)
    FROM   [Order.Contact.Role] OCR 
    WHERE  OCR.BID = @BID AND OCR.ID = @ExistingID
    ;

    SET @Result = @@ROWCOUNT;
END
");
            // fix Option.GetValue
            migrationBuilder.Sql(@"
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValue]
--DECLARE 
	  @OptionID       INT          = NULL -- = 2
	, @OptionName     VARCHAR(255) = NULL -- 'GLAccount.TaxName1'
	, @AssociationID  tinyint      = NULL
	, @BID            smallint     = NULL
	, @LocationID     smallint     = NULL
	, @StoreFrontID   smallint     = NULL
	, @UserLinkID     smallint     = NULL
	, @CompanyID      int          = NULL
	, @ContactID      int          = NULL
AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
            , @Level  TINYINT     
            ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition]
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserLinkID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

        IF (@AnswerCount > 1)
            THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT TOP 1
				  @CompanyID = Company.ID
				, @LocationID = Company.LocationID
            FROM [Company.Data] Company
				JOIN [Company.Contact.Link] Link ON Link.CompanyID = @CompanyID AND Link.BID = Company.BID
				JOIN [Contact.Data] Contact ON Contact.ID = Link.ContactID AND Contact.BID = Link.BID
            WHERE Contact.ID = @ContactID AND Contact.BID = @BID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID AND Company.BID = @BID
            ;

        IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
				JOIN [User.Link] UL on E.ID = UL.EmployeeID
            WHERE UL.ID = @UserLinkID AND UL.BID = @BID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @UserLinkID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @UserLinkID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;

    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    System=1
    --    Association=1
    --    Business=2
    --    Location=4
    --    Storefront=8
    --    Employee=16
    --    Company=32
    --    Contact=64

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (UserLinkID   IS NULL)
			AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, CompanyID, ContactID) IS NULL
                )
        ORDER BY OptionLevel DESC


    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (ContactID    IS NULL)
			AND (UserLinkID   IS NULL)
			AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, CompanyID) IS NULL
        )
        ORDER BY OptionLevel DESC

    ELSE IF (@UserLinkID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (CompanyID    IS NULL)
			AND (UserLinkID   = @UserLinkID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, UserLinkID) IS NULL
                )
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (CompanyID    IS NULL )
			AND (UserLinkID   IS NULL )
			AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID) IS NULL
                )
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (CompanyID    IS NULL )
			AND (UserLinkID   IS NULL )
			AND (StoreFrontID IS NULL )
			AND (LocationID   = @LocationID
				OR LocationID IS NULL
				)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (CompanyID    IS NULL )
			AND (UserLinkID   IS NULL )
			AND (StoreFrontID IS NULL )
			AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
			AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
                , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    SELECT 
        CAST (@OptionID AS SMALLINT) AS ID -- needed in the EF
        ,@Result as [Value]
        , @Level as [OptionLevel];
END
");
            // fix Option.GetValues
            migrationBuilder.Sql(@"
/* ========================================================
Name: [Option.GetValues]

Description: This procedure looks up a option/setting based on a hierarchy of possibilities.

Sample Use:   EXEC dbo.[Option.GetValues] @CategoryID = 300, @BID=2, @LocationID = 1
Returns: Table with columns
	[OptionID] INT
	[Name] VARCHAR(255),
	[Value] VARCHAR(255),
	[Level] TINYINT,
	[Label] VARCHAR(255),
	[Description] VARCHAR(MAX),
	[DataType] SMALLINT,
	[CategoryID] SMALLINT,
	[ListValues] VARCHAR(MAX)
	[IsHidden] BIT

	OPTION LEVELS
		System=0
		Association=1
		Business=2
		Location=4
		Storefront=8
		Employee=16
		Company=32
		Contact=64
======================================================== */
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValues]
-- DECLARE
	  @CategoryID     smallint     = NULL
	, @AssociationID  tinyint      = NULL
	, @BID            smallint     = NULL
	, @LocationID     smallint     = NULL
	, @StorefrontID   smallint     = NULL
	, @UserLinkID	  smallint     = NULL
	, @CompanyID      int          = NULL
	, @ContactID      int          = NULL
AS

BEGIN
    IF (@BID IS NULL) AND COALESCE(@ContactID, @CompanyID, @UserLinkID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @UserLinkID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;

    IF COALESCE(@BID, @AssociationID) IS NULL
    BEGIN;
        THROW 180000, 'You must supply at least one of the BID or the AssociationID' , 1;
        RETURN;
    END;

    -- Define the Results
    DECLARE @Result TABLE (   [OptionID] INT PRIMARY KEY
                            , [Name] VARCHAR(255)
                            , [Value] NVARCHAR(255) INDEX IX_Result_Value WHERE [Value] IS NULL
                            , [Level] TINYINT
                            , [Label] VARCHAR(255)
                            , [Description] VARCHAR(MAX)
                            , [DataType] SMALLINT
                            , [CategoryID] SMALLINT
                            , [ListValues] VARCHAR(MAX)
                            , [IsHidden] BIT
                            );

    -- Define a list of Categories to JOIN to get the results
    DECLARE @Categories TABLE( ID SMALLINT PRIMARY KEY  );
    IF (@CategoryID IS NULL)
        INSERT INTO @Categories SELECT ID FROM [System.Option.Category]
    ELSE
        INSERT INTO @Categories SELECT @CategoryID
    ;

    -- Now Fill the List with Option Definitions
    INSERT INTO @Result 
        SELECT DEF.[ID] as [OptionID]
            , DEF.[Name]
            , DEF.[DefaultValue] AS [Value] 
            , 0 AS [Level]
            , DEF.[Label]
            , DEF.Description
            , DEF.DataType
            , CAT.ID as CategoryID
            , DEF.ListValues
            , DEF.IsHidden

        FROM @Categories CAT
        JOIN [System.Option.Definition] DEF ON DEF.CategoryID = CAT.ID
    ;

    -- Lookup values in the option hiearchy 
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL)
            SELECT TOP 1
				  @CompanyID = Company.ID
                , @LocationID = Company.LocationID
            FROM [Company.Data] Company
				JOIN [Company.Contact.Link] Link ON Link.CompanyID = @CompanyID AND Link.BID = Company.BID
				JOIN [Contact.Data] Contact ON Link.ContactID = Contact.ID AND Link.BID = Contact.BID
            WHERE Contact.ID = @ContactID AND Contact.BID = @BID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID AND Company.BID = @BID
            ;

        IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
				LEFT JOIN [User.Link] UL ON UL.BID = E.BID AND UL.EmployeeID = E.ID
            WHERE UL.ID = @UserLinkID AND UL.BID = @BID AND UL.EmployeeID IS NOT NULL
            ;
    END;

    -- Not look up option value based on the specific instance
    IF (@ContactID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.UserLinkID   IS NULL)
            AND (OPT.ContactID    = @ContactID
                    OR OPT.CompanyID    = @CompanyID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ; 

    IF (@CompanyID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.UserLinkID   IS NULL)
            AND (OPT.ContactID    IS NULL)
            AND (OPT.CompanyID    = @CompanyID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@UserLinkID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL)
            AND (OPT.UserLinkID   = @UserLinkID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@StorefrontID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@LocationID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID IS NULL )
            AND (OPT.LocationID   = @LocationID)
    ;

    IF (@BID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID IS NULL )
            AND (OPT.LocationID   IS NULL )
    ;

    IF @AssociationID IS NOT NULL
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
			JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID
        WHERE RES.[Level] = 0
            AND (OPT.AssociationID = @AssociationID)
    ;

    -- Now return what you found!
    SELECT *
    FROM @Result;
END
");
            // fix Option.DeleteValue
            migrationBuilder.Sql(@"
/******************************************************** 
Name: [Option.DeleteValue]

Description: This procedure deletes the value for an option/setting.

Sample Use:   
EXEC dbo.[Option.DeleteValue] 
    -- Require Fields
        @OptionName       = 'GLAccount.TaxName2'
    , @OptionID         = NULL

    -- One (and only one) of these is required
    , @AssociationID    = NULL
    , @BID              = 2          -- Required (except for dev or franchise use)

    -- One (ad only one) of the following can be supplied when BID is also supplied
    -- BID must be supplied if these are used.
    , @LocationID       = NULL
    , @UserLinkID       = 1
    , @CompanyID        = NULL
    , @ContactID        = NULL
    , @StorefrontID     = NULL

SELECT * FROM [Option.Data]
********************************************************/
CREATE OR ALTER PROCEDURE [dbo].[Option.DeleteValue]
-- DECLARE 
	  @OptionID       INT          = NULL
	, @OptionName     VARCHAR(255) = NULL
	, @AssociationID  TINYINT      = NULL
	, @BID            SMALLINT     = NULL
	, @LocationID     SMALLINT     = NULL
	, @UserLinkID     SMALLINT     = NULL
	, @CompanyID      INT          = NULL
	, @ContactID      INT          = NULL
	, @StorefrontID   SMALLINT	   = NULL

AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
            , @Level  TINYINT     
            ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL)
            SELECT TOP 1
				  @CompanyID = Company.ID
                , @LocationID = Company.LocationID
            FROM [Company.Data] Company
				JOIN [Company.Contact.Link] Link ON Link.CompanyID = @CompanyID AND Link.BID = Company.BID
				JOIN [Contact.Data] Contact ON Link.ContactID = Contact.ID AND Link.BID = Contact.BID
            WHERE Contact.ID = @ContactID AND Contact.BID = @BID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID AND Company.BID = @BID
            ;

        IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
				LEFT JOIN [User.Link] UL on UL.BID = E.BID AND E.ID = UL.EmployeeID
            WHERE UL.ID = @UserLinkID AND UL.BID = @BID AND UL.EmployeeID IS NOT NULL
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @UserLinkID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @UserLinkID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;

    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (UserLinkID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC


    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (UserLinkID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@UserLinkID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (UserLinkID   = @UserLinkID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
                , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    DELETE FROM [Option.Data] 
    WHERE Value = @Result 
        AND OptionLevel = @Level 
        AND OptionID = @OptionID
    ;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // reset Option.GetValue
            migrationBuilder.Sql(@"
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValue]
--DECLARE 
      @OptionID       INT          = NULL -- = 2
    , @OptionName     VARCHAR(255) = NULL -- 'GLAccount.TaxName1'

    , @AssociationID  tinyint      = NULL
    , @BID            smallint     = NULL
    , @LocationID     smallint     = NULL
    , @StoreFrontID   smallint     = NULL
    , @UserLinkID     smallint     = NULL
    , @CompanyID      int          = NULL
    , @ContactID      int          = NULL
AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
            , @Level  TINYINT     
            ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition]
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserLinkID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

        IF (@AnswerCount > 1)
            THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                    , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            JOIN [User.Link] UL on E.ID = UL.EmployeeID
            WHERE UL.ID = @UserLinkID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @UserLinkID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @UserLinkID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    System=1
    --    Association=1
    --    Business=2
    --    Location=4
    --    Storefront=8
    --    Employee=16
    --    Company=32
    --    Contact=64

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (UserLinkID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, CompanyID, ContactID) IS NULL
                )
        ORDER BY OptionLevel DESC


    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (UserLinkID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, CompanyID) IS NULL
        )
        ORDER BY OptionLevel DESC

    ELSE IF (@UserLinkID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (UserLinkID   = @UserLinkID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, UserLinkID) IS NULL
                )
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID) IS NULL
                )
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID
                OR LocationID IS NULL
            )
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
                , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    SELECT 
        CAST (@OptionID AS SMALLINT) AS ID -- needed in the EF
        ,@Result as [Value]
        , @Level as [OptionLevel];
END;
");
            // reset Option.GetValues
            migrationBuilder.Sql(@"
-- ========================================================
-- 
-- Name: [Option.GetValues]
--
-- Description: This procedure looks up a option/setting based on a hierarchy of possibilities.
-- 
--
-- Sample Use:   EXEC dbo.[Option.GetValues] @CategoryID = 300, @BID=2, @LocationID = 1
-- Returns: Table with columns
    -- [OptionID] INT
    -- [Name] VARCHAR(255),
    -- [Value] VARCHAR(255),
    -- [Level] TINYINT,
    -- [Label] VARCHAR(255),
    -- [Description] VARCHAR(MAX),
    -- [DataType] SMALLINT,
    -- [CategoryID] SMALLINT,
    -- [ListValues] VARCHAR(MAX)
    -- [IsHidden] BIT

    -- OPTION LEVELS
    --    System=0
    --    Association=1
    --    Business=2
    --    Location=4
    --    Storefront=8
    --    Employee=16
    --    Company=32
    --    Contact=64
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValues]
-- DECLARE
      @CategoryID     smallint     = NULL
    , @AssociationID  tinyint      = NULL
    , @BID            smallint     = NULL
    , @LocationID     smallint     = NULL
    , @StorefrontID   smallint     = NULL
    , @UserLinkID	  smallint     = NULL
    , @CompanyID      int          = NULL
    , @ContactID      int          = NULL
AS

BEGIN
    IF (@BID IS NULL) AND COALESCE(@ContactID, @CompanyID, @UserLinkID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @UserLinkID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;

    IF COALESCE(@BID, @AssociationID) IS NULL
    BEGIN;
        THROW 180000, 'You must supply at least one of the BID or the AssociationID' , 1;
        RETURN;
    END;

    -- Define the Results
    DECLARE @Result TABLE (   [OptionID] INT PRIMARY KEY
                            , [Name] VARCHAR(255)
                            , [Value] NVARCHAR(255) INDEX IX_Result_Value WHERE [Value] IS NULL
                            , [Level] TINYINT
                            , [Label] VARCHAR(255)
                            , [Description] VARCHAR(MAX)
                            , [DataType] SMALLINT
                            , [CategoryID] SMALLINT
                            , [ListValues] VARCHAR(MAX)
                            , [IsHidden] BIT
                            );

    -- Define a list of Categories to JOIN to get the results
    DECLARE @Categories TABLE( ID SMALLINT PRIMARY KEY  );
    IF (@CategoryID IS NULL)
        INSERT INTO @Categories SELECT ID FROM [System.Option.Category]
    ELSE
        INSERT INTO @Categories SELECT @CategoryID
    ;

    -- Now Fill the List with Option Definitions
    INSERT INTO @Result 
        SELECT DEF.[ID] as [OptionID]
            , DEF.[Name]
            , DEF.[DefaultValue] AS [Value] 
            , 0 AS [Level]
            , DEF.[Label]
            , DEF.Description
            , DEF.DataType
            , CAT.ID as CategoryID
            , DEF.ListValues
            , DEF.IsHidden

        FROM @Categories CAT
        JOIN [System.Option.Definition] DEF ON DEF.CategoryID = CAT.ID
    ;

    -- Lookup values in the option hiearchy 
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                    , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            LEFT JOIN [User.Link] UL ON UL.BID = E.BID AND UL.EmployeeID = E.ID
            WHERE UL.ID = @UserLinkID AND UL.EmployeeID IS NOT NULL
            ;
    END;

    -- Not look up option value based on the specific instance
    IF (@ContactID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.UserLinkID   IS NULL)
            AND (OPT.ContactID    = @ContactID
                    OR OPT.CompanyID    = @CompanyID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ; 

    IF (@CompanyID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.UserLinkID   IS NULL)
            AND (OPT.ContactID    IS NULL)
            AND (OPT.CompanyID    = @CompanyID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@UserLinkID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL)
            AND (OPT.UserLinkID   = @UserLinkID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@StorefrontID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@LocationID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID IS NULL )
            AND (OPT.LocationID   = @LocationID)
    ;

    IF (@BID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID IS NULL )
            AND (OPT.LocationID   IS NULL )
    ;

    IF @AssociationID IS NOT NULL
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID
        WHERE RES.[Level] = 0
            AND (OPT.AssociationID = @AssociationID)
    ;

    -- Now return what you found!
    SELECT *
    FROM @Result;
END;
");
            // reset Option.DeleteValue
            migrationBuilder.Sql(@"
/******************************************************** 
Name: [Option.DeleteValue]

Description: This procedure deletes the value for an option/setting.

Sample Use:   
EXEC dbo.[Option.DeleteValue] 
    -- Require Fields
        @OptionName       = 'GLAccount.TaxName2'
    , @OptionID         = NULL

    -- One (and only one) of these is required
    , @AssociationID    = NULL
    , @BID              = 2          -- Required (except for dev or franchise use)

    -- One (ad only one) of the following can be supplied when BID is also supplied
    -- BID must be supplied if these are used.
    , @LocationID       = NULL
    , @UserLinkID       = 1
    , @CompanyID        = NULL
    , @ContactID        = NULL
    , @StorefrontID     = NULL

SELECT * FROM [Option.Data]
********************************************************/
CREATE OR ALTER PROCEDURE [dbo].[Option.DeleteValue]
-- DECLARE 
	  @OptionID       INT          = NULL
	, @OptionName     VARCHAR(255) = NULL

	, @AssociationID  TINYINT      = NULL
	, @BID            SMALLINT     = NULL
	, @LocationID     SMALLINT     = NULL
	, @UserLinkID	  SMALLINT     = NULL
	, @CompanyID      INT          = NULL
	, @ContactID      INT          = NULL
	, @StorefrontID   SMALLINT	   = NULL

AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
            , @Level  TINYINT     
            ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                    , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            LEFT JOIN [User.Link] UL on UL.BID = E.BID AND E.ID = UL.EmployeeID
            WHERE UL.ID = @UserLinkID and UL.EmployeeID IS NOT NULL
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @UserLinkID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @UserLinkID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (UserLinkID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC


    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (UserLinkID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@UserLinkID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (UserLinkID   = @UserLinkID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (UserLinkID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
                , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    DELETE FROM [Option.Data] 
    WHERE Value = @Result 
        AND OptionLevel = @Level 
        AND OptionID = @OptionID
    ;
END;
");
            // reset Order.Action.AddContactRole
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.AddContactRole]( @BID tinyint, @OrderID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255) )
--
-- Description: This function adds a order contact role
--
-- Sample Use:   EXEC dbo.[Order.Action.AddContactRole] @BID=1, @OrderID=1, @contactRoleType=1, @contactID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.AddContactRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1
        , @ContactName     VARCHAR(255)  = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the role type does not exist on the order
    IF EXISTS(SELECT * FROM [Order.Contact.Role] WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Contact role already exist on this order.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Contact specified is valid
    IF @ContactID IS NULL 
    BEGIN
      IF ISNULL(LTRIM(@ContactName), '') = ''
      BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Name.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
      END
    END
    ELSE IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR,@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END
    ELSE IF NOT EXISTS(SELECT *
                       FROM [Contact.Data] C
                            JOIN [Order.Data] O ON O.CompanyID = C.CompanyID
                       WHERE C.BID = @BID AND C.ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. Company is not the order company'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @NewID INT;
    EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10013, 1;

    IF @ContactID IS NULL 
    BEGIN
        INSERT INTO [Order.Contact.Role]
        (BID, ID, OrderID, RoleType, IsAdHoc, ContactID, ContactName, ContactCompany)
        VALUES
        (@BID, @NewID, @OrderID, @ContactRoleType, 1, NULL, @ContactName, NULL)
    END
    ELSE
    BEGIN
        INSERT INTO [Order.Contact.Role]
        (BID, ID, OrderID, RoleType, IsAdHoc, ContactID, ContactName, ContactCompany)
        SELECT @BID, @NewID, @OrderID, @ContactRoleType, 0, @ContactID, contact.ShortName, company.Name
        FROM   [Contact.Data] contact
               LEFT JOIN [Company.Data] company ON contact.BID = company.BID AND contact.CompanyID = company.ID
        WHERE  contact.BID = @BID AND contact.ID = @ContactID
    END;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
            // reset Order.Action.UpdateContactRole
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.UpdateContactRole]( @BID tinyint, @OrderID int, @RoleID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255) )
--
-- Description: This function updates a order contact role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateContactRole] @BID=1, @OrderID=1, @contactRoleType=1, @contactID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.UpdateContactRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1
        , @RoleID          INT           = NULL 
        , @ContactName     VARCHAR(255)  = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Contact Role exists

    IF (@RoleID IS NULL)
    BEGIN
        SELECT @ID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType
    END

    ELSE
    BEGIN
        SELECT @ID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order contact role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Check if the Contact specified is valid
    IF @ContactID IS NULL 
    BEGIN
      IF ISNULL(LTRIM(@ContactName), '') = ''
      BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Name.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
      END
    END
    ELSE IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR,@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END
    ELSE IF NOT EXISTS(SELECT *
                       FROM [Contact.Data] C
                            JOIN [Order.Data] O ON O.CompanyID = C.CompanyID
                       WHERE C.BID = @BID AND C.ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. Company is not the order company'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    IF @ContactID IS NULL 
    BEGIN
        UPDATE OCR
        SET    RoleType = @ContactRoleType, IsAdHoc = 1, ContactID = NULL, ContactName = @ContactName, ContactCompany = NULL
        FROM   [Order.Contact.Role] OCR 
        WHERE  OCR.BID = @BID AND OCR.ID = @ID
    END
    ELSE
    BEGIN
        UPDATE OCR
        SET    RoleType = @ContactRoleType, IsAdHoc = 0, ContactID = @ContactID, ContactName = contact.ShortName, ContactCompany = company.Name
        FROM   [Order.Contact.Role] OCR 
               LEFT JOIN [Contact.Data] contact ON contact.BID = @BID AND contact.ID = @ContactID
               LEFT JOIN [Company.Data] company ON contact.BID = company.BID AND contact.CompanyID = company.ID
        WHERE  OCR.BID = @BID AND OCR.ID = @ID
    END;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
            // re-add Contact.Action.SetBilling
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Contact.Action.SetBilling]( @BID tinyint, @ContactID int )
--
-- Description: This function sets any existing Billing Contact
-- to false then sets the Contact (@ContactID) to Billing and active
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetBilling] @BID=1, @ContactID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Contact.Action.SetBilling]
-- DECLARE 
	  @BID            TINYINT -- = 1
	, @ContactID      INT     -- = 1000

	, @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Contact specified is valid
    IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR(12),@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @CompanyID INT, @IsCompanyAdHoc BIT;
    SET @CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID=@ContactID);
    SET @IsCompanyAdHoc = (SELECT IsAdHoc FROM [Company.Data] WHERE ID=@CompanyID);

    IF (@IsCompanyAdHoc = 1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Cannot change Billing contact for AdHoc company.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Contact.Data] WHERE CompanyID=@CompanyID);

	IF (@Count > 1)
	BEGIN
		-- Remove IsBilling from any other Billing Contact(s) for the Company
		UPDATE C
		SET
              IsBilling = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Contact.Data] C
		WHERE BID = @BID AND IsBilling = 1 AND CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID = @ContactID)
	END

    -- Now update it
    UPDATE C
    SET
          IsActive = 1
		, IsBilling = 1
        , ModifiedDT = GetUTCDate()
    FROM [Contact.Data] C
    WHERE BID = @BID AND ID = @ContactID
		AND COALESCE(IsBilling,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END;
");
            // re-add Contact.Action.SetDefault
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Contact.Action.SetDefault]( @BID tinyint, @ContactID int )
--
-- Description: This function sets any existing default Contact
-- to false then sets the Contact (@ContactID) to default and active
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetDefault] @BID=1, @ContactID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Contact.Action.SetDefault]
-- DECLARE 
	  @BID            TINYINT -- = 1
	, @ContactID      INT     -- = 2

	, @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Contact specified is valid
    IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR(12),@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @CompanyID INT, @IsCompanyAdHoc BIT;
    SET @CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID=@ContactID);
    SET @IsCompanyAdHoc = (SELECT IsAdHoc FROM [Company.Data] WHERE ID=@CompanyID);

    IF (@IsCompanyAdHoc = 1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Cannot change Default contact for AdHoc company.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Contact.Data] WHERE CompanyID=@CompanyID);

	IF (@Count > 1)
	BEGIN
		-- Remove IsDefault from any other Default Contact(s) for the Company
		UPDATE C
		SET
              IsDefault = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Contact.Data] C
		WHERE BID = @BID AND IsDefault = 1 AND CompanyID = @CompanyID
	END

    -- Now update it
    UPDATE C
    SET
          IsActive = 1
		, IsDefault = 1
        , ModifiedDT = GetUTCDate()
    FROM [Contact.Data] C
    WHERE BID = @BID AND ID = @ContactID
		AND COALESCE(IsDefault,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END;
");
            // remove Contact.Action.SetRole
            migrationBuilder.Sql(@"DROP PROCEDURE IF EXISTS dbo.[Contact.Action.SetRole];");
        }
    }
}
