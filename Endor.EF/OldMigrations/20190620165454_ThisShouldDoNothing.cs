using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class ThisShouldDoNothing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_System.Dashboard.Widget.CategoryLink",
                table: "System.Dashboard.Widget.CategoryLink");

            
            migrationBuilder.DropIndex(
                name: "IX_DashboardWidgetCategoryLink.WidgetDefinition",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropIndex(
                name: "IX_System.Dashboard.Widget.CategoryLink_CategoryType",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropIndex(
                name: "IX_DashboardWidgetCategoryLink.DashboardWidgetCategoryType",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.CreateIndex(
                table: "System.Dashboard.Widget.CategoryLink",
                name: "IX_DashboardWidgetCategoryLink.WidgetDefinition", 
                column: "WidgetDefID");

            migrationBuilder.CreateIndex(
                name: "IX_DashboardWidgetCategoryLink.DashboardWidgetCategoryType",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "CategoryType"
            );

            migrationBuilder.DropColumn(
                name: "BID",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.AddPrimaryKey(
                name: "PK_System.Dashboard.Widget.CategoryLink",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] {  "CategoryType", "WidgetDefID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_System.Dashboard.Widget.CategoryLink",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.AddColumn<short>(
                name: "BID",
                table: "System.Dashboard.Widget.CategoryLink",
                nullable: false );

            migrationBuilder.AddPrimaryKey(
                name: "PK_System.Dashboard.Widget.CategoryLink",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] { "BID", "WidgetDefID", "CategoryType" });

            migrationBuilder.CreateIndex(
                name: "IX_DashboardWidgetCategoryLink.DashboardWidgetCategoryType",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] { "BID", "CategoryType" });

            migrationBuilder.CreateIndex(
                name: "IX_DashboardWidgetCategoryLink.WidgetDefinition",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] { "BID", "WidgetDefID" });
        }
    }
}
