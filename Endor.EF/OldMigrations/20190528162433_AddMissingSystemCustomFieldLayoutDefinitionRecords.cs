using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddMissingSystemCustomFieldLayoutDefinitionRecords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [CustomField.Layout.Definition]
([BID], [ID], [IsActive], [IsSystem], [AppliesToClassTypeID], [IsAllTab], [IsSubTab], [SortIndex], [Name])
VALUES
(1, 61,  1, 1, 12000, 1, 1, 1, 'All'), -- Material
(1, 71,  1, 1, 12020, 1, 1, 1, 'All'), -- Labor
(1, 81,  1, 1, 12030, 1, 1, 1, 'All'), -- Machine
(1, 111, 1, 1, 12040, 1, 1, 1, 'All')  -- Assembly
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [CustomField.Layout.Definition] WHERE [ID] IN (61,71,81,111);
");
        }
    }
}
