using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END_7643_Add_FK_EmailAccountData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddForeignKey(
                name: "FK_Email.Account.Data_Business.Data",
                table: "Email.Account.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Email.Account.Data_Business.Data",
                table: "Email.Account.Data");
        }
    }
}
