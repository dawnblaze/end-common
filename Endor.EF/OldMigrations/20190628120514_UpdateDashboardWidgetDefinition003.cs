using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateDashboardWidgetDefinition003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.003]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Order Throughput 
    The Order Throughput Widget displays a graph or a chart of the throughput of orders in each status for the specified period.
    This Widget measures the throughput of  each status, not the measure of what is in it. 
    Each time an order leaves a status it is recorded as throughput for that status.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/990969857/Dashboard+Widget+Order+Throughput
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.003] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.003]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
    --   DECLARE @BID SMALLINT = 1;
    --   DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --   DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    --   DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    --   DECLARE @AsTable BIT = 0;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    --                   = 26 for Closed                   = 9 for Closed
    --                   = 29 for Voided                   = 2 for Voided
    
    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , OrderStatusID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );


    DECLARE @MidDate DateTime2(2) = (SELECT DATEADD(SECOND, DATEDIFF(SECOND, @StartUTCDT, @EndUTCDT )/2, @StartUTCDT));

    -- Pull ID = 0 - Monthly Goal
    INSERT INTO @Results
        SELECT @LocationID, 0, 'Sales Goal', 1, SUM(LG.Budgeted)
        FROM [Location.Goal] LG
        WHERE LG.BID = @BID AND (@LocationID = LG.LocationID OR @LocationID IS NULL)
          AND LG.Year = DatePart(Year, @MidDate)
          AND LG.Month = DatePart(Month, @MidDate)
    ;

    -- pull orders that were created in the given range
    INSERT INTO @Results
        SELECT @LocationID, 1, 'Created', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 21 -- Created
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Pre-WIP (into WIP) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 21, 'Pre-WIP', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 5 -- WIP
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH WIP (into Built) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 22, 'WIP', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 6 -- Built
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Built (into Invoicing) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 23, 'Built', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 7 -- Invoicing
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Invoicing (into Invoiced) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 24, 'Invoicing', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 8 -- Invoiced
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Invoiced (into Closed) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 25, 'Invoiced', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 7 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were voided in the current range
    INSERT INTO @Results
        SELECT @LocationID, 29, 'Voided', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 9 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;


    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------

END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.003]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Order Throughput 
    The Order Throughput Widget displays a graph or a chart of the throughput of orders in each status for the specified period.
    This Widget measures the throughput of  each status, not the measure of what is in it. 
    Each time an order leaves a status it is recorded as throughput for that status.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/990969857/Dashboard+Widget+Order+Throughput
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.003] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.003]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
    --   DECLARE @BID SMALLINT = 1;
    --   DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --   DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    --   DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    --   DECLARE @AsTable BIT = 0;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    --                   = 26 for Closed                   = 9 for Closed
    --                   = 29 for Voided                   = 2 for Voided
    
    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , OrderStatusID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );


    -- pull orders that were created in the given range
    INSERT INTO @Results
        SELECT @LocationID, 1, 'Created', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 21 -- Created
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Pre-WIP (into WIP) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 21, 'Pre-WIP', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 5 -- WIP
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH WIP (into Built) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 22, 'WIP', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 6 -- Built
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Built (into Invoicing) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 23, 'Built', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 7 -- Invoicing
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Invoicing (into Invoiced) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 24, 'Invoicing', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 8 -- Invoiced
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Invoiced (into Closed) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 25, 'Invoiced', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 7 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were voided in the current range
    INSERT INTO @Results
        SELECT @LocationID, 29, 'Voided', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 9 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;


    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------

END
");
        }
    }
}
