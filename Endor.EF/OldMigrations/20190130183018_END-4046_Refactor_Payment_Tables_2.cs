using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4046_Refactor_Payment_Tables_2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Application_Activity.GLActivity_BID_GLActivityID",
                table: "Accounting.Payment.Application");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Data_BID_TaxExemptReasonID",
                table: "Order.Item.Data",
                columns: new[] { "BID", "TaxExemptReasonID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Destination.Data_BID_TaxExemptReasonID",
                table: "Order.Destination.Data",
                columns: new[] { "BID", "TaxExemptReasonID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Application_Activity.GLActivity",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "GLActivityID" },
                principalTable: "Activity.GLActivity",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Data_TaxExemptReason",
                table: "Order.Data",
                columns: new[] { "BID", "TaxExemptReasonID" },
                principalTable: "List.FlatList.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Destination.Data_TaxExemptReason",
                table: "Order.Destination.Data",
                columns: new[] { "BID", "TaxExemptReasonID" },
                principalTable: "List.FlatList.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_TaxExemptReason",
                table: "Order.Item.Data",
                columns: new[] { "BID", "TaxExemptReasonID" },
                principalTable: "List.FlatList.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
DECLARE @BID SMALLINT = 1;

INSERT [Accounting.GL.Account] ([BID], [ID], [ModifiedDT], [IsActive], [CanEdit], [Name], [Number], [GLAccountType], [ParentID], [ExportAccountName], [ExportAccountNumber], [ExportGLAccountType])
VALUES (@BID, 6200, '2018-01-01', 1, 0, N'Bad Debt Expense', 6200, 60, NULL, NULL, NULL, 60)
;

INSERT [enum.Accounting.PaymentMethodType] ([ID], [Name])
VALUES (250, N'Refundable Credit')
    , (251, N'NonRefundable Credit')
    , (252, N'Writeoff')
;
INSERT [Accounting.Payment.Method] ([BID], [ID], [ModifiedDT], [IsActive], [Name], [PaymentMethodType], [DepositGLAccountID], [IsIntegrated], [IsCustom])
VALUES (@BID, 250, '2018-01-01', 1, N'Refundable Credit', 250, 2200, 0, 0)
     , (@BID, 251, '2018-01-01', 1, N'NonRefundable Credit', 251, 2200, 0, 0)
     , (@BID, 252, '2018-01-01', 1, N'Writeoff', 252, 6200, 0, 0)
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Application_Activity.GLActivity",
                table: "Accounting.Payment.Application");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Data_TaxExemptReason",
                table: "Order.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Destination.Data_TaxExemptReason",
                table: "Order.Destination.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Item.Data_TaxExemptReason",
                table: "Order.Item.Data");

            migrationBuilder.DropIndex(
                name: "IX_Order.Item.Data_BID_TaxExemptReasonID",
                table: "Order.Item.Data");

            migrationBuilder.DropIndex(
                name: "IX_Order.Destination.Data_BID_TaxExemptReasonID",
                table: "Order.Destination.Data");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Application_Activity.GLActivity_BID_GLActivityID",
                table: "Accounting.Payment.Application",
                columns: new[] { "BID", "GLActivityID" },
                principalTable: "Activity.GLActivity",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
