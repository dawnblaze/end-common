using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class CopyDefaultRecords_For_AccountingPaymentTerms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName = 'Accounting.Payment.Term'
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
