using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180213165138_FixContactLocatorSortIndex")]
    public partial class FixContactLocatorSortIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
    ALTER TABLE [Contact.Locator] DROP COLUMN IsPrimary;
    ALTER TABLE [Contact.Locator] ALTER COLUMN SortIndex smallint NOT NULL;
	ALTER TABLE [Contact.Locator] ADD [IsPrimary]  AS (case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}

