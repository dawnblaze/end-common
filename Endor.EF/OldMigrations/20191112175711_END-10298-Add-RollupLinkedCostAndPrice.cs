﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10298AddRollupLinkedCostAndPrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "RollupLinkedPriceAndCost",
                table: "Order.Item.Component",
                nullable: true);

            migrationBuilder.Sql(@"
UPDATE [Order.Item.Component] SET [RollupLinkedPriceAndCost] = 1 WHERE ParentComponentID is null;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RollupLinkedPriceAndCost",
                table: "Order.Item.Component");
        }
    }
}
