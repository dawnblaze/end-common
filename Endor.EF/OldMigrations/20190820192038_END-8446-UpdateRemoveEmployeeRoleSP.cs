using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8446UpdateRemoveEmployeeRoleSP : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.RemoveEmployeeRole]( @BID tinyint, @OrderID int, @RoleID int, @EmployeeRoleID smallint, @EmployeeID int )
--
-- Description: This function remove a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.RemoveEmployeeRole] @BID=1, @OrderID=1, @RoleID=NULL, @EmployeeRoleID=1, @EmployeeID=1
-- ========================================================
ALTER PROCEDURE [dbo].[Order.Action.RemoveEmployeeRole]
-- DECLARE 
          @BID             TINYINT   -- = 1
        , @OrderID         INT       -- = 2
        , @RoleID          INT       = NULL
        , @EmployeeRoleID  SMALLINT  -- = 1
        , @EmployeeID      INT       -- = 1

        , @Result          INT       = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    IF @RoleID IS NOT NULL
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @RoleID
    END
    ELSE
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @EmployeeRoleID AND EmployeeID =  @EmployeeID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Delete record is exists
    DELETE FROM OER
    FROM   [Order.Employee.Role] OER
    WHERE  BID = @BID AND ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
