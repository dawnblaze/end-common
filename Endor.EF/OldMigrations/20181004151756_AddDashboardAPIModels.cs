using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181004151756_AddDashboardAPIModels")]
    public partial class AddDashboardAPIModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Dashboard.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "14500"),
                    Cols = table.Column<byte>(nullable: true),
                    Description = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    IsShared = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    LastUpdatedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Module = table.Column<short>(type: "smallint", nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Rows = table.Column<byte>(nullable: true),
                    SecurityRightID = table.Column<short>(nullable: true),
                    UserLinkID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dashboard.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Dashboard.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dashboard.Data_User.Link",
                        columns: x => new { x.BID, x.UserLinkID },
                        principalTable: "User.Link",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });


            migrationBuilder.CreateTable(
                name: "System.Dashboard.Widget.Definition",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false),
                    Categories = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "14503"),
                    DefaultCols = table.Column<byte>(nullable: false),
                    DefaultName = table.Column<string>(maxLength: 255, nullable: false),
                    DefaultOptions = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    DefaultRefreshIntervalInMin = table.Column<short>(nullable: true),
                    DefaultRows = table.Column<byte>(nullable: false),
                    Description = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    HasImage = table.Column<bool>(nullable: false),
                    MaxCols = table.Column<byte>(nullable: false),
                    MaxRows = table.Column<byte>(nullable: false),
                    MinCols = table.Column<byte>(nullable: false),
                    MinRows = table.Column<byte>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Modules = table.Column<short>(type: "smallint", nullable: true),
                    SecurityRightID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Dashboard.Widget.Definition", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Dashboard.Widget.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "14502"),
                    Cols = table.Column<byte>(nullable: true),
                    DashboardDataBID = table.Column<short>(nullable: true),
                    DashboardDataID = table.Column<short>(nullable: true),
                    DashboardID = table.Column<short>(nullable: false),
                    DashboardWidgetDefinitionID = table.Column<short>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    LastResults = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    LastUpdatedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Options = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    RefreshIntervalinMin = table.Column<short>(nullable: true),
                    Rows = table.Column<byte>(nullable: true),
                    WidgetDefinitionID = table.Column<short>(nullable: false),
                    X = table.Column<byte>(nullable: true),
                    Y = table.Column<byte>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dashboard.Widget.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Dashboard.Widget.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dashboard.Widget.Data_System.Dashboard.Widget.Definition",
                        column: x => x.WidgetDefinitionID,
                        principalTable: "System.Dashboard.Widget.Definition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dashboard.Widget.Data_Dashboard.Data",
                        columns: x => new { x.BID, x.DashboardID },
                        principalTable: "Dashboard.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data",
                columns: new[] { "BID", "Module", "IsSystem", "ID" });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_Dashboard",
                table: "Dashboard.Data",
                columns: new[] { "BID", "ID", "UserLinkID", "Module", "IsActive", "IsDefault" });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data",
                columns: new[] { "BID", "UserLinkID", "Module", "ID", "IsActive", "IsDefault" });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Widget.Data_Dashboard",
                table: "Dashboard.Widget.Data",
                columns: new[] { "BID", "DashboardID" });

            migrationBuilder.CreateIndex(
                name: "IX_System.Dashboard.Widget.Definition_Module",
                table: "System.Dashboard.Widget.Definition",
                column: "Modules");

            migrationBuilder.Sql(@"
INSERT INTO [Dashboard.Data]
    ([BID], [ID], [ModifiedDT], [IsActive], [Name], [Description], [IsSystem], [IsDefault], [IsShared], [Cols], [Rows], [Module], [UserLinkID], [SecurityRightID], [LastUpdatedDT])
    VALUES (1, 1, CAST(N'2018-10-01T23:34:48.5000000' AS DateTime2), 1, N'Management Dashboard', N'Default Dashboard for the management module.', 1, 1, 1, 4, 10, 1, NULL, NULL, NULL)
    , (1, 2, CAST(N'2018-10-01T23:34:48.5000000' AS DateTime2), 1, N'Accounting Dashboard', N'Default Dashboard for the Accounting Module.', 1, 1, 1, 4, 10, 2, NULL, NULL, NULL)
    , (1, 4, CAST(N'2018-10-01T23:34:48.5000000' AS DateTime2), 1, N'Sales Dashboard', N'Default Dashboard for the Sales Module.', 1, 1, 1, 4, 10, 4, NULL, NULL, NULL)
    , (1, 8, CAST(N'2018-10-01T23:34:48.5000000' AS DateTime2), 1, N'Production Dashboard', N'Default Dashboard for the Production Module.', 1, 1, 1, 4, 10, 8, NULL, NULL, NULL)
    , (1, 16, CAST(N'2018-10-01T23:34:48.5000000' AS DateTime2), 1, N'Purchasing Dashboard', N'Default Dashboard for the Purchasing Module.', 1, 1, 1, 4, 10, 16, NULL, NULL, NULL)
;
");

            migrationBuilder.Sql(@"
INSERT INTO [System.Dashboard.Widget.Definition]
    ([ID], [ModifiedDT], [DefaultName], [DefaultRows], [DefaultCols], [DefaultOptions], [DefaultRefreshIntervalInMin], [Description], [Modules], [SecurityRightID], [Categories], [MinRows], [MaxRows], [MinCols], [MaxCols], [HasImage])
    VALUES (1, CAST(N'2018-10-01T18:15:14.7900000' AS DateTime2), N'Image', 1, 1, NULL, 240, N'', NULL, NULL, N'General', 1, 3, 1, 3, 1)
    , (2, CAST(N'2018-10-01T22:43:14.1500000' AS DateTime2), N'Current Time', 1, 1, NULL, 1, N'Display the Current Time', NULL, NULL, NULL, 1, 1, 1, 1, 1)
    , (11, CAST(N'2018-10-01T18:17:10.0800000' AS DateTime2), N'Sales Breakdown', 1, 2, NULL, 60, N'Sales Breakdown', NULL, NULL, N'Sales', 1, 2, 1, 2, 0)
;
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Dashboard.Widget.Data");

            migrationBuilder.DropTable(
                name: "System.Dashboard.Widget.Definition");

            migrationBuilder.DropTable(
                name: "Dashboard.Data");
        }
    }
}

