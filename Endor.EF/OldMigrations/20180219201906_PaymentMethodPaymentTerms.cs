using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180219201906_PaymentMethodPaymentTerms")]
    public partial class PaymentMethodPaymentTerms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounting.Payment.Term.LocationLink");

            //EF doesn't know how to drop default value constraints???
            migrationBuilder.Sql(@"
ALTER TABLE [Accounting.Payment.Term] DROP CONSTRAINT [DF_Accounting.Payment.Term_CompoundInterest];
");

            migrationBuilder.DropColumn(
                name: "CompoundInterest",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "EarlyPaymentDiscount",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "GracePeriod",
                table: "Accounting.Payment.Term");

            //EF doesn't know how to drop default value constraints???
            migrationBuilder.Sql(@"
ALTER TABLE [Accounting.Payment.Term] DROP CONSTRAINT [DF_Accounting.Payment.Term_InterestBasedOnSaleDate];
");

            migrationBuilder.DropColumn(
                name: "InterestBasedOnSaleDate",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "InterestRate",
                table: "Accounting.Payment.Term");

            //rename a typo'd index
            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.Payment.Term].[IX_Accounting.Payment.Term)Name]', N'IX_Accounting.Payment.Term_Name', N'INDEX';");
            //EF cannot properly scaffold RenameIndex with dots in the name
            //migrationBuilder.RenameIndex(
            //    name: "IX_Accounting.Payment.Term)Name",
            //    table: "Accounting.Payment.Term",
            //    newName: "IX_Accounting.Payment.Term_Name");
            
            //migrationBuilder.AlterColumn<string>(
            //    name: "Name",
            //    table: "Accounting.Payment.Term",
            //    nullable: true,
            //    oldClrType: typeof(string),
            //    oldMaxLength: 255,
            //    oldNullable: true);

            migrationBuilder.Sql(@"
ALTER TABLE [Accounting.Payment.Term] DROP CONSTRAINT [PK_Accounting.Payment.Term];
ALTER TABLE [Accounting.Payment.Term] ALTER COLUMN ID int NOT NULL;
ALTER TABLE [Accounting.Payment.Term] ADD CONSTRAINT [PK_Accounting.Payment.Term] PRIMARY KEY CLUSTERED (
    [BID] ASC,
	[ID] ASC);
");
            //migrationBuilder.AlterColumn<int>(
            //    name: "ID",
            //    table: "Accounting.Payment.Term",
            //    nullable: false,
            //    oldClrType: typeof(short));

            migrationBuilder.AddColumn<short>(
                name: "DaysDue",
                table: "Accounting.Payment.Term",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<double>(
                name: "DownPaymentPercent",
                table: "Accounting.Payment.Term",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "DownPaymentPercentBelow",
                table: "Accounting.Payment.Term",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DownPaymentRequired",
                table: "Accounting.Payment.Term",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "DownPaymentThreshold",
                table: "Accounting.Payment.Term",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "EarlyPaymentPercent",
                table: "Accounting.Payment.Term",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PaymentDueBasedOnText",
                table: "Accounting.Payment.Term",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term",
                nullable: true);
            
            migrationBuilder.CreateTable(
                name: "enum.Accounting.PaymentDueBasedOnType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Accounting.PaymentDueBasedOnType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Accounting.PaymentMethodType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Accounting.PaymentMethodType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Payment.Method",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    BID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "8002"),
                    DepositGLAccountID = table.Column<int>(nullable: false),
                    IsACH = table.Column<bool>(nullable: false, computedColumnSql: "(CASE WHEN PaymentMethodType = 3 THEN CONVERT(BIT, 1) ELSE CONVERT([BIT], 0) END)"),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    IsCreditCard = table.Column<bool>(nullable: false, computedColumnSql: "(CASE WHEN PaymentMethodType in (5, 6, 7, 8, 9) THEN CONVERT([BIT], 1) ELSE CONVERT([BIT], 0) END)"),
                    IsCustom = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    IsIntegrated = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    ModifiedDT = table.Column<DateTime>(nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    PaymentMethodType = table.Column<byte>(type: "tinyint", nullable: false, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Payment.Method", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Method_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Method_enum.Accounting.PaymentMethodType",
                        column: x => x.PaymentMethodType,
                        principalTable: "enum.Accounting.PaymentMethodType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Method_Accounting.GL.Account",
                        columns: x => new { x.BID, x.DepositGLAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Term_PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term",
                column: "PaymentDueBasedOnTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Method_PaymentMethodType",
                table: "Accounting.Payment.Method",
                column: "PaymentMethodType");

            migrationBuilder.Sql(@"
IF EXISTS(select * from sys.objects where name = 'Accounting.Payment.Method.Action.SetActive' and type = 'P')
  DROP PROCEDURE [dbo].[Accounting.Payment.Method.Action.SetActive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Accounting.Payment.Method.Action.SetActive]
--
-- Description: This procedure sets the PaymentMethod to active or inactive
-- and sets the corresponding GLAccount to active or inactive as well
--
-- Sample Use:   EXEC dbo.[Accounting.Payment.Method.Action.SetActive] @BID=1, @PaymentMethodID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Payment.Method.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @PaymentMethodID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the PaymentMethod specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Method] WHERE BID = @BID and ID = @PaymentMethodID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid PaymentMethod Specified. PaymentMethodID='+CONVERT(VARCHAR(12),@PaymentMethodID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.Payment.Method] L
    WHERE BID = @BID and ID = @PaymentMethodID
      AND COALESCE(IsActive,~@IsActive) != @IsActive
	  
    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO


");
            migrationBuilder.Sql(@"
IF EXISTS(select * from sys.objects where name = 'Accounting.Payment.Method.Action.CanDelete' and type = 'P')
  DROP PROCEDURE [dbo].[Accounting.Payment.Method.Action.CanDelete]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* 
========================================================
    Name: [Accounting.Payment.Method.Action.CanDelete]

    Description: This procedure checks if the PaymentMethod is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Payment.Method.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Payment.Method.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	A paymentmethod can be deleted if:

	* It exists
	* It is a custom payment method.
	* It is not used by any payments.
	* It is not used by any stored payment methods.
*/

    SET @Result = 1;

    IF NOT EXISTS( SELECT * FROM [Accounting.Payment.Method] WHERE BID = @BID AND ID = @ID ) 
        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Method Specified Does not Exist. PaymentMethod=', @ID)

    -- Check if the record exists
    ELSE IF  0=(SELECT IsCustom FROM [Accounting.Payment.Method] WHERE ID = @ID)
        SELECT @Result = 0
             , @CantDeleteReason = 'Cannot delete System Payment Method'

    --ELSE IF EXISTS(SELECT * FROM [Accounting.Payment.Data] WHERE PaymentMethodID = @ID)
    --    SELECT @Result = 0
    --         , @CantDeleteReason = 'Payment Data for Payment Method Exists'

    --ELSE IF EXISTS(SELECT * FROM [Accounting.Payment.Credential] WHERE PaymentMethodID = @ID)
    --    SELECT @Result = 0
    --         , @CantDeleteReason = 'Payment Credentials for Payment Method Exists'

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO


");
            migrationBuilder.Sql(@"
IF EXISTS(select 1 from sys.views where name='Accounting.Payment.Method.SimpleList' and type='v')
DROP VIEW [dbo].[Accounting.Payment.Method.SimpleList];
GO
CREATE VIEW[dbo].[Accounting.Payment.Method.SimpleList]
    AS
SELECT[BID]
    , [ID]
    , [ClassTypeID]
    , [Name] as DisplayName
        , [IsActive]
        , CONVERT(BIT, 0) AS[HasImage]
        , CONVERT(BIT, 0) AS[IsDefault]
FROM[Accounting.Payment.Method]
WHERE IsActive = 1");

            migrationBuilder.Sql(@"
INSERT [enum.Accounting.PaymentMethodType] ([ID], [Name])
VALUES (0, N'Custom')
    , (1, N'Cash')
    , (2, N'Check')
    , (3, N'ACH')
    , (4, N'Wire Transfer')
    , (5, N'Credit Card')
    , (6, N'AmEx')
    , (7, N'MasterCard')
    , (8, N'Visa')
    , (9, N'Discover')
;");

            migrationBuilder.Sql(@"
DECLARE @BID SMALLINT = 1;

if NOT EXISTS (SELECT * FROM [Business.Data] where BID =@BID)
INSERT INTO [dbo].[Business.Data]
           ([BID]
           ,[CreatedDate]
           ,[ModifiedDT]
           ,[IsActive]
           ,[Name]
           ,[LegalName]
           ,[HasImage]
           ,[OwnerEmployeeID]
           ,[BillingEmployeeID]
           ,[AssociationType])
     VALUES
           (@BID
           ,GETUTCDATE()
           ,GETUTCDATE()
           ,1
           ,'Default Business'
           ,'Default Business'
           ,0
           ,null
           ,null
           ,null)
 
INSERT [Accounting.Payment.Method] ([BID], [ID], [ModifiedDT], [IsActive], [Name], [PaymentMethodType], [DepositGLAccountID], [IsIntegrated], [IsCustom])
VALUES (@BID, 1, '2018-01-01', 1, N'Cash', 1, 1100, 0, 0)
     , (@BID, 2, '2018-01-01', 1, N'Check', 2, 1100, 0, 0)
     , (@BID, 3, '2018-01-01', 0, N'ACH (External)', 3, 1100, 0, 0)
     , (@BID, 4, '2018-01-01', 0, N'Wire Transfer (External)', 4, 1100, 0, 0)
     , (@BID, 5, '2018-01-01', 1, N'AmEx (External)', 6, 1100, 0, 0)
     , (@BID, 6, '2018-01-01', 1, N'MasterCard (External)', 7, 1100, 0, 0)
     , (@BID, 7, '2018-01-01', 1, N'Visa (External)', 8, 1100, 0, 0)
     , (@BID, 8, '2018-01-01', 1, N'Discover (External)', 9, 1100, 0, 0)
     , (@BID, 23, '2018-01-01', 0, N'ACH', 3, 1100, 1, 0)
     , (@BID, 26, '2018-01-01', 1, N'Credit Card', 5, 1100, 1, 0)
;
");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Method_BID_DepositGLAccountID",
                table: "Accounting.Payment.Method",
                columns: new[] { "BID", "DepositGLAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Method_Name",
                table: "Accounting.Payment.Method",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Term_enum.Accounting.PaymentDueBasedOnType_PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term",
                column: "PaymentDueBasedOnTypeID",
                principalTable: "enum.Accounting.PaymentDueBasedOnType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [Accounting.Payment.Method] WHERE ID in (1, 2, 3, 4, 5, 6, 7, 8, 23, 26) AND BID = 1;
GO");

            migrationBuilder.Sql(@"
IF EXISTS(select * from sys.objects where name = 'Accounting.Payment.Method.Action.SetActive' and type = 'P')
  DROP PROCEDURE [dbo].[Accounting.Payment.Method.Action.SetActive]
GO");
            migrationBuilder.Sql(@"
IF EXISTS(select * from sys.objects where name = 'Accounting.Payment.Method.Action.CanDelete' and type = 'P')
  DROP PROCEDURE [dbo].[Accounting.Payment.Method.Action.CanDelete]
GO");
            migrationBuilder.Sql(@"
if exists(select 1 from sys.views where name='Accounting.Payment.Method.SimpleList' and type='v')
drop view [dbo].[Accounting.Payment.Method.SimpleList];");

            migrationBuilder.Sql("EXEC sp_rename N'[Accounting.Payment.Term].[IX_Accounting.Payment.Term_Name]', N'IX_Accounting.Payment.Term)Name', N'INDEX';");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Term_enum.Accounting.PaymentDueBasedOnType_PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropTable(
                name: "Accounting.Payment.Method");

            migrationBuilder.DropTable(
                name: "enum.Accounting.PaymentDueBasedOnType");

            migrationBuilder.DropTable(
                name: "enum.Accounting.PaymentMethodType");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Payment.Term_PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "DaysDue",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "DownPaymentPercent",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "DownPaymentPercentBelow",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "DownPaymentRequired",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "DownPaymentThreshold",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "EarlyPaymentPercent",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "PaymentDueBasedOnText",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Accounting.Payment.Term",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
            
            migrationBuilder.Sql(@"
ALTER TABLE [Accounting.Payment.Term] DROP CONSTRAINT [PK_Accounting.Payment.Term];
ALTER TABLE [Accounting.Payment.Term] ALTER COLUMN ID smallint NOT NULL;
ALTER TABLE [Accounting.Payment.Term] ADD CONSTRAINT [PK_Accounting.Payment.Term] PRIMARY KEY CLUSTERED (
    [BID] ASC,
	[ID] ASC);
");
            //migrationBuilder.AlterColumn<short>(
            //    name: "ID",
            //    table: "Accounting.Payment.Term",
            //    nullable: false,
            //    oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "CompoundInterest",
                table: "Accounting.Payment.Term",
                nullable: true,
                defaultValueSql: "((1))");

            migrationBuilder.AddColumn<decimal>(
                name: "EarlyPaymentDiscount",
                table: "Accounting.Payment.Term",
                type: "decimal(12, 4)",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "GracePeriod",
                table: "Accounting.Payment.Term",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "InterestBasedOnSaleDate",
                table: "Accounting.Payment.Term",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "InterestRate",
                table: "Accounting.Payment.Term",
                type: "decimal(12, 4)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Accounting.Payment.Term.LocationLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TermID = table.Column<short>(nullable: false),
                    LocationID = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Payment.Term.LocationLink", x => new { x.BID, x.TermID, x.LocationID });
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Term.LocationLink_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term",
                        columns: x => new { x.BID, x.TermID },
                        principalTable: "Accounting.Payment.Term",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Term.LocationLink",
                table: "Accounting.Payment.Term.LocationLink",
                columns: new[] { "BID", "LocationID", "TermID" });
        }
    }
}

