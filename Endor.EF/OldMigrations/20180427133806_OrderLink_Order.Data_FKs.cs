using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180427133806_OrderLink_Order.Data_FKs")]
    public partial class OrderLink_OrderData_FKs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {            
            migrationBuilder.AddForeignKey(
                name: "FK_Order.Data_TransactionType",
                table: "Order.Data",
                column: "TransactionType",
                principalTable: "enum.Order.TransactionType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.OrderLink_LinkType",
                table: "Order.OrderLink",
                column: "LinkType",
                principalTable: "enum.Order.OrderLinkType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.OrderLink_EmployeeID",
                table: "Order.OrderLink",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.OrderLink_LinkedOrderID",
                table: "Order.OrderLink",
                columns: new[] { "BID", "LinkedOrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.OrderLink_OrderID",
                table: "Order.OrderLink",
                columns: new[] { "BID", "OrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Data_TransactionType",
                table: "Order.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.OrderLink_LinkType",
                table: "Order.OrderLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.OrderLink_EmployeeID",
                table: "Order.OrderLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.OrderLink_LinkedOrderID",
                table: "Order.OrderLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.OrderLink_OrderID",
                table: "Order.OrderLink");
        }
    }
}

