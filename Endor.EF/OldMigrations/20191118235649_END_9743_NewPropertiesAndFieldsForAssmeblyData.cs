﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_9743_NewPropertiesAndFieldsForAssmeblyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ArriveByLabel",
                table: "Part.Subassembly.Data",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BlindShippingLabel",
                table: "Part.Subassembly.Data",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EnableArriveByDate",
                table: "Part.Subassembly.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "EnableBlindShipping",
                table: "Part.Subassembly.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "EnableFromAddress",
                table: "Part.Subassembly.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "EnablePackagesTab",
                table: "Part.Subassembly.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "EnableShipByDate",
                table: "Part.Subassembly.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "EnableToAddress",
                table: "Part.Subassembly.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "FromAddressLabel",
                table: "Part.Subassembly.Data",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShipByDateLabel",
                table: "Part.Subassembly.Data",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ToAddressLabel",
                table: "Part.Subassembly.Data",
                type: "nvarchar(50)",
                nullable: true);

            migrationBuilder.Sql(@"ALTER TABLE dbo.[Part.SubAssembly.Data]
                                   ALTER COLUMN ShipByDateLabel NVARCHAR(50) SPARSE NULL;
                                   ALTER TABLE dbo.[Part.SubAssembly.Data]
                                   ALTER COLUMN ArriveByLabel NVARCHAR(50) SPARSE NULL;
                                   ALTER TABLE dbo.[Part.SubAssembly.Data]
                                   ALTER COLUMN FromAddressLabel NVARCHAR(50) SPARSE NULL;
                                   ALTER TABLE dbo.[Part.SubAssembly.Data]
                                   ALTER COLUMN ToAddressLabel NVARCHAR(50) SPARSE NULL;
                                   ALTER TABLE dbo.[Part.SubAssembly.Data]
                                   ALTER COLUMN BlindShippingLabel NVARCHAR(50) SPARSE NULL;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArriveByLabel",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "BlindShippingLabel",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "EnableArriveByDate",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "EnableBlindShipping",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "EnableFromAddress",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "EnablePackagesTab",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "EnableShipByDate",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "EnableToAddress",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "FromAddressLabel",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "ShipByDateLabel",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "ToAddressLabel",
                table: "Part.Subassembly.Data");
        }
    }
}
