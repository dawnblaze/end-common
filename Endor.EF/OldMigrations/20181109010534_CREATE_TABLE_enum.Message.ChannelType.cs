using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CREATE_TABLE_enumMessageChannelType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Message.ChannelType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Message.ChannelType", x => x.ID);
                });

            migrationBuilder.Sql(@"
                INSERT [enum.Message.ChannelType] ([ID], [Name])
                    VALUES (0, N'None')
                    , (1, N'Message')
                    , (2, N'Alert')
                    , (4, N'Email')
                    , (8, N'SMS')
                    , (16, N'Slack')
                    , (32, N'WebHook')
                    , (64, N'Zapier')
                    , (128, N'System Notification')
                ;            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [enum.Message.ChannelType] WHERE ID IN(1,2,4,8,16,32,64,128);
            ");

            migrationBuilder.DropTable(
                name: "enum.Message.ChannelType");
        }
    }
}