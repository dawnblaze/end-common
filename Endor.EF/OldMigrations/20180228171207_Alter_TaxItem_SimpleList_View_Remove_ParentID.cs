using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180228171207_Alter_TaxItem_SimpleList_View_Remove_ParentID")]
    public partial class Alter_TaxItem_SimpleList_View_Remove_ParentID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Accounting.Tax.Item.SimpleList] AS
                    SELECT [BID]
                         , [ID]
                         , [ClassTypeID]
                         , [Name] as DisplayName
                         , [IsActive]
                         , CONVERT(BIT, 0) AS [HasImage]
                         , CONVERT(BIT, 0) AS [IsDefault]
                    FROM [Accounting.Tax.Item]
                GO            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Accounting.Tax.Item.SimpleList] AS
                    SELECT [BID]
                         , [ID]
                         , [ClassTypeID]
                         , [Name] as DisplayName
                         , [IsActive]
                         , CONVERT(BIT, 0) AS [HasImage]
                         , CONVERT(BIT, 0) AS [IsDefault]
                         , (SELECT GroupID from [Accounting.Tax.Group.ItemLink] i WHERE i.BID = BID AND i.ItemID = ID) as ParentID
                    FROM [Accounting.Tax.Item]
                GO
            ");
        }
    }
}

