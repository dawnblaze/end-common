using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Alter_PartMachineProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@" 
				
	        ALTER TABLE dbo.[Part.Machine.Profile]
	        DROP CONSTRAINT [FK_Part.Machine.Profile_Part.Machine.Data_MachineDataBID_MachineDataID]; 	

	        DROP INDEX IF EXISTS [IX_Part.Machine.Profile_MachineDataBID_MachineDataID] ON [dbo].[Part.Machine.Profile];
            ALTER TABLE dbo.[Part.Machine.Profile] 
	        DROP COLUMN [MachineDataBID],[MachineDataID];

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
