using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180503150724_alter_OrderItemStatus_ID_smallint")]
    public partial class alter_OrderItemStatus_ID_smallint : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE dbo.[Order.Item.Status]
                DROP CONSTRAINT [PK_Order.Item.Status]; 
            ");

            migrationBuilder.AlterColumn<short>(
                name: "ID",
                table: "Order.Item.Status",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Item.Status]
                ADD CONSTRAINT [PK_Order.Item.Status] PRIMARY KEY ([BID], [ID]); 
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE dbo.[Order.Item.Status]
                DROP CONSTRAINT [PK_Order.Item.Status]; 
            ");

            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "Order.Item.Status",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Item.Status]
                ADD CONSTRAINT [PK_Order.Item.Status] PRIMARY KEY ([BID], [ID]); 
            ");
        }
    }
}

