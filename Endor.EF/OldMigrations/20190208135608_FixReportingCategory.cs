using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixReportingCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Category] SET
	  [Name] = 'Analytic Report Templates'
	, [Description] = 'Analytic Report Templates'
WHERE [ID] = 803;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Category] SET
	  [Name] = 'Analytic Report Tempaltes'
	, [Description] = 'Analytic Report Tempaltes'
WHERE [ID] = 803;
");
        }
    }
}
