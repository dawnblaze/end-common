﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddTaxEngineTypeAndTaxItemUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "TaxEngineType",
                table: "Accounting.Tax.Item",
                nullable: false,
                defaultValue: (byte)2);

            migrationBuilder.CreateTable(
                name: "enum.Accounting.TaxEngineType",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Accounting.TaxEngineType", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Accounting.TaxEngineType");

            migrationBuilder.DropColumn(
                name: "TaxEngineType",
                table: "Accounting.Tax.Item");
        }
    }
}
