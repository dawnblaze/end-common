using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181001161716_Modify_List.Filter.MyLists")]
    public partial class Modify_ListFilterMyLists : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/****** Object:  UserDefinedFunction [dbo].[List.Filter.MyLists]    Script Date: 10/01/18 23:33:22 ******/
DROP FUNCTION [dbo].[List.Filter.MyLists]
GO

/****** Object:  UserDefinedFunction [dbo].[List.Filter.MyLists]    Script Date: 10/01/18 23:33:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- This function returns a users Lists for a particular TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.MyLists](7, 10001, 2000)
--
-- =============================================
-- =============================================
-- This function returns a users Lists for a particular TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.MyLists](7, 10001, 2000)
--
-- =============================================
CREATE FUNCTION [dbo].[List.Filter.MyLists] (@BID SMALLINT, @UserLinkID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT L.* 
    FROM [List.Filter] L
    WHERE BID = @BID 
      AND ID IN (
                    SELECT L.ID
                    FROM [List.Filter] L
                    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
                      AND (L.IsSystem=1 OR (L.BID = @BID AND L.OwnerID = @UserLinkID)) 
    
                    UNION

                    SELECT L.ID
                    FROM [List.Filter] L
					JOIN [User.Link] UL ON UL.BID = L.BID AND UL.ID = @UserLinkID
					JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = L.BID AND LS.FilterID = L.ID AND LS.EmployeeID = UL.EmployeeID
					
                    WHERE (L.BID = @BID) 
                        AND (L.TargetClassTypeID = @TargetClassTypeID)
                        AND (UL.ID = @UserLinkID)
                )
)
;
GO
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/****** Object:  UserDefinedFunction [dbo].[List.Filter.MyLists]    Script Date: 10/01/18 23:37:40 ******/
DROP FUNCTION [dbo].[List.Filter.MyLists]
GO

/****** Object:  UserDefinedFunction [dbo].[List.Filter.MyLists]    Script Date: 10/01/18 23:37:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- This function returns a users Lists for a particular TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.MyLists](7, 10001, 2000)
--
-- =============================================
-- =============================================
-- This function returns a users Lists for a particular TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.MyLists](7, 10001, 2000)
--
-- =============================================
CREATE FUNCTION [dbo].[List.Filter.MyLists] (@BID SMALLINT, @EmployeeID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT L.* 
    FROM [List.Filter] L
    WHERE BID = @BID 
      AND ID IN (
                    SELECT L.ID
                    FROM [List.Filter] L
                    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
                      AND (L.IsSystem=1 OR (L.BID = @BID AND L.OwnerID = @EmployeeID)) 
    
                    UNION

                    SELECT L.ID
                    FROM [List.Filter] L
                    JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = L.BID AND LS.FilterID = L.ID
                    WHERE (L.BID = @BID) 
                        AND (L.TargetClassTypeID = @TargetClassTypeID)
                        AND (LS.EmployeeID = @EmployeeID)
                )
)
;
GO
");
        }
    }
}

