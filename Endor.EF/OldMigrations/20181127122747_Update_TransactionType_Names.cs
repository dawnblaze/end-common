using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Update_TransactionType_Names : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
              UPDATE [enum.Order.TransactionType]
              SET Name = (CASE ID WHEN 1 THEN 'Estimate' WHEN 2 THEN 'Order' ELSE Name END)
              WHERE ID IN (1,2);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
