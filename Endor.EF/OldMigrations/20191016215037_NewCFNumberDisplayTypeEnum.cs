﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class NewCFNumberDisplayTypeEnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_DisplayType",
                table: "CustomField.Definition");

            migrationBuilder.Sql(
            @"
IF (OBJECT_ID('FK_Part.Subassembly.Variable_enum.CustomField.DisplayType') IS NOT NULL)
BEGIN
    ALTER TABLE [Part.Subassembly.Variable]
    DROP CONSTRAINT [FK_Part.Subassembly.Variable_enum.CustomField.DisplayType]
END
            "
            );

            //migrationBuilder.DropForeignKey(
            //    name: "FK_Part.Subassembly.Variable_enum.CustomField.DisplayType",
            //    table: "Part.Subassembly.Variable");

            migrationBuilder.DropTable(
                name: "enum.CustomField.DisplayType");

            migrationBuilder.AlterColumn<byte>(
                name: "DisplayType",
                table: "CustomField.Definition",
                type: "tinyint sparse",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "tinyint");

            migrationBuilder.CreateTable(
                name: "enum.CustomField.NumberDisplayType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.NumberDisplayType", x => x.ID);
                });

            migrationBuilder.Sql(@"
INSERT [enum.CustomField.NumberDisplayType] ([ID], [Name]) VALUES (0, N'General')
GO
INSERT [enum.CustomField.NumberDisplayType] ([ID], [Name]) VALUES (1, N'Fixed')
GO
INSERT [enum.CustomField.NumberDisplayType] ([ID], [Name]) VALUES (2, N'Currency')
GO
INSERT [enum.CustomField.NumberDisplayType] ([ID], [Name]) VALUES (3, N'Accounting')
GO 
UPDATE [CustomField.Definition] SET [DisplayType] = 0 WHERE DisplayType > 3;
UPDATE [Part.Subassembly.Variable] SET [DisplayType] = 0 WHERE DisplayType > 3;
UPDATE [System.Part.Subassembly.Variable] SET [DisplayType] = 0 WHERE DisplayType > 3;
");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_NumberDisplayType",
                table: "CustomField.Definition",
                column: "DisplayType",
                principalTable: "enum.CustomField.NumberDisplayType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.CustomField.NumberDisplayType",
                table: "Part.Subassembly.Variable",
                column: "DisplayType",
                principalTable: "enum.CustomField.NumberDisplayType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_NumberDisplayType",
                table: "CustomField.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.CustomField.NumberDisplayType",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropTable(
                name: "enum.CustomField.NumberDisplayType");

            migrationBuilder.AlterColumn<byte>(
                name: "DisplayType",
                table: "CustomField.Definition",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "enum.CustomField.DisplayType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    DataType = table.Column<short>(type: "smallint", nullable: false),
                    FormatString = table.Column<string>(type: "VARCHAR(MAX)", nullable: true),
                    Name = table.Column<string>(type: "varchar(100)", unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.DisplayType", x => x.ID);
                    table.ForeignKey(
                        name: "FK_enum.CustomField.DisplayType_enum.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.Sql(@"
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (0, 0, NULL, N'None')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (1, 0, NULL, N'Unformatted Single Line')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (2, 0, NULL, N'Unformatted Memo')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (3, 0, NULL, N'Formatted Line')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (4, 0, NULL, N'Formatted Memo')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (11, 1, NULL, N'Integer')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (21, 2, NULL, N'Number')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (22, 2, NULL, N'Integer (Rounded)')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (23, 2, NULL, N'2 Decimal Places')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (24, 2, NULL, N'4 Decimal Places')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (25, 2, NULL, N'Money')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (26, 2, NULL, N'Accounting')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (31, 3, NULL, N'Checkbox')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (32, 3, NULL, N'Yes-No')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (33, 3, NULL, N'True-False')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (34, 3, NULL, N'Enabled-Disabled')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (39, 3, NULL, N'Custom')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (71, 9, NULL, N'Date ( Short Format )')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (72, 9, NULL, N'Date ( Long Format )')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (73, 9, NULL, N'Date ( Mon 6/19 )')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (74, 9, NULL, N'Days from Today (Late)')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (75, 9, NULL, N'Workdays from Today (Late)')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (81, 9, NULL, N'Time ( Short Format )')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (82, 9, NULL, N'Time ( Long Format )')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (91, 9, NULL, N'DateTime ( Short Format )')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (92, 9, NULL, N'DateTime (Long Format)')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (93, 9, NULL, N'DateTime ( Mon 6/19 11:32 )')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (94, 9, NULL, N'Days and Hours from Today (Late)')
GO
INSERT [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name]) VALUES (95, 9, NULL, N'WorkDays and Hours from Today (Late)')
");

            migrationBuilder.CreateIndex(
                name: "IX_enum.CustomField.DisplayType_DataType",
                table: "enum.CustomField.DisplayType",
                column: "DataType");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_DisplayType",
                table: "CustomField.Definition",
                column: "DisplayType",
                principalTable: "enum.CustomField.DisplayType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.CustomField.DisplayType",
                table: "Part.Subassembly.Variable",
                column: "DisplayType",
                principalTable: "enum.CustomField.DisplayType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
