using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180618044153_AddCustomFieldDefinition")]
    public partial class AddCustomFieldDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomField.Definition",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AllowMultipleValues = table.Column<bool>(nullable: false),
                    AppliesToClass = table.Column<bool>(nullable: false, computedColumnSql: "(case when [AppliesToID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    AppliesToClassTypeID = table.Column<int>(nullable: false),
                    AppliesToID = table.Column<int>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((15025))"),
                    DataType = table.Column<byte>(type: "tinyint", nullable: false),
                    Description = table.Column<string>(type: "varchar(32)", nullable: true),
                    DisplayFormatString = table.Column<string>(type: "VARCHAR(32)", maxLength: 32, nullable: true),
                    DisplayFormatType = table.Column<byte>(type: "tinyint", nullable: false),
                    HasValidators = table.Column<bool>(nullable: false),
                    InputType = table.Column<byte>(type: "tinyint", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    Label = table.Column<string>(maxLength: 100, nullable: true),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomField.Definition", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CustomField.Definition_BID",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomField.Definition_DataType",
                        column: x => x.DataType,
                        principalTable: "enum.CustomField.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomField.Definition_InputType",
                        column: x => x.InputType,
                        principalTable: "enum.CustomField.InputType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_CustomField.Definition_DisplayType",
                        column: x => x.DisplayFormatType,
                        principalTable: "enum.CustomField.DisplayType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Definition_AppliesTo",
                table: "CustomField.Definition",
                columns: new[] { "BID", "Name", "IsActive", "AppliesToClassTypeID", "AppliesToID" });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomField.Definition");

        }
    }
}

