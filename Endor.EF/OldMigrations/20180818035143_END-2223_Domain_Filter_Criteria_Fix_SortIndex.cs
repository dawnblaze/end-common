using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180818035143_END-2223_Domain_Filter_Criteria_Fix_SortIndex")]
    public partial class END2223_Domain_Filter_Criteria_Fix_SortIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.List.Filter.Criteria]
                    SET SortIndex = 0
                    WHERE 
                    [Name] = 'Location'
                    AND 
                    TargetClassTypeID = 1021

                    UPDATE [System.List.Filter.Criteria]
                    SET SortIndex = 1
                    WHERE 
                    [Name] = 'Application Type'
                    AND 
                    TargetClassTypeID = 1021

                    UPDATE [System.List.Filter.Criteria]
                    SET SortIndex = 2
                    WHERE 
                    [Name] = 'Is Active'
                    AND 
                    TargetClassTypeID = 1021
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.List.Filter.Criteria]
                    SET SortIndex = NULL
                    WHERE 
                    [Name] = 'Location'
                    AND 
                    TargetClassTypeID = 1021

                    UPDATE [System.List.Filter.Criteria]
                    SET SortIndex = NULL
                    WHERE 
                    [Name] = 'Application Type'
                    AND 
                    TargetClassTypeID = 1021

                    UPDATE [System.List.Filter.Criteria]
                    SET SortIndex = 1
                    WHERE 
                    [Name] = 'Is Active'
                    AND 
                    TargetClassTypeID = 1021
                "
            );
        }
    }
}

