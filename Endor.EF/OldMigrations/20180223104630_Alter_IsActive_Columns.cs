using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180223104630_Alter_IsActive_Columns")]
    public partial class Alter_IsActive_Columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP INDEX IF EXISTS [IX_CRM.Origin_Name] ON [dbo].[CRM.Origin];
                ALTER TABLE [CRM.Origin] ALTER COLUMN IsACtive BIT NOT NULL;
                CREATE INDEX [IX_CRM.Origin_Name] ON [dbo].[CRM.Origin] ( BID, Name, IsActive );
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP INDEX IF EXISTS [IX_CRM.Origin_Name] ON [dbo].[CRM.Origin];
                ALTER TABLE [CRM.Origin] ALTER COLUMN IsACtive BIT NULL;
                CREATE INDEX [IX_CRM.Origin_Name] ON [dbo].[CRM.Origin] ( BID, Name, IsActive );
            ");
        }
    }
}

