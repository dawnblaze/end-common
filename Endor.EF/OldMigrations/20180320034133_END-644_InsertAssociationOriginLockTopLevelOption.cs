using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180320034133_END-644_InsertAssociationOriginLockTopLevelOption")]
    public partial class END644_InsertAssociationOriginLockTopLevelOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
if not Exists(select * from [System.Option.Definition] where name = 'Association.Origin.LockFirstLevel')
begin
INSERT INTO [dbo].[System.Option.Definition]
           ([ID]
           ,[Name]
           ,[Label]
           ,[Description]
           ,[DataType]
           ,[CategoryID]
           ,[ListValues]
           ,[DefaultValue]
           ,[IsHidden])
     VALUES
           (41, 'Association.Origin.LockFirstLevel', 'Lock First Origin Level', 'If true then Top level Origin records cannot be edited', 3, 7, NULL, 'false', 1)
		   
end
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE From [System.Option.Definition] where name = 'Association.Origin.LockFirstLevel'");
        }
    }
}

