using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180816050711_AddDomainDefinitionFilter")]
    public partial class AddDomainDefinitionFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [List.Filter] (BID, ID, IsActive, Name, TargetClassTypeID, Criteria, IsDefault,IsSystem,SortIndex)
VALUES (1,(SELECT MAX(ID) + 1 FROM [List.Filter]),1,'Active',1021,'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',1,1,0)
");

            migrationBuilder.Sql
                (
                @"
                    INSERT INTO [System.List.Filter.Criteria]
                    (
                       [ID]
                      ,[TargetClassTypeID]
                      ,[Name]
                      ,[Label]
                      ,[Field]
                      ,[IsHidden]
                      ,[DataType]
                      ,[InputType]
                      ,[AllowMultiple]
                      ,[ListValues]
                      ,[ListValuesEndpoint]
                      ,[IsLimitToList]
                      ,[SortIndex]
                    )
                    VALUES
                    (
                      /*Get Next ID*/
                      (SELECT MAX(ID) + 1 FROM [System.List.Filter.Criteria])
                      , 1021                     --[TargetClassTypeID] (Alert Definition)
                      , 'Is Active'              --[Name]
                      , 'Is Active'              --[Label]
                      , 'IsActive'               --[Field]
                      , 0                        --[IsHidden]
                      , 3                        --[DataType]
                      , 2                        --[InputType]
                      , 0                        --[AllowMultiple]
                      , 'Is Not Active,Is Active'--[ListValues]
                      , NULL                     --[ListValuesEndpoint]
                      , 1                        --[IsLimitToList]
                      , 1                        --[SortIndex]
                    )
                    ,(
                      /*Get Next ID*/
                      (SELECT MAX(ID) + 2 FROM [System.List.Filter.Criteria])
                      , 1021                     --[TargetClassTypeID] (Alert Definition)
                      , 'Location'               --[Name]
                      , 'Location'               --[Label]
                      , 'LocationID'             --[Field]
                      , 0                        --[IsHidden]
                      , 1                        --[DataType]
                      , 4                        --[InputType]
                      , 0                        --[AllowMultiple]
                      , NULL                     --[ListValues]
                      , NULL                     --[ListValuesEndpoint]
                      , 0                        --[IsLimitToList]
                      , NULL                     --[SortIndex]
                    )
                    ,(
                      /*Get Next ID*/
                      (SELECT MAX(ID) + 3 FROM [System.List.Filter.Criteria])
                      , 1021                     --[TargetClassTypeID] (Alert Definition)
                      , 'Application Type'               --[Name]
                      , 'Application Type'               --[Label]
                      , 'AccessType'             --[Field]
                      , 0                        --[IsHidden]
                      , 1                        --[DataType]
                      , 4                        --[InputType]
                      , 0                        --[AllowMultiple]
                      , NULL                     --[ListValues]
                      , NULL                     --[ListValuesEndpoint]
                      , 0                        --[IsLimitToList]
                      , NULL                     --[SortIndex]
                    )
                "
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [List.Filter] WHERE TargetClassTypeID = 1021;
");
            migrationBuilder.Sql(@"
DELETE FROM [System.List.Filter.Criteria] WHERE TargetClassTypeID = 1021;
");
        }
    }
}

