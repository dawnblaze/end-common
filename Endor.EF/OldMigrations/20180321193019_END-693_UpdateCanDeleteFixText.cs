using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180321193019_END-693_UpdateCanDeleteFixText")]
    public partial class END693_UpdateCanDeleteFixText : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"-- ========================================================
-- Name: [Origin.Action.CanDelete]
--
-- Description: This procedure checks if the origin is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Origin.Action.CanDelete] @BID=1, @ID=1005, @ShowCantDeleteReason=1
-- ========================================================
ALTER PROCEDURE [dbo].[Origin.Action.CanDelete]
		  @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @ID)
		SELECT @Result = 0
             , @CantDeleteReason = 'Origin specified was not found'

	ELSE IF EXISTS(SELECT * FROM [Company.Data] WHERE OriginID = @ID)
            --AND NOT EXISTS(SELECT * FROM [Order.Data] WHERE OriginID = @OriginID)
        SELECT @Result = 0
             , @CantDeleteReason = 'The Origin is being used by a Company and can''t be deleted.'

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END");
            migrationBuilder.Sql(@"-- ========================================================
-- Name: [Industry.Action.CanDelete]
--
-- Description: This procedure checks if the Industry is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Industry.Action.CanDelete] @BID=1, @ID=1
-- ========================================================
ALTER PROCEDURE [dbo].[Industry.Action.CanDelete]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ID       INT     -- = 2

        , @Result         BIT     = NULL  OUTPUT
		, @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Industry specified is valid

	 IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @ID)
		SELECT @Result = 0
             , @CantDeleteReason = 'Industry specified was not found'

	ELSE IF EXISTS(SELECT * FROM [Company.Data] WHERE IndustryID = @ID)
        SELECT @Result = 0
             , @CantDeleteReason = 'The Industry is being used by a Company and can''t be deleted.'

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"-- ========================================================
-- Name: [Industry.Action.CanDelete]
--
-- Description: This procedure checks if the Industry is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Industry.Action.CanDelete] @BID=1, @ID=1
-- ========================================================
ALTER PROCEDURE [dbo].[Industry.Action.CanDelete]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ID       INT     -- = 2

        , @Result         BIT     = NULL  OUTPUT
		, @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Industry specified is valid

	 IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @ID)
		SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Invalid Industry Specified. IndustryID=', @ID, ' not found')

	ELSE IF EXISTS(SELECT * FROM [Company.Data] WHERE IndustryID = @ID)
        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Industry exist in Company.Data table. IndustryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END");
            migrationBuilder.Sql(@"SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Name: [Origin.Action.CanDelete]
--
-- Description: This procedure checks if the origin is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Origin.Action.CanDelete] @BID=1, @ID=1005, @ShowCantDeleteReason=1
-- ========================================================
ALTER PROCEDURE [dbo].[Origin.Action.CanDelete]
		  @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @ID)
		SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Invalid Origin Specified. OriginID=', @ID, ' not found')

	ELSE IF EXISTS(SELECT * FROM [Company.Data] WHERE OriginID = @ID)
            --AND NOT EXISTS(SELECT * FROM [Order.Data] WHERE OriginID = @OriginID)
        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Origin exist in Company.Data table. OriginID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END");
        }
    }
}

