using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6862_Fixed_the_issue_on_FK_relationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Profile.Table_Part.Assembly.Table_AssemblyTableBID_AssemblyTableID",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Profile.Table_Part.Machine.Profile_MachineProfileBID1_MachineProfileID1",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Machine.Profile.Table_AssemblyTableBID_AssemblyTableID",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Machine.Profile.Table_MachineProfileBID1_MachineProfileID1",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.DropColumn(
                name: "AssemblyTableBID",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.DropColumn(
                name: "AssemblyTableID",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.DropColumn(
                name: "MachineProfileBID1",
                table: "Part.Machine.Profile.Table");

            migrationBuilder.DropColumn(
                name: "MachineProfileID1",
                table: "Part.Machine.Profile.Table");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "AssemblyTableBID",
                table: "Part.Machine.Profile.Table",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "AssemblyTableID",
                table: "Part.Machine.Profile.Table",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "MachineProfileBID1",
                table: "Part.Machine.Profile.Table",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MachineProfileID1",
                table: "Part.Machine.Profile.Table",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Table_AssemblyTableBID_AssemblyTableID",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "AssemblyTableBID", "AssemblyTableID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Table_MachineProfileBID1_MachineProfileID1",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "MachineProfileBID1", "MachineProfileID1" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Profile.Table_Part.Assembly.Table_AssemblyTableBID_AssemblyTableID",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "AssemblyTableBID", "AssemblyTableID" },
                principalTable: "Part.Assembly.Table",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Profile.Table_Part.Machine.Profile_MachineProfileBID1_MachineProfileID1",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "MachineProfileBID1", "MachineProfileID1" },
                principalTable: "Part.Machine.Profile",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
