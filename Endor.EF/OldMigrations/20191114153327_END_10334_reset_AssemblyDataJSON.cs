﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_10334_reset_AssemblyDataJSON : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [Order.Item.Component]
                SET AssemblyDataJSON = '{""Quantity"":{""Name"":""Quantity"",""Value"":0,""ValueOV"":false},""Tier"":{""Name"":""Tier"",""Value"":""Default"",""ValueOV"":false},""Price"":{""Name"":""Price"",""Value"":0,""ValueOV"":false}}'            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //no rollback query since we are resetting data
        }
    }
}
