using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180723200344_UpdateCompanyOptionDefinitions")]
    public partial class UpdateCompanyOptionDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [System.Option.Definition] SET Description = 'Check this box to allow the same company name to be used more than one time. Having duplicate company names can cause some confusion for users, including the possibility of orders entered for the wrong company.

If it is not checked, users will still be notified if they are entering a new company using a name that already exists in the system.' WHERE Name ='Company.AllowDuplicateName';

    UPDATE [System.Option.Definition] SET Description = 'Check this box to prevent a Company from being created without an Origin. This will not prevent Companies from being imported without a valid Origin, but those Companies must have an Origin entered during the first edit or when the first order is entered.' WHERE Name ='Company.Require.Origin';

    UPDATE [System.Option.Definition] SET Description = 'Check this box to prevent a Company from being created without an Industry. This will not prevent Companies from being imported without a valid Industry, 
but those Companies must have an Industry entered during the first edit or when the first order is entered.' WHERE Name ='Company.Require.Industry';

    UPDATE [System.Option.Definition] SET Description = 'If this box is checked, the ""PO Required by Default"" checkbox will be set initially when a new Company is created.The user may still uncheck the option.' WHERE Name ='Company.Default.PORequired';

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
    UPDATE [System.Option.Definition] SET Description = '' WHERE Name ='Company.AllowDuplicateName';
    UPDATE [System.Option.Definition] SET Description = '' WHERE Name ='Company.Require.Origin';
    UPDATE [System.Option.Definition] SET Description = '' WHERE Name ='Company.Require.Industry';
    UPDATE [System.Option.Definition] SET Description = '' WHERE Name ='Company.Default.PORequired';");

        }
    }
}

