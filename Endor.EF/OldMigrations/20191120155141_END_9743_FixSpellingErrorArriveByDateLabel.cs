﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_9743_FixSpellingErrorArriveByDateLabel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ArriveByLabel",
                table: "Part.Subassembly.Data",
                newName: "ArriveByDateLabel");

            migrationBuilder.Sql(@"ALTER TABLE dbo.[Part.SubAssembly.Data]
                                   ALTER COLUMN ArriveByDateLabel NVARCHAR(50) SPARSE NULL;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ArriveByDateLabel",
                table: "Part.Subassembly.Data",
                newName: "ArriveByLabel");
        }
    }
}
