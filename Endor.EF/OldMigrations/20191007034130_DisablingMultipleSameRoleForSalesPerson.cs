﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class DisablingMultipleSameRoleForSalesPerson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Employee.Role]
SET AllowMultiple = 0, AllowOnTeam = 1
WHERE ID = 1;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Employee.Role]
SET AllowMultiple = 1, AllowOnTeam = 1
WHERE ID = 1;");
        }
    }
}
