using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180713124841_UpdateSectionsAndCategories")]
    public partial class UpdateSectionsAndCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- Fix Category Sections
UPDATE [System.Option.Category] SET SectionID = 710 WHERE ID IN (711, 712);
UPDATE [System.Option.Category] SET SectionID = 730 WHERE ID IN (731, 732);
-- Fix typo
UPDATE [System.Option.Category] SET [Name] = 'QuickBooks Sync', [Description] = 'QuickBooks Sync', [SearchTerms] = 'QuickBooks Sync' WHERE ID = 731;

-- Add New Sections
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms]) 
VALUES 
      (2300, N'Themes', 2000, 0, 1, NULL)
;

-- Add New and Missing Categories
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms]) 
VALUES 
      (401, 'Artwork Options', 400, '401', 2, 0, 'Artwork Options')
    , (402, 'Destination Options', 400, '402', 2, 0, 'Destination Options')
    , (403, 'Line Item Statuses', 400, '403', 2, 0, 'Line Item Statuses')
    , (404, 'Line Item Substatuses', 400, '404', 2, 0, 'Line Item Substatuses')
    , (405, 'Time Clock Options', 400, '405', 2, 0, 'Time Clock Options')
    , (406, 'Time Clock Activities', 400, '406', 2, 0, 'Time Clock Activities')
    , (407, 'Time Clock Breaks', 400, '407', 2, 0, 'Time Clock Breaks')
    , (501, 'Inventory Options', 500, '501', 2, 0, 'Inventory Options')
    , (2301, 'Theme', 2300, '2301', 2, 0, 'Theme')
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Category] WHERE ID IN (2301,501,407,406,405,404,403,402,401);
DELETE FROM [System.Option.Section] WHERE ID = 2300;
UPDATE [System.Option.Category] SET SectionID = 700 WHERE ID IN (711, 712);
UPDATE [System.Option.Category] SET SectionID = 700 WHERE ID IN (731, 732);
UPDATE [System.Option.Category] SET [Name] = 'QickBooks Sync', [Description] = 'QickBooks Sync', [SearchTerms] = 'QickBooks Sync' WHERE ID = 731;
");
        }
    }
}

