using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddDashboardWidgetDefinition004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.Definition] WHERE [ID] = 4)
BEGIN
	INSERT INTO [System.Dashboard.Widget.Definition]
	([ID], [ModifiedDT], [DefaultName], [DefaultRows], [DefaultCols], [DefaultOptions], [DefaultRefreshIntervalInMin], [Description], [Modules], [SecurityRightID], [MinRows], [MaxRows], [MinCols], [MaxCols], [HasImage])
	VALUES (4, CAST(N'2018-10-01T18:15:14.7900000' AS DateTime2), N'Current Order Status ', 1, 2, NULL, 15, N'', NULL, NULL, 1, 2, 2, 4, 1);
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 4 AND [CategoryType] = 1)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (4,1)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 4 AND [CategoryType] = 2)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (4,2)
END

IF NOT EXISTS(SELECT TOP 1 * FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 4 AND [CategoryType] = 4)
BEGIN
	INSERT INTO [System.Dashboard.Widget.CategoryLink] (WidgetDefID, CategoryType)
	VALUES (4,4)
END
            ");

            migrationBuilder.Sql(@"
/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.004]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Current Order Status 
    The Current Order Status Widget displays a graph of the number of orders in each non-closed status.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/997588993/Dashboard+Widget+Current+Order+Status
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StatusID VARCHAR(255) = '21, 22, 23, 24, 25';  -- Pre-WIP and WIP from https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/595329164/OrderOrderStatus+Enum 

    EXEC dbo.[Dashboard.Widget.Definition.004] 
        @BID = @BID, @LocationID = @LocationID,
        @StatusID = @StatusID
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.004]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StatusID VARCHAR(255) = '(21), (22), (23), (24), (25)'
AS
BEGIN

    --  --- There are helpful for testing
    --   DECLARE @BID SMALLINT = 1;
    --   DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --   DECLARE @StatusID VARCHAR(255) = '21, 22, 23, 24, 25';
    --   DECLARE @AsTable BIT = 0;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    
    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    SELECT @LocationID [LocationID], OSE.ID, OSE.Name, COUNT(O.ID) [Count], ISNULL(SUM(O.[Price.PreTax]),0.0) [Amount]

    FROM [Order.Data] O

    FULL OUTER JOIN 
    ( SELECT CONVERT(TINYINT, value) [ID] from String_Split(@StatusID, ',') ) OS ON O.BID = @BID AND O.OrderStatusID = OS.ID

    JOIN [enum.Order.OrderStatus] OSE ON OS.ID = OSE.ID

    WHERE (@LocationID IS NULL OR O.LocationID IS NULL OR O.LocationID = @LocationID)
    GROUP BY OSE.ID, OSE.Name
    ORDER BY 1 DESC, 2
    FOR JSON PATH
    ;

END

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM[System.Dashboard.Widget.CategoryLink] WHERE WidgetDefID = 4
                DELETE FROM [System.Dashboard.Widget.Definition] WHERE ID=4
            ");
            migrationBuilder.Sql(@"
                DROP PROCEDURE [Dashboard.Widget.Definition.004]
            ");
        }
    }
}
