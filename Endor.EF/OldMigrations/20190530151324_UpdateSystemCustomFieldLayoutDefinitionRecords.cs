using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSystemCustomFieldLayoutDefinitionRecords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
MERGE [CustomField.Layout.Definition] AS Target
USING
(
    SELECT B.BID, NewData.*
    FROM [Business.Data] B
    JOIN (
            SELECT 61 [ID], 1 [IsActive], 1 [IsSystem], 12000 [AppliesToClassTypeID], 1 [IsAllTab], 1 [IsSubTab], 1 [SortIndex], 'All' [Name] -- Material
            UNION ALL SELECT 71,  1, 1, 12020, 1, 1, 1, 'All' -- Labor
            UNION ALL SELECT 81,  1, 1, 12030, 1, 1, 1, 'All' -- Machine
            UNION ALL SELECT 111, 1, 1, 12040, 1, 1, 1, 'All'  -- Assembly
         ) AS NewData ON 1=1
) AS Source
ON Target.BID = Source.BID AND Target.ID = Source.ID
  
WHEN NOT MATCHED BY TARGET THEN
   INSERT (BID, ID, IsActive, IsSystem, AppliesToClassTypeID, IsAllTab, IsSubTab, SortIndex, Name)
   VALUES (Source.BID, Source.ID, Source.IsActive, Source.IsSystem, Source.AppliesToClassTypeID, Source.IsAllTab, Source.IsSubTab, Source.SortIndex, Source.Name)
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
