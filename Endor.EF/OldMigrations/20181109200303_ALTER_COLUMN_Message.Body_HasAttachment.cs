using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class ALTER_COLUMN_MessageBody_HasAttachment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "HasAttachment",
                table: "Message.Body",
                nullable: false,
                computedColumnSql: "(isnull(case when [AttachedFileCount]>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,CONVERT([bit],(0))))",
                oldClrType: typeof(bool));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "HasAttachment",
                table: "Message.Body",
                nullable: false,
                oldClrType: typeof(bool),
                oldComputedColumnSql: "(isnull(case when [AttachedFileCount]>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,CONVERT([bit],(0))))");
        }
    }
}
