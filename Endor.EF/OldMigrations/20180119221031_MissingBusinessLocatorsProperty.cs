using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180119221031_MissingBusinessLocatorsProperty")]
    public partial class MissingBusinessLocatorsProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {         
            migrationBuilder.AddForeignKey(
                name: "FK_Business.Locator_Business.Data",
                table: "Business.Locator",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Business.Locator_Business.Data",
                table: "Business.Locator");
            
        }
    }
}

