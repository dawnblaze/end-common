using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180516210409_Add_View_IndexAndColumns_END-1327")]
    public partial class Add_View_IndexAndColumns_END1327 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    DROP VIEW IF EXISTS [dbo].[IndexAndColumns];
                    GO

                    CREATE VIEW [dbo].[IndexAndColumns] as 
                    SELECT TOP 100 PERCENT
                          t.name AS TableName
                        , i.name AS IndexName
                        , i.type_desc AS IndexType
                        , i.is_primary_key as IsPrimary
                        , i.is_unique as IsUnique
                        , i.has_filter as HasFilter
                        , i.filter_definition AS Filter
                        , c.name as ColumnName
                        , ic.index_column_id as IndexColumnNumber
                        , ic.is_descending_key as IsDescending
                        , ic.is_included_column as IsIncluded
                        , i.index_id as IndexID
                        , i.object_id as IndexObjectID
                    FROM sys.tables t 
                    JOIN sys.indexes i on i.object_id = t.object_id
                    JOIN sys.index_columns ic on ic.object_id = t.object_id and ic.index_id = i.index_id
                    JOIN sys.columns c on c.object_id = t.object_id and ic.column_id = c.column_id 

                    WHERE i.index_id > 0    
                        and i.type in (1, 2) -- clustered & nonclustered only
                        and i.is_unique_constraint = 0 -- do not include UQ
                        and i.is_hypothetical = 0
                        and ic.key_ordinal > 0
                        and t.is_ms_shipped = 0

                     Order By TableName, IndexID, IndexColumnNumber
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    DROP VIEW IF EXISTS [dbo].[IndexAndColumns];
                    GO
                "
            );
        }
    }
}

