using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSubAssembly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Subassembly.Category.SimpleList]
                GO
                CREATE VIEW [dbo].[Part.Subassembly.Category.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                    , [IsActive]
                    , CONVERT(BIT, 0) AS[HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[Part.Subassembly.Category];
                GO
            ");

            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Subassembly.SimpleList]
                GO
                CREATE VIEW [dbo].[Part.Subassembly.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                    , [IsActive]
                    , CONVERT(BIT, 0) AS[HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[Part.Subassembly.Data];
                GO
            ");

            migrationBuilder.Sql(@"
                /****** Object:  StoredProcedure [dbo].[Part.Subassembly.Category.Action.CanDelete]    Script Date: 4/3/2018 7:41:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
========================================================
    Name: [Part.Subassembly.Category.Action.CanDelete]

    Description: This procedure checks if the SubassemblyCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Subassembly.Category.Action.CanDelete] @BID=1, @ID=1, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Subassembly.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the SubassemblyCategory can be deleted. The boolean response is returned in the body.  A SubassemblyCategory can be deleted if
	- there are no subassemblies linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Subassembly.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Subassembly Category Specified Does not Exist. SubassemblyCategoryID=', @ID)

    -- there are no subassemblies linked to it or if the force option is specified to deletes the links.
	ELSE IF  EXISTS( SELECT * FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Subassembly Category is linked to a Subassembly Data. SubassemblyCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
/****** Object:  StoredProcedure [dbo].[Part.Subassembly.Category.Action.SetActive]    Script Date: 4/3/2018 7:53:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Part.Subassembly.Category.Action.SetActive]
--
-- Description: This procedure sets the SubassemblyCategory to active or inactive
--
-- Sample Use:   EXEC dbo.[Part.Subassembly.Category.Action.SetActive] @BID=1, @SubassemblyCategoryID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Subassembly.Category.Action.SetActive]
-- DECLARE 
          @BID                  TINYINT  -- = 1
        , @SubassemblyCategoryID    SMALLINT -- = 2

        , @IsActive             BIT     = 1

        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the SubassemblyCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Subassembly.Category] WHERE BID = @BID and ID = @SubassemblyCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid SubassemblyCategory Specified. SubassemblyCategoryID='+CONVERT(VARCHAR(12),@SubassemblyCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Part.Subassembly.Category] L
    WHERE BID = @BID and ID = @SubassemblyCategoryID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
GO
/****** Object:  StoredProcedure [dbo].[Part.Subassembly.Category.Action.LinkSubassembly]    Script Date: 4/3/2018 7:59:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Part.Subassembly.Category.Action.LinkSubassembly]
--
-- Description: This procedure links/unlinks the SubassemblyData to the SubassemblyCategory
--
-- Sample Use:   EXEC dbo.[Part.Subassembly.Category.Action.LinkSubassembly] @BID=1, @SubassemblyCategoryID=1, @SubassemblyDataID=1, @IsLinked=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Subassembly.Category.Action.LinkSubassembly]
--DECLARE 
          @BID                  TINYINT  --= 1
        , @SubassemblyCategoryID    SMALLINT --= 2
		, @SubassemblyDataID        INT --= 1
        , @IsLinked             BIT     = 1
        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the SubassemblyCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Subassembly.Category] WHERE BID = @BID and ID = @SubassemblyCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid SubassemblyCategory Specified. SubassemblyCategoryID='+CONVERT(VARCHAR(12),@SubassemblyCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the SubassemblyData specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Subassembly.Data] WHERE BID = @BID and ID = @SubassemblyDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid SubassemblyData Specified. SubassemblyDataID='+CONVERT(VARCHAR(12),@SubassemblyDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.SubassemblyDataLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID and CategoryID = @SubassemblyCategoryID and PartID = @SubassemblyDataID)
		BEGIN
			INSERT INTO [Part.Subassembly.CategoryLink] (BID, CategoryID, PartID)
			VALUES (@BID, @SubassemblyCategoryID, @SubassemblyDataID)
		END;

	END
	ELSE
	BEGIN
		-- Remove entry from Part.Subassembly.CategoryLink
		DELETE FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID and CategoryID = @SubassemblyCategoryID and PartID = @SubassemblyDataID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Subassembly.Category.SimpleList]
            ");

            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Subassembly.SimpleList]
            ");

            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Subassembly.Category.Action.LinkSubassembly]");
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Subassembly.Category.Action.SetActive]");
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Subassembly.Category.Action.CanDelete]");
        }


    }
}
