using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180718212453_END_1857_Data_Insert")]
    public partial class END_1857_Data_Insert : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (0, N'None')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (1, N'Employee')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (9, N'Employee Administrator')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (11, N'Contact')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (19, N'Contact Administrator')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (21, N'Franchise Staff')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (29, N'Franchise Admin')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (41, N'Support Staff')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (49, N'Support Manager')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (51, N'Developer Staff')
                    INSERT [dbo].[enum.User.Access.Type] ([ID], [Name]) VALUES (59, N'Developer Staff')
                "
            );

            migrationBuilder.Sql
            (
                @"
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 0, N'None', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 1, N'Employee', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 9, N'Employee Administrator', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 11, N'Contact', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 19, N'Contact Administrator', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 100, N'Login', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 101, N'User Setup', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 140, N'Order Entry', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 141, N'Order Adjustment', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 150, N'Payment Entry', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 151, N'Payment Adjustment', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (1, 1001, N'Scott St.Cyr', 0)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 0, N'None', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 1, N'Employee', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 9, N'Employee Administrator', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 11, N'Contact', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 19, N'Contact Administrator', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 100, N'Login', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 101, N'User Setup', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 140, N'Order Entry', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 141, N'Order Adjustment', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 150, N'Payment Entry', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 151, N'Payment Adjustment', 1)
                    INSERT [dbo].[Rights.Group] ([BID], [ID], [Name], [IsSystem]) VALUES (2, 1001, N'Scott St.Cyr', 0)
                "
            );

            migrationBuilder.Sql
            (
                @"
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (1, 1, 100)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (1, 9, 100)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (1, 9, 101)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (1, 9, 140)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (1, 9, 141)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (1, 9, 150)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (1, 9, 151)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (1, 11, 100)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (2, 1, 100)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (2, 9, 100)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (2, 9, 101)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (2, 9, 140)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (2, 9, 141)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (2, 9, 150)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (2, 9, 151)
                    INSERT [dbo].[Rights.Group.ChildGroupLink] ([BID], [ParentGroupID], [ChildGroupID]) VALUES (2, 11, 100)
                "
            );

            migrationBuilder.Sql
            (
                @"
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (1, 100, 1)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (1, 100, 2)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (1, 100, 7)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (1, 101, 8)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (1, 101, 190)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (1, 101, 300)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (2, 100, 1)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (2, 100, 2)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (2, 100, 7)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (2, 101, 8)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (2, 101, 190)
                    INSERT [dbo].[Rights.Group.RightLink] ([BID], [RightsGroupID], [RightID]) VALUES (2, 101, 300)
                "
            );

            migrationBuilder.Sql
            (
                @"
                    DECLARE @EmpID INT = 101;
                    IF ((SELECT [ID] FROM [Employee.Data] WHERE ID = @EmpID) = @EmpID)
                        INSERT [dbo].[User.Link] ([BID],[ID], [UserID], [EmployeeID], [ContactID], [UserAccessType]) VALUES (1, 1, 1, 101, NULL, 1)
                    SET @EmpID = 102;
                    IF ((SELECT [ID] FROM [Employee.Data] WHERE ID = @EmpID) = @EmpID)
                        INSERT [dbo].[User.Link] ([BID],[ID], [UserID], [EmployeeID], [ContactID], [UserAccessType]) VALUES (1, 2, 2, 102, NULL, 1)
                    SET @EmpID = 102;
                    IF ((SELECT [ID] FROM [Employee.Data] WHERE ID = @EmpID) = @EmpID)
                        INSERT [dbo].[User.Link] ([BID],[ID], [UserID], [EmployeeID], [ContactID], [UserAccessType]) VALUES (1, 3, 3, 103, NULL, 9)
                    SET @EmpID = 102;
                    IF ((SELECT [ID] FROM [Employee.Data] WHERE ID = @EmpID) = @EmpID)
                        INSERT [dbo].[User.Link] ([BID],[ID], [UserID], [EmployeeID], [ContactID], [UserAccessType]) VALUES (1, 4, 4, 104, NULL, 51)
                    SET @EmpID = 102;
                    IF ((SELECT [ID] FROM [Employee.Data] WHERE ID = @EmpID) = @EmpID)
                        INSERT [dbo].[User.Link] ([BID],[ID], [UserID], [EmployeeID], [ContactID], [UserAccessType]) VALUES (2, 1, 1, 105, NULL, 1)
                    SET @EmpID = 102;
                    IF ((SELECT [ID] FROM [Employee.Data] WHERE ID = @EmpID) = @EmpID)
                        INSERT [dbo].[User.Link] ([BID],[ID], [UserID], [EmployeeID], [ContactID], [UserAccessType]) VALUES (2, 2, 2, 105, NULL, 59)
                    SET @EmpID = 102;
                    IF ((SELECT [ID] FROM [Employee.Data] WHERE ID = @EmpID) = @EmpID)
                        INSERT [dbo].[User.Link] ([BID],[ID], [UserID], [EmployeeID], [ContactID], [UserAccessType]) VALUES (2, 3, 4, 106, NULL, 51)
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  UserDefinedFunction [dbo].[User.Security.Rights.String]    Script Date: 7/17/2018 6:16:15 PM ******/
                    SET ANSI_NULLS ON
                    GO
                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- 
                    -- Name: User.Security.Rights.String( BID int, UserID int )
                    --
                    -- Description: This Table Value Function returns a list of Rights associated
                    -- with a specific user for a given business id.
                    --
                    -- Sample Use:   select dbo.[User.Security.Rights.String]( 1, 100 )
                    --
                    -- ========================================================
                    ALTER FUNCTION [dbo].[User.Security.Rights.String] ( @BID int, @UserID int )
                    RETURNS BINARY(128)
                    AS
                    BEGIN
                        DECLARE @ResultSize SMALLINT = 128
                              , @Found BIT = 0
                              , @Results BINARY(128) = 0; 

                        DECLARE @Temp Table (RightID smallint, ByteIndex tinyint, BytePos tinyint, ByteFlag tinyint);

                            SELECT @Results = 0x0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000086;
        
                            RETURN @Results;
                    END;
                "
            );

            migrationBuilder.Sql
            (
                @"
                    UPDATE [dbo].[enum.ClassType]
                    SET 
                    [Name] = 'Rights Group'
                    , [TableName] = 'Rights.Group'
                    WHERE ID = 1101

                    UPDATE [dbo].[enum.ClassType]
                    SET 
                    [Name] = 'Rights Group List'
                    , [TableName] = 'Rights.Group.List'
                    WHERE ID = 1100

                    INSERT INTO [dbo].[enum.ClassType]
                    (ID, [Name], [TableName])
                    VALUES
                    (1012, 'User Link', 'User.Link')
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql
            (
                @"
                    DELETE FROM [dbo].[enum.User.Access.Type]
                "
            );

            migrationBuilder.Sql
            (
                @"
                    DELETE FROM [dbo].[Rights.Group]
                "
            );

            migrationBuilder.Sql
            (
                @"
                    DELETE FROM [dbo].[Rights.Group.ChildGroupLink]
                "
            );

            migrationBuilder.Sql
            (
                @"
                    DELETE FROM [dbo].[Rights.Group.RightLink]
                "
            );

            migrationBuilder.Sql
            (
                @"
                    DELETE FROM [dbo].[User.Link]
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  UserDefinedFunction [dbo].[User.Security.Rights.List]    Script Date: 7/18/2018 3:19:20 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- 
                    -- Name: User.Security.Rights.List( BID int, UserID int )
                    --
                    -- Description: This Table Value Function returns a list of Rights associated
                    -- with a specific user for a given business id.
                    --
                    -- Sample Use:   select * from [User.Security.Rights.List] ( 1, 100 )
                    --
                    -- ========================================================
                    ALTER FUNCTION [dbo].[User.Security.Rights.List] ( @BID int, @UserID int )
                    RETURNS TABLE
                    AS
                    RETURN
                    (
                        WITH Rights(BID, GroupID)
                        AS
                        (
                            -- Anchor Query

                            -- Pull the users ROLE security group
                            SELECT SRG.BID, SRG.ID
                            FROM [User.Link] U
                            JOIN [Security.Role.Link] SRoL on SRoL.BID = U.BID AND SRoL.RoleType = U.RoleType
                            JOIN [Security.Right.Group] SRG on SRG.BID = SRoL.BID AND SRG.ID = SRoL.RightGroupID
                            WHERE U.BID = @BID AND U.UserID = @UserID

                            -- Merge with the users top-level security group
                            UNION ALL 
                            SELECT SRG.BID, SRG.ID
                            FROM [User.Link] U
                            JOIN [Security.Right.Group] SRG on SRG.BID = U.BID AND SRG.ID = U.RightGroupID
                            LEFT JOIN [Security.Right.Link] SRiL on SRiL.BID = SRG.BID AND SRiL.RightGroupID = SRG.ID
                            WHERE U.BID = @BID AND U.UserID = @UserID

                            -- Now merge with recursively with Security Groups
                            UNION ALL
                            SELECT G.BID, G.ID
                            FROM Rights R 
                            JOIN [Security.Right.Group.Link] L ON L.BID = R.BID AND L.ParentGroupID = R.GroupID
                            JOIN [Security.Right.Group] G ON G.BID = L.BID AND G.ID = L.ChildGroupID
                        )
                            SELECT DISTINCT RL.BID, RL.RightID 
                            FROM Rights R
                            JOIN [Security.Right.Link] RL on RL.BID = R.BID and RL.RightGroupID = R.GroupID
                    );
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    UPDATE [dbo].[enum.ClassType]
                    SET 
                    [Name] = 'Security Group'
                    , [TableName] = 'Security.Right.Group'
                    WHERE ID = 1101

                    UPDATE [dbo].[enum.ClassType]
                    SET 
                    [Name] = 'Security Right'
                    , [TableName] = ''
                    WHERE ID = 1100

                    DELETE FROM [dbo].[enum.ClassType]
                    WHERE ID = 1012
                "
            );
        }
    }
}

