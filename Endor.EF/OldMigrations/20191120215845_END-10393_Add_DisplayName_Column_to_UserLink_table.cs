using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20191120215845_END-10393_Add_DisplayName_Column_to_UserLink_table")]
    public partial class Add_DisplayName_Column_to_UserLink_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "User.Link",
                type: "varchar(100)",
                nullable: true);

            //Renumber StatusIndex
            migrationBuilder.Sql(@"
                WITH EmployeeDataShortName AS (
                      SELECT
                         EDATA.ShortName,
                         EDATA.ID as EMPLOYEEREFID
                      FROM
                        dbo.[Employee.Data] EDATA
                        INNER JOIN dbo.[User.Link] ULink on ULink.EmployeeID = EDATA.ID
                     )
					 UPDATE [User.Link] 
					    set [User.Link].DisplayName = EmpData.ShortName 
				     FROM 
                        [User.Link] 
					   INNER JOIN EmployeeDataShortName EmpData 
                          ON [User.Link].EmployeeID =  EmpData.EMPLOYEEREFID
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "User.Link");
        }
    }
}

