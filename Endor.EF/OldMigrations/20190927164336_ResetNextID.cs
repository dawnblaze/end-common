﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class ResetNextID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {   

            migrationBuilder.Sql(@"UPDATE [Order.Data]
                SET InvoiceNumber = IIF(OrderStatusID < 25 , NULL, Number);");

            migrationBuilder.Sql(@"UPDATE N
                SET NextID = 1 + (SELECT COALESCE(Max(O.Number), 1000) as OrderNumber
                                    FROM[Order.Data] O
                                   WHERE O.BID = N.BID)
                FROM[Util.NextID] N
                WHERE ClassTypeID IN(-10000, -10200);");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
