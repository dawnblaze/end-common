using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddReceivedLocationID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
TRUNCATE TABLE [Accounting.GL.Data];
ALTER TABLE [Accounting.Payment.Application] SET (SYSTEM_VERSIONING = OFF); 
ALTER TABLE [Accounting.Payment.Master] SET (SYSTEM_VERSIONING = OFF); 
Delete [Accounting.Payment.Application];
Delete [Accounting.Payment.Master];
ALTER TABLE [Accounting.Payment.Application] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Accounting.Payment.Application])); 
ALTER TABLE [Accounting.Payment.Master] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Accounting.Payment.Master])); 
");

            migrationBuilder.AddColumn<byte>(
                name: "ReceivedLocationID",
                table: "Accounting.Payment.Master",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "ReceivedLocationID",
                table: "Accounting.Payment.Application",
                nullable: false,
                defaultValue: (byte)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReceivedLocationID",
                table: "Accounting.Payment.Master");

            migrationBuilder.DropColumn(
                name: "ReceivedLocationID",
                table: "Accounting.Payment.Application");
        }
    }
}
