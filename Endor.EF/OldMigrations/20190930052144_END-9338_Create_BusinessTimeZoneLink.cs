﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9338_Create_BusinessTimeZoneLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Business.TimeZone.Link",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    TimeZoneID = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business.TimeZone.Link", x => new { x.BID, x.TimeZoneID });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Business.TimeZone.Link");
        }
    }
}
