using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180501154151_INSERT_TABLE_Order_Item_Status")]
    public partial class INSERT_TABLE_Order_Item_Status : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"  
            
                if not exists (SELECT ID from [Order.Item.Status] where ID = 16 and name = 'Approved')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 16, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 1, 16, 50, N'Approved', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 18 and name = 'Lost')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 18, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 1, 18, 50, N'Lost', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 19 and name = 'Voided')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 19, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 1, 19, 50, N'Voided', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 23 and name = 'Built')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 23, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 2, 23, 50, N'Built', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 24 and name = 'Invoicing')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 24, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 2, 24, 50, N'Invoicing', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 25 and name = 'Invoiced')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 25, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 2, 25, 50, N'Invoiced', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 26 and name = 'Closed')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 26, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 2, 26, 50, N'Closed', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 29 and name = 'Voided')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 29, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 2, 29, 50, N'Voided', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 43 and name = 'Approved')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 43, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 4, 43, 50, N'Approved', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 45 and name = 'Ordered')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 45, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 4, 45, 50, N'Ordered', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 46 and name = 'Received')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 46, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 4, 46, 50, N'Received', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 49 and name = 'Voided')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 49, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 4, 49, 50, N'Voided', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 61 and name = 'Pending')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 61, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 64, 61, 50, N'Pending', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 63 and name = 'Ready')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 63, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 64, 63, 50, N'Ready', NULL, NULL)

                if not exists (SELECT ID from [Order.Item.Status] where ID = 65 and name = 'Completed')
                    INSERT INTO [Order.Item.Status] ([BID], [ID], [ModifiedDT], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description])
                    VALUES (1, 65, CAST(N'2018-04-09T20:03:41.1350506' AS DateTime2), 1, 1, 0, 64, 66, 50, N'Completed', NULL, NULL)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [Order.Item.Status]
            ");
        }
    }
}

