﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9177AlterCustomFieldLayoutContainer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "ElementType",
                table: "CustomField.Layout.Container",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<int>(
                name: "ParentID",
                table: "CustomField.Layout.Container",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Container_ElementType",
                table: "CustomField.Layout.Container",
                column: "ElementType");

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Container_BID_ParentID",
                table: "CustomField.Layout.Container",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Layout.Container_enum.Part.Subassembly.ElementType",
                table: "CustomField.Layout.Container",
                column: "ElementType",
                principalTable: "enum.Part.Subassembly.ElementType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Layout.Container_CustomField.Layout.Container",
                table: "CustomField.Layout.Container",
                columns: new[] { "BID", "ParentID" },
                principalTable: "CustomField.Layout.Container",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Layout.Container_enum.Part.Subassembly.ElementType",
                table: "CustomField.Layout.Container");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Layout.Container_CustomField.Layout.Container",
                table: "CustomField.Layout.Container");

            migrationBuilder.DropIndex(
                name: "IX_CustomField.Layout.Container_ElementType",
                table: "CustomField.Layout.Container");

            migrationBuilder.DropIndex(
                name: "IX_CustomField.Layout.Container_BID_ParentID",
                table: "CustomField.Layout.Container");

            migrationBuilder.DropColumn(
                name: "ElementType",
                table: "CustomField.Layout.Container");

            migrationBuilder.DropColumn(
                name: "ParentID",
                table: "CustomField.Layout.Container");
        }
    }
}
