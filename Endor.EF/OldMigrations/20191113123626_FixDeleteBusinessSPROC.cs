﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class FixDeleteBusinessSPROC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
    This procedure deletes an ENTIRE BUSINESS from the database.
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.Business.Delete] @BID = 999, @TestOnly = 1 ;

*/
ALTER PROCEDURE [dbo].[Util.Business.Delete]( @BID SMALLINT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteBusiness]
    BEGIN TRY

	    -- Turn off all referential integrity
		PRINT 'Turn off all referential integrity';
		EXEC sp_MSforeachtable 'ALTER TABLE ? NOCHECK CONSTRAINT all';

        PRINT '';

            DECLARE @IsTemporal bit;
            DECLARE @TableName VARCHAR(255);
            DECLARE @HistoricTableName VARCHAR(255);
            DECLARE @SQL NVARCHAR(4000);
            DECLARE CUR CURSOR FOR

        SELECT t.[name]
			 , IIF(t.[temporal_type] = 2, 1, 0) IsTemporal
			 , OBJECT_NAME(t.history_table_id) HistoricTableName
        FROM sys.columns c

             JOIN sys.tables t ON c.object_id = t.object_id

        WHERE c.[name] = N'BID' AND t.[temporal_type] != 1

        ORDER BY t.[name]

        OPEN CUR
        FETCH NEXT FROM CUR INTO @TableName, @IsTemporal, @HistoricTableName


        WHILE @@FETCH_STATUS = 0

        BEGIN
            PRINT '*** ' + @TableName + ' ***';
        PRINT '';
  
			-- Turn off system versioning if it's a temporal table
			IF(@IsTemporal = 1)

            BEGIN
				-- Turn OFF system versioning

                SET @SQL = 'ALTER TABLE [' + @TableName + '] SET (SYSTEM_VERSIONING = OFF);'

                EXEC Sp_executesql @SQL;
				PRINT '';

				SET @SQL = 'DELETE FROM [' + @TableName + '] WHERE BID = ' + CAST(@BID AS varchar) + ';'

                EXEC Sp_executesql @SQL;
				PRINT '';
    
				-- Turn ON system versioning

                SET @SQL = 'ALTER TABLE [' + @TableName + '] SET( SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[' + @HistoricTableName + ']) );'

                EXEC Sp_executesql @SQL;
				PRINT '';
			END

            ELSE

            BEGIN
                SET @SQL = 'DELETE FROM ['+@TableName+'] WHERE BID = ' + CAST(@BID AS varchar) + ';'
				EXEC Sp_executesql @SQL;
				PRINT '';
			END

            FETCH NEXT FROM CUR INTO @TableName, @IsTemporal, @HistoricTableName
        END


        CLOSE CUR

        DEALLOCATE CUR

		-- Turn on referential integrity

        PRINT 'Turn on all referential integrity';
		EXEC sp_MSforeachtable 'ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all';
        PRINT '';

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteBusiness]
        RETURN;
        END;

        END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteBusiness];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [DeleteBusiness]

        END;


");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
