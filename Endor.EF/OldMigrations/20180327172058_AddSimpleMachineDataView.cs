using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180327172058_AddSimpleMachineDataView")]
    public partial class AddSimpleMachineDataView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Machine.SimpleList]
                GO
                CREATE VIEW [dbo].[Part.Machine.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                    , [IsActive]
                    , [HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[Part.Machine.Data];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Machine.SimpleList]
            ");
        }
    }
}

