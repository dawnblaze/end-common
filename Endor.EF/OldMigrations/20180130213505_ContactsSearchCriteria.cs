using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180130213505_ContactsSearchCriteria")]
    public partial class ContactsSearchCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [System.List.Filter.Criteria]
([TargetClassTypeID], [Name], [Label], [Field], [DataType], [InputType],[ListValues],[IsLimitToList],[SortIndex]) VALUES
(3000,'Name','Name','LongName',0,0,NULL,0,0)

INSERT INTO [System.List.Filter.Criteria]
([TargetClassTypeID], [Name], [Label], [Field], [DataType], [InputType],[ListValues],[IsLimitToList],[SortIndex]) VALUES
(3000,'Company','Company','Company',0,0,NULL,0,1)

INSERT INTO [System.List.Filter.Criteria]
([TargetClassTypeID], [Name], [Label], [Field], [DataType], [InputType],[ListValues],[IsLimitToList],[SortIndex]) VALUES
(3000,'Email','Email','EmailAddress',0,0,NULL,0,2)

INSERT INTO [System.List.Filter.Criteria]
([TargetClassTypeID], [Name], [Label], [Field], [DataType], [InputType],[ListValues],[IsLimitToList],[SortIndex]) VALUES
(3000,'PhoneNumber','Phone Number','PhoneNumber',0,0,NULL,0,3)

INSERT INTO [System.List.Filter.Criteria]
([TargetClassTypeID], [Name], [Label], [Field], [DataType], [InputType],[ListValues],[IsLimitToList],[SortIndex]) VALUES
(3000,'Status','Status','StatusID',1,5,NULL,0,4)

INSERT INTO [System.List.Filter.Criteria]
([TargetClassTypeID], [Name], [Label], [Field], [DataType], [InputType],[ListValues],[IsLimitToList],[SortIndex]) VALUES
(3000,'Is Active','Is Active','IsActive',3,2,'Is Not Active,Is Active',1,5)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.List.Filter.Criteria] WHERE [TargetClassTypeID] = 3000
");
        }
    }
}

