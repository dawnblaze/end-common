using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180614154021_END-1509_AddFlatListItem_And_FlatListType")]
    public partial class END1509_AddFlatListItem_And_FlatListType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.List.FlatListType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    IsAlphaSorted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.List.ReasonType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "List.FlatList.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1902))"),
                    FlatListType = table.Column<byte>(type: "tinyint", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsAdHoc = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    SortIndex = table.Column<short>(type: "smallint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_List.FlatList.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_List.FlatList.Data_BID",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_List.FlatList.Data_enum.List.FlatListType",
                        column: x => x.FlatListType,
                        principalTable: "enum.List.FlatListType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_List.FlatList.Data_FlatListDefault",
                table: "List.FlatList.Data",
                columns: new[] { "BID", "FlatListType", "IsActive", "SortIndex", "Name" },
                filter: "IsAdHoc = 0");

            migrationBuilder.CreateIndex(
                name: "IX_List.FlatList.Data_FlatListByName",
                table: "List.FlatList.Data",
                columns: new[] { "BID", "FlatListType", "Name", "IsActive", "SortIndex" },
                filter: "IsAdHoc = 0");

            migrationBuilder.Sql(
@"INSERT [enum.List.FlatListType] ([ID], [Name], [IsAlphaSorted])
VALUES (1, N'Tax Exempt Reasons', 1)
    , (2, N'Estimate Voided Reasons', 1)
    , (3, N'Order Voided Reasons', 1)
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "List.FlatList.Data");

            migrationBuilder.DropTable(
                name: "enum.List.FlatListType");
        }
    }
}

