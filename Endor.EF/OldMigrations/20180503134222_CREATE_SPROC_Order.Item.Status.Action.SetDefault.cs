using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180503134222_CREATE_SPROC_Order.Item.Status.Action.SetDefault")]
    public partial class CREATE_SPROC_OrderItemStatusActionSetDefault : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                -- ========================================================
                -- Name: [Order.Item.Status.Action.SetDefault]
                --
                -- Description: This procedure sets the IsDefault in Order.Item.Status table
                --
                -- Sample Use: EXEC dbo.[Order.Item.Status.Action.SetDefault] @BID=1, @OrderItemStatusID=65
                -- ========================================================
                CREATE PROCEDURE [dbo].[Order.Item.Status.Action.SetDefault]
                -- DECLARE 
                          @BID            TINYINT -- = 1
                        , @OrderItemStatusID     SMALLINT     -- = 2

                        , @IsDefault       BIT     = 1

                        , @Result         INT     = NULL  OUTPUT
                AS
                BEGIN
                    DECLARE @Message VARCHAR(1024);

                    -- Check if the option specified is valid
                    IF NOT EXISTS(SELECT * FROM [Order.Item.Status] WHERE ID = @OrderItemStatusID)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'The Order.Item.Status is not found.'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END;

                    -- Now update it
                    UPDATE O
                    SET IsDefault = 0
                    FROM [Order.Item.Status] O
                    WHERE BID = @BID and IsDefault = 1 AND ID != @OrderItemStatusID

                    UPDATE O
                    SET IsDefault = 1
                    FROM [Order.Item.Status] O
                    WHERE  BID = @BID and ID = @OrderItemStatusID


                    SET @Result = @@ROWCOUNT;

                    SELECT @Result as Result;
                END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [dbo].[Order.Item.Status.Action.SetDefault];
            ");
        }
    }
}

