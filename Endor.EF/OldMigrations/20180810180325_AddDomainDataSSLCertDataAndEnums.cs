using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180810180325_AddDomainDataSSLCertDataAndEnums")]
    public partial class AddDomainDataSSLCertDataAndEnums : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.DomainAccessType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.DomainAccessType", x => x.ID);
                });

            migrationBuilder.Sql(@"
INSERT INTO [enum.DomainAccessType] ([ID], [Name]) VALUES (1, 'Business');
INSERT INTO [enum.DomainAccessType] ([ID], [Name]) VALUES (2, 'Customer Portal');
INSERT INTO [enum.DomainAccessType] ([ID], [Name]) VALUES (3, 'ECommerce');
INSERT INTO [enum.DomainAccessType] ([ID], [Name]) VALUES (4, 'Vendor Portal');
");

            migrationBuilder.CreateTable(
                name: "enum.DomainStatus",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.DomainStatus", x => x.ID);
                });

            migrationBuilder.Sql(@"
INSERT INTO [enum.DomainStatus] ([ID], [Name]) VALUES (1, 'Pending');
INSERT INTO [enum.DomainStatus] ([ID], [Name]) VALUES (2, 'Online');
INSERT INTO [enum.DomainStatus] ([ID], [Name]) VALUES (3, 'Offline');
INSERT INTO [enum.DomainStatus] ([ID], [Name]) VALUES (4, 'Expired');
INSERT INTO [enum.DomainStatus] ([ID], [Name]) VALUES (5, 'Inactive');
");

            migrationBuilder.CreateTable(
                name: "SSLCertificate.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    CanHaveMultipleDomains = table.Column<bool>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "1030"),
                    CommonName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    FileName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    InstalledDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Thumbprint = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    ValidFromDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    ValidToDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SSLCertificate.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_SSLCertificate.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Domain.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AccessType = table.Column<byte>(type: "tinyint", nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "1004"),
                    DNSLastAttemptDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    DNSLastAttemptResult = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    DNSVerifiedDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    Domain = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    LocationID = table.Column<byte>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    SSLCertificateID = table.Column<short>(nullable: true),
                    SSLLastAttemptDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    SSLLastAttemptResult = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Status = table.Column<byte>(type: "tinyint", nullable: false),
                    Use3rdPartyCertificate = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Domain.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Domain.Data_enum.DomainAccessType",
                        column: x => x.AccessType,
                        principalTable: "enum.DomainAccessType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Data_enum.DomainStatus",
                        column: x => x.Status,
                        principalTable: "enum.DomainStatus",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Data_Location",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Domain.Data_SSLCertificate.Data",
                        columns: x => new { x.BID, x.SSLCertificateID },
                        principalTable: "SSLCertificate.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Domain.Data_Domain",
                table: "Domain.Data",
                columns: new[] { "BID", "Domain" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Domain.Data");

            migrationBuilder.DropTable(
                name: "enum.DomainAccessType");

            migrationBuilder.DropTable(
                name: "enum.DomainStatus");

            migrationBuilder.DropTable(
                name: "SSLCertificate.Data");
        }
    }
}

