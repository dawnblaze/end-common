﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class CustomFieldIgnoreTempFixIsDefault : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
    UPDATE [CustomField.Definition] SET IsRequired = 0 WHERE IsRequired is null;
");
            migrationBuilder.AlterColumn<bool>(
                name: "IsRequired",
                table: "CustomField.Definition",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsRequired",
                table: "CustomField.Definition",
                type: "bit",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}
