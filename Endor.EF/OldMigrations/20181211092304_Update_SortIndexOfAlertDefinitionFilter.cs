using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Update_SortIndexOfAlertDefinitionFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.List.Filter.Criteria]
               SET [SortIndex] = 1
               WHERE ID = 72
            GO
            UPDATE [System.List.Filter.Criteria]
               SET [SortIndex] = 2
               WHERE ID = 71
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
