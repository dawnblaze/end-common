using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class RenameCustomFieldFormDesignerOptionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [System.Option.Category] 
                SET Name = 'Custom Fields', Description = 'Custom Fields'
                WHERE ID = 203;     
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [System.Option.Category] 
                SET Name = 'Custom Field Form Designers', Description = 'Custom Field Form Designers'
                WHERE ID = 203;     
            ");
        }
    }
}
