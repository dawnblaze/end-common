using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180726171346_AddTaxIntegrationOptionDefinitions")]
    public partial class AddTaxIntegrationOptionDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
VALUES (7031, N'Integration.Tax.Enabled', N'Enabled Online Sales Tax Integration', N'Enable Online Sales Tax Integration', 3, 703, N'', N'0', 0)
    , (7032, N'Integration.Tax.Provider', N'Online Sales Tax Provider', N'Select which Online Sales Tax Provider you are using.', 0, 703, N'', N'TaxJar', 0)
    , (7033, N'Integration.Tax.AccountNumber', N'Account Number', N'Enter your Account Number with the Online Sales Tax Provider', 0, 703, N'', N'', 0)
    , (7034, N'Integration.Tax.APIToken', N'API Token', N'Enter your API Token provided by the Online Sales Tax Provider', 0, 703, N'', N'', 0)
    , (7035, N'Integration.Tax.URL', N'URL', N'Enter the URL of the API for the Online Sales Tax Provider.', 0, 703, N'', N'', 0)
    , (7036, N'Integration.Tax.CompanyIdentifier', N'Company Identifier', N'Enter the Company Identifier provided by the Online Sales Tax Provider.', 0, 703, N'', N'', 0)
    , (7037, N'Integration.Tax.Address.ValidateOnEntry', N'Validate Addresses on Entry', N'Validate All Addresses When Entered', 3, 703, N'', N'1', 0)
    , (7038, N'Integration.Tax.Address.ReplaceOnValidation', N'Replace Entered Address with Validated Address', N'Replace Address with Validated Address if Different', 3, 703, N'', N'1', 0)
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Definition] WHERE [Name] in 
(
N'Integration.Tax.Enabled',
N'Integration.Tax.Provider', 
N'Integration.Tax.AccountNumber', 
N'Integration.Tax.APIToken', 
N'Integration.Tax.URL', N'URL', 
N'Integration.Tax.CompanyIdentifier', 
N'Integration.Tax.Address.ValidateOnEntry', 
N'Integration.Tax.Address.ReplaceOnValidation'
)
;
");
        }
    }
}

