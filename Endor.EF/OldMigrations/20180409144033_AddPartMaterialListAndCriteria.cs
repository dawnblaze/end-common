using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180409144033_AddPartMaterialListAndCriteria")]
    public partial class AddPartMaterialListAndCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                       ([TargetClassTypeID]
                       ,[Name]
                       ,[Label]
                       ,[Field]
                       ,[IsHidden]
                       ,[DataType]
                       ,[InputType]
                       ,[AllowMultiple]
                       ,[ListValues]
                       ,[ListValuesEndpoint]
                       ,[IsLimitToList]
                       ,[SortIndex])
                 VALUES
                       (12000 --<TargetClassTypeID, int,>
                       ,'Name' --<Name, varchar(255),>
                       ,'Name'  --<Label, varchar(255),>
                       ,'Name' --<Field, varchar(255),>
                       ,0 --<IsHidden, bit,>
                       ,0 --<DataType, tinyint,>
                       ,0 --<InputType, tinyint,>
                       ,0 --<AllowMultiple, bit,>
                       ,NULL --<ListValues, varchar(max),>
                       ,NULL --<ListValuesEndpoint, varchar(255),>
                       ,0 --<IsLimitToList, bit,>
                       ,0) --<SortIndex, tinyint,>

                       ,(12000 --<TargetClassTypeID, int,>
                       ,'Is Active' --<Name, varchar(255),>
                       ,'Is Active'  --<Label, varchar(255),>
                       ,'IsActive' --<Field, varchar(255),>
                       ,0 --<IsHidden, bit,>
                       ,3 --<DataType, tinyint,>
                       ,2 --<InputType, tinyint,>
                       ,0 --<AllowMultiple, bit,>
                       ,'Is Not Active,Is Active' --<ListValues, varchar(max),>
                       ,NULL --<ListValuesEndpoint, varchar(255),>
                       ,1 --<IsLimitToList, bit,>
                       ,1) --<SortIndex, tinyint,>
            ");

            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[List.Filter]
                ([BID],ID,[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1022,'2018-3-14','2018-3-14',1,'Active',12000,null,'<ArrayOfListFilterItem>
	                <ListFilterItem>
		                <SearchValue>true</SearchValue>
		                <Field>IsActive</Field>
		                <IsHidden>true</IsHidden>
		                <IsSystem>true</IsSystem>
		                <DisplayText>Is Active</DisplayText>
	                </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,0)
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [dbo].[List.Filter] where ID = 1022");

            migrationBuilder.Sql(@"DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE TargetClassTypeID = 12000 AND (Name='Name' OR NAME='Is Active')");
        }
    }
}

