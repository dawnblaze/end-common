using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180424174349_Order_CreditMemoHasBalance_WrongType")]
    public partial class Order_CreditMemoHasBalance_WrongType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "CreditMemo.HasBalance",
                table: "Order.Data",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([bit],  abs(sign(coalesce( [CreditMemo.Credit], (0)) - coalesce([CreditMemo.Used], (0))))),(0)))",
                oldClrType: typeof(decimal),
                oldNullable: true,
                oldComputedColumnSql: "(isnull(CONVERT([bit],  abs(sign(coalesce( [CreditMemo.Credit], (0)) - coalesce([CreditMemo.Used], (0))))),(0)))");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "CreditMemo.HasBalance",
                table: "Order.Data",
                nullable: true,
                computedColumnSql: "(isnull(CONVERT([bit],  abs(sign(coalesce( [CreditMemo.Credit], (0)) - coalesce([CreditMemo.Used], (0))))),(0)))",
                oldClrType: typeof(bool),
                oldComputedColumnSql: "(isnull(CONVERT([bit],  abs(sign(coalesce( [CreditMemo.Credit], (0)) - coalesce([CreditMemo.Used], (0))))),(0)))");
        }
    }
}

