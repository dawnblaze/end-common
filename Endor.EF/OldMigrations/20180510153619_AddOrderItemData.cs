using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180510153619_AddOrderItemData")]
    public partial class AddOrderItemData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE TABLE [dbo].[Order.Item.Data](
    [BID] [smallint] NOT NULL,
    [ID] [int] NOT NULL,
    [ClassTypeID]  AS ((10020)),
    [ModifiedDT] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT (SYSUTCDATETIME()),
    [ValidToDT] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL DEFAULT (CONVERT([datetime2](7),'9999-12-31 23:59:59.99999999')),
    [OrderID] [int] NOT NULL,
    [ItemNumber] [smallint] NOT NULL,
    [Quantity] [decimal](18, 6) NOT NULL,
    [Name] [nvarchar](255) NOT NULL,
    [Description] [nvarchar](max) NULL,
    [ItemIsSinglePart]  AS (isnull(case when [MaterialID] IS NULL AND [LaborID] IS NULL AND [MachineID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0))),
    [ProductID] [int] NULL,
    [MaterialID] [int] SPARSE  NULL,
    [LaborID] [int] SPARSE  NULL,
    [MachineID] [smallint] SPARSE  NULL,
    [IsOutsourced] [bit] NOT NULL,
    [OrderStatusID] [tinyint] NOT NULL,
    [ItemStatusID] [smallint] NOT NULL,
    [SubStatusID] [smallint] NULL,
    [ProductionLocationID] [tinyint] NOT NULL,
    [Price.IsLocked] [bit] NOT NULL,
    [Price.Unit] [decimal](18, 6) NULL,
    [Price.UnitOV] [bit] NOT NULL,
    [Price.Net] [decimal](18, 4) NULL,
    [Price.NetOV] [bit] NOT NULL,
    [Price.ItemDiscountPercent] [decimal](9, 4) SPARSE  NULL,
    [Price.AppliedOrderDiscountPercent] [decimal](9, 4) SPARSE  NULL,
    [Price.DiscountPercent]  AS (case when isnull([Price.ItemDiscountPercent],[Price.AppliedOrderDiscountPercent]) IS NOT NULL then (100.0)-(((100.0)-isnull([Price.ItemDiscountPercent],(0.00)))*((100.0)-isnull([Price.AppliedOrderDiscountPercent],(0.00))))/(100.0)  end),
    [Price.ItemDiscount] [decimal](18, 4) NULL,
    [Price.AppliedOrderDiscount] [decimal](18, 4) SPARSE  NULL,
    [Price.Discount]  AS (isnull([Price.ItemDiscount],(0.0))+isnull([Price.AppliedOrderDiscount],(0.0))),
    [Price.PreTax]  AS (([Price.Net]-isnull([Price.ItemDiscount],(0.0)))-isnull([Price.AppliedOrderDiscount],(0.0))),
    [Price.Taxable] [decimal](18, 4) NULL,
    [Price.TaxableOV] [bit] NOT NULL,
    [Price.Tax] [decimal](18, 4) NULL,
    [Price.Total]  AS ((([Price.Net]+[Price.Tax])-isnull([Price.ItemDiscount],(0.0)))-isnull([Price.AppliedOrderDiscount],(0.0))) PERSISTED,
    [Computed.Net] [decimal](18, 4) NULL,
    [Computed.PreTax]  AS (([Computed.Net]-isnull([Price.ItemDiscount],(0.0)))-isnull([Price.AppliedOrderDiscount],(0.0))),
    [TaxGroupID] [smallint] NOT NULL,
    [TaxGroupOV] [bit] NOT NULL,
    [IsTaxExempt] [bit] NOT NULL,
    [TaxExemptReasonID] [smallint] SPARSE NULL,
    [Cost.Material] [decimal](18, 4) SPARSE  NULL,
    [Cost.Labor] [decimal](18, 4) SPARSE  NULL,
    [Cost.Machine] [decimal](18, 4) SPARSE  NULL,
    [Cost.Other] [decimal](18, 4) SPARSE  NULL,
    [Cost.Total]  AS (isnull(((isnull([Cost.Material],(0.0))+isnull([Cost.Labor],(0.0)))+isnull([Cost.Machine],(0.0)))+isnull([Cost.Other],(0.0)),(0.0))),
    [HasProof] [bit] NOT NULL,
    [HasDocuments] [bit] NOT NULL,
    [HasCustomImage] [bit] NOT NULL,
    CONSTRAINT [PK_Order.Item.Data] PRIMARY KEY ( BID, ID ),
    PERIOD FOR SYSTEM_TIME(ModifiedDT, ValidToDT)
);
");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Data_OrderID",
                table: "Order.Item.Data",
                columns: new[] { "BID", "OrderID", "ItemNumber" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Data_Status",
                table: "Order.Item.Data",
                columns: new[] { "BID", "ItemStatusID", "SubStatusID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Data_Product",
                table: "Order.Item.Data",
                columns: new[] { "BID", "ProductID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Data_ProductionLocation",
                table: "Order.Item.Data",
                columns: new[] { "BID", "ProductionLocationID", "OrderStatusID", "ItemStatusID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_Order",
                table: "Order.Item.Data",
                columns: new[] { "BID", "OrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_OrderStatus",
                table: "Order.Item.Data",
                columns: new[] { "OrderStatusID" },
                principalTable: "enum.Order.OrderStatus",
                principalColumns: new[] { "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_ItemStatus",
                table: "Order.Item.Data",
                columns: new[] { "BID", "ItemStatusID" },
                principalTable: "Order.Item.Status",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_SubStatus",
                table: "Order.Item.Data",
                columns: new[] { "BID", "SubStatusID" },
                principalTable: "Order.Item.SubStatus",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_Material",
                table: "Order.Item.Data",
                columns: new[] { "BID", "MaterialID" },
                principalTable: "Part.Material.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_Labor",
                table: "Order.Item.Data",
                columns: new[] { "BID", "LaborID" },
                principalTable: "Part.Labor.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_Machine",
                table: "Order.Item.Data",
                columns: new[] { "BID", "MachineID" },
                principalTable: "Part.Machine.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_TaxGroup",
                table: "Order.Item.Data",
                columns: new[] { "BID", "TaxGroupID" },
                principalTable: "Accounting.Tax.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_ProdLoc",
                table: "Order.Item.Data",
                columns: new[] { "BID", "ProductionLocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
ALTER TABLE[Order.Item.Data] SET(SYSTEM_VERSIONING = ON(HISTORY_TABLE = dbo.[Historic.Order.Item.Data]));
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE[Order.Item.Data] SET(SYSTEM_VERSIONING = OFF);
");
            migrationBuilder.DropTable(
                name: "Historic.Order.Item.Data");

            migrationBuilder.DropTable(
                name: "Order.Item.Data");
        }
    }
}

