using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class implement_iprioritizable_to_orderdata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsUrgent",
                table: "Order.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ItemCount",
                table: "Order.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "Priority",
                table: "Order.Data",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUrgent",
                table: "Order.Data");

            migrationBuilder.DropColumn(
                name: "ItemCount",
                table: "Order.Data");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "Order.Data");

        }
    }
}
