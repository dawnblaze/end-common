using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180502180141_CorrectEmployeeRoles")]
    public partial class CorrectEmployeeRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddForeignKey(
                name: "FK_Order.Employee.Role_RoleType",
                table: "Order.Employee.Role",
                column: "RoleType",
                principalTable: "enum.Employee.RoleType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Employee.Role_RoleType",
                table: "Order.Employee.Role");
        }
    }
}

