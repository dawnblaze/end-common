using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180816203558_END-2250_RemoveIncorrectEmployeeRoleClassTypeID")]
    public partial class END2250_RemoveIncorrectEmployeeRoleClassTypeID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                (@"
                    --Wrong ClassTypeID for Employee Role Removed
                    DELETE FROM [enum.ClassType] WHERE ID = 5101
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                (@"
                    INSERT INTO [enum.ClassType]
                      (ID, [Name], TableName)
                      VALUES
                      --Employee Role
                      (5101 , 'EmployeeRole', '')
                ");
        }
    }
}

