using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class InsertMachineTemplateFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [dbo].[List.Filter]
                SET [Name]='All Assemblies',
                [Criteria]='<ArrayOfListFilterItem><ListFilterItem><SearchValue>0</SearchValue><Field>AssemblyType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>'                
                WHERE [TargetClasstypeID]=12040;

                INSERT INTO [dbo].[List.Filter]
                ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (1,1056,'2019-7-2','2019-7-2',1,'All Machine Templates',12040,null,
                '<ArrayOfListFilterItem><ListFilterItem><SearchValue>3</SearchValue><Field>AssemblyType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>',
                null,1,1,null,1,0);
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[List.Filter]
                WHERE
                [TargetClasstypeID]=12040 AND [Name]='All Machine Templates';

                UPDATE [dbo].[List.Filter]
                SET [Name]='All',
                [Criteria]=null                
                WHERE [TargetClasstypeID]=12040;
            ");

        }
    }
}
