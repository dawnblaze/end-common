using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddAssemblyDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql($"DELETE FROM dbo.[enum.DataType] WHERE ID = 12040");
            migrationBuilder.Sql($"DELETE FROM dbo.[enum.DataType] WHERE ID = -12040");
            migrationBuilder.Sql($"INSERT INTO dbo.[enum.DataType] (ID, Name) values(12040 ,'Assembly')");
            migrationBuilder.Sql($"INSERT INTO dbo.[enum.DataType] (ID, Name) values(-12040 ,'AssemblyList')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           
        }
    }
}
