using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8157_Add_New_Business_Data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
               @"

                    -- REQUIRED DATA
                    INSERT INTO [Business.Data] (
	                       [BID]
                          ,[CreatedDate]
                          ,[ModifiedDT]
                          ,[IsActive]
                          ,[Name]
                          ,[LegalName]
                          ,[HasImage]
                          ,[AssociationType]
	                    )
                    VALUES (-1, '2019-08-08', '2019-08-08 00:00:00:00', 1, 'Required', NULL, 0, 0);

                    -- DEFAULT DATA 
                    INSERT INTO [Business.Data] (
	                       [BID]
                          ,[CreatedDate]
                          ,[ModifiedDT]
                          ,[IsActive]
                          ,[Name]
                          ,[LegalName]
                          ,[HasImage]
                          ,[AssociationType]
	                    )
                    VALUES (-2, '2019-08-08', '2019-08-08 00:00:00:00', 1, 'Default', NULL, 0, 0);

                    -- HISTORIC DEFAULT
                    INSERT INTO [Business.Data] (
	                       [BID]
                          ,[CreatedDate]
                          ,[ModifiedDT]
                          ,[IsActive]
                          ,[Name]
                          ,[LegalName]
                          ,[HasImage]
                          ,[AssociationType]
	                    )
                    VALUES (-3, '2019-08-08', '2019-08-08 00:00:00:00', 1, 'Historic Default', NULL, 0, 0);

                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [Business.Data] WHERE BID IN (-1, -2, -3)");
        }
    }
}
