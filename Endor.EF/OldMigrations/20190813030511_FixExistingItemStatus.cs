using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.EF.Migrations
{
    public partial class FixExistingItemStatus : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- Fix some conflicting data
UPDATE OS 
SET Name = CONCAT('x', OS.Name)
FROM [Order.Item.Status] OS
JOIN [Order.Item.Status] DefaultData ON DefaultData.OrderStatusID = OS.OrderStatusID AND DefaultData.Name = OS.Name
WHERE OS.BID > 0
  AND DefaultData.BID in (-1, -2)
;

-- Reset data already there
UPDATE [Order.Item.Status]
SET OrderStatusID = ID
WHERE ID BETWEEN 11 AND 65 
;

-- Insert the new records for the ItemStatus
EXECUTE [Util.Table.CopyDefaultRecords] 
   @TableName = 'Order.Item.Status'
  ,@IncludeRequired = 1
  ,@IncludeDefault = 1
;

-- Reset all of the defaults
UPDATE [Order.Item.Status]
SET IsDefault = (CASE WHEN ID BETWEEN 11 AND 65 THEN 1 ELSE 0 END)
    , IsActive = (CASE WHEN ID BETWEEN 11 AND 65 THEN 1 ELSE IsActive END)
;
");

            migrationBuilder.Sql(@"
UPDATE OI
SET OI.ItemStatusID = O.OrderStatusID
  , OI.OrderStatusID = O.OrderStatusID
  , OI.TransactionType = O.TransactionType
FROM [Order.Item.Data] OI
JOIN [Order.Data] O ON O.BID = OI.BID AND O.ID = OI.OrderID
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
                
        public override List<int> ReindexClasstypes()
        {
            return new List<int>() { 10000,10020,10021 };
        }

    }
}
