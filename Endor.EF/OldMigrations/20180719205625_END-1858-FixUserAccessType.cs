using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180719205625_END-1858-FixUserAccessType")]
    public partial class END1858FixUserAccessType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                Update [dbo].[enum.User.Access.Type] Set [Name] = 'Developer God' where [ID]=59 and [Name] like '%Developer Staff%'
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

