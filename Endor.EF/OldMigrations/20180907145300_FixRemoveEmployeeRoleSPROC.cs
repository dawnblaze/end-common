using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180907145300_FixRemoveEmployeeRoleSPROC")]
    public partial class FixRemoveEmployeeRoleSPROC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [Business.Goal]
            ");

            migrationBuilder.Sql(@"
/*
    This view takes all of the Location Goal data and adds to it so it includes the
    Business Goal data (LocationID = NULL) and the annual totals (Month = NULL).
 
    This view is read-only.  The Stored Procedure [Location.Goal.CreateOrUpdate]
    should be used for updating the data.
*/
 
CREATE VIEW [dbo].[Business.Goal] AS
SELECT  TOP 100 PERCENT
        Locations.BID as BID
      , CONVERT(SMALLINT, (CASE WHEN Max(LG.ID) IS NULL THEN -ROW_NUMBER() OVER( ORDER BY Locations.LocationID, Years.Year, Months.Month) ELSE Max(LG.ID) END)) AS ID
      , 1009 as ClassTypeID
      , COALESCE(MAX(LG.ModifiedDT), GetUTCDate()) as ModifiedDT
      , Locations.LocationID
      , Years.Year
      , CONVERT(tinyint,Months.Month) as Month
      , SUM(LG.Budgeted) AS Budgeted
      , SUM(LG.Actual) AS Actual
      , (case when SUM(LG.Budgeted)<>(0) then (SUM(LG.Actual)/SUM(LG.Budgeted))*(100.0) end) AS PercentOfGoal
      , (case when Months.Month IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end) AS IsYearlyTotal
      , (case when Locations.LocationID IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end) AS IsBusinessTotal
 
FROM ( SELECT DISTINCT BID, ID as LocationID FROM [Location.Data]) as Locations
LEFT JOIN ( SELECT DISTINCT BID, Year FROM [Location.Goal] ) as Years ON Years.BID = Locations.BID
LEFT JOIN (VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12)) AS Months(Month) ON 1=1
LEFT JOIN [Location.Goal] LG ON LG.BID = Locations.BID AND LG.LocationID = Locations.LocationID AND LG.Year = Years.Year AND LG.Month = Months.Month
GROUP BY Locations.BID, Years.Year, ROLLUP(Months.Month), ROLLUP(Locations.LocationID)
ORDER BY Locations.BID, Locations.LocationID, Years.Year, Months.Month
;
");
            migrationBuilder.Sql(@"
                -- ========================================================
-- Name: [Order.Action.RemoveEmployeeRole]( @BID tinyint, @OrderID int, @RoleID int, @EmployeeRoleType tinyint, @EmployeeID int )
--
-- Description: This function remove a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.RemoveEmployeeRole] @BID=1, @OrderID=1, @RoleID=NULL, @EmployeeRoleType=1, @EmployeeID=1
-- ========================================================
ALTER PROCEDURE [dbo].[Order.Action.RemoveEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
		, @RoleID          INT           = NULL
        , @EmployeeRoleType TINYINT   -- = 1
        , @EmployeeID       INT       -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    IF @RoleID IS NOT NULL
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
	END
	ELSE
	BEGIN
		SELECT @ID = ID
		FROM [Order.Employee.Role]
		WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @EmployeeRoleType AND EmployeeID =  @EmployeeID
	END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Delete record is exists
    DELETE FROM OER
    FROM   [Order.Employee.Role] OER
    WHERE  BID = @BID AND ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

