using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180116035237_InitialMigration")]
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE TYPE [dbo].[OptionsArray] AS TABLE(
	[OptionID] [int] NULL,
	[OptionName] [varchar](255) NULL,
	[Value] [varchar](max) NULL
)");
            migrationBuilder.CreateTable(
                name: "_Root.Custom",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    BID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Root.Custom", x => new { x.ID, x.BID });
                });

            migrationBuilder.CreateTable(
                name: "_Root.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((0))"),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Root.Data", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Payment.Term",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((11105))"),
                    CompoundInterest = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    EarlyPaymentDays = table.Column<short>(nullable: true),
                    EarlyPaymentDiscount = table.Column<decimal>(type: "decimal(12, 4)", nullable: true),
                    GracePeriod = table.Column<short>(nullable: true),
                    InterestBasedOnSaleDate = table.Column<bool>(nullable: false),
                    InterestRate = table.Column<decimal>(type: "decimal(12, 4)", nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Payment.Term", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Tax.Assessment",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AccountNumber = table.Column<string>(maxLength: 255, nullable: true),
                    AgencyName = table.Column<string>(maxLength: 255, nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((11102))"),
                    InvoiceText = table.Column<string>(maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    LookupCode = table.Column<string>(maxLength: 255, nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    TaxRate = table.Column<decimal>(type: "decimal(12, 4)", nullable: false),
                    VendorID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Tax.Assessment", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Tax.Group",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((11101))"),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsTaxExempt = table.Column<bool>(nullable: true, computedColumnSql: "(case when [TaxRate]=(0.0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    TaxRate = table.Column<decimal>(type: "decimal(12, 4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Tax.Group", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Activity.Action",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ActivityType = table.Column<byte>(nullable: false),
                    AutoComplete = table.Column<bool>(nullable: false),
                    AutoRollover = table.Column<bool>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((11001))"),
                    CompletedByID = table.Column<int>(nullable: true),
                    CompletedDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    EndDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsComplete = table.Column<bool>(nullable: true, computedColumnSql: "(case when [CompletedDT] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Notes = table.Column<string>(nullable: true),
                    StartDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false),
                    Subject = table.Column<string>(maxLength: 255, nullable: false),
                    TeamID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity.Action", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Activity.Event",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ActivityType = table.Column<byte>(nullable: false),
                    AutoComplete = table.Column<bool>(nullable: false),
                    AutoRollover = table.Column<bool>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((11002))"),
                    CompletedByID = table.Column<int>(nullable: true),
                    CompletedDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    EndDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false, computedColumnSql: "([StartDT])"),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsComplete = table.Column<bool>(nullable: true, computedColumnSql: "(case when [CompletedDT] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Notes = table.Column<string>(nullable: true),
                    StartDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false),
                    Subject = table.Column<string>(maxLength: 255, nullable: false),
                    TeamID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity.Event", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Activity.GLActivity",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ActivityType = table.Column<byte>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((11003))"),
                    CompletedByID = table.Column<int>(nullable: false),
                    CompletedDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    EndDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false, computedColumnSql: "([CompletedDT])"),
                    IsActive = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],(1)),(1)))"),
                    IsComplete = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],(1)),(1)))"),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Notes = table.Column<string>(unicode: false, nullable: true, computedColumnSql: "(CONVERT([varchar](max),NULL))"),
                    StartDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false, computedColumnSql: "([CompletedDT])"),
                    Subject = table.Column<string>(maxLength: 255, nullable: false),
                    TeamID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity.GLActivity", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Association.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AssociationType = table.Column<byte>(nullable: false),
                    CampaignID = table.Column<int>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1600))"),
                    CompanyID = table.Column<int>(nullable: true),
                    ContactID = table.Column<int>(nullable: true),
                    DocumentID = table.Column<int>(nullable: true),
                    EmployeeID = table.Column<int>(nullable: true),
                    LeadID = table.Column<int>(nullable: true),
                    OpportunityID = table.Column<int>(nullable: true),
                    TeamID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Association.Data", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Business.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business.Custom", x => x.BID);
                });

            migrationBuilder.CreateTable(
                name: "Campaign.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9101))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaign.Custom", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Company.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company.Custom", x => new { x.BID, x.ID });
                });
            
            migrationBuilder.CreateTable(
                name: "Contact.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((3001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact.Custom", x => new { x.BID, x.ID });
                });
            
            migrationBuilder.CreateTable(
                name: "CRM.Industry",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2011))"),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsLocked = table.Column<bool>(nullable: false),
                    IsTopLevel = table.Column<bool>(nullable: true, computedColumnSql: "(case when [ParentID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    ParentID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.Industry", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CRM.Industry_CRM.Industry",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "CRM.Industry",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CRM.Source",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2012))"),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsLocked = table.Column<bool>(nullable: false),
                    IsTopLevel = table.Column<bool>(nullable: true, computedColumnSql: "(case when [ParentID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    ParentID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.Source", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CRM.Source_CRM.Source",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "CRM.Source",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employee.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((5001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.Custom", x => new { x.BID, x.ID });
                });

            
            migrationBuilder.CreateTable(
                name: "Employee.Team",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((5020))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    HasImage = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsAdHocTeam = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.Team", x => new { x.BID, x.ID });
                });
            
            migrationBuilder.CreateTable(
                name: "enum.ActivityType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    RecordType = table.Column<byte>(nullable: false),
                    RecordTypeText = table.Column<string>(unicode: false, maxLength: 15, nullable: false, computedColumnSql: "(isnull(case [RecordType] when (1) then 'Action' when (2) then 'Event' when (3) then 'GL Activity' else 'Generic Actvity' end,''))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.ActivityType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Campaign.Stage",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Campaign.Stage", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Campaign.Type",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Campaign.Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.ClassType",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false),
                    Description = table.Column<string>(unicode: false, maxLength: 2048, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.ClassType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.CRM.Company.Status",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CRM.Company.Status", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.CustomField.DataType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.DataType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.CustomField.HelperType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.HelperType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Employee.Role",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Employee.Role", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Lead.Stage",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Lead.Stage", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Locator.Type",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    PossibilityRegEx = table.Column<string>(unicode: false, nullable: true),
                    PossibilitySortOrder = table.Column<byte>(nullable: true),
                    ValidityRegEx = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Locator.Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Opportunity.Stage",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Opportunity.Stage", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Option.Level",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Option.Level", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.TimeZone",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false),
                    IsCommon = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    StandardName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    UTCOffset = table.Column<decimal>(type: "decimal(6, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.TimeZone", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.User.Role.Type",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.User.Role.Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "List.Filter",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1700))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    Criteria = table.Column<string>(type: "xml", nullable: true),
                    Hint = table.Column<string>(unicode: false, nullable: true),
                    IDs = table.Column<string>(type: "xml", nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "((1))"),
                    IsDefault = table.Column<bool>(nullable: false),
                    IsDynamic = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],case when [IDs] IS NULL then (1) else (0) end),(1)))"),
                    IsPublic = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    OwnerID = table.Column<short>(nullable: true),
                    SortIndex = table.Column<byte>(nullable: false, defaultValueSql: "((50))"),
                    TargetClassTypeID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_List.Filter", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Opportunity.Custom",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9001))"),
                    CustomFieldData = table.Column<string>(type: "xml", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opportunity.Custom", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Report.Menu",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2000))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    DefaultMenuID = table.Column<short>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(maxLength: 255, nullable: false),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsOV = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    ParentID = table.Column<short>(nullable: true),
                    ReportID = table.Column<short>(nullable: false),
                    ReportOptions = table.Column<string>(type: "xml", nullable: true),
                    SortIndex = table.Column<short>(nullable: false, defaultValueSql: "((100))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Report.Menu", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Security.Right.Group",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<short>(nullable: true, computedColumnSql: "(CONVERT([smallint],(1101)))"),
                    IsSystem = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security.Right.Group", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "SimpleBusinessData",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "1005"),
                    DisplayName = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    IsDefault = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SimpleBusinessData", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "System.List.Column",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1711))"),
                    Field = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    HeaderText = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    IsExpander = table.Column<bool>(nullable: false),
                    IsFrozen = table.Column<bool>(nullable: false),
                    IsSortable = table.Column<bool>(nullable: false),
                    IsVisible = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [SortIndex] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    SortField = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    SortIndex = table.Column<byte>(nullable: false, defaultValueSql: "((50))"),
                    StyleClass = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    TargetClassTypeID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.List.Column", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "System.List.Filter.Criteria",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AllowMultiple = table.Column<bool>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1710))"),
                    DataType = table.Column<byte>(nullable: false),
                    Field = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    InputType = table.Column<byte>(nullable: false),
                    IsHidden = table.Column<bool>(nullable: false),
                    IsLimitToList = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsVisible = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [SortIndex] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    Label = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    ListValues = table.Column<string>(unicode: false, nullable: true),
                    ListValuesEndpoint = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    SortIndex = table.Column<byte>(nullable: true),
                    TargetClassTypeID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.List.Filter.Criteria", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "System.Option.Section",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false),
                    Depth = table.Column<byte>(nullable: false, defaultValueSql: "((0))"),
                    IsHidden = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    ParentID = table.Column<short>(nullable: true),
                    SearchTerms = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Option.Section", x => x.ID);
                    table.ForeignKey(
                        name: "FK_System.Option.Section_System.Option.Section",
                        column: x => x.ParentID,
                        principalTable: "System.Option.Section",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Util.NextID",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false),
                    NextID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Util.NextID", x => new { x.BID, x.ClassTypeID });
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Tax.Group.AssessmentLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    GroupID = table.Column<short>(nullable: false),
                    AssessmentID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Tax.Group.AssessmentLink", x => new { x.BID, x.GroupID, x.AssessmentID });
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment",
                        columns: x => new { x.BID, x.AssessmentID },
                        principalTable: "Accounting.Tax.Assessment",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group",
                        columns: x => new { x.BID, x.GroupID },
                        principalTable: "Accounting.Tax.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "enum.CustomField.InputType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    DataType = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.InputType", x => x.ID);
                    table.ForeignKey(
                        name: "FK_enum.CustomField.InputType_enum.CustomField.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.CustomField.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "enum.Locator.SubType",
                columns: table => new
                {
                    LocatorType = table.Column<byte>(nullable: false),
                    ID = table.Column<byte>(nullable: false),
                    LinkFormatString = table.Column<string>(unicode: false, maxLength: 255, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Locator.SubType", x => new { x.LocatorType, x.ID });
                    table.ForeignKey(
                        name: "FK_enum.Locator.SubType_enum.Locator.Type",
                        column: x => x.LocatorType,
                        principalTable: "enum.Locator.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Security.Right.Group.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ParentGroupID = table.Column<short>(nullable: false),
                    ChildGroupID = table.Column<short>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security.Right.Group.Link", x => new { x.BID, x.ParentGroupID, x.ChildGroupID });
                    table.ForeignKey(
                        name: "FK_Security.Right.Group.Link_Security.Right.Group1",
                        columns: x => new { x.BID, x.ChildGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Security.Right.Group.Link_Security.Right.Group",
                        columns: x => new { x.BID, x.ParentGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Security.Right.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    RightGroupID = table.Column<short>(nullable: false),
                    RightID = table.Column<short>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security.Right.Link", x => new { x.BID, x.RightGroupID, x.RightID });
                    table.ForeignKey(
                        name: "FK_Security.Right.Link_Security.Right.Collection",
                        columns: x => new { x.BID, x.RightGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Security.Role.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    RoleType = table.Column<byte>(nullable: false),
                    RightGroupID = table.Column<short>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security.Role.Link", x => new { x.BID, x.RoleType, x.RightGroupID });
                    table.ForeignKey(
                        name: "FK_Security.Role.Link_enum.User.Role.Type",
                        column: x => x.RoleType,
                        principalTable: "enum.User.Role.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Security.Role.Link_Security.Right.Group",
                        columns: x => new { x.BID, x.RightGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "System.Option.Category",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false),
                    Description = table.Column<string>(unicode: false, nullable: true),
                    IsAssociationOption = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],case when ([OptionLevels]&(1))<>(0) then (1) else (0) end),(0)))"),
                    IsBusinessOption = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],case when ([OptionLevels]&(2))<>(0) then (1) else (0) end),(0)))"),
                    IsCompanyOption = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],case when ([OptionLevels]&(32))<>(0) then (1) else (0) end),(0)))"),
                    IsContactOption = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],case when ([OptionLevels]&(64))<>(0) then (1) else (0) end),(0)))"),
                    IsEmployeeOption = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],case when ([OptionLevels]&(16))<>(0) then (1) else (0) end),(0)))"),
                    IsHidden = table.Column<bool>(nullable: false),
                    IsLocationOption = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],case when ([OptionLevels]&(4))<>(0) then (1) else (0) end),(0)))"),
                    IsStorefrontOption = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(CONVERT([bit],case when ([OptionLevels]&(8))<>(0) then (1) else (0) end),(0)))"),
                    IsSystemOption = table.Column<int>(nullable: false, computedColumnSql: "((1))"),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    OptionLevels = table.Column<byte>(nullable: false),
                    SearchTerms = table.Column<string>(unicode: false, nullable: true),
                    SectionID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Option.Category", x => x.ID);
                    table.ForeignKey(
                        name: "FK_System.Option.Category_System.Option.Section",
                        column: x => x.SectionID,
                        principalTable: "System.Option.Section",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "System.Option.Definition",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false),
                    CategoryID = table.Column<short>(nullable: false),
                    DataType = table.Column<byte>(nullable: false, defaultValueSql: "((0))"),
                    DefaultValue = table.Column<string>(unicode: false, nullable: true),
                    Description = table.Column<string>(unicode: false, nullable: true),
                    IsHidden = table.Column<bool>(nullable: false),
                    Label = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    ListValues = table.Column<string>(unicode: false, nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Option.Definition", x => x.ID);
                    table.ForeignKey(
                        name: "FK_System.Option.Definition_System.Option.Category",
                        column: x => x.CategoryID,
                        principalTable: "System.Option.Category",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_System.Option.Definition_enum.CustomField.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.CustomField.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Option.Data",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssociationID = table.Column<byte>(nullable: true),
                    BID = table.Column<short>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1800))"),
                    CompanyID = table.Column<int>(nullable: true),
                    ContactID = table.Column<int>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    EmployeeID = table.Column<short>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    LocationID = table.Column<short>(nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    OptionID = table.Column<short>(nullable: false),
                    OptionLevel = table.Column<byte>(nullable: false, computedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (64) when [CompanyID] IS NOT NULL then (32) when [EmployeeID] IS NOT NULL then (16) when [StorefrontID] IS NOT NULL then (8) when [LocationID] IS NOT NULL then (4) when [BID] IS NOT NULL then (2) when [AssociationID] IS NOT NULL then (1) else (0) end),(0)))"),
                    StoreFrontID = table.Column<short>(nullable: true),
                    Value = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Option.Data", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Option.Data_System.Option.Definition",
                        column: x => x.OptionID,
                        principalTable: "System.Option.Definition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    //                    table.ForeignKey(
                    //                        name: "FK_Option.Data_enum.Option.Level",
                    //                        column: x => x.OptionLevel,
                    //                        principalTable: "Enum.Option.Level",
                    //                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Payment.Term.LocationLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TermID = table.Column<short>(nullable: false),
                    LocationID = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Payment.Term.LocationLink", x => new { x.BID, x.TermID, x.LocationID });
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term",
                        columns: x => new { x.BID, x.TermID },
                        principalTable: "Accounting.Payment.Term",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Accounting.Tax.Group.LocationLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    GroupID = table.Column<short>(nullable: false),
                    LocationID = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Tax.Group.LocationLink", x => new { x.BID, x.GroupID, x.LocationID });
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group",
                        columns: x => new { x.BID, x.GroupID },
                        principalTable: "Accounting.Tax.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Company.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2000))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    CreditApprovalDate = table.Column<DateTime>(type: "date", nullable: true),
                    CreditLimit = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    CreditNumber = table.Column<string>(maxLength: 100, nullable: true),
                    DefaultPONumber = table.Column<string>(maxLength: 100, nullable: true),
                    HasCreditAccount = table.Column<bool>(nullable: true, computedColumnSql: "(case when [CreditLimit] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    HasImage = table.Column<bool>(nullable: false),
                    IndustryID = table.Column<short>(nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsAdHoc = table.Column<bool>(nullable: false),
                    IsClient = table.Column<bool>(nullable: true, computedColumnSql: "(case when ([StatusID]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsLead = table.Column<bool>(nullable: true, computedColumnSql: "(case when ([StatusID]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsPersonal = table.Column<bool>(nullable: true, computedColumnSql: "(case when ([StatusID]&(16))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsPORequired = table.Column<bool>(nullable: false),
                    IsProspect = table.Column<bool>(nullable: true, computedColumnSql: "(case when ([StatusID]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsVendor = table.Column<bool>(nullable: true, computedColumnSql: "(case when ([StatusID]&(8))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    LocationID = table.Column<byte>(nullable: false, defaultValueSql: "((1))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    NonRefundableCredit = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    ParentID = table.Column<int>(nullable: true),
                    PaymentTermsID = table.Column<short>(nullable: true),
                    RefundableCredit = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    SourceID = table.Column<short>(nullable: true),
                    StatusID = table.Column<byte>(nullable: false, defaultValueSql: "((0))"),
                    StatusText = table.Column<string>(unicode: false, maxLength: 24, nullable: false, computedColumnSql: "(concat(case when [StatusID]=(1) then 'Lead'  end,case when ([StatusID]&(2))<>(0) then 'Prospect'  end,case when ([StatusID]&(4))<>(0) then 'Client'  end,case when ([StatusID]&(8))<>(4) then 'Vendor'  end))"),
                    Tags = table.Column<string>(nullable: true),
                    TaxGroupID = table.Column<short>(nullable: true),
                    TeamID = table.Column<int>(nullable: true),
                    TimeZoneID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Company.Data_enum.CRM.Company.Status",
                        column: x => x.StatusID,
                        principalTable: "enum.CRM.Company.Status",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Company.Data_enum.TimeZone",
                        column: x => x.TimeZoneID,
                        principalTable: "enum.TimeZone",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Company.Data_Company.Custom",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Company.Custom",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Company.Data_CRM.Industry",
                        columns: x => new { x.BID, x.IndustryID },
                        principalTable: "CRM.Industry",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Company.Data_Company.Data",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Company.Data_CRM.Source",
                        columns: x => new { x.BID, x.SourceID },
                        principalTable: "CRM.Source",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Company.Data_Accounting.Tax.Group",
                        columns: x => new { x.BID, x.TaxGroupID },
                        principalTable: "Accounting.Tax.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Company.Data_Employee.Team",
                        columns: x => new { x.BID, x.TeamID },
                        principalTable: "Employee.Team",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Company.Locator",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2003))"),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsPrimary = table.Column<bool>(nullable: false, computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsValid = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsVerified = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    Locator = table.Column<string>(maxLength: 1024, nullable: true),
                    LocatorSubType = table.Column<byte>(nullable: false),
                    LocatorType = table.Column<byte>(nullable: false),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    ParentID = table.Column<int>(nullable: false),
                    RawInput = table.Column<string>(maxLength: 1024, nullable: false),
                    SortIndex = table.Column<short>(nullable: false, defaultValueSql: "2")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company.Locator", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Company.Locator_enum.LocatorType",
                        column: x => x.LocatorType,
                        principalTable: "enum.Locator.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Company.Locator_Company.Data",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Company.Locator_enum.LocatorSubType",
                        columns: x => new { x.LocatorType, x.LocatorSubType },
                        principalTable: "enum.Locator.SubType",
                        principalColumns: new[] { "LocatorType", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Location.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<byte>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1005))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    DBA = table.Column<string>(maxLength: 255, nullable: true),
                    DefaultAreaCode = table.Column<string>(maxLength: 10, nullable: true),
                    DefaultTaxGroupID = table.Column<short>(nullable: true),
                    EstimatePrefix = table.Column<string>(maxLength: 10, nullable: true),
                    HasImage = table.Column<bool>(nullable: false),
                    InvoicePrefix = table.Column<string>(maxLength: 10, nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsDefault = table.Column<bool>(nullable: false),
                    LegalName = table.Column<string>(maxLength: 255, nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    POPrefix = table.Column<string>(maxLength: 10, nullable: true),
                    Slogan = table.Column<string>(maxLength: 255, nullable: true),
                    TaxID = table.Column<string>(maxLength: 100, nullable: true),
                    TimeZoneID = table.Column<short>(nullable: true),
                    UseDST = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Location.Data_enum.TimeZone",
                        column: x => x.TimeZoneID,
                        principalTable: "enum.TimeZone",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Location.Data_Accounting.Tax.Group",
                        columns: x => new { x.BID, x.DefaultTaxGroupID },
                        principalTable: "Accounting.Tax.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employee.Team.LocationLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TeamID = table.Column<int>(nullable: false),
                    LocationID = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.Team.LocationLink", x => new { x.BID, x.TeamID, x.LocationID });
                    table.ForeignKey(
                        name: "FK_Employee.Team.LocationLink_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Employee.Team.LocationLink_Employee.Team",
                        columns: x => new { x.BID, x.TeamID },
                        principalTable: "Employee.Team",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Location.Locator",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1008))"),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsPrimary = table.Column<bool>(nullable: false, computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsValid = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsVerified = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    Locator = table.Column<string>(maxLength: 1024, nullable: true),
                    LocatorSubType = table.Column<byte>(nullable: false),
                    LocatorType = table.Column<byte>(nullable: false),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    ParentID = table.Column<byte>(nullable: false),
                    RawInput = table.Column<string>(maxLength: 1024, nullable: false),
                    SortIndex = table.Column<short>(nullable: false, defaultValueSql: "2")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location.Locator", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Location.Locator_enum.LocatorType",
                        column: x => x.LocatorType,
                        principalTable: "enum.Locator.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Location.Locator_Location.Data",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Business.Location.Locator_enum.LocatorSubType",
                        columns: x => new { x.LocatorType, x.LocatorSubType },
                        principalTable: "enum.Locator.SubType",
                        principalColumns: new[] { "LocatorType", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Business.Locator",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1003))"),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsPrimary = table.Column<bool>(nullable: false, computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsValid = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsVerified = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    Locator = table.Column<string>(maxLength: 1024, nullable: true),
                    LocatorSubType = table.Column<byte>(nullable: false),
                    LocatorType = table.Column<byte>(nullable: false),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    ParentID = table.Column<short>(nullable: false),
                    RawInput = table.Column<string>(maxLength: 1024, nullable: false),
                    SortIndex = table.Column<short>(nullable: false, defaultValueSql: "2")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business.Locator", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Business.Locator_enum.LocatorType",
                        column: x => x.LocatorType,
                        principalTable: "enum.Locator.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Business.Locator_enum.LocatorSubType",
                        columns: x => new { x.LocatorType, x.LocatorSubType },
                        principalTable: "enum.Locator.SubType",
                        principalColumns: new[] { "LocatorType", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Campaign.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ActualCost = table.Column<decimal>(type: "money", nullable: true),
                    ActualRevenue = table.Column<decimal>(type: "money", nullable: true),
                    Budget = table.Column<decimal>(type: "money", nullable: true),
                    CampaignType = table.Column<byte>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9100))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    Description = table.Column<string>(nullable: true),
                    EndDate = table.Column<DateTime>(type: "date", nullable: true),
                    ExceptedRevenue = table.Column<decimal>(type: "money", nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Objective = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(type: "date", nullable: true),
                    StatusID = table.Column<byte>(nullable: false),
                    TeamID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaign.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Campaign.Data_enum.Campaign.Type",
                        column: x => x.CampaignType,
                        principalTable: "enum.Campaign.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Campaign.Data_Campaign.Custom",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Campaign.Custom",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Campaign.Data_Employee.Team",
                        columns: x => new { x.BID, x.TeamID },
                        principalTable: "Employee.Team",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contact.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    BirthDate = table.Column<DateTime>(type: "date", nullable: true, computedColumnSql: "(datefromparts([BirthYear],[BirthMonth],[BirthDayOfMonth]))"),
                    BirthDayOfMonth = table.Column<byte>(nullable: true),
                    BirthMonth = table.Column<byte>(nullable: true),
                    BirthYear = table.Column<short>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((3000))"),
                    CompanyID = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    Department = table.Column<string>(maxLength: 100, nullable: true),
                    First = table.Column<string>(maxLength: 100, nullable: true),
                    GenderType = table.Column<bool>(nullable: true),
                    HasImage = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsBilling = table.Column<bool>(nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    Last = table.Column<string>(maxLength: 100, nullable: true),
                    LocationID = table.Column<byte>(nullable: true),
                    LongName = table.Column<string>(maxLength: 238, nullable: true, computedColumnSql: "(ltrim(rtrim(concat([Prefix]+' ',[First]+' ',left([Middle],(1))+'. ',[Last]+' ',[Suffix]))))"),
                    Middle = table.Column<string>(maxLength: 100, nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    NickName = table.Column<string>(maxLength: 100, nullable: true),
                    Position = table.Column<string>(maxLength: 100, nullable: true),
                    Prefix = table.Column<string>(maxLength: 16, nullable: true),
                    ShortName = table.Column<string>(maxLength: 201, nullable: true, computedColumnSql: "(ltrim(rtrim(concat(coalesce([First],[NickName])+' ',[Last]))))"),
                    Suffix = table.Column<string>(maxLength: 16, nullable: true),
                    Tags = table.Column<string>(nullable: true),
                    TimeZoneID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Contact.Data_enum.TimeZone",
                        column: x => x.TimeZoneID,
                        principalTable: "enum.TimeZone",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contact.Data_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Contact.Data_Contact.Custom",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Contact.Custom",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Contact.Data_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contact.Locator",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((3003))"),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsPrimary = table.Column<bool>(nullable: false, computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsValid = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsVerified = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    Locator = table.Column<string>(maxLength: 1024, nullable: true),
                    LocatorSubType = table.Column<byte>(nullable: false),
                    LocatorType = table.Column<byte>(nullable: false),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    ParentID = table.Column<int>(nullable: false),
                    RawInput = table.Column<string>(maxLength: 1024, nullable: false),
                    SortIndex = table.Column<short>(nullable: false, defaultValueSql: "2")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact.Locator", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Contact.Locator_enum.LocatorType",
                        column: x => x.LocatorType,
                        principalTable: "enum.Locator.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Contact.Locator_Contact.Data",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Contact.Locator_enum.LocatorSubType",
                        columns: x => new { x.LocatorType, x.LocatorSubType },
                        principalTable: "enum.Locator.SubType",
                        principalColumns: new[] { "LocatorType", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CRM.CustomField.Def",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9100))"),
                    DataType = table.Column<byte>(nullable: false),
                    FieldName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    InputType = table.Column<byte>(nullable: false),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.CustomField.Def", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField_enum.CustomField.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.CustomField.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField_enum.CustomField.InputType",
                        column: x => x.InputType,
                        principalTable: "enum.CustomField.InputType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "CRM.CustomField.Helper",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9101))"),
                    CompareValue = table.Column<string>(unicode: false, maxLength: 1024, nullable: true),
                    ComparisonType = table.Column<byte>(nullable: true),
                    DataType = table.Column<byte>(nullable: false),
                    HelperType = table.Column<byte>(nullable: false),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    IsWarningOnly = table.Column<bool>(nullable: false),
                    MaxValue = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    Message = table.Column<string>(unicode: false, maxLength: 1024, nullable: true),
                    MinValue = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    RegEx = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.CustomField.Helper", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.CustomField.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType",
                        column: x => x.HelperType,
                        principalTable: "enum.CustomField.HelperType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "CRM.CustomField.HelperLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    CustomFieldID = table.Column<short>(nullable: false),
                    HelperID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.CustomField.HelperLink", x => new { x.BID, x.CustomFieldID, x.HelperID });
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField",
                        columns: x => new { x.BID, x.CustomFieldID },
                        principalTable: "CRM.CustomField.Def",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper",
                        columns: x => new { x.BID, x.HelperID },
                        principalTable: "CRM.CustomField.Helper",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "Employee.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    BirthDate = table.Column<DateTime>(type: "date", nullable: true, computedColumnSql: "(datefromparts([BirthYear],[BirthMonth],[BirthDayOfMonth]))"),
                    BirthDayOfMonth = table.Column<byte>(nullable: true),
                    BirthMonth = table.Column<byte>(nullable: true),
                    BirthYear = table.Column<short>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((5000))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    First = table.Column<string>(maxLength: 100, nullable: false),
                    GenderType = table.Column<bool>(nullable: true),
                    HasImage = table.Column<bool>(nullable: false),
                    HireDate = table.Column<DateTime>(type: "date", nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    Last = table.Column<string>(maxLength: 100, nullable: false),
                    LocationID = table.Column<byte>(nullable: false, defaultValueSql: "((1))"),
                    LongName = table.Column<string>(maxLength: 238, nullable: true, computedColumnSql: "(ltrim(rtrim(concat([Prefix]+' ',[First]+' ',left([Middle],(1))+'. ',[Last]+' ',[Suffix]))))"),
                    Middle = table.Column<string>(maxLength: 100, nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    NickName = table.Column<string>(maxLength: 100, nullable: true),
                    Position = table.Column<string>(maxLength: 128, nullable: true),
                    Prefix = table.Column<string>(maxLength: 16, nullable: true),
                    ReleaseDate = table.Column<DateTime>(type: "date", nullable: true),
                    ReportsToID = table.Column<short>(nullable: true),
                    ShortName = table.Column<string>(maxLength: 201, nullable: true, computedColumnSql: "(ltrim(rtrim(concat(coalesce([First],[NickName])+' ',[Last]))))"),
                    Suffix = table.Column<string>(maxLength: 16, nullable: true),
                    TimeZoneID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Employee.Data_ReportsToID",
                        columns: x => new { x.BID, x.ReportsToID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Business.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    AssociationType = table.Column<byte>(nullable: true),
                    BillingEmployeeID = table.Column<short>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1000))"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    HasImage = table.Column<bool>(nullable: false),
                    ID = table.Column<short>(nullable: false, computedColumnSql: "(isnull(CONVERT([smallint],(1)),(1)))"),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    LegalName = table.Column<string>(maxLength: 255, nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    OwnerEmployeeID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Business.Data", x => x.BID);
                    table.ForeignKey(
                        name: "FK_Business.Data_Employee.Data1",
                        columns: x => new { x.BID, x.BillingEmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Business.Data_Employee.Data",
                        columns: x => new { x.BID, x.OwnerEmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employee.Locator",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((5003))"),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsPrimary = table.Column<bool>(nullable: false, computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsValid = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsVerified = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    Locator = table.Column<string>(maxLength: 1024, nullable: true),
                    LocatorSubType = table.Column<byte>(nullable: false),
                    LocatorType = table.Column<byte>(nullable: false),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    ParentID = table.Column<short>(nullable: false),
                    RawInput = table.Column<string>(maxLength: 1024, nullable: false),
                    SortIndex = table.Column<short>(nullable: false, defaultValueSql: "2")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.Locator", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Employee.Locator_enum.LocatorType",
                        column: x => x.LocatorType,
                        principalTable: "enum.Locator.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Employee.Locator_Employee.Data",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Employee.Locator_enum.LocatorSubType",
                        columns: x => new { x.LocatorType, x.LocatorSubType },
                        principalTable: "enum.Locator.SubType",
                        principalColumns: new[] { "LocatorType", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Employee.TeamLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    EmployeeID = table.Column<short>(nullable: false),
                    TeamID = table.Column<int>(nullable: false),
                    RoleType = table.Column<byte>(nullable: false, defaultValueSql: "((0))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.TeamLink", x => new { x.BID, x.EmployeeID, x.TeamID });
                    table.ForeignKey(
                        name: "FK_Employee.TeamLink_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Employee.TeamLink_Employee.Team",
                        columns: x => new { x.BID, x.TeamID },
                        principalTable: "Employee.Team",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "List.Filter.EmployeeSubscription",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    FilterID = table.Column<int>(nullable: false),
                    EmployeeID = table.Column<short>(nullable: false),
                    IsFavorite = table.Column<bool>(nullable: false),
                    IsShownAsTab = table.Column<bool>(nullable: false),
                    SortIndex = table.Column<byte>(nullable: false, defaultValueSql: "((50))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_List.Filter.EmployeeSubscription", x => new { x.BID, x.FilterID, x.EmployeeID });
                    table.ForeignKey(
                        name: "FK_List.Filter.EmployeeSubscription_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_List.Filter.EmployeeSubscription_List.Filter",
                        columns: x => new { x.BID, x.FilterID },
                        principalTable: "List.Filter",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "User.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    UserID = table.Column<int>(nullable: false),
                    ContactID = table.Column<int>(nullable: true),
                    EmployeeID = table.Column<short>(nullable: true),
                    RightGroupID = table.Column<short>(nullable: true),
                    RoleType = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User.Link", x => new { x.BID, x.UserID });
                    table.ForeignKey(
                        name: "FK_User.Link_enum.User.Role.Type",
                        column: x => x.RoleType,
                        principalTable: "enum.User.Role.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_User.Link_Contact.Data",
                        columns: x => new { x.BID, x.ContactID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_User.Link_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_User.Link_Security.Right.Collection",
                        columns: x => new { x.BID, x.RightGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Opportunity.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(type: "money", nullable: true),
                    CampaignID = table.Column<int>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9000))"),
                    ClosedDate = table.Column<DateTime>(type: "date", nullable: true),
                    CompanyID = table.Column<int>(nullable: true),
                    ContactID = table.Column<int>(nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false, defaultValueSql: "(getutcdate())"),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    ExpectedDate = table.Column<DateTime>(type: "date", nullable: true),
                    IsActive = table.Column<bool>(nullable: true, defaultValueSql: "((1))"),
                    LocationID = table.Column<byte>(nullable: false, defaultValueSql: "((1))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    Probability = table.Column<decimal>(type: "decimal(9, 4)", nullable: true),
                    SourceID = table.Column<int>(nullable: true),
                    StatusID = table.Column<byte>(nullable: false, defaultValueSql: "((0))"),
                    TeamID = table.Column<int>(nullable: true),
                    WeightedAmount = table.Column<decimal>(type: "money", nullable: true, computedColumnSql: "(CONVERT([money],([Amount]*[Probability])/(100)))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opportunity.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Opportunity.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Opportunity.Data_Campaign.Data",
                        columns: x => new { x.BID, x.CampaignID },
                        principalTable: "Campaign.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Opportunity.Data_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Opportunity.Data_Contact.Data",
                        columns: x => new { x.BID, x.ContactID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Opportunity.Data_Opportunity.Custom",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Opportunity.Custom",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Opportunity.Data_Employee.Team",
                        columns: x => new { x.BID, x.TeamID },
                        principalTable: "Employee.Team",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });
            
            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Term.Name",
                table: "Accounting.Payment.Term",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Term.LocationLink",
                table: "Accounting.Payment.Term.LocationLink",
                columns: new[] { "BID", "LocationID", "TermID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Assessment_Name",
                table: "Accounting.Tax.Assessment",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Group.AssessmentLink_Assessment",
                table: "Accounting.Tax.Group.AssessmentLink",
                columns: new[] { "BID", "AssessmentID", "GroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Group.LocationLink_Location",
                table: "Accounting.Tax.Group.LocationLink",
                columns: new[] { "BID", "LocationID", "GroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Contact",
                table: "Association.Data",
                column: "ContactID",
                filter: "([ContactID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Campaign",
                table: "Association.Data",
                columns: new[] { "BID", "CampaignID" },
                filter: "([CampaignID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Company",
                table: "Association.Data",
                columns: new[] { "BID", "CompanyID" },
                filter: "([CompanyID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Document",
                table: "Association.Data",
                columns: new[] { "BID", "DocumentID" },
                filter: "([DocumentID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Employee",
                table: "Association.Data",
                columns: new[] { "BID", "EmployeeID" },
                filter: "([EmployeeID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Lead",
                table: "Association.Data",
                columns: new[] { "BID", "LeadID" },
                filter: "([LeadID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Opportunity",
                table: "Association.Data",
                columns: new[] { "BID", "OpportunityID" },
                filter: "([OpportunityID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Association.Data_Team",
                table: "Association.Data",
                columns: new[] { "BID", "TeamID" },
                filter: "([TeamID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_Association",
                table: "Business.Data",
                columns: new[] { "AssociationType", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_BID_BillingEmployeeID",
                table: "Business.Data",
                columns: new[] { "BID", "BillingEmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_BID_OwnerEmployeeID",
                table: "Business.Data",
                columns: new[] { "BID", "OwnerEmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Business.Data_Business",
                table: "Business.Data",
                columns: new[] { "Name", "AssociationType" });

            migrationBuilder.Sql(@"CREATE PRIMARY XML INDEX 
[XML_IX_Business.Locator]
ON [Business.Locator] ([MetaData])");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Locator_ParentID",
                table: "Business.Locator",
                column: "ParentID");

            migrationBuilder.CreateIndex(
                name: "IX_Business.Locator_Parent",
                table: "Business.Locator",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Business.Locator_LocatorType_LocatorSubType",
                table: "Business.Locator",
                columns: new[] { "LocatorType", "LocatorSubType" });

            migrationBuilder.CreateIndex(
                name: "IX_Business.Locator_LoactorType",
                table: "Business.Locator",
                columns: new[] { "BID", "LocatorType", "LocatorSubType" });

            migrationBuilder.CreateIndex(
                name: "IX_Campaign.Data_CampaignType",
                table: "Campaign.Data",
                column: "CampaignType");

            migrationBuilder.CreateIndex(
                name: "IX_Campaign.Data_BID_TeamID",
                table: "Campaign.Data",
                columns: new[] { "BID", "TeamID" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_StatusID",
                table: "Company.Data",
                column: "StatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_TimeZoneID",
                table: "Company.Data",
                column: "TimeZoneID");

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_BID_IndustryID",
                table: "Company.Data",
                columns: new[] { "BID", "IndustryID" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_BID_LocationID",
                table: "Company.Data",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_Parent",
                table: "Company.Data",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_BID_SourceID",
                table: "Company.Data",
                columns: new[] { "BID", "SourceID" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_BID_TaxGroupID",
                table: "Company.Data",
                columns: new[] { "BID", "TaxGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_Name",
                table: "Company.Data",
                columns: new[] { "BID", "Name", "StatusID", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_Status",
                table: "Company.Data",
                columns: new[] { "BID", "StatusID", "IsActive", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_Team",
                table: "Company.Data",
                columns: new[] { "BID", "TeamID", "Name", "IsActive" });

            migrationBuilder.Sql(@"CREATE PRIMARY XML INDEX 
[XML_IX_Company.Locator]
ON [Company.Locator] ([MetaData])");

            migrationBuilder.CreateIndex(
                name: "IX_Company.Locator_LocatorType_LocatorSubType",
                table: "Company.Locator",
                columns: new[] { "LocatorType", "LocatorSubType" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Locator_Parent",
                table: "Company.Locator",
                columns: new[] { "BID", "ParentID", "LocatorType" });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Locator_Locator",
                table: "Company.Locator",
                columns: new[] { "BID", "LocatorType", "LocatorSubType", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Data_TimeZoneID",
                table: "Contact.Data",
                column: "TimeZoneID");

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Data_Last",
                table: "Contact.Data",
                columns: new[] { "BID", "Last" });

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Data_BID_LocationID",
                table: "Contact.Data",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Data_Short",
                table: "Contact.Data",
                columns: new[] { "BID", "ShortName" });

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Data_Company",
                table: "Contact.Data",
                columns: new[] { "BID", "CompanyID", "IsDefault", "IsBilling" });

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Locator_Parent",
                table: "Contact.Locator",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Locator_LocatorType_LocatorSubType",
                table: "Contact.Locator",
                columns: new[] { "LocatorType", "LocatorSubType" });

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Locator",
                table: "Contact.Locator",
                columns: new[] { "BID", "LocatorType", "LocatorSubType", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.CustomField.Def_DataType",
                table: "CRM.CustomField.Def",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_CRM.CustomField.Def_InputType",
                table: "CRM.CustomField.Def",
                column: "InputType");

            migrationBuilder.CreateIndex(
                name: "IX_CRM.CustomField.Helper_DataType",
                table: "CRM.CustomField.Helper",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_CRM.CustomField.Helper_HelperType",
                table: "CRM.CustomField.Helper",
                column: "HelperType");

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Setup.CustomField.HelperLink",
                table: "CRM.CustomField.HelperLink",
                columns: new[] { "BID", "HelperID", "CustomFieldID" });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Industry_Name",
                table: "CRM.Industry",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Industry_Parent",
                table: "CRM.Industry",
                columns: new[] { "BID", "ParentID", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Source_Name",
                table: "CRM.Source",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Source_Parent",
                table: "CRM.Source",
                columns: new[] { "BID", "ParentID", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.DataLocation",
                table: "Employee.Data",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.Data_BID_ReportsToID",
                table: "Employee.Data",
                columns: new[] { "BID", "ReportsToID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.Data_LastName",
                table: "Employee.Data",
                columns: new[] { "BID", "Last", "First" });

            migrationBuilder.Sql(@"CREATE PRIMARY XML INDEX 
[XML_IX_Employee.Locator]
ON [Employee.Locator] ([MetaData])");

            migrationBuilder.CreateIndex(
                name: "IX_Employee.Locator_LocatorType_LocatorSubType",
                table: "Employee.Locator",
                columns: new[] { "LocatorType", "LocatorSubType" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.Locator_LocatorType",
                table: "Employee.Locator",
                columns: new[] { "BID", "LocatorType", "LocatorSubType" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.Locator_parent",
                table: "Employee.Locator",
                columns: new[] { "BID", "ParentID", "LocatorType" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.Team.LocationLink_BID_LocationID",
                table: "Employee.Team.LocationLink",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TeamLink_BID_TeamID",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "TeamID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TeamLink_Employee",
                table: "Employee.TeamLink",
                columns: new[] { "BID", "EmployeeID", "TeamID" });

            migrationBuilder.CreateIndex(
                name: "IX_enum.CustomField.InputType_DataType",
                table: "enum.CustomField.InputType",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_enum.TimeZone_DisplayName",
                table: "enum.TimeZone",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_enum.TimeZone_StandardName",
                table: "enum.TimeZone",
                column: "StandardName");

            migrationBuilder.CreateIndex(
                name: "IX_List.Filter_TargetCTID",
                table: "List.Filter",
                columns: new[] { "TargetClassTypeID", "IsSystem", "BID" });

            migrationBuilder.CreateIndex(
                name: "IX_List.Filter.Subscription_Employee",
                table: "List.Filter.EmployeeSubscription",
                columns: new[] { "BID", "EmployeeID", "FilterID" });

            migrationBuilder.CreateIndex(
                name: "IX_Location.Data_TimeZoneID",
                table: "Location.Data",
                column: "TimeZoneID");

            migrationBuilder.CreateIndex(
                name: "IX_Location.Data_BID_DefaultTaxGroupID",
                table: "Location.Data",
                columns: new[] { "BID", "DefaultTaxGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Location.Data_Name",
                table: "Location.Data",
                columns: new[] { "BID", "Name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location.Locator_Parent",
                table: "Location.Locator",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Location.Locator_LocatorType_LocatorSubType",
                table: "Location.Locator",
                columns: new[] { "LocatorType", "LocatorSubType" });

            migrationBuilder.CreateIndex(
                name: "IX_Location.Locator_LocatorType",
                table: "Location.Locator",
                columns: new[] { "BID", "LocatorType", "LocatorSubType", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Opportunity.Data_BID_CampaignID",
                table: "Opportunity.Data",
                columns: new[] { "BID", "CampaignID" });

            migrationBuilder.CreateIndex(
                name: "IX_Opportunity_Company",
                table: "Opportunity.Data",
                columns: new[] { "BID", "CompanyID" },
                filter: "([CompanyID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Opportunity_Contact",
                table: "Opportunity.Data",
                columns: new[] { "BID", "ContactID" },
                filter: "([ContactID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Opportunity.Data_BID_TeamID",
                table: "Opportunity.Data",
                columns: new[] { "BID", "TeamID" });

            migrationBuilder.CreateIndex(
                name: "IX_Option.Data_OptionID_Level_BIDNull",
                table: "Option.Data",
                columns: new[] { "OptionID", "OptionLevel" },
                filter: "([BID] IS NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_Option.Data_BID_OptionID_Level",
                table: "Option.Data",
                columns: new[] { "BID", "OptionID", "OptionLevel" });

            migrationBuilder.CreateIndex(
                name: "IX_Security.Right.Collection_Name",
                table: "Security.Right.Group",
                columns: new[] { "BID", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_Security.Right.Group.Link_Child",
                table: "Security.Right.Group.Link",
                columns: new[] { "BID", "ChildGroupID", "ParentGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Security.Right.Link_Right",
                table: "Security.Right.Link",
                columns: new[] { "BID", "RightID", "RightGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Security.Role.Link_RoleType",
                table: "Security.Role.Link",
                column: "RoleType");

            migrationBuilder.CreateIndex(
                name: "IX_Security.Role.Link_Group",
                table: "Security.Role.Link",
                columns: new[] { "BID", "RightGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_System.List.Column_TargetCTID",
                table: "System.List.Column",
                columns: new[] { "TargetClassTypeID", "SortIndex", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_System.List.Filter.Criteria_TargetCTID",
                table: "System.List.Filter.Criteria",
                columns: new[] { "TargetClassTypeID", "SortIndex", "Name" });

            migrationBuilder.CreateIndex(
                name: "[IX_System.Option.Category_SectionID",
                table: "System.Option.Category",
                columns: new[] { "SectionID", "Name", "IsHidden", "OptionLevels" });

            migrationBuilder.CreateIndex(
                name: "IX_System.Option.Definition_DataType",
                table: "System.Option.Definition",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_System.Option.Definition_CategoryID",
                table: "System.Option.Definition",
                columns: new[] { "CategoryID", "Name", "IsHidden" });

            migrationBuilder.CreateIndex(
                name: "IX_System.Option.Definition_Name",
                table: "System.Option.Definition",
                columns: new[] { "Name", "CategoryID", "IsHidden" });

            migrationBuilder.CreateIndex(
                name: "IX_System.Option.Section_ParentID",
                table: "System.Option.Section",
                columns: new[] { "ParentID", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_User.Link_RoleType",
                table: "User.Link",
                column: "RoleType");

            migrationBuilder.CreateIndex(
                name: "IX_User.Link_BID_ContactID",
                table: "User.Link",
                columns: new[] { "BID", "ContactID" });

            migrationBuilder.CreateIndex(
                name: "IX_User.Link_BID_EmployeeID",
                table: "User.Link",
                columns: new[] { "BID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_User.Link_BID_RightGroupID",
                table: "User.Link",
                columns: new[] { "BID", "RightGroupID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Term.LocationLink_Location.Data",
                table: "Accounting.Payment.Term.LocationLink",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Tax.Group.LocationLink_Location.Data",
                table: "Accounting.Tax.Group.LocationLink",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Location.Data",
                table: "Company.Data",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Business.Data",
                table: "Company.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Location.Data_Business.Data",
                table: "Location.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Business.Locator_Business.Data_ParentID",
                table: "Business.Locator",
                column: "ParentID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Data_Business.Data",
                table: "Campaign.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Business.Data",
                table: "Contact.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField_Business.Data",
                table: "CRM.CustomField.Def",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_CRM.Setup.CustomField.Helper_Business.Data",
                table: "CRM.CustomField.Helper",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Data_Business.Data",
                table: "Employee.Data",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.Sql(@"CREATE VIEW[Util.ID.NewGUID] AS SELECT NewID() AS New_ID");
            
            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[Accounting.Payment.Term.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.Payment.Term]");
            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[Accounting.Tax.Assessment.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.Tax.Assessment]");
            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[Accounting.Tax.Group.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.Tax.Group]");
            migrationBuilder.Sql(@"	
CREATE VIEW [dbo].[Business.Location.SimpleList] AS
    SELECT [BID]
          ,[ID]
          ,[ClassTypeID]
          ,[Name] as DisplayName
          ,[IsActive]
		  ,[HasImage]
          ,[IsDefault]
      FROM [Location.Data]");
            migrationBuilder.Sql(@"
CREATE VIEW dbo.[Business.SimpleList]
AS
SELECT        BID, BID AS ID, ClassTypeID, Name AS DisplayName, IsActive, HasImage, CONVERT(BIT, 0) AS IsDefault
FROM            dbo.[Business.Data]");
            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[Company.SimpleList] AS
    SELECT [BID]
        ,[ID]
        ,[ClassTypeID]
        ,[Name] as DisplayName
        ,[IsActive]
        ,[HasImage]
        ,CONVERT(BIT, 0) AS [IsDefault]
    FROM [Company.Data]");
            migrationBuilder.Sql(@"
	CREATE VIEW dbo.[Contact.SimpleList]
AS
SELECT        BID, ID, ClassTypeID, ShortName AS DisplayName, IsActive, HasImage, CONVERT(BIT, 0) AS IsDefault
FROM            dbo.[Contact.Data]");
            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[CRM.Industry.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [CRM.Industry]");
            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[CRM.Source.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [CRM.Source]");
            migrationBuilder.Sql(@"
CREATE VIEW dbo.[Employee.SimpleList]
AS
SELECT        BID, ID, ClassTypeID, ShortName AS DisplayName, IsActive, HasImage, CONVERT(BIT, 0) AS IsDefault
FROM            dbo.[Employee.Data]");
            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[Employee.Team.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Employee.Team]
    WHERE IsAdHocTeam = 0");
            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[enum.TimeZone.SimpleList] AS
    SELECT
          [ID]
        , [Name] as DisplayName
        , [IsCommon] as [IsActive]
        , CONVERT(BIT, 0) AS [HasImage]
    FROM [enum.TimeZone]");
            migrationBuilder.Sql(@"
CREATE VIEW [Location.SimpleList] AS
    SELECT [BID]
        ,[ID]
        ,[ClassTypeID]
        ,[Name] as DisplayName
        ,[IsActive]
        ,[HasImage]
        ,[IsDefault]
    FROM [Location.Data]");

            migrationBuilder.Sql(@"
CREATE FUNCTION [User.Security.Rights.String] ( @BID int, @UserID int )
RETURNS BINARY(128)
AS
BEGIN
    DECLARE @ResultSize SMALLINT = 128
	      , @Found BIT = 0
          , @Results BINARY(128) = 0; 

    DECLARE @Temp Table (RightID smallint, ByteIndex tinyint, BytePos tinyint, ByteFlag tinyint);

    WITH Rights(BID, GroupID)
    AS
    (
        -- Anchor Query

        -- Pull the users ROLE security group
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Role.Link] SRoL on SRoL.BID = U.BID AND SRoL.RoleType = U.RoleType
        JOIN [Security.Right.Group] SRG on SRG.BID = SRoL.BID AND SRG.ID = SRoL.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Merge with the users top-level security group
        UNION ALL 
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Right.Group] SRG on SRG.BID = U.BID AND SRG.ID = U.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Now merge with recursively with Security Groups
        UNION ALL
        SELECT G.BID, G.ID
        FROM Rights R 
        JOIN [Security.Right.Group.Link] L ON L.BID = R.BID AND L.ParentGroupID = R.GroupID
        JOIN [Security.Right.Group] G ON G.BID = L.BID AND G.ID = L.ChildGroupID
    )
        SELECT @Results = SUBSTRING(@Results, 1, @ResultSize-ByteIndex) + CONVERT(BINARY(1),CHAR(Flags)) + SUBSTRING(@Results, @ResultSize-(ByteIndex-2), ByteIndex-1)
		     , @Found = 1  -- use this to determine that we really found results.  Will not get set so @Found will remain 0 if not
        FROM (
            -- Sum the Byte Flag by ByteIndex to get the character to insert
            SELECT ByteIndex, SUM(ByteFlag) Flags
            FROM (
                SELECT DISTINCT 
                      (RL.RightID / 8)+1 as ByteIndex
                    , Power(2, RL.RightID % 8) as ByteFlag
                    --, (RL.RightID % 8) as BytePos
                    -- , RL.RightID
                FROM Rights R
                JOIN [Security.Right.Link] RL on RL.BID = R.BID and RL.RightGroupID = R.GroupID
            ) RightsList
            GROUP By ByteIndex
        ) RightsString
        ;

		IF (@Found=0) SET @Results = NULL;

        RETURN @Results;
END;");
            migrationBuilder.Sql(@"
CREATE FUNCTION [dbo].[List.Filter.MyLists] (@BID SMALLINT, @EmployeeID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT L.* 
    FROM [List.Filter] L
    WHERE BID = @BID 
      AND ID IN (
                    SELECT L.ID
                    FROM [List.Filter] L
                    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
                      AND (L.IsSystem=1 OR (L.BID = @BID AND L.OwnerID = @EmployeeID)) 
    
                    UNION

                    SELECT L.ID
                    FROM [List.Filter] L
                    JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = L.BID AND LS.FilterID = L.ID
                    WHERE (L.BID = @BID) 
                        AND (L.TargetClassTypeID = @TargetClassTypeID)
                        AND (LS.EmployeeID = @EmployeeID)
                )
)");
            migrationBuilder.Sql(@"
CREATE FUNCTION [dbo].[List.Filter.SimpleList] (@BID SMALLINT, @EmployeeID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT TOP 100 PERCENT 
           @BID as BID
         , L.[ID]
         , L.[ClassTypeID]
         , L.[Name] as DisplayName
         , L.[IsActive]
         , CONVERT(BIT,0) AS [HasImage]
         , L.TargetClassTypeID
         , (CASE WHEN LS.FilterID IS NOT NULL THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END) AS IsSubscribed
         , LS.IsShownAsTab
         , LS.IsFavorite
         , L.IsSystem
    FROM [List.Filter] L
    LEFT JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = @BID AND LS.FilterID = L.ID
    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
      AND (L.IsSystem=1 
           OR (L.BID = @BID 
               AND (L.IsPublic=1 OR L.OwnerID = @EmployeeID)
               )
          )
    ORDER BY [Name]
)");
            migrationBuilder.Sql(@"
CREATE FUNCTION [dbo].[Option.Categories.ByLevel] ( @Level tinyint )
		RETURNS TABLE
AS

RETURN 

SELECT
TOP 1000
	a.[ID]
	,a.[Name]
	,a.[SectionID]
	,a.[Description]
	,a.[OptionLevels]
	,a.[IsHidden]
	,a.[IsSystemOption]
	,a.[IsAssociationOption]
	,a.[IsBusinessOption]
	,a.[IsLocationOption]
	,a.[IsStorefrontOption]
	,a.[IsEmployeeOption]
	,a.[IsCompanyOption]
	,a.[IsContactOption]
FROM [dbo].[System.Option.Category] a
WHERE OptionLevels = @Level
ORDER BY a.[Name] ASC");
            migrationBuilder.Sql(@"
CREATE FUNCTION [User.Security.Rights.List] ( @BID int, @UserID int )
RETURNS TABLE
AS
RETURN
(
    WITH Rights(BID, GroupID)
    AS
    (
        -- Anchor Query

        -- Pull the users ROLE security group
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Role.Link] SRoL on SRoL.BID = U.BID AND SRoL.RoleType = U.RoleType
        JOIN [Security.Right.Group] SRG on SRG.BID = SRoL.BID AND SRG.ID = SRoL.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Merge with the users top-level security group
        UNION ALL 
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Right.Group] SRG on SRG.BID = U.BID AND SRG.ID = U.RightGroupID
        LEFT JOIN [Security.Right.Link] SRiL on SRiL.BID = SRG.BID AND SRiL.RightGroupID = SRG.ID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Now merge with recursively with Security Groups
        UNION ALL
        SELECT G.BID, G.ID
        FROM Rights R 
        JOIN [Security.Right.Group.Link] L ON L.BID = R.BID AND L.ParentGroupID = R.GroupID
        JOIN [Security.Right.Group] G ON G.BID = L.BID AND G.ID = L.ChildGroupID
    )
        SELECT DISTINCT RL.BID, RL.RightID 
        FROM Rights R
        JOIN [Security.Right.Link] RL on RL.BID = R.BID and RL.RightGroupID = R.GroupID
);");
            migrationBuilder.Sql(@"
            CREATE FUNCTION[Util.ID.BIDFromTimeStampGUID](@ID UniqueIdentifier)
RETURNS SMALLINT
AS
BEGIN
    RETURN CONVERT(SMALLINT, CONVERT(VARBINARY(2), RIGHT(CONVERT(VARBINARY(16), @ID), 2)));
            END");

            migrationBuilder.Sql(@"CREATE FUNCTION [Util.ID.NewTimeStampGUID](@BID SMALLINT)
RETURNS UniqueIdentifier
AS
BEGIN
    DECLARE @ResultArray VARBINARY(16)
          , @DTArray     VARBINARY(07)
          ;

           
           SELECT  @ResultArray = CONVERT(VARBINARY(16), (SELECT New_ID FROM[Util.ID.NewGUID]))
          , @DTArray = CONVERT(VARBINARY(07), CONVERT(DateTime2(2), GetDate()))
          ;

            SELECT @ResultArray = CONVERT(VARBINARY(16),
                                       CONVERT(VARBINARY(8), LEFT(@ResultArray, 16))
                                    + CONVERT(VARBINARY(6), RIGHT(@DTArray, 7))
                                    + CONVERT(VARBINARY(2), @BID)
                                    )
            ;

            --Return the result of the function
           RETURN CONVERT(UniqueIdentifier, @ResultArray)
END");
            migrationBuilder.Sql(@"CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Recalc]
                                @BID SMALLINT
AS
BEGIN
    DECLARE @DT DateTime2(2) = GetDate();

    DECLARE @T TABLE(
        BID SMALLINT
      , ID SMALLINT
      , ClassTypeID SMALLINT
      , OldRate DECIMAL(12,4)
      , NewRate DECIMAL(12,4)
    );

    -- Insert into the Temp Table
    --   All records where the Group.TaxRate != Sum(Assessment.TaxRate)
    INSERT INTO @T
        SELECT G.BID, G.ID, G.ClassTypeID, COALESCE(G.TaxRate,0.0000), SUM(A.TaxRate)
        FROM [Accounting.Tax.Group] G
        JOIN [Accounting.Tax.Group.AssessmentLink] GAL ON (GAL.BID = G.BID AND GAL.GroupID = G.ID)
        JOIN [Accounting.Tax.Assessment] A             ON (A.BID = GAL.BID AND A.ID = GAL.AssessmentID)
        WHERE G.BID = @BID
          AND A.IsActive = 1
        GROUP BY G.BID, G.ID, G.ClassTypeID, COALESCE(G.TaxRate,0.0000)
        HAVING COALESCE(G.TaxRate,0.0000) != SUM(A.TaxRate)
        ORDER BY G.BID, G.ID
    ;

    -- Update the Tax Groups
    UPDATE G
    SET ModifiedDT = @DT
      , TaxRate = T.NewRate
    FROM [Accounting.Tax.Group] G
    JOIN @T T ON (T.BID = G.BID AND T.ID = G.ID)
    ;

    -- Output the Result
    SELECT * FROM @T
    ;
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [dbo].[Location.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @LocationID     INT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that some other Location is Active
    IF (@IsActive = 0) AND NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID != @LocationID and IsActive=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to Set The Only Active Location Inactive. Set another Location active first before setting this one inactive.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that this is not the default location
    IF (@IsActive = 0) AND EXISTS(SELECT * FROM [Location.Data] WHERE BID=@BID and ID=@LocationID and IsDefault=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to Set The Default Location Inactive. Set another Location as the Default first before setting this one Inactive.'
             ;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Location.Data] L
    WHERE BID = @BID and ID = @LocationID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [dbo].[Location.Action.SetActiveMultiple]
-- DECLARE 
          @BID            TINYINT		-- = 1
        , @LocationIDs    VARCHAR(1024) -- = '1,2'
        , @IsActive       BIT			-- = 1
        , @Result         INT     = NULL	OUTPUT
AS
BEGIN
	DECLARE @Message VARCHAR(MAX) = '';

	DECLARE @TempTable TABLE (
			  BID SMALLINT NOT NULL
			, LocationId SMALLINT  NULL
			, IsActive BIT  NULL
			, IsSuccess BIT NOT NULL
			, ErrorMessage VARCHAR(1024)
			);

	INSERT INTO @TempTable( BID, LocationID, IsActive, IsSuccess, ErrorMessage)
		SELECT @BID
		     , LData.ID
			 , LData.IsActive
			 , (CASE WHEN LData.ID IS NULL THEN 0 
			         WHEN @IsActive=0 AND IsDefault=1 THEN 0 
					 ELSE  1 END )
			 , (CASE WHEN LData.ID IS NULL THEN 'Location Not Found; ' 
			         WHEN @IsActive=0 AND IsDefault=1 THEN 'Attempting to set the Default Location to Inactive. Set another Location as the Default first before setting this one to Inactive.; '
			         ELSE '' END )
		FROM string_split(@LocationIDs,',') LInput 
		LEFT JOIN [Location.Data] LData ON LData.BID = @BID AND LData.ID = CONVERT(INT, LInput.[value])
	;

	-- Check that we don't have any errors
	SELECT @Message += ErrorMessage
	FROM @TempTable
	WHERE IsSuccess = 0
	;

 	-- Check that some other Location is Active
	IF (@IsActive = 0) 
		AND NOT EXISTS( SELECT * 
						FROM [Location.Data] LData 
						WHERE BID = @BID AND IsActive=1 and ID NOT IN (SELECT LocationID FROM @TempTable) )
		SET @Message = @Message + ' Attempting to set the only Active Location(s) to Inactive. Set another Location to active first before setting this one to Inactive.';


	IF LEN(@Message) > 1 
		THROW 50000, @Message, 1
	ELSE
		-- Now update it
		UPDATE L
		SET   IsActive   = @IsActive
			, ModifiedDT = GetUTCDate()
		FROM [Location.Data] L
		JOIN @TempTable T on L.BID = @BID AND L.ID = T.LocationId
		WHERE COALESCE(L.IsActive,~@IsActive) != @IsActive

	SET @Result = @@ROWCOUNT;

	SELECT @Result AS Result;
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [dbo].[Location.Action.SetDefault]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @LocationID     INT     -- = 2

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Location.Data]);

	IF (@Count > 1)
	BEGIN
		-- Remove IsDefault from any other Default location(s) for the business
		UPDATE L
		SET IsDefault = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Location.Data] L
		WHERE BID = @BID AND IsDefault = 1
	END

    -- Now update it
    UPDATE L
    SET IsActive   = 1
		, IsDefault = 1
        , ModifiedDT = GetUTCDate()
    FROM [Location.Data] L
    WHERE BID = @BID and ID = @LocationID
		AND COALESCE(IsDefault,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [Option.DeleteCategoryOptions]
-- DECLARE 
            @CategoryID        INT          = NULL
			,@CategoryName     VARCHAR(255) = NULL
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@CategoryID IS NULL) AND (@CategoryName IS NULL)) OR ((@CategoryID IS NOT NULL) AND (@CategoryName IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @CategoryID or the @CategoryName.', 1;

    -- ======================================
    -- Lookup CategoryID
    -- ======================================
    IF (@CategoryID IS NULL)
    BEGIN
        SELECT @CategoryID = ID 
        FROM [System.Option.Category] 
        WHERE Name = @CategoryName;

        IF (@CategoryID IS NULL)
			THROW 180000, '@CategoryName not found.', 1;
    END;

	WITH Category (ID, Name, Value, Level, CategoryID, CategoryName, SectionName)
	AS
	(
		SELECT D.ID, D.Name, O.Value, O.OptionLevel, C.ID, C.Name, S.Name
		FROM [System.Option.Definition] D
		INNER JOIN [Option.Data] O ON O.OptionID = D.ID
		INNER JOIN [System.Option.Category] C ON C.ID = D.CategoryID
		INNER JOIN [System.Option.Section] S ON S.ID = C.SectionID
	
		WHERE CategoryID = @CategoryID
	)
	DELETE FROM [Option.Data] WHERE OptionID IN (SELECT ID FROM Category)
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [dbo].[Option.DeleteValue]
-- DECLARE 
            @OptionID       INT          = NULL
          , @OptionName     VARCHAR(255) = NULL

          , @AssociationID  TINYINT      = NULL
          , @BID            SMALLINT     = NULL
          , @LocationID     SMALLINT     = NULL
          , @EmployeeID     SMALLINT     = NULL
          , @CompanyID      INT          = NULL
          , @ContactID      INT          = NULL
          , @StorefrontID   SMALLINT	 = NULL

AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
          , @Level  TINYINT     
          ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
             , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

	DELETE FROM [Option.Data] WHERE Value = @Result AND OptionLevel = @Level AND OptionID = @OptionID

	IF ((SELECT COUNT(*) FROM [Option.Data] WHERE OptionID = @OptionID) = 0)
	BEGIN
		-- Clear out definition if none in data
		DELETE FROM [System.Option.Definition] WHERE ID = @OptionID
	END

	
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [Option.DeleteValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT       = NULL
    , @BID            SMALLINT      = NULL
    , @LocationID     SMALLINT      = NULL
    , @EmployeeID     SMALLINT      = NULL
    , @CompanyID      INT           = NULL
    , @ContactID      INT           = NULL
    , @StorefrontID   SMALLINT      = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
              RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing  IDs from the Definitions Table
    -- -------------------------------------------------

    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

	DECLARE @v1 XML = (SELECT * FROM @Options FOR XML AUTO)
	DECLARE @v2 XML = (SELECT * FROM @Options_Array FOR XML AUTO)
    -- Remove missing IDs (AdHoc)
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
    ;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
	DECLARE @v3 XML = (select * from [Option.Data] D  FOR XML AUTO)

    UPDATE Opt
    SET InstanceID = 
           CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.LocationID = @LocationID)

                WHEN @EmployeeID IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.EmployeeID = @EmployeeID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT Top 1  ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID)

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.AssociationID = @AssociationID)

           ELSE NULL
           END
    FROM @Options Opt
    ;

    IF (@Debug=1)
        SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    ;
END");

            migrationBuilder.Sql(@"
CREATE PROCEDURE [Option.GetCategoryOptions]
-- DECLARE 
            @CategoryID        INT          = NULL
			,@CategoryName     VARCHAR(255) = NULL
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@CategoryID IS NULL) AND (@CategoryName IS NULL)) OR ((@CategoryID IS NOT NULL) AND (@CategoryName IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @CategoryID or the @CategoryName.', 1;

    -- ======================================
    -- Lookup CategoryID
    -- ======================================
    IF (@CategoryID IS NULL)
    BEGIN
        SELECT @CategoryID = ID 
        FROM [System.Option.Category] 
        WHERE Name = @CategoryName;

        IF (@CategoryID IS NULL)
			THROW 180000, '@CategoryName not found.', 1;
    END;

	WITH Category (ID, Name, Value, Level, CategoryID, CategoryName, SectionName)
	AS
	(
		SELECT D.ID, D.Name, O.Value, O.OptionLevel, C.ID, C.Name, S.Name
		FROM [System.Option.Definition] D
		INNER JOIN [Option.Data] O ON O.OptionID = D.ID
		INNER JOIN [System.Option.Category] C ON C.ID = D.CategoryID
		INNER JOIN [System.Option.Section] S ON S.ID = C.SectionID
	
		WHERE CategoryID = @CategoryID
	)
	SELECT *
	FROM Category
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [Option.GetSection]
-- DECLARE 
            @SectionID        INT          = NULL
			,@SectionName     VARCHAR(255) = NULL
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@SectionID IS NULL) AND (@SectionName IS NULL)) OR ((@SectionID IS NOT NULL) AND (@SectionName IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @SectionID or the @SectionName.', 1;

    -- ======================================
    -- Lookup SectionID
    -- ======================================
    IF (@SectionID IS NULL)
    BEGIN
        SELECT @SectionID = ID 
        FROM [System.Option.Section] 
        WHERE Name = @SectionName;

        IF (@SectionID IS NULL)
			THROW 180000, '@SectionName not found.', 1;
    END;

	WITH Section (LevelType, ID, Level, Name, Description, ParentID, ImageName)
	AS
	(
		SELECT CONVERT(VARCHAR(12),'Section'), S.ID, CONVERT(TINYINT, 0), S.Name, CONVERT(VARCHAR(max), NULL), CONVERT(SMALLINT, NULL) AS ParentID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Section] S
		WHERE S.ID = @SectionID

		UNION ALL

		SELECT CONVERT(VARCHAR(12),'Section'), S.ID, CONVERT(TINYINT, P.Level+1), S.Name, CONVERT(VARCHAR(max), NULL), P.ID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Section] S
		JOIN Section P on P.ID = S.ParentID AND P.LevelType = 'Section'

		UNION ALL

		SELECT CONVERT(VARCHAR(12),'Category'), C.ID, CONVERT(TINYINT, P.Level+1), C.Name, C.Description, P.ID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Category] C
		JOIN Section P on P.ID = C.SectionID AND P.LevelType = 'Section'
	)
	SELECT *
	FROM Section S
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [Option.GetValue]
 --DECLARE 
        @OptionID       INT          = NULL -- = 2
      , @OptionName     VARCHAR(255) = NULL -- 'GLAccount.TaxName1'

      , @AssociationID  tinyint      = NULL
	  , @BID            smallint     = NULL
	  , @LocationID     smallint     = NULL
	  , @StoreFrontID   smallint     = NULL
	  , @EmployeeID     smallint     = NULL
	  , @CompanyID      int          = NULL
	  , @ContactID      int          = NULL
AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
          , @Level  TINYINT     
          ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition]
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
	    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

		IF (@AnswerCount > 1)
			THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
             , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    SELECT 
	    CAST (@OptionID AS SMALLINT) AS ID -- needed in the EF
		,@Result as [Value]
        , @Level as [OptionLevel];
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [dbo].[Option.GetValues]
-- DECLARE 
		@AssociationID  tinyint      = NULL
		,@BID            smallint     = NULL
		,@LocationID     smallint     = NULL
		,@StoreFrontID   smallint     = NULL
		,@EmployeeID     smallint     = NULL
		,@CompanyID      int          = NULL
		,@ContactID      int          = NULL
AS

BEGIN
	DECLARE @Result TABLE (
		ID INT
		,[Name] VARCHAR(255)
		,[Value] VARCHAR(255)
		,[Level] TINYINT
		,CategoryID SMALLINT
		,CategoryName VARCHAR(255)
		,SectionName VARCHAR(255)
	)

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
		INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        INSERT INTO @Result 
		SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN  [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF NOT EXISTS (SELECT COUNT(*) FROM @Result) 
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID IS NULL
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF NOT EXISTS (SELECT COUNT(*) FROM @Result) 
        INSERT INTO @Result
		SELECT 
		0,
		'Default',
		DefaultValue, 
		0,
		0,
		'Default Category',
		'Default Section'
        FROM [System.Option.Definition]
        --WHERE ID = @OptionID
    ;

    SELECT 
	ID
	,[Name]
	,[Value]
	,[Level]
	,CategoryID
	,CategoryName
	,SectionName
	FROM @Result;
END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [Util.ID.GetID] 
        @BID smallint
      , @ClassTypeID int

      -- Optional fields
      , @Count      int = 1   -- how many IDs are requested.  The first ID in a sequential block is returned.
      , @StartingID int = 1000
AS
BEGIN
    DECLARE @NextID INT;
    IF (@BID IS NULL) SET @BID = -1; -- Use -1 for System

    -- Assume the record exists ... 
    UPDATE [Util.NextID]
    SET @NextID = NextID = COALESCE(NextID, @StartingID-1) + @Count
    WHERE BID = @BID AND ClassTypeID = @ClassTypeID
    ;

    -- Check if no rows found, in which case use an add
    IF (@@RowCount=0)
    BEGIN
        SET @NextID = @StartingID + @Count;
        INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) VALUES(@BID, @ClassTypeID, @NextID);
    END;

    SELECT (@NextID-@Count) as FirstID;
    RETURN (@NextID-@Count);
END;");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [Util.ID.ValidateTableID] 
        @BID smallint
      , @ClassTypeID int
      , @TableName  varchar(155) = '' -- The corresponding tablename.  Only used when @ValidateTable=1
AS
BEGIN
    DECLARE @NextTrackingID INT =  (
                    SELECT NextID
                    FROM [Util.NextID]
                    WHERE BID = @BID AND ClassTypeID = @ClassTypeID
                );

    DECLARE @NextTableID int
            , @cmd Nvarchar(max) = 'SELECT @outvar = MAX(ID)+1 FROM ['+@TableName+'] WHERE BID='+CONVERT(VARCHAR(12), @BID);

    EXEC SP_ExecuteSQL @Query = @cmd
                    , @Params = N'@outvar INT OUTPUT'
                    , @outvar = @NextTableID OUTPUT
    ;

    IF (@NextTrackingID IS NULL)
    BEGIN
        DECLARE @IDDataType VARCHAR(32)
              , @StartingID INT
              ;

        -- The the NextTableID is not high, we need to check if it is below the max
        IF (@NextTableID > 1000)
            SET @StartingID = 1000

        -- Look up the Data Type for the ID field
        ELSE
        BEGIN
            SET @IDDataType = (
                        SELECT DATA_TYPE 
                        FROM INFORMATION_SCHEMA.COLUMNS
                        WHERE TABLE_NAME = @TableName
                        AND COLUMN_NAME = 'ID'
                    );

            -- Set the starting value based on it
            SET @StartingID = (
                        CASE @IDDataType
                            WHEN 'tinyint' THEN 11
                            WHEN 'smallint' THEN 100
                            ELSE 1000
                        END
                    );
        END;

        INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) 
        VALUES( @BID
              , @ClassTypeID
              , IIF( @NextTableID > @StartingID, @NextTableID, @StartingID )
        );

        SELECT @TableName + ' NextID Updated to ' + CONVERT(Varchar(12), IIF( @NextTableID > 1000, @NextTableID, @StartingID ) )
        ;
    END

    ELSE IF (@NextTableID > @NextTrackingID)
    BEGIN
        UPDATE [Util.NextID]
        SET NextID = @NextTableID
        WHERE BID = @BID AND ClassTypeID = @ClassTypeID
        ;
        SELECT @TableName + ' NextID Updated to ' + CONVERT(Varchar(12), @NextTableID )
        ;
    END
    ELSE
        SELECT @TableName + ' Ok ';
END;");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [Option.SaveValue]
-- DECLARE 
            @OptionID       INT          = NULL
          , @OptionName     VARCHAR(255) = NULL
          , @Value          VARCHAR(MAX) = NULL     -- Pass in NULL to delete the current value (if any)

          , @AssociationID  TINYINT      = NULL
          , @BID            SMALLINT     = NULL
          , @LocationID     SMALLINT     = NULL
          , @EmployeeID     SMALLINT     = NULL
          , @CompanyID      INT          = NULL
          , @ContactID      INT          = NULL
          , @StorefrontID   SMALLINT	 = NULL

AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@OptionName IS NULL) AND (@OptionID IS NULL)) OR ((@OptionName IS NOT NULL) AND (@OptionID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @OptionID or the @OptionName.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================
    IF (@OptionID IS NULL)
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName;

        IF (@OptionID IS NULL)
        BEGIN
            -- If not defined, and NULL, there will be nothing to delete
            -- -------------------------------------------------
            IF (@Value IS NULL)
                RETURN;

            PRINT 'create new - check parameters, save and return id'

			DECLARE @NewID INT
			EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, 1;
			SET @OptionID = @NewID

            INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
            SELECT @OptionID AS ID
                 , @OptionName AS Name
                 , 'Custom: '+@OptionName AS Label
                 , NULL AS DefaultValue
                 , 0 AS DataType -- Always string
                 , -1 AS CategoryID  -- AdHoc Options
                 , 1 AS IsHidden
            ;

            PRINT @OptionID
        END
    END

    -- ======================================
    -- Lookup Current Value if it Exists
    -- ======================================
    DECLARE @InstanceID INT = 

        CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND LocationID = @LocationID)

                WHEN @EmployeeID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND EmployeeID = @EmployeeID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID )

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND AssociationID = @AssociationID )

                ELSE NULL
        END;

    -- ======================================
    -- Now Save (Update or Create) Value
    -- ======================================
    IF (@Value IS NOT NULL)
    BEGIN
        IF (@InstanceID IS NOT NULL)
            UPDATE [Option.Data]
            SET ModifiedDT = GetUTCDate()
            , Value = @Value
            WHERE ID = @InstanceID

        ELSE
            INSERT INTO [Option.Data] 
                ( CreatedDate, ModifiedDT, IsActive, OptionID
                , [Value], AssociationID, BID, LocationID
                , StoreFrontID, EmployeeID, CompanyID, ContactID
                )
            VALUES
                ( GetUTCDate(), GetUTCDate(), 1, @OptionID
                , @Value, @AssociationID, @BID, @LocationID
                , @StoreFrontID, @EmployeeID, @CompanyID, @ContactID
                );
    END

    -- ======================================
    -- Else Delete the Options with NULL Values Passed in
    -- ======================================
    ELSE
        DELETE FROM [Option.Data]
        WHERE ID = @InstanceID
    


END");
            migrationBuilder.Sql(@"
CREATE PROCEDURE [dbo].[Option.SaveValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT       = NULL
    , @BID            SMALLINT      = NULL
    , @LocationID     SMALLINT      = NULL
    , @EmployeeID     SMALLINT      = NULL
    , @CompanyID      INT           = NULL
    , @ContactID      INT           = NULL
    , @StorefrontID   SMALLINT      = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
              RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing  IDs from the Definitions Table
    -- -------------------------------------------------
    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

    -- If not defined, and NULL, there will be nothing to delete
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
        AND (Value IS NULL)
    ;

    -- Any missing IDs must be AdHoc so create them
    -- -------------------------------------------------
    DECLARE @NewIDs INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);

    IF (@NewIDs > 0)
    BEGIN
		DECLARE @NewID INT
		DECLARE @NewIDCount INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);
		EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, @NewIDCount;


        -- Any missing IDs must be AdHoc so create them
        -- -------------------------------------------------
        UPDATE @Options
        SET   IsNewAdHoc = 1
            , OptionID = @NewID, @NewID = @NewID + 1
        WHERE OptionID IS NULL
        ;

        INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
        SELECT OptionID AS ID
                , OptionName AS Name
                , 'Custom: '+OptionName AS Label
                , NULL AS DefaultValue
                , 0 AS DataType -- Always string
                , -1 AS CategoryID  -- AdHoc Options
                , 1 AS IsHidden
        FROM @Options
        WHERE IsNewAdHoc = 1
        ;
    END;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
    UPDATE Opt
    SET InstanceID = 
           CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.LocationID = @LocationID)

                WHEN @EmployeeID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.EmployeeID = @EmployeeID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID)

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.AssociationID = @AssociationID)

           ELSE NULL
           END
    FROM @Options Opt
    ;

    IF (@Debug=1)
        SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NULL
    ;

    -- ======================================
    -- Update any Existing Values
    -- ======================================
    UPDATE D
    SET ModifiedDT = GetUTCDate()
      , Value = Opt.Value
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NOT NULL
    ;

    -- ======================================
    -- Create New Options if not Found Values
    -- ======================================
    INSERT INTO [Option.Data] 
        ( CreatedDate, ModifiedDT, IsActive, OptionID
        , [Value], AssociationID, BID, LocationID
        , StoreFrontID, EmployeeID, CompanyID, ContactID
        )
        SELECT
            GetUTCDate(), GetUTCDate(), 1, OptionID
            , Value, @AssociationID, @BID, @LocationID
            , @StoreFrontID, @EmployeeID, @CompanyID, @ContactID
        FROM @Options
        WHERE InstanceID IS NULL
          AND Value IS NOT NULL
    ;
END");
            

                    migrationBuilder.Sql(@"
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(0, N'Unknown', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(1, N'Address', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(2, N'Phone', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(3, N'Email', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(4, N'Web Address', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(5, N'Twitter', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(6, N'Facebook', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(7, N'Linked In', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(8, N'FTP', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(9, N'IP', NULL, NULL, NULL)
INSERT [enum.Locator.Type] ([ID], [Name], [ValidityRegEx], [PossibilityRegEx], [PossibilitySortOrder]) VALUES(255, N'Other', NULL, NULL, NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(0, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(1, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(1, 1, N'Work', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(1, 2, N'Billing', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(1, 3, N'Shipping', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(1, 4, N'Home', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(1, 255, N'Custom', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(2, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(2, 1, N'Work (Main)', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(2, 2, N'Work (Direct)', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(2, 3, N'Personal', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(2, 4, N'Home', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(2, 5, N'Assistant''s', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(2, 9, N'Fax', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(2, 255, N'Custom Phone', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(3, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(3, 1, N'Work Email', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(3, 2, N'Personal Email', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(3, 5, N'Assistant''s Email', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(3, 255, N'Custom Email', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(4, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(4, 1, N'Business Web Page', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(4, 2, N'Personal Web Page', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(4, 255, N'Custom Web Page', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(5, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(5, 1, N'Twitter Business', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(5, 2, N'Twitter Personal', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(6, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(6, 1, N'FB Business Page', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(6, 2, N'FB Personal Page', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(7, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(7, 1, N'Linked In', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(8, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(8, 1, N'Work FTP', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(8, 255, N'Custom FTP', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(9, 0, N'Unknown', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(9, 255, N'Custom', NULL)
INSERT [enum.Locator.SubType] ([LocatorType], [ID], [Name], [LinkFormatString]) VALUES(255, 0, N'Unknown', NULL)
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms]) VALUES(1, N'Accounting', NULL, 0, 0, NULL)
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms]) VALUES(2, N'Tax Accounting', 1, 0, 1, NULL)
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms]) VALUES(3, N'Income Accounting', 1, 0, 1, NULL)
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms]) VALUES(4, N'Expense Accounting', 1, 0, 1, NULL)
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms]) VALUES(5, N'Domain Settings', NULL, 0, 0, NULL)
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms]) VALUES(6, N'Office & Facilities', NULL, 0, 0, NULL)
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms]) VALUES(9, N'Invoice', NULL, 0, 0, NULL)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms]) VALUES(1, N'COGS Accounts', 4, N'Morbi arcu odio, condimentum vitae sapien scelerisque...', 8, 0, NULL)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms]) VALUES(2, N'Income Accounts', 3, N'Morbi arcu odio, condimentum vitae sapien scelerisque...', 8, 0, NULL)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms]) VALUES(3, N'Domain', 5, N'Morbi arcu odio, condimentum vitae sapien scelerisque...', 8, 0, NULL)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms]) VALUES(4, N'Email', 5, N'Morbi arcu odio, condimentum vitae sapien scelerisque...', 8, 0, NULL)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms]) VALUES(5, N'Hours of Operation', 6, N'Change this location''s hours of operation', 8, 0, NULL)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms]) VALUES(6, N'Invoice Numbering', 9, N'Invoice numbering options', 4, 0, NULL)
INSERT [enum.CustomField.DataType] ([ID], [Name]) VALUES(0, N'Text')
INSERT [enum.CustomField.DataType] ([ID], [Name]) VALUES(1, N'Integer')
INSERT [enum.CustomField.DataType] ([ID], [Name]) VALUES(2, N'Numeric')
INSERT [enum.CustomField.DataType] ([ID], [Name]) VALUES(3, N'Boolean')
INSERT [enum.CustomField.DataType] ([ID], [Name]) VALUES(4, N'List')
INSERT [enum.CustomField.DataType] ([ID], [Name]) VALUES(5, N'Set')
INSERT [enum.CustomField.DataType] ([ID], [Name]) VALUES(6, N'Image')
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(1, N'GLAccount.Discount', N'Discount Account', N'Select the GL', 0, 1, NULL, N'1080', 0)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(2, N'GLAccount.TaxName1', N'Sales Tax #1 Name', N'Enter the name used for the highest category of sales tax', 0, 1, NULL, N'GST', 0)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(3, N'InvoicePrefix', N'Invoice Prefix', NULL, 0, 6, NULL, NULL, 0)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(4, N'InvoiceNextNumber', N'Next Invoice Number', NULL, 1, 6, NULL, NULL, 0)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(5, N'InvoiceVersionDisplayFormat', N'Version Numbering', NULL, 1, 6, NULL, N'0', 0)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(10, N'Hours.Sales.Day0.Open', N'Hours Sales Day 0 Open', NULL, 0, 5, NULL, NULL, 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(11, N'Hours.Sales.Day1.Open', N'Hours Sales Day 1 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(12, N'Hours.Sales.Day2.Open', N'Hours Sales Day 2 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(13, N'Hours.Sales.Day3.Open', N'Hours Sales Day 3 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(14, N'Hours.Sales.Day4.Open', N'Hours Sales Day 4 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(15, N'Hours.Sales.Day5.Open', N'Hours Sales Day 5 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(16, N'Hours.Sales.Day6.Open', N'Hours Sales Day 6 Open', NULL, 0, 5, NULL, NULL, 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(17, N'Hours.Sales.Day0.Close', N'Hours Sales Day 0 Close', NULL, 0, 5, NULL, NULL, 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(18, N'Hours.Sales.Day1.Close', N'Hours Sales Day 1 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(19, N'Hours.Sales.Day2.Close', N'Hours Sales Day 2 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(20, N'Hours.Sales.Day3.Close', N'Hours Sales Day 3 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(21, N'Hours.Sales.Day4.Close', N'Hours Sales Day 4 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(22, N'Hours.Sales.Day5.Close', N'Hours Sales Day 5 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(23, N'Hours.Sales.Day6.Close', N'Hours Sales Day 6 Close', NULL, 0, 5, NULL, NULL, 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(24, N'Hours.Production.Day0.Open', N'Hours Production Day 0 Open', NULL, 0, 5, NULL, NULL, 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(25, N'Hours.Production.Day1.Open', N'Hours Production Day 1 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(26, N'Hours.Production.Day2.Open', N'Hours Production Day 2 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(27, N'Hours.Production.Day3.Open', N'Hours Production Day 3 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(28, N'Hours.Production.Day4.Open', N'Hours Production Day 4 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(29, N'Hours.Production.Day5.Open', N'Hours Production Day 5 Open', NULL, 0, 5, NULL, N'08:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(30, N'Hours.Production.Day6.Open', N'Hours Production Day 6 Open', NULL, 0, 5, NULL, NULL, 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(31, N'Hours.Production.Day0.Close', N'Hours Production Day 0 Close', NULL, 0, 5, NULL, NULL, 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(32, N'Hours.Production.Day1.Close', N'Hours Production Day 1 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(33, N'Hours.Production.Day2.Close', N'Hours Production Day 2 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(34, N'Hours.Production.Day3.Close', N'Hours Production Day 3 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(35, N'Hours.Production.Day4.Close', N'Hours Production Day 4 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(36, N'Hours.Production.Day5.Close', N'Hours Production Day 5 Close', NULL, 0, 5, NULL, N'17:00', 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(37, N'Hours.Production.Day6.Close', N'Hours Production Day 6 Close', NULL, 0, 5, NULL, NULL, 1)
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES(38, N'Hours.UseDefault', N'Use Default Hours', NULL, 0, 5, NULL, N'false', 0)
SET IDENTITY_INSERT [Option.Data] ON

INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES(1, CAST(N'2017-08-20' AS Date), CAST(N'2017-08-20T15:18:37.4200000' AS DateTime2), 1, 2, N'HST', NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES(2, CAST(N'2017-08-20' AS Date), CAST(N'2017-08-20T15:21:15.8900000' AS DateTime2), 1, 2, N'State', 4, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES(3, CAST(N'2018-01-03' AS Date), CAST(N'2018-01-05T14:47:01.8400000' AS DateTime2), 1, 4, N'10008', NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES(4, CAST(N'2018-01-03' AS Date), CAST(N'2018-01-05T14:47:10.4200000' AS DateTime2), 1, 3, N'INVe', NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES(5, CAST(N'2018-01-03' AS Date), CAST(N'2018-01-05T14:45:30.8400000' AS DateTime2), 1, 5, N'0', NULL, 1, NULL, NULL, NULL, NULL, NULL)
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES(6, CAST(N'2018-01-03' AS Date), CAST(N'2018-01-03T22:25:29.8200000' AS DateTime2), 1, 4, N'10002', NULL, 2, NULL, NULL, NULL, NULL, NULL)
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES(7, CAST(N'2018-01-03' AS Date), CAST(N'2018-01-03T22:25:29.8200000' AS DateTime2), 1, 3, N'INVb', NULL, 2, NULL, NULL, NULL, NULL, NULL)
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES(8, CAST(N'2018-01-03' AS Date), CAST(N'2018-01-03T22:25:29.8200000' AS DateTime2), 1, 5, N'1', NULL, 2, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [Option.Data] OFF
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(1, N'Unformatted Single Line', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(2, N'Unformatted Memo', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(3, N'Formatted Single Line', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(4, N'Formatted Memo', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(5, N'Phone (US&CA)', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(6, N'Email', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(7, N'URL', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(11, N'All Integers', 1)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(12, N'Positive Integers', 1)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(19, N'Custom Integer Range', 1)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(21, N'All Numbers', 2)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(22, N'Positive Numbers', 2)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(23, N'Numbers to 1 Decimal', 2)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(24, N'Numbers to 2 Decimals', 2)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(25, N'Numbers to 4 Decimals', 2)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(29, N'Custom Number Range', 2)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(31, N'Checkbox', 3)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(32, N'Yes-No', 3)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(39, N'Custom Boolean Option', 3)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(41, N'List of TimeZones', 4)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(42, N'Lists of States', 4)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(43, N'Lists of Employees', 4)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(44, N'Lists of Salespeople', 4)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(49, N'Custom List', 4)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(59, N'Custom Set', 5)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(101, N'Phone (UK)', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(102, N'Phone (AU Landline)', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(103, N'Phone (AU Mobile)', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(111, N'Facebook', 0)
INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType]) VALUES(112, N'Twitter', 0)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(1, N'Meeting', 1)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(2, N'Call', 1)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(3, N'Appointment', 1)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(11, N'Task', 2)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(12, N'Note', 2)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(13, N'Email', 2)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(14, N'SMS Message', 2)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(51, N'Pre-Sale Order Activity', 3)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(52, N'Sale', 3)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(53, N'Post-Sale Change', 3)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(54, N'Pre-Sale Void', 3)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(55, N'Post-Sale Void', 3)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(61, N'Payment on Order', 3)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(62, N'Payment to Credit', 3)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(63, N'Payment Void', 3)
INSERT [enum.ActivityType] ([ID], [Name], [RecordType]) VALUES(64, N'Credit Adjustment', 3)
INSERT [enum.Campaign.Type] ([ID], [Name]) VALUES(0, N'Unknown')
INSERT [enum.Campaign.Type] ([ID], [Name]) VALUES(1, N'Email')
INSERT [enum.Campaign.Type] ([ID], [Name]) VALUES(2, N'SMS')
INSERT [enum.Campaign.Type] ([ID], [Name]) VALUES(3, N'Call')
INSERT [enum.Campaign.Type] ([ID], [Name]) VALUES(4, N'Mail')
INSERT [enum.Campaign.Type] ([ID], [Name]) VALUES(5, N'Visit')
INSERT [enum.Campaign.Type] ([ID], [Name]) VALUES(9, N'Other')
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1000, N'Business', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1001, N'BusinessCustom', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1003, N'BusinessLocator', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1005, N'Location', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1006, N'LocationCustom', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1008, N'LocationLocator', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1100, N'Security Right', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1101, N'Secuirty Group', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1500, N'Document', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(1600, N'Association', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(2000, N'Company', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(2001, N'CompanyCustom', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(2003, N'CompanyLocator', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(2011, N'Industry', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(2012, N'Source', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(2013, N'TimeZone', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(2101, N'CustomFieldDef', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(3000, N'Contact', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(3001, N'ContactCustom', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(3003, N'ContactLocator', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(4000, N'Lead', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(4001, N'LeadCustom', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(4003, N'LeadLocator', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(4101, N'LeadSource', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(5000, N'Employee', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(5001, N'EmployeeCustom', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(5003, N'EmployeeLocator', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(5020, N'EmployeeTeam', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(5101, N'EmployeeRole', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(9000, N'Opportunity', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(9001, N'OpportunityCustom', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(9100, N'Campaign', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(9101, N'CampaignCustom', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(10000, N'Order', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(10002, N'Estimate', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(10100, N'OrderLineItem', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(10102, N'EstimateLineItem', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(10300, N'EstimateVariation', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(10800, N'Job', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(11001, N'Activity', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(11002, N'Event', NULL)
INSERT [enum.ClassType] ([ID], [Name], [Description]) VALUES(11003, N'GLActivity', NULL)
INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(0, N'Member')
INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(1, N'Salesperson')
INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(2, N'CSR')
INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(3, N'Designer')
INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(4, N'Project Manager')
INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(11, N'Salesperson 2')
INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(12, N'CSR 2')
INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(13, N'Designer 2')
INSERT [enum.Lead.Stage] ([ID], [Name]) VALUES(0, N'Unknown')
INSERT [enum.Lead.Stage] ([ID], [Name]) VALUES(1, N'Inactive')
INSERT [enum.Lead.Stage] ([ID], [Name]) VALUES(2, N'Converted')
INSERT [enum.Lead.Stage] ([ID], [Name]) VALUES(6, N'Dead end')
INSERT [enum.Lead.Stage] ([ID], [Name]) VALUES(10, N'New')
INSERT [enum.Lead.Stage] ([ID], [Name]) VALUES(11, N'Cold')
INSERT [enum.Lead.Stage] ([ID], [Name]) VALUES(12, N'Warm')
INSERT [enum.Lead.Stage] ([ID], [Name]) VALUES(13, N'Hot')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(0, N'Unknown')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(1, N'Qualification')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(2, N'Needs Analysis')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(3, N'Value Proposition')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(4, N'Proposal')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(5, N'Negotiation')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(6, N'Ready to Close')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(11, N'Won')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(12, N'Lost')
INSERT [enum.Opportunity.Stage] ([ID], [Name]) VALUES(13, N'Abandonded')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (0, N'Default')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (1, N'System')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (2, N'Association')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (4, N'Business')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (8, N'Location')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (16, N'Storefront')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (32, N'Employee')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (64, N'Company')
INSERT [enum.Option.Level] ([ID], [Name]) VALUES (128, N'Contact')
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(0, N'Dateline Standard Time', N'(GMT-12:00) International Date Line West', CAST(-12.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(1, N'Samoa Standard Time', N'(GMT-11:00) Midway Island, Samoa', CAST(-11.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(2, N'Hawaiian Standard Time', N'(GMT-10:00) Hawaii', CAST(-10.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(3, N'Alaskan Standard Time', N'(GMT-09:00) Alaska', CAST(-9.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(4, N'Pacific Standard Time', N'(GMT-08:00) Pacific Time (US and Canada); Tijuana', CAST(-8.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(10, N'Mountain Standard Time', N'(GMT-07:00) Mountain Time (US and Canada)', CAST(-7.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(13, N'Mexico Standard Time 2', N'(GMT-07:00) Chihuahua, La Paz, Mazatlan', CAST(-7.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(15, N'U.S. Mountain Standard Time', N'(GMT-07:00) Arizona', CAST(-7.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(20, N'Central Standard Time', N'(GMT-06:00) Central Time (US and Canada)', CAST(-6.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(25, N'Canada Central Standard Time', N'(GMT-06:00) Saskatchewan', CAST(-6.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(30, N'Mexico Standard Time', N'(GMT-06:00) Guadalajara, Mexico City, Monterrey', CAST(-6.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(33, N'Central America Standard Time', N'(GMT-06:00) Central America', CAST(-6.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(35, N'Eastern Standard Time', N'(GMT-05:00) Eastern Time (US and Canada)', CAST(-5.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(40, N'U.S. Eastern Standard Time', N'(GMT-05:00) Indiana (East)', CAST(-5.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(45, N'S.A. Pacific Standard Time', N'(GMT-05:00) Bogota, Lima, Quito', CAST(-5.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(50, N'Atlantic Standard Time', N'(GMT-04:00) Atlantic Time (Canada)', CAST(-4.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(55, N'S.A. Western Standard Time', N'(GMT-04:00) Georgetown, La Paz, San Juan', CAST(-4.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(56, N'Pacific S.A. Standard Time', N'(GMT-04:00) Santiago', CAST(-4.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(60, N'Newfoundland and Labrador Standard Time', N'(GMT-03:30) Newfoundland', CAST(-3.50 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(65, N'E. South America Standard Time', N'(GMT-03:00) Brasilia', CAST(-3.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(70, N'S.A. Eastern Standard Time', N'(GMT-03:00) Georgetown', CAST(-3.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(73, N'Greenland Standard Time', N'(GMT-03:00) Greenland', CAST(-3.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(75, N'Mid-Atlantic Standard Time', N'(GMT-02:00) Mid-Atlantic', CAST(-2.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(80, N'Azores Standard Time', N'(GMT-01:00) Azores', CAST(-1.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(83, N'Cape Verde Standard Time', N'(GMT-01:00) Cape Verde Islands', CAST(-1.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(85, N'GMT Standard Time', N'(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lisbon, London', CAST(0.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(90, N'Greenwich Standard Time', N'(GMT) Monrovia, Reykjavik', CAST(0.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(95, N'Central Europe Standard Time', N'(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', CAST(1.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(100, N'Central European Standard Time', N'(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', CAST(1.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(105, N'Romance Standard Time', N'(GMT+01:00) Brussels, Copenhagen, Madrid, Paris', CAST(1.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(110, N'W. Europe Standard Time', N'(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', CAST(1.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(113, N'W. Central Africa Standard Time', N'(GMT+01:00) West Central Africa', CAST(1.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(115, N'E. Europe Standard Time', N'(GMT+02:00) Minsk', CAST(2.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(120, N'Egypt Standard Time', N'(GMT+02:00) Cairo', CAST(2.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(125, N'FLE Standard Time', N'(GMT+02:00) Helsinki, Kiev, Riga, Sofia, Tallinn, Vilnius', CAST(2.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(130, N'GTB Standard Time', N'(GMT+02:00) Athens, Bucharest, Istanbul', CAST(2.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(135, N'Israel Standard Time', N'(GMT+02:00) Jerusalem', CAST(2.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(140, N'South Africa Standard Time', N'(GMT+02:00) Harare, Pretoria', CAST(2.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(145, N'Russian Standard Time', N'(GMT+03:00) Moscow, St. Petersburg, Volgograd', CAST(3.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(150, N'Arab Standard Time', N'(GMT+03:00) Kuwait, Riyadh', CAST(3.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(155, N'E. Africa Standard Time', N'(GMT+03:00) Nairobi', CAST(3.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(158, N'Arabic Standard Time', N'(GMT+03:00) Baghdad', CAST(3.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(160, N'Iran Standard Time', N'(GMT+03:30) Tehran', CAST(3.50 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(165, N'Arabian Standard Time', N'(GMT+04:00) Abu Dhabi, Muscat', CAST(4.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(170, N'Caucasus Standard Time', N'(GMT+04:00) Baku, Tbilisi, Yerevan', CAST(4.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(175, N'Transitional Islamic State of Afghanistan Standard Time', N'(GMT+04:30) Kabul', CAST(4.50 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(180, N'Ekaterinburg Standard Time', N'(GMT+05:00) Ekaterinburg', CAST(5.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(185, N'West Asia Standard Time', N'(GMT+05:00) Tashkent', CAST(5.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(190, N'India Standard Time', N'(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi', CAST(5.50 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(193, N'Nepal Standard Time', N'(GMT+05:45) Kathmandu', CAST(5.75 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(195, N'Central Asia Standard Time', N'(GMT+06:00) Astana, Dhaka', CAST(6.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(200, N'Sri Lanka Standard Time', N'(GMT+06:00) Sri Jayawardenepura', CAST(6.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(201, N'N. Central Asia Standard Time', N'(GMT+06:00) Almaty, Novosibirsk', CAST(6.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(203, N'Myanmar Standard Time', N'(GMT+06:30) Yangon (Rangoon)', CAST(6.50 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(205, N'S.E. Asia Standard Time', N'(GMT+07:00) Bangkok, Hanoi, Jakarta', CAST(7.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(207, N'North Asia Standard Time', N'(GMT+07:00) Krasnoyarsk', CAST(7.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(210, N'China Standard Time', N'(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', CAST(8.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(215, N'Singapore Standard Time', N'(GMT+08:00) Kuala Lumpur, Singapore', CAST(8.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(220, N'Taipei Standard Time', N'(GMT+08:00) Taipei', CAST(8.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(225, N'W. Australia Standard Time', N'(GMT+08:00) Perth', CAST(8.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(227, N'North Asia East Standard Time', N'(GMT+08:00) Irkutsk, Ulaanbaatar', CAST(8.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(230, N'Korea Standard Time', N'(GMT+09:00) Seoul', CAST(9.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(235, N'Tokyo Standard Time', N'(GMT+09:00) Osaka, Sapporo, Tokyo', CAST(9.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(240, N'Yakutsk Standard Time', N'(GMT+09:00) Yakutsk', CAST(9.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(245, N'A.U.S. Central Standard Time', N'(GMT+09:30) Darwin', CAST(9.50 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(250, N'Cen. Australia Standard Time', N'(GMT+09:30) Adelaide', CAST(9.50 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(255, N'A.U.S. Eastern Standard Time', N'(GMT+10:00) Canberra, Melbourne, Sydney', CAST(10.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(260, N'E. Australia Standard Time', N'(GMT+10:00) Brisbane', CAST(10.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(265, N'Tasmania Standard Time', N'(GMT+10:00) Hobart', CAST(10.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(270, N'Vladivostok Standard Time', N'(GMT+10:00) Vladivostok', CAST(10.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(275, N'West Pacific Standard Time', N'(GMT+10:00) Guam, Port Moresby', CAST(10.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(280, N'Central Pacific Standard Time', N'(GMT+11:00) Magadan, Solomon Islands, New Caledonia', CAST(11.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(285, N'Fiji Islands Standard Time', N'(GMT+12:00) Fiji, Kamchatka, Marshall Is.', CAST(12.00 AS Decimal(6, 2)), 0)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(290, N'New Zealand Standard Time', N'(GMT+12:00) Auckland, Wellington', CAST(12.00 AS Decimal(6, 2)), 1)
INSERT [enum.TimeZone] ([ID], [Name], [StandardName], [UTCOffset], [IsCommon]) VALUES(300, N'Tonga Standard Time', N'(GMT+13:00) Nuku''alofa', CAST(13.00 AS Decimal(6, 2)), 0)
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(0, N'None')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(1, N'Employee')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(9, N'Employee Administrator')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(11, N'Contact')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(19, N'Contact Administrator')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(21, N'Franchise Staff')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(29, N'Franchise Admin')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(41, N'Support Staff')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(49, N'Support Manager')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(51, N'Developer Staff')
INSERT [enum.User.Role.Type] ([ID], [Name]) VALUES(59, N'Developer Staff')
INSERT [List.Filter] ([BID], [ID], [CreatedDate], [ModifiedDT], [IsActive], [Name], [TargetClassTypeID], [IDs], [Criteria], [OwnerID], [IsPublic], [IsSystem], [Hint], [IsDefault], [SortIndex]) VALUES(1, 1000, CAST(N'2017-10-18' AS Date), CAST(N'2017-10-18T18:55:40.4500000' AS DateTime2), 1, N'All', 1005, NULL, NULL, NULL, 1, 1, NULL, 1, 50)
INSERT [List.Filter] ([BID], [ID], [CreatedDate], [ModifiedDT], [IsActive], [Name], [TargetClassTypeID], [IDs], [Criteria], [OwnerID], [IsPublic], [IsSystem], [Hint], [IsDefault], [SortIndex]) VALUES(1, 1001, CAST(N'2017-10-18' AS Date), CAST(N'2017-10-18T18:55:40.4500000' AS DateTime2), 1, N'Active', 1005, NULL, N'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>', NULL, 1, 1, NULL, 1, 50)
INSERT [List.Filter] ([BID], [ID], [CreatedDate], [ModifiedDT], [IsActive], [Name], [TargetClassTypeID], [IDs], [Criteria], [OwnerID], [IsPublic], [IsSystem], [Hint], [IsDefault], [SortIndex]) VALUES(1, 1006, CAST(N'2017-10-18' AS Date), CAST(N'2017-10-18T18:55:44.5500000' AS DateTime2), 1, N'All', 5000, NULL, NULL, NULL, 1, 1, NULL, 1, 50)
INSERT [List.Filter] ([BID], [ID], [CreatedDate], [ModifiedDT], [IsActive], [Name], [TargetClassTypeID], [IDs], [Criteria], [OwnerID], [IsPublic], [IsSystem], [Hint], [IsDefault], [SortIndex]) VALUES(1, 1007, CAST(N'2017-10-18' AS Date), CAST(N'2017-10-18T18:55:44.5500000' AS DateTime2), 1, N'Active', 5000, NULL, N'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>', NULL, 1, 1, NULL, 1, 50)
SET IDENTITY_INSERT [System.List.Filter.Criteria] ON

INSERT [System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES(1, 5000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 0, 0, 0, NULL, NULL, 0, NULL)
INSERT [System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES(2, 5000, N'Name', N'Name', N'FullName', 0, 0, 0, 0, NULL, NULL, 0, 0)
INSERT [System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES(3, 5000, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, NULL)
INSERT [System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES(4, 5000, N'Email', N'Email', N'EmailAddress', 0, 0, 0, 0, NULL, NULL, 0, NULL)
INSERT [System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES(5, 5000, N'Location', N'Location', N'LocationID', 0, 1, 4, 0, NULL, NULL, 0, NULL)
INSERT [System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES(6, 1005, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
INSERT [System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES(7, 1005, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, NULL)
SET IDENTITY_INSERT [System.List.Filter.Criteria] OFF");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.List.Filter.Criteria]
GO
DELETE FROM [System.List.Column]
GO
DELETE FROM [List.Filter]
GO
DELETE FROM [enum.User.Role.Type]
GO
DELETE FROM [enum.TimeZone]
GO
DELETE FROM [enum.Option.Level]
GO
DELETE FROM [enum.Opportunity.Stage]
GO
DELETE FROM [enum.Lead.Stage]
GO
DELETE FROM [enum.Employee.Role]
GO
DELETE FROM [enum.CRM.Company.Status]
GO
DELETE FROM [enum.ClassType]
GO
DELETE FROM [enum.Campaign.Type]
GO
DELETE FROM [enum.Campaign.Stage]
GO
DELETE FROM [enum.ActivityType]
GO
DELETE FROM [CRM.Source]
GO
DELETE FROM [CRM.Industry]
GO
DELETE FROM [CRM.CustomField.HelperLink]
GO
DELETE FROM [CRM.CustomField.Helper]
GO
DELETE FROM [enum.CustomField.HelperType]
GO
DELETE FROM [CRM.CustomField.Def]
GO
DELETE FROM [enum.CustomField.InputType]
GO
DELETE FROM [Option.Data]
GO
DELETE FROM [System.Option.Definition]
GO
DELETE FROM [enum.CustomField.DataType]
GO
DELETE FROM [System.Option.Category]
GO
DELETE FROM [System.Option.Section]
GO
DELETE FROM [enum.Locator.SubType]
GO
DELETE FROM [enum.Locator.Type]
GO");
migrationBuilder.Sql(@"DROP FUNCTION [dbo].[User.Security.Rights.String]");
migrationBuilder.Sql(@"DROP FUNCTION [dbo].[List.Filter.MyLists]");
migrationBuilder.Sql(@"DROP FUNCTION [dbo].[List.Filter.SimpleList]");
migrationBuilder.Sql(@"DROP FUNCTION [dbo].[Option.Categories.ByLevel]");
migrationBuilder.Sql(@"DROP FUNCTION [dbo].[User.Security.Rights.List]");
migrationBuilder.Sql(@"DROP VIEW [dbo].[Business.Location.SimpleList]");
migrationBuilder.Sql(@"DROP VIEW [dbo].[Business.SimpleList]");
migrationBuilder.Sql(@"DROP VIEW [dbo].[Company.SimpleList]");
migrationBuilder.Sql(@"DROP VIEW [dbo].[Contact.SimpleList]");
migrationBuilder.Sql(@"DROP VIEW [dbo].[Employee.SimpleList]");
migrationBuilder.Sql(@"DROP VIEW [dbo].[Employee.Team.SimpleList]");
migrationBuilder.Sql(@"DROP VIEW [dbo].[enum.TimeZone.SimpleList]");
migrationBuilder.Sql(@"DROP VIEW [dbo].[Location.SimpleList]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Location.Action.SetActive]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Location.Action.SetActiveMultiple]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Location.Action.SetDefault]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.DeleteCategoryOptions]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.DeleteValue]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.DeleteValues]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.GetCategoryOptions]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.GetSection]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.GetValue]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.GetValues]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.SaveValue]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Option.SaveValues]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Util.ID.GetID]");
migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Util.ID.ValidateTableID]");
            
            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Employee.Data1",
                table: "Business.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Business.Data_Employee.Data",
                table: "Business.Data");

            migrationBuilder.DropTable(
                name: "_Root.Custom");

            migrationBuilder.DropTable(
                name: "_Root.Data");

            migrationBuilder.DropTable(
                name: "Accounting.Payment.Term.LocationLink");

            migrationBuilder.DropTable(
                name: "Accounting.Tax.Group.AssessmentLink");

            migrationBuilder.DropTable(
                name: "Accounting.Tax.Group.LocationLink");

            migrationBuilder.DropTable(
                name: "Activity.Action");

            migrationBuilder.DropTable(
                name: "Activity.Event");

            migrationBuilder.DropTable(
                name: "Activity.GLActivity");

            migrationBuilder.DropTable(
                name: "Association.Data");

            migrationBuilder.DropTable(
                name: "Business.Custom");

            migrationBuilder.DropTable(
                name: "Business.Location.SimpleList");

            migrationBuilder.DropTable(
                name: "Business.Locator");

            migrationBuilder.DropTable(
                name: "Company.Locator");

            migrationBuilder.DropTable(
                name: "Company.SimpleList");

            migrationBuilder.DropTable(
                name: "Contact.Locator");

            migrationBuilder.DropTable(
                name: "Contact.SimpleList");

            migrationBuilder.DropTable(
                name: "CRM.CustomField.HelperLink");

            migrationBuilder.DropTable(
                name: "Employee.Custom");

            migrationBuilder.DropTable(
                name: "Employee.Locator");

            migrationBuilder.DropTable(
                name: "Employee.SimpleList");

            migrationBuilder.DropTable(
                name: "Employee.Team.LocationLink");

            migrationBuilder.DropTable(
                name: "Employee.Team.SimpleList");

            migrationBuilder.DropTable(
                name: "Employee.TeamLink");

            migrationBuilder.DropTable(
                name: "enum.ActivityType");

            migrationBuilder.DropTable(
                name: "enum.Campaign.Stage");

            migrationBuilder.DropTable(
                name: "enum.ClassType");

            migrationBuilder.DropTable(
                name: "enum.Employee.Role");

            migrationBuilder.DropTable(
                name: "enum.Lead.Stage");

            migrationBuilder.DropTable(
                name: "enum.Opportunity.Stage");

            migrationBuilder.DropTable(
                name: "enum.TimeZone.SimpleList");

            migrationBuilder.DropTable(
                name: "List.Filter.EmployeeSubscription");

            migrationBuilder.DropTable(
                name: "Location.Locator");

            migrationBuilder.DropTable(
                name: "Opportunity.Data");

            migrationBuilder.DropTable(
                name: "Option.Data");

            migrationBuilder.DropTable(
                name: "Report.Menu");

            migrationBuilder.DropTable(
                name: "Security.Right.Group.Link");

            migrationBuilder.DropTable(
                name: "Security.Right.Link");

            migrationBuilder.DropTable(
                name: "Security.Role.Link");

            migrationBuilder.DropTable(
                name: "SimpleBusinessData");

            migrationBuilder.DropTable(
                name: "System.List.Column");

            migrationBuilder.DropTable(
                name: "System.List.Filter.Criteria");

            migrationBuilder.DropTable(
                name: "User.Link");

            migrationBuilder.DropTable(
                name: "Util.NextID");

            migrationBuilder.DropTable(
                name: "Accounting.Payment.Term");

            migrationBuilder.DropTable(
                name: "Accounting.Tax.Assessment");

            migrationBuilder.DropTable(
                name: "CRM.CustomField.Def");

            migrationBuilder.DropTable(
                name: "CRM.CustomField.Helper");

            migrationBuilder.DropTable(
                name: "List.Filter");

            migrationBuilder.DropTable(
                name: "enum.Locator.SubType");

            migrationBuilder.DropTable(
                name: "Campaign.Data");

            migrationBuilder.DropTable(
                name: "Opportunity.Custom");

            migrationBuilder.DropTable(
                name: "System.Option.Definition");

            migrationBuilder.DropTable(
                name: "enum.User.Role.Type");

            migrationBuilder.DropTable(
                name: "Contact.Data");

            migrationBuilder.DropTable(
                name: "Security.Right.Group");

            migrationBuilder.DropTable(
                name: "enum.CustomField.InputType");

            migrationBuilder.DropTable(
                name: "enum.CustomField.HelperType");

            migrationBuilder.DropTable(
                name: "enum.Locator.Type");

            migrationBuilder.DropTable(
                name: "enum.Campaign.Type");

            migrationBuilder.DropTable(
                name: "Campaign.Custom");

            migrationBuilder.DropTable(
                name: "System.Option.Category");

            migrationBuilder.DropTable(
                name: "Company.Data");

            migrationBuilder.DropTable(
                name: "Contact.Custom");

            migrationBuilder.DropTable(
                name: "enum.CustomField.DataType");

            migrationBuilder.DropTable(
                name: "System.Option.Section");

            migrationBuilder.DropTable(
                name: "enum.CRM.Company.Status");

            migrationBuilder.DropTable(
                name: "Company.Custom");

            migrationBuilder.DropTable(
                name: "CRM.Industry");

            migrationBuilder.DropTable(
                name: "Location.Data");

            migrationBuilder.DropTable(
                name: "CRM.Source");

            migrationBuilder.DropTable(
                name: "Employee.Team");

            migrationBuilder.DropTable(
                name: "enum.TimeZone");

            migrationBuilder.DropTable(
                name: "Accounting.Tax.Group");

            migrationBuilder.DropTable(
                name: "Employee.Data");

            migrationBuilder.DropTable(
                name: "Business.Data");

            migrationBuilder.Sql(@"DROP TYPE [dbo].[OptionsArray]");
        }
    }
}

