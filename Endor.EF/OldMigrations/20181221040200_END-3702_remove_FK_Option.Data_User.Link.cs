using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END3702_remove_FK_OptionData_UserLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /*
            https://corebridge.atlassian.net/browse/END-3702?focusedCommentId=50945&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-50945
            */

            migrationBuilder.Sql(@"
                ALTER TABLE dbo.[Option.Data]
                DROP CONSTRAINT IF EXISTS [FK_Option.Data_User.Link]
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
