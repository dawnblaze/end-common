using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180503134353_CREATE_SPROC_Order.Item.Status.Action.Move")]
    public partial class CREATE_SPROC_OrderItemStatusActionMove : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Renumber StatusIndex
            migrationBuilder.Sql(@"
                DECLARE @id int;
                SET @id = 0;

                WITH OrderItemStatus AS (
                      SELECT t1.*, row_number() OVER (ORDER BY id) AS seqnum
                      FROM [Order.Item.Status]  t1 
	                  WHERE t1.BID = 1
                     )
                UPDATE OrderItemStatus
                    SET StatusIndex = @id + seqnum;
            ");

            migrationBuilder.Sql(@"
                /* 
                    This procedures moves an Item Status in sort before another Item Status.
                    The approach is to take the StatusIndex of the target ID and then increment all other
                    Item Statuses with a StatusIndex >= that.

                    Example Usage:
                        EXEC [Order.Item.Status.Action.Move] @BID=1, @UD=49, @TargetID=19               -- movebefore
		                EXEC [Order.Item.Status.Action.Move] @BID=1, @UD=49, @TargetID=19, @MoveAfter=1 -- moveafter
		                select * from  [Order.Item.Status]

                */
                CREATE PROCEDURE [dbo].[Order.Item.Status.Action.Move]
                -- DECLARE 
                          @BID   TINYINT
                        , @ID    SMALLINT
                        , @TargetID smallint
		                , @MoveAfter BIT = 0

                        , @Result         INT     = NULL  OUTPUT
                AS
                BEGIN
                    DECLARE @TargetStatusIndex TINYINT
                          , @Message VARCHAR(1024)
                          ;

                    -- Ignore if dropping on itself
                    IF (@ID = @TargetID)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Can''t drop an item on itself.'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END
                    ;

	                SELECT @TargetStatusIndex = StatusIndex + (CASE WHEN @MoveAfter=1 THEN 1 ELSE 0 END)
                    FROM [Order.Item.Status] 
                    WHERE BID = @BID
                      AND ID = @TargetID
                    ;

                    -- Make Sure we Have a Result
                    IF (@TargetStatusIndex IS NULL)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Specified TargetID not Found.'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END
                    ;

	                UPDATE [Order.Item.Status] 
                    SET StatusIndex = @TargetStatusIndex 
                    WHERE BID = @BID
                      AND ID = @ID;

	                WITH OrderItemStatus AS (
		                  SELECT t1.*, row_number() OVER (ORDER BY StatusIndex) AS seqnum
		                  FROM [Order.Item.Status]  t1 
                          WHERE @BID = BID AND ID != @ID
                            AND StatusIndex >= @TargetStatusIndex 
		                 )
	                    UPDATE OrderItemStatus
	                    SET StatusIndex = @TargetStatusIndex + seqnum;

                    SET @Result = @@ROWCOUNT;

                    SELECT @Result as Result;
                END;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [dbo].[Order.Item.Status.Action.Move];
            ");
        }
    }
}

