using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181010160051_END-2784_Update_CurrentTime_Widget_Categories")]
    public partial class END2784_Update_CurrentTime_Widget_Categories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.Dashboard.Widget.Definition]
                    SET Categories = 'General'
                    WHERE ID = 2
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.Dashboard.Widget.Definition]
                    SET Categories = NULL
                    WHERE ID = 2
                "
            );
        }
    }
}

