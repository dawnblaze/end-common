using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4046_Refactor_Payment_Tables_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "GLActivityID",
                table: "Accounting.Payment.Application",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "GLActivityID",
                table: "Accounting.Payment.Application",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
