using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class DeleteCustomFieldDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [dbo].[System.Option.Category] where ID = 15026");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
