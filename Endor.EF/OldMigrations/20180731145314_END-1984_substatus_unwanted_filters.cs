using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180731145314_END-1984_substatus_unwanted_filters")]
    public partial class END1984_substatus_unwanted_filters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria]
                WHERE [TargetClassTypeID]=10023 AND [Field]='IsActive';

                UPDATE dbo.[List.Filter]
                SET Criteria='
                <ArrayOfListFilterItem>
                </ArrayOfListFilterItem>
                '
                WHERE [TargetClassTypeID]=10023 AND [Name]='Active';         
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

