using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateMachineDataAndProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstimatingCostPerHourFormula",
                table: "Part.Machine.Profile");

            migrationBuilder.DropColumn(
                name: "EstimatingPricePerHourFormula",
                table: "Part.Machine.Profile");

            migrationBuilder.RenameColumn(
                name: "AssemblyDataJSON",
                table: "[Part.Machine.Profile]",
                schema: "",
                newName: "AssemblyOVDataJSON");

            migrationBuilder.AddColumn<string>(
                name: "EstimatingCostPerHourFormula",
                table: "Part.Machine.Data",
                type: "varchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EstimatingPricePerHourFormula",
                table: "Part.Machine.Data",
                type: "varchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstimatingCostPerHourFormula",
                table: "Part.Machine.Data");

            migrationBuilder.DropColumn(
                name: "EstimatingPricePerHourFormula",
                table: "Part.Machine.Data");

            migrationBuilder.RenameColumn(
                name: "AssemblyOVDataJSON",
                table: "[Part.Machine.Profile]",
                newName: "AssemblyDataJSON");

            migrationBuilder.AddColumn<string>(
                name: "EstimatingCostPerHourFormula",
                table: "Part.Machine.Profile",
                type: "varchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EstimatingPricePerHourFormula",
                table: "Part.Machine.Profile",
                type: "varchar(max)",
                nullable: true);
        }
    }
}
