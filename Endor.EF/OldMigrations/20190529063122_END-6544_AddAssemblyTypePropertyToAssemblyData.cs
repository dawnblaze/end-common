using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6544_AddAssemblyTypePropertyToAssemblyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "AssemblyType",
                table: "Part.Subassembly.Data",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Data_AssemblyType",
                table: "Part.Subassembly.Data",
                column: "AssemblyType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Data_Type",
                table: "Part.Subassembly.Data",
                columns: new[] { "BID", "AssemblyType", "Name", "IsActive" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Data_enum.Part.Subassembly.AssemblyType",
                table: "Part.Subassembly.Data",
                column: "AssemblyType",
                principalTable: "enum.Assembly.AssemblyType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Data_enum.Part.Subassembly.AssemblyType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Data_AssemblyType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Data_Type",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "AssemblyType",
                table: "Part.Subassembly.Data");
        }
    }
}
