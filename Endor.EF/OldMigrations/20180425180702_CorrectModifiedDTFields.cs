using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180425180702_CorrectModifiedDTFields")]
    public partial class CorrectModifiedDTFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Order.Data]
DROP COLUMN ModifiedDT
;
ALTER TABLE [Order.Note]
DROP COLUMN ModifiedDT
;
ALTER TABLE [Order.KeyDate]
DROP COLUMN ModifiedDT
;
ALTER TABLE [Order.Data]
ADD ModifiedDT DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT SysUTCDateTime()
  , ValidToDT DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL DEFAULT CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')
  , PERIOD FOR SYSTEM_TIME (ModifiedDT,ValidToDT)
;
ALTER TABLE [Order.Note]
ADD ModifiedDT DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT SysUTCDateTime()
  , ValidToDT DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL DEFAULT CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')
  , PERIOD FOR SYSTEM_TIME (ModifiedDT,ValidToDT)
;
ALTER TABLE [Order.KeyDate]
ADD ModifiedDT DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT SysUTCDateTime()
  , ValidToDT DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL DEFAULT CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')
  , PERIOD FOR SYSTEM_TIME (ModifiedDT,ValidToDT)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Order.Data]
DROP COLUMN ModifiedDT
;
ALTER TABLE [Order.Note]
DROP COLUMN ModifiedDT
;
ALTER TABLE [Order.KeyDate]
DROP COLUMN ModifiedDT
;
ALTER TABLE [Order.Data]
ADD ModifiedDT AS SYSUTCDATETIME()
;
ALTER TABLE [Order.Note]
ADD ModifiedDT AS SYSUTCDATETIME()
;
ALTER TABLE [Order.KeyDate]
ADD ModifiedDT AS SYSUTCDATETIME()
");
        }
    }
}

