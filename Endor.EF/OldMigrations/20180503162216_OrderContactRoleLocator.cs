using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180503162216_OrderContactRoleLocator")]
    public partial class OrderContactRoleLocator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE TABLE [dbo].[Order.Contact.Locator](
    [BID] [smallint] NOT NULL,
    [ID] [int] NOT NULL,
    [ClassTypeID]  AS ((10016)),
    [ModifiedDT] DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT SysUTCDateTIme(),
    [ValidToDT] DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL DEFAULT CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'),
    PERIOD FOR SYSTEM_TIME(ModifiedDT, ValidToDT),
    [ParentID] [int] NOT NULL,
    [LocatorType] [tinyint] NOT NULL,
    [LocatorSubType] [tinyint] NULL,
    [Locator] [nvarchar](1024) NULL,
    [MetaData] [xml] NULL,
    [RawInput] [nvarchar](1024) NULL,
    [SortIndex] [tinyint] NOT NULL,
    [IsValid] [bit] NOT NULL,
    [IsVerified] [bit] NOT NULL,
    [HasImage] [bit] NOT NULL,
    [IsPrimary]  AS (case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end),
    CONSTRAINT [PK_Order.Contact.Locator] PRIMARY KEY ( [BID] ASC, [ID] ASC )
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Contact.Locator]))
;
");

            migrationBuilder.CreateIndex(
                name: "IX_OrderContactRole.Locator_Parent",
                table: "Order.Contact.Locator",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Locator_LocatorType_LocatorSubType",
                table: "Order.Contact.Locator",
                columns: new[] { "LocatorType", "LocatorSubType" });

            migrationBuilder.CreateIndex(
                name: "IX_OrderContactRole.Locator",
                table: "Order.Contact.Locator",
                columns: new[] { "BID", "LocatorType", "LocatorSubType", "ParentID" });

            migrationBuilder.AddForeignKey(
                table: "Order.Contact.Locator",
                name: "FK_OrderContactRole.Locator_enum.LocatorType",
                column: "LocatorType",
                principalTable: "enum.Locator.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                table: "Order.Contact.Locator",
                name: "FK_OrderContactRole.Locator_OrderContactRole.Data",
                columns: new[] { "BID", "ParentID" },
                principalTable: "Order.Contact.Role",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                table: "Order.Contact.Locator",
                name: "FK_OrderContactRole.Locator_enum.LocatorSubType",
                columns: new[] { "LocatorType", "LocatorSubType" },
                principalTable: "enum.Locator.SubType",
                principalColumns: new[] { "LocatorType", "ID" },
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE[Order.Contact.Locator] SET(SYSTEM_VERSIONING = OFF);
");
            migrationBuilder.DropTable(
                name: "Historic.Order.Contact.Locator");

            migrationBuilder.DropTable(
                name: "Order.Contact.Locator");
        }
    }
}

