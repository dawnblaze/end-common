using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6944_END6991_DashboardWidgetDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Dashboard.Widget.Definition.005
            migrationBuilder.Sql(@"/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.005]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Line Item Status Count
    The Line Item Status Count Widget displays a graph or a chart of the number of lines items currently in each status.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/991395876/Dashboard+Widget+Line+Item+Status+Count
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StatusID VARCHAR(255) = '21, 22';  -- Pre-WIP and WIP from https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/595329164/OrderOrderStatus+Enum 
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.005] 
        @BID = @BID, @LocationID = @LocationID, 
        @StatusID = @StatusID, @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.005]
                  @BID SMALLINT
                , @LocationID TINYINT
                , @StatusID VARCHAR(255)
                , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
    --  DECLARE @BID SMALLINT = 1;
    --  DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --  DECLARE @StatusID VARCHAR(255) = '21, 22';
    --  DECLARE @AsTable BIT = 0;

    -- ---------------------------------------------
    -- Convert the StatusIDs to a list of IDs
    -- ---------------------------------------------
    DECLARE @IDs TABLE (ID TINYINT PRIMARY KEY);

    INSERT INTO @IDs
        SELECT value from String_Split(@StatusID, ',');
    
    --  if none, add in WIP
    IF (NOT EXISTS(SELECT * FROM @IDs))
        INSERT INTO @IDs (ID) 
            VALUES (22);
    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Pull a list and count of the line item status in those order statuses
    -- Store them in a temporary table so we can transform the output
    -- ---------------------------------------------
    DECLARE @Results Table (
                      ItemStatusID SMALLINT
                    , OrderStatusID TINYINT
                    , LocationID TINYINT
                    , [BreakdownCount] INT
                    );

    INSERT INTO @Results
        SELECT OI.ItemStatusID
             , O.OrderStatusID
             , O.LocationID
             , COUNT(*) as BreakdownCount
        FROM [Order.Data] O
        JOIN [Order.Item.Data] OI ON O.ID = OI.OrderID AND O.BID = OI.BID
        JOIN @IDs IDs ON O.OrderStatusID = IDs.ID
        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
        GROUP BY O.OrderStatusID, O.LocationID, OI.ItemStatusID
        ORDER BY ItemStatusID, O.OrderStatusID
    ;
    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT R.ItemStatusID, OIS.Name as 'Status'
             , SUM(R.BreakdownCount) OVER( PARTITION BY R.ItemStatusID ) as 'Count'
             , R.LocationID as 'Breakdown.LocationID'
             , R.OrderStatusID as 'Breakdown.OrderStatusID'
             , R.BreakdownCount as 'Breakdown.Count'
        FROM @Results R
        JOIN [Order.Item.Status] OIS ON ItemStatusID = OIS.ID AND @BID = OIS.BID
        ORDER BY 3 DESC, 2
        ;

    ELSE
    BEGIN
        SELECT DISTINCT R.ItemStatusID, OIS.Name as 'Status'
             , SUM(R.BreakdownCount) OVER( PARTITION BY R.ItemStatusID ) as 'Count'
             , (SELECT   R2.LocationID as 'LocationID'
                       , R2.OrderStatusID as 'OrderStatusID'
                       , R2.BreakdownCount as 'Count'
                FROM @Results R2 WHERE R2.ItemStatusID = R.ItemStatusID
                FOR JSON AUTO) AS Breadkdown
        FROM @Results R
        JOIN [Order.Item.Status] OIS ON ItemStatusID = OIS.ID AND @BID = OIS.BID
        ORDER BY 3 DESC, 2
        FOR JSON PATH
        ;
    END;
    -- ---------------------------------------------

END
");

            // Dashboard.Widget.Definition.003
            migrationBuilder.Sql(@"/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.003]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Order Throughput 
    The Order Throughput Widget displays a graph or a chart of the throughput of orders in each status for the specified period.
    This Widget measures the throughput of  each status, not the measure of what is in it. 
    Each time an order leaves a status it is recorded as throughput for that status.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/990969857/Dashboard+Widget+Order+Throughput
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.003] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.003]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
    --   DECLARE @BID SMALLINT = 1;
    --   DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --   DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    --   DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    --   DECLARE @AsTable BIT = 0;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    --                   = 26 for Closed                   = 9 for Closed
    --                   = 29 for Voided                   = 2 for Voided
    
    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , OrderStatusID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );


    -- pull orders that were created in the given range
    INSERT INTO @Results
        SELECT @LocationID, 1, 'Created', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 21 -- Created
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Pre-WIP (into WIP) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 21, 'Pre-WIP', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 5 -- WIP
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH WIP (into Built) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 22, 'WIP', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 6 -- Built
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Built (into Invoicing) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 23, 'Built', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 7 -- Invoicing
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Invoicing (into Invoiced) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 24, 'Invoicing', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 8 -- Invoiced
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were moved THROUGH Invoiced (into Closed) in the current range
    INSERT INTO @Results
        SELECT @LocationID, 25, 'Invoiced', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 7 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- pull orders that were voided in the current range
    INSERT INTO @Results
        SELECT @LocationID, 29, 'Voided', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 9 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;


    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------

END
");
            // Dashboard.Widget.Definition.007
            migrationBuilder.Sql(@"/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.007]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Estimate Summary
    The Estimate Summary Widget displays key information on estimate productivity for the specified location and period.  
    The Pending Estimates count and totals are the current number of pending estimates in the system.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/991658033/Dashboard+Widget+Estimate+Summary
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.007] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.007]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
    --   DECLARE @BID SMALLINT = 1;
    --   DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --   DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    --   DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    --   DECLARE @AsTable BIT = 1;


    --  Note
    --     OrderStatusID = 11 for Pending Estimate      KeyDateType = 1 for Created
    --                   = 12 for Approved (Won)                    = 14 for Approved
    --                   = 18 for Lost                              = 3 for Lost
    --                   = 19 for Voided                            = 2 for Voided
    
    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , OrderStatusID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );


    INSERT INTO @Results
        SELECT @LocationID, 11, 'Pending', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        JOIN [enum.Order.OrderStatus] OS ON OS.ID = E.OrderStatusID
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND E.OrderStatusID = 11 -- Pending
    ;

    INSERT INTO @Results
        SELECT @LocationID, 1, 'Created', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND (@LocationID = E.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 1 -- Created
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    INSERT INTO @Results
        SELECT @LocationID, 12, 'Approved', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND E.LocationID = ISNULL(@LocationID, E.LocationID)
        AND E.OrderStatusID = 12
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 14
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    INSERT INTO @Results
        SELECT @LocationID, 3, 'Lost', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND E.LocationID = ISNULL(@LocationID, E.LocationID)
        AND E.OrderStatusID = 18
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 3
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    INSERT INTO @Results
        SELECT @LocationID, 19, 'Voided', COUNT(*), SUM(E.[Price.PreTax])
        FROM [Order.Data] E
        WHERE E.BID = @BID AND E.LocationID = ISNULL(@LocationID, E.LocationID)
        AND E.OrderStatusID = 19
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = E.BID
                        AND OD.OrderID = E.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 2
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 1 DESC, 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------

END
");

            // Dashboard.Widget.Definition.006
            migrationBuilder.Sql(@"/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.006]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Financial Snapshot 
    The Financial Snapshot Widget displays a graph or a chart that summarizes many of the key financial data points for the business or location.  
    The information is most valuable when (visually) compared to the sales goal of the business or location to see how progress for the month is going.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/991657993/Dashboard+Widget+Financial+Snapshot
    for additional information.
        1. Monthly Goal
        2. MTD Sales
        3. MTD Payments
        4. Current A/Rs
        5. MTD Closed
        6. Current Pre-WIP
        7. Current WIP
        8. Current Built
        9. Current Invoicing
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.006] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.006]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
        -- DECLARE @BID SMALLINT = 1;
        -- DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
        -- DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
        -- DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
        -- DECLARE @AsTable BIT = 1;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    --                   = 26 for Closed                   = 9 for Closed
    --                   = 29 for Voided                   = 2 for Voided
    

    -- ---------------------------------------------
    -- Compute some working Data
    -- ---------------------------------------------
    DECLARE @MidDate DateTime2(2) = (SELECT DATEADD(SECOND, DATEDIFF(SECOND, @StartUTCDT, @EndUTCDT )/2, @StartUTCDT));


    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , ID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );


    -- Pull ID = 1 - Monthly Goal
    INSERT INTO @Results
        SELECT @LocationID, 1, 'Monthly Goal', 1, SUM(LG.Budgeted)
        FROM [Location.Goal] LG
        WHERE LG.BID = @BID AND (@LocationID = LG.LocationID OR @LocationID IS NULL)
          AND LG.Year = DatePart(Year, @MidDate)
          AND LG.Month = DatePart(Month, @MidDate)
    ;

    -- Pull ID = 2 - MTD Sales
    -- Rework to pull from GL .. but for now ... 
    INSERT INTO @Results
        SELECT @LocationID, 2, 'MTD Sales', COUNT(*), SUM(O.[Price.PreTax])
        FROM [Order.Data] O
        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 8 -- Invoiced
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- Pull ID = 3 - MTD Payments
    -- to get this, we can look at all new master payments (money in) and all payment applications refunds (money out)
    DECLARE @MoneyIn DECIMAL(18,4) = 
                    ( SELECT SUM(Amount)
                       FROM [Accounting.Payment.Master] P
                       WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                       AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                       AND P.PaymentTransactionType = 1 
                    )
    ;

    DECLARE @MoneyOut DECIMAL(18,4) = 
                    (   SELECT SUM(Amount)
                        FROM [Accounting.Payment.Application] P
                        WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                        AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                        AND P.PaymentTransactionType = 2
                    )
    ;

    INSERT INTO @Results
        SELECT @LocationID, 3, 'MTD Payments', 1, ISNULL(@MoneyIn, 0) + IsNull(@MoneyOut, 0)
    ;

    -- Pull ID = 4 - Current A/Rs
    -- Pull ID = 6 - Current Pre-WIP
    -- Pull ID = 7 - Current WIP
    -- Pull ID = 8 - Current Built
    -- Pull ID = 9 - Current Invoicing
    INSERT INTO @Results
        SELECT @LocationID
                , CASE OrderStatusID WHEN 21 THEN 6 WHEN 22 THEN 7 WHEN 23 THEN 8 WHEN 24 THEN 9 WHEN 25 THEN 4 END
                , 'Current ' + CASE OrderStatusID WHEN 21 THEN 'Pre-WIP' WHEN 22 THEN 'WIP' WHEN 23 THEN 'Built' WHEN 24 THEN 'Invoicing' WHEN 25 THEN 'A/Rs' END
                , TheCount
                , TheSum
        FROM 
        (
            SELECT OS.ID AS [OrderStatusID]
                    , COUNT(O.[Price.PreTax]) TheCount
                    , SUM(O.[Price.PreTax]) TheSum 
            FROM [Order.Data] O
            RIGHT JOIN (Values (21), (22), (23), (24), (25)) AS OS(ID) ON O.OrderStatusID = OS.ID
            WHERE (O.BID IS NULL)
               OR (O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL))
            GROUP BY OS.ID
        ) AS Temp
    ;

    -- Pull ID = 5 - MTD Closed
    INSERT INTO @Results
        SELECT @LocationID, 5, 'MTD Closed', COUNT(*), SUM(O.[Price.PreTax])
        FROM [Order.Data] O
        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 9 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE [Dashboard.Widget.Definition.005]
DROP PROCEDURE [Dashboard.Widget.Definition.003]
DROP PROCEDURE [Dashboard.Widget.Definition.007]
DROP PROCEDURE [Dashboard.Widget.Definition.006]
            ");
        }
    }
}
