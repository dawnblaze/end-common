using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180222024658_CreatePaymentTermTable")]
    public partial class CreatePaymentTermTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Drop Default Value Constraint on DaysDue, constraint name is random
            migrationBuilder.Sql(@"
DECLARE @ConstraintName nvarchar(200)
SELECT @ConstraintName = Name 
FROM SYS.DEFAULT_CONSTRAINTS 
WHERE PARENT_OBJECT_ID = OBJECT_ID('[Accounting.Payment.Term]') 
AND PARENT_COLUMN_ID = (SELECT column_id FROM sys.columns WHERE NAME = N'DaysDue' AND object_id = OBJECT_ID(N'[Accounting.Payment.Term]'))

Select @ConstraintName

IF @ConstraintName IS NOT NULL
BEGIN
	EXEC('ALTER TABLE [Accounting.Payment.Term] DROP CONSTRAINT  IF EXISTS ' + @ConstraintName)
END
"
            );
            migrationBuilder.Sql(@"
    ALTER TABLE [Accounting.Payment.Term] DROP CONSTRAINT IF EXISTS [DF_Accounting.Payment.Term_ModifiedDT];

    ALTER TABLE [Accounting.Payment.Term] DROP CONSTRAINT IF EXISTS [DF_Accounting.Payment.Term_IsActive];

");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Term_enum.Accounting.PaymentDueBasedOnType_PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Payment.Term_PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Payment.Term_Name",
                table: "Accounting.Payment.Term");
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Accounting.Payment.Term",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDT",
                table: "Accounting.Payment.Term",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "GetUTCDate()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldDefaultValueSql: "(getutcdate())");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Payment.Term",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<short>(
                name: "DaysDue",
                table: "Accounting.Payment.Term",
                nullable: false,
                defaultValueSql: "0",
                oldClrType: typeof(short));

            migrationBuilder.AddColumn<byte>(
                name: "PaymentDueBasedOnType",
                table: "Accounting.Payment.Term",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AlterColumn<bool>(
                name: "CanEdit",
                table: "Accounting.GL.Account",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<string>(
                name: "PaymentDueBasedOnText",
                table: "Accounting.Payment.Term",
                nullable: true,
                computedColumnSql: "(case [PaymentDueBasedOnType] when (0) then 'Days after Invoice' when (1) then 'Days after Built' when (2) then 'Days after Ordered' when (3) then 'Days After End of Invoice Month' when (4) then 'Day of the Month' when (5) then 'Day of the Following Month' end)",
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "DownPaymentRequired",
                table: "Accounting.Payment.Term",
                nullable: true,
                computedColumnSql: " (case when [DownPaymentPercent]>(0.00) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)",
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Accounting.Payment.Term",
                nullable: false,
                computedColumnSql: "11105",
                oldClrType: typeof(int),
                oldComputedColumnSql: "((11105))");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Term_PaymentDueBasedOnType",
                table: "Accounting.Payment.Term",
                column: "PaymentDueBasedOnType");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Term_Business.Data",
                table: "Accounting.Payment.Term",
                column: "BID",
                principalTable: "Business.Data",
                principalColumn: "BID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Method_enum.Accounting.PaymentDueBasedOnType",
                table: "Accounting.Payment.Term",
                column: "PaymentDueBasedOnType",
                principalTable: "enum.Accounting.PaymentDueBasedOnType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            //Insert enum values...
            migrationBuilder.Sql(@"
            INSERT [enum.Accounting.PaymentDueBasedOnType] ([ID], [Name])
            VALUES (0, N'Days After Invoice')
                , (1, N'Days After Built')
                , (2, N'Days After Placed')
                , (3, N'Days After End of Invoice Month')
                , (4, N'Day of the Month')
                , (5, N'Day of the Following Month')
            ");

            //Create simplelist view...
            migrationBuilder.Sql(@"
            IF EXISTS(select 1 from sys.views where name='Accounting.Payment.Term.SimpleList' and type='v')
            DROP VIEW [dbo].[Accounting.Payment.Term.SimpleList];
            ");

            migrationBuilder.Sql(@"
            CREATE VIEW[dbo].[Accounting.Payment.Term.SimpleList]
                AS
            SELECT[BID]
                , [ID]
                , [ClassTypeID]
                , [Name] as DisplayName
                , [IsActive]
                , CONVERT(BIT, 0) AS [IsDefault]
            FROM[Accounting.Payment.Term]
            ");

            //SPARSE columns
            migrationBuilder.Sql(@"
            IF COL_LENGTH('[dbo].[Accounting.Payment.Term]', 'EarlyPaymentPercent') IS NOT NULL
            BEGIN
	            ALTER TABLE [Accounting.Payment.Term] ALTER COLUMN
		            [EarlyPaymentPercent]    DECIMAL (12, 4) SPARSE NULL
            END

            IF COL_LENGTH('[dbo].[Accounting.Payment.Term]', 'EarlyPaymentDays') IS NOT NULL
            BEGIN
	            ALTER TABLE [Accounting.Payment.Term] ALTER COLUMN
		            [EarlyPaymentDays]        SMALLINT SPARSE        NULL
            END
            ");

            //Add PaymentTerm options
            migrationBuilder.Sql(@"
            --Payment Section
            INSERT INTO [System.Option.Section] (ID, Name, ParentID, IsHidden, Depth, SearchTerms)
            VALUES (11, 'Payment', 1, 0, 2, 'Payment')

            --Payment Type Category
            INSERT INTO [System.Option.Section] (ID, Name, ParentID, IsHidden, Depth, SearchTerms)
            VALUES (12, 'Payment Type', 11, 0, 3, 'Payment Type, Type, Deposit, Credit Card, Mastercard, Visa, AmEx, Check, Cash, Method, Payment Method')

            INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
            VALUES (10, 'Accounting.Defaults', 11, '', 4, 0)

            INSERT INTO [System.Option.Definition] (ID, Name, Label, Description, DataType, CategoryID, ListValues, DefaultValue, IsHidden)
            VALUES (40, 'Accounting.PaymentTerm.DefaultID', 'Payment Term Default ID', 'The PaymentTermID default to use for this business.', 1, 10, NULL, '', 0)
            ");

            //Stored procedures
            migrationBuilder.Sql(@"
            IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.CanDelete')
            BEGIN
                DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.CanDelete]
            END

            IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.SetActive')
            BEGIN
                DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetActive]
            END

            IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.SetDefault')
            BEGIN
                DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetDefault]
            END
            ");

            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Accounting.Payment.Term.Action.CanDelete]

    Description: This procedure checks if the PaymentTerm is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Payment.Term.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Payment.Term.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the PaymentTerm can be deleted. The boolean response is returned in the body.  A PaymentTerm can be deleted if
	It is used by no companies. 
	It is used by no order
	It is used by no estimates
	It is used by no purchase orders
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Payment.Term] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term Specified Does not Exist. PaymentTermID=', @ID)

/* --FUTURE
	ELSE IF  EXISTS( SELECT * FROM [Company.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Company Data. PaymentTermID=', @ID)

	ELSE IF EXISTS( SELECT * FROM [Order.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Order Data. PaymentTermID=', @ID)
	ELSE IF EXISTS( SELECT * FROM [Estimate.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Estimate Data. PaymentTermID=', @ID)
	ELSE IF EXISTS( SELECT * FROM [PO.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Purchase Order Data. PaymentTermID=', @ID)
*/
    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
            ");

            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Payment.Term.Action.SetActive]
--
-- Description: This procedure sets the PaymentTerm to active or inactive
--
-- Sample Use:   EXEC dbo.[Accounting.Payment.Term.Action.SetActive] @BID=1, @PaymentTermID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @PaymentTermID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the PaymentTerm specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Term] WHERE BID = @BID and ID = @PaymentTermID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid PaymentTerm Specified. PaymentTermID='+CONVERT(VARCHAR(12),@PaymentTermID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.Payment.Term] L
    WHERE BID = @BID and ID = @PaymentTermID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
            ");

            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Payment.Term.Action.SetDefault]
--
-- Description: This procedure sets the default PaymentTerm in the options definition table.
--
-- Sample Use:   EXEC dbo.[Accounting.Payment.Term.Action.SetDefault] @BID=1, @PaymentTermID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetDefault]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @PaymentTermID     SMALLINT     -- = 2

        , @IsDefault       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the option specified is valid
    IF NOT EXISTS(SELECT * FROM [System.Option.Definition] WHERE Name = 'Accounting.PaymentTerm.DefaultID')
    BEGIN
        SELECT @Result = 0
             , @Message = 'The Accounting.PaymentTerm.DefaultID OPTION is not found.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET DefaultValue = @PaymentTermID
    FROM [System.Option.Definition] L
    WHERE Name = 'Accounting.PaymentTerm.DefaultID'

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            DELETE FROM [System.Option.Section] WHERE ID=11
            DELETE FROM [System.Option.Section] WHERE ID=12
            DELETE FROM [System.Option.Category] WHERE ID=10
            DELETE FROM [System.Option.Definition] WHERE ID=40
            ");

            migrationBuilder.Sql(@"
            IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.CanDelete')
            BEGIN
                DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.CanDelete]
            END

            IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.SetActive')
            BEGIN
                DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetActive]
            END

            IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.SetDefault')
            BEGIN
                DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetDefault]
            END
            ");

            migrationBuilder.Sql(@"
            IF EXISTS(select 1 from sys.views where name='Accounting.Payment.Term.SimpleList' and type='v')
            DROP VIEW [dbo].[Accounting.Payment.Term.SimpleList];
            ");

            migrationBuilder.Sql(@"
            DELETE FROM[enum.Accounting.PaymentDueBasedOnType]
            ");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Term_Business.Data",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Method_enum.Accounting.PaymentDueBasedOnType",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Payment.Term_PaymentDueBasedOnType",
                table: "Accounting.Payment.Term");

            migrationBuilder.DropColumn(
                name: "PaymentDueBasedOnType",
                table: "Accounting.Payment.Term");

            migrationBuilder.AlterColumn<string>(
                name: "PaymentDueBasedOnText",
                table: "Accounting.Payment.Term",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true,
                oldComputedColumnSql: "(case [PaymentDueBasedOnType] when (0) then 'Days after Invoice' when (1) then 'Days after Built' when (2) then 'Days after Ordered' when (3) then 'Days After End of Invoice Month' when (4) then 'Day of the Month' when (5) then 'Day of the Following Month' end)");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Accounting.Payment.Term",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ModifiedDT",
                table: "Accounting.Payment.Term",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "(getutcdate())",
                oldClrType: typeof(DateTime),
                oldType: "datetime2(2)",
                oldDefaultValueSql: "GetUTCDate()");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Payment.Term",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "DownPaymentRequired",
                table: "Accounting.Payment.Term",
                nullable: true,
                oldClrType: typeof(bool),
                oldNullable: true,
                oldComputedColumnSql: " (case when [DownPaymentPercent]>(0.00) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)0");

            migrationBuilder.AlterColumn<short>(
                name: "DaysDue",
                table: "Accounting.Payment.Term",
                nullable: false,
                oldClrType: typeof(short),
                oldDefaultValueSql: "0");

            migrationBuilder.AddColumn<byte>(
                name: "PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "CanEdit",
                table: "Accounting.GL.Account",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Accounting.Payment.Term",
                nullable: false,
                computedColumnSql: "((11105))",
                oldClrType: typeof(int),
                oldComputedColumnSql: "11105");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Term_PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term",
                column: "PaymentDueBasedOnTypeID");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Term_enum.Accounting.PaymentDueBasedOnType_PaymentDueBasedOnTypeID",
                table: "Accounting.Payment.Term",
                column: "PaymentDueBasedOnTypeID",
                principalTable: "enum.Accounting.PaymentDueBasedOnType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

