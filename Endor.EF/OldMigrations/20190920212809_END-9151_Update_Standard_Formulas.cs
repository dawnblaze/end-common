﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9151_Update_Standard_Formulas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Part.Subassembly.Variable]
SET ConsumptionDefaultValue = '=Quantity.Value'
WHERE ElementType IN (104,105,106,107)
;
UPDATE V
SET  DefaultValue = '=Quantity.Value * FixedPrice'
FROM [Part.Subassembly.Variable] V
     JOIN [Part.Subassembly.Data] A ON A.ID = V.SubassemblyID AND A.BID = V.BID
WHERE V.Name = 'Price' AND A.PricingType = 0 AND A.PriceFormulaType = 0
;
UPDATE V
SET  DefaultValue = '=Quantity.Value * TierPriceTable'
FROM [Part.Subassembly.Variable] V
     JOIN [Part.Subassembly.Data] A ON A.ID = V.SubassemblyID AND A.BID = V.BID
WHERE V.Name = 'Price' AND A.PricingType = 0 AND A.PriceFormulaType = 1
;
UPDATE V
SET  DefaultValue = '=Quantity.Value * FixedPrice * (1-TierDiscountTable)'
FROM [Part.Subassembly.Variable] V
     JOIN [Part.Subassembly.Data] A ON A.ID = V.SubassemblyID AND A.BID = V.BID
WHERE V.Name = 'Price' AND A.PricingType = 0 AND A.PriceFormulaType = 2
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
