using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180405050305_CREATE_VIEW_Part.Material.SimpleList")]
    public partial class CREATE_VIEW_PartMaterialSimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Material.SimpleList]
                GO
                CREATE VIEW [dbo].[Part.Material.SimpleList]
                    AS
                SELECT	[BID]
	                  , [ID]
                      , [ClassTypeID]
                      , [Name] as DisplayName
                      , [IsActive]
                      , [HasImage]
                      , CONVERT(BIT, 0) AS [IsDefault]
                FROM [Part.Material.Data];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Material.SimpleList]
            ");
        }
    }
}

