using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180802193941_AddDomainEmailFilterListAndCriteria")]
    public partial class AddDomainEmailFilterListAndCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                    ([TargetClassTypeID]
                    ,[Name]
                    ,[Label]
                    ,[Field]
                    ,[IsHidden]
                    ,[DataType]
                    ,[InputType]
                    ,[AllowMultiple]
                    ,[ListValues]
                    ,[ListValuesEndpoint]
                    ,[IsLimitToList]
                    ,[SortIndex]
                    ,[ID])
                VALUES
                    (1022
                    ,'Is Active'
                    ,'Is Active'
                    ,'IsActive'
                    ,0
                    ,3
                    ,2
                    ,0
                    ,'Is Not Active,Is Active'
                    ,NULL
                    ,1
                    ,1
                    ,(SELECT (MAX(ID)+1) FROM [dbo].[System.List.Filter.Criteria])
                        );


                INSERT INTO [dbo].[System.List.Filter.Criteria]
                    ([TargetClassTypeID]
                    ,[Name]
                    ,[Label]
                    ,[Field]
                    ,[IsHidden]
                    ,[DataType]
                    ,[InputType]
                    ,[AllowMultiple]
                    ,[ListValues]
                    ,[ListValuesEndpoint]
                    ,[IsLimitToList]
                    ,[SortIndex]
                    ,[ID])
                VALUES
                    (1022--<TargetClassTypeID, int,>
                    ,'Location'--<Name, varchar(255),>
                    ,'Location'--<Label, varchar(255),>
                    ,'LocationIDs'--<Field, varchar(255),>
                    ,0--<IsHidden, bit,>
                    ,1--<DataType, tinyint,>
                    ,4--<InputType, tinyint,>
                    ,0--<AllowMultiple, bit,>
                    ,NULL--<ListValues, varchar(max),>
                    ,NULL--<ListValuesEndpoint, varchar(255),>
                    ,1--<IsLimitToList, bit,>
                    ,0--<SortIndex, tinyint,>
                    ,(SELECT (MAX(ID)+1) FROM [dbo].[System.List.Filter.Criteria]));--<ID, smallint,>

                
                INSERT INTO [dbo].[List.Filter]
                    ([BID]
                    ,[ID]
                    ,[CreatedDate]
                    ,[ModifiedDT]
                    ,[IsActive]
                    ,[Name]
                    ,[TargetClassTypeID]
                    ,[IDs]
                    ,[Criteria]
                    ,[OwnerID]
                    ,[IsPublic]
                    ,[IsSystem]
                    ,[Hint]
                    ,[IsDefault]
                    ,[SortIndex])
                VALUES
                    (1--<BID, smallint,>
                    ,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]) --<ID, int,>
                    ,GETUTCDATE()
                    ,GETUTCDATE()
                    ,1--<IsActive, bit,>
                    ,'Active'--<Name, varchar(255),>
                    ,1022--<TargetClassTypeID, int,>
                    ,NULL--<IDs, xml,>
                    ,'
                    <ArrayOfListFilterItem>
                        <ListFilterItem>
                            <Label>Active</Label>
                            <SearchValue>True</SearchValue>
                            <Field>IsActive</Field>
                            <DisplayText>Is Active</DisplayText>
                            <IsHidden>false</IsHidden>
                        </ListFilterItem>
                        </ArrayOfListFilterItem>
                    '
                    ,NULL--<OwnerID, smallint,>
                    ,0--<IsPublic, bit,>
                    ,1--<IsSystem, bit,>
                    ,NULL--<Hint, varchar(max),>
                    ,1--<IsDefault, bit,>
                    ,0);--<SortIndex, tinyint,>                
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria]
                WHERE [TargetClassTypeID] = 5101;

                DELETE FROM [dbo].[List.Filter]
                WHERE [TargetClassTypeID] = 5101;            
            ");
        }
    }
}

