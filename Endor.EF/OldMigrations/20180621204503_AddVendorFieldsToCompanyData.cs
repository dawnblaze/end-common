using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180621204503_AddVendorFieldsToCompanyData")]
    public partial class AddVendorFieldsToCompanyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PaymentTermsID",
                table: "Company.Data");

            migrationBuilder.AddColumn<bool>(
                name: "IsTaxExempt",
                table: "Company.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PaymentTermID",
                table: "Company.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "TaxExemptReasonID",
                table: "Company.Data",
                type: "smallint",
                nullable: true);

            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Company.Data]
                ALTER COLUMN TaxExemptReasonID SMALLINT SPARSE NULL
            ");

            migrationBuilder.AddColumn<string>(
                name: "VendorAccountNumber",
                table: "Company.Data",
                maxLength: 100,
                nullable: true);

            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Company.Data]
                ALTER COLUMN VendorAccountNumber VARCHAR(100) SPARSE NULL
            ");

            migrationBuilder.AddColumn<int>(
                name: "VendorPaymentTermID",
                table: "Company.Data",
                nullable: true);

            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Company.Data]
                ALTER COLUMN VendorPaymentTermID INT SPARSE NULL
            ");

            migrationBuilder.AddColumn<string>(
                name: "VendorTaxIDNumber",
                table: "Company.Data",
                maxLength: 100,
                nullable: true);

            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Company.Data]
                ALTER COLUMN VendorTaxIDNumber VARCHAR(100) SPARSE NULL
            ");

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_PaymentTerm",
                table: "Company.Data",
                columns: new[] { "BID", "PaymentTermID" },
                principalTable: "Accounting.Payment.Term",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_VendorPaymentTerm",
                table: "Company.Data",
                columns: new[] { "BID", "VendorPaymentTermID" },
                principalTable: "Accounting.Payment.Term",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_PaymentTerm",
                table: "Company.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_VendorPaymentTerm",
                table: "Company.Data");

            migrationBuilder.DropColumn(
                name: "IsTaxExempt",
                table: "Company.Data");

            migrationBuilder.DropColumn(
                name: "PaymentTermID",
                table: "Company.Data");

            migrationBuilder.DropColumn(
                name: "TaxExemptReasonID",
                table: "Company.Data");

            migrationBuilder.DropColumn(
                name: "VendorAccountNumber",
                table: "Company.Data");

            migrationBuilder.DropColumn(
                name: "VendorPaymentTermID",
                table: "Company.Data");

            migrationBuilder.DropColumn(
                name: "VendorTaxIDNumber",
                table: "Company.Data");

            migrationBuilder.AddColumn<short>(
                name: "PaymentTermsID",
                table: "Company.Data",
                nullable: true);
        }
    }
}

