using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateAssemblyLayoutTypeEnumsRenameLayouts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE dbo.[Part.Subassembly.Layout]
                SET [Name] = 'Other'
                WHERE [LayoutType]=12;   

                UPDATE [enum.Machine.LayoutType] SET Name = 'Other' WHERE ID = 0;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Template Primary' WHERE ID = 11;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Template Material Layout' WHERE ID = 12;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Template Setup' WHERE ID = 13;         
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE dbo.[Part.Subassembly.Layout]
                SET [Name] = 'Layout Manager'
                WHERE [LayoutType]=12;            

                UPDATE [enum.Machine.LayoutType] SET Name = 'None' WHERE ID = 0;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Order Entry Basic' WHERE ID = 11;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Order Entry Advanced' WHERE ID = 12;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Setup' WHERE ID = 13;
            ");
        }
    }
}
