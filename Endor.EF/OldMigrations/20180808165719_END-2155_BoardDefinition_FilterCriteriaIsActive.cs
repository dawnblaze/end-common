using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180808165719_END-2155_BoardDefinition_FilterCriteriaIsActive")]
    public partial class END2155_BoardDefinition_FilterCriteriaIsActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                (
                @"
                    INSERT INTO [System.List.Filter.Criteria]
                    (
                       [ID]
                      ,[TargetClassTypeID]
                      ,[Name]
                      ,[Label]
                      ,[Field]
                      ,[IsHidden]
                      ,[DataType]
                      ,[InputType]
                      ,[AllowMultiple]
                      ,[ListValues]
                      ,[ListValuesEndpoint]
                      ,[IsLimitToList]
                      ,[SortIndex]
                    )
                    VALUES
                    (
                      /*Get Next ID*/
                      (SELECT MAX(ID) + 1 FROM [System.List.Filter.Criteria])
                      , 14000                       --[TargetClassTypeID] (Board Definition)
                      , 'Is Active'                 --[Name]
                      , 'Is Active'                 --[Label]
                      , 'IsActive'                  --[Field]
                      , 'False'                       --[IsHidden]
                      , 3                           --[DataType]
                      , 2                           --[InputType]
                      , 'False'                       --[AllowMultiple]
                      , 'Is Not Active,Is Active'   --[ListValues]
                      , NULL                        --[ListValuesEndpoint]
                      , 'True'                        --[IsLimitToList]
                      , 1                           --[SortIndex]
                    )
                "
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    DELETE
                    FROM [System.List.Filter.Criteria]
                    WHERE TargetClassTypeID = 14000
                    AND Field = 'IsActive'
                "
            );
        }
    }
}

