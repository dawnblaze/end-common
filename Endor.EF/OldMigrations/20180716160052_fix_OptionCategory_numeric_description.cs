using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180716160052_fix_OptionCategory_numeric_description")]
    public partial class fix_OptionCategory_numeric_description : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [dbo].[System.Option.Category]
                SET [Description] = [Name]
                WHERE ISNUMERIC([Description]) = 1
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

