﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Alter_EnumPaymentDuebaseOnType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    UPDATE [enum.Accounting.PaymentDueBasedOnType] SET [Name] = 'Day of the Month After Built' WHERE [ID] = 3;
                    UPDATE[enum.Accounting.PaymentDueBasedOnType] SET[Name] = 'Day of the Month After Invoiced' WHERE [ID] = 5;
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
