using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180718210344_END_1857_Fix_EnumUserRightIdType")]
    public partial class END_1857_Fix_EnumUserRightIdType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Rights.Group.RightLink_RightID",
                table: "Rights.Group.RightLink");

            migrationBuilder.DropIndex(
                name: "IX_Rights.Group.RightLink_Right",
                table: "Rights.Group.RightLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.RightLink_enum.User.Right",
                table: "Rights.Group.RightLink");

            migrationBuilder.Sql(@"ALTER TABLE [dbo].[Rights.Group.RightLink] DROP CONSTRAINT [PK_Rights.Group.RightLink] WITH ( ONLINE = OFF )");

            migrationBuilder.Sql(@"ALTER TABLE [dbo].[enum.User.Right] DROP CONSTRAINT [PK_enum.User.Right] WITH ( ONLINE = OFF )");

            migrationBuilder.AlterColumn<short>(
                name: "RightID",
                table: "Rights.Group.RightLink",
                nullable: false,
                oldClrType: typeof(byte));

            migrationBuilder.AlterColumn<short>(
                name: "ID",
                table: "enum.User.Right",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint");

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group.RightLink_RightID",
                table: "Rights.Group.RightLink",
                column: "RightID");

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group.RightLink_Right",
                table: "Rights.Group.RightLink",
                columns: new[] { "BID", "RightID", "RightsGroupID" });

            migrationBuilder.Sql
            (
                @"
                    ALTER TABLE [dbo].[Rights.Group.RightLink] ADD  CONSTRAINT [PK_Rights.Group.RightLink] PRIMARY KEY CLUSTERED 
                    (
	                    [BID] ASC,
	                    [RightsGroupID] ASC,
	                    [RightID] ASC
                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                "
            );

            migrationBuilder.Sql
            (
                @"
                    ALTER TABLE [dbo].[enum.User.Right] ADD  CONSTRAINT [PK_enum.User.Right] PRIMARY KEY CLUSTERED 
                    (
	                    [ID] ASC
                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Rights.Group.RightLink_RightID",
                table: "Rights.Group.RightLink");

            migrationBuilder.DropIndex(
                name: "IX_Rights.Group.RightLink_Right",
                table: "Rights.Group.RightLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.RightLink_enum.User.Right",
                table: "Rights.Group.RightLink");

            migrationBuilder.Sql(@"ALTER TABLE [dbo].[Rights.Group.RightLink] DROP CONSTRAINT [PK_Rights.Group.RightLink] WITH ( ONLINE = OFF )");

            migrationBuilder.Sql(@"ALTER TABLE [dbo].[enum.User.Right] DROP CONSTRAINT [PK_enum.User.Right] WITH ( ONLINE = OFF )");

            migrationBuilder.AlterColumn<byte>(
                name: "RightID",
                table: "Rights.Group.RightLink",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<byte>(
                name: "ID",
                table: "enum.User.Right",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group.RightLink_RightID",
                table: "Rights.Group.RightLink",
                column: "RightID");

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group.RightLink_Right",
                table: "Rights.Group.RightLink",
                columns: new[] { "BID", "RightID", "RightsGroupID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.RightLink_enum.User.Right",
                table: "Rights.Group.RightLink",
                column: "RightID",
                principalTable: "enum.User.Right",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql
            (
                @"
                    ALTER TABLE [dbo].[Rights.Group.RightLink] ADD  CONSTRAINT [PK_Rights.Group.RightLink] PRIMARY KEY CLUSTERED 
                    (
	                    [BID] ASC,
	                    [RightsGroupID] ASC,
	                    [RightID] ASC
                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                "
            );

            migrationBuilder.Sql
            (
                @"
                    ALTER TABLE [dbo].[enum.User.Right] ADD  CONSTRAINT [PK_enum.User.Right] PRIMARY KEY CLUSTERED 
                    (
	                    [ID] ASC
                    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                "
            );
        }
    }
}

