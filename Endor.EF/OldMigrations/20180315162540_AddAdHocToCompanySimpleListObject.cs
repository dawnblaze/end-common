using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180315162540_AddAdHocToCompanySimpleListObject")]
    public partial class AddAdHocToCompanySimpleListObject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP VIEW [dbo].[Company.SimpleList];
GO
CREATE VIEW [dbo].[Company.SimpleList] AS
    SELECT [BID]
        ,[ID]
        ,[ClassTypeID]
        ,[Name] as DisplayName
        ,[IsActive]
        ,[HasImage]
        ,CONVERT(BIT, 0) AS [IsDefault]
        ,[IsAdHoc]
    FROM [Company.Data]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP VIEW [dbo].[Company.SimpleList];
GO
CREATE VIEW [dbo].[Company.SimpleList] AS
    SELECT [BID]
        ,[ID]
        ,[ClassTypeID]
        ,[Name] as DisplayName
        ,[IsActive]
        ,[HasImage]
        ,CONVERT(BIT, 0) AS [IsDefault]
    FROM [Company.Data]");
        }
    }
}

