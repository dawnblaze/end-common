using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Correct_Item_Statuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Order.Item.Status]
SET    Name = 'Pre-WIP'
WHERE  ID = 21 AND Name = 'WIP'
;
UPDATE [Order.Item.Status]
SET    Name = 'WIP'
WHERE  ID = 22 AND Name = 'Pre-WIP'
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
