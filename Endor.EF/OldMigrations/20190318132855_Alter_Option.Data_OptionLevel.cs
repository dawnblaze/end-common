using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Alter_OptionData_OptionLevel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex("IX_Option.Data_OptionID_Level_BIDNull", "Option.Data");
            migrationBuilder.DropIndex("IX_Option.Data_BID_OptionID_Level", "Option.Data");
            migrationBuilder.DropColumn("OptionLevel", "Option.Data");

            migrationBuilder.AddColumn<byte>(
                name: "OptionLevel",
                table: "Option.Data",
                nullable: false,
                computedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (128) when [CompanyID] IS NOT NULL then (64) when [UserID] IS NOT NULL then (32) when [StorefrontID] IS NOT NULL then (16) when [LocationID] IS NOT NULL then (8) when [BID] IS NOT NULL then (4) when [AssociationID] IS NOT NULL then (2) else (0) end),(0)))"
                );

            migrationBuilder.CreateIndex("IX_Option.Data_OptionID_Level_BIDNull", "Option.Data", new string[] { "OptionID", "OptionLevel" });
            migrationBuilder.CreateIndex("IX_Option.Data_BID_OptionID_Level", "Option.Data", new string[] { "BID", "OptionID", "OptionLevel" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex("IX_Option.Data_OptionID_Level_BIDNull", "Option.Data");
            migrationBuilder.DropIndex("IX_Option.Data_BID_OptionID_Level", "Option.Data");
            migrationBuilder.DropColumn("OptionLevel", "Option.Data");

            migrationBuilder.AddColumn<bool>(
                name: "OptionLevel",
                table: "Option.Data",
                nullable: true,
                computedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (128) when [CompanyID] IS NOT NULL then (64) when [UserID] IS NOT NULL then (32) when [StorefrontID] IS NOT NULL then (16) when [LocationID] IS NOT NULL then (8) when [BID] IS NOT NULL then (4) when [AssociationID] IS NOT NULL then (2) else (0) end),(0)))"
                );

            migrationBuilder.CreateIndex("IX_Option.Data_OptionID_Level_BIDNull", "Option.Data", new string[] { "OptionID", "OptionLevel" });
            migrationBuilder.CreateIndex("IX_Option.Data_BID_OptionID_Level", "Option.Data", new string[] { "BID", "OptionID", "OptionLevel" });
        }
    }
}
