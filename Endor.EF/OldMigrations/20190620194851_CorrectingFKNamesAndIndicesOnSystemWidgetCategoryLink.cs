using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CorrectingFKNamesAndIndicesOnSystemWidgetCategoryLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DashboardWidgetCategoryLink.Data_enum.DashboardWidgetCategoryType",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropForeignKey(
                name: "FK_DashboardWidgetCategoryLink.WidgetDefinition",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropPrimaryKey(
                name: "PK_System.Dashboard.Widget.CategoryLink",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropIndex(
                name: "IX_System.Dashboard.Widget.CategoryLink_WidgetDefID",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropIndex(
                name: "IX_DashboardWidgetCategoryLink.DashboardWidgetCategoryType",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropIndex(
                name: "IX_DashboardWidgetCategoryLink.WidgetDefinition",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.AddPrimaryKey(
                name: "PK_System.Dashboard.Widget.CategoryLink",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] { "CategoryType", "WidgetDefID" });

            migrationBuilder.CreateIndex(
                name: "IX_System.Dashboard.Widget.CategoryLink_WidgetDefID",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] { "WidgetDefID", "CategoryType" });

            migrationBuilder.AddForeignKey(
                name: "FK_System.Dashboard.Widget.CategoryLink_enum.Dashboard.Widget.CategoryType",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "CategoryType",
                principalTable: "enum.Dashboard.Widget.CategoryType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_System.Dashboard.Widget.CategoryLink_System.Dashboard.Widget.Definition",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "WidgetDefID",
                principalTable: "System.Dashboard.Widget.Definition",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_System.Dashboard.Widget.CategoryLink_enum.Dashboard.Widget.CategoryType",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropForeignKey(
                name: "FK_System.Dashboard.Widget.CategoryLink_System.Dashboard.Widget.Definition",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropPrimaryKey(
                name: "PK_System.Dashboard.Widget.CategoryLink",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.DropIndex(
                name: "IX_System.Dashboard.Widget.CategoryLink_WidgetDefID",
                table: "System.Dashboard.Widget.CategoryLink");

            migrationBuilder.AddPrimaryKey(
                name: "PK_System.Dashboard.Widget.CategoryLink",
                table: "System.Dashboard.Widget.CategoryLink",
                columns: new[] { "WidgetDefID", "CategoryType" });

            migrationBuilder.CreateIndex(
                name: "IX_DashboardWidgetCategoryLink.DashboardWidgetCategoryType",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "CategoryType");

            migrationBuilder.CreateIndex(
                name: "IX_DashboardWidgetCategoryLink.WidgetDefinition",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "WidgetDefID");

            migrationBuilder.AddForeignKey(
                name: "FK_DashboardWidgetCategoryLink.Data_enum.DashboardWidgetCategoryType",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "CategoryType",
                principalTable: "enum.Dashboard.Widget.CategoryType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DashboardWidgetCategoryLink.WidgetDefinition",
                table: "System.Dashboard.Widget.CategoryLink",
                column: "WidgetDefID",
                principalTable: "System.Dashboard.Widget.Definition",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
