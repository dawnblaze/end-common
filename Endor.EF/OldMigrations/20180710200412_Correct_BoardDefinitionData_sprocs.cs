using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180710200412_Correct_BoardDefinitionData_sprocs")]
    public partial class Correct_BoardDefinitionData_sprocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"
DROP PROCEDURE IF EXISTS dbo.[Board.Definition.Action.PostResults];
GO
 
-- ========================================================
-- Name: [Board.Definition.Action.PostResults]( @BID tinyint, @BoardID smallint, @RecordCount int, @TimeInSec float )
--
-- Description: This function records the time for a board execution
-- and updates the cummulative timer on the board.
--  Sample Use:  
/*
    DECLARE @BID SMALLINT = 1
          , @BoardID INT = 10
          , @RecordCount INT = 5
          , @TimeInSec FLOAT = 3.2;
 
    EXEC dbo.[Board.Definition.Action.PostResults] @BID=@BID, @BoardID=@BoardID, @RecordCount=@RecordCount, @TimeInSec=@TimeInSec
    SELECT * FROM [Board.Definition] WHERE BID = @BID AND ID = @BoardID
*/
-- ========================================================
CREATE PROCEDURE [dbo].[Board.Definition.Action.PostResults]
--DECLARE
          @BID          TINYINT = 1
        , @BoardID      INT     = 10
        , @RecordCount  INT     = 7
        , @TimeInSec    FLOAT   = 1.24
AS
BEGIN
    -- Now update it
    UPDATE B
    SET   ModifiedDT = GETUTCDATE()
        , LastRunDT = GETUTCDATE()
        , LastRecordCount = @RecordCount
        , LastRunDurationSec = @TimeInSec
        , CummCounterDT = IsNull(CummCounterDT, GETUTCDATE())
        , CummRunCount += 1
        , CummRunDurationSec += @TimeInSec
    FROM [Board.Definition.Data] B
    WHERE BID = @BID
      AND ID = @BoardID
    ;
 
    DECLARE @Rows INT = @@RowCount;
 
    -- Check if the Board specified is valid
    IF (@Rows = 0)
    BEGIN
        DECLARE @Message VARCHAR(1024) = CONCAT('Invalid Board Specified. BoardID=',@BoardID,' not found');
        THROW 50000, @Message, 1;
    END;
 
    RETURN @Rows;
END;
");

            migrationBuilder.Sql(
@"
DROP PROCEDURE IF EXISTS dbo.[Board.Definition.Action.ResetTimer];
 
GO
-- ========================================================
-- Name: [Board.Definition.Action.ResetTimer]( @BID tinyint = null, @BoardID smallint = null )
--
-- Description: This function resets the specified Board Cummulative Timers.
-- If no @BoardID is specified, all Boards for the given BID are reset.
-- If not @BID is specified, all BIDs are reset.
--
-- Sample Use:   EXEC dbo.[Board.Definition.Action.ResetTimer] @BID=1
-- Sample Use:   EXEC dbo.[Board.Definition.Action.ResetTimer] @BID=1, @BoardID=10
-- ========================================================
CREATE PROCEDURE [dbo].[Board.Definition.Action.ResetTimer]
--DECLARE
          @BID          TINYINT = 1
        , @BoardID      INT     = 10
 
AS
BEGIN
    -- Update it
    UPDATE B
    SET   ModifiedDT = GETUTCDATE()
        , CummCounterDT = GETUTCDATE()
        , CummRunCount = 0
        , CummRunDurationSec = 0
    FROM [Board.Definition.Data] B
    WHERE BID = ISNULL(@BID, BID)
      AND ID = ISNULL(@BoardID, ID)
    ;
 
    DECLARE @Rows INT = @@RowCount;
 
    -- Check if the Board specified is valid
    IF (@Rows = 0)
    BEGIN
        DECLARE @Message VARCHAR(1024) = CONCAT('Invalid Board Specified. BoardID=',@BoardID,' not found');
        THROW 50000, @Message, 1;
    END;
 
    RETURN @Rows;
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"
DROP PROCEDURE IF EXISTS dbo.[Board.Definition.Action.PostResults];
GO
 
-- ========================================================
-- Name: [Board.Definition.Action.PostResults]( @BID tinyint, @BoardID smallint, @RecordCount int, @TimeInSec float )
--
-- Description: This function records the time for a board execution
-- and updates the cummulative timer on the board.
--  Sample Use:  
/*
    DECLARE @BID SMALLINT = 1
          , @BoardID INT = 10
          , @RecordCount INT = 5
          , @TimeInSec FLOAT = 3.2;
 
    EXEC dbo.[Board.Definition.Action.PostResults] @BID=@BID, @BoardID=@BoardID, @RecordCount=@RecordCount, @TimeInSec=@TimeInSec
    SELECT * FROM [Board.Definition] WHERE BID = @BID AND ID = @BoardID
*/
-- ========================================================
CREATE PROCEDURE [dbo].[Board.Definition.Action.PostResults]
--DECLARE
          @BID          TINYINT = 1
        , @BoardID      INT     = 10
        , @RecordCount  INT     = 7
        , @TimeInSec    FLOAT   = 1.24
AS
BEGIN
    -- Now update it
    UPDATE B
    SET   ModifiedDT = GETUTCDATE()
        , LastRunDT = GETUTCDATE()
        , LastRecordCount = @RecordCount
        , LastRunDurationSec = @TimeInSec
        , CummCounterDT = IsNull(CummCounterDT, GETUTCDATE())
        , CummRunCount += 1
        , CummRunDurationSec += @TimeInSec
    FROM [Board.Definition] B
    WHERE BID = @BID
      AND ID = @BoardID
    ;
 
    DECLARE @Rows INT = @@RowCount;
 
    -- Check if the Board specified is valid
    IF (@Rows = 0)
    BEGIN
        DECLARE @Message VARCHAR(1024) = CONCAT('Invalid Board Specified. BoardID=',@BoardID,' not found');
        THROW 50000, @Message, 1;
    END;
 
    RETURN @Rows;
END;
");

            migrationBuilder.Sql(
@"
DROP PROCEDURE IF EXISTS dbo.[Board.Definition.Action.ResetTimer];
 
GO
-- ========================================================
-- Name: [Board.Definition.Action.ResetTimer]( @BID tinyint = null, @BoardID smallint = null )
--
-- Description: This function resets the specified Board Cummulative Timers.
-- If no @BoardID is specified, all Boards for the given BID are reset.
-- If not @BID is specified, all BIDs are reset.
--
-- Sample Use:   EXEC dbo.[Board.Definition.Action.ResetTimer] @BID=1
-- Sample Use:   EXEC dbo.[Board.Definition.Action.ResetTimer] @BID=1, @BoardID=10
-- ========================================================
CREATE PROCEDURE [dbo].[Board.Definition.Action.ResetTimer]
--DECLARE
          @BID          TINYINT = 1
        , @BoardID      INT     = 10
 
AS
BEGIN
    -- Update it
    UPDATE B
    SET   ModifiedDT = GETUTCDATE()
        , CummCounterDT = GETUTCDATE()
        , CummRunCount = 0
        , CummRunDurationSec = 0
    FROM [Board.Definition] B
    WHERE BID = ISNULL(@BID, BID)
      AND ID = ISNULL(@BoardID, ID)
    ;
 
    DECLARE @Rows INT = @@RowCount;
 
    -- Check if the Board specified is valid
    IF (@Rows = 0)
    BEGIN
        DECLARE @Message VARCHAR(1024) = CONCAT('Invalid Board Specified. BoardID=',@BoardID,' not found');
        THROW 50000, @Message, 1;
    END;
 
    RETURN @Rows;
END;
");
        }
    }
}

