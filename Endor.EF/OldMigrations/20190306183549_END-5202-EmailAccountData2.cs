using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END5202EmailAccountData2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsPrivate",
                table: "Email.Account.Data",
                nullable: false,
                computedColumnSql: "(isnull(case when [EmployeeID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))",
                oldClrType: typeof(bool),
                oldComputedColumnSql: "isnull(case when [EmployeeID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Email.Account.Data",
                nullable: false,
                computedColumnSql: "(isnull(case when [StatusType]=(2) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))",
                oldClrType: typeof(bool),
                oldComputedColumnSql: "case when [StatusType]=(2) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsPrivate",
                table: "Email.Account.Data",
                nullable: false,
                computedColumnSql: "isnull(case when [EmployeeID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))",
                oldClrType: typeof(bool),
                oldComputedColumnSql: "(isnull(case when [EmployeeID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Email.Account.Data",
                nullable: false,
                computedColumnSql: "case when [StatusType]=(2) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))",
                oldClrType: typeof(bool),
                oldComputedColumnSql: "(isnull(case when [StatusType]=(2) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");
        }
    }
}
