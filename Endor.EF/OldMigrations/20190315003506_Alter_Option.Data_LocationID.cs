using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Alter_OptionData_LocationID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropIndex("IX_Option.Data_OptionID_Level_BIDNull", "Option.Data");
            migrationBuilder.DropIndex("IX_Option.Data_BID_OptionID_Level", "Option.Data");
            migrationBuilder.DropColumn("OptionLevel", "Option.Data");

            migrationBuilder.Sql
                (
                    @"
                    DELETE [option.data]
                    WHERE LocationID NOT IN (SELECT DISTINCT ID FROM [Location.Data])
                    "
                );

            migrationBuilder.AlterColumn<byte>(
                name: "LocationID",
                table: "Option.Data",
                nullable: true,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OptionLevel",
                table: "Option.Data",
                computedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (128) when [CompanyID] IS NOT NULL then (64) when [UserID] IS NOT NULL then (32) when [StorefrontID] IS NOT NULL then (16) when [LocationID] IS NOT NULL then (8) when [BID] IS NOT NULL then (4) when [AssociationID] IS NOT NULL then (2) else (0) end),(0)))",
                nullable: true);

            migrationBuilder.CreateIndex("IX_Option.Data_OptionID_Level_BIDNull", "Option.Data", new string[] { "OptionID", "OptionLevel" });
            migrationBuilder.CreateIndex("IX_Option.Data_BID_OptionID_Level", "Option.Data", new string[] { "BID", "OptionID", "OptionLevel" });


            migrationBuilder.AddForeignKey(
                name: "FK_Option.Data_Location.Data",
                table: "Option.Data",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropIndex("IX_Option.Data_OptionID_Level_BIDNull", "Option.Data");
            migrationBuilder.DropIndex("IX_Option.Data_BID_OptionID_Level", "Option.Data");
            migrationBuilder.DropColumn("OptionLevel", "Option.Data");

            migrationBuilder.DropForeignKey(
                            name: "FK_Option.Data_Location.Data",
                            table: "Option.Data");

            migrationBuilder.Sql
                (
                    @"
                    DELETE [option.data]
                    WHERE LocationID NOT IN (SELECT DISTINCT ID FROM [Location.Data]) OR LocationID IS NULL
                    "
                );

            migrationBuilder.AlterColumn<short>(
                name: "LocationID",
                table: "Option.Data",
                nullable: true,
                oldClrType: typeof(byte),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "OptionLevel",
                table: "Option.Data",
                computedColumnSql: "(isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (128) when [CompanyID] IS NOT NULL then (64) when [UserID] IS NOT NULL then (32) when [StorefrontID] IS NOT NULL then (16) when [LocationID] IS NOT NULL then (8) when [BID] IS NOT NULL then (4) when [AssociationID] IS NOT NULL then (2) else (0) end),(0)))",
                nullable: true);

            migrationBuilder.CreateIndex("IX_Option.Data_OptionID_Level_BIDNull", "Option.Data", new string[] { "OptionID", "OptionLevel" });
            migrationBuilder.CreateIndex("IX_Option.Data_BID_OptionID_Level", "Option.Data", new string[] { "BID", "OptionID", "OptionLevel" });

        }
    }
}
