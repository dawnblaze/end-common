using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class EstimatesFiltersAndCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                        (
                            @"
DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=10200;
INSERT INTO [dbo].[List.Filter]
           ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
     VALUES
           (1 --<BID, smallint,>
           ,(SELECT MAX(ID)+1 FROM dbo.[List.Filter]) --<ID, int,>
           ,GETUTCDATE() --<CreatedDate, date,>
           ,GETUTCDATE() --<ModifiedDT, datetime2(2),>
           ,1 --<IsActive, bit,>
           ,'All' --<Name, varchar(255),>
           ,10200 --<TargetClassTypeID, int,>
           ,NULL --<IDs, xml,>
           ,NULL --<Criteria, xml,>
           ,NULL --<OwnerID, smallint,>
           ,1 --<IsPublic, bit,>
           ,1 --<IsSystem, bit,>
           ,NULL --<Hint, varchar(max),>
           ,1 --<IsDefault, bit,>
           ,0);--<SortIndex, tinyint,>
"
            );



            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (10200 --<TargetClassTypeID, int,>
                        ,'Estimate #' --<Name, varchar(255),>
                        ,'Estimate #' --<Label, varchar(255),>
                        ,'Number' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,1 --<DataType, tinyint,>
                        ,0 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,1 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (10200 --<TargetClassTypeID, int,>
                        ,'Estimate Status' --<Name, varchar(255),>
                        ,'Estimate Status' --<Label, varchar(255),>
                        ,'OrderStatusID' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,2 --<DataType, tinyint,>
                        ,5 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,2 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (10200 --<TargetClassTypeID, int,>
                        ,'Employees' --<Name, varchar(255),>
                        ,'Employees' --<Label, varchar(255),>
                        ,'Employees' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,1 --<DataType, tinyint,>
                        ,0 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,3 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (10200 --<TargetClassTypeID, int,>
                        ,'Company' --<Name, varchar(255),>
                        ,'Company' --<Label, varchar(255),>
                        ,'Company' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,1 --<DataType, tinyint,>
                        ,0 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,4 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (10200 --<TargetClassTypeID, int,>
                        ,'Contact' --<Name, varchar(255),>
                        ,'Contact' --<Label, varchar(255),>
                        ,'Contact' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,1 --<DataType, tinyint,>
                        ,0 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,5 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (10200 --<TargetClassTypeID, int,>
                        ,'Include Closed Estimates' --<Name, varchar(255),>
                        ,'Include Closed Estimates' --<Label, varchar(255),>
                        ,'-IsActive' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,13 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,6 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                        (
                            @"
DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=10200;
"
            );

            migrationBuilder.Sql(@"
DELETE FROM [System.List.Filter.Criteria] WHERE [TargetClassTypeID] = 10200
");
        }
    }
}
