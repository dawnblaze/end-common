using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class OrderItemComponentColumnChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DisableSystemVersioningIfEnabled("Order.Item.Component");

            this.ImplementChanges(migrationBuilder, "Order.Item.Component");
            this.ImplementChanges(migrationBuilder, "Historic.Order.Item.Component");

            migrationBuilder.Sql("DROP TABLE IF EXISTS dbo.[Historic.Order.Item.Component]");
            migrationBuilder.EnableSystemVersioning("Order.Item.Component");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DisableSystemVersioningIfEnabled("Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "Cost.PerItem",
                table: "Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "Price.PerItem",
                table: "Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "Quantity.IsLocked",
                table: "Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "Quantity.PerItem",
                table: "Order.Item.Component");

            migrationBuilder.RenameColumn(
                name: "Quantity.OV",
                table: "Order.Item.Component",
                newName: "Quantity.Estimated.OV");

            migrationBuilder.RenameColumn(
                name: "Price.Computed",
                table: "Order.Item.Component",
                newName: "Quantity.Remaining");

            migrationBuilder.RenameColumn(
                name: "Cost.OV",
                table: "Order.Item.Component",
                newName: "Quantity.Actual.OV");

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Unit",
                table: "Order.Item.Component",
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2) NULL",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Cost.UnitOV",
                table: "Order.Item.Component",
                type: "BIT",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "Quantity.Actual",
                table: "Order.Item.Component",
                type: "DECIMAL(18,2) SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Quantity.Estimated",
                table: "Order.Item.Component",
                type: "DECIMAL(18,2)",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Quantity",
                table: "Order.Item.Component",
                nullable: true,
                computedColumnSql: "(isnull([Quantity.Actual],[Quantity.Estimated]))",
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Price.Net",
                table: "Order.Item.Component",
                nullable: true,
                computedColumnSql: "([Price.Unit]*[Quantity.Estimated])",
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2) NULL",
                oldNullable: true,
                oldComputedColumnSql: "([Quantity] * [Price.Unit])");

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Net",
                table: "Order.Item.Component",
                nullable: true,
                computedColumnSql: "(isnull([Quantity.Actual],[Quantity.Estimated])*[Cost.Unit])",
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2) NULL",
                oldNullable: true,
                oldComputedColumnSql: "([Quantity] * [Cost.Unit])");

            migrationBuilder.AddColumn<decimal>(
                name: "Cost.Net.Actual",
                table: "Order.Item.Component",
                nullable: true,
                computedColumnSql: "([Quantity.Actual]*[Cost.Unit])");

            migrationBuilder.AddColumn<decimal>(
                name: "Cost.Net.Estimated",
                table: "Order.Item.Component",
                nullable: true,
                computedColumnSql: "([Quantity.Estimated]*[Cost.Unit])");

            migrationBuilder.EnableSystemVersioning("Order.Item.Component");
        }

        private void ImplementChanges(MigrationBuilder migrationBuilder, string tableName)
        {
            migrationBuilder.DropColumn(
                name: "Cost.Net.Actual",
                table: tableName);

            migrationBuilder.DropColumn(
                name: "Cost.Net.Estimated",
                table: tableName);

            migrationBuilder.DropDefaultConstraintIfExists(
                tableName: tableName,
                columnName: "Cost.UnitOV");

            migrationBuilder.DropColumn(
                name: "Cost.UnitOV",
                table: tableName);

            migrationBuilder.DropColumn(
                name: "Quantity",
                table: tableName);

            migrationBuilder.DropColumn(
                name: "Cost.Net",
                table: tableName);

            migrationBuilder.DropColumn(
                name: "Quantity.Actual",
                table: tableName);

            migrationBuilder.DropColumn(
                name: "Price.Net",
                table: tableName);

            migrationBuilder.DropColumn(
                name: "Quantity.Estimated",
                table: tableName);

            migrationBuilder.DropDefaultConstraintIfExists(
                tableName: tableName,
                columnName: "Quantity.Estimated.OV");

            migrationBuilder.DropColumn(
                name: "Quantity.Estimated.OV",
                table: tableName);

            migrationBuilder.DropColumn(
                name: "Quantity.Remaining",
                table: tableName);

            migrationBuilder.AddColumn<decimal>(
                name: "Price.Computed",
                table: tableName,
                type: "DECIMAL(18,2) SPARSE",
                nullable: true);

            migrationBuilder.DropDefaultConstraintIfExists(
                tableName: tableName,
                columnName: "Quantity.Actual.OV");

            migrationBuilder.DropColumn(
                name: "Quantity.Actual.OV",
                table: tableName);

            migrationBuilder.AddColumn<bool>(
                name: "Quantity.OV",
                table: tableName,
                type: "BIT",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<bool>(
                name: "Cost.OV",
                table: tableName,
                type: "BIT",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<decimal>(
                name: "Quantity",
                table: tableName,
                type: "DECIMAL(18,2)",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Cost.Unit",
                table: tableName,
                type: "DECIMAL(18,2)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,2)",
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Quantity.IsLocked",
                table: tableName,
                type: "BIT",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<decimal>(
                name: "Quantity.PerItem",
                table: tableName,
                type: "DECIMAL(18,2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price.Net",
                table: tableName,
                type: "DECIMAL(18,2)",
                computedColumnSql: "([Quantity] * [Price.Unit])",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Cost.Net",
                table: tableName,
                type: "DECIMAL(18,2)",
                computedColumnSql: "([Quantity] * [Cost.Unit])",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Cost.PerItem",
                table: tableName,
                type: "DECIMAL(18,2)",
                nullable: true,
                computedColumnSql: "([Cost.Unit]*[Quantity.PerItem])");

            migrationBuilder.AddColumn<decimal>(
                name: "Price.PerItem",
                table: tableName,
                type: "DECIMAL(18,2)",
                nullable: true,
                computedColumnSql: "([Price.Unit]*[Quantity.PerItem])");
        }
    }
}
