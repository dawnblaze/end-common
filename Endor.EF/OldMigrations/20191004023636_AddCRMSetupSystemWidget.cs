﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddCRMSetupSystemWidget : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT [System.Dashboard.Widget.Definition] ([ID], [DefaultCols], [DefaultName], [DefaultOptions], [DefaultRefreshIntervalInMin], [DefaultRows], [Description], [HasImage], [MaxCols], [MaxRows], [MinCols], [MinRows], [Modules], [SecurityRightID]) 
                VALUES (8, 1, N'CRM Setup', NULL, 3600, 1, NULL, 1, 1, 1, 1, 1, NULL, NULL);

                GO

                INSERT [System.Dashboard.Widget.CategoryLink] ([WidgetDefID], [CategoryType]) 
                VALUES (8, 1), (8, 4);    
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [System.Dashboard.Widget.Definition] WHERE [ID] = 8;

                GO

                DELETE FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 8 AND [CategoryType] = 1;

                GO

                DELETE FROM [System.Dashboard.Widget.CategoryLink] WHERE [WidgetDefID] = 8 AND [CategoryType] = 4;
            ");
        }
    }
}
