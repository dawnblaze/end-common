using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class MessageFilterCriteriaEmployeeID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO[dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID]
                       ,[Name]
                       ,[Label]
                       ,[Field]
                       ,[IsHidden]
                       ,[DataType]
                       ,[InputType]
                       ,[AllowMultiple]
                       ,[ListValues]
                       ,[ListValuesEndpoint]
                       ,[IsLimitToList]
                       ,[SortIndex]
                       ,[ID])
                   VALUES
                       (14109 --<TargetClassTypeID, int,>
                        ,'Employee' --<Name, varchar(255),>
                        ,'Employee'  --<Label, varchar(255),>
                        ,'EmployeeID' --<Field, varchar(255),>
                        ,1 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,9 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,1 --<IsLimitToList, bit,>
                        ,1 --<SortIndex, tinyint,>
                        ,(SELECT (MAX(ID)+1) FROM[dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE [TargetClassTypeID] = 14109 AND Name = 'Employee';
            ");
        }
    }
}
