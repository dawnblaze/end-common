using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Insert_Space_And_SplitColumn_InputType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT [dbo].[enum.CustomField.InputType] ([ID], [Name], [DataType]) 
                VALUES (7, N'Spacer', 0);

                INSERT [dbo].[enum.CustomField.InputType] ([ID], [Name], [DataType]) 
                VALUES (103, N'SplitColumn', 0);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[enum.CustomField.InputType] WHERE [Name] = 'Spacer' AND [ID] = 7;

                DELETE FROM [dbo].[enum.CustomField.InputType] WHERE [Name] = 'SplitColumn' AND [ID] = 103;
            ");
        }
    }
}
