using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class NewColumnForCustomFieldLayoutElement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_CustomField.Layout.Element_Containter",
                table: "CustomField.Layout.Element");

            migrationBuilder.AddColumn<int>(
                name: "ParentID",
                table: "CustomField.Layout.Element",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Element_BID_ParentID",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Element_Containter",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "ContainerID", "SortIndex", "ParentID" });

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Layout.Element_CustomField.Layout.Element",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "ParentID" },
                principalTable: "CustomField.Layout.Element",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Layout.Element_CustomField.Layout.Element",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropIndex(
                name: "IX_CustomField.Layout.Element_BID_ParentID",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropIndex(
                name: "IX_CustomField.Layout.Element_Containter",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "ParentID",
                table: "CustomField.Layout.Element");

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Element_Containter",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "ContainerID", "SortIndex" });
        }
    }
}
