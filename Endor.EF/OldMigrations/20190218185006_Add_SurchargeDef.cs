using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_SurchargeDef : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Surcharge.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AppliedByDefault = table.Column<bool>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12060"),
                    CompanyID = table.Column<int>(type: "INT SPARSE", nullable: true),
                    DefaultFixedFee = table.Column<decimal>(type: "decimal(18,6)", nullable: true),
                    DefaultPerUnitFee = table.Column<decimal>(type: "decimal(18,6)", nullable: true),
                    IncomeAccountID = table.Column<int>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(type: "nvarchar(255)", nullable: false),
                    SortIndex = table.Column<short>(nullable: false),
                    TaxCodeID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Surcharge.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Surcharge.Data_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Surcharge.Data_Accounting.Tax.Code",
                        columns: x => new { x.BID, x.TaxCodeID },
                        principalTable: "Accounting.Tax.Code",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Surcharge.Data_Name",
                table: "Part.Surcharge.Data",
                columns: new[] { "BID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Surcharge.Data_Company",
                table: "Part.Surcharge.Data",
                columns: new[] { "BID", "CompanyID", "SortIndex", "IsActive" },
                filter: "[CompanyID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Surcharge.Data_Sort",
                table: "Part.Surcharge.Data",
                columns: new[] { "BID", "SortIndex", "Name", "IsActive" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Surcharge.Data");
        }
    }
}
