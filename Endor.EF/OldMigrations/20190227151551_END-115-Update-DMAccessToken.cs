using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END115UpdateDMAccessToken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {            
            migrationBuilder.DropColumn(
                name: "SubassemblyDataBID",
                table: "Part.Assembly.Table");

            migrationBuilder.DropColumn(
                name: "SubassemblyDataID",
                table: "Part.Assembly.Table");

            migrationBuilder.CreateIndex(
                name: "IX_DM.Access.Token_URL",
                table: "DM.Access.Token",
                columns: new[] { "BID", "URL" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_DM.Access.Token_URL",
                table: "DM.Access.Token");

            migrationBuilder.AddColumn<short>(
                name: "SubassemblyDataBID",
                table: "Part.Assembly.Table",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SubassemblyDataID",
                table: "Part.Assembly.Table",
                nullable: true);
        }
    }
}
