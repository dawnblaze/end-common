using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180612130433_FixOrderItemActionChangeStatusSproc")]
    public partial class FixOrderItemActionChangeStatusSproc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Item.Action.ChangeStatus];
GO

/*********************************************************
  Name:
    [Order.Item.Action.ChangeStatus]( @BID tinyint, @OrderItemID int, @TargetStatusID smallint)
  
  Description:
    This function changes an Order Item Status to the target Order Item Status
  
  Sample Use:
    EXEC [dbo].[Order.Item.Action.ChangeStatus] @BID=1, @OrderItemID=1049, @TargetStatusID=23
*********************************************************/
CREATE PROCEDURE [dbo].[Order.Item.Action.ChangeStatus]
(
    @BID			TINYINT   --= 1
  , @OrderItemID	INT       --= 1049
  , @TargetStatusID	SMALLINT  --= 23
  
  , @Result			INT		  = NULL OUTPUT
)
AS
BEGIN
	DECLARE @Message VARCHAR(1024);

	-- Check if the OrderItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Item.Data] WHERE BID = @BID AND ID = @OrderItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid OrderItem Specified. OrderItemID='+CONVERT(VARCHAR,@OrderItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @OrderID INT = (SELECT [OrderID] FROM [Order.Item.Data] WHERE BID = @BID AND ID = @OrderItemID);

	-- Check if the OrderItemStatus specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Item.Status] WHERE BID = @BID AND ID = @TargetStatusID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid OrderItemStatus Specified. OrderItemStatusID='+CONVERT(VARCHAR,@TargetStatusID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @OrderStatusID INT = (SELECT OrderStatusID FROM [Order.Item.Status] WHERE BID = @BID AND ID = @TargetStatusID);

	IF (@OrderStatusID < (SELECT OrderStatusID FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID))
	BEGIN
        SELECT @Result = 0
             , @Message = 'The new Line Item Status can not be for an Order Status that is before the current one.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
	END

	UPDATE	OID
	SET		[ItemStatusID] = @TargetStatusID,
			[OrderStatusID] = @OrderStatusID
	FROM	[Order.Item.Data] AS OID
	WHERE	[BID] = @BID AND [ID] = @OrderItemID
		
    SET @Result = @@ROWCOUNT;
    SELECT @Result AS Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Item.Action.ChangeStatus];
GO

/*********************************************************
  Name:
    [Order.Item.Action.ChangeStatus]( @BID tinyint, @OrderItemID int, @TargetStatusID smallint)
  
  Description:
    This function changes an Order Item Status to the target Order Item Status
  
  Sample Use:
    EXEC [dbo].[Order.Item.Action.ChangeStatus] @BID=1, @OrderItemID=1049, @TargetStatusID=23
*********************************************************/
CREATE PROCEDURE [dbo].[Order.Item.Action.ChangeStatus]
(
    @BID			TINYINT   --= 1
  , @OrderItemID	INT       --= 1049
  , @TargetStatusID	SMALLINT  --= 23
  
  , @Result			INT		  = NULL OUTPUT
)
AS
BEGIN
	DECLARE @Message VARCHAR(1024);

	-- Check if the OrderItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Item.Data] WHERE BID = @BID AND ID = @OrderItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid OrderItem Specified. OrderItemID='+CONVERT(VARCHAR,@OrderItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @OrderID INT = (SELECT [OrderID] FROM [Order.Item.Data] WHERE BID = @BID AND ID = @OrderItemID);

	-- Check if the OrderItemStatus specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Item.Status] WHERE BID = @BID AND ID = @TargetStatusID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid OrderItemStatus Specified. OrderItemStatusID='+CONVERT(VARCHAR,@TargetStatusID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF ((SELECT OrderStatusID FROM [Order.Item.Status] WHERE BID = @BID AND ID = @TargetStatusID) < (SELECT OrderStatusID FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID))
	BEGIN
        SELECT @Result = 0
             , @Message = 'The new Line Item Status can not be for an Order Status that is before the current one.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
	END

	UPDATE	OID
	SET		[ItemStatusID] = @TargetStatusID
	FROM	[Order.Item.Data] AS OID
	WHERE	[BID] = @BID AND [ID] = @OrderItemID
		
    SET @Result = @@ROWCOUNT;
    SELECT @Result AS Result;
END
");
        }
    }
}

