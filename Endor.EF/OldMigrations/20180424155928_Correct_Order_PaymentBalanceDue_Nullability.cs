using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180424155928_Correct_Order_PaymentBalanceDue_Nullability")]
    public partial class Correct_Order_PaymentBalanceDue_Nullability : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Payment.BalanceDue",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: true,
                computedColumnSql: "(((((((([Price.ProductTotal] - isnull([Price.Discount], (0))) + isnull([Price.DestinationTotal], (0))) + isnull([Price.FinanceCharge], (0))) + [Price.Tax]) - isnull([Payment.Paid],(0))) - isnull([Payment.WriteOff],(0))) - isnull([CreditMemo.Applied], (0))) + isnull([CreditMemo.Credit],(0)))",
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldComputedColumnSql: "(((((((([Price.ProductTotal] - isnull([Price.Discount], (0))) + isnull([Price.DestinationTotal], (0))) + isnull([Price.FinanceCharge], (0))) + [Price.Tax]) - isnull([Payment.Paid],(0))) - isnull([Payment.WriteOff],(0))) - isnull([CreditMemo.Applied], (0))) + isnull([CreditMemo.Credit],(0)))");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Payment.BalanceDue",
                table: "Order.Data",
                type: "DECIMAL(18,4)",
                nullable: false,
                computedColumnSql: "(((((((([Price.ProductTotal] - isnull([Price.Discount], (0))) + isnull([Price.DestinationTotal], (0))) + isnull([Price.FinanceCharge], (0))) + [Price.Tax]) - isnull([Payment.Paid],(0))) - isnull([Payment.WriteOff],(0))) - isnull([CreditMemo.Applied], (0))) + isnull([CreditMemo.Credit],(0)))",
                oldClrType: typeof(decimal),
                oldType: "DECIMAL(18,4)",
                oldNullable: true,
                oldComputedColumnSql: "(((((((([Price.ProductTotal] - isnull([Price.Discount], (0))) + isnull([Price.DestinationTotal], (0))) + isnull([Price.FinanceCharge], (0))) + [Price.Tax]) - isnull([Payment.Paid],(0))) - isnull([Payment.WriteOff],(0))) - isnull([CreditMemo.Applied], (0))) + isnull([CreditMemo.Credit],(0)))");

        }
    }
}

