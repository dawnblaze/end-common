using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180417141533_Insert_MaterialInventory_GLAccount")]
    public partial class Insert_MaterialInventory_GLAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[Accounting.GL.Account] 
                ([BID], [ID], [ModifiedDT], [IsActive], [CanEdit], [Name], [Number], [GLAccountType], [ParentID], [ExportAccountName], [ExportAccountNumber], [ExportGLAccountType]) 
                VALUES (1, 1510, CAST(N'2018-01-01T00:00:00.0000000' AS DateTime2), 1, 1, N'Materials Inventory', 1510, 13, 1500, NULL, NULL, 13)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[Accounting.GL.Account]
                WHERE [BID]=1 AND [ID]=1510
            ");
        }
    }
}

