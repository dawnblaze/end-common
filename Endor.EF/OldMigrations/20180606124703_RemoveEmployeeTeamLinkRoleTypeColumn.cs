using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180606124703_RemoveEmployeeTeamLinkRoleTypeColumn")]
    public partial class RemoveEmployeeTeamLinkRoleTypeColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Employee.TeamLink] DROP CONSTRAINT IF EXISTS [DF_Employee.TeamLink_RoleType];
ALTER TABLE [Employee.TeamLink] DROP COLUMN IF EXISTS [RoleType];
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Employee.TeamLink] ADD [RoleType] TINYINT NOT NULL
CONSTRAINT [DF_Employee.TeamLink_RoleType] DEFAULT ((0))
WITH VALUES;
");
        }
    }
}

