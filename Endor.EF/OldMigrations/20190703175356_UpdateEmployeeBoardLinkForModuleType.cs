using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateEmployeeBoardLinkForModuleType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Board.Employee.Link",
                table: "Board.Employee.Link");

            migrationBuilder.DropIndex(
                name: "IX_Board.Employee.Link_Employee",
                table: "Board.Employee.Link");

            migrationBuilder.AddColumn<short>(
                name: "ModuleType",
                table: "Board.Employee.Link",
                type: "smallint",
                nullable: false,
                defaultValueSql: "(1)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Board.Employee.Link",
                table: "Board.Employee.Link",
                columns: new[] { "BID", "BoardID", "EmployeeID", "ModuleType" });

            migrationBuilder.CreateIndex(
                name: "IX_Board.Employee.Link_Employee",
                table: "Board.Employee.Link",
                columns: new[] { "BID", "EmployeeID", "BoardID", "ModuleType" });

            migrationBuilder.AddForeignKey(
                name: "FK_Board.Employee.Link_enum.Module",
                table: "Board.Employee.Link",
                column: "ModuleType",
                principalTable: "enum.Module",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Board.Employee.Link_enum.Module",
                table: "Board.Employee.Link");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Board.Employee.Link",
                table: "Board.Employee.Link");

            migrationBuilder.DropIndex(
                name: "IX_Board.Employee.Link_Employee",
                table: "Board.Employee.Link");

            migrationBuilder.DropColumn(
                name: "ModuleType",
                table: "Board.Employee.Link");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Board.Employee.Link",
                table: "Board.Employee.Link",
                columns: new[] { "BID", "BoardID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Board.Employee.Link_Employee",
                table: "Board.Employee.Link",
                columns: new[] { "BID", "EmployeeID", "BoardID" });
        }
    }
}
