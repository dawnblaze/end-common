using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180608141308_AddItemMoveSproc")]
    public partial class AddItemMoveSproc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
@"CREATE PROCEDURE [Order.Item.Action.Move] 
        @BID            SMALLINT
      , @SourceItemID   INT
      , @TargetItemID   INT
      , @MoveBefore     BIT
      , @Result         BIT          = NULL OUTPUT
      , @FailureReason  VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
    DECLARE @SourceItemNumber SMALLINT
          , @TargetItemNumber SMALLINT 
          , @StartItemNumber SMALLINT 
          , @EndItemNumber SMALLINT 
          , @SourceOrderID INT
          , @TargetOrderID INT
          ;

    SELECT @SourceOrderID = OrderID, @SourceItemNumber = ItemNumber
    FROM   [Order.Item.Data]
    WHERE  BID = @BID AND ID = @SourceItemID
    ;
    SELECT @TargetOrderID = OrderID, @TargetItemNumber = ItemNumber
    FROM   [Order.Item.Data]
    WHERE  BID = @BID AND ID = @TargetItemID
    ;

    IF (@SourceOrderID IS NULL OR @TargetOrderID IS NULL)
    BEGIN
        SELECT
            @FailureReason = 'Record not found'
          , @Result = 0;
        RETURN;
    END;

    IF (@SourceItemID = @TargetItemID)
    BEGIN
        SELECT
            @FailureReason = NULL
          , @Result = 1;
        RETURN;
    END;

    IF (@SourceOrderID != @TargetOrderID)
    BEGIN
        SELECT
            @FailureReason = 'Items are from different orders'
          , @Result = 0;
        RETURN;
    END;

    IF (@SourceItemNumber > @TargetItemNumber)
    BEGIN
      SELECT @StartItemNumber = @TargetItemNumber
           , @EndItemNumber = @SourceItemNumber
      ;
    END

    ELSE
    BEGIN
      SELECT @StartItemNumber = @SourceItemNumber
           , @EndItemNumber = @TargetItemNumber
      ;
    END

    DECLARE @T TABLE(ID INT PRIMARY KEY, SortIndex INT);

    INSERT INTO @T
    (ID, SortIndex)
    SELECT   ID, ItemNumber*10
    FROM     [Order.Item.Data]
    WHERE    BID = 1
             AND OrderID = @SourceOrderID
             AND ItemNumber BETWEEN @StartItemNumber AND @EndItemNumber
    ORDER BY ItemNumber
    ;

    IF (@MoveBefore = 0)
    BEGIN
        UPDATE @T
        SET    SortIndex = (@TargetItemNumber*10)+5
        WHERE  ID = @SourceItemID
    END

    ELSE
    BEGIN
        UPDATE @T
        SET    SortIndex = (@TargetItemNumber*10)-5
        WHERE  ID = @SourceItemID
    END;

    UPDATE OI
    SET    ItemNumber = T.NewNumber
    FROM   [Order.Item.Data] OI
            JOIN ( SELECT ID, NewNumber = (ROW_NUMBER() OVER (ORDER BY SortIndex)-1)+@StartItemNumber
                    FROM @T ) T ON OI.BID = @BID AND OI.ID = T.ID 

    SELECT
        @FailureReason = NULL
        , @Result = 1;
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP PROCEDURE [Order.Item.Action.Move]");
        }
    }
}

