using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class LinkedLaborAndLinkedMaterial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ConsumptionDefaultValue",
                table: "Part.Subassembly.Variable",
                type: "VARCHAR(MAX) SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsConsumptionFormula",
                table: "Part.Subassembly.Variable",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.Sql(@"
Insert [enum.ElementType] ([ID],[Name])
Values
        ( 104, N'LinkedMaterial')
        , (105, N'LinkedLabor')
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE [enum.ElementType] WHERE [ID]=104;");
            migrationBuilder.Sql(@"DELETE [enum.ElementType] WHERE [ID]=105;");

            migrationBuilder.DropColumn(
                name: "ConsumptionDefaultValue",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "IsConsumptionFormula",
                table: "Part.Subassembly.Variable");
        }
    }
}
