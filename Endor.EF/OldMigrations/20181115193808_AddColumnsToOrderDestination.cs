using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddColumnsToOrderDestination : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<bool>(
                name: "IsUrgent",
                table: "Order.Destination.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "Priority",
                table: "Order.Destination.Data",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUrgent",
                table: "Order.Destination.Data");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "Order.Destination.Data");

        }
    }
}
