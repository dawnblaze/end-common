using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class rename_material_layouts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [dbo].[Part.Subassembly.Layout]
                SET [Name]='Layout Manager'
                WHERE [LayoutType]=12;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [dbo].[Part.Subassembly.Layout]
                SET [Name]='Material Layout'
                WHERE [LayoutType]=12;
            ");
        }
    }
}
