using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180815181628_Update_OrderOptionDefinitions")]
    public partial class Update_OrderOptionDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.Option.Definition]
               SET [Description] = 'In some countries invoice numbers must be issued sequentially based on when the order is invoiced. If checked, invoices will generate a unique invoice number based on the invoice date.\nIf not checked, invoices will be generated based on the order creation date.'
               WHERE ID = 54
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.Option.Definition]
               SET [Description] = ''
               WHERE ID = 54
            GO
            ");
        }
    }
}

