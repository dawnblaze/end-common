﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9338_Modify_UtilBusinessDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE OR ALTER   PROCEDURE [dbo].[Util.Business.Delete]( @BID SMALLINT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteBusiness]
    BEGIN TRY

        DELETE FROM [Accounting.GL.Data] WHERE BID = @BID
        DELETE FROM [Order.Contact.Locator] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID
        DELETE FROM [Order.Contact.Role] WHERE BID = @BID
        DELETE FROM [Order.Employee.Role] WHERE BID = @BID
        DELETE FROM [Order.Item.Component] WHERE BID = @BID
        DELETE FROM [Order.Item.Surcharge] WHERE BID = @BID
        DELETE FROM [Order.Item.TagLink] WHERE BID = @BID
        DELETE FROM [Order.KeyDate] WHERE BID = @BID
        DELETE FROM [Order.Note] WHERE BID = @BID
        DELETE FROM [Order.Tax.Item.Assessment] WHERE BID = @BID
        DELETE FROM [Activity.GLActivity] WHERE BID = @BID
        DELETE FROM [Dashboard.Widget.Data] WHERE BID = @BID
        DELETE FROM [Order.Custom.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Data] WHERE BID = @BID
        DELETE FROM [Order.OrderLink] WHERE BID = @BID
        DELETE FROM [Order.TagLink] WHERE BID = @BID
        DELETE FROM [Dashboard.Data] WHERE BID = @BID
        DELETE FROM [Message.Delivery.Record] WHERE BID = @BID
        DELETE FROM [Message.Header] WHERE BID = @BID
        DELETE FROM [Order.Data] WHERE BID = @BID
        DELETE FROM [User.Draft] WHERE BID = @BID
        DELETE FROM [Alert.Definition.Action] WHERE BID = @BID
        DELETE FROM [Contact.Custom.Data] WHERE BID = @BID
        DELETE FROM [Contact.Locator] WHERE BID = @BID
        DELETE FROM [Contact.TagLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard.Detail] WHERE BID = @BID
        DELETE FROM [Message.Participant.Info] WHERE BID = @BID
        DELETE FROM [Opportunity.Data] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.CategoryLink] WHERE BID = @BID
        DELETE FROM [User.Link] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Master] WHERE BID = @BID
        DELETE FROM [Alert.Definition] WHERE BID = @BID
        DELETE FROM [Board.Employee.Link] WHERE BID = @BID
        DELETE FROM [Company.Custom.Data] WHERE BID = @BID
        DELETE FROM [Company.Locator] WHERE BID = @BID
        DELETE FROM [Company.TagLink] WHERE BID = @BID
        DELETE FROM [Contact.Data] WHERE BID = @BID
        DELETE FROM [Document.Report.Menu] WHERE BID = @BID
        DELETE FROM [Employee.Locator] WHERE BID = @BID
        DELETE FROM [Employee.TeamLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard] WHERE BID = @BID
        DELETE FROM [List.Filter.EmployeeSubscription] WHERE BID = @BID
		DELETE FROM [Part.Machine.Profile.Table] WHERE BID = @BID
        DELETE FROM [Part.Machine.Profile.Variable] WHERE BID = @BID
		DELETE FROM [Part.Machine.Profile] WHERE BID = @BID
		DELETE FROM [Part.Machine.Instance] WHERE BID = @BID
		DELETE FROM [Part.Assembly.Table] WHERE BID = @BID
		DELETE FROM [Part.Assembly.Variable.Formula] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Element] WHERE BID = @BID
        DELETE FROM [Part.Surcharge.Data] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code.ItemExemptionLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID
        DELETE FROM [Company.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Element] WHERE BID = @BID
        DELETE FROM [Domain.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.LocationLink] WHERE BID = @BID
        DELETE FROM [Employee.Data] WHERE BID = @BID
        DELETE FROM [Employee.Team.LocationLink] WHERE BID = @BID
        DELETE FROM [Location.Goal] WHERE BID = @BID
        DELETE FROM [Location.Locator] WHERE BID = @BID
        DELETE FROM [Order.Destination.TagLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Machine.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Material.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Layout] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Variable] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Method] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Item] WHERE BID = @BID
        DELETE FROM [Board.Module.Link] WHERE BID = @BID
        DELETE FROM [Board.Role.Link] WHERE BID = @BID
        DELETE FROM [Board.View.Link] WHERE BID = @BID
        DELETE FROM [Campaign.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Container] WHERE BID = @BID
        DELETE FROM [List.Tag.OtherLink] WHERE BID = @BID
		DELETE FROM [Option.Data] WHERE BID = @BID
        DELETE FROM [Location.Data] WHERE BID = @BID
        DELETE FROM [Message.Object.Link] WHERE BID = @BID
        DELETE FROM [Order.Destination.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.Data] WHERE BID = @BID
        DELETE FROM [Part.Machine.Data] WHERE BID = @BID
        DELETE FROM [Part.Material.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Data] WHERE BID = @BID
        DELETE FROM [Rights.Group.List.RightsGroupLink] WHERE BID = @BID
        DELETE FROM [Accounting.GL.Account] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Term] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group] WHERE BID = @BID
        DELETE FROM [Activity.Action] WHERE BID = @BID
        DELETE FROM [Activity.Event] WHERE BID = @BID
        DELETE FROM [Board.Definition.Data] WHERE BID = @BID
        DELETE FROM [Board.View] WHERE BID = @BID
        DELETE FROM [Business.Locator] WHERE BID = @BID
        DELETE FROM [CRM.Industry] WHERE BID = @BID
        DELETE FROM [CRM.Origin] WHERE BID = @BID
        DELETE FROM [CustomField.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Other.Data] WHERE BID = @BID
		DELETE FROM [Email.Account.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.Data] WHERE BID = @BID
        DELETE FROM [Employee.Role] WHERE BID = @BID
        DELETE FROM [Employee.Team] WHERE BID = @BID
        DELETE FROM [List.Filter] WHERE BID = @BID
        DELETE FROM [List.FlatList.Data] WHERE BID = @BID
        DELETE FROM [List.Tag] WHERE BID = @BID
        DELETE FROM [Message.Body] WHERE BID = @BID
        DELETE FROM [Order.Item.Status] WHERE BID = @BID
        DELETE FROM [Order.Item.SubStatus] WHERE BID = @BID
        DELETE FROM [Part.Labor.Category] WHERE BID = @BID
        DELETE FROM [Part.Machine.Category] WHERE BID = @BID
        DELETE FROM [Part.Material.Category] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Category] WHERE BID = @BID
        DELETE FROM [Report.Menu] WHERE BID = @BID
        DELETE FROM [Rights.Group.List] WHERE BID = @BID
        DELETE FROM [SSLCertificate.Data] WHERE BID = @BID
        DELETE FROM [Util.NextID] WHERE BID = @BID
		DELETE FROM [Business.TimeZone.Link] WHERE BID = @BID

        DELETE FROM [Business.Data] WHERE BID = @BID

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteBusiness]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteBusiness];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [DeleteBusiness]

END;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"CREATE OR ALTER   PROCEDURE [dbo].[Util.Business.Delete]( @BID SMALLINT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteBusiness]
    BEGIN TRY

        DELETE FROM [Accounting.GL.Data] WHERE BID = @BID
        DELETE FROM [Order.Contact.Locator] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID
        DELETE FROM [Order.Contact.Role] WHERE BID = @BID
        DELETE FROM [Order.Employee.Role] WHERE BID = @BID
        DELETE FROM [Order.Item.Component] WHERE BID = @BID
        DELETE FROM [Order.Item.Surcharge] WHERE BID = @BID
        DELETE FROM [Order.Item.TagLink] WHERE BID = @BID
        DELETE FROM [Order.KeyDate] WHERE BID = @BID
        DELETE FROM [Order.Note] WHERE BID = @BID
        DELETE FROM [Order.Tax.Item.Assessment] WHERE BID = @BID
        DELETE FROM [Activity.GLActivity] WHERE BID = @BID
        DELETE FROM [Dashboard.Widget.Data] WHERE BID = @BID
        DELETE FROM [Order.Custom.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Data] WHERE BID = @BID
        DELETE FROM [Order.OrderLink] WHERE BID = @BID
        DELETE FROM [Order.TagLink] WHERE BID = @BID
        DELETE FROM [Dashboard.Data] WHERE BID = @BID
        DELETE FROM [Message.Delivery.Record] WHERE BID = @BID
        DELETE FROM [Message.Header] WHERE BID = @BID
        DELETE FROM [Order.Data] WHERE BID = @BID
        DELETE FROM [User.Draft] WHERE BID = @BID
        DELETE FROM [Alert.Definition.Action] WHERE BID = @BID
        DELETE FROM [Contact.Custom.Data] WHERE BID = @BID
        DELETE FROM [Contact.Locator] WHERE BID = @BID
        DELETE FROM [Contact.TagLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard.Detail] WHERE BID = @BID
        DELETE FROM [Message.Participant.Info] WHERE BID = @BID
        DELETE FROM [Opportunity.Data] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.CategoryLink] WHERE BID = @BID
        DELETE FROM [User.Link] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Master] WHERE BID = @BID
        DELETE FROM [Alert.Definition] WHERE BID = @BID
        DELETE FROM [Board.Employee.Link] WHERE BID = @BID
        DELETE FROM [Company.Custom.Data] WHERE BID = @BID
        DELETE FROM [Company.Locator] WHERE BID = @BID
        DELETE FROM [Company.TagLink] WHERE BID = @BID
        DELETE FROM [Contact.Data] WHERE BID = @BID
        DELETE FROM [Document.Report.Menu] WHERE BID = @BID
        DELETE FROM [Employee.Locator] WHERE BID = @BID
        DELETE FROM [Employee.TeamLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard] WHERE BID = @BID
        DELETE FROM [List.Filter.EmployeeSubscription] WHERE BID = @BID
		DELETE FROM [Part.Machine.Profile.Table] WHERE BID = @BID
        DELETE FROM [Part.Machine.Profile.Variable] WHERE BID = @BID
		DELETE FROM [Part.Machine.Profile] WHERE BID = @BID
		DELETE FROM [Part.Machine.Instance] WHERE BID = @BID
		DELETE FROM [Part.Assembly.Table] WHERE BID = @BID
		DELETE FROM [Part.Assembly.Variable.Formula] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Element] WHERE BID = @BID
        DELETE FROM [Part.Surcharge.Data] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code.ItemExemptionLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID
        DELETE FROM [Company.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Element] WHERE BID = @BID
        DELETE FROM [Domain.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.LocationLink] WHERE BID = @BID
        DELETE FROM [Employee.Data] WHERE BID = @BID
        DELETE FROM [Employee.Team.LocationLink] WHERE BID = @BID
        DELETE FROM [Location.Goal] WHERE BID = @BID
        DELETE FROM [Location.Locator] WHERE BID = @BID
        DELETE FROM [Order.Destination.TagLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Machine.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Material.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Layout] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Variable] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Method] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Item] WHERE BID = @BID
        DELETE FROM [Board.Module.Link] WHERE BID = @BID
        DELETE FROM [Board.Role.Link] WHERE BID = @BID
        DELETE FROM [Board.View.Link] WHERE BID = @BID
        DELETE FROM [Campaign.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Container] WHERE BID = @BID
        DELETE FROM [List.Tag.OtherLink] WHERE BID = @BID
		DELETE FROM [Option.Data] WHERE BID = @BID
        DELETE FROM [Location.Data] WHERE BID = @BID
        DELETE FROM [Message.Object.Link] WHERE BID = @BID
        DELETE FROM [Order.Destination.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.Data] WHERE BID = @BID
        DELETE FROM [Part.Machine.Data] WHERE BID = @BID
        DELETE FROM [Part.Material.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Data] WHERE BID = @BID
        DELETE FROM [Rights.Group.List.RightsGroupLink] WHERE BID = @BID
        DELETE FROM [Accounting.GL.Account] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Term] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group] WHERE BID = @BID
        DELETE FROM [Activity.Action] WHERE BID = @BID
        DELETE FROM [Activity.Event] WHERE BID = @BID
        DELETE FROM [Board.Definition.Data] WHERE BID = @BID
        DELETE FROM [Board.View] WHERE BID = @BID
        DELETE FROM [Business.Locator] WHERE BID = @BID
        DELETE FROM [CRM.Industry] WHERE BID = @BID
        DELETE FROM [CRM.Origin] WHERE BID = @BID
        DELETE FROM [CustomField.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Other.Data] WHERE BID = @BID
		DELETE FROM [Email.Account.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.Data] WHERE BID = @BID
        DELETE FROM [Employee.Role] WHERE BID = @BID
        DELETE FROM [Employee.Team] WHERE BID = @BID
        DELETE FROM [List.Filter] WHERE BID = @BID
        DELETE FROM [List.FlatList.Data] WHERE BID = @BID
        DELETE FROM [List.Tag] WHERE BID = @BID
        DELETE FROM [Message.Body] WHERE BID = @BID
        DELETE FROM [Order.Item.Status] WHERE BID = @BID
        DELETE FROM [Order.Item.SubStatus] WHERE BID = @BID
        DELETE FROM [Part.Labor.Category] WHERE BID = @BID
        DELETE FROM [Part.Machine.Category] WHERE BID = @BID
        DELETE FROM [Part.Material.Category] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Category] WHERE BID = @BID
        DELETE FROM [Report.Menu] WHERE BID = @BID
        DELETE FROM [Rights.Group.List] WHERE BID = @BID
        DELETE FROM [SSLCertificate.Data] WHERE BID = @BID
        DELETE FROM [Util.NextID] WHERE BID = @BID

        DELETE FROM [Business.Data] WHERE BID = @BID

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteBusiness]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteBusiness];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [DeleteBusiness]

END;");
        }
    }
}
