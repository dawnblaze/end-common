using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateAssemblyWithNewFixedFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "FixedMargin",
                table: "Part.Subassembly.Data",
                type: "decimal(18,4) sparse",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FixedMarkup",
                table: "Part.Subassembly.Data",
                type: "decimal(18,4) sparse",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FixedPrice",
                table: "Part.Subassembly.Data",
                type: "decimal(18,4) sparse",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "HasTierTable",
                table: "Part.Subassembly.Data",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.Sql(@"
UPDATE [enum.Part.Subassembly.PricingType]
SET Name = 'Margin Based Pricing'
WHERE ID = 1;

INSERT INTO [enum.Part.Subassembly.PricingType]
  SELECT 2, 'Markup Based Pricing';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FixedMargin",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "FixedMarkup",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "FixedPrice",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "HasTierTable",
                table: "Part.Subassembly.Data");
        }
    }
}
