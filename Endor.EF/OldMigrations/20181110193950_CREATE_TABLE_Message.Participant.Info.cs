using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CREATE_TABLE_MessageParticipantInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Message.Participant.Info",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14112))"),
                    BodyID = table.Column<int>(nullable: false),
                    ParticipantRoleType = table.Column<byte>(type: "tinyint", nullable: false),
                    Channel = table.Column<byte>(type: "tinyint", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: true),
                    IsMergeField = table.Column<bool>(nullable: false),
                    EmployeeID = table.Column<short>(nullable: true),
                    TeamID = table.Column<int>(type: "int sparse", nullable: true),
                    ContactID = table.Column<int>(type: "int sparse", nullable: true),
                    DeliveredDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    IsDelivered = table.Column<bool>(nullable: false, computedColumnSql: "(case when [DeliveredDT] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)"),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Participant.Info", x => new { x.BID, x.ID });
                }
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Participant.Info_Employee",
                table: "Message.Participant.Info",
                columns: new[] {"BID", "EmployeeID"},
                principalTable: "Employee.Data",
                principalColumns: new[] {"BID", "ID"});

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Participant.Info_Team",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "TeamID" },
                principalTable: "Employee.Team",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Participant.Info_enum.Message.ChannelType",
                table: "Message.Participant.Info",
                column: "Channel",
                principalTable: "enum.Message.ChannelType",
                principalColumn:"ID");


            migrationBuilder.AddForeignKey(
                name: "FK_Message.Participant.Info_enum.Message.Participant.RoleType",
                table: "Message.Participant.Info",
                column: "ParticipantRoleType",
                principalTable: "enum.Message.Participant.RoleType",
                principalColumn: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Participant.Info_Message.Body",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "BodyID" },
                principalTable: "Message.Body",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Notification.Participant.Info_Contact.Data",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "ContactID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Body",
                table: "Message.Participant.Info",
                columns: new[] { "BID", "BodyID", "ParticipantRoleType" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Contact",
                table: "Message.Participant.Info",
                filter: "ContactID IS NOT NULL",
                columns: new[] {"BID", "ContactID"});

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Employee",
                table: "Message.Participant.Info",
                filter: "EmployeeID IS NOT NULL",
                columns: new[] { "BID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Participant.Info_Team",
                table: "Message.Participant.Info",
                filter: "TeamID IS NOT NULL",
                columns: new[] { "BID", "TeamID" });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Participant.Info_Employee",
                table: "Message.Participant.Info");

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Participant.Info_Team",
                table: "Message.Participant.Info");

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Participant.Info_enum.Message.ChannelType",
                table: "Message.Participant.Info");

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Participant.Info_enum.Message.Participant.RoleType",
                table: "Message.Participant.Info");

            migrationBuilder.DropForeignKey(
                name: "FK_Message.Participant.Info_Message.Body",
                table: "Message.Participant.Info");


            migrationBuilder.DropForeignKey(
                name: "FK_Notification.Participant.Info_Contact.Data",
                table: "Message.Participant.Info");

            migrationBuilder.DropIndex(
                name: "IX_Message.Participant.Info_Bod",
                table: "Message.Participant.Info");

            migrationBuilder.DropIndex(
                name: "IX_Message.Participant.Info_Contact",
                table: "Message.Participant.Info");

            migrationBuilder.DropIndex(
                name: "IX_Message.Participant.Info_Employee",
                table: "Message.Participant.Info");

            migrationBuilder.DropIndex(
                name: "IX_Message.Participant.Info_Team",
                table: "Message.Participant.Info");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Participant.Info",
                table: "Message.Participant.Info");

            migrationBuilder.DropTable(
                name: "Message.Participant.Info");
        }
    }
}
