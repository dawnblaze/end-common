using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class DeleteOptionDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    DELETE FROM [Option.Data] WHERE OptionID IN (20003, 20004, 20005, 20006, 20007, 20027);
                    DELETE FROM [System.Option.Definition] WHERE ID IN (20003, 20004, 20005, 20006, 20007, 20027);
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
