using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CREATE_PROC_UtilGenerateInserts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [Util.GenerateInserts]
GO

/***********************************************************************************************************
Purpose:	To generate INSERT statements from existing data. 
            These INSERTS can be executed to regenerate the data at some other location.
            This procedure is also useful to create a database setup, where in you can 
            script your data along with your table definitions.

Modified by: Scott

Sample Usage:

    EXEC [Util.GenerateInserts] 'List.Tag', @WhereClause = 'BID = 1', @MaxRows = 50

Original Source by: Narayana Vyas Kondreddi   http://vyaskn.tripod.com

***********************************************************************************************************/

CREATE PROC [Util.GenerateInserts] (
    @SourceTableName varchar(776)  		-- The table/view for which the INSERT statements will be generated using the existing data

    , @TargetTableName varchar(776) = NULL 	-- Use this parameter to specify a different table name into which the data will be inserted
    , @IncludeColumnNames bit = 1		-- Use this parameter to include/ommit column list in the generated INSERT statement
    , @WhereClause varchar(800) = NULL 		-- Use this parameter to filter the rows based on a filter condition (using WHERE)
    , @MaxRows int = NULL			-- Use this parameter to generate INSERT statements only for the TOP n rows

    , @ColumnsToInclude varchar(8000) = NULL	-- List of columns to be included in the INSERT statement
    , @ColumnsToExclude varchar(8000) = NULL	-- List of columns to be excluded from the INSERT statement

    , @OmitComputedColumns bit = 1		-- When 1 computed columns will not be included in the INSERT statement
    , @OmitIdentityColumn bit = 1		-- Use this parameter to ommit the identity columns
    , @OmitImages bit = 0			-- Use this parameter to generate INSERT statements by omitting the 'image' columns
    , @IncludeTimestamp bit = 0 		-- Specify 1 for this parameter if you want to include the TIMESTAMP/ROWVERSION column's data in the INSERT statement

    , @owner varchar(64) = NULL		-- Use this parameter if you are not the owner of the table
    , @disable_constraints bit = 0		-- When 1 disables foreign key constraints and enables them after the INSERT statements
    , @debug_mode bit = 0			-- If @debug_mode is set to 1 the SQL statements constructed by this procedure will be printed for later examination
)
AS
BEGIN
    /***********************************************************************************************************
    Example 1:	To generate INSERT statements for table 'titles':
        
            EXEC [Util.GenerateInserts] 'titles'

    Example 2: 	To ommit the column list in the INSERT statement: (Column list is included by default)
            IMPORTANT: If you have too many columns, you are advised to ommit column list, as shown below,
            to avoid erroneous results
        
            EXEC [Util.GenerateInserts] 'titles', @IncludeColumnNames = 0

    Example 3:	To generate INSERT statements for 'titlesCopy' table from 'titles' table:

            EXEC [Util.GenerateInserts] 'titles', 'titlesCopy'

    Example 4:	To generate INSERT statements for 'titles' table for only those titles 
            which contain the word 'Computer' in them:
            NOTE: Do not complicate the FROM or WHERE clause here. It's assumed that you are good with T-SQL if you are using this parameter

            EXEC [Util.GenerateInserts] 'titles', @WhereClause = ""title like '%Computer%'""

    Example 5: 	To specify that you want to include TIMESTAMP column's data as well in the INSERT statement:
            (By default TIMESTAMP column's data is not scripted)

            EXEC [Util.GenerateInserts] 'titles', @IncludeTimestamp = 1

    Example 6:	To print the debug information:

            EXEC [Util.GenerateInserts] 'titles', @debug_mode = 1

    Example 7: 	If you are not the owner of the table, use @owner parameter to specify the owner name
            To use this option, you must have SELECT permissions on that table

            EXEC [Util.GenerateInserts] Nickstable, @owner = 'Nick'

    Example 8: 	To generate INSERT statements for the rest of the columns excluding images
            When using this otion, DO NOT set @IncludeColumnNames parameter to 0.

            EXEC [Util.GenerateInserts] imgtable, @OmitImages = 1

    Example 9: 	To generate INSERT statements excluding (ommiting) IDENTITY columns:
            (By default IDENTITY columns are included in the INSERT statement)

            EXEC [Util.GenerateInserts] mytable, @OmitIdentityColumn = 1

    Example 10: 	To generate INSERT statements for the TOP 10 rows in the table:
        
            EXEC [Util.GenerateInserts] mytable, @MaxRows = 10

    Example 11: 	To generate INSERT statements with only those columns you want:
        
            EXEC [Util.GenerateInserts] titles, @ColumnsToInclude = ""'title','title_id','au_id'""

    Example 12: 	To generate INSERT statements by omitting certain columns:
        
            EXEC [Util.GenerateInserts] titles, @ColumnsToExclude = ""'title','title_id','au_id'""

    Example 13:	To avoid checking the foreign key constraints while loading data with INSERT statements:
        
            EXEC [Util.GenerateInserts] titles, @disable_constraints = 1

    Example 14: 	To exclude computed columns from the INSERT statement:
            EXEC [Util.GenerateInserts] MyTable, @OmitComputedColumns = 1

    ***********************************************************************************************************/

    SET NOCOUNT ON

    --Making sure user only uses either @ColumnsToInclude or @ColumnsToExclude
    IF ((@ColumnsToInclude IS NOT NULL) AND (@ColumnsToExclude IS NOT NULL))
    BEGIN
        RAISERROR('Use either @ColumnsToInclude or @ColumnsToExclude. Do not use both the parameters at once',16,1)
        RETURN -1 --Failure. Reason: Both @ColumnsToInclude and @ColumnsToExclude parameters are specified
    END

    --Making sure the @ColumnsToInclude and @ColumnsToExclude parameters are receiving values in proper format
    IF ((@ColumnsToInclude IS NOT NULL) AND (PATINDEX('''%''',@ColumnsToInclude) = 0))
    BEGIN
        RAISERROR('Invalid use of @ColumnsToInclude property',16,1)
        PRINT 'Specify column names surrounded by single quotes and separated by commas'
        PRINT 'Eg: EXEC [Util.GenerateInserts] titles, @ColumnsToInclude = ""''title_id'',''title''""'
        RETURN -1 --Failure. Reason: Invalid use of @ColumnsToInclude property
    END

    IF ((@ColumnsToExclude IS NOT NULL) AND (PATINDEX('''%''',@ColumnsToExclude) = 0))
    BEGIN
        RAISERROR('Invalid use of @ColumnsToExclude property',16,1)
        PRINT 'Specify column names surrounded by single quotes and separated by commas'
        PRINT 'Eg: EXEC [Util.GenerateInserts] titles, @ColumnsToExclude = ""''title_id'',''title''""'
        RETURN -1 --Failure. Reason: Invalid use of @ColumnsToExclude property
    END


    --Checking for the existence of 'user table' or 'view'
    --This procedure is not written to work on system tables
    --To script the data in system tables, just create a view on the system tables and script the view instead

    DECLARE @TableObjectID INT = COALESCE( OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + @SourceTableName), 
                                           OBJECT_ID(QUOTENAME(COALESCE(@owner,USER_NAME())) + '.[' + @SourceTableName + ']') );

    IF @TableObjectID IS NULL
    BEGIN
        RAISERROR('User table or view not found.',16,1)
        PRINT 'You may see this error, if you are not the owner of this table or view. In that case use @owner parameter to specify the owner name.'
        PRINT 'Make sure you have SELECT permission on that table or view.'
        RETURN -1 --Failure. Reason: There is no user table or view with this name
    END

    --Variable declarations
    DECLARE		@Column_ID int = 0, 		
                @Column_List varchar(8000) = '', 
                @Column_Name varchar(128) = '', 
                @Start_Insert varchar(786), 
                @Data_Type varchar(128), 
                @Actual_Values varchar(8000)  = '', --This is the string that will be finally executed to generate INSERT statements
                @IDN varchar(128) = ''              --Will contain the IDENTITY column's name in the table

    IF @owner IS NULL 
        SET @Start_Insert = 'INSERT INTO ' + '[' + RTRIM(COALESCE(@TargetTableName,@SourceTableName)) + ']' 

    ELSE
        SET @Start_Insert = 'INSERT ' + '[' + LTRIM(RTRIM(@owner)) + '].' + '[' + RTRIM(COALESCE(@TargetTableName,@SourceTableName)) + ']';

    --To get the first column's ID
    SELECT	@Column_ID = MIN(ORDINAL_POSITION) 	
    FROM	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
    WHERE 	TABLE_NAME = @SourceTableName AND
    (@owner IS NULL OR TABLE_SCHEMA = @owner)


    --Loop through all the columns of the table, to get the column names and their data types
    WHILE @Column_ID IS NOT NULL
        BEGIN
            SELECT 	@Column_Name = QUOTENAME(COLUMN_NAME), 
            @Data_Type = DATA_TYPE 
            FROM 	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
            WHERE 	ORDINAL_POSITION = @Column_ID AND 
            TABLE_NAME = @SourceTableName AND
            (@owner IS NULL OR TABLE_SCHEMA = @owner)



            IF @ColumnsToInclude IS NOT NULL --Selecting only user specified columns
            BEGIN
                IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@ColumnsToInclude) = 0 
                BEGIN
                    GOTO SKIP_LOOP
                END
            END

            IF @ColumnsToExclude IS NOT NULL --Selecting only user specified columns
            BEGIN
                IF CHARINDEX( '''' + SUBSTRING(@Column_Name,2,LEN(@Column_Name)-2) + '''',@ColumnsToExclude) <> 0 
                BEGIN
                    GOTO SKIP_LOOP
                END
            END

            --Making sure to output SET IDENTITY_INSERT ON/OFF in case the table has an IDENTITY column
            IF (SELECT COLUMNPROPERTY( @TableObjectID, SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2), 'IsIdentity')) = 1 
            BEGIN
                IF @OmitIdentityColumn = 0 --Determing whether to include or exclude the IDENTITY column
                    SET @IDN = @Column_Name
                ELSE
                    GOTO SKIP_LOOP;
            END
        
            --Making sure whether to output computed columns or not
            IF @OmitComputedColumns = 1
            BEGIN
                IF (SELECT COLUMNPROPERTY( @TableObjectID, SUBSTRING(@Column_Name,2,LEN(@Column_Name) - 2),'IsComputed')) = 1 
                    GOTO SKIP_LOOP;
            END
        
            --Tables with columns of IMAGE data type are not supported for obvious reasons
            IF(@Data_Type in ('image'))
                BEGIN
                    IF (@OmitImages = 0)
                        BEGIN
                            RAISERROR('Tables with image columns are not supported.',16,1)
                            PRINT 'Use @OmitImages = 1 parameter to generate INSERTs for the rest of the columns.'
                            PRINT 'DO NOT ommit Column List in the INSERT statements. If you ommit column list using @IncludeColumnNames=0, the generated INSERTs will fail.'
                            RETURN -1 --Failure. Reason: There is a column with image data type
                        END
                    ELSE
                        BEGIN
                        GOTO SKIP_LOOP
                        END
                END

            --Determining the data type of the column and depending on the data type, the VALUES part of
            --the INSERT statement is generated. Care is taken to handle columns with NULL values. Also
            --making sure, not to lose any data from flot, real, money, smallmomey, datetime columns
            SET @Actual_Values = @Actual_Values  +
            CASE 
                WHEN @Data_Type IN ('char','varchar','nchar','nvarchar') 
                    THEN 
                        'COALESCE('''''''' + REPLACE(RTRIM(' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'
                WHEN @Data_Type IN ('datetime','smalldatetime', 'date', 'time', 'datetime2') 
                    THEN 
                        'COALESCE('''''''' + RTRIM(CONVERT(char,' + @Column_Name + ',109))+'''''''',''NULL'')'
                WHEN @Data_Type IN ('uniqueidentifier') 
                    THEN  
                        'COALESCE('''''''' + REPLACE(CONVERT(char(255),RTRIM(' + @Column_Name + ')),'''''''','''''''''''')+'''''''',''NULL'')'
                WHEN @Data_Type IN ('text','ntext') 
                    THEN  
                        'COALESCE('''''''' + REPLACE(CONVERT(char(8000),' + @Column_Name + '),'''''''','''''''''''')+'''''''',''NULL'')'					
                WHEN @Data_Type IN ('binary','varbinary') 
                    THEN  
                        'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'  
                WHEN @Data_Type IN ('timestamp','rowversion') 
                    THEN  
                        CASE 
                            WHEN @IncludeTimestamp = 0 
                                THEN 
                                    '''DEFAULT''' 
                                ELSE 
                                    'COALESCE(RTRIM(CONVERT(char,' + 'CONVERT(int,' + @Column_Name + '))),''NULL'')'  
                        END
                WHEN @Data_Type IN ('float','real','money','smallmoney')
                    THEN
                        'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ',2)' + ')),''NULL'')' 
                ELSE 
                    'COALESCE(LTRIM(RTRIM(' + 'CONVERT(char, ' +  @Column_Name  + ')' + ')),''NULL'')' 
            END   + '+' +  ''',''' + ' + '
        
            --Generating the column list for the INSERT statement
            SET @Column_List = @Column_List +  @Column_Name + ','	

            SKIP_LOOP: --The label used in GOTO

            SELECT 	@Column_ID = MIN(ORDINAL_POSITION) 
            FROM 	INFORMATION_SCHEMA.COLUMNS (NOLOCK) 
            WHERE 	TABLE_NAME = @SourceTableName AND 
            ORDINAL_POSITION > @Column_ID AND
            (@owner IS NULL OR TABLE_SCHEMA = @owner)


        --Loop ends here!
        END

    --To get rid of the extra characters that got concatenated during the last run through the loop
    SET @Column_List = LEFT(@Column_List,len(@Column_List) - 1)
    SET @Actual_Values = LEFT(@Actual_Values,len(@Actual_Values) - 6)

    IF LTRIM(@Column_List) = '' 
        BEGIN
            RAISERROR('No columns to select. There should at least be one column to generate the output',16,1)
            RETURN -1 --Failure. Reason: Looks like all the columns are ommitted using the @ColumnsToExclude parameter
        END

    --Forming the final string that will be executed, to output the INSERT statements
    IF (@IncludeColumnNames <> 0)
        BEGIN
            SET @Actual_Values = 
                'SELECT ' +  
                CASE WHEN @MaxRows IS NULL OR @MaxRows < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@MaxRows)) + ' ' END + 
                '''' + RTRIM(@Start_Insert) + 
                ' ''+' + '''(' + RTRIM(@Column_List) +  '''+' + ''')''' + 
                ' +''VALUES(''+ ' +  @Actual_Values  + '+'')''' + ' ' + 
                ' FROM ' + CASE WHEN @owner IS NULL THEN '' ELSE '[' + LTRIM(RTRIM(@owner)) + '].' END + '[' + rtrim(@SourceTableName) + ']' + '(NOLOCK)'+
                CASE WHEN @WhereClause IS NULL THEN '' ELSE ' WHERE '+@WhereClause END
        END
    ELSE IF (@IncludeColumnNames = 0)
        BEGIN
            SET @Actual_Values = 
                'SELECT ' + 
                CASE WHEN @MaxRows IS NULL OR @MaxRows < 0 THEN '' ELSE ' TOP ' + LTRIM(STR(@MaxRows)) + ' ' END + 
                '''' + RTRIM(@Start_Insert) + 
                ' '' +''VALUES(''+ ' +  @Actual_Values + '+'')''' + ' ' + 
                ' FROM ' + CASE WHEN @owner IS NULL THEN '' ELSE '[' + LTRIM(RTRIM(@owner)) + '].' END + '[' + rtrim(@SourceTableName) + ']' + '(NOLOCK)'+
                CASE WHEN @WhereClause IS NULL THEN '' ELSE ' WHERE '+@WhereClause END
        END	

    --Determining whether to ouput any debug information
    IF @debug_mode =1
        BEGIN
            PRINT '/*****START OF DEBUG INFORMATION*****'
            PRINT 'Beginning of the INSERT statement:'
            PRINT @Start_Insert
            PRINT ''
            PRINT 'The column list:'
            PRINT @Column_List
            PRINT ''
            PRINT 'The SELECT statement executed to generate the INSERTs'
            PRINT @Actual_Values
            PRINT ''
            PRINT '*****END OF DEBUG INFORMATION*****/'
            PRINT ''
        END
        
    PRINT '--INSERTs generated by ''[Util.GenerateInserts]'' stored procedure written by Vyas'
    PRINT '--Build number: 22'
    PRINT '--Problems/Suggestions? Contact Vyas @ vyaskn@hotmail.com'
    PRINT '--http://vyaskn.tripod.com'
    PRINT ''
    PRINT 'SET NOCOUNT ON'
    PRINT ''


    --Determining whether to print IDENTITY_INSERT or not
    IF (@IDN <> '')
        BEGIN
            PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + QUOTENAME(@SourceTableName) + ' ON'
            PRINT 'GO'
            PRINT ''
        END


    IF @disable_constraints = 1 AND (@TableObjectID IS NOT NULL)
        BEGIN
            IF @owner IS NULL
                BEGIN
                    SELECT 	'ALTER TABLE ' + QUOTENAME(COALESCE(@TargetTableName, @SourceTableName)) + ' NOCHECK CONSTRAINT ALL' AS '--Code to disable constraints temporarily'
                END
            ELSE
                BEGIN
                    SELECT 	'ALTER TABLE ' + QUOTENAME(@owner) + '.' + QUOTENAME(COALESCE(@TargetTableName, @SourceTableName)) + ' NOCHECK CONSTRAINT ALL' AS '--Code to disable constraints temporarily'
                END

            PRINT 'GO'
        END

    PRINT ''
    PRINT 'PRINT ''Inserting values into ' + '[' + RTRIM(COALESCE(@TargetTableName,@SourceTableName)) + ']' + ''''


    --All the hard work pays off here!!! You'll get your INSERT statements, when the next line executes!
    PRINT ''
    PRINT 'Actual SQL:'
    PRINT @Actual_Values;
    PRINT ''

    EXEC (@Actual_Values)

    PRINT 'PRINT ''Done'''
    PRINT ''


    IF @disable_constraints = 1 AND (@TableObjectID IS NOT NULL)
        BEGIN
            IF @owner IS NULL
                BEGIN
                    SELECT 	'ALTER TABLE ' + QUOTENAME(COALESCE(@TargetTableName, @SourceTableName)) + ' CHECK CONSTRAINT ALL'  AS '--Code to enable the previously disabled constraints'
                END
            ELSE
                BEGIN
                    SELECT 	'ALTER TABLE ' + QUOTENAME(@owner) + '.' + QUOTENAME(COALESCE(@TargetTableName, @SourceTableName)) + ' CHECK CONSTRAINT ALL' AS '--Code to enable the previously disabled constraints'
                END

            PRINT 'GO'
        END

    PRINT ''
    IF (@IDN <> '')
        BEGIN
            PRINT 'SET IDENTITY_INSERT ' + QUOTENAME(COALESCE(@owner,USER_NAME())) + '.' + QUOTENAME(@SourceTableName) + ' OFF'
            PRINT 'GO'
        END

    PRINT 'SET NOCOUNT OFF'


    SET NOCOUNT OFF
    RETURN 0 --Success. We are done!
END

            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [Util.GenerateInserts]
            ");
        }
    }
}
