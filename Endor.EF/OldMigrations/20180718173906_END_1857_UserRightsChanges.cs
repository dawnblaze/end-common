using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180718173906_END_1857_UserRightsChanges")]
    public partial class END_1857_UserRightsChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [User.Link]");

            migrationBuilder.DropForeignKey(
                name: "FK_User.Link_enum.User.Role.Type",
                table: "User.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_User.Link_Security.Right.Collection",
                table: "User.Link");

            migrationBuilder.DropTable(
                name: "Security.Right.Group.Link");

            migrationBuilder.DropTable(
                name: "Security.Right.Link");

            migrationBuilder.DropTable(
                name: "Security.Role.Link");

            migrationBuilder.DropTable(
                name: "enum.User.Role.Type");

            migrationBuilder.DropTable(
                name: "Security.Right.Group");

            migrationBuilder.DropPrimaryKey(
                name: "PK_User.Link",
                table: "User.Link");

            migrationBuilder.DropColumn(
                name: "RoleType",
                table: "User.Link");

            migrationBuilder.DropColumn(
                name: "RightGroupID",
                table: "User.Link");

            migrationBuilder.AddColumn<short?>(
                name: "RightsGroupListID",
                table: "User.Link",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "User.Link",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<short>(
                name: "ID",
                table: "User.Link",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<Guid>(
                name: "TempUserID",
                table: "User.Link",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "UserAccessType",
                table: "User.Link",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<int>(
                name: "ClassTypeID",
                table: "User.Link",
                nullable: false,
                computedColumnSql: "((1012))");

            migrationBuilder.AddPrimaryKey(
                name: "PK_User.Link",
                table: "User.Link",
                columns: new[] { "BID", "ID" });

            migrationBuilder.CreateTable(
                name: "enum.User.Access.Type",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.User.Access.Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.User.Right",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.User.Right", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Rights.Group.List",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1100))"),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    RightsArray = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rights.Group.List", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Rights.Group",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((1101))"),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false, defaultValueSql: "(0)"),
                    Module = table.Column<short>(nullable: true),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    UserAccessType = table.Column<byte>(type: "tinyint SPARSE", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rights.Group", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Rights.Group_enum.User.Access.Type",
                        column: x => x.UserAccessType,
                        principalTable: "enum.User.Access.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rights.Group.ChildGroupLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ParentGroupID = table.Column<short>(nullable: false),
                    ChildGroupID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rights.Group.ChildGroupLink", x => new { x.BID, x.ParentGroupID, x.ChildGroupID });
                    table.ForeignKey(
                        name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Child",
                        columns: x => new { x.BID, x.ChildGroupID },
                        principalTable: "Rights.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Parent",
                        columns: x => new { x.BID, x.ParentGroupID },
                        principalTable: "Rights.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rights.Group.List.RightsGroupLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    RightsGroupListID = table.Column<short>(nullable: false),
                    RightsGroupID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rights.Group.List.RightsGroupLink", x => new { x.BID, x.RightsGroupListID, x.RightsGroupID });
                    table.ForeignKey(
                        name: "FK_Rights.Group.List.RightsGroupLink_RightsGroup",
                        columns: x => new { x.BID, x.RightsGroupID },
                        principalTable: "Rights.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rights.Group.List.RightsGroupLink_RightsGroupList",
                        columns: x => new { x.BID, x.RightsGroupListID },
                        principalTable: "Rights.Group.List",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rights.Group.RightLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    RightsGroupID = table.Column<short>(nullable: false),
                    RightID = table.Column<byte>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rights.Group.RightLink", x => new { x.BID, x.RightsGroupID, x.RightID });
                    table.ForeignKey(
                        name: "FK_Rights.Group.RightLink_enum.User.Right",
                        column: x => x.RightID,
                        principalTable: "enum.User.Right",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Rights.Group.RightLink_Rights.Group",
                        columns: x => new { x.BID, x.RightsGroupID },
                        principalTable: "Rights.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User.Link_UserAccessType",
                table: "User.Link",
                column: "UserAccessType");

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group_UserAccessType",
                table: "Rights.Group",
                column: "UserAccessType");

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group.ChildGroupLink_Child",
                table: "Rights.Group.ChildGroupLink",
                columns: new[] { "BID", "ChildGroupID", "ParentGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group.List.RightsGroupLink_RightsGroupID",
                table: "Rights.Group.List.RightsGroupLink",
                columns: new[] { "BID", "RightsGroupID", "RightsGroupListID" });

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group.RightLink_RightID",
                table: "Rights.Group.RightLink",
                column: "RightID");

            migrationBuilder.CreateIndex(
                name: "IX_Rights.Group.RightLink_Right",
                table: "Rights.Group.RightLink",
                columns: new[] { "BID", "RightID", "RightsGroupID" });

            migrationBuilder.AddForeignKey(
                name: "FK_User.Link_enum.User.Access.Type",
                table: "User.Link",
                column: "UserAccessType",
                principalTable: "enum.User.Access.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User.Link_Rights.Group.List",
                table: "User.Link",
                columns: new[] { "BID", "RightsGroupListID" },
                principalTable: "Rights.Group.List",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User.Link_enum.User.Access.Type",
                table: "User.Link");

            migrationBuilder.DropForeignKey(
                name: "FK_User.Link_Rights.Group.List",
                table: "User.Link");

            migrationBuilder.DropTable(
                name: "Rights.Group.ChildGroupLink");

            migrationBuilder.DropTable(
                name: "Rights.Group.List.RightsGroupLink");

            migrationBuilder.DropTable(
                name: "Rights.Group.RightLink");

            migrationBuilder.DropTable(
                name: "Rights.Group.List");

            migrationBuilder.DropTable(
                name: "enum.User.Right");

            migrationBuilder.DropTable(
                name: "Rights.Group");

            migrationBuilder.DropTable(
                name: "enum.User.Access.Type");

            migrationBuilder.DropPrimaryKey(
                name: "PK_User.Link",
                table: "User.Link");

            migrationBuilder.DropIndex(
                name: "IX_User.Link_UserAccessType",
                table: "User.Link");

            migrationBuilder.DropColumn(
                name: "ID",
                table: "User.Link");

            migrationBuilder.DropColumn(
                name: "ClassTypeID",
                table: "User.Link");

            migrationBuilder.DropColumn(
                name: "TempUserID",
                table: "User.Link");

            migrationBuilder.DropColumn(
                name: "UserAccessType",
                table: "User.Link");

            migrationBuilder.DropColumn(
                name: "RightsGroupListID",
                table: "User.Link");

            migrationBuilder.AddColumn<short>(
                name: "RightGroupID",
                table: "User.Link");

            migrationBuilder.AlterColumn<int>(
                name: "UserID",
                table: "User.Link",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "RoleType",
                table: "User.Link",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_User.Link",
                table: "User.Link",
                columns: new[] { "BID", "UserID" });

            migrationBuilder.CreateTable(
                name: "enum.User.Role.Type",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.User.Role.Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Security.Right.Group",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<short>(nullable: true, computedColumnSql: "(CONVERT([smallint],(1101)))"),
                    IsSystem = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security.Right.Group", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Security.Right.Group.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ParentGroupID = table.Column<short>(nullable: false),
                    ChildGroupID = table.Column<short>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security.Right.Group.Link", x => new { x.BID, x.ParentGroupID, x.ChildGroupID });
                    table.ForeignKey(
                        name: "FK_Security.Right.Group.Link_Security.Right.Group1",
                        columns: x => new { x.BID, x.ChildGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Security.Right.Group.Link_Security.Right.Group",
                        columns: x => new { x.BID, x.ParentGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Security.Right.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    RightGroupID = table.Column<short>(nullable: false),
                    RightID = table.Column<short>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security.Right.Link", x => new { x.BID, x.RightGroupID, x.RightID });
                    table.ForeignKey(
                        name: "FK_Security.Right.Link_Security.Right.Collection",
                        columns: x => new { x.BID, x.RightGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Security.Role.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    RoleType = table.Column<byte>(nullable: false),
                    RightGroupID = table.Column<short>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Security.Role.Link", x => new { x.BID, x.RoleType, x.RightGroupID });
                    table.ForeignKey(
                        name: "FK_Security.Role.Link_enum.User.Role.Type",
                        column: x => x.RoleType,
                        principalTable: "enum.User.Role.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Security.Role.Link_Security.Right.Group",
                        columns: x => new { x.BID, x.RightGroupID },
                        principalTable: "Security.Right.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Security.Right.Collection_Name",
                table: "Security.Right.Group",
                columns: new[] { "BID", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_Security.Right.Group.Link_Child",
                table: "Security.Right.Group.Link",
                columns: new[] { "BID", "ChildGroupID", "ParentGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Security.Right.Link_Right",
                table: "Security.Right.Link",
                columns: new[] { "BID", "RightID", "RightGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Security.Role.Link_RoleType",
                table: "Security.Role.Link",
                column: "RoleType");

            migrationBuilder.CreateIndex(
                name: "IX_Security.Role.Link_Group",
                table: "Security.Role.Link",
                columns: new[] { "BID", "RightGroupID" });

            migrationBuilder.AddForeignKey(
                name: "FK_User.Link_enum.User.Role.Type",
                table: "User.Link",
                column: "RoleType",
                principalTable: "enum.User.Role.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User.Link_Security.Right.Collection",
                table: "User.Link",
                columns: new[] { "BID", "RightGroupID" },
                principalTable: "Security.Right.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}

