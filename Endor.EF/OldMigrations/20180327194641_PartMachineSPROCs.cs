using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180327194641_PartMachineSPROCs")]
    public partial class PartMachineSPROCs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
GO
/****** Object:  StoredProcedure [dbo].[Part.Machine.Action.SetActive]    Script Date: 3/28/2018 3:49:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Name: [Part.Machine.Data.Action.SetActive]
--
-- Description: This procedure sets the Machine Data to active or inactive
--
-- Sample Use:   EXEC dbo.[Part.Machine.Action.SetActive] @BID=1, @ID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Machine.Action.SetActive]
-- DECLARE 
          @BID				TINYINT
        , @ID				SMALLINT

        , @IsActive			BIT     = 1

        , @Result			INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Machine Data specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Machine.Data] WHERE BID = @BID and ID = @ID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Machine Data Specified. MachineDataID='+CONVERT(VARCHAR(12),@ID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Part.Machine.Data] L
    WHERE BID = @BID and ID = @ID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
GO
/****** Object:  StoredProcedure [dbo].[Part.Machine.Action.CanDelete]    Script Date: 3/28/2018 3:41:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
========================================================
    Name: [Part.Machine.Action.CanDelete]

    Description: This procedure checks if the Machine.Data is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Machine.Action.CanDelete] @BID=1, @ID=2, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Machine.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
    Machine Object records can be deleted if all of the following is true:
	(future) It is linked to no Machine Categories.
    (future) It is not in use in any Product Template.
    (future) Is it not in use in any Order or Estimate.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Machine.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Machine Specified Does not Exist. MachineID=', @ID)

	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]
    ELSE
        SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Machine.Action.CanDelete]");
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Machine.Action.SetActive]");
        }
    }
}

