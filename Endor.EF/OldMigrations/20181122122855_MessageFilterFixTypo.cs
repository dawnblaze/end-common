using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class MessageFilterFixTypo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                    UPDATE [dbo].[List.Filter]
                       SET [Criteria] = '<ArrayOfListFilterItem>
                      <ListFilterItem>
                        <SearchValue>false</SearchValue>
                        <Field>IsDeleted</Field>
                        <IsHidden>true</IsHidden>
                        <IsSystem>true</IsSystem>
                      </ListFilterItem>
                      <ListFilterItem>
                        <SearchValue>false</SearchValue>
                        <Field>InSentFolder</Field>
                        <IsHidden>true</IsHidden>
                        <IsSystem>true</IsSystem>
                      </ListFilterItem>
                      <ListFilterItem>
                        <SearchValue>false</SearchValue>
                        <Field>IsRead</Field>
                        <IsHidden>true</IsHidden>
                        <IsSystem>true</IsSystem>
                      </ListFilterItem>
                    </ArrayOfListFilterItem>'
                    WHERE TargetClassTypeID = 14109 AND Name = 'Inbox';

                    UPDATE [dbo].[List.Filter]
                       SET [Criteria] = '<ArrayOfListFilterItem>
                      <ListFilterItem>
                        <SearchValue>false</SearchValue>
                        <Field>IsDeleted</Field>
                        <IsHidden>true</IsHidden>
                        <IsSystem>true</IsSystem>
                      </ListFilterItem>
                      <ListFilterItem>
                        <SearchValue>true</SearchValue>
                        <Field>InSentFolder</Field>
                        <IsHidden>true</IsHidden>
                        <IsSystem>true</IsSystem>
                      </ListFilterItem>
                    </ArrayOfListFilterItem>'
                    WHERE TargetClassTypeID = 14109 AND Name = 'Sent';
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[List.Filter] TargetClassTypeID = 14109 AND Name = 'Inbox';

                DELETE FROM [dbo].[List.Filter] TargetClassTypeID = 14109 AND Name = 'Sent';
            ");
        }
    }
}
