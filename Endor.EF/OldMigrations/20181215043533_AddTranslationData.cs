using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddTranslationData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "System.Language.Type",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Code = table.Column<string>(type: "VARCHAR(10)", maxLength: 10, nullable: false),
                    DisplayName = table.Column<string>(maxLength: 100, nullable: false),
                    GoogleCode = table.Column<string>(type: "VARCHAR(10)", maxLength: 10, nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Language.Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "System.Translation.Definition",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AltMeaning = table.Column<string>(maxLength: 2147483647, nullable: true),
                    IsVerified = table.Column<bool>(nullable: false),
                    LanguageTypeId = table.Column<byte>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    SourceText = table.Column<string>(maxLength: 2147483647, nullable: false),
                    TranslatedText = table.Column<string>(type: "NVARCHAR(MAX)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Translation.Definition", x => x.ID);
                    table.ForeignKey(
                        name: "FK_System.Language.Definition.LanguageType",
                        column: x => x.LanguageTypeId,
                        principalTable: "System.Language.Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_System.Language.Verified",
                table: "System.Translation.Definition",
                columns: new[] { "LanguageTypeId", "IsVerified" });

            migrationBuilder.Sql(@"
alter table [System.Translation.Definition]
alter column AltMeaning [varchar](max) SPARSE
;");

            migrationBuilder.Sql(@"
create NONCLUSTERED index [IX_System.Language.SourceText] on [System.Translation.Definition] (LanguageTypeId)
include (SourceText, AltMeaning)
;");

            migrationBuilder.Sql(@"
INSERT [System.Language.Type] ([ID], [Code], [GoogleCode], [Name], [DisplayName]) VALUES
    (0, N'en', N'en', N'English', N'English')
    , (1, N'en-uk', N'en', N'English (United Kingdom)', N'English (United Kingdom)')
    , (2, N'en-au', N'en', N'English (Australia)', N'English (Australia)')
    , (3, N'fr', N'fr', N'French', N'Français')
    , (4, N'es', N'es', N'Spanish', N'Español')
;");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "System.Translation.Definition");

            migrationBuilder.DropTable(
                name: "System.Language.Type");
        }
    }
}
