using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180816202159_END-2250_FixingClassTypeIDs")]
    public partial class END2250_FixingClassTypeIDs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Employee.Role",
                nullable: false,
                computedColumnSql: "((5004))",
                oldClrType: typeof(int),
                oldComputedColumnSql: "5004");

            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Domain.Data",
                nullable: false,
                computedColumnSql: "((1021))",
                oldClrType: typeof(int),
                oldComputedColumnSql: "1004");

            migrationBuilder.Sql
                (@"
                    --Wrong ClassTypeID for Custom Field Definition Removed
                    DELETE FROM [enum.ClassType] WHERE ID = 2101

                    INSERT INTO [enum.ClassType]
                      (ID, [Name], TableName)
                      VALUES
                      --DomainData
                      (1021 , 'Domain', 'Domain.Data'),
                      --Custom Field Definition Corrected
                      (15025, 'CustomFieldDefinition', 'CustomField.Definition' ),
                      --Employee Role
                      (5004 , 'EmployeeRole', 'Employee.Role')
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Employee.Role",
                nullable: false,
                computedColumnSql: "5004",
                oldClrType: typeof(int),
                oldComputedColumnSql: "((5004))");

            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Domain.Data",
                nullable: false,
                computedColumnSql: "1004",
                oldClrType: typeof(int),
                oldComputedColumnSql: "((1021))");

            migrationBuilder.Sql
                (@"
                    --Remove DomainData
                    DELETE FROM [enum.ClassType] WHERE ID = 1021
                    --Remove Employee Role
                    DELETE FROM [enum.ClassType] WHERE ID = 5004
                    --Remove Correct ClassTypeID for Custom Field Definition
                    DELETE FROM [enum.ClassType] WHERE ID = 15025

                    --Custom Field Definition Un-Corrected
                    INSERT INTO [enum.ClassType]
                      (ID, [Name], TableName)
                      VALUES  
                      (2101, 'CustomFieldDefinition', 'CustomField.Definition' )
                ");
        }
    }
}

