using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180525200050_Add_InvoiceText_material_and_labor_simplelist")]
    public partial class Add_InvoiceText_material_and_labor_simplelist : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Part.Material.SimpleList]
                                    AS
                                SELECT	[BID]
	                                  , [ID]
                                      , [ClassTypeID]
                                      , [Name] as DisplayName
                                      , [IsActive]
                                      , [HasImage]
                                      , CONVERT(BIT, 0) AS [IsDefault]
					                  , InvoiceText
                                FROM [Part.Material.Data];
                GO
                ALTER VIEW [dbo].[Part.Labor.SimpleList]
                                    AS
                                SELECT[BID]
                                    , [ID]
                                    , [ClassTypeID]
                                    , [Name] as DisplayName
                                    , [IsActive]
                                    , [HasImage]
                                    , CONVERT(BIT, 0) AS[IsDefault]
					                ,	InvoiceText
                                FROM [dbo].[Part.Labor.Data];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Part.Material.SimpleList]
                                    AS
                                SELECT	[BID]
	                                  , [ID]
                                      , [ClassTypeID]
                                      , [Name] as DisplayName
                                      , [IsActive]
                                      , [HasImage]
                                      , CONVERT(BIT, 0) AS [IsDefault]
                                FROM [Part.Material.Data];
                GO
                ALTER VIEW [dbo].[Part.Labor.SimpleList]
                                    AS
                                SELECT[BID]
                                    , [ID]
                                    , [ClassTypeID]
                                    , [Name] as DisplayName
                                    , [IsActive]
                                    , [HasImage]
                                    , CONVERT(BIT, 0) AS[IsDefault]
                                FROM [dbo].[Part.Labor.Data];
                GO
            ");
        }
    }
}

