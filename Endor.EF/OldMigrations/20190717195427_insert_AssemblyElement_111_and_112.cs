using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class insert_AssemblyElement_111_and_112 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT [enum.Part.Subassembly.ElementType] ([ID], [Name])
                VALUES (111, N'Detail Form Button'),(112, N'Layout Visualizer');
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [enum.Part.Subassembly.ElementType]
                WHERE [ID] in (111,112);
            ");
        }
    }
}
