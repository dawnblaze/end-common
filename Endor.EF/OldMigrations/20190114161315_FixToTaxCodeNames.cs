using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixToTaxCodeNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Accounting.Tax.Code] SET [Name] = 'Finance Charge'
WHERE [Name] = 'FinanceCharge';

UPDATE [Accounting.Tax.Code] SET [Name] = 'Forced Exempt'
WHERE [Name] = 'ForcedExempt';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Accounting.Tax.Code] SET [Name] = 'FinanceCharge'
WHERE [Name] = 'Finance Charge';

UPDATE [Accounting.Tax.Code] SET [Name] = 'ForcedExempt'
WHERE [Name] = 'Forced Exempt';
");
        }
    }
}
