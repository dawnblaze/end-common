using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180920155741_UpdateListFilter_IsActive")]
    public partial class UpdateListFilter_IsActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [List.Filter]
WHERE [Name] = 'Active'
	AND [TargetClassTypeID] IN (1005,5000,5020,1902,15025);

UPDATE [List.Filter]
SET [Name] = 'All', [Criteria] = NULL
WHERE [Name] = 'Active'
	AND [TargetClassTypeID] IN (11101,11105,11102,2012,2011,12000,12002,12022,12032,12020,12030,2000,10000,10023,5101,1022,14000,14100,1021);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [List.Filter] (BID,ID,CreatedDate,ModifiedDT,IsActive,Name,TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,Hint,IsDefault,SortIndex) VALUES
(1,1001,'2018-08-16','2018-08-16 12:44:37.05',1,'Active',1005,NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,0);

INSERT INTO [List.Filter] (BID,ID,CreatedDate,ModifiedDT,IsActive,Name,TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,Hint,IsDefault,SortIndex) VALUES
(1,1007,'2018-08-16','2018-08-16 12:44:37.05',1,'Active',5000,NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,0);

INSERT INTO [List.Filter] (BID,ID,CreatedDate,ModifiedDT,IsActive,Name,TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,Hint,IsDefault,SortIndex) VALUES
(1,1031,'2018-08-16','2018-08-16 12:44:37.05',1,'Active',5020,NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,0);

INSERT INTO [List.Filter] (BID,ID,CreatedDate,ModifiedDT,IsActive,Name,TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,Hint,IsDefault,SortIndex) VALUES
(1,1033,'2018-08-16','2018-08-16 12:44:37.05',1,'Active',1902,NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,0);

INSERT INTO [List.Filter] (BID,ID,CreatedDate,ModifiedDT,IsActive,Name,TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,Hint,IsDefault,SortIndex) VALUES
(1,1035,'2018-08-16','2018-08-16 12:44:37.05',1,'Active',15025,NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,0);

UPDATE [List.Filter]
SET [Name] = 'Active', [Criteria] = '<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>'
WHERE [Name] = 'All'
	AND [TargetClassTypeID] IN (11101,11105,11102,2012,2011,12000,12002,12022,12032,12020,12030,2000,10000,10023,5101,1022,14000,14100,1021);
");
        }
    }
}

