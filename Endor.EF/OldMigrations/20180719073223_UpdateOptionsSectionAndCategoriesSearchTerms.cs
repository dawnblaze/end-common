using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180719073223_UpdateOptionsSectionAndCategoriesSearchTerms")]
    public partial class UpdateOptionsSectionAndCategoriesSearchTerms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ---------------------------------------------------------
-- 2018-07-18 Add Search Terms and update Names Categories
-- 
-- ---------------------------------------------------------
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Origins', [SectionID] = 600, [Description] = 'Origins', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Origin Origination Classification Marketing Tag' WHERE ID = 7;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Industries', [SectionID] = 600, [Description] = 'Industries', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Industry Industries Classification Marketing Tag' WHERE ID = 8;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Chart of Accounts', [SectionID] = 300, [Description] = 'Chart of Accounts', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Chart of Accounts Accounting Income Asset Liability Equity COGS Cost of Goods Sold Expense GL Account' WHERE ID = 9;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Payment Types', [SectionID] = 300, [Description] = 'Payment Types', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Payment Types Cash Check Credit Card Visa Mastercard AmEx American Express Discover Money ' WHERE ID = 11;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Payment Terms', [SectionID] = 300, [Description] = 'Payment Terms', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Payment Terms Net Days Aging' WHERE ID = 12;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Order Options', [SectionID] = 600, [Description] = 'Order Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Order Options' WHERE ID = 13;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Estimate Options', [SectionID] = 600, [Description] = 'Estimate Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Estimate Options' WHERE ID = 14;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Tax Groups', [SectionID] = 300, [Description] = 'Tax Groups', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Tax Group Taxes Sales Rate Government State City County Federal VAT' WHERE ID = 21;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Tax Items', [SectionID] = 300, [Description] = 'Tax Items', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Tax Items Taxes Sales Rate Government State City County Federal VAT' WHERE ID = 22;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Tax Exempt Reasons', [SectionID] = 210, [Description] = 'Tax Exempt Reasons', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Tax Exempt Reasons Non Taxable NonTaxable Exemption List' WHERE ID = 23;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'User Authentication', [SectionID] = 100, [Description] = 'User Authentication Settings', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'login user sign in authentication Google Microsoft Office 365 oauth' WHERE ID = 101;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Domains', [SectionID] = 100, [Description] = 'Domain Settings', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'domain url web address https http dns  email internet' WHERE ID = 102;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Localization', [SectionID] = 100, [Description] = 'Localization Settings', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'region currency date time datetime localization international country' WHERE ID = 103;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Employee Roles', [SectionID] = 200, [Description] = 'Employee Roles', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Employee Roles Salesperson Designer Job Function' WHERE ID = 201;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Custom Field Layouts', [SectionID] = 200, [Description] = 'Custom Field Layouts', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Custom Field Layouts User Defined' WHERE ID = 203;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Payment Voided Reasons', [SectionID] = 210, [Description] = 'Payment Voided Reasons', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Payment Voided Reasons Cancelled NSF return List' WHERE ID = 211;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Credit Given Reasons', [SectionID] = 210, [Description] = 'Credit Given Reasons', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Credit Given Reasons List Customer Credit Refund Rebate' WHERE ID = 212;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Order Edited Reasons', [SectionID] = 210, [Description] = 'Order Edited Reasons', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Order Edited Reasons List Change' WHERE ID = 213;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Order Voided Reasons', [SectionID] = 210, [Description] = 'Order Voided Reasons', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Order Voided Reasons List Void Cancel' WHERE ID = 214;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Estimate Edited Reasons', [SectionID] = 210, [Description] = 'Estimate Edited Reasons', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Estimate Edited Reasons List Change' WHERE ID = 215;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Estimate Cancelled Reasons', [SectionID] = 210, [Description] = 'Estimate Cancelled Reasons', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Estimate Cancelled Reasons List Void Cancel' WHERE ID = 216;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Accounting Options', [SectionID] = 300, [Description] = 'Accounting Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'fiscal year number code accounting export quickbooks' WHERE ID = 301;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Royalty Options', [SectionID] = 300, [Description] = 'Royalty Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Royalty Franchise Fee' WHERE ID = 302;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Finance Charges', [SectionID] = 300, [Description] = 'Finance Charges', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'interest finance charge fees late accounting ' WHERE ID = 303;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Commission Plans', [SectionID] = 600, [Description] = 'Commission Plans', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Sales Commission Plans Salesperson' WHERE ID = 601;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Company Options', [SectionID] = 600, [Description] = 'Company Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Sales Company Options' WHERE ID = 602;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Order Credit Check', [SectionID] = 600, [Description] = 'Order Credit Check', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Sales Order Credit Check Rules Balance Restrictions' WHERE ID = 603;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Quick Checkout', [SectionID] = 600, [Description] = 'Quick Checkout', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Sales Quick Checkout Walkin Walk-In Walk In Resale' WHERE ID = 604;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'API Access', [SectionID] = 700, [Description] = 'API Access', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'API Access REST HTTP TCP JSON XML' WHERE ID = 701;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Online Credit Card', [SectionID] = 700, [Description] = 'Online Credit Card', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration Online Credit Card Visa AmEx American Express Mastercard Discover CapitalQ CMS Slyce Payment Gateway CardConnect Portal Integration' WHERE ID = 702;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Online Tax', [SectionID] = 700, [Description] = 'Online Tax', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Online Tax Avalara TaxJar Sales Rate' WHERE ID = 703;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Shipping', [SectionID] = 700, [Description] = 'Shipping', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration Shipping XPS FedEx UPS WorldShip Ship Manager' WHERE ID = 704;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Pressaro', [SectionID] = 700, [Description] = 'Pressaro', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration Pressaro' WHERE ID = 705;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Zapier', [SectionID] = 700, [Description] = 'Zapier', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration Zapier' WHERE ID = 706;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'T-Flow', [SectionID] = 700, [Description] = 'T-Flow', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration T-Flow Tucanna Artwork Approval' WHERE ID = 707;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Online Packing', [SectionID] = 700, [Description] = 'Online Packing', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration Online Packing' WHERE ID = 708;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Slack', [SectionID] = 700, [Description] = 'Slack', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration Slack Chat Communication Channel' WHERE ID = 709;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Document Management Options', [SectionID] = 710, [Description] = 'Document Management Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration Document Management Files Folders Storage Assets' WHERE ID = 711;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Default Files and Folders', [SectionID] = 710, [Description] = 'Default Files and Folders', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration Default Files and Folders Storage Assets' WHERE ID = 712;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'QuickBooks Sync', [SectionID] = 730, [Description] = 'QuickBooks Sync', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Integration QuickBooks Sync Accounting Export GL General Ledger' WHERE ID = 731;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Xero Sync', [SectionID] = 730, [Description] = 'Xero Sync', [OptionLevels] = 2, [IsHidden] = 1, [SearchTerms] = 'Integration Xero Sync Accounting Export GL General Ledger Zero' WHERE ID = 732;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Custom Field Definitions', [SectionID] = 200, [Description] = 'Custom Field Definitions', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Custom Field Definitions User Defined Input Entry' WHERE ID = 15026;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'System Status', [SectionID] = 15000, [Description] = 'System Status', [OptionLevels] = 2, [IsHidden] = 1, [SearchTerms] = '' WHERE ID = 15101;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'System Authorization', [SectionID] = 15000, [Description] = 'System Authorization', [OptionLevels] = 2, [IsHidden] = 1, [SearchTerms] = '' WHERE ID = 15102;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'System Locks', [SectionID] = 15000, [Description] = 'System Locks', [OptionLevels] = 2, [IsHidden] = 1, [SearchTerms] = '' WHERE ID = 15103;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Operations', [SectionID] = 2200, [Description] = 'Operations', [OptionLevels] = 4, [IsHidden] = 0, [SearchTerms] = 'Operations Hours Time Closing Opening Weekend' WHERE ID = 5;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Ad Hoc', [SectionID] = 10000, [Description] = 'Ad Hoc', [OptionLevels] = 255, [IsHidden] = 1, [SearchTerms] = '' WHERE ID = 10100;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Artwork Options', [SectionID] = 400, [Description] = 'Artwork Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Production Artwork Art Approval Design Designer' WHERE ID = 401;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Destination Options', [SectionID] = 400, [Description] = 'Destination Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Production Destination Shipping Pickup Delivery' WHERE ID = 402;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Line Item Statuses', [SectionID] = 400, [Description] = 'Line Item Statuses', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Production Line Item Status Station Stage Workstation Center' WHERE ID = 403;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Line Item Substatuses', [SectionID] = 400, [Description] = 'Line Item Substatuses', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Production Line Item Substatuses Status' WHERE ID = 404;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Time Clock Options', [SectionID] = 400, [Description] = 'Time Clock Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Production Time Clock In Out On Off' WHERE ID = 405;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Time Clock Activities', [SectionID] = 400, [Description] = 'Time Clock Activities', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Production Time Clock Activities' WHERE ID = 406;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Time Clock Breaks', [SectionID] = 400, [Description] = 'Time Clock Breaks', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Production Time Clock Break Lunch Smoke Rest' WHERE ID = 407;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Inventory Options', [SectionID] = 500, [Description] = 'Inventory Options', [OptionLevels] = 2, [IsHidden] = 0, [SearchTerms] = 'Purchasing Inventory Material Stock Tracking Part Warehouse Level' WHERE ID = 501;
UPDATE [dbo].[System.Option.Category] SET [Name] = 'Theme', [SectionID] = 2300, [Description] = 'Theme', [OptionLevels] = 4, [IsHidden] = 0, [SearchTerms] = 'Theme Color Logo Font Style Look Location URL Web Internet Design' WHERE ID = 2301;

-- ---------------------------------------------------------
-- Build up the Section Search Terms by combining the children
-- ---------------------------------------------------------
UPDATE S 
SET SearchTerms = 
        REPLACE(REPLACE(
        ( 
            SELECT SearchTerms 
            FROM [System.Option.Category] SC
            WHERE SC.SectionID = S.ID
            FOR XML PATH('')
        ), '<SearchTerms>', ''), '</SearchTerms>', '')
FROM [System.Option.Section] S
;

UPDATE S 
SET SearchTerms = 
        REPLACE(REPLACE(
        ( 
            SELECT SearchTerms 
            FROM [System.Option.Section] SC
            WHERE SC.ParentID = S.ID
              AND SearchTerms IS NULL
            FOR XML PATH('')
        ), '<SearchTerms>', ''), '</SearchTerms>', '') 
FROM [System.Option.Section] S
WHERE Len(SearchTerms) = 0
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

