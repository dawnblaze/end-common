using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixToCFDefDataTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE
	[cfd]
SET
	[cfd].[DataType] = cfit.[DataType]
FROM
	[CustomField.Definition] AS [cfd]
	LEFT JOIN [enum.CustomField.InputType] AS [cfit] ON [cfd].[InputType] = [cfit].[ID]
WHERE
	[cfd].[DataType] = 0;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // no down
        }
    }
}
