using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180606160613_AddNumberingOptionsDefinitions")]
    public partial class AddNumberingOptionsDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF NOT EXISTS(SELECT * FROM [System.Option.Definition] WHERE [Name] = 'UseEstimateNumberForOrder')
BEGIN
    INSERT INTO [dbo].[System.Option.Definition] ([ID] ,[Name] ,[Label] ,[Description] ,[DataType] ,[CategoryID] ,[ListValues] ,[DefaultValue] ,[IsHidden])
    VALUES (53, 'UseEstimateNumberForOrder', 'Use Estimate Number for Order', 'If true then the Estimate Number will be used for the Order Number', 3, 6, NULL, 'true', 0)	   
END;

IF NOT EXISTS(SELECT * FROM [System.Option.Definition] WHERE [Name] = 'UseOrderNumberForInvoice')
BEGIN
    INSERT INTO [dbo].[System.Option.Definition] ([ID] ,[Name] ,[Label] ,[Description] ,[DataType] ,[CategoryID] ,[ListValues] ,[DefaultValue] ,[IsHidden])
    VALUES (54, 'UseOrderNumberForInvoice', 'Use Order Number for Invoice', 'If true then the Order Number will be used for the Invoice Number', 3, 6, NULL, 'true', 0)	   
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Definition] WHERE [Name] = 'UseEstimateNumberForOrder';
DELETE FROM [System.Option.Definition] WHERE [Name] = 'UseOrderNumberForInvoice';
");
        }
    }
}

