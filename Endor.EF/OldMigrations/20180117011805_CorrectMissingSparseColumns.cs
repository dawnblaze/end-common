using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180117011805_CorrectMissingSparseColumns")]
    public partial class CorrectMissingSparseColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Add Sparse Columns To Accounting.Payment.Term
            migrationBuilder.Sql(@"
DROP INDEX [IX_Accounting.Payment.Term.Name]
    ON [dbo].[Accounting.Payment.Term];

DROP INDEX [IX_Business.Data_BID_BillingEmployeeID]
    ON [dbo].[Business.Data];

DROP INDEX [IX_Business.Data_BID_OwnerEmployeeID]
    ON [dbo].[Business.Data];

DROP INDEX [IX_Business.Locator_LocatorType_LocatorSubType]
    ON [dbo].[Business.Locator];

DROP INDEX [IX_Business.Locator_ParentID]
    ON [dbo].[Business.Locator];

DROP INDEX [IX_Campaign.Data_BID_TeamID]
    ON [dbo].[Campaign.Data];
DROP INDEX [IX_Campaign.Data_CampaignType]
    ON [dbo].[Campaign.Data];
DROP INDEX [IX_Company.Data_BID_IndustryID]
    ON [dbo].[Company.Data];
DROP INDEX [IX_Company.Data_BID_LocationID]
    ON [dbo].[Company.Data];
DROP INDEX [IX_Company.Data_BID_SourceID]
    ON [dbo].[Company.Data];
DROP INDEX [IX_Company.Data_BID_TaxGroupID]
    ON [dbo].[Company.Data];
DROP INDEX [IX_Company.Data_StatusID]
    ON [dbo].[Company.Data];
DROP INDEX [IX_Company.Data_TimeZoneID]
    ON [dbo].[Company.Data];
DROP INDEX [IX_Company.Locator_LocatorType_LocatorSubType]
    ON [dbo].[Company.Locator];
DROP INDEX [IX_Contact.Data_BID_LocationID]
    ON [dbo].[Contact.Data];
DROP INDEX [IX_Contact.Data_TimeZoneID]
    ON [dbo].[Contact.Data];
DROP INDEX [IX_Contact.Locator_LocatorType_LocatorSubType]
    ON [dbo].[Contact.Locator];
DROP INDEX [IX_CRM.CustomField.Def_DataType]
    ON [dbo].[CRM.CustomField.Def];
DROP INDEX [IX_CRM.CustomField.Def_InputType]
    ON [dbo].[CRM.CustomField.Def];
DROP INDEX [IX_CRM.CustomField.Helper_DataType]
    ON [dbo].[CRM.CustomField.Helper];
DROP INDEX [IX_CRM.CustomField.Helper_HelperType]
    ON [dbo].[CRM.CustomField.Helper];
DROP INDEX [IX_Employee.Data_BID_ReportsToID]
    ON [dbo].[Employee.Data];
DROP INDEX [IX_Employee.Locator_LocatorType_LocatorSubType]
    ON [dbo].[Employee.Locator];
DROP INDEX [IX_Employee.Team.LocationLink_BID_LocationID]
    ON [dbo].[Employee.Team.LocationLink];
DROP INDEX [IX_Employee.TeamLink_BID_TeamID]
    ON [dbo].[Employee.TeamLink];
DROP INDEX [IX_enum.CustomField.InputType_DataType]
    ON [dbo].[enum.CustomField.InputType];
DROP INDEX [IX_Location.Data_BID_DefaultTaxGroupID]
    ON [dbo].[Location.Data];
DROP INDEX [IX_Location.Data_TimeZoneID]
    ON [dbo].[Location.Data];
DROP INDEX [IX_Location.Locator_LocatorType_LocatorSubType]
    ON [dbo].[Location.Locator];
DROP INDEX [IX_Opportunity.Data_BID_CampaignID]
    ON [dbo].[Opportunity.Data];
DROP INDEX [IX_Opportunity.Data_BID_TeamID]
    ON [dbo].[Opportunity.Data];
DROP INDEX [IX_Security.Role.Link_RoleType]
    ON [dbo].[Security.Role.Link];
DROP INDEX [IX_System.Option.Definition_DataType]
    ON [dbo].[System.Option.Definition];
DROP INDEX [IX_User.Link_BID_ContactID]
    ON [dbo].[User.Link];
DROP INDEX [IX_User.Link_BID_EmployeeID]
    ON [dbo].[User.Link];
DROP INDEX [IX_User.Link_BID_RightGroupID]
    ON [dbo].[User.Link];
DROP INDEX [IX_User.Link_RoleType]
    ON [dbo].[User.Link];");


            migrationBuilder.DropDefaultConstraintIfExists("_Root.Data", "ModifiedDT");

            migrationBuilder.DropDefaultConstraintIfExists("_Root.Data", "IsActive");

            migrationBuilder.DropDefaultConstraintIfExists("Accounting.Payment.Term", "CompoundInterest");
            migrationBuilder.DropDefaultConstraintIfExists("Accounting.Payment.Term", "IsActive");

            migrationBuilder.DropDefaultConstraintIfExists("Accounting.Payment.Term", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Accounting.Tax.Assessment", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Accounting.Tax.Assessment", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Accounting.Tax.Group", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Accounting.Tax.Group", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Activity.Action", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Activity.Action", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Activity.Action", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Activity.Event", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Activity.Event", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Activity.Event", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Activity.GLActivity", "CreatedDT");

            migrationBuilder.DropDefaultConstraintIfExists("Activity.GLActivity", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Data", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Locator", "HasImage");

            migrationBuilder.DropDefaultConstraintIfExists("Business.Locator", "IsValid");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Locator", "IsVerified");
            migrationBuilder.DropDefaultConstraintIfExists("Business.Locator", "SortIndex");

            migrationBuilder.DropDefaultConstraintIfExists("Campaign.Data", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Campaign.Data", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Campaign.Data", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Data", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Data", "LocationID");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Data", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Data", "StatusText");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Data", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Locator", "IsVerified");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Locator", "HasImage");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Locator", "IsValid");
            migrationBuilder.DropDefaultConstraintIfExists("Company.Locator", "SortIndex");
            migrationBuilder.DropDefaultConstraintIfExists("Contact.Data", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Contact.Data", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Contact.Data", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Contact.Locator", "SortIndex");
            migrationBuilder.DropDefaultConstraintIfExists("Contact.Locator", "IsVerified");
            migrationBuilder.DropDefaultConstraintIfExists("Contact.Locator", "HasImage");
            migrationBuilder.DropDefaultConstraintIfExists("Contact.Locator", "IsValid");
            migrationBuilder.DropDefaultConstraintIfExists("CRM.CustomField.Def", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("CRM.CustomField.Def", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("CRM.CustomField.Helper", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("CRM.CustomField.Helper", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("CRM.Industry", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("CRM.Industry", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("CRM.Source", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("CRM.Source", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Data", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Data", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Data", "LocationID");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Data", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Locator", "HasImage");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Locator", "IsValid");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Locator", "IsVerified");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Locator", "SortIndex");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Team", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Team", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.Team", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Employee.TeamLink", "RoleType");
            migrationBuilder.DropDefaultConstraintIfExists("enum.TimeZone", "IsCommon");
            migrationBuilder.DropDefaultConstraintIfExists("List.Filter", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("List.Filter", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("List.Filter", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("List.Filter", "SortIndex");
            migrationBuilder.DropDefaultConstraintIfExists("Location.Data", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Location.Data", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Location.Data", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Location.Locator", "HasImage");
            migrationBuilder.DropDefaultConstraintIfExists("Location.Locator", "IsValid");
            migrationBuilder.DropDefaultConstraintIfExists("Location.Locator", "IsVerified");
            migrationBuilder.DropDefaultConstraintIfExists("Location.Locator", "SortIndex");
            migrationBuilder.DropDefaultConstraintIfExists("Opportunity.Data", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Opportunity.Data", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Opportunity.Data", "LocationID");
            migrationBuilder.DropDefaultConstraintIfExists("Opportunity.Data", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Opportunity.Data", "StatusText");
            migrationBuilder.DropDefaultConstraintIfExists("Option.Data", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Option.Data", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Option.Data", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("Report.Menu", "CreatedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Report.Menu", "ModifiedDT");
            migrationBuilder.DropDefaultConstraintIfExists("Report.Menu", "SortIndex");
            migrationBuilder.DropDefaultConstraintIfExists("Report.Menu", "IsActive");
            migrationBuilder.DropDefaultConstraintIfExists("System.List.Column", "SortIndex");
            migrationBuilder.DropDefaultConstraintIfExists("System.List.Filter.Criteria", "IsLimitToList");
            migrationBuilder.DropDefaultConstraintIfExists("System.Option.Definition", "DataType");
            migrationBuilder.DropDefaultConstraintIfExists("System.Option.Section", "Depth");
            migrationBuilder.DropDefaultConstraintIfExists("List.Filter.EmployeeSubscription", "SortIndex");
            migrationBuilder.DropDefaultConstraintIfExists("SimpleBusinessData", "IsActive");
            migrationBuilder.Sql(@"
            ALTER TABLE [dbo].[Accounting.Payment.Term.LocationLink] DROP CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term];
ALTER TABLE [dbo].[Accounting.Tax.Group.AssessmentLink] DROP CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment];
ALTER TABLE [dbo].[Accounting.Tax.Group.AssessmentLink] DROP CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group];
ALTER TABLE [dbo].[Accounting.Tax.Group.LocationLink] DROP CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_Accounting.Tax.Group];
ALTER TABLE [dbo].[Location.Data] DROP CONSTRAINT [FK_Location.Data_Accounting.Tax.Group];
ALTER TABLE [dbo].[CRM.CustomField.Def] DROP CONSTRAINT [FK_CRM.Setup.CustomField_Business.Data];
ALTER TABLE [dbo].[CRM.CustomField.Helper] DROP CONSTRAINT [FK_CRM.Setup.CustomField.Helper_Business.Data];
ALTER TABLE [dbo].[Employee.Data] DROP CONSTRAINT [FK_Employee.Data_Business.Data];
ALTER TABLE [dbo].[Business.Data] DROP CONSTRAINT [FK_Business.Data_Employee.Data1];
ALTER TABLE [dbo].[Business.Data] DROP CONSTRAINT [FK_Business.Data_Employee.Data];
ALTER TABLE [dbo].[Opportunity.Data] DROP CONSTRAINT [FK_Opportunity.Data_Business.Data];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_Business.Data];
ALTER TABLE [dbo].[Location.Data] DROP CONSTRAINT [FK_Location.Data_Business.Data];
ALTER TABLE [dbo].[Business.Locator] DROP CONSTRAINT [FK_Business.Locator_Business.Data_ParentID];
ALTER TABLE [dbo].[Campaign.Data] DROP CONSTRAINT [FK_Campaign.Data_Business.Data];
ALTER TABLE [dbo].[Contact.Data] DROP CONSTRAINT [FK_Contact.Data_Business.Data];
ALTER TABLE [dbo].[Business.Locator] DROP CONSTRAINT [FK_Business.Locator_enum.LocatorSubType];
ALTER TABLE [dbo].[Business.Locator] DROP CONSTRAINT [FK_Business.Locator_enum.LocatorType];
ALTER TABLE [dbo].[Opportunity.Data] DROP CONSTRAINT [FK_Opportunity.Data_Campaign.Data];
ALTER TABLE [dbo].[Campaign.Data] DROP CONSTRAINT [FK_Campaign.Data_enum.Campaign.Type];
ALTER TABLE [dbo].[Campaign.Data] DROP CONSTRAINT [FK_Campaign.Data_Campaign.Custom];
ALTER TABLE [dbo].[Campaign.Data] DROP CONSTRAINT [FK_Campaign.Data_Employee.Team];
ALTER TABLE [dbo].[Opportunity.Data] DROP CONSTRAINT [FK_Opportunity.Data_Company.Data];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_enum.CRM.Company.Status];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_Company.Custom];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_Location.Data];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_enum.TimeZone];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_CRM.Industry];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_Company.Data];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_CRM.Source];
ALTER TABLE [dbo].[Company.Data] DROP CONSTRAINT [FK_Company.Data_Employee.Team];
ALTER TABLE [dbo].[Company.Locator] DROP CONSTRAINT [FK_Company.Locator_Company.Data];
ALTER TABLE [dbo].[Contact.Data] DROP CONSTRAINT [FK_Contact.Data_Company.Data];
ALTER TABLE [dbo].[Company.Locator] DROP CONSTRAINT [FK_Company.Locator_enum.LocatorType];
ALTER TABLE [dbo].[Company.Locator] DROP CONSTRAINT [FK_Company.Locator_enum.LocatorSubType];
ALTER TABLE [dbo].[User.Link] DROP CONSTRAINT [FK_User.Link_Contact.Data];
ALTER TABLE [dbo].[Opportunity.Data] DROP CONSTRAINT [FK_Opportunity.Data_Contact.Data];
ALTER TABLE [dbo].[Contact.Data] DROP CONSTRAINT [FK_Contact.Data_Contact.Custom];
ALTER TABLE [dbo].[Contact.Data] DROP CONSTRAINT [FK_Contact.Data_enum.TimeZone];
ALTER TABLE [dbo].[Contact.Data] DROP CONSTRAINT [FK_Contact.Data_Location.Data];
ALTER TABLE [dbo].[Contact.Locator] DROP CONSTRAINT [FK_Contact.Locator_Contact.Data];
ALTER TABLE [dbo].[Contact.Locator] DROP CONSTRAINT [FK_Contact.Locator_enum.LocatorSubType];
ALTER TABLE [dbo].[Contact.Locator] DROP CONSTRAINT [FK_Contact.Locator_enum.LocatorType];
ALTER TABLE [dbo].[CRM.CustomField.Def] DROP CONSTRAINT [FK_CRM.Setup.CustomField_enum.CustomField.DataType];
ALTER TABLE [dbo].[CRM.CustomField.Def] DROP CONSTRAINT [FK_CRM.Setup.CustomField_enum.CustomField.InputType];
ALTER TABLE [dbo].[CRM.CustomField.HelperLink] DROP CONSTRAINT [FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField];
ALTER TABLE [dbo].[CRM.CustomField.Helper] DROP CONSTRAINT [FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType];
ALTER TABLE [dbo].[CRM.CustomField.Helper] DROP CONSTRAINT [FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType];
ALTER TABLE [dbo].[CRM.CustomField.HelperLink] DROP CONSTRAINT [FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper];
ALTER TABLE [dbo].[CRM.Industry] DROP CONSTRAINT [FK_CRM.Industry_CRM.Industry];
ALTER TABLE [dbo].[CRM.Source] DROP CONSTRAINT [FK_CRM.Source_CRM.Source];
ALTER TABLE [dbo].[Employee.Data] DROP CONSTRAINT [FK_Employee.Data_ReportsToID];
ALTER TABLE [dbo].[Employee.Locator] DROP CONSTRAINT [FK_Employee.Locator_Employee.Data];
ALTER TABLE [dbo].[Employee.TeamLink] DROP CONSTRAINT [FK_Employee.TeamLink_Employee.Data];
ALTER TABLE [dbo].[List.Filter.EmployeeSubscription] DROP CONSTRAINT [FK_List.Filter.EmployeeSubscription_Employee.Data];
ALTER TABLE [dbo].[User.Link] DROP CONSTRAINT [FK_User.Link_Employee.Data];
ALTER TABLE [dbo].[Employee.Locator] DROP CONSTRAINT [FK_Employee.Locator_enum.LocatorType];
ALTER TABLE [dbo].[Employee.Locator] DROP CONSTRAINT [FK_Employee.Locator_enum.LocatorSubType];
ALTER TABLE [dbo].[Employee.TeamLink] DROP CONSTRAINT [FK_Employee.TeamLink_Employee.Team];
ALTER TABLE [dbo].[Opportunity.Data] DROP CONSTRAINT [FK_Opportunity.Data_Employee.Team];
ALTER TABLE [dbo].[Employee.Team.LocationLink] DROP CONSTRAINT [FK_Employee.Team.LocationLink_Employee.Team];
ALTER TABLE [dbo].[enum.CustomField.InputType] DROP CONSTRAINT [FK_enum.CustomField.InputType_enum.CustomField.DataType];
ALTER TABLE [dbo].[enum.Locator.SubType] DROP CONSTRAINT [FK_enum.Locator.SubType_enum.Locator.Type];
ALTER TABLE [dbo].[Location.Locator] DROP CONSTRAINT [FK_Business.Location.Locator_enum.LocatorSubType];
ALTER TABLE [dbo].[Location.Locator] DROP CONSTRAINT [FK_Location.Locator_enum.LocatorType];
ALTER TABLE [dbo].[Location.Data] DROP CONSTRAINT [FK_Location.Data_enum.TimeZone];
ALTER TABLE [dbo].[List.Filter.EmployeeSubscription] DROP CONSTRAINT [FK_List.Filter.EmployeeSubscription_List.Filter];
ALTER TABLE [dbo].[Accounting.Payment.Term.LocationLink] DROP CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Location.Data];
ALTER TABLE [dbo].[Accounting.Tax.Group.LocationLink] DROP CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Location.Data];
ALTER TABLE [dbo].[Employee.Team.LocationLink] DROP CONSTRAINT [FK_Employee.Team.LocationLink_Location.Data];
ALTER TABLE [dbo].[Location.Locator] DROP CONSTRAINT [FK_Location.Locator_Location.Data];
ALTER TABLE [dbo].[Opportunity.Data] DROP CONSTRAINT [FK_Opportunity.Data_Opportunity.Custom];
ALTER TABLE [dbo].[Option.Data] DROP CONSTRAINT [FK_Option.Data_System.Option.Definition];
ALTER TABLE [dbo].[Security.Right.Group.Link] DROP CONSTRAINT [FK_Security.Right.Group.Link_Security.Right.Group1];
ALTER TABLE [dbo].[Security.Right.Group.Link] DROP CONSTRAINT [FK_Security.Right.Group.Link_Security.Right.Group];
ALTER TABLE [dbo].[Security.Right.Link] DROP CONSTRAINT [FK_Security.Right.Link_Security.Right.Collection];
ALTER TABLE [dbo].[Security.Role.Link] DROP CONSTRAINT [FK_Security.Role.Link_Security.Right.Group];
ALTER TABLE [dbo].[User.Link] DROP CONSTRAINT [FK_User.Link_Security.Right.Collection];
ALTER TABLE [dbo].[System.Option.Category] DROP CONSTRAINT [FK_System.Option.Category_System.Option.Section];
ALTER TABLE [dbo].[System.Option.Definition] DROP CONSTRAINT [FK_System.Option.Definition_System.Option.Category];
ALTER TABLE [dbo].[System.Option.Definition] DROP CONSTRAINT [FK_System.Option.Definition_enum.CustomField.DataType];
ALTER TABLE [dbo].[System.Option.Section] DROP CONSTRAINT [FK_System.Option.Section_System.Option.Section];
ALTER TABLE [dbo].[User.Link] DROP CONSTRAINT [FK_User.Link_enum.User.Role.Type];
ALTER TABLE [dbo].[Security.Role.Link] DROP CONSTRAINT [FK_Security.Role.Link_enum.User.Role.Type];
DROP TABLE [dbo].[SimpleBusinessData];

BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx__Root.Data] (
    [ID]          INT            NOT NULL,
    [BID]         SMALLINT       NOT NULL,
    [ClassTypeID] AS             ((0)),
    [ModifiedDT]  DATETIME2 (2)  CONSTRAINT [DF__RootTemplate_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF__RootTemplate_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]        NVARCHAR (255) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__RootTemplate1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[_Root.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx__Root.Data] ([BID], [ID], [IsActive], [ModifiedDT], [Name])
        SELECT   [BID],
                 [ID],
                 [IsActive],
                 [ModifiedDT],
                 [Name]
        FROM     [dbo].[_Root.Data]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[_Root.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx__Root.Data]', N'_Root.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__RootTemplate1]', N'PK__RootTemplate', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
/*
The column CompoundInterest on table [dbo].[Accounting.Payment.Term] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.

The column IsActive on table [dbo].[Accounting.Payment.Term] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Accounting.Payment.Term]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Accounting.Payment.Term] (
    [BID]                     SMALLINT               NOT NULL,
    [ID]                      SMALLINT               NOT NULL,
    [ClassTypeID]             AS                     ((11105)),
    [ModifiedDT]              DATETIME2 (2)          CONSTRAINT [DF_Accounting.Payment.Term_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]                BIT                    CONSTRAINT [DF_Accounting.Payment.Term_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]                    NVARCHAR (255)         NULL,
    [GracePeriod]             SMALLINT               NULL,
    [InterestRate]            DECIMAL (12, 4)        NULL,
    [InterestBasedOnSaleDate] BIT                    CONSTRAINT [DF_Accounting.Payment.Term_InterestBasedOnSaleDate] DEFAULT ((0)) NOT NULL,
    [CompoundInterest]        BIT                    CONSTRAINT [DF_Accounting.Payment.Term_CompoundInterest] DEFAULT ((1)) NOT NULL,
    [EarlyPaymentDiscount]    DECIMAL (12, 4) SPARSE NULL,
    [EarlyPaymentDays]        SMALLINT SPARSE        NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Accounting.Payment.Term1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Accounting.Payment.Term])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Accounting.Payment.Term] ([BID], [ID], [CompoundInterest], [EarlyPaymentDays], [EarlyPaymentDiscount], [GracePeriod], [InterestBasedOnSaleDate], [InterestRate], [IsActive], [ModifiedDT], [Name])
        SELECT   [BID],
                 [ID],
                 [CompoundInterest],
                 [EarlyPaymentDays],
                 [EarlyPaymentDiscount],
                 [GracePeriod],
                 [InterestBasedOnSaleDate],
                 [InterestRate],
                 [IsActive],
                 [ModifiedDT],
                 [Name]
        FROM     [dbo].[Accounting.Payment.Term]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Accounting.Payment.Term];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Accounting.Payment.Term]', N'Accounting.Payment.Term';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Accounting.Payment.Term1]', N'PK_Accounting.Payment.Term', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Accounting.Payment.Term)Name]
    ON [dbo].[Accounting.Payment.Term]([BID] ASC, [Name] ASC, [IsActive] ASC);


GO
/*
The column IsActive on table [dbo].[Accounting.Tax.Assessment] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Accounting.Tax.Assessment]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Accounting.Tax.Assessment] (
    [BID]           SMALLINT        NOT NULL,
    [ID]            SMALLINT        NOT NULL,
    [ClassTypeID]   AS              ((11102)),
    [ModifiedDT]    DATETIME2 (2)   CONSTRAINT [DF_Settings.Tax.Assessment.Data_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]      BIT             CONSTRAINT [DF_Settings.Tax.Assessment.Data_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]          NVARCHAR (255)  NOT NULL,
    [TaxRate]       DECIMAL (12, 4) NOT NULL,
    [InvoiceText]   NVARCHAR (50)   NOT NULL,
    [LookupCode]    NVARCHAR (255)  NULL,
    [AgencyName]    NVARCHAR (255)  NULL,
    [AccountNumber] NVARCHAR (255)  NULL,
    [VendorID]      INT             NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Accounting.Tax.Assessment_11] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Accounting.Tax.Assessment])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Accounting.Tax.Assessment] ([BID], [ID], [AccountNumber], [AgencyName], [InvoiceText], [IsActive], [LookupCode], [ModifiedDT], [Name], [TaxRate], [VendorID])
        SELECT   [BID],
                 [ID],
                 [AccountNumber],
                 [AgencyName],
                 [InvoiceText],
                 [IsActive],
                 [LookupCode],
                 [ModifiedDT],
                 [Name],
                 [TaxRate],
                 [VendorID]
        FROM     [dbo].[Accounting.Tax.Assessment]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Accounting.Tax.Assessment];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Accounting.Tax.Assessment]', N'Accounting.Tax.Assessment';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Accounting.Tax.Assessment_11]', N'PK_Accounting.Tax.Assessment_1', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Accounting.Tax.Assessment_Name]
    ON [dbo].[Accounting.Tax.Assessment]([BID] ASC, [Name] ASC, [IsActive] ASC);


GO
/*
The column IsActive on table [dbo].[Accounting.Tax.Group] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Accounting.Tax.Group]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Accounting.Tax.Group] (
    [BID]         SMALLINT        NOT NULL,
    [ID]          SMALLINT        NOT NULL,
    [ClassTypeID] AS              ((11101)),
    [ModifiedDT]  DATETIME2 (2)   CONSTRAINT [DF_Accounting.Tax.Group.Data_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]    BIT             CONSTRAINT [DF_Accounting.Tax.Group.Data_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]        NVARCHAR (255)  NOT NULL,
    [TaxRate]     DECIMAL (12, 4) NULL,
    [IsTaxExempt] AS              (CASE WHEN [TaxRate] = (0.0) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_Accounting.Tax.Group1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Accounting.Tax.Group])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Accounting.Tax.Group] ([BID], [ID], [IsActive], [ModifiedDT], [Name], [TaxRate])
        SELECT   [BID],
                 [ID],
                 [IsActive],
                 [ModifiedDT],
                 [Name],
                 [TaxRate]
        FROM     [dbo].[Accounting.Tax.Group]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Accounting.Tax.Group];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Accounting.Tax.Group]', N'Accounting.Tax.Group';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Accounting.Tax.Group1]', N'PK_Accounting.Tax.Group', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
/*
The column IsActive on table [dbo].[Activity.Action] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Activity.Action]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Activity.Action] (
    [BID]           SMALLINT       NOT NULL,
    [ID]            INT            NOT NULL,
    [ClassTypeID]   AS             ((11001)),
    [CreatedDate]   DATE           CONSTRAINT [DF_Activity_Action_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]    DATETIME2 (2)  CONSTRAINT [DF_Activity_Action_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]      BIT            CONSTRAINT [DF_Activity_Action_IsActive] DEFAULT ((1)) NOT NULL,
    [ActivityType]  TINYINT        NOT NULL,
    [Subject]       NVARCHAR (255) NOT NULL,
    [StartDT]       DATETIME2 (0)  NOT NULL,
    [EndDT]         DATETIME2 (0)  NOT NULL,
    [Notes]         NVARCHAR (MAX) NULL,
    [TeamID]        INT            NULL,
    [IsComplete]    AS             (CASE WHEN [CompletedDT] IS NULL THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    [CompletedByID] INT            NULL,
    [CompletedDT]   DATETIME2 (0)  NULL,
    [MetaData]      XML            NULL,
    [AutoComplete]  BIT            CONSTRAINT [DF_Activity.Action_AutoComplete] DEFAULT ((0)) NOT NULL,
    [AutoRollover]  BIT            CONSTRAINT [DF_Activity.Action_AutoRollover] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Activity_Action1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Activity.Action])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Activity.Action] ([BID], [ID], [ActivityType], [AutoComplete], [AutoRollover], [CompletedByID], [CompletedDT], [CreatedDate], [EndDT], [IsActive], [MetaData], [ModifiedDT], [Notes], [StartDT], [Subject], [TeamID])
        SELECT   [BID],
                 [ID],
                 [ActivityType],
                 [AutoComplete],
                 [AutoRollover],
                 [CompletedByID],
                 [CompletedDT],
                 [CreatedDate],
                 [EndDT],
                 [IsActive],
                 [MetaData],
                 [ModifiedDT],
                 [Notes],
                 [StartDT],
                 [Subject],
                 [TeamID]
        FROM     [dbo].[Activity.Action]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Activity.Action];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Activity.Action]', N'Activity.Action';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Activity_Action1]', N'PK_Activity_Action', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
/*
The column IsActive on table [dbo].[Activity.Event] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Activity.Event]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Activity.Event] (
    [BID]           SMALLINT       NOT NULL,
    [ID]            INT            NOT NULL,
    [ClassTypeID]   AS             ((11002)),
    [CreatedDate]   DATE           CONSTRAINT [DF_Activity_Event_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]    DATETIME2 (2)  CONSTRAINT [DF_Activity_Event_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]      BIT            CONSTRAINT [DF_Activity_Event_IsActive] DEFAULT ((1)) NOT NULL,
    [ActivityType]  TINYINT        NOT NULL,
    [Subject]       NVARCHAR (255) NOT NULL,
    [StartDT]       DATETIME2 (0)  NOT NULL,
    [EndDT]         AS             ([StartDT]),
    [Notes]         NVARCHAR (MAX) NULL,
    [TeamID]        INT            NULL,
    [IsComplete]    AS             (CASE WHEN [CompletedDT] IS NULL THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    [CompletedByID] INT            NULL,
    [CompletedDT]   DATETIME2 (0)  NULL,
    [MetaData]      XML            NULL,
    [AutoComplete]  BIT            CONSTRAINT [DF_Activity.Event_AutoComplete] DEFAULT ((0)) NOT NULL,
    [AutoRollover]  BIT            CONSTRAINT [DF_Activity.Event_AutoRollover] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Activity_Event1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Activity.Event])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Activity.Event] ([BID], [ID], [ActivityType], [AutoComplete], [AutoRollover], [CompletedByID], [CompletedDT], [CreatedDate], [IsActive], [MetaData], [ModifiedDT], [Notes], [StartDT], [Subject], [TeamID])
        SELECT   [BID],
                 [ID],
                 [ActivityType],
                 [AutoComplete],
                 [AutoRollover],
                 [CompletedByID],
                 [CompletedDT],
                 [CreatedDate],
                 [IsActive],
                 [MetaData],
                 [ModifiedDT],
                 [Notes],
                 [StartDT],
                 [Subject],
                 [TeamID]
        FROM     [dbo].[Activity.Event]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Activity.Event];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Activity.Event]', N'Activity.Event';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Activity_Event1]', N'PK_Activity_Event', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Activity.GLActivity] (
    [BID]           SMALLINT       NOT NULL,
    [ID]            INT            NOT NULL,
    [ClassTypeID]   AS             ((11003)),
    [CreatedDate]   DATE           CONSTRAINT [DF_Activity_GLActivity_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]    DATETIME2 (2)  CONSTRAINT [DF_Activity_GLActivity_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]      AS             (isnull(CONVERT (BIT, (1)), (1))),
    [ActivityType]  TINYINT        NOT NULL,
    [Subject]       NVARCHAR (255) NOT NULL,
    [StartDT]       AS             ([CompletedDT]),
    [EndDT]         AS             ([CompletedDT]),
    [Notes]         AS             (CONVERT (VARCHAR (MAX), NULL)),
    [TeamID]        INT SPARSE     NULL,
    [IsComplete]    AS             (isnull(CONVERT (BIT, (1)), (1))),
    [CompletedByID] INT            NOT NULL,
    [CompletedDT]   DATETIME2 (0)  NOT NULL,
    [MetaData]      XML SPARSE     NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Activity_GLActivity1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Activity.GLActivity])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Activity.GLActivity] ([BID], [ID], [ActivityType], [CompletedByID], [CompletedDT], [CreatedDate], [MetaData], [ModifiedDT], [Subject], [TeamID])
        SELECT   [BID],
                 [ID],
                 [ActivityType],
                 [CompletedByID],
                 [CompletedDT],
                 [CreatedDate],
                 [MetaData],
                 [ModifiedDT],
                 [Subject],
                 [TeamID]
        FROM     [dbo].[Activity.GLActivity]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Activity.GLActivity];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Activity.GLActivity]', N'Activity.GLActivity';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Activity_GLActivity1]', N'PK_Activity_GLActivity', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Association.Data] (
    [BID]             SMALLINT   NOT NULL,
    [ID]              INT        NOT NULL,
    [ClassTypeID]     AS         ((1600)),
    [AssociationType] TINYINT    NOT NULL,
    [CampaignID]      INT SPARSE NULL,
    [CompanyID]       INT SPARSE NULL,
    [ContactID]       INT SPARSE NULL,
    [DocumentID]      INT SPARSE NULL,
    [EmployeeID]      INT SPARSE NULL,
    [LeadID]          INT SPARSE NULL,
    [OpportunityID]   INT SPARSE NULL,
    [TeamID]          INT SPARSE NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_AssociationTemplate1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Association.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Association.Data] ([BID], [ID], [AssociationType], [CampaignID], [CompanyID], [ContactID], [DocumentID], [EmployeeID], [LeadID], [OpportunityID], [TeamID])
        SELECT   [BID],
                 [ID],
                 [AssociationType],
                 [CampaignID],
                 [CompanyID],
                 [ContactID],
                 [DocumentID],
                 [EmployeeID],
                 [LeadID],
                 [OpportunityID],
                 [TeamID]
        FROM     [dbo].[Association.Data]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Association.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Association.Data]', N'Association.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AssociationTemplate1]', N'PK_AssociationTemplate', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Association.Data_Campaign]
    ON [dbo].[Association.Data]([BID] ASC, [CampaignID] ASC) WHERE ([CampaignID] IS NOT NULL);
CREATE NONCLUSTERED INDEX [IX_Association.Data_Company]
    ON [dbo].[Association.Data]([BID] ASC, [CompanyID] ASC) WHERE ([CompanyID] IS NOT NULL);
CREATE NONCLUSTERED INDEX [IX_Association.Data_Contact]
    ON [dbo].[Association.Data]([ContactID] ASC) WHERE ([ContactID] IS NOT NULL);
CREATE NONCLUSTERED INDEX [IX_Association.Data_Document]
    ON [dbo].[Association.Data]([BID] ASC, [DocumentID] ASC) WHERE ([DocumentID] IS NOT NULL);
CREATE NONCLUSTERED INDEX [IX_Association.Data_Employee]
    ON [dbo].[Association.Data]([BID] ASC, [EmployeeID] ASC) WHERE ([EmployeeID] IS NOT NULL);
CREATE NONCLUSTERED INDEX [IX_Association.Data_Lead]
    ON [dbo].[Association.Data]([BID] ASC, [LeadID] ASC) WHERE ([LeadID] IS NOT NULL);
CREATE NONCLUSTERED INDEX [IX_Association.Data_Opportunity]
    ON [dbo].[Association.Data]([BID] ASC, [OpportunityID] ASC) WHERE ([OpportunityID] IS NOT NULL);
CREATE NONCLUSTERED INDEX [IX_Association.Data_Team]
    ON [dbo].[Association.Data]([BID] ASC, [TeamID] ASC) WHERE ([TeamID] IS NOT NULL);


GO
/*
The column IsActive on table [dbo].[Business.Data] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Business.Data]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Business.Data] (
    [BID]               SMALLINT        NOT NULL,
    [ID]                AS              (isnull(CONVERT (SMALLINT, (1)), (1))),
    [ClassTypeID]       AS              ((1000)),
    [CreatedDate]       DATE            CONSTRAINT [DF_Business.Data_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]        DATETIME2 (2)   CONSTRAINT [DF_Business.Data_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]          BIT             CONSTRAINT [DF_Business.Data_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]              NVARCHAR (255)  NOT NULL,
    [LegalName]         NVARCHAR (255)  NULL,
    [HasImage]          BIT             CONSTRAINT [DF_Business.Data_HasImage] DEFAULT ((0)) NOT NULL,
    [OwnerEmployeeID]   SMALLINT SPARSE NULL,
    [BillingEmployeeID] SMALLINT SPARSE NULL,
    [AssociationType]   TINYINT         NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Business.Data1] PRIMARY KEY CLUSTERED ([BID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Business.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Business.Data] ([BID], [AssociationType], [BillingEmployeeID], [CreatedDate], [HasImage], [IsActive], [LegalName], [ModifiedDT], [Name], [OwnerEmployeeID])
        SELECT   [BID],
                 [AssociationType],
                 [BillingEmployeeID],
                 [CreatedDate],
                 [HasImage],
                 [IsActive],
                 [LegalName],
                 [ModifiedDT],
                 [Name],
                 [OwnerEmployeeID]
        FROM     [dbo].[Business.Data]
        ORDER BY [BID] ASC;
    END

DROP TABLE [dbo].[Business.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Business.Data]', N'Business.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Business.Data1]', N'PK_Business.Data', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Business.Data_Association]
    ON [dbo].[Business.Data]([AssociationType] ASC, [Name] ASC);
CREATE NONCLUSTERED INDEX [IX_Business.Data_Business]
    ON [dbo].[Business.Data]([Name] ASC, [AssociationType] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Business.Locator] (
    [BID]            SMALLINT        NOT NULL,
    [ID]             INT             NOT NULL,
    [ClassTypeID]    AS              ((1003)),
    [ParentID]       INT             NOT NULL,
    [LocatorType]    TINYINT         NOT NULL,
    [LocatorSubType] TINYINT         NULL,
    [Locator]        NVARCHAR (1024) NULL,
    [MetaData]       XML             NULL,
    [ModifiedDT]     DATETIME2 (2)   NOT NULL,
    [RawInput]       NVARCHAR (1024) NOT NULL,
    [SortIndex]      SMALLINT        CONSTRAINT [df_Business_Loc_sort_default] DEFAULT ((0)) NOT NULL,
    [IsValid]        BIT             CONSTRAINT [df_Business_Loc_isvalid_default] DEFAULT ((0)) NOT NULL,
    [IsVerified]     BIT             CONSTRAINT [df_Business_isverified_sort_default] DEFAULT ((0)) NOT NULL,
    [HasImage]       BIT             CONSTRAINT [DF_Business.Locator_HasImage] DEFAULT ((0)) NOT NULL,
    [IsPrimary]      AS              (CASE WHEN [SortIndex] = (1) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_Business.Locator1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Business.Locator])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Business.Locator] ([BID], [ID], [HasImage], [IsValid], [IsVerified], [Locator], [LocatorSubType], [LocatorType], [MetaData], [ModifiedDT], [ParentID], [RawInput], [SortIndex])
        SELECT   [BID],
                 [ID],
                 [HasImage],
                 [IsValid],
                 [IsVerified],
                 [Locator],
                 [LocatorSubType],
                 [LocatorType],
                 [MetaData],
                 [ModifiedDT],
                 [ParentID],
                 [RawInput],
                 [SortIndex]
        FROM     [dbo].[Business.Locator]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Business.Locator];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Business.Locator]', N'Business.Locator';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Business.Locator1]', N'PK_Business.Locator', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Business.Locator_LoactorType]
    ON [dbo].[Business.Locator]([BID] ASC, [LocatorType] ASC, [LocatorSubType] ASC);
CREATE NONCLUSTERED INDEX [IX_Business.Locator_Parent]
    ON [dbo].[Business.Locator]([BID] ASC, [ParentID] ASC);
CREATE PRIMARY XML INDEX [XML_IX_Business.Locator]
    ON [dbo].[Business.Locator]([MetaData])
    WITH (PAD_INDEX = OFF);


GO
/*
The column IsActive on table [dbo].[Campaign.Data] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Campaign.Data]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Campaign.Data] (
    [BID]             SMALLINT       NOT NULL,
    [ID]              INT            NOT NULL,
    [ClassTypeID]     AS             ((9100)),
    [CreatedDate]     DATE           CONSTRAINT [DF_CampaignTemplate_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]      DATETIME2 (2)  CONSTRAINT [DF_CampaignTemplate_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]        BIT            CONSTRAINT [DF_CampaignTemplate_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]            NVARCHAR (255) NOT NULL,
    [StatusID]        TINYINT        NOT NULL,
    [TeamID]          INT            NULL,
    [StartDate]       DATE           NULL,
    [EndDate]         DATE           NULL,
    [CampaignType]    TINYINT        NOT NULL,
    [Budget]          MONEY          NULL,
    [ActualCost]      MONEY          NULL,
    [ExceptedRevenue] MONEY          NULL,
    [ActualRevenue]   MONEY          NULL,
    [Description]     NVARCHAR (MAX) NULL,
    [Objective]       NVARCHAR (MAX) NULL,
    [MetaData]        XML            NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_CampaignTemplate1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Campaign.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Campaign.Data] ([BID], [ID], [ActualCost], [ActualRevenue], [Budget], [CampaignType], [CreatedDate], [Description], [EndDate], [ExceptedRevenue], [IsActive], [MetaData], [ModifiedDT], [Name], [Objective], [StartDate], [StatusID], [TeamID])
        SELECT   [BID],
                 [ID],
                 [ActualCost],
                 [ActualRevenue],
                 [Budget],
                 [CampaignType],
                 [CreatedDate],
                 [Description],
                 [EndDate],
                 [ExceptedRevenue],
                 [IsActive],
                 [MetaData],
                 [ModifiedDT],
                 [Name],
                 [Objective],
                 [StartDate],
                 [StatusID],
                 [TeamID]
        FROM     [dbo].[Campaign.Data]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Campaign.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Campaign.Data]', N'Campaign.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CampaignTemplate1]', N'PK_CampaignTemplate', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
/*
The column IsActive on table [dbo].[Company.Data] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Company.Data]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Company.Data] (
    [BID]                 SMALLINT               NOT NULL,
    [ID]                  INT                    NOT NULL,
    [ClassTypeID]         AS                     ((2000)),
    [CreatedDate]         DATE                   CONSTRAINT [DF_Company.Data_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]          DATETIME2 (2)          CONSTRAINT [DF_Company.Data_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]            BIT                    CONSTRAINT [DF_Company.Data_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]                NVARCHAR (255)         NOT NULL,
    [LocationID]          TINYINT                CONSTRAINT [DF_Company.Data_LocationID] DEFAULT ((1)) NOT NULL,
    [ParentID]            INT SPARSE             NULL,
    [IsAdHoc]             BIT                    CONSTRAINT [DF_Company.Data_IsSingleContact] DEFAULT ((0)) NOT NULL,
    [StatusID]            TINYINT                CONSTRAINT [DF_Company.Data_CompanyStatusID] DEFAULT ((0)) NOT NULL,
    [TeamID]              INT                    NULL,
    [SourceID]            SMALLINT               NULL,
    [IndustryID]          SMALLINT               NULL,
    [TaxGroupID]          SMALLINT               NULL,
    [PaymentTermsID]      SMALLINT               NULL,
    [IsPORequired]        BIT                    CONSTRAINT [DF_Company.Data_PORequired] DEFAULT ((0)) NOT NULL,
    [DefaultPONumber]     NVARCHAR (100) SPARSE  NULL,
    [RefundableCredit]    DECIMAL (18, 4) SPARSE NULL,
    [NonRefundableCredit] DECIMAL (18, 4) SPARSE NULL,
    [CreditLimit]         DECIMAL (18, 4) SPARSE NULL,
    [CreditNumber]        NVARCHAR (100) SPARSE  NULL,
    [CreditApprovalDate]  DATE SPARSE            NULL,
    [TimeZoneID]          SMALLINT SPARSE        NULL,
    [HasImage]            BIT                    CONSTRAINT [DF_Company.Data_HasImage] DEFAULT ((0)) NOT NULL,
    [Tags]                NVARCHAR (MAX)         NULL,
    [StatusText]          AS                     (concat(CASE WHEN [StatusID] = (1) THEN 'Lead' END, CASE WHEN ([StatusID] & (2)) <> (0) THEN 'Prospect' END, CASE WHEN ([StatusID] & (4)) <> (0) THEN 'Client' END, CASE WHEN ([StatusID] & (8)) <> (4) THEN 'Vendor' END)),
    [HasCreditAccount]    AS                     (CASE WHEN [CreditLimit] IS NULL THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    [IsLead]              AS                     (CASE WHEN ([StatusID] & (1)) <> (0) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    [IsProspect]          AS                     (CASE WHEN ([StatusID] & (2)) <> (0) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    [IsClient]            AS                     (CASE WHEN ([StatusID] & (4)) <> (0) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    [IsVendor]            AS                     (CASE WHEN ([StatusID] & (8)) <> (0) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    [IsPersonal]          AS                     (CASE WHEN ([StatusID] & (16)) <> (0) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_Company1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Company.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Company.Data] ([BID], [ID], [CreatedDate], [CreditApprovalDate], [CreditLimit], [CreditNumber], [DefaultPONumber], [HasImage], [IndustryID], [IsActive], [IsAdHoc], [IsPORequired], [LocationID], [ModifiedDT], [Name], [NonRefundableCredit], [ParentID], [PaymentTermsID], [RefundableCredit], [SourceID], [StatusID], [Tags], [TaxGroupID], [TeamID], [TimeZoneID])
        SELECT   [BID],
                 [ID],
                 [CreatedDate],
                 [CreditApprovalDate],
                 [CreditLimit],
                 [CreditNumber],
                 [DefaultPONumber],
                 [HasImage],
                 [IndustryID],
                 [IsActive],
                 [IsAdHoc],
                 [IsPORequired],
                 [LocationID],
                 [ModifiedDT],
                 [Name],
                 [NonRefundableCredit],
                 [ParentID],
                 [PaymentTermsID],
                 [RefundableCredit],
                 [SourceID],
                 [StatusID],
                 [Tags],
                 [TaxGroupID],
                 [TeamID],
                 [TimeZoneID]
        FROM     [dbo].[Company.Data]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Company.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Company.Data]', N'Company.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Company1]', N'PK_Company', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Company.Data_Name]
    ON [dbo].[Company.Data]([BID] ASC, [Name] ASC, [StatusID] ASC, [IsActive] ASC);
CREATE NONCLUSTERED INDEX [IX_Company.Data_Parent]
    ON [dbo].[Company.Data]([BID] ASC, [ParentID] ASC);
CREATE NONCLUSTERED INDEX [IX_Company.Data_Status]
    ON [dbo].[Company.Data]([BID] ASC, [StatusID] ASC, [IsActive] ASC, [Name] ASC);
CREATE NONCLUSTERED INDEX [IX_Company.Data_Team]
    ON [dbo].[Company.Data]([BID] ASC, [TeamID] ASC, [Name] ASC, [IsActive] ASC);


GO
/*
The type for column SortIndex in table [dbo].[Company.Locator] is currently  SMALLINT NOT NULL but is being changed to  TINYINT NOT NULL. Data loss could occur.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Company.Locator]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Company.Locator] (
    [BID]            SMALLINT        NOT NULL,
    [ID]             INT             NOT NULL,
    [ClassTypeID]    AS              ((2003)),
    [ModifiedDT]     DATETIME2 (2)   NOT NULL,
    [ParentID]       INT             NOT NULL,
    [LocatorType]    TINYINT         NOT NULL,
    [LocatorSubType] TINYINT         NULL,
    [Locator]        NVARCHAR (2014) NULL,
    [MetaData]       XML             NULL,
    [RawInput]       NVARCHAR (1024) NULL,
    [SortIndex]      TINYINT         NOT NULL,
    [IsValid]        BIT             CONSTRAINT [DF_Company.Locator_IsValid] DEFAULT ((0)) NOT NULL,
    [IsVerified]     BIT             CONSTRAINT [DF_Company.Locator_IsVerified] DEFAULT ((0)) NOT NULL,
    [HasImage]       BIT             CONSTRAINT [DF_Company.Locator_HasImage] DEFAULT ((0)) NOT NULL,
    [IsPrimary]      AS              (CASE WHEN [SortIndex] = (1) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_Company.Locator1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Company.Locator])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Company.Locator] ([BID], [ID], [HasImage], [IsValid], [IsVerified], [Locator], [LocatorSubType], [LocatorType], [MetaData], [ModifiedDT], [ParentID], [RawInput], [SortIndex])
        SELECT   [BID],
                 [ID],
                 [HasImage],
                 [IsValid],
                 [IsVerified],
                 [Locator],
                 [LocatorSubType],
                 [LocatorType],
                 [MetaData],
                 [ModifiedDT],
                 [ParentID],
                 [RawInput],
                 [SortIndex]
        FROM     [dbo].[Company.Locator]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Company.Locator];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Company.Locator]', N'Company.Locator';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Company.Locator1]', N'PK_Company.Locator', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Company.Locator_Locator]
    ON [dbo].[Company.Locator]([BID] ASC, [LocatorType] ASC, [LocatorSubType] ASC, [ParentID] ASC);
CREATE NONCLUSTERED INDEX [IX_Company.Locator_Parent]
    ON [dbo].[Company.Locator]([BID] ASC, [ParentID] ASC, [LocatorType] ASC);
CREATE PRIMARY XML INDEX [XML_IX_Company.Locator]
    ON [dbo].[Company.Locator]([MetaData])
    WITH (PAD_INDEX = OFF);


GO
/*
The column IsActive on table [dbo].[Contact.Data] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Contact.Data]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Contact.Data] (
    [BID]             SMALLINT              NOT NULL,
    [ID]              INT                   NOT NULL,
    [ClassTypeID]     AS                    ((3000)),
    [CreatedDate]     DATE                  CONSTRAINT [DF_Contact.Data_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]      DATETIME2 (2)         CONSTRAINT [DF_Contact.Data_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]        BIT                   CONSTRAINT [DF_Contact.Data_IsActive] DEFAULT ((1)) NOT NULL,
    [CompanyID]       INT                   NOT NULL,
    [IsDefault]       BIT                   CONSTRAINT [DF_Contact.Data_IsDefault] DEFAULT ((0)) NOT NULL,
    [IsBilling]       BIT                   CONSTRAINT [DF_Contact.Data_IsBilling] DEFAULT ((0)) NOT NULL,
    [Prefix]          NVARCHAR (16) SPARSE  NULL,
    [First]           NVARCHAR (100)        NULL,
    [Middle]          NVARCHAR (100) SPARSE NULL,
    [Last]            NVARCHAR (100)        NULL,
    [Suffix]          NVARCHAR (16) SPARSE  NULL,
    [NickName]        NVARCHAR (100) SPARSE NULL,
    [ShortName]       AS                    (ltrim(rtrim(concat(COALESCE ([First], [NickName]) + ' ', [Last])))),
    [LongName]        AS                    (ltrim(rtrim(concat([Prefix] + ' ', [First] + ' ', LEFT([Middle], (1)) + '. ', [Last] + ' ', [Suffix])))),
    [Position]        NVARCHAR (100) SPARSE NULL,
    [GenderType]      BIT                   NULL,
    [Department]      NVARCHAR (100) SPARSE NULL,
    [BirthDate]       AS                    (datefromparts([BirthYear], [BirthMonth], [BirthDayOfMonth])),
    [BirthDayOfMonth] TINYINT SPARSE        NULL,
    [BirthMonth]      TINYINT SPARSE        NULL,
    [BirthYear]       SMALLINT SPARSE       NULL,
    [TimeZoneID]      SMALLINT              NULL,
    [LocationID]      TINYINT               NULL,
    [HasImage]        BIT                   CONSTRAINT [DF_Contact.Data_HasImage] DEFAULT ((0)) NOT NULL,
    [Tags]            NVARCHAR (MAX)        NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Contact.Data1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Contact.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Contact.Data] ([BID], [ID], [BirthDayOfMonth], [BirthMonth], [BirthYear], [CompanyID], [CreatedDate], [Department], [First], [GenderType], [HasImage], [IsActive], [IsBilling], [IsDefault], [Last], [LocationID], [Middle], [ModifiedDT], [NickName], [Position], [Prefix], [Suffix], [Tags], [TimeZoneID])
        SELECT   [BID],
                 [ID],
                 [BirthDayOfMonth],
                 [BirthMonth],
                 [BirthYear],
                 [CompanyID],
                 [CreatedDate],
                 [Department],
                 [First],
                 [GenderType],
                 [HasImage],
                 [IsActive],
                 [IsBilling],
                 [IsDefault],
                 [Last],
                 [LocationID],
                 [Middle],
                 [ModifiedDT],
                 [NickName],
                 [Position],
                 [Prefix],
                 [Suffix],
                 [Tags],
                 [TimeZoneID]
        FROM     [dbo].[Contact.Data]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Contact.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Contact.Data]', N'Contact.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Contact.Data1]', N'PK_Contact.Data', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Contact.Data_Company]
    ON [dbo].[Contact.Data]([BID] ASC, [CompanyID] ASC, [IsDefault] ASC, [IsBilling] ASC);
CREATE NONCLUSTERED INDEX [IX_Contact.Data_Last]
    ON [dbo].[Contact.Data]([BID] ASC, [Last] ASC);
CREATE NONCLUSTERED INDEX [IX_Contact.Data_Short]
    ON [dbo].[Contact.Data]([BID] ASC, [ShortName] ASC);


GO
/*
The type for column SortIndex in table [dbo].[Contact.Locator] is currently  SMALLINT NOT NULL but is being changed to  TINYINT NOT NULL. Data loss could occur.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Contact.Locator]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Contact.Locator] (
    [BID]            SMALLINT        NOT NULL,
    [ID]             INT             NOT NULL,
    [ClassTypeID]    AS              ((3003)),
    [ModifiedDT]     DATETIME2 (2)   NOT NULL,
    [ParentID]       INT             NOT NULL,
    [LocatorType]    TINYINT         NOT NULL,
    [LocatorSubType] TINYINT         NULL,
    [Locator]        NVARCHAR (1024) NULL,
    [MetaData]       XML             NULL,
    [RawInput]       NVARCHAR (1024) NULL,
    [SortIndex]      TINYINT         NOT NULL,
    [IsValid]        BIT             CONSTRAINT [DF_Contact.Locator_IsValid] DEFAULT ((0)) NOT NULL,
    [IsVerified]     BIT             CONSTRAINT [DF_Contact.Locator_IsVerified] DEFAULT ((0)) NOT NULL,
    [HasImage]       BIT             CONSTRAINT [DF_Contact.Locator_HasImage_1] DEFAULT ((0)) NOT NULL,
    [IsPrimary]      AS              (CASE WHEN [SortIndex] = (1) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_Contact.Locator1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Contact.Locator])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Contact.Locator] ([BID], [ID], [HasImage], [IsValid], [IsVerified], [Locator], [LocatorSubType], [LocatorType], [MetaData], [ModifiedDT], [ParentID], [RawInput], [SortIndex])
        SELECT   [BID],
                 [ID],
                 [HasImage],
                 [IsValid],
                 [IsVerified],
                 [Locator],
                 [LocatorSubType],
                 [LocatorType],
                 [MetaData],
                 [ModifiedDT],
                 [ParentID],
                 [RawInput],
                 [SortIndex]
        FROM     [dbo].[Contact.Locator]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Contact.Locator];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Contact.Locator]', N'Contact.Locator';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Contact.Locator1]', N'PK_Contact.Locator', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Contact.Locator]
    ON [dbo].[Contact.Locator]([BID] ASC, [LocatorType] ASC, [LocatorSubType] ASC, [ParentID] ASC);
CREATE NONCLUSTERED INDEX [IX_Contact.Locator_Parent]
    ON [dbo].[Contact.Locator]([BID] ASC, [ParentID] ASC);


GO
/*
The column IsActive on table [dbo].[CRM.CustomField.Def] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[CRM.CustomField.Def]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CRM.CustomField.Def] (
    [BID]                  SMALLINT      NOT NULL,
    [ID]                   SMALLINT      NOT NULL,
    [ClassTypeID]          AS            ((9100)),
    [ModifiedDT]           DATETIME2 (2) CONSTRAINT [DF_CRM.CustomFields_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]             BIT           CONSTRAINT [DF_CRM.CustomFields_IsActive] DEFAULT ((1)) NOT NULL,
    [AppliesToClassTypeID] INT           NOT NULL,
    [FieldName]            VARCHAR (100) NOT NULL,
    [DataType]             TINYINT       NOT NULL,
    [InputType]            TINYINT       NOT NULL,
    [MetaData]             XML           NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_CRM.CustomFields1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CRM.CustomField.Def])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_CRM.CustomField.Def] ([BID], [ID], [AppliesToClassTypeID], [DataType], [FieldName], [InputType], [IsActive], [MetaData], [ModifiedDT])
        SELECT   [BID],
                 [ID],
                 [AppliesToClassTypeID],
                 [DataType],
                 [FieldName],
                 [InputType],
                 [IsActive],
                 [MetaData],
                 [ModifiedDT]
        FROM     [dbo].[CRM.CustomField.Def]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[CRM.CustomField.Def];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CRM.CustomField.Def]', N'CRM.CustomField.Def';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CRM.CustomFields1]', N'PK_CRM.CustomFields', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
/*
The column IsActive on table [dbo].[CRM.CustomField.Helper] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[CRM.CustomField.Helper]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CRM.CustomField.Helper] (
    [BID]            SMALLINT        NOT NULL,
    [ID]             SMALLINT        NOT NULL,
    [ClassTypeID]    AS              ((9101)),
    [ModifiedDT]     DATETIME2 (2)   CONSTRAINT [DF_CRM.Setup.CustomField.Helper_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]       BIT             CONSTRAINT [DF_CRM.Setup.CustomField.Helper_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]           VARCHAR (255)   NOT NULL,
    [HelperType]     TINYINT         NOT NULL,
    [DataType]       TINYINT         NOT NULL,
    [RegEx]          VARCHAR (MAX)   NULL,
    [ComparisonType] TINYINT         NULL,
    [CompareValue]   VARCHAR (1024)  NULL,
    [MaxValue]       DECIMAL (18, 4) NULL,
    [MinValue]       DECIMAL (18, 4) NULL,
    [IsWarningOnly]  BIT             CONSTRAINT [DF_CRM.Setup.CustomField.Helper_IsBlocking] DEFAULT ((0)) NOT NULL,
    [Message]        VARCHAR (1024)  NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_CRM.Setup.CustomField.Helper1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CRM.CustomField.Helper])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_CRM.CustomField.Helper] ([BID], [ID], [CompareValue], [ComparisonType], [DataType], [HelperType], [IsActive], [IsWarningOnly], [MaxValue], [Message], [MinValue], [ModifiedDT], [Name], [RegEx])
        SELECT   [BID],
                 [ID],
                 [CompareValue],
                 [ComparisonType],
                 [DataType],
                 [HelperType],
                 [IsActive],
                 [IsWarningOnly],
                 [MaxValue],
                 [Message],
                 [MinValue],
                 [ModifiedDT],
                 [Name],
                 [RegEx]
        FROM     [dbo].[CRM.CustomField.Helper]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[CRM.CustomField.Helper];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CRM.CustomField.Helper]', N'CRM.CustomField.Helper';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CRM.Setup.CustomField.Helper1]', N'PK_CRM.Setup.CustomField.Helper', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
/*
The column IsActive on table [dbo].[CRM.Industry] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[CRM.Industry]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CRM.Industry] (
    [BID]         SMALLINT       NOT NULL,
    [ID]          SMALLINT       NOT NULL,
    [ClassTypeID] AS             ((2011)),
    [ModifiedDT]  DATETIME2 (2)  CONSTRAINT [DF_CRM.Industry_ModifiedDT] DEFAULT (getdate()) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_CRM.Industry_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]        NVARCHAR (255) NOT NULL,
    [IsLocked]    BIT            CONSTRAINT [DF_CRM.Industry_IsLocked] DEFAULT ((0)) NOT NULL,
    [ParentID]    SMALLINT       NULL,
    [IsTopLevel]  AS             (CASE WHEN [ParentID] IS NULL THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_CRM.Industry1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CRM.Industry])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_CRM.Industry] ([BID], [ID], [IsActive], [IsLocked], [ModifiedDT], [Name], [ParentID])
        SELECT   [BID],
                 [ID],
                 [IsActive],
                 [IsLocked],
                 [ModifiedDT],
                 [Name],
                 [ParentID]
        FROM     [dbo].[CRM.Industry]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[CRM.Industry];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CRM.Industry]', N'CRM.Industry';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CRM.Industry1]', N'PK_CRM.Industry', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_CRM.Industry_Name]
    ON [dbo].[CRM.Industry]([BID] ASC, [Name] ASC, [IsActive] ASC);
CREATE NONCLUSTERED INDEX [IX_CRM.Industry_Parent]
    ON [dbo].[CRM.Industry]([BID] ASC, [ParentID] ASC, [Name] ASC);


GO
/*
The column IsActive on table [dbo].[CRM.Source] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[CRM.Source]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CRM.Source] (
    [BID]         SMALLINT       NOT NULL,
    [ID]          SMALLINT       NOT NULL,
    [ClassTypeID] AS             ((2012)),
    [ModifiedDT]  DATETIME2 (2)  CONSTRAINT [DF_CRM.Setup.Source_ModifiedDT] DEFAULT (getdate()) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_CRM.Setup.Source_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [IsLocked]    BIT            CONSTRAINT [DF_CRM.Source_IsLocked] DEFAULT ((0)) NOT NULL,
    [ParentID]    SMALLINT       NULL,
    [IsTopLevel]  AS             (CASE WHEN [ParentID] IS NULL THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_CRM.Setup.Source1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CRM.Source])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_CRM.Source] ([BID], [ID], [IsActive], [IsLocked], [ModifiedDT], [Name], [ParentID])
        SELECT   [BID],
                 [ID],
                 [IsActive],
                 [IsLocked],
                 [ModifiedDT],
                 [Name],
                 [ParentID]
        FROM     [dbo].[CRM.Source]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[CRM.Source];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CRM.Source]', N'CRM.Source';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CRM.Setup.Source1]', N'PK_CRM.Setup.Source', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_CRM.Source_Name]
    ON [dbo].[CRM.Source]([BID] ASC, [Name] ASC, [IsActive] ASC);
CREATE NONCLUSTERED INDEX [IX_CRM.Source_Parent]
    ON [dbo].[CRM.Source]([BID] ASC, [ParentID] ASC, [Name] ASC);
ALTER TABLE [dbo].[Employee.Custom] DROP COLUMN [ClassTypeID];


GO
ALTER TABLE [dbo].[Employee.Custom]
    ADD [ClassTypeID] AS ((5001));


GO
/*
The column IsActive on table [dbo].[Employee.Data] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Employee.Data]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Employee.Data] (
    [BID]             SMALLINT              NOT NULL,
    [ID]              SMALLINT              NOT NULL,
    [ClassTypeID]     AS                    ((5000)),
    [CreatedDate]     DATE                  CONSTRAINT [DF_Employee.Data_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]      DATETIME2 (2)         CONSTRAINT [DF_Employee.Data_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]        BIT                   CONSTRAINT [DF_Employee.Data_IsActive] DEFAULT ((1)) NOT NULL,
    [Prefix]          NVARCHAR (16) SPARSE  NULL,
    [First]           NVARCHAR (100)        NOT NULL,
    [Middle]          NVARCHAR (100) SPARSE NULL,
    [Last]            NVARCHAR (100)        NOT NULL,
    [Suffix]          NVARCHAR (16) SPARSE  NULL,
    [NickName]        NVARCHAR (100) SPARSE NULL,
    [ShortName]       AS                    (ltrim(rtrim(concat(COALESCE ([First], [NickName]) + ' ', [Last])))),
    [LongName]        AS                    (ltrim(rtrim(concat([Prefix] + ' ', [First] + ' ', LEFT([Middle], (1)) + '. ', [Last] + ' ', [Suffix])))),
    [Position]        NVARCHAR (128)        NULL,
    [GenderType]      BIT                   NULL,
    [HireDate]        DATE                  NULL,
    [ReleaseDate]     DATE                  NULL,
    [BirthDate]       AS                    (datefromparts([BirthYear], [BirthMonth], [BirthDayOfMonth])) PERSISTED,
    [BirthDayOfMonth] TINYINT               NULL,
    [BirthMonth]      TINYINT               NULL,
    [BirthYear]       SMALLINT              NULL,
    [TimeZoneID]      SMALLINT SPARSE       NULL,
    [LocationID]      TINYINT               CONSTRAINT [DF_Employee.Data_LocationID] DEFAULT ((1)) NOT NULL,
    [HasImage]        BIT                   CONSTRAINT [DF_Employee.Data_HasImage] DEFAULT ((0)) NOT NULL,
    [ReportsToID]     SMALLINT              NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Employee.Data1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Employee.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Employee.Data] ([BID], [ID], [BirthDayOfMonth], [BirthMonth], [BirthYear], [CreatedDate], [First], [GenderType], [HasImage], [HireDate], [IsActive], [Last], [LocationID], [Middle], [ModifiedDT], [NickName], [Position], [Prefix], [ReleaseDate], [ReportsToID], [Suffix], [TimeZoneID])
        SELECT   [BID],
                 [ID],
                 [BirthDayOfMonth],
                 [BirthMonth],
                 [BirthYear],
                 [CreatedDate],
                 [First],
                 [GenderType],
                 [HasImage],
                 [HireDate],
                 [IsActive],
                 [Last],
                 [LocationID],
                 [Middle],
                 [ModifiedDT],
                 [NickName],
                 [Position],
                 [Prefix],
                 [ReleaseDate],
                 [ReportsToID],
                 [Suffix],
                 [TimeZoneID]
        FROM     [dbo].[Employee.Data]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Employee.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Employee.Data]', N'Employee.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Employee.Data1]', N'PK_Employee.Data', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Employee.Data_LastName]
    ON [dbo].[Employee.Data]([BID] ASC, [Last] ASC, [First] ASC);
CREATE NONCLUSTERED INDEX [IX_Employee.DataLocation]
    ON [dbo].[Employee.Data]([BID] ASC, [LocationID] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Employee.Locator] (
    [BID]            SMALLINT        NOT NULL,
    [ID]             INT             NOT NULL,
    [ClassTypeID]    AS              ((5003)),
    [ModifiedDT]     DATETIME2 (2)   NOT NULL,
    [ParentID]       SMALLINT        NOT NULL,
    [LocatorType]    TINYINT         NOT NULL,
    [LocatorSubType] TINYINT         NULL,
    [Locator]        NVARCHAR (2014) NULL,
    [MetaData]       XML             NULL,
    [RawInput]       NVARCHAR (1024) NULL,
    [SortIndex]      SMALLINT        CONSTRAINT [df_Emp_Loc_sort_default] DEFAULT ((2)) NOT NULL,
    [IsValid]        BIT             CONSTRAINT [df_Emp_Loc_isvalid_default] DEFAULT ((0)) NOT NULL,
    [IsVerified]     BIT             CONSTRAINT [df_Emp_isverified_sort_default] DEFAULT ((0)) NOT NULL,
    [HasImage]       BIT             CONSTRAINT [DF_Employee.Locator_HasImage] DEFAULT ((0)) NOT NULL,
    [IsPrimary]      AS              (CASE WHEN [SortIndex] = (1) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_Employee.Locator1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Employee.Locator])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Employee.Locator] ([BID], [ID], [HasImage], [IsValid], [IsVerified], [Locator], [LocatorSubType], [LocatorType], [MetaData], [ModifiedDT], [ParentID], [RawInput], [SortIndex])
        SELECT   [BID],
                 [ID],
                 [HasImage],
                 [IsValid],
                 [IsVerified],
                 [Locator],
                 [LocatorSubType],
                 [LocatorType],
                 [MetaData],
                 [ModifiedDT],
                 [ParentID],
                 [RawInput],
                 [SortIndex]
        FROM     [dbo].[Employee.Locator]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Employee.Locator];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Employee.Locator]', N'Employee.Locator';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Employee.Locator1]', N'PK_Employee.Locator', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Employee.Locator_LocatorType]
    ON [dbo].[Employee.Locator]([BID] ASC, [LocatorType] ASC, [LocatorSubType] ASC);
CREATE NONCLUSTERED INDEX [IX_Employee.Locator_parent]
    ON [dbo].[Employee.Locator]([BID] ASC, [ParentID] ASC, [LocatorType] ASC);
CREATE PRIMARY XML INDEX [XML_IX_Employee.Locator]
    ON [dbo].[Employee.Locator]([MetaData])
    WITH (PAD_INDEX = OFF);


GO
/*
The column IsActive on table [dbo].[Employee.Team] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Employee.Team]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Employee.Team] (
    [BID]         SMALLINT       NOT NULL,
    [ID]          INT            NOT NULL,
    [ClassTypeID] AS             ((5020)),
    [CreatedDate] DATE           CONSTRAINT [DF_Employee.Team_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]  DATETIME2 (2)  CONSTRAINT [DF_Employee.Team_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]    BIT            CONSTRAINT [DF_Employee.Team_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]        NVARCHAR (100) NOT NULL,
    [HasImage]    BIT            CONSTRAINT [DF_Employee.Team_HasImage] DEFAULT ((0)) NOT NULL,
    [IsAdHocTeam] BIT            CONSTRAINT [DF_Employee.Team_IsAdHocTeam] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Employee.Team1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Employee.Team])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Employee.Team] ([BID], [ID], [CreatedDate], [HasImage], [IsActive], [IsAdHocTeam], [ModifiedDT], [Name])
        SELECT   [BID],
                 [ID],
                 [CreatedDate],
                 [HasImage],
                 [IsActive],
                 [IsAdHocTeam],
                 [ModifiedDT],
                 [Name]
        FROM     [dbo].[Employee.Team]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Employee.Team];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Employee.Team]', N'Employee.Team';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Employee.Team1]', N'PK_Employee.Team', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Employee.TeamLink] (
    [BID]        SMALLINT NOT NULL,
    [TeamID]     INT      NOT NULL,
    [EmployeeID] SMALLINT NOT NULL,
    [RoleType]   TINYINT  CONSTRAINT [DF_Employee.TeamLink_RoleType] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Employee.TeamLink1] PRIMARY KEY CLUSTERED ([BID] ASC, [EmployeeID] ASC, [TeamID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Employee.TeamLink])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Employee.TeamLink] ([BID], [EmployeeID], [TeamID], [RoleType])
        SELECT   [BID],
                 [EmployeeID],
                 [TeamID],
                 [RoleType]
        FROM     [dbo].[Employee.TeamLink]
        ORDER BY [BID] ASC, [EmployeeID] ASC, [TeamID] ASC;
    END

DROP TABLE [dbo].[Employee.TeamLink];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Employee.TeamLink]', N'Employee.TeamLink';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Employee.TeamLink1]', N'PK_Employee.TeamLink', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Employee.TeamLink_Employee]
    ON [dbo].[Employee.TeamLink]([BID] ASC, [EmployeeID] ASC, [TeamID] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.ClassType] (
    [ID]          SMALLINT       NOT NULL,
    [Name]        VARCHAR (255)  NOT NULL,
    [Description] VARCHAR (2048) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_enum.ClassType1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.ClassType])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.ClassType] ([ID], [Description], [Name])
        SELECT   [ID],
                 [Description],
                 [Name]
        FROM     [dbo].[enum.ClassType]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[enum.ClassType];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.ClassType]', N'enum.ClassType';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_enum.ClassType1]', N'PK_enum.ClassType', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.CustomField.InputType] (
    [ID]       TINYINT       NOT NULL,
    [Name]     VARCHAR (255) NOT NULL,
    [DataType] TINYINT       NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_enum.CustomFieldSubType1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.CustomField.InputType])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.CustomField.InputType] ([ID], [DataType], [Name])
        SELECT   [ID],
                 [DataType],
                 [Name]
        FROM     [dbo].[enum.CustomField.InputType]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[enum.CustomField.InputType];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.CustomField.InputType]', N'enum.CustomField.InputType';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_enum.CustomFieldSubType1]', N'PK_enum.CustomFieldSubType', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.Locator.SubType] (
    [LocatorType]      TINYINT       NOT NULL,
    [ID]               TINYINT       NOT NULL,
    [Name]             VARCHAR (255) NOT NULL,
    [LinkFormatString] VARCHAR (255) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_enum.LocatorSubType1] PRIMARY KEY CLUSTERED ([LocatorType] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.Locator.SubType])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.Locator.SubType] ([LocatorType], [ID], [LinkFormatString], [Name])
        SELECT   [LocatorType],
                 [ID],
                 [LinkFormatString],
                 [Name]
        FROM     [dbo].[enum.Locator.SubType]
        ORDER BY [LocatorType] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[enum.Locator.SubType];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.Locator.SubType]', N'enum.Locator.SubType';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_enum.LocatorSubType1]', N'PK_enum.LocatorSubType', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.Locator.Type] (
    [ID]                   TINYINT       NOT NULL,
    [Name]                 VARCHAR (255) NOT NULL,
    [ValidityRegEx]        VARCHAR (MAX) NULL,
    [PossibilityRegEx]     VARCHAR (MAX) NULL,
    [PossibilitySortOrder] TINYINT       NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_enum.LocatorType1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.Locator.Type])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.Locator.Type] ([ID], [Name], [PossibilityRegEx], [PossibilitySortOrder], [ValidityRegEx])
        SELECT   [ID],
                 [Name],
                 [PossibilityRegEx],
                 [PossibilitySortOrder],
                 [ValidityRegEx]
        FROM     [dbo].[enum.Locator.Type]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[enum.Locator.Type];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.Locator.Type]', N'enum.Locator.Type';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_enum.LocatorType1]', N'PK_enum.LocatorType', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
/*
The column IsCommon on table [dbo].[enum.TimeZone] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[enum.TimeZone]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.TimeZone] (
    [ID]           SMALLINT       NOT NULL,
    [Name]         VARCHAR (100)  NOT NULL,
    [StandardName] VARCHAR (100)  NOT NULL,
    [UTCOffset]    DECIMAL (6, 2) NOT NULL,
    [IsCommon]     BIT            CONSTRAINT [DF_enum.TimeZone_IsCommon] DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.TimeZone])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.TimeZone] ([ID], [IsCommon], [Name], [StandardName], [UTCOffset])
        SELECT   [ID],
                 [IsCommon],
                 [Name],
                 [StandardName],
                 [UTCOffset]
        FROM     [dbo].[enum.TimeZone]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[enum.TimeZone];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.TimeZone]', N'enum.TimeZone';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_enum.TimeZone_DisplayName]
    ON [dbo].[enum.TimeZone]([Name] ASC);
CREATE NONCLUSTERED INDEX [IX_enum.TimeZone_StandardName]
    ON [dbo].[enum.TimeZone]([StandardName] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_List.Filter] (
    [BID]               SMALLINT             NOT NULL,
    [ID]                INT                  NOT NULL,
    [ClassTypeID]       AS                   ((1700)),
    [CreatedDate]       DATE                 CONSTRAINT [DF_List.Filter_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]        DATETIME2 (2)        CONSTRAINT [DF_List.Filter_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]          BIT                  CONSTRAINT [DF_List.Filter_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]              VARCHAR (255)        NOT NULL,
    [TargetClassTypeID] INT                  NOT NULL,
    [IDs]               XML                  NULL,
    [Criteria]          XML                  NULL,
    [OwnerID]           SMALLINT             NULL,
    [IsDynamic]         AS                   (isnull(CONVERT (BIT, CASE WHEN [IDs] IS NULL THEN (1) ELSE (0) END), (1))),
    [IsPublic]          BIT                  CONSTRAINT [DF_List.Filter_IsPublic] DEFAULT ((0)) NOT NULL,
    [IsSystem]          BIT                  CONSTRAINT [DF_List.Filter_IsSystem] DEFAULT ((0)) NOT NULL,
    [Hint]              VARCHAR (MAX) SPARSE NULL,
    [IsDefault]         BIT                  CONSTRAINT [DF_List.Filter_IsDefault] DEFAULT ((0)) NOT NULL,
    [SortIndex]         TINYINT              CONSTRAINT [DF_List.Filter_SortIndex] DEFAULT ((50)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_List.Filter1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[List.Filter])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_List.Filter] ([BID], [ID], [CreatedDate], [Criteria], [Hint], [IDs], [IsActive], [IsDefault], [IsPublic], [IsSystem], [ModifiedDT], [Name], [OwnerID], [SortIndex], [TargetClassTypeID])
        SELECT   [BID],
                 [ID],
                 [CreatedDate],
                 [Criteria],
                 [Hint],
                 [IDs],
                 [IsActive],
                 [IsDefault],
                 [IsPublic],
                 [IsSystem],
                 [ModifiedDT],
                 [Name],
                 [OwnerID],
                 [SortIndex],
                 [TargetClassTypeID]
        FROM     [dbo].[List.Filter]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[List.Filter];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_List.Filter]', N'List.Filter';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_List.Filter1]', N'PK_List.Filter', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_List.Filter_TargetCTID]
    ON [dbo].[List.Filter]([TargetClassTypeID] ASC, [IsSystem] ASC, [BID] ASC);


GO
/*
The column IsActive on table [dbo].[Location.Data] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Location.Data]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Location.Data] (
    [BID]               SMALLINT       NOT NULL,
    [ID]                TINYINT        NOT NULL,
    [ClassTypeID]       AS             ((1005)),
    [CreatedDate]       DATE           CONSTRAINT [DF_Location.Data_CreatedDT] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]        DATETIME2 (2)  CONSTRAINT [DF_Location.Data_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]          BIT            CONSTRAINT [DF_Location.Data_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]              NVARCHAR (255) NOT NULL,
    [IsDefault]         BIT            CONSTRAINT [DF_Location.Data_IsDefault] DEFAULT ((0)) NOT NULL,
    [TimeZoneID]        SMALLINT       NULL,
    [UseDST]            BIT            NULL,
    [LegalName]         NVARCHAR (255) NULL,
    [DBA]               NVARCHAR (255) NULL,
    [InvoicePrefix]     NVARCHAR (10)  NULL,
    [EstimatePrefix]    NVARCHAR (10)  NULL,
    [POPrefix]          NVARCHAR (10)  NULL,
    [TaxID]             NVARCHAR (100) NULL,
    [DefaultTaxGroupID] SMALLINT       NULL,
    [DefaultAreaCode]   NVARCHAR (10)  NULL,
    [Slogan]            NVARCHAR (255) NULL,
    [HasImage]          BIT            CONSTRAINT [DF__Business.__HasIm__160F4887] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Location.Data_Name1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Location.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Location.Data] ([BID], [ID], [CreatedDate], [DBA], [DefaultAreaCode], [DefaultTaxGroupID], [EstimatePrefix], [HasImage], [InvoicePrefix], [IsActive], [IsDefault], [LegalName], [ModifiedDT], [Name], [POPrefix], [Slogan], [TaxID], [TimeZoneID], [UseDST])
        SELECT   [BID],
                 [ID],
                 [CreatedDate],
                 [DBA],
                 [DefaultAreaCode],
                 [DefaultTaxGroupID],
                 [EstimatePrefix],
                 [HasImage],
                 [InvoicePrefix],
                 [IsActive],
                 [IsDefault],
                 [LegalName],
                 [ModifiedDT],
                 [Name],
                 [POPrefix],
                 [Slogan],
                 [TaxID],
                 [TimeZoneID],
                 [UseDST]
        FROM     [dbo].[Location.Data]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Location.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Location.Data]', N'Location.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Location.Data_Name1]', N'PK_Location.Data_Name', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE UNIQUE NONCLUSTERED INDEX [IX_Location.Data_Name]
    ON [dbo].[Location.Data]([BID] ASC, [Name] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Location.Locator] (
    [BID]            SMALLINT        NOT NULL,
    [ID]             INT             NOT NULL,
    [ClassTypeID]    AS              ((1008)),
    [ParentID]       TINYINT         NOT NULL,
    [LocatorType]    TINYINT         NOT NULL,
    [Locator]        NVARCHAR (1024) NULL,
    [LocatorSubType] TINYINT         NULL,
    [MetaData]       XML             NULL,
    [ModifiedDT]     DATETIME2 (2)   NULL,
    [RawInput]       NVARCHAR (1024) NOT NULL,
    [SortIndex]      SMALLINT        CONSTRAINT [df_Business_Location_Loc_sort_default] DEFAULT ((0)) NOT NULL,
    [IsValid]        BIT             CONSTRAINT [df_Business_Location_Loc_isvalid_default] DEFAULT ((0)) NOT NULL,
    [IsVerified]     BIT             CONSTRAINT [df_Business_Location_isverified_sort_default] DEFAULT ((0)) NOT NULL,
    [HasImage]       BIT             CONSTRAINT [DF__Business.__HasIm__1AD3FDA4] DEFAULT ((0)) NOT NULL,
    [IsPrimary]      AS              (CASE WHEN [SortIndex] = (1) THEN CONVERT (BIT, (1)) ELSE CONVERT (BIT, (0)) END),
    CONSTRAINT [tmp_ms_xx_constraint_PK_Location.Locator1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Location.Locator])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Location.Locator] ([BID], [ID], [HasImage], [IsValid], [IsVerified], [Locator], [LocatorSubType], [LocatorType], [MetaData], [ModifiedDT], [ParentID], [RawInput], [SortIndex])
        SELECT   [BID],
                 [ID],
                 [HasImage],
                 [IsValid],
                 [IsVerified],
                 [Locator],
                 [LocatorSubType],
                 [LocatorType],
                 [MetaData],
                 [ModifiedDT],
                 [ParentID],
                 [RawInput],
                 [SortIndex]
        FROM     [dbo].[Location.Locator]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Location.Locator];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Location.Locator]', N'Location.Locator';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Location.Locator1]', N'PK_Location.Locator', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Location.Locator_LocatorType]
    ON [dbo].[Location.Locator]([BID] ASC, [LocatorType] ASC, [LocatorSubType] ASC, [ParentID] ASC);
CREATE NONCLUSTERED INDEX [IX_Location.Locator_Parent]
    ON [dbo].[Location.Locator]([BID] ASC, [ParentID] ASC);


GO
/*
The column IsActive on table [dbo].[Opportunity.Data] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Opportunity.Data]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Opportunity.Data] (
    [BID]            SMALLINT       NOT NULL,
    [ID]             INT            NOT NULL,
    [ClassTypeID]    AS             ((9000)),
    [CreatedDate]    DATE           CONSTRAINT [DF_OpportunityTemplate_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]     DATETIME2 (2)  CONSTRAINT [DF_OpportunityTemplate_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]       BIT            CONSTRAINT [DF_OpportunityTemplate_IsActive] DEFAULT ((1)) NOT NULL,
    [Name]           NVARCHAR (255) NOT NULL,
    [Amount]         MONEY          NULL,
    [Probability]    DECIMAL (9, 4) NULL,
    [WeightedAmount] AS             (CONVERT (MONEY, ([Amount] * [Probability]) / (100))),
    [StatusID]       TINYINT        CONSTRAINT [DF_Opportunity.Data_OpportunityStatusID] DEFAULT ((0)) NOT NULL,
    [ExpectedDate]   DATE           NULL,
    [ClosedDate]     DATE           NULL,
    [Description]    NVARCHAR (255) NULL,
    [Notes]          NVARCHAR (MAX) NULL,
    [CompanyID]      INT            NULL,
    [ContactID]      INT            NULL,
    [TeamID]         INT            NULL,
    [CampaignID]     INT            NULL,
    [SourceID]       INT            NULL,
    [LocationID]     TINYINT        CONSTRAINT [DF_Opportunity.Data_LocationID] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_OpportunityTemplate1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Opportunity.Data])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Opportunity.Data] ([BID], [ID], [Amount], [CampaignID], [ClosedDate], [CompanyID], [ContactID], [CreatedDate], [Description], [ExpectedDate], [IsActive], [LocationID], [ModifiedDT], [Name], [Notes], [Probability], [SourceID], [StatusID], [TeamID])
        SELECT   [BID],
                 [ID],
                 [Amount],
                 [CampaignID],
                 [ClosedDate],
                 [CompanyID],
                 [ContactID],
                 [CreatedDate],
                 [Description],
                 [ExpectedDate],
                 [IsActive],
                 [LocationID],
                 [ModifiedDT],
                 [Name],
                 [Notes],
                 [Probability],
                 [SourceID],
                 [StatusID],
                 [TeamID]
        FROM     [dbo].[Opportunity.Data]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Opportunity.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Opportunity.Data]', N'Opportunity.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OpportunityTemplate1]', N'PK_OpportunityTemplate', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Opportunity_Company]
    ON [dbo].[Opportunity.Data]([BID] ASC, [CompanyID] ASC) WHERE ([CompanyID] IS NOT NULL);
CREATE NONCLUSTERED INDEX [IX_Opportunity_Contact]
    ON [dbo].[Opportunity.Data]([BID] ASC, [ContactID] ASC) WHERE ([ContactID] IS NOT NULL);


GO
/*
The column IsActive on table [dbo].[Option.Data] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Option.Data]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Option.Data] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [ClassTypeID]   AS              ((1800)),
    [CreatedDate]   DATE            CONSTRAINT [DF_Option.Data2_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]    DATETIME2 (2)   CONSTRAINT [DF_Option.Data2_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]      BIT             CONSTRAINT [DF_Option.Data2_IsActive] DEFAULT ((1)) NOT NULL,
    [OptionID]      SMALLINT        NOT NULL,
    [Value]         NVARCHAR (MAX)  NOT NULL,
    [AssociationID] TINYINT SPARSE  NULL,
    [BID]           SMALLINT        NULL,
    [LocationID]    SMALLINT SPARSE NULL,
    [StoreFrontID]  SMALLINT SPARSE NULL,
    [EmployeeID]    SMALLINT SPARSE NULL,
    [CompanyID]     INT SPARSE      NULL,
    [ContactID]     INT SPARSE      NULL,
    [OptionLevel]   AS              (isnull(CONVERT (TINYINT, CASE WHEN [ContactID] IS NOT NULL THEN (64) WHEN [CompanyID] IS NOT NULL THEN (32) WHEN [EmployeeID] IS NOT NULL THEN (16) WHEN [StorefrontID] IS NOT NULL THEN (8) WHEN [LocationID] IS NOT NULL THEN (4) WHEN [BID] IS NOT NULL THEN (2) WHEN [AssociationID] IS NOT NULL THEN (1) ELSE (0) END), (0))) PERSISTED NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Option.Data1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Option.Data])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Option.Data] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Option.Data] ([ID], [AssociationID], [BID], [CompanyID], [ContactID], [CreatedDate], [EmployeeID], [IsActive], [LocationID], [ModifiedDT], [OptionID], [StoreFrontID], [Value])
        SELECT   [ID],
                 [AssociationID],
                 [BID],
                 [CompanyID],
                 [ContactID],
                 [CreatedDate],
                 [EmployeeID],
                 [IsActive],
                 [LocationID],
                 [ModifiedDT],
                 [OptionID],
                 [StoreFrontID],
                 [Value]
        FROM     [dbo].[Option.Data]
        ORDER BY [ID] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Option.Data] OFF;
    END

DROP TABLE [dbo].[Option.Data];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Option.Data]', N'Option.Data';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Option.Data1]', N'PK_Option.Data', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Option.Data_BID_OptionID_Level]
    ON [dbo].[Option.Data]([BID] ASC, [OptionID] ASC, [OptionLevel] DESC);
CREATE NONCLUSTERED INDEX [IX_Option.Data_OptionID_Level_BIDNull]
    ON [dbo].[Option.Data]([OptionID] ASC, [OptionLevel] DESC) WHERE ([BID] IS NULL);


GO
/*
The column IsActive on table [dbo].[Report.Menu] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[Report.Menu]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Report.Menu] (
    [BID]           SMALLINT       NOT NULL,
    [ID]            SMALLINT       NOT NULL,
    [ClassTypeID]   AS             ((2000)),
    [CreatedDate]   DATE           CONSTRAINT [DF_Table_1_CreatedDate] DEFAULT (getutcdate()) NOT NULL,
    [ModifiedDT]    DATETIME2 (2)  CONSTRAINT [DF_Table_1_ModifiedDT] DEFAULT (getutcdate()) NOT NULL,
    [IsActive]      BIT            CONSTRAINT [DF_Table_1_IsActive] DEFAULT ((1)) NOT NULL,
    [ReportID]      SMALLINT       NOT NULL,
    [DisplayName]   NVARCHAR (255) NOT NULL,
    [Description]   NVARCHAR (MAX) NULL,
    [ReportOptions] XML            NULL,
    [ParentID]      SMALLINT       NULL,
    [SortIndex]     SMALLINT       CONSTRAINT [DF_Reports.Menu_SortIndex] DEFAULT ((100)) NOT NULL,
    [DefaultMenuID] SMALLINT       NULL,
    [IsOV]          BIT            CONSTRAINT [DF_Reports.Menu_IsOV] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Reports.Menu1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Report.Menu])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Report.Menu] ([BID], [ID], [CreatedDate], [DefaultMenuID], [Description], [DisplayName], [IsActive], [IsOV], [ModifiedDT], [ParentID], [ReportID], [ReportOptions], [SortIndex])
        SELECT   [BID],
                 [ID],
                 [CreatedDate],
                 [DefaultMenuID],
                 [Description],
                 [DisplayName],
                 [IsActive],
                 [IsOV],
                 [ModifiedDT],
                 [ParentID],
                 [ReportID],
                 [ReportOptions],
                 [SortIndex]
        FROM     [dbo].[Report.Menu]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Report.Menu];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Report.Menu]', N'Report.Menu';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Reports.Menu1]', N'PK_Reports.Menu', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Security.Right.Group] (
    [BID]         SMALLINT      NOT NULL,
    [ID]          SMALLINT      NOT NULL,
    [ClassTypeID] AS            (CONVERT (SMALLINT, (1101))),
    [Name]        VARCHAR (255) NOT NULL,
    [IsSystem]    BIT           CONSTRAINT [DF_Security.Right.Group_IsSystem] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Security.Right.Collection1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Security.Right.Group])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Security.Right.Group] ([BID], [ID], [IsSystem], [Name])
        SELECT   [BID],
                 [ID],
                 [IsSystem],
                 [Name]
        FROM     [dbo].[Security.Right.Group]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Security.Right.Group];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Security.Right.Group]', N'Security.Right.Group';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Security.Right.Collection1]', N'PK_Security.Right.Collection', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_Security.Right.Collection_Name]
    ON [dbo].[Security.Right.Group]([BID] ASC, [Name] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_System.List.Column] (
    [ID]                SMALLINT      IDENTITY (1, 1) NOT NULL,
    [ClassTypeID]       AS            ((1711)),
    [TargetClassTypeID] INT           NULL,
    [Name]              VARCHAR (255) NOT NULL,
    [HeaderText]        VARCHAR (255) NULL,
    [Field]             VARCHAR (255) NULL,
    [IsSortable]        BIT           CONSTRAINT [DF_System.List.Column_IsSortable] DEFAULT ((0)) NOT NULL,
    [SortField]         VARCHAR (255) NULL,
    [IsFrozen]          BIT           CONSTRAINT [DF_System.List.Column_IsFrozen] DEFAULT ((0)) NOT NULL,
    [IsExpander]        BIT           CONSTRAINT [DF_System.List.Column_IsExpander] DEFAULT ((0)) NOT NULL,
    [StyleClass]        VARCHAR (255) NULL,
    [SortIndex]         TINYINT       CONSTRAINT [DF_Table_1_Index] DEFAULT ((50)) NULL,
    [IsVisible]         AS            (isnull(CASE WHEN [SortIndex] IS NULL THEN CONVERT (BIT, (0)) ELSE CONVERT (BIT, (1)) END, (0))),
    CONSTRAINT [tmp_ms_xx_constraint_PK_System.List.Column1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[System.List.Column])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_System.List.Column] ON;
        INSERT INTO [dbo].[tmp_ms_xx_System.List.Column] ([ID], [Field], [HeaderText], [IsExpander], [IsFrozen], [IsSortable], [Name], [SortField], [SortIndex], [StyleClass], [TargetClassTypeID])
        SELECT   [ID],
                 [Field],
                 [HeaderText],
                 [IsExpander],
                 [IsFrozen],
                 [IsSortable],
                 [Name],
                 [SortField],
                 [SortIndex],
                 [StyleClass],
                 [TargetClassTypeID]
        FROM     [dbo].[System.List.Column]
        ORDER BY [ID] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_System.List.Column] OFF;
    END

DROP TABLE [dbo].[System.List.Column];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_System.List.Column]', N'System.List.Column';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_System.List.Column1]', N'PK_System.List.Column', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_System.List.Column_TargetCTID]
    ON [dbo].[System.List.Column]([TargetClassTypeID] ASC, [SortIndex] ASC, [Name] ASC);


GO
/*
The column IsLimitToList on table [dbo].[System.List.Filter.Criteria] must be changed from NULL to NOT NULL. If the table contains data, the ALTER script may not work. To avoid this issue, you must add values to this column for all rows or mark it as allowing NULL values, or enable the generation of smart-defaults as a deployment option.
*/
GO
PRINT N'Starting rebuilding table [dbo].[System.List.Filter.Criteria]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_System.List.Filter.Criteria] (
    [ID]                 SMALLINT      IDENTITY (1, 1) NOT NULL,
    [ClassTypeID]        AS            ((1710)),
    [TargetClassTypeID]  INT           NULL,
    [Name]               VARCHAR (255) NOT NULL,
    [Label]              VARCHAR (255) NULL,
    [Field]              VARCHAR (255) NULL,
    [IsHidden]           BIT           CONSTRAINT [DF_System.List.Filter.Criteria_IsFrozen] DEFAULT ((0)) NOT NULL,
    [DataType]           TINYINT       NOT NULL,
    [InputType]          TINYINT       NOT NULL,
    [AllowMultiple]      BIT           CONSTRAINT [DF_System.List.Filter.Criteria_AllowMultiple] DEFAULT ((0)) NOT NULL,
    [ListValues]         VARCHAR (MAX) NULL,
    [ListValuesEndpoint] VARCHAR (255) NULL,
    [IsLimitToList]      BIT           CONSTRAINT [DF_System.List.Filter.Criteria_IsLimitToList] DEFAULT ((1)) NOT NULL,
    [SortIndex]          TINYINT       NULL,
    [IsVisible]          AS            (isnull(CASE WHEN [SortIndex] IS NULL THEN CONVERT (BIT, (0)) ELSE CONVERT (BIT, (1)) END, (0))),
    CONSTRAINT [tmp_ms_xx_constraint_PK_System.List.Filter.Criteria1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[System.List.Filter.Criteria])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_System.List.Filter.Criteria] ON;
        INSERT INTO [dbo].[tmp_ms_xx_System.List.Filter.Criteria] ([ID], [AllowMultiple], [DataType], [Field], [InputType], [IsHidden], [IsLimitToList], [Label], [ListValues], [ListValuesEndpoint], [Name], [SortIndex], [TargetClassTypeID])
        SELECT   [ID],
                 [AllowMultiple],
                 [DataType],
                 [Field],
                 [InputType],
                 [IsHidden],
                 [IsLimitToList],
                 [Label],
                 [ListValues],
                 [ListValuesEndpoint],
                 [Name],
                 [SortIndex],
                 [TargetClassTypeID]
        FROM     [dbo].[System.List.Filter.Criteria]
        ORDER BY [ID] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_System.List.Filter.Criteria] OFF;
    END

DROP TABLE [dbo].[System.List.Filter.Criteria];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_System.List.Filter.Criteria]', N'System.List.Filter.Criteria';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_System.List.Filter.Criteria1]', N'PK_System.List.Filter.Criteria', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_System.List.Filter.Criteria_TargetCTID]
    ON [dbo].[System.List.Filter.Criteria]([TargetClassTypeID] ASC, [SortIndex] ASC, [Name] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_System.Option.Category] (
    [ID]                  SMALLINT      NOT NULL,
    [Name]                VARCHAR (255) NOT NULL,
    [SectionID]           SMALLINT      NULL,
    [Description]         VARCHAR (MAX) NULL,
    [OptionLevels]        TINYINT       NOT NULL,
    [IsHidden]            BIT           CONSTRAINT [DF_System.Option.Category_IsHidden_1] DEFAULT ((0)) NOT NULL,
    [SearchTerms]         VARCHAR (MAX) NULL,
    [IsSystemOption]      AS            ((1)) PERSISTED NOT NULL,
    [IsAssociationOption] AS            (isnull(CONVERT (BIT, CASE WHEN ([OptionLevels] & (1)) <> (0) THEN (1) ELSE (0) END), (0))) PERSISTED NOT NULL,
    [IsBusinessOption]    AS            (isnull(CONVERT (BIT, CASE WHEN ([OptionLevels] & (2)) <> (0) THEN (1) ELSE (0) END), (0))) PERSISTED NOT NULL,
    [IsLocationOption]    AS            (isnull(CONVERT (BIT, CASE WHEN ([OptionLevels] & (4)) <> (0) THEN (1) ELSE (0) END), (0))) PERSISTED NOT NULL,
    [IsStorefrontOption]  AS            (isnull(CONVERT (BIT, CASE WHEN ([OptionLevels] & (8)) <> (0) THEN (1) ELSE (0) END), (0))) PERSISTED NOT NULL,
    [IsEmployeeOption]    AS            (isnull(CONVERT (BIT, CASE WHEN ([OptionLevels] & (16)) <> (0) THEN (1) ELSE (0) END), (0))) PERSISTED NOT NULL,
    [IsCompanyOption]     AS            (isnull(CONVERT (BIT, CASE WHEN ([OptionLevels] & (32)) <> (0) THEN (1) ELSE (0) END), (0))) PERSISTED NOT NULL,
    [IsContactOption]     AS            (isnull(CONVERT (BIT, CASE WHEN ([OptionLevels] & (64)) <> (0) THEN (1) ELSE (0) END), (0))) PERSISTED NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_System.Option.Category1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[System.Option.Category])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_System.Option.Category] ([ID], [Description], [IsHidden], [Name], [OptionLevels], [SearchTerms], [SectionID])
        SELECT   [ID],
                 [Description],
                 [IsHidden],
                 [Name],
                 [OptionLevels],
                 [SearchTerms],
                 [SectionID]
        FROM     [dbo].[System.Option.Category]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[System.Option.Category];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_System.Option.Category]', N'System.Option.Category';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_System.Option.Category1]', N'PK_System.Option.Category', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [[IX_System.Option.Category_SectionID]
    ON [dbo].[System.Option.Category]([SectionID] ASC, [Name] ASC, [IsHidden] ASC, [OptionLevels] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_System.Option.Definition] (
    [ID]           SMALLINT      NOT NULL,
    [Name]         VARCHAR (255) NOT NULL,
    [Label]        VARCHAR (255) NOT NULL,
    [Description]  VARCHAR (MAX) NULL,
    [DataType]     TINYINT       CONSTRAINT [DF_System.Option.Definition_DataType] DEFAULT ((0)) NOT NULL,
    [CategoryID]   SMALLINT      NOT NULL,
    [ListValues]   VARCHAR (MAX) NULL,
    [DefaultValue] VARCHAR (MAX) NULL,
    [IsHidden]     BIT           CONSTRAINT [DF_System.Option.Definition_IsHidden] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_System.Option.Definition1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[System.Option.Definition])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_System.Option.Definition] ([ID], [CategoryID], [DataType], [DefaultValue], [Description], [IsHidden], [Label], [ListValues], [Name])
        SELECT   [ID],
                 [CategoryID],
                 [DataType],
                 [DefaultValue],
                 [Description],
                 [IsHidden],
                 [Label],
                 [ListValues],
                 [Name]
        FROM     [dbo].[System.Option.Definition]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[System.Option.Definition];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_System.Option.Definition]', N'System.Option.Definition';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_System.Option.Definition1]', N'PK_System.Option.Definition', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_System.Option.Definition_CategoryID]
    ON [dbo].[System.Option.Definition]([CategoryID] ASC, [Name] ASC, [IsHidden] ASC);
CREATE NONCLUSTERED INDEX [IX_System.Option.Definition_Name]
    ON [dbo].[System.Option.Definition]([Name] ASC, [CategoryID] ASC, [IsHidden] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_System.Option.Section] (
    [ID]          SMALLINT      NOT NULL,
    [Name]        VARCHAR (255) NOT NULL,
    [ParentID]    SMALLINT      NULL,
    [IsHidden]    BIT           CONSTRAINT [DF_System.Option.Section_IsHidden] DEFAULT ((0)) NOT NULL,
    [Depth]       TINYINT       CONSTRAINT [DF_System.Option.Section_Depth] DEFAULT ((0)) NOT NULL,
    [SearchTerms] VARCHAR (MAX) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_System.Option.Section1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[System.Option.Section])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_System.Option.Section] ([ID], [Depth], [IsHidden], [Name], [ParentID], [SearchTerms])
        SELECT   [ID],
                 [Depth],
                 [IsHidden],
                 [Name],
                 [ParentID],
                 [SearchTerms]
        FROM     [dbo].[System.Option.Section]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[System.Option.Section];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_System.Option.Section]', N'System.Option.Section';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_System.Option.Section1]', N'PK_System.Option.Section', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_System.Option.Section_ParentID]
    ON [dbo].[System.Option.Section]([ParentID] ASC, [Name] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_User.Link] (
    [BID]          SMALLINT NOT NULL,
    [UserID]       INT      NOT NULL,
    [EmployeeID]   SMALLINT NULL,
    [ContactID]    INT      NULL,
    [RoleType]     TINYINT  NOT NULL,
    [RightGroupID] SMALLINT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_User.Link1] PRIMARY KEY CLUSTERED ([BID] ASC, [UserID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[User.Link])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_User.Link] ([BID], [UserID], [ContactID], [EmployeeID], [RightGroupID], [RoleType])
        SELECT   [BID],
                 [UserID],
                 [ContactID],
                 [EmployeeID],
                 [RightGroupID],
                 [RoleType]
        FROM     [dbo].[User.Link]
        ORDER BY [BID] ASC, [UserID] ASC;
    END

DROP TABLE [dbo].[User.Link];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_User.Link]', N'User.Link';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_User.Link1]', N'PK_User.Link', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx__Root.Custom] (
    [ID]              INT      NOT NULL,
    [BID]             SMALLINT NOT NULL,
    [ClassTypeID]     AS       ((2001)),
    [CustomFieldData] XML      NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK__Root.Custom_11] PRIMARY KEY CLUSTERED ([ID] ASC, [BID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[_Root.Custom])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx__Root.Custom] ([ID], [BID], [CustomFieldData])
        SELECT   [ID],
                 [BID],
                 [CustomFieldData]
        FROM     [dbo].[_Root.Custom]
        ORDER BY [ID] ASC, [BID] ASC;
    END

DROP TABLE [dbo].[_Root.Custom];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx__Root.Custom]', N'_Root.Custom';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK__Root.Custom_11]', N'PK__Root.Custom_1', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Business.Custom] (
    [BID]             SMALLINT NOT NULL,
    [ClassTypeID]     AS       ((1001)),
    [CustomFieldData] XML      NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Business.CustomField1] PRIMARY KEY CLUSTERED ([BID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Business.Custom])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Business.Custom] ([BID], [CustomFieldData])
        SELECT   [BID],
                 [CustomFieldData]
        FROM     [dbo].[Business.Custom]
        ORDER BY [BID] ASC;
    END

DROP TABLE [dbo].[Business.Custom];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Business.Custom]', N'Business.Custom';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Business.CustomField1]', N'PK_Business.CustomField', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Campaign.Custom] (
    [BID]             SMALLINT NOT NULL,
    [ID]              INT      NOT NULL,
    [ClassTypeID]     AS       ((9101)),
    [CustomFieldData] XML      NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Campaign.CustomField1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Campaign.Custom])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Campaign.Custom] ([BID], [ID], [CustomFieldData])
        SELECT   [BID],
                 [ID],
                 [CustomFieldData]
        FROM     [dbo].[Campaign.Custom]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Campaign.Custom];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Campaign.Custom]', N'Campaign.Custom';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Campaign.CustomField1]', N'PK_Campaign.CustomField', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Company.Custom] (
    [BID]             SMALLINT NOT NULL,
    [ID]              INT      NOT NULL,
    [ClassTypeID]     AS       ((2001)),
    [CustomFieldData] XML      NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Company.CustomField1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Company.Custom])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Company.Custom] ([BID], [ID], [CustomFieldData])
        SELECT   [BID],
                 [ID],
                 [CustomFieldData]
        FROM     [dbo].[Company.Custom]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Company.Custom];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Company.Custom]', N'Company.Custom';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Company.CustomField1]', N'PK_Company.CustomField', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_CRM.CustomField.HelperLink] (
    [BID]           SMALLINT NOT NULL,
    [CustomFieldID] SMALLINT NOT NULL,
    [HelperID]      SMALLINT NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_CRM.Setup.CustomField.HelperLink1] PRIMARY KEY CLUSTERED ([BID] ASC, [CustomFieldID] ASC, [HelperID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[CRM.CustomField.HelperLink])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_CRM.CustomField.HelperLink] ([BID], [CustomFieldID], [HelperID])
        SELECT   [BID],
                 [CustomFieldID],
                 [HelperID]
        FROM     [dbo].[CRM.CustomField.HelperLink]
        ORDER BY [BID] ASC, [CustomFieldID] ASC, [HelperID] ASC;
    END

DROP TABLE [dbo].[CRM.CustomField.HelperLink];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_CRM.CustomField.HelperLink]', N'CRM.CustomField.HelperLink';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_CRM.Setup.CustomField.HelperLink1]', N'PK_CRM.Setup.CustomField.HelperLink', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_CRM.Setup.CustomField.HelperLink]
    ON [dbo].[CRM.CustomField.HelperLink]([BID] ASC, [HelperID] ASC, [CustomFieldID] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.CRM.Company.Status] (
    [ID]   TINYINT       NOT NULL,
    [Name] VARCHAR (100) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_enum.CRM.CompanyStatus1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.CRM.Company.Status])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.CRM.Company.Status] ([ID], [Name])
        SELECT   [ID],
                 [Name]
        FROM     [dbo].[enum.CRM.Company.Status]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[enum.CRM.Company.Status];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.CRM.Company.Status]', N'enum.CRM.Company.Status';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_enum.CRM.CompanyStatus1]', N'PK_enum.CRM.CompanyStatus', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.CustomField.DataType] (
    [ID]   TINYINT       NOT NULL,
    [Name] VARCHAR (255) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_enum.CustomFieldType1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.CustomField.DataType])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.CustomField.DataType] ([ID], [Name])
        SELECT   [ID],
                 [Name]
        FROM     [dbo].[enum.CustomField.DataType]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[enum.CustomField.DataType];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.CustomField.DataType]', N'enum.CustomField.DataType';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_enum.CustomFieldType1]', N'PK_enum.CustomFieldType', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.Employee.Role] (
    [ID]   TINYINT       NOT NULL,
    [Name] VARCHAR (100) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_enum.EmployeeRole1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.Employee.Role])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.Employee.Role] ([ID], [Name])
        SELECT   [ID],
                 [Name]
        FROM     [dbo].[enum.Employee.Role]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[enum.Employee.Role];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.Employee.Role]', N'enum.Employee.Role';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_enum.EmployeeRole1]', N'PK_enum.EmployeeRole', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_enum.User.Role.Type] (
    [ID]   TINYINT       NOT NULL,
    [Name] VARCHAR (255) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_enum.User.RoleType1] PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[enum.User.Role.Type])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_enum.User.Role.Type] ([ID], [Name])
        SELECT   [ID],
                 [Name]
        FROM     [dbo].[enum.User.Role.Type]
        ORDER BY [ID] ASC;
    END

DROP TABLE [dbo].[enum.User.Role.Type];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_enum.User.Role.Type]', N'enum.User.Role.Type';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_enum.User.RoleType1]', N'PK_enum.User.RoleType', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_List.Filter.EmployeeSubscription] (
    [BID]          SMALLINT NOT NULL,
    [FilterID]     INT      NOT NULL,
    [EmployeeID]   SMALLINT NOT NULL,
    [IsFavorite]   BIT      CONSTRAINT [DF_List.Filter.Subscription_IsFavorite] DEFAULT ((0)) NOT NULL,
    [IsShownAsTab] BIT      CONSTRAINT [DF_List.Filter.EmployeeSubscription_IsShownAsTab] DEFAULT ((0)) NOT NULL,
    [SortIndex]    TINYINT  CONSTRAINT [DF_List.Filter.EmployeeSubscription_SortIndex] DEFAULT ((50)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_List.Filter.Subscription1] PRIMARY KEY CLUSTERED ([BID] ASC, [FilterID] ASC, [EmployeeID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[List.Filter.EmployeeSubscription])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_List.Filter.EmployeeSubscription] ([BID], [FilterID], [EmployeeID], [IsFavorite], [IsShownAsTab], [SortIndex])
        SELECT   [BID],
                 [FilterID],
                 [EmployeeID],
                 [IsFavorite],
                 [IsShownAsTab],
                 [SortIndex]
        FROM     [dbo].[List.Filter.EmployeeSubscription]
        ORDER BY [BID] ASC, [FilterID] ASC, [EmployeeID] ASC;
    END

DROP TABLE [dbo].[List.Filter.EmployeeSubscription];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_List.Filter.EmployeeSubscription]', N'List.Filter.EmployeeSubscription';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_List.Filter.Subscription1]', N'PK_List.Filter.Subscription', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
CREATE NONCLUSTERED INDEX [IX_List.Filter.Subscription_Employee]
    ON [dbo].[List.Filter.EmployeeSubscription]([BID] ASC, [EmployeeID] ASC, [FilterID] ASC);
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Opportunity.Custom] (
    [BID]             SMALLINT NOT NULL,
    [ID]              INT      NOT NULL,
    [ClassTypeID]     AS       ((9001)),
    [CustomFieldData] XML      NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Opportunity.CustomField1] PRIMARY KEY CLUSTERED ([BID] ASC, [ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Opportunity.Custom])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Opportunity.Custom] ([BID], [ID], [CustomFieldData])
        SELECT   [BID],
                 [ID],
                 [CustomFieldData]
        FROM     [dbo].[Opportunity.Custom]
        ORDER BY [BID] ASC, [ID] ASC;
    END

DROP TABLE [dbo].[Opportunity.Custom];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Opportunity.Custom]', N'Opportunity.Custom';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Opportunity.CustomField1]', N'PK_Opportunity.CustomField', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Util.NextID] (
    [BID]         SMALLINT NOT NULL,
    [ClassTypeID] INT      NOT NULL,
    [NextID]      INT      NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_NextID1] PRIMARY KEY CLUSTERED ([BID] ASC, [ClassTypeID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Util.NextID])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Util.NextID] ([BID], [ClassTypeID], [NextID])
        SELECT   [BID],
                 [ClassTypeID],
                 [NextID]
        FROM     [dbo].[Util.NextID]
        ORDER BY [BID] ASC, [ClassTypeID] ASC;
    END

DROP TABLE [dbo].[Util.NextID];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Util.NextID]', N'Util.NextID';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_NextID1]', N'PK_NextID', N'OBJECT';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
ALTER TABLE [dbo].[Security.Right.Group.Link]
    ADD CONSTRAINT [DF_Security.Right.Group.Link_OV] DEFAULT ((0)) FOR [IsSystem];
ALTER TABLE [dbo].[Security.Right.Link]
    ADD CONSTRAINT [DF_Security.Right.Link_OV] DEFAULT ((0)) FOR [IsSystem];
ALTER TABLE [dbo].[Security.Role.Link]
    ADD CONSTRAINT [DF_Security.Role.Link_OV] DEFAULT ((0)) FOR [IsSystem];
ALTER TABLE [dbo].[Accounting.Payment.Term.LocationLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term] FOREIGN KEY ([BID], [TermID]) REFERENCES [dbo].[Accounting.Payment.Term] ([BID], [ID]);
ALTER TABLE [dbo].[Accounting.Tax.Group.AssessmentLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment] FOREIGN KEY ([BID], [AssessmentID]) REFERENCES [dbo].[Accounting.Tax.Assessment] ([BID], [ID]);
ALTER TABLE [dbo].[Accounting.Tax.Group.AssessmentLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group] FOREIGN KEY ([BID], [GroupID]) REFERENCES [dbo].[Accounting.Tax.Group] ([BID], [ID]);
ALTER TABLE [dbo].[Accounting.Tax.Group.LocationLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group] FOREIGN KEY ([BID], [GroupID]) REFERENCES [dbo].[Accounting.Tax.Group] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_Accounting.Tax.Group] FOREIGN KEY ([BID], [TaxGroupID]) REFERENCES [dbo].[Accounting.Tax.Group] ([BID], [ID]);
ALTER TABLE [dbo].[Location.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Location.Data_Accounting.Tax.Group] FOREIGN KEY ([BID], [DefaultTaxGroupID]) REFERENCES [dbo].[Accounting.Tax.Group] ([BID], [ID]);
ALTER TABLE [dbo].[CRM.CustomField.Def] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Setup.CustomField_Business.Data] FOREIGN KEY ([BID]) REFERENCES [dbo].[Business.Data] ([BID]);
ALTER TABLE [dbo].[CRM.CustomField.Helper] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Setup.CustomField.Helper_Business.Data] FOREIGN KEY ([BID]) REFERENCES [dbo].[Business.Data] ([BID]);
ALTER TABLE [dbo].[Employee.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.Data_Business.Data] FOREIGN KEY ([BID]) REFERENCES [dbo].[Business.Data] ([BID]);
ALTER TABLE [dbo].[Business.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Business.Data_Employee.Data1] FOREIGN KEY ([BID], [BillingEmployeeID]) REFERENCES [dbo].[Employee.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Business.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Business.Data_Employee.Data] FOREIGN KEY ([BID], [OwnerEmployeeID]) REFERENCES [dbo].[Employee.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Opportunity.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Opportunity.Data_Business.Data] FOREIGN KEY ([BID]) REFERENCES [dbo].[Business.Data] ([BID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_Business.Data] FOREIGN KEY ([BID]) REFERENCES [dbo].[Business.Data] ([BID]);
ALTER TABLE [dbo].[Location.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Location.Data_Business.Data] FOREIGN KEY ([BID]) REFERENCES [dbo].[Business.Data] ([BID]);
ALTER TABLE [dbo].[Campaign.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Campaign.Data_Business.Data] FOREIGN KEY ([BID]) REFERENCES [dbo].[Business.Data] ([BID]);
ALTER TABLE [dbo].[Contact.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Contact.Data_Business.Data] FOREIGN KEY ([BID]) REFERENCES [dbo].[Business.Data] ([BID]);
ALTER TABLE [dbo].[Business.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Business.Locator_enum.LocatorSubType] FOREIGN KEY ([LocatorType], [LocatorSubType]) REFERENCES [dbo].[enum.Locator.SubType] ([LocatorType], [ID]);
ALTER TABLE [dbo].[Business.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Business.Locator_enum.LocatorType] FOREIGN KEY ([LocatorType]) REFERENCES [dbo].[enum.Locator.Type] ([ID]);
ALTER TABLE [dbo].[Opportunity.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Opportunity.Data_Campaign.Data] FOREIGN KEY ([BID], [CampaignID]) REFERENCES [dbo].[Campaign.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Campaign.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Campaign.Data_enum.Campaign.Type] FOREIGN KEY ([CampaignType]) REFERENCES [dbo].[enum.Campaign.Type] ([ID]);
ALTER TABLE [dbo].[Campaign.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Campaign.Data_Campaign.Custom] FOREIGN KEY ([BID], [ID]) REFERENCES [dbo].[Campaign.Custom] ([BID], [ID]);
ALTER TABLE [dbo].[Campaign.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Campaign.Data_Employee.Team] FOREIGN KEY ([BID], [TeamID]) REFERENCES [dbo].[Employee.Team] ([BID], [ID]);
ALTER TABLE [dbo].[Opportunity.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Opportunity.Data_Company.Data] FOREIGN KEY ([BID], [CompanyID]) REFERENCES [dbo].[Company.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_enum.CRM.Company.Status] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[enum.CRM.Company.Status] ([ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_Company.Custom] FOREIGN KEY ([BID], [ID]) REFERENCES [dbo].[Company.Custom] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_Location.Data] FOREIGN KEY ([BID], [LocationID]) REFERENCES [dbo].[Location.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_enum.TimeZone] FOREIGN KEY ([TimeZoneID]) REFERENCES [dbo].[enum.TimeZone] ([ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_CRM.Industry] FOREIGN KEY ([BID], [IndustryID]) REFERENCES [dbo].[CRM.Industry] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_Company.Data] FOREIGN KEY ([BID], [ParentID]) REFERENCES [dbo].[Company.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_CRM.Source] FOREIGN KEY ([BID], [SourceID]) REFERENCES [dbo].[CRM.Source] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Data_Employee.Team] FOREIGN KEY ([BID], [TeamID]) REFERENCES [dbo].[Employee.Team] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Locator_Company.Data] FOREIGN KEY ([BID], [ParentID]) REFERENCES [dbo].[Company.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Contact.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Contact.Data_Company.Data] FOREIGN KEY ([BID], [CompanyID]) REFERENCES [dbo].[Company.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Company.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Locator_enum.LocatorType] FOREIGN KEY ([LocatorType]) REFERENCES [dbo].[enum.Locator.Type] ([ID]);
ALTER TABLE [dbo].[Company.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Company.Locator_enum.LocatorSubType] FOREIGN KEY ([LocatorType], [LocatorSubType]) REFERENCES [dbo].[enum.Locator.SubType] ([LocatorType], [ID]);
ALTER TABLE [dbo].[User.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_User.Link_Contact.Data] FOREIGN KEY ([BID], [ContactID]) REFERENCES [dbo].[Contact.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Opportunity.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Opportunity.Data_Contact.Data] FOREIGN KEY ([BID], [ContactID]) REFERENCES [dbo].[Contact.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Contact.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Contact.Data_Contact.Custom] FOREIGN KEY ([BID], [ID]) REFERENCES [dbo].[Contact.Custom] ([BID], [ID]);
ALTER TABLE [dbo].[Contact.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Contact.Data_enum.TimeZone] FOREIGN KEY ([TimeZoneID]) REFERENCES [dbo].[enum.TimeZone] ([ID]);
ALTER TABLE [dbo].[Contact.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Contact.Data_Location.Data] FOREIGN KEY ([BID], [LocationID]) REFERENCES [dbo].[Location.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Contact.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Contact.Locator_Contact.Data] FOREIGN KEY ([BID], [ParentID]) REFERENCES [dbo].[Contact.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Contact.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Contact.Locator_enum.LocatorSubType] FOREIGN KEY ([LocatorType], [LocatorSubType]) REFERENCES [dbo].[enum.Locator.SubType] ([LocatorType], [ID]);
ALTER TABLE [dbo].[Contact.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Contact.Locator_enum.LocatorType] FOREIGN KEY ([LocatorType]) REFERENCES [dbo].[enum.Locator.Type] ([ID]);
ALTER TABLE [dbo].[CRM.CustomField.Def] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Setup.CustomField_enum.CustomField.DataType] FOREIGN KEY ([DataType]) REFERENCES [dbo].[enum.CustomField.DataType] ([ID]);
ALTER TABLE [dbo].[CRM.CustomField.Def] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Setup.CustomField_enum.CustomField.InputType] FOREIGN KEY ([InputType]) REFERENCES [dbo].[enum.CustomField.InputType] ([ID]);
ALTER TABLE [dbo].[CRM.CustomField.HelperLink] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField] FOREIGN KEY ([BID], [CustomFieldID]) REFERENCES [dbo].[CRM.CustomField.Def] ([BID], [ID]);
ALTER TABLE [dbo].[CRM.CustomField.Helper] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType] FOREIGN KEY ([DataType]) REFERENCES [dbo].[enum.CustomField.DataType] ([ID]);
ALTER TABLE [dbo].[CRM.CustomField.Helper] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType] FOREIGN KEY ([HelperType]) REFERENCES [dbo].[enum.CustomField.HelperType] ([ID]);
ALTER TABLE [dbo].[CRM.CustomField.HelperLink] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper] FOREIGN KEY ([BID], [HelperID]) REFERENCES [dbo].[CRM.CustomField.Helper] ([BID], [ID]);
ALTER TABLE [dbo].[CRM.Industry] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Industry_CRM.Industry] FOREIGN KEY ([BID], [ParentID]) REFERENCES [dbo].[CRM.Industry] ([BID], [ID]);
ALTER TABLE [dbo].[CRM.Source] WITH NOCHECK
    ADD CONSTRAINT [FK_CRM.Source_CRM.Source] FOREIGN KEY ([BID], [ParentID]) REFERENCES [dbo].[CRM.Source] ([BID], [ID]);
ALTER TABLE [dbo].[Employee.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.Data_ReportsToID] FOREIGN KEY ([BID], [ReportsToID]) REFERENCES [dbo].[Employee.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Employee.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.Locator_Employee.Data] FOREIGN KEY ([BID], [ParentID]) REFERENCES [dbo].[Employee.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Employee.TeamLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.TeamLink_Employee.Data] FOREIGN KEY ([BID], [EmployeeID]) REFERENCES [dbo].[Employee.Data] ([BID], [ID]);
ALTER TABLE [dbo].[List.Filter.EmployeeSubscription] WITH NOCHECK
    ADD CONSTRAINT [FK_List.Filter.EmployeeSubscription_Employee.Data] FOREIGN KEY ([BID], [EmployeeID]) REFERENCES [dbo].[Employee.Data] ([BID], [ID]);
ALTER TABLE [dbo].[User.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_User.Link_Employee.Data] FOREIGN KEY ([BID], [EmployeeID]) REFERENCES [dbo].[Employee.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Employee.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.Locator_enum.LocatorType] FOREIGN KEY ([LocatorType]) REFERENCES [dbo].[enum.Locator.Type] ([ID]);
ALTER TABLE [dbo].[Employee.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.Locator_enum.LocatorSubType] FOREIGN KEY ([LocatorType], [LocatorSubType]) REFERENCES [dbo].[enum.Locator.SubType] ([LocatorType], [ID]);
ALTER TABLE [dbo].[Employee.TeamLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.TeamLink_Employee.Team] FOREIGN KEY ([BID], [TeamID]) REFERENCES [dbo].[Employee.Team] ([BID], [ID]);
ALTER TABLE [dbo].[Opportunity.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Opportunity.Data_Employee.Team] FOREIGN KEY ([BID], [TeamID]) REFERENCES [dbo].[Employee.Team] ([BID], [ID]);
ALTER TABLE [dbo].[Employee.Team.LocationLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.Team.LocationLink_Employee.Team] FOREIGN KEY ([BID], [TeamID]) REFERENCES [dbo].[Employee.Team] ([BID], [ID]);
ALTER TABLE [dbo].[enum.CustomField.InputType] WITH NOCHECK
    ADD CONSTRAINT [FK_enum.CustomField.InputType_enum.CustomField.DataType] FOREIGN KEY ([DataType]) REFERENCES [dbo].[enum.CustomField.DataType] ([ID]);
ALTER TABLE [dbo].[enum.Locator.SubType] WITH NOCHECK
    ADD CONSTRAINT [FK_enum.Locator.SubType_enum.Locator.Type] FOREIGN KEY ([LocatorType]) REFERENCES [dbo].[enum.Locator.Type] ([ID]);
ALTER TABLE [dbo].[Location.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Business.Location.Locator_enum.LocatorSubType] FOREIGN KEY ([LocatorType], [LocatorSubType]) REFERENCES [dbo].[enum.Locator.SubType] ([LocatorType], [ID]);
ALTER TABLE [dbo].[Location.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Location.Locator_enum.LocatorType] FOREIGN KEY ([LocatorType]) REFERENCES [dbo].[enum.Locator.Type] ([ID]);
ALTER TABLE [dbo].[Location.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Location.Data_enum.TimeZone] FOREIGN KEY ([TimeZoneID]) REFERENCES [dbo].[enum.TimeZone] ([ID]);
ALTER TABLE [dbo].[List.Filter.EmployeeSubscription] WITH NOCHECK
    ADD CONSTRAINT [FK_List.Filter.EmployeeSubscription_List.Filter] FOREIGN KEY ([BID], [FilterID]) REFERENCES [dbo].[List.Filter] ([BID], [ID]);
ALTER TABLE [dbo].[Accounting.Payment.Term.LocationLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Location.Data] FOREIGN KEY ([BID], [LocationID]) REFERENCES [dbo].[Location.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Accounting.Tax.Group.LocationLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Location.Data] FOREIGN KEY ([BID], [LocationID]) REFERENCES [dbo].[Location.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Employee.Team.LocationLink] WITH NOCHECK
    ADD CONSTRAINT [FK_Employee.Team.LocationLink_Location.Data] FOREIGN KEY ([BID], [LocationID]) REFERENCES [dbo].[Location.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Location.Locator] WITH NOCHECK
    ADD CONSTRAINT [FK_Location.Locator_Location.Data] FOREIGN KEY ([BID], [ParentID]) REFERENCES [dbo].[Location.Data] ([BID], [ID]);
ALTER TABLE [dbo].[Opportunity.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Opportunity.Data_Opportunity.Custom] FOREIGN KEY ([BID], [ID]) REFERENCES [dbo].[Opportunity.Custom] ([BID], [ID]);
ALTER TABLE [dbo].[Option.Data] WITH NOCHECK
    ADD CONSTRAINT [FK_Option.Data_System.Option.Definition] FOREIGN KEY ([OptionID]) REFERENCES [dbo].[System.Option.Definition] ([ID]);
ALTER TABLE [dbo].[Security.Right.Group.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_Security.Right.Group.Link_Security.Right.Group1] FOREIGN KEY ([BID], [ChildGroupID]) REFERENCES [dbo].[Security.Right.Group] ([BID], [ID]);
ALTER TABLE [dbo].[Security.Right.Group.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_Security.Right.Group.Link_Security.Right.Group] FOREIGN KEY ([BID], [ParentGroupID]) REFERENCES [dbo].[Security.Right.Group] ([BID], [ID]);
ALTER TABLE [dbo].[Security.Right.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_Security.Right.Link_Security.Right.Collection] FOREIGN KEY ([BID], [RightGroupID]) REFERENCES [dbo].[Security.Right.Group] ([BID], [ID]);
ALTER TABLE [dbo].[Security.Role.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_Security.Role.Link_Security.Right.Group] FOREIGN KEY ([BID], [RightGroupID]) REFERENCES [dbo].[Security.Right.Group] ([BID], [ID]);
ALTER TABLE [dbo].[User.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_User.Link_Security.Right.Collection] FOREIGN KEY ([BID], [RightGroupID]) REFERENCES [dbo].[Security.Right.Group] ([BID], [ID]);
ALTER TABLE [dbo].[System.Option.Category] WITH NOCHECK
    ADD CONSTRAINT [FK_System.Option.Category_System.Option.Section] FOREIGN KEY ([SectionID]) REFERENCES [dbo].[System.Option.Section] ([ID]);
ALTER TABLE [dbo].[System.Option.Definition] WITH NOCHECK
    ADD CONSTRAINT [FK_System.Option.Definition_System.Option.Category] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[System.Option.Category] ([ID]);
ALTER TABLE [dbo].[System.Option.Definition] WITH NOCHECK
    ADD CONSTRAINT [FK_System.Option.Definition_enum.CustomField.DataType] FOREIGN KEY ([DataType]) REFERENCES [dbo].[enum.CustomField.DataType] ([ID]);
ALTER TABLE [dbo].[System.Option.Section] WITH NOCHECK
    ADD CONSTRAINT [FK_System.Option.Section_System.Option.Section] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[System.Option.Section] ([ID]);
ALTER TABLE [dbo].[User.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_User.Link_enum.User.Role.Type] FOREIGN KEY ([RoleType]) REFERENCES [dbo].[enum.User.Role.Type] ([ID]);
ALTER TABLE [dbo].[Security.Role.Link] WITH NOCHECK
    ADD CONSTRAINT [FK_Security.Role.Link_enum.User.Role.Type] FOREIGN KEY ([RoleType]) REFERENCES [dbo].[enum.User.Role.Type] ([ID]);
EXECUTE sp_refreshsqlmodule N'[dbo].[Accounting.Payment.Term.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[Accounting.Tax.Assessment.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[Accounting.Tax.Group.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[Business.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[Company.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[Contact.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[CRM.Industry.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[CRM.Source.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[Employee.SimpleList]';
EXECUTE sp_refreshsqlmodule N'[dbo].[Employee.Team.SimpleList]';
GO
-- TimeZone SimpleList
ALTER VIEW [dbo].[enum.TimeZone.SimpleList] AS
    SELECT
          [ID]
        , [Name] as DisplayName
        , [IsCommon] as [IsActive]
        , CONVERT(BIT, 0) AS [HasImage]
    FROM [enum.TimeZone]
GO
PRINT N'Refreshing [dbo].[Business.Location.SimpleList]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[Business.Location.SimpleList]';
-- Location View
GO
ALTER VIEW [Location.SimpleList] AS
    SELECT [BID]
        ,[ID]
        ,[ClassTypeID]
        ,[Name] as DisplayName
        ,[IsActive]
        ,[HasImage]
        ,[IsDefault]
    FROM [Location.Data]
GO
PRINT N'Creating [dbo].[TablesAndColumns]...';


GO

CREATE VIEW [dbo].[TablesAndColumns]
AS
SELECT TOP 100 PERCENT
       tbl.name as TableName, tbl.object_id as TableID, tbl.type_desc as TableType,
       col.column_id as ColumnID,  col.name as ColumnName,
       coltypes.name as ColumnType, col.Is_Nullable as IsNullable,
       col.is_computed IsComputed, col.is_sparse IsSparse, col.is_identity IsIdentity,
       comcol.definition ComputedFormula, col.max_length, PKs.IsPK, col.Precision, col.Scale

FROM sys.tables tbl
     JOIN sys.columns col ON tbl.object_id = col.object_id
     JOIN sys.types coltypes ON col.system_type_id = coltypes.system_type_id AND col.user_type_id = coltypes.user_type_id
     LEFT JOIN ( SELECT 1 AS IsPK, table_name, column_name
                 FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                 WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1 ) AS PKs ON PKs.table_name = tbl.name AND PKs.column_name = col.name
     LEFT JOIN sys.computed_columns comcol ON tbl.object_id = comcol.object_id AND col.column_id = comcol.column_id

WHERE tbl.type = 'U' AND tbl.is_ms_shipped = 0

ORDER BY TableName, ColumnID
GO
PRINT N'Altering [dbo].[User.Security.Rights.String]...';


GO
-- ========================================================
-- 
-- Name: User.Security.Rights.String( BID int, UserID int )
--
-- Description: This Table Value Function returns a list of Rights associated
-- with a specific user for a given business id.
--
-- Sample Use:   select dbo.[User.Security.Rights.String]( 1, 100 )
--
-- ========================================================
ALTER FUNCTION [User.Security.Rights.String] ( @BID int, @UserID int )
RETURNS BINARY(128)
AS
BEGIN
    DECLARE @ResultSize SMALLINT = 128
	      , @Found BIT = 0
          , @Results BINARY(128) = 0; 

    DECLARE @Temp Table (RightID smallint, ByteIndex tinyint, BytePos tinyint, ByteFlag tinyint);

    WITH Rights(BID, GroupID)
    AS
    (
        -- Anchor Query

        -- Pull the users ROLE security group
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Role.Link] SRoL on SRoL.BID = U.BID AND SRoL.RoleType = U.RoleType
        JOIN [Security.Right.Group] SRG on SRG.BID = SRoL.BID AND SRG.ID = SRoL.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Merge with the users top-level security group
        UNION ALL 
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Right.Group] SRG on SRG.BID = U.BID AND SRG.ID = U.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Now merge with recursively with Security Groups
        UNION ALL
        SELECT G.BID, G.ID
        FROM Rights R 
        JOIN [Security.Right.Group.Link] L ON L.BID = R.BID AND L.ParentGroupID = R.GroupID
        JOIN [Security.Right.Group] G ON G.BID = L.BID AND G.ID = L.ChildGroupID
    )
        SELECT @Results = SUBSTRING(@Results, 1, @ResultSize-ByteIndex) + CONVERT(BINARY(1),CHAR(Flags)) + SUBSTRING(@Results, @ResultSize-(ByteIndex-2), ByteIndex-1)
		     , @Found = 1  -- use this to determine that we really found results.  Will not get set so @Found will remain 0 if not
        FROM (
            -- Sum the Byte Flag by ByteIndex to get the character to insert
            SELECT ByteIndex, SUM(ByteFlag) Flags
            FROM (
                SELECT DISTINCT 
                      (RL.RightID / 8)+1 as ByteIndex
                    , Power(2, RL.RightID % 8) as ByteFlag
                    --, (RL.RightID % 8) as BytePos
                    -- , RL.RightID
                FROM Rights R
                JOIN [Security.Right.Link] RL on RL.BID = R.BID and RL.RightGroupID = R.GroupID
            ) RightsList
            GROUP By ByteIndex
        ) RightsString
        ;

		IF (@Found=0) SET @Results = NULL;

        RETURN @Results;
END;
GO
PRINT N'Altering [dbo].[Util.ID.BIDFromTimeStampGUID]...';


GO
-- =============================================
-- Author:      Scott St.Cyr
-- Create date: 2017-09-15
-- Description: This function returns the BID embedded in a time-sequenced GUID
-- =============================================
ALTER FUNCTION [Util.ID.BIDFromTimeStampGUID](@ID UniqueIdentifier)
RETURNS SMALLINT
AS
BEGIN
    RETURN CONVERT(SMALLINT, CONVERT(VARBINARY(2), RIGHT(CONVERT(VARBINARY(16), @ID), 2)));
END;
GO
PRINT N'Altering [dbo].[Util.ID.NewTimeStampGUID]...';


GO
-- =============================================
-- Author:      Scott St.Cyr
-- Create date: 2017-09-15
-- Description: This function returns a time-sequenced GUID, which will 
--              not cause fragmentation when inserted.
-- =============================================
ALTER FUNCTION [Util.ID.NewTimeStampGUID](@BID SMALLINT)
RETURNS UniqueIdentifier
AS
BEGIN
    -- A time stamp guid modifies the Guid so that the last 6 bytes (40-bits)
    -- are based on the date and time.  This is helpful in database keys
    -- because the last 32 bits of the Guid are used as the first part of the
    -- index.  Therefore, this gives a 'random' guid a chronological order and 
    -- makes sure that new records are usually inserted after existing ones.
    -- The first two bytes are replaced with the BID so that can be extracted after.

    -- The remaining 88 bits are left randomized, so the likelihood of a duplicate
    -- key is still less probable than the canonization of Madonna.

    --   Note that SQLGuids are sorted in a screwy way ...
    --      Byte[10] -> sorted first    ->> store Date2(2) byte 1
    --      Byte[11] -> sorted next     ->> store Date2(2) byte 1
    --      Byte[12] -> sorted next     ->> store Date2(2) byte 1
    --      Byte[13] -> sorted next     ->> store Date2(2) byte 1
    --      Byte[14] -> sorted next     ->> store Date2(2) byte 1
    --      Byte[15] -> sorted next     ->> store Date2(2) byte 1
    --      Byte[08] -> sorted next     
    --      Byte[09] -> sorted next    
    --      Byte[07] -> sorted next
    --      Byte[06] -> sorted next
    --      Byte[05] -> sorted next
    --      Byte[04] -> sorted next
    --      Byte[03] -> sorted next
    --      Byte[02] -> sorted next
    --      Byte[01] -> sorted next     ->> store BID part 2
    --      Byte[00] -> sorted last     ->> store BID part 1


    DECLARE @ResultArray VARBINARY(16)
          , @DTArray     VARBINARY(07)
          ;

    -- For reasons for which I can't find any explantion, you can't use NewID in a function directly.
    -- It gives a 'side effect' error, but of course there are no side effects relevant here.
    --
    -- The work-around is to create a view that returns a NewID and then use that.
    -- 
    /* 
        CREATE VIEW [Util.ID.NewGUID] AS SELECT NewID() AS New_ID;
    */
    
    ;

    -- Convert to Binary Array for Working With
    SELECT  @ResultArray = CONVERT(VARBINARY(16), (SELECT New_ID FROM [Util.ID.NewGUID]))
          , @DTArray     = CONVERT(VARBINARY(07), CONVERT(DateTime2(2), GetDate()) )
          ;

    SELECT @ResultArray = CONVERT(VARBINARY(16), 
                               CONVERT(VARBINARY(8), LEFT(@ResultArray, 16)) 
                            +  CONVERT(VARBINARY(6), RIGHT(@DTArray, 7)) 
                            +  CONVERT(VARBINARY(2), @BID) 
                            )
    ;

	-- Return the result of the function
	RETURN CONVERT(UniqueIdentifier, @ResultArray)
END
GO
PRINT N'Altering [dbo].[List.Filter.MyLists]...';


GO


-- =============================================
-- This function returns a users Lists for a particular TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.MyLists](7, 10001, 2000)
--
-- =============================================
-- =============================================
-- This function returns a users Lists for a particular TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.MyLists](7, 10001, 2000)
--
-- =============================================
ALTER FUNCTION [dbo].[List.Filter.MyLists] (@BID SMALLINT, @EmployeeID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT L.* 
    FROM [List.Filter] L
    WHERE BID = @BID 
      AND ID IN (
                    SELECT L.ID
                    FROM [List.Filter] L
                    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
                      AND (L.IsSystem=1 OR (L.BID = @BID AND L.OwnerID = @EmployeeID)) 
    
                    UNION

                    SELECT L.ID
                    FROM [List.Filter] L
                    JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = L.BID AND LS.FilterID = L.ID
                    WHERE (L.BID = @BID) 
                        AND (L.TargetClassTypeID = @TargetClassTypeID)
                        AND (LS.EmployeeID = @EmployeeID)
                )
)
;
GO
PRINT N'Altering [dbo].[List.Filter.SimpleList]...';


GO

--#######################################################################--

-- =============================================
-- This function returns a SimpleList of Lists for a particular Employee for a TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.SimpleList](7, 10001, 2000)
--
-- =============================================
ALTER FUNCTION [dbo].[List.Filter.SimpleList] (@BID SMALLINT, @EmployeeID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT TOP 100 PERCENT 
           @BID as BID
         , L.[ID]
         , L.[ClassTypeID]
         , L.[Name] as DisplayName
         , L.[IsActive]
         , CONVERT(BIT,0) AS [HasImage]
         , L.TargetClassTypeID
         , (CASE WHEN LS.FilterID IS NOT NULL THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END) AS IsSubscribed
         , LS.IsShownAsTab
         , LS.IsFavorite
         , L.IsSystem
    FROM [List.Filter] L
    LEFT JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = @BID AND LS.FilterID = L.ID
    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
      AND (L.IsSystem=1 
           OR (L.BID = @BID 
               AND (L.IsPublic=1 OR L.OwnerID = @EmployeeID)
               )
          )
    ORDER BY [Name]
)
GO
PRINT N'Altering [dbo].[User.Security.Rights.List]...';


GO
-- ========================================================
-- 
-- Name: User.Security.Rights.List( BID int, UserID int )
--
-- Description: This Table Value Function returns a list of Rights associated
-- with a specific user for a given business id.
--
-- Sample Use:   select * from [User.Security.Rights.List] ( 1, 100 )
--
-- ========================================================
ALTER FUNCTION [User.Security.Rights.List] ( @BID int, @UserID int )
RETURNS TABLE
AS
RETURN
(
    WITH Rights(BID, GroupID)
    AS
    (
        -- Anchor Query

        -- Pull the users ROLE security group
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Role.Link] SRoL on SRoL.BID = U.BID AND SRoL.RoleType = U.RoleType
        JOIN [Security.Right.Group] SRG on SRG.BID = SRoL.BID AND SRG.ID = SRoL.RightGroupID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Merge with the users top-level security group
        UNION ALL 
        SELECT SRG.BID, SRG.ID
        FROM [User.Link] U
        JOIN [Security.Right.Group] SRG on SRG.BID = U.BID AND SRG.ID = U.RightGroupID
        LEFT JOIN [Security.Right.Link] SRiL on SRiL.BID = SRG.BID AND SRiL.RightGroupID = SRG.ID
        WHERE U.BID = @BID AND U.UserID = @UserID

        -- Now merge with recursively with Security Groups
        UNION ALL
        SELECT G.BID, G.ID
        FROM Rights R 
        JOIN [Security.Right.Group.Link] L ON L.BID = R.BID AND L.ParentGroupID = R.GroupID
        JOIN [Security.Right.Group] G ON G.BID = L.BID AND G.ID = L.ChildGroupID
    )
        SELECT DISTINCT RL.BID, RL.RightID 
        FROM Rights R
        JOIN [Security.Right.Link] RL on RL.BID = R.BID and RL.RightGroupID = R.GroupID
);
GO
PRINT N'Refreshing [dbo].[Option.Categories.ByLevel]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[Option.Categories.ByLevel]';


-- ------------------------------------------------------------
-- This stored procedure checks and updates the Tax Rates for all groups in a business based on the sum of the rates for all lined Tax Assessments.
-- Sample Usage:
--     EXEC [Accounting.Tax.Group.Recalc] @BID=BID
--
-- After updating the data, the procedure returns a table matching the following signature for any Tax Groups that were changed.
--     SELECT BID, ID, ClassTypeID, OldRate, NewRate
--     FROM ...
-- ------------------------------------------------------------
GO
ALTER PROCEDURE [dbo].[Accounting.Tax.Group.Recalc]
                                @BID SMALLINT
AS
BEGIN
    DECLARE @DT DateTime2(2) = GetDate();

    DECLARE @T TABLE(
        BID SMALLINT
      , ID SMALLINT
      , ClassTypeID SMALLINT
      , OldRate DECIMAL(12,4)
      , NewRate DECIMAL(12,4)
    );

    -- Insert into the Temp Table
    --   All records where the Group.TaxRate != Sum(Assessment.TaxRate)
    INSERT INTO @T
        SELECT G.BID, G.ID, G.ClassTypeID, COALESCE(G.TaxRate,0.0000), SUM(A.TaxRate)
        FROM [Accounting.Tax.Group] G
        JOIN [Accounting.Tax.Group.AssessmentLink] GAL ON (GAL.BID = G.BID AND GAL.GroupID = G.ID)
        JOIN [Accounting.Tax.Assessment] A             ON (A.BID = GAL.BID AND A.ID = GAL.AssessmentID)
        WHERE G.BID = @BID
          AND A.IsActive = 1
        GROUP BY G.BID, G.ID, G.ClassTypeID, COALESCE(G.TaxRate,0.0000)
        HAVING COALESCE(G.TaxRate,0.0000) != SUM(A.TaxRate)
        ORDER BY G.BID, G.ID
    ;

    -- Update the Tax Groups
    UPDATE G
    SET ModifiedDT = @DT
      , TaxRate = T.NewRate
    FROM [Accounting.Tax.Group] G
    JOIN @T T ON (T.BID = G.BID AND T.ID = G.ID)
    ;

    -- Output the Result
    SELECT * FROM @T
    ;
END
GO
PRINT N'Altering [dbo].[Option.DeleteValue]...';


GO
-- ========================================================
-- 
-- Name: [Option.DeleteValue]
--
-- Description: This procedure deletes the value for an option/setting.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.DeleteValue] 
        -- Require Fields
          @OptionName       = 'GLAccount.TaxName2'
        , @OptionID         = NULL

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @EmployeeID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

    SELECT * FROM [Option.Data]
*/
-- ========================================================

ALTER PROCEDURE [dbo].[Option.DeleteValue]
-- DECLARE 
            @OptionID       INT          = NULL
          , @OptionName     VARCHAR(255) = NULL

          , @AssociationID  TINYINT      = NULL
          , @BID            SMALLINT     = NULL
          , @LocationID     SMALLINT     = NULL
          , @EmployeeID     SMALLINT     = NULL
          , @CompanyID      INT          = NULL
          , @ContactID      INT          = NULL
          , @StorefrontID   SMALLINT	 = NULL

AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
          , @Level  TINYINT     
          ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
             , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

	DELETE FROM [Option.Data] WHERE Value = @Result AND OptionLevel = @Level AND OptionID = @OptionID

	IF ((SELECT COUNT(*) FROM [Option.Data] WHERE OptionID = @OptionID) = 0)
	BEGIN
		-- Clear out definition if none in data
		DELETE FROM [System.Option.Definition] WHERE ID = @OptionID
	END

	
END
GO
PRINT N'Altering [dbo].[Option.GetValue]...';


GO
-- ========================================================
-- 
-- Name: [Option.GetValue]
--
-- Description: This procedure looks up a option/setting based on a hierarchy of possibilities.
-- 
--
-- Sample Use:   EXEC dbo.[Option.GetValue] @OptionName = 'GLAccount.TaxName1', @BID=2, @LocationID = 1
-- Returns:  Value varchar(MAX), OptionLevel TINYINT
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128
-- ========================================================

ALTER PROCEDURE [Option.GetValue]
 --DECLARE 
        @OptionID       INT          = NULL -- = 2
      , @OptionName     VARCHAR(255) = NULL -- 'GLAccount.TaxName1'

      , @AssociationID  tinyint      = NULL
	  , @BID            smallint     = NULL
	  , @LocationID     smallint     = NULL
	  , @StoreFrontID   smallint     = NULL
	  , @EmployeeID     smallint     = NULL
	  , @CompanyID      int          = NULL
	  , @ContactID      int          = NULL
AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
          , @Level  TINYINT     
          ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition]
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
	    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

		IF (@AnswerCount > 1)
			THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    System=1
    --    Association=1
    --    Business=2
    --    Location=4
    --    Storefront=8
    --    Employee=16
    --    Company=32
    --    Contact=64

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
             , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    SELECT 
	    CAST (@OptionID AS SMALLINT) AS ID -- needed in the EF
		,@Result as [Value]
        , @Level as [OptionLevel];
END
GO
PRINT N'Altering [dbo].[Option.GetValues]...';


GO

-- ========================================================
-- 
-- Name: [Option.GetValues]
--
-- Description: This procedure looks up a option/setting based on a hierarchy of possibilities.
-- 
--
-- Sample Use:   EXEC dbo.[Option.GetValue] @OptionName = 'GLAccount.TaxName1', @BID=2, @LocationID = 1
-- Returns: Table with columns
	-- ID INT
	-- [Name] VARCHAR(255),
	-- [Value] VARCHAR(255),
	-- [Level] INT,
	-- CategoryID INT,
	-- CategoryName VARCHAR(255),
	-- SectionName VARCHAR(255)

    -- OPTION LEVELS
    --    System=0
    --    Association=1
    --    Business=2
    --    Location=4
    --    Storefront=8
    --    Employee=16
    --    Company=32
    --    Contact=64
-- ========================================================

ALTER PROCEDURE [dbo].[Option.GetValues]
-- DECLARE 
		@AssociationID  tinyint      = NULL
		,@BID            smallint     = NULL
		,@LocationID     smallint     = NULL
		,@StorefrontID   smallint     = NULL
		,@EmployeeID     smallint     = NULL
		,@CompanyID      int          = NULL
		,@ContactID      int          = NULL
AS

BEGIN
	DECLARE @Result TABLE (
		ID INT
		,[Name] VARCHAR(255)
		,[Value] VARCHAR(255)
		,[Level] TINYINT
		,CategoryID SMALLINT
		,CategoryName VARCHAR(255)
		,SectionName VARCHAR(255)
	)

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for Storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
		INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StorefrontID = @StorefrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        INSERT INTO @Result 
		SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN  [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StorefrontID = @StorefrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StorefrontID = @StorefrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StorefrontID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StorefrontID = @StorefrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StorefrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StorefrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF NOT EXISTS (SELECT COUNT(*) FROM @Result) 
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID IS NULL
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF NOT EXISTS (SELECT COUNT(*) FROM @Result) 
        INSERT INTO @Result
		SELECT 
		0,
		'Default',
		DefaultValue, 
		0,
		0,
		'Default Category',
		'Default Section'
        FROM [System.Option.Definition]
        --WHERE ID = @OptionID
    ;

    SELECT 
	ID
	,[Name]
	,[Value]
	,[Level]
	,CategoryID
	,CategoryName
	,SectionName
	FROM @Result;
END
GO
PRINT N'Altering [dbo].[Location.Action.SetActive]...';


GO

-- ========================================================
-- Name: [Location.Action.SetActive]
--
-- Description: This procedure sets the reference location
--
-- Sample Use:   EXEC dbo.[Location.Action.SetActive] @BID=1, @LocationID=1, @IsActive=1
-- ========================================================
ALTER PROCEDURE [dbo].[Location.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @LocationID     INT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that some other Location is Active
    IF (@IsActive = 0) AND NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID != @LocationID and IsActive=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to Set The Only Active Location Inactive. Set another Location active first before setting this one inactive.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that this is not the default location
    IF (@IsActive = 0) AND EXISTS(SELECT * FROM [Location.Data] WHERE BID=@BID and ID=@LocationID and IsDefault=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to Set The Default Location Inactive. Set another Location as the Default first before setting this one Inactive.'
             ;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Location.Data] L
    WHERE BID = @BID and ID = @LocationID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
PRINT N'Altering [dbo].[Location.Action.SetActiveMultiple]...';


GO

-- ========================================================
-- Name: [Location.Action.SetActiveMultiple]
--
-- Description: This procedure sets the reference location
--
-- Sample Use:   EXEC dbo.[Location.Action.SetActiveMultiple] @BID=1, @LocationIDs='1,2', @IsActive=1
-- ========================================================
ALTER PROCEDURE [dbo].[Location.Action.SetActiveMultiple]
-- DECLARE 
          @BID            TINYINT		-- = 1
        , @LocationIDs    VARCHAR(1024) -- = '1,2'
        , @IsActive       BIT			-- = 1
        , @Result         INT     = NULL	OUTPUT
AS
BEGIN
	DECLARE @Message VARCHAR(MAX) = '';

	DECLARE @TempTable TABLE (
			  BID SMALLINT NOT NULL
			, LocationId SMALLINT  NULL
			, IsActive BIT  NULL
			, IsSuccess BIT NOT NULL
			, ErrorMessage VARCHAR(1024)
			);

	INSERT INTO @TempTable( BID, LocationID, IsActive, IsSuccess, ErrorMessage)
		SELECT @BID
		     , LData.ID
			 , LData.IsActive
			 , (CASE WHEN LData.ID IS NULL THEN 0 
			         WHEN @IsActive=0 AND IsDefault=1 THEN 0 
					 ELSE  1 END )
			 , (CASE WHEN LData.ID IS NULL THEN 'Location Not Found; ' 
			         WHEN @IsActive=0 AND IsDefault=1 THEN 'Attempting to set the Default Location to Inactive. Set another Location as the Default first before setting this one to Inactive.; '
			         ELSE '' END )
		FROM string_split(@LocationIDs,',') LInput 
		LEFT JOIN [Location.Data] LData ON LData.BID = @BID AND LData.ID = CONVERT(INT, LInput.[value])
	;

	-- Check that we don't have any errors
	SELECT @Message += ErrorMessage
	FROM @TempTable
	WHERE IsSuccess = 0
	;

 	-- Check that some other Location is Active
	IF (@IsActive = 0) 
		AND NOT EXISTS( SELECT * 
						FROM [Location.Data] LData 
						WHERE BID = @BID AND IsActive=1 and ID NOT IN (SELECT LocationID FROM @TempTable) )
		SET @Message = @Message + ' Attempting to set the only Active Location(s) to Inactive. Set another Location to active first before setting this one to Inactive.';


	IF LEN(@Message) > 1 
		THROW 50000, @Message, 1
	ELSE
		-- Now update it
		UPDATE L
		SET   IsActive   = @IsActive
			, ModifiedDT = GetUTCDate()
		FROM [Location.Data] L
		JOIN @TempTable T on L.BID = @BID AND L.ID = T.LocationId
		WHERE COALESCE(L.IsActive,~@IsActive) != @IsActive

	SET @Result = @@ROWCOUNT;

	SELECT @Result AS Result;
END
GO
PRINT N'Altering [dbo].[Location.Action.SetDefault]...';


GO

-- ========================================================
-- Name: [Location.Action.SetDefault]( @BID tinyint, @LocationID int )
--
-- Description: This function sets any existing default location
-- to false then sets the location (@LocationID) to default and active
--
-- Sample Use:   EXEC dbo.[Location.Action.SetDefault] @BID=1, @LocationID=1
-- ========================================================
ALTER PROCEDURE [dbo].[Location.Action.SetDefault]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @LocationID     INT     -- = 2

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Location.Data]);

	IF (@Count > 1)
	BEGIN
		-- Remove IsDefault from any other Default location(s) for the business
		UPDATE L
		SET IsDefault = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Location.Data] L
		WHERE BID = @BID AND IsDefault = 1
	END

    -- Now update it
    UPDATE L
    SET IsActive   = 1
		, IsDefault = 1
        , ModifiedDT = GetUTCDate()
    FROM [Location.Data] L
    WHERE BID = @BID and ID = @LocationID
		AND COALESCE(IsDefault,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
PRINT N'Altering [dbo].[Option.DeleteCategoryOptions]...';


GO
-- ========================================================
-- 
-- Name: [Option.DeleteCategoryOptions]
--
-- Description: This procedure deletes the options list for the selected category.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.DeleteCategoryOptions] 
        -- Require Fields
          @CategoryID       = 1
		  @CategoryName     = 'COGS Accounts'
*/
-- ========================================================

ALTER PROCEDURE [Option.DeleteCategoryOptions]
-- DECLARE 
            @CategoryID        INT          = NULL
			,@CategoryName     VARCHAR(255) = NULL
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@CategoryID IS NULL) AND (@CategoryName IS NULL)) OR ((@CategoryID IS NOT NULL) AND (@CategoryName IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @CategoryID or the @CategoryName.', 1;

    -- ======================================
    -- Lookup CategoryID
    -- ======================================
    IF (@CategoryID IS NULL)
    BEGIN
        SELECT @CategoryID = ID 
        FROM [System.Option.Category] 
        WHERE Name = @CategoryName;

        IF (@CategoryID IS NULL)
			THROW 180000, '@CategoryName not found.', 1;
    END;

	WITH Category (ID, Name, Value, Level, CategoryID, CategoryName, SectionName)
	AS
	(
		SELECT D.ID, D.Name, O.Value, O.OptionLevel, C.ID, C.Name, S.Name
		FROM [System.Option.Definition] D
		INNER JOIN [Option.Data] O ON O.OptionID = D.ID
		INNER JOIN [System.Option.Category] C ON C.ID = D.CategoryID
		INNER JOIN [System.Option.Section] S ON S.ID = C.SectionID
	
		WHERE CategoryID = @CategoryID
	)
	DELETE FROM [Option.Data] WHERE OptionID IN (SELECT ID FROM Category)
END
GO
PRINT N'Altering [dbo].[Option.DeleteValues]...';


GO
-- ===============================================
ALTER PROCEDURE [Option.DeleteValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT       = NULL
    , @BID            SMALLINT      = NULL
    , @LocationID     SMALLINT      = NULL
    , @EmployeeID     SMALLINT      = NULL
    , @CompanyID      INT           = NULL
    , @ContactID      INT           = NULL
    , @StorefrontID   SMALLINT      = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
              RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing  IDs from the Definitions Table
    -- -------------------------------------------------

    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

	DECLARE @v1 XML = (SELECT * FROM @Options FOR XML AUTO)
	DECLARE @v2 XML = (SELECT * FROM @Options_Array FOR XML AUTO)
    -- Remove missing IDs (AdHoc)
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
    ;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
	DECLARE @v3 XML = (select * from [Option.Data] D  FOR XML AUTO)

    UPDATE Opt
    SET InstanceID = 
           CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.LocationID = @LocationID)

                WHEN @EmployeeID IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.EmployeeID = @EmployeeID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT Top 1  ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID)

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT Top 1 ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.AssociationID = @AssociationID)

           ELSE NULL
           END
    FROM @Options Opt
    ;

    IF (@Debug=1)
        SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    ;
END
GO
PRINT N'Altering [dbo].[Option.GetCategoryOptions]...';


GO
-- ========================================================
-- 
-- Name: [Option.GetCategoryOptions]
--
-- Description: This procedure gets the options list for the selected category.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.GetCategorys] 
        -- Require Fields
          @CategoryID       = 1
		  @CategoryName     = 'COGS Accounts'
*/
-- ========================================================

ALTER PROCEDURE [Option.GetCategoryOptions]
-- DECLARE 
            @CategoryID        INT          = NULL
			,@CategoryName     VARCHAR(255) = NULL
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@CategoryID IS NULL) AND (@CategoryName IS NULL)) OR ((@CategoryID IS NOT NULL) AND (@CategoryName IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @CategoryID or the @CategoryName.', 1;

    -- ======================================
    -- Lookup CategoryID
    -- ======================================
    IF (@CategoryID IS NULL)
    BEGIN
        SELECT @CategoryID = ID 
        FROM [System.Option.Category] 
        WHERE Name = @CategoryName;

        IF (@CategoryID IS NULL)
			THROW 180000, '@CategoryName not found.', 1;
    END;

	WITH Category (ID, Name, Value, Level, CategoryID, CategoryName, SectionName)
	AS
	(
		SELECT D.ID, D.Name, O.Value, O.OptionLevel, C.ID, C.Name, S.Name
		FROM [System.Option.Definition] D
		INNER JOIN [Option.Data] O ON O.OptionID = D.ID
		INNER JOIN [System.Option.Category] C ON C.ID = D.CategoryID
		INNER JOIN [System.Option.Section] S ON S.ID = C.SectionID
	
		WHERE CategoryID = @CategoryID
	)
	SELECT *
	FROM Category
END
GO
PRINT N'Altering [dbo].[Option.GetSection]...';


GO
-- ========================================================
-- 
-- Name: [Option.GetSections]
--
-- Description: This procedure gets the heirarchy for sections and categories.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.GetSections] 
        -- Require Fields
          @SectionID       = 1
		  @SectionName     = 'Accounting'
*/
-- ========================================================

ALTER PROCEDURE [Option.GetSection]
-- DECLARE 
            @SectionID        INT          = NULL
			,@SectionName     VARCHAR(255) = NULL
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@SectionID IS NULL) AND (@SectionName IS NULL)) OR ((@SectionID IS NOT NULL) AND (@SectionName IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @SectionID or the @SectionName.', 1;

    -- ======================================
    -- Lookup SectionID
    -- ======================================
    IF (@SectionID IS NULL)
    BEGIN
        SELECT @SectionID = ID 
        FROM [System.Option.Section] 
        WHERE Name = @SectionName;

        IF (@SectionID IS NULL)
			THROW 180000, '@SectionName not found.', 1;
    END;

	WITH Section (LevelType, ID, Level, Name, Description, ParentID, ImageName)
	AS
	(
		SELECT CONVERT(VARCHAR(12),'Section'), S.ID, CONVERT(TINYINT, 0), S.Name, CONVERT(VARCHAR(max), NULL), CONVERT(SMALLINT, NULL) AS ParentID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Section] S
		WHERE S.ID = @SectionID

		UNION ALL

		SELECT CONVERT(VARCHAR(12),'Section'), S.ID, CONVERT(TINYINT, P.Level+1), S.Name, CONVERT(VARCHAR(max), NULL), P.ID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Section] S
		JOIN Section P on P.ID = S.ParentID AND P.LevelType = 'Section'

		UNION ALL

		SELECT CONVERT(VARCHAR(12),'Category'), C.ID, CONVERT(TINYINT, P.Level+1), C.Name, C.Description, P.ID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Category] C
		JOIN Section P on P.ID = C.SectionID AND P.LevelType = 'Section'
	)
	SELECT *
	FROM Section S
END
GO
PRINT N'Altering [dbo].[Util.ID.GetID]...';


GO
-- ========================================================
-- 
-- Name: Util.ID.GetID( @BID smallint, @ClassTypeID int, @Count int = 1 )
--
-- Description: This Function Gets a New ID for a ClassType (Table).
--  If requesting multiple IDs, the first ID in the series is returned.
--
-- Sample Use:   
--      declare @NewID int;
--      exec @NewID = dbo.[Util.ID.GetID] @BID=100, @ClassTypeID=3500, @Count=1
--
-- for Byte IDs, use
--      exec @NewID = dbo.[Util.ID.GetID] @BID=100, @ClassTypeID=3500, @StartingID=10
-- ========================================================
ALTER PROCEDURE [Util.ID.GetID] 
        @BID smallint
      , @ClassTypeID int

      -- Optional fields
      , @Count      int = 1   -- how many IDs are requested.  The first ID in a sequential block is returned.
      , @StartingID int = 1000
AS
BEGIN
    DECLARE @NextID INT;
    IF (@BID IS NULL) SET @BID = -1; -- Use -1 for System

    -- Assume the record exists ... 
    UPDATE [Util.NextID]
    SET @NextID = NextID = COALESCE(NextID, @StartingID-1) + @Count
    WHERE BID = @BID AND ClassTypeID = @ClassTypeID
    ;

    -- Check if no rows found, in which case use an add
    IF (@@RowCount=0)
    BEGIN
        SET @NextID = @StartingID + @Count;
        INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) VALUES(@BID, @ClassTypeID, @NextID);
    END;

    SELECT (@NextID-@Count) as FirstID;
    RETURN (@NextID-@Count);
END;
GO
PRINT N'Altering [dbo].[Util.ID.ValidateTableID]...';


GO
-- ========================================================
-- 
-- Name: Util.ID.ValidateTableID( @BID smallint, @ClassTypeID int, @TableName varchar(155) )
--
-- Description: This Function Gets a New ID for a ClassType (Table).
--  If requesting multiple IDs, the first ID in the series is returned.
--
-- Sample Use:   
--      exec dbo.[Util.ID.ValidateTableID] @BID=100, @ClassTypeID=3500, @TableName='Employee.Data'
--
-- ========================================================
ALTER PROCEDURE [Util.ID.ValidateTableID] 
        @BID smallint
      , @ClassTypeID int
      , @TableName  varchar(155) = '' -- The corresponding tablename.  Only used when @ValidateTable=1
AS
BEGIN
    DECLARE @NextTrackingID INT =  (
                    SELECT NextID
                    FROM [Util.NextID]
                    WHERE BID = @BID AND ClassTypeID = @ClassTypeID
                );

    DECLARE @NextTableID int
            , @cmd Nvarchar(max) = 'SELECT @outvar = MAX(ID)+1 FROM ['+@TableName+'] WHERE BID='+CONVERT(VARCHAR(12), @BID);

    EXEC SP_ExecuteSQL @Query = @cmd
                    , @Params = N'@outvar INT OUTPUT'
                    , @outvar = @NextTableID OUTPUT
    ;

    IF (@NextTrackingID IS NULL)
    BEGIN
        DECLARE @IDDataType VARCHAR(32)
              , @StartingID INT
              ;

        -- The the NextTableID is not high, we need to check if it is below the max
        IF (@NextTableID > 1000)
            SET @StartingID = 1000

        -- Look up the Data Type for the ID field
        ELSE
        BEGIN
            SET @IDDataType = (
                        SELECT DATA_TYPE 
                        FROM INFORMATION_SCHEMA.COLUMNS
                        WHERE TABLE_NAME = @TableName
                        AND COLUMN_NAME = 'ID'
                    );

            -- Set the starting value based on it
            SET @StartingID = (
                        CASE @IDDataType
                            WHEN 'tinyint' THEN 11
                            WHEN 'smallint' THEN 100
                            ELSE 1000
                        END
                    );
        END;

        INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) 
        VALUES( @BID
              , @ClassTypeID
              , IIF( @NextTableID > @StartingID, @NextTableID, @StartingID )
        );

        SELECT @TableName + ' NextID Updated to ' + CONVERT(Varchar(12), IIF( @NextTableID > 1000, @NextTableID, @StartingID ) )
        ;
    END

    ELSE IF (@NextTableID > @NextTrackingID)
    BEGIN
        UPDATE [Util.NextID]
        SET NextID = @NextTableID
        WHERE BID = @BID AND ClassTypeID = @ClassTypeID
        ;
        SELECT @TableName + ' NextID Updated to ' + CONVERT(Varchar(12), @NextTableID )
        ;
    END
    ELSE
        SELECT @TableName + ' Ok ';
END;
GO
PRINT N'Altering [dbo].[Option.SaveValue]...';


GO
-- ========================================================
-- 
-- Name: [Option.SaveValue]
--
-- Description: This procedure saves the setting for an option/setting.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.SaveValue] 
        -- Require Fields
          @OptionName       = 'GLAccount.TaxName2'
        , @OptionID         = NULL
        , @Value            = 'NewValue'  -- Pass in NULL to delete the current value (if any)

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @EmployeeID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

    SELECT * FROM [Option.Data]
*/
-- ========================================================

ALTER PROCEDURE [Option.SaveValue]
-- DECLARE 
            @OptionID       INT          = NULL
          , @OptionName     VARCHAR(255) = NULL
          , @Value          VARCHAR(MAX) = NULL     -- Pass in NULL to delete the current value (if any)

          , @AssociationID  TINYINT      = NULL
          , @BID            SMALLINT     = NULL
          , @LocationID     SMALLINT     = NULL
          , @EmployeeID     SMALLINT     = NULL
          , @CompanyID      INT          = NULL
          , @ContactID      INT          = NULL
          , @StorefrontID   SMALLINT	 = NULL

AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@OptionName IS NULL) AND (@OptionID IS NULL)) OR ((@OptionName IS NOT NULL) AND (@OptionID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @OptionID or the @OptionName.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================
    IF (@OptionID IS NULL)
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName;

        IF (@OptionID IS NULL)
        BEGIN
            -- If not defined, and NULL, there will be nothing to delete
            -- -------------------------------------------------
            IF (@Value IS NULL)
                RETURN;

            PRINT 'create new - check parameters, save and return id'

			DECLARE @NewID INT
			EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, 1;
			SET @OptionID = @NewID

            INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
            SELECT @OptionID AS ID
                 , @OptionName AS Name
                 , 'Custom: '+@OptionName AS Label
                 , NULL AS DefaultValue
                 , 0 AS DataType -- Always string
                 , -1 AS CategoryID  -- AdHoc Options
                 , 1 AS IsHidden
            ;

            PRINT @OptionID
        END
    END

    -- ======================================
    -- Lookup Current Value if it Exists
    -- ======================================
    DECLARE @InstanceID INT = 

        CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND LocationID = @LocationID)

                WHEN @EmployeeID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND EmployeeID = @EmployeeID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID )

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND AssociationID = @AssociationID )

                ELSE NULL
        END;

    -- ======================================
    -- Now Save (Update or Create) Value
    -- ======================================
    IF (@Value IS NOT NULL)
    BEGIN
        IF (@InstanceID IS NOT NULL)
            UPDATE [Option.Data]
            SET ModifiedDT = GetUTCDate()
            , Value = @Value
            WHERE ID = @InstanceID

        ELSE
            INSERT INTO [Option.Data] 
                ( CreatedDate, ModifiedDT, IsActive, OptionID
                , [Value], AssociationID, BID, LocationID
                , StoreFrontID, EmployeeID, CompanyID, ContactID
                )
            VALUES
                ( GetUTCDate(), GetUTCDate(), 1, @OptionID
                , @Value, @AssociationID, @BID, @LocationID
                , @StoreFrontID, @EmployeeID, @CompanyID, @ContactID
                );
    END

    -- ======================================
    -- Else Delete the Options with NULL Values Passed in
    -- ======================================
    ELSE
        DELETE FROM [Option.Data]
        WHERE ID = @InstanceID
    


END
GO
PRINT N'Altering [dbo].[Option.SaveValues]...';


GO

-- ===============================================
/*
    Name: [Option.SaveValues]

    Description: 
        Create a procedure that saves multiple options values
        at once.  The values are all given the same access
        level when saved.

    Sample Use:   

    DECLARE @T OptionsArray;
    INSERT INTO @T
      VALUES  (NULL, 'GLAccount.TaxName1', 'State')
            , (NULL, 'GLAccount.TaxName2', 'City')
            , (NULL, 'Employee.Collection.SortOrder', '4;5;1;5')
            , (NULL, 'Employee.OldSort', NULL)
            ;

    EXEC dbo.[Option.SaveValues] 
        -- Require Fields
          @Options_Array    = @T

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @EmployeeID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

        , @Debug            = 1

*/
-- ===============================================
ALTER PROCEDURE [dbo].[Option.SaveValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT       = NULL
    , @BID            SMALLINT      = NULL
    , @LocationID     SMALLINT      = NULL
    , @EmployeeID     SMALLINT      = NULL
    , @CompanyID      INT           = NULL
    , @ContactID      INT           = NULL
    , @StorefrontID   SMALLINT      = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
              RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing  IDs from the Definitions Table
    -- -------------------------------------------------
    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

    -- If not defined, and NULL, there will be nothing to delete
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
        AND (Value IS NULL)
    ;

    -- Any missing IDs must be AdHoc so create them
    -- -------------------------------------------------
    DECLARE @NewIDs INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);

    IF (@NewIDs > 0)
    BEGIN
		DECLARE @NewID INT
		DECLARE @NewIDCount INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);
		EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, @NewIDCount;


        -- Any missing IDs must be AdHoc so create them
        -- -------------------------------------------------
        UPDATE @Options
        SET   IsNewAdHoc = 1
            , OptionID = @NewID, @NewID = @NewID + 1
        WHERE OptionID IS NULL
        ;

        INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
        SELECT OptionID AS ID
                , OptionName AS Name
                , 'Custom: '+OptionName AS Label
                , NULL AS DefaultValue
                , 0 AS DataType -- Always string
                , -1 AS CategoryID  -- AdHoc Options
                , 1 AS IsHidden
        FROM @Options
        WHERE IsNewAdHoc = 1
        ;
    END;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
    UPDATE Opt
    SET InstanceID = 
           CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.LocationID = @LocationID)

                WHEN @EmployeeID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.EmployeeID = @EmployeeID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID)

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.AssociationID = @AssociationID)

           ELSE NULL
           END
    FROM @Options Opt
    ;

    IF (@Debug=1)
        SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NULL
    ;

    -- ======================================
    -- Update any Existing Values
    -- ======================================
    UPDATE D
    SET ModifiedDT = GetUTCDate()
      , Value = Opt.Value
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NOT NULL
    ;

    -- ======================================
    -- Create New Options if not Found Values
    -- ======================================
    INSERT INTO [Option.Data] 
        ( CreatedDate, ModifiedDT, IsActive, OptionID
        , [Value], AssociationID, BID, LocationID
        , StoreFrontID, EmployeeID, CompanyID, ContactID
        )
        SELECT
            GetUTCDate(), GetUTCDate(), 1, OptionID
            , Value, @AssociationID, @BID, @LocationID
            , @StoreFrontID, @EmployeeID, @CompanyID, @ContactID
        FROM @Options
        WHERE InstanceID IS NULL
          AND Value IS NOT NULL
    ;
END

GO
ALTER TABLE [dbo].[Accounting.Payment.Term.LocationLink] WITH CHECK CHECK CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term];

ALTER TABLE [dbo].[Accounting.Tax.Group.AssessmentLink] WITH CHECK CHECK CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment];

ALTER TABLE [dbo].[Accounting.Tax.Group.AssessmentLink] WITH CHECK CHECK CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group];

ALTER TABLE [dbo].[Accounting.Tax.Group.LocationLink] WITH CHECK CHECK CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_Accounting.Tax.Group];

ALTER TABLE [dbo].[Location.Data] WITH CHECK CHECK CONSTRAINT [FK_Location.Data_Accounting.Tax.Group];

ALTER TABLE [dbo].[CRM.CustomField.Def] WITH CHECK CHECK CONSTRAINT [FK_CRM.Setup.CustomField_Business.Data];

ALTER TABLE [dbo].[CRM.CustomField.Helper] WITH CHECK CHECK CONSTRAINT [FK_CRM.Setup.CustomField.Helper_Business.Data];

ALTER TABLE [dbo].[Employee.Data] WITH CHECK CHECK CONSTRAINT [FK_Employee.Data_Business.Data];

ALTER TABLE [dbo].[Business.Data] WITH CHECK CHECK CONSTRAINT [FK_Business.Data_Employee.Data1];

ALTER TABLE [dbo].[Business.Data] WITH CHECK CHECK CONSTRAINT [FK_Business.Data_Employee.Data];

ALTER TABLE [dbo].[Opportunity.Data] WITH CHECK CHECK CONSTRAINT [FK_Opportunity.Data_Business.Data];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_Business.Data];

ALTER TABLE [dbo].[Location.Data] WITH CHECK CHECK CONSTRAINT [FK_Location.Data_Business.Data];

ALTER TABLE [dbo].[Campaign.Data] WITH CHECK CHECK CONSTRAINT [FK_Campaign.Data_Business.Data];

ALTER TABLE [dbo].[Contact.Data] WITH CHECK CHECK CONSTRAINT [FK_Contact.Data_Business.Data];

ALTER TABLE [dbo].[Business.Locator] WITH CHECK CHECK CONSTRAINT [FK_Business.Locator_enum.LocatorSubType];

ALTER TABLE [dbo].[Business.Locator] WITH CHECK CHECK CONSTRAINT [FK_Business.Locator_enum.LocatorType];

ALTER TABLE [dbo].[Opportunity.Data] WITH CHECK CHECK CONSTRAINT [FK_Opportunity.Data_Campaign.Data];

ALTER TABLE [dbo].[Campaign.Data] WITH CHECK CHECK CONSTRAINT [FK_Campaign.Data_enum.Campaign.Type];

ALTER TABLE [dbo].[Campaign.Data] WITH CHECK CHECK CONSTRAINT [FK_Campaign.Data_Campaign.Custom];

ALTER TABLE [dbo].[Campaign.Data] WITH CHECK CHECK CONSTRAINT [FK_Campaign.Data_Employee.Team];

ALTER TABLE [dbo].[Opportunity.Data] WITH CHECK CHECK CONSTRAINT [FK_Opportunity.Data_Company.Data];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_enum.CRM.Company.Status];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_Company.Custom];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_Location.Data];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_enum.TimeZone];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_CRM.Industry];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_Company.Data];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_CRM.Source];

ALTER TABLE [dbo].[Company.Data] WITH CHECK CHECK CONSTRAINT [FK_Company.Data_Employee.Team];

ALTER TABLE [dbo].[Company.Locator] WITH CHECK CHECK CONSTRAINT [FK_Company.Locator_Company.Data];

ALTER TABLE [dbo].[Contact.Data] WITH CHECK CHECK CONSTRAINT [FK_Contact.Data_Company.Data];

ALTER TABLE [dbo].[Company.Locator] WITH CHECK CHECK CONSTRAINT [FK_Company.Locator_enum.LocatorType];

ALTER TABLE [dbo].[Company.Locator] WITH CHECK CHECK CONSTRAINT [FK_Company.Locator_enum.LocatorSubType];

ALTER TABLE [dbo].[User.Link] WITH CHECK CHECK CONSTRAINT [FK_User.Link_Contact.Data];

ALTER TABLE [dbo].[Opportunity.Data] WITH CHECK CHECK CONSTRAINT [FK_Opportunity.Data_Contact.Data];

ALTER TABLE [dbo].[Contact.Data] WITH CHECK CHECK CONSTRAINT [FK_Contact.Data_Contact.Custom];

ALTER TABLE [dbo].[Contact.Data] WITH CHECK CHECK CONSTRAINT [FK_Contact.Data_enum.TimeZone];

ALTER TABLE [dbo].[Contact.Data] WITH CHECK CHECK CONSTRAINT [FK_Contact.Data_Location.Data];

ALTER TABLE [dbo].[Contact.Locator] WITH CHECK CHECK CONSTRAINT [FK_Contact.Locator_Contact.Data];

ALTER TABLE [dbo].[Contact.Locator] WITH CHECK CHECK CONSTRAINT [FK_Contact.Locator_enum.LocatorSubType];

ALTER TABLE [dbo].[Contact.Locator] WITH CHECK CHECK CONSTRAINT [FK_Contact.Locator_enum.LocatorType];

ALTER TABLE [dbo].[CRM.CustomField.Def] WITH CHECK CHECK CONSTRAINT [FK_CRM.Setup.CustomField_enum.CustomField.DataType];

ALTER TABLE [dbo].[CRM.CustomField.Def] WITH CHECK CHECK CONSTRAINT [FK_CRM.Setup.CustomField_enum.CustomField.InputType];

ALTER TABLE [dbo].[CRM.CustomField.HelperLink] WITH CHECK CHECK CONSTRAINT [FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField];

ALTER TABLE [dbo].[CRM.CustomField.Helper] WITH CHECK CHECK CONSTRAINT [FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType];

ALTER TABLE [dbo].[CRM.CustomField.Helper] WITH CHECK CHECK CONSTRAINT [FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType];

ALTER TABLE [dbo].[CRM.CustomField.HelperLink] WITH CHECK CHECK CONSTRAINT [FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper];

ALTER TABLE [dbo].[CRM.Industry] WITH CHECK CHECK CONSTRAINT [FK_CRM.Industry_CRM.Industry];

ALTER TABLE [dbo].[CRM.Source] WITH CHECK CHECK CONSTRAINT [FK_CRM.Source_CRM.Source];

ALTER TABLE [dbo].[Employee.Data] WITH CHECK CHECK CONSTRAINT [FK_Employee.Data_ReportsToID];

ALTER TABLE [dbo].[Employee.Locator] WITH CHECK CHECK CONSTRAINT [FK_Employee.Locator_Employee.Data];

ALTER TABLE [dbo].[Employee.TeamLink] WITH CHECK CHECK CONSTRAINT [FK_Employee.TeamLink_Employee.Data];

ALTER TABLE [dbo].[List.Filter.EmployeeSubscription] WITH CHECK CHECK CONSTRAINT [FK_List.Filter.EmployeeSubscription_Employee.Data];

ALTER TABLE [dbo].[User.Link] WITH CHECK CHECK CONSTRAINT [FK_User.Link_Employee.Data];

ALTER TABLE [dbo].[Employee.Locator] WITH CHECK CHECK CONSTRAINT [FK_Employee.Locator_enum.LocatorType];

ALTER TABLE [dbo].[Employee.Locator] WITH CHECK CHECK CONSTRAINT [FK_Employee.Locator_enum.LocatorSubType];

ALTER TABLE [dbo].[Employee.TeamLink] WITH CHECK CHECK CONSTRAINT [FK_Employee.TeamLink_Employee.Team];

ALTER TABLE [dbo].[Opportunity.Data] WITH CHECK CHECK CONSTRAINT [FK_Opportunity.Data_Employee.Team];

ALTER TABLE [dbo].[Employee.Team.LocationLink] WITH CHECK CHECK CONSTRAINT [FK_Employee.Team.LocationLink_Employee.Team];

ALTER TABLE [dbo].[enum.CustomField.InputType] WITH CHECK CHECK CONSTRAINT [FK_enum.CustomField.InputType_enum.CustomField.DataType];

ALTER TABLE [dbo].[enum.Locator.SubType] WITH CHECK CHECK CONSTRAINT [FK_enum.Locator.SubType_enum.Locator.Type];

ALTER TABLE [dbo].[Location.Locator] WITH CHECK CHECK CONSTRAINT [FK_Business.Location.Locator_enum.LocatorSubType];

ALTER TABLE [dbo].[Location.Locator] WITH CHECK CHECK CONSTRAINT [FK_Location.Locator_enum.LocatorType];

ALTER TABLE [dbo].[Location.Data] WITH CHECK CHECK CONSTRAINT [FK_Location.Data_enum.TimeZone];

ALTER TABLE [dbo].[List.Filter.EmployeeSubscription] WITH CHECK CHECK CONSTRAINT [FK_List.Filter.EmployeeSubscription_List.Filter];

ALTER TABLE [dbo].[Accounting.Payment.Term.LocationLink] WITH CHECK CHECK CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Location.Data];

ALTER TABLE [dbo].[Accounting.Tax.Group.LocationLink] WITH CHECK CHECK CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Location.Data];

ALTER TABLE [dbo].[Employee.Team.LocationLink] WITH CHECK CHECK CONSTRAINT [FK_Employee.Team.LocationLink_Location.Data];

ALTER TABLE [dbo].[Location.Locator] WITH CHECK CHECK CONSTRAINT [FK_Location.Locator_Location.Data];

ALTER TABLE [dbo].[Opportunity.Data] WITH CHECK CHECK CONSTRAINT [FK_Opportunity.Data_Opportunity.Custom];

ALTER TABLE [dbo].[Option.Data] WITH CHECK CHECK CONSTRAINT [FK_Option.Data_System.Option.Definition];

ALTER TABLE [dbo].[Security.Right.Group.Link] WITH CHECK CHECK CONSTRAINT [FK_Security.Right.Group.Link_Security.Right.Group1];

ALTER TABLE [dbo].[Security.Right.Group.Link] WITH CHECK CHECK CONSTRAINT [FK_Security.Right.Group.Link_Security.Right.Group];

ALTER TABLE [dbo].[Security.Right.Link] WITH CHECK CHECK CONSTRAINT [FK_Security.Right.Link_Security.Right.Collection];

ALTER TABLE [dbo].[Security.Role.Link] WITH CHECK CHECK CONSTRAINT [FK_Security.Role.Link_Security.Right.Group];

ALTER TABLE [dbo].[User.Link] WITH CHECK CHECK CONSTRAINT [FK_User.Link_Security.Right.Collection];

ALTER TABLE [dbo].[System.Option.Category] WITH CHECK CHECK CONSTRAINT [FK_System.Option.Category_System.Option.Section];

ALTER TABLE [dbo].[System.Option.Definition] WITH CHECK CHECK CONSTRAINT [FK_System.Option.Definition_System.Option.Category];

ALTER TABLE [dbo].[System.Option.Definition] WITH CHECK CHECK CONSTRAINT [FK_System.Option.Definition_enum.CustomField.DataType];

ALTER TABLE [dbo].[System.Option.Section] WITH CHECK CHECK CONSTRAINT [FK_System.Option.Section_System.Option.Section];

ALTER TABLE [dbo].[User.Link] WITH CHECK CHECK CONSTRAINT [FK_User.Link_enum.User.Role.Type];

ALTER TABLE [dbo].[Security.Role.Link] WITH CHECK CHECK CONSTRAINT [FK_Security.Role.Link_enum.User.Role.Type];

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"");
        }
    }
}

