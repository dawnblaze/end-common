using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Update_SortIndexOfEmployeeFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.List.Filter.Criteria]
               SET [SortIndex] = 1
               WHERE ID = 4
            GO
            UPDATE [System.List.Filter.Criteria]
               SET [SortIndex] = 2
               WHERE ID = 1
            GO
            UPDATE [System.List.Filter.Criteria]
               SET [SortIndex] = 3
               WHERE ID = 5
            GO
            UPDATE [System.List.Filter.Criteria]
               SET [SortIndex] = 4
               WHERE ID = 3
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
