using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180808193211_AddSystemAutomationActionDefAndLink")]
    public partial class AddSystemAutomationActionDefAndLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "System.Automation.Action.Definition",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToAlerts = table.Column<bool>(nullable: false),
                    AppliesToAllDataTypes = table.Column<bool>(nullable: false),
                    AppliesToAutomations = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Automation.Action.Definition", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "System.Automation.Action.DataTypeLink",
                columns: table => new
                {
                    ActionID = table.Column<byte>(type: "tinyint", nullable: false),
                    ID = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Automation.Action.DataTypeLink", x => new { x.ActionID, x.ID });
                    table.ForeignKey(
                        name: "FK_System.Automation.Action.DataTypeLink_System.Automation.Action.Definition",
                        column: x => x.ActionID,
                        principalTable: "System.Automation.Action.Definition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.Sql(@"
INSERT [System.Automation.Action.Definition] ([ID], [Name], [AppliesToAlerts], [AppliesToAutomations], [AppliesToAllDataTypes])
VALUES (0, N'None', 0, 0, 1)
    , (1, N'Internal Alert', 1, 1, 1)
    , (2, N'Email', 1, 1, 1)
    , (3, N'Text Message (SMS)', 1, 1, 1)
    , (4, N'Slack Message', 1, 1, 1)
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "System.Automation.Action.DataTypeLink");

            migrationBuilder.DropTable(
                name: "System.Automation.Action.Definition");
        }
    }
}

