﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9755_AddGlobalCascadeDeleteSprocUpdateOtherSprocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"/* 
    This procedure deletes an ENTIRE BUSINESS from the database.
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.Business.Delete] @BID = 999, @TestOnly = 1 ;

*/
CREATE OR ALTER PROCEDURE [Util.Business.Delete]( @BID SMALLINT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteBusiness]
    BEGIN TRY

        DELETE FROM [Accounting.GL.Data] WHERE BID = @BID
        DELETE FROM [Order.Contact.Locator] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID
        DELETE FROM [Order.Contact.Role] WHERE BID = @BID
        DELETE FROM [Order.Employee.Role] WHERE BID = @BID
        DELETE FROM [Order.Item.Component] WHERE BID = @BID
        DELETE FROM [Order.Item.Surcharge] WHERE BID = @BID
        DELETE FROM [Order.Item.TagLink] WHERE BID = @BID
        DELETE FROM [Order.KeyDate] WHERE BID = @BID
        DELETE FROM [Order.Note] WHERE BID = @BID
        DELETE FROM [Order.Tax.Item.Assessment] WHERE BID = @BID
        DELETE FROM [Activity.GLActivity] WHERE BID = @BID
        DELETE FROM [Dashboard.Widget.Data] WHERE BID = @BID
        DELETE FROM [Order.Custom.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Data] WHERE BID = @BID
        DELETE FROM [Order.OrderLink] WHERE BID = @BID
        DELETE FROM [Order.TagLink] WHERE BID = @BID
        DELETE FROM [Dashboard.Data] WHERE BID = @BID
        DELETE FROM [Message.Delivery.Record] WHERE BID = @BID
        DELETE FROM [Message.Header] WHERE BID = @BID
        DELETE FROM [Order.Data] WHERE BID = @BID
        DELETE FROM [User.Draft] WHERE BID = @BID
        DELETE FROM [Alert.Definition.Action] WHERE BID = @BID
        DELETE FROM [Contact.Custom.Data] WHERE BID = @BID
        DELETE FROM [Contact.Locator] WHERE BID = @BID
        DELETE FROM [Contact.TagLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard.Detail] WHERE BID = @BID
        DELETE FROM [Message.Participant.Info] WHERE BID = @BID
        DELETE FROM [Opportunity.Data] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.CategoryLink] WHERE BID = @BID
        DELETE FROM [User.Link] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Master] WHERE BID = @BID
        DELETE FROM [Alert.Definition] WHERE BID = @BID
        DELETE FROM [Board.Employee.Link] WHERE BID = @BID
        DELETE FROM [Company.Contact.Link] WHERE BID = @BID
        DELETE FROM [Company.Custom.Data] WHERE BID = @BID
        DELETE FROM [Company.Locator] WHERE BID = @BID
        DELETE FROM [Company.TagLink] WHERE BID = @BID
        DELETE FROM [Contact.Data] WHERE BID = @BID
        DELETE FROM [Document.Report.Menu] WHERE BID = @BID
        DELETE FROM [Employee.Locator] WHERE BID = @BID
        DELETE FROM [Employee.TeamLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard] WHERE BID = @BID
        DELETE FROM [List.Filter.EmployeeSubscription] WHERE BID = @BID
		DELETE FROM [Part.Machine.Profile.Table] WHERE BID = @BID
        DELETE FROM [Part.Machine.Profile.Variable] WHERE BID = @BID
		DELETE FROM [Part.Machine.Profile] WHERE BID = @BID
		DELETE FROM [Part.Machine.Instance] WHERE BID = @BID
		DELETE FROM [Part.Assembly.Table] WHERE BID = @BID
		DELETE FROM [Part.Assembly.Variable.Formula] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Element] WHERE BID = @BID
        DELETE FROM [Part.Surcharge.Data] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code.ItemExemptionLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID
        DELETE FROM [Company.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Element] WHERE BID = @BID
        DELETE FROM [Domain.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.LocationLink] WHERE BID = @BID
        DELETE FROM [Employee.Data] WHERE BID = @BID
        DELETE FROM [Employee.Team.LocationLink] WHERE BID = @BID
        DELETE FROM [Location.Goal] WHERE BID = @BID
        DELETE FROM [Location.Locator] WHERE BID = @BID
        DELETE FROM [Order.Destination.TagLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Machine.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Material.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Layout] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Variable] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Method] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Item] WHERE BID = @BID
        DELETE FROM [Board.Module.Link] WHERE BID = @BID
        DELETE FROM [Board.Role.Link] WHERE BID = @BID
        DELETE FROM [Board.View.Link] WHERE BID = @BID
        DELETE FROM [Campaign.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Container] WHERE BID = @BID
        DELETE FROM [List.Tag.OtherLink] WHERE BID = @BID
		DELETE FROM [Option.Data] WHERE BID = @BID
        DELETE FROM [Location.Data] WHERE BID = @BID
        DELETE FROM [Message.Object.Link] WHERE BID = @BID
        DELETE FROM [Order.Destination.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.Data] WHERE BID = @BID
        DELETE FROM [Part.Machine.Data] WHERE BID = @BID
        DELETE FROM [Part.Material.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Data] WHERE BID = @BID
        DELETE FROM [Rights.Group.List.RightsGroupLink] WHERE BID = @BID
        DELETE FROM [Accounting.GL.Account] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Term] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group] WHERE BID = @BID
        DELETE FROM [Activity.Action] WHERE BID = @BID
        DELETE FROM [Activity.Event] WHERE BID = @BID
        DELETE FROM [Board.Definition.Data] WHERE BID = @BID
        DELETE FROM [Board.View] WHERE BID = @BID
        DELETE FROM [Business.Locator] WHERE BID = @BID
        DELETE FROM [CRM.Industry] WHERE BID = @BID
        DELETE FROM [CRM.Origin] WHERE BID = @BID
        DELETE FROM [CustomField.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Other.Data] WHERE BID = @BID
		DELETE FROM [Email.Account.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.Data] WHERE BID = @BID
        DELETE FROM [Employee.Role] WHERE BID = @BID
        DELETE FROM [Employee.Team] WHERE BID = @BID
        DELETE FROM [List.Filter] WHERE BID = @BID
        DELETE FROM [List.FlatList.Data] WHERE BID = @BID
        DELETE FROM [List.Tag] WHERE BID = @BID
        DELETE FROM [Message.Body] WHERE BID = @BID
        DELETE FROM [Order.Item.Status] WHERE BID = @BID
        DELETE FROM [Order.Item.SubStatus] WHERE BID = @BID
        DELETE FROM [Part.Labor.Category] WHERE BID = @BID
        DELETE FROM [Part.Machine.Category] WHERE BID = @BID
        DELETE FROM [Part.Material.Category] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Category] WHERE BID = @BID
        DELETE FROM [Report.Menu] WHERE BID = @BID
        DELETE FROM [Rights.Group.List] WHERE BID = @BID
        DELETE FROM [SSLCertificate.Data] WHERE BID = @BID
        DELETE FROM [Util.NextID] WHERE BID = @BID
		DELETE FROM [Business.TimeZone.Link] WHERE BID = @BID

        DELETE FROM [Business.Data] WHERE BID = @BID

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteBusiness]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteBusiness];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [DeleteBusiness]

END;
");

            migrationBuilder.Sql(@"/* 
    This procedure deletes ALL orders for a BID.  
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.Company.Delete] @BID = 1, @CompanyID = 1013, @TestOnly = 1 ;
*/
CREATE OR ALTER PROCEDURE [dbo].[Util.Company.Delete]( @BID SMALLINT, @CompanyID INT, @TestOnly BIT = 1 )
AS
BEGIN
    DECLARE @ExecuteSQL BIT = ~@TestOnly;

    EXEC dbo.[Util.Table.CascadeDelete] @TableName = 'dbo.[Company.Data]', @BID = @BID, @ID = @CompanyID, @ReturnSQL = @TestOnly, @ExecuteSQL = 0, @ReturnAffectedTables = 0
END;
");
            migrationBuilder.Sql(@"-- ========================================================
/*
Name: [Util.Table.CascadeDelete] @TableName VARCHAR(255), @BID SMALLINT, @ID INT 
Description: For the specified Table, forces a CASCADE delete on the item 
    AND ALL FOREIGN KEY dependencies.  Be very careful with this, as it deletes 
    far more than just the row being deleted.

Sample Use:   

    BEGIN TRANSACTION 
        DECLARE @BID SMALLINT = 1;
        DECLARE @ID INT = 1000;
        SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ID
        SELECT * FROM [Order.Data] WHERE BID = @BID AND CompanyID = @ID

        EXEC [Util.Table.CascadeDelete] @TableName = 'dbo.[Company.Data]', @BID = @BID, @ID = @ID, @ReturnSQL = 1, @ExecuteSQL = 0, @ReturnAffectedTables = 1

        SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ID
        SELECT * FROM [Order.Data] WHERE BID = @BID AND CompanyID = @ID
    ROLLBACK TRANSACTION 
*/
-- ========================================================
CREATE OR ALTER PROCEDURE dbo.[Util.Table.CascadeDelete]
        @TableName VARCHAR(255) -- use two part name convention
        , @BID SMALLINT 
        , @ID INT
        , @RollbackOnError BIT = 1
        , @ExecuteSQL BIT = 0
        , @ReturnSQL BIT = 1
        , @ReturnAffectedTables BIT = 1
AS
BEGIN
    SET NOCOUNT ON;

    DROP TABLE IF EXISTS #DependencyList;

    CREATE TABLE #DependencyList (
            id int, 
            tablename VARCHAR(255), 
            RecurseLevel int, 
            ParentTable VARCHAR(255)
            );

    INSERT INTO #DependencyList 
        EXEC dbo.[Util.Table.DependencyList] @TableName=@TableName, @debug=0;

    DECLARE @where VARCHAR(MAX) = CONCAT('  WHERE ', @TableName, '.BID = @BID AND ', @TableName, '.ID = @ID ')
          , @FKCursor cursor
          , @FK_ObjectID int
          , @crlf char(2)=char(0x0d)+char(0x0a)
          , @ChildTable VARCHAR(255)
          , @ParentTable VARCHAR(255)
          , @RecurseLevel int, @CurrentID int
          , @i int
    ;

    DECLARE @SQLCmd NVARCHAR(MAX)=CONCAT(
                  'DECLARE @BID SMALLINT =' , @BID, ';', @crlf
                , 'DECLARE @ID INT =' , @ID, ';', @crlf, @crlf
          );


    DECLARE curT CURSOR FOR
        SELECT  RecurseLevel, id
        FROM #DependencyList
        ORDER BY RecurseLevel DESC;

    OPEN curT;
    FETCH NEXT FROM curT into  @RecurseLevel, @CurrentID;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        SET @i=0;
        IF @RecurseLevel = 0
            SELECT @SQLCmd += CONCAT('  DELETE FROM ', tablename)
            FROM #DependencyList 
            WHERE id = @CurrentID;

        WHILE @i < @RecurseLevel
        BEGIN -- WHILE

            SELECT TOP(1) @ChildTable=TableName, @ParentTable=ParentTable 
            FROM #DependencyList 
            WHERE id <= @CurrentID-@i AND RecurseLevel <= @RecurseLevel-@i 
            ORDER BY RecurseLevel DESC, id DESC;

            SET @FKCursor = CURSOR FOR
                -- There is a bug if there are multiple FKs to the same table.  For now, the only (partial) workaround is to only use one.
                SELECT TOP(1) object_id 
                FROM sys.foreign_keys 
                WHERE parent_object_id = object_id(@ChildTable)
                AND referenced_object_id = object_id(@ParentTable)

            OPEN @FKCursor;
            FETCH NEXT FROM @FKCursor INTO @FK_ObjectID
            WHILE @@fetch_status =0
            BEGIN -- @FKCursor

                IF @i=0
                    SET @SQLCmd += CONCAT('  DELETE ', @ChildTable, @crlf, '  FROM ', @ChildTable, @crlf, '  JOIN ', @ParentTable);
                ELSE
                    SET @SQLCmd += CONCAT(@crlf, '  JOIN ', @ParentTable);

                WITH c AS 
                (
                    SELECT child = object_schema_name(fc.parent_object_id)+'.' + object_name(fc.parent_object_id), child_col=c.name
                    , parent = object_schema_name(fc.referenced_object_id)+'.' + object_name(fc.referenced_object_id), parent_col=c2.name
                    , rnk = row_number() over (ORDER BY (SELECT null))
                    FROM sys.foreign_key_columns fc
                    JOIN sys.columns c ON fc.parent_column_id = c.column_id AND fc.parent_object_id = c.object_id
                    JOIN sys.columns c2 ON fc.referenced_column_id = c2.column_id AND fc.referenced_object_id = c2.object_id
                    WHERE fc.constraint_object_id = @FK_ObjectID
                )
                    SELECT @SQLCmd += (CASE rnk when 1 then ' ON '  ELSE ' AND ' END) 
                                        + (@ChildTable +'.'+ child_col +'='  +  @ParentTable +'.' + parent_col)
                    FROM c;

                FETCH NEXT FROM @FKCursor into @FK_ObjectID;
            END --@FKCursor

            CLOSE @FKCursor;
            DEALLOCATE @FKCursor;
            SET @i += 1;
        END --WHILE

        SET @SQLCmd += CONCAT(@crlf, @where, ';', @crlf, @crlf);

        FETCH NEXT FROM curT into  @RecurseLevel, @CurrentID;

    END
    
    CLOSE curT;
    DEALLOCATE curT;

    IF (@RollbackOnError = 1)
    BEGIN
        SET @SQLCmd = CONCAT(
                'BEGIN TRANSACTION' , @crlf
                , 'BEGIN TRY' , @crlf
                , @SQLCmd , @crlf
                , 'COMMIT TRANSACTION' , @crlf
                , 'END TRY' , @crlf
                , 'BEGIN CATCH' , @crlf
                , '  ROLLBACK TRANSACTION;' , @crlf
                , '  THROW;' , @crlf
                , 'END CATCH' , @crlf
        );

    END;

    IF (@ExecuteSQL = 1)
        EXEC sp_executesql @SQLCmd ;

    IF (@ReturnSQL = 1)
        SELECT @SQLCmd as SQLCommand;

    IF (@ReturnAffectedTables = 1)
        SELECT DISTINCT tablename as TableName
        FROM #DependencyList;

    DROP TABLE #DependencyList;
END");
            migrationBuilder.Sql(@"-- ========================================================
/*
Name: [Util.Table.DependencyList]  @TableName VARCHAR(255) 
Description: For the specified Table, retrieves the dependency list.

Sample Use:   

    EXEC [Util.Table.DependencyList] @TableName = 'dbo.[Part.Material.Data]'
*/
-- ========================================================
CREATE OR ALTER PROCEDURE dbo.[Util.Table.DependencyList]
        @TableName VARCHAR(255) -- use two part name convention
        , @RecurseLevel INT = 0 -- do not change
        , @ParentTable VARCHAR(255) = '' -- do not change
        , @Debug BIT = 0
AS
BEGIN
    SET nocount on;
    DECLARE @dbg BIT = @Debug
          , @curS cursor
    ;

    -- The temp table is shared between this SPROC and the [Util.Table.CascadeDelete] SPROC
    IF object_id('tempdb..#TempTable', 'U') IS NULL
        CREATE TABLE #TempTable (
                id int, 
                tablename VARCHAR(255), 
                RecurseLevel int, 
                ParentTable VARCHAR(255)
                );


    IF @RecurseLevel = 0

        insert into #TempTable (tablename, RecurseLevel, ParentTable)
            SELECT @TableName, @RecurseLevel, Null;
    
    ELSE

        insert into #TempTable (tablename, RecurseLevel, ParentTable)
            SELECT @TableName, @RecurseLevel, @ParentTable;
    
    IF @dbg=1	
        print replicate('--
        --', @RecurseLevel) + 'RecurseLevel ' + cast(@RecurseLevel as varchar(10)) + ' = ' + @TableName;
    
    IF not exists (SELECT * FROM sys.foreign_keys WHERE referenced_object_id = object_id(@TableName))
        return;

    ELSE
    BEGIN -- ELSE
        SET @ParentTable = @TableName
        SET @curS = 
                cursor for
                    SELECT tablename=CONCAT(object_schema_name(parent_object_id),'.[',object_name(parent_object_id),']')
                    FROM sys.foreign_keys 
                    WHERE referenced_object_id = object_id(@TableName)
                    AND parent_object_id <> referenced_object_id; -- add this to prevent self-referencing which can create a indefinitive loop;

        OPEN @curS;
        FETCH NEXT FROM @curS INTO @TableName;

        WHILE @@fetch_status = 0
        BEGIN --while
            SET @RecurseLevel += 1;
            -- recursive call
            exec dbo.[Util.Table.DependencyList] @TableName, @RecurseLevel, @ParentTable, @dbg;
            SET @RecurseLevel -= 1;
            
            FETCH NEXT FROM @curS INTO @TableName;
        END --while
        CLOSE @curS;
        deallocate @curS;
    END -- ELSE

    IF @RecurseLevel = 0
        SELECT * FROM #TempTable;

    return;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"ALTER     PROCEDURE [dbo].[Util.Business.Delete]( @BID SMALLINT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteBusiness]
    BEGIN TRY

        DELETE FROM [Accounting.GL.Data] WHERE BID = @BID
        DELETE FROM [Order.Contact.Locator] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID
        DELETE FROM [Order.Contact.Role] WHERE BID = @BID
        DELETE FROM [Order.Employee.Role] WHERE BID = @BID
        DELETE FROM [Order.Item.Component] WHERE BID = @BID
        DELETE FROM [Order.Item.Surcharge] WHERE BID = @BID
        DELETE FROM [Order.Item.TagLink] WHERE BID = @BID
        DELETE FROM [Order.KeyDate] WHERE BID = @BID
        DELETE FROM [Order.Note] WHERE BID = @BID
        DELETE FROM [Order.Tax.Item.Assessment] WHERE BID = @BID
        DELETE FROM [Activity.GLActivity] WHERE BID = @BID
        DELETE FROM [Dashboard.Widget.Data] WHERE BID = @BID
        DELETE FROM [Order.Custom.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Data] WHERE BID = @BID
        DELETE FROM [Order.OrderLink] WHERE BID = @BID
        DELETE FROM [Order.TagLink] WHERE BID = @BID
        DELETE FROM [Dashboard.Data] WHERE BID = @BID
        DELETE FROM [Message.Delivery.Record] WHERE BID = @BID
        DELETE FROM [Message.Header] WHERE BID = @BID
        DELETE FROM [Order.Data] WHERE BID = @BID
        DELETE FROM [User.Draft] WHERE BID = @BID
        DELETE FROM [Alert.Definition.Action] WHERE BID = @BID
        DELETE FROM [Contact.Custom.Data] WHERE BID = @BID
        DELETE FROM [Contact.Locator] WHERE BID = @BID
        DELETE FROM [Contact.TagLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard.Detail] WHERE BID = @BID
        DELETE FROM [Message.Participant.Info] WHERE BID = @BID
        DELETE FROM [Opportunity.Data] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.CategoryLink] WHERE BID = @BID
        DELETE FROM [User.Link] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Master] WHERE BID = @BID
        DELETE FROM [Alert.Definition] WHERE BID = @BID
        DELETE FROM [Board.Employee.Link] WHERE BID = @BID
        DELETE FROM [Company.Custom.Data] WHERE BID = @BID
        DELETE FROM [Company.Locator] WHERE BID = @BID
        DELETE FROM [Company.TagLink] WHERE BID = @BID
        DELETE FROM [Contact.Data] WHERE BID = @BID
        DELETE FROM [Document.Report.Menu] WHERE BID = @BID
        DELETE FROM [Employee.Locator] WHERE BID = @BID
        DELETE FROM [Employee.TeamLink] WHERE BID = @BID
        DELETE FROM [Employee.TimeCard] WHERE BID = @BID
        DELETE FROM [List.Filter.EmployeeSubscription] WHERE BID = @BID
		DELETE FROM [Part.Machine.Profile.Table] WHERE BID = @BID
        DELETE FROM [Part.Machine.Profile.Variable] WHERE BID = @BID
		DELETE FROM [Part.Machine.Profile] WHERE BID = @BID
		DELETE FROM [Part.Machine.Instance] WHERE BID = @BID
		DELETE FROM [Part.Assembly.Table] WHERE BID = @BID
		DELETE FROM [Part.Assembly.Variable.Formula] WHERE BID = @BID
        DELETE FROM [Part.QuickItem.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Element] WHERE BID = @BID
        DELETE FROM [Part.Surcharge.Data] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code.ItemExemptionLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID
        DELETE FROM [Company.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Element] WHERE BID = @BID
        DELETE FROM [Domain.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.LocationLink] WHERE BID = @BID
        DELETE FROM [Employee.Data] WHERE BID = @BID
        DELETE FROM [Employee.Team.LocationLink] WHERE BID = @BID
        DELETE FROM [Location.Goal] WHERE BID = @BID
        DELETE FROM [Location.Locator] WHERE BID = @BID
        DELETE FROM [Order.Destination.TagLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Machine.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Material.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Layout] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Variable] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Method] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Item] WHERE BID = @BID
        DELETE FROM [Board.Module.Link] WHERE BID = @BID
        DELETE FROM [Board.Role.Link] WHERE BID = @BID
        DELETE FROM [Board.View.Link] WHERE BID = @BID
        DELETE FROM [Campaign.Data] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Container] WHERE BID = @BID
        DELETE FROM [List.Tag.OtherLink] WHERE BID = @BID
		DELETE FROM [Option.Data] WHERE BID = @BID
        DELETE FROM [Location.Data] WHERE BID = @BID
        DELETE FROM [Message.Object.Link] WHERE BID = @BID
        DELETE FROM [Order.Destination.Data] WHERE BID = @BID
        DELETE FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID
        DELETE FROM [Part.Labor.Data] WHERE BID = @BID
        DELETE FROM [Part.Machine.Data] WHERE BID = @BID
        DELETE FROM [Part.Material.Data] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Data] WHERE BID = @BID
        DELETE FROM [Rights.Group.List.RightsGroupLink] WHERE BID = @BID
        DELETE FROM [Accounting.GL.Account] WHERE BID = @BID
        DELETE FROM [Accounting.Payment.Term] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Code] WHERE BID = @BID
        DELETE FROM [Accounting.Tax.Group] WHERE BID = @BID
        DELETE FROM [Activity.Action] WHERE BID = @BID
        DELETE FROM [Activity.Event] WHERE BID = @BID
        DELETE FROM [Board.Definition.Data] WHERE BID = @BID
        DELETE FROM [Board.View] WHERE BID = @BID
        DELETE FROM [Business.Locator] WHERE BID = @BID
        DELETE FROM [CRM.Industry] WHERE BID = @BID
        DELETE FROM [CRM.Origin] WHERE BID = @BID
        DELETE FROM [CustomField.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Layout.Definition] WHERE BID = @BID
        DELETE FROM [CustomField.Other.Data] WHERE BID = @BID
		DELETE FROM [Email.Account.Data] WHERE BID = @BID
        DELETE FROM [Domain.Email.Data] WHERE BID = @BID
        DELETE FROM [Employee.Role] WHERE BID = @BID
        DELETE FROM [Employee.Team] WHERE BID = @BID
        DELETE FROM [List.Filter] WHERE BID = @BID
        DELETE FROM [List.FlatList.Data] WHERE BID = @BID
        DELETE FROM [List.Tag] WHERE BID = @BID
        DELETE FROM [Message.Body] WHERE BID = @BID
        DELETE FROM [Order.Item.Status] WHERE BID = @BID
        DELETE FROM [Order.Item.SubStatus] WHERE BID = @BID
        DELETE FROM [Part.Labor.Category] WHERE BID = @BID
        DELETE FROM [Part.Machine.Category] WHERE BID = @BID
        DELETE FROM [Part.Material.Category] WHERE BID = @BID
        DELETE FROM [Part.Subassembly.Category] WHERE BID = @BID
        DELETE FROM [Report.Menu] WHERE BID = @BID
        DELETE FROM [Rights.Group.List] WHERE BID = @BID
        DELETE FROM [SSLCertificate.Data] WHERE BID = @BID
        DELETE FROM [Util.NextID] WHERE BID = @BID
		DELETE FROM [Business.TimeZone.Link] WHERE BID = @BID

        DELETE FROM [Business.Data] WHERE BID = @BID

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteBusiness]
            RETURN;
        END;

    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteBusiness];

        THROW; -- Rethrow the error
    END CATCH

    COMMIT TRANSACTION [DeleteBusiness]

END;");
            migrationBuilder.Sql(@"ALTER   PROCEDURE [dbo].[Util.Company.Delete]( @BID SMALLINT, @CompanyID INT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION;
    BEGIN TRY

        -- Delete Contacts and related fields

        DELETE FROM [User.Link] WHERE BID = @BID AND ContactID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);

        DELETE FROM [Contact.] WHERE BID = @BID AND ID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Contact.] WHERE BID = @BID AND ID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Contact.] WHERE BID = @BID AND ID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);

        DELETE FROM [Contact.Custom.Data] WHERE BID = @BID AND ID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Contact.Locator] WHERE BID = @BID AND ParentID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Contact.TagLink] WHERE BID = @BID AND ContactID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);

        DELETE FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID
        

        -- Delete Company Related Fields

        DELETE FROM [Accounting.GLData] WHERE BID = @BID AND CompanyID = @CompanyID
        DELETE FROM [Accounting.Payment.Master] WHERE BID = @BID AND CompanyID = @CompanyID
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID AND CompanyID = @CompanyID

        DELETE FROM [Company.Custom.Data] WHERE BID = @BID AND ID = @CompanyID
        DELETE FROM [Company.Locator] WHERE BID = @BID AND ParentID = @CompanyID
        DELETE FROM [Company.TagLink] WHERE BID = @BID AND CompanyID = @CompanyID
        DELETE FROM [Part.QuickItem.CategoryLink] WHERE BID = @BID AND QuickItemID in (SELECT ID FROM [Part.QuickItem.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Part.QuickItem.Data] WHERE BID = @BID AND CompanyID = @CompanyID
        DELETE FROM [Part.Surcharge.Data] WHERE BID = @BID AND CompanyID = @CompanyID


        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION;
            RETURN;
        END;

        COMMIT TRANSACTION;
    
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;

        THROW; -- Rethrow the error
    END CATCH

END;");

            migrationBuilder.Sql(@"DROP PROCEDURE dbo.[Util.Table.CascadeDelete];");
            migrationBuilder.Sql(@"DROP PROCEDURE dbo.[Util.Table.DependencyList];");
        }
    }
}
