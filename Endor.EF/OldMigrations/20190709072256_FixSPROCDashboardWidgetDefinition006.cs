using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixSPROCDashboardWidgetDefinition006 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.006]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Financial Snapshot 
    The Financial Snapshot Widget displays a graph or a chart that summarizes many of the key financial data points for the business or location.  
    The information is most valuable when (visually) compared to the sales goal of the business or location to see how progress for the month is going.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/991657993/Dashboard+Widget+Financial+Snapshot
    for additional information.
        1. Monthly Goal
        2. MTD Sales
        3. MTD Payments
        4. Current A/Rs
        5. MTD Closed
        6. Current Pre-WIP
        7. Current WIP
        8. Current Built
        9. Current Invoicing
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.006] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

ALTER   PROCEDURE [dbo].[Dashboard.Widget.Definition.006]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
        -- DECLARE @BID SMALLINT = 1;
        -- DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
        -- DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
        -- DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
        -- DECLARE @AsTable BIT = 1;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    --                   = 26 for Closed                   = 9 for Closed
    --                   = 29 for Voided                   = 2 for Voided
    

    -- ---------------------------------------------
    -- Compute some working Data
    -- ---------------------------------------------
    DECLARE @MidDate DateTime2(2) = (SELECT DATEADD(SECOND, DATEDIFF(SECOND, @StartUTCDT, @EndUTCDT )/2, @StartUTCDT));


    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , ID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );


    -- Pull ID = 1 - Monthly Goal
    INSERT INTO @Results
        SELECT @LocationID, 1, 'Monthly Goal', 1, SUM(LG.Budgeted)
        FROM [Location.Goal] LG
        WHERE LG.BID = @BID AND (@LocationID = LG.LocationID OR @LocationID IS NULL)
          AND LG.Year = DatePart(Year, @MidDate)
          AND LG.Month = DatePart(Month, @MidDate)
    ;

    -- Pull ID = 2 - MTD Sales
    -- Rework to pull from GL .. but for now ... 
    INSERT INTO @Results
        SELECT @LocationID, 2, 'MTD Sales', COUNT(*), SUM(O.[Price.PreTax])
        FROM [Order.Data] O
        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 8 -- Invoiced
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- Pull ID = 3 - MTD Payments
    -- to get this, we can look at all new master payments (money in) and all payment applications refunds (money out)
    DECLARE @MoneyIn DECIMAL(18,4) = 
                    ( SELECT SUM(Amount)
                       FROM [Accounting.Payment.Master] P
                       WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                       AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                       AND P.PaymentTransactionType = 1 
                    )
    ;

    DECLARE @MoneyOut DECIMAL(18,4) = 
                    (   SELECT SUM(Amount)
                        FROM [Accounting.Payment.Application] P
                        WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                        AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                        AND P.PaymentTransactionType = 2
                    )
    ;

    INSERT INTO @Results
        SELECT @LocationID, 3, 'MTD Payments', 1, ISNULL(@MoneyIn, 0) + IsNull(@MoneyOut, 0)
    ;

    -- Pull ID = 4 - Current A/Rs
    -- Pull ID = 6 - Current Pre-WIP
    -- Pull ID = 7 - Current WIP
    -- Pull ID = 8 - Current Built
    -- Pull ID = 9 - Current Invoicing
    INSERT INTO @Results
        SELECT @LocationID
                , CASE OrderStatusID WHEN 21 THEN 6 WHEN 22 THEN 7 WHEN 23 THEN 8 WHEN 24 THEN 9 WHEN 25 THEN 4 END
                , 'Current ' + CASE OrderStatusID WHEN 21 THEN 'Pre-WIP' WHEN 22 THEN 'WIP' WHEN 23 THEN 'Built' WHEN 24 THEN 'Invoicing' WHEN 25 THEN 'A/Rs' END
                , TheCount
                , TheSum
        FROM 
        (
            SELECT OS.ID AS [OrderStatusID]
                    , COUNT(O.[Price.PreTax]) TheCount
                    , SUM(O.[Price.PreTax]) TheSum 
            FROM [Order.Data] O
            RIGHT JOIN (Values (21), (22), (23), (24), (25)) AS OS(ID) ON O.OrderStatusID = OS.ID
            WHERE (O.BID IS NULL)
               OR (O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL))
            GROUP BY OS.ID
        ) AS Temp
    ;

    -- Pull ID = 5 - MTD Closed
    INSERT INTO @Results
        SELECT @LocationID, 5, 'MTD Closed', COUNT(*), SUM(O.[Price.PreTax])
        FROM [Order.Data] O
        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 9 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------
END


            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.006]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Financial Snapshot 
    The Financial Snapshot Widget displays a graph or a chart that summarizes many of the key financial data points for the business or location.  
    The information is most valuable when (visually) compared to the sales goal of the business or location to see how progress for the month is going.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/991657993/Dashboard+Widget+Financial+Snapshot
    for additional information.
        1. Monthly Goal
        2. MTD Sales
        3. MTD Payments
        4. Current A/Rs
        5. MTD Closed
        6. Current Pre-WIP
        7. Current WIP
        8. Current Built
        9. Current Invoicing
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
    DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.006] 
        @BID = @BID, @LocationID = @LocationID
        , @StartUTCDT = @StartUTCDT, @EndUTCDT = @EndUTCDT
        , @AsTable = 1
    ;
*/

ALTER   PROCEDURE [dbo].[Dashboard.Widget.Definition.006]
                 @BID SMALLINT
               , @LocationID TINYINT
               , @StartUTCDT DateTime2(2)
               , @EndUTCDT DateTime2(2)
               , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
        -- DECLARE @BID SMALLINT = 1;
        -- DECLARE @LocationID TINYINT = 143; -- (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
        -- DECLARE @StartUTCDT DateTime2(2) = DATEADD(Month, -1, EOMonth(GetUTCDate()));
        -- DECLARE @EndUTCDT DateTime2(2) = EOMonth(GetUTCDate());
        -- DECLARE @AsTable BIT = 1;


    --  Note
    --     OrderStatusID = 21 for Pre-WIP      KeyDateType = 1 for Created
    --                   = 22 for WIP                      = 5 for WIP
    --                   = 23 for Built                    = 6 for Built
    --                   = 24 for Invoicing                = 7 for Invoicing
    --                   = 25 for Invoiced                 = 8 for Invoiced
    --                   = 26 for Closed                   = 9 for Closed
    --                   = 29 for Voided                   = 2 for Voided
    

    -- ---------------------------------------------
    -- Compute some working Data
    -- ---------------------------------------------
    DECLARE @MidDate DateTime2(2) = (SELECT DATEADD(SECOND, DATEDIFF(SECOND, @StartUTCDT, @EndUTCDT )/2, @StartUTCDT));


    -- ---------------------------------------------
    -- Pull the data
    -- ---------------------------------------------
    DECLARE   @Results Table 
                ( 
                      LocationID TINYINT
                    , OrderStatusID TINYINT
                    , Name VARCHAR(100)
                    , Count INT
                    , Amount DECIMAL(18,4)
                );


    -- Pull ID = 1 - Monthly Goal
    INSERT INTO @Results
        SELECT @LocationID, 1, 'Monthly Goal', 1, SUM(LG.Budgeted)
        FROM [Location.Goal] LG
        WHERE LG.BID = @BID AND (@LocationID = LG.LocationID OR @LocationID IS NULL)
          AND LG.Year = DatePart(Year, @MidDate)
          AND LG.Month = DatePart(Month, @MidDate)
    ;

    -- Pull ID = 2 - MTD Sales
    -- Rework to pull from GL .. but for now ... 
    INSERT INTO @Results
        SELECT @LocationID, 2, 'MTD Sales', COUNT(*), SUM(O.[Price.PreTax])
        FROM [Order.Data] O
        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 8 -- Invoiced
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- Pull ID = 3 - MTD Payments
    -- to get this, we can look at all new master payments (money in) and all payment applications refunds (money out)
    DECLARE @MoneyIn DECIMAL(18,4) = 
                    ( SELECT SUM(Amount)
                       FROM [Accounting.Payment.Master] P
                       WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                       AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                       AND P.PaymentTransactionType = 1 
                    )
    ;

    DECLARE @MoneyOut DECIMAL(18,4) = 
                    (   SELECT SUM(Amount)
                        FROM [Accounting.Payment.Application] P
                        WHERE P.BID = @BID AND (@LocationID = P.LocationID OR @LocationID IS NULL)
                        AND P.AccountingDT BETWEEN @StartUTCDT AND @EndUTCDT
                        AND P.PaymentTransactionType = 2
                    )
    ;

    INSERT INTO @Results
        SELECT @LocationID, 3, 'MTD Payments', 1, ISNULL(@MoneyIn, 0) + IsNull(@MoneyOut, 0)
    ;

    -- Pull ID = 4 - Current A/Rs
    -- Pull ID = 6 - Current Pre-WIP
    -- Pull ID = 7 - Current WIP
    -- Pull ID = 8 - Current Built
    -- Pull ID = 9 - Current Invoicing
    INSERT INTO @Results
        SELECT @LocationID
                , CASE OrderStatusID WHEN 21 THEN 6 WHEN 22 THEN 7 WHEN 23 THEN 8 WHEN 24 THEN 9 WHEN 25 THEN 4 END
                , 'Current ' + CASE OrderStatusID WHEN 21 THEN 'Pre-WIP' WHEN 22 THEN 'WIP' WHEN 23 THEN 'Built' WHEN 24 THEN 'Invoicing' WHEN 25 THEN 'A/Rs' END
                , TheCount
                , TheSum
        FROM 
        (
            SELECT OS.ID AS [OrderStatusID]
                    , COUNT(O.[Price.PreTax]) TheCount
                    , SUM(O.[Price.PreTax]) TheSum 
            FROM [Order.Data] O
            RIGHT JOIN (Values (21), (22), (23), (24), (25)) AS OS(ID) ON O.OrderStatusID = OS.ID
            WHERE (O.BID IS NULL)
               OR (O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL))
            GROUP BY OS.ID
        ) AS Temp
    ;

    -- Pull ID = 5 - MTD Closed
    INSERT INTO @Results
        SELECT @LocationID, 5, 'MTD Closed', COUNT(*), SUM(O.[Price.PreTax])
        FROM [Order.Data] O
        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
        AND EXISTS( SELECT * 
                    FROM [Order.KeyDate] OD 
                    WHERE OD.BID = O.BID
                        AND OD.OrderID = O.ID 
                        AND OD.OrderItemID IS NULL
                        AND OD.KeyDateType = 9 -- Closed
                        AND OD.KeyDT BETWEEN @StartUTCDT AND @EndUTCDT
        )
    ;

    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT *
        FROM @Results R
        ORDER BY 2
        ;

    ELSE
    BEGIN
        SELECT *
        FROM @Results R
        ORDER BY 2
        FOR JSON PATH
        ;
    END;
    ---- ---------------------------------------------
END


            ");

        }
    }
}
