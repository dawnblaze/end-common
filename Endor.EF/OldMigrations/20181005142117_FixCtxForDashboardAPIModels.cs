using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181005142117_FixCtxForDashboardAPIModels")]
    public partial class FixCtxForDashboardAPIModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DashboardDataBID",
                table: "Dashboard.Widget.Data");

            migrationBuilder.DropColumn(
                name: "DashboardDataID",
                table: "Dashboard.Widget.Data");

            migrationBuilder.DropColumn(
                name: "DashboardWidgetDefinitionID",
                table: "Dashboard.Widget.Data");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "DashboardDataBID",
                table: "Dashboard.Widget.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "DashboardDataID",
                table: "Dashboard.Widget.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "DashboardWidgetDefinitionID",
                table: "Dashboard.Widget.Data",
                nullable: true);
        }
    }
}

