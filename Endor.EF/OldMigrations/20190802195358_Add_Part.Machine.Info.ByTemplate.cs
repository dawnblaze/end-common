using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_PartMachineInfoByTemplate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
/*
Name: Part.Machine.Info.ByTemplate( @BID smallint, @ClassTypeID int, @Count int = 1 )

Description: Returns a JSON array of data for the client to use
    when constructing search criteria for machines based on categories and machineides

Sample Use:   

    DECLARE @BID SMALLINT = 1;
    DECLARE @TemplateID SMALLINT = 1549
    DECLARE @Debug BIT = 0;
    DECLARE @IncludeInactive BIT = 0;
    DECLARE @CategoryList VARCHAR(8000) = '1003, 1014, 1019, 1034, 1022';
    DECLARE @MachineList VARCHAR(8000) = '1085'
    EXEC [Part.Machine.Info.ByTemplate] @BID=@BID, @TemplateID=@TemplateID
       , @CategoryList=@CategoryList, @MachineList=@MachineList
       , @IncludeInactive=@IncludeInactive, @Debug=@Debug

-- DROP PROCEDURE [dbo].[Part.Machine.Info.ByTemplate] 

*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Part.Machine.Info.ByTemplate] 
      @BID SMALLINT
    , @TemplateID SMALLINT

      -- Optional fields
    , @CategoryList VARCHAR(8000) = NULL
    , @MachineList VARCHAR(8000) = NULL
    , @IncludeInactive BIT = 0
    , @Debug BIT = 0
AS
BEGIN
    -- DECLARE @BID SMALLINT = 1;
    -- DECLARE @Debug BIT = 1;
    -- DECLARE @IncludeInactive BIT = 0;
    -- DECLARE @TemplateID SMALLINT = 1549
    -- DECLARE @CategoryList VARCHAR(8000) = '1003, 1014, 1019, 1034, 1022';
    -- DECLARE @MachineList VARCHAR(8000) = '1085'

    DROP TABLE IF EXISTS [#MachineList];
    DROP TABLE IF EXISTS [#CategoryList];
    DROP TABLE IF EXISTS [#ProfileList];
    DROP TABLE IF EXISTS [#InstanceList];

    -- Get list of Machines in specified categories
    SELECT DISTINCT ID, Name
    INTO #MachineList
    FROM [Part.Machine.Data] M
    WHERE M.BID=@BID 
    AND M.MachineTemplateID = @TemplateID
    AND (@IncludeInactive=1 OR M.IsActive=1)
    AND (@CategoryList IS NULL 
        OR M.ID IN (SELECT L.PartID FROM [Part.Machine.CategoryLink] L
                    WHERE L.BID=@BID AND L.CategoryID IN (SELECT VALUE FROM string_split(@CategoryList, ','))))
    ;

    -- Get list of Machine Categories for selected machines
    SELECT DISTINCT ID, Name
    INTO #CategoryList
    FROM [Part.Machine.Category] C
    JOIN [Part.Machine.CategoryLink] L ON L.BID=C.BID AND L.CategoryID = C.ID
    WHERE C.BID=@BID 
    AND L.PartID IN (SELECT ID from #MachineList)
    AND (@CategoryList IS NULL OR L.CategoryID IN (SELECT Value FROM string_split(@CategoryList, ',')))
    ;

    -- Add any machines manually added
    IF (@MachineList IS NOT NULL)
        INSERT INTO #MachineList (ID, Name)
        SELECT ID, Name
        FROM [Part.Machine.Data] M
        JOIN 
        ( 
            SELECT VALUE FROM string_split(@MachineList, ',') 
        ) PreDefined ON PreDefined.value = M.ID
        WHERE M.BID=@BID 
        AND M.MachineTemplateID = @TemplateID
        AND M.ID NOT IN ( SELECT M2.ID FROM #MachineList M2 )
    ;

    -- Get the Profiles
    SELECT DISTINCT P.ID, P.Name, P.IsDefault,  M.ID AS MachineID
    INTO [#ProfileList] 
    FROM [#MachineList] M
    JOIN [Part.Machine.Profile] P ON P.MachineID = M.ID
    WHERE P.BID=@BID 
    AND (@IncludeInactive=1 OR P.IsActive=1)
    ;

    -- Get the Instances
    SELECT DISTINCT I.ID, I.Name, M.ID AS MachineID
    INTO [#InstanceList]
    FROM [#MachineList] M
    JOIN [Part.Machine.Instance] I ON I.MachineID = M.ID
    WHERE I.BID=@BID 
    AND (@IncludeInactive=1 OR I.IsActive=1)
    ;

    -- Now Put it together as JSON and return the
    SELECT 
        (SELECT 
            ID, 
            Name,
            (
            CONCAT( '[',
            STUFF(( SELECT  ',' + ID
                FROM    ( SELECT DISTINCT CONVERT(VARCHAR(10),ML.ID) ID 
                          FROM [#MachineList] ML
                          JOIN [Part.Machine.CategoryLink] L ON L.BID = @BID AND L.PartID = ML.ID AND L.CategoryID = CL.ID
                        ) Temp
              FOR
                XML PATH('')
              ), 1, 1, '')
              , ']' )
            ) MachineIDs
            FROM [#CategoryList] CL
            FOR JSON AUTO) Categories,

        (SELECT 
            ID, 
            Name,
            (SELECT ID, Name
                FROM [#ProfileList] PL
                WHERE PL.MachineID = ML.ID
                FOR JSON AUTO) Profiles,
            (SELECT TOP(1) ID
                FROM [#ProfileList] PL
                WHERE PL.MachineID = ML.ID AND PL.IsDefault=1
                ) DefaultProfile,
            (SELECT ID, Name
                FROM [#InstanceList] IL
                WHERE IL.MachineID = ML.ID
                FOR JSON AUTO) Instances
            FROM [#MachineList] ML
            FOR JSON AUTO) Machines

    FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
    ;

    -- Debug information
    IF (@Debug=1)
    BEGIN 
        SELECT * FROM [#MachineList] ;
        SELECT * FROM [#CategoryList];
        SELECT * FROM [#ProfileList];
        SELECT * FROM [#InstanceList];
    END;

    -- Clean up
    DROP TABLE [#MachineList];
    DROP TABLE [#CategoryList];
    DROP TABLE [#ProfileList];
    DROP TABLE [#InstanceList];
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
