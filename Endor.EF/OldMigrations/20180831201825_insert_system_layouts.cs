using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180831201825_insert_system_layouts")]
    public partial class insert_system_layouts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
				DECLARE @alterConstraint VARCHAR(255)
							  SELECT TOP 1 @alterConstraint = 'ALTER TABLE [dbo].[CustomField.Layout.Definition]
				DROP CONSTRAINT ' + name
							  FROM Sys.default_constraints A
							  JOIN sysconstraints B on A.parent_object_id = B.id
							  WHERE id = OBJECT_ID('[CustomField.Layout.Definition]')
							  AND COL_NAME(id, colid)='ModifiedDT'
							  AND OBJECTPROPERTY(constid,'IsDefaultCnst')=1
				EXEC (@alterConstraint)

                ALTER TABLE dbo.[CustomField.Layout.Definition]
                ADD CONSTRAINT [DF_ModifiedDT] DEFAULT GETUTCDATE() for [ModifiedDT];

                DELETE FROM dbo.[CustomField.Layout.Definition] WHERE ID IN(11,12,21,22,31,32,51,52,91,92,101,102);
                INSERT [CustomField.Layout.Definition] ([BID], [ID], [IsActive], [IsSystem], [Name], [AppliesToClassTypeID], [IsAllTab], [IsSubTab], [SortIndex])
                VALUES (1, 11, 1, 1, N'Location Details', 1005, 0, 0, 1)
                    , (1, 12, 1, 1, N'All', 1005, 1, 1, 32767)
                    , (1, 21, 1, 1, N'Customer Details', 2000, 0, 0, 1)
                    , (1, 22, 1, 1, N'All', 2000, 1, 1, 32767)
                    , (1, 31, 1, 1, N'Contact Details', 3000, 0, 0, 1)
                    , (1, 32, 1, 1, N'All', 3000, 1, 1, 32767)
                    , (1, 51, 1, 1, N'Employee Details', 5000, 0, 0, 1)
                    , (1, 52, 1, 1, N'All', 5000, 1, 1, 32767)
                    , (1, 91, 1, 1, N'Opportunity Details', 9000, 0, 0, 1)
                    , (1, 92, 1, 1, N'All', 9000, 1, 1, 32767)
                    , (1, 101, 1, 1, N'Order Details', 10000, 0, 0, 1)
                    , (1, 102, 1, 1, N'All', 10000, 1, 1, 32767);            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE dbo.[CustomField.Layout.Definition]
                DROP CONSTRAINT IF EXISTS [DF_ModifiedDT];    

                DELETE FROM dbo.[CustomField.Layout.Definition] WHERE ID IN(11,12,21,22,31,32,51,52,91,92,101,102);        
            ");
        }
    }
}

