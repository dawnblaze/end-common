using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_Mutex_Lock : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- =====================================================
/* 
    This procedure can be used to create a database lock 
    on a particular object. Use ClassTypeID = -1 for a global lock.
    Each application calls the PROC and checks if it obtained the 
    lock or not.  If it does, it proceeds.  If it does not, it aborts 
    or waits (depending on the use case).
    
    For example:

        DECLARE @MutexID INT = 1239;

        DECLARE @Result BIT;
        DECLARE @LockExists BIT;

        EXEC @LockExists = dbo.[Util.LockExists.Mutex.Object] @ID=@MutexID; SELECT @LockExists as LockExists;
        EXEC @Result = dbo.[Util.Lock.Mutex.Object] @ID=@MutexID
        SELECT GetDate() as RunAt, @Result as 'Lock Success - First Attempt';
        EXEC @LockExists = dbo.[Util.LockExists.Mutex.Object] @ID=@MutexID; SELECT @LockExists as LockExists;

        -- Try a second attempt, this time waiting for 15 seconds
        IF (@Result = 0)
        BEGIN
            EXEC @Result = dbo.[Util.Lock.Mutex.Object] @ID=@MutexID, @WaitTimeInMS = 15000
            SELECT GetDate() as RunAt, @Result as 'Lock Success - 2nd Attempt';
            EXEC @LockExists = dbo.[Util.LockExists.Mutex.Object] @ID=@MutexID; SELECT @LockExists as LockExists;
        END;

        IF (@Result = 1)
        BEGIN
            -- Do some work that takes time (simulated at 10 seconds)
            WAITFOR DELAY '00:00:10'

            -- Unlock the Record
            EXEC @Result = dbo.[Util.UnLock.Mutex.Object] @ID=@MutexID
            SELECT GetDate() as RunAt, @Result as 'UnLock Success';
            EXEC @LockExists = dbo.[Util.LockExists.Mutex.Object] @ID=@MutexID; SELECT @LockExists as LockExists;
        END;
      
*/
CREATE OR ALTER PROCEDURE [dbo].[Util.Lock.Mutex.Object]
                  @ID INT
                , @ClassTypeID INT = 123456789  -- Set this to a CTID to lock a specific object
                , @WaitTimeInMS INT = 0
AS
BEGIN
    DECLARE @MutexName NVARCHAR(255) = CONCAT('Cyrious.Mutex.Object.', CONVERT(Date, GetDate()), '/', @ClassTypeID, '/', @ID);
    DECLARE @Result INT;

    EXEC @Result = sp_getapplock 
                @Resource = @MutexName  -- Name of Lock
                , @Lockmode = 'Exclusive'
                , @LockOwner = 'Session'
                , @LockTimeout = @WaitTimeInMS  -- Maximum Time to Wait for a Lock
                , @DbPrincipal = 'public'
    ;

    -- ---------------------------------------------------------------------
    -- The output of the sp_GetAppLock is the following
    -- ---------------------------------------------------------------------
    -- Value Result
    --  0    The lock was successfully granted synchronously.
    --  1    The lock was granted successfully after waiting for other incompatible locks to be released.
    -- -1    The lock request timed out
    -- -2    The lock request was canceled.
    -- -3    The lock request was chosen as a deadlock victim.
    -- -999  Indicates a parameter validation or other call error.
    -- ---------------------------------------------------------------------

    IF (@Result IN (0, 1)) 
        RETURN CONVERT(BIT, 1);

    RETURN CONVERT(BIT, 0);
END;

GO

CREATE OR ALTER PROCEDURE dbo.[Util.UnLock.Mutex.Object]
                @ID INT
                , @ClassTypeID INT = 123456789
AS
BEGIN
    DECLARE @MutexName NVARCHAR(255) = CONCAT('Cyrious.Mutex.Object.', CONVERT(Date, GetDate()), '/', @ClassTypeID, '/', @ID);
    DECLARE @Result INT;

    EXEC @Result = sp_releaseapplock 
                @Resource = @MutexName  -- Name of Lock
                , @LockOwner = 'Session'
                , @DbPrincipal = 'public'
    ;

    IF (@Result < 0)
        RETURN CONVERT(BIT, 0);

    RETURN CONVERT(BIT, 1);
END;

GO

CREATE OR ALTER PROCEDURE dbo.[Util.LockExists.Mutex.Object](@ID INT, @ClassTypeID INT = 123456789)
AS
BEGIN
    DECLARE @MutexName NVARCHAR(255) = CONCAT('Cyrious.Mutex.Object.', CONVERT(Date, GetDate()), '/', @ClassTypeID, '/', @ID);
    DECLARE @Result NVARCHAR(32);

    SELECT @Result = AppLock_Mode('public', @MutexName, 'Session');

    IF (@Result IS NULL) OR (@Result IN ('', 'NoLock'))
        RETURN CONVERT(BIT, 0);

    RETURN CONVERT(BIT, 1);
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE [dbo].[Util.Lock.Mutex.Object];
DROP PROCEDURE [dbo].[Util.UnLock.Mutex.Object];
DROP PROCEDURE [dbo].[Util.LockExists.Mutex.Object];
");
        }
    }
}
