using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END3935AddTaxabilityCodeSimpleTaxabilityCodeandTaxabilityCodeItemExemptionLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "TaxCodeID",
                table: "Part.Material.Data",
                type: "smallint sparse",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "TaxCodeID",
                table: "Part.Machine.Data",
                type: "smallint sparse",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "TaxCodeID",
                table: "Part.Labor.Data",
                type: "smallint sparse",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Accounting.Tax.Code",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "8005"),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    HasExcludedTaxItems = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    IsTaxExempt = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    TaxCode = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Tax.Code", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Code_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.Sql
            (
                @"
                IF EXISTS(select 1 from sys.views where name='Accounting.Tax.Code.SimpleList' and type='v')
                DROP VIEW [dbo].[Accounting.Tax.Code.SimpleList];
                GO
                CREATE VIEW[dbo].[Accounting.Tax.Code.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                        , [IsActive]
                        , CONVERT(BIT, 0) AS[HasImage]
                        , CONVERT(BIT, 0) AS[IsDefault]
                FROM[Accounting.Tax.Code]
                WHERE IsActive = 1"
            );

            migrationBuilder.CreateTable(
                name: "Accounting.Tax.Code.ItemExemptionLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TaxCodeID = table.Column<short>(nullable: false),
                    TaxItemID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Tax.Code.ItemExemptionLink", x => new { x.BID, x.TaxCodeID, x.TaxItemID });
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Code.ItemExemptionLink_Accounting.Tax.Code",
                        columns: x => new { x.BID, x.TaxCodeID },
                        principalTable: "Accounting.Tax.Code",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Tax.Code.ItemExemptionLink_Accounting.Tax.Item",
                        columns: x => new { x.BID, x.TaxItemID },
                        principalTable: "Accounting.Tax.Item",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_BID_TaxCodeID",
                table: "Part.Material.Data",
                columns: new[] { "BID", "TaxCodeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Data_BID_TaxCodeID",
                table: "Part.Machine.Data",
                columns: new[] { "BID", "TaxCodeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Labor.Data_BID_TaxCodeID",
                table: "Part.Labor.Data",
                columns: new[] { "BID", "TaxCodeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Code_Name",
                table: "Accounting.Tax.Code",
                columns: new[] { "BID", "Name", "IsActive", "IsSystem" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Tax.Code.ItemExemptionLink_TaxItem",
                table: "Accounting.Tax.Code.ItemExemptionLink",
                columns: new[] { "BID", "TaxItemID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Labor.Data_TaxCodeID",
                table: "Part.Labor.Data",
                columns: new[] { "BID", "TaxCodeID" },
                principalTable: "Accounting.Tax.Code",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Data_TaxCodeID",
                table: "Part.Machine.Data",
                columns: new[] { "BID", "TaxCodeID" },
                principalTable: "Accounting.Tax.Code",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_Account.Tax.Code",
                table: "Part.Material.Data",
                columns: new[] { "BID", "TaxCodeID" },
                principalTable: "Accounting.Tax.Code",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Labor.Data_TaxCodeID",
                table: "Part.Labor.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Data_TaxCodeID",
                table: "Part.Machine.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_Account.Tax.Code",
                table: "Part.Material.Data");

            migrationBuilder.DropTable(
                name: "Accounting.Tax.Code.ItemExemptionLink");

            migrationBuilder.DropTable(
                name: "Accounting.Tax.Code.SimpleList");

            migrationBuilder.DropTable(
                name: "Accounting.Tax.Code");

            migrationBuilder.DropIndex(
                name: "IX_Part.Material.Data_BID_TaxCodeID",
                table: "Part.Material.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Machine.Data_BID_TaxCodeID",
                table: "Part.Machine.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Labor.Data_BID_TaxCodeID",
                table: "Part.Labor.Data");

            migrationBuilder.DropColumn(
                name: "TaxCodeID",
                table: "Part.Material.Data");

            migrationBuilder.DropColumn(
                name: "TaxCodeID",
                table: "Part.Machine.Data");

            migrationBuilder.DropColumn(
                name: "TaxCodeID",
                table: "Part.Labor.Data");
        }
    }
}
