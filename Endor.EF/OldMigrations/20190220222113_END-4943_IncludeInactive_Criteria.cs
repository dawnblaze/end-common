using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4943_IncludeInactive_Criteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                           ([TargetClassTypeID]
                           ,[Name]
                           ,[Label]
                           ,[Field]
                           ,[IsHidden]
                           ,[DataType]
                           ,[InputType]
                           ,[AllowMultiple]
                           ,[ListValues]
                           ,[ListValuesEndpoint]
                           ,[IsLimitToList]
                           ,[SortIndex]
                           ,[ID])
                VALUES
                    (
		                12060--<TargetClassTypeID, int,>
                        ,'Include Inactive'--,<Name, varchar(255),>
                        ,'Include Inactive'--,<Label, varchar(255),>
                        ,'-IsActive'--,<Field, varchar(255),>
                        ,0--,<IsHidden, bit,>
                        ,3--,<DataType, smallint,>
                        ,13--,<InputType, tinyint,>
                        ,0--,<AllowMultiple, bit,>
                        ,NULL--,<ListValues, varchar(max),>
                        ,NULL--,<ListValuesEndpoint, varchar(255),>
                        ,0--,<IsLimitToList, bit,>
                        ,10--,<SortIndex, tinyint,>
                        ,(SELECT (MAX(ID)+1) FROM [dbo].[System.List.Filter.Criteria])--,<ID, smallint,>)
		            )
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [System.List.Filter.Criteria] WHERE [TargetClassTypeID] = 12060 AND Field = '-IsActive';
            ");
        }
    }
}
