using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddCustomFieldDataXML : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DataXML",
                table: "Order.Custom.Data",
                type: "xml",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DataXML",
                table: "CustomField.Other.Data",
                type: "xml",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DataXML",
                table: "Contact.Custom.Data",
                type: "xml",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DataXML",
                table: "Company.Custom.Data",
                type: "xml",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Opportunity.Custom.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false, defaultValueSql: "((9000))"),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9001))"),
                    DataJSON = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataXML = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "(getutcdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Opportunity.Custom.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Opportunity.Custom.Data_Opportunity.Data_BID_ID",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Opportunity.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.Sql(@"CREATE PRIMARY XML INDEX 
            [XML_IX_Order.Custom.Data]
            ON [Order.Custom.Data] ([DataXML])");

            migrationBuilder.Sql(@"CREATE PRIMARY XML INDEX 
            [XML_IX_Contact.Custom.Data]
            ON [Contact.Custom.Data] ([DataXML])");

            migrationBuilder.Sql(@"CREATE PRIMARY XML INDEX 
            [XML_IX_Company.Custom.Data]
            ON [Company.Custom.Data] ([DataXML])");

            migrationBuilder.Sql(@"CREATE PRIMARY XML INDEX 
            [XML_IX_Opportunity.Custom.Data]
            ON [Opportunity.Custom.Data] ([DataXML])");

            migrationBuilder.Sql(@"CREATE PRIMARY XML INDEX 
            [XML_IX_CustomField.Other.Data]
            ON [CustomField.Other.Data] ([DataXML])");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Opportunity.Custom.Data");

            migrationBuilder.DropIndex(
                name: "XML_IX_Order.Custom.Data",
                table: "Order.Custom.Data");

            migrationBuilder.DropIndex(
                name: "XML_IX_Contact.Custom.Data",
                table: "Contact.Custom.Data");

            migrationBuilder.DropIndex(
                name: "XML_IX_Company.Custom.Data",
                table: "Company.Custom.Data");

            migrationBuilder.DropColumn(
                name: "DataXML",
                table: "Order.Custom.Data");

            migrationBuilder.DropColumn(
                name: "DataXML",
                table: "CustomField.Other.Data");

            migrationBuilder.DropColumn(
                name: "DataXML",
                table: "Contact.Custom.Data");

            migrationBuilder.DropColumn(
                name: "DataXML",
                table: "Company.Custom.Data");
        }
    }
}
