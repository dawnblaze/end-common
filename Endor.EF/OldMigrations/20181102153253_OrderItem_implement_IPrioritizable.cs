using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class OrderItem_implement_IPrioritizable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsUrgent",
                table: "Order.Item.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ItemCount",
                table: "Order.Item.Data",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "Priority",
                table: "Order.Item.Data",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsUrgent",
                table: "Order.Item.Data");

            migrationBuilder.DropColumn(
                name: "ItemCount",
                table: "Order.Item.Data");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "Order.Item.Data");

        }
    }
}
