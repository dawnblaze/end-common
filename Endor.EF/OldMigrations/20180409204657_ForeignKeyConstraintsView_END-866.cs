using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180409204657_ForeignKeyConstraintsView_END-866")]
    public partial class ForeignKeyConstraintsView_END866 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP VIEW IF EXISTS [dbo].[ForeignKeyConstraints]
GO

CREATE VIEW [dbo].[ForeignKeyConstraints] as 
SELECT TOP 100 PERCENT
    fk.name AS FK_ConstraintName,
    o1.name AS FK_TableName,
    c1.name AS FK_ColumnName,
    ROW_NUMBER() OVER (PARTITION BY fk.name, o1.name order by fkc.constraint_column_id) as FK_ColumnNumber,
    o2.name AS PK_TableName,
    c2.name AS PK_ColumnName,
    pk.name AS PK_IndexName,
    fk.delete_referential_action_desc AS Delete_Action,
    fk.update_referential_action_desc AS Update_Action
FROM sys.objects o1
JOIN sys.foreign_keys fk ON o1.object_id = fk.parent_object_id
JOIN sys.foreign_key_columns fkc ON fk.object_id = fkc.constraint_object_id
JOIN sys.columns c1 ON fkc.parent_object_id = c1.object_id AND fkc.parent_column_id = c1.column_id
JOIN sys.columns c2 ON fkc.referenced_object_id = c2.object_id AND fkc.referenced_column_id = c2.column_id
JOIN sys.objects o2 ON fk.referenced_object_id = o2.object_id
JOIN sys.key_constraints pk ON fk.referenced_object_id = pk.parent_object_id AND fk.key_index_id = pk.unique_index_id
ORDER BY o1.name, o2.name, fkc.constraint_column_id

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP VIEW IF EXISTS [dbo].[ForeignKeyConstraints]");
        }
    }
}

