using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180327023114_LaborReworkMachineAdd")]
    public partial class LaborReworkMachineAdd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "[Labor.Data]",
                newName: "Part.Labor.Data");

            migrationBuilder.RenameColumn(
                name: "BillingIcrementInMin",
                table: "[Part.Labor.Data]",
                newName: "BillingIncrementInMin");

            migrationBuilder.AlterColumn<int>(
                name: "IncomeAccountID",
                table: "Part.Labor.Data",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<int>(
                name: "ExpenseAccountID",
                table: "Part.Labor.Data",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.CreateTable(
                name: "Part.Machine.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12030"),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    ExpenseAccountID = table.Column<int>(nullable: true),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IncomeAccountID = table.Column<int>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    SKU = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Machine.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Machine.Data_ExpenseAccountID	",
                        columns: x => new { x.BID, x.ExpenseAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Data_IncomeAccountID",
                        columns: x => new { x.BID, x.IncomeAccountID },
                        principalTable: "Accounting.GL.Account",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Labor.Data_BID_ExpenseAccountID",
                table: "Part.Labor.Data",
                columns: new[] { "BID", "ExpenseAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Labor.Data_BID_IncomeAccountID",
                table: "Part.Labor.Data",
                columns: new[] { "BID", "IncomeAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Data_BID_ExpenseAccountID",
                table: "Part.Machine.Data",
                columns: new[] { "BID", "ExpenseAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Data_BID_IncomeAccountID",
                table: "Part.Machine.Data",
                columns: new[] { "BID", "IncomeAccountID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Data_Name",
                table: "Part.Machine.Data",
                columns: new[] { "BID", "Name" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Labor.Data_ExpenseAccountID	",
                table: "Part.Labor.Data",
                columns: new[] { "BID", "ExpenseAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Labor.Data_IncomeAccountID",
                table: "Part.Labor.Data",
                columns: new[] { "BID", "IncomeAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Labor.Data_ExpenseAccountID	",
                table: "Part.Labor.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Labor.Data_IncomeAccountID",
                table: "Part.Labor.Data");

            migrationBuilder.DropTable(
                name: "Part.Machine.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Labor.Data_BID_ExpenseAccountID",
                table: "Part.Labor.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Labor.Data_BID_IncomeAccountID",
                table: "Part.Labor.Data");

            migrationBuilder.RenameTable(
                name: "[Part.Labor.Data]",
                newName: "Labor.Data");

            migrationBuilder.RenameColumn(
                name: "BillingIncrementInMin",
                table: "[Labor.Data]",
                newName: "BillingIcrementInMin");

            migrationBuilder.AlterColumn<short>(
                name: "IncomeAccountID",
                table: "Labor.Data",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<short>(
                name: "ExpenseAccountID",
                table: "Labor.Data",
                nullable: false,
                oldClrType: typeof(int));
        }
    }
}

