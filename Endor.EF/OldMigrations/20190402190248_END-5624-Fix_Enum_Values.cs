using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END5624Fix_Enum_Values : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE [enum.ElementType] WHERE [ID]=104;");
            migrationBuilder.Sql(@"DELETE [enum.ElementType] WHERE [ID]=105;");

            migrationBuilder.Sql(@"
                Insert [enum.Part.Subassembly.ElementType] ([ID],[Name])
                Values
                        ( 104, N'LinkedMaterial')
                        , (105, N'LinkedLabor')
                ;
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE [enum.Part.Subassembly.ElementType] WHERE [ID]=104;");
            migrationBuilder.Sql(@"DELETE [enum.Part.Subassembly.ElementType] WHERE [ID]=105;");

            migrationBuilder.Sql(@"
                Insert [enum.ElementType] ([ID],[Name])
                Values
                        ( 104, N'LinkedMaterial')
                        , (105, N'LinkedLabor')
                ;
                ");
        }
    }
}
