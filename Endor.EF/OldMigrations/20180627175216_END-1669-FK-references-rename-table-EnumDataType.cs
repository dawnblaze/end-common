using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using Endor.Models;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180627175216_END-1669-FK-references-rename-table-EnumDataType")]
    public partial class END1669FKreferencesrenametableEnumDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP INDEX IF EXISTS [IX_System.Option.Definition_DataType] ON [dbo].[System.Option.Definition]
");
            migrationBuilder.Sql(@"
DROP INDEX IF EXISTS [IX_enum.CustomField.InputType_DataType] ON [dbo].[enum.CustomField.InputType]
");
            migrationBuilder.Sql(@"
DROP INDEX IF EXISTS [IX_CustomField.Definition_DataType] ON [dbo].[CustomField.Definition]
");
            migrationBuilder.Sql(@"
DROP INDEX IF EXISTS [IX_enum.CustomField.DisplayType_DataType] ON [dbo].[enum.CustomField.DisplayType]
");

            //manually dropped objects
            migrationBuilder.Sql("ALTER TABLE [dbo].[System.Option.Definition] DROP CONSTRAINT [DF_System.Option.Definition_DataType]");



            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_DataType",
                table: "CustomField.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_enum.CustomField.DisplayType_enum.CustomField.DataType",
                table: "enum.CustomField.DisplayType");

            migrationBuilder.DropForeignKey(
                name: "FK_enum.CustomField.InputType_enum.CustomField.DataType",
                table: "enum.CustomField.InputType");

            migrationBuilder.DropForeignKey(
                name: "FK_System.Option.Definition_enum.CustomField.DataType",
                table: "System.Option.Definition");

            migrationBuilder.DropTable(
                name: "enum.CustomField.DataType");

            migrationBuilder.AlterColumn<short>(
                name: "DataType",
                table: "System.Option.Definition",
                type: "smallint",
                nullable: false,
                defaultValueSql: "((0))",
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldDefaultValueSql: "((0))");

            migrationBuilder.AlterColumn<short>(
                name: "DataType",
                table: "enum.CustomField.InputType",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint");

            migrationBuilder.AlterColumn<short>(
                name: "DataType",
                table: "enum.CustomField.DisplayType",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint");

            migrationBuilder.AlterColumn<short>(
                name: "DataType",
                table: "CustomField.Definition",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(byte),
                oldType: "tinyint");

            migrationBuilder.CreateTable(
                name: "enum.DataType",
                columns: table => new
                {
                    ID = table.Column<short>(type: "smallint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.DataType", x => x.ID);
                });

            //insert needed rows
            foreach(string name in Enum.GetNames(typeof(DataType)))
            {
                short id = (short)Enum.Parse(typeof(DataType), name);
                migrationBuilder.Sql($"INSERT INTO dbo.[enum.DataType] (ID, Name) values({id} ,'{name}')");
            }

            //updated conflicting FK
            migrationBuilder.Sql("UPDATE dbo.[enum.CustomField.DisplayType] SET DataType=9 where DataType=7 OR DataType=8");


            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_enum.DataType_DataType",
                table: "CustomField.Definition",
                column: "DataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_enum.CustomField.DisplayType_enum.DataType",
                table: "enum.CustomField.DisplayType",
                column: "DataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_enum.CustomField.InputType_enum.DataType",
                table: "enum.CustomField.InputType",
                column: "DataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_System.Option.Definition_enum.DataType",
                table: "System.Option.Definition",
                column: "DataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);



            //prepare migration to override errors
            //these objects dont exist but EF is trying to drop them
            migrationBuilder.CreateIndex(
                name: "IX_System.Option.Definition_DataType",
                table: "System.Option.Definition",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_enum.CustomField.InputType_DataType",
                table: "enum.CustomField.InputType",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Definition_DataType",
                table: "CustomField.Definition",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_enum.CustomField.DisplayType_DataType",
                table: "enum.CustomField.DisplayType",
                column: "DataType");

            //recreate manually dropped objects
            //migrationBuilder.Sql("ALTER TABLE [dbo].[System.Option.Definition] ADD  CONSTRAINT [DF_System.Option.Definition_DataType]  DEFAULT ((0)) FOR [DataType]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_enum.DataType_DataType",
                table: "CustomField.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_enum.CustomField.DisplayType_enum.DataType",
                table: "enum.CustomField.DisplayType");

            migrationBuilder.DropForeignKey(
                name: "FK_enum.CustomField.InputType_enum.DataType",
                table: "enum.CustomField.InputType");

            migrationBuilder.DropForeignKey(
                name: "FK_System.Option.Definition_enum.DataType",
                table: "System.Option.Definition");

            migrationBuilder.DropTable(
                name: "enum.DataType");

            migrationBuilder.AlterColumn<byte>(
                name: "DataType",
                table: "System.Option.Definition",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "((0))",
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "((0))");

            migrationBuilder.AlterColumn<byte>(
                name: "DataType",
                table: "enum.CustomField.InputType",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AlterColumn<byte>(
                name: "DataType",
                table: "enum.CustomField.DisplayType",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AlterColumn<byte>(
                name: "DataType",
                table: "CustomField.Definition",
                type: "tinyint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.CreateTable(
                name: "enum.CustomField.DataType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.DataType", x => x.ID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_enum.CustomField.DataType_DataType",
                table: "CustomField.Definition",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_enum.CustomField.DisplayType_enum.CustomField.DataType",
                table: "enum.CustomField.DisplayType",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_enum.CustomField.InputType_enum.CustomField.DataType",
                table: "enum.CustomField.InputType",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_System.Option.Definition_enum.CustomField.DataType",
                table: "System.Option.Definition",
                column: "DataType",
                principalTable: "enum.CustomField.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

