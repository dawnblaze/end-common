using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END_5127_CorrectOrderItemHistoric : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
    DECLARE @TemporalType TINYINT = (SELECT temporal_type FROM sys.tables WHERE  object_id = OBJECT_ID('[Order.Item.Data]', 'u'))
    IF @TemporalType = 2
        ALTER TABLE [Order.Item.Data] SET (SYSTEM_VERSIONING = OFF); 

    -- Drop the Altered Columns
    ALTER TABLE [Historic.Order.Item.Data] 
    DROP COLUMN [Price.Total]
              , [Price.Component]
              , [Price.Surcharge]
              , [Price.List]
              , [Price.PreTax]
              , [Price.UnitPreTax]
    ;

    -- Now add the Columns with their correct formula
    ALTER TABLE [Historic.Order.Item.Data]
    ADD [Price.Component] DECIMAL(18,4) NULL
      , [Price.Surcharge] DECIMAL(18,4) NULL
      , [Price.List] DECIMAL(19,4) NULL
      , [Price.PreTax] DECIMAL(21,4) NULL
      , [Price.UnitPreTax] DECIMAL(38,15) NULL
      , [Price.Total] DECIMAL(22,4) NULL
    ;    

    -- Turn of System Versioniong back on!
    ALTER TABLE [Order.Item.Data] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Item.Data])); 
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
