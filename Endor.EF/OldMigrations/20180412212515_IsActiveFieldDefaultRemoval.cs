using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180412212515_IsActiveFieldDefaultRemoval")]
    public partial class IsActiveFieldDefaultRemoval : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {            
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Report.Menu",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Material.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Material.Category",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");
            
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Machine.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Machine.Category",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Labor.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Labor.Category",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Option.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Opportunity.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Location.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "List.Filter",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Employee.Team",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Employee.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "CRM.Origin",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");
                        
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "CRM.Industry",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "CRM.CustomField.Helper",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "CRM.CustomField.Def",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");
            
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Contact.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");
            
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Company.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Campaign.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Business.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Activity.Event",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Activity.Action",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Tax.Item",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");
                        
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Tax.Group",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Payment.Term",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Payment.Method",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.GL.Account",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "1");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "_Root.Data",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "((1))");            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {        
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Report.Menu",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Material.Data",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Material.Category",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Machine.Data",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Machine.Category",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Labor.Data",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Part.Labor.Category",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Option.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Opportunity.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Location.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "List.Filter",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));
                        
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Employee.Team",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Employee.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "CRM.Origin",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "CRM.Industry",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "CRM.CustomField.Helper",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "CRM.CustomField.Def",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Contact.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Company.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Campaign.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Business.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Activity.Event",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Activity.Action",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Tax.Item",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Tax.Group",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Payment.Term",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.Payment.Method",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "Accounting.GL.Account",
                nullable: false,
                defaultValueSql: "1",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "_Root.Data",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(bool));            
        }
    }
}

