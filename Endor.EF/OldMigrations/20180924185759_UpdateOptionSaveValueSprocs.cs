using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180924185759_UpdateOptionSaveValueSprocs")]
    public partial class UpdateOptionSaveValueSprocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Option.SaveValues];
GO

-- ===============================================
/*
    Name: [Option.SaveValues]

    Description: 
        Create a procedure that saves multiple options values
        at once.  The values are all given the same access
        level when saved.
    Updated: 2018-07-05

    Sample Use:   

    DECLARE @T OptionsArray;
    INSERT INTO @T
      VALUES  (NULL, 'GLAccount.TaxName1', 'State')
            , (NULL, 'GLAccount.TaxName2', 'City')
            , (NULL, 'Employee.Collection.SortOrder', '4;5;1;5')
            , (NULL, 'Employee.OldSort', NULL)
            ;

    EXEC dbo.[Option.SaveValues] 
        -- Require Fields
          @Options_Array    = @T

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @UserID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

        , @Debug            = 1

*/
-- ===============================================
CREATE PROCEDURE [dbo].[Option.SaveValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT       = NULL
    , @BID            SMALLINT      = NULL
    , @LocationID     SMALLINT      = NULL
    , @UserID     SMALLINT      = NULL
    , @CompanyID      INT           = NULL
    , @ContactID      INT           = NULL
    , @StorefrontID   SMALLINT      = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @UserID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
              RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing  IDs from the Definitions Table
    -- -------------------------------------------------
    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

    -- If not defined, and NULL, there will be nothing to delete
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
        AND (Value IS NULL)
    ;

    -- Any missing IDs must be AdHoc so create them
    -- -------------------------------------------------
    DECLARE @NewIDs INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);

    IF (@NewIDs > 0)
    BEGIN
        DECLARE @NewID INT
        DECLARE @NewIDCount INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, @NewIDCount;


        -- Any missing IDs must be AdHoc so create them
        -- -------------------------------------------------
        UPDATE @Options
        SET   IsNewAdHoc = 1
            , OptionID = @NewID, @NewID = @NewID + 1
        WHERE OptionID IS NULL
        ;

        INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
        SELECT OptionID AS ID
                , OptionName AS Name
                , 'Custom: '+OptionName AS Label
                , NULL AS DefaultValue
                , 0 AS DataType -- Always string
                , 10100 AS CategoryID  -- AdHoc Options
                , 1 AS IsHidden
        FROM @Options
        WHERE IsNewAdHoc = 1
        ;
    END;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
    UPDATE Opt
    SET InstanceID = 
           CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.LocationID = @LocationID)

                WHEN @UserID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.UserID = @UserID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND UserID IS NULL AND CompanyID IS NULL and ContactID IS NULL and StorefrontID IS NULL)

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.AssociationID = @AssociationID)

           ELSE NULL
           END
    FROM @Options Opt
    ;

    IF (@Debug=1)
        SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NULL
    ;

    -- ======================================
    -- Update any Existing Values
    -- ======================================
    UPDATE D
    SET ModifiedDT = GetUTCDate()
      , Value = Opt.Value
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NOT NULL
    ;

    -- ======================================
    -- Create New Options if not Found Values
    -- ======================================
    INSERT INTO [Option.Data] 
        ( CreatedDate, ModifiedDT, IsActive, OptionID
        , [Value], AssociationID, BID, LocationID
        , StoreFrontID, UserID, CompanyID, ContactID
        )
        SELECT
            GetUTCDate(), GetUTCDate(), 1, OptionID
            , Value, @AssociationID, @BID, @LocationID
            , @StoreFrontID, @UserID, @CompanyID, @ContactID
        FROM @Options
        WHERE InstanceID IS NULL
          AND Value IS NOT NULL
    ;
END
");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Option.SaveValue];
GO

-- ========================================================
-- 
-- Name: [Option.SaveValue]
--
-- Description: This procedure saves the setting for an option/setting.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.SaveValue] 
        -- Require Fields
          @OptionName       = 'GLAccount.TaxName2'
        , @OptionID         = NULL
        , @Value            = 'NewValue'  -- Pass in NULL to delete the current value (if any)

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @UserID       	= 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

    SELECT * FROM [Option.Data]
*/
-- ========================================================
CREATE PROCEDURE [dbo].[Option.SaveValue]
-- DECLARE 
            @OptionID       INT          = NULL
          , @OptionName     VARCHAR(255) = NULL
          , @Value          VARCHAR(MAX) = NULL     -- Pass in NULL to delete the current value (if any)

          , @AssociationID  TINYINT      = NULL
          , @BID            SMALLINT     = NULL
          , @LocationID     SMALLINT     = NULL
          , @UserID     	SMALLINT     = NULL
          , @CompanyID      INT          = NULL
          , @ContactID      INT          = NULL
          , @StorefrontID   SMALLINT	 = NULL

AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@OptionName IS NULL) AND (@OptionID IS NULL)) OR ((@OptionName IS NOT NULL) AND (@OptionID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @OptionID or the @OptionName.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @UserID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================
    IF (@OptionID IS NULL)
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName;

        IF (@OptionID IS NULL)
        BEGIN
            -- If not defined, and NULL, there will be nothing to delete
            -- -------------------------------------------------
            IF (@Value IS NULL)
                RETURN;

            PRINT 'create new - check parameters, save and return id'

			DECLARE @NewID INT
			EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, 1;
			SET @OptionID = @NewID

            INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
            SELECT @OptionID AS ID
                 , @OptionName AS Name
                 , 'Custom: '+@OptionName AS Label
                 , NULL AS DefaultValue
                 , 0 AS DataType -- Always string
                 , 10100 AS CategoryID  -- AdHoc Options
                 , 1 AS IsHidden
            ;

            PRINT @OptionID
        END
    END

    -- ======================================
    -- Lookup Current Value if it Exists
    -- ======================================
    DECLARE @InstanceID INT = 

        CASE 
            WHEN @AssociationID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND AssociationID = @AssociationID )

            WHEN @LocationID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND LocationID = @LocationID)

            WHEN @UserID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND UserID = @UserID)

            WHEN @CompanyID  IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND CompanyID  = @CompanyID )

            WHEN @ContactID  IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND ContactID  = @ContactID )

            WHEN @StorefrontID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND StorefrontID = @StorefrontID)

            WHEN @BID        IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND COALESCE(LocationID, UserID, CompanyID, ContactID, StorefrontID) IS NULL )

        ELSE NULL
        END;

    -- ======================================
    -- Now Save (Update or Create) Value
    -- ======================================
    IF (@Value IS NOT NULL)
    BEGIN
        IF (@InstanceID IS NOT NULL)
            UPDATE [Option.Data]
            SET ModifiedDT = GetUTCDate()
            , Value = @Value
            WHERE ID = @InstanceID

        ELSE
            INSERT INTO [Option.Data] 
                ( CreatedDate, ModifiedDT, IsActive, OptionID
                , [Value], AssociationID, BID, LocationID
                , StoreFrontID, UserID, CompanyID, ContactID
                )
            VALUES
                ( GetUTCDate(), GetUTCDate(), 1, @OptionID
                , @Value, @AssociationID, @BID, @LocationID
                , @StoreFrontID, @UserID, @CompanyID, @ContactID
                );
    END

    -- ======================================
    -- Else Delete the Options with NULL Values Passed in
    -- ======================================
    ELSE
        DELETE FROM [Option.Data]
        WHERE ID = @InstanceID

END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Option.SaveValues];
GO

-- ===============================================
/*
    Name: [Option.SaveValues]

    Description: 
        Create a procedure that saves multiple options values
        at once.  The values are all given the same access
        level when saved.
    Updated: 2018-07-05

    Sample Use:   

    DECLARE @T OptionsArray;
    INSERT INTO @T
      VALUES  (NULL, 'GLAccount.TaxName1', 'State')
            , (NULL, 'GLAccount.TaxName2', 'City')
            , (NULL, 'Employee.Collection.SortOrder', '4;5;1;5')
            , (NULL, 'Employee.OldSort', NULL)
            ;

    EXEC dbo.[Option.SaveValues] 
        -- Require Fields
          @Options_Array    = @T

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @UserID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

        , @Debug            = 1

*/
-- ===============================================
CREATE PROCEDURE [dbo].[Option.SaveValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT       = NULL
    , @BID            SMALLINT      = NULL
    , @LocationID     SMALLINT      = NULL
    , @UserID     SMALLINT      = NULL
    , @CompanyID      INT           = NULL
    , @ContactID      INT           = NULL
    , @StorefrontID   SMALLINT      = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @UserID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
              RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing  IDs from the Definitions Table
    -- -------------------------------------------------
    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

    -- If not defined, and NULL, there will be nothing to delete
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
        AND (Value IS NULL)
    ;

    -- Any missing IDs must be AdHoc so create them
    -- -------------------------------------------------
    DECLARE @NewIDs INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);

    IF (@NewIDs > 0)
    BEGIN
        DECLARE @NewID INT
        DECLARE @NewIDCount INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, @NewIDCount;


        -- Any missing IDs must be AdHoc so create them
        -- -------------------------------------------------
        UPDATE @Options
        SET   IsNewAdHoc = 1
            , OptionID = @NewID, @NewID = @NewID + 1
        WHERE OptionID IS NULL
        ;

        INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
        SELECT OptionID AS ID
                , OptionName AS Name
                , 'Custom: '+OptionName AS Label
                , NULL AS DefaultValue
                , 0 AS DataType -- Always string
                , -1 AS CategoryID  -- AdHoc Options
                , 1 AS IsHidden
        FROM @Options
        WHERE IsNewAdHoc = 1
        ;
    END;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
    UPDATE Opt
    SET InstanceID = 
           CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.LocationID = @LocationID)

                WHEN @UserID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.UserID = @UserID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND UserID IS NULL AND CompanyID IS NULL and ContactID IS NULL and StorefrontID IS NULL)

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.AssociationID = @AssociationID)

           ELSE NULL
           END
    FROM @Options Opt
    ;

    IF (@Debug=1)
        SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NULL
    ;

    -- ======================================
    -- Update any Existing Values
    -- ======================================
    UPDATE D
    SET ModifiedDT = GetUTCDate()
      , Value = Opt.Value
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NOT NULL
    ;

    -- ======================================
    -- Create New Options if not Found Values
    -- ======================================
    INSERT INTO [Option.Data] 
        ( CreatedDate, ModifiedDT, IsActive, OptionID
        , [Value], AssociationID, BID, LocationID
        , StoreFrontID, UserID, CompanyID, ContactID
        )
        SELECT
            GetUTCDate(), GetUTCDate(), 1, OptionID
            , Value, @AssociationID, @BID, @LocationID
            , @StoreFrontID, @UserID, @CompanyID, @ContactID
        FROM @Options
        WHERE InstanceID IS NULL
          AND Value IS NOT NULL
    ;
END
");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Option.SaveValue];
GO

-- ========================================================
-- 
-- Name: [Option.SaveValue]
--
-- Description: This procedure saves the setting for an option/setting.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.SaveValue] 
        -- Require Fields
          @OptionName       = 'GLAccount.TaxName2'
        , @OptionID         = NULL
        , @Value            = 'NewValue'  -- Pass in NULL to delete the current value (if any)

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @UserID       	= 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

    SELECT * FROM [Option.Data]
*/
-- ========================================================
CREATE PROCEDURE [dbo].[Option.SaveValue]
-- DECLARE 
            @OptionID       INT          = NULL
          , @OptionName     VARCHAR(255) = NULL
          , @Value          VARCHAR(MAX) = NULL     -- Pass in NULL to delete the current value (if any)

          , @AssociationID  TINYINT      = NULL
          , @BID            SMALLINT     = NULL
          , @LocationID     SMALLINT     = NULL
          , @UserID     	SMALLINT     = NULL
          , @CompanyID      INT          = NULL
          , @ContactID      INT          = NULL
          , @StorefrontID   SMALLINT	 = NULL

AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@OptionName IS NULL) AND (@OptionID IS NULL)) OR ((@OptionName IS NOT NULL) AND (@OptionID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @OptionID or the @OptionName.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @UserID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================
    IF (@OptionID IS NULL)
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName;

        IF (@OptionID IS NULL)
        BEGIN
            -- If not defined, and NULL, there will be nothing to delete
            -- -------------------------------------------------
            IF (@Value IS NULL)
                RETURN;

            PRINT 'create new - check parameters, save and return id'

			DECLARE @NewID INT
			EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, 1;
			SET @OptionID = @NewID

            INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
            SELECT @OptionID AS ID
                 , @OptionName AS Name
                 , 'Custom: '+@OptionName AS Label
                 , NULL AS DefaultValue
                 , 0 AS DataType -- Always string
                 , -1 AS CategoryID  -- AdHoc Options
                 , 1 AS IsHidden
            ;

            PRINT @OptionID
        END
    END

    -- ======================================
    -- Lookup Current Value if it Exists
    -- ======================================
    DECLARE @InstanceID INT = 

        CASE 
            WHEN @AssociationID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND AssociationID = @AssociationID )

            WHEN @LocationID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND LocationID = @LocationID)

            WHEN @UserID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND UserID = @UserID)

            WHEN @CompanyID  IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND CompanyID  = @CompanyID )

            WHEN @ContactID  IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND ContactID  = @ContactID )

            WHEN @StorefrontID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND StorefrontID = @StorefrontID)

            WHEN @BID        IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND COALESCE(LocationID, UserID, CompanyID, ContactID, StorefrontID) IS NULL )

        ELSE NULL
        END;

    -- ======================================
    -- Now Save (Update or Create) Value
    -- ======================================
    IF (@Value IS NOT NULL)
    BEGIN
        IF (@InstanceID IS NOT NULL)
            UPDATE [Option.Data]
            SET ModifiedDT = GetUTCDate()
            , Value = @Value
            WHERE ID = @InstanceID

        ELSE
            INSERT INTO [Option.Data] 
                ( CreatedDate, ModifiedDT, IsActive, OptionID
                , [Value], AssociationID, BID, LocationID
                , StoreFrontID, UserID, CompanyID, ContactID
                )
            VALUES
                ( GetUTCDate(), GetUTCDate(), 1, @OptionID
                , @Value, @AssociationID, @BID, @LocationID
                , @StoreFrontID, @UserID, @CompanyID, @ContactID
                );
    END

    -- ======================================
    -- Else Delete the Options with NULL Values Passed in
    -- ======================================
    ELSE
        DELETE FROM [Option.Data]
        WHERE ID = @InstanceID

END
");
        }
    }
}

