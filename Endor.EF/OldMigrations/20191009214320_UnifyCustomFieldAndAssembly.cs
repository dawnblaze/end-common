﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class UnifyCustomFieldAndAssembly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            #region add columns to CustomField.Definition

            migrationBuilder.AddColumn<bool>(
                name: "AllowCustomValue",
                table: "CustomField.Definition",
                type: "bit",
                nullable: true,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AllowDecimals",
                table: "CustomField.Definition",
                type: "bit",
                nullable: true,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AllowMultiSelect",
                table: "CustomField.Definition",
                type: "bit",
                nullable: true,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "AllowNotSetOption",
                table: "CustomField.Definition",
                type: "bit sparse",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AllowTextFormatting",
                table: "CustomField.Definition",
                type: "bit sparse",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "AllowUserTypedOptions",
                table: "CustomField.Definition",
                type: "bit sparse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AltText",
                table: "CustomField.Definition",
                type: "nvarchar(255) sparse",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "DecimalPlaces",
                table: "CustomField.Definition",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DefaultOption",
                table: "CustomField.Definition",
                type: "nvarchar(1024) sparse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DefaultValue",
                table: "CustomField.Definition",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DisplayFormat",
                table: "CustomField.Definition",
                type: "nvarchar(255) sparse",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "DisplaySpinner",
                table: "CustomField.Definition",
                type: "bit sparse",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "DisplayType",
                table: "CustomField.Definition",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "ElementType",
                table: "CustomField.Definition",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<byte>(
                name: "ElementUseCount",
                table: "CustomField.Definition",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<bool>(
                name: "GroupOptionsByCategory",
                table: "CustomField.Definition",
                type: "bit sparse",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "GroupType",
                table: "CustomField.Definition",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "HasMaxLength",
                table: "CustomField.Definition",
                type: "smallint sparse",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDisabled",
                table: "CustomField.Definition",
                type: "bit",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<byte>(
                name: "LabelType",
                table: "CustomField.Definition",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ListDataType",
                table: "CustomField.Definition",
                type: "smallint sparse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ListOptions",
                table: "CustomField.Definition",
                type: "nvarchar(2048) sparse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ListValuesJSON",
                table: "CustomField.Definition",
                type: "nvarchar(max) sparse",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "NumberOfDecimals",
                table: "CustomField.Definition",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OptionOneLabel",
                table: "CustomField.Definition",
                type: "nvarchar(255) sparse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OptionTwoLabel",
                table: "CustomField.Definition",
                type: "nvarchar(255) sparse",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "PickerType",
                table: "CustomField.Definition",
                type: "tinyint sparse",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PresetTime",
                table: "CustomField.Definition",
                type: "bit sparse",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "SpinnerIncrement",
                table: "CustomField.Definition",
                type: "decimal(6, 3) sparse",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Time",
                table: "CustomField.Definition",
                type: "DATETIME2(2) sparse",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "ToggleOptions",
                table: "CustomField.Definition",
                type: "bit sparse",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Tooltip",
                table: "CustomField.Definition",
                type: "nvarchar(max)",
                nullable: true);
            #endregion

            #region migrate variable fields
            migrationBuilder.Sql(@"

    -- Now Update the Fields from the XML
    UPDATE [CustomField.Definition] 
    SET
        [AllowCustomValue] = MetaData.value('(/Root/AllowCustomValue)[1]','bit'),
        [AllowDecimals] = MetaData.value('(/Root/AllowDecimals)[1]','bit'),
        [AllowMultiSelect] = MetaData.value('(/Root/AllowMultiSelect)[1]','bit'),
        [AllowNotSetOption] = MetaData.value('(/Root/AllowNotSetOption)[1]','bit'),
        [AllowTextFormatting] = MetaData.value('(/Root/AllowTextFormatting)[1]','bit'),
        [AllowUserTypedOptions] = MetaData.value('(/Root/AllowUserTypedOptions)[1]','bit'),
        [AltText] = MetaData.value('(/Root/AltText)[1]','nvarchar(255)'),
        [DecimalPlaces] = MetaData.value('(/Root/DecimalPlaces)[1]','tinyint'),
        [DefaultOption] = MetaData.value('(/Root/DefaultOption)[1]','nvarchar(1024)'),
        [DefaultValue] = MetaData.value('(/Root/DefaultValue)[1]','nvarchar(max)'),
        [DisplayFormat] = MetaData.value('(/Root/DisplayFormat)[1]','nvarchar(255)'),
        [DisplayType] = [DisplayFormatType],
        [DisplaySpinner] = MetaData.value('(/Root/DisplaySpinner)[1]','bit'),
        [ElementType] = [InputType],
        [ElementUseCount] = IsNull(MetaData.value('(/Root/ElementUseCount)[1]','tinyint'), 0),
        [GroupOptionsByCategory] = MetaData.value('(/Root/GroupOptionsByCategory)[1]','bit'),
        [GroupType] = MetaData.value('(/Root/GroupType)[1]','tinyint'),
        [HasMaxLength] = MetaData.value('(/Root/HasMaxLength)[1]','smallint'),
        [IsDisabled] = IsNull(MetaData.value('(/Root/IsDisabled)[1]','bit'), 0),
        [LabelType] = MetaData.value('(/Root/LabelType)[1]','tinyint'),
        [ListDataType] = MetaData.value('(/Root/ListDataType)[1]','smallint'),
        [ListOptions] = MetaData.value('(/Root/ListOptions)[1]','nvarchar(2048)'),
        [ListValuesJSON] = IIF(LEN(LTRIM(RTRIM(MetaData.value('(/Root/ListValuesJSON)[1]','nvarchar(max)'))))=0, NULL, MetaData.value('(/Root/ListValuesJSON)[1]','nvarchar(max)')),
        [NumberOfDecimals] = MetaData.value('(/Root/NumberOfDecimals)[1]','tinyint'),
        [OptionOneLabel] = MetaData.value('(/Root/OptionOneLabel)[1]','nvarchar(255)'),
        [OptionTwoLabel] = MetaData.value('(/Root/OptionTwoLabel)[1]','nvarchar(255)'),
        [PickerType] = MetaData.value('(/Root/PickerType)[1]','tinyint'),
        [PresetTime] = MetaData.value('(/Root/PresetTime)[1]','bit'),
        [SpinnerIncrement] = CONVERT(decimal(6,3), MetaData.value('(/Root/SpinnerIncrement)[1]','int')),
        [Time] = MetaData.value('(/Root/Time)[1]',' DateTime2(2)'),
        [ToggleOptions] = MetaData.value('(/Root/ToggleOptions)[1]','bit'),
        [Tooltip] = MetaData.value('(/Root/Tooltip)[1]','nvarchar(max)'),
        [UnitID] = MetaData.value('(/Root/UnitID)[1]','tinyint'),
        [UnitType] = MetaData.value('(/Root/UnitType)[1]','tinyint')
    ;


    -- Insert Missing Element Types
    INSERT INTO [enum.Part.Subassembly.ElementType] (ID, Name)
        SELECT ID, Name
        FROM [enum.CustomField.InputType]
        WHERE ID NOT IN (SELECT ID FROM [enum.Part.Subassembly.ElementType])
        ;
");
            #endregion

            #region add new Element fields

            migrationBuilder.AddColumn<byte>(
                name: "Column",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "1");

            migrationBuilder.AddColumn<byte>(
                name: "ColumnsWide",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "1");

            migrationBuilder.AddColumn<byte>(
                name: "ElementType",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "255");

            migrationBuilder.AddColumn<bool>(
                name: "IsDisabled",
                table: "CustomField.Layout.Element",
                type: "bit",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<bool>(
                name: "IsReadOnly",
                table: "CustomField.Layout.Element",
                type: "bit",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<short>(
                name: "LayoutID",
                table: "CustomField.Layout.Element",
                type: "smallint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<byte>(
                name: "Row",
                table: "CustomField.Layout.Element",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<string>(
                name: "Tooltip",
                table: "CustomField.Layout.Element",
                type: "nvarchar(512)",
                nullable: true);
            #endregion

            #region migrate element fields
            migrationBuilder.Sql(@"

    -- ====================== MIGRATE ELEMENT DATA ===================

    -- Clean up some bad data
    DELETE FROM [CustomField.Layout.Element]
    WHERE DefinitionID IS NULL
    ;

    -- Add a temporary field track the old IDs
    ALTER TABLE [CustomField.Layout.Element]
    ADD [OldContainerID] INT NULL  -- This is temporary for migration
    ;
    GO

    -- Update the CF Elements table's new fields
    UPDATE El
    SET -- update the data type in the Element list, since it doesn't appear to be correct
        DataType = Def.DataType,    
        -- Set the Element Type based on the Input 
        -- these are the same fields for all used by Custom Fields
        -- https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/881131620/AssemblyElementType+Enum
        ElementType = Def.InputType, 
        -- Pull the LayoutID from the Container
        LayoutID = Con.LayoutID,
        -- Copy Tooltip
        Tooltip = Def.Description,
        -- Pull the column number out of the Element Properties field
        [Column] = CASE WHEN CHARINDEX(':1}', CONVERT(VARCHAR(50), ElementProperties)) > 0 THEN 1 ELSE 0 END
    FROM [CustomField.Layout.Element] El
    LEFT JOIN [CustomField.Definition] Def on El.DefinitionID = Def.id
    LEFT JOIN [CustomField.Layout.Container] Con on Con.ID = El.ContainerID
    ;

	ALTER TABLE [CustomField.Layout.Element] ALTER COLUMN ContainerID int null;

    -- Now Add Each Container as a Group into the Elements Table
    INSERT INTO [CustomField.Layout.Element] (
            [BID] ,[ID], [ContainerID], [Name], [SortIndex], [DefinitionID]
            , [DataType], [ParentID]
            , [ColumnsWide], [Column], [ElementType]
            , [Row], [Tooltip], [LayoutID], [OldContainerID]
            )
        SELECT G.BID
                -- Compute the next IDs for the New Groups
                , ISNULL((Select NextID FROM [Util.NextID] N WHERE N.BID = G.BID AND N.ClassTypeID = 15029),1000) + ROW_NUMBER() OVER(PARTITION BY BID ORDER BY BID, ID) - 1 AS ID
                , G.ParentID AS ContainerID
                , G.[Name]
                , G.[SortIndex]
                , CONVERT(int, NULL) AS [DefinitionID]
                , CONVERT(int, NULL) AS [DataType]
                , CONVERT(int, NULL) AS [ParentID]
                , CONVERT(TINYINT, 1) AS [ColumnsWide]
                , G.ColumnNo AS [Column]
                , G.ElementType AS [ElementType]
                , CONVERT(TINYINT, 0) AS [Row]
                , CONVERT(NVARCHAR(512), NULL) AS [Tooltip]
                , G.LayoutID
                , G.ID AS [OldContainerID]
        FROM [CustomField.Layout.Container] G
    ;

    -- Now Update the Util.NextIDs based on the IDs added
    UPDATE N
    SET NextID = 1 + IsNull((SELECT Max(ID) FROM [CustomField.Layout.Element] E WHERE E.BID = N.BID), 999)
    FROM [Util.NextID] N
    WHERE N.ClassTypeID = 15029;

    -- Now update the ParentIDs to point to their new Groups
    UPDATE El
    SET ParentID = G.ID
    FROM [CustomField.Layout.Element] El
    JOIN [CustomField.Layout.Element] G ON El.BID = G.BID AND El.ContainerID = G.OldContainerID
    WHERE El.ParentID IS NULL  -- Don't update things already in groups
    ;

    -- Remove the temporary value we added for migration
    ALTER TABLE [CustomField.Layout.Element]
    DROP COLUMN [OldContainerID]
    ;
");
            #endregion

            #region remove old FKs, Indexes, Tables, and Columns

            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_enum.DataType_DataType",
                table: "CustomField.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_DisplayType",
                table: "CustomField.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_InputType",
                table: "CustomField.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Layout.Element_CustomField.Layout.Container",
                table: "CustomField.Layout.Element");


            migrationBuilder.DropIndex(
                name: "IX_CustomField.Layout.Element_Containter",
                table: "CustomField.Layout.Element");

            migrationBuilder.Sql(
            @"
                DROP INDEX IF EXISTS [IX_CustomField.Definition_DisplayFormatType] ON [CustomField.Definition]
                DROP INDEX IF EXISTS [IX_CustomField.Definition_InputType] ON [CustomField.Definition]
            "
            );

            migrationBuilder.DropColumn(
                name: "ContainerID",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "ElementProperties",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "DisplayFormatType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "InputType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "MetaData",
                table: "CustomField.Definition");

            migrationBuilder.DropTable(
                name: "CustomField.Layout.Container");

            migrationBuilder.DropTable(
                name: "enum.CustomField.InputType");
            #endregion

            #region add back FKs, Indexes

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Element_BID_LayoutID",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "LayoutID" });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Definition_DisplayType",
                table: "CustomField.Definition",
                column: "DisplayType");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_DataType",
                table: "CustomField.Definition",
                column: "DataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_DisplayType",
                table: "CustomField.Definition",
                column: "DisplayType",
                principalTable: "enum.CustomField.DisplayType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Layout.Element_CustomField.Layout.Definition",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "LayoutID" },
                principalTable: "CustomField.Layout.Definition",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
            #endregion
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_DataType",
                table: "CustomField.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Definition_DisplayType",
                table: "CustomField.Definition");

            migrationBuilder.DropForeignKey(
                name: "FK_CustomField.Layout.Element_CustomField.Layout.Definition",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropIndex(
                name: "IX_CustomField.Layout.Element_BID_LayoutID",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropIndex(
                name: "IX_CustomField.Definition_DisplayType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "Column",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "ColumnsWide",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "ElementType",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "IsDisabled",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "IsReadOnly",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "LayoutID",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "Row",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "Tooltip",
                table: "CustomField.Layout.Element");

            migrationBuilder.DropColumn(
                name: "AllowCustomValue",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "AllowDecimals",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "AllowMultiSelect",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "AllowNotSetOption",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "AllowTextFormatting",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "AllowUserTypedOptions",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "AltText",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "DecimalPlaces",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "DefaultOption",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "DefaultValue",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "DisplayFormat",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "DisplaySpinner",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "DisplayType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "ElementType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "ElementUseCount",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "GroupOptionsByCategory",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "GroupType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "HasMaxLength",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "IsDisabled",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "LabelType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "ListDataType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "ListOptions",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "ListValuesJSON",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "NumberOfDecimals",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "OptionOneLabel",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "OptionTwoLabel",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "PickerType",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "PresetTime",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "SpinnerIncrement",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "Time",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "ToggleOptions",
                table: "CustomField.Definition");

            migrationBuilder.DropColumn(
                name: "Tooltip",
                table: "CustomField.Definition");

            migrationBuilder.AlterColumn<byte>(
                name: "UnitType",
                table: "Part.Subassembly.Variable",
                type: "TINYINT SPARSE",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "TINYINT",
                oldNullable: true);

            migrationBuilder.AlterColumn<byte>(
                name: "UnitID",
                table: "Part.Subassembly.Variable",
                type: "TINYINT SPARSE",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "TINYINT",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CustomUnitText",
                table: "Part.Subassembly.Variable",
                type: "NVARCHAR(255) SPARSE",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "NVARCHAR(255)",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContainerID",
                table: "CustomField.Layout.Element",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ElementProperties",
                table: "CustomField.Layout.Element",
                type: "xml",
                nullable: true);

            migrationBuilder.AlterColumn<byte>(
                name: "UnitType",
                table: "CustomField.Definition",
                type: "TINYINT SPARSE",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldNullable: true);

            migrationBuilder.AlterColumn<byte>(
                name: "UnitID",
                table: "CustomField.Definition",
                type: "TINYINT SPARSE",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CustomUnitText",
                table: "CustomField.Definition",
                type: "NVARCHAR(255) SPARSE",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "NVARCHAR(255)",
                oldNullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "DisplayFormatType",
                table: "CustomField.Definition",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "InputType",
                table: "CustomField.Definition",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<string>(
                name: "MetaData",
                table: "CustomField.Definition",
                type: "xml",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CustomField.Layout.Container",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    ID = table.Column<int>(type: "int", nullable: false),
                    ClassTypeID = table.Column<int>(type: "int", nullable: false, computedColumnSql: "((15028))"),
                    ColumnNo = table.Column<byte>(type: "tinyint", nullable: false),
                    ElementType = table.Column<byte>(type: "tinyint", nullable: false, defaultValueSql: "0"),
                    Hint = table.Column<string>(type: "NVARCHAR(MAX)", nullable: true),
                    LayoutID = table.Column<short>(type: "smallint", nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    Name = table.Column<string>(type: "NVARCHAR(100)", nullable: false),
                    ParentID = table.Column<int>(type: "int", nullable: true),
                    SortIndex = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomField.Layout.Container", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CustomField.Layout.Container_enum.Part.Subassembly.ElementType",
                        column: x => x.ElementType,
                        principalTable: "enum.Part.Subassembly.ElementType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomField.Layout.Container_CustomField.Layout.Definition",
                        columns: x => new { x.BID, x.LayoutID },
                        principalTable: "CustomField.Layout.Definition",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomField.Layout.Container_CustomField.Layout.Container",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "CustomField.Layout.Container",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "enum.CustomField.InputType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    DataType = table.Column<short>(type: "smallint", nullable: false),
                    Name = table.Column<string>(type: "varchar(255)", unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.InputType", x => x.ID);
                    table.ForeignKey(
                        name: "FK_enum.CustomField.InputType_enum.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Element_Containter",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "ContainerID", "SortIndex", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Definition_DisplayFormatType",
                table: "CustomField.Definition",
                column: "DisplayFormatType");

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Definition_InputType",
                table: "CustomField.Definition",
                column: "InputType");

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Container_ElementType",
                table: "CustomField.Layout.Container",
                column: "ElementType");

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Container_BID_ParentID",
                table: "CustomField.Layout.Container",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Container_Layout",
                table: "CustomField.Layout.Container",
                columns: new[] { "BID", "LayoutID", "SortIndex" });

            migrationBuilder.CreateIndex(
                name: "IX_enum.CustomField.InputType_DataType",
                table: "enum.CustomField.InputType",
                column: "DataType");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_enum.DataType_DataType",
                table: "CustomField.Definition",
                column: "DataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_DisplayType",
                table: "CustomField.Definition",
                column: "DisplayFormatType",
                principalTable: "enum.CustomField.DisplayType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Definition_InputType",
                table: "CustomField.Definition",
                column: "InputType",
                principalTable: "enum.CustomField.InputType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CustomField.Layout.Element_CustomField.Layout.Container",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "ContainerID" },
                principalTable: "CustomField.Layout.Container",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
