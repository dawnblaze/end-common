using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180721045622_Insert_OR_Update_Options_to_enable_all_modules_for_BID_1")]
    public partial class Insert_OR_Update_Options_to_enable_all_modules_for_BID_1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    /*
                        2018-07-20
                        Insert/Update Option.Data records to enable all modules for BID 1
                    */

                    DECLARE @AccountingModule INT = 20003;
                    DECLARE @ManagementModule INT = 20004;
                    DECLARE @ProductionModule INT = 20005;
                    DECLARE @PurchasingModule INT = 20006;
                    DECLARE @SalesModule INT = 20007;

                    IF NOT EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @AccountingModule)
                    BEGIN
                        INSERT INTO [Option.Data]
                        (IsActive, OptionID, [Value], BID)
                        VALUES
                        (1, @AccountingModule, '1', 1)
                    END
                    ELSE
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '1'
                        WHERE OptionID = @AccountingModule
                    END

                    IF NOT EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @ManagementModule)
                    BEGIN
                        INSERT INTO [Option.Data]
                        (IsActive, OptionID, [Value], BID)
                        VALUES
                        (1, @ManagementModule, '1', 1)
                    END
                    ELSE
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '1'
                        WHERE OptionID = @ManagementModule
                    END

                    IF NOT EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @ProductionModule)
                    BEGIN
                        INSERT INTO [Option.Data]
                        (IsActive, OptionID, [Value], BID)
                        VALUES
                        (1, @ProductionModule, '1', 1)
                    END
                    ELSE
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '1'
                        WHERE OptionID = @ProductionModule
                    END

                    IF NOT EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @PurchasingModule)
                    BEGIN
                        INSERT INTO [Option.Data]
                        (IsActive, OptionID, [Value], BID)
                        VALUES
                        (1, @PurchasingModule, '1', 1)
                    END
                    ELSE
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '1'
                        WHERE OptionID = @PurchasingModule
                    END

                    IF NOT EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @SalesModule)
                    BEGIN
                        INSERT INTO [Option.Data]
                        (IsActive, OptionID, [Value], BID)
                        VALUES
                        (1, @SalesModule, '1', 1)
                    END
                    ELSE
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '1'
                        WHERE OptionID = @SalesModule
                    END
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    /*
                        REVERT MIGRATION!
                        2018-07-20
                        Update Option.Data records (if exists) to DISABLE all modules for BID 1
                    */

                    DECLARE @AccountingModule INT = 20003;
                    DECLARE @ManagementModule INT = 20004;
                    DECLARE @ProductionModule INT = 20005;
                    DECLARE @PurchasingModule INT = 20006;
                    DECLARE @SalesModule INT = 20007;

                    IF EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @AccountingModule)
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '0'
                        WHERE OptionID = @AccountingModule
                    END

                    IF EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @ManagementModule)
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '0'
                        WHERE OptionID = @ManagementModule
                    END

                    IF EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @ProductionModule)
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '0'
                        WHERE OptionID = @ProductionModule
                    END

                    IF EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @PurchasingModule)
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '0'
                        WHERE OptionID = @PurchasingModule
                    END

                    IF EXISTS (SELECT TOP 1 [Value] FROM [Option.Data] WHERE OptionID = @SalesModule)
                    BEGIN
                        UPDATE [Option.Data]
                        SET [Value] = '0'
                        WHERE OptionID = @SalesModule
                    END
                "
            );
        }
    }
}

