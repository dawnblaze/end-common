using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180409204152_TablesAndColumnsView_END-866")]
    public partial class TablesAndColumnsView_END866 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP VIEW IF EXISTS [dbo].[TablesAndColumns]
GO

CREATE VIEW [dbo].[TablesAndColumns]
AS
SELECT TOP 100 PERCENT
       tbl.name as TableName, tbl.object_id as TableID, tbl.type_desc as TableType,
       col.column_id as ColumnID,  col.name as ColumnName,
       coltypes.name as ColumnType, col.Is_Nullable as IsNullable,
       col.is_computed IsComputed, col.is_sparse IsSparse, col.is_identity IsIdentity,
       comcol.definition ComputedFormula, col.max_length, PKs.IsPK, col.Precision, col.Scale,
       IIF(EXISTS(SELECT * FROM sys.tables ST WHERE tbl.object_id = ST.history_table_id), 1, 0) AS IsHistoryTable,
       IIF(tbl.history_table_id IS NOT NULL, 1, 0) AS IsTemporalTable,
       HistoryTable.name as HistoryTableName

FROM sys.tables tbl
     JOIN sys.columns col ON tbl.object_id = col.object_id
     JOIN sys.types coltypes ON col.system_type_id = coltypes.system_type_id AND col.user_type_id = coltypes.user_type_id
     LEFT JOIN ( SELECT 1 AS IsPK, table_name, column_name
                 FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
                 WHERE OBJECTPROPERTY(OBJECT_ID(constraint_name), 'IsPrimaryKey') = 1 ) AS PKs ON PKs.table_name = tbl.name AND PKs.column_name = col.name
     LEFT JOIN sys.computed_columns comcol ON tbl.object_id = comcol.object_id AND col.column_id = comcol.column_id
     LEFT JOIN sys.tables HistoryTable on HistoryTable.object_id = tbl.history_table_id

WHERE tbl.type = 'U' AND tbl.is_ms_shipped = 0

ORDER BY TableName, ColumnID
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP VIEW IF EXISTS [dbo].[TablesAndColumns]
GO

CREATE VIEW [dbo].[TablesAndColumns]
AS
SELECT TOP (100) PERCENT tbl.name AS TableName, tbl.object_id AS TableID, tbl.type_desc AS TableType, col.column_id AS ColumnID, col.name AS ColumnName, coltypes.name AS ColumnType, col.is_nullable AS IsNullable, col.is_computed AS IsComputed, col.is_sparse AS IsSparse, col.is_identity AS IsIdentity, comcol.definition AS ComputedFormula, col.max_length, PKs.IsPK, col.precision, 
 col.scale
FROM            sys.tables AS tbl INNER JOIN
   sys.columns AS col ON tbl.object_id = col.object_id INNER JOIN
   sys.types AS coltypes ON col.system_type_id = coltypes.system_type_id AND col.user_type_id = coltypes.user_type_id LEFT OUTER JOIN
       (SELECT        1 AS IsPK, TABLE_NAME, COLUMN_NAME
         FROM            INFORMATION_SCHEMA.KEY_COLUMN_USAGE
         WHERE        (OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_NAME), 'IsPrimaryKey') = 1)) AS PKs ON PKs.TABLE_NAME = tbl.name AND PKs.COLUMN_NAME = col.name LEFT OUTER JOIN
   sys.computed_columns AS comcol ON tbl.object_id = comcol.object_id AND col.column_id = comcol.column_id
WHERE        (tbl.type = 'U') AND (tbl.is_ms_shipped = 0)
ORDER BY TableName, ColumnID
");
        }
    }
}

