using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_IsPrivate_SystemListFilterCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria]
                           ([TargetClassTypeID]
                           ,[Name]
                           ,[Label]
                           ,[Field]
                           ,[IsHidden]
                           ,[DataType]
                           ,[InputType]
                           ,[AllowMultiple]
                           ,[ListValues]
                           ,[ListValuesEndpoint]
                           ,[IsLimitToList]
                           ,[SortIndex]
                           ,[ID])
                VALUES
                    (
		                12060--<TargetClassTypeID, int,>
                        ,'IsPrivate'--,<Name, varchar(255),>
                        ,'IsPrivate'--,<Label, varchar(255),>
                        ,'-IsShared'--,<Field, varchar(255),>
                        ,0--,<IsHidden, bit,>
                        ,3--,<DataType, smallint,>
                        ,2--,<InputType, tinyint,>
                        ,0--,<AllowMultiple, bit,>
                        ,NULL--,<ListValues, varchar(max),>
                        ,NULL--,<ListValuesEndpoint, varchar(255),>
                        ,0--,<IsLimitToList, bit,>
                        ,9--,<SortIndex, tinyint,>
                        ,(SELECT (MAX(ID)+1) FROM [dbo].[System.List.Filter.Criteria])--,<ID, smallint,>)
		            )
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [System.List.Filter.Criteria] WHERE [TargetClassTypeID] = 12060 AND Field = 'IsShared';
            ");
        }
    }
}
