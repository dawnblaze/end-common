using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180814110350_UpdateOrderOptionDefinitions")]
    public partial class UpdateOrderOptionDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.Option.Definition]
               SET [Description] = ''
               WHERE ID = 4
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'In some countries invoice numbers must be issued sequentially based on when the order is invoiced.?            If checked, invoices will generate a unique invoice number based on the invoice date.
            If not checked, invoices will be generated based on the order creation date.'
               WHERE ID = 54
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = ''
               WHERE ID = 6041
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'When creating an order, the time of day pre-populated in Design Due will default to the time provided here.'
               WHERE ID = 6042
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'When creating an order, the time of day pre-populated in Line Item Due will default to the time provided here. Line Item due time is also the Production due time.'
               WHERE ID = 6043
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = ''
               WHERE ID = 6044
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If your orders are typically custom produced (Manufacturing), consider selecting WIP. If your orders are typically completed immediately (Retail), consider selecting Invoiced. If your orders are typically fulfilled (Ecommerce), consider selecting Built.'
               WHERE ID = 6045
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, the system will automatically prompt to apply credit from the customer�s account. If not checked, the system will not automatically prompt to apply credit from the customer�s account.'
               WHERE ID = 6046
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, an Order Origin must be selected before an order can be saved. If not checked, an Order Origin is not required prior to saving an order.'
               WHERE ID = 6047
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = ''
               WHERE ID = 6048
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, a Tax Exempt ID must be entered in order to make the order tax exempt. If not checked, an order can be made tax exempt with out entering a Tax Exempt ID.'
               WHERE ID = 6049
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = ''
               WHERE ID = 6050
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, a reason from the predefined Reasons List must be selected. If not checked, the user can type in a custom reason.'
               WHERE ID = 6051
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = ''
               WHERE ID = 6052
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, a reason from the predefined Reasons List must be selected. If not checked, the user can type in a custom reason.'
               WHERE ID = 6053
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'When cloning an order the text supplied here will be inserted before the original description in the new order.'
               WHERE ID = 6054
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = ''
               WHERE ID = 6055
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, all pricing will be recomputed in the cloned order. If not checked, the prices will be marked as overridden and use the original pricing in the cloned order.'
               WHERE ID = 6056
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, proof files will be duplicated in the proof folders of the cloned order. If not checked, no proof files will be copied to the cloned order.'
               WHERE ID = 6057
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, Document Management files from the original order will be duplicated into the folders of the cloned order. If not checked, no Document Management files will be copied to the cloned order. Note: Copying Documents will be in addition to any files in the template and copied documents will overwrite any template files copied into the order with the same name.'
               WHERE ID = 6058
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, when an order is cloned the Roles associated with the customer�s team will overwrite the roles from the original order. If not checked, the roles on the cloned order will be copied from the original order.'
               WHERE ID = 6059
            GO

            UPDATE [System.Option.Definition]
               SET [Description] = 'If checked, product notes will be copied into the cloned order. If not checked, product notes will not be copied to the cloned order.'
               WHERE ID = 6060
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

