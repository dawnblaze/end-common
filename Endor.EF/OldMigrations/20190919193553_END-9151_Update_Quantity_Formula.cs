﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9151_Update_Quantity_Formula : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Part.Subassembly.Variable]
SET [DefaultValue] = '=QuantityPerItem.Value * LineItemQuantity', [IsRequired] = 1, [IsFormula] = 1
WHERE [Name] = 'Quantity'
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
