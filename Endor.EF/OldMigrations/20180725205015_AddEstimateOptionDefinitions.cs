using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180725205015_AddEstimateOptionDefinitions")]
    public partial class AddEstimateOptionDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
VALUES (6081, N'Estimate.Variations.Enabled', N'Enable Estimate Variations', N'', 1, 14, N'', N'1000', 0)
    , (6082, N'Estimate.Default.Design.DueTime', N'Default Time for Design Due Date', N'', 9, 14, N'', N'12:00 PM', 0)
    , (6083, N'Estimate.Default.LineItem.DueTime', N'Default Time for Due Date', N'', 9, 14, N'', N'4:00 PM', 0)
    , (6084, N'Estimate.Default.DestinationType', N'Default Destination for New Orders', N'', 1, 14, N'', N'1', 0)
    , (6085, N'Estimate.Require.OrderOrigin', N'Require Order Origin', N'', 3, 14, N'', N'0', 0)
    , (6086, N'Estimate.Require.Exempt.TaxNumber', N'Require Tax Number for Tax Exempt Estimates', N'', 3, 14, N'', N'0', 0)
    , (6087, N'Estimate.Require.Reason.Edit', N'Prompt for Reason on Estimate Edit', N'', 3, 14, N'', N'1', 0)
    , (6088, N'Estimate.Require.Reason.Edit.ForceToList', N'Force Selection from Existing Reason', N'', 3, 14, N'', N'1', 0)
    , (6089, N'Estimate.Require.Reason.Cancellation', N'Prompt for Reason on Estimate Cancellation', N'', 3, 14, N'', N'1', 0)
    , (6090, N'Estimate.Require.Reason.Cancellation.ForceToList', N'Force Selection from  Existing Reason', N'', 3, 14, N'', N'1', 0)
    , (6091, N'Estimate.Clone.Prefix', N'Prefix for Description on Cloned Estimates', N'', 0, 14, N'', N'Re-Order:', 0)
    , (6092, N'Estimate.Clone.BlankPONumber', N'Blank out PO Number when an Estimate is Cloned', N'', 3, 14, N'', N'1', 0)
    , (6093, N'Estimate.Clone.RecomputePricing', N'Update Pricing to Current Prices', N'', 3, 14, N'', N'1', 0)
    , (6094, N'Estimate.Clone.CopyArtwork', N'Copy Artwork to New Order/Estimate', N'', 3, 14, N'', N'1', 0)
    , (6095, N'Estimate.Clone.CopyDocuments', N'Copy Documents to New Order/Estimate', N'', 3, 14, N'', N'1', 0)
    , (6096, N'Estimate.Clone.CopyProductNotes', N'Copy Product Notes', N'', 3, 14, N'', N'1', 0)
    , (6097, N'Estimate.Clone.UpdateEmployeeRoles', N'Update Employee Roles to Customer''s Current Team', N'', 3, 14, N'', N'1', 0)
    , (6098, N'Estimate.Clone.UpdateContactRoles', N'Update Contact Roles', N'', 3, 14, N'', N'1', 0)
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Definition] WHERE Name in (
      N'Estimate.Default.Design.DueTime'
    , N'Estimate.Default.LineItem.DueTime'
    , N'Estimate.Default.DestinationType'
    , N'Estimate.Require.OrderOrigin'
    , N'Estimate.Require.Exempt.TaxNumber'
    , N'Estimate.Require.Reason.Edit'
    , N'Estimate.Require.Reason.Edit.ForceToList'
    , N'Estimate.Require.Reason.Cancellation'
    , N'Estimate.Require.Reason.Cancellation.ForceToList'
    , N'Estimate.Clone.Prefix'
    , N'Estimate.Clone.BlankPONumber'
    , N'Estimate.Clone.RecomputePricing'
    , N'Estimate.Clone.CopyArtwork'
    , N'Estimate.Clone.CopyDocuments'
    , N'Estimate.Clone.CopyProductNotes'
    , N'Estimate.Clone.UpdateEmployeeRoles'
    , N'Estimate.Clone.UpdateContactRoles'
)
;
");
        }
    }
}

