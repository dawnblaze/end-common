using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180328211037_AddLaborCategory")]
    public partial class AddLaborCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Labor.Category",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12022"),
                    CreatedDate = table.Column<DateTime>(type: "date", nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    ParentID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Labor.Category", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Labor.Category_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.Labor.Category_Part.Labor.Category",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Part.Labor.Category",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.Sql(@"
DROP VIEW IF EXISTS [dbo].[Part.Labor.Category.SimpleList]
GO

CREATE VIEW [dbo].[Part.Labor.Category.SimpleList]
    AS
SELECT	[BID]
		, [ID]
		, [ClassTypeID]
		, [Name] as DisplayName
		, [IsActive]
		, CONVERT(BIT, 0) AS[HasImage]
		, CONVERT(BIT, 0) AS[IsDefault]
FROM [dbo].[Part.Labor.Category]
");

            migrationBuilder.CreateTable(
                name: "Part.Labor.CategoryLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    PartID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Labor.CategoryLink", x => new { x.BID, x.PartID, x.CategoryID });
                    table.ForeignKey(
                        name: "FK_Part.Labor.CategoryLink_Part.Labor.Category",
                        columns: x => new { x.BID, x.CategoryID },
                        principalTable: "Part.Labor.Category",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Labor.CategoryLink_Part.Labor.Data",
                        columns: x => new { x.BID, x.PartID },
                        principalTable: "Part.Labor.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Labor.Data_Name_IsActive",
                table: "Part.Labor.Category",
                columns: new[] { "BID", "Name", "IsActive", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Labor.Data_ParentID_Name",
                table: "Part.Labor.Category",
                columns: new[] { "BID", "ParentID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Labor.CategoryLink_Category",
                table: "Part.Labor.CategoryLink",
                columns: new[] { "BID", "CategoryID", "PartID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP VIEW IF EXISTS [dbo].[Part.Labor.Category.SimpleList]
");

            migrationBuilder.DropTable(
                name: "Part.Labor.CategoryLink");

            migrationBuilder.DropTable(
                name: "Part.Labor.Category");
        }
    }
}

