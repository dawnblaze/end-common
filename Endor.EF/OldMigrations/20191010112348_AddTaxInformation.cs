﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddTaxInformation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP INDEX [IX_Order.Tax.Item.Assessment_BID_TaxCodeID] ON [Order.Tax.Item.Assessment]
;

DROP INDEX [IX_Order.Tax.Item.Assessment_BID_TaxItemID] ON [Order.Tax.Item.Assessment]
;
");

            migrationBuilder.AddColumn<int>(
                name: "ItemComponentID",
                table: "Order.Tax.Item.Assessment",
                type: "INT SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemSurchargeID",
                table: "Order.Tax.Item.Assessment",
                type: "INT SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "TaxGroupID",
                table: "Order.Tax.Item.Assessment",
                type: "SMALLINT SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemSurchargeID",
                table: "Accounting.GL.Data",
                type: "int sparse",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "TaxCodeID",
                table: "Accounting.GL.Data",
                type: "smallint sparse",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Order.Tax.Item.Assessment_ItemComponentID",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "ItemComponentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Tax.Item.Assessment_ItemSurchargeID",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "ItemSurchargeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Tax.Item.Assessment_TaxGroupID",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "TaxGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_ItemSurchargeID",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "ItemSurchargeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_TaxCodeID",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "TaxCodeID" });

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_Order.Item.Surcharge",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "ItemSurchargeID" },
                principalTable: "Order.Item.Surcharge",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_Accounting.Tax.Code",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "TaxCodeID" },
                principalTable: "Accounting.Tax.Code",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Order.Item.Component",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "ItemComponentID" },
                principalTable: "Order.Tax.Item.Assessment",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Order.Item.Surcharge",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "ItemSurchargeID" },
                principalTable: "Order.Item.Surcharge",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Accounting.Tax.Group",
                table: "Order.Tax.Item.Assessment",
                columns: new[] { "BID", "TaxGroupID" },
                principalTable: "Accounting.Tax.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_Order.Item.Surcharge",
                table: "Accounting.GL.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_Accounting.Tax.Code",
                table: "Accounting.GL.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Order.Item.Component",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Order.Item.Surcharge",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Tax.Item.Assessment_Accounting.Tax.Group",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropIndex(
                name: "IX_Order.Tax.Item.Assessment_BID_ItemComponentID",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropIndex(
                name: "IX_Order.Tax.Item.Assessment_BID_ItemSurchargeID",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropIndex(
                name: "IX_Order.Tax.Item.Assessment_BID_TaxGroupID",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_BID_ItemSurchargeID",
                table: "Accounting.GL.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_BID_TaxCodeID",
                table: "Accounting.GL.Data");

            migrationBuilder.DropColumn(
                name: "ItemComponentID",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropColumn(
                name: "ItemSurchargeID",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropColumn(
                name: "TaxGroupID",
                table: "Order.Tax.Item.Assessment");

            migrationBuilder.DropColumn(
                name: "ItemSurchargeID",
                table: "Accounting.GL.Data");

            migrationBuilder.DropColumn(
                name: "TaxCodeID",
                table: "Accounting.GL.Data");
        }
    }
}
