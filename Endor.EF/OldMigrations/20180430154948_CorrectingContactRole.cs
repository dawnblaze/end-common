using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180430154948_CorrectingContactRole")]
    public partial class CorrectingContactRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Contact.Role_Contact.Data_BID_ContactID",
                table: "Order.Contact.Role");

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Contact.Role_RoleType",
                table: "Order.Contact.Role",
                column: "RoleType",
                principalTable: "enum.Order.ContactRoleType ",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Contact.Role_ContactID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "ContactID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
DELETE FROM [enum.Order.ContactRoleType] WHERE [ID] = 1;
INSERT [enum.Order.ContactRoleType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (1, N'Primary', 7, 1);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Contact.Role_RoleType",
                table: "Order.Contact.Role");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Contact.Role_ContactID",
                table: "Order.Contact.Role");

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Contact.Role_Contact.Data_BID_ContactID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "ContactID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
DELETE FROM [enum.Order.ContactRoleType] WHERE [ID] = 1;
INSERT [enum.Order.ContactRoleType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (1, N'Primary', 3, 1);
");
        }

    }
}

