using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181002165700_CompanyDetails_LayoutName_Correction")]
    public partial class CompanyDetails_LayoutName_Correction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE dbo.[CustomField.Layout.Definition]
                SET [Name] = 'Company Details'
                WHERE [Name] = 'Customer Details' AND [AppliesToClassTypeID]=2000            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE dbo.[CustomField.Layout.Definition]
                SET [Name] = 'Customer Details'
                WHERE [Name] = 'Company Details' AND [AppliesToClassTypeID]=2000            
            ");
        }
    }
}

