﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10423EmailTemplatesAddIsActiveFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                VALUES
                    (14220 --<TargetClassTypeID, int,>
                    ,'Show Inactive' --<Name, varchar(255),>
                    ,'Show Inactive' --<Label, varchar(255),>
                    ,'-IsActive' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,3 --<DataType, tinyint,>
                    ,13 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,NULL --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,0 --<IsLimitToList, bit,>
                    ,4 --<SortIndex, tinyint,>
                    ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                    );
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
