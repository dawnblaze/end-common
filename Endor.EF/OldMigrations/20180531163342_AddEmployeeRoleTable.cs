using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180531163342_AddEmployeeRoleTable")]
    public partial class AddEmployeeRoleTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Employee.Role",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AllowMultiple = table.Column<bool>(nullable: false),
                    AllowOnTeam = table.Column<bool>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "5004"),
                    EstimateDestinationRestriction = table.Column<byte>(type: "tinyint", nullable: false),
                    EstimateItemRestriction = table.Column<byte>(type: "tinyint", nullable: false),
                    EstimateRestriction = table.Column<byte>(type: "tinyint", nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    OpportunityRestriction = table.Column<byte>(type: "tinyint", nullable: false),
                    OrderDestinationRestriction = table.Column<byte>(type: "tinyint", nullable: false),
                    OrderItemRestriction = table.Column<byte>(type: "tinyint", nullable: false),
                    OrderRestriction = table.Column<byte>(type: "tinyint", nullable: false),
                    PORestriction = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.Role", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Employee.Role_BID",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.Role_enum.Role.Access_EstimateDestinationRestriction",
                        column: x => x.EstimateDestinationRestriction,
                        principalTable: "enum.Role.Access",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.Role_enum.Role.Access_EstimateItemRestriction",
                        column: x => x.EstimateItemRestriction,
                        principalTable: "enum.Role.Access",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.Role_enum.Role.Access_EstimateRestriction",
                        column: x => x.EstimateRestriction,
                        principalTable: "enum.Role.Access",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.Role_enum.Role.Access_OpportunityRestriction",
                        column: x => x.OpportunityRestriction,
                        principalTable: "enum.Role.Access",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.Role_enum.Role.Access_OrderDestinationRestriction",
                        column: x => x.OrderDestinationRestriction,
                        principalTable: "enum.Role.Access",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.Role_enum.Role.Access_OrderItemRestriction",
                        column: x => x.OrderItemRestriction,
                        principalTable: "enum.Role.Access",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.Role_enum.Role.Access_OrderRestriction",
                        column: x => x.OrderRestriction,
                        principalTable: "enum.Role.Access",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.Role_enum.Role.Access_PORestriction",
                        column: x => x.PORestriction,
                        principalTable: "enum.Role.Access",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.Sql(@"
DECLARE @BID SMALLINT = 1;
 
INSERT [Employee.Role] ([BID], [ID], [Name], [ModifiedDT], [IsActive], [IsSystem], [AllowMultiple], [AllowOnTeam], [OrderRestriction], [OrderItemRestriction], [OrderDestinationRestriction], [EstimateRestriction], [EstimateItemRestriction], [EstimateDestinationRestriction], [OpportunityRestriction], [PORestriction])
VALUES (@BID, 1, N'Salesperson', CAST(N'2018-05-24T19:49:32.8400000' AS DateTime2), 1, 1, 1, 1, 2, 0, 0, 2, 0, 0, 2, 0)
    , (@BID, 2, N'CSR', CAST(N'2018-05-24T19:51:07.9400000' AS DateTime2), 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0)
    , (@BID, 3, N'Designer', CAST(N'2018-05-24T19:51:14.9200000' AS DateTime2), 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0)
    , (@BID, 4, N'Project Manager', CAST(N'2018-05-24T19:51:21.0600000' AS DateTime2), 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0)
    , (@BID, 5, N'Account Manager', CAST(N'2018-05-24T19:51:28.6900000' AS DateTime2), 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0)
    , (@BID, 6, N'Production', CAST(N'2018-05-24T19:51:40.8500000' AS DateTime2), 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0)
    , (@BID, 9, N'QA', CAST(N'2018-05-24T19:51:44.8100000' AS DateTime2), 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0)
    , (@BID, 41, N'Requestor', CAST(N'2018-05-24T19:51:54.0700000' AS DateTime2), 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2)
    , (@BID, 42, N'Approver', CAST(N'2018-05-24T19:51:59.0100000' AS DateTime2), 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
    , (@BID, 43, N'Orderer', CAST(N'2018-05-24T19:52:06.0200000' AS DateTime2), 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
    , (@BID, 44, N'Receiver', CAST(N'2018-05-24T19:52:12.3400000' AS DateTime2), 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1)
    , (@BID, 61, N'Shipper', CAST(N'2018-05-24T19:52:22.6800000' AS DateTime2), 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0)
    , (@BID, 62, N'Installer', CAST(N'2018-05-24T19:52:30.2600000' AS DateTime2), 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 0)
    , (@BID, 63, N'Delivery Person', CAST(N'2018-05-28T17:22:52.3700000' AS DateTime2), 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0)
    , (@BID, 64, N'Service Person', CAST(N'2018-05-28T17:23:02.6000000' AS DateTime2), 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0)
    , (@BID, 253, N'Assigned To', CAST(N'2018-05-28T17:23:13.1700000' AS DateTime2), 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1)
    , (@BID, 254, N'Entered By', CAST(N'2018-05-28T17:23:20.5400000' AS DateTime2), 1, 1, 0, 0, 2, 0, 0, 2, 0, 0, 2, 2)
    , (@BID, 255, N'Observer', CAST(N'2018-05-28T17:23:29.4400000' AS DateTime2), 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
;");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employee.Role");
        }
    }
}

