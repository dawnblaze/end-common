using Endor.Models;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180406182931_MaterialCategoryAndMaterialCategoryLinkSprocs_END-809")]
    public partial class MaterialCategoryAndMaterialCategoryLinkSprocs_END809 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            ClassType ct = ClassType.MaterialCategory;

            migrationBuilder.Sql($@"IF not Exists (Select 1 from [Util.NextID] where ClassTypeID = {ct.ID()} and BID = 1)
  Begin
	Insert into [Util.NextID]
	Values (1, {ct.ID()}, 1000)
  end");

            migrationBuilder.Sql(@"
                /****** Object:  StoredProcedure [dbo].[Part.Material.Category.Action.CanDelete]    Script Date: 4/3/2018 7:41:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
========================================================
    Name: [Part.Material.Category.Action.CanDelete]

    Description: This procedure checks if the MaterialCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Material.Category.Action.CanDelete] @BID=1, @ID=1, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Material.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the MaterialCategory can be deleted. The boolean response is returned in the body.  A MaterialCategory can be deleted if
	- there are no materials linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Material.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Material Category Specified Does not Exist. MaterialCategoryID=', @ID)

    -- there are no materials linked to it or if the force option is specified to deletes the links.
	ELSE IF  EXISTS( SELECT * FROM [Part.Material.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Material Category is linked to a Material Data. MaterialCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
/****** Object:  StoredProcedure [dbo].[Part.Material.Category.Action.SetActive]    Script Date: 4/3/2018 7:53:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Part.Material.Category.Action.SetActive]
--
-- Description: This procedure sets the MaterialCategory to active or inactive
--
-- Sample Use:   EXEC dbo.[Part.Material.Category.Action.SetActive] @BID=1, @MaterialCategoryID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Material.Category.Action.SetActive]
-- DECLARE 
          @BID                  TINYINT  -- = 1
        , @MaterialCategoryID    SMALLINT -- = 2

        , @IsActive             BIT     = 1

        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the MaterialCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Material.Category] WHERE BID = @BID and ID = @MaterialCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MaterialCategory Specified. MaterialCategoryID='+CONVERT(VARCHAR(12),@MaterialCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Part.Material.Category] L
    WHERE BID = @BID and ID = @MaterialCategoryID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
GO
/****** Object:  StoredProcedure [dbo].[Part.Material.Category.Action.LinkMaterial]    Script Date: 4/3/2018 7:59:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Part.Material.Category.Action.LinkMaterial]
--
-- Description: This procedure links/unlinks the MaterialData to the MaterialCategory
--
-- Sample Use:   EXEC dbo.[Part.Material.Category.Action.LinkMaterial] @BID=1, @MaterialCategoryID=1, @MaterialDataID=1, @IsLinked=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Material.Category.Action.LinkMaterial]
--DECLARE 
          @BID                  TINYINT  --= 1
        , @MaterialCategoryID    SMALLINT --= 2
		, @MaterialDataID        SMALLINT --= 1
        , @IsLinked             BIT     = 1
        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the MaterialCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Material.Category] WHERE BID = @BID and ID = @MaterialCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MaterialCategory Specified. MaterialCategoryID='+CONVERT(VARCHAR(12),@MaterialCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the MaterialData specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Material.Data] WHERE BID = @BID and ID = @MaterialDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MaterialData Specified. MaterialDataID='+CONVERT(VARCHAR(12),@MaterialDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Part.Material.CategoryLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Part.Material.CategoryLink] WHERE BID = @BID and CategoryID = @MaterialCategoryID and PartID = @MaterialDataID)
		BEGIN
			INSERT INTO [Part.Material.CategoryLink] (BID, CategoryID, PartID)
			VALUES (@BID, @MaterialCategoryID, @MaterialDataID)
		END;

	END
	ELSE
	BEGIN
		-- Remove entry from Part.Material.CategoryLink
		DELETE FROM [Part.Material.CategoryLink] WHERE BID = @BID and CategoryID = @MaterialCategoryID and PartID = @MaterialDataID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Material.Category.Action.LinkMaterial]");
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Material.Category.Action.SetActive]");
            migrationBuilder.Sql(@"DROP PROCEDURE [dbo].[Part.Material.Category.Action.CanDelete]");
        }
    }
}

