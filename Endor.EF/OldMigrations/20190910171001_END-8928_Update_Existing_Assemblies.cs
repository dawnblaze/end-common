using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8928_Update_Existing_Assemblies : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Part.Assembly.Variable.Formula] 
SET [FormulaText] = 'QuantityPerItem' 
WHERE [FormulaText] = 'Quantity';

UPDATE [Part.Subassembly.Variable]
SET [IsFormula] = 1, [DefaultValue] = '=QuantityPerItem*LineItemQuantity'
WHERE [Name] = 'Quantity'
            ");

            migrationBuilder.Sql(@"
DECLARE @NewVariables TABLE(
	  [BID] SMALLINT
	, [ID] INT
	, [DataType] SMALLINT
	, [DefaultValue] NVARCHAR(MAX)
	, [IsRequired] BIT
	, [IsFormula] BIT
	, [Name] NVARCHAR(255)
    , [SubassemblyID] INT
	, [AllowDecimals] BIT
	, [DecimalPlaces] TINYINT
	, [DisplayType] TINYINT
	, [ElementType] TINYINT
    , [Label] NVARCHAR(255)
	, [LabelType] TINYINT
	, [ListDataType] SMALLINT
	, [AllowCustomValue] BIT
	, [AllowMultiSelect] BIT
    , [IsDisabled] BIT
	, [IsConsumptionFormula] BIT
	, [RollupLinkedPriceAndCost] BIT
);


INSERT INTO @NewVariables(
	  [BID]
	, [DataType]
	, [DefaultValue]
	, [IsRequired]
	, [IsFormula]
	, [Name]
	, [SubAssemblyID]
	, [AllowDecimals]
	, [DecimalPlaces]
	, [DisplayType]
	, [ElementType]
	, [Label]
	, [LabelType]
	, [ListDataType]
	, [AllowCustomValue]
	, [AllowMultiSelect]
	, [IsDisabled]
	, [IsConsumptionFormula]
	, [RollupLinkedPriceAndCost]
)
SELECT
	  A.BID
	, 2 AS DataType
	,'1' AS DefaultValue
	, 0 AS IsRequired
	, 0 AS IsFormula
	, 'QuantityPerItem' AS Name
	, A.ID AS SubAssemblyID
	, 0 AS AllowDecimals
	, 0 AS DecimalPlaces
	, 0 AS DisplayType
	, 21 AS ElementType
	, 'Quantity Per Item' AS Label
	, NULL AS LabelType
	, NULL AS ListDataType
	, NULL AS AllowCustomValue
	, NULL AS AllowMultiSelect
	, 0 AS IsDisabled
	, 0 AS IsConsumptionFormula
	, 0 AS RollupLinkedPriceAndCost
FROM [Part.Subassembly.Data] A
WHERE BID > 0 AND NOT EXISTS (
	SELECT * FROM [Part.Subassembly.Variable] V 
	WHERE V.Name = 'QuantityPerItem' AND V.BID = A.BID AND V.SubassemblyID = A.ID
);


UPDATE T
SET ID = T.NewID 
FROM ( 
        SELECT V.ID, ISNULL(I.ID, 1000) + ROW_NUMBER() OVER (PARTITION BY V.BID ORDER BY V.SubassemblyID) AS NewID
        FROM @NewVariables V 
        LEFT JOIN ( SELECT BID, MAX(ID) ID FROM [Part.Subassembly.Variable] GROUP BY BID ) I ON I.BID = V.BID
    ) T

INSERT INTO [Part.Subassembly.Variable](  
      [BID]
    , [ID]
    , [DataType]
    , [DefaultValue]
    , [IsRequired]
    , [IsFormula]
    , [Name]
    , [SubAssemblyID]
    , [AllowDecimals]
    , [DecimalPlaces]
    , [DisplayType]
    , [ElementType]
    , [Label]
    , [LabelType]
    , [ListDataType]
    , [AllowCustomValue]
    , [AllowMultiSelect]
    , [IsDisabled]
    , [IsConsumptionFormula]
    , [RollupLinkedPriceAndCost]
)
SELECT *
FROM @NewVariables 



-- Update Next IDs

DECLARE @BID SMALLINT;
DECLARE @NextID INT;

DECLARE cur CURSOR LOCAL FOR
SELECT BID, MAX(ID)+1
FROM   @NewVariables
GROUP BY BID

OPEN cur

FETCH NEXT FROM cur INTO @BID, @NextID

WHILE @@FETCH_STATUS = 0
BEGIN
    EXEC [Util.ID.SetID] 
            @BID = @BID
          , @ClassTypeID = 12046
          , @NextID = @NextID
          , @AvoidLowering = 1

    FETCH NEXT FROM cur INTO @BID, @NextID
END

CLOSE cur
DEALLOCATE cur
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }

        public override bool ClearAllAssemblies()
        {
            return true;
        }
    }
}
