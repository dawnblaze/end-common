using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181024200118_CreateTagTables")]
    public partial class CreateTagTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "System.Color",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false),
                    LESSVariableName = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    RGB = table.Column<string>(maxLength: 8, nullable: false),
                    UseInTags = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Color", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "List.Tag",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AssociatedClassTypeID = table.Column<int>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "1900"),
                    ColorID = table.Column<short>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_List.Tag", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_List.Tag_System.Tag.Color",
                        column: x => x.ColorID,
                        principalTable: "System.Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Company.TagLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TagID = table.Column<short>(nullable: false),
                    CompanyID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company.TagLink", x => new { x.BID, x.TagID, x.CompanyID });
                    table.ForeignKey(
                        name: "FK_Company.TagLink_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Company.TagLink_List.Tag",
                        columns: x => new { x.BID, x.TagID },
                        principalTable: "List.Tag",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contact.TagLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TagID = table.Column<short>(nullable: false),
                    ContactID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact.TagLink", x => new { x.BID, x.TagID, x.ContactID });
                    table.ForeignKey(
                        name: "FK_Contact.TagLink_Contact.Data",
                        columns: x => new { x.BID, x.ContactID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Contact.TagLink_List.Tag",
                        columns: x => new { x.BID, x.TagID },
                        principalTable: "List.Tag",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "List.Tag.OtherLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TagID = table.Column<short>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false),
                    AppliesToID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_List.Tag.OtherLink", x => new { x.BID, x.TagID, x.AppliesToClassTypeID, x.AppliesToID });
                    table.ForeignKey(
                        name: "FK_List.Tag.OtherLink_List.Tag",
                        columns: x => new { x.BID, x.TagID },
                        principalTable: "List.Tag",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order.Destination.TagLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TagID = table.Column<short>(nullable: false),
                    OrderDestinationID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Destination.TagLink", x => new { x.BID, x.TagID, x.OrderDestinationID });
                    table.ForeignKey(
                        name: "FK_Order.Destination.TagLink_Order.Destination.Data",
                        columns: x => new { x.BID, x.OrderDestinationID },
                        principalTable: "Order.Destination.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Destination.TagLink_List.Tag",
                        columns: x => new { x.BID, x.TagID },
                        principalTable: "List.Tag",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order.Item.TagLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TagID = table.Column<short>(nullable: false),
                    OrderItemID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Item.TagLink", x => new { x.BID, x.TagID, x.OrderItemID });
                    table.ForeignKey(
                        name: "FK_Order.Item.TagLink_Order.Item.Data",
                        columns: x => new { x.BID, x.OrderItemID },
                        principalTable: "Order.Item.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Item.TagLink_List.Tag",
                        columns: x => new { x.BID, x.TagID },
                        principalTable: "List.Tag",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order.TagLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    TagID = table.Column<short>(nullable: false),
                    OrderID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.TagLink", x => new { x.BID, x.TagID, x.OrderID });
                    table.ForeignKey(
                        name: "FK_Order.TagLink_Order.Data",
                        columns: x => new { x.BID, x.OrderID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.TagLink_List.Tag",
                        columns: x => new { x.BID, x.TagID },
                        principalTable: "List.Tag",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_List.Tag.OtherLink_AppliesTo",
                table: "List.Tag.OtherLink",
                columns: new[] { "BID", "AppliesToClassTypeID", "AppliesToID", "TagID" });

            migrationBuilder.CreateIndex(
                name: "IX_List.Tag.OtherLink",
                table: "List.Tag.OtherLink",
                columns: new[] { "BID", "TagID", "AppliesToClassTypeID", "AppliesToID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Destination.TagsLink_Order",
                table: "Order.Destination.TagLink",
                columns: new[] { "BID", "OrderDestinationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Destination.TagsLink",
                table: "Order.Destination.TagLink",
                columns: new[] { "BID", "TagID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.TagsLink_Order",
                table: "Order.Item.TagLink",
                columns: new[] { "BID", "OrderItemID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.TagsLink",
                table: "Order.Item.TagLink",
                columns: new[] { "BID", "TagID" });

            migrationBuilder.Sql(@"
INSERT [System.Color] ([ID], [Name], [RGB], [UseInTags], [LESSVariableName])
VALUES (1, N'pink', N'F47382', 1, N'label-color-pink')
    , (2, N'red', N'D72727', 1, N'label-color-red')
    , (3, N'peach', N'FF936C', 1, N'label-color-peach')
    , (4, N'orange', N'FF6B35', 1, N'label-color-orange')
    , (5, N'goldenrod', N'FFD46C', 1, N'label-color-goldenrod')
    , (6, N'yellow', N'FFBD1A', 1, N'label-color-yellow')
    , (7, N'lime', N'7EE516', 1, N'label-color-lime')
    , (8, N'green', N'75CD69', 1, N'label-color-green')
    , (9, N'olive', N'0E7C2C', 1, N'label-color-olive')
    , (10, N'springgreen', N'0BE58A', 1, N'label-color-springgreen')
    , (11, N'turquoise', N'6ACDE8', 1, N'label-color-turquoise')
    , (12, N'cerulean', N'13B9CF', 1, N'label-color-cerulean')
    , (13, N'steelblue', N'6593CF', 1, N'label-color-steelblue')
    , (14, N'blue', N'0D3EB5', 1, N'label-color-blue')
    , (15, N'lavender', N'9B71F4', 1, N'label-color-lavender')
    , (16, N'purple', N'6320EE', 1, N'label-color-purple')
    , (17, N'violet', N'EE20E1', 1, N'label-color-violet')
    , (18, N'brown', N'6C4421', 1, N'label-color-brown')
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Company.TagLink");

            migrationBuilder.DropTable(
                name: "Contact.TagLink");

            migrationBuilder.DropTable(
                name: "List.Tag.OtherLink");

            migrationBuilder.DropTable(
                name: "Order.Destination.TagLink");

            migrationBuilder.DropTable(
                name: "Order.Item.TagLink");

            migrationBuilder.DropTable(
                name: "Order.TagLink");

            migrationBuilder.DropTable(
                name: "List.Tag");

            migrationBuilder.DropTable(
                name: "System.Color");
        }
    }
}

