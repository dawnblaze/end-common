﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class CompanyContactLinkTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Contact.Role",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Contact.Role", x => x.ID);
                });

            migrationBuilder.Sql(@"
-- ===========================================================
-- Fill the Enum Contact.Role
-- ===========================================================
INSERT [enum.Contact.Role] ([ID], [Name])
VALUES (0, N'Inactive')
    , (1, N'Primary')
    , (2, N'Billing')
    , (3, N'Primary and Billing')
    , (4, N'Owner')
    , (5, N'Owner and Primary')
    , (6, N'Owner and Billing')
    , (7, N'Owner and Primary and Billing')
    , (128, N'Observer')
;");

            migrationBuilder.CreateTable(
                name: "Company.Contact.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    CompanyID = table.Column<int>(nullable: false),
                    ContactID = table.Column<int>(nullable: false),
                    Roles = table.Column<byte>(nullable: false, defaultValue: (byte)0),
                    IsActive = table.Column<bool>(nullable: true, computedColumnSql: "(CONVERT([bit],[Roles]))"),
                    IsPrimary = table.Column<bool>(nullable: true, computedColumnSql: "(CONVERT([bit],[Roles]&(1)))"),
                    IsBilling = table.Column<bool>(nullable: true, computedColumnSql: "(CONVERT([bit],[Roles]&(2)))"),
                    Position = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company.Contact.Link", x => new { x.BID, x.CompanyID, x.ContactID });
                    table.ForeignKey(
                        name: "FK_Company.Contact.Link_enum.Contact.Role",
                        column: x => x.Roles,
                        principalTable: "enum.Contact.Role",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Company.Contact.Link_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Company.Contact.Link_Contact.Data",
                        columns: x => new { x.BID, x.ContactID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Company.Contact.Link_Roles",
                table: "Company.Contact.Link",
                column: "Roles");

            migrationBuilder.CreateIndex(
                name: "IX_Company.Contact.Link_Contact",
                table: "Company.Contact.Link",
                columns: new[] { "BID", "ContactID", "CompanyID", "Roles" });
            
            migrationBuilder.Sql(@"
-- ===========================================================
-- Fill the Company.Contact.Link
-- ===========================================================
INSERT INTO [Company.Contact.Link]
    SELECT Company.BID
        , Company.ID as CompanyID
        , Contact.ID as ContactID
        , (Case WHEN Contact.IsActive = 0 THEN 0
                WHEN Contact.IsDefault = 0 AND Contact.IsBilling = 0 THEN 128
                ELSE Contact.IsDefault * 1 + Contact.IsBilling * 2 
                END) AS Roles
        , Contact.Position
    FROM [Company.Data] Company
    JOIN [Contact.Data] Contact on Company.BID = Contact.BID AND Company.ID = Contact.CompanyID
    ORDER BY BID, CompanyID, Contact.IsDefault, Contact.IsBilling, ContactID
    ;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Company.Contact.Link");

            migrationBuilder.DropTable(
                name: "enum.Contact.Role");
        }
    }
}
