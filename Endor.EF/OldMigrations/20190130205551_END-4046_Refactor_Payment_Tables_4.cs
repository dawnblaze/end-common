using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4046_Refactor_Payment_Tables_4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_Payment.Data",
                table: "Accounting.GLData");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Payment.Master_Activity.GLActivity",
                table: "Accounting.Payment.Master");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Payment.Master_BID_GLActivityID",
                table: "Accounting.Payment.Master");

            migrationBuilder.DropColumn(
                name: "GLActivityID",
                table: "Accounting.Payment.Master");

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_Payment.Application",
                table: "Accounting.GLData",
                columns: new[] { "BID", "PaymentId" },
                principalTable: "Accounting.Payment.Application",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_Payment.Application",
                table: "Accounting.GLData");

            migrationBuilder.AddColumn<int>(
                name: "GLActivityID",
                table: "Accounting.Payment.Master",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Master_BID_GLActivityID",
                table: "Accounting.Payment.Master",
                columns: new[] { "BID", "GLActivityID" });

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_Payment.Data",
                table: "Accounting.GLData",
                columns: new[] { "BID", "PaymentId" },
                principalTable: "Accounting.Payment.Master",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Payment.Master_Activity.GLActivity",
                table: "Accounting.Payment.Master",
                columns: new[] { "BID", "GLActivityID" },
                principalTable: "Activity.GLActivity",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
