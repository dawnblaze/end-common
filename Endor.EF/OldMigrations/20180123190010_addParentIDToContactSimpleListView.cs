using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180123190010_addParentIDToContactSimpleListView")]
    public partial class addParentIDToContactSimpleListView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.views WHERE name = 'Contact.SimpleList')
	DROP VIEW [dbo].[Contact.SimpleList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Contact.SimpleList]
AS
SELECT        BID, ID, ClassTypeID, ShortName AS DisplayName, IsActive, HasImage, CONVERT(BIT, 0) AS IsDefault, CompanyID as ParentID
FROM            dbo.[Contact.Data]
GO
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.views WHERE name = 'Contact.SimpleList')
	DROP VIEW [dbo].[Contact.SimpleList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Contact.SimpleList]
AS
SELECT        BID, ID, ClassTypeID, ShortName AS DisplayName, IsActive, HasImage, CONVERT(BIT, 0) AS IsDefault
FROM            dbo.[Contact.Data]
GO
");
        }
    }
}

