using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_Custom_enumPartSubassemblyPricingType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT [enum.Part.Subassembly.PricingType] ([ID], [Name])
                VALUES (3, N'Custom');
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[enum.Part.Subassembly.PricingType]
                WHERE ID = 3;
            ");
        }
    }
}
