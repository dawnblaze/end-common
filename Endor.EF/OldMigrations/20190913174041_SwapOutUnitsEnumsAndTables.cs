using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class SwapOutUnitsEnumsAndTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Item.Component_enum.Part.Unit",
                table: "Order.Item.Component");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_ColumnUnit",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Assembly.Table_RowUnit",
                table: "Part.Assembly.Table");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit",
                table: "Part.Material.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit1",
                table: "Part.Material.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit3",
                table: "Part.Material.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit4",
                table: "Part.Material.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit2",
                table: "Part.Material.Data");

            migrationBuilder.DropTable(
                name: "enum.Part.Unit");

            migrationBuilder.DropTable(
                name: "enum.Part.Unit.Classification");

            migrationBuilder.DropTable(
                name: "enum.Part.Unit.System");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_ColumnUnitID",
                table: "Part.Assembly.Table");

            migrationBuilder.DropIndex(
                name: "IX_Part.Assembly.Table_RowUnitID",
                table: "Part.Assembly.Table");

            migrationBuilder.Sql(@"
CREATE TABLE [enum.Unit](
    [ID] [tinyint] NOT NULL,
    [Name] [varchar](100) NOT NULL,
CONSTRAINT [PK_enum.Unit] PRIMARY KEY ([ID])
);");
            foreach (var unit in (Units.Unit[])Enum.GetValues(typeof(Units.Unit)))
            {
                migrationBuilder.Sql($@"
INSERT [enum.Unit] ([ID], [Name])
VALUES ({(int)unit}, N'{unit}');
");
            }

                migrationBuilder.Sql(@"
CREATE TABLE [enum.Unit.Type](
    [ID] [tinyint] NOT NULL,
    [Name] [varchar](100) NOT NULL,
CONSTRAINT [PK_enum.Unit.Type] PRIMARY KEY ([ID])
);");
            foreach (var unitType in (Units.UnitType[])Enum.GetValues(typeof(Units.UnitType)))
            {
                migrationBuilder.Sql($@"
INSERT [enum.Unit.Type] ([ID], [Name])
VALUES ({(int)unitType}, N'{unitType.ToString().Replace('_', ' ')}');
");
            }

            migrationBuilder.Sql(@"
CREATE TABLE [enum.Unit.System](
    [ID] [tinyint] NOT NULL,
    [Name] [varchar](100) NOT NULL,
CONSTRAINT [PK_enum.Unit.System] PRIMARY KEY ([ID])
);
 
INSERT [enum.Unit.System] ([ID], [Name])
VALUES (1, N'Metric')
    , (2, N'Imperial')
    , (3, N'Metric and Imperial')
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP TABLE [enum.Unit.SystemOfUnits];
DROP TABLE [enum.Unit.Type];
DROP TABLE [enum.Unit];
");

            migrationBuilder.CreateTable(
                name: "enum.Part.Unit.Classification",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Unit.Classification", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Part.Unit.System",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Unit.System", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Part.Unit",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Abbreviation = table.Column<string>(unicode: false, maxLength: 15, nullable: false),
                    Dimensionality = table.Column<byte>(type: "tinyint", nullable: false, defaultValueSql: "((1))"),
                    FromStandardMultiplyBy = table.Column<decimal>(nullable: false, computedColumnSql: "(1.0)/[ToStandardMultiplyBy]"),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    ToStandardMultiplyBy = table.Column<decimal>(type: "decimal(25, 18)", nullable: false),
                    UnitClassification = table.Column<byte>(type: "tinyint", nullable: false),
                    UnitClassificationText = table.Column<string>(nullable: true, computedColumnSql: "case [UnitClassification] when (0) then 'None' when (1) then 'Discrete' when (2) then 'Dimension' when (3) then 'Weight' when (4) then 'Time' else 'Unknown' end"),
                    UnitSystem = table.Column<byte>(type: "tinyint", nullable: false),
                    UnitSystemText = table.Column<string>(nullable: true, computedColumnSql: "case [UnitSystem] when (0) then 'Default' when (1) then 'Imperial' when (2) then 'Metric' when (3) then 'Metric and Imperial' else 'Unknown' end")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Unit", x => x.ID);
                    table.ForeignKey(
                        name: "FK_enum.Part.Unit_enum.Part.Unit.Classification_UnitClassification",
                        column: x => x.UnitClassification,
                        principalTable: "enum.Part.Unit.Classification",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_enum.Part.Unit_enum.Part.Unit.System_UnitSystem",
                        column: x => x.UnitSystem,
                        principalTable: "enum.Part.Unit.System",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Variable_UnitID",
                table: "Part.Subassembly.Variable",
                column: "UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_ConsumptionUnit",
                table: "Part.Material.Data",
                column: "ConsumptionUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_HeightUnit",
                table: "Part.Material.Data",
                column: "HeightUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_LengthUnit",
                table: "Part.Material.Data",
                column: "LengthUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_WeightUnit",
                table: "Part.Material.Data",
                column: "WeightUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_WidthUnit",
                table: "Part.Material.Data",
                column: "WidthUnit");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_ColumnUnitID",
                table: "Part.Assembly.Table",
                column: "ColumnUnitID");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_RowUnitID",
                table: "Part.Assembly.Table",
                column: "RowUnitID");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Component_Quantity.UnitID",
                table: "Order.Item.Component",
                column: "Quantity.UnitID");

            migrationBuilder.CreateIndex(
                name: "IX_enum.Part.Unit_UnitClassification",
                table: "enum.Part.Unit",
                column: "UnitClassification");

            migrationBuilder.CreateIndex(
                name: "IX_enum.Part.Unit_UnitSystem",
                table: "enum.Part.Unit",
                column: "UnitSystem");

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Component_enum.Part.Unit",
                table: "Order.Item.Component",
                column: "Quantity.UnitID",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_ColumnUnit",
                table: "Part.Assembly.Table",
                column: "ColumnUnitID",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Assembly.Table_RowUnit",
                table: "Part.Assembly.Table",
                column: "RowUnitID",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit",
                table: "Part.Material.Data",
                column: "ConsumptionUnit",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit1",
                table: "Part.Material.Data",
                column: "HeightUnit",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit3",
                table: "Part.Material.Data",
                column: "LengthUnit",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit4",
                table: "Part.Material.Data",
                column: "WeightUnit",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit2",
                table: "Part.Material.Data",
                column: "WidthUnit",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Variable_enum.Unit",
                table: "Part.Subassembly.Variable",
                column: "UnitID",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
