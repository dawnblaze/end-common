using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8466_AddEmployeeRoleStagedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "URL.Registration.Data",
                nullable: false,
                computedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                AND [UseCount] < coalesce([MaxUseCount], (1))
                                    AND ([UseCount] = (0)
                                            OR[ExpireAfterUseDays] IS NULL
                                            OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                then CONVERT([bit], (1))
                                else CONVERT([bit],(0)) end,(0)))",
                oldClrType: typeof(bool),
                oldComputedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                    AND [UseCount] < coalesce([MaxUseCount], (1))
                                        AND ([UseCount] = (0)
                                                OR[ExpireAfterUseDays] IS NULL
                                                OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                    then CONVERT([bit], (1))
                                    else CONVERT([bit],(0)) end,(0)))");

            migrationBuilder.Sql(@"DECLARE @StagedEmployeeRoles TABLE ( [ID] SMALLINT
	, AllowMultiple BIT
	, AllowOnTeam BIT
	, EstimateDestinationRestriction TINYINT
	, EstimateItemRestriction TINYINT
	, EstimateRestriction TINYINT
	, IsActive BIT
	, IsSystem BIT
	, ModifiedDT DATETIME2(2) DEFAULT GetUTCDate()
	, [Name] NVARCHAR(200)
	, OpportunityRestriction TINYINT
	, OrderDestinationRestriction TINYINT
	, OrderItemRestriction TINYINT
	, OrderRestriction TINYINT
	, PORestriction TINYINT)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (1, 1, 1, 0, 0, 2, 1, 1, 'Salesperson', 2, 0, 0, 2, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (2, 0, 1, 0, 0, 1, 1, 0, 'CSR', 1, 0, 0, 1, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (3, 0, 1, 0, 1, 1, 1, 0, 'Designer', 1, 0, 1, 1, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (4, 0, 1, 0, 0, 1, 1, 0, 'Project Manager', 1, 0, 0, 1, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (5, 0, 1, 0, 0, 1, 1, 0, 'Account Manager', 1, 0, 0, 1, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (6, 1, 0, 0, 1, 1, 1, 0, 'Production', 0, 0, 1, 1, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (41, 0, 0, 0, 0, 0, 1, 0, 'Requestor', 0, 0, 0, 0, 2)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (42, 0, 0, 0, 0, 0, 1, 0, 'Approver', 0, 0, 0, 0, 1)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (43, 0, 0, 0, 0, 0, 1, 0, 'Orderer', 0, 0, 0, 0, 1)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (61, 0, 0, 1, 0, 0, 1, 0, 'Shipper', 0, 1, 0, 0, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (62, 1, 1, 1, 0, 1, 1, 0, 'Installer', 0, 1, 0, 1, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (63, 0, 0, 1, 0, 0, 1, 0, 'Delivery Person', 0, 1, 0, 0, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (64, 0, 1, 1, 0, 1, 1, 0, 'Service Person', 0, 1, 0, 1, 0)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (253, 1, 0, 1, 1, 1, 1, 1, 'Assigned To', 1, 1, 1, 1, 1)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (254, 0, 0, 0, 0, 2, 1, 1, 'Entered By', 2, 0, 0, 2, 2)

INSERT INTO @StagedEmployeeRoles (ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	VALUES (255, 1, 1, 1, 1, 1, 1, 1, 'Observer', 1, 1, 1, 1, 1)

INSERT INTO [Employee.Role] (BID
	, ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction)
	SELECT -1
	, ID
	, AllowMultiple
	, AllowOnTeam
	, EstimateDestinationRestriction
	, EstimateItemRestriction
	, EstimateRestriction
	, IsActive
	, IsSystem
	, [Name]
	, OpportunityRestriction
	, OrderDestinationRestriction
	, OrderItemRestriction
	, OrderRestriction
	, PORestriction FROM @StagedEmployeeRoles
WHERE ID NOT IN (SELECT ID FROM @StagedEmployeeRoles)

EXEC [Util.Table.CopyDefaultRecords] 'Employee.Role', 1, 1");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "URL.Registration.Data",
                nullable: false,
                computedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                    AND [UseCount] < coalesce([MaxUseCount], (1))
                                        AND ([UseCount] = (0)
                                                OR[ExpireAfterUseDays] IS NULL
                                                OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                    then CONVERT([bit], (1))
                                    else CONVERT([bit],(0)) end,(0)))",
                oldClrType: typeof(bool),
                oldComputedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                AND [UseCount] < coalesce([MaxUseCount], (1))
                                    AND ([UseCount] = (0)
                                            OR[ExpireAfterUseDays] IS NULL
                                            OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                then CONVERT([bit], (1))
                                else CONVERT([bit],(0)) end,(0)))");

            migrationBuilder.Sql(@"DELETE FROM [Employee.Role] WHERE BID = -1");
        }
    }
}
