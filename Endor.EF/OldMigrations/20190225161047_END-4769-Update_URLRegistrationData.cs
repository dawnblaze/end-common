using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4769Update_URLRegistrationData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
Delete [URL.Registration.Lookup.History];
Delete  [URL.Registration.Data];
Delete [enum.URL.RegistrationType]
;
        ");

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[enum.URL.RegistrationType]([ID], [Name])
VALUES (0, N'Tracking'),
    (1, N'Redirect'),
    (2, N'RedirectWithToken'),
    (3, N'RedirectAsEndorEmployee'),
    (4, N'RedirectAsEndorContact'),
    (5, N'RedirectAsEcommContact'),
    (10, N'BlobRead'),
    (11, N'BlobReadWrite'),
    (20, N'ArtworkApproval'),
    (30, N'OnlinePayment'),
    (31, N'OnlineInvoice')
;
        ");

            migrationBuilder.DropForeignKey(
                name: "FK_URL.Registration.Data_Company.Data",
                table: "URL.Registration.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_URL.Registration.Data_Contact.Data",
                table: "URL.Registration.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_URL.Registration.Data_Employee.Data",
                table: "URL.Registration.Data");

            migrationBuilder.DropColumn(
                name: "TargetURLHeaders",
                table: "URL.Registration.Data");

            migrationBuilder.RenameColumn(
                name: "EmployeeID",
                table: "[URL.Registration.Data]",
                newName: "TargetEmployeeID");

            migrationBuilder.RenameColumn(
                name: "ContactID",
                table: "[URL.Registration.Data]",
                newName: "TargetContactID");

            migrationBuilder.RenameColumn(
                name: "CompanyID",
                table: "[URL.Registration.Data]",
                newName: "TargetCompanyID");

            migrationBuilder.DropIndex(
                name: "IX_URL.Registration.Data_Company",
                table: "URL.Registration.Data");

            migrationBuilder.DropIndex(
                name: "IX_URL.Registration.Data_Contact",
                table: "URL.Registration.Data");

            migrationBuilder.DropIndex(
                 name: "IX_URL.Registration.Data_Employee",
                 table: "URL.Registration.Data");

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_Company",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "TargetCompanyID" },
                filter: "([TargetCompanyID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_Contact",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "TargetContactID" },
                filter: "([TargetContactID] IS NOT NULL)");

            migrationBuilder.CreateIndex(
                name: "IX_URL.Registration.Data_Employee",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "TargetEmployeeID" },
                filter: "([TargetEmployeeID] IS NOT NULL)");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TargetEmployeeID",
                table: "URL.Registration.Data",
                newName: "EmployeeID");

            migrationBuilder.RenameColumn(
                name: "TargetContactID",
                table: "URL.Registration.Data",
                newName: "ContactID");

            migrationBuilder.RenameColumn(
                name: "TargetCompanyID",
                table: "URL.Registration.Data",
                newName: "CompanyID");

            migrationBuilder.AddColumn<string>(
                name: "TargetURLHeaders",
                table: "URL.Registration.Data",
                type: "varchar(max)",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_URL.Registration.Data_Company.Data",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "CompanyID" },
                principalTable: "Company.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_URL.Registration.Data_Contact.Data",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "ContactID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_URL.Registration.Data_Employee.Data",
                table: "URL.Registration.Data",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
