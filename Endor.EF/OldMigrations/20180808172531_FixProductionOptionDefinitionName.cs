using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180808172531_FixProductionOptionDefinitionName")]
    public partial class FixProductionOptionDefinitionName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Definition]
SET [Name] = 'License.Module.Production.Enabled'
WHERE ID = 20005;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Definition]
SET [Name] = 'License.Module.Prodution.Enabled'
WHERE ID = 20005;
");
        }
    }
}

