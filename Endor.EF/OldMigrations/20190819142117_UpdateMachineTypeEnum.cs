using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateMachineTypeEnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [enum.Machine.LayoutType] SET [Name] = 'No Layout Manager' WHERE [ID] = 0;
INSERT INTO [enum.Machine.LayoutType] ([ID], [Name]) VALUES (4, 'Other');
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [enum.Machine.LayoutType] SET [Name] = 'Other' WHERE [ID] = 0;
DELETE FROM [enum.Machine.LayoutType] WHERE [ID] = 4;
");
        }
    }
}
