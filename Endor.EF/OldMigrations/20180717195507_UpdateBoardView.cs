using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180717195507_UpdateBoardView")]
    public partial class UpdateBoardView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Board.View.SimpleList]
                GO
                CREATE VIEW [dbo].[Board.View.SimpleList]
                    AS
                SELECT	[BID]
                        , [ID]
                        , [ClassTypeID]
                        , [Name] as DisplayName
                        , [IsActive]
                        , CONVERT(BIT, 0) AS [IsDefault]
                        , DataType
                FROM [Board.View];
                GO
            ");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Board.View.Action.Link]
GO
CREATE PROCEDURE [dbo].[Board.View.Action.Link]
-- DECLARE 
  @BID TINYINT 
, @BoardID SMALLINT 
, @BoardViewID SMALLINT 
, @SortIndex TINYINT = NULL
AS
BEGIN
    IF NOT EXISTS(SELECT 1 FROM [Board.Definition.Data] WHERE BID =  @BID AND ID = @BoardID)
        THROW 50000, 'Board not found', 1

    IF NOT EXISTS(SELECT 1 FROM [Board.View] WHERE BID = @BID AND ID = @BoardViewID)
        THROW 50000, 'Board view not found', 1

    IF EXISTS(SELECT 1 FROM [Board.View.Link] WHERE BID = @BID AND ViewID = @BoardViewID AND BoardID = @BoardID)
        THROW 50000, 'Board link already exist', 1

    IF @SortIndex IS NULL
        SELECT @SortIndex = MAX(SortIndex)+1 FROM [Board.View.Link] WHERE BID = @BID AND ViewID = @BoardViewID;

    IF @SortIndex IS NULL
        SET @SortIndex = 1;

    UPDATE [Board.View.Link]
    SET SortIndex += 1
    WHERE BID = @BID AND ViewID = @BoardViewID AND SortIndex >= @SortIndex
    ;

    INSERT INTO [Board.View.Link]
    (BID, BoardID, ViewID, SortIndex)
    VALUES
    (@BID, @BoardID, @BoardViewID, @SortIndex)
END
");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Board.View.Action.Unlink]
GO
CREATE PROCEDURE [dbo].[Board.View.Action.Unlink]
-- DECLARE 
  @BID TINYINT 
, @BoardID SMALLINT 
, @BoardViewID SMALLINT 
AS
BEGIN
    IF NOT EXISTS(SELECT 1 FROM [Board.Definition.Data] WHERE BID =  @BID AND ID = @BoardID)
        THROW 50000, 'Board not found', 1

    IF NOT EXISTS(SELECT 1 FROM [Board.View] WHERE BID = @BID AND ID = @BoardViewID)
        THROW 50000, 'Board view not found', 1

    IF NOT EXISTS(SELECT 1 FROM [Board.View.Link] WHERE BID = @BID AND ViewID = @BoardViewID AND BoardID = @BoardID)
        THROW 50000, 'Board link not found', 1

    DELETE FROM [Board.View.Link]
    WHERE BID = @BID AND ViewID = @BoardViewID AND BoardID = @BoardID
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Board.View.SimpleList]
            ");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Board.View.Action.Link]
            ");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Board.View.Action.Unlink]
            ");
        }
    }
}

