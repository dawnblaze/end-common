﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class UpdateGLDataIndexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_ActivityID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_AssemblyID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_CompanyID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_CurrencyType] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_DestinationID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_GLAccountID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_ItemComponentID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_LaborID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_LocationID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_MachineID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_MaterialID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_OrderID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_OrderItemID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_PaymentID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_PaymentMethodID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_TaxGroupID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_TaxItemID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_ItemSurchargeID] on [Accounting.GL.Data]
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_TaxCodeID] on [Accounting.GL.Data]
");
            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_BID_ActivityID",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "ActivityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_Company",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "CompanyID", "AccountingDT" },
                filter: "CompanyID IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_Order",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "OrderID", "AccountingDT" },
                filter: "OrderID IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_AccountingDT",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "AccountingDT", "GLAccountID", "Amount" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GL.Data_GLAccount",
                table: "Accounting.GL.Data",
                columns: new[] { "BID", "GLAccountID", "AccountingDT", "Amount" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_Company",
                table: "Accounting.GL.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_Order",
                table: "Accounting.GL.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_AccountingDT",
                table: "Accounting.GL.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_GLAccount",
                table: "Accounting.GL.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GL.Data_BID_ActivityID",
                table: "Accounting.GL.Data"
            );

            migrationBuilder.Sql(@"
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_ActivityID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_ActivityID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_AssemblyID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_AssemblyID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[AssemblyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_CompanyID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_CompanyID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[CompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_CurrencyType] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_CurrencyType] ON [dbo].[Accounting.GL.Data]
(
	[CurrencyType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_DestinationID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_DestinationID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[DestinationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_GLAccountID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_GLAccountID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[GLAccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_ItemComponentID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_ItemComponentID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[ItemComponentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_LaborID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_LaborID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[LaborID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_LocationID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_LocationID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_MachineID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_MachineID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[MachineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_MaterialID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_MaterialID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[MaterialID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_OrderID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_OrderID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_OrderItemID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_OrderItemID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[OrderItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_PaymentID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_PaymentID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[PaymentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_PaymentMethodID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_PaymentMethodID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[PaymentMethodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_TaxGroupID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_TaxGroupID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[TaxGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_BID_TaxItemID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_BID_TaxItemID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[TaxItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_ItemSurchargeID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_ItemSurchargeID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[ItemSurchargeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DROP INDEX IF EXISTS [IX_Accounting.GL.Data_TaxCodeID] ON [dbo].[Accounting.GL.Data]
CREATE INDEX [IX_Accounting.GL.Data_TaxCodeID] ON [dbo].[Accounting.GL.Data]
(
	[BID] ASC,
	[TaxCodeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
");
        }
    }
}
