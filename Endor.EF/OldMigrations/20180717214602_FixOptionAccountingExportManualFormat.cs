using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180717214602_FixOptionAccountingExportManualFormat")]
    public partial class FixOptionAccountingExportManualFormat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Definition] SET DataType = 2 WHERE NAME = 'Accounting.Export.Manual.Format'
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Definition] SET DataType = 1 WHERE NAME = 'Accounting.Export.Manual.Format'
");
        }
    }
}

