using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8163_PaymentTermDefaultSupport_Rework : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF EXISTS (SELECT * FROM [Accounting.Payment.Term] WHERE BID = -1 AND [Name] = 'Cash')
BEGIN
	DELETE FROM [Accounting.Payment.Term] WHERE BID = -1 AND [Name] = 'Cash';
	INSERT [Accounting.Payment.Term] 
	([BID], [ID], [IsActive], [Name], [EarlyPaymentDays], [DaysDue], [DownPaymentPercent], [DownPaymentPercentBelow], [DownPaymentThreshold], [EarlyPaymentPercent], [PaymentDueBasedOnType]) 
	VALUES (-1, 1, 1, N'Cash', 0, 0, 50.0000, NULL, 0.0000, NULL, 0);
END
ELSE
BEGIN
	INSERT [Accounting.Payment.Term] 
	([BID], [ID], [IsActive], [Name], [EarlyPaymentDays], [DaysDue], [DownPaymentPercent], [DownPaymentPercentBelow], [DownPaymentThreshold], [EarlyPaymentPercent], [PaymentDueBasedOnType]) 
	VALUES (-1, 1, 1, N'Cash', 0, 0, 50.0000, NULL, 0.0000, NULL, 0);
END");

            migrationBuilder.Sql(@"UPDATE [System.Option.Definition]
SET ID = 121, CategoryID = 12, DefaultValue = 1, DataType = 1, IsHidden = 0
WHERE ID = 40;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [Accounting.Payment.Term] WHERE BID = -1 AND [Name] = 'Cash'");
            migrationBuilder.Sql(@"UPDATE [System.Option.Definition]
SET ID = 40, CategoryID = 10100, DefaultValue = 1000, DataType = 2, IsHidden = 1
WHERE ID = 40;");
        }
    }
}
