using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180725144247_END-1953_Remove_IsPrimary_Column_From_Locators")]
    public partial class END1953_Remove_IsPrimary_Column_From_Locators : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPrimary",
                table: "Order.Contact.Locator");

            migrationBuilder.DropColumn(
                name: "IsPrimary",
                table: "Location.Locator");

            migrationBuilder.DropColumn(
                name: "IsPrimary",
                table: "Employee.Locator");

            migrationBuilder.DropColumn(
                name: "IsPrimary",
                table: "Contact.Locator");

            migrationBuilder.DropColumn(
                name: "IsPrimary",
                table: "Company.Locator");

            migrationBuilder.DropColumn(
                name: "IsPrimary",
                table: "Business.Locator");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Order.Contact.Locator",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Location.Locator",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Employee.Locator",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Contact.Locator",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Company.Locator",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Business.Locator",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");
        }
    }
}

