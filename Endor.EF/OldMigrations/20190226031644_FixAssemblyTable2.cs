using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixAssemblyTable2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    DECLARE @var0 sysname;
                    SELECT @var0 = [d].[name]
                    FROM [sys].[default_constraints] [d]
                    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
                    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Part.Assembly.Table]') AND [c].[name] = N'ModifiedDT');
                    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Part.Assembly.Table] DROP CONSTRAINT [' + @var0 + '];');
                    ALTER TABLE [Part.Assembly.Table] ALTER COLUMN [ModifiedDT] datetime2 NOT NULL;
                    ALTER TABLE [Part.Assembly.Table] ADD DEFAULT (GetUTCDate()) FOR [ModifiedDT];

                    DECLARE @var1 sysname;
                    SELECT @var1 = [d].[name]
                    FROM [sys].[default_constraints] [d]
                    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
                    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Part.Assembly.Table]') AND [c].[name] = N'ClassTypeID');
                    IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Part.Assembly.Table] DROP CONSTRAINT [' + @var1 + '];');
                    ALTER TABLE [Part.Assembly.Table] DROP COLUMN [ClassTypeID];
                    ALTER TABLE [Part.Assembly.Table] ADD [ClassTypeID] AS 12047;

                    INSERT INTO [dbo].[enum.ClassType]
                    (ID, [Name], [TableName])
                    VALUES
                    (12047, 'AssemblyTable', 'Part.Assembly.Table')
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    DELETE FROM [dbo].[enum.ClassType]
                    WHERE ID = 12047
                "
            );
        }
    }
}
