using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddPaymentData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsTaxable",
                table: "Accounting.GLData",
                nullable: true,
                oldClrType: typeof(bool),
                oldDefaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "PaymentId",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PaymentMethodId",
                table: "Accounting.GLData",
                type: "int SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "TaxItemID",
                table: "Accounting.GLData",
                type: "smallint SPARSE",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Accounting.Payment.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AccountingDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    ActivityID = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(type: "decimal(18, 4)", nullable: false, defaultValue: 0m),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((8010))"),
                    CompanyID = table.Column<int>(nullable: true),
                    CurrencyType = table.Column<byte>(nullable: false),
                    DisplayNumber = table.Column<string>(maxLength: 50, nullable: true),
                    EnteredByContactID = table.Column<int>(nullable: true),
                    EnteredByEmployeeID = table.Column<short>(nullable: true),
                    IsSubPayment = table.Column<bool>(nullable: true, computedColumnSql: "(case when [ParentID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)"),
                    IsVoided = table.Column<bool>(nullable: true, computedColumnSql: "(case when [VoidedPaymentID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsVoidedEntry = table.Column<bool>(nullable: true, computedColumnSql: "(case when [VoidedDT] IS NOT NULL AND [VoidedPaymentID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)"),
                    IsVoidingEntry = table.Column<bool>(nullable: true, computedColumnSql: "(case when [VoidedDT] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    LocationID = table.Column<byte>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Notes = table.Column<string>(maxLength: 100, nullable: true),
                    OrderID = table.Column<int>(nullable: true),
                    ParentID = table.Column<int>(nullable: false),
                    PaymentMethodId = table.Column<int>(nullable: true),
                    PaymentType = table.Column<byte>(nullable: false),
                    TokenID = table.Column<string>(type: "nvarchar(100)", maxLength: 64, nullable: true),
                    ValidToDT = table.Column<DateTime>(type: "datetime2(7)", nullable: false, defaultValueSql: "(CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'))"),
                    VoidedDT = table.Column<DateTime>(type: "datetime2(2) SPARSE", nullable: true),
                    VoidedPaymentID = table.Column<int>(type: "int SPARSE", nullable: true),
                    VoidedReason = table.Column<string>(type: "nvarchar(255) SPARSE", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting.Payment.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_enum.Accounting.CurrencyType",
                        column: x => x.CurrencyType,
                        principalTable: "enum.Accounting.CurrencyType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_enum.Accounting.PaymentMethodType",
                        column: x => x.PaymentType,
                        principalTable: "enum.Accounting.PaymentMethodType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Activity.GLActivity",
                        columns: x => new { x.BID, x.ActivityID },
                        principalTable: "Activity.GLActivity",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Contact.Data",
                        columns: x => new { x.BID, x.EnteredByContactID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Employee.Data",
                        columns: x => new { x.BID, x.EnteredByEmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Order.Data",
                        columns: x => new { x.BID, x.OrderID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Accounting.Payment.Data",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Accounting.Payment.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Accounting.Payment.Method",
                        columns: x => new { x.BID, x.PaymentMethodId },
                        principalTable: "Accounting.Payment.Method",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Accounting.Payment.Data_Accounting.Payment.Data1",
                        columns: x => new { x.BID, x.VoidedPaymentID },
                        principalTable: "Accounting.Payment.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_PaymentId",
                table: "Accounting.GLData",
                columns: new[] { "BID", "PaymentId" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_PaymentMethodId",
                table: "Accounting.GLData",
                columns: new[] { "BID", "PaymentMethodId" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_BID_TaxItemID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "TaxItemID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_CurrencyType",
                table: "Accounting.Payment.Data",
                column: "CurrencyType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_PaymentType",
                table: "Accounting.Payment.Data",
                column: "PaymentType");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_ActivityID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "ActivityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_CompanyID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "CompanyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_EnteredByContactID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "EnteredByContactID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_EnteredByEmployeeID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "EnteredByEmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_LocationID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_OrderID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "OrderID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_ParentID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_PaymentMethodId",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "PaymentMethodId" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Payment.Data_BID_VoidedPaymentID",
                table: "Accounting.Payment.Data",
                columns: new[] { "BID", "VoidedPaymentID" });

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_Payment.Data",
                table: "Accounting.GLData",
                columns: new[] { "BID", "PaymentId" },
                principalTable: "Accounting.Payment.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_PaymentMethod.Data",
                table: "Accounting.GLData",
                columns: new[] { "BID", "PaymentMethodId" },
                principalTable: "Accounting.Payment.Method",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GL.Data_TaxItem.Data",
                table: "Accounting.GLData",
                columns: new[] { "BID", "TaxItemID" },
                principalTable: "Accounting.Tax.Item",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_Payment.Data",
                table: "Accounting.GLData");

            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_PaymentMethod.Data",
                table: "Accounting.GLData");

            migrationBuilder.DropForeignKey(
                name: "FK_GL.Data_TaxItem.Data",
                table: "Accounting.GLData");

            migrationBuilder.DropTable(
                name: "Accounting.Payment.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GLData_BID_PaymentId",
                table: "Accounting.GLData");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GLData_BID_PaymentMethodId",
                table: "Accounting.GLData");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GLData_BID_TaxItemID",
                table: "Accounting.GLData");

            migrationBuilder.DropColumn(
                name: "PaymentId",
                table: "Accounting.GLData");

            migrationBuilder.DropColumn(
                name: "PaymentMethodId",
                table: "Accounting.GLData");

            migrationBuilder.DropColumn(
                name: "TaxItemID",
                table: "Accounting.GLData");

            migrationBuilder.AlterColumn<bool>(
                name: "IsTaxable",
                table: "Accounting.GLData",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldNullable: true);
        }
    }
}
