using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6862_Added_new_machine_profile_table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Machine.Profile.Table",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AssemblyTableBID = table.Column<short>(nullable: true),
                    AssemblyTableID = table.Column<short>(nullable: true),
                    CellDataJSON = table.Column<string>(type: "varchar(max)", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12034"),
                    ColumnCount = table.Column<short>(type: "smallint", nullable: false),
                    ColumnValuesJSON = table.Column<string>(type: "varchar(max)", nullable: true),
                    Label = table.Column<string>(type: "nvarchar(255)", nullable: false),
                    MachineProfileBID1 = table.Column<short>(nullable: true),
                    MachineProfileID1 = table.Column<int>(nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    OverrideDefault = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    ProfileID = table.Column<int>(nullable: false),
                    RowCount = table.Column<short>(nullable: false),
                    RowValuesJSON = table.Column<string>(type: "varchar(max)", nullable: true),
                    TableID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Machine.Profile.Table", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile.Table_Part.Assembly.Table_AssemblyTableBID_AssemblyTableID",
                        columns: x => new { x.AssemblyTableBID, x.AssemblyTableID },
                        principalTable: "Part.Assembly.Table",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile.Table_Part.Machine.Profile",
                        columns: x => new { x.BID, x.ProfileID },
                        principalTable: "Part.Machine.Profile",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile.Table_Part.Assembly.Table",
                        columns: x => new { x.BID, x.TableID },
                        principalTable: "Part.Assembly.Table",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile.Table_Part.Machine.Profile_MachineProfileBID1_MachineProfileID1",
                        columns: x => new { x.MachineProfileBID1, x.MachineProfileID1 },
                        principalTable: "Part.Machine.Profile",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Table_AssemblyTableBID_AssemblyTableID",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "AssemblyTableBID", "AssemblyTableID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Table_Table",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "BID", "TableID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Table_MachineProfileBID1_MachineProfileID1",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "MachineProfileBID1", "MachineProfileID1" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Table_Profile",
                table: "Part.Machine.Profile.Table",
                columns: new[] { "BID", "ProfileID", "Label" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Machine.Profile.Table");
        }
    }
}
