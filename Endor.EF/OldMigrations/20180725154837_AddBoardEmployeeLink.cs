using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180725154837_AddBoardEmployeeLink")]
    public partial class AddBoardEmployeeLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Board.Employee.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    BoardID = table.Column<short>(nullable: false),
                    EmployeeID = table.Column<short>(nullable: false),
                    IsFavorite = table.Column<bool>(nullable: true),
                    SortIndex = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Board.Employee.Link", x => new { x.BID, x.BoardID, x.EmployeeID });
                    table.ForeignKey(
                        name: "FK_Board.Employee.Link_Board.Data",
                        columns: x => new { x.BID, x.BoardID },
                        principalTable: "Board.Definition.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Board.Employee.Link_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Board.Employee.Link_Employee",
                table: "Board.Employee.Link",
                columns: new[] { "BID", "EmployeeID", "BoardID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Board.Employee.Link");
        }
    }
}

