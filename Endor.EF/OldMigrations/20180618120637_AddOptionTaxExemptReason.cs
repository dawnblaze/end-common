using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180618120637_AddOptionTaxExemptReason")]
    public partial class AddOptionTaxExemptReason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"  
    if not exists (SELECT ID from [System.Option.Category] where ID = 23 and name = 'Tax Exempt Reasons')
	INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
	VALUES (23, 'Tax Exempt Reasons', 1, 'Create and manage customer tax exempt reasons.', 4, 0)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
    DELETE FROM [System.Option.Category] where ID = 23 and name = 'Tax Exempt Reasons'
");
        }
    }
}

