using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_SPROC_Employee_GetNotificationInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [dbo].[Employee.GetNotificationInfo]
                GO

                /* =============================================
                Description:
                    This stored procedure is used to retrieve the Employee Notification information used
                    in the endpoint /api/employee/{id}/notificationinfo found on the following WIKI page
                    https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/715423783/Employee+Object+API+Endpoints

                    * UnreadMessageCount - the number of records in the employee's Message Inbox that are unread.
                    * UnreadAlertCount - the number of records in the employee's Alert Inbox that are unread.
                    * TimeclockStatus - the employees current time clock status, indicating if they are clocked in.  
                        If they are clocked on to a detail, the station and TimeCardDetail ID are also returned.

                Sample usage:

                ============================================= */

                CREATE PROCEDURE [dbo].[Employee.GetNotificationInfo]
 	                  @BID Smallint 
                    , @EmployeeID smallint 
                 AS
                BEGIN
	                SET NOCOUNT ON;

                    -- Return the following information
    
                    -- DECLARE @BID Smallint = 1
                    --       , @EmployeeID smallint = 10
                    --;

                        DECLARE 
                            --   @UnreadMessageCount INT
                            -- , @UnreadAlertCount INT
                            -- , @UnreadSystemNoticeCount INT
                            -- , 
                            @TimeCardID INT
                            , @IsLoggedIn BIT
                            , @TimeCardDetailID INT
                            , @OrderItemStatusID INT
                            , @TimeClockActivityID INT
                            , @TimeClockBreakID INT
                            , @StatusName NVARCHAR(255)
                            ;

                    -- WITH 
                    --     InboxCounts(Unread, InboxType) AS 
                    --     (
                    --         SELECT COUNT(*) as Unread, IB.InboxType
                    --         FROM [Message.Inbox.Item] IB
                    --         WHERE IB.BID = @BID AND IB.EmployeeID = @EmployeeID 
                    --         GROUP BY IB.InboxType
                    --     )
                    --     SELECT 
                    --           @UnreadMessageCount       = COALESCE(( SELECT Unread FROM InboxCounts WHERE InboxType = 1 ),0)
                    --         , @UnreadAlertCount         = COALESCE(( SELECT Unread FROM InboxCounts WHERE InboxType = 2 ),0)
                    --         , @UnreadSystemNoticeCount  = COALESCE(( SELECT Unread FROM InboxCounts WHERE InboxType = 3 ),0)

                    -- ;

                    SELECT TOP(1) @TimeCardID = ID
                    FROM [Employee.TimeCard] 
                    WHERE BID = @BID AND EmployeeID = @EmployeeID AND IsClosed = 0 
                    ORDER BY StartDT DESC
                    ;

                    -- Pull Time Card Details if available
                    IF (@TimeCardID IS NULL)
                        SET @IsLoggedIn = 0
                    ELSE
                    BEGIN
                        SET @IsLoggedIn = 1;
        
                        SELECT @TimeCardDetailID = ID
                             , @OrderItemStatusID = OrderItemStatusID
                             , @TimeClockActivityID = TimeClockActivityID
                             , @TimeClockBreakID = TimeClockBreakID
                        FROM [Employee.TimeCard.Detail]
                        WHERE BID = @BID AND TimeCardID = @TimeCardID AND IsClosed = 0 
                        ORDER BY StartDT DESC
                        ;

                        SET @StatusName = 
                                (CASE 
                                    WHEN @OrderItemStatusID IS NOT NULL 
                                    THEN (SELECT Name FROM [Order.Item.Status] WHERE BID = @BID AND ID = @OrderItemStatusID)
                                    WHEN @TimeClockActivityID IS NOT NULL 
                                    THEN (SELECT Name FROM [List.FlatList.Data] WHERE BID = @BID AND ID = @TimeClockActivityID)
                                    WHEN @TimeClockBreakID IS NOT NULL 
                                    THEN (SELECT Name FROM [List.FlatList.Data] WHERE BID = @BID AND ID = @TimeClockBreakID)
                                ELSE NULL
                                END);
                    END;

                    SELECT 
                        @BID as BID
                        , @EmployeeID as EmployeeID
                        -- , @UnreadMessageCount AS UnreadMessageCount 
                        -- , @UnreadAlertCount AS UnreadAlertCount 
                        -- , @UnreadSystemNoticeCount AS UnreadSystemNoticeCount 
                        , @TimeCardID AS TimeCardID 
                        , @IsLoggedIn AS IsLoggedIn 
                        , @TimeCardDetailID AS TimeCardDetailID 
                        , @OrderItemStatusID AS OrderItemStatusID 
                        , @TimeClockActivityID AS TimeClockActivityID 
                        , @TimeClockBreakID AS TimeClockBreakID 
                        , @StatusName AS StatusName


                END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP PROCEDURE IF EXISTS [dbo].[Employee.GetNotificationInfo]
            ");
        }
    }
}
