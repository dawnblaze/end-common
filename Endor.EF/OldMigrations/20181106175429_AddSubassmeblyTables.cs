using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddSubassmeblyTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Subassembly.Category",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12042"),
                    Description = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    ParentID = table.Column<short>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Subassembly.Category", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.Category_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.Category_Part.Subassembly.Category",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Part.Subassembly.Category",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Part.Subassembly.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12040"),
                    Description = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    HasImage = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IsActive = table.Column<bool>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    SKU = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Subassembly.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Part.Subassembly.CategoryLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    PartID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Subassembly.CategoryLink", x => new { x.BID, x.PartID, x.CategoryID });
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.CategoryLink_Part.Subassembly.Category",
                        columns: x => new { x.BID, x.CategoryID },
                        principalTable: "Part.Subassembly.Category",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.CategoryLink_Part.Subassembly.Data",
                        columns: x => new { x.BID, x.PartID },
                        principalTable: "Part.Subassembly.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Category_Name_IsActive",
                table: "Part.Subassembly.Category",
                columns: new[] { "BID", "Name", "IsActive", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Category_ParentID_Name",
                table: "Part.Subassembly.Category",
                columns: new[] { "BID", "ParentID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.CategoryLink_Category",
                table: "Part.Subassembly.CategoryLink",
                columns: new[] { "BID", "CategoryID", "PartID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Data_Name",
                table: "Part.Subassembly.Data",
                columns: new[] { "BID", "Name", "IsActive" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Subassembly.CategoryLink");

            migrationBuilder.DropTable(
                name: "Part.Subassembly.Category");

            migrationBuilder.DropTable(
                name: "Part.Subassembly.Data");
        }
    }
}
