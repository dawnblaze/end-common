using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180124161231_ReverseRestOfCustomFKs")]
    public partial class ReverseRestOfCustomFKs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Campaign.Data_Campaign.Custom",
                table: "Campaign.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_Contact.Custom",
                table: "Contact.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Opportunity.Data_Opportunity.Custom",
                table: "Opportunity.Data");

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Custom_Campaign.Data",
                table: "Campaign.Custom",
                columns: new[] { "BID", "ID" },
                principalTable: "Campaign.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Custom_Contact.Data",
                table: "Contact.Custom",
                columns: new[] { "BID", "ID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Opportunity.Custom_Opportunity.Data",
                table: "Opportunity.Custom",
                columns: new[] { "BID", "ID" },
                principalTable: "Opportunity.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Campaign.Custom_Campaign.Data",
                table: "Campaign.Custom");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Custom_Contact.Data",
                table: "Contact.Custom");

            migrationBuilder.DropForeignKey(
                name: "FK_Opportunity.Custom_Opportunity.Data",
                table: "Opportunity.Custom");

            migrationBuilder.AddForeignKey(
                name: "FK_Campaign.Data_Campaign.Custom",
                table: "Campaign.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Campaign.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Contact.Custom",
                table: "Contact.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Contact.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Opportunity.Data_Opportunity.Custom",
                table: "Opportunity.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Opportunity.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}

