using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class MachineInstanceUsedIncorrectTypeForLocationID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE pmi
SET LocationID = null
FROM [Part.Machine.Instance] pmi
LEFT JOIN [Location.Data] ld on ld.bid = pmi.bid and ld.id = pmi.LocationID
WHERE ld.id is NULL AND pmi.locationid IS NOT NULL");

            migrationBuilder.AlterColumn<byte>(
                name: "LocationID",
                table: "Part.Machine.Instance",
                type: "tinyint",
                nullable: true,
                oldClrType: typeof(short),
                oldType: "smallint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Instance_Location.Data",
                table: "Part.Machine.Instance",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Instance_Location.Data",
                table: "Part.Machine.Instance");

            migrationBuilder.AlterColumn<short>(
                name: "LocationID",
                table: "Part.Machine.Instance",
                type: "smallint",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "tinyint",
                oldNullable: true);
        }
    }
}
