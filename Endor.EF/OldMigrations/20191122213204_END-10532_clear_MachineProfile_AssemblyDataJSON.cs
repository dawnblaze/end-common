﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10532_clear_MachineProfile_AssemblyDataJSON : EndorMigration
    {
        public override bool ClearAllAssemblies()
        {
            return true;
        }

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [Part.Machine.Profile] SET AssemblyOVDataJSON = '{}'
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //no rollback migration since we are resetting data
        }
    }
}
