using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CreateTrigger_OrderItemData_UpdateOrderItemCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- =============================================
-- Author:		CoreBridge
-- Create date: 10/2018
-- Description:	This trigger updates the Order's ItemCount
--              whenever a line item is inserted or removed.
-- =============================================
CREATE TRIGGER [Order.Item.Data_UpdateOrderItemCount] 
   ON  [Order.Item.Data]
   AFTER INSERT, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    UPDATE OD 
    SET ItemCount = (SELECT COUNT(*) FROM [Order.Item.Data] LI WHERE LI.OrderID = OD.ID )
    FROM [Order.Data] OD
    JOIN INSERTED I on OD.ID = I.ID
    ;
END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF OBJECT_ID ('[Order.Item.Data_UpdateOrderItemCount]', 'TR') IS NOT NULL  
   DROP TRIGGER [Order.Item.Data_UpdateOrderItemCount]; 
            ");
        }
    }
}
