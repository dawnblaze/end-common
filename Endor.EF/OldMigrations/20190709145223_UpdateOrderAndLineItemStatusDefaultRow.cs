using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateOrderAndLineItemStatusDefaultRow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.Dashboard.Widget.Definition] SET MinRows = 2 WHERE ID = 4;
            UPDATE [System.Dashboard.Widget.Definition] SET MinRows = 2 WHERE ID = 5;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
