using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8258_Additemstatuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    INSERT [Order.Item.Status] ([BID], [ID], [IsActive], [IsSystem], [IsDefault], [TransactionType], [OrderStatusID], [StatusIndex], [Name], [CustomerPortalText], [Description]) 
                    VALUES (-1, 11, 1, 1, 1, 1, 11, 8, N'Pending', NULL, NULL) 
                        , (-1, 16, 1, 1, 1, 1, 16, 10, N'Approved', NULL, NULL)
                        , (-1, 18, 1, 1, 1, 1, 18, 9, N'Lost', NULL, NULL)
                        , (-1, 19, 1, 1, 1, 1, 19, 11, N'Voided', NULL, NULL)
                        , (-1, 21, 1, 1, 1, 2, 21, 21, N'WIP', NULL, NULL)
                        , (-1, 22, 1, 1, 1, 2, 22, 22, N'Pre-WIP', NULL, NULL)
                        , (-1, 23, 1, 1, 1, 2, 23, 23, N'Built', NULL, NULL)
                        , (-1, 24, 1, 1, 1, 2, 24, 15, N'Invoicing', N'Invoicing', NULL)
                        , (-1, 25, 1, 1, 1, 2, 25, 18, N'Invoiced', NULL, NULL)
                        , (-1, 26, 1, 1, 1, 2, 26, 20, N'Closed', N'Completed', NULL)
                        , (-1, 29, 1, 1, 1, 2, 29, 22, N'Voided', N'Canceled', NULL)
                        , (-1, 41, 1, 1, 1, 4, 41, 24, N'Requested', NULL, NULL)
                        , (-1, 43, 1, 1, 1, 4, 43, 24, N'Approved', NULL, NULL)
                        , (-1, 45, 1, 1, 1, 4, 45, 25, N'Ordered', NULL, NULL)
                        , (-1, 46, 1, 1, 1, 4, 46, 26, N'Received', NULL, NULL)
                        , (-1, 49, 1, 1, 1, 4, 49, 27, N'Voided', NULL, NULL)
                        , (-1, 61, 1, 1, 1, 64, 61, 28, N'Pending', NULL, NULL)
                        , (-1, 63, 1, 1, 1, 64, 63, 29, N'Ready', NULL, NULL)
                        , (-1, 65, 1, 1, 1, 64, 65, 30, N'Completed', NULL, NULL)
                    ;
                "
            );


            migrationBuilder.Sql(
                @"
                    exec [Util.Table.CopyDefaultRecords] 'Order.Item.Status'
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // do nothing
        }
    }
}
