using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddSubassmeblyElementTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.ElementListType ",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.ElementListType ", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.ElementType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.ElementType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Part.Subassembly.Element",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12044"),
                    Column = table.Column<byte>(nullable: false),
                    ColumnsWide = table.Column<byte>(nullable: false),
                    DefaultValueText = table.Column<string>(nullable: true),
                    ElementType = table.Column<byte>(nullable: false),
                    HasFormula = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDisabled = table.Column<bool>(nullable: false),
                    IsReadOnly = table.Column<bool>(nullable: false),
                    IsRequired = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    Label = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    ListType = table.Column<byte>(nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    Options = table.Column<string>(nullable: true),
                    ParentID = table.Column<int>(nullable: true),
                    Row = table.Column<byte>(nullable: false),
                    SubassemblyID = table.Column<int>(nullable: false),
                    Tooltip = table.Column<string>(type: "nvarchar(512)", maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subassembly.Element.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Subassembly.Element.Data_ElementType",
                        column: x => x.ElementType,
                        principalTable: "enum.ElementType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subassembly.Element.Data_ElementListType",
                        column: x => x.ListType,
                        principalTable: "enum.ElementListType ",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subassembly.Element.Data_Subassembly.Element.Data",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Part.Subassembly.Element",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subassembly.Element.Data_Part.Subassembly.Data",
                        columns: x => new { x.BID, x.SubassemblyID },
                        principalTable: "Part.Subassembly.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.Sql(@"
Insert [enum.ElementType] ([ID],[Name])
Values
        ( 0, N'NumericInput')
        , (1, N'BooleanInput')
        , (2, N'RadioGroupInput')
        , (3, N'StringInput')
        , (4, N'MemoInput')
        , (5, N'ListInput')
        , (6, N'Label')
        , (7, N'WebUrl')
        , (8, N'Container')
        , (9, N'LinkedSubassembly')
        , (10, N'ShowHideContainer')
        , (11, N'Document')
        , (12, N'Spacer')
        , (13, N'Measurement')
        , (14, N'Custom')
;
");

            migrationBuilder.Sql(@"
INSERT [enum.ElementListType] ([ID], [Name]) VALUES (0, N'CustomText');
INSERT [enum.ElementListType] ([ID], [Name]) VALUES (1, N'Labor');
INSERT [enum.ElementListType] ([ID], [Name]) VALUES (2, N'Machine');
INSERT [enum.ElementListType] ([ID], [Name]) VALUES (3, N'Material');
INSERT [enum.ElementListType] ([ID], [Name]) VALUES (4, N'Subassembly');
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Subassembly.Element");

            migrationBuilder.DropTable(
                name: "enum.ElementType");

            migrationBuilder.DropTable(
                name: "enum.ElementListType ");
        }
    }
}
