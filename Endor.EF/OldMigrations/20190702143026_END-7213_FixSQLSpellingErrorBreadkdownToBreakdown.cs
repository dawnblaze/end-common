using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END7213_FixSQLSpellingErrorBreadkdownToBreakdown : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            /* 
    PROCEDURE [dbo].[Dashboard.Widget.Definition.005]

    The stored procedure returns the JSON data for the 
        Dashboard Widget: Line Item Status Count
    The Line Item Status Count Widget displays a graph or a chart of the number of lines items currently in each status.
    See the WIKI at https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/991395876/Dashboard+Widget+Line+Item+Status+Count
    for additional information.
    
SAMPLE Use:

    DECLARE @BID SMALLINT = 1;
    DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    DECLARE @StatusID VARCHAR(255) = '21, 22';  -- Pre-WIP and WIP from https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/595329164/OrderOrderStatus+Enum 
    DECLARE @AsTable BIT = 1;

    EXEC dbo.[Dashboard.Widget.Definition.005] 
        @BID = @BID, @LocationID = @LocationID, 
        @StatusID = @StatusID, @AsTable = 1
    ;
*/

CREATE OR ALTER PROCEDURE [dbo].[Dashboard.Widget.Definition.005]
                  @BID SMALLINT
                , @LocationID TINYINT
                , @StatusID VARCHAR(255)
                , @AsTable BIT = 0
AS
BEGIN

    --  --- There are helpful for testing
    --  DECLARE @BID SMALLINT = 1;
    --  DECLARE @LocationID TINYINT = (SELECT TOP 1 ID FROM [Location.Data] WHERE @BID=@BID AND IsActive = 1);
    --  DECLARE @StatusID VARCHAR(255) = '21, 22';
    --  DECLARE @AsTable BIT = 0;

    -- ---------------------------------------------
    -- Convert the StatusIDs to a list of IDs
    -- ---------------------------------------------
    DECLARE @IDs TABLE (ID TINYINT PRIMARY KEY);

    INSERT INTO @IDs
        SELECT value from String_Split(@StatusID, ',');
    
    --  if none, add in WIP
    IF (NOT EXISTS(SELECT * FROM @IDs))
        INSERT INTO @IDs (ID) 
            VALUES (22);
    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Pull a list and count of the line item status in those order statuses
    -- Store them in a temporary table so we can transform the output
    -- ---------------------------------------------
    DECLARE @Results Table (
                      ItemStatusID SMALLINT
                    , OrderStatusID TINYINT
                    , LocationID TINYINT
                    , [BreakdownCount] INT
                    );

    INSERT INTO @Results
        SELECT OI.ItemStatusID
             , O.OrderStatusID
             , O.LocationID
             , COUNT(*) as BreakdownCount
        FROM [Order.Data] O
        JOIN [Order.Item.Data] OI ON O.ID = OI.OrderID AND O.BID = OI.BID
        JOIN @IDs IDs ON O.OrderStatusID = IDs.ID
        WHERE O.BID = @BID AND (@LocationID = O.LocationID OR @LocationID IS NULL)
        GROUP BY O.OrderStatusID, O.LocationID, OI.ItemStatusID
        ORDER BY ItemStatusID, O.OrderStatusID
    ;
    -- ---------------------------------------------

    -- ---------------------------------------------
    -- Now format the output as a table or JSON
    -- ---------------------------------------------
    IF (@AsTable = 1)
        SELECT R.ItemStatusID, OIS.Name as 'Status'
             , SUM(R.BreakdownCount) OVER( PARTITION BY R.ItemStatusID ) as 'Count'
             , R.LocationID as 'Breakdown.LocationID'
             , R.OrderStatusID as 'Breakdown.OrderStatusID'
             , R.BreakdownCount as 'Breakdown.Count'
        FROM @Results R
        JOIN [Order.Item.Status] OIS ON ItemStatusID = OIS.ID AND @BID = OIS.BID
        ORDER BY 3 DESC, 2
        ;

    ELSE
    BEGIN
        SELECT DISTINCT R.ItemStatusID, OIS.Name as 'Status'
             , SUM(R.BreakdownCount) OVER( PARTITION BY R.ItemStatusID ) as 'Count'
             , (SELECT   R2.LocationID as 'LocationID'
                       , R2.OrderStatusID as 'OrderStatusID'
                       , R2.BreakdownCount as 'Count'
                FROM @Results R2 WHERE R2.ItemStatusID = R.ItemStatusID
                FOR JSON AUTO) AS Breakdown
        FROM @Results R
        JOIN [Order.Item.Status] OIS ON ItemStatusID = OIS.ID AND @BID = OIS.BID
        ORDER BY 3 DESC, 2
        FOR JSON PATH
        ;
    END;
    -- ---------------------------------------------

END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
