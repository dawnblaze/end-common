using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSortIndexOfCompanyFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            Update [System.List.Filter.Criteria]
                SET [SortIndex] = 1
                WHERE ID = 46
            GO
            Update [System.List.Filter.Criteria]
                SET [SortIndex] = 2
                WHERE ID = 47
            GO
            Update [System.List.Filter.Criteria]
                SET [SortIndex] = 3
                WHERE ID = 48
            GO
            Update [System.List.Filter.Criteria]
                SET [SortIndex] = 4
                WHERE ID = 45
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
