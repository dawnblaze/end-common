﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9594_Remove_Default_Value_MessageDeliveryRecord : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Delivery.Record",
                table: "Message.Delivery.Record");

            migrationBuilder.AlterColumn<short>(
                name: "AttemptNumber",
                table: "Message.Delivery.Record",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValue: (short)0);

            migrationBuilder.AlterColumn<int>(
                name: "ParticipantID",
                table: "Message.Delivery.Record",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldDefaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Message.Delivery.Record",
                table: "Message.Delivery.Record",
                columns: new[] { "BID", "ParticipantID", "AttemptNumber" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Delivery.Record",
                table: "Message.Delivery.Record");

            migrationBuilder.AlterColumn<short>(
                name: "AttemptNumber",
                table: "Message.Delivery.Record",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0,
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AlterColumn<int>(
                name: "ParticipantID",
                table: "Message.Delivery.Record",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Message.Delivery.Record",
                table: "Message.Delivery.Record",
                columns: new[] { "BID", "ParticipantID", "AttemptNumber" });
        }
    }
}
