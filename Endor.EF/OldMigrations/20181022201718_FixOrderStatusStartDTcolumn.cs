using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181022201718_FixOrderStatusStartDTcolumn")]
    public partial class FixOrderStatusStartDTcolumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Because it's a computed column, we need to drop and then readd
            migrationBuilder.DropColumn("OrderStatusStartDT", "Order.Data");

            migrationBuilder.AddColumn<DateTime>(
                name: "OrderStatusStartDT",
                table: "Order.Data",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "(SYSUTCDATETIME())");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn("OrderStatusStartDT", "Order.Data");

            migrationBuilder.AddColumn<DateTime>(
                name: "OrderStatusStartDT",
                table: "Order.Data",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "(GETUTCDATE())");
        }
    }
}

