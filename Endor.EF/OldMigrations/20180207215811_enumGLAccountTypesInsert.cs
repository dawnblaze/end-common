using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180207215811_enumGLAccountTypesInsert")]
    public partial class enumGLAccountTypesInsert : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData("enum.Accounting.GLAccountType", new string[]
            { "ID", "Name" }, new object[,] {
            {10 ,  "Current Asset"      },      //General container for Asset accounts.  Also a "folder" for other GL Asset Accounts.
            {11 ,  "Bank Account"  },           //Used for bank deposit accounts.
            {12 ,  "Accounts Receivable"  },    //Used for Accounts Receivable.
            {13 ,  "Inventory"    },            //Used for Tracking Inventory Balances
            {20,  "Current Liabiliity"  },      //General container for Liability accounts.  Also a "folder" for other Liability Accounts.
            {23,  "Sales Tax Liability"  },     //Used for Sales Tax accounts.
            {30,  "Equity"  },                  //Used for Equity accounts.
            {40,  "Income"  },                  //Used for all Income accounts.
            {50,  "COGS"  },                    //Used for Cost of Goods Sold accounts, materials and other items directly associated with an order.
            {60,  "Expense"  },                 //Used for Expense accounts; charges the business incurs that are not directly associated with an order.
            });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData("enum.Accounting.GLAccountType", "ID", new object[] { 10, 11, 12, 13, 20, 23, 30, 40, 50, 60 });
        }
    }
}

