﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9597_Remove_ForeignKey_ID_On_RightsGroupMenuTree : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_System.Rights.Group.Menu.Tree_System.Rights.Group.Menu.Tree",
                table: "System.Rights.Group.Menu.Tree");

            migrationBuilder.AddForeignKey(
                name: "FK_System.Rights.Group.Menu.Tree_System.Rights.Group.Menu.Tree_ParentID",
                table: "System.Rights.Group.Menu.Tree",
                column: "ParentID",
                principalTable: "System.Rights.Group.Menu.Tree",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_System.Rights.Group.Menu.Tree_System.Rights.Group.Menu.Tree_ParentID",
                table: "System.Rights.Group.Menu.Tree");

            migrationBuilder.AddForeignKey(
                name: "FK_System.Rights.Group.Menu.Tree_System.Rights.Group.Menu.Tree",
                table: "System.Rights.Group.Menu.Tree",
                column: "ParentID",
                principalTable: "System.Rights.Group.Menu.Tree",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
