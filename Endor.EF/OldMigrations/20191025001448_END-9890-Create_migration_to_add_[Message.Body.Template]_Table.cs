﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9890Create_migration_to_add_MessageBodyTemplate_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Message.Body.Template",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14220))"),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "(GETUTCDATE())"),
                    IsActive = table.Column<bool>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false),
                    MessageTemplateType = table.Column<byte>(nullable: false),
                    ChannelType = table.Column<byte>(nullable: false),
                    LocationID = table.Column<byte>(type: "TINYINT SPARSE", nullable: true),
                    CompanyID = table.Column<int>(type: "INT SPARSE", nullable: true),
                    EmployeeID = table.Column<short>(type: "SMALLINT SPARSE", nullable: true),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Subject = table.Column<string>(maxLength: 2048, nullable: true),
                    Body = table.Column<string>(nullable: true),
                    AttachedFileCount = table.Column<byte>(nullable: false),
                    AttachedFileNames = table.Column<string>(type: "VARCHAR(MAX) SPARSE", nullable: true),
                    SizeInKB = table.Column<int>(nullable: true),
                    SortIndex = table.Column<short>(nullable: false),
                    MergeFieldList = table.Column<string>(nullable: true),
                    HasAttachment = table.Column<bool>(nullable: false, computedColumnSql: "(ISNULL(CASE WHEN [AttachedFileCount]>(0) THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))"),
                    HasMergeFields = table.Column<bool>(nullable: false, computedColumnSql: "(ISNULL(CASE WHEN LEN(MergeFieldList)>0 THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))"),
                    HasBody = table.Column<bool>(nullable: false, computedColumnSql: "(ISNULL(CASE WHEN LEN(Body)>0 THEN CONVERT([bit],(1)) ELSE CONVERT([bit],(0)) END,CONVERT([bit],(0))))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Body.Template", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Message.Body.Template_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Message.Body.Template_System.Message.TemplateType",
                        columns: x => new { x.AppliesToClassTypeID, x.MessageTemplateType },
                        principalTable: "System.Message.TemplateType",
                        principalColumns: new[] { "AppliesToClassTypeID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Message.Body.Template_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Message.Body.Template_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Message.Body.Template_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Message.Body.Template_AppliesToClassTypeID_MessageTemplateType",
            //    table: "Message.Body.Template",
            //    columns: new[] { "AppliesToClassTypeID", "MessageTemplateType" });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Message.Body.Template_BID_CompanyID",
            //    table: "Message.Body.Template",
            //    columns: new[] { "BID", "CompanyID" });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Message.Body.Template_BID_EmployeeID",
            //    table: "Message.Body.Template",
            //    columns: new[] { "BID", "EmployeeID" });

            //migrationBuilder.CreateIndex(
            //    name: "IX_Message.Body.Template_BID_LocationID",
            //    table: "Message.Body.Template",
            //    columns: new[] { "BID", "LocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Body.Template_Name",
                table: "Message.Body.Template",
                columns: new[] { "BID", "AppliesToClassTypeID", "MessageTemplateType", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Body.Template_Type",
                table: "Message.Body.Template",
                columns: new[] { "BID", "AppliesToClassTypeID", "MessageTemplateType", "SortIndex", "IsActive" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Message.Body.Template");
        }
    }
}
