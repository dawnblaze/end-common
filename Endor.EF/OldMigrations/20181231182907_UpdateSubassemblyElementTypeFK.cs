using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSubassemblyElementTypeFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subassembly.Element.Data_ElementType",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddForeignKey(
                name: "FK_Subassembly.Element.Data_ElementType",
                table: "Part.Subassembly.Element",
                column: "ElementType",
                principalTable: "enum.Part.Subassembly.ElementType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subassembly.Element.Data_ElementType",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddForeignKey(
                name: "FK_Subassembly.Element.Data_ElementType",
                table: "Part.Subassembly.Element",
                column: "ElementType",
                principalTable: "enum.ElementType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
