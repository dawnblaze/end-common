using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180614113905_UpdateEnumCustomFieldInputTypeTable")]
    public partial class UpdateEnumCustomFieldInputTypeTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [enum.CustomField.InputType]
                WHERE ID IN (1,2,3,4,11,21,31,32,71,81,91)
                ;

                INSERT [enum.CustomField.InputType] ([ID], [Name], [DataType])
                    VALUES
                    (71, N'Date Picker',7)
                  , (81, N'Time Picker',8)
                  , (91, N'DateTime Picker',9)
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [enum.CustomField.InputType]
                WHERE ID IN (71, 81, 91)
                ;

                INSERT [enum.CustomField.InputType] ([ID], [Name])
                    VALUES
                    (1, N'List')
                  , (2, N'Set')
                  , (3, N'Set')
                  , (4, N'Set')
                  , (11, N'Set')
                  , (21, N'Set')
                  , (22, N'Set')
                  , (31, N'Set')
                  , (32, N'Set')
                  , (71, N'Set')
                  , (81, N'Set')
                  , (91, N'Set')
                ;

            ");
        }
    }
}

