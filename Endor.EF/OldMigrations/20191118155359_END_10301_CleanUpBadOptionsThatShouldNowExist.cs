﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_10301_CleanUpBadOptionsThatShouldNowExist : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM O1
FROM [Option.Data] O1
WHERE OptionLevel >= 32
 AND OptionID IN (SELECT OptionID FROM [Option.Data] O2 WHERE O1.BID = O2.BID AND OptionLevel < 32)
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
