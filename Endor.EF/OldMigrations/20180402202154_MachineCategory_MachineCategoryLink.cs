using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180402202154_MachineCategory_MachineCategoryLink")]
    public partial class MachineCategory_MachineCategoryLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Machine.Category",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12032"),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    ParentID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Machine.Category", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Machine.Category_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Category_Part.Machine.Category",
                        columns: x => new { x.BID, x.ParentID },
                        principalTable: "Part.Machine.Category",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Part.Machine.CategoryLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    PartID = table.Column<short>(nullable: false),
                    CategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Machine.CategoryLink", x => new { x.BID, x.PartID, x.CategoryID });
                    table.ForeignKey(
                        name: "FK_Part.Machine.CategoryLink_Part.Machine.Category",
                        columns: x => new { x.BID, x.CategoryID },
                        principalTable: "Part.Machine.Category",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.CategoryLink_Part.Machine.Data",
                        columns: x => new { x.BID, x.PartID },
                        principalTable: "Part.Machine.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Category_Name_IsActive",
                table: "Part.Machine.Category",
                columns: new[] { "BID", "Name", "IsActive", "ParentID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Category_ParentID_Name",
                table: "Part.Machine.Category",
                columns: new[] { "BID", "ParentID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.CategoryLink_Category",
                table: "Part.Machine.CategoryLink",
                columns: new[] { "BID", "CategoryID", "PartID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Machine.CategoryLink");

            migrationBuilder.DropTable(
                name: "Part.Machine.Category");
        }
    }
}

