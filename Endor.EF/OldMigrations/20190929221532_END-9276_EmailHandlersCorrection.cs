﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9276_EmailHandlersCorrection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Category]
SET Name = 'Email Handlers', Description = 'Email Handlers'
WHERE ID = 104;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Category]
SET Name = 'Email Handler', Description = 'Email Handler'
WHERE ID = 104;
            ");
        }
    }
}
