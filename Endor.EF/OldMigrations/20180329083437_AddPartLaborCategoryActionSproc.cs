using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180329083437_AddPartLaborCategoryActionSproc")]
    public partial class AddPartLaborCategoryActionSproc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Part.Labor.Category.Action.CanDelete]

    Description: This procedure checks if the LaborCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Labor.Category.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Labor.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the LaborCategory can be deleted. The boolean response is returned in the body.  A LaborCategory can be deleted if
	- there are no labors linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Labor.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Category Specified Does not Exist. LaborCategoryID=', @ID)

    -- there are no labors linked to it or if the force option is specified to deletes the links.
	ELSE IF  EXISTS( SELECT * FROM [Part.Labor.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Category is linked to a Labor Data. LaborCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
            ");

            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Labor.Category.Action.LinkLabor]
--
-- Description: This procedure links/unlinks the LaborData to the LaborCategory
--
-- Sample Use:   EXEC dbo.[Part.Labor.Category.Action.LinkLabor] @BID=1, @LaborCategoryID=1, @LaborDataID=1, @IsLinked=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Labor.Category.Action.LinkLabor]
--DECLARE 
          @BID            TINYINT  --= 1
        , @LaborCategoryID     SMALLINT --= 2
		, @LaborDataID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the LaborCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Labor.Category] WHERE BID = @BID and ID = @LaborCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid LaborCategory Specified. LaborCategoryID='+CONVERT(VARCHAR(12),@LaborCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the LaborData specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Labor.Data] WHERE BID = @BID and ID = @LaborDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid LaborData Specified. LaborDataID='+CONVERT(VARCHAR(12),@LaborDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.LaborDataLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Part.Labor.CategoryLink] WHERE BID = @BID and CategoryID = @LaborCategoryID and PartID = @LaborDataID)
		BEGIN
			INSERT INTO [Part.Labor.CategoryLink] (BID, CategoryID, PartID)
			VALUES (@BID, @LaborCategoryID, @LaborDataID)
		END;

	END
	ELSE
	BEGIN
		-- Remove entry from Part.Labor.CategoryLink
		DELETE FROM [Part.Labor.CategoryLink] WHERE BID = @BID and CategoryID = @LaborCategoryID and PartID = @LaborDataID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
            ");

            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Labor.Category.Action.SetActive]
--
-- Description: This procedure sets the LaborCategory to active or inactive
--
-- Sample Use:   EXEC dbo.[Part.Labor.Category.Action.SetActive] @BID=1, @LaborCategoryID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Labor.Category.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @LaborCategoryID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the LaborCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Labor.Category] WHERE BID = @BID and ID = @LaborCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid LaborCategory Specified. LaborCategoryID='+CONVERT(VARCHAR(12),@LaborCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Part.Labor.Category] L
    WHERE BID = @BID and ID = @LaborCategoryID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
            ");

            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Part.Labor.Data.Action.CanDelete]

    Description: This procedure checks if the Labor Data is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Labor.Data.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

ALTER PROCEDURE [dbo].[Part.Labor.Data.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the Labor Data can be deleted. The boolean response is returned in the body.  A Labor Data can be deleted if
	- there are no categories linked to it.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Labor.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Data Specified Does not Exist. LaborDataID=', @ID)

    -- there are no categories linked to it.
	ELSE IF  EXISTS( SELECT * FROM [Part.Labor.CategoryLink] WHERE BID = @BID AND PartID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Data is linked to a Labor Category. LaborDataID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE [dbo].[Part.Labor.Category.Action.CanDelete]
DROP PROCEDURE [dbo].[Part.Labor.Category.Action.SetActive]
DROP PROCEDURE [dbo].[Part.Labor.Category.Action.LinkLabor]
            ");

            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Part.Labor.Data.Action.CanDelete]

    Description: This procedure checks if the Labor Data is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Labor.Data.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

ALTER PROCEDURE [dbo].[Part.Labor.Data.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the Labor Data can be deleted. The boolean response is returned in the body.  A Labor Data can be deleted if

*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Labor.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Labor Data Specified Does not Exist. LaborDataID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
            ");
        }
    }
}

