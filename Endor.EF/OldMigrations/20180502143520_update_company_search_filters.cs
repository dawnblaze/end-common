using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180502143520_update_company_search_filters")]
    public partial class update_company_search_filters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE dbo.[System.List.Filter.Criteria]
                SET Field='LocationID', DataType=1, InputType=4
                WHERE Name='Location' and TargetClassTypeID=2000
                GO
                UPDATE dbo.[System.List.Filter.Criteria]
                SET Field='SalesPersonID', DataType=1, InputType=1
                WHERE Name='SalesPerson' and TargetClassTypeID=2000
                GO
                UPDATE dbo.[System.List.Filter.Criteria]
                SET Field='PrimaryContactID', DataType=1, InputType=6
                WHERE Name='ContactName' and TargetClassTypeID=2000
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

