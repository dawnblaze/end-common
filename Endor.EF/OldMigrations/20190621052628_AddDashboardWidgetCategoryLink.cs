using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddDashboardWidgetCategoryLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            INSERT INTO [dbo].[System.Dashboard.Widget.CategoryLink]
                ([WidgetDefID], [CategoryType])
            VALUES
                (11, 16),
                (7, 4),
                (2, 16),
                (1, 128)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
