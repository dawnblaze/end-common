using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180813191829_add_column_RightsGroupList.ModifiedDT")]
    public partial class add_column_RightsGroupListModifiedDT : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDT",
                table: "Rights.Group.List",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "(GetUTCDate())");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModifiedDT",
                table: "Rights.Group.List");
        }
    }
}

