using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180426153829_OrderOrderLinkTypeEnumData")]
    public partial class OrderOrderLinkTypeEnumData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(1, N'Cloned From', 2, 7, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(2, N'Cloned To', 1, 7, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(3, N'Converted From', 4, 1, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(4, N'Converted To', 3, 2, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(5, N'Master Order', 6, 2, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(6, N'Child Order', 5, 2, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(7, N'Credit Memo Applied From', 8, 2, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(8, N'Credit Memo Applied To', 7, 2, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(9, N'Primary Variation For', 10, 1, 1);
INSERT [enum.Order.OrderLinkType] ([ID], [Name], [SymmetricLinkType], [TransactionTypeSet], [TransactionLevelSet]) VALUES(10, N'Alternate Variation Of', 9, 1, 1);");
    }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE [enum.Order.OrderLinkType] WHERE ID >=1 and ID <=10");
        }
    }
}

