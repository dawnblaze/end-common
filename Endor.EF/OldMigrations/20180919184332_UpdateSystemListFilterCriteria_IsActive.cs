using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180919184332_UpdateSystemListFilterCriteria_IsActive")]
    public partial class UpdateSystemListFilterCriteria_IsActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter.Criteria] SET
	[Name] = 'Include Inactive', [Label] = 'Include Inactive', [Field] = '-IsActive', [InputType] = 13, [ListValues] = null, [IsLimitToList] = 0
WHERE
	[Name] = 'Is Active';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter.Criteria] SET
	[Name] = 'Is Active', [Label] = 'Is Active', [Field] = 'IsActive', [InputType] = 2, [ListValues] = 'Is Not Active,Is Active', [IsLimitToList] = 1
WHERE
	[Name] = 'Include Inactive'
	AND [TargetClassTypeID] <> 15027;
");
        }
    }
}

