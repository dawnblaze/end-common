using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180528090420_alter_labor_and_material_simplelist_view_add_Description")]
    public partial class alter_labor_and_material_simplelist_view_add_Description : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Part.Material.SimpleList]
                                    AS
                                SELECT	[BID]
	                                  , [ID]
                                      , [ClassTypeID]
                                      , [Name] as DisplayName
                                      , [IsActive]
                                      , [HasImage]
                                      , CONVERT(BIT, 0) AS [IsDefault]
					                  , InvoiceText
                                      , Description
                                FROM [Part.Material.Data];
                GO
                ALTER VIEW [dbo].[Part.Labor.SimpleList]
                                    AS
                                SELECT[BID]
                                    , [ID]
                                    , [ClassTypeID]
                                    , [Name] as DisplayName
                                    , [IsActive]
                                    , [HasImage]
                                    , CONVERT(BIT, 0) AS[IsDefault]
					                , InvoiceText
                                    , Description
                                FROM [dbo].[Part.Labor.Data];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER VIEW [dbo].[Part.Material.SimpleList]
                                    AS
                                SELECT	[BID]
	                                  , [ID]
                                      , [ClassTypeID]
                                      , [Name] as DisplayName
                                      , [IsActive]
                                      , [HasImage]
                                      , CONVERT(BIT, 0) AS [IsDefault]
					                  , InvoiceText
                                FROM [Part.Material.Data];
                GO
                ALTER VIEW [dbo].[Part.Labor.SimpleList]
                                    AS
                                SELECT[BID]
                                    , [ID]
                                    , [ClassTypeID]
                                    , [Name] as DisplayName
                                    , [IsActive]
                                    , [HasImage]
                                    , CONVERT(BIT, 0) AS[IsDefault]
					                , InvoiceText
                                FROM [dbo].[Part.Labor.Data];
                GO
            ");
        }
    }
}

