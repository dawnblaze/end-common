using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180726213432_FixTaxIntegrationOptionDefinitions")]
    public partial class FixTaxIntegrationOptionDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Definition] SET DataType = 1 WHERE [Name] in 
(
N'Integration.Tax.AccountNumber', 
N'Integration.Tax.APIToken', 
N'Integration.Tax.URL', N'URL', 
N'Integration.Tax.CompanyIdentifier'
);

UPDATE [System.Option.Definition] SET defaultvalue = '0', DataType = 3 WHERE name = 'Estimate.Variations.Enabled';
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Definition] SET DataType = 0 WHERE [Name] in 
(
N'Integration.Tax.AccountNumber', 
N'Integration.Tax.APIToken', 
N'Integration.Tax.URL', N'URL', 
N'Integration.Tax.CompanyIdentifier'
);

update [System.Option.Definition] set defaultvalue = '1000', DataType = 1 where name = 'Estimate.Variations.Enabled'
");

        }
    }
}

