using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181011132742_FlipFiscalYears")]
    public partial class FlipFiscalYears : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.Option.Definition] SET ListValues = 'January - December,February - January,March - February,April - March,May - April,June - May,July - June,August - July,September - August,October - September,November - October,December - November' WHERE Name = 'Accounting.FiscalYear.StartingMonth';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

