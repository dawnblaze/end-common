using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180620011928_AddCompanyCustomData")]
    public partial class AddCompanyCustomData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Company.Custom.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2001))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    AppliesToClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((2000))"),
                    DataJSON = table.Column<string>(type: "varchar(max)", nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company.Custom.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Company.Custom.Data_Company",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Company.Custom.Data");
        }
    }
}

