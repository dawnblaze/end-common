using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180628150217_AddCustomFieldSectionAndCategory")]
    public partial class AddCustomFieldSectionAndCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.Option.Section] ([ID],[Name],[ParentID],[IsHidden],[Depth],[SearchTerms])
VALUES (15025,'Custom Fields',NULL,0,0,NULL);

INSERT INTO [dbo].[System.Option.Category] ([ID],[Name],[SectionID],[Description],[OptionLevels],[IsHidden],[SearchTerms])
VALUES (15026,'Custom Field Setup',15025,'User defined custom fields setup and search.',4,0,NULL);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[System.Option.Category] WHERE [ID] = 15026;
DELETE FROM [dbo].[System.Option.Section] WHERE [ID] = 15025;
");
        }
    }
}

