﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddDefaultsToPreSetAssemblies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Part.Subassembly.Variable] 
SET UnitType = IsNull(UnitType, 0)
  , DisplayType = IsNull(DisplayType, 0)
WHERE ElementType = 21 
  AND UnitType IS NULL
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Part.Subassembly.Variable] 
SET UnitType = NULL
  , DisplayType = NULL
WHERE ElementType = 21 
  AND UnitType = 0
;");
        }
    }
}
