using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_MachineLayoutType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Machine.LayoutType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Machine.LayoutType", x => x.ID);
                });

            migrationBuilder.Sql(@"
INSERT INTO [enum.Machine.LayoutType]
  ( [ID] , [Name]  )
VALUES
  (    0 , 'None'  )
, (    1 , 'Sheet' )
, (    2 , 'Roll'  )
");

            migrationBuilder.AddColumn<byte>(
                name: "MachineLayoutType",
                table: "Part.Subassembly.Layout",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<byte>(
                name: "MachineLayoutTypes",
                table: "Part.Subassembly.Data",
                type: "tinyint",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Layout_enum.Part.Subassembly.MachineLayoutType",
                table: "Part.Subassembly.Layout",
                column: "MachineLayoutType",
                principalTable: "enum.Machine.LayoutType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Layout_enum.Part.Subassembly.MachineLayoutType",
                table: "Part.Subassembly.Layout");

            migrationBuilder.DropColumn(
                name: "MachineLayoutType",
                table: "Part.Subassembly.Layout");

            migrationBuilder.DropColumn(
                name: "MachineLayoutTypes",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropTable(
                name: "enum.Machine.LayoutType");
        }
    }
}
