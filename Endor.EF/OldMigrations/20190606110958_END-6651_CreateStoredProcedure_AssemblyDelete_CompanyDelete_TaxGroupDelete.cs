using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6651_CreateStoredProcedure_AssemblyDelete_CompanyDelete_TaxGroupDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // Util.Assembly.Delete
            migrationBuilder.Sql(@"
/* 
    This procedure deletes an Assembly for a BID.  
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        DECLARE @BID SMALLINT = 1;
        DECLARE @AssemblyID INT = (SELECT TOP(1) ID From [Part.SubAssembly.Data] WHERE BID = @BID AND IsActive = 0 AND ID > 1 ORDER BY ID DESC );
        DECLARE @TargetAssemblyID INT = (SELECT TOP(1) ID From [Part.SubAssembly.Data] WHERE BID = @BID AND IsActive = 1 ORDER BY ID ASC )

        SELECT * FROM [Part.SubAssembly.Data] WHERE BID = @BID AND ID = @AssemblyID;

        EXEC dbo.[Util.Assembly.Delete] @BID = @BID, @AssemblyID = @AssemblyID, @TargetAssemblyID = @TargetAssemblyID, @TestOnly = 0 ;

        -- GO 20

        SELECT * FROM [Part.SubAssembly.Data] WHERE BID = @BID 
*/
CREATE OR ALTER PROCEDURE [Util.Assembly.Delete]( @BID SMALLINT, @AssemblyID INT, @TargetAssemblyID INT,  @TestOnly BIT = 0 )
AS
BEGIN
    BEGIN TRANSACTION [DeleteAssembly];
    BEGIN TRY

        -- Remove Assembly Fields that do Not Remain
        DELETE FROM [Part.Assembly.Table] WHERE BID = @BID AND AssemblyID = @AssemblyID;
        DELETE FROM [Part.Assembly.Variable.Formula] WHERE BID = @BID AND VariableID IN (SELECT ID FROM [Part.SubAssembly.Variable] WHERE BID = @BID AND @AssemblyID = @AssemblyID);
        DELETE FROM [Part.Subassembly.Element] WHERE BID = @BID AND SubassemblyID = @AssemblyID;
        DELETE FROM [Part.SubAssembly.Variable] WHERE BID = @BID AND SubassemblyID = @AssemblyID;
        DELETE FROM [Part.Subassembly.Layout] WHERE BID = @BID AND SubassemblyID = @AssemblyID;
        DELETE FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID AND PartID = @AssemblyID;

        -- Reassign Assembly Fields that Remain
        UPDATE [Order.Item.Component] SET AssemblyID = @TargetAssemblyID WHERE BID = @BID AND AssemblyID = @AssemblyID; 

        -- Now Delete the Assembly
        DELETE FROM [Part.SubAssembly.Data]  WHERE BID = @BID AND ID = @AssemblyID; 

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION [DeleteAssembly];
            RETURN;
        END;

        COMMIT TRANSACTION [DeleteAssembly];
    
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION [DeleteAssembly];

        THROW; -- Rethrow the error
    END CATCH

END;");

            // Util.Company.Delete
            migrationBuilder.Sql(@"
/* 
    This procedure deletes ALL orders for a BID.  
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.Order.Delete] @BID = 1, @CompanyID = 1013, @TestOnly = 1 ;

    or         

        DECLARE @CompanyID INT
            , @BID SMALLINT = 1
            , @TestOnly BIT = 1
            , @COUNT INT = 10
            , @SQL VARCHAR(MAX) = ''
            ;

        SELECT TOP(@COUNT) 
            @SQL += CONCAT('EXEC dbo.[Util.Order.Delete] @BID = ',BID,', @CompanyID = ',ID,', @TestOnly = ',@TestOnly,' ; ')
        FROM [Order.Data] 
        WHERE @BID = BID
        ORDER BY ModifiedDT
        ;

        SELECT @SQL as TheSQL;

        EXECUTE(@SQL);

        SELECT * FROM [Order.Data]
*/
CREATE OR ALTER PROCEDURE [Util.Company.Delete]( @BID SMALLINT, @CompanyID INT, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION;
    BEGIN TRY

        -- Delete Contacts and related fields

        DELETE FROM [User.Link] WHERE BID = @BID AND ContactID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);

        DELETE FROM [Contact.] WHERE BID = @BID AND ID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Contact.] WHERE BID = @BID AND ID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Contact.] WHERE BID = @BID AND ID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);

        DELETE FROM [Contact.Custom.Data] WHERE BID = @BID AND ID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Contact.Locator] WHERE BID = @BID AND ParentID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Contact.TagLink] WHERE BID = @BID AND ContactID in (SELECT ID FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID);

        DELETE FROM [Contact.Data] WHERE BID = @BID AND CompanyID = @CompanyID
        

        -- Delete Company Related Fields

        DELETE FROM [Accounting.GLData] WHERE BID = @BID AND CompanyID = @CompanyID
        DELETE FROM [Accounting.Payment.Master] WHERE BID = @BID AND CompanyID = @CompanyID
        DELETE FROM [Accounting.Payment.Application] WHERE BID = @BID AND CompanyID = @CompanyID

        DELETE FROM [Company.Custom.Data] WHERE BID = @BID AND ID = @CompanyID
        DELETE FROM [Company.Locator] WHERE BID = @BID AND ParentID = @CompanyID
        DELETE FROM [Company.TagLink] WHERE BID = @BID AND CompanyID = @CompanyID
        DELETE FROM [Part.QuickItem.CategoryLink] WHERE BID = @BID AND QuickItemID in (SELECT ID FROM [Part.QuickItem.Data] WHERE BID = @BID AND CompanyID = @CompanyID);
        DELETE FROM [Part.QuickItem.Data] WHERE BID = @BID AND CompanyID = @CompanyID
        DELETE FROM [Part.Surcharge.Data] WHERE BID = @BID AND CompanyID = @CompanyID


        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION;
            RETURN;
        END;

        COMMIT TRANSACTION;
    
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;

        THROW; -- Rethrow the error
    END CATCH

END;");

            // Util.TaxGroup.Delete
            migrationBuilder.Sql(@"
/* 
    This procedure deletes a Tax Group.  It can also delete unused Tax Items.
    The tax group can not be in use!  
    There is no backup. Don't use this unless you really know what you are doing!

    Example:

        EXEC dbo.[Util.TaxGroup.Delete] @BID = 102, @TaxGroupID = 1032, @DeleteUnusedItems = 1, @TestOnly = 0 ;
        SELECT * FROM [Accounting.Tax.Group] WHERE BID = 102

    or         

        DECLARE @TaxGroupID INT
            , @BID SMALLINT = 102
            , @TestOnly BIT = 0
            , @COUNT INT = 1
            , @SQL VARCHAR(MAX) = ''
            ;

        SELECT TOP(@COUNT) 
            @SQL += CONCAT('EXEC dbo.[Util.TaxGroup.Delete] @BID = ',BID,', @TaxGroupID = ',ID,', @TestOnly = ',@TestOnly,' ; ')
        FROM [Accounting.Tax.Group] 
        WHERE @BID = BID
        ORDER BY ModifiedDT
        ;

        SELECT @SQL as TheSQL;

        EXECUTE(@SQL);

        SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID
*/
CREATE OR ALTER PROCEDURE [Util.TaxGroup.Delete]( @BID SMALLINT, @TaxGroupID INT, @DeleteUnusedItems BIT = 0, @TestOnly BIT = 1 )
AS
BEGIN
    BEGIN TRANSACTION;
    BEGIN TRY

        IF (@TestOnly=1)
            SELECT BID, ID, Name from [Accounting.Tax.Group] WHERE BID = @BID AND ID = @TaxGroupID;

        DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID AND GroupID = @TaxGroupID;
        DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID AND GroupID = @TaxGroupID;

        DELETE FROM [Accounting.Tax.Group] WHERE BID = @BID AND ID = @TaxGroupID;

        IF (@DeleteUnusedItems = 1)
        BEGIN
            DELETE TI 
            FROM [Accounting.Tax.Item] TI
            WHERE TI.BID = @BID and NOT EXISTS(SELECT L.ItemID  FROM [Accounting.Tax.Group.ItemLink] L WHERE L.BID = @BID);
        END;

        -- If this was only a test, then Rollback.
        IF (@TestOnly = 1)
        BEGIN
            PRINT 'Successful Test!'
            ROLLBACK TRANSACTION;
            RETURN;
        END;

        COMMIT TRANSACTION;
    
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION;

        THROW; -- Rethrow the error
    END CATCH

END;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE [dbo].[Util.Assembly.Delete]
DROP PROCEDURE [dbo].[Util.Company.Delete]
DROP PROCEDURE [dbo].[Util.TaxGroup.Delete]
            ");
        }
    }
}
