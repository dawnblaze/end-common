using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180126214202_IndustryActionStoredProcedures")]
    public partial class IndustryActionStoredProcedures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"If (exists (select * from sys.objects where name='Industry.Action.SetParent' and type='P'))
	DROP PROCEDURE [dbo].[Industry.Action.SetParent]
GO

/****** Object:  StoredProcedure [dbo].[Industry.Action.SetParent]    Script Date: 1/26/2018 3:47:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Industry.Action.SetParent]
--
-- Description: This procedure sets the Parent of the industry
--
-- Sample Use:   EXEC dbo.[Industry.Action.SetParent] @BID=1, @IndustryID=1, @ParentID=2
-- ========================================================
CREATE PROCEDURE [dbo].[Industry.Action.SetParent]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @IndustryID     SMALLINT     -- = 2

        , @ParentID       SMALLINT -- = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

-- Check if the Industry and Parent are different
	IF (@IndustryID = @ParentID)
	BEGIN
	 SELECT @Result = 0
             , @Message = 'Invalid Parent Specified. ParentID can not be equal to IndustryID'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
	END

    -- Check if the Industry specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @IndustryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Industry Specified. IndustryID='+CONVERT(VARCHAR(12),@IndustryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;
	
    -- If specifying a parent industry, check if the parent industry specified is valid
    IF @ParentID IS NOT NULL 
	BEGIN
	  IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @ParentID)
      BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Parent Industry Specified. ParentID='+CONVERT(VARCHAR(12),@ParentID)+' not found'
             ;
	    
        THROW 50000, @Message, 1;
        RETURN @Result;
      END;
	END;

    -- Now update it
    UPDATE L
    SET ParentID = @ParentID
    FROM [CRM.Industry] L
    WHERE BID = @BID and ID = @IndustryID      

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO");

            migrationBuilder.Sql(@"/****** Object:  StoredProcedure [dbo].[Industry.Action.SetActive]    Script Date: 1/26/2018 2:27:42 PM ******/
If (exists( Select * from sys.objects where name='Industry.Action.SetActive' and Type='p'))
   DROP PROCEDURE [dbo].[Industry.Action.SetActive]

GO
-- ========================================================
-- Name: [Industry.Action.SetActive]
--
-- Description: This procedure sets the reference Industry
--
-- Sample Use:   EXEC dbo.[Industry.Action.SetActive] @BID=1, @IndustryID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Industry.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @IndustryID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the industry specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @IndustryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Industry Specified. IndustryID='+CONVERT(VARCHAR(12),@IndustryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;
	
    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [CRM.Industry] L
    WHERE BID = @BID and ID = @IndustryID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
If (exists( Select * from sys.objects where name='Industry.Action.SetActive' and Type='p'))
   DROP PROCEDURE [dbo].[Industry.Action.SetActive]
");
            migrationBuilder.Sql(@"
If (exists( Select * from sys.objects where name='Industry.Action.SetParent' and Type='p'))
   DROP PROCEDURE [dbo].[Industry.Action.SetActive]
");
        }
    }
}

