using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180817152721_END-2231_filter_correction")]
    public partial class END2231_filter_correction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

                DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=1100;
                DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE [TargetClassTypeID]=1100;


                INSERT INTO [dbo].[List.Filter]
                        ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem]
                        ,[Hint],[IsDefault],[SortIndex])
                    VALUES
                        (1 --<BID, smallint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]) --<ID, int,>
                        ,GETUTCDATE() --<CreatedDate, date,>
                        ,GETUTCDATE() --<ModifiedDT, datetime2(2),>
                        ,1 --<IsActive, bit,>
                        ,'All' --<Name, varchar(255),>
                        ,1100 --<TargetClassTypeID, int,>
                        ,NULL --<IDs, xml,>
                        ,NULL --<Criteria, xml,>
                        ,NULL --<OwnerID, smallint,>
                        ,1 --<IsPublic, bit,>
                        ,1 --<IsSystem, bit,>
                        ,NULL --<Hint, varchar(max),>
                        ,1 --<IsDefault, bit,>
                        ,0 --<SortIndex, tinyint,>
                        );

                INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (1100 --<TargetClassTypeID, int,>
                        ,'Name' --<Name, varchar(255),>
                        ,'Name' --<Label, varchar(255),>
                        ,'Name' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,0 --<DataType, tinyint,>
                        ,0 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,0 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );

                INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (1100 --<TargetClassTypeID, int,>
                        ,'Type' --<Name, varchar(255),>
                        ,'Type' --<Label, varchar(255),>
                        ,'Type' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,0 --<DataType, tinyint,>
                        ,12 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,'Shared,User Specific' --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,1 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

