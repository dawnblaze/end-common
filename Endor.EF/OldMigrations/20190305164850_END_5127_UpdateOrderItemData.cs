using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END_5127_UpdateOrderItemData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
--
-- This migration updates the computed fields in the OrderItem table.
-- Altering Computed Columns is not supported in SQL.  You have to drop and re-add. :-(
/* Changes:

    Price.UnitOV -> DELETED
    Price.Net -> DELETED
    ADD Price.Component
    ADD Price.Surcharge
    Add Price.List -> COMPUTED AS Price.Component + Price.Surcharge
    Price.PreTax -> COMPUTE AS PriceComponent + Price.Surcharge - Price.Discount
    Price.UnitPreTax -> COMPUTED AS Price.PreTax/Quantity
    Price.Total -> Price.PreTax + Price.Tax

    Computed.Net -> DELETE
    Computed.PreTax -> DELETE
*/
-- To make sure this update is an all or nothing ... wrap in a TRY/CATCH
BEGIN TRANSACTION [UpdateOrderItemComputedColumns];
BEGIN TRY
    -- Turn of System Versioniong on the Table
    ALTER TABLE [Order.Item.Data] SET (SYSTEM_VERSIONING = OFF); 

    PRINT 'System Versioning Disabled';

    DROP TABLE [dbo].[Historic.Order.Item.Data];

    -- Drop the Altered Columns
    -- Syntax: ALTER TABLE [x] DROP COLUMN col1, col2, ... 
    ALTER TABLE [Order.Item.Data] 
    DROP COLUMN [Price.UnitOV]
              , [Price.Total]
              , [Price.PreTax]
              , [Price.Net]
              , [Price.NetOV]
              , [Computed.PreTax]
              , [Computed.Net]
              , [Price.Unit]
    ;

    PRINT 'Columns Dropped';

    -- Now add the Columns with their correct formula
    ALTER TABLE [Order.Item.Data]
    ADD [Price.Component] DECIMAL(18,4) NULL
      , [Price.Surcharge] DECIMAL(18,4) NULL
      , [Price.List] AS [Price.Component] + [Price.Surcharge]
      , [Price.PreTax] AS [Price.Component] + [Price.Surcharge] - ISNULL([Price.ItemDiscount], 0.0) - ISNULL([Price.AppliedOrderDiscount], 0.0)
      , [Price.UnitPreTax] AS ([Price.Component] + [Price.Surcharge] - ISNULL([Price.ItemDiscount], 0.0) - ISNULL([Price.AppliedOrderDiscount], 0.0)) / IIF(Quantity = 0.0, NULL, Quantity)
      , [Price.Total] AS [Price.Component] + [Price.Surcharge] - ISNULL([Price.ItemDiscount], 0.0) - ISNULL([Price.AppliedOrderDiscount], 0.0) + [Price.Tax]
    ;    

    PRINT 'Columns Added';

    -- If we go here, it must have worked.  
    -- Commit and move on to the next task.
    COMMIT TRANSACTION [UpdateOrderItemComputedColumns];

    PRINT 'Changes Committed';

    -- Turn of System Versioniong back on!
    ALTER TABLE [Order.Item.Data] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Item.Data])); 

    PRINT 'System Versioning Enabled';
END TRY
BEGIN CATCH
    -- Bummer!  Let's save this for another time.
    ROLLBACK TRANSACTION [UpdateOrderItemComputedColumns];

    -- Turn of System Versioniong back on!
    ALTER TABLE [Order.Item.Data] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Item.Data])); 

    THROW; -- Rethrow the error
END CATCH
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
