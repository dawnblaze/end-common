using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180815170507_Add_RightsGroupListListFilter")]
    public partial class Add_RightsGroupListListFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
  INSERT INTO [List.Filter] (BID,ID,IsActive,Name,
  TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,hint,IsDefault,SortIndex ) 
  VALUES (1,(SELECT MAX(ID) + 1 FROM [List.Filter]),1,'All',1100, NULL,NULL,NULL,1,1,NULL,1,0)
  
  INSERT INTO [List.Filter] (BID,ID,IsActive,Name,
  TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,hint,IsDefault,SortIndex ) 
  VALUES (1,(SELECT MAX(ID) + 1 FROM [List.Filter]),1,'Active',1100, NULL,'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',NULL,1,1,NULL,1,0)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [List.Filter] WHERE TargetClassTypeID = 1100;
");
        }
    }
}

