using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END7762_Created_new_field_END776_CompletedByContactID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CompletedByContactID",
                table: "Activity.GLActivity",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Activity.GLActivity_BID_CompletedByContactID",
                table: "Activity.GLActivity",
                columns: new[] { "BID", "CompletedByContactID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Activity.GL_Contact.Data",
                table: "Activity.GLActivity",
                columns: new[] { "BID", "CompletedByContactID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activity.GL_Contact.Data",
                table: "Activity.GLActivity");

            migrationBuilder.DropIndex(
                name: "IX_Activity.GLActivity_BID_CompletedByContactID",
                table: "Activity.GLActivity");

            migrationBuilder.DropColumn(
                name: "CompletedByContactID",
                table: "Activity.GLActivity");
        }
    }
}
