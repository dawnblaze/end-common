using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddGLData2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.GLData_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.GLData");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.GLData_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "GLAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.GLData_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.GLData");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.GLData_Accounting.GL.Account_BID_GLAccountID",
                table: "Accounting.GLData",
                columns: new[] { "BID", "GLAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
