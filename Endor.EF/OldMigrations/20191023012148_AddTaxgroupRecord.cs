﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddTaxgroupRecord : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [Accounting.Tax.Group] 
    ([BID], [ID], [ModifiedDT], [IsActive], [Name])
    VALUES
    (-1, 1, '2018-01-01', 0, 'TaxJar');
            ");

            migrationBuilder.Sql(@"
EXEC [Util.Table.CopyDefaultRecords] 'Accounting.Tax.Group';
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [Accounting.Tax.Group] WHERE BID = -1;
            ");
            migrationBuilder.Sql(@"
EXEC dbo.[Util.TaxGroup.Delete] @BID = -1, @TaxGroupID = 1, @TestOnly = 0;
            ");            
        }
    }
}
