using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateSubassemblyDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IncomeAccountID",
                table: "Part.Subassembly.Data",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "IncomeAllocationType",
                table: "Part.Subassembly.Data",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "PricingType",
                table: "Part.Subassembly.Data",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<short>(
                name: "TaxabilityCodeID",
                table: "Part.Subassembly.Data",
                type: "smallint sparse",
                nullable: true);
            
            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Data_IncomeAllocationType",
                table: "Part.Subassembly.Data",
                column: "IncomeAllocationType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Data_PricingType",
                table: "Part.Subassembly.Data",
                column: "PricingType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Data_BID_IncomeAccountID",
                table: "Part.Subassembly.Data",
                columns: new[] { "BID", "IncomeAccountID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Data_enum.Part.Subassembly.IncomeAllocationType",
                table: "Part.Subassembly.Data",
                column: "IncomeAllocationType",
                principalTable: "enum.Part.Subassembly.IncomeAllocationType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Data_enum.Part.Subassembly.PricingType",
                table: "Part.Subassembly.Data",
                column: "PricingType",
                principalTable: "enum.Part.Subassembly.PricingType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Subassembly.Data_Accounting.GL.Account",
                table: "Part.Subassembly.Data",
                columns: new[] { "BID", "IncomeAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Data_enum.Part.Subassembly.IncomeAllocationType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Data_enum.Part.Subassembly.PricingType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Subassembly.Data_Accounting.GL.Account",
                table: "Part.Subassembly.Data");
            
            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Data_IncomeAllocationType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Data_PricingType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Data_BID_IncomeAccountID",
                table: "Part.Subassembly.Data");
            
            migrationBuilder.DropColumn(
                name: "IncomeAccountID",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "IncomeAllocationType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "PricingType",
                table: "Part.Subassembly.Data");

            migrationBuilder.DropColumn(
                name: "TaxabilityCodeID",
                table: "Part.Subassembly.Data");
        }
    }
}
