using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180823021405_premigration")]
    public partial class AddCustomFieldLayoutPremigrationCOPY : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE dbo.[CustomField.Layout.Definition]
                DROP COLUMN IF EXISTS [Name];

                ALTER TABLE dbo.[CustomField.Layout.Definition]
                ADD [Name] NVARCHAR(100);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
    
        }
    }
}

