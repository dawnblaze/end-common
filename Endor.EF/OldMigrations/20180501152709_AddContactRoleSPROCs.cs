using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180501152709_AddContactRoleSPROCs")]
    public partial class AddContactRoleSPROCs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.AddContactRole]( @BID tinyint, @OrderID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255) )
--
-- Description: This function adds a order contact role
--
-- Sample Use:   EXEC dbo.[Order.Action.AddContactRole] @BID=1, @OrderID=1, @contactRoleType=1, @contactID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.AddContactRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1
        , @ContactName     VARCHAR(255)  = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the role type does not exist on the order
    IF EXISTS(SELECT * FROM [Order.Contact.Role] WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Contact role already exist on this order.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Contact specified is valid
    IF @ContactID IS NULL 
    BEGIN
      IF ISNULL(LTRIM(@ContactName), '') = ''
      BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Name.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
      END
    END
    ELSE IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR,@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END
    ELSE IF NOT EXISTS(SELECT *
                       FROM [Contact.Data] C
                            JOIN [Order.Data] O ON O.CompanyID = C.CompanyID
                       WHERE C.BID = @BID AND C.ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. Company is not the order company'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @NewID INT;
    EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10013, 1;

    IF @ContactID IS NULL 
    BEGIN
        INSERT INTO [Order.Contact.Role]
        (BID, ID, OrderID, RoleType, IsAdHoc, ContactID, ContactName, ContactCompany)
        VALUES
        (@BID, @NewID, @OrderID, @ContactRoleType, 1, NULL, @ContactName, NULL)
    END
    ELSE
    BEGIN
        INSERT INTO [Order.Contact.Role]
        (BID, ID, OrderID, RoleType, IsAdHoc, ContactID, ContactName, ContactCompany)
        SELECT @BID, @NewID, @OrderID, @ContactRoleType, 0, @ContactID, contact.ShortName, company.Name
        FROM   [Contact.Data] contact
               LEFT JOIN [Company.Data] company ON contact.BID = company.BID AND contact.CompanyID = company.ID
        WHERE  contact.BID = @BID AND contact.ID = @ContactID
    END;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO

-- ========================================================
-- Name: [Order.Action.UpdateContactRole]( @BID tinyint, @OrderID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255) )
--
-- Description: This function updates a order contact role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateContactRole] @BID=1, @OrderID=1, @contactRoleType=1, @contactID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.UpdateContactRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1
        , @ContactName     VARCHAR(255)  = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Contact Role exists
    SELECT @ID = ID
    FROM [Order.Contact.Role]
    WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order contact role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Check if the Contact specified is valid
    IF @ContactID IS NULL 
    BEGIN
      IF ISNULL(LTRIM(@ContactName), '') = ''
      BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Name.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
      END
    END
    ELSE IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR,@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END
    ELSE IF NOT EXISTS(SELECT *
                       FROM [Contact.Data] C
                            JOIN [Order.Data] O ON O.CompanyID = C.CompanyID
                       WHERE C.BID = @BID AND C.ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. Company is not the order company'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    IF @ContactID IS NULL 
    BEGIN
        UPDATE OCR
        SET    IsAdHoc = 1, ContactID = NULL, ContactName = @ContactName, ContactCompany = NULL
        FROM   [Order.Contact.Role] OCR 
        WHERE  OCR.BID = @BID AND OCR.ID = @ID
    END
    ELSE
    BEGIN
        UPDATE OCR
        SET    IsAdHoc = 0, ContactID = @ContactID, ContactName = contact.ShortName, ContactCompany = company.Name
        FROM   [Order.Contact.Role] OCR 
               LEFT JOIN [Contact.Data] contact ON contact.BID = @BID AND contact.ID = @ContactID
               LEFT JOIN [Company.Data] company ON contact.BID = company.BID AND contact.CompanyID = company.ID
        WHERE  OCR.BID = @BID AND OCR.ID = @ID
    END;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO

-- ========================================================
-- Name: [Order.Action.RemoveContactRole]( @BID tinyint, @OrderID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255) )
--
-- Description: This function remove a order contact role
--
-- Sample Use:   EXEC dbo.[Order.Action.RemoveContactRole] @BID=1, @OrderID=1, @contactRoleType=1, @contactID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.RemoveContactRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1
        , @ContactName     VARCHAR(255)  = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Contact Role exists
    IF @ContactID IS NULL 
    BEGIN
      IF ISNULL(LTRIM(@ContactName), '') != ''
      BEGIN
          SELECT @ID = ID
          FROM [Order.Contact.Role]
          WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType AND ContactName =  @ContactName
      END
    END
    ELSE 
    BEGIN
        SELECT @ID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType AND ContactID =  @ContactID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order contact role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Delete record is exists
    DELETE FROM OCR
    FROM   [Order.Contact.Role] OCR
    WHERE  BID = @BID AND ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.AddContactRole]
GO
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.UpdateContactRole]
GO
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.RemoveContactRole]
");
        }
    }
}

