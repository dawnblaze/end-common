using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180809192452_AddListFilterForBoards")]
    public partial class AddListFilterForBoards : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [List.Filter] (BID, ID, IsActive, Name, TargetClassTypeID, Criteria, IsDefault,IsSystem,SortIndex)
VALUES (1,(SELECT MAX(ID) + 1 FROM [List.Filter]),1,'Active',14000,'<ArrayOfListFilterItem><ListFilterItem><Label>Active</Label><SearchValue>True</SearchValue><Field>IsActive</Field><DisplayText>Is Active</DisplayText><IsHidden>false</IsHidden></ListFilterItem></ArrayOfListFilterItem>',1,1,0)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [List.Filter] WHERE TargetClassTypeID = 14000;
");
        }
    }
}

