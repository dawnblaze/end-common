using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180404194451_PartMaterialEnumDefaultValues_END-802")]
    public partial class PartMaterialEnumDefaultValues_END802 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "WidthRequired",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "WeightRequired",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "LengthRequired",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "HeightRequired",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<byte>(
                name: "Dimensions",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                defaultValueSql: "(0)",
                oldClrType: typeof(byte));


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "WidthRequired",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "(0)");

            migrationBuilder.AlterColumn<bool>(
                name: "WeightRequired",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "(0)");

            migrationBuilder.AlterColumn<bool>(
                name: "LengthRequired",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "(0)");

            migrationBuilder.AlterColumn<bool>(
                name: "HeightRequired",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                oldClrType: typeof(bool),
                oldDefaultValueSql: "(0)");

            migrationBuilder.AlterColumn<byte>(
                name: "Dimensions",
                table: "enum.Part.Material.Physical.Type",
                nullable: false,
                oldClrType: typeof(byte),
                oldDefaultValueSql: "(0)");
        }
    }
}

