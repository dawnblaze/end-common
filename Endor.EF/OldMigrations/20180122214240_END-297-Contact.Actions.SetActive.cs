using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180122214240_END-297-Contact.Actions.SetActive")]
    public partial class END297ContactActionsSetActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetActive')
  DROP PROCEDURE [Contact.Action.SetActive];
GO

-- ========================================================
-- Name: [Contact.Action.SetActive]
--
-- Description: This procedure sets the referenced Contact's active status
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetActive] @BID=1, @ContactID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [Contact.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ContactID      INT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Contact specified is valid
    IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID and ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR(12),@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @CompanyID INT, @IsCompanyAdHoc BIT;
    SET @CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID=@ContactID);
    SET @IsCompanyAdHoc = (SELECT IsAdHoc FROM [Company.Data] WHERE ID=@CompanyID);

    -- Check that some other Contact is Active if Company is NOT AdHoc
    IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID
        AND ID != @ContactID AND IsActive = 1 AND CompanyID != (SELECT CompanyID FROM [Contact.Data] WHERE ID = @ContactID))
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to set the only Active Contact to Inactive. Set another Contact to active first before setting this one to Inactive.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that this is not the default Contact if Company is NOT AdHoc
    IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND EXISTS(SELECT * FROM [Contact.Data] WHERE BID=@BID AND ID=@ContactID AND IsDefault=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to set the Default Contact to Inactive. Set another Contact as the Default first before setting this one to Inactive.'
             ;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that this is not the billing Contact if Company is NOT AdHoc
    IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND EXISTS(SELECT * FROM [Contact.Data] WHERE BID=@BID AND ID=@ContactID AND IsBilling=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to set the Billing Contact to Inactive. Set another Contact as the Billing first before setting this one to Inactive.'
             ;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE C
    SET   
          IsActive = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Contact.Data] C
    WHERE BID = @BID and ID = @ContactID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    -- If associated company is AdHoc, update it as well
    IF (@IsCompanyAdHoc = 1)
    BEGIN
        UPDATE Co
        SET
              IsActive = @IsActive
            , ModifiedDT = GetUTCDate()
        FROM [Company.Data] Co
        WHERE BID = @BID AND ID = @CompanyID
            AND COALESCE(IsActive,~1) != @IsActive
    END

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetBilling')
  DROP PROCEDURE [Contact.Action.SetBilling];
GO

-- ========================================================
-- Name: [Contact.Action.SetBilling]( @BID tinyint, @ContactID int )
--
-- Description: This function sets any existing Billing Contact
-- to false then sets the Contact (@ContactID) to Billing and active
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetBilling] @BID=1, @ContactID=1
-- ========================================================
CREATE PROCEDURE [Contact.Action.SetBilling]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ContactID      INT     -- = 2

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Contact specified is valid
    IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR(12),@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @CompanyID INT, @IsCompanyAdHoc BIT;
    SET @CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID=@ContactID);
    SET @IsCompanyAdHoc = (SELECT IsAdHoc FROM [Company.Data] WHERE ID=@CompanyID);

    IF (@IsCompanyAdHoc = 0)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Cannot change Billing contact for AdHoc company.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Contact.Data] WHERE CompanyID=@CompanyID);

	IF (@Count > 1)
	BEGIN
		-- Remove IsBilling from any other Billing Contact(s) for the Company
		UPDATE C
		SET
              IsBilling = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Contact.Data] C
		WHERE BID = @BID AND IsBilling = 1 AND CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID = @ContactID)
	END

    -- Now update it
    UPDATE C
    SET
          IsActive = 1
		, IsBilling = 1
        , ModifiedDT = GetUTCDate()
    FROM [Contact.Data] C
    WHERE BID = @BID AND ID = @ContactID
		AND COALESCE(IsBilling,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetDefault')
  DROP PROCEDURE [Contact.Action.SetDefault];
GO

-- ========================================================
-- Name: [Contact.Action.SetDefault]( @BID tinyint, @ContactID int )
--
-- Description: This function sets any existing default Contact
-- to false then sets the Contact (@ContactID) to default and active
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetDefault] @BID=1, @ContactID=1
-- ========================================================
CREATE PROCEDURE [Contact.Action.SetDefault]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ContactID      INT     -- = 2

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Contact specified is valid
    IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR(12),@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @CompanyID INT, @IsCompanyAdHoc BIT;
    SET @CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID=@ContactID);
    SET @IsCompanyAdHoc = (SELECT IsAdHoc FROM [Company.Data] WHERE ID=@CompanyID);

    IF (@IsCompanyAdHoc = 0)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Cannot change Default contact for AdHoc company.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Contact.Data] WHERE CompanyID=@CompanyID);

	IF (@Count > 1)
	BEGIN
		-- Remove IsDefault from any other Default Contact(s) for the Company
		UPDATE C
		SET
              IsDefault = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Contact.Data] C
		WHERE BID = @BID AND IsDefault = 1 AND CompanyID = @CompanyID
	END

    -- Now update it
    UPDATE C
    SET
          IsActive = 1
		, IsDefault = 1
        , ModifiedDT = GetUTCDate()
    FROM [Contact.Data] C
    WHERE BID = @BID AND ID = @ContactID
		AND COALESCE(IsDefault,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetActive')
  DROP PROCEDURE [Contact.Action.SetActive];
GO

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetBilling')
  DROP PROCEDURE [Contact.Action.SetBilling];
GO

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetDefault')
  DROP PROCEDURE [Contact.Action.SetDefault];
GO
");
        }
    }
}

