using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180725165115_AddBoardRoleLink")]
    public partial class AddBoardRoleLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Board.Role.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    BoardID = table.Column<short>(nullable: false),
                    RoleID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Board.Role.Link", x => new { x.BID, x.BoardID, x.RoleID });
                    table.ForeignKey(
                        name: "FK_Board.Role.Link_Board.Definition",
                        columns: x => new { x.BID, x.BoardID },
                        principalTable: "Board.Definition.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Board.Role.Link_Link.Data",
                        columns: x => new { x.BID, x.RoleID },
                        principalTable: "Employee.Role",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Board.Role.Link_Role",
                table: "Board.Role.Link",
                columns: new[] { "BID", "RoleID", "BoardID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Board.Role.Link");
        }
    }
}

