using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180625094408_AddTaxExemptReasonToCompanyData")]
    public partial class AddTaxExemptReasonToCompanyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Company.Data_BID_TaxExemptReasonID",
                table: "Company.Data",
                columns: new[] { "BID", "TaxExemptReasonID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_TaxExemptReason",
                table: "Company.Data",
                columns: new[] { "BID", "TaxExemptReasonID" },
                principalTable: "List.FlatList.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_TaxExemptReason",
                table: "Company.Data");

            migrationBuilder.DropIndex(
                name: "IX_Company.Data_BID_TaxExemptReasonID",
                table: "Company.Data");
        }
    }
}

