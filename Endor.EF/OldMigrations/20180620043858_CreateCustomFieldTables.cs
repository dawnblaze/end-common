using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180620043858_CreateCustomFieldTables")]
    public partial class CreateCustomFieldTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Contact.Custom.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false, defaultValueSql: "((3000))"),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((3001))"),
                    DataJSON = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact.Custom.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Contact.Custom.Data_Contact",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Contact.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomField.Other.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((15026))"),
                    DataJSON = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomField.Other.Data", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateTable(
                name: "Order.Custom.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false, defaultValueSql: "((10000))"),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((10001))"),
                    DataJSON = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(7)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    ValidToDT = table.Column<DateTime>(type: "DATETIME2(7)", nullable: false, defaultValueSql: "CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Custom.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Order.Custom.Data_Order",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.Sql(@"
                ALTER TABLE [Order.Custom.Data]
                ADD PERIOD FOR SYSTEM_TIME([ModifiedDT], [ValidToDT])
                GO
                ALTER TABLE [Order.Custom.Data] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Custom.Data]));
                GO
                ALTER TABLE dbo.[Historic.Order.Custom.Data]
                REBUILD PARTITION = ALL WITH (DATA_COMPRESSION = NONE); 
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Contact.Custom.Data");

            migrationBuilder.DropTable(
                name: "CustomField.Other.Data");

            migrationBuilder.DropTable(
                name: "Order.Custom.Data");
        }
    }
}

