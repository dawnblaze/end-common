using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180718184707_END_1857_DeleteBehaviors")]
    public partial class END_1857_DeleteBehaviors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group_enum.User.Access.Type",
                table: "Rights.Group");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Child",
                table: "Rights.Group.ChildGroupLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Parent",
                table: "Rights.Group.ChildGroupLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.List.RightsGroupLink_RightsGroup",
                table: "Rights.Group.List.RightsGroupLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.List.RightsGroupLink_RightsGroupList",
                table: "Rights.Group.List.RightsGroupLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.RightLink_enum.User.Right",
                table: "Rights.Group.RightLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.RightLink_Rights.Group",
                table: "Rights.Group.RightLink");

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group_enum.User.Access.Type",
                table: "Rights.Group",
                column: "UserAccessType",
                principalTable: "enum.User.Access.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Child",
                table: "Rights.Group.ChildGroupLink",
                columns: new[] { "BID", "ChildGroupID" },
                principalTable: "Rights.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Parent",
                table: "Rights.Group.ChildGroupLink",
                columns: new[] { "BID", "ParentGroupID" },
                principalTable: "Rights.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.List.RightsGroupLink_RightsGroup",
                table: "Rights.Group.List.RightsGroupLink",
                columns: new[] { "BID", "RightsGroupID" },
                principalTable: "Rights.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.List.RightsGroupLink_RightsGroupList",
                table: "Rights.Group.List.RightsGroupLink",
                columns: new[] { "BID", "RightsGroupListID" },
                principalTable: "Rights.Group.List",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.RightLink_enum.User.Right",
                table: "Rights.Group.RightLink",
                column: "RightID",
                principalTable: "enum.User.Right",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.RightLink_Rights.Group",
                table: "Rights.Group.RightLink",
                columns: new[] { "BID", "RightsGroupID" },
                principalTable: "Rights.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group_enum.User.Access.Type",
                table: "Rights.Group");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Child",
                table: "Rights.Group.ChildGroupLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Parent",
                table: "Rights.Group.ChildGroupLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.List.RightsGroupLink_RightsGroup",
                table: "Rights.Group.List.RightsGroupLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.List.RightsGroupLink_RightsGroupList",
                table: "Rights.Group.List.RightsGroupLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.RightLink_enum.User.Right",
                table: "Rights.Group.RightLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Rights.Group.RightLink_Rights.Group",
                table: "Rights.Group.RightLink");

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group_enum.User.Access.Type",
                table: "Rights.Group",
                column: "UserAccessType",
                principalTable: "enum.User.Access.Type",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Child",
                table: "Rights.Group.ChildGroupLink",
                columns: new[] { "BID", "ChildGroupID" },
                principalTable: "Rights.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.ChildGroupLink_Rights.Group_Parent",
                table: "Rights.Group.ChildGroupLink",
                columns: new[] { "BID", "ParentGroupID" },
                principalTable: "Rights.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.List.RightsGroupLink_RightsGroup",
                table: "Rights.Group.List.RightsGroupLink",
                columns: new[] { "BID", "RightsGroupID" },
                principalTable: "Rights.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.List.RightsGroupLink_RightsGroupList",
                table: "Rights.Group.List.RightsGroupLink",
                columns: new[] { "BID", "RightsGroupListID" },
                principalTable: "Rights.Group.List",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.RightLink_enum.User.Right",
                table: "Rights.Group.RightLink",
                column: "RightID",
                principalTable: "enum.User.Right",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rights.Group.RightLink_Rights.Group",
                table: "Rights.Group.RightLink",
                columns: new[] { "BID", "RightsGroupID" },
                principalTable: "Rights.Group",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}

