using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateGLDataGLActivityLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.GLData_Activity.GLActivity_ActivityGlactivityBID_ActivityGlactivityID",
                table: "Accounting.GLData");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.GLData_ActivityGlactivityBID_ActivityGlactivityID",
                table: "Accounting.GLData");

            migrationBuilder.DropColumn(
                name: "ActivityGlactivityBID",
                table: "Accounting.GLData");

            migrationBuilder.DropColumn(
                name: "ActivityGlactivityID",
                table: "Accounting.GLData");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "ActivityGlactivityBID",
                table: "Accounting.GLData",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ActivityGlactivityID",
                table: "Accounting.GLData",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.GLData_ActivityGlactivityBID_ActivityGlactivityID",
                table: "Accounting.GLData",
                columns: new[] { "ActivityGlactivityBID", "ActivityGlactivityID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.GLData_Activity.GLActivity_ActivityGlactivityBID_ActivityGlactivityID",
                table: "Accounting.GLData",
                columns: new[] { "ActivityGlactivityBID", "ActivityGlactivityID" },
                principalTable: "Activity.GLActivity",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}
