﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9767_Remove_Workflow_Category_From_GlobalSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.UpdateData(
                table: "System.Option.Section",
                keyColumns: new[] { "ID", "NAME" },
                keyValues: new object[] { "10051", "Workflow" },
                column: "IsHidden",
                value: true
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                 table: "System.Option.Section",
                 keyColumns: new[] { "ID", "NAME" },
                 keyValues: new object[] { "10051", "Workflow" },
                 column: "IsHidden",
                 value: false
                 );
        }
    }
}
