using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class enumAssemblyEmbeddedType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
              name: "enum.Assembly.EmbeddedType",
              columns: table => new
              {
                  ID = table.Column<byte>(type: "tinyint", nullable: false),
                  Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
              },
              constraints: table =>
              {
                  table.PrimaryKey("PK_enum.Assembly.EmbeddedType", x => x.ID);
              });

            migrationBuilder.Sql(@"
            INSERT [enum.Assembly.EmbeddedType] ([ID], [Name])
            VALUES (0, N'Layout Component');
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Assembly.EmbeddedType");
        }
    }
}
