using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddQuickItemModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.QuickItem.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12060"),
                    CompanyID = table.Column<int>(nullable: true),
                    DataJSON = table.Column<string>(nullable: true),
                    EmployeeID = table.Column<short>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsCompanyItem = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [CompanyID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    IsGlobalItem = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [CompanyID] IS NULL AND ([EmployeeID] IS NULL OR [IsShared]=(1)) then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    IsShared = table.Column<bool>(nullable: false),
                    LineItemCount = table.Column<byte>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.QuickItem.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.QuickItem.Data_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Part.QuickItem.Data_Company.Data",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.QuickItem.Data_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [dbo].[Part.QuickItem.SimpleList] AS
SELECT
      [BID]
    , [ID]
    , [ClassTypeID]
    , [Name] as DisplayName
    , [IsActive]
    , CONVERT(BIT, 0) AS [HasImage]
    , CONVERT(BIT, 0) AS [IsDefault]
FROM [Part.QuickItem.Data]
");

            migrationBuilder.CreateTable(
                name: "Part.QuickItem.CategoryLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    QuickItemID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.QuickItem.CategoryLink", x => new { x.BID, x.QuickItemID, x.CategoryID });
                    table.ForeignKey(
                        name: "FK_Part.QuickItem.CategoryLink_List.FlatList.Data",
                        columns: x => new { x.BID, x.CategoryID },
                        principalTable: "List.FlatList.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.QuickItem.CategoryLink_Part.QuickItem.Data",
                        columns: x => new { x.BID, x.QuickItemID },
                        principalTable: "Part.QuickItem.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.QuickItem.CategoryLink_Category",
                table: "Part.QuickItem.CategoryLink",
                columns: new[] { "BID", "CategoryID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.QuickItem.Data_Company",
                table: "Part.QuickItem.Data",
                columns: new[] { "BID", "CompanyID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.QuickItem.Data_Employee",
                table: "Part.QuickItem.Data",
                columns: new[] { "BID", "EmployeeID", "Name", "IsActive" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.QuickItem.Data_Global",
                table: "Part.QuickItem.Data",
                columns: new[] { "BID", "IsGlobalItem", "Name", "IsActive" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.QuickItem.CategoryLink");

            migrationBuilder.Sql(@"DROP VIEW [dbo].[Part.QuickItem.SimpleList];");

            migrationBuilder.DropTable(
                name: "Part.QuickItem.Data");
        }
    }
}
