using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8491_Insert_EmbeddedAssembly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [Part.Subassembly.Data] ([BID], [ID], [Description], [HasImage], [IsActive], [Name], [SKU], [IncomeAccountID], [IncomeAllocationType], [PricingType], [TaxabilityCodeID], [FixedMargin], [FixedMarkup], [FixedPrice], [HasTierTable], [PriceFormulaType], [AssemblyType], [MachineLayoutTypes]) 
VALUES (-1, 1, NULL, 0, 0, N'Layout Visualizer', NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, 0, 0, 1, 0)
;
EXEC [Util.Table.CopyDefaultRecords] @TableName = 'Part.Subassembly.Data'
;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
