using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class SystemOptionSectionForLineItemFees : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.Option.Section]
    ( [ID], [Name], [ParentID], [IsHidden], [Depth], [SearchTerms] )
VALUES
    ( 900, 'Pricing', 1000, 0, 1, NULL )
;");
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
VALUES (901, 'Line Item Setup Fees', 900, 'Line Item Setup Fees', 2, 0, 'Setup Fee Fees Surcharge Surcharges Modifier Modifiers Charge Charges Extra')
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE [dbo].[System.Option.Category] WHERE [ID]=901;");
            migrationBuilder.Sql(@"DELETE [dbo].[System.Option.Section] WHERE [ID]=900;");
        }
    }
}
