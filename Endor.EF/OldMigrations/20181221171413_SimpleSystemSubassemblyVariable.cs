using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class SimpleSystemSubassemblyVariable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[System.Part.Subassembly.Variable.SimpleList]
                GO
                CREATE VIEW [dbo].[System.Part.Subassembly.Variable.SimpleList]
                    AS
                SELECT CONVERT(SMALLINT, 0) as BID
                    , [ID]
                    , [ClassTypeID]
                    , [VariableName] as DisplayName
                    , CONVERT(BIT, 1) AS[IsActive]
                    , CONVERT(BIT, 0) AS[HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[System.Part.Subassembly.Variable];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[System.Part.Subassembly.Variable.SimpleList]
                GO
            ");
        }
    }
}
