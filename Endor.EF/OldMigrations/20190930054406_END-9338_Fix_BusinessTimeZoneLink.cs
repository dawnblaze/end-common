﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END9338_Fix_BusinessTimeZoneLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP TABLE [Business.TimeZone.Link]");
            migrationBuilder.Sql(@"CREATE TABLE [Business.TimeZone.Link](
    [BID] [smallint] NOT NULL,
    [TimeZoneID] [smallint] NOT NULL,
    CONSTRAINT [PK_Business.TimeZone.Link] PRIMARY KEY ( BID, TimeZoneID )
);
 
ALTER TABLE [Business.TimeZone.Link] WITH CHECK
ADD CONSTRAINT [FK_Business.TimeZone.Link_Business.Data]
FOREIGN KEY([BID]) REFERENCES [Business.Data] ([BID])
;
 
ALTER TABLE [Business.TimeZone.Link] WITH CHECK
ADD CONSTRAINT [FK_Business.TimeZone.Link_enum.TimeZone]
FOREIGN KEY([TimeZoneID]) REFERENCES [enum.TimeZone] ([ID])
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP TABLE [Business.TimeZone.Link]");
        }
    }
}
