using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180816144839_FixDomainEmailCategory")]
    public partial class FixDomainEmailCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.Option.Category] 
([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
VALUES ( 104, 'Email Domains', 100, 'Allowed Email Domains', 2, 0, 'email gmail outlook smtp internet')
     , ( 105, 'Marketing Email', 100, 'Marketing Email Configuration', 2, 0, 'marketing email mailchimp mailgun relay bulk')
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[System.Option.Category] WHERE [ID] = 104;
");
        }
    }
}

