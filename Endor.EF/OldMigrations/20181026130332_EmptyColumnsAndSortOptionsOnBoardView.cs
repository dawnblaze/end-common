using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181026130332_EmptyColumnsAndSortOptionsOnBoardView")]
    public partial class EmptyColumnsAndSortOptionsOnBoardView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [dbo].[Board.View]
                SET [Columns] = '',[SortOptions] = '';");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

