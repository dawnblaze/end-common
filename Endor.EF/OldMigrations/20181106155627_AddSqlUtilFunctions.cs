using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddSqlUtilFunctions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION IF EXISTS [dbo].[Util.SQLTypeToCSharp];
GO

/* Function to Map a SQL type to a C# Type

    Sample Usage:
        SELECT dbo.[Util.SQLTypeToCSharp]('nvarchar', 0);

        SELECT ColumnName, ColumnType, dbo.[Util.SQLTypeToCSharp](ColumnType, IsNullable) as [C# Type]
        FROM TablesAndColumns
        WHERE TableName = 'Order.Data';
*/
CREATE FUNCTION [dbo].[Util.SQLTypeToCSharp] ( 
          @SQLType varchar(255)
       , @IsNullable bit = 0 
    ) 
RETURNS VARCHAR(255) 
AS
BEGIN
    RETURN (CASE 
                WHEN @SQLType = 'smallint'      THEN 'short'
                WHEN @SQLType = 'tinyint'       THEN 'byte / Enum'
                WHEN @SQLType = 'decimal'       THEN 'decimal'
                WHEN @SQLType = 'bit'           THEN 'bool'
                WHEN @SQLType like '%char'      THEN 'string'
                WHEN @SQLType like '%DateTime%' THEN 'DateTime'
                WHEN @SQLType like '%Date%'     THEN 'DateTime'
                WHEN @SQLType like '%Time%'     THEN 'DateTime'
                WHEN @SQLType = 'numeric'       THEN 'decimal'
                WHEN @SQLType = 'xml'           THEN 'string'
                ELSE @SQLType            END 
            + 
            CASE WHEN @IsNullable = 1 AND @SQLType not like '%char' THEN '?' ELSE '' END
            );
END
");

            migrationBuilder.Sql(@"
DROP FUNCTION IF EXISTS [dbo].[Util.SQLTypeText];
GO

/* Function to Map a SQL type to a SQL Type Text

    Sample Usage:
        SELECT dbo.[Util.SQLTypeText]('Order.Item.Data', 'ClassTypeID');

        SELECT ColumnName, ColumnType, dbo.[Util.SQLTypeText](TableName, ColumnName) as [SQLTypeText]
        FROM TablesAndColumns
        WHERE TableName = 'Order.Data';
*/
CREATE FUNCTION [dbo].[Util.SQLTypeText] ( 
          @TableName varchar(255)
        , @ColumnName varchar(255)
    ) 
RETURNS VARCHAR(255) 
AS
BEGIN
    RETURN (

            SELECT UPPER(ColumnType) + 
            (  
                CASE WHEN ColumnType like '%char%' THEN CONCAT('(', 
                        CASE WHEN max_length = -1 THEN 'MAX' ELSE CONVERT(VARCHAR(4),max_length / IIF((ColumnType like 'n%'), 2, 1)) END
                        , ')') ELSE '' END
                + CASE WHEN ColumnType = 'datetime2' THEN CONCAT('(', scale , ')') ELSE '' END
                + CASE WHEN IsIdentity = 1 THEN ' IDENTITY' ELSE '' END
                + CASE WHEN IsNullable = 0 THEN ' NOT NULL' ELSE '' END
                + CASE WHEN IsSparse   = 1 THEN ' SPARSE' ELSE '' END
                + CASE WHEN IsComputed = 1 THEN ' COMPUTED = '+ComputedFormula+'' ELSE '' END
            )
            FROM TablesAndColumns
            WHERE TableName = @TableName AND ColumnName = @ColumnName
          )

END
");

            migrationBuilder.Sql(@"
DROP FUNCTION IF EXISTS [dbo].[Util.TableToWiki];
GO

/* Function to create WIKI Table for a particular database table.

    Sample Usage:
        SELECT * from dbo.[Util.TableToWiki]('Order.Data');
*/
CREATE FUNCTION [dbo].[Util.TableToWiki] ( @TableName varchar(255) ) 
RETURNS TABLE 
AS
RETURN (
    SELECT TOP 100 PERCENT 
          ColumnName as [Property]
        , dbo.[Util.SQLTypeToCSharp](ColumnType, IsNullable) as [Data Type]
        , CASE 
            WHEN ColumnName = 'BID'         THEN 'The Business ID for this record.'
            WHEN ColumnName = 'ID'          THEN 'The ID of this object (Unique within the Business).'
            WHEN ColumnName = 'ClassTypeID' THEN '(Read Only) An ID identifying the type of object.  Always XXXX.'
            WHEN ColumnName = 'ModifiedDT'  THEN '(Read Only) The date time the object was last modified.  Updated on every save or action.'
            WHEN ColumnName = 'CreatedDT'   THEN '(Read Only) The date time the object was initially modified.  This should not be modified once the record is created.'
            WHEN ColumnName = 'IsSystem'    THEN 'Flag indicating if the record is a System Record. System Records can''t be deleted or altered.'
            WHEN ColumnName = 'IsActive'    THEN 'Flag indicating if the record is Active.'
            WHEN ColumnName = 'IsDefault'   THEN 'Flag indicating if this record is the default one for it''s class.  Setting a record default may unset the IsDefault for other records.'
            WHEN ColumnName = 'HasImage'    THEN 'Flag indicating if a unique image is stored in DM for this object.  If False, the default image for the classtype is used instead.'
            ELSE 'The '+ColumnName+' for this object.' 
          END as [Description]
        , 'Yes' as [Serialized in JSON]
        , ColumnName as [DB Field]
        , dbo.[Util.SQLTypeText](TableName, ColumnName) AS [DB Type]
        , CONCAT((SELECT COLUMN_DEFAULT
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_SCHEMA = 'dbo'
                    AND TABLE_NAME = TableName
                    AND COLUMN_NAME = ColumnName), '')
          as [Default]
        -- , *
    FROM TablesAndColumns
    WHERE TableName = @TableName
    ORDER BY ColumnID
)

");

            migrationBuilder.Sql(@"
Drop Function IF Exists [Util.TableFieldUnitTest]
GO

/* Function to Return C# unit tests body for an entity type given the table name and model name

    Sample Usage:
        SELECT dbo.[Util.TableFieldUnitTest]('Employee.TimeCard.Detail', 'TimeCardDetail');
				        
*/
CREATE FUNCTION dbo.[Util.TableFieldUnitTest]
(
    @TableName varchar(255)
    ,@ModelTypeName varchar(255)
)
RETURNS TABLE 
AS
RETURN 
(
    Select UnitTestBody from (
    select 'Assert.IsTrue(PropertyCheck(typeof('+@ModelTypeName+'), ""'+
    	ColumnName+'"", typeof('+ dbo.[Util.SQLTypeToCSharp] (columnType, isNullable) +'), ""'+ColumnType+'""));' as UnitTestBody
    from TablesAndColumns 
    where TableName = @TableName
    Union 
    Select 'Assert.IsTrue(ClassTypeIDCheck(""'+@ModelTypeName+'"", '+ ComputedFormula + '));' as UnitTestBody from TablesAndColumns where TableName = @TableName and ColumnName = 'ClassTypeID' 
    ) a
);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
Drop Function IF Exists [Util.TableFieldUnitTest];
GO

DROP FUNCTION IF EXISTS [dbo].[Util.TableToWiki];
GO

DROP FUNCTION IF EXISTS [dbo].[Util.SQLTypeText];
GO

DROP FUNCTION IF EXISTS [dbo].[Util.SQLTypeToCSharp];
GO
");

        }
    }
}
