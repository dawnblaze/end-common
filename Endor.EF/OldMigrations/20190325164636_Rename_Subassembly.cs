using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Rename_Subassembly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [Part.Subassembly.Category.Action.CanDelete]
GO

/* 
========================================================
    Name: [Part.Subassembly.Category.Action.CanDelete]

    Description: This procedure checks if the SubassemblyCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Subassembly.Category.Action.CanDelete] @BID=1, @ID=1, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Subassembly.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
    Checks if the AssemblyCategory can be deleted. The boolean response is returned in the body.  A AssemblyCategory can be deleted if
    - there are no assemblies linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Subassembly.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Assembly Category Specified Does not Exist. AssemblyCategoryID=', @ID)

    -- there are no assemblies linked to it or if the force option is specified to deletes the links.
    ELSE IF  EXISTS( SELECT * FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Assembly Category is linked to a Assembly Data. AssemblyCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
UPDATE [System.Rights.Group.Menu.Tree]
SET Name = 'Can View and Manage Assemblies', 
    Description = 'Can View and Manage Assemblies',
    SearchTerms = 'Assemblies'
WHERE ID = 1044
");

            migrationBuilder.Sql(@"
UPDATE [System.Rights.Group]
SET Name = 'Management: Can View and Manage Assemblies'
WHERE ID = 1044
");

            migrationBuilder.Sql(@"
UPDATE [enum.ElementType]
SET Name = 'LinkedAssembly'
WHERE ID = 9
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [Part.Subassembly.Category.Action.CanDelete]
GO

/* 
========================================================
    Name: [Part.Subassembly.Category.Action.CanDelete]

    Description: This procedure checks if the SubassemblyCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Subassembly.Category.Action.CanDelete] @BID=1, @ID=1, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Subassembly.Category.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
    Checks if the SubassemblyCategory can be deleted. The boolean response is returned in the body.  A SubassemblyCategory can be deleted if
    - there are no subassemblies linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Subassembly.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Subassembly Category Specified Does not Exist. SubassemblyCategoryID=', @ID)

    -- there are no subassemblies linked to it or if the force option is specified to deletes the links.
    ELSE IF  EXISTS( SELECT * FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Subassembly Category is linked to a Subassembly Data. SubassemblyCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
");

            migrationBuilder.Sql(@"
UPDATE [System.Rights.Group.Menu.Tree]
SET Name = 'Can View and Manage SubAssemblies', 
    Description = 'Can View and Manage SubAssemblies',
    SearchTerms = 'SubAssemblies'
WHERE ID = 1044
");

            migrationBuilder.Sql(@"
UPDATE [System.Rights.Group]
SET Name = 'Management: Can View and Manage SubAssemblies'
WHERE ID = 1044
");

            migrationBuilder.Sql(@"
UPDATE [enum.ElementType]
SET Name = 'LinkedSubassembly'
WHERE ID = 9
");
        }
    }
}
