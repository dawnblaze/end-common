using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180413203901_InsertOrderDataRelatedRecords")]
    public partial class InsertOrderDataRelatedRecords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"INSERT [enum.Order.TransactionLevel] ([ID], [Name]) VALUES (1, N'Header');
INSERT [enum.Order.TransactionLevel] ([ID], [Name]) VALUES (2, N'Destination');
INSERT [enum.Order.TransactionLevel] ([ID], [Name]) VALUES (4, N'Item');
INSERT [enum.Order.TransactionLevel] ([ID], [Name]) VALUES (8, N'Part');
INSERT [enum.Order.TransactionLevel] ([ID], [Name]) VALUES (32, N'Master Header');
INSERT [enum.Order.TransactionLevel] ([ID], [Name]) VALUES (64, N'Linked Header');");

            migrationBuilder.Sql(@"INSERT [enum.Order.TransactionType] ([ID], [Name]) VALUES (1, N'Order');
INSERT [enum.Order.TransactionType] ([ID], [Name]) VALUES (2, N'Estimate');
INSERT [enum.Order.TransactionType] ([ID], [Name]) VALUES (4, N'Purchase Order');
INSERT [enum.Order.TransactionType] ([ID], [Name]) VALUES (32, N'Opportunity');
INSERT [enum.Order.TransactionType] ([ID], [Name]) VALUES (64, N'Destination');");

            migrationBuilder.Sql(@"INSERT [enum.Order.NoteType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (1, N'Other', 7, 7);
INSERT [enum.Order.NoteType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (2, N'Sales', 3, 7);
INSERT [enum.Order.NoteType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (4, N'Vendor', 3, 7);
INSERT [enum.Order.NoteType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (5, N'Design', 3, 5);
INSERT [enum.Order.NoteType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (6, N'Production', 3, 7);
INSERT [enum.Order.NoteType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (7, N'Customer', 3, 7);");
            
            migrationBuilder.Sql(@"INSERT [enum.Order.DestinationType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (0, N'Other', 7, 2);
INSERT [enum.Order.DestinationType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (1, N'Picked Up', 7, 2);
INSERT [enum.Order.DestinationType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (2, N'Emailed/Downloaded', 7, 2);
INSERT [enum.Order.DestinationType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (3, N'Delivered', 7, 2);
INSERT [enum.Order.DestinationType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (4, N'Installed', 7, 2);
INSERT [enum.Order.DestinationType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (5, N'On-Site Service', 7, 2);
INSERT [enum.Order.DestinationType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (11, N'Shipped', 7, 2);");

            migrationBuilder.Sql(@"INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (1, N'Created', 1, 0, 7, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (2, N'Voided', 1, 0, 7, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (3, N'Lost', 1, 0, 1, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (4, N'Converted', 1, 0, 1, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (5, N'Released To WIP', 1, 0, 2, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (6, N'Built', 1, 0, 2, 5);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (7, N'Released To Invoicing', 1, 0, 2, 3);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (8, N'Invoiced', 1, 0, 2, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (9, N'Closed', 1, 0, 6, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (10, N'Destination Pending', 1, 0, 2, 2);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (11, N'Destination Ready', 1, 0, 2, 2);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (12, N'Destination Complete', 1, 0, 2, 2);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (13, N'Requested', 1, 0, 4, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (14, N'Approved', 1, 0, 4, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (15, N'Ordered', 1, 0, 4, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (16, N'Received', 1, 0, 4, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (20, N'Design Due', 1, 1, 3, 5);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (21, N'Design Completed', 1, 1, 3, 5);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (22, N'Production Due', 1, 1, 3, 5);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (23, N'Shipping Due', 1, 1, 7, 3);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (24, N'Arrival Due', 1, 1, 7, 3);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (25, N'Arrived', 1, 1, 6, 2);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (26, N'Proposal Due', 1, 1, 3, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (27, N'Proposal Delivered', 1, 1, 3, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (28, N'Finance Charge Due', 1, 1, 2, 1);
INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) VALUES (29, N'Finance Charge Applied', 1, 0, 2, 1);");

            migrationBuilder.Sql(@"INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (11, N'Pending', 1, 0, 0, NULL, 0);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (16, N'Approved', 1, 0, 0, 16, 0);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (18, N'Lost', 1, 0, 0, 18, 0);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (19, N'Voided', 1, 0, 0, 19, 0);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (21, N'Pre-WIP', 2, 1, 1, NULL, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (22, N'WIP', 2, 0, 0, NULL, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (23, N'Built', 2, 1, 0, 23, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (24, N'Invoicing', 2, 0, 1, 24, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (25, N'Invoiced', 2, 0, 0, 25, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (26, N'Closed', 2, 0, 0, 26, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (29, N'Voided', 2, 0, 0, 29, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (41, N'Requested', 4, 0, 0, NULL, 0);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (43, N'Approved', 4, 0, 0, 43, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (45, N'Ordered', 4, 0, 0, 45, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (46, N'Received', 4, 0, 0, 46, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (49, N'Voided', 4, 0, 0, 49, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (61, N'Pending', 64, 0, 0, 61, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (63, N'Ready', 64, 0, 0, 63, 1);
INSERT [enum.Order.OrderStatus] ([ID], [Name], [TransactionType], [CanRename], [CanHide], [FixedItemStatusID], [EnableCustomItemSubStatus]) VALUES (66, N'Completed', 64, 0, 0, 66, 1);");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [enum.Order.OrderStatus];
DELETE FROM [enum.Order.KeyDateType];
DELETE FROM [enum.Order.DestinationType];
DELETE FROM [enum.Order.NoteType];
DELETE FROM [enum.Order.TransactionLevel];");
        }
    }
}

