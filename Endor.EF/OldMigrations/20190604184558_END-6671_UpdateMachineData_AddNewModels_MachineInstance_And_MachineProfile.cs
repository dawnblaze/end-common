using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6671_UpdateMachineData_AddNewModels_MachineInstance_And_MachineProfile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "ActiveInstanceCount",
                table: "Part.Machine.Data",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<int>(
                name: "MachineTemplateID",
                table: "Part.Machine.Data",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorksheetData",
                table: "Part.Machine.Data",
                type: "xml",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Part.Machine.Instance",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12033"),
                    ContractEndDate = table.Column<DateTime>(type: "date sparse", nullable: true),
                    ContractInfo = table.Column<string>(type: "nvarchar(MAX) sparse", nullable: true),
                    ContractNumber = table.Column<string>(type: "nvarchar(100) sparse", nullable: true),
                    ContractStartDate = table.Column<DateTime>(type: "date sparse", nullable: true),
                    HasServiceContract = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    IPAddress = table.Column<string>(maxLength: 100, nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    LocationID = table.Column<short>(type: "smallint", nullable: true),
                    MachineDataBID = table.Column<short>(nullable: true),
                    MachineDataID = table.Column<short>(nullable: true),
                    MachineID = table.Column<short>(nullable: false),
                    Manufacturer = table.Column<string>(maxLength: 100, nullable: true),
                    Model = table.Column<string>(maxLength: 100, nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    PurchaseDate = table.Column<DateTime>(type: "date", nullable: true),
                    SerialNumber = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Machine.Instance", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Machine.Instance_Part.Machine.Data",
                        columns: x => new { x.BID, x.MachineID },
                        principalTable: "Part.Machine.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Instance_Part.Machine.Data_MachineDataBID_MachineDataID",
                        columns: x => new { x.MachineDataBID, x.MachineDataID },
                        principalTable: "Part.Machine.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Part.Machine.Profile",
                columns: table => new
                {
                    BID = table.Column<short>(type: "smallint", nullable: false),
                    ID = table.Column<int>(nullable: false),
                    AssemblyDataJSON = table.Column<string>(type: "varchar(max)", nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12034"),
                    EstimatingCostPerHourFormula = table.Column<string>(type: "varchar(max)", nullable: true),
                    EstimatingPricePerHourFormula = table.Column<string>(type: "varchar(max)", nullable: true),
                    IsActive = table.Column<bool>(nullable: false, defaultValueSql: "1"),
                    IsDefault = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    MachineDataBID = table.Column<short>(nullable: true),
                    MachineDataID = table.Column<short>(nullable: true),
                    MachineID = table.Column<short>(nullable: false),
                    MachineTemplateID = table.Column<int>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(type: "nvarchar(100)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Machine.Profile", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile_Part.Machine.Data",
                        columns: x => new { x.BID, x.MachineID },
                        principalTable: "Part.Machine.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile_Part.Subassembly.Data",
                        columns: x => new { x.BID, x.MachineTemplateID },
                        principalTable: "Part.Subassembly.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile_Part.Machine.Data_MachineDataBID_MachineDataID",
                        columns: x => new { x.MachineDataBID, x.MachineDataID },
                        principalTable: "Part.Machine.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Data_Template",
                table: "Part.Machine.Data",
                columns: new[] { "BID", "MachineTemplateID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Instance_MachineDataBID_MachineDataID",
                table: "Part.Machine.Instance",
                columns: new[] { "MachineDataBID", "MachineDataID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Instance_Machine",
                table: "Part.Machine.Instance",
                columns: new[] { "BID", "MachineID", "IsActive", "Name" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile_BID_MachineTemplateID",
                table: "Part.Machine.Profile",
                columns: new[] { "BID", "MachineTemplateID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile_MachineDataBID_MachineDataID",
                table: "Part.Machine.Profile",
                columns: new[] { "MachineDataBID", "MachineDataID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile_Machine",
                table: "Part.Machine.Profile",
                columns: new[] { "BID", "MachineID", "IsActive", "Name", "IsDefault" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Data_Part.Subassembly.Data",
                table: "Part.Machine.Data",
                columns: new[] { "BID", "MachineTemplateID" },
                principalTable: "Part.Subassembly.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Data_Part.Subassembly.Data",
                table: "Part.Machine.Data");

            migrationBuilder.DropTable(
                name: "Part.Machine.Instance");

            migrationBuilder.DropTable(
                name: "Part.Machine.Profile");

            migrationBuilder.DropIndex(
                name: "IX_Part.Machine.Data_Template",
                table: "Part.Machine.Data");

            migrationBuilder.DropColumn(
                name: "ActiveInstanceCount",
                table: "Part.Machine.Data");

            migrationBuilder.DropColumn(
                name: "ActiveProfileCount",
                table: "Part.Machine.Data");

            migrationBuilder.DropColumn(
                name: "MachineTemplateID",
                table: "Part.Machine.Data");

            migrationBuilder.DropColumn(
                name: "WorksheetData",
                table: "Part.Machine.Data");
        }
    }
}
