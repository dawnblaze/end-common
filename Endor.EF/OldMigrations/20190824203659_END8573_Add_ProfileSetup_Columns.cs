using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8573_Add_ProfileSetup_Columns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EnableProfileOV",
                table: "Part.Subassembly.Variable",
                type: "bit",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<string>(
                name: "ProfileSetupHint",
                table: "Part.Subassembly.Variable",
                type: "NVARCHAR(255) SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProfileSetupLabel",
                table: "Part.Subassembly.Variable",
                type: "NVARCHAR(255) SPARSE",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "EnableProfileOV",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "ProfileSetupHint",
                table: "Part.Subassembly.Variable");

            migrationBuilder.DropColumn(
                name: "ProfileSetupLabel",
                table: "Part.Subassembly.Variable");

        }
    }
}
