﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9845_Add_SystemMessageTemplateType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "System.Message.TemplateType",
                columns: table => new
                {
                    AppliesToClassTypeID = table.Column<int>(type: "int", nullable: false),
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(type: "varchar(100)", nullable: false),
                    IsSystem = table.Column<bool>(type: "bit", nullable: false),
                    ChannelType = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_System.Message.TemplateType", x => new { x.AppliesToClassTypeID, x.ID });
                    table.ForeignKey(
                        name: "FK_System.Message.TemplateType_enum.Message.ChannelType",
                        column: x => x.ChannelType,
                        principalTable: "enum.Message.ChannelType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_System.Message.TemplateType_ChannelType",
                table: "System.Message.TemplateType",
                column: "ChannelType");

            migrationBuilder.Sql(@"
                INSERT INTO [System.Message.TemplateType] ( [AppliesToClassTypeID], [ID], [Name], [IsSystem], [ChannelType] )
                VALUES
                        (  1012, 1, 'New Employee Login Email', 1, 4 )
                        , (  1012, 2, 'Employee Password Reset Email', 1, 4 )
                        , (  1012, 11, 'New Contact Login Email', 1, 4 )
                        , (  1012, 12, 'Contact Password Reset Email', 1, 4 )
                        , (  2000, 0, 'General Contact Email', 0, 4 )
                        , (  2000, 1, 'Send Statement Email', 0, 4 )
                        , (  3000, 0, 'General Company Email', 0, 4 )
                        , (  5000, 0, 'General Email', 0, 4 )
                        , ( 10000, 0, 'General Order Email', 0, 4 )
                        , ( 10000, 1, 'Send Invoice Email', 0, 4 )
                        , ( 10000, 2, 'Send Work Order Email', 0, 4 )
                        , ( 10000, 3, 'Send Packing Slip Email', 0, 4 )
                        , ( 10000, 4, 'New Order Confirmation Email', 0, 4 )
                        , ( 10000, 5, 'New Ecommerce Order Confirmation Email', 0, 4 )
                        , ( 10000, 6, 'Order Ready for Pickup Email', 0, 4 )
                        , ( 10000, 7, 'Shipping Confirmation Email', 0, 4 )
                        , ( 10000, 8, 'Dunning Email', 0, 4 )
                        , ( 10200, 0, 'General Estimate Email', 0, 4 )
                        , ( 10200, 1, 'Send Estimate Email', 0, 4 )
                        , ( 10200, 2, 'Estimate Followup Email', 0, 4 )
                        , ( 10300, 0, 'General Credit Memo Email', 0, 4 )
                        , ( 10300, 1, 'Send Credit Memo Email', 0, 4 )
                        , ( 10400, 0, 'General Purchase Order Email', 0, 4 )
                        , ( 10400, 1, 'Send Purchase Order Email', 0, 4 )
                        ;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "System.Message.TemplateType");
        }
    }
}
