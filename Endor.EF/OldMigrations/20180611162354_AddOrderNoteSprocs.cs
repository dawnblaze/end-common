using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180611162354_AddOrderNoteSprocs")]
    public partial class AddOrderNoteSprocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.ChangeNote];
GO

/*********************************************************
  Name:
    [Order.Action.ChangeNote]( @BID tinyint, @OrderID int, @OrderItemID int = null, @DestinationID int = null, @NoteType tinyint, @Note varchar(max), @NoteParts xml, @NoteCount smallint )
  
  Description:
    This function sets a note of an order
  
  Sample Use:
    EXEC [dbo].[Order.Action.ChangeNote] @BID=1, @OrderID=1, @OrderItemID=1, @DestinationID=1, @NoteType=1, @Note=null, @NoteParts=null, @NoteCount=1;
*********************************************************/
CREATE PROCEDURE [dbo].[Order.Action.ChangeNote]
(
--DECLARE
    @BID            TINYINT		--= 1
  , @OrderID        INT			--= 1049
  , @OrderItemID	INT			= NULL
  , @DestinationID	INT			= NULL
  , @NoteType       TINYINT     = 1
  , @Note           VARCHAR(MAX)
  , @NoteParts      XML
  , @NoteCount      SMALLINT
  
  , @Result         INT			= NULL  OUTPUT
)
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update record if exists
    IF (@OrderItemID IS NULL AND @DestinationID IS NULL)
    BEGIN
        UPDATE N
        SET    Note = @Note, NoteCount = @NoteCount, NoteParts = @NoteParts
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID IS NULL AND DestinationID IS NULL
    END
    ELSE IF (@OrderItemID IS NOT NULL AND @DestinationID IS NULL)
    BEGIN
        UPDATE N
        SET    Note = @Note, NoteCount = @NoteCount, NoteParts = @NoteParts
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID = @OrderItemID AND DestinationID IS NULL
    END
    ELSE IF (@OrderItemID IS NULL AND @DestinationID IS NOT NULL)
    BEGIN
        UPDATE N
        SET    Note = @Note, NoteCount = @NoteCount, NoteParts = @NoteParts
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID IS NULL AND DestinationID = @DestinationID
    END
    ELSE
    BEGIN
        UPDATE N
        SET    Note = @Note, NoteCount = @NoteCount, NoteParts = @NoteParts
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID = @OrderItemID AND DestinationID = @DestinationID
    END

    -- Add record if it does not exists
    IF @@ROWCOUNT = 0
    BEGIN
        DECLARE @NewID INT;
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10011, 1;

        INSERT INTO [Order.Note]
        (BID, ID, OrderID, OrderItemID, DestinationID, NoteType, Note, NoteCount, NoteParts)
        VALUES
        (@BID, @NewID, @OrderID, @OrderItemID, @DestinationID, @NoteType, @Note, @NoteCount, @NoteParts)
    END;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END


GO


DROP PROCEDURE IF EXISTS [dbo].[Order.Action.RemoveNote];
GO

/*********************************************************
  Name:
    [Order.Action.RemoveNote]( @BID tinyint, @OrderID int, @OrderItemID int = null, @DestinationID int = null )
  
  Description:
    This function removes a note of an order
  
  Sample Use:
    EXEC [dbo].[Order.Action.RemoveNote] @BID=1, @OrderID=1, @OrderItemID=1, @DestinationID=1, @NoteType=1
*********************************************************/
CREATE PROCEDURE [dbo].[Order.Action.RemoveNote]
(
-- DECLARE 
    @BID            TINYINT --= 1
  , @OrderID        INT     --= 2
  , @OrderItemID	INT		= NULL
  , @DestinationID	INT		= NULL
  , @NoteType       TINYINT --= 1

  , @Result         INT     = NULL  OUTPUT
)
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF (@OrderItemID IS NULL AND @DestinationID IS NULL)
    BEGIN
        DELETE FROM N
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID IS NULL AND DestinationID IS NULL
    END
    ELSE IF (@OrderItemID IS NOT NULL AND @DestinationID IS NULL)
    BEGIN
        DELETE FROM N
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID = @OrderItemID AND DestinationID IS NULL
    END
    ELSE IF (@OrderItemID IS NULL AND @DestinationID IS NOT NULL)
    BEGIN
        DELETE FROM N
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID IS NULL AND DestinationID = @DestinationID
    END
    ELSE
    BEGIN
        DELETE FROM N
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID = @OrderItemID AND DestinationID = @DestinationID
    END

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.ChangeNote];
GO
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.RemoveNote];
");
        }
    }
}

