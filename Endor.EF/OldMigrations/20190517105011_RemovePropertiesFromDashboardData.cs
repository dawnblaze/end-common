using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class RemovePropertiesFromDashboardData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data");

            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data");

            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "Dashboard.Data");

            migrationBuilder.DropColumn(
                name: "IsShared",
                table: "Dashboard.Data");

            migrationBuilder.DropColumn(
                name: "IsSystem",
                table: "Dashboard.Data");

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data",
                columns: new[] { "BID", "Module" });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data",
                columns: new[] { "BID", "UserLinkID", "Module", "IsActive" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data");

            migrationBuilder.DropIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data");

            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "Dashboard.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsShared",
                table: "Dashboard.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsSystem",
                table: "Dashboard.Data",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_Module",
                table: "Dashboard.Data",
                columns: new[] { "BID", "Module", "IsSystem" });

            migrationBuilder.CreateIndex(
                name: "IX_Dashboard.Data_User",
                table: "Dashboard.Data",
                columns: new[] { "BID", "UserLinkID", "Module", "IsActive", "IsDefault" });
        }
    }
}
