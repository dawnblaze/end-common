﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10100Create_migration_to_add_early_payment_date : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/*
Name:
	[Order.Action.ComputeEarlyPaymentDate]( @BID tinyint, @OrderID int)
  
Description:
	This function computes early payment date of an order

Rules:
	** Delete any existing if it is there.

	** If the EarlyPaymentPercent or EarlyPaymentDays is NULL, then do not create an early payment keydate.

	** The Early Payment Date is (Order.InvoicedDate + EarlyPaymentDays).

	** If the Early Payment Date is past the current UTC DateTime, then do not save the key date.  

	** Save the Early Payment Date in UTC time.

Story:
	** https://corebridge.atlassian.net/browse/END-10014

Created Date:
	11/4/19

Modified Date:
	11/5/19

*/
CREATE OR ALTER PROC [dbo].[Order.Action.ComputeEarlyPaymentDate]
    (
    --DECLARE
        @BID            TINYINT		--= 1
      , @OrderID        INT			--= 1049
    )
AS
BEGIN

	DECLARE @EarlyPaymentDate DATE = null;
	DECLARE @EarlyPaymentDateEnum INT = 34;
	DECLARE @EarlyPaymentDays SMALLINT;
	DECLARE @EarlyPaymentPercent DECIMAL;
	DECLARE @OrderStatusID INT;
	DECLARE @KeyDate DATE = null;

	/* When the order is saved, delete Early Payment Key Date if it is present and should be NULL */
	IF(EXISTS(SELECT 1 FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = @EarlyPaymentDateEnum))
	BEGIN
		DELETE FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = @EarlyPaymentDateEnum; --Early Payment Date
	END

	/************ Get EarlyPaymentDays, EarlyPaymentPercent and OrderStatusID ******/
	SELECT 
		@EarlyPaymentDays = APT.EarlyPaymentDays,
		@EarlyPaymentPercent = APT.EarlyPaymentPercent,
		@OrderStatusID = O.OrderStatusID
	FROM
		[Order.Data] O 
		LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
		LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
	WHERE O.BID = @BID AND O.ID = @OrderID

	/***** If the EarlyPaymentPercent or EarlyPaymentDays is NULL, then do not create an early payment keydate. ******/
	IF((@EarlyPaymentDays IS NULL) OR (@EarlyPaymentPercent IS NULL))
		SET @EarlyPaymentDate = NULL;

	/***** The Early Payment Date is (Order.InvoicedDate + EarlyPaymentDays). *****/
	SELECT TOP 1 @KeyDate = KeyDate FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = 8;

	IF(@KeyDate IS NULL)
		SET @EarlyPaymentDate = @KeyDate;
	ELSE
		SET @EarlyPaymentDate = DATEADD(DAY, @EarlyPaymentDays, @KeyDate);

	/*** If the Early Payment Date is past the current UTC DateTime, then do not save the key date. ***/
	IF(CONVERT(DATE, @EarlyPaymentDate) < CONVERT(DATE,GETUTCDATE()))
		SET @EarlyPaymentDate = NULL;

	-- RETURN 
	SELECT EarlyPaymentDate = @EarlyPaymentDate
END


");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
