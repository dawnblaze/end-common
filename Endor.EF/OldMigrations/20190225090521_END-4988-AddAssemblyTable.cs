using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4988AddAssemblyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Assembly.Table",
                columns: table => new
                {
                    ID = table.Column<short>(nullable: false),
                    AssemblyID = table.Column<int>(nullable: false),
                    BID = table.Column<short>(nullable: false),
                    CellDataJSON = table.Column<string>(nullable: true),
                    CellDataType = table.Column<short>(type: "smallint", nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false),
                    ColumnCount = table.Column<short>(type: "smallint", nullable: false),
                    ColumnDataType = table.Column<short>(type: "smallint", nullable: true),
                    ColumnIsSorted = table.Column<bool>(nullable: false),
                    ColumnLabel = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    ColumnMatchType = table.Column<byte>(type: "tinyint", nullable: false),
                    ColumnUnitID = table.Column<byte>(type: "tinyint SPARSE", nullable: true),
                    ColumnValuesJSON = table.Column<string>(nullable: true),
                    ColumnVariableID = table.Column<int>(nullable: true),
                    Description = table.Column<string>(type: "nvarchar(4000)", maxLength: 4000, nullable: true),
                    IsTierTable = table.Column<bool>(nullable: true, computedColumnSql: "isnull(case when [TableType]>=(1) AND [TableType]<=(4) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))"),
                    Label = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    RowCount = table.Column<short>(type: "smallint", nullable: false),
                    RowDataType = table.Column<short>(type: "smallint", nullable: false),
                    RowDefaultMarkupJSON = table.Column<string>(nullable: true),
                    RowIsSorted = table.Column<bool>(nullable: false),
                    RowLabel = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: true),
                    RowMatchType = table.Column<byte>(type: "tinyint", nullable: false),
                    RowUnitID = table.Column<byte>(type: "tinyint SPARSE", nullable: true),
                    RowValuesJSON = table.Column<string>(nullable: true),
                    RowVariableID = table.Column<int>(nullable: true),
                    SubassemblyDataBID = table.Column<short>(nullable: true),
                    SubassemblyDataID = table.Column<int>(nullable: true),
                    TableType = table.Column<byte>(type: "tinyint", nullable: false),
                    VariableName = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Assembly.Table", x => new { x.BID, x.ID });
                //table.ForeignKey(
                //    name: "FK_Part.Assembly.Table_Part.Subassembly.Data_SubassemblyDataBID_SubassemblyDataID",
                //    columns: x => new { x.SubassemblyDataBID, x.SubassemblyDataID },
                //    principalTable: "Part.Subassembly.Data",
                //    principalColumns: new[] { "BID", "ID" },
                //    onDelete: ReferentialAction.Restrict);

                    table.ForeignKey(
                        name: "FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType",
                        column: x => x.RowMatchType,
                        principalTable: "enum.Part.Subassembly.Table.MatchType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);

                    table.ForeignKey(
                        name: "FK_Part.Assembly.Table_enum.Part.Subassembly.Table.MatchType1",
                        column: x => x.ColumnMatchType,
                        principalTable: "enum.Part.Subassembly.Table.MatchType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Table_enum.Part.Subassembly.TableType",
                        column: x => x.TableType,
                        principalTable: "enum.Part.Subassembly.TableType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Table_Part.Subassembly.Data",
                        columns: x => new { x.BID, x.AssemblyID },
                        principalTable: "Part.Subassembly.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Table_Part.Subassembly.Variable",
                        columns: x => new { x.BID, x.RowVariableID },
                        principalTable: "Part.Subassembly.Variable",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Table_Part.Subassembly.Variable1",
                        columns: x => new { x.BID, x.ColumnVariableID },
                        principalTable: "Part.Subassembly.Variable",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Table_ColumnUnit",
                        column: x => x.ColumnUnitID,
                        principalTable: "enum.Part.Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Assembly.Table_RowUnit",
                        column: x => x.RowUnitID,
                        principalTable: "enum.Part.Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);

                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Assembly.Table_Subassembly",
                table: "Part.Assembly.Table",
                columns: new[] { "BID", "AssemblyID", "Label" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Assembly.Table");
        }
    }
}
