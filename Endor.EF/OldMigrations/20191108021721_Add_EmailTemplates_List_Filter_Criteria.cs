﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Add_EmailTemplates_List_Filter_Criteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                VALUES
                    (14220 --<TargetClassTypeID, int,>
                    ,'Name' --<Name, varchar(255),>
                    ,'Name' --<Label, varchar(255),>
                    ,'Name' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,1 --<DataType, tinyint,>
                    ,0 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,NULL --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,0 --<IsLimitToList, bit,>
                    ,1 --<SortIndex, tinyint,>
                    ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                    );
                GO

                INSERT INTO [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                VALUES
                    (14220 --<TargetClassTypeID, int,>
                    ,'Tooltip' --<Name, varchar(255),>
                    ,'Tooltip' --<Label, varchar(255),>
                    ,'Description' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,1 --<DataType, tinyint,>
                    ,0 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,NULL --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,0 --<IsLimitToList, bit,>
                    ,2 --<SortIndex, tinyint,>
                    ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                    );
                GO

                INSERT INTO [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                VALUES
                    (14220 --<TargetClassTypeID, int,>
                    ,'Owner' --<Name, varchar(255),>
                    ,'Owner' --<Label, varchar(255),>
                    ,'EmployeeID' --<Field, varchar(255),>
                    ,0 --<IsHidden, bit,>
                    ,2 --<DataType, tinyint,>
                    ,3 --<InputType, tinyint,>
                    ,0 --<AllowMultiple, bit,>
                    ,NULL --<ListValues, varchar(max),>
                    ,NULL --<ListValuesEndpoint, varchar(255),>
                    ,0 --<IsLimitToList, bit,>
                    ,3 --<SortIndex, tinyint,>
                    ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                    );
                GO

                DECLARE @NextID INT = 1058;

                INSERT INTO [dbo].[List.Filter]
                ([BID], [ID], [IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (-1, @NextID, 1,'Order',14220,null,'<ArrayOfListFilterItem>
                  <ListFilterItem>
                    <SearchValue>10000</SearchValue>
                    <Field>AppliesToClassTypeID</Field>
                    <IsHidden>true</IsHidden>
                    <IsSystem>true</IsSystem>
                  </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,0)
                ;
                SET @NextID += 1;

                INSERT INTO [dbo].[List.Filter]
                ([BID], [ID], [IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (-1, @NextID, 1,'Estimate',14220,null,'<ArrayOfListFilterItem>
                  <ListFilterItem>
                    <SearchValue>10200</SearchValue>
                    <Field>AppliesToClassTypeID</Field>
                    <IsHidden>true</IsHidden>
                    <IsSystem>true</IsSystem>
                  </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,1)
                ;
                SET @NextID += 1;

                INSERT INTO [dbo].[List.Filter]
                ([BID], [ID], [IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (-1, @NextID, 1,'Company',14220,null,'<ArrayOfListFilterItem>
                  <ListFilterItem>
                    <SearchValue>2000</SearchValue>
                    <Field>AppliesToClassTypeID</Field>
                    <IsHidden>true</IsHidden>
                    <IsSystem>true</IsSystem>
                  </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,2)
                ;
                SET @NextID += 1;

                INSERT INTO [dbo].[List.Filter]
                ([BID], [ID], [IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (-1, @NextID, 1,'Contact',14220,null,'<ArrayOfListFilterItem>
                  <ListFilterItem>
                    <SearchValue>3000</SearchValue>
                    <Field>AppliesToClassTypeID</Field>
                    <IsHidden>true</IsHidden>
                    <IsSystem>true</IsSystem>
                  </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,3)
                ;
                SET @NextID += 1;

                INSERT INTO [dbo].[List.Filter]
                ([BID], [ID], [IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (-1, @NextID, 1,'Opportunity',14220,null,'<ArrayOfListFilterItem>
                  <ListFilterItem>
                    <SearchValue>9000</SearchValue>
                    <Field>AppliesToClassTypeID</Field>
                    <IsHidden>true</IsHidden>
                    <IsSystem>true</IsSystem>
                  </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,4)
                ;
                SET @NextID += 1;

                INSERT INTO [dbo].[List.Filter]
                ([BID], [ID], [IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (-1, @NextID, 1,'Purchase Order',14220,null,'<ArrayOfListFilterItem>
                  <ListFilterItem>
                    <SearchValue>10100</SearchValue>
                    <Field>AppliesToClassTypeID</Field>
                    <IsHidden>true</IsHidden>
                    <IsSystem>true</IsSystem>
                  </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,5)
                ;
                SET @NextID += 1;

                INSERT INTO [dbo].[List.Filter]
                ([BID], [ID], [IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (-1, @NextID, 1,'Employee',14220,null,'<ArrayOfListFilterItem>
                  <ListFilterItem>
                    <SearchValue>5000</SearchValue>
                    <Field>AppliesToClassTypeID</Field>
                    <IsHidden>true</IsHidden>
                    <IsSystem>true</IsSystem>
                  </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,6)
                ;
                SET @NextID += 1;

                INSERT INTO [dbo].[List.Filter]
                ([BID], [ID], [IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
                VALUES
                (-1, @NextID, 1,'General',14220,null,'<ArrayOfListFilterItem>
                  <ListFilterItem>
                    <SearchValue>5000</SearchValue>
                    <Field>AppliesToClassTypeID</Field>
                    <IsHidden>true</IsHidden>
                    <IsSystem>true</IsSystem>
                  </ListFilterItem>
                </ArrayOfListFilterItem>',null,0,1,null,1,7)
                ;

                EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName='List.Filter';
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
