using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateOrderNotesFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NoteCount",
                table: "Order.Note");

            migrationBuilder.DropColumn(
                name: "NoteParts",
                table: "Order.Note");

            migrationBuilder.AddColumn<int>(
                name: "ContactID",
                table: "Order.Note",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDT",
                table: "Order.Note",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<short>(
                name: "EmployeeID",
                table: "Order.Note",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Order.Note",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Order.Note_BID_ContactID",
                table: "Order.Note",
                columns: new[] { "BID", "ContactID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Note_BID_EmployeeID",
                table: "Order.Note",
                columns: new[] { "BID", "EmployeeID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Note_ContactID",
                table: "Order.Note",
                columns: new[] { "BID", "ContactID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Note_EmployeeID",
                table: "Order.Note",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Note_ContactID",
                table: "Order.Note");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.Note_EmployeeID",
                table: "Order.Note");

            migrationBuilder.DropIndex(
                name: "IX_Order.Note_BID_ContactID",
                table: "Order.Note");

            migrationBuilder.DropIndex(
                name: "IX_Order.Note_BID_EmployeeID",
                table: "Order.Note");

            migrationBuilder.DropColumn(
                name: "ContactID",
                table: "Order.Note");

            migrationBuilder.DropColumn(
                name: "CreatedDT",
                table: "Order.Note");

            migrationBuilder.DropColumn(
                name: "EmployeeID",
                table: "Order.Note");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Order.Note");

            migrationBuilder.AddColumn<short>(
                name: "NoteCount",
                table: "Order.Note",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<string>(
                name: "NoteParts",
                table: "Order.Note",
                type: "xml",
                nullable: true);
        }
    }
}
