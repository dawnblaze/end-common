using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddTaxabilityCodeSysOptCatRecord : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            INSERT INTO [dbo].[System.Option.Category]
            ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
            VALUES
            (24, 'Taxability Codes', 300, 'Taxability Codes', 2, 0, 'Tax Exempt Taxability Codes AvaTax TaxJar Items NonTaxable')
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
