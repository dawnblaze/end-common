using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Update_SubassemblyElement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "Label",
                table: "Part.Subassembly.Element");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Part.Subassembly.Element",
                nullable: true);

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Part.Subassembly.Element",
                maxLength: 255,
                nullable: true);

            migrationBuilder.DropColumn(
                name: "Label",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddColumn<string>(
                name: "Label",
                table: "Part.Subassembly.Element",
                maxLength: 255,
                nullable: true);
        }
    }
}
