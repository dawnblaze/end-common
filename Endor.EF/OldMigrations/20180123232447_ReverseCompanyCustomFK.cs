using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180123232447_ReverseCompanyCustomFK")]
    public partial class ReverseCompanyCustomFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company.Data_Company.Custom",
                table: "Company.Data");

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Custom.Data_Company",
                table: "Company.Custom",
                columns: new[] { "BID", "ID" },
                principalTable: "Company.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company.Custom.Data_Company",
                table: "Company.Custom");

            migrationBuilder.AddForeignKey(
                name: "FK_Company.Data_Company.Custom",
                table: "Company.Data",
                columns: new[] { "BID", "ID" },
                principalTable: "Company.Custom",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}

