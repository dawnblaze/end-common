﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_9875_UpdateOptionStoredProcedures : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // update Option.GetValue
            migrationBuilder.Sql(@"
-- ========================================================
/* 
Name: [Option.GetValue]

Description: This procedure looks up a option/setting based on a hierarchy of possibilities.
    Either the AID or BID is required.
    The system gathers uses this approach to get the option value:
    * Pull data that matches the parameters specified
    * sorts based on specific vs general
    * take the top value.

Sample Uses:   

    EXEC dbo.[Option.GetValue] @OptionName = 'GLAccount.TaxName1', @BID=1, @LocationID = 45;

    EXEC dbo.[Option.GetValue] @OptionName = 'Order.Clone.Prefix', @BID=1;

    EXEC dbo.[Option.GetValue] @OptionID = 6054, @BID=1;  -- Same as Order.Clone.Prefix

Returns:  Value varchar(MAX), OptionLevel TINYINT
    OPTION LEVELS
        ID	Name
        0   Default
        1   System
        2   Association
        4   Business
        8   Location
        16  Storefront
        32  Employee (UserLink)
        64  Company
        128 Contact
*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValue]
--DECLARE 
	  @OptionID       INT          = NULL -- = 2
	, @OptionName     VARCHAR(255) = NULL -- 'GLAccount.TaxName1'

	, @BID            smallint     
	, @LocationID     smallint     = NULL
	, @StoreFrontID   smallint     = NULL
	, @UserLinkID     smallint     = NULL
	, @CompanyID      int          = NULL
	, @ContactID      int          = NULL
AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
            , @Level  TINYINT     
            ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition]
        WHERE Name = @OptionName
        ;

        -- If not found, assume AdHoc that has never been saved and return nothing
        IF (@OptionID IS NULL)
        BEGIN
            SELECT CONVERT(SMALLINT, NULL) AS ID -- needed in the EF
                , @Result as [Value]
                , @Level as [OptionLevel];
            RETURN;
        END;

    END;

    IF ( IIF(@LocationID   IS NULL, 0, 1) 
        + IIF(@UserLinkID   IS NULL, 0, 1)
        + IIF(@CompanyID    IS NULL AND @ContactID IS NULL, 0, 1)
        + IIF(@StoreFrontID IS NULL, 0, 1) > 1 )
        THROW 180000, 'You can only specify one of the following: @LocationID, @UserLinkID, (@CompanyID and/or @ContactID), or @StorefrontID.', 1;

    -- If a contact is specified but not a Company, take the first Company that is active (sorted alpha) if there are multiple
    IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
        SELECT TOP(1) @CompanyID = Company.ID
                , @LocationID = Company.LocationID
        FROM [Company.Data] Company
        JOIN [Company.Contact.Link] CL ON CL.BID = Company.BID AND CL.CompanyID = Company.ID
        JOIN [Contact.Data] Contact ON Contact.BID = CL.BID AND Contact.ID = CL.ContactID
        WHERE Contact.BID = @BID AND Contact.ID = @ContactID
        ORDER BY CL.IsActive DESC, Company.Name ASC
        ;

    IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
        SELECT @LocationID = Company.LocationID
        FROM [Company.Data] Company
        WHERE Company.BID = @BID AND Company.ID = @CompanyID 
        ;

    IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
        SELECT @LocationID = E.LocationID
        FROM [Employee.Data] E
        JOIN [User.Link] UL on UL.BID = E.BID AND UL.EmployeeID = E.ID
        WHERE UL.BID = @BID AND UL.ID = @UserLinkID
        ;

    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    System=1
    --    Association=1
    --    Business=2
    --    Location=4
    --    Storefront=8
    --    Employee=16
    --    Company=32
    --    Contact=64

    SELECT TOP(1) @Result = Value, @Level = OptionLevel
    FROM [Option.Data] O
    WHERE O.BID = @BID 
        AND O.OptionID = @OptionID
        AND ( ( COALESCE(O.ContactID, O.CompanyID, O.StorefrontID, O.LocationID, O.UserLinkID) IS NULL )
            OR ( O.ContactID    = @ContactID )
            OR ( O.CompanyID    = @CompanyID  AND (O.ContactID IS NULL OR @ContactID = O.ContactID) )
            OR ( O.StorefrontID = @StorefrontID AND (O.ContactID IS NULL OR @ContactID = O.ContactID) )
            OR ( O.LocationID   = @LocationID )
            OR ( O.UserLinkID   = @UserLinkID ) AND (O.LocationID IS NULL OR ( O.LocationID   = @LocationID) )
            )
    ORDER BY O.OptionLevel DESC;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data] O
        WHERE O.BID IS NULL AND O.OptionID = @OptionID
        AND O.AssociationID = (SELECT AssociationType from [Business.Data] WHERE BID = @BID)
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue, @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    SELECT 
        CAST (@OptionID AS SMALLINT) AS ID -- needed in the EF
        , @Result as [Value]
        , @Level as [OptionLevel];
END
");
            // update Option.GetValues
            migrationBuilder.Sql(@"
-- ========================================================
/* 
Name: [Option.GetValues]

Description: This procedure looks up a option/setting based on a hierarchy of possibilities.

Sample Use:   
    EXEC dbo.[Option.GetValues] @CategoryID = 12, @BID=1, @LocationID = 1

Returns: Table with columns
    [OptionID] INT
    [Name] VARCHAR(255),
    [Value] VARCHAR(255),
    [Level] TINYINT,
    [Label] VARCHAR(255),
    [Description] VARCHAR(MAX),
    [DataType] SMALLINT,
    [CategoryID] SMALLINT,
    [ListValues] VARCHAR(MAX)
    [IsHidden] BIT

    OPTION LEVELS
        ID	Name
        0   Default
        1   System
        2   Association
        4   Business
        8   Location
        16  Storefront
        32  Employee (UserLink)
        64  Company
        128 Contact
*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValues]
-- DECLARE
	  @CategoryID     smallint     = NULL

	, @BID            smallint     
	, @LocationID     smallint     = NULL
	, @StorefrontID   smallint     = NULL
	, @UserLinkID     smallint     = NULL
	, @CompanyID      int          = NULL
	, @ContactID      int          = NULL
AS
BEGIN
    -- Define the Results
    DECLARE @Result TABLE (   [OptionID] INT PRIMARY KEY
                            , [Name] VARCHAR(255)
                            , [Value] NVARCHAR(255) INDEX IX_Result_Value WHERE [Value] IS NULL
                            , [Level] TINYINT
                            , [Label] VARCHAR(255)
                            , [Description] VARCHAR(MAX)
                            , [DataType] SMALLINT
                            , [CategoryID] SMALLINT
                            , [ListValues] VARCHAR(MAX)
                            , [IsHidden] BIT
                            );

    -- Define a list of Categories to JOIN to get the results
    DECLARE @Categories TABLE( ID SMALLINT PRIMARY KEY  );
    IF (@CategoryID IS NULL)
        INSERT INTO @Categories SELECT ID FROM [System.Option.Category]
    ELSE
        INSERT INTO @Categories SELECT @CategoryID
    ;

    -- Now Fill the List with Option Definitions
    INSERT INTO @Result 
        SELECT DEF.[ID] as [OptionID]
            , DEF.[Name]
            , DEF.[DefaultValue] AS [Value] 
            , 0 AS [Level]
            , DEF.[Label]
            , DEF.Description
            , DEF.DataType
            , CAT.ID as CategoryID
            , DEF.ListValues
            , DEF.IsHidden

        FROM @Categories CAT
        JOIN [System.Option.Definition] DEF ON DEF.CategoryID = CAT.ID
    ;

    IF ( IIF(@LocationID   IS NULL, 0, 1) 
        + IIF(@UserLinkID   IS NULL, 0, 1)
        + IIF(@CompanyID    IS NULL AND @ContactID IS NULL, 0, 1)
        + IIF(@StoreFrontID IS NULL, 0, 1) > 1 )
        THROW 180000, 'You can only specify one of the following: @LocationID, @UserLinkID, (@CompanyID and/or @ContactID), or @StorefrontID.', 1;

    -- If a contact is specified but not a Company, take the first Company that is active (sorted alpha) if there are multiple
    IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
        SELECT TOP(1) @CompanyID = Company.ID
                , @LocationID = Company.LocationID
        FROM [Company.Data] Company
        JOIN [Company.Contact.Link] CL ON CL.BID = Company.BID AND CL.CompanyID = Company.ID
        JOIN [Contact.Data] Contact ON Contact.BID = CL.BID AND Contact.ID = CL.ContactID
        WHERE Contact.BID = @BID AND Contact.ID = @ContactID
        ORDER BY CL.IsActive DESC, Company.Name ASC
        ;

    IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
        SELECT @LocationID = Company.LocationID
        FROM [Company.Data] Company
        WHERE Company.BID = @BID AND Company.ID = @CompanyID 
        ;

    IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
        SELECT @LocationID = E.LocationID
        FROM [Employee.Data] E
        JOIN [User.Link] UL on UL.BID = E.BID AND UL.EmployeeID = E.ID
        WHERE UL.BID = @BID AND UL.ID = @UserLinkID
        ;

    -- Now look up option value based on the specific instance
    UPDATE RES
    SET [Value] = OPT.[Value]
        , [Level] = OPT.[OptionLevel]
    FROM  @Result RES
    JOIN 
    (
        SELECT TOP(1) O.OptionID, O.Value, O.OptionLevel
        FROM [Option.Data] O
        WHERE O.BID = @BID 
        AND ( ( COALESCE(O.ContactID, O.CompanyID, O.StorefrontID, O.LocationID, O.UserLinkID) IS NULL )
            OR ( O.ContactID    = @ContactID )
            OR ( O.CompanyID    = @CompanyID  AND (O.ContactID IS NULL OR @ContactID = O.ContactID) )
            OR ( O.StorefrontID = @StorefrontID AND (O.ContactID IS NULL OR @ContactID = O.ContactID) )
            OR ( O.LocationID   = @LocationID)
            OR ( O.UserLinkID   = @UserLinkID ) AND (O.LocationID IS NULL OR ( O.LocationID   = @LocationID) )
            )
        ORDER BY O.OptionLevel DESC
    ) OPT ON OPT.OptionID = RES.OptionID
    ;

    -- Check Association Separately because it does not use the BID
    UPDATE RES
    SET [Value] = O.[Value] , [Level] = O.[OptionLevel]
    FROM @Result RES
    JOIN [Option.Data] O ON RES.OptionID = O.OptionID
    WHERE RES.[Level] = 0
      AND O.AssociationID = (SELECT AssociationType from [Business.Data] WHERE BID = @BID)

    -- Now return what you found!
    SELECT *
    FROM @Result;
END
");
            // update Option.SaveValue
            migrationBuilder.Sql(@"
-- ========================================================
/*
Name: [Option.SaveValue]

Description: This procedure saves the setting for an option/setting.

Sample Use:   

    EXEC dbo.[Option.SaveValue] 
        -- Require Fields
          @OptionName       = 'GLAccount.TaxName2'
        , @OptionID         = NULL
        , @Value            = 'NewValue'  -- Pass in NULL to delete the current value (if any)

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @UserLinkID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL
*/
-- ========================================================
ALTER   PROCEDURE [dbo].[Option.SaveValue]
-- DECLARE 
      @OptionID       INT          = NULL
    , @OptionName     VARCHAR(255) = NULL
    , @Value          VARCHAR(MAX) = NULL     -- Pass in NULL to delete the current value (if any)

    , @AssociationID  TINYINT      = NULL
    , @BID            SMALLINT     = NULL
    , @LocationID     SMALLINT     = NULL
    , @UserLinkID     SMALLINT     = NULL
    , @CompanyID      INT          = NULL
    , @ContactID      INT          = NULL
    , @StorefrontID   SMALLINT	   = NULL

AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    -- Check if exactly one is used (hurray, a legitimate use for XOR! though we can do it without)
    IF ( IIF(@OptionName IS NULL, 0, 1) = IIF(@OptionID IS NULL, 0, 1) )
        THROW 180000, 'You must specify either (and only one) of the @OptionID or the @OptionName.', 1;

    IF ( IIF(@BID IS NULL, 0, 1) = IIF(@AssociationID IS NULL, 0, 1) )
        THROW 180000, 'You must specify either (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserLinkID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL AND @ContactID IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @UserLinkID, (@CompanyID and/or @ContactID), or @StorefrontID.', 1;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================
    IF (@OptionID IS NULL)
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName;

        IF (@OptionID IS NULL)
        BEGIN
            -- If not defined, and NULL, there will be nothing to delete
            -- -------------------------------------------------
            IF (@Value IS NULL)
                RETURN;

            PRINT 'create new - check parameters, save and return id'

            DECLARE @NewID INT
            EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, 1;
            SET @OptionID = @NewID

            INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
            SELECT @OptionID AS ID
                    , @OptionName AS Name
                    , 'Custom: '+@OptionName AS Label
                    , NULL AS DefaultValue
                    , 0 AS DataType -- Always string
                    , 10100 AS CategoryID  -- AdHoc Options
                    , 1 AS IsHidden
            ;

            PRINT @OptionID
        END
    END

    -- ======================================
    -- Lookup Current Value if it Exists
    -- ======================================
    DECLARE @ID INT = 
        (   CASE WHEN @UserLinkID    IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = @OptionID AND D.UserLinkID = @UserLinkID)
                 WHEN @ContactID     IS NOT NULL AND @CompanyID IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = @OptionID AND D.ContactID  = @ContactID AND D.CompanyID = @CompanyID )
                 WHEN @ContactID     IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = @OptionID AND D.ContactID  = @ContactID )
                 WHEN @CompanyID     IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = @OptionID AND D.CompanyID  = @CompanyID )
                 WHEN @StorefrontID  IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = @OptionID AND D.StorefrontID = @StorefrontID)
                 WHEN @LocationID    IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = @OptionID AND D.LocationID = @LocationID)
                 WHEN @BID           IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = @OptionID AND UserLinkID IS NULL AND CompanyID IS NULL and ContactID IS NULL and StorefrontID IS NULL)
                 WHEN @AssociationID IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.AssociationID = @AssociationID AND D.OptionID = @OptionID )
            ELSE NULL
            END
        );

    -- ======================================
    -- Now Save (Delete or Update or Create) Value
    -- ======================================
    IF (@Value IS NULL)
        DELETE FROM [Option.Data]
        WHERE ID = @ID;

    ELSE
    BEGIN
        IF (@ID IS NULL)
            INSERT INTO [Option.Data] 
                ( CreatedDate, ModifiedDT, IsActive, OptionID
                , [Value], AssociationID, BID, LocationID
                , StoreFrontID, UserLinkID, CompanyID, ContactID
                )
            VALUES
                ( GetUTCDate(), GetUTCDate(), 1, @OptionID
                , @Value, @AssociationID, @BID, @LocationID
                , @StoreFrontID, @UserLinkID, @CompanyID, @ContactID
                );
        ELSE
            UPDATE [Option.Data]
            SET ModifiedDT = GetUTCDate(), Value = @Value
            WHERE ID = @ID

    END
END
");
            // update Option.SaveValues
            migrationBuilder.Sql(@"
-- ===============================================
/*
    Name: [Option.SaveValues]

    Description: 
        Create a procedure that saves multiple options values at once.  
        The values are all given the same access level when saved.
    Updated: 2018-07-05

    Sample Use:   

    DECLARE @T OptionsArray;
    INSERT INTO @T
        VALUES (52, 'GLAccount.TaxName1', 'State')
            --, (121, 'Accounting.PaymentTerm.DefaultID', 'Pizza') 
            , (121, NULL, 'Pizza') 
            , (NULL, 'Employee.Collection.SortOrder', '4;5;1;5')
            , (NULL, 'Employee.OldSort', NULL)
            ;

    EXEC dbo.[Option.SaveValues] @Options_Array = @T , @BID = 1 -- , @debug = 1
*/
-- ===============================================
ALTER   PROCEDURE [dbo].[Option.SaveValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT      = NULL
    , @BID            SMALLINT     = NULL
    , @LocationID     SMALLINT     = NULL
    , @UserLinkID     SMALLINT     = NULL
    , @CompanyID      INT          = NULL
    , @ContactID      INT          = NULL
    , @StorefrontID   SMALLINT	   = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserLinkID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL AND @ContactID IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @UserLinkID, (@CompanyID and/or @ContactID), or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
              RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing IDs from the Definitions Table
    -- -------------------------------------------------
    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

    -- If not defined, and NULL, there will be nothing to delete
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
        AND (Value IS NULL)
    ;

    -- Any missing IDs must be AdHoc so create them
    -- -------------------------------------------------
    DECLARE @NewIDs INT = (SELECT COUNT(*) FROM @Options WHERE OptionID IS NULL);

    IF (@NewIDs > 0)
    BEGIN
        DECLARE @NewID INT
        DECLARE @NewIDCount INT = (SELECT COUNT(*) FROM @Options WHERE OptionID IS NULL);
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, @NewIDCount;


        -- Any missing IDs must be AdHoc so create them
        -- -------------------------------------------------
        UPDATE @Options
        SET   IsNewAdHoc = 1
            , OptionID = @NewID, @NewID = @NewID + 1
        WHERE OptionID IS NULL
        ;

        INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
        SELECT OptionID AS ID
                , OptionName AS Name
                , 'Custom: '+OptionName AS Label
                , NULL AS DefaultValue
                , 0 AS DataType -- Always string
                , 10100 AS CategoryID  -- AdHoc Options
                , 1 AS IsHidden
        FROM @Options
        WHERE IsNewAdHoc = 1
        ;
    END;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
    UPDATE O
    SET InstanceID = 
        (   CASE WHEN @UserLinkID    IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = O.OptionID AND D.UserLinkID = @UserLinkID)
                 WHEN @ContactID     IS NOT NULL AND @CompanyID IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = O.OptionID AND D.ContactID  = @ContactID AND D.CompanyID = @CompanyID )
                 WHEN @ContactID     IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = O.OptionID AND D.ContactID  = @ContactID )
                 WHEN @CompanyID     IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = O.OptionID AND D.CompanyID  = @CompanyID )
                 WHEN @StorefrontID  IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = O.OptionID AND D.StorefrontID = @StorefrontID)
                 WHEN @LocationID    IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = O.OptionID AND D.LocationID = @LocationID)
                 WHEN @BID           IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.BID = @BID AND D.OptionID = O.OptionID AND UserLinkID IS NULL AND CompanyID IS NULL and ContactID IS NULL and StorefrontID IS NULL)
                 WHEN @AssociationID IS NOT NULL THEN (SELECT ID FROM [Option.Data] D WHERE D.AssociationID = @AssociationID AND D.OptionID = O.OptionID )
            ELSE NULL
            END
        )
    FROM @Options O
    ;

    IF (@Debug=1) SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options O ON O.InstanceID = D.ID
    WHERE O.Value IS NULL
    ;

    -- ======================================
    -- Update any Existing Values
    -- ======================================
    UPDATE D
    SET ModifiedDT = GetUTCDate(), Value = O.Value
    FROM [Option.Data] D
    JOIN @Options O ON O.InstanceID = D.ID
    WHERE O.Value IS NOT NULL
    ;

    -- ======================================
    -- Create New Options if not Found Values
    -- ======================================
    INSERT INTO [Option.Data] 
        ( CreatedDate, ModifiedDT, IsActive, OptionID
        , [Value], AssociationID, BID, LocationID
        , StoreFrontID, UserLinkID, CompanyID, ContactID
        )
        SELECT
            GetUTCDate(), GetUTCDate(), 1, OptionID
            , Value, @AssociationID, @BID, @LocationID
            , @StoreFrontID, @UserLinkID, @CompanyID, @ContactID
        FROM @Options
        WHERE InstanceID IS NULL
            AND Value IS NOT NULL
    ;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // reset Option.GetValue
            migrationBuilder.Sql(@"
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValue]
--DECLARE 
	  @OptionID       INT          = NULL -- = 2
	, @OptionName     VARCHAR(255) = NULL -- 'GLAccount.TaxName1'
	, @AssociationID  tinyint      = NULL
	, @BID            smallint     = NULL
	, @LocationID     smallint     = NULL
	, @StoreFrontID   smallint     = NULL
	, @UserLinkID     smallint     = NULL
	, @CompanyID      int          = NULL
	, @ContactID      int          = NULL
AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
            , @Level  TINYINT     
            ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition]
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserLinkID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

        IF (@AnswerCount > 1)
            THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT TOP 1
				  @CompanyID = Company.ID
				, @LocationID = Company.LocationID
            FROM [Company.Data] Company
				JOIN [Company.Contact.Link] Link ON Link.CompanyID = @CompanyID AND Link.BID = Company.BID
				JOIN [Contact.Data] Contact ON Contact.ID = Link.ContactID AND Contact.BID = Link.BID
            WHERE Contact.ID = @ContactID AND Contact.BID = @BID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID AND Company.BID = @BID
            ;

        IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
				JOIN [User.Link] UL on E.ID = UL.EmployeeID
            WHERE UL.ID = @UserLinkID AND UL.BID = @BID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @UserLinkID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @UserLinkID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;

    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    System=1
    --    Association=1
    --    Business=2
    --    Location=4
    --    Storefront=8
    --    Employee=16
    --    Company=32
    --    Contact=64

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (UserLinkID   IS NULL)
			AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, CompanyID, ContactID) IS NULL
                )
        ORDER BY OptionLevel DESC


    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (ContactID    IS NULL)
			AND (UserLinkID   IS NULL)
			AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, CompanyID) IS NULL
        )
        ORDER BY OptionLevel DESC

    ELSE IF (@UserLinkID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (CompanyID    IS NULL)
			AND (UserLinkID   = @UserLinkID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID, UserLinkID) IS NULL
                )
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (CompanyID    IS NULL )
			AND (UserLinkID   IS NULL )
			AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID
                OR COALESCE(LocationID, StoreFrontID) IS NULL
                )
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (CompanyID    IS NULL )
			AND (UserLinkID   IS NULL )
			AND (StoreFrontID IS NULL )
			AND (LocationID   = @LocationID
				OR LocationID IS NULL
				)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
			AND (CompanyID    IS NULL )
			AND (UserLinkID   IS NULL )
			AND (StoreFrontID IS NULL )
			AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
			AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
                , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    SELECT 
        CAST (@OptionID AS SMALLINT) AS ID -- needed in the EF
        ,@Result as [Value]
        , @Level as [OptionLevel];
END
");
            // reset Option.GetValues
            migrationBuilder.Sql(@"
/* ========================================================
Name: [Option.GetValues]

Description: This procedure looks up a option/setting based on a hierarchy of possibilities.

Sample Use:   EXEC dbo.[Option.GetValues] @CategoryID = 300, @BID=2, @LocationID = 1
Returns: Table with columns
	[OptionID] INT
	[Name] VARCHAR(255),
	[Value] VARCHAR(255),
	[Level] TINYINT,
	[Label] VARCHAR(255),
	[Description] VARCHAR(MAX),
	[DataType] SMALLINT,
	[CategoryID] SMALLINT,
	[ListValues] VARCHAR(MAX)
	[IsHidden] BIT

	OPTION LEVELS
		System=0
		Association=1
		Business=2
		Location=4
		Storefront=8
		Employee=16
		Company=32
		Contact=64
======================================================== */
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValues]
-- DECLARE
	  @CategoryID     smallint     = NULL
	, @AssociationID  tinyint      = NULL
	, @BID            smallint     = NULL
	, @LocationID     smallint     = NULL
	, @StorefrontID   smallint     = NULL
	, @UserLinkID	  smallint     = NULL
	, @CompanyID      int          = NULL
	, @ContactID      int          = NULL
AS

BEGIN
    IF (@BID IS NULL) AND COALESCE(@ContactID, @CompanyID, @UserLinkID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @UserLinkID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;

    IF COALESCE(@BID, @AssociationID) IS NULL
    BEGIN;
        THROW 180000, 'You must supply at least one of the BID or the AssociationID' , 1;
        RETURN;
    END;

    -- Define the Results
    DECLARE @Result TABLE (   [OptionID] INT PRIMARY KEY
                            , [Name] VARCHAR(255)
                            , [Value] NVARCHAR(255) INDEX IX_Result_Value WHERE [Value] IS NULL
                            , [Level] TINYINT
                            , [Label] VARCHAR(255)
                            , [Description] VARCHAR(MAX)
                            , [DataType] SMALLINT
                            , [CategoryID] SMALLINT
                            , [ListValues] VARCHAR(MAX)
                            , [IsHidden] BIT
                            );

    -- Define a list of Categories to JOIN to get the results
    DECLARE @Categories TABLE( ID SMALLINT PRIMARY KEY  );
    IF (@CategoryID IS NULL)
        INSERT INTO @Categories SELECT ID FROM [System.Option.Category]
    ELSE
        INSERT INTO @Categories SELECT @CategoryID
    ;

    -- Now Fill the List with Option Definitions
    INSERT INTO @Result 
        SELECT DEF.[ID] as [OptionID]
            , DEF.[Name]
            , DEF.[DefaultValue] AS [Value] 
            , 0 AS [Level]
            , DEF.[Label]
            , DEF.Description
            , DEF.DataType
            , CAT.ID as CategoryID
            , DEF.ListValues
            , DEF.IsHidden

        FROM @Categories CAT
        JOIN [System.Option.Definition] DEF ON DEF.CategoryID = CAT.ID
    ;

    -- Lookup values in the option hiearchy 
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL)
            SELECT TOP 1
				  @CompanyID = Company.ID
                , @LocationID = Company.LocationID
            FROM [Company.Data] Company
				JOIN [Company.Contact.Link] Link ON Link.CompanyID = @CompanyID AND Link.BID = Company.BID
				JOIN [Contact.Data] Contact ON Link.ContactID = Contact.ID AND Link.BID = Contact.BID
            WHERE Contact.ID = @ContactID AND Contact.BID = @BID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID AND Company.BID = @BID
            ;

        IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
				LEFT JOIN [User.Link] UL ON UL.BID = E.BID AND UL.EmployeeID = E.ID
            WHERE UL.ID = @UserLinkID AND UL.BID = @BID AND UL.EmployeeID IS NOT NULL
            ;
    END;

    -- Not look up option value based on the specific instance
    IF (@ContactID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.UserLinkID   IS NULL)
            AND (OPT.ContactID    = @ContactID
                    OR OPT.CompanyID    = @CompanyID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ; 

    IF (@CompanyID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.UserLinkID   IS NULL)
            AND (OPT.ContactID    IS NULL)
            AND (OPT.CompanyID    = @CompanyID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@UserLinkID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL)
            AND (OPT.UserLinkID   = @UserLinkID
                    OR OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@StorefrontID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID = @StorefrontID
                    OR OPT.LocationID   = @LocationID)
    ;

    IF (@LocationID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID IS NULL )
            AND (OPT.LocationID   = @LocationID)
    ;

    IF (@BID IS NOT NULL)
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
        JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID AND @BID = OPT.BID
        WHERE RES.[Level] = 0
            AND (OPT.CompanyID    IS NULL )
            AND (OPT.UserLinkID   IS NULL )
            AND (OPT.StorefrontID IS NULL )
            AND (OPT.LocationID   IS NULL )
    ;

    IF @AssociationID IS NOT NULL
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM @Result RES
			JOIN [Option.Data] OPT ON RES.OptionID = OPT.OptionID
        WHERE RES.[Level] = 0
            AND (OPT.AssociationID = @AssociationID)
    ;

    -- Now return what you found!
    SELECT *
    FROM @Result;
END
");
            // reset Option.SaveValue
            migrationBuilder.Sql(@"
-- ========================================================
-- 
-- Name: [Option.SaveValue]
--
-- Description: This procedure saves the setting for an option/setting.
-- 
-- Sample Use:   
/*
    EXEC dbo.[Option.SaveValue] 
        -- Require Fields
          @OptionName       = 'GLAccount.TaxName2'
        , @OptionID         = NULL
        , @Value            = 'NewValue'  -- Pass in NULL to delete the current value (if any)

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @UserLinkID       	= 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

    SELECT * FROM [Option.Data]
*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Option.SaveValue]
-- DECLARE 
	  @OptionID       INT          = NULL
	, @OptionName     VARCHAR(255) = NULL
	, @Value          VARCHAR(MAX) = NULL     -- Pass in NULL to delete the current value (if any)

	, @AssociationID  TINYINT      = NULL
	, @BID            SMALLINT     = NULL
	, @LocationID     SMALLINT     = NULL
	, @UserLinkID     	SMALLINT     = NULL
	, @CompanyID      INT          = NULL
	, @ContactID      INT          = NULL
	, @StorefrontID   SMALLINT	 = NULL

AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@OptionName IS NULL) AND (@OptionID IS NULL)) OR ((@OptionName IS NOT NULL) AND (@OptionID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @OptionID or the @OptionName.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserLinkID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================
    IF (@OptionID IS NULL)
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName;

        IF (@OptionID IS NULL)
        BEGIN
            -- If not defined, and NULL, there will be nothing to delete
            -- -------------------------------------------------
            IF (@Value IS NULL)
                RETURN;

            PRINT 'create new - check parameters, save and return id'

            DECLARE @NewID INT
            EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, 1;
            SET @OptionID = @NewID

            INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
            SELECT @OptionID AS ID
                    , @OptionName AS Name
                    , 'Custom: '+@OptionName AS Label
                    , NULL AS DefaultValue
                    , 0 AS DataType -- Always string
                    , 10100 AS CategoryID  -- AdHoc Options
                    , 1 AS IsHidden
            ;

            PRINT @OptionID
        END
    END

    -- ======================================
    -- Lookup Current Value if it Exists
    -- ======================================
    DECLARE @InstanceID INT = 

        CASE 
            WHEN @AssociationID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND AssociationID = @AssociationID )

            WHEN @LocationID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND LocationID = @LocationID)

            WHEN @UserLinkID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND UserLinkID = @UserLinkID)

            WHEN @CompanyID  IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND CompanyID  = @CompanyID )

            WHEN @ContactID  IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND ContactID  = @ContactID )

            WHEN @StorefrontID IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND StorefrontID = @StorefrontID)

            WHEN @BID        IS NOT NULL 
            THEN (SELECT ID FROM [Option.Data] WHERE OptionID = @OptionID AND BID = @BID AND COALESCE(LocationID, UserLinkID, CompanyID, ContactID, StorefrontID) IS NULL )

        ELSE NULL
        END;

    -- ======================================
    -- Now Save (Update or Create) Value
    -- ======================================
    IF (@Value IS NOT NULL)
    BEGIN
        IF (@InstanceID IS NOT NULL)
            UPDATE [Option.Data]
            SET ModifiedDT = GetUTCDate()
            , Value = @Value
            WHERE ID = @InstanceID

        ELSE
            INSERT INTO [Option.Data] 
                ( CreatedDate, ModifiedDT, IsActive, OptionID
                , [Value], AssociationID, BID, LocationID
                , StoreFrontID, UserLinkID, CompanyID, ContactID
                )
            VALUES
                ( GetUTCDate(), GetUTCDate(), 1, @OptionID
                , @Value, @AssociationID, @BID, @LocationID
                , @StoreFrontID, @UserLinkID, @CompanyID, @ContactID
                );
    END

    -- ======================================
    -- Else Delete the Options with NULL Values Passed in
    -- ======================================
    ELSE
        DELETE FROM [Option.Data]
        WHERE ID = @InstanceID

END
");
            // reset Option.SaveValues
            migrationBuilder.Sql(@"
-- ===============================================
/*
Name: [Option.SaveValues]

Description: 
    Create a procedure that saves multiple options values
    at once.  The values are all given the same access
    level when saved.
Updated: 2018-07-05

Sample Use:   

DECLARE @T OptionsArray;
INSERT INTO @T
    VALUES  (NULL, 'GLAccount.TaxName1', 'State')
        , (NULL, 'GLAccount.TaxName2', 'City')
        , (NULL, 'Employee.Collection.SortOrder', '4;5;1;5')
        , (NULL, 'Employee.OldSort', NULL)
        ;

EXEC dbo.[Option.SaveValues] 
    -- Require Fields
      @Options_Array    = @T

    -- One (and only one) of these is required
    , @AssociationID    = NULL
    , @BID              = 2          -- Required (except for dev or franchise use)

    -- One (ad only one) of the following can be supplied when BID is also supplied
    -- BID must be supplied if these are used.
    , @LocationID       = NULL
    , @UserLinkID       = 1
    , @CompanyID        = NULL
    , @ContactID        = NULL
    , @StorefrontID     = NULL

    , @Debug            = 1

*/
-- ===============================================
CREATE OR ALTER PROCEDURE [dbo].[Option.SaveValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT       = NULL
    , @BID            SMALLINT      = NULL
    , @LocationID     SMALLINT      = NULL
    , @UserLinkID     SMALLINT      = NULL
    , @CompanyID      INT           = NULL
    , @ContactID      INT           = NULL
    , @StorefrontID   SMALLINT      = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@UserLinkID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @UserLinkID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
                RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing  IDs from the Definitions Table
    -- -------------------------------------------------
    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

    -- If not defined, and NULL, there will be nothing to delete
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
        AND (Value IS NULL)
    ;

    -- Any missing IDs must be AdHoc so create them
    -- -------------------------------------------------
    DECLARE @NewIDs INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);

    IF (@NewIDs > 0)
    BEGIN
        DECLARE @NewID INT
        DECLARE @NewIDCount INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, @NewIDCount;


        -- Any missing IDs must be AdHoc so create them
        -- -------------------------------------------------
        UPDATE @Options
        SET   IsNewAdHoc = 1
            , OptionID = @NewID, @NewID = @NewID + 1
        WHERE OptionID IS NULL
        ;

        INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
        SELECT OptionID AS ID
                , OptionName AS Name
                , 'Custom: '+OptionName AS Label
                , NULL AS DefaultValue
                , 0 AS DataType -- Always string
                , 10100 AS CategoryID  -- AdHoc Options
                , 1 AS IsHidden
        FROM @Options
        WHERE IsNewAdHoc = 1
        ;
    END;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
    UPDATE Opt
    SET InstanceID = 
            CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.LocationID = @LocationID)

                WHEN @UserLinkID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.UserLinkID = @UserLinkID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND UserLinkID IS NULL AND CompanyID IS NULL and ContactID IS NULL and StorefrontID IS NULL)

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.AssociationID = @AssociationID)

            ELSE NULL
            END
    FROM @Options Opt
    ;

    IF (@Debug=1)
        SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NULL
    ;

    -- ======================================
    -- Update any Existing Values
    -- ======================================
    UPDATE D
    SET ModifiedDT = GetUTCDate()
        , Value = Opt.Value
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NOT NULL
    ;

    -- ======================================
    -- Create New Options if not Found Values
    -- ======================================
    INSERT INTO [Option.Data] 
        ( CreatedDate, ModifiedDT, IsActive, OptionID
        , [Value], AssociationID, BID, LocationID
        , StoreFrontID, UserLinkID, CompanyID, ContactID
        )
        SELECT
            GetUTCDate(), GetUTCDate(), 1, OptionID
            , Value, @AssociationID, @BID, @LocationID
            , @StoreFrontID, @UserLinkID, @CompanyID, @ContactID
        FROM @Options
        WHERE InstanceID IS NULL
            AND Value IS NOT NULL
    ;
END
");
        }
    }
}
