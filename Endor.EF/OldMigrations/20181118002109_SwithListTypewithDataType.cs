using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class SwithListTypewithDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subassembly.Element.Data_ElementListType",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropTable(
                name: "enum.ElementListType ");

            migrationBuilder.DropColumn(
                name: "ListType",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddColumn<short>(
                name: "DataType",
                table: "Part.Subassembly.Element",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Element_DataType",
                table: "Part.Subassembly.Element",
                column: "DataType");

            migrationBuilder.AddForeignKey(
                name: "FK_Subassembly.Element.Data_DataType",
                table: "Part.Subassembly.Element",
                column: "DataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subassembly.Element.Data_DataType",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Element_DataType",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropColumn(
                name: "DataType",
                table: "Part.Subassembly.Element");

            migrationBuilder.AddColumn<byte>(
                name: "ListType",
                table: "Part.Subassembly.Element",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "enum.ElementListType ",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.ElementListType ", x => x.ID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Subassembly.Element.Data_ElementListType",
                table: "Part.Subassembly.Element",
                column: "ListType",
                principalTable: "enum.ElementListType ",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
