using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180505014432_FixLocatorTablesDefaultValueConstraintForSortIndexColumn")]
    public partial class FixLocatorTablesDefaultValueConstraintForSortIndexColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DisableSystemVersioningIfEnabled("Order.Contact.Locator");

            migrationBuilder.DropColumn("IsPrimary", "Order.Contact.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Order.Contact.Locator", "SortIndex");

            // may need to write straight sql here
            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Order.Contact.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "50",
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Order.Contact.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Location.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Location.Locator", "SortIndex");

            // may need to write straight sql here
            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Location.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "50",
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "0");


            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Location.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Employee.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Employee.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Employee.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "50",
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "2");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Employee.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Contact.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Contact.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Contact.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "50",
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Contact.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Company.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Company.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Company.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "50",
                oldClrType: typeof(short),
                oldType: "smallint");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Company.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Business.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Business.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Business.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "50",
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "0");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Business.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.EnableSystemVersioning("Order.Contact.Locator", "Historic.Order.Contact.Loctaor");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DisableSystemVersioningIfEnabled("Order.Contact.Locator");

            migrationBuilder.DropColumn("IsPrimary", "Order.Contact.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Order.Contact.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Order.Contact.Locator",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "50");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Order.Contact.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.Sql("ALTER TABLE[Order.Contact.Locator] SET(SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Contact.Locator]))");

            migrationBuilder.DropColumn("IsPrimary", "Location.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Location.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Location.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "0",
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "50");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Location.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Employee.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Employee.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Employee.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "2",
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "50");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Employee.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Contact.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Contact.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Contact.Locator",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "50");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Contact.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Company.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Company.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Company.Locator",
                type: "smallint",
                nullable: false,
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "50");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Company.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.DropColumn("IsPrimary", "Business.Locator");

            migrationBuilder.DropDefaultConstraintIfExists("Business.Locator", "SortIndex");

            migrationBuilder.AlterColumn<short>(
                name: "SortIndex",
                table: "Business.Locator",
                type: "smallint",
                nullable: false,
                defaultValueSql: "0",
                oldClrType: typeof(short),
                oldType: "smallint",
                oldDefaultValueSql: "50");

            migrationBuilder.AddColumn<bool>(
                name: "IsPrimary",
                table: "Business.Locator",
                type: "bit",
                nullable: false,
                computedColumnSql: "(case when [SortIndex]=(1) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)");

            migrationBuilder.EnableSystemVersioning("Order.Contact.Locator", "Historic.Order.Contact.Loctaor");
        }
    }
}

