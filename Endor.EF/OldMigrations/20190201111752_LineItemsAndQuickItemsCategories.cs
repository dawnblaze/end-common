using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class LineItemsAndQuickItemsCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
               INSERT INTO [enum.List.FlatListType] ([ID], [Name], [IsAlphaSorted] )
                VALUES (31, 'Line Item Categories', 1)
                ;

                INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
                VALUES (607, 'Line Item Categories', 600, 'Line Item Categories', 2, 0, 'Line Item Items Category Categories Products Products Part Component')
                ;

                INSERT INTO [enum.List.FlatListType] ([ID], [Name], [IsAlphaSorted] )
                VALUES (32, 'Quick Item Categories', 1)
                ;

                INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
                VALUES (608, 'Quick Item Categories', 600, 'Quick Item Categories', 2, 0, 'Quick Fast Item Items Category Categories Products Products Line')
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql( @"DELETE [enum.List.FlatListType] WHERE [ID]=31;");
            migrationBuilder.Sql(@"DELETE [enum.List.FlatListType] WHERE [ID]=32;");
            migrationBuilder.Sql(@"DELETE [dbo].[System.Option.Category] WHERE [ID]=607;");
            migrationBuilder.Sql(@"DELETE [dbo].[System.Option.Category] WHERE [ID]=608;");
        }
    }
}
