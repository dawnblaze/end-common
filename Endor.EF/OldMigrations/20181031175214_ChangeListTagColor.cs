using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class ChangeListTagColor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_List.Tag_System.Tag.Color",
                table: "List.Tag");

            migrationBuilder.AlterColumn<short>(
                name: "ColorID",
                table: "List.Tag",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AddColumn<string>(
                name: "RGB",
                table: "List.Tag",
                maxLength: 8,
                nullable: false,
                defaultValue: "");

            migrationBuilder.Sql
           (
               @"
                    UPDATE [List.Tag] SET RGB = 'D72727' where ID = 1;
                    UPDATE [List.Tag] SET RGB = '6593CF' where ID = 2;
                    UPDATE [List.Tag] SET RGB = 'FFD46C' where ID = 3;
                "
           );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "RGB",
                table: "List.Tag");

            migrationBuilder.AlterColumn<short>(
                name: "ColorID",
                table: "List.Tag",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.Sql
           (
               @"
                    UPDATE [List.Tag] SET ColorID = 2 where ID = 1;
                    UPDATE [List.Tag] SET ColorID = 13 where ID = 2;
                    UPDATE [List.Tag] SET ColorID = 5 where ID = 3;
                "
           );

            migrationBuilder.AddForeignKey(
                name: "FK_List.Tag_System.Tag.Color",
                table: "List.Tag",
                column: "ColorID",
                principalTable: "System.Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
