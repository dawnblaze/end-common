using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180305024433_AddTaxItemLinkAutoCompute")]
    public partial class AddTaxItemLinkAutoCompute : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Tax.Group.Action.LinkTaxItem]
--
-- Description: This procedure links/unlinks the TaxItem to the TaxGroup
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.LinkTaxItem] @BID=1, @TaxGroupID=1, @TaxItemID=1, @IsLinked=1
-- ========================================================
ALTER PROCEDURE [dbo].[Accounting.Tax.Group.Action.LinkTaxItem]
--DECLARE 
          @BID            TINYINT  --= 1
        , @TaxGroupID     SMALLINT --= 2
		, @TaxItemID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the TaxItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxItem Specified. TaxItemID='+CONVERT(VARCHAR(12),@TaxItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.ItemLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID and GroupID = @TaxGroupID and ItemID = @TaxItemID)
		BEGIN
			INSERT INTO [Accounting.Tax.Group.ItemLink] (BID, GroupID, ItemID)
			VALUES (@BID, @TaxGroupID, @TaxItemID)
		END;
	END
	ELSE
	BEGIN
		-- Remove entry from Tax.Group.ItemLink
		DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID and GroupID = @TaxGroupID and ItemID = @TaxItemID
	END

	-- Compute total tax rate for taxgroup based on linked tax items
	DECLARE @TotalTaxRate decimal(12,4) = 0
	SET @TotalTaxRate = (SELECT SUM([Accounting.Tax.Item].TaxRate) FROM [Accounting.Tax.Group.ItemLink] INNER JOIN [Accounting.Tax.Item] ON [Accounting.Tax.Group.ItemLink].ItemID = [Accounting.Tax.Item].ID
	WHERE [Accounting.Tax.Group.ItemLink].BID = @BID and [Accounting.Tax.Group.ItemLink].GroupID = @TaxGroupID)
	SET @TotalTaxRate = ISNULL(@TotalTaxRate, 0);
	UPDATE [Accounting.Tax.Group] SET TaxRate = @TotalTaxRate WHERE ID = @TaxGroupID

    SET @Result = 1;

    SELECT @Result as Result;
END         
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Tax.Group.Action.LinkTaxItem]
--
-- Description: This procedure links/unlinks the TaxItem to the TaxGroup
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.LinkTaxItem] @BID=1, @TaxGroupID=1, @TaxItemID=1, @IsLinked=1
-- ========================================================
ALTER PROCEDURE [dbo].[Accounting.Tax.Group.Action.LinkTaxItem]
--DECLARE 
          @BID            TINYINT  --= 1
        , @TaxGroupID     SMALLINT --= 2
		, @TaxItemID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the TaxItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxItem Specified. TaxItemID='+CONVERT(VARCHAR(12),@TaxItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.ItemLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID and GroupID = @TaxGroupID and ItemID = @TaxItemID)
		BEGIN
			INSERT INTO [Accounting.Tax.Group.ItemLink] (BID, GroupID, ItemID)
			VALUES (@BID, @TaxGroupID, @TaxItemID)
		END;
	END
	ELSE
	BEGIN
		-- Remove entry from Tax.Group.ItemLink
		DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID and GroupID = @TaxGroupID and ItemID = @TaxItemID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END         
            ");
        }
    }
}

