using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180319155209_UpdateSprocsOriginCanDelete")]
    public partial class UpdateSprocsOriginCanDelete : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Name: [Origin.Action.CanDelete]
--
-- Description: This procedure checks if the origin is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Origin.Action.CanDelete] @BID=1, @ID=1005, @ShowCantDeleteReason=1
-- ========================================================
ALTER PROCEDURE [dbo].[Origin.Action.CanDelete]
		  @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @ID)
		SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Invalid Origin Specified. OriginID=', @ID, ' not found')

	ELSE IF EXISTS(SELECT * FROM [Company.Data] WHERE OriginID = @ID)
            --AND NOT EXISTS(SELECT * FROM [Order.Data] WHERE OriginID = @OriginID)
        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Origin exist in Company.Data table. OriginID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

