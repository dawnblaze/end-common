using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdatePaymentApplicationPaymentMethodIDNotNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                IF EXISTS (SELECT temporal_type FROM sys.tables WHERE object_id = OBJECT_ID('dbo.[Historic.Accounting.Payment.Application]', 'u'))
                    BEGIN
                        ALTER TABLE [Accounting.Payment.Application] SET (SYSTEM_VERSIONING = OFF);
                        DROP TABLE dbo.[Historic.Accounting.Payment.Application];
                    END
            ");

            migrationBuilder.Sql(@"
                UPDATE [Accounting.Payment.Application] SET PaymentMethodId = 1 WHERE PaymentMethodId IS NULL;
            ");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentMethodId",
                table: "Accounting.Payment.Application",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PaymentMethodId",
                table: "Accounting.Payment.Application",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
