using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180719221306_UpdateLockOptionDefinitions")]
    public partial class UpdateLockOptionDefinitions : Migration
    {
        /*
         * debug sql:
         * 
         *   SELECT Name, Label FROM [System.Option.Definition] WHERE Name ='Association.IncomeAccount.Lock'
                or Name ='Association.Expense.Lock'
                or Name ='Association.RoyaltyStructure.Lock'
                or Name ='Association.Location.Lock'
                or Name ='Association.Require.Origin'
                or Name ='Association.Require.Industry'
                or Name ='Association.Require.OrderOrigin'
         */

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
    UPDATE [System.Option.Definition] SET Label = 'Lock First Income Account Level' WHERE Name ='Association.IncomeAccount.Lock'
    UPDATE [System.Option.Definition] SET Label = 'Lock First Expense Account Level' WHERE Name ='Association.Expense.Lock'
    UPDATE [System.Option.Definition] SET Label = 'Lock Royalty Structure' WHERE Name ='Association.RoyaltyStructure.Lock'
    UPDATE [System.Option.Definition] SET Label = 'Lock Location Number' WHERE Name ='Association.Location.Lock'
    UPDATE [System.Option.Definition] SET Label = 'Require Origin on Company' WHERE Name ='Association.Require.Origin'
    UPDATE [System.Option.Definition] SET Label = 'Require Industry on Company' WHERE Name ='Association.Require.Industry'
    UPDATE [System.Option.Definition] SET Label = 'Require Origin on Order' WHERE Name ='Association.Require.OrderOrigin'
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
    UPDATE [System.Option.Definition] SET Label = '' WHERE Name ='Association.IncomeAccount.Lock'
    UPDATE [System.Option.Definition] SET Label = '' WHERE Name ='Association.Expense.Lock'
    UPDATE [System.Option.Definition] SET Label = '' WHERE Name ='Association.RoyaltyStructure.Lock'
    UPDATE [System.Option.Definition] SET Label = '' WHERE Name ='Association.Location.Lock'
    UPDATE [System.Option.Definition] SET Label = '' WHERE Name ='Association.Require.Origin'
    UPDATE [System.Option.Definition] SET Label = '' WHERE Name ='Association.Require.Industry'
    UPDATE [System.Option.Definition] SET Label = '' WHERE Name ='Association.Require.OrderOrigin'
");
        }
    }
}

