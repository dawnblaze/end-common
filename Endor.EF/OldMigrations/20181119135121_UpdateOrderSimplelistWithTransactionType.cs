using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateOrderSimplelistWithTransactionType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
            @"
                ALTER VIEW [dbo].[Order.SimpleList]
                                    AS
                                SELECT	[BID]
                , [ID]
                , [ClassTypeID]
                , [FormattedNumber] as DisplayName
                , CONVERT(BIT, 1) as [IsActive]
                , CONVERT(BIT, 0) as [HasImage]
                , CONVERT(BIT, 0) AS [IsDefault]
                , [TransactionType] AS [TransactionType]
                                FROM [Order.Data];
            "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
            @"
                ALTER VIEW [dbo].[Order.SimpleList]
                                    AS
                                SELECT	[BID]
                , [ID]
                , [ClassTypeID]
                , [FormattedNumber] as DisplayName
                , CONVERT(BIT, 1) as [IsActive]
                , CONVERT(BIT, 0) as [HasImage]
                , CONVERT(BIT, 0) AS [IsDefault]
                                FROM [Order.Data];
            "
            );
        }
    }
}
