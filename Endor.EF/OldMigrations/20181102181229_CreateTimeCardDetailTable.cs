using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CreateTimeCardDetailTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "LatEnd",
                table: "Employee.TimeCard",
                type: "decimal(18,11)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LatStart",
                table: "Employee.TimeCard",
                type: "decimal(18,11)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LongEnd",
                table: "Employee.TimeCard",
                type: "decimal(18,11)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "LongStart",
                table: "Employee.TimeCard",
                type: "decimal(18,11)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Employee.TimeCard.Detail",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AdjustedByEmployeeID = table.Column<short>(nullable: true),
                    AdjustedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false),
                    EmployeeID = table.Column<short>(nullable: false),
                    EndDT = table.Column<DateTime>(type: "datetime2(0)", nullable: true),
                    IsAdjusted = table.Column<bool>(nullable: false),
                    IsClosed = table.Column<bool>(nullable: true),
                    IsPaid = table.Column<bool>(nullable: false),
                    LatEnd = table.Column<decimal>(type: "decimal(18,11)", nullable: true),
                    LatStart = table.Column<decimal>(type: "decimal(18,11)", nullable: true),
                    LongEnd = table.Column<decimal>(type: "decimal(18,11)", nullable: true),
                    LongStart = table.Column<decimal>(type: "decimal(18,11)", nullable: true),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    OrderItemID = table.Column<int>(nullable: true),
                    OrderItemStatusID = table.Column<short>(nullable: true),
                    PaidTimeInMin = table.Column<decimal>(type: "decimal(18,4)", nullable: true),
                    SimultaneousDetailCards = table.Column<byte>(nullable: false),
                    StartDT = table.Column<DateTime>(type: "datetime2(0)", nullable: false),
                    TimeCardID = table.Column<short>(nullable: false),
                    TimeClockActivityID = table.Column<short>(nullable: true),
                    TimeClockBreakID = table.Column<short>(nullable: true),
                    TimeInMin = table.Column<decimal>(type: "decimal(18,4)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee.TimeCard.Detail", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Employee.TimeCard.Detail_AdjustedBy",
                        columns: x => new { x.BID, x.AdjustedByEmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.TimeCard.Detail_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.TimeCard.Detail_Order.Item.Status",
                        columns: x => new { x.BID, x.OrderItemStatusID },
                        principalTable: "Order.Item.Status",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.TimeCard.Detail_Employee.TimeCard",
                        columns: x => new { x.BID, x.TimeCardID },
                        principalTable: "Employee.TimeCard",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.TimeCard.Detail_List.FlatList.Data",
                        columns: x => new { x.BID, x.TimeClockActivityID },
                        principalTable: "List.FlatList.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Employee.TimeCard.Detail_TimeClockBreak",
                        columns: x => new { x.BID, x.TimeClockBreakID },
                        principalTable: "List.FlatList.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard.Detail_BID_AdjustedByEmployeeID",
                table: "Employee.TimeCard.Detail",
                columns: new[] { "BID", "AdjustedByEmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard.Detail_BID_OrderItemStatusID",
                table: "Employee.TimeCard.Detail",
                columns: new[] { "BID", "OrderItemStatusID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard.Detail_TimeCard",
                table: "Employee.TimeCard.Detail",
                columns: new[] { "BID", "TimeCardID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard.Detail_BID_TimeClockActivityID",
                table: "Employee.TimeCard.Detail",
                columns: new[] { "BID", "TimeClockActivityID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard.Detail_BID_TimeClockBreakID",
                table: "Employee.TimeCard.Detail",
                columns: new[] { "BID", "TimeClockBreakID" });

            migrationBuilder.CreateIndex(
                name: "IX_Employee.TimeCard.Detail_Employee",
                table: "Employee.TimeCard.Detail",
                columns: new[] { "BID", "EmployeeID", "StartDT", "IsClosed" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Employee.TimeCard.Detail");

            migrationBuilder.DropColumn(
                name: "LatEnd",
                table: "Employee.TimeCard");

            migrationBuilder.DropColumn(
                name: "LatStart",
                table: "Employee.TimeCard");

            migrationBuilder.DropColumn(
                name: "LongEnd",
                table: "Employee.TimeCard");

            migrationBuilder.DropColumn(
                name: "LongStart",
                table: "Employee.TimeCard");
        }
    }
}
