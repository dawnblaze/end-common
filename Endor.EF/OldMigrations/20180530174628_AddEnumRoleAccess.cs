using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180530174628_AddEnumRoleAccess")]
    public partial class AddEnumRoleAccess : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Role.Access",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Role.Access", x => x.ID);
                });

            migrationBuilder.Sql(@"
INSERT INTO [enum.Role.Access] ([ID],[Name]) VALUES (0, 'Not Allowed');
INSERT INTO [enum.Role.Access] ([ID],[Name]) VALUES (1, 'Optional');
INSERT INTO [enum.Role.Access] ([ID],[Name]) VALUES (2, 'Mandatory');
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Role.Access");
        }
    }
}

