using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CREATE_TABLE_enumMessageParticipantRoleType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Message.Participant.RoleType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Message.Participant.RoleType", x => x.ID);
                });
            
            migrationBuilder.Sql(@"
                INSERT [enum.Message.Participant.RoleType] ([ID], [Name])
                    VALUES (1, N'From')
                    , (2, N'To')
                    , (3, N'CC')
                    , (4, N'BCC')
                ;            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [enum.Message.Participant.RoleType] WHERE ID IN(1,2,3,4);
            ");

            migrationBuilder.DropTable(
                name: "enum.Message.Participant.RoleType");
        }
    }
}
