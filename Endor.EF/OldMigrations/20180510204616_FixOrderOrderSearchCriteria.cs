using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180510204616_FixOrderOrderSearchCriteria")]
    public partial class FixOrderOrderSearchCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter.Criteria]
SET Field = 'Order'
WHERE  TargetClassTypeID = 10000 AND Name = 'OrderNumber';
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

