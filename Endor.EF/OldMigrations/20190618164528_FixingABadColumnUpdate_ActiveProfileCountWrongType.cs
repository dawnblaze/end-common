using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixingABadColumnUpdate_ActiveProfileCountWrongType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropDefaultConstraintIfExists("Part.Machine.Data", "ActiveProfileCount");
            migrationBuilder.AddColumn<short>("ActiveProfileCountTemp", "Part.Machine.Data", defaultValueSql: "(0)");
            migrationBuilder.Sql("Update [Part.Machine.Data] set ActiveProfileCountTemp = ActiveProfileCount");
            migrationBuilder.DropColumn("ActiveProfileCount", "Part.Machine.Data");
            migrationBuilder.AddColumn<short>("ActiveProfileCount", "Part.Machine.Data", defaultValueSql: "(0)");
            migrationBuilder.Sql("Update [Part.Machine.Data] set ActiveProfileCount = ActiveProfileCountTemp");
            migrationBuilder.DropDefaultConstraintIfExists("Part.Machine.Data", "ActiveProfileCountTemp");
            migrationBuilder.DropColumn("ActiveProfileCountTemp", "Part.Machine.Data");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
