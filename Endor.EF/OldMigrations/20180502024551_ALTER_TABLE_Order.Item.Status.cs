using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180502024551_ALTER_TABLE_Order.Item.Status")]
    public partial class ALTER_TABLE_OrderItemStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE dbo.[Order.Item.Status]
                DROP CONSTRAINT [PK_Order.Item.Status]; 

                ALTER TABLE [Order.Item.Status]
                ALTER COLUMN ID INT NOT NULL

                ALTER TABLE [Order.Item.Status]
                DROP COLUMN ModifiedDT

                ALTER TABLE [Order.Item.Status]
                ADD ModifiedDT DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT SysUTCDateTime()
                  , ValidToDT DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL DEFAULT CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999')
                  , PERIOD FOR SYSTEM_TIME (ModifiedDT,ValidToDT)

                ALTER TABLE [Order.Item.Status]
                ADD CONSTRAINT [PK_Order.Item.Status] PRIMARY KEY ([BID], [ID]); 
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"");
        }
    }
}

