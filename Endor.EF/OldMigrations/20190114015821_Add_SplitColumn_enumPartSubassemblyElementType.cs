using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_SplitColumn_enumPartSubassemblyElementType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (@"
                INSERT INTO [enum.Part.Subassembly.ElementType] ([ID],[Name])
                VALUES (103,'SplitColumn')
            "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [enum.Part.Subassembly.ElementType] WHERE ID = 103");
        }
    }
}
