using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Add_Approved_Lost_Estimate_Filter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [System.List.Filter.Criteria]
                GO
            ");

            migrationBuilder.Sql(@"INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 1, 0, 0, NULL, NULL, 0, 2, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'Name', N'Name', N'LongName', 0, 1, 0, 0, NULL, NULL, 0, 0, 2)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 3)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'Email', N'Email', N'EmailAddress', 0, 1, 0, 0, NULL, NULL, 0, 1, 4)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'Location', N'Location', N'LocationID', 0, 2, 4, 0, NULL, NULL, 0, 3, 5)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1005, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 6)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1005, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, NULL, 7)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Name', N'Name', N'LongName', 0, 1, 0, 0, NULL, NULL, 0, 0, 8)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Company', N'Company', N'Company', 0, 1, 0, 0, NULL, NULL, 0, 1, 9)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Email', N'Email', N'EmailAddress', 0, 1, 0, 0, NULL, NULL, 0, 2, 10)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 1, 0, 0, NULL, NULL, 0, 3, 11)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Status', N'Status', N'StatusID', 0, 2, 5, 0, NULL, NULL, 0, 4, 12)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 5, 13)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11101, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 14)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11101, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 15)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11102, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 16)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11102, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 17)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11105, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 18)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11105, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 19)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2012, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 20)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2012, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 21)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2011, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 30)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2011, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 31)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12000, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 32)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 33)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12002, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 34)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12002, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 35)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12022, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 36)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12022, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 37)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12032, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 38)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12032, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 39)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12020, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 40)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12020, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 41)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12030, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 42)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12030, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 43)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 44)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 45)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'SalesPerson', N'Sales Person', N'SalesPersonID', 0, 2, 1, 0, NULL, NULL, 0, 1, 46)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'Location', N'Location', N'LocationID', 0, 2, 4, 0, NULL, NULL, 0, 2, 47)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'ContactName', N'Contact Name', N'PrimaryContactID', 0, 2, 6, 0, NULL, NULL, 0, 3, 48)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'OrderNumber', N'Order/Invoice #', N'Order', 0, 1, 0, 0, NULL, NULL, 0, 0, 49)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'OrderStatus', N'Order Status', N'StatusID', 0, 32005, 10, 0, NULL, NULL, 0, 1, 50)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'Employees', N'Employees', N'EmployeeRolesIDs', 0, 32005, 9, 0, NULL, NULL, 0, 2, 51)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'Company', N'Company', N'CompanyName', 0, 1, 0, 0, NULL, NULL, 0, 3, 52)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'Contact', N'Contact', N'ContactRolesNames', 0, 1, 0, 0, NULL, NULL, 0, 4, 53)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'ShowVoidedOrders', N'Show Voided Orders', N'IsVoided', 0, 3, 11, 0, N'Hide Voided,Show Voided', NULL, 1, 5, 54)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5020, N'TeamName', N'Team Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 1, 55)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5020, N'Employee', N'Employee', N'EmployeeIDs', 0, 32005, 9, 0, NULL, NULL, 0, 2, 56)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5020, N'Location', N'Location', N'LocationIDs', 0, 2, 4, 0, NULL, NULL, 0, 3, 57)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5020, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 58)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1902, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 1, 59)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1902, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 60)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15025, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 1, 61)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15025, N'AppliesTo', N'Applies to', N'AppliesTo', 0, 1, 12, 0, N'Company,Contact,Employee,Location,Order', NULL, 1, 2, 62)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15025, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 3, 63)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10023, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 64)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5101, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 66)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5101, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 67)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1022, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 68)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1022, N'Location', N'Location', N'LocationIDs', 0, 2, 4, 0, NULL, NULL, 1, 0, 69)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 70)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14100, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 2, 71)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14100, N'Employee', N'Employee', N'EmployeeID', 0, 3, 9, 0, NULL, NULL, 1, 1, 72)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1021, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 2, 73)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1021, N'Location', N'Location', N'LocationID', 0, 2, 4, 0, NULL, NULL, 0, 0, 74)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1021, N'Application Type', N'Application Type', N'ApplicationType', 0, 2, 12, 0, N'Business,Customer Portal,ECommerce,Vendor Portal', NULL, 0, 1, 75)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1100, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 76)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1100, N'Type', N'Type', N'Type', 0, 1, 12, 0, N'Shared,User Specific', NULL, 0, 1, 77)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15027, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 78)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15027, N'AppliesToClassTypeIDText', N'Record Type', N'AppliesToClassTypeIDText', 0, 1, 12, 0, N'Order,Company,Contact,Employee,Location,Opportunity', NULL, 0, 0, 79)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12042, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 80)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12042, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 81)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12040, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 82)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12040, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 83)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Estimate #', N'Estimate #', N'Order', 0, 1, 0, 0, NULL, NULL, 0, 1, 84)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Estimate Status', N'Estimate Status', N'StatusID', 0, 32005, 14, 0, NULL, NULL, 0, 2, 85)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Employees', N'Employees', N'EmployeeRolesIDs', 0, 32005, 9, 0, NULL, NULL, 0, 3, 86)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Company', N'Company', N'CompanyName', 0, 1, 0, 0, NULL, NULL, 0, 4, 87)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Contact', N'Contact', N'ContactRolesNames', 0, 1, 0, 0, NULL, NULL, 0, 5, 88)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Show Voided', N'Show Voided', N'IsVoided', 0, 3, 11, 0, N'Hide Voided,Show Voided', NULL, 0, 8, 89)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14109, N'IsRead', N'Show Read Messages', N'IsRead', 0, 3, 2, 0, NULL, NULL, 0, 0, 90)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14109, N'Employee', N'Employee', N'EmployeeID', 1, 3, 9, 0, NULL, NULL, 1, 1, 91)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (8005, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 1, 92)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (8005, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 2, 93)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12060, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 10, 94)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12060, N'IsPrivate', N'Private Only', N'-IsShared', 0, 3, 2, 0, NULL, NULL, 0, 9, 95)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Show Approved', N'Show Approved', N'IsApproved', 0, 3, 11, 0, N'Hide Approved,Show Approved', NULL, 0, 6, 96)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Show Lost', N'Show Lost', N'IsLost', 0, 3, 11, 0, N'Hide Lost,Show Lost', NULL, 0, 7, 97)
GO
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 1, 0, 0, NULL, NULL, 0, 2, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'Name', N'Name', N'LongName', 0, 1, 0, 0, NULL, NULL, 0, 0, 2)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 3)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'Email', N'Email', N'EmailAddress', 0, 1, 0, 0, NULL, NULL, 0, 1, 4)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5000, N'Location', N'Location', N'LocationID', 0, 2, 4, 0, NULL, NULL, 0, 3, 5)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1005, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 6)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1005, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, NULL, 7)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Name', N'Name', N'LongName', 0, 1, 0, 0, NULL, NULL, 0, 0, 8)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Company', N'Company', N'Company', 0, 1, 0, 0, NULL, NULL, 0, 1, 9)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Email', N'Email', N'EmailAddress', 0, 1, 0, 0, NULL, NULL, 0, 2, 10)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 1, 0, 0, NULL, NULL, 0, 3, 11)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Status', N'Status', N'StatusID', 0, 2, 5, 0, NULL, NULL, 0, 4, 12)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (3000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 5, 13)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11101, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 14)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11101, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 15)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11102, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 16)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11102, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 17)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11105, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 18)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (11105, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 19)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2012, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 20)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2012, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 21)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2011, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 30)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2011, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 31)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12000, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 32)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 33)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12002, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 34)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12002, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 35)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12022, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 36)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12022, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 37)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12032, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 38)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12032, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 39)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12020, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 40)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12020, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 41)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12030, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 42)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12030, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 43)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 44)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 45)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'SalesPerson', N'Sales Person', N'SalesPersonID', 0, 2, 1, 0, NULL, NULL, 0, 1, 46)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'Location', N'Location', N'LocationID', 0, 2, 4, 0, NULL, NULL, 0, 2, 47)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (2000, N'ContactName', N'Contact Name', N'PrimaryContactID', 0, 2, 6, 0, NULL, NULL, 0, 3, 48)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'OrderNumber', N'Order/Invoice #', N'Order', 0, 1, 0, 0, NULL, NULL, 0, 0, 49)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'OrderStatus', N'Order Status', N'StatusID', 0, 32005, 10, 0, NULL, NULL, 0, 1, 50)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'Employees', N'Employees', N'EmployeeRolesIDs', 0, 32005, 9, 0, NULL, NULL, 0, 2, 51)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'Company', N'Company', N'CompanyName', 0, 1, 0, 0, NULL, NULL, 0, 3, 52)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'Contact', N'Contact', N'ContactRolesNames', 0, 1, 0, 0, NULL, NULL, 0, 4, 53)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10000, N'ShowVoidedOrders', N'Show Voided Orders', N'IsVoided', 0, 3, 11, 0, N'Hide Voided,Show Voided', NULL, 1, 5, 54)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5020, N'TeamName', N'Team Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 1, 55)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5020, N'Employee', N'Employee', N'EmployeeIDs', 0, 32005, 9, 0, NULL, NULL, 0, 2, 56)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5020, N'Location', N'Location', N'LocationIDs', 0, 2, 4, 0, NULL, NULL, 0, 3, 57)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5020, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 58)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1902, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 1, 59)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1902, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 60)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15025, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 1, 61)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15025, N'AppliesTo', N'Applies to', N'AppliesTo', 0, 1, 12, 0, N'Company,Contact,Employee,Location,Order', NULL, 1, 2, 62)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15025, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 3, 63)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10023, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 64)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5101, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 66)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (5101, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 67)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1022, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 68)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1022, N'Location', N'Location', N'LocationIDs', 0, 2, 4, 0, NULL, NULL, 1, 0, 69)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14000, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 70)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14100, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 2, 71)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14100, N'Employee', N'Employee', N'EmployeeID', 0, 3, 9, 0, NULL, NULL, 1, 1, 72)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1021, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 2, 73)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1021, N'Location', N'Location', N'LocationID', 0, 2, 4, 0, NULL, NULL, 0, 0, 74)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1021, N'Application Type', N'Application Type', N'ApplicationType', 0, 2, 12, 0, N'Business,Customer Portal,ECommerce,Vendor Portal', NULL, 0, 1, 75)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1100, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 76)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (1100, N'Type', N'Type', N'Type', 0, 1, 12, 0, N'Shared,User Specific', NULL, 0, 1, 77)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15027, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 78)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (15027, N'AppliesToClassTypeIDText', N'Record Type', N'AppliesToClassTypeIDText', 0, 1, 12, 0, N'Order,Company,Contact,Employee,Location,Opportunity', NULL, 0, 0, 79)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12042, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 80)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12042, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 81)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12040, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 0, 82)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12040, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 1, 83)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Estimate #', N'Estimate #', N'Order', 0, 1, 0, 0, NULL, NULL, 0, 1, 84)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Estimate Status', N'Estimate Status', N'StatusID', 0, 32005, 14, 0, NULL, NULL, 0, 2, 85)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Employees', N'Employees', N'EmployeeRolesIDs', 0, 32005, 9, 0, NULL, NULL, 0, 3, 86)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Company', N'Company', N'CompanyName', 0, 1, 0, 0, NULL, NULL, 0, 4, 87)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Contact', N'Contact', N'ContactRolesNames', 0, 1, 0, 0, NULL, NULL, 0, 5, 88)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (10200, N'Show Voided', N'Show Voided', N'IsVoided', 0, 3, 11, 0, N'Hide Voided,Show Voided', NULL, 0, 8, 89)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14109, N'IsRead', N'Show Read Messages', N'IsRead', 0, 3, 2, 0, NULL, NULL, 0, 0, 90)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (14109, N'Employee', N'Employee', N'EmployeeID', 1, 3, 9, 0, NULL, NULL, 1, 1, 91)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (8005, N'Name', N'Name', N'Name', 0, 1, 0, 0, NULL, NULL, 0, 1, 92)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (8005, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 2, 93)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12060, N'Include Inactive', N'Include Inactive', N'-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 10, 94)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID]) VALUES (12060, N'IsPrivate', N'Private Only', N'-IsShared', 0, 3, 2, 0, NULL, NULL, 0, 9, 95)

");
        }
    }
}
