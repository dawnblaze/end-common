using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8824UpdateKilogramMilligramUnitSystem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(
                @"
                UPDATE [dbo].[enum.Part.Unit]
                   SET [UnitSystem] = 2
                 WHERE [ID] IN (32, 33)
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
