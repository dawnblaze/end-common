using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180711162538_UpdateFlatListTypeEnums")]
    public partial class UpdateFlatListTypeEnums : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT [enum.List.FlatListType] ([ID], [Name], [IsAlphaSorted]) 
    VALUES 
	  (4, N'Estimate Edited Reasons', 1)
    , (5, N'Order Edited Reasons', 1)
    , (11, N'Time Card Activities', 1)
    , (12, N'Time Card Breaks', 0)
    , (21, N'Payment Voided Reasons', 1)
    , (22, N'Credit Given Reasons', 1)
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [enum.List.FlatListType] WHERE [ID] > 3;
");
        }
    }
}

