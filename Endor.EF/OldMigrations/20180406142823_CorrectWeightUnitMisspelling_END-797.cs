using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180406142823_CorrectWeightUnitMisspelling_END-797")]
    public partial class CorrectWeightUnitMisspelling_END797 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit4",
                table: "Part.Material.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Material.Data_WeigthUnit",
                table: "Part.Material.Data");

            migrationBuilder.DropColumn(
                name: "WeigthUnit",
                table: "Part.Material.Data");

            migrationBuilder.AddColumn<byte>(
                name: "WeightUnit",
                table: "Part.Material.Data",
                type: "tinyint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_WeightUnit",
                table: "Part.Material.Data",
                column: "WeightUnit");

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit4",
                table: "Part.Material.Data",
                column: "WeightUnit",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit4",
                table: "Part.Material.Data");

            migrationBuilder.DropIndex(
                name: "IX_Part.Material.Data_WeightUnit",
                table: "Part.Material.Data");

            migrationBuilder.DropColumn(
                name: "WeightUnit",
                table: "Part.Material.Data");

            migrationBuilder.AddColumn<byte>(
                name: "WeigthUnit",
                table: "Part.Material.Data",
                type: "tinyint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Part.Material.Data_WeigthUnit",
                table: "Part.Material.Data",
                column: "WeigthUnit");

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_enum.Part.Unit4",
                table: "Part.Material.Data",
                column: "WeigthUnit",
                principalTable: "enum.Part.Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

