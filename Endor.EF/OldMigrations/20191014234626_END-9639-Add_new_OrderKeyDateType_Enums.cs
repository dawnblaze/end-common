﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9639Add_new_OrderKeyDateType_Enums : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    INSERT [enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet])
                    VALUES (32, N'Balance Due', 0, 1, 2, 1)
                         , (33, N'Scheduled Payment', 0, 1, 2, 1)
                         , (34, N'Early Payment Date', 0, 1, 2, 1)
                    ;
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
