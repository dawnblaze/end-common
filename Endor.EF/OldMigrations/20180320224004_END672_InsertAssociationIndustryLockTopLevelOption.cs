using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180320224004_END672_InsertAssociationIndustryLockTopLevelOption")]
    public partial class END672_InsertAssociationIndustryLockTopLevelOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
if not Exists(select * from [System.Option.Definition] where name = 'Association.Industry.LockFirstLevel')
begin
INSERT INTO [dbo].[System.Option.Definition]
           ([ID]
           ,[Name]
           ,[Label]
           ,[Description]
           ,[DataType]
           ,[CategoryID]
           ,[ListValues]
           ,[DefaultValue]
           ,[IsHidden])
     VALUES
           (42, 'Association.Industry.LockFirstLevel', 'Lock First Industry Level', 'If true then Top level of Industry cannot be adjusted or added to', 3, 8, NULL, 'false', 1)
		   
end
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE From [System.Option.Definition] where name = 'Association.Industry.LockFirstLevel'");
        }
    }
}

