﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class UpdateContactAndCompanyModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_Company.Data",
                table: "Contact.Data");

            migrationBuilder.DropIndex(
                name: "IX_Contact.Data_Company",
                table: "Contact.Data");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "Contact.Data");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Contact.Data");

            migrationBuilder.DropColumn(
                name: "IsBilling",
                table: "Contact.Data");

            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "Contact.Data");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "Contact.Data",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Contact.Data",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsBilling",
                table: "Contact.Data",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "Contact.Data",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Contact.Data_Company",
                table: "Contact.Data",
                columns: new[] { "BID", "CompanyID", "IsDefault", "IsBilling" });

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_Company.Data",
                table: "Contact.Data",
                columns: new[] { "BID", "CompanyID" },
                principalTable: "Company.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
