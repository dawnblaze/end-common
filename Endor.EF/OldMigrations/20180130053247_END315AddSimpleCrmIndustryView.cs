using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180130053247_END315AddSimpleCrmIndustryView")]
    public partial class END315AddSimpleCrmIndustryView : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/****** Object:  View [dbo].[CRM.Industry.SimpleList]    Script Date: 1/29/2018 11:34:05 PM ******/
IF Exists(select * from sys.objects where name = 'CRM.Industry.SimpleList' and type = 'V')
  DROP VIEW [dbo].[CRM.Industry.SimpleList]
GO

/****** Object:  View [dbo].[CRM.Industry.SimpleList]    Script Date: 1/29/2018 11:34:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[CRM.Industry.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [CRM.Industry]
GO");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

If Exists(select * from sys.objects where name = 'CRM.Industry.SimpleList' and type = 'V')
    Drop View [CRM.Industry.SimpleList]");
        }
    }
}

