using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class LayoutVisualizationOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.Option.Category] 
                    ( [ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms] )
                VALUES
                    ( 910, N'Layout Visualization Options', 900, N'Layout Visualization Options', 2, 0, N'' );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        910, 
                        N'LayoutVisualization.HideMaterialDimensions', 
                        N'Shows or hides the Dimension labels on the Material Visualizer.', 
                        N'Shows or hides the Dimension labels on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'false', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        911, 
                        N'LayoutVisualization.HideMargin', 
                        N'Shows or hides the Material Margin on the Material Visualizer.', 
                        N'Shows or hides the Material Margin on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'false', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        912, 
                        N'LayoutVisualization.HideBleed', 
                        N'Shows or hides the Material Bleed on the Material Visualizer.', 
                        N'Shows or hides the Material Bleed on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'false', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        913, 
                        N'LayoutVisualization.HideLeader', 
                        N'Shows or hides the Material Leader on the Material Visualizer.', 
                        N'Shows or hides the Material Leader on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'false', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        914, 
                        N'LayoutVisualization.HideGutter', 
                        N'Shows or hides the Material Gutter on the Material Visualizer.', 
                        N'Shows or hides the Material Gutter on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'false', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        915, 
                        N'LayoutVisualization.HideColorbar', 
                        N'Shows or hides the Material Colorbar on the Material Visualizer.', 
                        N'Shows or hides the Material Colorbar on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'false', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        916, 
                        N'LayoutVisualization.HideCutlines', 
                        N'Shows or hides the Material Cutlines on the Material Visualizer.', 
                        N'Shows or hides the Material Cutlines on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'true', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        917, 
                        N'LayoutVisualization.HideFoldlines', 
                        N'Shows or hides the Material Foldlines on the Material Visualizer.', 
                        N'Shows or hides the Material Foldlines on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'true', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        918, 
                        N'LayoutVisualization.HideWatermark', 
                        N'Shows or hides the Material Watermark on the Material Visualizer.', 
                        N'Shows or hides the Material Watermark on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'true', 
                        0
                    );
            ");
            migrationBuilder.Sql(@"
                INSERT INTO [System.Option.Definition] 
                    ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) 
                VALUES 
                    (
                        919, 
                        N'LayoutVisualization.HideRegistration', 
                        N'Shows or hides the Material Registration on the Material Visualizer.', 
                        N'Shows or hides the Material Registration on the Material Visualizer.', 
                        3, 
                        910, 
                        N'', 
                        N'false', 
                        0
                    );
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Definition] WHERE ID = 910;
DELETE FROM [System.Option.Definition] WHERE ID = 911;
DELETE FROM [System.Option.Definition] WHERE ID = 912;
DELETE FROM [System.Option.Definition] WHERE ID = 913;
DELETE FROM [System.Option.Definition] WHERE ID = 914;
DELETE FROM [System.Option.Definition] WHERE ID = 915;
DELETE FROM [System.Option.Definition] WHERE ID = 916;
DELETE FROM [System.Option.Definition] WHERE ID = 917;
DELETE FROM [System.Option.Definition] WHERE ID = 918;
DELETE FROM [System.Option.Definition] WHERE ID = 919;
DELETE FROM [System.Option.Category] WHERE [ID] = 910;
");
        }
    }
}
