using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CategoryID_OrderItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "CategoryID",
                table: "Order.Item.Data",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Data_CategoryID",
                table: "Order.Item.Data",
                columns: new[] { "BID", "CategoryID" },
                principalTable: "List.FlatList.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            //SET [TransactionLevelSet] = 3
            migrationBuilder.Sql(@"
                UPDATE[dbo].[enum.Order.KeyDateType]
                SET [TransactionLevelSet] = 7
                WHERE ID = 24
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
