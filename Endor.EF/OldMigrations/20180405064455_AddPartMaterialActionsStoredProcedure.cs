using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180405064455_AddPartMaterialActionsStoredProcedure")]
    public partial class AddPartMaterialActionsStoredProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Part.Material.Data.Action.CanDelete]

    Description: This procedure checks if the Material is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Material.Data.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Part.Material.Data.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the Material can be deleted. The boolean response is returned in the body.  A Material can be deleted if
	It is not linked to any Material Categories.
	(future) It is not in use in any Product Template.
	(future) Is it not in use in any Order or Estimate.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Material.Data] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Material Specified Does not Exist. MaterialID=', @ID)

    -- It is not linked to any Material Categories.
		--ELSE IF EXISTS( SELECT * FROM [Part.Material.CategoryLink] WHERE BID = @BID AND PartID = @ID )

		--SELECT @Result = 0
		--	, @CantDeleteReason = CONCAT('Material is linked to a Material Category. MaterialID=', @ID)

	-- (future) It is not in use in any Product Template.
	-- (future) Is it not in use in any Order or Estimate.
	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
            ");

            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Material.Data.Action.SetActive]
--
-- Description: This procedure sets the Material to active or inactive
--
-- Sample Use:   EXEC dbo.[Part.Material.Data.Action.SetActive] @BID=1, @MaterialID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Part.Material.Data.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @MaterialID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Material specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Material.Data] WHERE BID = @BID and ID = @MaterialID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Material Specified. MaterialID='+CONVERT(VARCHAR(12),@MaterialID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Part.Material.Data] L
    WHERE BID = @BID and ID = @MaterialID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE [dbo].[Part.Material.Data.Action.SetActive]
DROP PROCEDURE [dbo].[Part.Material.Data.Action.CanDelete]
            ");
        }
    }
}

