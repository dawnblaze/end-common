using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180514044229_InsertWorkflowOptions")]
    public partial class InsertWorkflowOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

                INSERT INTO [System.Option.Section] (ID, Name, ParentID, IsHidden, Depth, SearchTerms)
                VALUES (13,'Workflow', NULL, 0, 0, NULL)

                INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden, SearchTerms)
                VALUES (13,'Order', 13, 'Workflow Orders options',4,0,NULL)

                INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden, SearchTerms)
                VALUES (14,'Estimate', 13, 'Workflow Estimates options',4,0,null)

                INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden, SearchTerms)
                VALUES (15,'Purchase', 13, 'Workflow Purchase Orders options',4,0,null)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE [System.Option.Section] WHERE ID = 13 
                DELETE [System.Option.Category] WHERE ID IN (13,14,15) 
            ");
        }
    }
}

