using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddedAndUpdateModelForSecurityConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            -- ====================================
            -- Drop old Tables (after dropping contraints)
            -- ====================================
                ALTER TABLE [Rights.Group.RightLink] DROP CONSTRAINT IF EXISTS [FK_Rights.Group.RightLink_Rights.Group];
                ALTER TABLE [Rights.Group.ChildGroupLink] DROP CONSTRAINT IF EXISTS [FK_Rights.Group.ChildGroupLink_Rights.Group_Parent];
                ALTER TABLE [Rights.Group.ChildGroupLink] DROP CONSTRAINT IF EXISTS [FK_Rights.Group.ChildGroupLink_Rights.Group_Child];
                ALTER TABLE [Rights.Group.List.RightsGroupLink] DROP CONSTRAINT IF EXISTS [FK_Rights.Group.List.RightsGroupLink_RightsGroupList]
                ALTER TABLE [Rights.Group.List.RightsGroupLink] DROP CONSTRAINT IF EXISTS [FK_Rights.Group.List.RightsGroupLink_RightsGroup]
                ALTER TABLE [Rights.Group] DROP CONSTRAINT IF EXISTS [FK_Rights.Group_enum.User.Access.Type];
                ALTER TABLE [Rights.Group] DROP CONSTRAINT IF EXISTS [DF__Rights.Gr__IsSys__031C6FA4];

                DROP TABLE IF EXISTS [Rights.Group.RightLink];
                DROP TABLE IF EXISTS [Rights.Group.ChildGroupLink];
                DROP TABLE IF EXISTS [Rights.Group.List.RightsGroupLink]
                DROP TABLE IF EXISTS [Rights.Group];

            -- =================================================================
            CREATE TABLE [System.Rights.Group](
	            [ID] [smallint] NOT NULL,
	            [Name] [varchar](255) NOT NULL,
	            CONSTRAINT [PK_System.Rights.Group] PRIMARY KEY ( [ID] )
            );

            -- =================================================================
            CREATE TABLE [System.Rights.Group.ChildLink](
	            [ParentID] [smallint] NOT NULL,
	            [ChildID] [smallint] NOT NULL,
	            CONSTRAINT [PK_System.Rights.Group.ChildLink] PRIMARY KEY ( [ParentID], [ChildID] )
            );

            ALTER TABLE [System.Rights.Group.ChildLink] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.Group.ChildLink_Parent] 
            FOREIGN KEY([ParentID]) REFERENCES [System.Rights.Group] ([ID])
            ;

            ALTER TABLE [System.Rights.Group.ChildLink] 
            CHECK CONSTRAINT [FK_System.Rights.Group.ChildLink_Parent]
            ;

            ALTER TABLE [System.Rights.Group.ChildLink] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.Group.ChildLink_Child] 
            FOREIGN KEY([ChildID]) REFERENCES [System.Rights.Group] ([ID])
            ;

            ALTER TABLE [System.Rights.Group.ChildLink] 
            CHECK CONSTRAINT [FK_System.Rights.Group.ChildLink_Child]
            ;

            -- =================================================================
            CREATE TABLE [System.Rights.Group.RightLink](
	            [GroupID] [smallint] NOT NULL,
	            [RightID] [smallint] NOT NULL,
	            CONSTRAINT [PK_System.Rights.Group.RightLink] PRIMARY KEY ( [GroupID], [RightID] )
            );

            ALTER TABLE [System.Rights.Group.RightLink] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.Group.RightLink_enum.User.Right] 
            FOREIGN KEY([RightID]) REFERENCES [enum.User.Right] ([ID])
            ;

            ALTER TABLE [System.Rights.Group.RightLink] 
            CHECK CONSTRAINT [FK_System.Rights.Group.RightLink_enum.User.Right]
            ;

            ALTER TABLE [System.Rights.Group.RightLink] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.Group.RightLink_System.Rights.Group] 
            FOREIGN KEY([GroupID]) REFERENCES [System.Rights.Group] ([ID])
            ;

            ALTER TABLE [System.Rights.Group.RightLink] 
            CHECK CONSTRAINT [FK_System.Rights.Group.RightLink_System.Rights.Group]
            ;

            -- =================================================================
            CREATE TABLE [Rights.Group.List.RightsGroupLink](
	            [BID] [smallint] NOT NULL,
	            [ListID] [smallint] NOT NULL,
	            [GroupID] [smallint] NOT NULL,
	            [CreatedDT] [datetime2](2) NOT NULL DEFAULT GetUTCDate(),
	            CONSTRAINT [PK_Security.Group.List.GroupLink] PRIMARY KEY ( [BID], [ListID], [GroupID] )
            );

            CREATE INDEX [IX_Security.Group.List.GroupLink_GroupID] 
            ON [Rights.Group.List.RightsGroupLink] ( GroupID, BID, ListID )
            ;

            ALTER TABLE [Rights.Group.List.RightsGroupLink] WITH CHECK 
            ADD CONSTRAINT [FK_Rights.Group.List.RightsGroupLink_System.Rights.Group] 
            FOREIGN KEY([GroupID]) REFERENCES [System.Rights.Group] ([ID])
            ;

            ALTER TABLE [Rights.Group.List.RightsGroupLink] 
            CHECK CONSTRAINT [FK_Rights.Group.List.RightsGroupLink_System.Rights.Group]
            ;

            ALTER TABLE [Rights.Group.List.RightsGroupLink] WITH CHECK 
            ADD CONSTRAINT [FK_Security.Group.List.GroupLink_Security.Group.List] 
            FOREIGN KEY([BID], [ListID]) REFERENCES [Rights.Group.List] ([BID], [ID])
            ;

            ALTER TABLE [Rights.Group.List.RightsGroupLink] 
            CHECK CONSTRAINT [FK_Security.Group.List.GroupLink_Security.Group.List]
            ;

            -- =================================================================
            CREATE TABLE [System.Rights.UserAccessType.GroupLink](
	            [ID] [smallint] NOT NULL,
	            [UserAccessType] [tinyint] NOT NULL,
	            [IncludedRightsGroupID] [smallint] NOT NULL,
	            CONSTRAINT [PK_System.Rights.UserAccessType.GroupLink] PRIMARY KEY ( [ID] )
            );

            ALTER TABLE [System.Rights.UserAccessType.GroupLink] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.UserAccessType.GroupLink_enum.User.Access.Type] 
            FOREIGN KEY([UserAccessType]) REFERENCES [enum.User.Access.Type] ([ID])
            ;

            ALTER TABLE [System.Rights.UserAccessType.GroupLink] 
            CHECK CONSTRAINT [FK_System.Rights.UserAccessType.GroupLink_enum.User.Access.Type]
            ;

            ALTER TABLE [System.Rights.UserAccessType.GroupLink] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.UserAccessType.GroupLink_System.Rights.Group] 
            FOREIGN KEY([IncludedRightsGroupID]) REFERENCES [System.Rights.Group] ([ID])
            ;

            ALTER TABLE [System.Rights.UserAccessType.GroupLink] 
            CHECK CONSTRAINT [FK_System.Rights.UserAccessType.GroupLink_System.Rights.Group]
            ;

            -- =================================================================
            CREATE TABLE [dbo].[System.Rights.Group.Menu.Tree](
                [ID] [smallint] NOT NULL,
                [ParentID] [smallint] NULL,
                [Module] [smallint] NULL,
                [Name] [nvarchar](100) NOT NULL,
                [Depth] [tinyint] NOT NULL,
                [SortIndex] [tinyint] NULL,
                [EnabledGroupID] [smallint] NULL,
                [FullAccessGroupID] [smallint] NULL,
                [IsSharedGroup] [bit] NOT NULL,
                [Description] [nvarchar](max) NULL,
                [SearchTerms] [nvarchar](max) NULL,
                [HintText] [nvarchar](max) NULL,
                [IsInternal] [bit] NOT NULL,
                CONSTRAINT [PK_System.Rights.Group.Menu.Tree] PRIMARY KEY ( ID ) 
            );

            CREATE INDEX [IX_System.Rights.Group.Menu.Tree_EnabledID] 
            ON [System.Rights.Group.Menu.Tree] ( [EnabledGroupID] )
            ;

            CREATE INDEX [IX_System.Rights.Group.Menu.Tree_Parent] 
            ON [System.Rights.Group.Menu.Tree] ( ParentID, SortIndex, Name )
            ;

            ALTER TABLE [System.Rights.Group.Menu.Tree] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.Group.Menu.Tree_EnabledGroup] 
            FOREIGN KEY([EnabledGroupID]) REFERENCES [System.Rights.Group] ([ID])
            ;

            ALTER TABLE [System.Rights.Group.Menu.Tree] 
            CHECK CONSTRAINT [FK_System.Rights.Group.Menu.Tree_EnabledGroup]
            ;

            ALTER TABLE [System.Rights.Group.Menu.Tree] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.Group.Menu.Tree_System.Rights.Group.Menu.Tree] 
            FOREIGN KEY([ParentID]) REFERENCES [System.Rights.Group.Menu.Tree] ([ID])
            ;

            ALTER TABLE [System.Rights.Group.Menu.Tree] 
            CHECK CONSTRAINT [FK_System.Rights.Group.Menu.Tree_System.Rights.Group.Menu.Tree]
            ;

            ALTER TABLE [System.Rights.Group.Menu.Tree] WITH CHECK 
            ADD CONSTRAINT [FK_System.Rights.Group.Menu.Tree_FullAccessGroup] 
            FOREIGN KEY([FullAccessGroupID]) REFERENCES [System.Rights.Group] ([ID])
            ;

            ALTER TABLE [System.Rights.Group.Menu.Tree] 
            CHECK CONSTRAINT [FK_System.Rights.Group.Menu.Tree_FullAccessGroup]
            ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
