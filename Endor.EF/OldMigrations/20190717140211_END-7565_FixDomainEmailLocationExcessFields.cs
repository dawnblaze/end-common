using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END7565_FixDomainEmailLocationExcessFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE CONSTRAINT_NAME = 'FK_Domain.Email.LocationLink_Domain.Email.Data_DomainEmailBID_DomainEmailID' 
AND TABLE_NAME = 'Domain.Email.LocationLink')
BEGIN
	ALTER TABLE [dbo].[Domain.Email.LocationLink] DROP CONSTRAINT [FK_Domain.Email.LocationLink_Domain.Email.Data_DomainEmailBID_DomainEmailID]
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE CONSTRAINT_NAME = 'FK_Domain.Email.LocationLink_Location.Data_LocationDataBID_LocationDataID' 
AND TABLE_NAME = 'Domain.Email.LocationLink')
BEGIN
	ALTER TABLE [dbo].[Domain.Email.LocationLink] DROP CONSTRAINT [FK_Domain.Email.LocationLink_Location.Data_LocationDataBID_LocationDataID]
END

IF EXISTS (SELECT 1 FROM SYS.INDEXES 
WHERE NAME = 'IX_Domain.Email.LocationLink_DomainEmailBID_DomainEmailID' AND object_id = OBJECT_ID('[Domain.Email.LocationLink]'))
BEGIN
	DROP INDEX [IX_Domain.Email.LocationLink_DomainEmailBID_DomainEmailID] ON [dbo].[Domain.Email.LocationLink]
END

IF EXISTS (SELECT 1 FROM SYS.INDEXES 
WHERE NAME = 'IX_Domain.Email.LocationLink_LocationDataBID_LocationDataID' AND object_id = OBJECT_ID('[Domain.Email.LocationLink]'))
BEGIN
	DROP INDEX [IX_Domain.Email.LocationLink_LocationDataBID_LocationDataID] ON [dbo].[Domain.Email.LocationLink]
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Domain.Email.LocationLink' AND COLUMN_NAME = 'DomainEmailBID')
BEGIN
    ALTER TABLE [Domain.Email.LocationLink] DROP COLUMN [DomainEmailBID]
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Domain.Email.LocationLink' AND COLUMN_NAME = 'DomainEmailID')
BEGIN
    ALTER TABLE [Domain.Email.LocationLink] DROP COLUMN [DomainEmailID]
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Domain.Email.LocationLink' AND COLUMN_NAME = 'LocationDataBID')
BEGIN
    ALTER TABLE [Domain.Email.LocationLink] DROP COLUMN [LocationDataBID]
END

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Domain.Email.LocationLink' AND COLUMN_NAME = 'LocationDataID')
BEGIN
    ALTER TABLE [Domain.Email.LocationLink] DROP COLUMN [LocationDataID]
END

EXEC [Util.Constraints.CheckTrust] @Repair = 1;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Domain.Email.LocationLink' AND COLUMN_NAME = 'DomainEmailBID')
BEGIN
    ALTER TABLE [Domain.Email.LocationLink] ADD [DomainEmailBID] SMALLINT NULL
END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Domain.Email.LocationLink' AND COLUMN_NAME = 'DomainEmailID')
BEGIN
    ALTER TABLE [Domain.Email.LocationLink] ADD [DomainEmailID] SMALLINT NULL
END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Domain.Email.LocationLink' AND COLUMN_NAME = 'LocationDataBID')
BEGIN
    ALTER TABLE [Domain.Email.LocationLink] ADD [LocationDataBID] SMALLINT NULL
END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Domain.Email.LocationLink' AND COLUMN_NAME = 'LocationDataID')
BEGIN
    ALTER TABLE [Domain.Email.LocationLink] ADD [LocationDataID] TINYINT NULL
END

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES 
WHERE NAME = 'IX_Domain.Email.LocationLink_DomainEmailBID_DomainEmailID' AND object_id = OBJECT_ID('[Domain.Email.LocationLink]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Domain.Email.LocationLink_DomainEmailBID_DomainEmailID] ON [dbo].[Domain.Email.LocationLink]
	(
		[DomainEmailBID] ASC,
		[DomainEmailID] ASC
	)
	WITH (
	PAD_INDEX = OFF
	, STATISTICS_NORECOMPUTE = OFF
	, SORT_IN_TEMPDB = OFF
	, DROP_EXISTING = OFF
	, ONLINE = OFF
	, ALLOW_ROW_LOCKS = ON
	, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END

IF NOT EXISTS (SELECT 1 FROM SYS.INDEXES 
WHERE NAME = 'IX_Domain.Email.LocationLink_LocationDataBID_LocationDataID' AND object_id = OBJECT_ID('[Domain.Email.LocationLink]'))
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Domain.Email.LocationLink_LocationDataBID_LocationDataID] ON [dbo].[Domain.Email.LocationLink]
	(
		[LocationDataBID] ASC,
		[LocationDataID] ASC
	)
	WITH (
	PAD_INDEX = OFF
	, STATISTICS_NORECOMPUTE = OFF
	, SORT_IN_TEMPDB = OFF
	, DROP_EXISTING = OFF
	, ONLINE = OFF
	, ALLOW_ROW_LOCKS = ON
	, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE CONSTRAINT_NAME = 'FK_Domain.Email.LocationLink_Domain.Email.Data_DomainEmailBID_DomainEmailID' 
AND TABLE_NAME = 'Domain.Email.LocationLink')
BEGIN
	ALTER TABLE [dbo].[Domain.Email.LocationLink]  WITH CHECK ADD  CONSTRAINT [FK_Domain.Email.LocationLink_Domain.Email.Data_DomainEmailBID_DomainEmailID] FOREIGN KEY([DomainEmailBID], [DomainEmailID])
	REFERENCES [dbo].[Domain.Email.Data] ([BID], [ID])
	ALTER TABLE [dbo].[Domain.Email.LocationLink] CHECK CONSTRAINT [FK_Domain.Email.LocationLink_Domain.Email.Data_DomainEmailBID_DomainEmailID]
END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE CONSTRAINT_NAME = 'FK_Domain.Email.LocationLink_Location.Data_LocationDataBID_LocationDataID' 
AND TABLE_NAME = 'Domain.Email.LocationLink')
BEGIN
	ALTER TABLE [dbo].[Domain.Email.LocationLink]  WITH CHECK ADD  CONSTRAINT [FK_Domain.Email.LocationLink_Location.Data_LocationDataBID_LocationDataID] FOREIGN KEY([LocationDataBID], [LocationDataID])
	REFERENCES [dbo].[Location.Data] ([BID], [ID])
	ALTER TABLE [dbo].[Domain.Email.LocationLink] CHECK CONSTRAINT [FK_Domain.Email.LocationLink_Location.Data_LocationDataBID_LocationDataID]
END

EXEC [Util.Constraints.CheckTrust] @Repair = 1;

");
        }
    }
}
