using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Correct_Temporal_Tables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DECLARE @T TABLE(TableName VARCHAR(255), IsTemporal BIT, HasHistory BIT);

INSERT INTO @T
(TableName)
VALUES
('Accounting.Payment.Application'),
('Accounting.Payment.Master'),
('Order.Contact.Locator'),
('Order.Contact.Role'),
('Order.Custom.Data'),
('Order.Data'),
('Order.Destination.Data'),
('Order.Employee.Role'),
('Order.Item.Component'),
('Order.Item.Data'),
('Order.Item.Surcharge'),
('Order.KeyDate'),
('Order.Note'),
('Order.OrderLink'),
('Order.Tax.Item.Assessment')
;
DELETE FROM T
FROM @T T
     JOIN sys.tables TemporalTables ON TemporalTables.name = T.TableName
     LEFT JOIN sys.tables HistoryTables ON HistoryTables.object_id = TemporalTables.history_table_id
WHERE HistoryTables.name = 'Historic.' + T.TableName
;
UPDATE T
SET  IsTemporal = CASE WHEN TemporalTables.history_table_id IS NULL THEN 0 ELSE 1 END,
     HasHistory = CASE WHEN EXISTS(SELECT 1 FROM sys.tables WHERE name = 'Historic.' + T.TableName) THEN 1 ELSE 0 END
FROM @T T
     JOIN sys.tables TemporalTables ON TemporalTables.name = T.TableName
     LEFT JOIN sys.tables HistoryTables ON HistoryTables.object_id = TemporalTables.history_table_id


DECLARE @SQL NVARCHAR(255);
DECLARE @TblName VARCHAR(255);
DECLARE @IsTemporal BIT;
DECLARE @HasHistory BIT;

DECLARE tbl_cursor CURSOR FOR  
SELECT  TableName, IsTemporal, HasHistory
FROM    @T
  
OPEN tbl_cursor;  
  
FETCH NEXT FROM tbl_cursor
INTO @TblName, @IsTemporal, @HasHistory;
  
WHILE @@FETCH_STATUS = 0  
BEGIN  
    IF @IsTemporal = 1
    BEGIN
        SET @SQL = 'ALTER TABLE dbo.[' + @TblName + '] SET (SYSTEM_VERSIONING = OFF)'
        EXEC(@SQL);
    END

    IF @HasHistory = 1
    BEGIN
        SET @SQL = 'DROP TABLE dbo.[Historic.' + @TblName + ']'
        EXEC(@SQL);
    END

    SET @SQL = 'ALTER TABLE dbo.[' + @TblName + '] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.' + @TblName + ']))'
    EXEC(@SQL);

    FETCH NEXT FROM tbl_cursor
    INTO @TblName, @IsTemporal, @HasHistory;
END  
  
CLOSE tbl_cursor;  
DEALLOCATE tbl_cursor;

-- Drop default history tables
DECLARE @Drop TABLE(SQLQuery NVARCHAR(1000));
INSERT INTO @Drop
SELECT 'DROP TABLE [' + name + ']'
FROM   sys.tables 
WHERE  name like 'MSSQL_TemporalHistory%'

DECLARE myCursor CURSOR FOR
SELECT SQLQuery
FROM   @Drop

OPEN myCursor
FETCH NEXT FROM myCursor INTO @SQL

WHILE @@FETCH_STATUS = 0
BEGIN
    EXEC(@SQL)
    FETCH NEXT FROM myCursor INTO @SQL
END

CLOSE myCursor
DEALLOCATE myCursor
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
