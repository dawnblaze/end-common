using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180403035446_Part_Category_and_CategoryLink_PK_Fix_END789")]
    public partial class Part_Category_and_CategoryLink_PK_Fix_END789 : Migration
    {
        private void CreateDependentObjects(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddPrimaryKey("PK_Part.Labor.Category", "Part.Labor.Category", new string[] { "BID", "ID" });
            migrationBuilder.CreateIndex("IX_Part.Labor.Data_ParentID_Name", "Part.Labor.Category", new string[] { "BID", "ParentID", "Name", "IsActive" });
            migrationBuilder.CreateIndex("IX_Part.Labor.Data_Name_IsActive", "Part.Labor.Category", new string[] { "BID", "Name", "IsActive", "ParentID" });
            migrationBuilder.AddForeignKey("FK_Part.Labor.Category_Part.Labor.Category", "Part.Labor.Category", new string[] { "BID", "ParentID" }, "Part.Labor.Category");

            migrationBuilder.AddPrimaryKey("PK_Part.Labor.CategoryLink", "Part.Labor.CategoryLink", new string[] { "BID", "PartID", "CategoryID" });
            migrationBuilder.CreateIndex("IX_Part.Labor.CategoryLink_Category", "Part.Labor.CategoryLink", new string[] { "BID", "CategoryID", "PartID" });
            migrationBuilder.AddForeignKey("FK_Part.Labor.CategoryLink_Part.Labor.Category", "Part.Labor.CategoryLink", new string[] { "BID", "CategoryID" }, "Part.Labor.Category");

            migrationBuilder.AddPrimaryKey("PK_Part.Machine.Category", "Part.Machine.Category", new string[] { "BID", "ID" });
            migrationBuilder.CreateIndex("IX_Part.Machine.Category_ParentID_Name", "Part.Machine.Category", new string[] { "BID", "ParentID", "Name", "IsActive" });
            migrationBuilder.CreateIndex("IX_Part.Machine.Category_Name_IsActive", "Part.Machine.Category", new string[] { "BID", "Name", "IsActive", "ParentID" });
            migrationBuilder.AddForeignKey("FK_Part.Machine.Category_Part.Machine.Category", "Part.Machine.Category", new string[] { "BID", "ParentID" }, "Part.Machine.Category");

            migrationBuilder.AddPrimaryKey("PK_Part.Machine.CategoryLink", "Part.Machine.CategoryLink", new string[] { "BID", "PartID", "CategoryID" });
            migrationBuilder.CreateIndex("IX_Part.Machine.CategoryLink_Category", "Part.Machine.CategoryLink", new string[] { "BID", "CategoryID", "PartID" });
            migrationBuilder.AddForeignKey("FK_Part.Machine.CategoryLink_Part.Machine.Category", "Part.Machine.CategoryLink", new string[] { "BID", "CategoryID" }, "Part.Machine.Category");
        }

        private void DropDependentObjects(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey("FK_Part.Machine.CategoryLink_Part.Machine.Category", "Part.Machine.CategoryLink");
            migrationBuilder.DropIndex("IX_Part.Machine.CategoryLink_Category", "Part.Machine.CategoryLink");
            migrationBuilder.DropPrimaryKey("PK_Part.Machine.CategoryLink", "Part.Machine.CategoryLink");

            migrationBuilder.DropForeignKey("FK_Part.Machine.Category_Part.Machine.Category", "Part.Machine.Category");
            migrationBuilder.DropIndex("IX_Part.Machine.Category_Name_IsActive", "Part.Machine.Category");
            migrationBuilder.DropIndex("IX_Part.Machine.Category_ParentID_Name", "Part.Machine.Category");
            migrationBuilder.DropPrimaryKey("PK_Part.Machine.Category", "Part.Machine.Category");

            migrationBuilder.DropForeignKey("FK_Part.Labor.CategoryLink_Part.Labor.Category", "Part.Labor.CategoryLink");
            migrationBuilder.DropIndex("IX_Part.Labor.CategoryLink_Category", "Part.Labor.CategoryLink");
            migrationBuilder.DropPrimaryKey("PK_Part.Labor.CategoryLink", "Part.Labor.CategoryLink");

            migrationBuilder.DropForeignKey("FK_Part.Labor.Category_Part.Labor.Category", "Part.Labor.Category");
            migrationBuilder.DropIndex("IX_Part.Labor.Data_Name_IsActive", "Part.Labor.Category");
            migrationBuilder.DropIndex("IX_Part.Labor.Data_ParentID_Name", "Part.Labor.Category");
            migrationBuilder.DropPrimaryKey("PK_Part.Labor.Category", "Part.Labor.Category");
        }

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            DropDependentObjects(migrationBuilder);

            migrationBuilder.AlterColumn<short>(
                name: "CategoryID",
                table: "Part.Machine.CategoryLink",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<short>(
                name: "ParentID",
                table: "Part.Machine.Category",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "ID",
                table: "Part.Machine.Category",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<short>(
                name: "CategoryID",
                table: "Part.Labor.CategoryLink",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<short>(
                name: "ParentID",
                table: "Part.Labor.Category",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "ID",
                table: "Part.Labor.Category",
                nullable: false,
                oldClrType: typeof(int));

            CreateDependentObjects(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            DropDependentObjects(migrationBuilder);

            migrationBuilder.AlterColumn<int>(
                name: "CategoryID",
                table: "Part.Machine.CategoryLink",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<int>(
                name: "ParentID",
                table: "Part.Machine.Category",
                nullable: true,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "Part.Machine.Category",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<int>(
                name: "CategoryID",
                table: "Part.Labor.CategoryLink",
                nullable: false,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<int>(
                name: "ParentID",
                table: "Part.Labor.Category",
                nullable: true,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ID",
                table: "Part.Labor.Category",
                nullable: false,
                oldClrType: typeof(short));

            CreateDependentObjects(migrationBuilder);
        }
    }
}

