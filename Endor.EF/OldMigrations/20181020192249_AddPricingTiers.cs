using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181020192249_AddPricingTiers")]
    public partial class AddPricingTiers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    INSERT INTO [enum.List.FlatListType] (ID, Name, IsAlphaSorted)
                    VALUES (100, 'Pricing Tiers', 0);
                "
            );

            migrationBuilder.Sql
            (
                @"
                    INSERT INTO [List.FlatList.Data] 
                      ( [BID], [ID], [FlatListType], [IsActive], [IsSystem], [Name], [IsAdHoc], [SortIndex] )
                    VALUES ( 1, 100, 100, 1, 1, 'Default', 0, 0 );
                "
            );

            migrationBuilder.Sql
            (
                @"
                    INSERT INTO [dbo].[System.Option.Category] 
                    ( [ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms] )
                VALUES
                    ( 605, 'Pricing Tiers', 600, 'Pricing Tiers', 2, 0,  'Pricing Tier Tiers Custom Customer Plans Special Specific' );
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Category] WHERE [ID] = 605;
DELETE FROM [List.FlatList.Data]  WHERE [BID] = 1 AND [ID] = 100;
DELETE FROM [enum.List.FlatListType] WHERE [ID] = 100;
");
        }
    }
}

