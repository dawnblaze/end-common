using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180709161309_END-1615_Origin_Industry_Remove_IsLocked_Column")]
    public partial class END1615_Origin_Industry_Remove_IsLocked_Column : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropDefaultConstraintIfExists("CRM.Origin", "IsLocked");
            migrationBuilder.DropColumn(
                name: "IsLocked",
                table: "CRM.Origin");

            migrationBuilder.DropDefaultConstraintIfExists("CRM.Industry", "IsLocked");
            migrationBuilder.DropColumn(
                name: "IsLocked",
                table: "CRM.Industry");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsLocked",
                table: "CRM.Origin",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsLocked",
                table: "CRM.Industry",
                nullable: false,
                defaultValue: false);
        }
    }
}

