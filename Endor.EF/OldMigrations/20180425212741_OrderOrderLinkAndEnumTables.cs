using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180425212741_OrderOrderLinkAndEnumTables")]
    public partial class OrderOrderLinkAndEnumTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Order.OrderLinkType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToEstimate = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when ([TransactionTypeSet]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToOrder = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when ([TransactionTypeSet]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToPO = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when ([TransactionTypeSet]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsDestinationLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when ([TransactionLevelSet]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsHeaderLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when ([TransactionLevelSet]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsItemLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when ([TransactionLevelSet]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    SymmetricLinkType = table.Column<byte>(type: "tinyint", nullable: false),
                    TransactionLevelSet = table.Column<byte>(type: "tinyint", nullable: false),
                    TransactionTypeSet = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Order.OrderLinkType", x => x.ID);
                });           

            // Using sql here because of the "Generated Always as" 
            migrationBuilder.Sql(@"CREATE TABLE [dbo].[Order.OrderLink](
    [BID] [smallint] NOT NULL,
    [OrderID] [int] NOT NULL,
    [LinkType] [tinyint] NOT NULL,
    [LinkedOrderID] [int] NOT NULL,
    [ModifiedDT] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT (SYSUTCDATETIME()),
    [ValidToDT] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL DEFAULT (CONVERT([datetime2](7),'9999-12-31 23:59:59.99999999')),
    PERIOD FOR SYSTEM_TIME([ModifiedDT], [ValidToDT]),
    [LinkedFormattedNumber] [varchar](12) NOT NULL,
    [Description] [varchar](255) NOT NULL,
    [Amount] [decimal](18, 4) SPARSE  NULL,
    [EmployeeID] [smallint] NULL,

    CONSTRAINT [PK_Order.OrderLink] PRIMARY KEY ( [BID], [OrderID], [LinkType], [LinkedOrderID] )
)
    WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.OrderLink]));");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Order.OrderLinkType");

            migrationBuilder.Sql(@"
ALTER TABLE[Order.OrderLink] SET(SYSTEM_VERSIONING = OFF);
");

            migrationBuilder.DropTable(
                name: "Historic.Order.OrderLink");

            migrationBuilder.DropTable(
                name: "Order.OrderLink");
        }
    }
}

