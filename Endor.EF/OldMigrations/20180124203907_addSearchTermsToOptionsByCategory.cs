using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180124203907_addSearchTermsToOptionsByCategory")]
    public partial class addSearchTermsToOptionsByCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE name = 'Option.Categories.ByLevel')
	DROP Function [dbo].[Option.Categories.ByLevel]
GO
CREATE FUNCTION [dbo].[Option.Categories.ByLevel] ( @Level tinyint )
		RETURNS TABLE
AS

RETURN 

SELECT
TOP 1000
	a.[ID]
	,a.[Name]
	,a.[SectionID]
	,a.[Description]
	,a.[OptionLevels]
	,a.[IsHidden]
	,a.[IsSystemOption]
	,a.[IsAssociationOption]
	,a.[IsBusinessOption]
	,a.[IsLocationOption]
	,a.[IsStorefrontOption]
	,a.[IsEmployeeOption]
	,a.[IsCompanyOption]
	,a.[IsContactOption]
	,a.[SearchTerms]
FROM [dbo].[System.Option.Category] a
WHERE OptionLevels = @Level
ORDER BY a.[Name] ASC
GO
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE name = 'Option.Categories.ByLevel')
	DROP Function [dbo].[Option.Categories.ByLevel]
GO
CREATE FUNCTION [dbo].[Option.Categories.ByLevel] ( @Level tinyint )
		RETURNS TABLE
AS

RETURN 

SELECT
TOP 1000
	a.[ID]
	,a.[Name]
	,a.[SectionID]
	,a.[Description]
	,a.[OptionLevels]
	,a.[IsHidden]
	,a.[IsSystemOption]
	,a.[IsAssociationOption]
	,a.[IsBusinessOption]
	,a.[IsLocationOption]
	,a.[IsStorefrontOption]
	,a.[IsEmployeeOption]
	,a.[IsCompanyOption]
	,a.[IsContactOption]
FROM [dbo].[System.Option.Category] a
WHERE OptionLevels = @Level
ORDER BY a.[Name] ASC
GO
");
        }
    }
}

