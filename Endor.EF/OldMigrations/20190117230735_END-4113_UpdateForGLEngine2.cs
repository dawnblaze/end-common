using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4113_UpdateForGLEngine2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "IncomeAllocationType",
                table: "Order.Item.Component",
                type: "tinyint sparse",
                nullable: true,
                oldClrType: typeof(byte),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "IncomeAllocationType",
                table: "Order.Item.Component",
                nullable: true,
                oldClrType: typeof(byte),
                oldType: "tinyint sparse",
                oldNullable: true);
        }
    }
}
