using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181015014145_Alter_CustomField.Definition")]
    public partial class Alter_CustomFieldDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP INDEX [IX_CustomField.Definition_AppliesTo] ON [CustomField.Definition]
ALTER TABLE [CustomField.Definition] ALTER COLUMN [Name] VARCHAR(200) NOT NULL;
ALTER TABLE [CustomField.Definition] ALTER COLUMN [Label] VARCHAR(200);
ALTER TABLE [CustomField.Definition] ALTER COLUMN [Description] VARCHAR(max);
CREATE INDEX [IX_CustomField.Definition_AppliesTo] ON [CustomField.Definition] ([BID], [Name], [IsActive], [AppliesToClassTypeID], [AppliesToID]);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP INDEX [IX_CustomField.Definition_AppliesTo] ON [CustomField.Definition]
ALTER TABLE [CustomField.Definition] ALTER COLUMN [Name] VARCHAR(100) NOT NULL;
ALTER TABLE [CustomField.Definition] ALTER COLUMN [Label] VARCHAR(100);
ALTER TABLE [CustomField.Definition] ALTER COLUMN [Description] VARCHAR(32);
CREATE INDEX [IX_CustomField.Definition_AppliesTo] ON [CustomField.Definition] ([BID], [Name], [IsActive], [AppliesToClassTypeID], [AppliesToID]);
            ");
        }
    }
}

