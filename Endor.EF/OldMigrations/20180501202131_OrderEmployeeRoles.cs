using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180501202131_OrderEmployeeRoles")]
    public partial class OrderEmployeeRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Employee.Role");

            migrationBuilder.CreateTable(
                name: "enum.Employee.RoleType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    IsUserEntered = table.Column<bool>(type: "bit", nullable: false),
                    AppliesToTeam = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToOrder = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToEstimate = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToPO = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToTeamText = table.Column<string>(maxLength: 11, nullable: true, computedColumnSql: "(case [AppliesToTeam] when (0) then 'Not Allowed' when (1) then 'Optional' when (2) then 'Mandatory' end)"),
                    AppliesToOrderText = table.Column<string>(maxLength: 11, nullable: true, computedColumnSql: "(case [AppliesToOrder] when (0) then 'Not Allowed' when (1) then 'Optional' when (2) then 'Mandatory' end)"),
                    AppliesToEstimateText = table.Column<string>(maxLength: 11, nullable: true, computedColumnSql: "(case [AppliesToEstimate] when (0) then 'Not Allowed' when (1) then 'Optional' when (2) then 'Mandatory' end)"),
                    AppliesToPOText = table.Column<string>(maxLength: 11, nullable: true, computedColumnSql: "(case [AppliesToPO] when (0) then 'Not Allowed' when (1) then 'Optional' when (2) then 'Mandatory' end)")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Employee.RoleType", x => x.ID);
                });

            migrationBuilder.Sql(@"
INSERT [enum.Employee.RoleType] ([ID], [Name], [IsUserEntered], [AppliesToTeam], [AppliesToOrder], [AppliesToEstimate], [AppliesToPO])
VALUES (1, N'Salesperson', 1, 1, 1, 1, 1)
    , (2, N'CSR', 1, 1, 1, 1, 1)
    , (3, N'Designer', 1, 1, 1, 1, 0)
    , (4, N'Project Manager', 1, 0, 1, 1, 1)
    , (5, N'Account Manager', 1, 1, 1, 1, 1)
    , (6, N'Production', 1, 1, 1, 1, 0)
    , (9, N'QA', 1, 1, 1, 1, 0)
    , (41, N'Requestor', 1, 0, 0, 0, 2)
    , (42, N'Approver', 1, 0, 0, 0, 1)
    , (43, N'Orderer', 1, 0, 0, 0, 1)
    , (44, N'Receiver', 1, 0, 0, 0, 1)
    , (61, N'Shipper', 1, 1, 1, 1, 0)
    , (62, N'Installer', 1, 1, 1, 1, 0)
    , (63, N'Delivery Person', 1, 1, 1, 1, 0)
    , (64, N'Service Person', 1, 1, 1, 1, 0)
    , (253, N'Assigned To', 0, 0, 1, 1, 0)
    , (254, N'Entered By', 0, 2, 2, 2, 2)
    , (255, N'Observer', 1, 1, 1, 1, 1)
;    ;");

            // Using sql here because of the "Generated Always as" 
            migrationBuilder.Sql(@"
CREATE TABLE [dbo].[Order.Employee.Role](
    [BID] [smallint] NOT NULL,
    [ID] [int] NOT NULL,
    [ClassTypeID]  AS ((10014)),
    [ModifiedDT] DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT SysUTCDateTIme(),
    [ValidToDT] DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL DEFAULT CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'),
    PERIOD FOR SYSTEM_TIME(ModifiedDT, ValidToDT),
    [OrderID] [int] NOT NULL,
    [OrderItemID] [int] NULL,
    [DestinationID] [int] NULL,
    [RoleType] [tinyint] NOT NULL,
    [EmployeeID] [smallint] NULL,
    [EmployeeName] [varchar](255) NULL,
    [IsOrderRole]  AS (isnull(case when [OrderItemID] IS NULL AND [DestinationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsOrderItemRole]  AS (isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0))),
    [IsDestinationRole]  AS (isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0))),
    CONSTRAINT [PK_Order.Employee.Role] PRIMARY KEY ( [BID] ASC, [ID] ASC )
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.Order.Employee.Role]));");



            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_EmployeeID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "EmployeeID", "OrderID", "RoleType" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_DestinationID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "DestinationID", "RoleType", "EmployeeID" },
                filter: "[DestinationID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_OrderItemID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "OrderItemID", "RoleType", "EmployeeID" },
                filter: "[OrderItemID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_OrderID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "OrderID", "RoleType", "EmployeeID", "IsOrderRole" });

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Employee.Role_EmployeeID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "EmployeeID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Employee.Role_OrderID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "OrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.AddEmployeeRole]( @BID tinyint, @OrderID int, @EmployeeRoleType tinyint, @EmployeeID int )
--
-- Description: This function adds a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.AddEmployeeRole] @BID=1, @OrderID=1, @EmployeeRoleType=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.AddEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleType TINYINT   -- = 1
        , @EmployeeID      INT        -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the role type does not exist on the order
    IF ISNULL(@EmployeeRoleType, -1) != 255 AND EXISTS(SELECT * FROM [Order.Employee.Role] WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @EmployeeRoleType)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Employee role already exist on this order.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @NewID INT;
    EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10014, 1;

    INSERT INTO [Order.Employee.Role]
    (BID, ID, OrderID, RoleType, EmployeeID, EmployeeName)
    SELECT @BID, @NewID, @OrderID, @EmployeeRoleType, @EmployeeID, employee.ShortName
    FROM   [Employee.Data] employee
    WHERE  employee.BID = @BID AND employee.ID = @EmployeeID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO

-- ========================================================
-- Name: [Order.Action.UpdateEmployeeRole]( @BID tinyint, @OrderID int, @EmployeeRoleType tinyint, @EmployeeID int )
--
-- Description: This function updates a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateEmployeeRole] @BID=1, @OrderID=1, @EmployeeRoleType=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.UpdateEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleType TINYINT   -- = 1
        , @EmployeeID       INT       -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF ISNULL(@EmployeeRoleType,-1) = 255
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to change this employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    SELECT @ID = ID
    FROM [Order.Employee.Role]
    WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @EmployeeRoleType

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    UPDATE OER
    SET    EmployeeID = @EmployeeID, EmployeeName = employee.ShortName
    FROM   [Order.Employee.Role] OER 
            LEFT JOIN [Employee.Data] employee ON employee.BID = @BID AND employee.ID = @EmployeeID
    WHERE  OER.BID = @BID AND OER.ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO

-- ========================================================
-- Name: [Order.Action.RemoveEmployeeRole]( @BID tinyint, @OrderID int, @EmployeeRoleType tinyint, @EmployeeID int )
--
-- Description: This function remove a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.RemoveEmployeeRole] @BID=1, @OrderID=1, @EmployeeRoleType=1, @EmployeeID=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.RemoveEmployeeRole]
-- DECLARE 
          @BID             TINYINT    -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleType TINYINT   -- = 1
        , @EmployeeID       INT       -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    SELECT @ID = ID
    FROM [Order.Employee.Role]
    WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @EmployeeRoleType AND EmployeeID =  @EmployeeID

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Delete record is exists
    DELETE FROM OER
    FROM   [Order.Employee.Role] OER
    WHERE  BID = @BID AND ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.AddEmployeeRole]
GO
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.UpdateEmployeeRole]
GO
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.RemoveEmployeeRole]
");


            migrationBuilder.Sql(@"
ALTER TABLE[Order.Employee.Role] SET(SYSTEM_VERSIONING = OFF);
");

            migrationBuilder.DropTable(
                name: "Historic.Order.Employee.Role");
            
            migrationBuilder.DropTable(
                name: "Order.Employee.Role");

            migrationBuilder.DropTable(
                name: "enum.Employee.RoleType");


            migrationBuilder.CreateTable(
                name: "enum.Employee.Role",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Employee.Role", x => x.ID);
                });

            migrationBuilder.Sql(@"
            INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(0, N'Member');
            INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(1, N'Salesperson');
            INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(2, N'CSR');
            INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(3, N'Designer');
            INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(4, N'Project Manager');
            INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(11, N'Salesperson 2');
            INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(12, N'CSR 2');
            INSERT [enum.Employee.Role] ([ID], [Name]) VALUES(13, N'Designer 2');");
        }
    }
}

