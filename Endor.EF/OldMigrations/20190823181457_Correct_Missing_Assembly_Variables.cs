using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Correct_Missing_Assembly_Variables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Part.Subassembly.Variable]
SET Name = 'Quantity'
WHERE Name = 'AssemblyQuantity'
;
UPDATE [Part.Subassembly.Variable]
SET Name = 'Tier'
WHERE Name = 'AssemblyTier'
            ");

            migrationBuilder.Sql(@"
DECLARE @NewVariables TABLE(BID SMALLINT, ID INT, DataType SMALLINT, DefaultValue NVARCHAR(MAX), IsRequired BIT, IsFormula BIT, Name NVARCHAR(255)
                          , SubassemblyID INT, AllowDecimals BIT, DecimalPlaces TINYINT, DisplayType TINYINT, ElementType TINYINT
                          , Label NVARCHAR(255), LabelType TINYINT, ListDataType SMALLINT, AllowCustomValue BIT, AllowMultiSelect BIT
                          , IsDisabled BIT, IsConsumptionFormula BIT, RollupLinkedPriceAndCost BIT);


INSERT INTO @NewVariables
(  BID, DataType, DefaultValue, IsRequired, IsFormula, Name, SubAssemblyID
 , AllowDecimals, DecimalPlaces, DisplayType, ElementType
 , Label, LabelType, ListDataType, AllowCustomValue, AllowMultiSelect
 , IsDisabled, IsConsumptionFormula, RollupLinkedPriceAndCost)
SELECT
   A.BID, 2 As DataType,'1' AS DefaultValue, 0 AS IsRequired, 0 AS IsFormula, 'Quantity' AS Name, A.ID AS SubAssemblyID
 , 0 AS AllowDecimals,0 AS DecimalPlaces, 0 AS DisplayType, 21 AS ElementType
 , 'Quantity' AS Label, NULL AS LabelType, NULL AS ListDataType, NULL AS AllowCustomValue, NULL AS AllowMultiSelect
 , 0 AS IsDisabled, 0 AS IsConsumptionFormula, 0 AS RollupLinkedPriceAndCost
FROM [Part.Subassembly.Data] A
WHERE BID > 0
      AND NOT EXISTS (SELECT * FROM [Part.Subassembly.Variable] V WHERE V.Name = 'Quantity' AND V.BID = A.BID AND V.SubassemblyID = A.ID)
;
INSERT INTO @NewVariables
(BID, DataType, DefaultValue, IsRequired, IsFormula, Name, SubAssemblyID
 , AllowDecimals, DecimalPlaces, DisplayType, ElementType
 , Label, LabelType, ListDataType, AllowCustomValue, AllowMultiSelect
 , IsDisabled, IsConsumptionFormula, RollupLinkedPriceAndCost)
SELECT
   A.BID, 2 As DataType, NULL AS DefaultValue, 1 AS IsRequired, 0 AS IsFormula, 'Price' AS Name, A.ID AS SubAssemblyID
 , 1 AS AllowDecimals, 2 AS DecimalPlaces, 2 AS DisplayType, 21 AS ElementType
 , 'Total Retail Price' AS Label, NULL AS LabelType, NULL AS ListDataType, NULL AS AllowCustomValue, NULL AS AllowMultiSelect
 , 0 AS IsDisabled, 0 AS IsConsumptionFormula, 0 AS RollupLinkedPriceAndCost
FROM [Part.Subassembly.Data] A
WHERE BID > 0
      AND AssemblyType = 0
      AND NOT EXISTS (SELECT * FROM [Part.Subassembly.Variable] V WHERE V.Name = 'Price' AND V.BID = A.BID AND V.SubassemblyID = A.ID)
;
INSERT INTO @NewVariables
(BID, DataType, DefaultValue, IsRequired, IsFormula, Name, SubAssemblyID
 , AllowDecimals, DecimalPlaces, DisplayType, ElementType
 , Label, LabelType, ListDataType, AllowCustomValue, AllowMultiSelect
 , IsDisabled, IsConsumptionFormula, RollupLinkedPriceAndCost)
SELECT
   A.BID, 1 As DataType, '=CompanyTier();' AS DefaultValue, 1 AS IsRequired, 1 AS IsFormula, 'Tier' AS Name, A.ID AS SubAssemblyID
 , NULL AS AllowDecimals, NULL AS DecimalPlaces, NULL AS DisplayType, 1 AS ElementType
 , 'Tier' AS Label, 0 AS LabelType, NULL AS ListDataType, NULL AS AllowCustomValue, NULL AS AllowMultiSelect
 , 0 AS IsDisabled, 0 AS IsConsumptionFormula, 0 AS RollupLinkedPriceAndCost
FROM [Part.Subassembly.Data] A
WHERE BID > 0
      AND AssemblyType = 0
      AND NOT EXISTS (SELECT * FROM [Part.Subassembly.Variable] V WHERE V.Name = 'Tier' AND V.BID = A.BID AND V.SubassemblyID = A.ID)
;
INSERT INTO @NewVariables
(BID, DataType, DefaultValue, IsRequired, IsFormula, Name, SubAssemblyID
 , AllowDecimals, DecimalPlaces, DisplayType, ElementType
 , Label, LabelType, ListDataType, AllowCustomValue, AllowMultiSelect
 , IsDisabled, IsConsumptionFormula, RollupLinkedPriceAndCost)
SELECT
   A.BID, 1 As DataType, NULL AS DefaultValue, 1 AS IsRequired, 0 AS IsFormula, 'Profile' AS Name, A.ID AS SubAssemblyID
 , NULL AS AllowDecimals, NULL AS DecimalPlaces, 1 AS DisplayType, 13 AS ElementType
 , 'Profile' AS Label, 0 AS LabelType, 1 AS ListDataType, 0 AS AllowCustomValue, 0 AS AllowMultiSelect
 , 0 AS IsDisabled, 0 AS IsConsumptionFormula, 0 AS RollupLinkedPriceAndCost
FROM [Part.Subassembly.Data] A
WHERE BID > 0
      AND AssemblyType = 3
      AND NOT EXISTS (SELECT * FROM [Part.Subassembly.Variable] V WHERE V.Name = 'Profile' AND V.BID = A.BID AND V.SubassemblyID = A.ID)
;
INSERT INTO @NewVariables
(BID, DataType, DefaultValue, IsRequired, IsFormula, Name, SubAssemblyID
 , AllowDecimals, DecimalPlaces, DisplayType, ElementType
 , Label, LabelType, ListDataType, AllowCustomValue, AllowMultiSelect
 , IsDisabled, IsConsumptionFormula, RollupLinkedPriceAndCost)
SELECT
   A.BID, 1 As DataType, NULL AS DefaultValue, 0 AS IsRequired, 0 AS IsFormula, 'Instance' AS Name, A.ID AS SubAssemblyID
 , NULL AS AllowDecimals, NULL AS DecimalPlaces, 1 AS DisplayType, 13 AS ElementType
 , 'Instance' AS Label, 0 AS LabelType, 1 AS ListDataType, 0 AS AllowCustomValue, 0 AS AllowMultiSelect
 , 0 AS IsDisabled, 0 AS IsConsumptionFormula, 0 AS RollupLinkedPriceAndCost
FROM [Part.Subassembly.Data] A
WHERE BID > 0
      AND AssemblyType = 3
      AND NOT EXISTS (SELECT * FROM [Part.Subassembly.Variable] V WHERE V.Name = 'Instance' AND V.BID = A.BID AND V.SubassemblyID = A.ID)
;



UPDATE T
SET ID = T.NewID 
FROM ( 
       SELECT V.ID, ISNULL(I.ID, 1000) + ROW_NUMBER() OVER (PARTITION BY V.BID ORDER BY V.SubassemblyID) AS NewID
       FROM @NewVariables V 
            LEFT JOIN ( SELECT BID, MAX(ID) ID FROM [Part.Subassembly.Variable] GROUP BY BID ) I ON I.BID = V.BID
     ) T

INSERT INTO [Part.Subassembly.Variable]
(  BID, ID, DataType, DefaultValue, IsRequired, IsFormula, Name, SubAssemblyID
 , AllowDecimals, DecimalPlaces, DisplayType, ElementType
 , Label, LabelType, ListDataType, AllowCustomValue, AllowMultiSelect
 , IsDisabled, IsConsumptionFormula, RollupLinkedPriceAndCost)
SELECT *
FROM @NewVariables 



-- Update Next IDs

DECLARE @BID SMALLINT;
DECLARE @NextID INT;

DECLARE cur CURSOR LOCAL FOR
SELECT BID, MAX(ID)+1
FROM   @NewVariables
GROUP BY BID

OPEN cur

FETCH NEXT FROM cur INTO @BID, @NextID

WHILE @@FETCH_STATUS = 0
BEGIN
    EXEC [Util.ID.SetID] 
            @BID = @BID
          , @ClassTypeID = 12046
          , @NextID = @NextID
          , @AvoidLowering = 1

    FETCH NEXT FROM cur INTO @BID, @NextID
END

CLOSE cur
DEALLOCATE cur
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
