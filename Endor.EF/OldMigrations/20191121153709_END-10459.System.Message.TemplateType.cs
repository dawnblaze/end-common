﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10459SystemMessageTemplateType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"Delete [System.Message.TemplateType]
            ;");

            migrationBuilder.Sql(@"
                INSERT INTO [System.Message.TemplateType] ( [AppliesToClassTypeID], [ID], [Name], [IsSystem], [ChannelType] )
                VALUES
                (  5000, 1, 'New Employee Login Email', 1, 4 )
                , (  5000, 2, 'Employee Password Reset Email', 1, 4 )
                , (  3000, 11, 'New Contact Login Email', 1, 4 )
                , (  3000, 12, 'Contact Password Reset Email', 1, 4 )
                , (  3000, 0, 'General Contact Email', 0, 4 )
                , (  2000, 1, 'Send Statement Email', 0, 4 )
                , (  2000, 0, 'General Company Email', 0, 4 )
                , (  5000, 0, 'General Email', 0, 4 )
                , ( 10000, 0, 'General Order Email', 0, 4 )
                , ( 10000, 1, 'Send Invoice Email', 0, 4 )
                , ( 10000, 2, 'Send Work Order Email', 0, 4 )
                , ( 10000, 3, 'Send Packing Slip Email', 0, 4 )
                , ( 10000, 4, 'New Order Confirmation Email', 0, 4 )
                , ( 10000, 5, 'New Ecommerce Order Confirmation Email', 0, 4 )
                , ( 10000, 6, 'Order Ready for Pickup Email', 0, 4 )
                , ( 10000, 7, 'Shipping Confirmation Email', 0, 4 )
                , ( 10000, 8, 'Dunning Email', 0, 4 )
                , ( 10200, 0, 'General Estimate Email', 0, 4 )
                , ( 10200, 1, 'Send Estimate Email', 0, 4 )
                , ( 10200, 2, 'Estimate Followup Email', 0, 4 )
                , ( 10300, 0, 'General Credit Memo Email', 0, 4 )
                , ( 10300, 1, 'Send Credit Memo Email', 0, 4 )
                , ( 10400, 0, 'General Purchase Order Email', 0, 4 )
                , ( 10400, 1, 'Send Purchase Order Email', 0, 4 )
                ;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
