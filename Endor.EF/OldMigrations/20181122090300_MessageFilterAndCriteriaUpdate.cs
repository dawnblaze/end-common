using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class MessageFilterAndCriteriaUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=14109;
            ");

            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE [TargetClassTypeID]=14109;
            ");

            migrationBuilder.Sql(@"
                INSERT INTO[dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID]
                       ,[Name]
                       ,[Label]
                       ,[Field]
                       ,[IsHidden]
                       ,[DataType]
                       ,[InputType]
                       ,[AllowMultiple]
                       ,[ListValues]
                       ,[ListValuesEndpoint]
                       ,[IsLimitToList]
                       ,[SortIndex]
                       ,[ID])
                   VALUES
                       (14109 --<TargetClassTypeID, int,>
                        ,'IsRead' --<Name, varchar(255),>
                        ,'Show Read Messages'  --<Label, varchar(255),>
                        ,'IsRead' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,2 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,0 --<SortIndex, tinyint,>
                        ,(SELECT (MAX(ID)+1) FROM[dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");

            migrationBuilder.Sql(@"
                    INSERT INTO [dbo].[List.Filter]
                        ([BID]
                        ,[ID]
                        ,[CreatedDate]
                        ,[ModifiedDT]
                        ,[IsActive]
                        ,[Name]
                        ,[TargetClassTypeID]
                        ,[IDs]
                        ,[Criteria]
                        ,[OwnerID]
                        ,[IsPublic]
                        ,[IsSystem]
                        ,[Hint]
                        ,[IsDefault]
                        ,[SortIndex])
                    VALUES
                        (1--<BID, smallint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]) --<ID, int,>
                        ,GETUTCDATE()
                        ,GETUTCDATE()
                        ,1--<IsActive, bit,>
                        ,'Inbox'--<Name, varchar(255),>
                        ,14109--<TargetClassTypeID, int,>
                        ,NULL--<IDs, xml,>
                        ,'<ArrayOfListFilterItem>
						  <ListFilterItem>
							<SearchValue>false</SearchValue>
							<Field>IsDeleted</Field>
							<IsHidden>true</IsHidden>
							<IsSystem>true</IsSystem>
						  </ListFilterItem>
						  <ListFilterItem>
							<SearchValue>false</SearchValue>
							<Field>IsSentFolder</Field>
							<IsHidden>true</IsHidden>
							<IsSystem>true</IsSystem>
						  </ListFilterItem>
						  <ListFilterItem>
							<SearchValue>false</SearchValue>
							<Field>IsRead</Field>
							<IsHidden>true</IsHidden>
							<IsSystem>true</IsSystem>
						  </ListFilterItem>
						</ArrayOfListFilterItem>'
                        ,NULL--<OwnerID, smallint,>
                        ,0--<IsPublic, bit,>
                        ,1--<IsSystem, bit,>
                        ,NULL--<Hint, varchar(max),>
                        ,1--<IsDefault, bit,>
                        ,0--<SortIndex, tinyint,>
                        );

                INSERT INTO [dbo].[List.Filter]
                        ([BID]
                        ,[ID]
                        ,[CreatedDate]
                        ,[ModifiedDT]
                        ,[IsActive]
                        ,[Name]
                        ,[TargetClassTypeID]
                        ,[IDs]
                        ,[Criteria]
                        ,[OwnerID]
                        ,[IsPublic]
                        ,[IsSystem]
                        ,[Hint]
                        ,[IsDefault]
                        ,[SortIndex])
                    VALUES
                        (1--<BID, smallint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]) --<ID, int,>
                        ,GETUTCDATE()
                        ,GETUTCDATE()
                        ,1--<IsActive, bit,>
                        ,'Sent'--<Name, varchar(255),>
                        ,14109--<TargetClassTypeID, int,>
                        ,NULL--<IDs, xml,>
                        ,'<ArrayOfListFilterItem>
						  <ListFilterItem>
							<SearchValue>false</SearchValue>
							<Field>IsDeleted</Field>
							<IsHidden>true</IsHidden>
							<IsSystem>true</IsSystem>
						  </ListFilterItem>
						  <ListFilterItem>
							<SearchValue>true</SearchValue>
							<Field>IsSentFolder</Field>
							<IsHidden>true</IsHidden>
							<IsSystem>true</IsSystem>
						  </ListFilterItem>
						</ArrayOfListFilterItem>'
                        ,NULL--<OwnerID, smallint,>
                        ,0--<IsPublic, bit,>
                        ,1--<IsSystem, bit,>
                        ,NULL--<Hint, varchar(max),>
                        ,1--<IsDefault, bit,>
                        ,1--<SortIndex, tinyint,>
                        );

                INSERT INTO [dbo].[List.Filter]
                        ([BID]
                        ,[ID]
                        ,[CreatedDate]
                        ,[ModifiedDT]
                        ,[IsActive]
                        ,[Name]
                        ,[TargetClassTypeID]
                        ,[IDs]
                        ,[Criteria]
                        ,[OwnerID]
                        ,[IsPublic]
                        ,[IsSystem]
                        ,[Hint]
                        ,[IsDefault]
                        ,[SortIndex])
                    VALUES
                        (1--<BID, smallint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[List.Filter]) --<ID, int,>
                        ,GETUTCDATE()
                        ,GETUTCDATE()
                        ,1--<IsActive, bit,>
                        ,'Deleted'--<Name, varchar(255),>
                        ,14109--<TargetClassTypeID, int,>
                        ,NULL--<IDs, xml,>
                        ,'<ArrayOfListFilterItem>
						  <ListFilterItem>
							<SearchValue>true</SearchValue>
							<Field>IsDeleted</Field>
							<IsHidden>true</IsHidden>
							<IsSystem>true</IsSystem>
						  </ListFilterItem>
						</ArrayOfListFilterItem>'
                        ,NULL--<OwnerID, smallint,>
                        ,0--<IsPublic, bit,>
                        ,1--<IsSystem, bit,>
                        ,NULL--<Hint, varchar(max),>
                        ,1--<IsDefault, bit,>
                        ,2--<SortIndex, tinyint,>
                        );
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=14109;
            ");

            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE [TargetClassTypeID]=14109;
            ");
        }
    }
}
