using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180427154848_OrderContactRoleType")]
    public partial class OrderContactRoleType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Order.ContactRoleType ",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    TransactionLevelSet = table.Column<byte>(type: "tinyint", nullable: false),
                    TransactionTypeSet = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToEstimate = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when ([TransactionTypeSet]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToOrder = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when ([TransactionTypeSet]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToPO = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when ([TransactionTypeSet]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsDestinationLevel = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when ([TransactionLevelSet]&(2))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsHeaderLevel = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when ([TransactionLevelSet]&(1))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsItemLevel = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when ([TransactionLevelSet]&(4))<>(0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Order.ContactRoleType ", x => x.ID);
                });

            migrationBuilder.Sql(@"
INSERT [enum.Order.ContactRoleType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (1, N'Primary', 3, 1);
INSERT [enum.Order.ContactRoleType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (2, N'Billing', 0, 1);
INSERT [enum.Order.ContactRoleType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (3, N'ShipTo', 3, 2);
INSERT [enum.Order.ContactRoleType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (4, N'ShipFrom', 3, 2);
INSERT [enum.Order.ContactRoleType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (11, N'Design Approval', 3, 5);
INSERT [enum.Order.ContactRoleType] ([ID], [Name], [TransactionTypeSet], [TransactionLevelSet]) VALUES (12, N'Design Reviewer', 3, 5);
");

            // Using sql here because of the "Generated Always as" 
            migrationBuilder.Sql(@"
CREATE TABLE [dbo].[Order.Contact.Role](
    [BID] [smallint] NOT NULL,
    [ID] [int] NOT NULL,
    [ClassTypeID]  AS ((10013)),
    [ModifiedDT] DATETIME2(7) GENERATED ALWAYS AS ROW START NOT NULL DEFAULT SysUTCDateTIme(),
    [ValidToDT] DATETIME2(7) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL DEFAULT CONVERT(DATETIME2(7), '9999-12-31 23:59:59.99999999'),
    PERIOD FOR SYSTEM_TIME(ModifiedDT, ValidToDT),
    [OrderID] [int] NOT NULL,
    [OrderItemID] [int] NULL,
    [DestinationID] [int] NULL,
    [RoleType] [tinyint] NOT NULL,
    [IsAdHoc] [bit] NOT NULL,
    [ContactID] [int] NULL,
    [ContactName] [varchar](255) NULL,
    [ContactCompany] [varchar](255) SPARSE  NULL,
    [IsOrderRole]  AS (isnull(case when [OrderItemID] IS NULL AND [DestinationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0))),
    [IsOrderItemRole]  AS (isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0))),
    [IsDestinationRole]  AS (isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0))),
    CONSTRAINT [PK_Order.Contact.Role] PRIMARY KEY ( [BID] ASC, [ID] ASC )
);
");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Role_ContactID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "ContactID", "OrderID", "RoleType" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Role_DestinationID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "DestinationID", "RoleType", "ContactID" },
                filter: "[DestinationID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Role_OrderItemID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "OrderItemID", "RoleType", "ContactID" },
                filter: "[OrderItemID] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Role_OrderID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "OrderID", "RoleType", "ContactID", "IsOrderRole" });

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Contact.Role_Contact.Data_BID_ContactID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "ContactID" },
                principalTable: "Contact.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Contact.Role_OrderID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "OrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);


            migrationBuilder.Sql(@"
ALTER TABLE[Order.Data] SET(SYSTEM_VERSIONING = ON(HISTORY_TABLE = dbo.[Historic.Order.Data]));
ALTER TABLE[Order.Note] SET(SYSTEM_VERSIONING = ON(HISTORY_TABLE = dbo.[Historic.Order.Note]));
ALTER TABLE[Order.KeyDate] SET(SYSTEM_VERSIONING = ON(HISTORY_TABLE = dbo.[Historic.Order.KeyDate]));
ALTER TABLE[Order.Contact.Role] SET(SYSTEM_VERSIONING = ON(HISTORY_TABLE = dbo.[Historic.Order.Contact.Role]));
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE[Order.Data] SET(SYSTEM_VERSIONING = OFF);
ALTER TABLE[Order.Note] SET(SYSTEM_VERSIONING = OFF);
ALTER TABLE[Order.KeyDate] SET(SYSTEM_VERSIONING = OFF);
ALTER TABLE[Order.Contact.Role] SET(SYSTEM_VERSIONING = OFF);
");

            migrationBuilder.DropTable(
                name: "Historic.Order.Data");

            migrationBuilder.DropTable(
                name: "Historic.Order.Note");

            migrationBuilder.DropTable(
                name: "Historic.Order.KeyDate");

            migrationBuilder.DropTable(
                name: "Historic.Order.Contact.Role");

            migrationBuilder.DropTable(
                name: "Order.Contact.Role");

            migrationBuilder.DropTable(
                name: "enum.Order.ContactRoleType ");
        }
    }
}

