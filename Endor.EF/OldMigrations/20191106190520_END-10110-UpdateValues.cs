﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10110UpdateValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TransactionTypeText",
                table: "enum.Order.OrderStatus",
                unicode: false,
                maxLength: 14,
                nullable: false,
                computedColumnSql: "(case [TransactionType] when(1) then 'Estimate' when(2) then 'Order' when(4) then 'Purchase Order' when(8) then 'Credit Memo' when(32) then 'Opportunity' when(64) then 'Destination' else 'Unknown' end)",
                oldClrType: typeof(string),
                oldType: "varchar(14)",
                oldUnicode: false,
                oldMaxLength: 14,
                oldComputedColumnSql: "(case [TransactionType] when(1) then 'Estimate' when(2) then 'Order' when(4) then 'Purchase Order' when(32) then 'Opportunity' when(64) then 'Destination' else 'Unknown' end)");

            migrationBuilder.Sql(@"
                ALTER TABLE[Order.Item.Status]
                DROP COLUMN TransactionTypeText;

                ALTER TABLE[Order.Item.Status]
                ADD TransactionTypeText AS
                (case [TransactionType] when (1) then 'Estimate' when (2) then 'Order' when (4) then 'PO' when (8) then 'CreditMemo' when (32) then 'Opportunity' when (64) then 'Destination' else 'Unknown' end)
                ;
            ");

            migrationBuilder.Sql(@"
                Update [Order.Item.Status]
                Set IsDefault=1 Where TransactionType = 8
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TransactionTypeText",
                table: "enum.Order.OrderStatus",
                type: "varchar(14)",
                unicode: false,
                maxLength: 14,
                nullable: false,
                computedColumnSql: "(case [TransactionType] when(1) then 'Estimate' when(2) then 'Order' when(4) then 'Purchase Order' when(32) then 'Opportunity' when(64) then 'Destination' else 'Unknown' end)",
                oldClrType: typeof(string),
                oldUnicode: false,
                oldMaxLength: 14,
                oldComputedColumnSql: "(case [TransactionType] when(1) then 'Estimate' when(2) then 'Order' when(4) then 'Purchase Order' when(8) then 'Credit Memo' when(32) then 'Opportunity' when(64) then 'Destination' else 'Unknown' end)");
        }
    }
}
