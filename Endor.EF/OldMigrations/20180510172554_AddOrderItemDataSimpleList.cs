using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180510172554_AddOrderItemDataSimpleList")]
    public partial class AddOrderItemDataSimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSimpleListView(
                name: "Order.Item.Data.SimpleList",
                viewSql:
@"[BID]
, [ID]
, [ClassTypeID]
, [Name] as DisplayName
, CONVERT(BIT, 1) as [IsActive]
, CONVERT(BIT, 0) as [HasImage]
, CONVERT(BIT, 0) AS [IsDefault]",
                tableName: "Order.Item.Data");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropSimpleListView(
                name: "Order.Item.Data.SimpleList");
        }
    }
}

