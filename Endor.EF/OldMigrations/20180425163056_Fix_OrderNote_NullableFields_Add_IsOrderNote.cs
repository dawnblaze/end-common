using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180425163056_Fix_OrderNote_NullableFields_Add_IsOrderNote")]
    public partial class Fix_OrderNote_NullableFields_Add_IsOrderNote : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("UPDATE [Order.Note] SET Note = '' WHERE Note IS NULL");

            // Drop Indexes that use OrderItemID and indexes that use DestinationID
            migrationBuilder.DropIndex("IX_Order.Note_OrderItemID", "Order.Note");
            migrationBuilder.DropIndex("IX_Order.Note_DestinationID", "Order.Note");

            migrationBuilder.DropColumn("IsOrderItemNote", "Order.Note");
            migrationBuilder.DropColumn("IsDestinationNote", "Order.Note");

            migrationBuilder.AlterColumn<int>(
                name: "OrderItemID",
                table: "Order.Note",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "NoteParts",
                table: "Order.Note",
                type: "xml",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "Order.Note",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "xml",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DestinationID",
                table: "Order.Note",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "IsOrderNote",
                table: "Order.Note",
                nullable: false,
                computedColumnSql: "(isnull(case when [OrderItemID] IS NULL AND [DestinationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))");

            migrationBuilder.AddColumn<bool>(
                name: "IsOrderItemNote",
                table: "Order.Note",
                nullable: false,
                computedColumnSql: "(isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

            migrationBuilder.AddColumn<bool>(
                name: "IsDestinationNote",
                table: "Order.Note",
                nullable: false,
                computedColumnSql: "(isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

            // Re-Add dropped Indexes that use OrderItemID and indexes that use DestinationID
            migrationBuilder.CreateIndex("IX_Order.Note_OrderItemID", "Order.Note", new [] { "BID", "OrderItemID"  , "NoteType" });
            migrationBuilder.CreateIndex("IX_Order.Note_DestinationID", "Order.Note", new [] { "BID", "DestinationID", "NoteType" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // Drop Indexes that use OrderItemID and indexes that use DestinationID
            migrationBuilder.DropIndex("IX_Order.Note_OrderItemID", "Order.Note");
            migrationBuilder.DropIndex("IX_Order.Note_DestinationID", "Order.Note");

            migrationBuilder.DropColumn("IsOrderItemNote", "Order.Note");
            migrationBuilder.DropColumn("IsDestinationNote", "Order.Note");

            migrationBuilder.DropColumn(
                name: "IsOrderNote",
                table: "Order.Note");

            migrationBuilder.AlterColumn<int>(
                name: "OrderItemID",
                table: "Order.Note",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NoteParts",
                table: "Order.Note",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "xml",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Note",
                table: "Order.Note",
                type: "xml",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "DestinationID",
                table: "Order.Note",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsOrderItemNote",
                table: "Order.Note",
                nullable: false,
                computedColumnSql: "(isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

            migrationBuilder.AddColumn<bool>(
                name: "IsDestinationNote",
                table: "Order.Note",
                nullable: false,
                computedColumnSql: "(isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

            // Re-Add dropped Indexes that use OrderItemID and indexes that use DestinationID
            migrationBuilder.CreateIndex("IX_Order.Note_OrderItemID", "Order.Note", new[] { "BID", "OrderItemID", "NoteType" });
            migrationBuilder.CreateIndex("IX_Order.Note_DestinationID", "Order.Note", new[] { "BID", "DestinationID", "NoteType" });
        }
    }
}

