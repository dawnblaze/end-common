﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9484UpdateOrderItemSurcharge : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DisableSystemVersioningIfEnabled("Order.Item.Surcharge");

            migrationBuilder.Sql(@"
                DROP TABLE [Historic.Order.Item.Surcharge];
            ");
            migrationBuilder.DropColumn(
                name: "PriceNet",
                table: "Order.Item.Surcharge");

            migrationBuilder.DropColumn(
                name: "PriceUnitOV",
                table: "Order.Item.Surcharge");

            migrationBuilder.AddColumn<bool>(
                name: "PriceIsOV",
                table: "Order.Item.Surcharge",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "Price.PreTax",
                table: "Order.Item.Surcharge",
                type: "decimal(18,6)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price.Tax",
                table: "Order.Item.Surcharge",
                type: "decimal(18,6)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price.Taxable",
                table: "Order.Item.Surcharge",
                type: "decimal(18,6)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Price.Total",
                table: "Order.Item.Surcharge",
                type: "decimal(18,6)",
                nullable: true,
                computedColumnSql: "([Price.PreTax] + [Price.Tax])");

            migrationBuilder.EnableSystemVersioning("Order.Item.Surcharge", "Historic.Order.Item.Surcharge");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PriceIsOV",
                table: "Order.Item.Surcharge");

            migrationBuilder.DropColumn(
                name: "Price.PreTax",
                table: "Order.Item.Surcharge");

            migrationBuilder.DropColumn(
                name: "Price.Tax",
                table: "Order.Item.Surcharge");

            migrationBuilder.DropColumn(
                name: "Price.Taxable",
                table: "Order.Item.Surcharge");

            migrationBuilder.DropColumn(
                name: "Price.Total",
                table: "Order.Item.Surcharge");

            migrationBuilder.AddColumn<decimal>(
                name: "PriceNet",
                table: "Order.Item.Surcharge",
                type: "decimal(18,6)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "PriceUnitOV",
                table: "Order.Item.Surcharge",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
