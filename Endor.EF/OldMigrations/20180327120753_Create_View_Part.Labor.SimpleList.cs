using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180327120753_Create_View_Part.Labor.SimpleList")]
    public partial class Create_View_PartLaborSimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Labor.SimpleList]
                GO
                CREATE VIEW [dbo].[Part.Labor.SimpleList]
                    AS
                SELECT[BID]
                    , [ID]
                    , [ClassTypeID]
                    , [Name] as DisplayName
                    , [IsActive]
                    , [HasImage]
                    , CONVERT(BIT, 0) AS[IsDefault]
                FROM [dbo].[Part.Labor.Data];
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DROP VIEW IF EXISTS [dbo].[Part.Labor.SimpleList]
            ");
        }
    }
}

