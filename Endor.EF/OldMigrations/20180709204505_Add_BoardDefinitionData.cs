using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180709204505_Add_BoardDefinitionData")]
    public partial class Add_BoardDefinitionData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Board.Definition.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14000))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    IsSystem = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(MAX)", nullable: true),
                    DataType = table.Column<short>(type: "smallint", nullable: false),
                    LimitToRoles = table.Column<bool>(nullable: false),
                    IsAlwaysShown = table.Column<bool>(nullable: false),
                    ConditionFx = table.Column<string>(type: "varchar(MAX)", nullable: true),
                    ConditionSQL = table.Column<string>(type: "varchar(MAX)", nullable: true),
                    LastRecordCount = table.Column<int>(nullable: true),
                    LastRunDT = table.Column<DateTime>(type: "datetime", nullable: true),
                    LastRunDurationSec = table.Column<double>(type: "float", nullable: true),
                    CummCounterDT = table.Column<DateTime>(type: "datetime", nullable: true),
                    CummRunCount = table.Column<int>(nullable: true),
                    CummRunDurationSec = table.Column<double>(type: "float", nullable: true),
                    AverageRunDuration = table.Column<double>(type: "float", nullable: true, computedColumnSql: "(case when [CummRunDurationSec]=(0.0) then CONVERT([float],NULL) else [CummRunDurationSec]/[CummRunCount] end)")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Board.Definition.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Board.Definition_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Board.Definition_Name",
                table: "Board.Definition.Data",
                columns: new[] { "BID", "Name", "IsActive", "DataType" });

            migrationBuilder.Sql(
@"
DROP PROCEDURE IF EXISTS dbo.[Board.Definition.Action.PostResults];
GO
 
-- ========================================================
-- Name: [Board.Definition.Action.PostResults]( @BID tinyint, @BoardID smallint, @RecordCount int, @TimeInSec float )
--
-- Description: This function records the time for a board execution
-- and updates the cummulative timer on the board.
--  Sample Use:  
/*
    DECLARE @BID SMALLINT = 1
          , @BoardID INT = 10
          , @RecordCount INT = 5
          , @TimeInSec FLOAT = 3.2;
 
    EXEC dbo.[Board.Definition.Action.PostResults] @BID=@BID, @BoardID=@BoardID, @RecordCount=@RecordCount, @TimeInSec=@TimeInSec
    SELECT * FROM [Board.Definition] WHERE BID = @BID AND ID = @BoardID
*/
-- ========================================================
CREATE PROCEDURE [dbo].[Board.Definition.Action.PostResults]
--DECLARE
          @BID          TINYINT = 1
        , @BoardID      INT     = 10
        , @RecordCount  INT     = 7
        , @TimeInSec    FLOAT   = 1.24
AS
BEGIN
    -- Now update it
    UPDATE B
    SET   ModifiedDT = GETUTCDATE()
        , LastRunDT = GETUTCDATE()
        , LastRecordCount = @RecordCount
        , LastRunDurationSec = @TimeInSec
        , CummCounterDT = IsNull(CummCounterDT, GETUTCDATE())
        , CummRunCount += 1
        , CummRunDurationSec += @TimeInSec
    FROM [Board.Definition] B
    WHERE BID = @BID
      AND ID = @BoardID
    ;
 
    DECLARE @Rows INT = @@RowCount;
 
    -- Check if the Board specified is valid
    IF (@Rows = 0)
    BEGIN
        DECLARE @Message VARCHAR(1024) = CONCAT('Invalid Board Specified. BoardID=',@BoardID,' not found');
        THROW 50000, @Message, 1;
    END;
 
    RETURN @Rows;
END;
");

            migrationBuilder.Sql(
@"
DROP PROCEDURE IF EXISTS dbo.[Board.Definition.Action.ResetTimer];
 
GO
-- ========================================================
-- Name: [Board.Definition.Action.ResetTimer]( @BID tinyint = null, @BoardID smallint = null )
--
-- Description: This function resets the specified Board Cummulative Timers.
-- If no @BoardID is specified, all Boards for the given BID are reset.
-- If not @BID is specified, all BIDs are reset.
--
-- Sample Use:   EXEC dbo.[Board.Definition.Action.ResetTimer] @BID=1
-- Sample Use:   EXEC dbo.[Board.Definition.Action.ResetTimer] @BID=1, @BoardID=10
-- ========================================================
CREATE PROCEDURE [dbo].[Board.Definition.Action.ResetTimer]
--DECLARE
          @BID          TINYINT = 1
        , @BoardID      INT     = 10
 
AS
BEGIN
    -- Update it
    UPDATE B
    SET   ModifiedDT = GETUTCDATE()
        , CummCounterDT = GETUTCDATE()
        , CummRunCount = 0
        , CummRunDurationSec = 0
    FROM [Board.Definition] B
    WHERE BID = ISNULL(@BID, BID)
      AND ID = ISNULL(@BoardID, ID)
    ;
 
    DECLARE @Rows INT = @@RowCount;
 
    -- Check if the Board specified is valid
    IF (@Rows = 0)
    BEGIN
        DECLARE @Message VARCHAR(1024) = CONCAT('Invalid Board Specified. BoardID=',@BoardID,' not found');
        THROW 50000, @Message, 1;
    END;
 
    RETURN @Rows;
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DROP PROCEDURE IF EXISTS dbo.[Board.Definition.Action.PostResults];");
            migrationBuilder.Sql(@"DROP PROCEDURE IF EXISTS dbo.[Board.Definition.Action.ResetTimer];");

            migrationBuilder.DropTable(
                name: "Board.Definition.Data");
        }
    }
}

