﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_10096_AddStatusIDToContactData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "StatusID",
                table: "Contact.Data",
                nullable: false,
                defaultValueSql: "((1))");

            migrationBuilder.AddForeignKey(
                name: "FK_Contact.Data_enum.CRM.Company.Status",
                table: "Contact.Data",
                column: "StatusID",
                principalTable: "enum.CRM.Company.Status",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contact.Data_enum.CRM.Company.Status",
                table: "Contact.Data");

            migrationBuilder.DropColumn(
                name: "StatusID",
                table: "Contact.Data");
        }
    }
}
