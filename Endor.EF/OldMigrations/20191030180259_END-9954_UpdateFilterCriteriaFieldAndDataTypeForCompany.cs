﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9954_UpdateFilterCriteriaFieldAndDataTypeForCompany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [System.List.Filter.Criteria]
SET   [Field] = 'CompanyIDs'
    , [DataType] = 32005
    , [InputType] = 8
WHERE [TargetClassTypeID] = 3000 AND [Name] = 'Company'");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [System.List.Filter.Criteria]
SET   [Field] = 'Company'
    , [DataType] = 1
    , [InputType] = 0
WHERE [TargetClassTypeID] = 3000 AND [Name] = 'Company'");
        }
    }
}
