﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9461_Update_EmailConfiguration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Section]
SET [Name] = 'Email Configuration'
WHERE ID=2100;

UPDATE [System.Option.Category]
SET SectionID = 2100
WHERE [Name] = 'Email Handlers';
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Section]
SET [Name] = 'Configuration'
WHERE ID=2100;

UPDATE [System.Option.Category]
SET SectionID = 100
WHERE [Name] = 'Email Handlers';
            ");
        }

    }
}
