using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180809143130_FixSystemAutomationActionLinkColumnName")]
    public partial class FixSystemAutomationActionLinkColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ID",
                table: "[System.Automation.Action.DataTypeLink]",
                newName: "DataType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DataType",
                table: "[System.Automation.Action.DataTypeLink]",
                newName: "ID");
        }
    }
}

