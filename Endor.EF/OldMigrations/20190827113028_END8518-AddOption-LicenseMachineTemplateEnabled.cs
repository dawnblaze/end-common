using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8518AddOptionLicenseMachineTemplateEnabled : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.Option.Definition] 
                ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) 
                VALUES 
                (15102, 20027, 'License.MachineTemplate.Enabled', 'Enable Machine Templates for the Company to Edit', '', 3, '', 0, 0);
            ");
        }
        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
