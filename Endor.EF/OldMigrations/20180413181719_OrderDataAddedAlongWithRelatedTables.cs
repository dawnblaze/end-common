using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180413181719_OrderDataAddedAlongWithRelatedTables")]
    public partial class OrderDataAddedAlongWithRelatedTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Order.DestinationType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToEstimate = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToOrder = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToPO = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsDestinationLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsHeaderLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsItemLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    TransactionLevelSet = table.Column<byte>(type: "tinyint", nullable: false),
                    TransactionTypeSet = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Order.DestinationType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Order.NoteType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToEstimate = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToOrder = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToPO = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsDestinationLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsHeaderLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsItemLevel = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    TransactionLevelSet = table.Column<byte>(type: "tinyint", nullable: false),
                    TransactionTypeSet = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Order.NoteType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Order.KeyDateType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToEstimate = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToOrder = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    AppliesToPO = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when([TransactionTypeSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    HasTime = table.Column<bool>(type: "bit", nullable: false),
                    IsDestinationLevel = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (2)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsHeaderLevel = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (1)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsItemLevel = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when([TransactionLevelSet] & (4)) <> (0) then CONVERT([bit], (1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsUserInput = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    TransactionLevelSet = table.Column<byte>(type: "tinyint", nullable: false),
                    TransactionTypeSet = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Order.KeyDateType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Order.TransactionLevel",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Order.TransactionLevel", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Order.TransactionType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Order.TransactionType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Order.OrderStatus",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    CanHide = table.Column<bool>(nullable: false),
                    CanRename = table.Column<bool>(nullable: false),
                    EnableCustomItemStatus = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [FixedItemStatusID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,CONVERT([bit],(0))))"),
                    EnableCustomItemSubStatus = table.Column<bool>(nullable: false),
                    FixedItemStatusID = table.Column<byte>(type: "tinyint", nullable: true),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    TransactionType = table.Column<byte>(type: "tinyint", nullable: false),
                    TransactionTypeText = table.Column<string>(unicode: false, maxLength: 14, nullable: false, computedColumnSql: "(case [TransactionType] when(1) then 'Estimate' when(2) then 'Order' when(4) then 'Purchase Order' when(32) then 'Opportunity' when(64) then 'Destination' else 'Unknown' end)")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Order.OrderStatus", x => x.ID);
                    table.ForeignKey(
                        name: "FK_enum.Order.OrderStatus_enum.Order.OrderStatus_FixedItemStatusID",
                        column: x => x.FixedItemStatusID,
                        principalTable: "enum.Order.OrderStatus",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_enum.Order.OrderStatus_enum.Order.TransactionType_TransactionType",
                        column: x => x.TransactionType,
                        principalTable: "enum.Order.TransactionType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "10000"),
                    CompanyID = table.Column<int>(nullable: false),
                    CostLabor = table.Column<decimal>(name: "Cost.Labor", type: "DECIMAL(18,4)", nullable: true),
                    CostMachine = table.Column<decimal>(name: "Cost.Machine", type: "DECIMAL(18,4)", nullable: true),
                    CostMaterial = table.Column<decimal>(name: "Cost.Material", type: "DECIMAL(18,4)", nullable: true),
                    CostOther = table.Column<decimal>(name: "Cost.Other", type: "DECIMAL(18,4)", nullable: true),
                    CostTotal = table.Column<decimal>(name: "Cost.Total", type: "DECIMAL(18,4)", nullable: false, computedColumnSql: "(isnull(((isnull([Cost.Material],(0.0))+isnull([Cost.Labor],(0.0)))+isnull([Cost.Machine],(0.0)))+isnull([Cost.Other],(0.0)),(0.0)))"),
                    CreditMemoApplied = table.Column<decimal>(name: "CreditMemo.Applied", type: "DECIMAL(18,4)", nullable: true),
                    CreditMemoBalance = table.Column<decimal>(name: "CreditMemo.Balance", type: "DECIMAL(18,4)", nullable: true, computedColumnSql: "([CreditMemo.Credit]-coalesce([CreditMemo.Used],(0.0)))"),
                    CreditMemoCredit = table.Column<decimal>(name: "CreditMemo.Credit", type: "DECIMAL(18,4)", nullable: true),
                    CreditMemoHasBalance = table.Column<decimal>(name: "CreditMemo.HasBalance", nullable: true, computedColumnSql: "(isnull(CONVERT([bit],  abs(sign(coalesce( [CreditMemo.Credit], (0)) - coalesce([CreditMemo.Used], (0))))),(0)))"),
                    CreditMemoUsed = table.Column<decimal>(name: "CreditMemo.Used", type: "DECIMAL(18,4)", nullable: true),
                    Description = table.Column<string>(nullable: true),
                    DestinationType = table.Column<byte>(type: "tinyint", nullable: false),
                    FormattedNumber = table.Column<string>(type: "varchar(32)", unicode: false, maxLength: 32, nullable: false),
                    HasDocuments = table.Column<bool>(nullable: false),
                    HasOrderLinks = table.Column<bool>(nullable: false),
                    HasSingleDestination = table.Column<bool>(nullable: false),
                    InvoiceNumber = table.Column<int>(nullable: true),
                    LocationID = table.Column<byte>(nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(7)", nullable: false, computedColumnSql: "SYSUTCDATETIME()"),
                    Number = table.Column<int>(nullable: false),
                    OpportunityID = table.Column<int>(nullable: true),
                    OrderPONumber = table.Column<string>(nullable: true),
                    OrderStatusID = table.Column<byte>(type: "tinyint", nullable: false),
                    OrderStatusStartDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, computedColumnSql: "GETUTCDATE()"),
                    OriginID = table.Column<short>(nullable: true),
                    PaymentAuthorized = table.Column<decimal>(name: "Payment.Authorized", type: "DECIMAL(18,4)", nullable: true),
                    PaymentBalanceDue = table.Column<decimal>(name: "Payment.BalanceDue", type: "DECIMAL(18,4)", nullable: false, computedColumnSql: "(((((((([Price.ProductTotal] - isnull([Price.Discount], (0))) + isnull([Price.DestinationTotal], (0))) + isnull([Price.FinanceCharge], (0))) + [Price.Tax]) - isnull([Payment.Paid],(0))) - isnull([Payment.WriteOff],(0))) - isnull([CreditMemo.Applied], (0))) + isnull([CreditMemo.Credit],(0)))"),
                    PaymentPaid = table.Column<decimal>(name: "Payment.Paid", type: "DECIMAL(18,4)", nullable: false, defaultValue: 0m),
                    PaymentTotal = table.Column<decimal>(name: "Payment.Total", type: "DECIMAL(18,4)", nullable: false, computedColumnSql: "(isnull([Payment.Paid] + isnull([Payment.WriteOff],(0)) + IsNull([CreditMemo.Applied],0.0) ,(0.0)))"),
                    PaymentWriteOff = table.Column<decimal>(name: "Payment.WriteOff", type: "DECIMAL(18,4)", nullable: true),
                    PickupLocationID = table.Column<byte>(nullable: false),
                    PriceDestinationTotal = table.Column<decimal>(name: "Price.DestinationTotal", type: "DECIMAL(18,4)", nullable: true),
                    PriceDiscount = table.Column<decimal>(name: "Price.Discount", type: "DECIMAL(18,4)", nullable: true),
                    PriceDiscountPercent = table.Column<decimal>(name: "Price.DiscountPercent", type: "DECIMAL(9,4)", nullable: true),
                    PriceFinanceCharge = table.Column<decimal>(name: "Price.FinanceCharge", type: "DECIMAL(18,4)", nullable: true),
                    PriceIsLocked = table.Column<bool>(name: "Price.IsLocked", nullable: false),
                    PriceIsTaxExempt = table.Column<bool>(name: "Price.IsTaxExempt", nullable: false, computedColumnSql: "Case when [Price.TaxRate] = 0 then cast(1 as bit) else cast(0 as bit) end"),
                    PriceNet = table.Column<decimal>(name: "Price.Net", type: "DECIMAL(18,4)", nullable: true, computedColumnSql: "[Price.ProductTotal] - IsNull([Price.DestinationTotal],(0)) + IsNull([Price.FinanceCharge],(0))"),
                    PricePreTax = table.Column<decimal>(name: "Price.PreTax", type: "DECIMAL(18,4)", nullable: true, computedColumnSql: "[Price.ProductTotal] - IsNull([Price.DestinationTotal],(0)) + IsNull([Price.FinanceCharge],(0)) - IsNull([Price.Discount],0)"),
                    PriceProductTotal = table.Column<decimal>(name: "Price.ProductTotal", type: "DECIMAL(18,4)", nullable: true),
                    PriceTax = table.Column<decimal>(name: "Price.Tax", type: "DECIMAL(18,4)", nullable: true),
                    PriceTaxRate = table.Column<decimal>(name: "Price.TaxRate", type: "DECIMAL(18,4)", nullable: false),
                    PriceTaxable = table.Column<decimal>(name: "Price.Taxable", type: "DECIMAL(18,4)", nullable: true),
                    PriceTaxableOV = table.Column<bool>(name: "Price.TaxableOV", nullable: false),
                    PriceTotal = table.Column<decimal>(name: "Price.Total", type: "DECIMAL(18,4)", nullable: true, computedColumnSql: "[Price.ProductTotal] - IsNull([Price.DestinationTotal],(0)) + IsNull([Price.FinanceCharge],(0)) - IsNull([Price.Discount],0) + [Price.Tax]"),
                    ProductionLocationID = table.Column<byte>(nullable: false),
                    TaxGroupID = table.Column<short>(nullable: false),
                    TaxGroupOV = table.Column<bool>(nullable: false),
                    TransactionType = table.Column<byte>(type: "tinyint", nullable: false),
                    Version = table.Column<byte>(nullable: false),
                    VersionText = table.Column<string>(type: "varchar(2)", unicode: false, maxLength: 2, nullable: true, computedColumnSql: "(case when [Version]=(0) then '' when [Version]<=(26) then char((64)+[Version]) else char(CONVERT([int],([Version]-(1))/(26))+(64))+char(([Version]-(1))%(26)+(65)) end)")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Order.Data_DestinationType",
                        column: x => x.DestinationType,
                        principalTable: "enum.Order.DestinationType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Data_OrderStatus",
                        column: x => x.OrderStatusID,
                        principalTable: "enum.Order.OrderStatus",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Data_CompanyID",
                        columns: x => new { x.BID, x.CompanyID },
                        principalTable: "Company.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Data_LocationID",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Data_OpportunityID",
                        columns: x => new { x.BID, x.OpportunityID },
                        principalTable: "Opportunity.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Data_OriginID",
                        columns: x => new { x.BID, x.OriginID },
                        principalTable: "CRM.Origin",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Data_PickupLocationID",
                        columns: x => new { x.BID, x.PickupLocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Data_ProdLocationID",
                        columns: x => new { x.BID, x.ProductionLocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Data_TaxGroupID",
                        columns: x => new { x.BID, x.TaxGroupID },
                        principalTable: "Accounting.Tax.Group",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order.KeyDate",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "10011"),
                    DestinationID = table.Column<int>(type: "int", nullable: true),
                    IsDestinationDate = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    IsFirmDate = table.Column<bool>(type: "bit", nullable: false),
                    IsOrderItemDate = table.Column<bool>(type: "bit", nullable: false, computedColumnSql: "(isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    KeyDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false),
                    KeyDate = table.Column<DateTime>(type: "date", nullable: true, computedColumnSql: "(CONVERT([date],[KeyDT]))"),
                    KeyDateType = table.Column<byte>(type: "tinyint", nullable: false),
                    KeyTime = table.Column<TimeSpan>(type: "time", nullable: true, computedColumnSql: "(CONVERT([time],[KeyDT]))"),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(7)", nullable: false, computedColumnSql: "SYSUTCDATETIME()"),
                    OrderID = table.Column<int>(type: "int", nullable: false),
                    OrderItemID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.KeyDate", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Order.KeyDate_DateType",
                        column: x => x.KeyDateType,
                        principalTable: "enum.Order.KeyDateType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.KeyDate_OrderID",
                        columns: x => new { x.BID, x.OrderID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order.Note",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "10015"),
                    DestinationID = table.Column<int>(nullable: false),
                    IsDestinationNote = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [DestinationID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    IsOrderItemNote = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [OrderItemID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))"),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(7)", nullable: false, computedColumnSql: "SYSUTCDATETIME()"),
                    Note = table.Column<string>(unicode: false, nullable: true),
                    NoteCount = table.Column<short>(nullable: false),
                    NoteParts = table.Column<string>(type: "xml", nullable: true),
                    NoteType = table.Column<byte>(nullable: false),
                    OrderDataBID = table.Column<short>(nullable: true),
                    OrderDataID = table.Column<int>(nullable: true),
                    OrderID = table.Column<int>(nullable: false),
                    OrderItemID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order.Note", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Order.Note_NoteType",
                        column: x => x.NoteType,
                        principalTable: "enum.Order.NoteType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Note_OrderID",
                        columns: x => new { x.BID, x.OrderID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Order.Note_Order.Data_OrderDataBID_OrderDataID",
                        columns: x => new { x.OrderDataBID, x.OrderDataID },
                        principalTable: "Order.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_enum.Order.OrderStatus_FixedItemStatusID",
                table: "enum.Order.OrderStatus",
                column: "FixedItemStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_enum.Order.OrderStatus_TransactionType",
                table: "enum.Order.OrderStatus",
                column: "TransactionType");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_DestinationType",
                table: "Order.Data",
                column: "DestinationType");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_OrderStatusID0",
                table: "Order.Data",
                column: "OrderStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_CompanyID",
                table: "Order.Data",
                columns: new[] { "BID", "CompanyID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_InvoiceNumber",
                table: "Order.Data",
                columns: new[] { "BID", "InvoiceNumber" },
                filter: "InvoiceNumber IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_OpportunityID",
                table: "Order.Data",
                columns: new[] { "BID", "OpportunityID" },
                filter: "OpportunityID IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_OrderStatusID",
                table: "Order.Data",
                columns: new[] { "BID", "OrderStatusID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_BID_OriginID",
                table: "Order.Data",
                columns: new[] { "BID", "OriginID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_BID_PickupLocationID",
                table: "Order.Data",
                columns: new[] { "BID", "PickupLocationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_BID_TaxGroupID",
                table: "Order.Data",
                columns: new[] { "BID", "TaxGroupID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_Number",
                table: "Order.Data",
                columns: new[] { "BID", "Number", "TransactionType" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_LocationID",
                table: "Order.Data",
                columns: new[] { "BID", "LocationID", "OrderStatusID", "Number" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Data_ProdLocationID",
                table: "Order.Data",
                columns: new[] { "BID", "ProductionLocationID", "OrderStatusID", "Number" },
                filter: "OrderStatusID < 6");

            migrationBuilder.CreateIndex(
                name: "IX_Order.KeyDate_KeyDateType",
                table: "Order.KeyDate",
                column: "KeyDateType");

            migrationBuilder.CreateIndex(
                name: "IX_Order.KeyDate_KeyDateID",
                table: "Order.KeyDate",
                columns: new[] { "BID", "KeyDateType", "KeyDT", "OrderID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.KeyDate_OrderID",
                table: "Order.KeyDate",
                columns: new[] { "BID", "OrderID", "KeyDateType", "KeyDT" },
                filter: "OrderItemID IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.KeyDate_DestinationID",
                table: "Order.KeyDate",
                columns: new[] { "BID", "OrderItemID", "KeyDateType", "KeyDT" },
                filter: "OrderItemID IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Note_DestinationID",
                table: "Order.Note",
                columns: new[] { "BID", "DestinationID", "NoteType" },
                filter: "DestinationID IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Note_OrderID",
                table: "Order.Note",
                columns: new[] { "BID", "OrderID", "NoteType" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Note_OrderItemID",
                table: "Order.Note",
                columns: new[] { "BID", "OrderItemID", "NoteType" },
                filter:"OrderItemID IS NOT NULL");


            migrationBuilder.Sql(@"
CREATE VIEW [Order.KeyDate.SimpleList]
               as
SELECT [Order.KeyDate].[BID]
, [Order.KeyDate].[ID]
, [Order.KeyDate].[ClassTypeID]
, OKDT.[Name] as DisplayName
, CONVERT(BIT, 1) as [IsActive]
, CONVERT(BIT, 0) as [HasImage]
, CONVERT(BIT, 0) AS [IsDefault]
from [Order.KeyDate] 
Left join [enum.Order.KeyDateType] OKDT on OKDT.ID = [Order.KeyDate].KeyDateType");

            migrationBuilder.Sql(@"
CREATE VIEW [Order.Note.SimpleList] 
               as
SELECT [Order.Note].[BID]
, [Order.Note].[ID]
, [Order.Note].[ClassTypeID]
, ONT.[Name] as DisplayName
, CONVERT(BIT, 1) as [IsActive]
, CONVERT(BIT, 0) as [HasImage]
, CONVERT(BIT, 0) AS [IsDefault]
from [Order.Note] 
Left join [enum.Order.NoteType] ONT on ONT.ID = [Order.Note].NoteType");

            migrationBuilder.CreateSimpleListView(
                name: "Order.SimpleList",
                viewSql:
@"[BID]
, [ID]
, [ClassTypeID]
, [FormattedNumber] as DisplayName
, CONVERT(BIT, 1) as [IsActive]
, CONVERT(BIT, 0) as [HasImage]
, CONVERT(BIT, 0) AS [IsDefault]",
                tableName: "Order.Data");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropSimpleListView(
                name: "Order.KeyDate.SimpleList");

            migrationBuilder.DropSimpleListView(
                name: "Order.Note.SimpleList");

            migrationBuilder.DropSimpleListView(
                name: "Order.SimpleList");

            migrationBuilder.DropTable(
                name: "enum.Order.TransactionLevel");

            migrationBuilder.DropTable(
                name: "Order.KeyDate");

            migrationBuilder.DropTable(
                name: "Order.Note");


            migrationBuilder.DropTable(
                name: "enum.Order.OrderStatus");

            migrationBuilder.DropTable(
                name: "enum.Order.NoteType");

            migrationBuilder.DropTable(
                name: "Order.Data");

            migrationBuilder.DropTable(
                name: "enum.Order.DestinationType");

            migrationBuilder.DropTable(
                name: "enum.Order.KeyDateType");

            migrationBuilder.DropTable(
                name: "enum.Order.TransactionType");
        }
    }
}

