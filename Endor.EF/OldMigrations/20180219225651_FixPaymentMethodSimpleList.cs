using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180219225651_FixPaymentMethodSimpleList")]
    public partial class FixPaymentMethodSimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF EXISTS(select 1 from sys.views where name='Accounting.Payment.Method.SimpleList' and type='v')
DROP VIEW [dbo].[Accounting.Payment.Method.SimpleList];
GO

CREATE VIEW[dbo].[Accounting.Payment.Method.SimpleList]
    AS
SELECT[BID]
    , [ID]
    , [ClassTypeID]
    , [Name] as DisplayName
        , [IsActive]
        , CONVERT(BIT, 0) AS[HasImage]
        , CONVERT(BIT, 0) AS[IsDefault]
FROM[Accounting.Payment.Method];");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}

