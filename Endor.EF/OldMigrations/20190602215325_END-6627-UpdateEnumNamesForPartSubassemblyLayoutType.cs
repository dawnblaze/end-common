using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END6627UpdateEnumNamesForPartSubassemblyLayoutType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            /* END-6627 Update Enum Names for [enum.Part.Subassembly.LayoutType] */
            migrationBuilder.Sql(@"
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Assembly Basic' WHERE ID = 1;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Assembly Advanced' WHERE ID = 2;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Assembly Ecommerce' WHERE ID = 3;

                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Order Entry Basic' WHERE ID = 11;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Order Entry Advanced' WHERE ID = 12;
                UPDATE [enum.Part.Subassembly.LayoutType] SET Name = 'Machine Setup' WHERE ID = 13;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
