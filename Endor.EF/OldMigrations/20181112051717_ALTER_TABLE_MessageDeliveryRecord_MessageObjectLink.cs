using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class ALTER_TABLE_MessageDeliveryRecord_MessageObjectLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Message.Delivery.Record
            migrationBuilder.DropForeignKey(
                name: "FK_Message.Delivery.Record_Message.Participant",
                table: "Message.Delivery.Record");

            migrationBuilder.DropIndex(
                name: "IX_Message.Delivery.Record_Pending",
                table: "Message.Delivery.Record");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Delivery.Record",
                table: "Message.Delivery.Record");

            migrationBuilder.DropTable(
                name: "Message.Delivery.Record");

            migrationBuilder.CreateTable(
               name: "Message.Delivery.Record",
               columns: table => new
               {
                   BID = table.Column<short>(nullable: false),
                   ID = table.Column<short>(nullable: false),
                   ParticipantID = table.Column<byte>(type: "int", nullable: false, defaultValue: (byte)0),
                   AttempNumber = table.Column<byte>(type: "smallint", nullable: false, defaultValue: (byte)0),
                   ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((14114))"),
                   IsPending = table.Column<int>(nullable: false, computedColumnSql: "(case when [ScheduledDT] IS NULL OR AttemptDT IS NOT NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end)"),
                   ScheduledDT = table.Column<DateTime>(type: "datetime2(2) sparse", nullable: true),
                   AttemptDT = table.Column<DateTime>(type: "datetime2(2)", nullable: true),
                   WasSuccessful = table.Column<bool>(nullable: false),
                   FailureMessage = table.Column<string>(type: "nvarchar(max) sparse", nullable: true),
                   MetaData = table.Column<string>(type: "xml sparse", nullable: true),
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_Message.Delivery.Record", x => new { x.BID, x.ParticipantID, x.AttempNumber });
               }
            );

            migrationBuilder.AddForeignKey(
                name: "FK_Message.Delivery.Record_Message.Participant",
                table: "Message.Delivery.Record",
                columns: new[] { "BID", "ParticipantID" },
                principalTable: "Message.Participant.Info",
                principalColumns: new[] { "BID", "ID" });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Delivery.Record_Pending",
                table: "Message.Delivery.Record",
                columns: new[] { "BID", "IsPending", "ScheduledDT" });


            //Message.Object.Link
            migrationBuilder.DropForeignKey(
                name: "FK_Message.Related.Records_Message.Data",
                table: "Message.Object.Link");

            migrationBuilder.DropIndex(
                name: "IX_Message.Object.Link_LinkedObject",
                table: "Message.Object.Link");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Object.Link",
                table: "Message.Object.Link");

            migrationBuilder.DropTable(
                name: "Message.Object.Link");

            migrationBuilder.CreateTable(
                name: "Message.Object.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    BodyID = table.Column<int>(type: "int", nullable: false),
                    LinkedObjectClassTypeID = table.Column<int>(type: "int", nullable: false),
                    LinkedObjectID = table.Column<int>(type: "int", nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false),
                    DisplayText = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    ID = table.Column<short>(nullable: false),
                    SourceMessage = table.Column<bool>(type: "bit", nullable: false, defaultValue: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Message.Object.Link", x => new { x.BID, x.BodyID, x.LinkedObjectClassTypeID, x.LinkedObjectID });
                    table.ForeignKey(
                        name: "FK_Message.Related.Records_Message.Data",
                        columns: x => new { x.BID, x.BodyID },
                        principalTable: "Message.Body",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Message.Object.Link_LinkedObject",
                table: "Message.Object.Link",
                columns: new[] { "BID", "LinkedObjectClassTypeID", "LinkedObjectID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            //Message.Delivery.Record
            migrationBuilder.DropForeignKey(
                name: "FK_Message.Delivery.Record_Message.Participant",
                table: "Message.Delivery.Record");

            migrationBuilder.DropIndex(
                name: "IX_Message.Delivery.Record_Pending",
                table: "Message.Delivery.Record");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Delivery.Record",
                table: "Message.Delivery.Record");

            migrationBuilder.DropTable(
                name: "Message.Delivery.Record");


            //Message.Object.Link
            migrationBuilder.DropForeignKey(
                name: "FK_Message.Related.Records_Message.Data",
                table: "Message.Object.Link");

            migrationBuilder.DropIndex(
                name: "IX_Message.Object.Link_LinkedObject",
                table: "Message.Object.Link");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Message.Object.Link",
                table: "Message.Object.Link");

            migrationBuilder.DropTable(
                name: "Message.Object.Link");

            migrationBuilder.DropTable(
                name: "Message.Object.Link");
        }
    }
}
