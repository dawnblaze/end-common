using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddSubassemblyLayout : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Subassembly.Layout",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12043"),
                    LayoutType = table.Column<byte>(type: "tinyint", nullable: false),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(type: "varchar(255)", maxLength: 255, nullable: false),
                    SubassemblyID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Subassembly.Layout", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.Layout_enum.Part.Subassembly.LayoutType",
                        column: x => x.LayoutType,
                        principalTable: "enum.Part.Subassembly.LayoutType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Subassembly.Layout_Part.Subassembly.Data",
                        columns: x => new { x.BID, x.SubassemblyID },
                        principalTable: "Part.Subassembly.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Element_BID_LayoutID",
                table: "Part.Subassembly.Element",
                columns: new[] { "BID", "LayoutID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Layout_LayoutType",
                table: "Part.Subassembly.Layout",
                column: "LayoutType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Subassembly.Layout_Subassembly",
                table: "Part.Subassembly.Layout",
                columns: new[] { "BID", "SubassemblyID", "LayoutType" });

            migrationBuilder.AddForeignKey(
                name: "FK_Subassembly.Element.Data_Subassembly.Layout",
                table: "Part.Subassembly.Element",
                columns: new[] { "BID", "LayoutID" },
                principalTable: "Part.Subassembly.Layout",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subassembly.Element.Data_Subassembly.Layout",
                table: "Part.Subassembly.Element");

            migrationBuilder.DropTable(
                name: "Part.Subassembly.Layout");

            migrationBuilder.DropIndex(
                name: "IX_Part.Subassembly.Element_BID_LayoutID",
                table: "Part.Subassembly.Element");
        }
    }
}
