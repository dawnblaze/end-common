using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class TaxabilityCodesFiltersAndCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                        (
                            @"
DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=8005;
INSERT INTO [dbo].[List.Filter]
           ([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
     VALUES
           (1 --<BID, smallint,>
           ,(SELECT MAX(ID)+1 FROM dbo.[List.Filter]) --<ID, int,>
           ,GETUTCDATE() --<CreatedDate, date,>
           ,GETUTCDATE() --<ModifiedDT, datetime2(2),>
           ,1 --<IsActive, bit,>
           ,'All' --<Name, varchar(255),>
           ,8005 --<TargetClassTypeID, int,>
           ,NULL --<IDs, xml,>
           ,NULL --<Criteria, xml,>
           ,NULL --<OwnerID, smallint,>
           ,1 --<IsPublic, bit,>
           ,1 --<IsSystem, bit,>
           ,NULL --<Hint, varchar(max),>
           ,1 --<IsDefault, bit,>
           ,0);--<SortIndex, tinyint,>
"
            );

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (8005 --<TargetClassTypeID, int,>
                        ,'Name' --<Name, varchar(255),>
                        ,'Name' --<Label, varchar(255),>
                        ,'Name' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,1 --<DataType, tinyint,>
                        ,0 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,1 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");

            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (8005 --<TargetClassTypeID, int,>
                        ,'Include Inactive' --<Name, varchar(255),>
                        ,'Include Inactive' --<Label, varchar(255),>
                        ,'-IsActive' --<Field, varchar(255),>
                        ,0 --<IsHidden, bit,>
                        ,3 --<DataType, tinyint,>
                        ,13 --<InputType, tinyint,>
                        ,0 --<AllowMultiple, bit,>
                        ,NULL --<ListValues, varchar(max),>
                        ,NULL --<ListValuesEndpoint, varchar(255),>
                        ,0 --<IsLimitToList, bit,>
                        ,2 --<SortIndex, tinyint,>
                        ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
                        (
                            @"
DELETE FROM [dbo].[List.Filter] WHERE [TargetClassTypeID]=8005;
"
            );

            migrationBuilder.Sql(@"
DELETE FROM [System.List.Filter.Criteria] WHERE [TargetClassTypeID] = 8005
");
        }
    }
}
