﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddTaxBusinessKeyOptionDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.Option.Definition] (
        [CategoryID],
        [ID],
        [Name],
        [Label],
        [Description],
        [DataType],
        [ListValues],
        [DefaultValue],
        [IsHidden]
) 
VALUES (
        703, 
        7030, 
        'Integration.Tax.BusinessKey', 
        'Business Key for Atomology Tax Engine (ATE)', 
        'Business Key for Atomology Tax Engine (ATE)', 
        0, 
        '', 
        0, 
        1);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [dbo].[System.Option.Definition] WHERE ID = 7030");
        }
    }
}
