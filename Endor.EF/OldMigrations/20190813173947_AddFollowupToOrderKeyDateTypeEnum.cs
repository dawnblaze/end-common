using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddFollowupToOrderKeyDateTypeEnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT [dbo].[enum.Order.KeyDateType] ([ID], [Name], [HasTime], [IsUserInput], [TransactionTypeSet], [TransactionLevelSet]) 
VALUES (31, 'Followup', 1, 0, 1, 1);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[enum.Order.KeyDateType] WHERE [Name] = 'Followup' AND [TransactionTypeSet] = 1 ;
            ");

        }
    }
}
