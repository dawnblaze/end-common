using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class GLDataColumnUpdates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.RenameColumn(
            //    name: "ReconciliaitonID",
            //    table: "Accounting.GL.Data",
            //    newName: "ReconciliationID");
            migrationBuilder.Sql(
    "EXEC sp_rename N'[dbo].[Accounting.GL.Data].[ReconciliaitonID]', N'ReconciliationID', N'COLUMN';");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
    "EXEC sp_rename N'[dbo].[Accounting.GL.Data].[ReconciliationID]', N'ReconciliaitonID', N'COLUMN';");
            //migrationBuilder.RenameColumn(
            //    name: "ReconciliationID",
            //    table: "Accounting.GL.Data",
            //    newName: "ReconciliaitonID");
        }
    }
}
