using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180718040521_END1847_EmployeeAndContactFilterFixes")]
    public partial class END1847_EmployeeAndContactFilterFixes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [System.List.Filter.Criteria]
                Set Field = 'LongName'
                WHERE TargetClassTypeID = 5000 AND [Name] = 'Name'

                --Fixes Contact Status Filter
                UPDATE [System.List.Filter.Criteria]
                Set Field = 'StatusID'
                WHERE TargetClassTypeID = 3000 AND [Name] = 'Status'
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

