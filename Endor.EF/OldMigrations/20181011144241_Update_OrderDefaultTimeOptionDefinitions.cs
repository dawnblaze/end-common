using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181011144241_Update_OrderDefaultTimeOptionDefinitions")]
    public partial class Update_OrderDefaultTimeOptionDefinitions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            UPDATE [System.Option.Definition]
               SET [Label] = 'Default Time for Design Due'
               WHERE ID = 6042
            GO
            UPDATE [System.Option.Definition]
               SET [Label] = 'Default Time for Order Due'
               WHERE ID = 6043
            GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

