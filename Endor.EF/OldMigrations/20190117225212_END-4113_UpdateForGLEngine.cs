using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END4113_UpdateForGLEngine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "IncomeAllocationType",
                table: "Order.Item.Component",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "RollUpLinkedIncome",
                table: "Order.Item.Component",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Component_IncomeAllocationType",
                table: "Order.Item.Component",
                column: "IncomeAllocationType");

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Component_enum.Part.Subassembly.IncomeAllocationType",
                table: "Order.Item.Component",
                column: "IncomeAllocationType",
                principalTable: "enum.Part.Subassembly.IncomeAllocationType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.Sql(@"
  Update [Accounting.GL.Account] set [Name] = 'Undeposited Funds' where ID = 1100
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Item.Component_enum.Part.Subassembly.IncomeAllocationType",
                table: "Order.Item.Component");

            migrationBuilder.DropIndex(
                name: "IX_Order.Item.Component_IncomeAllocationType",
                table: "Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "IncomeAllocationType",
                table: "Order.Item.Component");

            migrationBuilder.DropColumn(
                name: "RollUpLinkedIncome",
                table: "Order.Item.Component");

            migrationBuilder.Sql(@"
  Update [Accounting.GL.Account] set [Name] = 'Bank Account' where ID = 1100
            ");
        }
    }
}
