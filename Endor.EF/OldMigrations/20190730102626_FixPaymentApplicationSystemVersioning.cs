using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class FixPaymentApplicationSystemVersioning : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [dbo].[Accounting.Payment.Application] SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [dbo].[Historic.Accounting.Payment.Application]))
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [dbo].[Accounting.Payment.Application] SET (SYSTEM_VERSIONING = OFF);");
        }
    }
}
