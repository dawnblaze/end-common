using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180627162445_UpdateCustomerFieldInputTypeEnums")]
    public partial class UpdateCustomerFieldInputTypeEnums : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[CustomField.Definition];
DELETE FROM [dbo].[enum.CustomField.InputType];

INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (11,'Single Line String',1);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (12,'Multi-Line String',1);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (13,'Drop-Down',1);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (14,'Radio Group',1);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (21,'Number',2);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (31,'Checkbox',3);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (32,'Toggle',3);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (91,'DateTime Picker',9);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[CustomField.Definition];
DELETE FROM [dbo].[enum.CustomField.InputType];

INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (1,'Unformatted Single Line',0);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (2,'Unformatted Memo',0);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (3,'Formatted Single Line',0);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (4,'Formatted Memo',0);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (11,'Integer',1);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (21,'Number',2);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (31,'Checkbox',3);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (32,'Toggle',3);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (39,'Custom Boolean Option',3);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (71,'Date Picker',7);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (81,'Time Picker',8);
INSERT INTO [dbo].[enum.CustomField.InputType] ([ID],[Name],[DataType]) VALUES (91,'DateTime Picker',9);
");
        }
    }
}

