using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class CorrectingMissingColumnActiveProfileCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
            
            migrationBuilder.Sql(@"
if not exists(select * from[INFORMATION_SCHEMA].columns where[TABLE_NAME] = 'Part.Machine.Data' and COLUMN_NAME = 'ActiveProfileCount')
begin
ALTER TABLE dbo.[Part.Machine.Data]
        ADD
    ActiveProfileCount int NOT NULL CONSTRAINT[DF_Part.Machine.Data_ActiveProfileCount] DEFAULT 0
end ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
