using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180824194230_END-2313_Remove_SetActive_SPROCs_No_Longer_Used")]
    public partial class END2313_Remove_SetActive_SPROCs_No_Longer_Used : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Company.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Company.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Contact.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Industry.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Industry.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Location.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Location.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Location.Action.SetActiveMultiple')
                    BEGIN
                        DROP PROCEDURE [Location.Action.SetActiveMultiple]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Origin.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.GL.Account.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Accounting.GL.Account.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.Payment.Method.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Accounting.Payment.Method.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.Payment.Term.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Accounting.Payment.Term.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.Tax.Group.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Accounting.Tax.Group.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Accounting.Tax.Item.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Accounting.Tax.Item.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Labor.Data.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Part.Labor.Data.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Labor.Category.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Part.Labor.Category.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Machine.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Part.Machine.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Machine.Category.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Part.Machine.Category.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Material.Category.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Part.Material.Category.Action.SetActive]
                    END

                    IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Part.Material.Data.Action.SetActive')
                    BEGIN
                        DROP PROCEDURE [Part.Material.Data.Action.SetActive]
                    END
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Company.Action.SetActive]    Script Date: 8/24/2018 2:45:16 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Company.Action.SetActive]
                    --
                    -- Description: This procedure sets the referenced Company's active status
                    --
                    -- Sample Use:   EXEC dbo.[Company.Action.SetActive] @BID=1, @CompanyID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Company.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @CompanyID      INT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the Company specified is valid
                        IF NOT EXISTS(SELECT * FROM [Company.Data] WHERE BID = @BID and ID = @CompanyID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid Company Specified. CompanyID='+CONVERT(VARCHAR(12),@CompanyID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Update it
                        UPDATE C
                        SET   
                            IsActive = @IsActive
                          , ModifiedDT = GetUTCDate()
                        FROM [Company.Data] C
                        WHERE BID = @BID and ID = @CompanyID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Contact.Action.SetActive]    Script Date: 8/24/2018 2:45:58 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Contact.Action.SetActive]
                    --
                    -- Description: This procedure sets the referenced Contact's active status
                    --
                    -- Sample Use:   EXEC dbo.[Contact.Action.SetActive] @BID=1, @ContactID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Contact.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @ContactID      INT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the Contact specified is valid
                        IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID and ID = @ContactID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR(12),@ContactID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        DECLARE @CompanyID INT, @IsCompanyAdHoc BIT;
                        SET @CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID=@ContactID);
                        SET @IsCompanyAdHoc = (SELECT IsAdHoc FROM [Company.Data] WHERE ID=@CompanyID);

                        -- Check that some other Contact is Active if Company is NOT AdHoc
                        IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID
                            AND ID != @ContactID AND IsActive = 1 AND CompanyID != (SELECT CompanyID FROM [Contact.Data] WHERE ID = @ContactID))
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Attempting to set the only Active Contact to Inactive. Set another Contact to active first before setting this one to Inactive.'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Check that this is not the default Contact if Company is NOT AdHoc
                        IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND EXISTS(SELECT * FROM [Contact.Data] WHERE BID=@BID AND ID=@ContactID AND IsDefault=1)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Attempting to set the Default Contact to Inactive. Set another Contact as the Default first before setting this one to Inactive.'
                                 ;
                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Check that this is not the billing Contact if Company is NOT AdHoc
                        IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND EXISTS(SELECT * FROM [Contact.Data] WHERE BID=@BID AND ID=@ContactID AND IsBilling=1)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Attempting to set the Billing Contact to Inactive. Set another Contact as the Billing first before setting this one to Inactive.'
                                 ;
                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE C
                        SET   
                              IsActive = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Contact.Data] C
                        WHERE BID = @BID and ID = @ContactID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        -- If associated company is AdHoc, update it as well
                        IF (@IsCompanyAdHoc = 1)
                        BEGIN
                            UPDATE Co
                            SET
                                  IsActive = @IsActive
                                , ModifiedDT = GetUTCDate()
                            FROM [Company.Data] Co
                            WHERE BID = @BID AND ID = @CompanyID
                                AND COALESCE(IsActive,~1) != 1
                        END

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Industry.Action.SetActive]    Script Date: 8/24/2018 2:46:26 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Industry.Action.SetActive]
                    --
                    -- Description: This procedure sets the reference Industry
                    --
                    -- Sample Use:   EXEC dbo.[Industry.Action.SetActive] @BID=1, @IndustryID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Industry.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @IndustryID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the industry specified is valid
                        IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @IndustryID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid Industry Specified. IndustryID='+CONVERT(VARCHAR(12),@IndustryID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;
	                    
                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [CRM.Industry] L
                        WHERE BID = @BID and ID = @IndustryID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Location.Action.SetActive]    Script Date: 8/24/2018 2:47:13 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Location.Action.SetActive]
                    --
                    -- Description: This procedure sets the reference location
                    --
                    -- Sample Use:   EXEC dbo.[Location.Action.SetActive] @BID=1, @LocationID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Location.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @LocationID     INT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the location specified is valid
                        IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Check that some other Location is Active
                        IF (@IsActive = 0) AND NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID != @LocationID and IsActive=1)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Attempting to Set The Only Active Location Inactive. Set another Location active first before setting this one inactive.'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Check that this is not the default location
                        IF (@IsActive = 0) AND EXISTS(SELECT * FROM [Location.Data] WHERE BID=@BID and ID=@LocationID and IsDefault=1)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Attempting to Set The Default Location Inactive. Set another Location as the Default first before setting this one Inactive.'
                                 ;
                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Location.Data] L
                        WHERE BID = @BID and ID = @LocationID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Location.Action.SetActiveMultiple]    Script Date: 8/24/2018 2:47:40 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Location.Action.SetActiveMultiple]
                    --
                    -- Description: This procedure sets the reference location
                    --
                    -- Sample Use:   EXEC dbo.[Location.Action.SetActiveMultiple] @BID=1, @LocationIDs='1,2', @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Location.Action.SetActiveMultiple]
                    -- DECLARE 
                              @BID            TINYINT		-- = 1
                            , @LocationIDs    VARCHAR(1024) -- = '1,2'
                            , @IsActive       BIT			-- = 1
                            , @Result         INT     = NULL	OUTPUT
                    AS
                    BEGIN
	                    DECLARE @Message VARCHAR(MAX) = '';

	                    DECLARE @TempTable TABLE (
			                      BID SMALLINT NOT NULL
			                    , LocationId SMALLINT  NULL
			                    , IsActive BIT  NULL
			                    , IsSuccess BIT NOT NULL
			                    , ErrorMessage VARCHAR(1024)
			                    );

	                    INSERT INTO @TempTable( BID, LocationID, IsActive, IsSuccess, ErrorMessage)
		                    SELECT @BID
		                         , LData.ID
			                     , LData.IsActive
			                     , (CASE WHEN LData.ID IS NULL THEN 0 
			                             WHEN @IsActive=0 AND IsDefault=1 THEN 0 
					                     ELSE  1 END )
			                     , (CASE WHEN LData.ID IS NULL THEN 'Location Not Found; ' 
			                             WHEN @IsActive=0 AND IsDefault=1 THEN 'Attempting to set the Default Location to Inactive. Set another Location as the Default first before setting this one to Inactive.; '
			                             ELSE '' END )
		                    FROM string_split(@LocationIDs,',') LInput 
		                    LEFT JOIN [Location.Data] LData ON LData.BID = @BID AND LData.ID = CONVERT(INT, LInput.[value])
	                    ;

	                    -- Check that we don't have any errors
	                    SELECT @Message += ErrorMessage
	                    FROM @TempTable
	                    WHERE IsSuccess = 0
	                    ;

 	                    -- Check that some other Location is Active
	                    IF (@IsActive = 0) 
		                    AND NOT EXISTS( SELECT * 
						                    FROM [Location.Data] LData 
						                    WHERE BID = @BID AND IsActive=1 and ID NOT IN (SELECT LocationID FROM @TempTable) )
		                    SET @Message = @Message + ' Attempting to set the only Active Location(s) to Inactive. Set another Location to active first before setting this one to Inactive.';


	                    IF LEN(@Message) > 1 
		                    THROW 50000, @Message, 1
	                    ELSE
		                    -- Now update it
		                    UPDATE L
		                    SET   IsActive   = @IsActive
			                    , ModifiedDT = GetUTCDate()
		                    FROM [Location.Data] L
		                    JOIN @TempTable T on L.BID = @BID AND L.ID = T.LocationId
		                    WHERE COALESCE(L.IsActive,~@IsActive) != @IsActive

	                    SET @Result = @@ROWCOUNT;

	                    SELECT @Result AS Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Origin.Action.SetActive]    Script Date: 8/24/2018 2:48:11 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Origin.Action.SetActive]
                    --
                    -- Description: This procedure sets the referenced Origin's active status
                    --
                    -- Sample Use:   EXEC dbo.[Origin.Action.SetActive] @BID=1, @OriginID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Origin.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @OriginID       INT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the Origin specified is valid
                        IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @OriginID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid Origin Specified. OriginID='+CONVERT(VARCHAR(12),@OriginID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Update it
                        UPDATE O
                        SET   
                            IsActive = @IsActive
                          , ModifiedDT = GetUTCDate()
                        FROM [CRM.Origin] O
                        WHERE BID = @BID and ID = @OriginID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Accounting.GL.Account.Action.SetActive]    Script Date: 8/24/2018 2:48:40 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Accounting.GL.Account.Action.SetActive]
                    --
                    -- Description: This procedure sets the GLAccount to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Accounting.GL.Account.Action.SetActive] @BID=1, @GLAccountID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Accounting.GL.Account.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @GLAccountID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the GLAccount specified is valid
                        IF NOT EXISTS(SELECT * FROM [Accounting.GL.Account] WHERE BID = @BID and ID = @GLAccountID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid GLAccount Specified. GLAccountID='+CONVERT(VARCHAR(12),@GLAccountID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        IF ( SELECT CanEdit FROM [Accounting.GL.Account] WHERE BID = @BID AND ID = @GLAccountID ) = 0
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Uneditable GLAccount Specified.'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;
	                    
                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Accounting.GL.Account] L
                        WHERE BID = @BID and ID = @GLAccountID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Accounting.Payment.Method.Action.SetActive]    Script Date: 8/24/2018 2:49:05 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Accounting.Payment.Method.Action.SetActive]
                    --
                    -- Description: This procedure sets the PaymentMethod to active or inactive
                    -- and sets the corresponding GLAccount to active or inactive as well
                    --
                    -- Sample Use:   EXEC dbo.[Accounting.Payment.Method.Action.SetActive] @BID=1, @PaymentMethodID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Accounting.Payment.Method.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @PaymentMethodID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the PaymentMethod specified is valid
                        IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Method] WHERE BID = @BID and ID = @PaymentMethodID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid PaymentMethod Specified. PaymentMethodID='+CONVERT(VARCHAR(12),@PaymentMethodID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Accounting.Payment.Method] L
                        WHERE BID = @BID and ID = @PaymentMethodID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive
	                      
                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Accounting.Payment.Term.Action.SetActive]    Script Date: 8/24/2018 2:49:58 PM ******/
                    SET ANSI_NULLS ON
                    GO
                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Accounting.Payment.Term.Action.SetActive]
                    --
                    -- Description: This procedure sets the PaymentTerm to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Accounting.Payment.Term.Action.SetActive] @BID=1, @PaymentTermID=1, @IsActive=1
                    -- ========================================================
                    ALTER PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @PaymentTermID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the PaymentTerm specified is valid
                        IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Term] WHERE BID = @BID and ID = @PaymentTermID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid PaymentTerm Specified. PaymentTermID='+CONVERT(VARCHAR(12),@PaymentTermID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Accounting.Payment.Term] L
                        WHERE BID = @BID and ID = @PaymentTermID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Accounting.Tax.Group.Action.SetActive]    Script Date: 8/24/2018 2:50:26 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO



                    -- ========================================================
                    -- Name: [Accounting.Tax.Group.Action.SetActive]
                    --
                    -- Description: This procedure sets the TaxGroup to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.SetActive] @BID=1, @TaxGroupID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @TaxGroupID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the TaxGroup specified is valid
                        IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Accounting.Tax.Group] L
                        WHERE BID = @BID and ID = @TaxGroupID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Accounting.Tax.Item.Action.SetActive]    Script Date: 8/24/2018 2:50:48 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Accounting.Tax.Item.Action.SetActive]
                    --
                    -- Description: This procedure sets the TaxItem to active or inactive
                    -- and sets the corresponding GLAccount to active or inactive as well
                    --
                    -- Sample Use:   EXEC dbo.[Accounting.Tax.Item.Action.SetActive] @BID=1, @TaxItemID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Accounting.Tax.Item.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @TaxItemID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the TaxItem specified is valid
                        IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid TaxItem Specified. TaxItemID='+CONVERT(VARCHAR(12),@TaxItemID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Accounting.Tax.Item] L
                        WHERE BID = @BID and ID = @TaxItemID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive
	                      
                        SET @Result = @@ROWCOUNT;

                        -- Now update it
                        UPDATE GL
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Accounting.GL.Account] GL
                        WHERE BID = @BID and ID = (SELECT GLAccountID FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @Result + @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Part.Labor.Data.Action.SetActive]    Script Date: 8/24/2018 2:51:28 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO


                    -- ========================================================
                    -- Name: [Part.Labor.Data.Action.SetActive]
                    --
                    -- Description: This procedure sets the Labor Data to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Part.Labor.Data.Action.SetActive] @BID=1, @TaxGroupID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Part.Labor.Data.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @LaborDataID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the Labor Data specified is valid
                        IF NOT EXISTS(SELECT * FROM [Part.Labor.Data] WHERE BID = @BID and ID = @LaborDataID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid Labor Data Specified. LaborDataID='+CONVERT(VARCHAR(12),@LaborDataID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Part.Labor.Data] L
                        WHERE BID = @BID and ID = @LaborDataID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                                
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Part.Labor.Category.Action.SetActive]    Script Date: 8/24/2018 2:55:03 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO


                    -- ========================================================
                    -- Name: [Part.Labor.Category.Action.SetActive]
                    --
                    -- Description: This procedure sets the LaborCategory to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Part.Labor.Category.Action.SetActive] @BID=1, @LaborCategoryID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Part.Labor.Category.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @LaborCategoryID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the LaborCategory specified is valid
                        IF NOT EXISTS(SELECT * FROM [Part.Labor.Category] WHERE BID = @BID and ID = @LaborCategoryID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid LaborCategory Specified. LaborCategoryID='+CONVERT(VARCHAR(12),@LaborCategoryID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Part.Labor.Category] L
                        WHERE BID = @BID and ID = @LaborCategoryID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                                
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Part.Machine.Action.SetActive]    Script Date: 8/24/2018 2:55:33 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Part.Machine.Data.Action.SetActive]
                    --
                    -- Description: This procedure sets the Machine Data to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Part.Machine.Action.SetActive] @BID=1, @ID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Part.Machine.Action.SetActive]
                    -- DECLARE 
                              @BID				TINYINT
                            , @ID				SMALLINT

                            , @IsActive			BIT     = 1

                            , @Result			INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the Machine Data specified is valid
                        IF NOT EXISTS(SELECT * FROM [Part.Machine.Data] WHERE BID = @BID and ID = @ID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid Machine Data Specified. MachineDataID='+CONVERT(VARCHAR(12),@ID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Part.Machine.Data] L
                        WHERE BID = @BID and ID = @ID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END

                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Part.Machine.Category.Action.SetActive]    Script Date: 8/24/2018 2:55:56 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO

                    -- ========================================================
                    -- Name: [Part.Machine.Category.Action.SetActive]
                    --
                    -- Description: This procedure sets the MachineCategory to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Part.Machine.Category.Action.SetActive] @BID=1, @MachineCategoryID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Part.Machine.Category.Action.SetActive]
                    -- DECLARE 
                              @BID                  TINYINT  -- = 1
                            , @MachineCategoryID    SMALLINT -- = 2

                            , @IsActive             BIT     = 1

                            , @Result               INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the MachineCategory specified is valid
                        IF NOT EXISTS(SELECT * FROM [Part.Machine.Category] WHERE BID = @BID and ID = @MachineCategoryID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid MachineCategory Specified. MachineCategoryID='+CONVERT(VARCHAR(12),@MachineCategoryID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Part.Machine.Category] L
                        WHERE BID = @BID and ID = @MachineCategoryID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END

                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Part.Material.Data.Action.SetActive]    Script Date: 8/24/2018 2:56:21 PM ******/
                    SET ANSI_NULLS ON
                    GO

                    SET QUOTED_IDENTIFIER ON
                    GO


                    -- ========================================================
                    -- Name: [Part.Material.Data.Action.SetActive]
                    --
                    -- Description: This procedure sets the Material to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Part.Material.Data.Action.SetActive] @BID=1, @MaterialID=1, @IsActive=1
                    -- ========================================================
                    CREATE PROCEDURE [dbo].[Part.Material.Data.Action.SetActive]
                    -- DECLARE 
                              @BID            TINYINT -- = 1
                            , @MaterialID     SMALLINT     -- = 2

                            , @IsActive       BIT     = 1

                            , @Result         INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the Material specified is valid
                        IF NOT EXISTS(SELECT * FROM [Part.Material.Data] WHERE BID = @BID and ID = @MaterialID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid Material Specified. MaterialID='+CONVERT(VARCHAR(12),@MaterialID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Part.Material.Data] L
                        WHERE BID = @BID and ID = @MaterialID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                                
                    GO
                "
            );

            migrationBuilder.Sql
            (
                @"
                    /****** Object:  StoredProcedure [dbo].[Part.Material.Category.Action.SetActive]    Script Date: 8/24/2018 2:56:39 PM ******/
                    SET ANSI_NULLS ON
                    GO
                    SET QUOTED_IDENTIFIER ON
                    GO
                    -- ========================================================
                    -- Name: [Part.Material.Category.Action.SetActive]
                    --
                    -- Description: This procedure sets the MaterialCategory to active or inactive
                    --
                    -- Sample Use:   EXEC dbo.[Part.Material.Category.Action.SetActive] @BID=1, @MaterialCategoryID=1, @IsActive=1
                    -- ========================================================
                    ALTER PROCEDURE [dbo].[Part.Material.Category.Action.SetActive]
                    -- DECLARE 
                              @BID                  TINYINT  -- = 1
                            , @MaterialCategoryID    SMALLINT -- = 2

                            , @IsActive             BIT     = 1

                            , @Result               INT     = NULL  OUTPUT
                    AS
                    BEGIN
                        DECLARE @Message VARCHAR(1024);

                        -- Check if the MaterialCategory specified is valid
                        IF NOT EXISTS(SELECT * FROM [Part.Material.Category] WHERE BID = @BID and ID = @MaterialCategoryID)
                        BEGIN
                            SELECT @Result = 0
                                 , @Message = 'Invalid MaterialCategory Specified. MaterialCategoryID='+CONVERT(VARCHAR(12),@MaterialCategoryID)+' not found'
                                 ;

                            THROW 50000, @Message, 1;
                            RETURN @Result;
                        END;

                        -- Now update it
                        UPDATE L
                        SET IsActive   = @IsActive
                            , ModifiedDT = GetUTCDate()
                        FROM [Part.Material.Category] L
                        WHERE BID = @BID and ID = @MaterialCategoryID
                          AND COALESCE(IsActive,~@IsActive) != @IsActive

                        SET @Result = @@ROWCOUNT;

                        SELECT @Result as Result;
                    END
                "
            );
        }
    }
}

