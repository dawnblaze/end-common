using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class RemoveEstimateLineItemEnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE [dbo].[enum.ClassType] WHERE Name='EstimateLineItem' and ID=10102;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[enum.ClassType] (ID, [Name], [Description], TableName, RecordNameFromObjectIDSQL)
                VALUES (10102,'EstimateLineItem',NULL,'','');
            ");

        }
    }
}
