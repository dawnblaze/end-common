using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180405201634_RenameBracketedIndex")]
    public partial class RenameBracketedIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "[FK_Part.Material.Data_Accounting.GL.Account2]",
                table: "Part.Material.Data");

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Material.Data_Accounting.GL.Account2",
                table: "Part.Material.Data",
                columns: new[] { "BID", "InventoryAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Material.Data_Accounting.GL.Account2",
                table: "Part.Material.Data");

            migrationBuilder.AddForeignKey(
                name: "[FK_Part.Material.Data_Accounting.GL.Account2]",
                table: "Part.Material.Data",
                columns: new[] { "BID", "InventoryAccountID" },
                principalTable: "Accounting.GL.Account",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }
    }
}

