using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8567_Create_MachineProfileVariable_Model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Part.Machine.Profile.Variable",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AssemblyID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "12036"),
                    DefaultValue = table.Column<string>(nullable: true),
                    GroupOptionByCategory = table.Column<bool>(nullable: true),
                    IsDisabled = table.Column<bool>(nullable: true),
                    IsRequired = table.Column<bool>(nullable: true),
                    Label = table.Column<string>(nullable: true),
                    ListValuesJSON = table.Column<string>(type: "nvarchar(max) sparse", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    Name = table.Column<string>(nullable: true),
                    OverrideDefault = table.Column<bool>(nullable: false, defaultValueSql: "0"),
                    ProfileID = table.Column<int>(nullable: false),
                    Tooltip = table.Column<string>(nullable: true),
                    UnitID = table.Column<byte>(type: "tinyint sparse", nullable: true),
                    VariableID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part.Machine.Profile.Variable", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile.Variable_Part.Machine.Profile",
                        columns: x => new { x.BID, x.ProfileID },
                        principalTable: "Part.Machine.Profile",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Part.Machine.Profile.Variable_Part.Subassembly.Variable",
                        columns: x => new { x.BID, x.VariableID },
                        principalTable: "Part.Subassembly.Variable",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Variable_Profile",
                table: "Part.Machine.Profile.Variable",
                columns: new[] { "BID", "ProfileID", "VariableID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Variable_Variable",
                table: "Part.Machine.Profile.Variable",
                columns: new[] { "BID", "VariableID", "ProfileID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Part.Machine.Profile.Variable");
        }
    }
}
