﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class PurgeMeasurementCBELAssemblies : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }

        public override bool ClearAllAssemblies()
        {
            return true;
        }
    }
}
