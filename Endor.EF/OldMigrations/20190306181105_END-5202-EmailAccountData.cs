using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END5202EmailAccountData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.CreateTable(
                name: "enum.EmailAccountStatus",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.EmailAccountStatus", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Email.Account.Data",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AliasUserNames = table.Column<string>(type: "varchar(1024)", nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "1023"),
                    Credentials = table.Column<string>(type: "varchar(max)", nullable: false),
                    CredentialsExpDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    DisplayName = table.Column<string>(type: "varchar(255)", nullable: true),
                    DomainEmailID = table.Column<short>(nullable: false),
                    DomainName = table.Column<string>(type: "varchar(255)", nullable: false),
                    EmailAddress = table.Column<string>(nullable: true, computedColumnSql: "(concat([UserName],'@',[DomainName]))"),
                    EmployeeID = table.Column<short>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [StatusType]=(2) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    IsPrivate = table.Column<bool>(nullable: false, computedColumnSql: "(isnull(case when [EmployeeID] IS NOT NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end,(0)))"),
                    LastEmailFailureDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    LastEmailSuccessDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "DATETIME2(2)", nullable: false, defaultValueSql: "GetUTCDate()"),
                    StatusType = table.Column<byte>(nullable: false),
                    UserName = table.Column<string>(type: "varchar(255)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Email.Account.Data", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Email.Account.Data_enum.Email.AccountStatus",
                        column: x => x.StatusType,
                        principalTable: "enum.EmailAccountStatus",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Email.Account.Data_Domain.Email.Data",
                        columns: x => new { x.BID, x.DomainEmailID },
                        principalTable: "Domain.Email.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Email.Account.Data_Employee.Data",
                        columns: x => new { x.BID, x.EmployeeID },
                        principalTable: "Employee.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.Data_StatusType",
                table: "Email.Account.Data",
                column: "StatusType");

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.Data_BID_DomainEmailID",
                table: "Email.Account.Data",
                columns: new[] { "BID", "DomainEmailID" });

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.Data_Employee",
                table: "Email.Account.Data",
                columns: new[] { "BID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.Data_Email",
                table: "Email.Account.Data",
                columns: new[] { "BID", "EmailAddress", "IsActive", "IsPrivate" });

            migrationBuilder.Sql(@"
CREATE VIEW [dbo].[EmailAccountData.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [DisplayName]
         , [IsActive]
		 , [EmployeeID]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Email.Account.Data]");


            migrationBuilder.Sql(@"
INSERT [enum.EmailAccountStatus] ([ID], [Name])
VALUES (1, N'Pending')
    , (2, N'Authorized')
    , (3, N'Failed')
    , (4, N'Expired')
    , (5, N'Inactive')
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Email.Account.Data");

            migrationBuilder.DropTable(
                name: "EmailAccountData.SimpleList");

            migrationBuilder.DropTable(
                name: "enum.EmailAccountStatus");
        }
    }
}
