using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180725214132_Add_BoardModuleLink")]
    public partial class Add_BoardModuleLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Module",
                columns: table => new
                {
                    ID = table.Column<short>(type: "smallint", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Module", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Board.Module.Link",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    BoardID = table.Column<short>(nullable: false),
                    ModuleType = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Board.Module.Link", x => new { x.BID, x.BoardID, x.ModuleType });
                    table.ForeignKey(
                        name: "FK_Board.Module.Link_enum.Module",
                        column: x => x.ModuleType,
                        principalTable: "enum.Module",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.NoAction);
                    table.ForeignKey(
                        name: "FK_Board.Module.Link_Board.Definition",
                        columns: x => new { x.BID, x.BoardID },
                        principalTable: "Board.Definition.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Board.Module.Link_ModuleType",
                table: "Board.Module.Link",
                column: "ModuleType");

            migrationBuilder.CreateIndex(
                name: "IX_Board.Module.Link_Module",
                table: "Board.Module.Link",
                columns: new[] { "BID", "ModuleType", "BoardID" });

            migrationBuilder.Sql(@"
                INSERT INTO [enum.Module]
                ([ID], [Name])
                VALUES
                (1, 'Management'),
                (2, 'Accounting'),
                (4, 'Sales'),
                (8, 'Production'),
                (16, 'Purchasing'),
                (32, 'Ecommerce'),
                (128, 'User')
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Board.Module.Link");

            migrationBuilder.DropTable(
                name: "enum.Module");
        }
    }
}

