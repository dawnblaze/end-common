using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180719164124_END-1910_fix_missing_searchterms")]
    public partial class END1910_fix_missing_searchterms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [dbo].[System.Option.Category]
                SET [SearchTerms] = CONCAT('Integration ',[SearchTerms])
                WHERE [SectionID]=700 AND [SearchTerms] NOT LIKE '%integration%'
            ");

            migrationBuilder.Sql(@"
UPDATE [System.Option.Definition] set Description = 'Note: If you chose to place the order in Invoiced, it will skip the entire workflow process.' where Name = 'QuickCheckout.StraightToInvoiced.Enable'
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

