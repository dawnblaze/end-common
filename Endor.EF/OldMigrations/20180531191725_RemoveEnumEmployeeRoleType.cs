using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180531191725_RemoveEnumEmployeeRoleType")]
    public partial class RemoveEnumEmployeeRoleType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Employee.Role_RoleType",
                table: "Order.Employee.Role");

            migrationBuilder.DropTable(
                name: "enum.Employee.RoleType");

            migrationBuilder.Sql(@"
DROP INDEX IF EXISTS [IX_Order.Employee.Role_RoleType] ON [dbo].[Order.Employee.Role];
");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_DestinationID",
                table: "Order.Employee.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_EmployeeID",
                table: "Order.Employee.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_OrderItemID",
                table: "Order.Employee.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_OrderID",
                table: "Order.Employee.Role");

            migrationBuilder.DropColumn(
                name: "RoleType",
                table: "Order.Employee.Role");

            migrationBuilder.AddColumn<short>(
                name: "RoleTypeID",
                table: "Order.Employee.Role",
                nullable: false,
                defaultValue: (short)1);

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_DestinationID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "DestinationID", "RoleTypeID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_EmployeeID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "EmployeeID", "OrderID", "RoleTypeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_OrderItemID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "OrderItemID", "RoleTypeID", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_OrderID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "OrderID", "RoleTypeID", "EmployeeID", "IsOrderRole" });

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Employee.Role_Employee.Role",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "RoleTypeID" },
                principalTable: "Employee.Role",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Employee.Role_Employee.Role",
                table: "Order.Employee.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_BID_RoleTypeID",
                table: "Order.Employee.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_DestinationID",
                table: "Order.Employee.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_EmployeeID",
                table: "Order.Employee.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_OrderItemID",
                table: "Order.Employee.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Employee.Role_OrderID",
                table: "Order.Employee.Role");

            migrationBuilder.DropColumn(
                name: "RoleTypeID",
                table: "Order.Employee.Role");

            migrationBuilder.AddColumn<byte>(
                name: "RoleType",
                table: "Order.Employee.Role",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.CreateTable(
                name: "enum.Employee.RoleType",
                columns: table => new
                {
                    ID = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToEstimate = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToEstimateText = table.Column<string>(maxLength: 11, nullable: true, computedColumnSql: "(case [AppliesToEstimate] when (0) then 'Not Allowed' when (1) then 'Optional' when (2) then 'Mandatory' end)"),
                    AppliesToOrder = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToOrderText = table.Column<string>(maxLength: 11, nullable: true, computedColumnSql: "(case [AppliesToOrder] when (0) then 'Not Allowed' when (1) then 'Optional' when (2) then 'Mandatory' end)"),
                    AppliesToPO = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToPOText = table.Column<string>(maxLength: 11, nullable: true, computedColumnSql: "(case [AppliesToPO] when (0) then 'Not Allowed' when (1) then 'Optional' when (2) then 'Mandatory' end)"),
                    AppliesToTeam = table.Column<byte>(type: "tinyint", nullable: false),
                    AppliesToTeamText = table.Column<string>(maxLength: 11, nullable: true, computedColumnSql: "(case [AppliesToTeam] when (0) then 'Not Allowed' when (1) then 'Optional' when (2) then 'Mandatory' end)"),
                    IsUserEntered = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Employee.RoleType", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_RoleType",
                table: "Order.Employee.Role",
                column: "RoleType");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_DestinationID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "DestinationID", "RoleType", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_EmployeeID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "EmployeeID", "OrderID", "RoleType" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_OrderItemID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "OrderItemID", "RoleType", "EmployeeID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Employee.Role_OrderID",
                table: "Order.Employee.Role",
                columns: new[] { "BID", "OrderID", "RoleType", "EmployeeID", "IsOrderRole" });

            migrationBuilder.AddForeignKey(
                name: "FK_Order.Employee.Role_RoleType",
                table: "Order.Employee.Role",
                column: "RoleType",
                principalTable: "enum.Employee.RoleType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

