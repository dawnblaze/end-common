using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180613213332_RemoveDeprecatedCustomFieldTables")]
    public partial class RemoveDeprecatedCustomFieldTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CRM.CustomField.HelperLink");

            migrationBuilder.DropTable(
                name: "CRM.CustomField.Def");

            migrationBuilder.DropTable(
                name: "CRM.CustomField.Helper");

            migrationBuilder.DropTable(
                name: "enum.CustomField.HelperType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CRM.CustomField.Def",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9100))"),
                    DataType = table.Column<byte>(nullable: false),
                    FieldName = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    InputType = table.Column<byte>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    MetaData = table.Column<string>(type: "xml", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.CustomField.Def", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField_enum.CustomField.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.CustomField.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField_enum.CustomField.InputType",
                        column: x => x.InputType,
                        principalTable: "enum.CustomField.InputType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "enum.CustomField.HelperType",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.CustomField.HelperType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CRM.CustomField.Helper",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((9101))"),
                    CompareValue = table.Column<string>(unicode: false, maxLength: 1024, nullable: true),
                    ComparisonType = table.Column<byte>(nullable: true),
                    DataType = table.Column<byte>(nullable: false),
                    HelperType = table.Column<byte>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    IsWarningOnly = table.Column<bool>(nullable: false),
                    MaxValue = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    Message = table.Column<string>(unicode: false, maxLength: 1024, nullable: true),
                    MinValue = table.Column<decimal>(type: "decimal(18, 4)", nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getutcdate())"),
                    Name = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    RegEx = table.Column<string>(unicode: false, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.CustomField.Helper", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.Helper_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.DataType",
                        column: x => x.DataType,
                        principalTable: "enum.CustomField.DataType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.Helper_enum.CustomField.HelperType",
                        column: x => x.HelperType,
                        principalTable: "enum.CustomField.HelperType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CRM.CustomField.HelperLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    CustomFieldID = table.Column<short>(nullable: false),
                    HelperID = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CRM.CustomField.HelperLink", x => new { x.BID, x.CustomFieldID, x.HelperID });
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField",
                        columns: x => new { x.BID, x.CustomFieldID },
                        principalTable: "CRM.CustomField.Def",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CRM.Setup.CustomField.HelperLink_CRM.Setup.CustomField.Helper",
                        columns: x => new { x.BID, x.HelperID },
                        principalTable: "CRM.CustomField.Helper",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CRM.CustomField.Def_DataType",
                table: "CRM.CustomField.Def",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_CRM.CustomField.Def_InputType",
                table: "CRM.CustomField.Def",
                column: "InputType");

            migrationBuilder.CreateIndex(
                name: "IX_CRM.CustomField.Helper_DataType",
                table: "CRM.CustomField.Helper",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_CRM.CustomField.Helper_HelperType",
                table: "CRM.CustomField.Helper",
                column: "HelperType");

            migrationBuilder.CreateIndex(
                name: "IX_CRM.Setup.CustomField.HelperLink",
                table: "CRM.CustomField.HelperLink",
                columns: new[] { "BID", "HelperID", "CustomFieldID" });
        }
    }
}

