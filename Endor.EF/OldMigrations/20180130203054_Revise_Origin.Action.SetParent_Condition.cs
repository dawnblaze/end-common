using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180130203054_Revise_Origin.Action.SetParent_Condition")]
    public partial class Revise_OriginActionSetParent_Condition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
                        migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.SetParent')
DROP PROCEDURE [Origin.Action.SetParent];
GO

-- ========================================================
-- Name: [Origin.Action.SetParent]
--
-- Description: This procedure sets the referenced Origin's parent ID
--
-- Sample Use:   EXEC dbo.[Origin.Action.SetParent] @BID=1, @OriginID=1, @ParentID=NULL
-- ========================================================
CREATE PROCEDURE [Origin.Action.SetParent]
-- DECLARE 
        @BID            TINYINT -- = 1
        , @OriginID       INT     -- = 2

        , @ParentID       INT     = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @OriginID)
    BEGIN
        SELECT @Result = 0
            , @Message = 'Invalid Origin Specified. OriginID='+CONVERT(VARCHAR(12),@OriginID)+' not found'
            ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update it
    UPDATE O
    SET   
        ParentID = @ParentID
    , ModifiedDT = GetUTCDate()
    FROM [CRM.Origin] O
    WHERE BID = @BID and ID = @OriginID
    AND ( (COALESCE(ParentID,~@ParentID) != @ParentID) OR (@ParentID IS NULL AND ParentID IS NOT NULL) )

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
                        ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
                        migrationBuilder.Sql(@"
IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.SetParent')
DROP PROCEDURE [Origin.Action.SetParent];
GO
                        ");
        }
    }
}

