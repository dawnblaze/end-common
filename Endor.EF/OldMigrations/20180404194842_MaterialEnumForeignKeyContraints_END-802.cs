using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180404194842_MaterialEnumForeignKeyContraints_END-802")]
    public partial class MaterialEnumForeignKeyContraints_END802 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@" 
ALTER TABLE [enum.Part.Unit] WITH CHECK
ADD CONSTRAINT [FK_enum.Part.Unit_enum.Part.Unit.Classification] FOREIGN KEY([UnitClassification])
 
REFERENCES [enum.Part.Unit.Classification] ([ID])
ALTER TABLE [enum.Part.Unit] CHECK CONSTRAINT [FK_enum.Part.Unit_enum.Part.Unit.Classification]
 
ALTER TABLE [enum.Part.Unit] WITH CHECK
ADD CONSTRAINT [FK_enum.Part.Unit_enum.Part.Unit.System] FOREIGN KEY([UnitSystem])
 
REFERENCES [enum.Part.Unit.System] ([ID])
ALTER TABLE [enum.Part.Unit] CHECK CONSTRAINT [FK_enum.Part.Unit_enum.Part.Unit.System]");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey("FK_enum.Part.Unit_enum.Part.Unit.Classification", "enum.Part.Unit");
            migrationBuilder.DropForeignKey("FK_enum.Part.Unit_enum.Part.Unit.System", "enum.Part.Unit");
        }
    }
}

