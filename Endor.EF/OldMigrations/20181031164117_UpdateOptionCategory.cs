using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateOptionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.Option.Category]
                     SET Name = 'Tag Setup',
                        SectionID = 400,
                        Description = 'Tag Setup for Board Items and General Use',
                        OptionLevels = 2,
                        IsHidden = 0,
                        SearchTerms = 'tag board card label color task tile' 
                    WHERE ID = 401;
                "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (
                @"
                    UPDATE [System.Option.Category]
                     SET Name = 'Artwork Options',
                        SectionID = 400,
                        Description = 'Artwork Options',
                        OptionLevels = 2,
                        IsHidden = 0,
                        SearchTerms = 'Production Artwork Art Approval Design Designer' 
                    WHERE ID = 401;
                "
            );
        }
    }
}
