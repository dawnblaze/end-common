﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddFrenchCanadianLanguage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT [System.Language.Type] ([ID], [Code], [GoogleCode], [Name], [DisplayName]) VALUES
    (5, N'fr-ca', N'fr', N'French (CA)', N'Français Canadien')
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
