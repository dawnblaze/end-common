using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180424194214_AddOrderKeyDateSprocs")]
    public partial class AddOrderKeyDateSprocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.ChangeKeyDate]( @BID tinyint, @OrderID int, @KeyDateType tinyint, @DateTime datetime2 )
--
-- Description: This function sets a key date of an order
--
-- Sample Use:   EXEC dbo.[Order.Action.ChangeKeyDate] @BID=1, @OrderID=1, @KeyDateType=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.ChangeKeyDate]
-- DECLARE 
          @BID            TINYINT    -- = 1
        , @OrderID        INT        -- = 2
        , @KeyDateType    TINYINT    -- = 1
        , @DateTime       DATETIME2  = NULL
        , @IsFirmDate     BIT        = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF (@DateTime IS NULL)
      SET @DateTime = GETUTCDATE();

    -- Update record is exists
    UPDATE KD
    SET    KeyDT = @DateTime, IsFirmDate = ISNULL(@IsFirmDate,0)
    FROM   [Order.KeyDate] KD
    WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType

    -- Add record is it does not exists
    IF @@ROWCOUNT = 0
    BEGIN
        DECLARE @NewID INT;
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10011, 1;

        INSERT INTO [Order.KeyDate]
        (BID, ID, OrderID, KeyDateType, KeyDT, IsFirmDate)
        VALUES
        (@BID, @NewID, @OrderID, @KeyDateType, @DateTime, ISNULL(@IsFirmDate,0))
    END;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END


GO

-- ========================================================
-- Name: [Order.Action.RemoveKeyDate]( @BID tinyint, @OrderID int )
--
-- Description: This function removes a key date of an order
--
-- Sample Use:   EXEC dbo.[Order.Action.RemoveKeyDate] @BID=1, @OrderID=1, @KeyDateType=1
-- ========================================================
CREATE PROCEDURE [dbo].[Order.Action.RemoveKeyDate]
-- DECLARE 
          @BID            TINYINT    -- = 1
        , @OrderID        INT        -- = 2
        , @KeyDateType    TINYINT    -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update record is exists
    DELETE FROM KD
    FROM   [Order.KeyDate] KD
    WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.ChangeKeyDate]
GO
DROP PROCEDURE IF EXISTS [dbo].[Order.Action.RemoveKeyDate]
");
        }
    }
}

