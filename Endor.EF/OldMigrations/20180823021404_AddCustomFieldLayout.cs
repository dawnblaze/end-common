using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180823021404_AddCustomFieldLayout")]
    public partial class AddCustomFieldLayout : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //[CustomField.Layout.Definition] table
            migrationBuilder.CreateTable(
                name: "CustomField.Layout.Definition",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((15027))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    IsActive = table.Column<bool>(nullable: false),
                    IsSystem = table.Column<bool>(nullable: false),
                    AppliesToClassTypeID = table.Column<int>(nullable: false),
                    IsAllTab = table.Column<bool>(nullable: false),
                    IsSubTab = table.Column<bool>(nullable: false),
                    SortIndex = table.Column<short>(nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomField.Layout.Definition", x => new { x.BID, x.ID });
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Definition_Type",
                table: "CustomField.Layout.Definition",
                columns: new[] { "BID", "AppliesToClassTypeID", "SortIndex", "IsSubTab" });

            //[CustomField.Layout.Container] table
            migrationBuilder.CreateTable(
                name: "CustomField.Layout.Container",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((15028))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),

                    LayoutID = table.Column<short>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Hint = table.Column<string>(type: "varchar(max)", nullable: true),
                    ColumnNo = table.Column<byte>(type: "tinyint", nullable: false),
                    SortIndex = table.Column<byte>(type: "tinyint", nullable: false),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomField.Layout.Container", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CustomField.Layout.Container_CustomField.Layout.Definition",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "CustomField.Layout.Definition",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Container_Layout",
                table: "CustomField.Layout.Container",
                columns: new[] { "BID", "LayoutID", "SortIndex"});

            //[CustomField.Layout.Element] table
            migrationBuilder.CreateTable(
                name: "CustomField.Layout.Element",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "((15029))"),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "(getdate())"),
                    ContainerID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    SortIndex = table.Column<short>(nullable: false),
                    DefinitionID = table.Column<short>(nullable: true),
                    DataType = table.Column<short>(nullable: true),
                    ElementProperties = table.Column<string>(type: "xml", nullable: true),
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomField.Layout.Element", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_CustomField.Layout.Element_CustomField.Definition",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "CustomField.Definition",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomField.Layout.Element_CustomField.Layout.Container",
                        columns: x => new { x.BID, x.ID },
                        principalTable: "CustomField.Layout.Container",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);

                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomField.Layout.Element_Containter",
                table: "CustomField.Layout.Element",
                columns: new[] { "BID", "ContainerID", "SortIndex" });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomField.Layout.Definition");
            migrationBuilder.DropTable(
                name: "CustomField.Layout.Container");
            migrationBuilder.DropTable(
                name: "CustomField.Layout.Element");
        }
    }
}

