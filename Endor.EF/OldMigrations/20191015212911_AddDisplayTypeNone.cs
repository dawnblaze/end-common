﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddDisplayTypeNone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [enum.CustomField.DisplayType] ([ID], [DataType], [FormatString], [Name])
VALUES (0, 0, NULL, 'None')
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [enum.CustomField.DisplayType] WHERE ID = 0;
VALUES (0, 0, NULL, 'None')
");
        }
    }
}
