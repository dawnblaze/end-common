using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20181023030740_ADDEmployeeLocationIDForeignKey")]
    public partial class ADDEmployeeLocationIDForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //first make sure all LocationID's point to a correct location
            migrationBuilder.Sql
            (
                @"
                    UPDATE [Employee.Data]
                    SET LocationID = 
                    ( Select TOP 1 ID from [Location.Data] ld where ld.BID = BID)
                    WHERE LocationID Not IN 
                    (Select ID from [Location.Data] ld where ld.BID = BID);
                "
            );

            //The following two statements clean up test data that may have been created with employee with a BID that has no locations
            migrationBuilder.Sql
            (
                @"
                    Delete[User.Link] where EmployeeID in 
                    (Select ID from[Employee.Data] ed
                    WHERE ed.BID Not IN
                    (Select BID from[Location.Data]));
                "
            );

            migrationBuilder.Sql
            (
                @"
                    Delete[Employee.Data]
                    WHERE BID Not IN
                    (Select BID from[Location.Data]);
                "
            );
            

            //Finally we should be good to add the foreign key
            migrationBuilder.AddForeignKey(
                name: "FK_Employee.Data_LocationID",
                table: "Employee.Data",
                columns: new[] { "BID", "LocationID" },
                principalTable: "Location.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Employee.Data_LocationID",
                table: "Employee.Data");
        }
    }
}

