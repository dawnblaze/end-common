using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END8259FixupRequiredDataThatGotRenamed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

exec [Util.Table.CopyDefaultRecords] 'Order.Item.Status'

-- Reset all of the defaults
UPDATE[Order.Item.Status]
SET IsDefault = (CASE WHEN ID BETWEEN 11 AND 65 THEN 1 ELSE 0 END)
    , IsActive = (CASE WHEN ID BETWEEN 11 AND 65 THEN 1 ELSE IsActive END)
;

-- Reset the names of the required
Update ois
set name = oisRequired.Name
from[order.item.status] ois
left join[Order.Item.Status] oisRequired on ois.id = oisRequired.ID
where ois.id < 100 and oisRequired.bid = -1
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
