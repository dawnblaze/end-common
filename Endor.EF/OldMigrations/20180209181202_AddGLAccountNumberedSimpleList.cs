using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180209181202_AddGLAccountNumberedSimpleList")]
    public partial class AddGLAccountNumberedSimpleList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.GL.Account.NumberedSimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.GL.Account.NumberedSimpleList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Accounting.GL.Account.NumberedSimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , NumberedName as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.GL.Account]
GO
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF Exists(select * from sys.objects where name = 'Accounting.GL.Account.NumberedSimpleList' and type = 'V')
  DROP VIEW [dbo].[Accounting.GL.Account.NumberedSimpleList]
GO
");
        }
    }
}

