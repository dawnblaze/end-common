using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180831161712_AddLocationGoal")]
    public partial class AddLocationGoal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Location.Goal",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    ID = table.Column<short>(nullable: false),
                    Actual = table.Column<decimal>(nullable: true),
                    Budgeted = table.Column<decimal>(nullable: true),
                    ClassTypeID = table.Column<int>(nullable: false, computedColumnSql: "1009"),
                    IsBusinessTotal = table.Column<bool>(nullable: true, computedColumnSql: "(case when [LocationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    IsYearlyTotal = table.Column<bool>(nullable: true, computedColumnSql: "(case when [Month] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end)"),
                    LocationID = table.Column<byte>(nullable: true),
                    ModifiedDT = table.Column<DateTime>(type: "datetime2(2)", nullable: false, defaultValueSql: "SYSUTCDATETIME()"),
                    Month = table.Column<byte>(nullable: true),
                    PercentOfGoal = table.Column<decimal>(nullable: true, computedColumnSql: "(case when [Budgeted]<>(0) then ([Actual]/[Budgeted])*(100.0)  end)"),
                    Year = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location.Goal", x => new { x.BID, x.ID });
                    table.ForeignKey(
                        name: "FK_Location.Goal_Business.Data",
                        column: x => x.BID,
                        principalTable: "Business.Data",
                        principalColumn: "BID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Location.Goal_Location.Data",
                        columns: x => new { x.BID, x.LocationID },
                        principalTable: "Location.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Location.Goal_Location_Date",
                table: "Location.Goal",
                columns: new[] { "BID", "LocationID", "Year", "Month" });

            migrationBuilder.CreateIndex(
                name: "IX_Location.Goal_Date_Location",
                table: "Location.Goal",
                columns: new[] { "BID", "Year", "Month", "LocationID" });

            migrationBuilder.Sql(@"
ALTER TABLE [Location.Goal] WITH CHECK
ADD CONSTRAINT [CK_Location.Goal_Month]
CHECK (([Month] IS NULL OR [Month]>=(1) AND [Month]<=(12)))
;
 
ALTER TABLE [Location.Goal]
CHECK CONSTRAINT [CK_Location.Goal_Month]
;
");

            migrationBuilder.Sql(@"
/*
    This view takes all of the Location Goal data and adds to it so it includes the
    Business Goal data (LocationID = NULL) and the annual totals (Month = NULL).
 
    This view is read-only.  The Stored Procedure [Location.Goal.CreateOrUpdate]
    should be used for updating the data.
*/
 
CREATE VIEW [Business.Goal] AS
SELECT TOP 100 PERCENT
        BID
      , (CASE WHEN MONTH IS NULL OR LocationID IS NULL THEN -ROW_NUMBER() OVER( ORDER BY Year DESC, Month DESC ) ELSE Max(ID) END) AS ID
      , Max(ClassTypeID) as ClassTypeID, Max(ModifiedDT) as ModifiedDT
      , LocationID, Year, Month
      , SUM(Budgeted) AS Budgeted, SUM(Actual) AS Actual
      , (case when SUM(Budgeted)<>(0) then (SUM(Actual)/SUM(Budgeted))*(100.0) end) AS PercentOfGoal
      , (case when [Month] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end) AS IsYearlyTotal
      , (case when [LocationID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end) AS IsBusinessTotal
FROM [Location.Goal]
GROUP BY BID, Year, ROLLUP(Month), ROLLUP(LocationID)
ORDER BY LocationID, Year, Month
;
");

            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Location.Goal.CreateOrUpdate]
GO
 
/***********************************************************
  Name: [Location.Goal.Update]

  Description: This procedure updates the Monthly Budgeted or Actual Goal for a location

  Sample Use:   EXEC dbo.[Location.Goal.Update] @BID=1, @LocationID=1, @Year=2017, @Month=1, @Budgeted = 52750
***********************************************************/
CREATE PROCEDURE [dbo].[Location.Goal.CreateOrUpdate]
-- DECLARE
          @BID            TINYINT  = 1
        , @LocationID     INT      = 1
 
        , @Year           SMALLINT = 2017
        , @Month          TINYINT  = 1
        , @Budgeted       DECIMAL(18,4) = NULL
        , @Actual         DECIMAL(18,4) = NULL
 
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    IF COALESCE(@Year, @Month, @LocationID, @BID) IS NULL 
        THROW 180000, 'You must provide the following values: @Year, @Month, @LocationID, @BID.', 1;
 
    -- See if we already have one in the database
    IF EXISTS(  SELECT * FROM [Location.Goal]
                WHERE BID = @BID
                AND LocationID = @LocationID
                AND Year = @Year
                AND Month = @Month )
    BEGIN -- UPDATE
        UPDATE [Location.Goal]
        SET ModifiedDT = GetUTCDate()
        , Budgeted = IsNull(@Budgeted, Budgeted)
        , Actual = IsNull(@Actual, Actual)
        WHERE BID = @BID
        AND LocationID = @LocationID
        AND Year = @Year
        AND Month = @Month
    END
     
    ELSE
 
    BEGIN -- INSERT if not
 
        -- Get the next ID
        DECLARE @NewID INT;
 
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1009, 1;  -- Class Type 1009 is Location.Goal
 
        -- Now insert
 
INSERT INTO [dbo].[Location.Goal]
    ( [BID], [ID], [ModifiedDT], [LocationID], [Year], [Month], [Budgeted], [Actual] )
VALUES
    ( @BID
    , @NewID
    , GetUTCDate()
    , @LocationID
    , @Year
    , @Month
    , @Budgeted
    , @Actual
    )
 
    END;
 
    SET @Result = @@ROWCOUNT;
    SELECT @Result as Result;
END;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP PROCEDURE IF EXISTS [dbo].[Location.Goal.CreateOrUpdate];
GO

DROP VIEW [Business.Goal];
GO
");

            migrationBuilder.DropTable(
                name: "Location.Goal");
        }
    }
}

