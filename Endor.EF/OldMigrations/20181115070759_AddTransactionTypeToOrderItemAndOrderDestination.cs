using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class AddTransactionTypeToOrderItemAndOrderDestination : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "TransactionType",
                table: "Order.Item.Data",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.AddColumn<byte>(
                name: "TransactionType",
                table: "Order.Destination.Data",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);

            migrationBuilder.Sql(@"
                -- Update the Order.Item.Data table
                --
                UPDATE OI
                SET TransactionType = OD.TransactionType
                FROM [Order.Item.Data] OI
                JOIN [Order.Data] OD on OD.ID = OI.OrderID
                ;

                ALTER TABLE dbo.[Order.Item.Data] 
                ADD CONSTRAINT [FK_Order.Item.Data_enum.Order.TransactionType] 
                FOREIGN KEY( TransactionType ) REFERENCES dbo.[enum.Order.TransactionType] ( ID )
                ;

                -- Update the Order.Destination.Data table
                --

                UPDATE OI
                SET TransactionType = OD.TransactionType
                FROM [Order.Destination.Data] OI
                JOIN [Order.Data] OD on OD.ID = OI.OrderID
                ;

                UPDATE [Order.Destination.Data]
                SET TransactionType = 2
                WHERE OrderID = 0  -- Force Update some bad data
                ;

                ALTER TABLE dbo.[Order.Destination.Data] 
                ADD CONSTRAINT [FK_Order.Destination.Data_enum.Order.TransactionType] 
                FOREIGN KEY( TransactionType ) REFERENCES dbo.[enum.Order.TransactionType] ( ID )
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TransactionType",
                table: "Order.Item.Data");

            migrationBuilder.DropColumn(
                name: "TransactionType",
                table: "Order.Destination.Data");
        }
    }
}
