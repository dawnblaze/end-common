using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180216163756_UpdateTaxItemSetActiveSPROC")]
    public partial class UpdateTaxItemSetActiveSPROC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
IF EXISTS(select * from sys.objects where name = 'Accounting.Tax.Item.Action.SetActive' and type = 'P')
  DROP PROCEDURE [dbo].[Accounting.Tax.Item.Action.SetActive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Accounting.Tax.Item.Action.SetActive]
--
-- Description: This procedure sets the TaxItem to active or inactive
-- and sets the corresponding GLAccount to active or inactive as well
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Item.Action.SetActive] @BID=1, @TaxItemID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Tax.Item.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @TaxItemID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxItem Specified. TaxItemID='+CONVERT(VARCHAR(12),@TaxItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.Tax.Item] L
    WHERE BID = @BID and ID = @TaxItemID
      AND COALESCE(IsActive,~@IsActive) != @IsActive
	  
    SET @Result = @@ROWCOUNT;

    -- Now update it
    UPDATE GL
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.GL.Account] GL
    WHERE BID = @BID and ID = (SELECT GLAccountID FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @Result + @@ROWCOUNT;

    SELECT @Result as Result;
END

GO

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

