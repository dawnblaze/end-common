using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180511140945_RebuildSearchCriteria")]
    /// <summary>
    /// this migration rebuilds System.List.Filter.Criteria to not use Identity IDs
    /// </summary>
    public partial class RebuildSearchCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

ALTER TABLE [dbo].[System.List.Filter.Criteria] DROP CONSTRAINT [PK_System.List.Filter.Criteria];
GO
ALTER TABLE [dbo].[System.List.Filter.Criteria] DROP COLUMN ID;
GO
DELETE FROM [dbo].[System.List.Filter.Criteria];
GO
ALTER TABLE [dbo].[System.List.Filter.Criteria] ADD ID smallint NOT NULL;
GO
ALTER TABLE [dbo].[System.List.Filter.Criteria] ADD
CONSTRAINT [PK_System.List.Filter.Criteria] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (1, 5000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 0, 0, 0, NULL, NULL, 0, NULL)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (2, 5000, N'Name', N'Name', N'FullName', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (3, 5000, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, NULL)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (4, 5000, N'Email', N'Email', N'EmailAddress', 0, 0, 0, 0, NULL, NULL, 0, NULL)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (5, 5000, N'Location', N'Location', N'LocationID', 0, 1, 4, 0, NULL, NULL, 0, NULL)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (6, 1005, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (7, 1005, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, NULL)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (8, 3000, N'Name', N'Name', N'LongName', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (9, 3000, N'Company', N'Company', N'Company', 0, 0, 0, 0, NULL, NULL, 0, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (10, 3000, N'Email', N'Email', N'EmailAddress', 0, 0, 0, 0, NULL, NULL, 0, 2)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (11, 3000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 0, 0, 0, NULL, NULL, 0, 3)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (12, 3000, N'Status', N'Status', N'Status', 0, 1, 5, 0, NULL, NULL, 0, 4)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (13, 3000, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 5)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (14, 11101, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (15, 11101, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (16, 11102, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (17, 11102, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (18, 11105, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (19, 11105, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (20, 2012, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (21, 2012, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (30, 2011, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (31, 2011, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (32, 12000, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (33, 12000, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (34, 12002, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (35, 12002, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (36, 12022, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (37, 12022, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (38, 12032, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (39, 12032, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (40, 12020, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (41, 12020, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (42, 12030, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (43, 12030, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (44, 2000, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (45, 2000, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, N'Is Not Active,Is Active', NULL, 1, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (46, 2000, N'SalesPerson', N'Sales Person', N'SalesPersonID', 0, 1, 1, 0, NULL, NULL, 0, 2)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (47, 2000, N'Location', N'Location', N'LocationID', 0, 1, 4, 0, NULL, NULL, 0, 3)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (48, 2000, N'ContactName', N'Contact Name', N'PrimaryContactID', 0, 1, 6, 0, NULL, NULL, 0, 4)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (49, 10000, N'OrderNumber', N'Order/Invoice #', N'Order', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (50, 10000, N'OrderStatus', N'Order Status', N'StatusID', 0, 4, 10, 0, NULL, NULL, 0, 1)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (51, 10000, N'Employees', N'Employees', N'EmployeeRolesIDs', 0, 4, 9, 0, NULL, NULL, 0, 2)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (52, 10000, N'Company', N'Company', N'CompanyName', 0, 0, 0, 0, NULL, NULL, 0, 3)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (53, 10000, N'Contact', N'Contact', N'ContactRolesNames', 0, 0, 0, 0, NULL, NULL, 0, 4)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (54, 10000, N'ShowVoidedOrders', N'Show Voided Orders', N'IsVoided', 0, 3, 11, 0, N'Hide Voided,Show Voided', NULL, 1, 5)
GO

");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

