using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180404174512_PartMaterialEnumTables_END802")]
    public partial class PartMaterialEnumTables_END802 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "enum.Part.Material.Consumption.Method",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Material.Consumption.Method", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Part.Material.Costing.Method",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Material.Costing.Method", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Part.Material.Physical.Type",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Dimensions = table.Column<byte>(nullable: false),
                    HeightRequired = table.Column<bool>(nullable: false),
                    LengthRequired = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    WeightRequired = table.Column<bool>(nullable: false),
                    WidthRequired = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Material.Physical.Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Part.Unit",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Abbreviation = table.Column<string>(unicode: false, maxLength: 15, nullable: false),
                    FromStandardMultiplyBy = table.Column<decimal>(nullable: false, computedColumnSql: "(1.0)/[ToStandardMultiplyBy]"),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    ToStandardMultiplyBy = table.Column<decimal>(type: "decimal(25, 18)", nullable: false),
                    UnitClassification = table.Column<byte>(nullable: false),
                    UnitClassificationText = table.Column<string>(nullable: true, computedColumnSql: "case [UnitClassification] when (0) then 'None' when (1) then 'Discrete' when (2) then 'Dimension' when (3) then 'Weight' when (4) then 'Time' else 'Unknown' end"),
                    UnitSystem = table.Column<byte>(nullable: false),
                    UnitSystemText = table.Column<string>(nullable: true, computedColumnSql: "case [UnitSystem] when (0) then 'Default' when (1) then 'Imperial' when (2) then 'Metric' when (3) then 'Metric and Imperial' else 'Unknown' end")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Unit", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Part.Unit.Classification",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Unit.Classification", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "enum.Part.Unit.System",
                columns: table => new
                {
                    ID = table.Column<byte>(nullable: false),
                    Name = table.Column<string>(unicode: false, maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enum.Part.Unit.System", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enum.Part.Material.Consumption.Method");

            migrationBuilder.DropTable(
                name: "enum.Part.Material.Costing.Method");

            migrationBuilder.DropTable(
                name: "enum.Part.Material.Physical.Type");

            migrationBuilder.DropTable(
                name: "enum.Part.Unit");

            migrationBuilder.DropTable(
                name: "enum.Part.Unit.Classification");

            migrationBuilder.DropTable(
                name: "enum.Part.Unit.System");
        }
    }
}

