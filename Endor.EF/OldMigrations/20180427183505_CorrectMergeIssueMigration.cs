using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180427183505_CorrectMergeIssueMigration")]
    public partial class CorrectMergeIssueMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.OrderLink_LinkedOrderID",
                table: "Order.OrderLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.OrderLink_OrderID",
                table: "Order.OrderLink");

            migrationBuilder.DropIndex(
                name: "IX_Order.Contact.Role_DestinationID",
                table: "Order.Contact.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Contact.Role_OrderItemID",
                table: "Order.Contact.Role");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Role_DestinationID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "DestinationID", "RoleType", "ContactID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Role_OrderItemID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "OrderItemID", "RoleType", "ContactID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Order.OrderLink_LinkedOrderID",
                table: "Order.OrderLink",
                columns: new[] { "BID", "LinkedOrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.OrderLink_OrderID",
                table: "Order.OrderLink",
                columns: new[] { "BID", "OrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.OrderLink_LinkedOrderID",
                table: "Order.OrderLink");

            migrationBuilder.DropForeignKey(
                name: "FK_Order.OrderLink_OrderID",
                table: "Order.OrderLink");

            migrationBuilder.DropIndex(
                name: "IX_Order.Contact.Role_DestinationID",
                table: "Order.Contact.Role");

            migrationBuilder.DropIndex(
                name: "IX_Order.Contact.Role_OrderItemID",
                table: "Order.Contact.Role");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Role_DestinationID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "DestinationID", "RoleType", "ContactID" },
                filter: "IsDestinationRole = 1");

            migrationBuilder.CreateIndex(
                name: "IX_Order.Contact.Role_OrderItemID",
                table: "Order.Contact.Role",
                columns: new[] { "BID", "OrderItemID", "RoleType", "ContactID" },
                filter: "IsOrderItemRole = 1");

            migrationBuilder.AddForeignKey(
                name: "FK_Order.OrderLink_LinkedOrderID",
                table: "Order.OrderLink",
                columns: new[] { "BID", "LinkedOrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Order.OrderLink_OrderID",
                table: "Order.OrderLink",
                columns: new[] { "BID", "OrderID" },
                principalTable: "Order.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}

