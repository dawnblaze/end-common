using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class END3935InsertofTaxabilityCodeClassTypeandTaxabilityCodedata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (@"
                IF NOT EXISTS(SELECT TOP 1 ID FROM [enum.ClassType] WHERE ID = 8005)
                BEGIN
                    INSERT INTO [enum.ClassType]
                    (ID, [Name], [Description], TableName)
                    VALUES
                    (8005, 'TaxabilityCode', 'A code or identifier that specifies the type of goods or services an item is for sales tax purposes.', 'Accounting.Taxability.Code')
                END
            "
            );

            migrationBuilder.Sql
            (@"
                INSERT INTO [Accounting.Tax.Code] ([BID],[ID],[ModifiedDT],[IsActive],[IsSystem],[Name],[Description],[TaxCode],[IsTaxExempt],[HasExcludedTaxItems])
                VALUES (1,0,'Nov 14 2018',1,1,'Goods','(Default) Used for taxable goods and services.​','',0,0)
                    , (1,1,'Nov 14 2018',1,0,'Service','Miscellaneous services which are not subject to a service-specific tax levy. This category will only treat services as taxable if the jurisdiction taxes services generally.','19000',0,0)
                    , (1,2,'Nov 14 2018',1,0,'Printing Service','Services provided to apply graphics and/or text to paper or other substrates which do not involve an exchange of tangible personal property','19009',0,0)
                    , (1,3,'Nov 14 2018',1,0,'Installation Service','Installation services separately stated from sales of tangible personal property','19001',0,0)
                    , (1,4,'Nov 14 2018',1,0,'Training Service','Services provided to educate users on the proper use of a product','19004',0,0)
                    , (1,5,'Nov 14 2018',1,0,'Advertising Service','Services rendered for advertising which do not include the exchange of tangible personal property','19001',0,0)
                    , (1,6,'Nov 14 2018',1,0,'Professional Service','Professional services which are not subject to a service-specific tax levy','19005',0,0)
                    , (1,7,'Nov 14 2018',1,0,'Repair Service','Services provided to restore tangible personal property to working order or optimal functionality','19007',0,0)
                    , (1,8,'Nov 14 2018',1,0,'Clothing','All human wearing apparel suitable for general use','20010',0,0)
                    , (1,9,'Nov 14 2018',1,0,'Digital Goods','Digital products transferred electronically','31000',0,0)
                    , (1,10,'Nov 14 2018',1,0,'Books','Books, printed','81100',0,0)
                    , (1,11,'Nov 14 2018',1,0,'Textbooks','Textbooks, printed','81110',0,0)
                    , (1,12,'Nov 14 2018',1,0,'Religious books','Religious books and manuals, printed','81120',0,0)
                    , (1,91,'Nov 14 2018',1,1,'FinanceCharge','Used for Finance Charges','99999',0,0)
                    , (1,92,'Nov 14 2018',1,0,'Handling','Used for Handling','',0,0)
                    , (1,98,'Nov 14 2018',1,0,'Shipping','Used for Shipping and Freight','',0,0)
                    , (1,99,'Nov 14 2018',1,1,'ForcedExempt','Used to force an Item as TaxExempt.','99999',0,0)
            "
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
            (@"
                IF EXISTS(SELECT TOP 1 ID FROM [enum.ClassType] WHERE ID = 8005)
                BEGIN
                    DELETE FROM [enum.ClassType] WHERE ID = 8005
                END
            "
            );

            migrationBuilder.Sql(@"DELETE FROM [Accounting.Tax.Code]");
        }
    }
}
