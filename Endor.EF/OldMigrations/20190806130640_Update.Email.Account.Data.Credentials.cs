using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class UpdateEmailAccountDataCredentials : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Email.Account.Data]
                ALTER COLUMN [Credentials] VARCHAR(MAX) NULL
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Email.Account.Data]
                ALTER COLUMN [Credentials] VARCHAR(MAX) NOT NULL
            ");
        }
    }
}
