using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180515160856_UpdateOrderNotes")]
    public partial class UpdateOrderNotes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn("OrderDataID", "Order.Note");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

