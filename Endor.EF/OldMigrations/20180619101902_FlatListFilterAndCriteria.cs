using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180619101902_FlatListFilterAndCriteria")]
    public partial class FlatListFilterAndCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[List.Filter]
([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],
[Criteria],
[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
VALUES
(1,    1032,'2018-6-19',  '2018-6-19',           1,'All',            1902,null,
null,
null,            0,        1,  null,       1,         0)


INSERT INTO [dbo].[List.Filter]
([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],
[Criteria],
[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
VALUES
(1,    1033,'2018-6-19',  '2018-6-19',           1,'Active',            1902,null,
'<ArrayOfListFilterItem>
    <ListFilterItem>
	    <Label>Active</Label>
        <SearchValue>true</SearchValue>
        <Field>IsActive</Field>
        <IsHidden>true</IsHidden>
        <IsSystem>true</IsSystem>
        <DisplayText>Is Active</DisplayText>
    </ListFilterItem>
</ArrayOfListFilterItem>',
null,            0,        1,  null,       1,         0)
");


            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (1902,'Name','Name','Name',0,0,0,0,NULL,NULL,0,1, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)

INSERT INTO [dbo].[System.List.Filter.Criteria] (TargetClassTypeID,[Name],Label,Field,IsHidden,DataType,InputType,AllowMultiple,
ListValues,ListValuesEndpoint,IsLimitToList,SortIndex,ID) VALUES (1902,'Is Active','Is Active','IsActive',0,3,2,0,'Is Not Active,Is Active',NULL,1,4, (SELECT TOP 1 ID FROM [dbo].[System.List.Filter.Criteria] ORDER BY ID DESC) + 1)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE [Dev.Endor.Business.DB1].[dbo].[System.List.Filter.Criteria]  where TargetClassTypeID=1902
        ");

            migrationBuilder.Sql(@"DELETE FROM [dbo].[List.Filter] where ID = 1033;");
            migrationBuilder.Sql(@"DELETE FROM [dbo].[List.Filter] where ID = 1032;");
        }
    }
}

