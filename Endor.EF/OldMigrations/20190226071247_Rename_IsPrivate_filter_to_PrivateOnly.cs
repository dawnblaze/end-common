using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class Rename_IsPrivate_filter_to_PrivateOnly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
              UPDATE [dbo].[System.List.Filter.Criteria]
                SET [Label] = 'Private Only'      
                WHERE [TargetClassTypeID] = 12060 and ID = 95
              GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
