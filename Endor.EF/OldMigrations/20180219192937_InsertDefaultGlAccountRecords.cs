using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180219192937_InsertDefaultGlAccountRecords")]
    public partial class InsertDefaultGlAccountRecords : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DECLARE @BID SMALLINT = 1;

DELETE FROM [Accounting.GL.Account] WHERE BID = @BID
  
INSERT [Accounting.GL.Account] ([BID], [ID], [ModifiedDT], [IsActive], [CanEdit], [Name], [Number], [GLAccountType], [ParentID], [ExportAccountName], [ExportAccountNumber], [ExportGLAccountType])
VALUES (@BID, 1000, '2018-01-01', 1, 0, N'Assets', 1000, 10, NULL, NULL, NULL, 10)
    , (@BID, 1100, '2018-01-01', 1, 1, N'Bank Account', 1100, 11, 1000, NULL, NULL, 11)
    , (@BID, 1200, '2018-01-01', 1, 0, N'Accounts Receivable', 1200, 12, 1000, NULL, NULL, 12)
    , (@BID, 1300, '2018-01-01', 1, 0, N'WIP', 1300, 10, 1000, NULL, NULL, 10)
    , (@BID, 1400, '2018-01-01', 1, 0, N'Built', 1400, 10, 1000, NULL, NULL, 10)
    , (@BID, 1500, '2018-01-01', 1, 0, N'Inventory', 1500, 10, 1000, NULL, NULL, 10)
    , (@BID, 2000, '2018-01-01', 1, 0, N'Liabilities', 2000, 20, NULL, NULL, NULL, 20)
    , (@BID, 2100, '2018-01-01', 1, 0, N'Customer Deposits', 2100, 20, 2000, NULL, NULL, 20)
    , (@BID, 2200, '2018-01-01', 1, 0, N'Customer Credits', 2200, 20, 2000, NULL, NULL, 20)
    , (@BID, 2300, '2018-01-01', 1, 0, N'Production Deliverables', 2300, 20, 2000, NULL, NULL, 20)
    , (@BID, 2400, '2018-01-01', 1, 0, N'Sales Tax Payable', 2400, 20, 2000, NULL, NULL, 20)
    , (@BID, 3000, '2018-01-01', 0, 0, N'Equity', 3000, 30, NULL, NULL, NULL, 30)
    , (@BID, 3100, '2018-01-01', 0, 0, N'Retained Earnings', 3100, 30, 3000, NULL, NULL, 30)
    , (@BID, 4000, '2018-01-01', 1, 0, N'Income', 4000, 40, NULL, NULL, NULL, 40)
    , (@BID, 4900, '2018-01-01', 1, 0, N'Finance Charges', 4900, 40, 4000, NULL, NULL, 40)
    , (@BID, 5000, '2018-01-01', 1, 0, N'COGS', 5000, 50, NULL, NULL, NULL, 50)
    , (@BID, 5100, '2018-01-01', 1, 1, N'Printing COGS', 5100, 50, 5000, NULL, NULL, 50)
    , (@BID, 5110, '2018-01-01', 1, 1, N'Paper', 5110, 50, 5100, NULL, NULL, 50)
    , (@BID, 5120, '2018-01-01', 1, 1, N'Ink', 5120, 50, 5100, NULL, NULL, 50)
    , (@BID, 5130, '2018-01-01', 1, 1, N'Lamination', 5130, 50, 5100, NULL, NULL, 50)
    , (@BID, 5200, '2018-01-01', 1, 1, N'Installation COGS', 5200, 50, 5000, NULL, NULL, 50)
    , (@BID, 5210, '2018-01-01', 1, 1, N'Installation Equipment Rental', 5210, 50, 5200, NULL, NULL, 50)
    , (@BID, 5220, '2018-01-01', 1, 1, N'Installation Supplies', 5220, 50, 5200, NULL, NULL, 50)
    , (@BID, 6000, '2018-01-01', 1, 0, N'Expenses', 6000, 60, NULL, NULL, NULL, 60)
    , (@BID, 6100, '2018-01-01', 1, 0, N'Credit Adjustments', 6100, 60, 6000, NULL, NULL, 60)
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DECLARE @BID SMALLINT = 1;

DELETE FROM [Accounting.GL.Account] WHERE BID = @BID
");
        }
    }
}

