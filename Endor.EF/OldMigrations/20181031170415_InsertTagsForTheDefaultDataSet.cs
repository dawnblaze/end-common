using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    public partial class InsertTagsForTheDefaultDataSet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
           (
               @"
                    INSERT INTO [List.Tag] (BID, ID, IsSystem, AssociatedClassTypeID, Name, ColorID, IsDeleted)
                        VALUES (1, 1, 0, NULL, 'Rush', 2, 0);
                    INSERT INTO [List.Tag] (BID, ID, IsSystem, AssociatedClassTypeID, Name, ColorID, IsDeleted)
                        VALUES (1, 2, 0, NULL, 'Install', 13, 0);
                    INSERT INTO [List.Tag] (BID, ID, IsSystem, AssociatedClassTypeID, Name, ColorID, IsDeleted)
                        VALUES (1, 3, 0, NULL, 'Shipped', 5, 0);
                "
           );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql
           (
               @"
                    DELETE FROM [List.Tag] WHERE ID = 1 AND Name = 'Rush';
                    DELETE FROM [List.Tag] WHERE ID = 2 AND Name = 'Install;
                    DELETE FROM [List.Tag] WHERE ID = 3 AND Name = 'Shipped;
                "
           );
        }
    }
}
