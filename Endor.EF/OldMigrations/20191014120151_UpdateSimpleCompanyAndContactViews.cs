﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class UpdateSimpleCompanyAndContactViews : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [dbo].[Company.SimpleList] 
AS
SELECT [BID]
	, [ID]
	, [ClassTypeID]
	, [Name] AS [DisplayName]
	, [IsActive]
	, [HasImage]
    , CONVERT(BIT,0) AS [IsDefault]
	, [IsAdhoc]
FROM
	[Company.Data] 	
GO
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [dbo].[Contact.SimpleList]
AS
SELECT
	  c.[BID]
	, c.[ID]
	, c.[ClassTypeID]
	, c.[ShortName] AS [DisplayName]
	, l.[IsActive]
	, c.[HasImage]
	, l.[IsPrimary] AS [IsDefault]
	, l.[CompanyID] as [ParentID]
FROM
	dbo.[Contact.Data] c
	LEFT JOIN dbo.[Company.Contact.Link] l ON c.[BID] = l.[BID] AND c.[ID] = l.[ContactID]
GO
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [dbo].[Company.SimpleList] 
AS
SELECT        
    BID, 
    ID, 
    ClassTypeID, 
    Name AS DisplayName, 
    IsActive, 
    HasImage, 
    CONVERT(BIT, 0) AS IsDefault, 
    IsAdHoc
FROM            
    dbo.[Company.Data]
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [dbo].[Contact.SimpleList]
AS
SELECT        
    BID, 
    ID, 
    ClassTypeID, 
    ShortName AS DisplayName, 
    IsActive, 
    HasImage, 
    CONVERT(BIT, 0) AS IsDefault, 
    CompanyID AS ParentID
FROM            
    dbo.[Contact.Data]
");
        }
    }
}
