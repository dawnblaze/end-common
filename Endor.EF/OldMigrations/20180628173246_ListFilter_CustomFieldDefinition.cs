using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180628173246_ListFilter_CustomFieldDefinition")]
    public partial class ListFilter_CustomFieldDefinition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[List.Filter] (BID,ID,CreatedDate,IsActive,[Name],
TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,hint,IsDefault,SortIndex ) 
VALUES (1,(SELECT TOP 1 ID from [dbo].[List.Filter] ORDER BY ID DESC)+1,'2017-10-18',1,'All',15025, NULL,NULL,NULL,0,1,NULL,1,0)
  
INSERT INTO [dbo].[List.Filter] (BID,ID,CreatedDate,IsActive,[Name],
TargetClassTypeID,IDs,Criteria,OwnerID,IsPublic,IsSystem,hint,IsDefault,SortIndex ) 
VALUES (1,(SELECT TOP 1 ID from [dbo].[List.Filter] ORDER BY ID DESC)+1,'2017-10-18',1,'Active',15025, NULL,'
<ArrayOfListFilterItem>
  <ListFilterItem>
    <Label>Active</Label>
    <SearchValue>True</SearchValue>
    <Field>IsActive</Field>
    <DisplayText>Is Active</DisplayText>
    <IsHidden>false</IsHidden>
  </ListFilterItem>
</ArrayOfListFilterItem>  
  ',NULL,0,1,NULL,1,0)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE [dbo].[List.Filter]  where TargetClassTypeID=15025
            ");
        }
    }
}

