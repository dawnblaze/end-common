using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180403093649_DeleteACHPaymentMethodFromDatabase")]
    public partial class DeleteACHPaymentMethodFromDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE
                FROM [Accounting.Payment.Method]
                WHERE ID = 3
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT [Accounting.Payment.Method] ([BID], [ID], [ModifiedDT], [IsActive], [Name], [PaymentMethodType], [DepositGLAccountID], [IsIntegrated], [IsCustom])
                VALUES (1, 3, '2018-01-01', 0, N'ACH (External)', 3, 1100, 0, 0);
            ");
        }
    }
}

