using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20180222190019_PaymentTermFloatsToDecimals")]
    public partial class PaymentTermFloatsToDecimals : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "EarlyPaymentPercent",
                table: "Accounting.Payment.Term",
                type: "decimal(12,4)",
                nullable: true,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DownPaymentThreshold",
                table: "Accounting.Payment.Term",
                type: "decimal(18,4)",
                nullable: true,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DownPaymentPercentBelow",
                table: "Accounting.Payment.Term",
                type: "decimal(12,4)",
                nullable: true,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.Sql(@"
ALTER TABLE [Accounting.Payment.Term] DROP COLUMN [DownPaymentRequired];
");

            migrationBuilder.AlterColumn<decimal>(
                name: "DownPaymentPercent",
                table: "Accounting.Payment.Term",
                type: "decimal(12,4)",
                nullable: true,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.Sql(@"
ALTER TABLE [Accounting.Payment.Term] ADD [DownPaymentRequired]  AS (case when [DownPaymentPercent]>(0.00) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Accounting.Payment.Term] DROP COLUMN [DownPaymentRequired];
");
            migrationBuilder.AlterColumn<double>(
                name: "EarlyPaymentPercent",
                table: "Accounting.Payment.Term",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(12,4)",
                oldNullable: true);

            migrationBuilder.Sql(@"
ALTER TABLE [Accounting.Payment.Term] ADD [DownPaymentRequired]  AS (case when [DownPaymentPercent]>(0.00) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end);
");
            migrationBuilder.AlterColumn<double>(
                name: "DownPaymentThreshold",
                table: "Accounting.Payment.Term",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "DownPaymentPercentBelow",
                table: "Accounting.Payment.Term",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(12,4)",
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "DownPaymentPercent",
                table: "Accounting.Payment.Term",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(12,4)",
                oldNullable: true);
        }
    }
}

