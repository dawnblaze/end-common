﻿using Microsoft.EntityFrameworkCore.Migrations;
using Endor.Models;

namespace Endor.EF.Migrations
{
    public partial class END11309_DefaultWalkInLocationCompanyContact : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"
/* 
-- ========================================================
    Procedure Name: [Util.Table.CopyDefaultRecords] 
    Description: This function copys all of the -1 and -2 records for a table 
                    into all of the BIDs for which they don't exist.
                    It then deletes all the BID=-2 records.
    Sample Use:   
        EXEC [Util.Table.CopyDefaultRecords] @TableName = '_Root.Data';
        
-- ========================================================
*/
CREATE OR ALTER PROCEDURE [dbo].[Util.Table.CopyDefaultRecords] 
    (@TableName NVARCHAR(200), @IncludeRequired BIT = 1, @IncludeDefault BIT = 1, @TargetBID SMALLINT = NULL, @IDFieldName VARCHAR(100) = 'ID')
AS
BEGIN
    -- DECLARE @TableName NVARCHAR(200) = '_Root.Data';
    -- DECLARE @IncludeRequired BIT = 1;
    -- DECLARE @IncludeDefault BIT = 1
    DECLARE @BIDS NVARCHAR(30) = IIF(@IncludeRequired=1, IIF(@IncludeDefault=1, '(-1, -2)', '(-1)'), IIF(@IncludeDefault=1, '(-2)', '(null)'));
    DECLARE @TargetBIDString NVARCHAR(12) = IIF(@TargetBID IS NULL, ' > 0 ', CONCAT(' = ', @TargetBID));
    DECLARE @Columns NVARCHAR(MAX);
    SELECT @Columns = CONCAT(@Columns + ', ', '[',ColumnName,']')
    FROM TablesAndColumns
    WHERE TableName = @TableName
        AND ColumnName NOT IN ('BID', 'ValidToDT', 'ModifiedDT')
        AND IsComputed = 0
        AND IsIdentity = 0
    ;
    --PRINT @Columns;
    DECLARE @SQL NVARCHAR(MAX) = '
    INSERT INTO [@TABLENAME] ( BID, '+@Columns+' )
        SELECT B.BID, T.*
        FROM [Business.Data] B
        JOIN 
        (   SELECT '+@Columns+'
            FROM [@TABLENAME]
            WHERE BID IN '+@BIDS+'
        ) T ON 1=1
        WHERE NOT EXISTS (SELECT * FROM [@TABLENAME] D WHERE D.BID = B.BID AND D.'+@IDFieldName+' = T.'+@IDFieldName+')
            AND B.BID'+@TargetBIDString+'
    ;
    ';
    IF (@IncludeDefault=1)
        SET @SQL += '
    DELETE T
    FROM [@TABLENAME] T
    WHERE T.BID = -3
        AND T.'+@IDFieldName+' IN (SELECT T2.'+@IDFieldName+' FROM [@TABLENAME] T2 WHERE T2.BID = -2)
    ;
    UPDATE [@TABLENAME]
    SET BID = -3
    WHERE BID = -2
    ;
    ';
    SET @SQL = REPLACE(@SQL, '@TABLENAME', @TableName);
    --PRINT @SQL
    EXECUTE(@SQL);
END;
            ");

            migrationBuilder.Sql(@"
IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[dbo].[Contact.Data]') 
         AND name = 'StatusID'
) BEGIN
  ALTER TABLE [Contact.Data]
  ADD [StatusID] tinyint NOT NULL CONSTRAINT [DF_Contact.Data_StatusID]  DEFAULT ((1))
  ALTER TABLE [dbo].[Contact.Data]  WITH CHECK ADD  CONSTRAINT [FK_Contact.Data_enum.CRM.Company.Status] FOREIGN KEY([StatusID])
  REFERENCES [dbo].[enum.CRM.Company.Status] ([ID])
END
            ");

            migrationBuilder.Sql(@"
DECLARE @BID SMALLINT = -1;

IF NOT EXISTS ( SELECT * FROM [dbo].[CRM.Origin] WHERE BID = @BID AND ID=1)
BEGIN
INSERT INTO [dbo].[CRM.Origin]
([BID], [ID], [IsActive], [Name])
VALUES (@BID, 1, 0, 'Walk-In')
END;

IF NOT EXISTS ( SELECT * FROM [dbo].[Location.Data] WHERE BID = @BID AND ID=1)
BEGIN
INSERT INTO [Location.Data] 
([BID], [ID], [IsActive], [Name], [IsDefault], [TimeZoneID], [LegalName], [InvoicePrefix],[EstimatePrefix], [POPrefix], [OrderNumberPrefix], [CreditMemoPrefix], [DefaultTaxGroupID], [HasImage])
VALUES(@BID, 1, 1, 'Default', 1, 10, 'My Company', 'I','E','PO', 'ORD','CM', 1, 0)
END
;

IF NOT EXISTS ( SELECT * FROM [dbo].[List.FlatList.Data] WHERE BID = @BID AND ID=100) 
BEGIN
  insert into [dbo].[List.FlatList.Data] (BID,ID,FlatListType,IsActive,IsAdHoc,IsSystem,ModifiedDT,[Name],SortIndex)
  values (-1,100,100,1,0,1,GETDATE(),'Default',0)
END;

IF NOT EXISTS ( SELECT * FROM [dbo].[Company.Data] WHERE BID = @BID AND ID=1) 
BEGIN
INSERT INTO [Company.Data]
([BID],[ID],[IsActive],[Name],[LocationID], [IsAdHoc], [StatusID], [IsPORequired], [RefundableCredit], [NonRefundableCredit], [CreditLimit], [HasImage], [IsTaxExempt], [PaymentTermID], [PricingTierID], [OriginID])
VALUES (@BID, 1, 1, 'Walk In', 1, 0, 4, 0, 0, 0, 0, 0, 0, 1, 100, 1)
END;

IF NOT EXISTS ( SELECT * FROM [dbo].[Contact.Data] WHERE BID = @BID AND ID=1) 
BEGIN
INSERT INTO [Contact.Data] 
([BID], [ID], [First], [Last], [LocationID], [HasImage], [StatusID])
VALUES(@BID, 1, 'Walk', 'In', 1, 0, 1)
END;

IF NOT EXISTS ( SELECT * FROM [dbo].[Company.Contact.Link] WHERE BID = @BID AND [CompanyID]=1 AND [ContactID]=1) 
BEGIN
INSERT INTO [Company.Contact.Link] 
([BID],[CompanyID],[ContactID],[Roles],[Position])
VALUES (@BID, 1, 1, 3, 'Customer')
END;

EXEC [Util.Table.CopyDefaultRecords] @TableName = 'CRM.Origin';
EXEC [Util.Table.CopyDefaultRecords] @TableName = 'Location.Data';
EXEC [Util.Table.CopyDefaultRecords] @TableName = 'List.FlatList.Data';
EXEC [Util.Table.CopyDefaultRecords] @TableName = 'Company.Data';
EXEC [Util.Table.CopyDefaultRecords] @TableName = 'Contact.Data';
EXEC [Util.Table.CopyDefaultRecords] @TableName = 'Company.Contact.Link', @IDFieldName = 'CompanyID';
            ");


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
        public override int? ReindexClasstype()
        {
            return ClassType.Location.ID();
        }
    }
}
