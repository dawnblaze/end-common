﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END9886_AddPerThousandUnitTypeEnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [enum.Unit.Type] ([ID],[Name])
                VALUES (3, 'Per Thousand')
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE [enum.Unit.Type] WHERE [ID]=3
            ");
        }
    }
}
