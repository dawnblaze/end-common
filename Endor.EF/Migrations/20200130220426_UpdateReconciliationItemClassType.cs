﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class UpdateReconciliationItemClassType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Accounting.Reconciliation.Item",
                nullable: false,
                computedColumnSql: "8016",
                oldClrType: typeof(int),
                oldType: "int",
                oldComputedColumnSql: "8015");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Accounting.Reconciliation.Item",
                type: "int",
                nullable: false,
                computedColumnSql: "8015",
                oldClrType: typeof(int),
                oldComputedColumnSql: "8016");
        }
    }
}
