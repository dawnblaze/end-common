﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11733AddLastAccessDateColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE  [User.Link]
ADD LastConnectionDT DateTime2(2), LastDisconnectionDT DateTime2(2), LastActivityDT DateTime2(2);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE  [User.Link]
DROP COLUMN LastConnectionDT, LastDisconnectionDT, LastActivityDT;
            ");
        }
    }
}
