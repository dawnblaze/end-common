﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11313_UpdateSPTinyBIDToSmallInt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //Accounting.Payment.Method.Action.CanDelete
            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Accounting.Payment.Method.Action.CanDelete]

    Description: This procedure checks if the PaymentMethod is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Payment.Method.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE OR ALTER PROCEDURE [dbo].[Accounting.Payment.Method.Action.CanDelete]
          @BID                  SMALLINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	A paymentmethod can be deleted if:

	* It exists
	* It is a custom payment method.
	* It is not used by any payments.
	* It is not used by any stored payment methods.
*/

    SET @Result = 1;

    IF NOT EXISTS( SELECT * FROM [Accounting.Payment.Method] WHERE BID = @BID AND ID = @ID ) 
        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Method Specified Does not Exist. PaymentMethod=', @ID)

    -- Check if the record exists
    ELSE IF  0=(SELECT IsCustom FROM [Accounting.Payment.Method] WHERE ID = @ID)
        SELECT @Result = 0
             , @CantDeleteReason = 'Custom Payment Type is in use and can''t be deleted'

    --ELSE IF EXISTS(SELECT * FROM [Accounting.Payment.Data] WHERE PaymentMethodID = @ID)
    --    SELECT @Result = 0
    --         , @CantDeleteReason = 'Payment Data for Payment Method Exists'

    --ELSE IF EXISTS(SELECT * FROM [Accounting.Payment.Credential] WHERE PaymentMethodID = @ID)
    --    SELECT @Result = 0
    --         , @CantDeleteReason = 'Payment Credentials for Payment Method Exists'

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
            ");

            //Accounting.Payment.Term.Action.SetDefault
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Payment.Term.Action.SetDefault]
--
-- Description: This procedure sets the default PaymentTerm in the options definition table.
--
-- Date: 2018-02-22
-- 
-- Sample Use: EXEC dbo.[Accounting.Payment.Term.Action.SetDefault] @BID=1, @PaymentTermID=1, @IsActive=1
-- ========================================================
CREATE OR ALTER     PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetDefault]
          @BID           SMALLINT     -- = 1
        , @PaymentTermID SMALLINT     -- = 2
        , @IsDefault     BIT           = 1
        , @Result        INT           = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);
    -- Check if the option specified is valid
    IF NOT EXISTS(SELECT * FROM [System.Option.Definition] WHERE Name = 'Accounting.PaymentTerm.DefaultID')
    BEGIN
        SELECT @Result = 0
             , @Message = 'The Accounting.PaymentTerm.DefaultID OPTION is not found.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;
	IF (@PaymentTermID != 0)
	BEGIN
		-- Check if the payment term exists
		IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Term] WHERE ID = @PaymentTermID)
		BEGIN
			SELECT @Result = 0
				 , @Message = 'Accounting.Payment.Term does not exist.'
				 ;

			THROW 50000, @Message, 1;
			RETURN @Result;
		END;

		UPDATE [Accounting.Payment.Term]
		SET IsActive = 1
		WHERE ID = @PaymentTermID

		-- Now update it
		UPDATE L
		SET DefaultValue = @PaymentTermID
		FROM [System.Option.Definition] L
		WHERE Name = 'Accounting.PaymentTerm.DefaultID'
	END
	ELSE
	BEGIN
		UPDATE [System.Option.Definition]
		SET DefaultValue = NULL
		WHERE [Name] = 'Accounting.PaymentTerm.DefaultID'
	END
	SET @Result = @@ROWCOUNT;
	SELECT @Result as Result;
END
GO
            ");

            //Accounting.Tax.Group.Action.LinkLocation
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Tax.Group.Action.LinkLocation]
--
-- Description: This procedure links/unlinks the Location to the TaxGroup
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.LinkLocation] @BID=1, @TaxGroupID=1, @LocationID=1, @IsLinked=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Accounting.Tax.Group.Action.LinkLocation]
--DECLARE 
          @BID            SMALLINT --= 1
        , @TaxGroupID     SMALLINT --= 2
		, @LocationID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.LocationLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID and GroupID = @TaxGroupID and LocationID = @LocationID)
		BEGIN
			INSERT INTO [Accounting.Tax.Group.LocationLink] (BID, GroupID, LocationID)
			VALUES (@BID, @TaxGroupID, @LocationID)
		END;

		-- Check location default taxgroup and add taxgroup ID if null
		IF EXISTS (SELECT * FROM [Location.Data] WHERE ID = @LocationID and DefaultTaxGroupID IS NULL)
		BEGIN
			UPDATE [Location.Data]
			SET DefaultTaxGroupID = @TaxGroupID
			WHERE ID = @LocationID and DefaultTaxGroupID IS NULL
		END
	END
	ELSE
	BEGIN
		-- Remove entry from Tax.Group.LocationLink
		DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID and GroupID = @TaxGroupID and LocationID = @LocationID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
GO
            ");

            //Accounting.Tax.Group.Action.LinkTaxItem
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Tax.Group.Action.LinkTaxItem]
--
-- Description: This procedure links/unlinks the TaxItem to the TaxGroup
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.LinkTaxItem] @BID=1, @TaxGroupID=1, @TaxItemID=1, @IsLinked=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Accounting.Tax.Group.Action.LinkTaxItem]
--DECLARE 
          @BID            SMALLINT --= 1
        , @TaxGroupID     SMALLINT --= 2
		, @TaxItemID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the TaxItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxItem Specified. TaxItemID='+CONVERT(VARCHAR(12),@TaxItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.ItemLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID and GroupID = @TaxGroupID and ItemID = @TaxItemID)
		BEGIN
			INSERT INTO [Accounting.Tax.Group.ItemLink] (BID, GroupID, ItemID)
			VALUES (@BID, @TaxGroupID, @TaxItemID)
		END;
	END
	ELSE
	BEGIN
		-- Remove entry from Tax.Group.ItemLink
		DELETE FROM [Accounting.Tax.Group.ItemLink] WHERE BID = @BID and GroupID = @TaxGroupID and ItemID = @TaxItemID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
GO
            ");

            //Accounting.Tax.Group.Action.RecomputeTaxRate
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Accounting.Tax.Group.Action.RecomputeTaxRate]
--
-- Description: This procedure recomputes the TaxRate for the TaxGroup based on the linked TaxItems.
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.RecomputeTaxRate] @BID=1, @TaxGroupID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Accounting.Tax.Group.Action.RecomputeTaxRate]
--DECLARE 
          @BID            SMALLINT --= 1
        , @TaxGroupID     SMALLINT --= 2
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

 	-- Compute total tax rate for taxgroup based on linked tax items
	DECLARE @TotalTaxRate decimal(12,4) = 0
	SET @TotalTaxRate = (SELECT SUM([Accounting.Tax.Item].TaxRate) FROM [Accounting.Tax.Group.ItemLink] INNER JOIN [Accounting.Tax.Item] ON [Accounting.Tax.Group.ItemLink].ItemID = [Accounting.Tax.Item].ID
	WHERE [Accounting.Tax.Group.ItemLink].BID = @BID and [Accounting.Tax.Group.ItemLink].GroupID = @TaxGroupID)
	SET @TotalTaxRate = ISNULL(@TotalTaxRate, 0);
	UPDATE [Accounting.Tax.Group] SET TaxRate = @TotalTaxRate WHERE ID = @TaxGroupID

    SET @Result = 1;

    SELECT @Result as Result;
END
GO
            ");

            //Board.Definition.Action.PostResults
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Board.Definition.Action.PostResults]( @BID tinyint, @BoardID smallint, @RecordCount int, @TimeInSec float )
--
-- Description: This function records the time for a board execution
-- and updates the cummulative timer on the board.
--  Sample Use:  
/*
    DECLARE @BID SMALLINT = 1
          , @BoardID INT = 10
          , @RecordCount INT = 5
          , @TimeInSec FLOAT = 3.2;
 
    EXEC dbo.[Board.Definition.Action.PostResults] @BID=@BID, @BoardID=@BoardID, @RecordCount=@RecordCount, @TimeInSec=@TimeInSec
    SELECT * FROM [Board.Definition] WHERE BID = @BID AND ID = @BoardID
*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Board.Definition.Action.PostResults]
--DECLARE
          @BID          SMALLINT = 1
        , @BoardID      INT     = 10
        , @RecordCount  INT     = 7
        , @TimeInSec    FLOAT   = 1.24
AS
BEGIN
    -- Now update it
    UPDATE B
    SET   ModifiedDT = GETUTCDATE()
        , LastRunDT = GETUTCDATE()
        , LastRecordCount = @RecordCount
        , LastRunDurationSec = @TimeInSec
        , CummCounterDT = IsNull(CummCounterDT, GETUTCDATE())
        , CummRunCount += 1
        , CummRunDurationSec += @TimeInSec
    FROM [Board.Definition.Data] B
    WHERE BID = @BID
      AND ID = @BoardID
    ;
 
    DECLARE @Rows INT = @@RowCount;
 
    -- Check if the Board specified is valid
    IF (@Rows = 0)
    BEGIN
        DECLARE @Message VARCHAR(1024) = CONCAT('Invalid Board Specified. BoardID=',@BoardID,' not found');
        THROW 50000, @Message, 1;
    END;
 
    RETURN @Rows;
END;

GO
            ");

            //Board.Definition.Action.ResetTimer
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Board.Definition.Action.ResetTimer]( @BID tinyint = null, @BoardID smallint = null )
--
-- Description: This function resets the specified Board Cummulative Timers.
-- If no @BoardID is specified, all Boards for the given BID are reset.
-- If not @BID is specified, all BIDs are reset.
--
-- Sample Use:   EXEC dbo.[Board.Definition.Action.ResetTimer] @BID=1
-- Sample Use:   EXEC dbo.[Board.Definition.Action.ResetTimer] @BID=1, @BoardID=10
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Board.Definition.Action.ResetTimer]
--DECLARE
          @BID          SMALLINT = 1
        , @BoardID      INT     = 10
 
AS
BEGIN
    -- Update it
    UPDATE B
    SET   ModifiedDT = GETUTCDATE()
        , CummCounterDT = GETUTCDATE()
        , CummRunCount = 0
        , CummRunDurationSec = 0
    FROM [Board.Definition.Data] B
    WHERE BID = ISNULL(@BID, BID)
      AND ID = ISNULL(@BoardID, ID)
    ;
 
    DECLARE @Rows INT = @@RowCount;
 
    -- Check if the Board specified is valid
    IF (@Rows = 0)
    BEGIN
        DECLARE @Message VARCHAR(1024) = CONCAT('Invalid Board Specified. BoardID=',@BoardID,' not found');
        THROW 50000, @Message, 1;
    END;
 
    RETURN @Rows;
END;

GO
            ");

            //Board.View.Action.Link
            migrationBuilder.Sql(@"
CREATE OR ALTER PROCEDURE [dbo].[Board.View.Action.Link]
-- DECLARE 
  @BID SMALLINT 
, @BoardID SMALLINT 
, @BoardViewID SMALLINT 
, @SortIndex TINYINT = NULL
AS
BEGIN
    IF NOT EXISTS(SELECT 1 FROM [Board.Definition.Data] WHERE BID =  @BID AND ID = @BoardID)
        THROW 50000, 'Board not found', 1

    IF NOT EXISTS(SELECT 1 FROM [Board.View] WHERE BID = @BID AND ID = @BoardViewID)
        THROW 50000, 'Board view not found', 1

    IF EXISTS(SELECT 1 FROM [Board.View.Link] WHERE BID = @BID AND ViewID = @BoardViewID AND BoardID = @BoardID)
        THROW 50000, 'Board link already exist', 1

    IF @SortIndex IS NULL
        SELECT @SortIndex = MAX(SortIndex)+1 FROM [Board.View.Link] WHERE BID = @BID AND ViewID = @BoardViewID;

    IF @SortIndex IS NULL
        SET @SortIndex = 1;

    UPDATE [Board.View.Link]
    SET SortIndex += 1
    WHERE BID = @BID AND ViewID = @BoardViewID AND SortIndex >= @SortIndex
    ;

    INSERT INTO [Board.View.Link]
    (BID, BoardID, ViewID, SortIndex)
    VALUES
    (@BID, @BoardID, @BoardViewID, @SortIndex)
END

GO
            ");

            //Board.View.Action.Unlink
            migrationBuilder.Sql(@"
CREATE OR ALTER PROCEDURE [dbo].[Board.View.Action.Unlink]
-- DECLARE 
  @BID SMALLINT 
, @BoardID SMALLINT 
, @BoardViewID SMALLINT 
AS
BEGIN
    IF NOT EXISTS(SELECT 1 FROM [Board.Definition.Data] WHERE BID =  @BID AND ID = @BoardID)
        THROW 50000, 'Board not found', 1

    IF NOT EXISTS(SELECT 1 FROM [Board.View] WHERE BID = @BID AND ID = @BoardViewID)
        THROW 50000, 'Board view not found', 1

    IF NOT EXISTS(SELECT 1 FROM [Board.View.Link] WHERE BID = @BID AND ViewID = @BoardViewID AND BoardID = @BoardID)
        THROW 50000, 'Board link not found', 1

    DELETE FROM [Board.View.Link]
    WHERE BID = @BID AND ViewID = @BoardViewID AND BoardID = @BoardID
END

GO
            ");

            //Employee.TimeClock.HoursThisWeek
            migrationBuilder.Sql(@"
                /* ========================================================
                --Name: [Employee.TimeClock.HoursThisWeek] (@BID tinyint, @EmployeeID int )
                --
                -- Description: This function retrieves the time an employee worked for the day and week.
                --      The starting DateTime for the Day and the Week must be passed in since the server
                --      time is UTC and this won't work for measuring the week and day start!
                --
                -- Sample Use:   EXEC dbo.[Employee.TimeClock.HoursThisWeek] @BID= 1, @EmployeeID= 1
                -- ======================================================== */
                CREATE OR ALTER PROCEDURE [dbo].[Employee.TimeClock.HoursThisWeek]
                -- DECLARE
                          @BID SMALLINT -- = 1
                        , @EmployeeID INT -- = 100
                        , @WeekStartDT DateTime2(2)
                        , @DayStartDT DateTime2(2)
                AS
                BEGIN

                    SELECT COALESCE(TotalTimeForWeek, 0)/60.0 AS TotalHoursForWeek
                           , COALESCE(TotalTimeForWeek, 0)/60.0 - COALESCE(UnpaidTimeForWeek, 0)/60.0 AS PaidHoursForWeek
                           , COALESCE(UnpaidTimeForWeek, 0)/60.0 AS UnpaidHoursForWeek
                           , COALESCE(TotalTimeForDay, 0)/60.0 AS TotalHoursForDay
                           , COALESCE(TotalTimeForDay, 0)/60.0 - COALESCE(UnpaidTimeForDay , 0)/60.0 AS PaidHoursForDay
                          , COALESCE(UnpaidTimeForDay, 0)/60.0 AS UnpaidHoursForDay
                    FROM
                        (
                            SELECT SUM(TimeInMin ) as TotalTimeForWeek
                                , SUM(IIF(StartDT BETWEEN @DayStartDT AND GetUTCDate(), TimeInMin, 0  ) ) as TotalTimeForDay
                          FROM[Employee.TimeCard] TC
                         WHERE BID = @BID AND EmployeeID = @EmployeeID
                         AND StartDT BETWEEN @WeekStartDT AND GetUTCDate()
                        ) TotalTime

                    JOIN
                        (
                            SELECT SUM(TimeInMin ) as UnpaidTimeForWeek
                                , SUM(IIF(StartDT BETWEEN @DayStartDT AND GetUTCDate(), TimeInMin, 0  ) ) as UnpaidTimeForDay
                          FROM[Employee.TimeCard.Detail] TCD
                         WHERE BID = @BID AND EmployeeID = @EmployeeID
                         AND StartDT BETWEEN @WeekStartDT AND GetUTCDate()
                            AND IsPaid = 0
                        ) UnpaidTime
                    ON 1=1

                END
GO
            ");

            //IAtom.Action.CanDelete
            migrationBuilder.Sql(@"
CREATE OR ALTER PROCEDURE [dbo].[IAtom.Action.CanDelete]
--DECLARE
          @BID            SMALLINT 
        , @ID             INT     
        , @ClassTypeID    INT     
        
        , @ShowCantDeleteReason BIT          = 1
        , @Result               BIT          = NULL OUTPUT
        , @CantDeleteReason     VARCHAR(MAX) = NULL OUTPUT
AS
BEGIN
    -- Find the table Name
    DECLARE @TableName VARCHAR(255) = RTRIM(LTRIM(dbo.[Util.ClassType.GetTableName]( @ClassTypeID )))
          , @CMD       NVARCHAR(MAX)
          , @Message   VARCHAR(1024)
	;

    -- Check that we found a table name by ClassTypeID
    IF LEN(@TableName) = 0
    BEGIN
        SELECT @Message = CONCAT('Unable to Resolve a Table Name for ClassTypeID ', @ClassTypeID)
             , @Result = 0;

        THROW 50000, @Message, 1;
        -- RETURN @Result;
    END

    -- Now look any FK dependencies on the table, BID, and ID

    SELECT @CMD = CONCAT(@CMD + ' UNION ', ' SELECT ''[', FK_TableName, ']'' AS [TableName] WHERE EXISTS(SELECT * FROM [', FK_TableName, '] WHERE BID=', @BID, ' AND [', FK_ColumnName, ']=', @ID, ') ')
    FROM [ForeignKeyConstraints]
    WHERE PK_TableName = dbo.[Util.ClassType.GetTableName]( @ClassTypeID ) -- 'Accounting.GL.Data'
    AND PK_ColumnName != 'BID'
	;

	IF (@@ROWCOUNT = 0) -- Check if we have any constraints ... 
	BEGIN
		SET @Result = 1;
		SELECT @Result;
		RETURN @Result;
	END;

	SET @CMD = CONCAT(' SELECT @CantDeleteReason = CONCAT(@CantDeleteReason+'' AND '', [TableName]) FROM ( ',@CMD, ') TEMP ')

	-- SELECT @CMD;
	EXEC sp_executesql @CMD, N'@CantDeleteReason VARCHAR(MAX) OUTPUT', @CantDeleteReason=@CantDeleteReason OUTPUT

	-- SELECT @CantDeleteReason;
	SET @Result = IIF(@CantDeleteReason IS NULL, 1, 0);

    IF (@Result = 0)
    BEGIN
        SET @CantDeleteReason = 'Unable to Delete Record - Reference by the following Tables: '+@CantDeleteReason;

        IF (@ShowCantDeleteReason=1) 
            SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]  --' -- to fix VS Code formatting bug
        ELSE
            SELECT @Result as Result;
    END
    ELSE
        SELECT @Result as Result;
END
GO
            ");

            //Industry.Action.SetParent
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Industry.Action.SetParent]
--
-- Description: This procedure sets the Parent of the industry
--
-- Sample Use:   EXEC dbo.[Industry.Action.SetParent] @BID=1, @IndustryID=1, @ParentID=2
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Industry.Action.SetParent]
-- DECLARE 
          @BID            SMALLINT -- = 1
        , @IndustryID     SMALLINT     -- = 2

        , @ParentID       SMALLINT -- = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

-- Check if the Industry and Parent are different
	IF (@IndustryID = @ParentID)
	BEGIN
	 SELECT @Result = 0
             , @Message = 'Invalid Parent Specified. ParentID can not be equal to IndustryID'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
	END

    -- Check if the Industry specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @IndustryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Industry Specified. IndustryID='+CONVERT(VARCHAR(12),@IndustryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;
	
    -- If specifying a parent industry, check if the parent industry specified is valid
    IF @ParentID IS NOT NULL 
	BEGIN
	  IF NOT EXISTS(SELECT * FROM [CRM.Industry] WHERE BID = @BID and ID = @ParentID)
      BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Parent Industry Specified. ParentID='+CONVERT(VARCHAR(12),@ParentID)+' not found'
             ;
	    
        THROW 50000, @Message, 1;
        RETURN @Result;
      END;
	END;

    -- Now update it
    UPDATE L
    SET ParentID = @ParentID
    FROM [CRM.Industry] L
    WHERE BID = @BID and ID = @IndustryID      

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Location.Action.SetDefault
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Location.Action.SetDefault]( @BID tinyint, @LocationID int )
--
-- Description: This function sets any existing default location
-- to false then sets the location (@LocationID) to default and active
--
-- Sample Use:   EXEC dbo.[Location.Action.SetDefault] @BID=1, @LocationID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Location.Action.SetDefault]
-- DECLARE 
          @BID            SMALLINT -- = 1
        , @LocationID     INT     -- = 2

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Location.Data]);

	IF (@Count > 1)
	BEGIN
		-- Remove IsDefault from any other Default location(s) for the business
		UPDATE L
		SET IsDefault = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Location.Data] L
		WHERE BID = @BID AND IsDefault = 1
	END

    -- Now update it
    UPDATE L
    SET IsActive   = 1
		, IsDefault = 1
        , ModifiedDT = GetUTCDate()
    FROM [Location.Data] L
    WHERE BID = @BID and ID = @LocationID
		AND COALESCE(IsDefault,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Location.Goal.CreateOrUpdate
            migrationBuilder.Sql(@"
/***********************************************************
  Name: [Location.Goal.Update]

  Description: This procedure updates the Monthly Budgeted or Actual Goal for a location

  Sample Use:   EXEC dbo.[Location.Goal.Update] @BID=1, @LocationID=1, @Year=2017, @Month=1, @Budgeted = 52750
***********************************************************/
CREATE OR ALTER PROCEDURE [dbo].[Location.Goal.CreateOrUpdate]
-- DECLARE
          @BID            SMALLINT = 1
        , @LocationID     INT      = 1
 
        , @Year           SMALLINT = 2017
        , @Month          TINYINT  = 1
        , @Budgeted       DECIMAL(18,4) = NULL
        , @Actual         DECIMAL(18,4) = NULL
 
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    IF COALESCE(@Year, @Month, @LocationID, @BID) IS NULL 
        THROW 180000, 'You must provide the following values: @Year, @Month, @LocationID, @BID.', 1;
 
    -- See if we already have one in the database
    IF EXISTS(  SELECT * FROM [Location.Goal]
                WHERE BID = @BID
                AND LocationID = @LocationID
                AND Year = @Year
                AND Month = @Month )
    BEGIN -- UPDATE
        UPDATE [Location.Goal]
        SET ModifiedDT = GetUTCDate()
        , Budgeted = IsNull(@Budgeted, Budgeted)
        , Actual = IsNull(@Actual, Actual)
        WHERE BID = @BID
        AND LocationID = @LocationID
        AND Year = @Year
        AND Month = @Month
    END
     
    ELSE
 
    BEGIN -- INSERT if not
 
        -- Get the next ID
        DECLARE @NewID INT;
 
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1009, 1;  -- Class Type 1009 is Location.Goal
 
        -- Now insert
 
INSERT INTO [dbo].[Location.Goal]
    ( [BID], [ID], [ModifiedDT], [LocationID], [Year], [Month], [Budgeted], [Actual] )
VALUES
    ( @BID
    , @NewID
    , GetUTCDate()
    , @LocationID
    , @Year
    , @Month
    , @Budgeted
    , @Actual
    )
 
    END;
 
    SET @Result = @@ROWCOUNT;
    SELECT @Result as Result;
END;

GO
            ");

            //Order.Action.AddContactRole
            migrationBuilder.Sql(@"
-- ========================================================
/* 
     SPROC: [Order.Action.AddContactRole] @BID smallint, @OrderID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255)
     Description: This function adds a order contact role

     Sample Use:   

        SELECT * FROM [Order.Contact.Role] WHERE BID =1 AND OrderID = 1100
        EXEC dbo.[Order.Action.AddContactRole] @BID=1, @OrderID=1100, @contactRoleType=2, @contactID=1001
        SELECT * FROM [Order.Contact.Role] WHERE BID =1 AND OrderID = 1100

*/
-- ========================================================
CREATE OR ALTER   PROCEDURE [dbo].[Order.Action.AddContactRole]
-- DECLARE 
          @BID             SMALLINT   -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024) = NULL
          , @CompanyID INT
          ;

    SELECT @CompanyID = CompanyID 
    FROM [Order.Data] O 
    WHERE O.BID = @BID AND O.ID = @OrderID
    ;

    -- Check if the Order specified is valid
    IF (@ContactID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'Contact Not Specified.');

    ELSE IF (@CompanyID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'Invalid Order Specified. OrderID=',@OrderID,' not found or Company not Set; ');


    ELSE IF NOT EXISTS( SELECT * FROM [Company.Contact.Link] CL WHERE CL.BID = @BID AND CL.CompanyID = @CompanyID AND CL.ContactID = @ContactID )
        SET @Message = CONCAT('//' + @Message, 'Invalid Contact Specified or Contact is not associated with the Order Company.');

    IF (@Message IS NOT NULL)
    BEGIN
        SET @Result = 0;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @NewID INT;
    EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10013, 1;

    INSERT INTO [Order.Contact.Role] (BID, ID, OrderID, RoleType, IsAdHoc, ContactID, ContactName, ContactCompany)
        SELECT @BID, @NewID, @OrderID, @ContactRoleType, 0, @ContactID
            , (SELECT ShortName FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
            , (SELECT Name FROM [Company.Data] WHERE BID = @BID AND ID = @CompanyID)
    ;

    SET @Result = @@ROWCOUNT;
END

GO
            ");

            //Order.Action.AddEmployeeRole
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.AddEmployeeRole]( @BID smallint, @OrderID int, @EmployeeRoleID smallint, @EmployeeID int )
--
-- Description: This function adds a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.AddEmployeeRole] @BID=1, @OrderID=1, @EmployeeRoleID=1, @EmployeeID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.AddEmployeeRole]
-- DECLARE 
          @BID             SMALLINT   -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleID  SMALLINT   -- = 1
        , @EmployeeID      INT        -- = 1

        , @Result          INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the role type does not exist on the order
    IF ISNULL(@EmployeeRoleID, -1) != 255 AND EXISTS(SELECT * FROM [Order.Employee.Role] WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @EmployeeRoleID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Employee role already exist on this order.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @NewID INT;
    EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10014, 1;

    INSERT INTO [Order.Employee.Role]
    (BID, ID, OrderID, RoleID, EmployeeID, EmployeeName)
    SELECT @BID, @NewID, @OrderID, @EmployeeRoleID, @EmployeeID, employee.ShortName
    FROM   [Employee.Data] employee
    WHERE  employee.BID = @BID AND employee.ID = @EmployeeID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END

GO
            ");

            //Order.Action.ChangeKeyDate
            migrationBuilder.Sql(@"
/*********************************************************
  Name:
    [Order.Action.ChangeKeyDate]( @BID smallint, @OrderID int, @OrderItemID int = null, @DestinationID int = null, @KeyDateType tinyint, @DateTime datetime2 = null, @IsFirmDate bit = null )
  
  Description:
    This function sets a key date of an order
  
  Sample Use:
    EXEC [dbo].[Order.Action.ChangeKeyDate] @BID=1, @OrderID=1, @OrderItemID=1, @DestinationID=1, @KeyDateType=1, @DateTime=null, @IsFirmDate=null;
*********************************************************/
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.ChangeKeyDate]
(
--DECLARE
    @BID            SMALLINT    --= 1
  , @OrderID        INT			--= 1049
  , @OrderItemID	INT			= NULL
  , @DestinationID	INT			= NULL
  , @KeyDateType	TINYINT		= 1
  , @DateTime       DATETIME2	= NULL
  , @IsFirmDate		BIT			= NULL
  
  , @Result         INT			= NULL  OUTPUT
)
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF (@DateTime IS NULL)
      SET @DateTime = GETUTCDATE();
	
    -- Update record if exists
	IF (@OrderItemID IS NULL AND @DestinationID IS NULL)
	BEGIN
		UPDATE KD
		SET    KeyDT = @DateTime, IsFirmDate = COALESCE(@IsFirmDate, IsFirmDate)
		FROM   [Order.KeyDate] KD
		WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType
			   AND OrderItemID IS NULL AND DestinationID IS NULL
	END
	ELSE IF (@OrderItemID IS NOT NULL AND @DestinationID IS NULL)
	BEGIN
		UPDATE KD
		SET    KeyDT = @DateTime, IsFirmDate = COALESCE(@IsFirmDate, IsFirmDate)
		FROM   [Order.KeyDate] KD
		WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType
			   AND OrderItemID = @OrderItemID AND DestinationID IS NULL
	END
	ELSE IF (@OrderItemID IS NULL AND @DestinationID IS NOT NULL)
	BEGIN
		UPDATE KD
		SET    KeyDT = @DateTime, IsFirmDate = COALESCE(@IsFirmDate, IsFirmDate)
		FROM   [Order.KeyDate] KD
		WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType
			   AND OrderItemID IS NULL AND DestinationID = @DestinationID
	END
	ELSE
	BEGIN
		UPDATE KD
		SET    KeyDT = @DateTime, IsFirmDate = COALESCE(@IsFirmDate, IsFirmDate)
		FROM   [Order.KeyDate] KD
		WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType
			   AND OrderItemID = @OrderItemID AND DestinationID = @DestinationID
	END

    -- Add record if it does not exists
    IF @@ROWCOUNT = 0
    BEGIN
        DECLARE @NewID INT;
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10011, 1;

        INSERT INTO [Order.KeyDate]
        (BID, ID, OrderID, OrderItemID, DestinationID, KeyDateType, KeyDT, IsFirmDate)
        VALUES
        (@BID, @NewID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, ISNULL(@IsFirmDate,0))
    END;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Order.Action.ChangeNote
            migrationBuilder.Sql(@"
/*********************************************************
  Name:
    [Order.Action.ChangeNote]( @BID smallint, @OrderID int, @OrderItemID int = null, @DestinationID int = null, @NoteType tinyint, @Note varchar(max) )
  
  Description:
    This function sets a note of an order
  
  Sample Use:
    EXEC [dbo].[Order.Action.ChangeNote] @BID=1, @OrderID=1, @OrderItemID=1, @DestinationID=1, @NoteType=1, @Note=null;
*********************************************************/
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.ChangeNote]
(
--DECLARE
    @BID            SMALLINT		--= 1
  , @OrderID        INT			--= 1049
  , @OrderItemID	INT			= NULL
  , @DestinationID	INT			= NULL
  , @NoteType       TINYINT     = 1
  , @Note           VARCHAR(MAX)
  
  , @Result         INT			= NULL  OUTPUT
)
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update record if exists
    IF (@OrderItemID IS NULL AND @DestinationID IS NULL)
    BEGIN
        UPDATE N
        SET    Note = @Note
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID IS NULL AND DestinationID IS NULL
    END
    ELSE IF (@OrderItemID IS NOT NULL AND @DestinationID IS NULL)
    BEGIN
        UPDATE N
        SET    Note = @Note
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID = @OrderItemID AND DestinationID IS NULL
    END
    ELSE IF (@OrderItemID IS NULL AND @DestinationID IS NOT NULL)
    BEGIN
        UPDATE N
        SET    Note = @Note
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID IS NULL AND DestinationID = @DestinationID
    END
    ELSE
    BEGIN
        UPDATE N
        SET    Note = @Note
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID = @OrderItemID AND DestinationID = @DestinationID
    END

    -- Add record if it does not exists
    IF @@ROWCOUNT = 0
    BEGIN
        DECLARE @NewID INT;
        EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 10011, 1;

        INSERT INTO [Order.Note]
        (BID, ID, OrderID, OrderItemID, DestinationID, NoteType, Note)
        VALUES
        (@BID, @NewID, @OrderID, @OrderItemID, @DestinationID, @NoteType, @Note)
    END;

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
            
GO
            ");

            //Order.Action.ComputeBalanceDueDate
            migrationBuilder.Sql(@"
                /*********************************************************
                  Name:
                    [Order.Action.ComputeBalanceDueDate]( @BID smallint, @OrderID int)
  
                  Description:
                    This function computes balance due date of an order

	                0 - 	Return NULL if the Order Status != Invoiced
			                Start with Invoiced DateTime
			                Convert to the Location's TimeZone and take the Date Only (no time)
			                Add The DaysDue and Return 

	                1 -		Return NULL if the Order Status not in (Invoicing, Invoiced)
			                Start with Invoicing DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                Add The DaysDue and Return

	                2 -		Return NULL if the Order Status is Closed or Voided
			                Start with Created DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                Add The DaysDue and Return
	
	                3 -		Return NULL if the Order Status != Invoiced
			                Start with Invoiced DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                Move to the First Day of the Next Month 
			                If the (DaysDue > Days in that month), return the last day of the month.
			                Otherwise, return the first of the month + (DaysDue - 1 )

	                4 -		Return NULL if the Order Status != Invoiced
			                Start with Invoiced DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                If the DaysDue is > the Specified Day, Add 1 Month
			                If the (DaysDue > Days in that month), return the last day of the month.
			                Otherwise, return the first of the month + (DaysDue - 1 )

	                5 -		Return NULL if the Order Status != Invoiced
			                Start with Invoiced DateTime
			                Convert to the Order's Location's TimeZone and take the Date Only (no time)
			                Add 1 Month
			                If the (DaysDue > Days in that month), return the last day of the month.
			                Otherwise, return the first of the month + (DaysDue - 1 )


	                ===================================================================================

	                Ex: 0 - Days After Invoice 
	                Invoice Date        = 10/15/2019 + 10 DaysDue 
	                balance due date    =  10/25/2019

	                Ex. 1 - Days After Built
	                Built Date          = 10/15/2019  + 10 DaysDue 
	                balance due date    = 10/25/2019

	                Ex. 2 - Days After Placed
	                Create Date         = 10/15/2019  + 10 DaysDue 
	                balance due date    = 10/25/2019

	                Ex. 3 - Day of the Month After Built (DaysDue = 10)
	                Built Date          = 10/15/19
	                balance due date    = 11/10/19

	                Ex. 4 - Day of the Month (DaysDue = 10)
	                Invoice Date        = 10/15/19 
	                balance due date    = 11/10/19

	                Ex. 5 - Day of the Month After Invoiced (DaysDue = 10)
	                Invoice Date        = 10/15/19 
	                balance due date    = 11/10/19

  
                  Sample Use:
                    EXEC [dbo].[Order.Action.ComputeBalanceDueDate] @BID=1, @OrderID=2452;

				  

				  Date created:	 10/17/19
				  Date modified: 10/21/19
                *********************************************************/

                CREATE OR ALTER   PROCEDURE [dbo].[Order.Action.ComputeBalanceDueDate]
                (
                --DECLARE
                    @BID            SMALLINT	--= 1
                  , @OrderID        INT			--= 1049
                )
                AS
                BEGIN
	
	                DECLARE @PaymentDueBasedOnType INT;
	                DECLARE @OrderStatusID INT;
	                DECLARE @BalanceDueDate DATE = null;
	                DECLARE @InvoicedDate DATE;
	                DECLARE @DaysDue INT;

	                /* When the order is saved, delete Balance Due Key Date if it is present and should be NULL */
	                IF(EXISTS(SELECT 1 FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = 32))
	                BEGIN
		                DELETE FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = 32 --Balance Due
	                END

	                /************ Get PaymentDueBasedOnType and OrderStatusID ******/
	                SELECT 
		                @PaymentDueBasedOnType = APT.PaymentDueBasedOnType,
		                @OrderStatusID = O.OrderStatusID
	                FROM
		                [Order.Data] O 
		                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
		                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
	                WHERE O.BID = @BID AND O.ID = @OrderID

	                /************* 0: Days After Invoice **************************/
	                IF(@PaymentDueBasedOnType = 0 AND @OrderStatusID = 25) /* Return NULL if the Order Status != Invoiced*/
	                BEGIN
		                WITH DaysAfterInvoice(InvoicedDate,LocationTimezone,DaysDue)
		                AS (
			                SELECT 
				                InvoicedDate = (SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 8)
				                ,LocationTimezone = Z.Name
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
			                WHERE O.BID = @BID
				                AND O.ID = @OrderID
				                AND EOS.TransactionType = 2 --Order
				                AND EOS.ID = @OrderStatusID
			                )
		                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Return */
		                SELECT
			                @BalanceDueDate = DATEADD(DAY, DaysDue, CONVERT(DATETIME,InvoicedDate) AT TIME ZONE LocationTimezone)
		                FROM DaysAfterInvoice;
	                END

	                /************* 1: Days After Built **************************/
	                ELSE IF(@PaymentDueBasedOnType = 1 AND (@OrderStatusID = 24 OR @OrderStatusID = 25))
	                BEGIN
		                WITH DaysAfterBuilt(BuiltDate,LocationTimezone,DaysDue)
		                AS (
			                SELECT 
				                BuiltDate = (SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 6)
				                ,LocationTimezone = Z.Name
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
			                WHERE O.BID = @BID
				                AND O.ID = @OrderID
				                AND EOS.TransactionType = 2 --Order
				                AND EOS.ID = @OrderStatusID
			                )
		                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Return */
		                SELECT
			                @BalanceDueDate = DATEADD(DAY, DaysDue, CONVERT(DATETIME,BuiltDate) AT TIME ZONE LocationTimezone)
		                FROM DaysAfterBuilt
	                END

	                /************* 2: Days After Placed **************************/
					ELSE IF(@PaymentDueBasedOnType = 2 AND NOT(@OrderStatusID = 26 OR @OrderStatusID = 29))
	                BEGIN
		                WITH DaysAfterPlaced(CreatedDate,LocationTimezone,DaysDue)
		                AS (
			                SELECT 
			                CreatedDate = (SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 1)
			                ,LocationTimezone = Z.Name
			                ,APT.DaysDue
		                FROM
			                [Order.Data] O 
			                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
			                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
			                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
			                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
			                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
			                WHERE O.BID = @BID 
				                AND O.ID = @OrderID
				                AND EOS.TransactionType = 2 --Order
				                AND EOS.ID IN (@OrderStatusID)	
			                )
		                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Return */
		                SELECT
			                @BalanceDueDate = DATEADD(DAY, DaysDue, CONVERT(DATETIME,CreatedDate) AT TIME ZONE LocationTimezone)
		                FROM DaysAfterPlaced
	                END

	                /************* 3: Day of the Month After Built **************************/
	                ELSE IF(@PaymentDueBasedOnType = 3 AND @OrderStatusID = 25)
	                BEGIN
		                WITH DayOfTheMonthAfterBuilt(InvoicedDate,DaysDue)
		                AS (
			
			                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Return */
			                SELECT 
				                InvoicedDate = CONVERT(DATETIME,(SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 6)) AT TIME ZONE Z.Name
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
				                WHERE O.BID = @BID 
					                AND O.ID = @OrderID
					                AND EOS.TransactionType = 2 --Order
					                AND EOS.ID = @OrderStatusID	
				                )			
			                SELECT
				                @BalanceDueDate = DATEFROMPARTS(YEAR(InvoicedDate), MONTH(InvoicedDate), DaysDue),
				                @InvoicedDate = InvoicedDate,
				                @DaysDue = DaysDue
			                FROM DayOfTheMonthAfterBuilt

			                IF DATEDIFF(DAY, @InvoicedDate, @BalanceDueDate) <= 0
			                BEGIN
				                SET @BalanceDueDate = DATEADD(month, 1, @BalanceDueDate)
			                END

	                END

	                /************* 4: Day of the Month **************************/
	                ELSE IF(@PaymentDueBasedOnType = 4 AND @OrderStatusID = 25)
	                BEGIN
		                WITH DayOfTheMonth(InvoicedDate,DaysDue)
		                AS (
			                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue */
			                SELECT 
				                InvoicedDate = CONVERT(DATETIME,(SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 8)) AT TIME ZONE Z.Name
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
			                WHERE O.BID = @BID
				                AND O.ID = @OrderID
				                AND EOS.TransactionType = 2 --Order
				                AND EOS.ID = @OrderStatusID
			                )
		                SELECT
			                @BalanceDueDate = DATEFROMPARTS(YEAR(InvoicedDate), MONTH(InvoicedDate), DaysDue),
			                @InvoicedDate = InvoicedDate,
			                @DaysDue = DaysDue
		                FROM DayOfTheMonth

		                IF DATEDIFF(DAY, @InvoicedDate, @BalanceDueDate) <= 0
		                BEGIN
			                SET @BalanceDueDate = DATEADD(month, 1, @BalanceDueDate)
		                END
	                END

	                /************* 5: Day of the Month After Invoiced **************************/
	                ELSE IF(@PaymentDueBasedOnType = 5 AND @OrderStatusID = 25)
	                BEGIN
		                WITH DayOfTheMonthAfterInvoiced(InvoicedDate,DaysDue)
		                AS (
			                /* Convert to the Order's Location's TimeZone and take the Date Only (no time) THEN Add The DaysDue and Add 1 Month */
			                SELECT 
				                InvoicedDate = DATEADD(MONTH, 1, CONVERT(DATETIME,(SELECT TOP 1 KeyDate FROM [Order.KeyDate] KD WHERE KD.OrderID = O.ID AND KD.KeyDateType = 8)) AT TIME ZONE Z.Name)
				                ,APT.DaysDue
			                FROM
				                [Order.Data] O 
				                LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
				                LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
				                LEFT JOIN [enum.Order.OrderStatus] EOS ON EOS.ID = O.OrderStatusID
				                LEFT JOIN [dbo].[Location.Data] L ON O.LocationID = L.ID
				                LEFT JOIN [dbo].[enum.TimeZone] Z ON L.TimeZoneID = Z.ID
				                WHERE O.BID = @BID 
					                AND O.ID = @OrderID
					                AND EOS.TransactionType = 2 --Order
					                AND EOS.ID = @OrderStatusID	
				                )
			                SELECT
				                @BalanceDueDate = DATEFROMPARTS(YEAR(InvoicedDate), MONTH(InvoicedDate), DaysDue),
				                @InvoicedDate = InvoicedDate,
				                @DaysDue = DaysDue
			                FROM DayOfTheMonthAfterInvoiced

			                IF DATEDIFF(DAY, @InvoicedDate, @BalanceDueDate) <= 0
			                BEGIN
				                SET @BalanceDueDate = DATEADD(month, 1, @BalanceDueDate)
			                END
	                END


	                SELECT BalanceDueDate = @BalanceDueDate

                END

GO
            ");

            //Order.Action.ComputeEarlyPaymentDate
            migrationBuilder.Sql(@"
/*
Name:
	[Order.Action.ComputeEarlyPaymentDate]( @BID smallint, @OrderID int)
  
Description:
	This function computes early payment date of an order

Rules:
	** Delete any existing if it is there.

	** If the EarlyPaymentPercent or EarlyPaymentDays is NULL, then do not create an early payment keydate.

	** The Early Payment Date is (Order.InvoicedDate + EarlyPaymentDays).

	** If the Early Payment Date is past the current UTC DateTime, then do not save the key date.  

	** Save the Early Payment Date in UTC time.

Story:
	** https://corebridge.atlassian.net/browse/END-10014

Created Date:
	11/4/19

Modified Date:
	11/5/19

*/
CREATE OR ALTER   PROC [dbo].[Order.Action.ComputeEarlyPaymentDate]
    (
    --DECLARE
        @BID            SMALLINT	--= 1
      , @OrderID        INT			--= 1049
    )
AS
BEGIN

	DECLARE @EarlyPaymentDate DATE = null;
	DECLARE @EarlyPaymentDateEnum INT = 34;
	DECLARE @EarlyPaymentDays SMALLINT;
	DECLARE @EarlyPaymentPercent DECIMAL;
	DECLARE @OrderStatusID INT;
	DECLARE @KeyDate DATE = null;

	/* When the order is saved, delete Early Payment Key Date if it is present and should be NULL */
	IF(EXISTS(SELECT 1 FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = @EarlyPaymentDateEnum))
	BEGIN
		DELETE FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = @EarlyPaymentDateEnum; --Early Payment Date
	END

	/************ Get EarlyPaymentDays, EarlyPaymentPercent and OrderStatusID ******/
	SELECT 
		@EarlyPaymentDays = APT.EarlyPaymentDays,
		@EarlyPaymentPercent = APT.EarlyPaymentPercent,
		@OrderStatusID = O.OrderStatusID
	FROM
		[Order.Data] O 
		LEFT JOIN [Company.Data] C ON O.CompanyID = C.ID
		LEFT JOIN [Accounting.Payment.Term] APT ON APT.BID = O.BID AND C.PaymentTermID = APT.ID
	WHERE O.BID = @BID AND O.ID = @OrderID

	/***** If the EarlyPaymentPercent or EarlyPaymentDays is NULL, then do not create an early payment keydate. ******/
	IF((@EarlyPaymentDays IS NULL) OR (@EarlyPaymentPercent IS NULL))
		SET @EarlyPaymentDate = NULL;

	/***** The Early Payment Date is (Order.InvoicedDate + EarlyPaymentDays). *****/
	SELECT TOP 1 @KeyDate = KeyDate FROM [Order.KeyDate] WHERE OrderID = @OrderID AND KeyDateType = 8;

	IF(@KeyDate IS NULL)
		SET @EarlyPaymentDate = @KeyDate;
	ELSE
		SET @EarlyPaymentDate = DATEADD(DAY, @EarlyPaymentDays, @KeyDate);

	/*** If the Early Payment Date is past the current UTC DateTime, then do not save the key date. ***/
	IF(CONVERT(DATE, @EarlyPaymentDate) < CONVERT(DATE,GETUTCDATE()))
		SET @EarlyPaymentDate = NULL;

	-- RETURN 
	SELECT EarlyPaymentDate = @EarlyPaymentDate
END
GO
            ");

            //Order.Action.RemoveContactRole
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.RemoveContactRole]( @BID smallint, @OrderID int, @RoleID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255) )
--
-- Description: This function remove a order contact role
--
-- Sample Use:   EXEC dbo.[Order.Action.RemoveContactRole] @BID=1, @OrderID=1, @contactRoleType=1, @contactID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.RemoveContactRole]
-- DECLARE 
          @BID             SMALLINT   -- = 1
        , @OrderID         INT        -- = 2
        , @RoleID          INT           = NULL
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1
        , @ContactName     VARCHAR(255)  = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Contact Role exists
    IF @RoleID IS NOT NULL
    BEGIN
        SELECT @ID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
    END
    ELSE IF @ContactID IS NULL 
    BEGIN
        IF ISNULL(LTRIM(@ContactName), '') != ''
        BEGIN
            SELECT @ID = ID
            FROM [Order.Contact.Role]
            WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType AND ContactName =  @ContactName
        END
    END
    ELSE 
    BEGIN
        SELECT @ID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType AND ContactID =  @ContactID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order contact role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Delete record is exists
    DELETE FROM OCR
    FROM   [Order.Contact.Role] OCR
    WHERE  BID = @BID AND ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Order.Action.RemoveEmployeeRole
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.RemoveEmployeeRole]( @BID smallint, @OrderID int, @RoleID int, @EmployeeRoleID smallint, @EmployeeID int )
--
-- Description: This function remove a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.RemoveEmployeeRole] @BID=1, @OrderID=1, @RoleID=NULL, @EmployeeRoleID=1, @EmployeeID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.RemoveEmployeeRole]
-- DECLARE 
          @BID             SMALLINT  -- = 1
        , @OrderID         INT       -- = 2
        , @RoleID          INT       = NULL
        , @EmployeeRoleID  SMALLINT  -- = 1
        , @EmployeeID      INT       -- = 1

        , @Result          INT       = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    IF @RoleID IS NOT NULL
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @RoleID
    END
    ELSE
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @EmployeeRoleID AND EmployeeID =  @EmployeeID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Delete record is exists
    DELETE FROM OER
    FROM   [Order.Employee.Role] OER
    WHERE  BID = @BID AND ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Order.Action.RemoveKeyDate
            migrationBuilder.Sql(@"
/*********************************************************
  Name:
    [Order.Action.RemoveKeyDate]( @BID smallint, @OrderID int, @OrderItemID int = null, @DestinationID int = null )
  
  Description:
    This function removes a key date of an order
  
  Sample Use:
    EXEC [dbo].[Order.Action.RemoveKeyDate] @BID=1, @OrderID=1, @OrderItemID=1, @DestinationID=1, @KeyDateType=1
*********************************************************/
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.RemoveKeyDate]
(
-- DECLARE 
    @BID            SMALLINT --= 1
  , @OrderID        INT     --= 2
  , @OrderItemID	INT		= NULL
  , @DestinationID	INT		= NULL
  , @KeyDateType    TINYINT	--= 1

  , @Result         INT     = NULL  OUTPUT
)
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF (@OrderItemID IS NULL AND @DestinationID IS NULL)
	BEGIN
		DELETE FROM KD
		FROM   [Order.KeyDate] KD
		WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType
			   AND OrderItemID IS NULL AND DestinationID IS NULL
	END
	ELSE IF (@OrderItemID IS NOT NULL AND @DestinationID IS NULL)
	BEGIN
		DELETE FROM KD
		FROM   [Order.KeyDate] KD
		WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType
			   AND OrderItemID = @OrderItemID AND DestinationID IS NULL
	END
	ELSE IF (@OrderItemID IS NULL AND @DestinationID IS NOT NULL)
	BEGIN
		DELETE FROM KD
		FROM   [Order.KeyDate] KD
		WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType
			   AND OrderItemID IS NULL AND DestinationID = @DestinationID
	END
	ELSE
	BEGIN
		DELETE FROM KD
		FROM   [Order.KeyDate] KD
		WHERE  BID = @BID AND OrderID = @OrderID AND KeyDateType = @KeyDateType
			   AND OrderItemID = @OrderItemID AND DestinationID = @DestinationID
	END

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Order.Action.RemoveNote
            migrationBuilder.Sql(@"
/*********************************************************
  Name:
    [Order.Action.RemoveNote]( @BID smallint, @OrderID int, @OrderItemID int = null, @DestinationID int = null )
  
  Description:
    This function removes a note of an order
  
  Sample Use:
    EXEC [dbo].[Order.Action.RemoveNote] @BID=1, @OrderID=1, @OrderItemID=1, @DestinationID=1, @NoteType=1
*********************************************************/
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.RemoveNote]
(
-- DECLARE 
    @BID            SMALLINT --= 1
  , @OrderID        INT     --= 2
  , @OrderItemID	INT		= NULL
  , @DestinationID	INT		= NULL
  , @NoteType       TINYINT --= 1

  , @Result         INT     = NULL  OUTPUT
)
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF (@OrderItemID IS NULL AND @DestinationID IS NULL)
    BEGIN
        DELETE FROM N
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID IS NULL AND DestinationID IS NULL
    END
    ELSE IF (@OrderItemID IS NOT NULL AND @DestinationID IS NULL)
    BEGIN
        DELETE FROM N
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID = @OrderItemID AND DestinationID IS NULL
    END
    ELSE IF (@OrderItemID IS NULL AND @DestinationID IS NOT NULL)
    BEGIN
        DELETE FROM N
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID IS NULL AND DestinationID = @DestinationID
    END
    ELSE
    BEGIN
        DELETE FROM N
        FROM   [Order.Note] N
        WHERE  BID = @BID AND OrderID = @OrderID AND NoteType = @NoteType
               AND OrderItemID = @OrderItemID AND DestinationID = @DestinationID
    END

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Order.Action.UpdateContactRole
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: 
--
-- Description: This function updates a order contact role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateContactRole] @BID=1, @OrderID=1, @contactRoleType=1, @contactID=1
-- ========================================================
-- ========================================================
/* 
     SPROC: [Order.Action.UpdateContactRole] @BID smallint, @OrderID int, @RoleID int, @ContactRoleType tinyint, @ContactID int, @ContactName varchar(255) 
     Description: This function update a order contact role

     Sample Use:   

        SELECT * FROM [Order.Contact.Role] WHERE BID =1 AND OrderID = 1100
        EXEC dbo.[Order.Action.UpdateContactRole] @BID=1, @OrderID=1100, @contactRoleType=2, @contactID=1001
        SELECT * FROM [Order.Contact.Role] WHERE BID =1 AND OrderID = 1100

*/
-- ========================================================
CREATE OR ALTER   PROCEDURE [dbo].[Order.Action.UpdateContactRole]
-- DECLARE 
          @BID             SMALLINT   -- = 1
        , @OrderID         INT        -- = 2
        , @ContactRoleType TINYINT    -- = 1
        , @ContactID       INT        -- = 1
        , @RoleID          INT           = NULL 

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024) = NULL
          , @CompanyID INT
          , @ExistingID INT
          ;

    SELECT @CompanyID = CompanyID 
    FROM [Order.Data] O 
    WHERE O.BID = @BID AND O.ID = @OrderID
    ;

    -- Pull the Existing ID .... by either RoleType or RoleID
    IF (@RoleID IS NULL)
        SELECT TOP(1) @ExistingID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleType = @ContactRoleType

    ELSE
        SELECT TOP(1) @ExistingID = ID
        FROM [Order.Contact.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
    ;
 
    -- Check if the supplied information is valid
    IF (@ContactID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'Contact Not Specified.');

    ELSE IF (@CompanyID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'Invalid Order Specified. OrderID=',@OrderID,' not found or Company not Set; ');


    ELSE IF NOT EXISTS( SELECT * FROM [Company.Contact.Link] CL WHERE CL.BID = @BID AND CL.CompanyID = @CompanyID AND CL.ContactID = @ContactID )
        SET @Message = CONCAT('//' + @Message, 'Invalid Contact Specified or Contact is not associated with the Order Company.');

   ELSE IF (@ExistingID IS NULL) 
        SET @Message = CONCAT('//' + @Message, 'No Existing Role to Update for OrderID=',@OrderID,'; ');

    IF (@Message IS NOT NULL)
    BEGIN
        SET @Result = 0;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    UPDATE OCR
    SET    RoleType = @ContactRoleType, IsAdHoc = 0, ContactID = @ContactID
            , ContactName = (SELECT ShortName FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
            , ContactCompany = (SELECT Name FROM [Company.Data] WHERE BID = @BID AND ID = @CompanyID)
    FROM   [Order.Contact.Role] OCR 
    WHERE  OCR.BID = @BID AND OCR.ID = @ExistingID
    ;

    SET @Result = @@ROWCOUNT;
END
GO
            ");

            //Order.Action.UpdateEmployeeRole
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Order.Action.UpdateEmployeeRole]( @BID smallint, @OrderID int, @RoleID int, @EmployeeRoleID smallint, @EmployeeID int )
--
-- Description: This function updates a order employee role
--
-- Sample Use:   EXEC dbo.[Order.Action.UpdateEmployeeRole] @BID=1, @OrderID=1, @RoleID=NULL, @EmployeeRoleID=1, @EmployeeID=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Order.Action.UpdateEmployeeRole]
-- DECLARE 
          @BID             SMALLINT   -- = 1
        , @OrderID         INT        -- = 2
        , @EmployeeRoleID  SMALLINT   -- = 1
        , @EmployeeID      INT        -- = 1
		, @RoleID          INT           = NULL 

        , @Result          INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Order specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Order Specified. OrderID='+CONVERT(VARCHAR,@OrderID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    IF ISNULL(@EmployeeRoleID,-1) = 255
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to change this employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    DECLARE @ID INT = NULL;
    
    -- Check if the Employee Role exists
    IF (@RoleID IS NULL)
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND RoleID = @EmployeeRoleID
    END

    ELSE
    BEGIN
        SELECT @ID = ID
        FROM [Order.Employee.Role]
        WHERE BID = @BID AND OrderID = @OrderID AND ID = @RoleID
    END

    IF @ID IS NULL 
    BEGIN
        SELECT @Result = 0
             , @Message = 'Unable to locate order employee role.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    -- Check if the Employee specified is valid
    IF NOT EXISTS(SELECT * FROM [Employee.Data] WHERE BID = @BID AND ID = @EmployeeID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Employee Specified. EmployeeID='+CONVERT(VARCHAR,@EmployeeID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

    UPDATE OER
    SET    EmployeeID = @EmployeeID, EmployeeName = employee.ShortName, RoleID = @EmployeeRoleID
    FROM   [Order.Employee.Role] OER 
            LEFT JOIN [Employee.Data] employee ON employee.BID = @BID AND employee.ID = @EmployeeID
    WHERE  OER.BID = @BID AND OER.ID = @ID

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Order.Item.Action.ChangeStatus
            migrationBuilder.Sql(@"
/*********************************************************
  Name:
    [Order.Item.Action.ChangeStatus]( @BID smallint, @OrderItemID int, @TargetStatusID smallint)
  
  Description:
    This function changes an Order Item Status to the target Order Item Status
  
  Sample Use:
    EXEC [dbo].[Order.Item.Action.ChangeStatus] @BID=1, @OrderItemID=1049, @TargetStatusID=23
*********************************************************/
CREATE OR ALTER PROCEDURE [dbo].[Order.Item.Action.ChangeStatus]
(
    @BID			SMALLINT  --= 1
  , @OrderItemID	INT       --= 1049
  , @TargetStatusID	SMALLINT  --= 23
  
  , @Result			INT		  = NULL OUTPUT
)
AS
BEGIN
	DECLARE @Message VARCHAR(1024);

	-- Check if the OrderItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Item.Data] WHERE BID = @BID AND ID = @OrderItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid OrderItem Specified. OrderItemID='+CONVERT(VARCHAR,@OrderItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @OrderID INT = (SELECT [OrderID] FROM [Order.Item.Data] WHERE BID = @BID AND ID = @OrderItemID);

	-- Check if the OrderItemStatus specified is valid
    IF NOT EXISTS(SELECT * FROM [Order.Item.Status] WHERE BID = @BID AND ID = @TargetStatusID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid OrderItemStatus Specified. OrderItemStatusID='+CONVERT(VARCHAR,@TargetStatusID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @OrderStatusID INT = (SELECT OrderStatusID FROM [Order.Item.Status] WHERE BID = @BID AND ID = @TargetStatusID);

	IF (@OrderStatusID < (SELECT OrderStatusID FROM [Order.Data] WHERE BID = @BID AND ID = @OrderID))
	BEGIN
        SELECT @Result = 0
             , @Message = 'The new Line Item Status can not be for an Order Status that is before the current one.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
	END

	UPDATE	OID
	SET		[ItemStatusID] = @TargetStatusID,
			[OrderStatusID] = @OrderStatusID
	FROM	[Order.Item.Data] AS OID
	WHERE	[BID] = @BID AND [ID] = @OrderItemID
		
    SET @Result = @@ROWCOUNT;
    SELECT @Result AS Result;
END
GO
            ");

            //Order.Item.Status.Action.LinkSubStatus
            migrationBuilder.Sql(@"
-- ========================================================
                -- Name: [Order.Item.Status.Action.LinkSubStatus]
                --
                -- Description: This procedure links/unlinks the OrderStatus to the SubStatus
                --
                -- Sample Use:   EXEC dbo.[Order.Item.Status.Action.LinkSubStatus] @BID=1, @SubStatusID=1, @OrderStatusID=16, @IsLinked=1
                -- ========================================================
                CREATE OR ALTER PROCEDURE [dbo].[Order.Item.Status.Action.LinkSubStatus]
                --DECLARE 
                          @BID                  SMALLINT --= 1
                        , @SubStatusID          SMALLINT --= 2
                        , @OrderStatusID        SMALLINT --= 1
                        , @IsLinked             BIT     = 1
                        , @Result               INT     = NULL  OUTPUT
                AS
                BEGIN
                    DECLARE @Message VARCHAR(1024);

                    -- Check if the SubStatus specified is valid
                    IF NOT EXISTS(SELECT * FROM [Order.Item.SubStatus] WHERE BID = @BID and ID = @SubStatusID)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Invalid SubStatus Specified. SubStatusID='+CONVERT(VARCHAR(12),@SubStatusID)+' not found'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END;

                    -- Check if the OrderStatus specified is valid
                    IF NOT EXISTS(SELECT * FROM [Order.Item.Status] WHERE BID = @BID and ID = @OrderStatusID)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Invalid OrderStatus Specified. OrderStatusID='+CONVERT(VARCHAR(12),@OrderStatusID)+' not found'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END;

                    IF @IsLinked = 1
                    BEGIN
                        -- Add new entry to Order.Item.Status.SubStatusLink if link is not yet found
                        IF NOT EXISTS(SELECT * FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID and SubStatusID = @SubStatusID and StatusID = @OrderStatusID)
                        BEGIN
                            INSERT INTO [Order.Item.Status.SubStatusLink] (BID, SubStatusID, StatusID)
                            VALUES (@BID, @SubStatusID, @OrderStatusID)
                        END;

                    END
                    ELSE
                    BEGIN
                        -- Remove entry from Order.Item.Status.SubStatusLink
                        DELETE FROM [Order.Item.Status.SubStatusLink] WHERE BID = @BID and SubStatusID = @SubStatusID and StatusID = @OrderStatusID
                    END

                    SET @Result = 1;

                    SELECT @Result as Result;
                END
GO
            ");

            //Order.Item.Status.Action.Move
            migrationBuilder.Sql(@"
                /* 
                    This procedures moves an Item Status in sort before another Item Status.
                    The approach is to take the StatusIndex of the target ID and then increment all other
                    Item Statuses with a StatusIndex >= that.

                    Example Usage:
                        EXEC [Order.Item.Status.Action.Move] @BID=1, @UD=49, @TargetID=19               -- movebefore
		                EXEC [Order.Item.Status.Action.Move] @BID=1, @UD=49, @TargetID=19, @MoveAfter=1 -- moveafter
		                select * from  [Order.Item.Status]

                */
                CREATE OR ALTER PROCEDURE [dbo].[Order.Item.Status.Action.Move]
                -- DECLARE 
                          @BID   SMALLINT
                        , @ID    SMALLINT
                        , @TargetID smallint
		                , @MoveAfter BIT = 0

                        , @Result         INT     = NULL  OUTPUT
                AS
                BEGIN
                    DECLARE @TargetStatusIndex TINYINT
                          , @Message VARCHAR(1024)
	                      , @OrderStatusID tinyint
	                      , @TargetOrderStatusID tinyint
                          ;

                    -- Ignore if dropping on itself
                    IF (@ID = @TargetID)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Can''t drop an item on itself.'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END
                    ;

	                SELECT @TargetStatusIndex = StatusIndex + (CASE WHEN @MoveAfter=1 THEN 1 ELSE 0 END)
                    FROM [Order.Item.Status] 
                    WHERE BID = @BID
                      AND ID = @TargetID
                    ;

                    -- Make Sure we Have a Result
                    IF (@TargetStatusIndex IS NULL)
                    BEGIN
                        SELECT @Result = 0
                             , @Message = 'Specified TargetID not Found.'
                             ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END
                    ;

                    SELECT @OrderStatusID = OrderStatusID FROM [Order.Item.Status] where BID = @BID AND ID = @ID;
                    SELECT @TargetOrderStatusID = OrderStatusID FROM [Order.Item.Status] where BID = @BID AND ID = @TargetID;
                    IF (@OrderStatusID <> @TargetOrderStatusID)
                    BEGIN
                    SELECT @Result = 0
                            , @Message = 'Can''t move a status before or after status that does not have the same OrderStatusID.'
                            ;

                    THROW 50000, @Message, 1;
                    RETURN @Result;
                    END;

                    SET @Result = 0;

	                UPDATE [Order.Item.Status] 
                    SET StatusIndex = @TargetStatusIndex 
                    WHERE BID = @BID
                      AND ID = @ID;

                    SET @Result = @Result + @@ROWCOUNT;

	                WITH OrderItemStatus AS (
		                  SELECT t1.*, row_number() OVER (ORDER BY StatusIndex) AS seqnum
		                  FROM [Order.Item.Status]  t1 
                          WHERE @BID = BID AND ID != @ID
                            AND StatusIndex >= @TargetStatusIndex 
		                 )
	                    UPDATE OrderItemStatus
	                    SET StatusIndex = @TargetStatusIndex + seqnum;

                    SET @Result = @Result + @@ROWCOUNT;

                    SELECT @Result as Result;
                END;
GO
            ");

            //Order.Item.Status.Action.SetDefault
            migrationBuilder.Sql(@"
                -- ========================================================
                -- Name: [Order.Item.Status.Action.SetDefault]
                --
                -- Description: This procedure sets the IsDefault in Order.Item.Status table
                --
                -- Sample Use: EXEC dbo.[Order.Item.Status.Action.SetDefault] @BID=1, @ID=65, @OrderStatusID=21, @TransactionType=1
                -- ========================================================
                CREATE OR ALTER   PROCEDURE [dbo].[Order.Item.Status.Action.SetDefault]
                -- DECLARE 
                          @BID						SMALLINT
                        , @ID		                SMALLINT
                        , @OrderStatusID			SMALLINT
		                , @TransactionType			TINYINT
                        , @IsDefault				BIT     = 1
                        , @Result					INT     = NULL  OUTPUT
                AS
                BEGIN
                    DECLARE @Message VARCHAR(1024);

                    -- Check if the option specified is valid
                    IF NOT EXISTS(SELECT * FROM [Order.Item.Status] WHERE ID = @ID)
                    BEGIN
                        SELECT @Result = 0
                                , @Message = 'The Order.Item.Status is not found.'
                                ;

                        THROW 50000, @Message, 1;
                        RETURN @Result;
                    END;

                    -- Now update it
                    UPDATE O
                    SET IsDefault = 0
                    FROM [Order.Item.Status] O
                    WHERE BID = @BID and IsDefault = 1 AND ID != @ID AND OrderStatusID = @OrderStatusID AND TransactionType = @TransactionType 

                    UPDATE O
                    SET IsDefault = 1
                    FROM [Order.Item.Status] O
                    WHERE  BID = @BID and ID = @ID


                    SET @Result = @@ROWCOUNT;

                    SELECT @Result as Result;
                END
            
GO
            ");

            //Origin.Action.SetParent
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Origin.Action.SetParent]
--
-- Description: This procedure sets the referenced Origin's parent ID
--
-- Sample Use:   EXEC dbo.[Origin.Action.SetParent] @BID=1, @OriginID=1, @ParentID=NULL
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Origin.Action.SetParent]
-- DECLARE 
        @BID            SMALLINT -- = 1
        , @OriginID       INT     -- = 2

        , @ParentID       INT     = NULL

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @OriginID)
    BEGIN
        SELECT @Result = 0
            , @Message = 'Invalid Origin Specified. OriginID='+CONVERT(VARCHAR(12),@OriginID)+' not found'
            ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update it
    UPDATE O
    SET   
        ParentID = @ParentID
    , ModifiedDT = GetUTCDate()
    FROM [CRM.Origin] O
    WHERE BID = @BID and ID = @OriginID
    AND ( (COALESCE(ParentID,~@ParentID) != @ParentID) OR (@ParentID IS NULL AND ParentID IS NOT NULL) )

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

            //Part.Labor.Category.Action.LinkLabor
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Labor.Category.Action.LinkLabor]
--
-- Description: This procedure links/unlinks the LaborData to the LaborCategory
--
-- Sample Use:   EXEC dbo.[Part.Labor.Category.Action.LinkLabor] @BID=1, @LaborCategoryID=1, @LaborDataID=1, @IsLinked=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Part.Labor.Category.Action.LinkLabor]
--DECLARE 
          @BID            SMALLINT  --= 1
        , @LaborCategoryID     SMALLINT --= 2
		, @LaborDataID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the LaborCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Labor.Category] WHERE BID = @BID and ID = @LaborCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid LaborCategory Specified. LaborCategoryID='+CONVERT(VARCHAR(12),@LaborCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the LaborData specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Labor.Data] WHERE BID = @BID and ID = @LaborDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid LaborData Specified. LaborDataID='+CONVERT(VARCHAR(12),@LaborDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.LaborDataLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Part.Labor.CategoryLink] WHERE BID = @BID and CategoryID = @LaborCategoryID and PartID = @LaborDataID)
		BEGIN
			INSERT INTO [Part.Labor.CategoryLink] (BID, CategoryID, PartID)
			VALUES (@BID, @LaborCategoryID, @LaborDataID)
		END;

	END
	ELSE
	BEGIN
		-- Remove entry from Part.Labor.CategoryLink
		DELETE FROM [Part.Labor.CategoryLink] WHERE BID = @BID and CategoryID = @LaborCategoryID and PartID = @LaborDataID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
GO
            ");

            //Part.Machine.Category.Action.LinkMachine
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Machine.Category.Action.LinkMachine]
--
-- Description: This procedure links/unlinks the MachineData to the MachineCategory
--
-- Sample Use:   EXEC dbo.[Part.Machine.Category.Action.LinkMachine] @BID=1, @MachineCategoryID=1, @MachineDataID=1, @IsLinked=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Part.Machine.Category.Action.LinkMachine]
--DECLARE 
          @BID                  SMALLINT  --= 1
        , @MachineCategoryID    SMALLINT --= 2
		, @MachineDataID        SMALLINT --= 1
        , @IsLinked             BIT     = 1
        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the MachineCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Machine.Category] WHERE BID = @BID and ID = @MachineCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MachineCategory Specified. MachineCategoryID='+CONVERT(VARCHAR(12),@MachineCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the MachineData specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Machine.Data] WHERE BID = @BID and ID = @MachineDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MachineData Specified. MachineDataID='+CONVERT(VARCHAR(12),@MachineDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.MachineDataLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Part.Machine.CategoryLink] WHERE BID = @BID and CategoryID = @MachineCategoryID and PartID = @MachineDataID)
		BEGIN
			INSERT INTO [Part.Machine.CategoryLink] (BID, CategoryID, PartID)
			VALUES (@BID, @MachineCategoryID, @MachineDataID)
		END;

	END
	ELSE
	BEGIN
		-- Remove entry from Part.Machine.CategoryLink
		DELETE FROM [Part.Machine.CategoryLink] WHERE BID = @BID and CategoryID = @MachineCategoryID and PartID = @MachineDataID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
GO
            ");

            //Part.Material.Category.Action.LinkMaterial
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Material.Category.Action.LinkMaterial]
--
-- Description: This procedure links/unlinks the MaterialData to the MaterialCategory
--
-- Sample Use:   EXEC dbo.[Part.Material.Category.Action.LinkMaterial] @BID=1, @MaterialCategoryID=1, @MaterialDataID=1, @IsLinked=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Part.Material.Category.Action.LinkMaterial]
--DECLARE 
          @BID                   SMALLINT --= 1
        , @MaterialCategoryID    SMALLINT --= 2
		, @MaterialDataID        SMALLINT --= 1
        , @IsLinked             BIT     = 1
        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the MaterialCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Material.Category] WHERE BID = @BID and ID = @MaterialCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MaterialCategory Specified. MaterialCategoryID='+CONVERT(VARCHAR(12),@MaterialCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the MaterialData specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Material.Data] WHERE BID = @BID and ID = @MaterialDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid MaterialData Specified. MaterialDataID='+CONVERT(VARCHAR(12),@MaterialDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Part.Material.CategoryLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Part.Material.CategoryLink] WHERE BID = @BID and CategoryID = @MaterialCategoryID and PartID = @MaterialDataID)
		BEGIN
			INSERT INTO [Part.Material.CategoryLink] (BID, CategoryID, PartID)
			VALUES (@BID, @MaterialCategoryID, @MaterialDataID)
		END;

	END
	ELSE
	BEGIN
		-- Remove entry from Part.Material.CategoryLink
		DELETE FROM [Part.Material.CategoryLink] WHERE BID = @BID and CategoryID = @MaterialCategoryID and PartID = @MaterialDataID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
GO
            ");

            //Part.Subassembly.Category.Action.CanDelete
            migrationBuilder.Sql(@"
/* 
========================================================
    Name: [Part.Subassembly.Category.Action.CanDelete]

    Description: This procedure checks if the SubassemblyCategory is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Part.Subassembly.Category.Action.CanDelete] @BID=1, @ID=1, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE OR ALTER   PROCEDURE [dbo].[Part.Subassembly.Category.Action.CanDelete]
          @BID                  SMALLINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
    Checks if the AssemblyCategory can be deleted. The boolean response is returned in the body.  A AssemblyCategory can be deleted if
    - there are no assemblies linked to it or if the force option is specified to deletes the links.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Part.Subassembly.Category] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Assembly Category Specified Does not Exist. AssemblyCategoryID=', @ID)

    -- there are no assemblies linked to it or if the force option is specified to deletes the links.
    ELSE IF  EXISTS( SELECT * FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID AND CategoryID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Assembly Category is linked to a Assembly Data. AssemblyCategoryID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END
GO
            ");

            //Part.Subassembly.Category.Action.LinkSubassembly
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Subassembly.Category.Action.LinkSubassembly]
--
-- Description: This procedure links/unlinks the SubassemblyData to the SubassemblyCategory
--
-- Sample Use:   EXEC dbo.[Part.Subassembly.Category.Action.LinkSubassembly] @BID=1, @SubassemblyCategoryID=1, @SubassemblyDataID=1, @IsLinked=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Part.Subassembly.Category.Action.LinkSubassembly]
--DECLARE 
          @BID                  SMALLINT  --= 1
        , @SubassemblyCategoryID    SMALLINT --= 2
		, @SubassemblyDataID        INT --= 1
        , @IsLinked             BIT     = 1
        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the SubassemblyCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Subassembly.Category] WHERE BID = @BID and ID = @SubassemblyCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid SubassemblyCategory Specified. SubassemblyCategoryID='+CONVERT(VARCHAR(12),@SubassemblyCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the SubassemblyData specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Subassembly.Data] WHERE BID = @BID and ID = @SubassemblyDataID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid SubassemblyData Specified. SubassemblyDataID='+CONVERT(VARCHAR(12),@SubassemblyDataID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.SubassemblyDataLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID and CategoryID = @SubassemblyCategoryID and PartID = @SubassemblyDataID)
		BEGIN
			INSERT INTO [Part.Subassembly.CategoryLink] (BID, CategoryID, PartID)
			VALUES (@BID, @SubassemblyCategoryID, @SubassemblyDataID)
		END;

	END
	ELSE
	BEGIN
		-- Remove entry from Part.Subassembly.CategoryLink
		DELETE FROM [Part.Subassembly.CategoryLink] WHERE BID = @BID and CategoryID = @SubassemblyCategoryID and PartID = @SubassemblyDataID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END
GO
            ");

            //Part.Subassembly.Category.Action.SetActive
            migrationBuilder.Sql(@"
-- ========================================================
-- Name: [Part.Subassembly.Category.Action.SetActive]
--
-- Description: This procedure sets the SubassemblyCategory to active or inactive
--
-- Sample Use:   EXEC dbo.[Part.Subassembly.Category.Action.SetActive] @BID=1, @SubassemblyCategoryID=1, @IsActive=1
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Part.Subassembly.Category.Action.SetActive]
-- DECLARE 
          @BID                  SMALLINT  -- = 1
        , @SubassemblyCategoryID    SMALLINT -- = 2

        , @IsActive             BIT     = 1

        , @Result               INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the SubassemblyCategory specified is valid
    IF NOT EXISTS(SELECT * FROM [Part.Subassembly.Category] WHERE BID = @BID and ID = @SubassemblyCategoryID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid SubassemblyCategory Specified. SubassemblyCategoryID='+CONVERT(VARCHAR(12),@SubassemblyCategoryID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Part.Subassembly.Category] L
    WHERE BID = @BID and ID = @SubassemblyCategoryID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
