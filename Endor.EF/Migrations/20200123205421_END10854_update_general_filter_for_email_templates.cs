﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10854_update_general_filter_for_email_templates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [dbo].[System.List.Filter]
                   SET 
                      [Criteria] = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>1000</SearchValue><Field>AppliesToClassTypeID</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>'
                 WHERE [Name] = 'General' and [TargetClassTypeID] = 14220
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
