﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class ClearAssemblies_CBELLogging : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }

        public override bool ClearAllAssemblies()
        {
            return true;
        }
    }
}
