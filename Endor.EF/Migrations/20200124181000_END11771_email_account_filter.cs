﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11771_email_account_filter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                IF NOT EXISTS(SELECT 1 FROM [dbo].[System.List.Filter] WHERE [TargetClassTypeID]=1023 AND [Name]='All')
                BEGIN
                    INSERT INTO [dbo].[System.List.Filter]
                    ([ID]
                    ,[IsActive]
                    ,[Name]
                    ,[TargetClassTypeID]
                    ,[SortIndex])
                    VALUES
                    ((SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter])
                    ,1
                    ,'All'
                    ,1023
                    ,0);
                END            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
