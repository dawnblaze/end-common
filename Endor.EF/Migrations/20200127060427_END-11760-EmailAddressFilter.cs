﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11760EmailAddressFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"    
                delete from [System.List.Filter.Criteria] where 
                    TargetClassTypeID=1023 and 
                    Field in ('TypeText','StatusTypeText','DisplayName','EmployeeID','TeamIDs');

                insert into [System.List.Filter.Criteria] 
                (
                    TargetClassTypeID,
                    Name,
                    Label,
                    Field,
                    IsHidden,
                    DataType,
                    InputType,
                    AllowMultiple,
                    ListValues,
                    ListValuesEndpoint,
                    IsLimitToList,
                    SortIndex,
                    ID
                )
                values
                (
                    1023,
                    'Type',
                    'Type',
                    'TypeText',
                    0,
                    1,
                    12,
                    0,
                    'Personal,Teams',
                    null,
                    0,
                    1,
                    (select max(ID)+1 from [System.List.Filter.Criteria])
                ),
                (
                    1023,
                    'Status',
                    'Status',
                    'StatusTypeText',
                    0,
                    1,
                    12,
                    0,
                    'Authorized,Pending,Failed,Expired',
                    null,
                    0,
                    2,
                    (select max(ID)+2 from [System.List.Filter.Criteria])
                );

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
