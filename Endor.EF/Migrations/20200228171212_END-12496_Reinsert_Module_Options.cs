﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12496_Reinsert_Module_Options : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [System.Option.Definition]
([ID], [Name], [Label], [DataType], [CategoryID], [DefaultValue], [IsHidden]) 
VALUES
(200, 'License.Module.Sales.Enabled'     , 'Sales Module Enabled'     , 3, 15102, '0', 0),
(201, 'License.Module.Management.Enabled', 'Management Module Enabled', 3, 15102, '0', 0),
(202, 'License.Module.Ecommerce.Enabled' , 'Ecommerce Module Enabled' , 3, 15102, '0', 0),
(203, 'License.Module.Accounting.Enabled', 'Accounting Module Enabled', 3, 15102, '0', 0),
(204, 'License.Module.Production.Enabled', 'Production Module Enabled', 3, 15102, '1', 0),
(205, 'License.Module.Purchasing.Enabled', 'Purchasing Module Enabled', 3, 15102, '0', 0),
(206, 'License.Module.MyProfile.Enabled' , 'MyProfile Module Enabled' , 3, 15102, '1', 1),
(207, 'License.Module.DevOps.Enabled'    , 'Dev Ops Module Enabled'   , 3, 15102, '1', 1)
            ");

            migrationBuilder.Sql(@"
DECLARE @OptionIDs OptionsArray;
INSERT INTO @OptionIDs
(OptionID, Value)
VALUES
(200, '1'),
(201, '1'),
(202, '1'),
(203, '1'),
(204, '1'),
(205, '1')

DECLARE @BusinessID SMALLINT

DECLARE business_cursor CURSOR FOR
SELECT BID
FROM   [Business.Data]
WHERE  BID > 0

OPEN business_cursor
FETCH NEXT FROM business_cursor INTO @BusinessID

WHILE @@FETCH_STATUS = 0  
BEGIN
    EXEC [Option.SaveValues] @Options_Array = @OptionIDs, @BID = @BusinessID
    FETCH NEXT FROM business_cursor INTO @BusinessID
END

CLOSE business_cursor
DEALLOCATE business_cursor
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [Option.Data] WHERE OptionID IN (200, 201, 202, 203, 204, 205, 206, 207)
            ");

            migrationBuilder.Sql(@"
DELETE FROM [System.Option.Definition] WHERE ID IN (200, 201, 202, 203, 204, 205, 206, 207)
            ");
        }
    }
}
