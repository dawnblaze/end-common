﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11637_Insert_SPROC_Util_ID_SetNextIDs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                -- ========================================================
                /*
                 Name: Util.ID.SetNextIDs( )

                 Description: This Function Resets all of the NextIDs for each ClassTypeIDs.
                    It does this by taking the largest of the existing NextID, 
                    the tables Max(ID), and the starting ID number.  It runs for ALL BIDs.

                 Sample Use:   

                      EXEC dbo.[Util.ID.SetNextIDs] 

                */
                -- ========================================================
                CREATE OR ALTER PROCEDURE [dbo].[Util.ID.SetNextIDs] 
                AS
                BEGIN
                    DROP TABLE IF EXISTS #MaxIDList;

                    CREATE TABLE #MaxIDList (
                                                BID smallint,
                                                ClassTypeID int,
                                                TableName varchar(255),
                                                IDDataType varchar(255),
                                                TableMaxID int,
                                                CurrentNextID int,
                                                MinimumNextID int,
                                                NewNextID int
                                            );

                    WITH ClassTypeIDList AS
                    (
                        SELECT TableName, Replace(Replace(ComputedFormula, '(', ''), ')', '') AS ClassTypeID 
                        FROM TablesAndColumns 
                        WHERE ColumnName = 'ClassTypeID'
                            AND ComputedFormula IS NOT NULL
                    )
                        INSERT INTO #MaxIDList (TableName, ClassTypeID, BID)
                            SELECT C.TableName, CONVERT(INT, C.ClassTypeID) as ClassTypeID, B.BID
                            FROM ClassTypeIDList C
                            JOIN [Business.Data] B on B.BID > 0
                            WHERE ISNUMERIC(C.ClassTypeID)=1
                                AND CONVERT(INT, C.ClassTypeID) > 0
                            ORDER BY BID, ClassTypeID
                    ;

                    -- We have some bad tables with duplicate ClassTypeIDs ... we have to manually remove those
                    DELETE FROM #MaxIDList
                    WHERE TableName IN ('Report.Menu', 'Accounting.Reconciliation.Item')
                    ;

                    -- now update the size of the ID field
                    UPDATE M
                    SET IDDataType = TC.ColumnType,
                        MinimumNextID = (CASE TC.ColumnType
                                            WHEN 'int' THEN 1000
                                            WHEN 'smallint' THEN 500
                                            WHEN 'tinyint' THEN 10
                                        END)
                    FROM #MaxIDList M
                    JOIN TablesAndColumns TC ON M.TableName = TC.TableName
                    WHERE TC.ColumnName = 'ID'
                    ;

                    -- now pull the current NextID value
                    UPDATE M
                    SET CurrentNextID = NID.NextID
                    FROM #MaxIDList M
                    JOIN [Util.NextID] NID ON M.BID = NID.BID AND M.ClassTypeID = NID.ClassTypeID 
                    ;

                    -- now fill in the current table MaxIDs
                    DECLARE @SQL NVARCHAR(MAX);

                    SELECT @SQL = CONCAT(@SQL+CHAR(10),
                                        'UPDATE #MaxIDList '
                                        ,'SET TableMaxID = (SELECT MAX(ID) FROM [', TableName, ']) ', CHAR(10)
                                        ,'WHERE BID = ',BID,' AND ClassTypeID = ',ClassTypeID,'; ') 
                    FROM #MaxIDList
                    WHERE IDDataType LIKE '%int'

                    -- Run the procedure to fill in the TableMaxID for every table
                    EXEC sp_executesql @stmt = @SQL;

                    -- Set the New ID
                    UPDATE #MaxIDList
                    SET NewNextID = (SELECT Max(v) 
                                    FROM (VALUES (CurrentNextID), (TableMaxID), (MinimumNextID) ) 
                                    AS value(v)) 
                    ;

                    -- Now Update those records that already exists
                    UPDATE NI
                    SET NextID = M.NewNextID
                    FROM [Util.NextID] NI
                    JOIN #MaxIDList M ON M.BID = NI.BID AND M.ClassTypeID = NI.ClassTypeID
                    WHERE CurrentNextID != NewNextID
                    ;

                    -- Now Insert those records that don't exist
                    INSERT INTO [Util.NextID]
                        SELECT BID, ClassTypeID, NewNextID
                        FROM #MaxIDList M 
                        WHERE CurrentNextID IS NULL 
                        AND NewNextID IS NOT NULL
                        ;

                    -- SELECT * FROM #MaxIDList ORDER BY BID, ClassTypeID
                    -- SELECT * FROM [Util.NextID] ORDER BY BID, ClassTypeID;

                    DROP TABLE IF EXISTS #MaxIDList;

                END
                GO

                EXEC dbo.[Util.ID.SetNextIDs];
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
