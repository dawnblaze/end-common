﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class RepairDestinationDataComputedColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DisableSystemVersioningIfEnabled("Order.Item.Component");

            migrationBuilder.Sql(@"
DROP INDEX IF EXISTS [IX_Order.Item.Component_Machine] ON [Order.Item.Component]
ALTER TABLE [dbo].[Order.Item.Component] DROP CONSTRAINT IF EXISTS [FK_Order.Item.Component_Part.Machine.Data];
ALTER TABLE [dbo].[Order.Item.Component] DROP COLUMN IF EXISTS MachineID;

DROP INDEX IF EXISTS [IX_Order.Item.Component_DestinationID] ON [Order.Item.Component]
DROP INDEX IF EXISTS [IX_Order.Item.Component_Destination] ON [Order.Item.Component]

ALTER TABLE [dbo].[Order.Item.Component] DROP CONSTRAINT IF EXISTS [FK_Order.Item.Component_Part.Destination.Data];
ALTER TABLE [dbo].[Order.Item.Component] DROP CONSTRAINT IF EXISTS [FK_Order.Item.Component_Destination.Data];
ALTER TABLE [dbo].[Order.Item.Component] DROP COLUMN IF EXISTS DestinationID;
            ");

            migrationBuilder.Sql(@"
ALTER TABLE [dbo].[Order.Item.Component] ADD DestinationID AS (CASE WHEN ComponentType=12080 THEN Cast(ComponentID as SMALLINT) ELSE NULL END) PERSISTED;
ALTER TABLE [dbo].[Order.Item.Component] ADD MachineID AS (CASE WHEN ComponentType=12030 THEN Cast(ComponentID as SMALLINT) ELSE NULL END) PERSISTED;
            ");

            migrationBuilder.Sql(@"
ALTER TABLE [Order.Item.Component]
ADD CONSTRAINT [FK_Order.Item.Component_Part.Destination.Data]
FOREIGN KEY ([BID], [DestinationID]) REFERENCES [Destination.Data]([BID], [ID]);
");

            migrationBuilder.Sql(@"
ALTER TABLE [Order.Item.Component]
ADD CONSTRAINT [FK_Order.Item.Component_Part.Machine.Data]
FOREIGN KEY ([BID], [MachineID]) REFERENCES [Part.Machine.Data]([BID], [ID]);
");


            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Component_Machine",
                table: "Order.Item.Component",
                columns: new[] { "BID", "MachineID" });

            migrationBuilder.CreateIndex(
                name: "IX_Order.Item.Component_Destination",
                table: "Order.Item.Component",
                columns: new[] { "BID", "DestinationID" });

            migrationBuilder.Sql("DROP TABLE IF EXISTS dbo.[Historic.Order.Item.Component]");
            migrationBuilder.EnableSystemVersioning("Order.Item.Component");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
