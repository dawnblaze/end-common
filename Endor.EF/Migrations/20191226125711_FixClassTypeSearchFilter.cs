﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Endor.EF.Migrations
{
    [DbContext(typeof(ApiContext))]
    [Migration("20191226125711_FixClassTypeSearchFilter")]
    public partial class FixClassTypeSearchFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter] 
   SET CRITERIA = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>3</SearchValue><Field>AssemblyType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>'
WHERE TARGETCLASSTYPEID = 12040 AND ID = 1056; 
UPDATE [System.List.Filter] 
   SET CRITERIA = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>0</SearchValue><Field>AssemblyType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>'
WHERE TARGETCLASSTYPEID = 12040 AND ID = 1047
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
