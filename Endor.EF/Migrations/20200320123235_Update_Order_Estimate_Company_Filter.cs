﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Update_Order_Estimate_Company_Filter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [dbo].[System.List.Filter.Criteria]
                   SET [DataType] = 32005,
                       [Field] = 'CompanyIDs',
	                   [InputType] = 17
                WHERE 
                (TargetClassTypeID = 10000 OR TargetClassTypeID = 10200) and Name like 'Company';
                UPDATE [dbo].[System.List.Filter.Criteria]
                   SET [DataType] = 32005,
                       [Field] = 'ContactRolesIDs',
	                   [InputType] = 18
                WHERE 
                (TargetClassTypeID = 10000 OR TargetClassTypeID = 10200) and Name like 'Contact';
                GO
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
