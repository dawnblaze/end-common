﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Remove_Unneeded_Element_Variables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE TABLE #VariableIDs 
(
  BID SMALLINT, 
  ID INT,
  PRIMARY KEY (BID, ID)
);
;
INSERT INTO #VariableIDs
SELECT DISTINCT BID, VariableID
FROM   [Part.Subassembly.Element]
WHERE  ElementType in (0, 7, 103, 121) AND VariableID is not null
;
;
UPDATE [Part.Subassembly.Element]
SET    VariableID = NULL
WHERE  ElementType in (0, 7, 103, 121) AND VariableID is not null
;
DELETE FROM [Order.Item.Component]
FROM [Order.Item.Component] V
     JOIN #VariableIDs T ON V.BID = T.BID AND V.AssemblyVariableID = T.ID
;
DELETE FROM [Part.Assembly.Table]
FROM [Part.Assembly.Table] V
     JOIN #VariableIDs T ON V.BID = T.BID AND (V.ColumnVariableID = T.ID OR V.RowVariableID = T.ID)
;
DELETE FROM [Part.Assembly.Variable.Formula]
FROM [Part.Assembly.Variable.Formula] V
     JOIN #VariableIDs T ON V.BID = T.BID AND V.VariableID = T.ID
;
DELETE FROM [Destination.Profile.Variable]
FROM [Destination.Profile.Variable] V
     JOIN #VariableIDs T ON V.BID = T.BID AND V.VariableID = T.ID
;
DELETE FROM [Part.Machine.Profile.Variable]
FROM [Part.Machine.Profile.Variable] V
     JOIN #VariableIDs T ON V.BID = T.BID AND V.VariableID = T.ID
;
DELETE FROM [Part.Subassembly.Variable]
FROM [Part.Subassembly.Variable] V
     JOIN #VariableIDs T ON V.BID = T.BID AND V.ID = T.ID

DROP TABLE #VariableIDs
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
