﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12377_ContactSimpleListNoParentFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"ALTER VIEW [Contact.SimpleList]
AS
SELECT
	  c.[BID]
	, c.[ID]
	, c.[ClassTypeID]
	, c.[ShortName] AS [DisplayName]
	, IsNull(l.[IsActive], 0) AS IsActive
	, c.[HasImage]
	, IsNull(l.[IsPrimary], 0) AS [IsDefault]
	, IsNull(l.[CompanyID], 1) as [ParentID]
FROM
	dbo.[Contact.Data] c
	LEFT JOIN dbo.[Company.Contact.Link] l ON c.[BID] = l.[BID] AND c.[ID] = l.[ContactID]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"ALTER   VIEW [dbo].[Contact.SimpleList]
AS
SELECT
	  c.[BID]
	, c.[ID]
	, c.[ClassTypeID]
	, c.[ShortName] AS [DisplayName]
	, l.[IsActive]
	, c.[HasImage]
	, l.[IsPrimary] AS [IsDefault]
	, l.[CompanyID] as [ParentID]
FROM
	dbo.[Contact.Data] c
	LEFT JOIN dbo.[Company.Contact.Link] l ON c.[BID] = l.[BID] AND c.[ID] = l.[ContactID]");
        }
    }
}
