﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12683_SecurityRights_for_DeveloperUserAccess : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO [System.Rights.UserAccessType.GroupLink]
        SELECT UAT.ID AS UserAccessType, R.IncludedRightsGroupID
        FROM [System.Rights.UserAccessType.GroupLink] R
        JOIN [enum.User.Access.Type] UAT ON UAT.ID > 49
        WHERE R.UserAccessType = 49
;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
