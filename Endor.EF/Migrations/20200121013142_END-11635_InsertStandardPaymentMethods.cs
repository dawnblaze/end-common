﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11635_InsertStandardPaymentMethods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DECLARE @BID SMALLINT = -1;

IF NOT EXISTS(SELECT * FROM [Accounting.GL.Account] WHERE BID = @BID AND ID = 6200)
	INSERT INTO [Accounting.GL.Account] ([BID], [ID], [ExportGLAccountType], [GLAccountType], [Name], [Number], [IsActive], [CanEdit] )
	VALUES(@BID, 6200, 60, 60, 'Bad Debt Expense', 6200, 1, 0)
	;

INSERT [Accounting.Payment.Method] ([BID], [ID], [DepositGLAccountID], [IsActive], [IsCustom], [IsIntegrated], [Name], [PaymentMethodType]) 
VALUES (@BID, 1, 1100, 1, 0, 0, N'Cash', 1)
     , (@BID, 2, 1100, 1, 0, 0, N'Check', 2)
     , (@BID, 4, 1100, 1, 0, 0, N'Wire Transfer (External)', 4)
     , (@BID, 5, 1100, 1, 0, 0, N'AmEx (External)', 6)
     , (@BID, 6, 1100, 1, 0, 0, N'MasterCard (External)', 7)
     , (@BID, 7, 1100, 1, 0, 0, N'Visa (External)', 8)
     , (@BID, 8, 1100, 1, 0, 0, N'Discover (External)', 9)
     , (@BID, 23, 1100, 0, 0, 1, N'ACH', 3)
     , (@BID, 26, 1100, 1, 0, 1, N'Credit Card', 5)
     , (@BID, 250, 2200, 1, 0, 0, N'Refundable Credit', 250)
     , (@BID, 251, 2200, 1, 0, 0, N'NonRefundable Credit', 251)
     , (@BID, 252, 6200, 1, 0, 0, N'Writeoff', 252)
;

EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName = 'Accounting.GL.Account';
EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName = 'Accounting.Payment.Method';");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
