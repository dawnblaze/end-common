﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11220DropColorColumnFromListFlatListDataIfItExists : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"ALTER TABLE [List.FlatList.Data] DROP COLUMN IF EXISTS Color;
                                   Go");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
