﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_11200_AddNewSystemSMTPConfigurationType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO [System.Email.SMTP.ConfigurationType] ([ID], [Name], [SMTPAddress], [SMTPPort], [SMTPSecurityType], [SMTPAuthenticateFirst]) 
                                   VALUES (4, N'Gmail via SMTP', N'smtp.gmail.com', 587, 2, 1)
                                         , (5, N'Office365 via SMTP', N'smtp.office365.com', 587, 2, 1)
                                   Go");

            migrationBuilder.Sql(@"UPDATE [System.Option.Category] 
                SET Name = 'Email Domains'
                  , Description = 'Email Domains'
                  , SearchTerms = 'email gmail outlook smtp internet google microsoft handler yahoo send domain pop3 imap'
                WHERE ID = 104
                GO    
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [System.Email.SMTP.ConfigurationType]
                      WHERE ID IN (4,5)
                GO    
            ");

            migrationBuilder.Sql(@"UPDATE [System.Option.Category] 
                SET Name = 'Email Handlers'
                  , Description = 'Email Handlers'
                  , SearchTerms = 'email gmail outlook smtp internet google microsoft handler yahoo send'
                WHERE ID = 104
                GO    
            ");
        }
    }
}
