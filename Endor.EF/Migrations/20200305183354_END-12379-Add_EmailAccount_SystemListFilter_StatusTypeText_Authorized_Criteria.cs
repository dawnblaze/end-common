﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12379Add_EmailAccount_SystemListFilter_StatusTypeText_Authorized_Criteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [dbo].[System.List.Filter]
   SET [Criteria] = '<ArrayOfListFilterItem>
  <ListFilterItem>
    <SearchValue>Authorized</SearchValue>
    <Field>StatusTypeText</Field>
    <IsHidden>false</IsHidden>
    <IsSystem>true</IsSystem>
  </ListFilterItem>
</ArrayOfListFilterItem>'
 WHERE [TargetClassTypeID] = 1023");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.Sql(@"UPDATE [dbo].[System.List.Filter]
   SET [Criteria] = NULL
 WHERE [TargetClassTypeID] = 1023");

        }
    }
}
