﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END1667_InsertSecurityRightsLinksToGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- Insert the Security Rights List (Starting Set)
INSERT INTO [enum.User.Right] (ID, Name) 
VALUES (1, 'AccessModuleManagement')  -- Right:Allows Basic Access to the Management Module. = 1
     , (2, 'AccessModuleAccounting')  -- Right:Allows Basic Access to the Accounting Module. = 2
     , (4, 'AccessModuleSales')  -- Right:Allows Basic Access to the Sales Module. = 4
     , (8, 'AccessModuleProduction')  -- Right:Allows Basic Access to the Production Module. = 8
     , (16, 'AccessModulePurchasing')  -- Right:Allows Basic Access to the Purchasing Module. = 16
     , (32, 'AccessModuleEcommerce')  -- Right:Allows Basic Access to the Ecommerce Module. = 32
     , (128, 'AccessModuleUser')  -- Right:Allows Basic Access to the User Module. = 128
     , (256, 'AccessModuleDevOps')  -- Right:Allows Basic Access to the DevOps Module. = 256
     , (301, 'AccessLocationSetup')  -- Right:Access Location Setup = 301
     , (302, 'CanCreateNewLocations')  -- Right:Can Create New Locations = 302
     , (303, 'CanEditLocations')  -- Right:Can Edit Locations = 303
     , (304, 'AccessEmployeeSetup')  -- Right:Access Employee Setup = 304
     , (305, 'CanManageEmployees')  -- Right:Can Manage Employees = 305
     , (306, 'AccessAdjustTimeCards')  -- Right:Access Adjust TimeCards = 306
     , (307, 'AccessTeamSetup')  -- Right:Access Team Setup = 307
     , (308, 'CanManageTeams')  -- Right:Can Manage Teams = 308
     , (309, 'AccessPermissionSetup')  -- Right:Access Permission Setup = 309
     , (310, 'CanManagePermissionGroups')  -- Right:Can Manage Permission Groups = 310
     , (311, 'AccessSalesGoalsSetup')  -- Right:Access Sales Goals Setup = 311
     , (312, 'CanManageSalesGoals')  -- Right:Can Manage Sales Goals = 312
     , (313, 'AccessAlertSetup')  -- Right:Access Alert Setup = 313
     , (314, 'CanManageAlerts')  -- Right:Can Manage Alerts = 314
     , (315, 'AccessAutomationSetup')  -- Right:Access Automation Setup = 315
     , (316, 'CanManageAutomations')  -- Right:Can Manage Automations = 316
     , (317, 'AccessAutomationHistory')  -- Right:Access Automation History = 317
     , (318, 'CanManageAutomations')  -- Right:Can Manage Automations = 318
     , (319, 'AccessBoardSetup')  -- Right:Access Board Setup = 319
     , (320, 'CanManageBoardSetup')  -- Right:Can Manage Board Setup = 320
     , (321, 'CanFavoriteOthersBoards')  -- Right:Can Favorite Others Boards = 321
     , (322, 'AccessMaterialSetup')  -- Right:Access Material Setup = 322
     , (323, 'CanEditMaterialSetup')  -- Right:Can Edit Material Setup = 323
     , (324, 'AccessLaborSetup')  -- Right:Access Labor Setup = 324
     , (325, 'CanEditLaborSetup')  -- Right:Can Edit Labor Setup = 325
     , (326, 'AccessMachineSetup')  -- Right:Access Machine Setup = 326
     , (327, 'CanEditMachineSetup')  -- Right:Can Edit Machine Setup = 327
     , (328, 'AccessMachineTemplateSetup')  -- Right:Access Machine Template Setup = 328
     , (329, 'CanEditMachineTemplateSetup')  -- Right:Can Edit Machine Template Setup = 329
     , (330, 'AccessAssemblySetup')  -- Right:Access Assembly Setup = 330
     , (331, 'CanEditAssemblySetup')  -- Right:Can Edit Assembly Setup = 331
     , (332, 'AccessAccountingOptionsSetup')  -- Right:Access Accounting Options Setup = 332
     , (333, 'CanEditAccountingOptionsSetup')  -- Right:Can Edit Accounting Options Setup = 333
     , (334, 'AccessPurchasingOptionsSetup')  -- Right:Access Purchasing Options Setup = 334
     , (335, 'CanEditPurchasingOptionsSetup')  -- Right:Can Edit Purchasing Options Setup = 335
     , (336, 'AccessCustomFieldSetup')  -- Right:Access Custom Field Setup = 336
     , (337, 'CanEditCustomFieldSetup')  -- Right:Can Edit Custom Field Setup = 337
     , (338, 'AccessReasonListSetup')  -- Right:Access Reason List Setup = 338
     , (339, 'CanEditReasonListSetup')  -- Right:Can Edit Reason List Setup = 339
     , (340, 'AccessCustomerOptionsSetup')  -- Right:Access Customer Options Setup = 340
     , (341, 'CanEditCustomerOptionsSetup')  -- Right:Can Edit Customer Options Setup = 341
     , (342, 'AccessCustomerListSetup')  -- Right:Access Customer List Setup = 342
     , (343, 'CanEditCustomerListSetup')  -- Right:Can Edit Customer List Setup = 343
     , (344, 'AccessVendorOptionSetup')  -- Right:Access Vendor Option Setup = 344
     , (345, 'CanEditVendorOptionSetup')  -- Right:Can Edit Vendor Option Setup = 345
     , (346, 'AccessOrderOptionsSetup')  -- Right:Access Order Options Setup = 346
     , (347, 'CanEditOrderOptionsSetup')  -- Right:Can Edit Order Options Setup = 347
     , (348, 'AccessEstimateOptionsSetup')  -- Right:Access Estimate Options Setup = 348
     , (349, 'CanEditEstimateOptionsSetup')  -- Right:Can Edit Estimate Options Setup = 349
     , (350, 'AccessStatusSetup')  -- Right:Access Status Setup = 350
     , (351, 'CanEditStatusSetup')  -- Right:Can Edit Status Setup = 351
     , (352, 'AccessOtherEmployeeOptions')  -- Right:Access Other Employee Options = 352
     , (353, 'CanEditOtherEmployeeOptions')  -- Right:Can Edit Other Employee Options = 353
     , (354, 'AccessTimeClockOptions')  -- Right:Access TimeClock Options = 354
     , (355, 'CanEditTimeClockOptions')  -- Right:Can Edit TimeClock Options = 355
     , (356, 'AccessPricingTierSetup')  -- Right:Access Pricing Tier Setup = 356
     , (357, 'CanEditPricingTierSetup')  -- Right:Can Edit Pricing Tier Setup = 357
     , (358, 'AccessCommissionPlanSetup')  -- Right:Access Commission Plan Setup = 358
     , (359, 'CanEditCommissionPlanSetup')  -- Right:Can Edit Commission Plan Setup = 359
     , (360, 'AccessRoyaltyPlanSetup')  -- Right:Access Royalty Plan Setup = 360
     , (361, 'CanEditRoyaltyPlanSetup')  -- Right:Can Edit Royalty Plan Setup = 361
     , (362, 'Access3rdPartyIntegrationSetup')  -- Right:Access 3rd Party Integration Setup = 362
     , (363, 'CanEdit3rdPartyIntegrationSetup')  -- Right:Can Edit 3rd Party Integration Setup = 363
     , (364, 'AccessSystemOptionSetup')  -- Right:Access System Option Setup = 364
     , (365, 'CanEditSystemOptionSetup')  -- Right:Can Edit System Option Setup = 365
     , (366, 'AccessGlobalSettings')  -- Right:Access Global Settings = 366
     , (401, 'CanEditMySalesDashboard')  -- Right:Can Edit My Sales Dashboard = 401
     , (402, 'CanViewWidgetGoals')  -- Right:Can View Widget Goals = 402
     , (403, 'CanViewWidgetCompanySales')  -- Right:Can View Widget Company Sales = 403
     , (404, 'CanViewSalesBoards')  -- Right:Can View Sales Boards = 404
     , (405, 'CanMoveItemsonSalesBoards')  -- Right:Can Move Items on Sales Boards = 405
     , (406, 'CanFavoriteMySalesBoards')  -- Right:Can Favorite My Sales Boards = 406
     , (407, 'CanTagItemsonSalesBoards')  -- Right:Can Tag Items on Sales Boards = 407
     , (408, 'CanViewMyOrders')  -- Right:Can View My Orders = 408
     , (409, 'CanViewAllOrders')  -- Right:Can View All Orders = 409
     , (410, 'CanViewMyEstimates')  -- Right:Can View My Estimates = 410
     , (411, 'CanViewAllEstimates')  -- Right:Can View All Estimates = 411
     , (412, 'CanViewPrices')  -- Right:Can View Prices = 412
     , (413, 'CanViewPartCosts')  -- Right:Can View Part Costs = 413
     , (414, 'AccessOrders')  -- Right:Access Orders = 414
     , (415, 'CanCreateandEditMyOrders')  -- Right:Can Create and Edit My Orders = 415
     , (416, 'CanViewOthersOrders')  -- Right:Can View Others Orders = 416
     , (417, 'CanCreateandEditOthersOrders')  -- Right:Can Create and Edit Others Orders = 417
     , (418, 'CanChangeItemStatus')  -- Right:Can Change Item Status = 418
     , (419, 'CanChangeOrderStatustoBuilt')  -- Right:Can Change Order Status to Built = 419
     , (420, 'CanChangeOrderStatustoInvoicing')  -- Right:Can Change Order Status to Invoicing = 420
     , (421, 'CanChangeOrderStatustoInvoiced')  -- Right:Can Change Order Status to Invoiced = 421
     , (422, 'CanConvertEstimate')  -- Right:Can Convert Estimate = 422
     , (423, 'CanVoidorCloseEstimate')  -- Right:Can Void or Close Estimate = 423
     , (424, 'AccessEstimates')  -- Right:Access Estimates = 424
     , (425, 'CanCreateandEditMyEstimates')  -- Right:Can Create and Edit My Estimates = 425
     , (426, 'CanViewOthersEstimates')  -- Right:Can View Others Estimates = 426
     , (427, 'CanCreateandEditOthersEstimates')  -- Right:Can Create and Edit Others Estimates = 427
     , (428, 'AccessGlobalQuickProducts')  -- Right:Access Global Quick Products = 428
     , (429, 'CanVoidOrders')  -- Right:Can Void Orders = 429
     , (430, 'CanChangeTaxGroups')  -- Right:Can Change Tax Groups = 430
     , (431, 'CanChangeSalesLocation')  -- Right:Can Change Sales Location = 431
     , (432, 'CanOverridePrices')  -- Right:Can Override Prices = 432
     , (433, 'CanOverrideandAddParts')  -- Right:Can Override and Add Parts = 433
     , (434, 'CanOverridePartsCosts')  -- Right:Can Override Parts Costs = 434
     , (435, 'CanOverrideSurcharges')  -- Right:Can Override Surcharges = 435
     , (436, 'CanApplyOrderDiscounts')  -- Right:Can Apply Order Discounts = 436
     , (437, 'CanOverrideOrderValidations')  -- Right:Can Override Order Validations = 437
     , (438, 'CanChangeProductStatusesandSubstatuses')  -- Right:Can Change Product Statuses and Substatuses = 438
     , (439, 'CanOverrideCustomerPayments')  -- Right:Can Override Customer Payments = 439
     , (440, 'CanEditInvoicedandClosedOrders')  -- Right:Can Edit Invoiced and Closed Orders = 440
     , (441, 'CanChangeSalespersonOnOrder')  -- Right:Can Change Salesperson On Order = 441
     , (442, 'CanEditMyQuickProducts')  -- Right:Can Edit My Quick Products = 442
     , (443, 'CanViewCustomerQuickProducts')  -- Right:Can View Customer Quick Products = 443
     , (444, 'CanEditCustomerQuickProducts')  -- Right:Can Edit Customer Quick Products = 444
     , (445, 'CanViewGlobalQuickProducts')  -- Right:Can View Global Quick Products = 445
     , (446, 'CanEditGlobalQuickProducts')  -- Right:Can Edit Global Quick Products = 446
     , (447, 'AccessCompanies')  -- Right:Access Companies = 447
     , (448, 'CanEditMyCompanies')  -- Right:Can Edit My Companies = 448
     , (449, 'CanViewOthersCompanies')  -- Right:Can View Others Companies = 449
     , (450, 'CanEditOthersCompanies')  -- Right:Can Edit Others Companies = 450
     , (451, 'AccessContacts')  -- Right:Access Contacts = 451
     , (452, 'CanEditMyContacts')  -- Right:Can Edit My Contacts = 452
     , (453, 'CanViewOthersContacts')  -- Right:Can View Others Contacts = 453
     , (454, 'CanEditOthersContacts')  -- Right:Can Edit Others Contacts = 454
     , (455, 'AccessCustomerPortal')  -- Right:Access Customer Portal = 455
     , (456, 'CanSetupCustomerPortalAccess')  -- Right:Can Setup Customer Portal Access = 456
     , (457, 'CanAdjustCustomerSpecificPricing')  -- Right:Can Adjust Customer Specific Pricing = 457
     , (458, 'CanChangeCustomerSalesperson')  -- Right:Can Change Customer Salesperson = 458
     , (459, 'CanGiveCustomerCredit')  -- Right:Can Give Customer Credit = 459
     , (460, 'AccessOpportunties')  -- Right:Access Opportunties = 460
     , (461, 'CanViewOthersOpportunities')  -- Right:Can View Others Opportunities = 461
     , (462, 'CanCreateandEditMyOpportunities')  -- Right:Can Create and Edit My Opportunities = 462
     , (463, 'CanCreateandEditOthersOpportunities')  -- Right:Can Create and Edit Others Opportunities = 463
     , (464, 'AccessCalendars')  -- Right:Access Calendars = 464
     , (465, 'CanViewOthersCalendars')  -- Right:Can View Others Calendars = 465
     , (466, 'CanCreateTasksforOthers')  -- Right:Can Create Tasks for Others = 466
     , (467, 'CanEditTasksforOthers')  -- Right:Can Edit Tasks for Others = 467
     , (468, 'CanCreatePurchaseOrder')  -- Right:Can Create Purchase Order = 468
     , (501, 'CanEditMyProductionDashboard')  -- Right:Can Edit My Production Dashboard = 501
     , (502, 'CanViewWidgetsforProduction')  -- Right:Can View Widgets for Production = 502
     , (503, 'CanViewWidgetsforDesign')  -- Right:Can View Widgets for Design = 503
     , (504, 'AccessProductionBoards')  -- Right:Access Production Boards = 504
     , (505, 'CanMoveItemsonProductionBoards')  -- Right:Can Move Items on Production Boards = 505
     , (506, 'CanAdjustMachineSchedule')  -- Right:Can Adjust Machine Schedule = 506
     , (550, 'CanEditMyAccountingDashboard')  -- Right:Can Edit My Accounting Dashboard = 550
     , (551, 'CanViewWidgetsforGL')  -- Right:Can View Widgets for GL = 551
     , (552, 'CanViewWidgetsforPayments')  -- Right:Can View Widgets for Payments = 552
     , (553, 'AccessAccountingBoards')  -- Right:Access Accounting Boards = 553
     , (554, 'CanMoveItemsonAccountingBoards')  -- Right:Can Move Items on Accounting Boards = 554
     , (555, 'AccessPayments')  -- Right:Access Payments = 555
     , (556, 'CanApplyPayments')  -- Right:Can Apply Payments = 556
     , (557, 'CanApplyCustomerCredit')  -- Right:Can Apply Customer Credit = 557
     , (558, 'CanVoidandRefundPayments')  -- Right:Can Void and Refund Payments = 558
     , (559, 'AccessReconciliations')  -- Right:Access Reconciliations = 559
     , (560, 'CanPostReconcilliation')  -- Right:Can Post Reconcilliation = 560
     , (561, 'CanReopenReconciliation')  -- Right:Can Reopen Reconciliation = 561
     , (562, 'CanInitiateAccountingSync')  -- Right:Can Initiate Accounting Sync = 562
     , (563, 'CanExportAccountingData')  -- Right:Can Export Accounting Data = 563
     , (601, 'CanEditMyPurchasingDashboard')  -- Right:Can Edit My Purchasing Dashboard = 601
     , (602, 'CanViewWidgetsforPurchaseOrder')  -- Right:Can View Widgets for Purchase Order = 602
     , (603, 'CanViewWidgetsforVendors')  -- Right:Can View Widgets for Vendors = 603
     , (604, 'AccessPurchasingBoards')  -- Right:Access Purchasing Boards = 604
     , (605, 'CanMoveItemsonPurchasingBoards')  -- Right:Can Move Items on Purchasing Boards = 605
     , (606, 'AccessVendors')  -- Right:Access Vendors = 606
     , (607, 'AccessVendorCatalogs')  -- Right:Access Vendor Catalogs = 607
     , (608, 'CanEditVendorCatalogs')  -- Right:Can Edit Vendor Catalogs = 608
     , (609, 'AccessPurchaseOrders')  -- Right:Access Purchase Orders = 609
     , (610, 'CanApprovePurchaseOrders')  -- Right:Can Approve Purchase Orders = 610
     , (611, 'CanOrderPurchaseOrders')  -- Right:Can Order Purchase Orders = 611
     , (612, 'CanVoidPurchaseOrders')  -- Right:Can Void Purchase Orders = 612
     , (613, 'AccessInventory')  -- Right:Access Inventory = 613
     , (614, 'CanEditInventory')  -- Right:Can Edit Inventory = 614
     , (651, 'CanEditEcommerceOrders')  -- Right:Can Edit Ecommerce Orders = 651
     , (652, 'CanCreateEcommerceOrders')  -- Right:Can Create Ecommerce Orders = 652
     , (701, 'AccessMyProfile')  -- Right:Access My Profile = 701
     , (702, 'CanEditMyProfile')  -- Right:Can Edit My Profile = 702
     , (703, 'AccessTimeClock')  -- Right:Access Time Clock = 703
     , (704, 'CanClockInandOut')  -- Right:Can Clock In and Out = 704
     , (705, 'AccessAlerts')  -- Right:Access Alerts = 705
     , (706, 'CanCreatePersonalAlerts')  -- Right:Can Create Personal Alerts = 706
     , (707, 'AccessMessaging')  -- Right:Access Messaging = 707
     , (708, 'CanSendMessages')  -- Right:Can Send Messages = 708
     , (709, 'AccessMyCalendar')  -- Right:Access My Calendar = 709
     , (710, 'CanInteractwithMyCalendar')  -- Right:Can Interact with My Calendar = 710
     , (711, 'AccessMyDocuments')  -- Right:Access My Documents = 711
     , (712, 'CanAddtoMyDocuments')  -- Right:Can Add to My Documents = 712
     , (713, 'CanDeletefromMyDocuments')  -- Right:Can Delete from My Documents = 713
;
            ");

            migrationBuilder.Sql(@"
INSERT INTO [System.Rights.Group.RightLink] (RightID, GroupID) 
VALUES (1, 1000) -- Right:Allows Basic Access to the Management Module. in Group:Management: Management Module
     , (2, 4000) -- Right:Allows Basic Access to the Accounting Module. in Group:Accounting: Accounting Module
     , (4, 2000) -- Right:Allows Basic Access to the Sales Module. in Group:Sales: Sales Module
     , (8, 3000) -- Right:Allows Basic Access to the Production Module. in Group:Production: Production Module
     , (16, 5000) -- Right:Allows Basic Access to the Purchasing Module. in Group:Purchasing: Purchasing Module
     , (32, 6000) -- Right:Allows Basic Access to the Ecommerce Module. in Group:Ecommerce: Ecommerce Module
     , (128, 7000) -- Right:Allows Basic Access to the User Module. in Group:User: User Module
     , (256, 32000) -- Right:Allows Basic Access to the DevOps Module. in Group:DevOps Module
     , (301, 1011) -- Right:Access Location Setup in Group:Management: Can View and Manage Locations
     , (302, 1011) -- Right:Can Create New Locations in Group:Management: Can View and Manage Locations
     , (303, 1011) -- Right:Can Edit Locations in Group:Management: Can View and Manage Locations
     , (304, 1012) -- Right:Access Employee Setup in Group:Management: Can View and Manage Employees
     , (305, 1012) -- Right:Can Manage Employees in Group:Management: Can View and Manage Employees
     , (306, 1013) -- Right:Access Adjust TimeCards in Group:Management: Can View and Manage Employee Timecards
     , (307, 1014) -- Right:Access Team Setup in Group:Management: Can View and Manage Teams
     , (308, 1014) -- Right:Can Manage Teams in Group:Management: Can View and Manage Teams
     , (309, 1015) -- Right:Access Permission Setup in Group:Management: Can View and Manage Permissions
     , (310, 1015) -- Right:Can Manage Permission Groups in Group:Management: Can View and Manage Permissions
     , (311, 1016) -- Right:Access Sales Goals Setup in Group:Management: Can View and Manage Goals
     , (312, 1016) -- Right:Can Manage Sales Goals in Group:Management: Can View and Manage Goals
     , (313, 1021) -- Right:Access Alert Setup in Group:Management: Can View and Manage Alerts
     , (314, 1021) -- Right:Can Manage Alerts in Group:Management: Can View and Manage Alerts
     , (315, 1022) -- Right:Access Automation Setup in Group:Management: Can View and Manage Automations
     , (316, 1022) -- Right:Can Manage Automations in Group:Management: Can View and Manage Automations
     , (317, 1023) -- Right:Access Automation History in Group:Management: Can View Automation History
     , (318, 1023) -- Right:Can Manage Automations in Group:Management: Can View Automation History
     , (319, 1031) -- Right:Access Board Setup in Group:Management: Can View and Manage Boards
     , (320, 1031) -- Right:Can Manage Board Setup in Group:Management: Can View and Manage Boards
     , (321, 1032) -- Right:Can Favorite Others Boards in Group:Management: Can Favorite Boards for Others
     , (322, 1041) -- Right:Access Material Setup in Group:Management: Can View and Manage Materials
     , (323, 1041) -- Right:Can Edit Material Setup in Group:Management: Can View and Manage Materials
     , (324, 1042) -- Right:Access Labor Setup in Group:Management: Can View and Manage Labor
     , (325, 1042) -- Right:Can Edit Labor Setup in Group:Management: Can View and Manage Labor
     , (326, 1043) -- Right:Access Machine Setup in Group:Management: Can View and Manage Machines
     , (327, 1043) -- Right:Can Edit Machine Setup in Group:Management: Can View and Manage Machines
     , (328, 1043) -- Right:Access Machine Template Setup in Group:Management: Can View and Manage Machines
     , (329, 1043) -- Right:Can Edit Machine Template Setup in Group:Management: Can View and Manage Machines
     , (330, 1044) -- Right:Access Assembly Setup in Group:Management: Can View and Manage Assemblies
     , (331, 1044) -- Right:Can Edit Assembly Setup in Group:Management: Can View and Manage Assemblies
     , (332, 1101) -- Right:Access Accounting Options Setup in Group:Management: Can View and Manage Accounting Options
     , (333, 1101) -- Right:Can Edit Accounting Options Setup in Group:Management: Can View and Manage Accounting Options
     , (334, 1102) -- Right:Access Purchasing Options Setup in Group:Management: Can View and Manage Purchasing Options
     , (335, 1102) -- Right:Can Edit Purchasing Options Setup in Group:Management: Can View and Manage Purchasing Options
     , (336, 1103) -- Right:Access Custom Field Setup in Group:Management: Can View and Manage Custom Fields
     , (337, 1103) -- Right:Can Edit Custom Field Setup in Group:Management: Can View and Manage Custom Fields
     , (338, 1104) -- Right:Access Reason List Setup in Group:Management: Can View and Manage Reason Lists
     , (339, 1104) -- Right:Can Edit Reason List Setup in Group:Management: Can View and Manage Reason Lists
     , (340, 1105) -- Right:Access Customer Options Setup in Group:Management: Can View and Manage Customer Options
     , (341, 1105) -- Right:Can Edit Customer Options Setup in Group:Management: Can View and Manage Customer Options
     , (342, 1106) -- Right:Access Customer List Setup in Group:Management: Can View and Manage Customer Lists
     , (343, 1106) -- Right:Can Edit Customer List Setup in Group:Management: Can View and Manage Customer Lists
     , (344, 1107) -- Right:Access Vendor Option Setup in Group:Management: Can View and Manage Vendor Options
     , (345, 1107) -- Right:Can Edit Vendor Option Setup in Group:Management: Can View and Manage Vendor Options
     , (346, 1108) -- Right:Access Order Options Setup in Group:Management: Can View and Manage Order Options
     , (347, 1108) -- Right:Can Edit Order Options Setup in Group:Management: Can View and Manage Order Options
     , (348, 1109) -- Right:Access Estimate Options Setup in Group:Management: Can View and Manage Estimate Options
     , (349, 1109) -- Right:Can Edit Estimate Options Setup in Group:Management: Can View and Manage Estimate Options
     , (350, 1110) -- Right:Access Status Setup in Group:Management: Can View and Manage Statuses and Substatuses
     , (351, 1110) -- Right:Can Edit Status Setup in Group:Management: Can View and Manage Statuses and Substatuses
     , (352, 1111) -- Right:Access Other Employee Options in Group:Management: Can View and Manage Employee Options
     , (353, 1111) -- Right:Can Edit Other Employee Options in Group:Management: Can View and Manage Employee Options
     , (354, 1112) -- Right:Access TimeClock Options in Group:Management: Can View and Manage Time Clock Options
     , (355, 1112) -- Right:Can Edit TimeClock Options in Group:Management: Can View and Manage Time Clock Options
     , (356, 1113) -- Right:Access Pricing Tier Setup in Group:Management: Can View and Manage Pricing Tiers
     , (357, 1113) -- Right:Can Edit Pricing Tier Setup in Group:Management: Can View and Manage Pricing Tiers
     , (358, 1114) -- Right:Access Commission Plan Setup in Group:Management: Can View and Manage Commission Plans
     , (359, 1114) -- Right:Can Edit Commission Plan Setup in Group:Management: Can View and Manage Commission Plans
     , (360, 1115) -- Right:Access Royalty Plan Setup in Group:Management: Can View and Manage Royalty Plans
     , (361, 1115) -- Right:Can Edit Royalty Plan Setup in Group:Management: Can View and Manage Royalty Plans
     , (362, 1116) -- Right:Access 3rd Party Integration Setup in Group:Management: Can View and Manage 3rd Party Integrations
     , (363, 1116) -- Right:Can Edit 3rd Party Integration Setup in Group:Management: Can View and Manage 3rd Party Integrations
     , (364, 1117) -- Right:Access System Option Setup in Group:Management: Can View and Manage System Options
     , (365, 1117) -- Right:Can Edit System Option Setup in Group:Management: Can View and Manage System Options
     , (366, 1118) -- Right:Access Global Settings in Group:Management: Can View and Manage Other Global Settings
     , (401, 2011) -- Right:Can Edit My Sales Dashboard in Group:Sales: Can Manage My Dashboards
     , (402, 2012) -- Right:Can View Widget Goals in Group:Sales: Can View Goals Widget
     , (403, 2013) -- Right:Can View Widget Company Sales in Group:Sales: Can View Company Sales Widget
     , (404, 2021) -- Right:Can View Sales Boards in Group:Sales: Can View Sales Boards
     , (405, 2022) -- Right:Can Move Items on Sales Boards in Group:Sales: Can Move Items on Boards
     , (406, 2023) -- Right:Can Favorite My Sales Boards in Group:Sales: Can Favorite Boards
     , (407, 2024) -- Right:Can Tag Items on Sales Boards in Group:Sales: Can Tag Items on Boards
     , (408, 2031) -- Right:Can View My Orders in Group:Sales: Can View My Orders
     , (409, 2032) -- Right:Can View All Orders in Group:Sales: Can View All Orders
     , (410, 2033) -- Right:Can View My Estimates in Group:Sales: Can View My Estimates
     , (411, 2034) -- Right:Can View All Estimates in Group:Sales: Can View All Estimates
     , (412, 2035) -- Right:Can View Prices in Group:Sales: Can View Prices
     , (413, 2036) -- Right:Can View Part Costs in Group:Sales: Can View Part Costs
     , (413, 3041) -- Right:Can View Part Costs in Group:Production: Can View Part Costs
     , (414, 2041) -- Right:Access Orders in Group:Sales: Can Create and Manage My Orders
     , (415, 2041) -- Right:Can Create and Edit My Orders in Group:Sales: Can Create and Manage My Orders
     , (416, 2042) -- Right:Can View Others Orders in Group:Sales: Can Create and Manage All Orders
     , (417, 2042) -- Right:Can Create and Edit Others Orders in Group:Sales: Can Create and Manage All Orders
     , (418, 2042) -- Right:Can Change Item Status in Group:Sales: Can Create and Manage All Orders
     , (419, 2042) -- Right:Can Change Order Status to Built in Group:Sales: Can Create and Manage All Orders
     , (420, 2042) -- Right:Can Change Order Status to Invoicing in Group:Sales: Can Create and Manage All Orders
     , (421, 2042) -- Right:Can Change Order Status to Invoiced in Group:Sales: Can Create and Manage All Orders
     , (422, 2042) -- Right:Can Convert Estimate in Group:Sales: Can Create and Manage All Orders
     , (423, 2042) -- Right:Can Void or Close Estimate in Group:Sales: Can Create and Manage All Orders
     , (424, 2043) -- Right:Access Estimates in Group:Sales: Can Create and Manage My Estimates
     , (425, 2043) -- Right:Can Create and Edit My Estimates in Group:Sales: Can Create and Manage My Estimates
     , (426, 2044) -- Right:Can View Others Estimates in Group:Sales: Can Create and Manage All Estimates
     , (427, 2044) -- Right:Can Create and Edit Others Estimates in Group:Sales: Can Create and Manage All Estimates
     , (428, 2045) -- Right:Access Global Quick Products in Group:Sales: Can Create and Manage Global Quick Products
     , (429, 2046) -- Right:Can Void Orders in Group:Sales: Can Void Orders
     , (430, 2047) -- Right:Can Change Tax Groups in Group:Sales: Can Change Tax Groups
     , (431, 2048) -- Right:Can Change Sales Location in Group:Sales: Can Change Sales Location
     , (432, 2049) -- Right:Can Override Prices in Group:Sales: Can Override Prices
     , (433, 2050) -- Right:Can Override and Add Parts in Group:Sales: Can Override and Add Parts
     , (434, 2051) -- Right:Can Override Parts Costs in Group:Sales: Can Override Parts Costs
     , (435, 2052) -- Right:Can Override Surcharges in Group:Sales: Can Override Product Setup Fees
     , (436, 2053) -- Right:Can Apply Order Discounts in Group:Sales: Can Apply Order Discounts
     , (437, 2054) -- Right:Can Override Order Validations in Group:Sales: Can Override Order Validations
     , (438, 2055) -- Right:Can Change Product Statuses and Substatuses in Group:Sales: Can Change Product Statuses and Substatuses
     , (439, 2056) -- Right:Can Override Customer Payments in Group:Sales: Can Override Customer Payments
     , (440, 2057) -- Right:Can Edit Invoiced and Closed Orders in Group:Sales: Can Edit Orders After they are Invoiced
     , (441, 2059) -- Right:Can Change Salesperson On Order in Group:Sales: Can Change Salesperson Assigned to Order
     , (442, 2071) -- Right:Can Edit My Quick Products in Group:Sales: Can View and Manage My Quick Products
     , (443, 2072) -- Right:Can View Customer Quick Products in Group:Sales: Can View My Customers Quick Products
     , (444, 2074) -- Right:Can Edit Customer Quick Products in Group:Sales: Can Manage Global Quick Products
     , (445, 2073) -- Right:Can View Global Quick Products in Group:Sales: Can View Global Quick Products
     , (446, 2075) -- Right:Can Edit Global Quick Products in Group:Sales: Can Manage All Customers Quick Products
     , (447, 2081) -- Right:Access Companies in Group:Sales: Can View My Companies
     , (448, 2081) -- Right:Can Edit My Companies in Group:Sales: Can View My Companies
     , (449, 2082) -- Right:Can View Others Companies in Group:Sales: Can View All Companies
     , (450, 2082) -- Right:Can Edit Others Companies in Group:Sales: Can View All Companies
     , (451, 2083) -- Right:Access Contacts in Group:Sales: Can View My Contacts
     , (452, 2083) -- Right:Can Edit My Contacts in Group:Sales: Can View My Contacts
     , (453, 2084) -- Right:Can View Others Contacts in Group:Sales: Can View All Contacts
     , (454, 2084) -- Right:Can Edit Others Contacts in Group:Sales: Can View All Contacts
     , (455, 2085) -- Right:Access Customer Portal in Group:Sales: Can Access Customer Portal
     , (456, 2086) -- Right:Can Setup Customer Portal Access in Group:Sales: Can Setup Customer Portal Access
     , (457, 2087) -- Right:Can Adjust Customer Specific Pricing in Group:Sales: Can Adjust Customer Specific Pricing
     , (458, 2089) -- Right:Can Change Customer Salesperson in Group:Sales: Can Change Salsperson Assigned to Customer
     , (459, 2090) -- Right:Can Give Customer Credit in Group:Sales: Can Approve/Edit Customer Credit
     , (459, 4034) -- Right:Can Give Customer Credit in Group:Accounting: Can Give Customer Credit
     , (460, 2101) -- Right:Access Opportunties in Group:Sales: Can View My Opportunities
     , (461, 2102) -- Right:Can View Others Opportunities in Group:Sales: Can View All Opportunities
     , (462, 2103) -- Right:Can Create and Edit My Opportunities in Group:Sales: Can Create and Edit My Opportunities
     , (463, 2104) -- Right:Can Create and Edit Others Opportunities in Group:Sales: Can Create and Edit All Opportunities
     , (464, 2105) -- Right:Access Calendars in Group:Sales: Can View and Manage My Calendar
     , (465, 2106) -- Right:Can View Others Calendars in Group:Sales: Can View All Calendars
     , (466, 2107) -- Right:Can Create Tasks for Others in Group:Sales: Can Create Tasks for Others
     , (467, 2108) -- Right:Can Edit Tasks for Others in Group:Sales: Can Manage Tasks for Others
     , (468, 2151) -- Right:Can Create Purchase Order in Group:Sales: Can Create Purchase Orders for Parts
     , (501, 3011) -- Right:Can Edit My Production Dashboard in Group:Production: Can Manage My Dashboards
     , (502, 3012) -- Right:Can View Widgets for Production in Group:Production: Can View Production Widgets
     , (503, 3013) -- Right:Can View Widgets for Design in Group:Production: Can View Design Widgets
     , (504, 3021) -- Right:Access Production Boards in Group:Production: Can View Production Boards
     , (505, 3022) -- Right:Can Move Items on Production Boards in Group:Production: Can Move Items on Boards
     , (506, 3045) -- Right:Can Adjust Machine Schedule in Group:Production: Can Change Machine Schedule
     , (550, 4011) -- Right:Can Edit My Accounting Dashboard in Group:Accounting: Can Manage My Dashboards
     , (551, 4012) -- Right:Can View Widgets for GL in Group:Accounting: Can View GL Widgets
     , (552, 4013) -- Right:Can View Widgets for Payments in Group:Accounting: Can View Payment Widgets
     , (553, 4021) -- Right:Access Accounting Boards in Group:Accounting: Can View Accounting Boards
     , (554, 4022) -- Right:Can Move Items on Accounting Boards in Group:Accounting: Can Move Items on Boards
     , (555, 4000) -- Right:Access Payments in Group:Accounting: Accounting Module
     , (556, 4031) -- Right:Can Apply Payments in Group:Accounting: Can Apply Payments
     , (557, 4032) -- Right:Can Apply Customer Credit in Group:Accounting: Can Apply Customer Credit
     , (558, 4033) -- Right:Can Void and Refund Payments in Group:Accounting: Can Void and Refund Payment
     , (559, 4041) -- Right:Access Reconciliations in Group:Accounting: Can View Reconciliations
     , (560, 4042) -- Right:Can Post Reconcilliation in Group:Accounting: Can Reconcile Payments
     , (561, 4043) -- Right:Can Reopen Reconciliation in Group:Accounting: Can Reopen Latest Reconciliation
     , (562, 4051) -- Right:Can Initiate Accounting Sync in Group:Accounting: Can Initiate External Accounting Sync
     , (563, 4052) -- Right:Can Export Accounting Data in Group:Accounting: Can Manually Export Accounting Records
     , (601, 5011) -- Right:Can Edit My Purchasing Dashboard in Group:Production: Can Manage My Dashboards
     , (602, 5012) -- Right:Can View Widgets for Purchase Order in Group:Production: Can View Purchase Order Widgets
     , (603, 5013) -- Right:Can View Widgets for Vendors in Group:Production: Can View Vendor Widgets
     , (604, 5021) -- Right:Access Purchasing Boards in Group:Purchasing: Can View Purchasing Boards
     , (605, 5022) -- Right:Can Move Items on Purchasing Boards in Group:Purchasing: Can Move Items on Boards
     , (606, 5031) -- Right:Access Vendors in Group:Purchasing: Can View and Manage Vendors
     , (607, 5032) -- Right:Access Vendor Catalogs in Group:Purchasing: Can View Vendor Catalogs
     , (608, 5033) -- Right:Can Edit Vendor Catalogs in Group:Purchasing: Can Manage Vendor Catalogs
     , (609, 5041) -- Right:Access Purchase Orders in Group:Purchasing: Can Create and Manage Purchase Order Requests
     , (610, 5042) -- Right:Can Approve Purchase Orders in Group:Purchasing: Can Approve Purchase Orders
     , (611, 5043) -- Right:Can Order Purchase Orders in Group:Purchasing: Can Order Purchase Orders
     , (612, 5044) -- Right:Can Void Purchase Orders in Group:Purchasing: Can Void Purchase Orders
     , (613, 5061) -- Right:Access Inventory in Group:Purchasing: Can View Inventory
     , (614, 5062) -- Right:Can Edit Inventory in Group:Purchasing: Can Manage Inventory
     , (651, 2058) -- Right:Can Edit Ecommerce Orders in Group:Sales: Can Edit Ecommerce Orders
     , (651, 6011) -- Right:Can Edit Ecommerce Orders in Group:Ecommerce: Can edit orders inside Ecommerce
     , (652, 6012) -- Right:Can Create Ecommerce Orders in Group:Ecommerce: Can create new orders inside Ecommerce
     , (701, 7011) -- Right:Access My Profile in Group:User: Can View and Manage My Profile
     , (702, 7011) -- Right:Can Edit My Profile in Group:User: Can View and Manage My Profile
     , (703, 7012) -- Right:Access Time Clock in Group:User: Can Use Time Clock
     , (704, 7012) -- Right:Can Clock In and Out in Group:User: Can Use Time Clock
     , (705, 7013) -- Right:Access Alerts in Group:User: Can Create Personal Alerts
     , (706, 7013) -- Right:Can Create Personal Alerts in Group:User: Can Create Personal Alerts
     , (707, 7014) -- Right:Access Messaging in Group:User: Can Send Messages
     , (708, 7014) -- Right:Can Send Messages in Group:User: Can Send Messages
     , (709, 7015) -- Right:Access My Calendar in Group:User: Can View and Manage My Calendar
     , (710, 7015) -- Right:Can Interact with My Calendar in Group:User: Can View and Manage My Calendar
     , (711, 7016) -- Right:Access My Documents in Group:User: Can View and Manage My Documents
     , (712, 7016) -- Right:Can Add to My Documents in Group:User: Can View and Manage My Documents
     , (713, 7016) -- Right:Can Delete from My Documents in Group:User: Can View and Manage My Documents  ,
;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
