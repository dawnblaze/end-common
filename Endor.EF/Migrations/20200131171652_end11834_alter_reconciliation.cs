﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class end11834_alter_reconciliation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AdjustedReconciliationID",
                table: "Accounting.Reconciliation.Data",
                type: "INT SPARSE",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "ExportedByID",
                table: "Accounting.Reconciliation.Data",
                type: "SMALLINT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FormattedNumber",
                table: "Accounting.Reconciliation.Data",
                type: "NVARCHAR(32)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsEmpty",
                table: "Accounting.Reconciliation.Data",
                type: "BIT",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<short>(
                name: "SyncedByID",
                table: "Accounting.Reconciliation.Data",
                type: "SMALLINT",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SyncedDT",
                table: "Accounting.Reconciliation.Data",
                type: "DATETIME2(2)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalIncome",
                table: "Accounting.Reconciliation.Data",
                type: "DECIMAL(18,6)",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPayments",
                table: "Accounting.Reconciliation.Data",
                type: "DECIMAL(18,6)",
                nullable: false,
                defaultValueSql: "0");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_AdjustedReconciliationID",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "AdjustedReconciliationID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_ExportedByID",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "ExportedByID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_SyncedByID",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "SyncedByID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Reconciliation.Data_Reconciliation.Data",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "AdjustedReconciliationID" },
                principalTable: "Accounting.Reconciliation.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Reconciliation.Data_Employee.Exported",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "ExportedByID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Reconciliation.Data_Employee.Synced",
                table: "Accounting.Reconciliation.Data",
                columns: new[] { "BID", "SyncedByID" },
                principalTable: "Employee.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Reconciliation.Data_Reconciliation.Data",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Reconciliation.Data_Employee.Exported",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Reconciliation.Data_Employee.Synced",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_AdjustedReconciliationID",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_ExportedByID",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Reconciliation.Data_BID_SyncedByID",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropColumn(
                name: "AdjustedReconciliationID",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropColumn(
                name: "ExportedByID",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropColumn(
                name: "FormattedNumber",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropColumn(
                name: "IsEmpty",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropColumn(
                name: "SyncedByID",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropColumn(
                name: "SyncedDT",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropColumn(
                name: "TotalIncome",
                table: "Accounting.Reconciliation.Data");

            migrationBuilder.DropColumn(
                name: "TotalPayments",
                table: "Accounting.Reconciliation.Data");
        }
    }
}
