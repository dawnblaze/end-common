﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11657_Update_Old_Labor_And_Material_Dropdowns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                --1) set LinkedMaterialID 
                UPDATE v
                SET LinkedMaterialID = p.ID
                FROM [Part.Subassembly.Variable] v
                INNER JOIN [Part.Material.Data] p on p.Name = v.DefaultValue
                WHERE v.ElementType in (13) and (v.displaytype is null and v.ListDataType = 12000) and v.DefaultValue is not null

                --2) change element ElementType
                UPDATE e
                SET ElementType = 104
                FROM [Part.Subassembly.Element] e
                WHERE e.ElementType = 13 and Exists(SELECT * from [Part.Subassembly.Variable] v where e.BID = v.BID and v.ID = e.VariableID and v.ListDataType = 12000)

                --3) change variable ElementType
                UPDATE [Part.Subassembly.Variable]
                SET ElementType = 104
                WHERE ElementType in (13) and (displaytype is null and ListDataType = 12000)


                --### LABOR ###
                --1) set LinkedLaborID 
                UPDATE v
                SET LinkedLaborID = p.ID
                FROM [Part.Subassembly.Variable] v
                INNER JOIN [Part.Material.Data] p on p.Name = v.DefaultValue
                WHERE v.ElementType in (13) and (v.displaytype is null and v.ListDataType = 12020) and v.DefaultValue is not null

                --2) change element ElementType
                UPDATE e
                SET ElementType = 105
                FROM [Part.Subassembly.Element] e
                WHERE e.ElementType = 13 and Exists(SELECT * from [Part.Subassembly.Variable] v where e.BID = v.BID and v.ID = e.VariableID and v.ListDataType = 12020)

                --3) change variable ElementType
                UPDATE [Part.Subassembly.Variable]
                SET ElementType = 105
                WHERE ElementType in (13) and (displaytype is null and ListDataType = 12020)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
 
        }
    }
}
