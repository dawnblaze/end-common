﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11366_DomainsToCustomDomains : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DECLARE @SytemOptionCategoryOld TABLE(ID SMALLINT NOT NULL, [Name] VARCHAR(255) NOT NULL )

INSERT INTO @SytemOptionCategoryOld ( ID, [Name] )
VALUES (214, 'Order Voided Reasons')
, (104, 'Email Domains')
, (702, 'Online Credit Card')
, (703, 'TaxJar')
, (401, 'Tag Setup')
, (406, 'Time Clock Activities')
, (407, 'Time Clock Breaks')
, (801, 'Document Report Templates')
, (7, 'Origins')
, (8, 'Industries')
, (603, 'Order Credit Check')
, (608, 'Quick Item Categories')
, (102, 'Custom Domains')

UPDATE [System.Option.Category]
SET [Name] = (SELECT [Name] FROM @SytemOptionCategoryOld WHERE ID = [System.Option.Category].ID)
WHERE ID IN (SELECT ID FROM @SytemOptionCategoryOld);");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
