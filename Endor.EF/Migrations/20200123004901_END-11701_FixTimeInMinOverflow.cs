﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11701_FixTimeInMinOverflow : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Employee.TimeCard]
DROP COLUMN TimeInMin
;
ALTER TABLE[Employee.TimeCard]
ADD [TimeInMin] AS ((DATEDIFF_BIG(millisecond,[StartDT],[EndDT])/(1000.0))/(60.0))
;

ALTER TABLE [Employee.TimeCard.Detail]
DROP COLUMN TimeInMin, COLUMN PaidTimeInMin
;
ALTER TABLE[Employee.TimeCard.Detail]
ADD[PaidTimeInMin] AS (case when[IsPaid] = (1) then(DATEDIFF_BIG(millisecond,[StartDT],[EndDT]) / (1000.0)) / (60.0) else (0.0) end)
, [TimeInMin] AS ((DATEDIFF_BIG(millisecond,[StartDT],[EndDT])/(1000.0))/(60.0))
;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
ALTER TABLE [Employee.TimeCard]
DROP COLUMN TimeInMin
;

ALTER TABLE[Employee.TimeCard]
ADD [TimeInMin] AS ((datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0))
;

ALTER TABLE [Employee.TimeCard.Detail]
DROP COLUMN TimeInMin, COLUMN PaidTimeInMin
;

ALTER TABLE[Employee.TimeCard.Detail]
ADD[PaidTimeInMin] AS(case when [IsPaid]=(1) then (datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0) else (0.0) end)
  , [TimeInMin] AS ((datediff(millisecond,[StartDT],coalesce([EndDT],getutcdate()))/(1000.0))/(60.0))

            ");

        }
    }
}
