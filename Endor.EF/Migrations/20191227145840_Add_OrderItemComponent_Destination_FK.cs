﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Add_OrderItemComponent_Destination_FK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddForeignKey(
                name: "FK_Order.Item.Component_Part.Destination.Data",
                table: "Order.Item.Component",
                columns: new[] { "BID", "DestinationID" },
                principalTable: "Destination.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Order.Item.Component_Part.Destination.Data",
                table: "Order.Item.Component");
        }
    }
}
