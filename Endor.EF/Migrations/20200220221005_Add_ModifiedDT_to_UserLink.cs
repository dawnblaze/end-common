﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Add_ModifiedDT_to_UserLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDT",
                table: "User.Link",
                type: "datetime2(2)",
                nullable: false,
                defaultValueSql: "(getutcdate())");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModifiedDT",
                table: "User.Link");
        }
    }
}
