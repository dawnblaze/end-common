﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class MatchDataTypeToDBValues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [enum.DataType] SET Name = 'OrderStatus' WHERE ID = 100;
UPDATE [enum.DataType] SET Name = 'TimeZone' WHERE ID = 141;
UPDATE [enum.DataType] SET Name = 'OrderItemComponent' WHERE ID = 10021;
UPDATE [enum.DataType] SET Name = 'PaymentTerm' WHERE ID = 11105;
UPDATE [enum.DataType] SET Name = 'MaterialPart' WHERE ID = 12000;
UPDATE [enum.DataType] SET Name = 'LaborPart' WHERE ID = 12020;
UPDATE [enum.DataType] SET Name = 'MachinePart' WHERE ID = 12030;
UPDATE [enum.DataType] SET Name = 'SurchargeDef' WHERE ID = 12070;

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20010)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20010, 'OrderCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'OrderCategory' WHERE ID = 20010
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20020)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20020, 'LocationCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'LocationCategory' WHERE ID = 20020
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20030)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20030, 'DestinationCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'DestinationCategory' WHERE ID = 20030
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20040)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20040, 'LineItemCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'LineItemCategory' WHERE ID = 20040
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20050)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20050, 'TaxesCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'TaxesCategory' WHERE ID = 20050
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20060)
BEGIN
INSERT INTO [enum.DataType] (ID, Name) VALUES (20060, 'PricesCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'PricesCategory' WHERE ID = 20060
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20070)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20070, 'CostCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'CostCategory' WHERE ID = 20070
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20090)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20090, 'PaymentCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'PaymentCategory' WHERE ID = 20090
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20100)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20100, 'TransactionHeaderNotesCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'TransactionHeaderNotesCategory' WHERE ID = 20100
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20101)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20101, 'LineItemNotesCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'LineItemNotesCategory' WHERE ID = 20101
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20120)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20120, 'ContactCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'ContactCategory' WHERE ID = 20120
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20130)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20130, 'LineItemStatusCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'LineItemStatusCategory' WHERE ID = 20130
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20140)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20140, 'CompanyTypeCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'CompanyTypeCategory' WHERE ID = 20140
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20150)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20150, 'CompanyTaxCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20160)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20160, 'SubsidiariesCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'SubsidiariesCategory' WHERE ID = 20160
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20170)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20170, 'CustomerCreditCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'CustomerCreditCategory' WHERE ID = 20170
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20180)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20180, 'PONumberCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'PONumberCategory' WHERE ID = 20180
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20190)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20190, 'TransactionHeaderContactRoleCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'TransactionHeaderContactRoleCategory' WHERE ID = 20190
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20200)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20200, 'TransactionHeaderEmployeeRoleListCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'TransactionHeaderEmployeeRoleListCategory' WHERE ID = 20200
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20210)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20210, 'TransactionHeaderEmployeeRoleList');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'TransactionHeaderEmployeeRoleList' WHERE ID = 20210
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20211)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20211, 'TransactionHeaderTagsCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'TransactionHeaderTagsCategory' WHERE ID = 20211
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20220)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20220, 'EmployeeCategory');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'EmployeeCategory' WHERE ID = 20220
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20230)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20230, 'OrderDateCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20240)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20240, 'TaxExemptReason');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20250)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20250, 'EstimateCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20260)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20260, 'EstimateDateCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20280)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20280, 'OrderItemComponentCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20290)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20290, 'IComponentDef');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20300)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20300, 'ComponentType');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20310)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20310, 'CompanyCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20320)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20320, 'CompanyContactListCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20330)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20330, 'CompanyEmployeeListCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20340)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20340, 'CompanyEmployeeRoleListCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20350)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20350, 'CompanyEmployeeRoleCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20360)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20360, 'CompanyDatesCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20361)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20361, 'CompanyTagsCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20370)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20370, 'OrderItemPricesCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20380)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20380, 'OrderItemLocationCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20390)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20390, 'OrderItemTaxesCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20400)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20400, 'OrderItemCostsCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20410)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20410, 'OrderItemTagsCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20430)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20430, 'OrderItemContactRoleCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20440)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20440, 'OrderItemEmployeeRoleListCategory');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = 20450)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (20450, 'OrderItemEmployeeRoleList');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -12070)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-12070, 'SurchargeDefList');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -12030)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-12030, 'MachinePartList');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -12020)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-12020, 'LaborPartList');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'LaborPartList' WHERE ID = -12020
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -12000)
BEGIN
	--INSERT INTO [enum.DataType] (ID, Name) VALUES (-12000, 'MaterialPartList');
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-12000, 'MaterialPartList');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'MaterialPartList' WHERE ID = -12000
END


IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -10400)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-10400, 'PurchaseOrderList');
END 
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'PurchaseOrderList' WHERE ID = -10400
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -10200)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-10200, 'EstimateList');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'EstimateList' WHERE ID = -10200
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -10021)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-10021, 'OrderItemComponentList');
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -10000)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-10000, 'OrderList');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'OrderList' WHERE ID = -10000
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -9001)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-9001, 'OpportunityCustomFieldList');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'OpportunityCustomFieldList' WHERE ID = -9001
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -1902)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-1902, 'FlatListList');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'FlatListList' WHERE ID = -1902
END

IF NOT EXISTS(SELECT * FROM [enum.DataType] WHERE ID = -100)
BEGIN
	INSERT INTO [enum.DataType] (ID, Name) VALUES (-100, 'OrderStatusList');
END
ELSE
BEGIN
	UPDATE [enum.DataType] SET Name = 'OrderStatusList' WHERE ID = -100
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
