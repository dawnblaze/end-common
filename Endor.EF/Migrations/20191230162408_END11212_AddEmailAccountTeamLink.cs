﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11212_AddEmailAccountTeamLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Email.Account.TeamLink",
                columns: table => new
                {
                    BID = table.Column<short>(nullable: false),
                    EmailAccountID = table.Column<short>(nullable: false),
                    TeamID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Email.Account.TeamLink", x => new { x.BID, x.EmailAccountID, x.TeamID });
                    table.ForeignKey(
                        name: "FK_Email.Account.TeamLink_Email.Account.Data",
                        columns: x => new { x.BID, x.EmailAccountID },
                        principalTable: "Email.Account.Data",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Email.Account.TeamLink_Employee.Team",
                        columns: x => new { x.BID, x.TeamID },
                        principalTable: "Employee.Team",
                        principalColumns: new[] { "BID", "ID" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Email.Account.TeamLink_Team",
                table: "Email.Account.TeamLink",
                columns: new[] { "BID", "TeamID" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Email.Account.TeamLink");
        }
    }
}
