﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_11671_MigrationForEnumUserRightRemoval_NoChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // intentionally cleared out, we don't want the associated changes to affect the DB
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // intentionally cleared out, we don't want the associated changes to affect the DB
        }
    }
}
