﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class end11837_alter_location_add_Abbreviation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Abbreviation",
                table: "Location.Data",
                type: "nvarchar(10)",
                nullable: false,
                defaultValueSql: "'L01'");

            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.List.Filter]
                           ([ID]
                           ,[IsActive]
                           ,[Name]
                           ,[TargetClassTypeID]
                           ,[IDs]
                           ,[Criteria]
                           ,[Hint]
                           ,[SortIndex])
                     VALUES
                           ((SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter])
                           ,1
                           ,'All'
                           ,8015
                           ,NULL
                           ,NULL
                           ,NULL
                           ,0);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Abbreviation",
                table: "Location.Data");

            migrationBuilder.Sql(@"
                DELETE FROM [dbo].[System.List.Filter]
                WHERE [Name]='All' AND [TargetClassTypeID]=8015;
            ");
        }
    }
}
