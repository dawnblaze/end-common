﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11693CorrectConsumptionFormulas : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Part.Subassembly.Variable]
SET    ConsumptionDefaultValue = '=Quantity.Value', IsConsumptionFormula = 1
WHERE  ElementType = 107 and ConsumptionDefaultValue is null
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }

        public override bool ClearAllAssemblies()
        {
            return true;
        }
    }
}
