﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class CreditMemoFiltersAndCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Accounting.Reconciliation.Item",
                nullable: false,
                computedColumnSql: "8016",
                oldClrType: typeof(int),
                oldType: "int",
                oldComputedColumnSql: "8015");

            migrationBuilder.Sql(
                @"
                    IF NOT EXISTS(SELECT 1 FROM [dbo].[System.List.Filter] WHERE [TargetClassTypeID]=10300 AND [Name]='All')
                    BEGIN
                        INSERT INTO [dbo].[System.List.Filter]
                        ([ID]
                        ,[IsActive]
                        ,[Name]
                        ,[TargetClassTypeID]
                        ,[SortIndex])
                        VALUES
                        ((SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter])
                        ,1
                        ,'All'
                        ,10300
	                    ,0);
                    END

                    INSERT INTO [dbo].[System.List.Filter.Criteria]
                        ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                    VALUES
                        (10300 --<TargetClassTypeID, int,>
                            ,'Company' --<Name, varchar(255),>
                            ,'Company' --<Label, varchar(255),>
                            ,'CompanyName' --<Field, varchar(255),>
                            ,0 --<IsHidden, bit,>
                            ,1 --<DataType, tinyint,>
                            ,0 --<InputType, tinyint,>
                            ,0 --<AllowMultiple, bit,>
                            ,NULL --<ListValues, varchar(max),>
                            ,NULL --<ListValuesEndpoint, varchar(255),>
                            ,0 --<IsLimitToList, bit,>
                            ,0 --<SortIndex, tinyint,>
                            ,(SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        ),
	                    (10300 --<TargetClassTypeID, int,>
                            ,'Description' --<Name, varchar(255),>
                            ,'Description' --<Label, varchar(255),>
                            ,'Description' --<Field, varchar(255),>
                            ,0 --<IsHidden, bit,>
                            ,1 --<DataType, tinyint,>
                            ,0 --<InputType, tinyint,>
                            ,0 --<AllowMultiple, bit,>
                            ,NULL --<ListValues, varchar(max),>
                            ,NULL --<ListValuesEndpoint, varchar(255),>
                            ,0 --<IsLimitToList, bit,>
                            ,1 --<SortIndex, tinyint,>
                            ,(SELECT MAX(ID)+2 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        ),
	                    (10300 --<TargetClassTypeID, int,>
                            ,'CreditMemoStatus' --<Name, varchar(255),>
                            ,'Status' --<Label, varchar(255),>
                            ,'StatusID' --<Field, varchar(255),>
                            ,0 --<IsHidden, bit,>
                            ,32005 --<DataType, tinyint,>
                            ,15 --<InputType, tinyint,>
                            ,0 --<AllowMultiple, bit,>
                            ,NULL --<ListValues, varchar(max),>
                            ,NULL --<ListValuesEndpoint, varchar(255),>
                            ,0 --<IsLimitToList, bit,>
                            ,2 --<SortIndex, tinyint,>
                            ,(SELECT MAX(ID)+3 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        ),
	                    (10300 --<TargetClassTypeID, int,>
                            ,'ShowVoidedCreditMemos' --<Name, varchar(255),>
                            ,'Show Voided' --<Label, varchar(255),>
                            ,'IsVoided' --<Field, varchar(255),>
                            ,0 --<IsHidden, bit,>
                            ,3 --<DataType, tinyint,>
                            ,11 --<InputType, tinyint,>
                            ,0 --<AllowMultiple, bit,>
                            ,'Hide Voided,Show Voided' --<ListValues, varchar(max),>
                            ,NULL --<ListValuesEndpoint, varchar(255),>
                            ,0 --<IsLimitToList, bit,>
                            ,3 --<SortIndex, tinyint,>
                            ,(SELECT MAX(ID)+4 FROM [dbo].[System.List.Filter.Criteria]) --<ID, smallint,>
                        );
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ClassTypeID",
                table: "Accounting.Reconciliation.Item",
                type: "int",
                nullable: false,
                computedColumnSql: "8015",
                oldClrType: typeof(int),
                oldComputedColumnSql: "8016");

            migrationBuilder.Sql(
                @"
                    DELETE FROM [dbo].[System.List.Filter] WHERE [TargetClassTypeID]=10300;
                    DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE [TargetClassTypeID]=10300;
                ");
        }
    }
}
