﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END10764_Fix_Chart_of_Accounts_filter_criteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                UPDATE [System.List.Filter] SET Criteria = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>true</SearchValue><Field>IsAsset</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>' WHERE [ID]=1012 AND [ClassTypeID]=1700 AND [TargetClassTypeID]=8000;
                UPDATE [System.List.Filter] SET Criteria = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>true</SearchValue><Field>IsLiability</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>' WHERE [ID]=1013 AND [ClassTypeID]=1700 AND [TargetClassTypeID]=8000;
                UPDATE [System.List.Filter] SET Criteria = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>true</SearchValue><Field>IsIncome</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>' WHERE [ID]=1014 AND [ClassTypeID]=1700 AND [TargetClassTypeID]=8000;
                UPDATE [System.List.Filter] SET Criteria = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>true</SearchValue><Field>IsCOGS</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>' WHERE [ID]=1015 AND [ClassTypeID]=1700 AND [TargetClassTypeID]=8000;
                UPDATE [System.List.Filter] SET Criteria = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>true</SearchValue><Field>IsExpense</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>' WHERE [ID]=1016 AND [ClassTypeID]=1700 AND [TargetClassTypeID]=8000;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
