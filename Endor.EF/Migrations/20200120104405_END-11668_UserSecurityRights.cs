﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11668_UserSecurityRights : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"-- ========================================================
-- 
-- Name: User.Security.Rights( BID int, UserID int )
--
-- Description: This Table Value Function returns an array of Rights associated
-- with a specific user for a given business id.
--
-- Sample Use:   SELECT * FROM dbo.[User.Security.Rights]( 1, 3 )
--
-- ========================================================
CREATE OR ALTER FUNCTION [dbo].[User.Security.Rights] ( @BID int, @UserID int )
RETURNS TABLE
AS
RETURN
(
    --DECLARE 
    --    @BID SMALLINT = 1
    --    , @UserID int = 3  -- select * from [User.Link]
    --;
    WITH TopGroup AS 
    (
        SELECT 'PG' AS [Source], PG.ID as ID, PG.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [Rights.Group.List] PG ON UL.BID = PG.BID AND UL.RightsGroupListID = PG.ID
        JOIN [Rights.Group.List.RightsGroupLink] PGLink ON PG.BID = PGLink.BID AND PG.ID = PGLink.ListID
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = PGLink.GroupID
        WHERE UL.BID = @BID AND UL.ID = @UserID

        UNION

        SELECT 'UAT' AS [Source], UAT.ID, UAT.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [enum.User.Access.Type] UAT ON UL.UserAccessType = UAT.ID
        JOIN [System.Rights.UserAccessType.GroupLink] UATLink ON UAT.ID = UATLink.UserAccessType
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = UATLink.IncludedRightsGroupID
        WHERE UL.BID = @BID AND UL.ID = @UserID
    ),
        AllGroups AS
    (
    SELECT DISTINCT GroupID
        , GroupName
        , (SELECT CONCAT([Source], '(', ID, '-', Name, '); ') FROM TopGroup T2 WHERE T2.GroupID = TopGroup.GroupID FOR XML PATH('')) AS Sources
    FROM TopGroup
    GROUP BY GroupID, GroupName

    UNION ALL

    SELECT ChildGroup.ID as GroupID
        , ChildGroup.Name as GroupName
        , CONVERT(NVARCHAR(MAX),CONCAT('Child(', Links.ChildID, '-', ChildGroup.Name,  ')->Parent(', Links.ParentID, '-', AllGroups.GroupName, '); ')) as Sources
    FROM AllGroups 
    JOIN [System.Rights.Group.ChildLink] Links ON AllGroups.GroupID = Links.ParentID
    JOIN [System.Rights.Group] ChildGroup ON ChildGroup.ID = Links.ChildID
    )
        SELECT DISTINCT RightLink.RightID as RightID
        From AllGroups
        LEFT JOIN [System.Rights.Group.RightLink] RightLink on RightLink.GroupID = AllGroups.GroupID
)
;

GO");

            migrationBuilder.Sql(@"-- ===========================================================================
/*  Description: Creates a Function that lists the users rights and where 
                 they were obtained from.  Only rights granted are shown.

    Example Usage:
        
        SELECT * FROM dbo.[User.Security.RightsBySource] (1, 3);

*/
-- ===========================================================================
CREATE OR ALTER FUNCTION [User.Security.RightsBySource] ( @BID int, @UserID int )
RETURNS TABLE 
AS
RETURN 
(
    --DECLARE 
    --      @BID SMALLINT = -1
    --    , @UAT SMALLINT = 0  -- Use 0 to remove all, NULL to include all 
    --    , @PermissionGroupID SMALLINT = NULL  -- Use NULL to include All
    --    ;

    WITH TopGroup AS 
    (
        SELECT 'PG' AS [Source], PG.ID as ID, PG.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [Rights.Group.List] PG ON PG.BID = UL.BID AND UL.RightsGroupListID = PG.ID
        JOIN [Rights.Group.List.RightsGroupLink] PGLink ON PG.BID = PGLink.BID AND PG.ID = PGLink.ListID
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = PGLink.GroupID
        WHERE UL.BID = @BID AND UL.ID = @UserID

        UNION

        SELECT 'UAT' AS [Source], UAT.ID, UAT.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [enum.User.Access.Type] UAT ON UL.UserAccessType = UAT.ID
        JOIN [System.Rights.UserAccessType.GroupLink] UATLink ON UAT.ID = UATLink.UserAccessType
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = UATLink.IncludedRightsGroupID
        WHERE UL.BID = @BID AND UL.ID = @UserID
    ),
        AllGroups AS
    (
      SELECT DISTINCT GroupID
        , GroupName
        , (SELECT CONCAT([Source], '(', ID, '-', Name, '); ') FROM TopGroup T2 WHERE T2.GroupID = TopGroup.GroupID FOR XML PATH('')) AS Sources
      FROM TopGroup
      GROUP BY GroupID, GroupName

      UNION ALL

      SELECT ChildGroup.ID as GroupID
        , ChildGroup.Name as GroupName
        , CONVERT(NVARCHAR(MAX),CONCAT('Child(', Links.ChildID, '-', ChildGroup.Name,  ')->Parent(', Links.ParentID, '-', AllGroups.GroupName, '); ')) as Sources
      FROM AllGroups 
      JOIN [System.Rights.Group.ChildLink] Links ON AllGroups.GroupID = Links.ParentID
      JOIN [System.Rights.Group] ChildGroup ON ChildGroup.ID = Links.ChildID
    )
        SELECT DISTINCT RightID, R.Name as RightName, AllGroups.GroupID, GroupName, Sources
        From AllGroups
        LEFT JOIN [System.Rights.Group.RightLink] RightLink on RightLink.GroupID = AllGroups.GroupID
        LEFT JOIN [enum.User.Right] R on R.ID = RightLink.RightID
        WHERE RightID IS NOT NULL
        ORDER BY RightID, AllGroups.GroupID
        OFFSET 0 ROWS
)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
