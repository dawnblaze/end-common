﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12028Update_Criteria_column_SystemListFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migration not executing
            migrationBuilder.Sql(@"

IF EXISTS(SELECT 1 FROM [System.List.Filter] WHERE ID = 1049)
	UPDATE [dbo].[System.List.Filter] SET [Criteria] = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>false</SearchValue><Field>IsDeleted</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem><ListFilterItem><SearchValue>false</SearchValue><Field>InSentFolder</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem><ListFilterItem><SearchValue>false</SearchValue><Field>IsRead</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>' WHERE ID = 1049
;

IF EXISTS(SELECT 1 FROM [System.List.Filter] WHERE ID = 1050)
	UPDATE [dbo].[System.List.Filter] SET [Criteria] = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>false</SearchValue><Field>IsDeleted</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem><ListFilterItem><SearchValue>true</SearchValue><Field>InSentFolder</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>' WHERE ID = 1050
;

IF EXISTS(SELECT 1 FROM [System.List.Filter] WHERE ID = 1051)
	UPDATE [dbo].[System.List.Filter] SET [Criteria] = '<ArrayOfListFilterItem><ListFilterItem><SearchValue>true</SearchValue><Field>IsDeleted</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>' WHERE ID = 1051
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // migration not executing
        }
    }
}
