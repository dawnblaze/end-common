﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class CreditMemoSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) VALUES (301, 3015, 'Accounting.CreditMemo.Enable', 'Enable Credit Memos', '', 3, '', 0, 0)
                ;
                INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) VALUES (301, 3016, 'Accounting.CreditMemo.RequiredForGifts', 'Require a Credit Memos for all Credit Gifts', '', 3, '', 0, 0)
                ;
                INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) VALUES (301, 3017, 'Accounting.CreditMemo.RequiredWhenCreditApplied', 'Credit Memo must be specified when Applying Credit to an Order', '', 3, '', 0, 0);
                ;
                UPDATE [dbo].[System.Option.Category]
                SET SearchTerms = CONCAT(SearchTerms, ' Credit Memo CreditMemo Gift Adjustment')
                WHERE ID = 301
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            
        }
    }
}
