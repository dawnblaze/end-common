﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12379Remove_Inactive_StatusTextType_EmailAccount_Filter_Criteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [dbo].[System.List.Filter.Criteria]
   SET [ListValues] = 'Authorized,Pending,Failed,Expired'
 WHERE [TargetClassTypeID] = 1023 AND [Field] = 'StatusTypeText'");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"UPDATE [dbo].[System.List.Filter.Criteria]
   SET [ListValues] = 'Authorized,Pending,Failed,Expired, Inactive'
 WHERE [TargetClassTypeID] = 1023 AND [Field] = 'StatusTypeText'");

        }
    }
}
