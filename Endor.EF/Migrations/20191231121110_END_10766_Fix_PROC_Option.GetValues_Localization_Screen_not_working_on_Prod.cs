﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_10766_Fix_PROC_OptionGetValues_Localization_Screen_not_working_on_Prod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
/* 
Name: [Option.GetValues]

Description: This procedure looks up a option/setting based on a hierarchy of possibilities.

Sample Use:   
    EXEC dbo.[Option.GetValues] @CategoryID = 12, @BID=1, @LocationID = 1

Returns: Table with columns
    [OptionID] INT
    [Name] VARCHAR(255),
    [Value] VARCHAR(255),
    [Level] TINYINT,
    [Label] VARCHAR(255),
    [Description] VARCHAR(MAX),
    [DataType] SMALLINT,
    [CategoryID] SMALLINT,
    [ListValues] VARCHAR(MAX)
    [IsHidden] BIT

    OPTION LEVELS
        ID	Name
        0   Default
        1   System
        2   Association
        4   Business
        8   Location
        16  Storefront
        32  Employee (UserLink)
        64  Company
        128 Contact
*/
-- ========================================================
CREATE OR ALTER PROCEDURE [dbo].[Option.GetValues]
-- DECLARE
	  @CategoryID     smallint     = NULL

	, @BID            smallint     
	, @LocationID     smallint     = NULL
	, @StorefrontID   smallint     = NULL
	, @UserLinkID     smallint     = NULL
	, @CompanyID      int          = NULL
	, @ContactID      int          = NULL
    , @Debug          bit          = 0
AS
BEGIN
    -- Define the Results
    DECLARE @Result TABLE (   [OptionID] INT PRIMARY KEY
                            , [Name] VARCHAR(255)
                            , [Value] NVARCHAR(255) INDEX IX_Result_Value WHERE [Value] IS NULL
                            , [Level] TINYINT
                            , [Label] VARCHAR(255)
                            , [Description] VARCHAR(MAX)
                            , [DataType] SMALLINT
                            , [CategoryID] SMALLINT
                            , [ListValues] VARCHAR(MAX)
                            , [IsHidden] BIT
                            );

    -- Define a list of Categories to JOIN to get the results
    DECLARE @Categories TABLE( ID SMALLINT PRIMARY KEY  );
    IF (@CategoryID IS NULL)
        INSERT INTO @Categories SELECT ID FROM [System.Option.Category]
    ELSE
        INSERT INTO @Categories SELECT @CategoryID
    ;

    -- Now Fill the List with Option Definitions
    INSERT INTO @Result 
        SELECT DEF.[ID] as [OptionID]
            , DEF.[Name]
            , DEF.[DefaultValue] AS [Value] 
            , 0 AS [Level]
            , DEF.[Label]
            , DEF.Description
            , DEF.DataType
            , CAT.ID as CategoryID
            , DEF.ListValues
            , DEF.IsHidden

        FROM @Categories CAT
        JOIN [System.Option.Definition] DEF ON DEF.CategoryID = CAT.ID
    ;

    IF ( IIF(@LocationID   IS NULL, 0, 1) 
        + IIF(@UserLinkID   IS NULL, 0, 1)
        + IIF(@CompanyID    IS NULL AND @ContactID IS NULL, 0, 1)
        + IIF(@StoreFrontID IS NULL, 0, 1) > 1 )
        THROW 180000, 'You can only specify one of the following: @LocationID, @UserLinkID, (@CompanyID and/or @ContactID), or @StorefrontID.', 1;

    -- If a contact is specified but not a Company, take the first Company that is active (sorted alpha) if there are multiple
    IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
        SELECT TOP(1) @CompanyID = Company.ID
                , @LocationID = Company.LocationID
        FROM [Company.Data] Company
        JOIN [Company.Contact.Link] CL ON CL.BID = Company.BID AND CL.CompanyID = Company.ID
        JOIN [Contact.Data] Contact ON Contact.BID = CL.BID AND Contact.ID = CL.ContactID
        WHERE Contact.BID = @BID AND Contact.ID = @ContactID
        ORDER BY CL.IsActive DESC, Company.Name ASC
        ;

    IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
        SELECT @LocationID = Company.LocationID
        FROM [Company.Data] Company
        WHERE Company.BID = @BID AND Company.ID = @CompanyID 
        ;

    IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
        SELECT @LocationID = E.LocationID
        FROM [Employee.Data] E
        JOIN [User.Link] UL on UL.BID = E.BID AND UL.EmployeeID = E.ID
        WHERE UL.BID = @BID AND UL.ID = @UserLinkID
        ;

    -- Iterate through the result set
    DECLARE @OID INT = -999999

    -- Iterate over all Options
    WHILE (1 = 1) 
    BEGIN  

        -- Get next OptionsID
        SELECT TOP 1 @OID = OptionID
        FROM @Result R
        WHERE OptionID > @OID
        ORDER BY OptionID
        ;

        -- Exit loop if no more options
        IF @@ROWCOUNT = 0 BREAK;

        -- Update the Value
        UPDATE RES
        SET [Value] = OPT.[Value]
            , [Level] = OPT.[OptionLevel]
        FROM  @Result RES
        JOIN 
        (
            SELECT TOP(1) O.OptionID, O.Value, O.OptionLevel
            FROM [Option.Data] O
            WHERE O.BID = @BID 
            AND O.OptionID = @OID -- leave this here to get NULL result
            AND ( ( COALESCE(O.ContactID, O.CompanyID, O.StorefrontID, O.LocationID, O.UserLinkID) IS NULL ) OR
                    ( O.ContactID    = @ContactID )
                OR ( O.CompanyID    = @CompanyID )
                OR ( O.StorefrontID = @StorefrontID )
                OR ( O.LocationID   = @LocationID )
                OR ( O.UserLinkID   = @UserLinkID ) 
                )
            ORDER BY O.OptionLevel DESC
        ) OPT ON Opt.OptionID = RES.OptionID
        WHERE RES.OptionID = @OID
        ;
    END

    IF (@Debug=1) SELECT * FROM @Result;

    -- Check Association Separately because it does not use the BID
    UPDATE RES
    SET [Value] = O.[Value] , [Level] = O.[OptionLevel]
    FROM @Result RES
    JOIN [Option.Data] O ON RES.OptionID = O.OptionID
    WHERE RES.[Level] = 0
      AND O.AssociationID = (SELECT AssociationType from [Business.Data] WHERE BID = @BID)

    -- Now return what you found!
    SELECT *
    FROM @Result;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
/* 
Name: [Option.GetValues]

Description: This procedure looks up a option/setting based on a hierarchy of possibilities.

Sample Use:   
    EXEC dbo.[Option.GetValues] @CategoryID = 12, @BID=1, @LocationID = 1

Returns: Table with columns
    [OptionID] INT
    [Name] VARCHAR(255),
    [Value] VARCHAR(255),
    [Level] TINYINT,
    [Label] VARCHAR(255),
    [Description] VARCHAR(MAX),
    [DataType] SMALLINT,
    [CategoryID] SMALLINT,
    [ListValues] VARCHAR(MAX)
    [IsHidden] BIT

    OPTION LEVELS
        ID	Name
        0   Default
        1   System
        2   Association
        4   Business
        8   Location
        16  Storefront
        32  Employee (UserLink)
        64  Company
        128 Contact
*/
-- ========================================================
ALTER   PROCEDURE [dbo].[Option.GetValues]
-- DECLARE
	  @CategoryID     smallint     = NULL

	, @BID            smallint     
	, @LocationID     smallint     = NULL
	, @StorefrontID   smallint     = NULL
	, @UserLinkID     smallint     = NULL
	, @CompanyID      int          = NULL
	, @ContactID      int          = NULL
AS
BEGIN
    -- Define the Results
    DECLARE @Result TABLE (   [OptionID] INT PRIMARY KEY
                            , [Name] VARCHAR(255)
                            , [Value] NVARCHAR(255) INDEX IX_Result_Value WHERE [Value] IS NULL
                            , [Level] TINYINT
                            , [Label] VARCHAR(255)
                            , [Description] VARCHAR(MAX)
                            , [DataType] SMALLINT
                            , [CategoryID] SMALLINT
                            , [ListValues] VARCHAR(MAX)
                            , [IsHidden] BIT
                            );

    -- Define a list of Categories to JOIN to get the results
    DECLARE @Categories TABLE( ID SMALLINT PRIMARY KEY  );
    IF (@CategoryID IS NULL)
        INSERT INTO @Categories SELECT ID FROM [System.Option.Category]
    ELSE
        INSERT INTO @Categories SELECT @CategoryID
    ;

    -- Now Fill the List with Option Definitions
    INSERT INTO @Result 
        SELECT DEF.[ID] as [OptionID]
            , DEF.[Name]
            , DEF.[DefaultValue] AS [Value] 
            , 0 AS [Level]
            , DEF.[Label]
            , DEF.Description
            , DEF.DataType
            , CAT.ID as CategoryID
            , DEF.ListValues
            , DEF.IsHidden

        FROM @Categories CAT
        JOIN [System.Option.Definition] DEF ON DEF.CategoryID = CAT.ID
    ;

    IF ( IIF(@LocationID   IS NULL, 0, 1) 
        + IIF(@UserLinkID   IS NULL, 0, 1)
        + IIF(@CompanyID    IS NULL AND @ContactID IS NULL, 0, 1)
        + IIF(@StoreFrontID IS NULL, 0, 1) > 1 )
        THROW 180000, 'You can only specify one of the following: @LocationID, @UserLinkID, (@CompanyID and/or @ContactID), or @StorefrontID.', 1;

    -- If a contact is specified but not a Company, take the first Company that is active (sorted alpha) if there are multiple
    IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
        SELECT TOP(1) @CompanyID = Company.ID
                , @LocationID = Company.LocationID
        FROM [Company.Data] Company
        JOIN [Company.Contact.Link] CL ON CL.BID = Company.BID AND CL.CompanyID = Company.ID
        JOIN [Contact.Data] Contact ON Contact.BID = CL.BID AND Contact.ID = CL.ContactID
        WHERE Contact.BID = @BID AND Contact.ID = @ContactID
        ORDER BY CL.IsActive DESC, Company.Name ASC
        ;

    IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
        SELECT @LocationID = Company.LocationID
        FROM [Company.Data] Company
        WHERE Company.BID = @BID AND Company.ID = @CompanyID 
        ;

    IF (@UserLinkID IS NOT NULL) AND (@LocationID IS NULL) 
        SELECT @LocationID = E.LocationID
        FROM [Employee.Data] E
        JOIN [User.Link] UL on UL.BID = E.BID AND UL.EmployeeID = E.ID
        WHERE UL.BID = @BID AND UL.ID = @UserLinkID
        ;

    -- Now look up option value based on the specific instance
    UPDATE RES
    SET [Value] = OPT.[Value]
        , [Level] = OPT.[OptionLevel]
    FROM  @Result RES
    JOIN 
    (
        SELECT TOP(1) O.OptionID, O.Value, O.OptionLevel
        FROM [Option.Data] O
        WHERE O.BID = @BID 
        AND ( ( COALESCE(O.ContactID, O.CompanyID, O.StorefrontID, O.LocationID, O.UserLinkID) IS NULL )
            OR ( O.ContactID    = @ContactID )
            OR ( O.CompanyID    = @CompanyID  AND (O.ContactID IS NULL OR @ContactID = O.ContactID) )
            OR ( O.StorefrontID = @StorefrontID AND (O.ContactID IS NULL OR @ContactID = O.ContactID) )
            OR ( O.LocationID   = @LocationID)
            OR ( O.UserLinkID   = @UserLinkID ) AND (O.LocationID IS NULL OR ( O.LocationID   = @LocationID) )
            )
        ORDER BY O.OptionLevel DESC
    ) OPT ON OPT.OptionID = RES.OptionID
    ;

    -- Check Association Separately because it does not use the BID
    UPDATE RES
    SET [Value] = O.[Value] , [Level] = O.[OptionLevel]
    FROM @Result RES
    JOIN [Option.Data] O ON RES.OptionID = O.OptionID
    WHERE RES.[Level] = 0
      AND O.AssociationID = (SELECT AssociationType from [Business.Data] WHERE BID = @BID)

    -- Now return what you found!
    SELECT *
    FROM @Result;
END
");
        }
    }
}
