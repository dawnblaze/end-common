﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11657_update_linkedlabormaterial_to_multiple : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                update v
                set [DataType] = (case when e.ElementType = 104 then -12000 when e.ElementType = 105 then -12020 end)
                from [Part.Subassembly.Variable] v
                inner join [Part.Subassembly.Element] e on v.ID = e.VariableID
                where v.ListValuesJSON is not null
                    and e.ElementType in (104, 105)
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
 
        }
    }
}
