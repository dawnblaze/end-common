﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11892_WiringAccountingOptionsToLocationGoal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                
                /* 
                    This view takes all of the Location Goal data and adds to it so it includes the 
                    Business Goal data (LocationID = NULL) and the annual totals (Month = NULL).

                    This view is read-only.  The Stored Procedure [Location.Goal.CreateOrUpdate]
                    should be used for updating the data.

                    Sample Usage:

                        SELECT * 
                        FROM [Business.Goal]
                        WHERE BID = 1
                        ORDER BY BID, LocationID, FiscalYear, FiscalMonth
                        ;
                */

                

                CREATE OR ALTER  VIEW [dbo].[Business.Goal] AS

                WITH FiscalShift AS
                (
                    -- Retrieve the business option value from Option 3011 
                    SELECT TOP(1) BID, TRY_CONVERT(TINYINT, Value) AS FirstMonth
                    FROM [Option.Data]
                    WHERE OptionID = 3011 AND OptionLevel = 4
                )
                , LocationCalendar AS
                ( 
	                SELECT L.BID, L.LocationID As LocationID
		                ,Y.FiscalYear as  CalendarYear
		                , ((M.FiscalMonth + (SELECT (FirstMonth-1) FROM FiscalShift) + 11) % 12) + 1 AS CalendarMonth
		                , Y.FiscalYear
		                , M.FiscalMonth
     
	                FROM ( SELECT DISTINCT BID, ID as LocationID 
			                FROM [Location.Data] 
			                WHERE ID IS NOT NULL ) as L

 

	                LEFT JOIN ( SELECT DISTINCT BID, CONVERT(SMALLINT, Year) AS FiscalYear 
				                FROM [Location.Goal] 
				                WHERE Year IS NOT NULL ) as Y ON Y.BID = L.BID
      
	                JOIN ( VALUES (CONVERT(TINYINT, 1)),(CONVERT(TINYINT, 2)),(CONVERT(TINYINT, 3)),(CONVERT(TINYINT, 4)),(CONVERT(TINYINT, 5)),(CONVERT(TINYINT, 6))
		                ,(CONVERT(TINYINT, 7)),(CONVERT(TINYINT, 8)),(CONVERT(TINYINT, 9)),(CONVERT(TINYINT, 10)),(CONVERT(TINYINT, 11)),(CONVERT(TINYINT, 12))) AS M(FiscalMonth) ON 1=1

 

	                LEFT JOIN FiscalShift F ON F.BID = L.BID
                )
                , MonthlyGoals AS
                (
                    SELECT LC.BID as BID
                        , CONVERT(SMALLINT, (CASE WHEN Max(LG.ID) IS NULL THEN -ROW_NUMBER() OVER( ORDER BY LC.BID) ELSE Max(LG.ID) END)) AS ID
                        , 1009 as ClassTypeID
                        , COALESCE(MAX(LG.ModifiedDT), GetUTCDate()) as ModifiedDT
                        , CONVERT(tinyint, LC.LocationID) as LocationID
                        , CONVERT(SMALLINT, LC.FiscalYear) AS CalendarYear
                        , CONVERT(tinyint, LC.CalendarMonth) as CalendarMonth
                        , CONVERT(smallint, LC.FiscalYear +  CASE WHEN (LC.CalendarMonth > LC.FiscalMonth) THEN 1 ELSE 0 END) AS FiscalYear
                        , LC.FiscalMonth
                        , SUM(LG.Budgeted) AS Budgeted
                        , (
                                SELECT -SUM(Amount) as Income
                                FROM [Accounting.GL.Data] GL 
                                JOIN [Accounting.GL.Account] GLA ON GL.BID = GLA.BID AND GL.GLAccountID = GLA.ID
                                WHERE GL.BID = LC.BID AND GL.LocationID = LC.LocationID 
                                    AND DatePart(Year, GL.AccountingDT) = (LC.FiscalYear - CASE WHEN (LC.CalendarMonth > LC.FiscalMonth) THEN 1 ELSE 0 END)
                                    AND DatePart(Month, GL.AccountingDT) = LC.CalendarMonth
                                    AND GLA.GLAccountType = 40 -- Income
                            ) AS Actual
  
                    FROM LocationCalendar LC 
                    LEFT JOIN [Location.Goal] LG ON LG.BID = LC.BID AND LG.LocationID = LC.LocationID AND LG.Year = LC.CalendarYear AND LG.Month = LC.CalendarMonth
                    GROUP BY LC.BID, LC.FiscalYear, LC.CalendarMonth
                    --, ROLLUP(LC.FiscalMonth), ROLLUP(LC.LocationID)
                    , LC.FiscalMonth, LC.LocationID
                )
                    SELECT BID, CONVERT(SMALLINT, -ROW_NUMBER() OVER (ORDER BY BID)) AS ID, ClassTypeID, ModifiedDT, CONVERT(tinyint, LocationID) as LocationID, CONVERT(smallint, CalendarYear) as CalendarYear, CONVERT(tinyint, CalendarMonth) as CalendarMonth, FiscalYear, CONVERT(tinyint, FiscalMonth) as FiscalMonth, Budgeted, Actual
                        , (CASE WHEN Budgeted = 0 THEN NULL ELSE(Actual / Budgeted)*(100.0) end) AS PercentOfGoal
                        , CONVERT(BIT, 0) AS IsYearlyTotal 
                        , CONVERT(BIT, 0) AS IsBusinessTotal
                    FROM MonthlyGoals

                    --Yearly Totals for Each Locations
                    UNION ALL
                    SELECT BID, CONVERT(SMALLINT, -ROW_NUMBER() OVER (ORDER BY BID)), ClassTypeID, GetUTCDate(),  CONVERT(tinyint, LocationID) as LocationID, Convert(smallint, -1), NULL, FiscalYear, NULL
                        , SUM(Budgeted)
                        , SUM(Actual)
                        , (CASE WHEN SUM(Budgeted) = 0 THEN NULL ELSE(SUM(Actual) / SUM(Budgeted))*(100.0) end) AS PercentOfGoal 
                        , CONVERT(BIT, 1), CONVERT(BIT, 0)
                    FROM MonthlyGoals
                    GROUP BY BID, ClassTypeID, LocationID, FiscalYear

                    -- Monthly Totals for All Locations
                    UNION ALL
                    SELECT BID, CONVERT(SMALLINT, -ROW_NUMBER() OVER (ORDER BY BID)), ClassTypeID, NULL, NULL, NULL, NULL, FiscalYear, CONVERT(tinyint, FiscalMonth) as FiscalMonth
                        , SUM(Budgeted)
                        , SUM(Actual)
                        , (CASE WHEN SUM(Budgeted) = 0 THEN NULL ELSE(SUM(Actual) / SUM(Budgeted))*(100.0) end) AS PercentOfGoal 
                        , CONVERT(BIT, 0), CONVERT(BIT, 1)
                    FROM MonthlyGoals
                    GROUP BY BID, ClassTypeID, FiscalMonth, FiscalYear


                    -- Yearly Totals for All Locations
                    UNION ALL
                    SELECT BID, CONVERT(SMALLINT, -ROW_NUMBER() OVER (ORDER BY BID)), ClassTypeID, NULL, NULL, NULL, NULL, FiscalYear, NULL
                        , SUM(Budgeted)
                        , SUM(Actual)
                        , (CASE WHEN SUM(Budgeted) = 0 THEN NULL ELSE(SUM(Actual) / SUM(Budgeted))*(100.0) end) AS PercentOfGoal 
                        , CONVERT(BIT, 1), CONVERT(BIT, 1)
                    FROM MonthlyGoals
                    GROUP BY BID, ClassTypeID, FiscalYear
                    ;
                GO

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
