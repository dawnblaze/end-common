﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Add_Custom_Field_Data_Views : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Company.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, C.ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [Company.Custom.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Contact.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, C.ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [Contact.Custom.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Opportunity.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, C.ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [Opportunity.Custom.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Order.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, C.ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [Order.Custom.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Assembly.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, C.ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [CustomField.Other.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
    WHERE  AppliesToClassTypeID = 12040
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Employee.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, CONVERT(SMALLINT, C.ID) ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [CustomField.Other.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
    WHERE  AppliesToClassTypeID = 5000
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Labor.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, C.ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [CustomField.Other.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
    WHERE  AppliesToClassTypeID = 12020
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Location.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, CONVERT(TINYINT, C.ID) ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [CustomField.Other.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
    WHERE  AppliesToClassTypeID = 1005
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Machine.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, CONVERT(SMALLINT, C.ID) ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [CustomField.Other.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
    WHERE  AppliesToClassTypeID = 12030
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");

            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Material.Custom.Data.Value]
AS
WITH CustomFieldList AS
(
    SELECT C.BID, C.ID, C.AppliesToClassTypeID
           , CFV.value('./ID[1]', 'SMALLINT') CustomFieldDefID
           , CFV.value('./V[1]', 'VARCHAR(MAX)') Value
    FROM   [CustomField.Other.Data] C
           CROSS APPLY DataXML.nodes('/*') AS XML(CFV)
    WHERE  AppliesToClassTypeID = 12000
)
SELECT BID, ID, AppliesToClassTypeID ClassTypeID, CustomFieldDefID, Value
       , TRY_CONVERT(DECIMAL(18,4), Value) ValueAsNumber
       , CASE
             WHEN Value IS NULL THEN NULL
             WHEN LEN(Value) = 0 THEN NULL
             WHEN (SUBSTRING(Value, 1, 1)) IN ('1', 't', 'y') THEN CAST(1 AS BIT)
             ELSE CAST(0 AS BIT)
         END ValueAsBoolean
FROM   CustomFieldList
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
