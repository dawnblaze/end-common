﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddCopyAllDefaultsSPROC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"/* 
-- ========================================================

    Procedure Name: [Util.Table.CopyDefaultRecords] 

    Description: This function copys all of the -1 and -2 records for a table 
                    into all of the BIDs for which they don't exist.
                    It then deletes all the BID=-2 records.

    Note: @IDFieldName is not used but kept for backwards compatibility. The correct field is looked up automatically.

    Sample Use:   

        EXEC [Util.Table.CopyDefaultRecords] @TableName = '_Root.Data';
        
-- ========================================================
*/
CREATE OR ALTER PROCEDURE [dbo].[Util.Table.CopyDefaultRecords] 
    (@TableName NVARCHAR(200), @IncludeRequired BIT = 1, @IncludeDefault BIT = 1, @TargetBID SMALLINT = NULL, @IDFieldName VARCHAR(100) = 'DEPRECATED')
AS
BEGIN
    -- DECLARE @TableName NVARCHAR(200) = '_Root.Data';
    -- DECLARE @IncludeRequired BIT = 1;
    -- DECLARE @IncludeDefault BIT = 1

    DECLARE @BIDS NVARCHAR(30) = IIF(@IncludeRequired=1, IIF(@IncludeDefault=1, '(-1, -2)', '(-1)'), IIF(@IncludeDefault=1, '(-2)', '(null)'))
          , @TargetBIDString NVARCHAR(12) = IIF(@TargetBID IS NULL, ' > 0 ', CONCAT(' = ', @TargetBID))
          , @Columns NVARCHAR(MAX)
          , @PrimaryKeyMatch NVARCHAR(2000)
          ;

    -- Generate the Column List
    SELECT @Columns = CONCAT(@Columns + ', ', 'T.[',ColumnName,']')
    FROM TablesAndColumns
    WHERE TableName = @TableName
        AND ColumnName NOT IN ('BID', 'ValidToDT', 'ModifiedDT')
        AND IsComputed = 0
        AND IsIdentity = 0
    ;
    --PRINT @Columns;

    -- Generate the Primary Key Fields Match
    SELECT @PrimaryKeyMatch = CONCAT( @PrimaryKeyMatch + ' AND ', 'D.[',TC.Name,'] = T.[',TC.Name,']' )
    FROM sys.tables T
    JOIN sys.indexes I  on T.object_id=I.object_id
    JOIN sys.index_columns IC on I.object_id = IC.object_id AND I.index_id = IC.index_id
    JOIN sys.columns TC on IC.object_id=TC.object_id AND IC.column_id=TC.column_id
    WHERE T.name = @TableName 
        AND I.is_primary_key=1 
        AND TC.Name != 'BID'
    ORDER BY IC.key_ordinal
    ;
    --PRINT @PrimaryKeyMatch;

    DECLARE @SQL NVARCHAR(MAX) = '

    INSERT INTO [@TABLENAME] ( BID, '+@Columns+' )
        SELECT B.BID, '+@Columns+'
        FROM [Business.Data] B
        JOIN 
        (   SELECT '+@Columns+'
            FROM [@TABLENAME] T
            WHERE BID IN '+@BIDS+'
        ) T ON 1=1
        WHERE NOT EXISTS (SELECT * FROM [@TABLENAME] D WHERE D.BID = B.BID AND '+@PrimaryKeyMatch+')
            AND B.BID'+@TargetBIDString+'
    ;
    ';

    IF (@IncludeDefault=1)
        SET @SQL += '
    DELETE D
    FROM [@TABLENAME] D
    JOIN [@TABLENAME] T ON '+@PrimaryKeyMatch+'
    WHERE D.BID = -3
      AND T.BID = -2
    ;

    UPDATE [@TABLENAME]
    SET BID = -3
    WHERE BID = -2
    ;
    ';

    SET @SQL = REPLACE(@SQL, '@TABLENAME', @TableName);
    PRINT @SQL

    EXECUTE(@SQL);
END;


            ");

            migrationBuilder.Sql(@"
/* 
-- ========================================================

    Procedure Name: [Util.Table.CopyAllDefaultRecords] 

    Description: This function copys all of the -1 and -2 records for ALL tables
                    into all of the BIDs for which they don't exist.
                    It then deletes all the BID=-2 records.
                    It calls [Util.Table.CopyDefaultRecords] for each table.

    Sample Use:   

        EXEC [Util.Table.CopyAllDefaultRecords] ;
        
-- ========================================================
*/
CREATE OR ALTER PROCEDURE [dbo].[Util.Table.CopyAllDefaultRecords] 
    (@IncludeRequired BIT = 1, @IncludeDefault BIT = 1, @FailedTables VARCHAR(MAX) = NULL OUTPUT)
AS
BEGIN

    -- DECLARE @IncludeRequired BIT = 1
    --       , @IncludeDefault BIT = 1
    --       , @TargetBID SMALLINT = NULL
    --       , @FailedTables NVARCHAR(MAX)

    SET NOCOUNT ON;

    DECLARE @TableName NVARCHAR(200)
          , @ColumnName NVARCHAR(200)
          , @SQL nvarchar(250)
          , @Result int
          ;

    -- Generate a list of Table Names that contain a BID
    DECLARE TableList CURSOR FOR
        SELECT DISTINCT TableName
        FROM TablesAndColumns T1
        WHERE T1.ColumnName = 'BID'
          AND T1.TableName NOT LIKE '%Historic%'
          AND T1.TableName NOT LIKE '%Business.Data%'
          AND T1.TableName NOT LIKE '%Option.Data%'
          AND T1.TableName NOT LIKE '%Root.Data%'
        ;

    OPEN TableList
    ;

    FETCH NEXT FROM TableList INTO @TableName
    ;
    
    WHILE (@@FETCH_STATUS = 0)
    BEGIN
        -- Confirm that we have any -1 Records in that table, and skip if not
        SET @SQL = CONCAT('SELECT @Result=COUNT(*) FROM [',@TableName,'] WHERE BID < 0');
        EXECUTE sp_executesql @SQL, N'@Result int OUTPUT', @Result = @Result OUTPUT;
        IF (@Result = 0) 
        BEGIN
            --PRINT CONCAT('No Results for Table ',@TableName);
            FETCH NEXT FROM TableList INTO @TableName;
            CONTINUE;
        END;

        BEGIN TRY
            EXEC [Util.Table.CopyDefaultRecords]
                  @TableName = @TableName
                , @IncludeRequired = @IncludeRequired
                , @IncludeDefault = @IncludeDefault
            ;
            --PRINT @TableName + ' Executed';
        END TRY 
        -- if we have an error, just record the table name
        BEGIN CATCH
            SET @FailedTables = CONCAT(@FailedTables + ', ', @TableName);
            PRINT @TableName + ': ' + ERROR_MESSAGE() + CHAR(10) + CHAR(13);
        END CATCH

        FETCH NEXT FROM TableList INTO @TableName
        ;

    END;

    CLOSE TableList;
    DEALLOCATE TableList;

    IF (@FailedTables IS NOT NULL)
        PRINT 'Failed Tables: '+@FailedTables;
END;
            ");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
