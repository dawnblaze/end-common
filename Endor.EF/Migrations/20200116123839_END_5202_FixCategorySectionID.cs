﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_5202_FixCategorySectionID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Category]
SET SectionID = 2100 
WHERE ID = 107;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.Option.Category]
SET SectionID = 100 
WHERE ID = 107;
");
        }
    }
}
