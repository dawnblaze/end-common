﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12014_Clear_Assemblies : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }

        public override bool ClearAllAssemblies()
        {
            return true;
        }
    }
}
