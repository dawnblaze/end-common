﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12638_UpdateReconciliationPaymentInfoByRange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* =============================================
    Table Value Function: [Accounting.Reconciliation.PaymentInfo.ByRange]
    Returns the PaymentInfo object for the specified GL Range and Date Range.
 
    See https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1168736257/ReconciliationPaymentInfo+Object
    for the structure of the response.
 
    Sample Usage:
     
        DECLARE @BID SMALLINT = 1
            , @LocationID SMALLINT = (SELECT TOP 1 LocationID FROM [Accounting.GL.Data] WHERE BID = 1 ORDER BY ID DESC)
            , @StartingGLID INT = 0
            , @EndingGLID INT = 999999
            , @StartingAccountingDT DateTime2(2) = '1/1/1970'
            , @EndingAccountingDT DateTime2(2) = '2020-03-04'
            , @OrderID INT = (SELECT TOP 1 ID FROM [Order.Data] WHERE BID = 1 AND [Payment.BalanceDue] > 0)
            , @ActivityID INT = (SELECT TOP 1 ID FROM [Activity.GLActivity] WHERE BID = 1)
        ;

        -- Insert some bad debt records
        INSERT INTO [Accounting.GL.Data]
        (BID, ID, LocationID, AccountingDT, Amount, CurrencyType, GLAccountID, IsOffBS, IsTaxable, OrderID, ActivityID )
        VALUES
        (@BID, 995, @LocationID, DateAdd(Second, -1, @EndingAccountingDT), 50, 0, 6200, 0, 0, @OrderID, @ActivityID)
        ,(@BID, 996, @LocationID, DateAdd(Second, -1, @EndingAccountingDT), -50, 0, 1200, 0, 0, @OrderID, @ActivityID)
        ;

        SELECT * FROM [Accounting.Reconciliation.PaymentInfo.ByRange](@BID, @LocationID, @StartingGLID, @EndingGLID, @StartingAccountingDT, @EndingAccountingDT)
        ;
 
-- ============================================= */
CREATE OR ALTER FUNCTION [Accounting.Reconciliation.PaymentInfo.ByRange]
(  
      @BID SMALLINT
    , @LocationID SMALLINT
    , @StartingGLID INT
    , @EndingGLID INT
    , @StartingAccountingDT DateTime2(2)
    , @EndingAccountingDT DateTime2(2)
)
RETURNS TABLE
AS
RETURN
(
    WITH GLList AS
    (
        SELECT GL.BID
        , GL.LocationID
        , GL.ID as GLID
        , GL.AccountingDT
        , GL.Amount
        , GL.CurrencyType
        , GL.OrderID
        , PTType.Name AS PaymentTransactionType
        , (CASE WHEN PM.PaymentMethodType=0 THEN 'Custom'
                WHEN PM.PaymentMethodType=1 THEN 'Cash'
                WHEN PM.PaymentMethodType=2 THEN 'Check'
                WHEN PM.PaymentMethodType IN (3,4) THEN 'ACH/EFT'
                WHEN PM.PaymentMethodType IN (5,6,7,8,9) THEN 'Credit Card'
                WHEN PM.PaymentMethodType IN (250, 251) THEN 'In-Store Credit'
                WHEN PM.PaymentMethodType=252 THEN 'Bad Debt'
            ELSE 'Custom' END) AS PaymentMethodType
        , (CASE WHEN PM.PaymentMethodType=0 THEN 22
                WHEN PM.PaymentMethodType=1 THEN 11
                WHEN PM.PaymentMethodType=2 THEN 12
                WHEN PM.PaymentMethodType IN (3,4) THEN 13
                WHEN PM.PaymentMethodType IN (5,6,7,8,9) THEN 21
                WHEN PM.PaymentMethodType IN (250, 251) THEN 101
                WHEN PM.PaymentMethodType=252 THEN 102
            ELSE 22 END) AS SortIndex
        , PM.Name AS PaymentMethod
        , GLA.Name AS GLAccountName
        -- , GL.PaymentMethodID -- not filled in?
        -- , PA.PaymentType  -- not filled int
        , PA.ReceivedLocationID
        , GL.PaymentID
        , PA.MasterID as MasterPaymentID
        , PA.PaymentTransactionType AS PaymentTransactionTypeID
        , PA.PaymentMethodID
        , PM.PaymentMethodType AS PaymentMethodTypeID
        , GL.GLAccountID
        , PA.Amount as PaymentAmount
        , PA.HasAdjustments
        FROM [Accounting.GL.Data] GL
        JOIN [Accounting.GL.Account] GLA ON GL.BID = GLA.BID AND GL.GLAccountID = GLA.ID
        JOIN [Accounting.Payment.Application] PA on PA.BID = GL.BID AND PA.ID = GL.PaymentID
        JOIN [Accounting.Payment.Method] PM on PM.BID = PA.BID AND PM.ID = PA.PaymentMethodID
        JOIN [enum.Accounting.GLAccountType] GLType ON GLType.ID = GLA.GLAccountType
        --    JOIN [enum.Accounting.PaymentMethodType] PMType ON PMType.ID = PM.PaymentMethodType
        JOIN [enum.Accounting.PaymentTransactionType] PTType ON PTType.ID = PA.PaymentTransactionType
        WHERE GL.BID = @BID AND GL.LocationID = @LocationID
            AND GL.ID >= @StartingGLID AND GL.ID < @EndingGLID
            AND GL.AccountingDT >= @StartingAccountingDT
            AND GL.AccountingDT < @EndingAccountingDT
        AND GL.PaymentID IS NOT NULL  -- Only for Payments
        -- 2020-03-04 Change here to keep Instore Credit
        AND (
               (GLA.GLAccountType in (10,11,40,50,60))   -- And Only for the Bank Account and Bad Debt
               OR (GL.GLAccountID = 2200 AND PaymentMethodType >= 250)   -- Customer Credit Transactions
             ) -- 
    ),
    BadDebt AS
    (
        SELECT GL.LocationID, GL.CurrencyType, GL.Amount, GLA.Name as GLAccountName, GL.ActivityID, GL.OrderID
        FROM [Accounting.GL.Data] GL
        JOIN [Accounting.GL.Account] GLA ON GL.BID = GLA.BID AND GL.GLAccountID = GLA.ID
        WHERE GL.BID = @BID AND GL.LocationID = @LocationID
            AND GL.ID >= @StartingGLID AND GL.ID < @EndingGLID
            AND GL.AccountingDT >= @StartingAccountingDT
            AND GL.AccountingDT < @EndingAccountingDT
        AND GL.GLAccountID = 6200 -- Bad Debt
    )

        SELECT LocationID
        , CurrencyType
        , PaymentMethodType
        , SUM(SUM(Amount)) OVER (PARTITION BY PaymentMethodType) as PaymentMethodTypeAmount
        , PaymentMethod
        , SUM(Amount) AS PaymentMethodAmount
        , PaymentTransactionType, GLAccountName, ReceivedLocationID
        , PaymentMethodID, PaymentTransactionTypeID
        , COUNT(DISTINCT MasterPaymentID) AS PaymentCount
        , COUNT(DISTINCT OrderID) AS OrderCount
        -- 2020-03-04 Change here to de-duplicate the list
        , STUFF((SELECT T.PaymentID
                FROM (SELECT DISTINCT PaymentID
                        FROM GLList G2 
                        WHERE G1.LocationID = G2.LocationID AND G1.PaymentMethodID = G2.PaymentMethodID AND G1.PaymentTransactionTypeID = G2.PaymentTransactionTypeID 
                    ) T
                FOR XML PATH('')), 1, 1, '') AS PaymentIDsString
        , SortIndex
        FROM GLList G1
        GROUP BY LocationID, CurrencyType, PaymentTransactionType, PaymentMethod, PaymentMethodType, GLAccountName, ReceivedLocationID, SortIndex
        , PaymentTransactionType, PaymentMethodID, PaymentTransactionTypeID

        UNION ALL

        SELECT LocationID, CurrencyType, 'Bad Debt', SUM(Amount), 'Bad Debt' AS PaymentMethod, SUM(Amount), 'Bad Debt', GLAccountName
            , LocationID, NULL, NULL AS PaymentTransactionTypeID
            , COUNT(DISTINCT ActivityID) AS PaymentCount
            , COUNT(DISTINCT OrderID) AS OrderCount
            , STUFF((SELECT STRING_AGG(T.OrderID, ',')
                FROM (SELECT DISTINCT OrderID
                            FROM BadDebt B2 
                            WHERE B1.LocationID = B2.LocationID
                        ) T
                FOR XML PATH('')), 1, 1, '') AS OrderIDsString
            , 255 AS SortIndex
        FROM BadDebt B1
        GROUP BY LocationID, CurrencyType, GLAccountName

        ORDER BY SortIndex, PaymentTransactionTypeID, PaymentMethod
        OFFSET 0 ROWS
);

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"\
/* =============================================
    Table Value Function: [Accounting.Reconciliation.PaymentInfo.ByRange]
    Returns the PaymentInfo object for the specified GL Range and Date Range.
 
    See https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1168736257/ReconciliationPaymentInfo+Object
    for the structure of the response.
 
    Sample Usage:
     
        SELECT * FROM
        SELECT * FROM [Accounting.Reconciliation.PaymentInfo.ByRange](1, 45, 2195, 3073, '1970-01-01 00:00:00', '2020-02-05 12:56:43' )
 
-- ============================================= */
ALTER   FUNCTION [dbo].[Accounting.Reconciliation.PaymentInfo.ByRange]
(  
      @BID SMALLINT
    , @LocationID SMALLINT
    , @StartingGLID INT
    , @EndingGLID INT
    , @StartingAccountingDT DateTime2(2)
    , @EndingAccountingDT DateTime2(2)
)
RETURNS TABLE
AS
RETURN
(
    WITH GLList AS
    (
        SELECT GL.BID
        , GL.LocationID
        , GL.ID as GLID
        , GL.AccountingDT
        , GL.Amount
        , GL.CurrencyType
        , GL.OrderID
        , PTType.Name AS PaymentTransactionType
        , (CASE WHEN PM.PaymentMethodType=0 THEN 'Custom'
                WHEN PM.PaymentMethodType=1 THEN 'Cash'
                WHEN PM.PaymentMethodType=2 THEN 'Check'
                WHEN PM.PaymentMethodType IN (3,4) THEN 'ACH/EFT'
                WHEN PM.PaymentMethodType IN (5,6,7,8,9) THEN 'Credit Card'
                WHEN PM.PaymentMethodType IN (250, 251) THEN 'In-Store Credit'
                WHEN PM.PaymentMethodType=252 THEN 'Bad Debt'
            ELSE 'Custom' END) AS PaymentMethodType
        , (CASE WHEN PM.PaymentMethodType=0 THEN 22
                WHEN PM.PaymentMethodType=1 THEN 11
                WHEN PM.PaymentMethodType=2 THEN 12
                WHEN PM.PaymentMethodType IN (3,4) THEN 13
                WHEN PM.PaymentMethodType IN (5,6,7,8,9) THEN 21
                WHEN PM.PaymentMethodType IN (250, 251) THEN 101
                WHEN PM.PaymentMethodType=252 THEN 102
            ELSE 22 END) AS SortIndex
        , PM.Name AS PaymentMethod
        , GLA.Name AS GLAccountName
        -- , GL.PaymentMethodID -- not filled in?
        -- , PA.PaymentType  -- not filled int
        , PA.ReceivedLocationID
        , GL.PaymentID
        , PA.MasterID as MasterPaymentID
        , PA.PaymentTransactionType AS PaymentTransactionTypeID
        , PA.PaymentMethodID
        , PM.PaymentMethodType AS PaymentMethodTypeID
        , GL.GLAccountID
        , PA.Amount as PaymentAmount
        , PA.HasAdjustments
        FROM [Accounting.GL.Data] GL
        JOIN [Accounting.GL.Account] GLA ON GL.BID = GLA.BID AND GL.GLAccountID = GLA.ID
        JOIN [Accounting.Payment.Application] PA on PA.BID = GL.BID AND PA.ID = GL.PaymentID
        JOIN [Accounting.Payment.Method] PM on PM.BID = PA.BID AND PM.ID = PA.PaymentMethodID
        JOIN [enum.Accounting.GLAccountType] GLType ON GLType.ID = GLA.GLAccountType
        --    JOIN [enum.Accounting.PaymentMethodType] PMType ON PMType.ID = PM.PaymentMethodType
        JOIN [enum.Accounting.PaymentTransactionType] PTType ON PTType.ID = PA.PaymentTransactionType
        WHERE GL.BID = @BID AND GL.LocationID = @LocationID
            AND GL.ID >= @StartingGLID AND GL.ID < @EndingGLID
            AND GL.AccountingDT >= @StartingAccountingDT
            AND GL.AccountingDT < @EndingAccountingDT
        AND GL.PaymentID IS NOT NULL  -- Only for Payments
        AND GLA.GLAccountType in (10,11,40,50,60)   -- And Only for the Bank Account and Bad Debt
    )
        SELECT LocationID
        , CurrencyType
        , PaymentMethodType
        , SUM(SUM(Amount)) OVER (PARTITION BY PaymentMethodType) as PaymentMethodTypeAmount
        , PaymentMethod
        , SUM(Amount) AS PaymentMethodAmount
        , PaymentTransactionType, GLAccountName, ReceivedLocationID
        , PaymentMethodID, PaymentTransactionTypeID
        , COUNT(DISTINCT MasterPaymentID) AS PaymentCount
        , COUNT(DISTINCT OrderID) AS OrderCount
        , STUFF((SELECT DISTINCT CONCAT(',', PaymentID) FROM GLList G2 WHERE G1.LocationID = G2.LocationID AND G1.PaymentMethodID = G2.PaymentMethodID AND G1.PaymentTransactionTypeID = G2.PaymentTransactionTypeID FOR XML PATH('')), 1, 1, '') AS PaymentIDsString
        , SortIndex
        FROM GLList G1
        GROUP BY LocationID, CurrencyType, PaymentTransactionType, PaymentMethod, PaymentMethodType, GLAccountName, ReceivedLocationID, SortIndex
        , PaymentTransactionType, PaymentMethodID, PaymentTransactionTypeID
        ORDER BY SortIndex, PaymentTransactionTypeID, PaymentMethod
        OFFSET 0 ROWS
);
");
        }
    }
}
