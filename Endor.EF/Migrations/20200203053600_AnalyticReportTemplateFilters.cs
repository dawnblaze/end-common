﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AnalyticReportTemplateFilters : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(
                @"
                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+1 FROM [dbo].[System.List.Filter]), 1, 'Financial', 13010, 1, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>10</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')

                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+2 FROM [dbo].[System.List.Filter]), 1, 'Sales', 13010, 2, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>30</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')
                                
                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+3 FROM [dbo].[System.List.Filter]), 1, 'Purchases', 13010, 3, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>50</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')
                    
                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+4 FROM [dbo].[System.List.Filter]), 1, 'Accounting', 13010, 4, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>70</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')

                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+5 FROM [dbo].[System.List.Filter]), 1, 'Inventory', 13010, 5, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>90</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')

                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+6 FROM [dbo].[System.List.Filter]), 1, 'Production', 13010, 6, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>110</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')

                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+7 FROM [dbo].[System.List.Filter]), 1, 'Business', 13010, 7, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>130</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')

                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+8 FROM [dbo].[System.List.Filter]), 1, 'CRM', 13010, 8, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>150</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')

                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+9 FROM [dbo].[System.List.Filter]), 1, 'User', 13010, 9, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>170</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')

                    INSERT INTO [dbo].[System.List.Filter] ([ID], [IsActive], [Name], [TargetClassTypeID], [SortIndex], [Criteria])
                    VALUES ((SELECT MAX(ID)+10 FROM [dbo].[System.List.Filter]), 1, 'Ecommerce', 13010, 10, '<ArrayOfListFilterItem><ListFilterItem><SearchValue>230</SearchValue><Field>AnalyticReportType</Field><IsHidden>true</IsHidden><IsSystem>true</IsSystem></ListFilterItem></ArrayOfListFilterItem>')
                ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
               DELETE FROM [dbo].[System.List.Filter] WHERE [TargetClassTypeID] = 13010
            ");
        }
    }
}