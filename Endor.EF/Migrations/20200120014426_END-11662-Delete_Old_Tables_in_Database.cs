﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11662Delete_Old_Tables_in_Database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
    DROP TABLE IF EXISTS [Report.Menu];
    DROP TABLE IF EXISTS [_Root.Data];
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
