﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_12275_AddReconciliationPaymentInfoTVFs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
/* =============================================
    Table Value Function: [Accounting.Reconciliation.PaymentInfo.ByRange]
    Returns the PaymentInfo object for the specified GL Range and Date Range.
 
    See https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1168736257/ReconciliationPaymentInfo+Object
    for the structure of the response.
 
    Sample Usage:
     
        SELECT * FROM
        SELECT * FROM [Accounting.Reconciliation.PaymentInfo.ByRange](1, 45, 2195, 3073, '1970-01-01 00:00:00', '2020-02-05 12:56:43' )
 
-- ============================================= */
CREATE OR ALTER FUNCTION [Accounting.Reconciliation.PaymentInfo.ByRange]
(  
      @BID SMALLINT
    , @LocationID SMALLINT
    , @StartingGLID INT
    , @EndingGLID INT
    , @StartingAccountingDT DateTime2(2)
    , @EndingAccountingDT DateTime2(2)
)
RETURNS TABLE
AS
RETURN
(
    WITH GLList AS
    (
        SELECT GL.BID
        , GL.LocationID
        , GL.ID as GLID
        , GL.AccountingDT
        , GL.Amount
        , GL.CurrencyType
        , GL.OrderID
        , PTType.Name AS PaymentTransactionType
        , (CASE WHEN PM.PaymentMethodType=0 THEN 'Custom'
                WHEN PM.PaymentMethodType=1 THEN 'Cash'
                WHEN PM.PaymentMethodType=2 THEN 'Check'
                WHEN PM.PaymentMethodType IN (3,4) THEN 'ACH/EFT'
                WHEN PM.PaymentMethodType IN (5,6,7,8,9) THEN 'Credit Card'
                WHEN PM.PaymentMethodType IN (250, 251) THEN 'In-Store Credit'
                WHEN PM.PaymentMethodType=252 THEN 'Bad Debt'
            ELSE 'Custom' END) AS PaymentMethodType
        , (CASE WHEN PM.PaymentMethodType=0 THEN 22
                WHEN PM.PaymentMethodType=1 THEN 11
                WHEN PM.PaymentMethodType=2 THEN 12
                WHEN PM.PaymentMethodType IN (3,4) THEN 13
                WHEN PM.PaymentMethodType IN (5,6,7,8,9) THEN 21
                WHEN PM.PaymentMethodType IN (250, 251) THEN 101
                WHEN PM.PaymentMethodType=252 THEN 102
            ELSE 22 END) AS SortIndex
        , PM.Name AS PaymentMethod
        , GLA.Name AS GLAccountName
        -- , GL.PaymentMethodID -- not filled in?
        -- , PA.PaymentType  -- not filled int
        , PA.ReceivedLocationID
        , GL.PaymentID
        , PA.MasterID as MasterPaymentID
        , PA.PaymentTransactionType AS PaymentTransactionTypeID
        , PA.PaymentMethodID
        , PM.PaymentMethodType AS PaymentMethodTypeID
        , GL.GLAccountID
        , PA.Amount as PaymentAmount
        , PA.HasAdjustments
        FROM [Accounting.GL.Data] GL
        JOIN [Accounting.GL.Account] GLA ON GL.BID = GLA.BID AND GL.GLAccountID = GLA.ID
        JOIN [Accounting.Payment.Application] PA on PA.BID = GL.BID AND PA.ID = GL.PaymentID
        JOIN [Accounting.Payment.Method] PM on PM.BID = PA.BID AND PM.ID = PA.PaymentMethodID
        JOIN [enum.Accounting.GLAccountType] GLType ON GLType.ID = GLA.GLAccountType
        --    JOIN [enum.Accounting.PaymentMethodType] PMType ON PMType.ID = PM.PaymentMethodType
        JOIN [enum.Accounting.PaymentTransactionType] PTType ON PTType.ID = PA.PaymentTransactionType
        WHERE GL.BID = @BID AND GL.LocationID = @LocationID
            AND GL.ID >= @StartingGLID AND GL.ID < @EndingGLID
            AND GL.AccountingDT >= @StartingAccountingDT
            AND GL.AccountingDT < @EndingAccountingDT
        AND GL.PaymentID IS NOT NULL  -- Only for Payments
        AND GLA.GLAccountType in (10,11,40,50,60)   -- And Only for the Bank Account and Bad Debt
    )
        SELECT LocationID
        , CurrencyType
        , PaymentMethodType
        , SUM(SUM(Amount)) OVER (PARTITION BY PaymentMethodType) as PaymentMethodTypeAmount
        , PaymentMethod
        , SUM(Amount) AS PaymentMethodAmount
        , PaymentTransactionType, GLAccountName, ReceivedLocationID
        , PaymentMethodID, PaymentTransactionTypeID
        , COUNT(DISTINCT MasterPaymentID) AS PaymentCount
        , COUNT(DISTINCT OrderID) AS OrderCount
        , STUFF((SELECT DISTINCT CONCAT(',', PaymentID) FROM GLList G2 WHERE G1.LocationID = G2.LocationID AND G1.PaymentMethodID = G2.PaymentMethodID AND G1.PaymentTransactionTypeID = G2.PaymentTransactionTypeID FOR XML PATH('')), 1, 1, '') AS PaymentIDsString
        , SortIndex
        FROM GLList G1
        GROUP BY LocationID, CurrencyType, PaymentTransactionType, PaymentMethod, PaymentMethodType, GLAccountName, ReceivedLocationID, SortIndex
        , PaymentTransactionType, PaymentMethodID, PaymentTransactionTypeID
        ORDER BY SortIndex, PaymentTransactionTypeID, PaymentMethod
        OFFSET 0 ROWS
);
");
            migrationBuilder.Sql(@"
/* =============================================
    Table Value Function: [Accounting.Reconciliation.PaymentInfo.ByID]
    Returns the PaymentInfo object for the specified Reconciliation ID
 
    See https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1168736257/ReconciliationPaymentInfo+Object
    for the structure of the response.
 
    Sample Usage:
     
        SELECT * FROM [Accounting.Reconciliation.PaymentInfo.ByID](1, 1017 )
 
-- ============================================= */
CREATE OR ALTER FUNCTION [Accounting.Reconciliation.PaymentInfo.ByID]
(  
      @BID SMALLINT
    , @ReconciliationID INT
)
RETURNS TABLE
AS
RETURN
(
    WITH R AS
    (
        SELECT *
        FROM [Accounting.Reconciliation.Data]
        WHERE BID = @BID AND ID = @ReconciliationID
    )
      SELECT PI.*
      FROM [Accounting.Reconciliation.PaymentInfo.ByRange]
      (
              @BID
            , (SELECT LocationID FROM R)
            , (SELECT StartingGLID FROM R)
            , (SELECT EndingGLID FROM R)
            , (SELECT LastAccountingDT FROM R)
            , (SELECT AccountingDT FROM R)
       ) PI
    
);
");
            migrationBuilder.Sql(@"
/* =============================================
    Table Value Function: [Accounting.Reconciliation.PaymentInfo.Preview]
    Returns the PaymentInfo object for the Preview for a Specified Location.
     
    See https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1168736257/ReconciliationPaymentInfo+Object
    for the structure of the response.
 
    Sample Usage:
     
        SELECT * FROM [Accounting.Reconciliation.PaymentInfo.Preview]( 1, 45 );
 
-- ============================================= */
CREATE OR ALTER FUNCTION [Accounting.Reconciliation.PaymentInfo.Preview]
(  
      @BID SMALLINT
    , @LocationID INT
    , @EndingDT DateTime2(2) = NULL
)
RETURNS TABLE
AS
RETURN
(
    WITH R AS
    (
        SELECT COALESCE(MAX(EndingGLID)+1, 0) as StartingGLID
             , COALESCE(MAX(AccountingDT), '1/1/1970') AS LastAccountingDT
        FROM [Accounting.Reconciliation.Data]
        WHERE BID = @BID AND LocationID = @LocationID
    ),
    GL AS
    (
        SELECT COALESCE(MAX(ID), 0) AS EndingGLID,
               COALESCE(MAX(AccountingDT), GETUTCDate()) AS EndingAccountingDT
        FROM [Accounting.GL.Data]
        WHERE BID = @BID AND LocationID = @LocationID
    )
      SELECT PI.*
      FROM [Accounting.Reconciliation.PaymentInfo.ByRange]
      (
              @BID
            , @LocationID
            , (SELECT StartingGLID FROM R)
            , (SELECT EndingGLID FROM GL)
            , (SELECT LastAccountingDT FROM R)
            , (SELECT EndingAccountingDT FROM GL)
       ) PI
    
);
");
            migrationBuilder.Sql(@"
/* =============================================
    Table Value Function: [Accounting.Reconciliation.PaymentInfo.AdjsutedPreview]
    Returns the PaymentInfo object for the Preview that will Adjust a previous reconciliation
     
    See https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1168736257/ReconciliationPaymentInfo+Object
    for the structure of the response.
 
    Sample Usage:
     
        SELECT * FROM [Accounting.Reconciliation.PaymentInfo.AdjustedPreview]( 1, 45 );
 
-- ============================================= */
CREATE OR ALTER FUNCTION [Accounting.Reconciliation.PaymentInfo.AdjustedPreview]
(  
      @BID SMALLINT
    , @AdjustedReconciliationID INT
)
RETURNS TABLE
AS
RETURN
(
    WITH R AS
    (
        SELECT LocationID
             , COALESCE(EndingGLID+1, 0) as StartingGLID
             , LastAccountingDT AS StartingAccountingDT
             , AccountingDT AS EndingAccountingDT
             , ( SELECT COALESCE(MAX(GL.ID), 0)
                 FROM [Accounting.GL.Data] GL
                 WHERE R1.BID = GL.BID AND R1.LocationID = GL.LocationID
               ) AS EndingGLID
        FROM [Accounting.Reconciliation.Data] R1
        WHERE BID = @BID AND ID = @AdjustedReconciliationID
    )
      SELECT PI.*
      FROM [Accounting.Reconciliation.PaymentInfo.ByRange]
      (
              @BID
            , (SELECT LocationID FROM R)
            , (SELECT StartingGLID FROM R)
            , (SELECT EndingGLID FROM R)
            , (SELECT StartingAccountingDT FROM R)
            , (SELECT EndingAccountingDT FROM R)
       ) PI
    
);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION IF EXISTS [Accounting.Reconciliation.PaymentInfo.ByID];
DROP FUNCTION IF EXISTS [Accounting.Reconciliation.PaymentInfo.Preview];
DROP FUNCTION IF EXISTS [Accounting.Reconciliation.PaymentInfo.AdjustedPreview];
DROP FUNCTION IF EXISTS [Accounting.Reconciliation.PaymentInfo.ByRange];
");
        }
    }
}
