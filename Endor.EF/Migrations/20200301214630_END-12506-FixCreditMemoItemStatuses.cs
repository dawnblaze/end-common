﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12506FixCreditMemoItemStatuses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- Reassign non-Credit-Memo Item Statuses in that range
UPDATE OI
SET ItemStatusID = (SELECT TOP(1) ID
                    FROM [Order.Item.Status] OS 
                    WHERE OS.OrderStatusID = OI.OrderStatusID 
                      AND OS.TransactionType = OI.TransactionType
                      AND OS.ID NOT BETWEEN 81 AND 89)
FROM [Order.Item.Data] OI
WHERE OI.ItemStatusID BETWEEN 81 and 89 
AND OI.TransactionType != 8
;

-- Delete non- Credit-Memo Item Statuses in that range

-- Due to a problem with existing data, we are going to do it in two steps
DELETE FROM [Order.Item.Status]
WHERE ID BETWEEN 82 and 89 
  AND TransactionType != 8
;

-- Insert missing records
EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName = 'Order.Item.Status', @IncludeRequired = 1, @IncludeDefault = 1
;

-- handle some existing data by moving statuses
UPDATE [Order.Item.Data]
SET ItemStatusID = 86, OrderStatusID = 86
WHERE TransactionType = 8 And ItemStatusID = 81
;

DELETE FROM [Order.Item.Status]
WHERE ID = 81 
  AND TransactionType != 8
;

-- Insert missing records
EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName = 'Order.Item.Status', @IncludeRequired = 1, @IncludeDefault = 1
;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
