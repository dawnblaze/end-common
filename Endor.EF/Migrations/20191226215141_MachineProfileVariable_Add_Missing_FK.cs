﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class MachineProfileVariable_Add_Missing_FK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Variable_DataType",
                table: "Part.Machine.Profile.Variable",
                column: "DataType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Variable_ListDataType",
                table: "Part.Machine.Profile.Variable",
                column: "ListDataType");

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Variable_BID_LinkedLaborID",
                table: "Part.Machine.Profile.Variable",
                columns: new[] { "BID", "LinkedLaborID" });

            migrationBuilder.CreateIndex(
                name: "IX_Part.Machine.Profile.Variable_BID_LinkedMaterialID",
                table: "Part.Machine.Profile.Variable",
                columns: new[] { "BID", "LinkedMaterialID" });

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Profile.Variable_enum.DataType",
                table: "Part.Machine.Profile.Variable",
                column: "DataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Profile.Variable.ListDataType_enum.DataType",
                table: "Part.Machine.Profile.Variable",
                column: "ListDataType",
                principalTable: "enum.DataType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Profile.Variable_Part.Labor.Data",
                table: "Part.Machine.Profile.Variable",
                columns: new[] { "BID", "LinkedLaborID" },
                principalTable: "Part.Labor.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Part.Machine.Profile.Variable_Part.Material.Data",
                table: "Part.Machine.Profile.Variable",
                columns: new[] { "BID", "LinkedMaterialID" },
                principalTable: "Part.Material.Data",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Profile.Variable_enum.DataType",
                table: "Part.Machine.Profile.Variable");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Profile.Variable.ListDataType_enum.DataType",
                table: "Part.Machine.Profile.Variable");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Profile.Variable_Part.Labor.Data",
                table: "Part.Machine.Profile.Variable");

            migrationBuilder.DropForeignKey(
                name: "FK_Part.Machine.Profile.Variable_Part.Material.Data",
                table: "Part.Machine.Profile.Variable");

            migrationBuilder.DropIndex(
                name: "IX_Part.Machine.Profile.Variable_DataType",
                table: "Part.Machine.Profile.Variable");

            migrationBuilder.DropIndex(
                name: "IX_Part.Machine.Profile.Variable_ListDataType",
                table: "Part.Machine.Profile.Variable");

            migrationBuilder.DropIndex(
                name: "IX_Part.Machine.Profile.Variable_BID_LinkedLaborID",
                table: "Part.Machine.Profile.Variable");

            migrationBuilder.DropIndex(
                name: "IX_Part.Machine.Profile.Variable_BID_LinkedMaterialID",
                table: "Part.Machine.Profile.Variable");
        }
    }
}
