﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11251_InsertMissingEnumValuesToDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                CREATE TABLE #tempDataTypes(
	                [ID] [smallint] NOT NULL,
	                [Name] [varchar](255) NOT NULL,
                );

                INSERT INTO #tempDataTypes 
                VALUES (12000,'Material'),
                    (-12000,'MaterialList'),
                    (12020,'Labor'),
                    (-12020,'LaborList');


                INSERT INTO [enum.DataType] ([ID],[Name])
                SELECT temp.ID,temp.Name from #tempDataTypes temp WHERE NOT EXISTS(
                    SELECT 1 FROM [enum.DataType] dt WHERE dt.ID = temp.ID
                );

                DROP TABLE #tempDataTypes;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE  FROM  [Dev.Endor.Business.DB1].[dbo].[enum.DataType] WHERE ID IN (-12020, -12000, 12000, 12020);
            ");
        }
    }
}
