﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class UpdateEmailAccountStatusCriteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [System.List.Filter.Criteria] 
SET ListValues = 'Authorized,Pending,Failed,Expired,Inactive', InputType = 15 
WHERE [Field] = 'StatusTypeText' AND TargetClassTypeID = 1023;
");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
