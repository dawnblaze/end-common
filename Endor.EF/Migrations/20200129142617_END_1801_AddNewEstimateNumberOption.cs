﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_1801_AddNewEstimateNumberOption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden])
VALUES (14, 55, 'Order.Estimate.Number.UseEstimateNumber', 'Use same Estimate and Order Numbers', '', 3, '', 1, 0);
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [dbo].[System.Option.Definition] WHERE [ID]=55;
");
        }
    }
}
