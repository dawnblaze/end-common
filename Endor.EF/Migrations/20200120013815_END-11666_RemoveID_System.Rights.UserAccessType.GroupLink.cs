﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11666_RemoveID_SystemRightsUserAccessTypeGroupLink : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			migrationBuilder.Sql(@"
ALTER TABLE dbo.[System.Rights.UserAccessType.GroupLink]
DROP CONSTRAINT [PK_System.Rights.UserAccessType.GroupLink]
;
ALTER TABLE dbo.[System.Rights.UserAccessType.GroupLink]
DROP COLUMN ID
;
ALTER TABLE dbo.[System.Rights.UserAccessType.GroupLink] 
ADD CONSTRAINT [PK_System.Rights.UserAccessType.GroupLink_1] 
    PRIMARY KEY ( UserAccessType, IncludedRightsGroupID )
;
CREATE INDEX [IX_System.Rights.UserAccessType.GroupLink] 
ON dbo.[System.Rights.UserAccessType.GroupLink] ( IncludedRightsGroupID, UserAccessType ) 
;
            ");
		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
