﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class FixDepositAccountForCash : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
Update [Accounting.Payment.Method] set DepositGLAccountID = 1101 where ID = 1
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
Update [Accounting.Payment.Method] set DepositGLAccountID = 1100 where ID = 1
            ");
        }
    }
}
