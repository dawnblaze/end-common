﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END5202_EmailFromAccountsOptionCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
VALUES (107,'Email From Accounts',100, 'Manage Email Accounts for Sending Emails', 2, 0, 'email gmail microsoft outlook send from smtp outbound sendmail mail employee');");

            migrationBuilder.Sql(@"
INSERT INTO [System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID])
VALUES (1023, 'Display Name', 'Display Name', 'DisplayName', 0, 1, 0, 0, NULL, NULL, 0, 1, 103);

INSERT INTO [System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID])
VALUES (1023, 'Emplpoyee', 'Employee', 'EmployeeID', 0, 2, 3, 0, NULL, NULL, 1, 2, 104);

INSERT INTO [System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID])
VALUES (1023, 'Team', 'Team', 'TeamIDs', 0, 32005, 15, 0, NULL, NULL, 1, 3, 105);

INSERT INTO [System.List.Filter.Criteria] ([TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex], [ID])
VALUES (1023, 'Include Inactive', 'Include Inactive', '-IsActive', 0, 3, 13, 0, NULL, NULL, 0, 4, 106);
");        
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [System.List.Filter.Criteria] WHERE [ID] = 103;
DELETE FROM [System.List.Filter.Criteria] WHERE [ID] = 104;
DELETE FROM [System.List.Filter.Criteria] WHERE [ID] = 105;
DELETE FROM [System.List.Filter.Criteria] WHERE [ID] = 106;
");
            migrationBuilder.Sql("DELETE FROM [System.Option.Category] WHERE ID = 107");
        }
    }
}
