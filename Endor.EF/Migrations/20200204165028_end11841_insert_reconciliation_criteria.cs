﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class end11841_insert_reconciliation_criteria : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [System.List.Filter.Criteria]
                WHERE TargetClassTypeID = 8015 AND Field in ('LocationID','-IsEmpty');

                INSERT INTO [System.List.Filter.Criteria]
                ([TargetClassTypeID],[Name],[Label],[Field],[IsHidden],[DataType],[InputType],[AllowMultiple],[ListValues],[ListValuesEndpoint],[IsLimitToList],[SortIndex],[ID])
                VALUES
                (8015,'Location','Location','LocationID',0,2,20,0,'1005',NULL,0,0,(SELECT MAX(ID)+1 FROM [System.List.Filter.Criteria])),
                (8015,'Show Empty Reconciliations','Show Empty Reconciliations','-IsEmpty',0,3,13,0,NULL,NULL,0,1,(SELECT MAX(ID)+2 FROM [System.List.Filter.Criteria]));            
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM [System.List.Filter.Criteria]
                WHERE TargetClassTypeID = 8015 AND Field in ('LocationID','-IsEmpty');
            ");
        }
    }
}
