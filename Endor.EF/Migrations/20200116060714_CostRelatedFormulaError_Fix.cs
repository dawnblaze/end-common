﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class CostRelatedFormulaError_Fix : EndorMigration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // create func sql
            migrationBuilder.Sql(@"
-- =================================================================================
/*
    Function: This function replaced all words in an equation or sentence.
    The function replaces all occurrences that are delimited with something other than
    a letter or number.

    Sample Usage:

        DECLARE @T VARCHAR(100) = 'Old Oldey OldeyButGoodey Old Old.Old+Old*(Old+Old/2)';
        SELECT dbo.ReplaceWord(@T, 'Old', 'New')
        -- should return 'New Oldey OldeyButGoodey New New.New+New*(New+New/2)';

*/
-- =================================================================================
CREATE OR ALTER FUNCTION ReplaceWord( @SearchStr VARCHAR(MAX), @OriginalWord VARCHAR(1024), @ReplacementWord VARCHAR(1024) )
RETURNS VARCHAR(MAX)
AS
BEGIN
    --DECLARE @SearchStr VARCHAR(MAX) = 'Old Old Old.Old+Old*(Old+Old/2)'
    --   , @OriginalWord VARCHAR(1024) = 'Old'
    --   , @ReplacementWord VARCHAR(1024) = 'New'

	DECLARE @Result VARCHAR(MAX)
          , @P INT = 0 -- Starting Position 
          , @L INT = Len(@OriginalWord)
          , @CharBefore CHAR(1)
          , @CharAfter CHAR(1)
          ;
    
    -- Replace
    SET @Result = REPLACE(@SearchStr, ' '+@OriginalWord+' ', ' '+@ReplacementWord+' ');
    
    -- ;Replace again, which is necessary to handle  'Original Original' cases
    SET @Result = REPLACE(@SearchStr, ' ' + @OriginalWord + ' ', ' ' + @ReplacementWord + ' ');

            SET @P = CHARINDEX(@OriginalWord, @Result, @P + 1);
            WHILE(@P > 0)
    BEGIN
        SELECT @CharBefore = SUBSTRING(@Result, @P - 1, 1)
            ,  @CharAfter = SUBSTRING(@Result, @P + @L, 1)
        ;
            --PRINT CONCAT('Before=', @CharBefore, '  @After=', @CharAfter);

            --Replace the word if the delimiters are not more characters or numbers
           IF NOT(@CharBefore BETWEEN '0' AND '9' OR Upper(@CharBefore) BETWEEN 'A' AND 'Z'
   
               OR @CharAfter BETWEEN '0' AND '9' OR Upper(@CharAfter) BETWEEN 'A' AND 'Z')
        BEGIN
            SET @Result = LEFT(@Result, @P - 1) + @ReplacementWord + RIGHT(@Result, LEN(@Result) - @P - @L + 1);
            --PRINT @Result;
            END;

            SET @P = CHARINDEX(@OriginalWord, @Result, @P + 1);
            END;

            --Check if first position is the string
           SET @Result =
                   CASE WHEN CHARINDEX(@OriginalWord+' ', @Result) = 1
            THEN STUFF(@Result, 1, Len(@OriginalWord), @ReplacementWord)
            ELSE @Result END

    -- Check if last position is the string
    SET @Result =
            CASE WHEN CHARINDEX(REVERSE(' ' + @OriginalWord), REVERSE(@Result)) = 1
            THEN Reverse(STUFF(Reverse(@Result), CHARINDEX(Reverse(' ' + @OriginalWord), 
                 Reverse(@Result)), @L + 1, Reverse(@ReplacementWord)))
            ELSE @Result END

    -- Return the result of the function

    RETURN @Result;
END
");

            migrationBuilder.Sql(@"
UPDATE [Part.Subassembly.Variable] 
	SET 
		DefaultValue = dbo.ReplaceWord(DefaultValue, 'Cost', 'TotalCost');
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Part.Subassembly.Variable] 
	SET 
		DefaultValue = dbo.ReplaceWord(DefaultValue, 'TotalCost', 'Cost');
");
        }

        public override bool ClearAllAssemblies()
        {
            return true;
        }
    }
}
