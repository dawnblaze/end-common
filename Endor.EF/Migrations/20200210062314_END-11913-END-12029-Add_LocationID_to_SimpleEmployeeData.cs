﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11913END12029Add_LocationID_to_SimpleEmployeeData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE OR ALTER VIEW [Employee.SimpleList] AS
SELECT BID
, ID
, ClassTypeID
, ShortName AS DisplayName
, IsActive
, HasImage
, CONVERT(BIT, 0) AS IsDefault
, LocationID
FROM dbo.[Employee.Data]
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
