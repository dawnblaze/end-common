﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12115_Add_New_Reconciliation_Options : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
                VALUES (304,'Reconciliation Options',300, 'Reconciliation Options', 8, 0, 'reconciliation close closeout end period settle')
                ;

                INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) 
                VALUES (304, 3041, 'Reconciliation.Auto.Reconcile.Enabled', 'Enable Automatic Closing of Reconciliation', '', 3, '', 1, 0)
                    ,  (304, 3043, 'Reconciliation.Auto.Reconcile.PostEmpty', 'Automatically Post Reconciliation Even if Empty', '', 3, '', 1, 0)
                ;

                INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) 
                VALUES (304, 3042, 'Reconciliation.Auto.Reconcile.Time', 'Time of Date for Automatic Closing of Reconciliation', '', 9, '', '25:39', 0)
                ;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
