﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_4973_UrlRegistrationDataMaxUseCountFrom1To1000000 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "URL.Registration.Data",
                nullable: false,
                computedColumnSql: @"(0)",
                oldClrType: typeof(bool),
                oldType: "bit",
                oldComputedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                AND [UseCount] < coalesce([MaxUseCount], (1))
                                    AND ([UseCount] = (0)
                                            OR[ExpireAfterUseDays] IS NULL
                                            OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                then CONVERT([bit], (1))
                                else CONVERT([bit],(0)) end,(0)))");

            migrationBuilder.AlterColumn<int>(
                name: "MaxUseCount",
                table: "URL.Registration.Data",
                nullable: false,
                defaultValueSql: "((1000000))",
                oldClrType: typeof(int),
                oldType: "int",
                oldDefaultValueSql: "((1))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "URL.Registration.Data",
                nullable: false,
                computedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                AND [UseCount] < coalesce([MaxUseCount], (1000000))
                                    AND ([UseCount] = (0)
                                            OR[ExpireAfterUseDays] IS NULL
                                            OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                then CONVERT([bit], (1))
                                else CONVERT([bit],(0)) end,(0)))",
                oldClrType: typeof(bool),
                oldType: "bit",
                oldComputedColumnSql: @"(0)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "URL.Registration.Data",
                nullable: false,
                computedColumnSql: @"(0)",
                oldClrType: typeof(bool),
                oldType: "bit",
                oldComputedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                AND [UseCount] < coalesce([MaxUseCount], (1000000))
                                    AND ([UseCount] = (0)
                                            OR[ExpireAfterUseDays] IS NULL
                                            OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                then CONVERT([bit], (1))
                                else CONVERT([bit],(0)) end,(0)))");
            
            migrationBuilder.AlterColumn<int>(
                name: "MaxUseCount",
                table: "URL.Registration.Data",
                type: "int",
                nullable: false,
                defaultValueSql: "((1))",
                oldClrType: typeof(int),
                oldDefaultValueSql: "((1000000))");

            migrationBuilder.AlterColumn<bool>(
                name: "IsActive",
                table: "URL.Registration.Data",
                type: "bit",
                nullable: false,
                computedColumnSql: @"(isnull(case when IsRevoked = 0 AND [ExpirationDT] > getutcdate()
                                AND [UseCount] < coalesce([MaxUseCount], (1))
                                    AND ([UseCount] = (0)
                                            OR[ExpireAfterUseDays] IS NULL
                                            OR datediff(day, [FirstUseDT], getutcdate())<=[ExpireAfterUseDays])
                                then CONVERT([bit], (1))
                                else CONVERT([bit],(0)) end,(0)))",
                oldClrType: typeof(bool),
                oldComputedColumnSql: @"(0)");
        }
    }
}
