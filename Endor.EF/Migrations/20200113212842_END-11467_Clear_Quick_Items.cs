﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11467_Clear_Quick_Items : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DECLARE @T TABLE(BID SMALLINT, ID INT);

INSERT INTO @T
(BID, ID)
SELECT BID, ID
FROM   [Part.QuickItem.Data]
WHERE  ModifiedDT < '01/10/2020';

DELETE FROM C
FROM [Part.QuickItem.CategoryLink] C
     JOIN @T T ON T.BID = C.BID AND T.ID = C.QuickItemID;

DELETE FROM Q
FROM [Part.QuickItem.Data] Q
     JOIN @T T ON T.BID = Q.BID AND T.ID = Q.ID;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
