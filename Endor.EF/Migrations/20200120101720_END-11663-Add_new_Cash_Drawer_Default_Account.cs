﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11663Add_new_Cash_Drawer_Default_Account : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [Accounting.GL.Account]
    ([BID],[ID],[ExportAccountName],[ExportAccountNumber],[ExportGLAccountType],[GLAccountType],[Name],[Number],[ParentID],[IsActive],[CanEdit])
VALUES (-1, 1101, 'Cash Drawer', '1101', 10, 11, 'Cash Drawer', 1101, 1000, 1, 0)
;

EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName = 'Accounting.GL.Account'
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
