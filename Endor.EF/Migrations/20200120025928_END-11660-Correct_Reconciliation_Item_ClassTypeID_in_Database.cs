﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11660Correct_Reconciliation_Item_ClassTypeID_in_Database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

    ALTER TABLE [Accounting.Reconciliation.Item]
    DROP COLUMN ClassTypeID;
    GO

    ALTER TABLE [Accounting.Reconciliation.Item]
    ADD ClassTypeID AS (8016);

");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
