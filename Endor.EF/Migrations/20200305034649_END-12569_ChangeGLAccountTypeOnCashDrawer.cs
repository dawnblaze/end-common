﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12569_ChangeGLAccountTypeOnCashDrawer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			migrationBuilder.Sql(@"
UPDATE [Accounting.GL.Account]
SET GLAccountType = 10
WHERE ID = 1101
;");
		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
UPDATE [Accounting.GL.Account]
SET GLAccountType = 11
WHERE ID = 1101
;");
        }
    }
}
