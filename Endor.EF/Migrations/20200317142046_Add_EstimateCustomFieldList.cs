﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class Add_EstimateCustomFieldList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[enum.DataType] ([ID], [Name])
                VALUES (10201,'EstimateCustomField');

                INSERT INTO [dbo].[enum.DataType] ([ID], [Name])
                                VALUES (-10201,'EstimateCustomFieldList');
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
