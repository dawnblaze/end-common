﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class AddCRMOptionsCategory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO [dbo].[System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden], [SearchTerms])
                VALUES (612,'CRM Options',600, 'CRM Options', 2, 0, 'Stale CRM Company Contacts');

                INSERT INTO [dbo].[System.Option.Definition] ([CategoryID],[ID],[Name],[Label],[Description],[DataType],[ListValues],[DefaultValue],[IsHidden]) 
                VALUES (612, 6121, 'CRM.Lead.Stale.Days',     'Lead Stale After',     '', 2, '', NULL, 0)
	                  ,(612, 6122, 'CRM.Prospect.Stale.Days', 'Prospect Stale After', '', 2, '', NULL, 0)
	                  ,(612, 6123, 'CRM.Customer.Stale.Days', 'Customer Stale After', '', 2, '', NULL, 0);
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
        }
    }
}
