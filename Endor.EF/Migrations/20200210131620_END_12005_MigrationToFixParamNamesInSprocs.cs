﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_12005_MigrationToFixParamNamesInSprocs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- 
-- Name: User.Security.Rights( BID int, UserLinkID int )
--
-- Description: This Table Value Function returns an array of Rights associated
-- with a specific user for a given business id.
--
-- Sample Use:   SELECT * FROM dbo.[User.Security.Rights]( 1, 3 )
--
-- ========================================================
CREATE OR ALTER FUNCTION [dbo].[User.Security.Rights] ( @BID int, @UserLinkID int )
RETURNS TABLE
AS
RETURN
(
    --DECLARE 
    --    @BID SMALLINT = 1
    --    , @UserLinkID int = 3  -- select * from [User.Link]
    --;
    WITH TopGroup AS 
    (
        SELECT 'PG' AS [Source], PG.ID as ID, PG.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [Rights.Group.List] PG ON UL.BID = PG.BID AND UL.RightsGroupListID = PG.ID
        JOIN [Rights.Group.List.RightsGroupLink] PGLink ON PG.BID = PGLink.BID AND PG.ID = PGLink.ListID
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = PGLink.GroupID
        WHERE UL.BID = @BID AND UL.ID = @UserLinkID

        UNION

        SELECT 'UAT' AS [Source], UAT.ID, UAT.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [enum.User.Access.Type] UAT ON UL.UserAccessType = UAT.ID
        JOIN [System.Rights.UserAccessType.GroupLink] UATLink ON UAT.ID = UATLink.UserAccessType
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = UATLink.IncludedRightsGroupID
        WHERE UL.BID = @BID AND UL.ID = @UserLinkID
    ),
        AllGroups AS
    (
    SELECT DISTINCT GroupID
        , GroupName
        , (SELECT CONCAT([Source], '(', ID, '-', Name, '); ') FROM TopGroup T2 WHERE T2.GroupID = TopGroup.GroupID FOR XML PATH('')) AS Sources
    FROM TopGroup
    GROUP BY GroupID, GroupName

    UNION ALL

    SELECT ChildGroup.ID as GroupID
        , ChildGroup.Name as GroupName
        , CONVERT(NVARCHAR(MAX),CONCAT('Child(', Links.ChildID, '-', ChildGroup.Name,  ')->Parent(', Links.ParentID, '-', AllGroups.GroupName, '); ')) as Sources
    FROM AllGroups 
    JOIN [System.Rights.Group.ChildLink] Links ON AllGroups.GroupID = Links.ParentID
    JOIN [System.Rights.Group] ChildGroup ON ChildGroup.ID = Links.ChildID
    )
        SELECT DISTINCT RightLink.RightID as RightID
        From AllGroups
        LEFT JOIN [System.Rights.Group.RightLink] RightLink on RightLink.GroupID = AllGroups.GroupID
)
;
");
            migrationBuilder.Sql(@"
-- ===========================================================================
/*  Description: Creates a Function that lists the users rights and where 
                 they were obtained from.  Only rights granted are shown.

    Example Usage:
        
        SELECT * FROM dbo.[User.Security.RightsBySource] (1, 3);

*/
-- ===========================================================================
CREATE OR ALTER FUNCTION [User.Security.RightsBySource] ( @BID int, @UserLinkID int )
RETURNS TABLE 
AS
RETURN 
(
    --DECLARE 
    --      @BID SMALLINT = -1
    --    , @UAT SMALLINT = 0  -- Use 0 to remove all, NULL to include all 
    --    , @PermissionGroupID SMALLINT = NULL  -- Use NULL to include All
    --    ;

    WITH TopGroup AS 
    (
        SELECT 'PG' AS [Source], PG.ID as ID, PG.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [Rights.Group.List] PG ON PG.BID = UL.BID AND UL.RightsGroupListID = PG.ID
        JOIN [Rights.Group.List.RightsGroupLink] PGLink ON PG.BID = PGLink.BID AND PG.ID = PGLink.ListID
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = PGLink.GroupID
        WHERE UL.BID = @BID AND UL.ID = @UserLinkID

        UNION

        SELECT 'UAT' AS [Source], UAT.ID, UAT.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [enum.User.Access.Type] UAT ON UL.UserAccessType = UAT.ID
        JOIN [System.Rights.UserAccessType.GroupLink] UATLink ON UAT.ID = UATLink.UserAccessType
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = UATLink.IncludedRightsGroupID
        WHERE UL.BID = @BID AND UL.ID = @UserLinkID
    ),
        AllGroups AS
    (
      SELECT DISTINCT GroupID
        , GroupName
        , (SELECT CONCAT([Source], '(', ID, '-', Name, '); ') FROM TopGroup T2 WHERE T2.GroupID = TopGroup.GroupID FOR XML PATH('')) AS Sources
      FROM TopGroup
      GROUP BY GroupID, GroupName

      UNION ALL

      SELECT ChildGroup.ID as GroupID
        , ChildGroup.Name as GroupName
        , CONVERT(NVARCHAR(MAX),CONCAT('Child(', Links.ChildID, '-', ChildGroup.Name,  ')->Parent(', Links.ParentID, '-', AllGroups.GroupName, '); ')) as Sources
      FROM AllGroups 
      JOIN [System.Rights.Group.ChildLink] Links ON AllGroups.GroupID = Links.ParentID
      JOIN [System.Rights.Group] ChildGroup ON ChildGroup.ID = Links.ChildID
    )
        SELECT DISTINCT RightID, R.Name as RightName, AllGroups.GroupID, GroupName, Sources
        From AllGroups
        LEFT JOIN [System.Rights.Group.RightLink] RightLink on RightLink.GroupID = AllGroups.GroupID
        LEFT JOIN [enum.User.Right] R on R.ID = RightLink.RightID
        WHERE RightID IS NOT NULL
        ORDER BY RightID, AllGroups.GroupID
        OFFSET 0 ROWS
)
");
            migrationBuilder.Sql(@"
-- ========================================================
-- 
-- Name: User.Security.Rights.String( BID int, UserLinkID int )
--
-- Description: This Table Value Function returns a string of Rights associated
-- with a specific user for a given business id, where each bit position = 1 corresponds to
-- the value of the right.
--
-- Sample Use:   select dbo.[User.Security.Rights.String]( 1, 100 )
--
-- ========================================================
CREATE OR ALTER FUNCTION [dbo].[User.Security.Rights.String] ( @BID int, @UserLinkID int )
RETURNS BINARY(128)
AS
BEGIN
    DECLARE @ResultSize SMALLINT = 128
          , @Results BINARY(128) = 0
    ; 

    WITH Step1 AS 
    (
        SELECT (RightID / 8)+1 as ByteIndex,
               Power(2, RightID % 8) as ByteFlag 
        FROM dbo.[User.Security.Rights]( @BID, @UserLinkID )
    ),
    Step2 AS
    (
        SELECT ByteIndex
             , SUM(ByteFlag) as Flags
        FROM Step1
        GROUP By ByteIndex
    )
        SELECT @Results = SUBSTRING(@Results, 1, @ResultSize-ByteIndex) 
                            + CONVERT(BINARY(1),CHAR(Flags)) 
                            + SUBSTRING(@Results, @ResultSize-(ByteIndex-2), ByteIndex-1)
        FROM Step2 
        WHERE ByteIndex > 0
    ;
        
    RETURN @Results;
END
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
-- ========================================================
-- 
-- Name: User.Security.Rights( BID int, UserID int )
--
-- Description: This Table Value Function returns an array of Rights associated
-- with a specific user for a given business id.
--
-- Sample Use:   SELECT * FROM dbo.[User.Security.Rights]( 1, 3 )
--
-- ========================================================
CREATE OR ALTER FUNCTION [dbo].[User.Security.Rights] ( @BID int, @UserID int )
RETURNS TABLE
AS
RETURN
(
    --DECLARE 
    --    @BID SMALLINT = 1
    --    , @UserID int = 3  -- select * from [User.Link]
    --;
    WITH TopGroup AS 
    (
        SELECT 'PG' AS [Source], PG.ID as ID, PG.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [Rights.Group.List] PG ON UL.BID = PG.BID AND UL.RightsGroupListID = PG.ID
        JOIN [Rights.Group.List.RightsGroupLink] PGLink ON PG.BID = PGLink.BID AND PG.ID = PGLink.ListID
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = PGLink.GroupID
        WHERE UL.BID = @BID AND UL.ID = @UserID

        UNION

        SELECT 'UAT' AS [Source], UAT.ID, UAT.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [enum.User.Access.Type] UAT ON UL.UserAccessType = UAT.ID
        JOIN [System.Rights.UserAccessType.GroupLink] UATLink ON UAT.ID = UATLink.UserAccessType
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = UATLink.IncludedRightsGroupID
        WHERE UL.BID = @BID AND UL.ID = @UserID
    ),
        AllGroups AS
    (
    SELECT DISTINCT GroupID
        , GroupName
        , (SELECT CONCAT([Source], '(', ID, '-', Name, '); ') FROM TopGroup T2 WHERE T2.GroupID = TopGroup.GroupID FOR XML PATH('')) AS Sources
    FROM TopGroup
    GROUP BY GroupID, GroupName

    UNION ALL

    SELECT ChildGroup.ID as GroupID
        , ChildGroup.Name as GroupName
        , CONVERT(NVARCHAR(MAX),CONCAT('Child(', Links.ChildID, '-', ChildGroup.Name,  ')->Parent(', Links.ParentID, '-', AllGroups.GroupName, '); ')) as Sources
    FROM AllGroups 
    JOIN [System.Rights.Group.ChildLink] Links ON AllGroups.GroupID = Links.ParentID
    JOIN [System.Rights.Group] ChildGroup ON ChildGroup.ID = Links.ChildID
    )
        SELECT DISTINCT RightLink.RightID as RightID
        From AllGroups
        LEFT JOIN [System.Rights.Group.RightLink] RightLink on RightLink.GroupID = AllGroups.GroupID
)
;
");
            migrationBuilder.Sql(@"

-- ===========================================================================
/*  Description: Creates a Function that lists the users rights and where 
                 they were obtained from.  Only rights granted are shown.

    Example Usage:
        
        SELECT * FROM dbo.[User.Security.RightsBySource] (1, 3);

*/
-- ===========================================================================
CREATE OR ALTER FUNCTION [dbo].[User.Security.RightsBySource] ( @BID int, @UserID int )
RETURNS TABLE 
AS
RETURN 
(
    --DECLARE 
    --      @BID SMALLINT = -1
    --    , @UAT SMALLINT = 0  -- Use 0 to remove all, NULL to include all 
    --    , @PermissionGroupID SMALLINT = NULL  -- Use NULL to include All
    --    ;

    WITH TopGroup AS 
    (
        SELECT 'PG' AS [Source], PG.ID as ID, PG.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [Rights.Group.List] PG ON PG.BID = UL.BID AND UL.RightsGroupListID = PG.ID
        JOIN [Rights.Group.List.RightsGroupLink] PGLink ON PG.BID = PGLink.BID AND PG.ID = PGLink.ListID
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = PGLink.GroupID
        WHERE UL.BID = @BID AND UL.ID = @UserID

        UNION

        SELECT 'UAT' AS [Source], UAT.ID, UAT.Name, TopGroup.ID as GroupID, TopGroup.Name as GroupName
        FROM [User.Link] UL
        JOIN [enum.User.Access.Type] UAT ON UL.UserAccessType = UAT.ID
        JOIN [System.Rights.UserAccessType.GroupLink] UATLink ON UAT.ID = UATLink.UserAccessType
        JOIN [System.Rights.Group] TopGroup ON TopGroup.ID = UATLink.IncludedRightsGroupID
        WHERE UL.BID = @BID AND UL.ID = @UserID
    ),
        AllGroups AS
    (
      SELECT DISTINCT GroupID
        , GroupName
        , (SELECT CONCAT([Source], '(', ID, '-', Name, '); ') FROM TopGroup T2 WHERE T2.GroupID = TopGroup.GroupID FOR XML PATH('')) AS Sources
      FROM TopGroup
      GROUP BY GroupID, GroupName

      UNION ALL

      SELECT ChildGroup.ID as GroupID
        , ChildGroup.Name as GroupName
        , CONVERT(NVARCHAR(MAX),CONCAT('Child(', Links.ChildID, '-', ChildGroup.Name,  ')->Parent(', Links.ParentID, '-', AllGroups.GroupName, '); ')) as Sources
      FROM AllGroups 
      JOIN [System.Rights.Group.ChildLink] Links ON AllGroups.GroupID = Links.ParentID
      JOIN [System.Rights.Group] ChildGroup ON ChildGroup.ID = Links.ChildID
    )
        SELECT DISTINCT RightID, R.Name as RightName, AllGroups.GroupID, GroupName, Sources
        From AllGroups
        LEFT JOIN [System.Rights.Group.RightLink] RightLink on RightLink.GroupID = AllGroups.GroupID
        LEFT JOIN [enum.User.Right] R on R.ID = RightLink.RightID
        WHERE RightID IS NOT NULL
        ORDER BY RightID, AllGroups.GroupID
        OFFSET 0 ROWS
)
;
");
            migrationBuilder.Sql(@"
-- ========================================================
-- 
-- Name: User.Security.Rights.String( BID int, UserID int )
--
-- Description: This Table Value Function returns a list of Rights associated
-- with a specific user for a given business id.
--
-- Sample Use:   select dbo.[User.Security.Rights.String]( 1, 100 )
--
-- ========================================================
CREATE OR ALTER FUNCTION [dbo].[User.Security.Rights.String] ( @BID int, @UserID int )
RETURNS BINARY(128)
AS
BEGIN
    DECLARE 
            --@BID int = 1,       
            --@UserID int = 100,

            @ResultSize SMALLINT = 128
            , @Results BINARY(128) = 0
            ; 

    DECLARE @Temp Table 
        (
            RightID SMALLINT,
            ByteIndex AS (RightID / 8)+1,
            ByteFlag AS Power(2, RightID % 8)
        );

    --DECLARE @Temp Table (RightID smallint, ByteIndex tinyint, BytePos tinyint, ByteFlag tinyint);

    WITH RightGroups(GroupID)
    AS
    (
        -- Anchor Query

        -- Pull the users ROLE security group
        SELECT RG.ID
        FROM [User.Link] U
        JOIN [System.Rights.UserAccessType.GroupLink] UAT on UAT.UserAccessType = U.UserAccessType
        JOIN [System.Rights.Group] RG on RG.ID = UAT.IncludedRightsGroupID
        WHERE U.BID = @BID AND U.AuthUserID = @UserID

        -- Merge with the users top-level security group
        UNION ALL 
        SELECT RG.ID
        FROM [User.Link] U
        JOIN [Rights.Group.List] RGL ON RGL.BID = U.BID AND RGL.ID = U.RightsGroupListID
        JOIN [Rights.Group.List.RightsGroupLink] RGLink ON RGLink.BID = RGL.BID AND RGLink.ListID = RGL.ID
        JOIN [System.Rights.Group] RG on RG.ID = RGLink.GroupID
        WHERE U.BID = @BID AND U.AuthUserID = @UserID

        -- Now merge with recursively with Child Security 
        UNION ALL
        SELECT Child.ID
        FROM RightGroups R
        JOIN [System.Rights.Group.ChildLink] CLink on CLink.ParentID = R.GroupID
        JOIN [System.Rights.Group] Child on CLink.ChildID = Child.ID
    )
        -- Now that we have all the right groups, extract the Rights into a Temp Table
        INSERT INTO @Temp
            SELECT DISTINCT RLink.RightID
            FROM RightGroups RG 
            JOIN [System.Rights.Group.RightLink] RLink ON RLink.GroupID = RG.GroupID

        SELECT @Results = SUBSTRING(@Results, 1, @ResultSize-ByteIndex) 
                            + CONVERT(BINARY(1),CHAR(Flags)) 
                            + SUBSTRING(@Results, @ResultSize-(ByteIndex-2), ByteIndex-1)
                --, @Found = 1  -- use @Found to determine that we really found results.  
                --              -- Will not get set so @Found will remain 0 if not
        FROM 
        (
            -- Sum the Byte Flag by ByteIndex to get the character to insert
            SELECT ByteIndex, SUM(ByteFlag) Flags
            FROM @Temp
            GROUP By ByteIndex
        ) RightsList
        ;
        
        -- IF @@ROWCOUNT = 0
        --    SET @Results = NULL;

        RETURN @Results;
END;
");
        }
    }
}
