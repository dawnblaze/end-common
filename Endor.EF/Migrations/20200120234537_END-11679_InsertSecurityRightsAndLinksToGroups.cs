﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END11679_InsertSecurityRightsAndLinksToGroups : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"MERGE INTO [System.Rights.UserAccessType.GroupLink] AS _TARGET
USING (
	VALUES 
	-- Insert the Full Rights for Regular Users
	(1, 7000) -- Employee Acess to user Module
    , ( 1, 7011) -- Employee Access to User Profile
    , (21, 7000) -- Franchisor Staff Access to user Module
    , (21, 7011) -- Franchisor Staff Access to User Profile
    , (41, 7000) -- Support Staff Access to user Module
    , (41, 7011) -- Support Staff Access to User Profile
	-- Insert the Full Rights for Employee Administrators
	, (9, 1000)
    , (9, 1001)
    , (9, 2000)
    , (9, 2001)
    , (9, 3000)
    , (9, 3001)
    , (9, 4000)
    , (9, 4001)
    , (9, 5000)
    , (9, 5001)
    , (9, 6000)
    , (9, 6001)
    , (9, 7000)
    , (9, 7001)
	-- Insert the Full Rights for Franchisor Administrators
	, (29, 1000)
    , (29, 1001)
    , (29, 2000)
    , (29, 2001)
    , (29, 3000)
    , (29, 3001)
    , (29, 4000)
    , (29, 4001)
    , (29, 5000)
    , (29, 5001)
    , (29, 6000)
    , (29, 6001)
    , (29, 7000)
    , (29, 7001)
	-- Insert the Full Rights for CoreBridge Support Administrators
	, (49, 1000)
    , (49, 1001)
    , (49, 2000)
    , (49, 2001)
    , (49, 3000)
    , (49, 3001)
    , (49, 4000)
    , (49, 4001)
    , (49, 5000)
    , (49, 5001)
    , (49, 6000)
    , (49, 6001)
    , (49, 7000)
    , (49, 7001)
	) AS _SOURCE (UserAccessType, IncludedRightsGroupID)
ON (_TARGET.UserAccessType = _SOURCE.UserAccessType) AND (_TARGET.IncludedRightsGroupID = _SOURCE.IncludedRightsGroupID)
WHEN NOT MATCHED BY TARGET THEN
INSERT (UserAccessType, IncludedRightsGroupID) VALUES (_SOURCE.UserAccessType, _SOURCE.IncludedRightsGroupID);");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"DELETE FROM [System.Rights.UserAccessType.GroupLink]");
        }
    }
}
