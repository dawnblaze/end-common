﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END_12691_AddCashDrawerAdjustmentGlAccountsAndTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
INSERT INTO [Accounting.GL.Account] ([BID],[ID],[ExportGLAccountType],[GLAccountType],[Name],[ExportAccountName],[Number],[ExportAccountNumber],[ParentID],[IsActive])
VALUES (-1,6101,60,60,'Cash Drawer Adjustment','Cash Drawer Adjustment',6101,6101,6000,1);

EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName = 'Accounting.GL.Account';
");
            migrationBuilder.Sql(@"
INSERT [enum.Activity.GLEntryType] ([ID], [Name]) 
VALUES
      (34, 'Cash Drawer Adjustment')
    , (35, 'Cash Drawer Deposit')
;
");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DELETE FROM [Account.GL.Account] WHERE [ID]=6101;
");
            migrationBuilder.Sql(@"
DELETE FROM [enum.Activity.GLEntryType] WHERE [ID] IN (34,35);
");
        }
    }
}
