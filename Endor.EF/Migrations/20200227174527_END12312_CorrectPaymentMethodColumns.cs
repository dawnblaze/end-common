﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class END12312_CorrectPaymentMethodColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Reconciliation.Item_Accounting.Payment.Method",
                table: "Accounting.Reconciliation.Item");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Reconciliation.Item_PaymentMethodID",
                table: "Accounting.Reconciliation.Item");
            migrationBuilder.DropIndex(
                name: "IX_Accounting.Reconciliation.Item_Master",
                table: "Accounting.Reconciliation.Item");
            migrationBuilder.DropColumn(
                name: "IsPaymentSummary",
                table: "Accounting.Reconciliation.Item");


            //Make sure columns map to new value
            migrationBuilder.Sql(@"
                ALTER TABLE [dbo].[Accounting.Reconciliation.Item]
                ALTER COLUMN PaymentMethodID int;

                Update [dbo].[Accounting.Reconciliation.Item]
                set PaymentMethodID = (Select Top 1 ID from [dbo].[Accounting.Payment.Method] PM where PM.PaymentMethodType = PaymentMethodId)
                where PaymentMethodID is not null
            ");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Item_Master",
                table: "Accounting.Reconciliation.Item",
                columns: new[] { "BID", "ReconciliationID", "GLAccountID", "PaymentMethodID" });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Item_BID_PaymentMethodID",
                table: "Accounting.Reconciliation.Item",
                columns: new[] { "BID", "PaymentMethodID" });

            migrationBuilder.AddColumn<bool>(
                name: "IsPaymentSummary",
                table: "Accounting.Reconciliation.Item",
                nullable: false,
                defaultValueSql: "((0))",
                computedColumnSql: "(isnull(case when [PaymentMethodID] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0)))");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Reconciliation.Item_Accounting.Payment.Method",
                table: "Accounting.Reconciliation.Item",
                columns: new[] { "BID", "PaymentMethodID" },
                principalTable: "Accounting.Payment.Method",
                principalColumns: new[] { "BID", "ID" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounting.Reconciliation.Item_Accounting.Payment.Method",
                table: "Accounting.Reconciliation.Item");

            migrationBuilder.DropIndex(
                name: "IX_Accounting.Reconciliation.Item_BID_PaymentMethodID",
                table: "Accounting.Reconciliation.Item");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting.Reconciliation.Item_PaymentMethodID",
                table: "Accounting.Reconciliation.Item",
                column: "PaymentMethodID");

            migrationBuilder.AddForeignKey(
                name: "FK_Accounting.Reconciliation.Item_Accounting.Payment.Method",
                table: "Accounting.Reconciliation.Item",
                column: "PaymentMethodID",
                principalTable: "enum.Accounting.PaymentMethodType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
