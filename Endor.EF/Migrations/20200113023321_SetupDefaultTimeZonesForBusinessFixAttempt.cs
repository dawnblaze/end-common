﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Endor.EF.Migrations
{
    public partial class SetupDefaultTimeZonesForBusinessFixAttempt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"/* 
-- ========================================================

    Procedure Name: [Util.Table.CopyDefaultRecords] 

    Description: This function copys all of the -1 and -2 records for a table 
                    into all of the BIDs for which they don't exist.
                    It then deletes all the BID=-2 records.

    Sample Use:   

        EXEC [Util.Table.CopyDefaultRecords] @TableName = '_Root.Data';
        
-- ========================================================
*/
CREATE OR ALTER PROCEDURE [dbo].[Util.Table.CopyDefaultRecords] 
    (@TableName NVARCHAR(200), @IncludeRequired BIT = 1, @IncludeDefault BIT = 1, @TargetBID SMALLINT = NULL, @IDFieldName VARCHAR(100) = 'ID')
AS
BEGIN
    -- DECLARE @TableName NVARCHAR(200) = '_Root.Data';
    -- DECLARE @IncludeRequired BIT = 1;
    -- DECLARE @IncludeDefault BIT = 1

    DECLARE @BIDS NVARCHAR(30) = IIF(@IncludeRequired=1, IIF(@IncludeDefault=1, '(-1, -2)', '(-1)'), IIF(@IncludeDefault=1, '(-2)', '(null)'));
    DECLARE @TargetBIDString NVARCHAR(12) = IIF(@TargetBID IS NULL, ' > 0 ', CONCAT(' = ', @TargetBID));
    DECLARE @Columns NVARCHAR(MAX);

    SELECT @Columns = CONCAT(@Columns + ', ', '[',ColumnName,']')
    FROM TablesAndColumns
    WHERE TableName = @TableName
        AND ColumnName NOT IN ('BID', 'ValidToDT', 'ModifiedDT')
        AND IsComputed = 0
        AND IsIdentity = 0
    ;

    --PRINT @Columns;

    DECLARE @SQL NVARCHAR(MAX) = '

    INSERT INTO [@TABLENAME] ( BID, '+@Columns+' )
        SELECT B.BID, T.*
        FROM [Business.Data] B
        JOIN 
        (   SELECT '+@Columns+'
            FROM [@TABLENAME]
            WHERE BID IN '+@BIDS+'
        ) T ON 1=1
        WHERE NOT EXISTS (SELECT * FROM [@TABLENAME] D WHERE D.BID = B.BID AND D.'+@IDFieldName+' = T.'+@IDFieldName+')
            AND B.BID'+@TargetBIDString+'
    ;
    ';

    IF (@IncludeDefault=1)
        SET @SQL += '
    DELETE T
    FROM [@TABLENAME] T
    WHERE T.BID = -3
        AND T.'+@IDFieldName+' IN (SELECT T2.'+@IDFieldName+' FROM [@TABLENAME] T2 WHERE T2.BID = -2)
    ;

    UPDATE [@TABLENAME]
    SET BID = -3
    WHERE BID = -2
    ;
    ';

    SET @SQL = REPLACE(@SQL, '@TABLENAME', @TableName);

    --PRINT @SQL

    EXECUTE(@SQL);
END;");

            migrationBuilder.Sql(@"DELETE FROM [Business.TimeZone.Link] WHERE BID in (-2,-3);

INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID)
VALUES  (-2, 35)  -- Eastern Time
      , (-2, 20)  -- Central Time
      , (-2, 10)  -- Mountain Time
      , (-2, 4)   -- Pacific Time
      , (-2, 3)   -- Alaska Time
      , (-2, 2)   -- Hawaii Time
      , (-2, 15)  -- Arizona
      , (-2, 40)  -- Indiana
;

IF NOT EXISTS ( SELECT * FROM [Business.TimeZone.Link] WHERE BID = -1 AND TimeZoneID = 35 )
BEGIN
	INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID) VALUES (-1, 35);
END
IF NOT EXISTS ( SELECT * FROM [Business.TimeZone.Link] WHERE BID = -1 AND TimeZoneID = 20 )
BEGIN
	INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID) VALUES (-1, 20);
END
IF NOT EXISTS ( SELECT * FROM [Business.TimeZone.Link] WHERE BID = -1 AND TimeZoneID = 10 )
BEGIN
	INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID) VALUES (-1, 10);
END
IF NOT EXISTS ( SELECT * FROM [Business.TimeZone.Link] WHERE BID = -1 AND TimeZoneID = 4 )
BEGIN
	INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID) VALUES (-1, 4);
END
IF NOT EXISTS ( SELECT * FROM [Business.TimeZone.Link] WHERE BID = -1 AND TimeZoneID = 3 )
BEGIN
	INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID) VALUES (-1, 3);
END
IF NOT EXISTS ( SELECT * FROM [Business.TimeZone.Link] WHERE BID = -1 AND TimeZoneID = 2 )
BEGIN
	INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID) VALUES (-1, 2);
END
IF NOT EXISTS ( SELECT * FROM [Business.TimeZone.Link] WHERE BID = -1 AND TimeZoneID = 15 )
BEGIN
	INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID) VALUES (-1, 15);
END
IF NOT EXISTS ( SELECT * FROM [Business.TimeZone.Link] WHERE BID = -1 AND TimeZoneID = 40 )
BEGIN
	INSERT INTO [Business.TimeZone.Link] (BID, TimeZoneID) VALUES (-1, 40);
END");

            migrationBuilder.Sql(@"EXEC dbo.[Util.Table.CopyDefaultRecords] @TableName='Business.TimeZone.Link', @IDFieldName = 'TimeZoneID';");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
