﻿using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data.Common;
using System.Linq;

namespace Endor.EF
{
    public class DataProvider : IDataProvider
    {
        public DataProvider(short bid, ApiContext ctx)
        {
            this.BID = bid;
            this._ApiContext = ctx;
        }

        private ApiContext _ApiContext;
        public short BID { get; private set; }

        public DbConnection Connection => this._ApiContext.Database.GetDbConnection();
        public IQueryable<T> GetData<T>() where T : class, IAtom
        {
            return this._ApiContext.Set<T>().Where(x => x.BID == BID);
        }

        public IQueryable<IAtom> GetData(Type dataType)
        {
            System.Reflection.PropertyInfo propertyInfo = typeof(ApiContext).GetProperty(dataType.Name);

            if (propertyInfo == null)
                return null;

            if (!(propertyInfo.GetValue(this._ApiContext) is IQueryable<IAtom> dbSet))
                return null;

            return dbSet.Where(x => x.BID.Equals(this.BID));
        }
    }
}
