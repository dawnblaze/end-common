﻿using Endor.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Endor.EF
{
    public class MigrationHelper : IMigrationHelper
    {
        static MigrationHelper()
        {
            MigratedConnectionStrings = new ConcurrentDictionary<string, bool>();
            CopiedDefaultConnectionStrings = new ConcurrentDictionary<string, bool>();
        }

        public MigrationHelper(ITaskQueuer queuer)
        {
            this.Queuer = queuer;
        }

        private readonly ITaskQueuer Queuer;
        private static readonly ConcurrentDictionary<string, bool> MigratedConnectionStrings;
        private static readonly ConcurrentDictionary<string, bool> CopiedDefaultConnectionStrings;

        public void ResetCopyAllDefaults(ApiContext ctx)
        {
            CopiedDefaultConnectionStrings.Clear();
        }

        public void RunCopyAllDefaults(ApiContext ctx)
        {
            CopyAllDefaults(ctx, true);
        }

        private void CopyAllDefaults(ApiContext ctx, bool force = false)
        {
            string connString = ctx.Database.GetDbConnection().ConnectionString;
            bool isCopied = CopiedDefaultConnectionStrings.GetOrAdd(connString, false);

            if (!force)
            {
                if (isCopied)
                    return;
            }

            ctx.Database.ExecuteSqlRaw("EXEC [Util.Table.CopyAllDefaultRecords]");

            CopiedDefaultConnectionStrings.AddOrUpdate(connString, true, (t, u) => true);
        }

        public void MigrateDb(ApiContext ctx)
        {
            string connString = ctx.Database.GetDbConnection().ConnectionString;
            bool isMigrated = MigratedConnectionStrings.GetOrAdd(connString, false);

            if (isMigrated)
            {
                CopyAllDefaults(ctx);
                return;
            }

            var migrations = ctx.Database.GetPendingMigrations();

            if (migrations != null && migrations.Count() > 0)
            {
                var efAssembly = Assembly.GetAssembly(ctx.GetType());
                var endorMigrations = efAssembly.GetTypes().Where(t => t.IsSubclassOf(typeof(EndorMigration)));

                List<int> classTypesToReindex = new List<int>();
                List<(int ctid, int id)> entitiesToReindex = new List<(int, int)>();
                bool allAssembliesToClear = false;
                List<(int ctid, int id)> assembliesToClear = new List<(int, int)>();

                foreach (string migrationName in migrations)
                {
                    string migrationClassName = migrationName.Substring(migrationName.IndexOf('_') + 1);
                    migrationClassName = Regex.Replace(migrationClassName, "[^0-9A-Za-z_]", "");
                    Type migrationType = endorMigrations.FirstOrDefault(t => t.Name == migrationClassName);
                    if (migrationType != null)
                    {
                        try
                        {

                            EndorMigration endorMigration = (EndorMigration)Activator.CreateInstance(migrationType);
                            if (endorMigration != null)
                            {
                                if (endorMigration.ReindexClasstype().HasValue)
                                    classTypesToReindex.Add(endorMigration.ReindexClasstype().Value);

                                if (endorMigration.ReindexClasstypes() != null)
                                    classTypesToReindex.AddRange(endorMigration.ReindexClasstypes());

                                if (endorMigration.ReindexEntity().HasValue)
                                    entitiesToReindex.Add(endorMigration.ReindexEntity().Value);

                                if (endorMigration.ReindexEntities() != null)
                                    entitiesToReindex.AddRange(endorMigration.ReindexEntities());

                                if (endorMigration.ClearAllAssemblies())
                                    allAssembliesToClear = true;

                                if (endorMigration.ClearAssemblies() != null)
                                    assembliesToClear.AddRange(endorMigration.ClearAssemblies());
                            }
                        }
                        catch
                        {
                        }
                    }
                }

                ctx.Database.Migrate();

                entitiesToReindex = entitiesToReindex.Distinct().ToList();
                classTypesToReindex = classTypesToReindex.Distinct().ToList();

                entitiesToReindex = entitiesToReindex.Where(t => !classTypesToReindex.Contains(t.ctid)).ToList();

                if (entitiesToReindex.Count > 0 || classTypesToReindex.Count > 0)
                {
                    List<short> bids = ctx.BusinessData.Where(b => b.IsActive && b.BID > 0).Select(t => t.BID).ToList();

                    foreach (short bid in bids)
                    {
                        foreach (int ctid in classTypesToReindex)
                        {
                            try
                            {
                                Queuer.IndexClasstype(bid, ctid);
                            }
                            catch
                            {
                            }
                        }

                        foreach ((int ctid, int id) entity in entitiesToReindex)
                        {
                            try
                            {
                                Queuer.IndexModel(bid, entity.ctid, entity.id);
                            }
                            catch
                            {
                            }
                        }
                    }
                }


                if (allAssembliesToClear)
                {
                    List<short> bids = ctx.BusinessData.Where(b => b.IsActive && b.BID > 0).Select(t => t.BID).ToList();

                    foreach (short bid in bids)
                    {
                        try
                        {
                            Queuer.ClearAllAssemblies(bid);
                        }
                        catch
                        {
                        }
                    }
                }

                else
                {
                    if (assembliesToClear.Count > 0)
                    {
                        assembliesToClear = assembliesToClear.Distinct().ToList();

                        List<short> bids = ctx.BusinessData.Where(b => b.IsActive && b.BID > 0).Select(t => t.BID).ToList();

                        foreach (short bid in bids)
                        {
                            foreach ((int ctid, int id) in assembliesToClear)
                            {
                                Queuer.ClearAssembly(bid, ctid, id);
                            }
                        }
                    }

                }
            }

            MigratedConnectionStrings.AddOrUpdate(connString, true, (t, u) => true);
            
            //Run CopyAllDefault values AFTER migration finished.
            CopyAllDefaults(ctx);
        }
    }
}
