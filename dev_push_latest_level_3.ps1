param (
    [Parameter(Mandatory=$true)][string]$mygetkey
)

function Publish(
   [Parameter(Mandatory)][string]$project,
   [Parameter(Mandatory)][string]$mygetkey
) {
	& ./dev_push_latest_package.ps1 $project $mygetkey
}

pushd .\_scripts
$valid = .\_dev_validate
popd

if ($valid) {
	Publish 'Endor.Api.Common' $mygetkey

	Publish 'Endor.GLEngine' $mygetkey

	Publish 'Endor.Pricing' $mygetkey
}



