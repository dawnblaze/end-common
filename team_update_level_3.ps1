param (
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [Parameter(Mandatory=$true)][string]$teamname,
    [string]$build = "Debug"
)

pushd .\_scripts
$valid = .\_team_validate
popd

if ($valid) {
	& .\_team_publish_package 'Endor.Api.Common' $mygetkey $teamname

	& .\_team_publish_package 'Endor.GLEngine' $mygetkey $teamname

	& .\_team_publish_package 'Endor.Pricing' $mygetkey $teamname
}


