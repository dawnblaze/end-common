﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.AzureStorage
{
    public class DocumentPermissionOptions
    {
        public bool CanDelete { get; set; }
        public bool CanMove { get; set; }
        public bool CanRename { get; set; }
        public bool IsShared { get; set; }

        public DocumentPermissionOptions()
        {
            this.CanDelete = true;
            this.CanMove = true;
            this.CanRename = true;
            this.IsShared = false;
        }
        
    }
}
