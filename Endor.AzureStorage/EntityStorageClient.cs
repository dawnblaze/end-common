﻿using Endor.DocumentStorage.Models;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.WindowsAzure.Storage.DataMovement;
using System.Globalization;
using Microsoft.WindowsAzure.Storage.Shared.Protocol;

namespace Endor.AzureStorage
{
    /// <summary>
    /// Entity Storage Client
    /// </summary>
    public class EntityStorageClient
    {
        public const int DefaultSASReadExpirationHours = 4392;
        public const char SpecialFolderBlobName = '_';
        private readonly CloudStorageAccount _storageAccount;
        private readonly CloudBlobClient _client;
        private readonly short _bid;
        private readonly short? _aid;
        private DocumentPermissionOptions DefaultDocumentPermissionOptions = new DocumentPermissionOptions();

        /// <summary>
        /// Entity Storage Client
        /// </summary>
        /// <param name="connectionString">Connection String</param>
        /// <param name="bid">Business ID</param>
        /// <param name="aid">Assocation ID</param>
        public EntityStorageClient(string connectionString, short bid, short? aid = null)
        {
            this._storageAccount = CloudStorageAccount.Parse(connectionString);
            this._client = this._storageAccount.CreateCloudBlobClient();
            this._bid = bid;
            this._aid = aid;
        }

        #region public image CRUD

        /// <summary>
        /// Stores an image into a specific bucket using the name "Image" for a permanent entity
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="classtypeID"></param>
        /// <param name="ID"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public async Task<DMItem> StoreDefaultImage(Stream stream, DMID id, string contentType, int createdByID, int createdByCTID)
        {
            return await AddFile(stream, StorageBin.Permanent, Bucket.Data, id, $"Image", contentType, createdByID, createdByCTID);
        }

        /// <summary>
        /// Returns a boolean that indicates whether a specific entity has a default image
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> HasDefaultImage(DMID id)
        {
            CloudBlobContainer businessContainer = await GetOrCreateBusinessContainerAsync(id);
            CloudBlobDirectory classtypeDirectory = businessContainer.GetDirectoryReference($"Data/{id.ctid}/{id.id}");

            CloudBlockBlob blob = classtypeDirectory.GetBlockBlobReference("Image");

            return await blob.ExistsAsync();
        }

        /// <summary>
        /// Stores a stream into a specific bucket using the blob name "Image"
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="tempGuid"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        public async Task<DMItem> StoreTemporaryImage(Stream stream, Guid tempGuid, string contentType, int createdByID, int createdByCTID)
        {
            string fileName = $"Image";

            DMItem item = await AddFile(stream, StorageBin.Temp, Bucket.Data, new DMID() { guid = tempGuid }, fileName, contentType, createdByID, createdByCTID, true);

            return item;
        }

        /// <summary>
        /// Returns the storage uri for a default image on an unsaved entity
        /// </summary>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public async Task<Uri> GetTempDefaultImageUri(Guid tempGuid)
        {
            CloudBlobContainer businessContainer = await GetOrCreateBusinessContainerAsync();
            CloudBlobDirectory classtypeDirectory = businessContainer.GetDirectoryReference($"temp/{tempGuid}/Data");

            CloudBlockBlob blob = classtypeDirectory.GetBlockBlobReference("Image");

            string sasToken = blob.GetSharedAccessSignature(new SharedAccessBlobPolicy() { Permissions = SharedAccessBlobPermissions.Read, SharedAccessExpiryTime = DateTime.UtcNow.AddDays(1) });
            UriBuilder resultUri = new UriBuilder(blob.Uri)
            {
                Query = sasToken
            };

            return resultUri.Uri;
        }

        /// <summary>
        /// Returns the storage uri for a default image on a saved entity
        /// </summary>
        /// <param name="classtypeid">ClassType ID for this Entity Type</param>
        /// <param name="id">ID of the Entity</param>
        /// <returns></returns>
        public async Task<Uri> GetDefaultImageUri(int classtypeid, int id)
        {
            CloudBlobContainer businessContainer = await GetOrCreateBusinessContainerAsync();
            CloudBlobDirectory classtypeDirectory = businessContainer.GetDirectoryReference($"Data/{classtypeid}/{id}");

            CloudBlockBlob blob = classtypeDirectory.GetBlockBlobReference("Image");
            string sasToken = blob.GetSharedAccessSignature(new SharedAccessBlobPolicy() { Permissions = SharedAccessBlobPermissions.Read, SharedAccessExpiryTime = DateTime.UtcNow.AddDays(1) });
            UriBuilder resultUri = new UriBuilder(blob.Uri)
            {
                Query = sasToken
            };

            return resultUri.Uri;
        }

        /// <summary>
        /// Returns a read only storage uri for a default image on a saved entity without an expiration date
        /// </summary>
        /// <param name="classtypeid">ClassType ID for this Entity Type</param>
        /// <param name="id">ID of the Entity</param>
        /// <returns></returns>
        public async Task<Uri> GetDefaultImageUriWithNoExpiration(int classtypeid, int id)
        {
            CloudBlobContainer businessContainer = await GetOrCreateBusinessContainerAsync();
            CloudBlobDirectory classtypeDirectory = businessContainer.GetDirectoryReference($"Data/{classtypeid}/{id}");

            CloudBlockBlob blob = classtypeDirectory.GetBlockBlobReference("Image");
            string sasToken = blob.GetSharedAccessSignature(new SharedAccessBlobPolicy() { Permissions = SharedAccessBlobPermissions.Read, SharedAccessExpiryTime = DateTime.MaxValue.ToUniversalTime() });
            UriBuilder resultUri = new UriBuilder(blob.Uri)
            {
                Query = sasToken
            };

            return resultUri.Uri;
        }

        /// <summary>
        /// Checks if blob exists using relative path and IDMID
        /// </summary>
        /// <param name="relativePath">Can be prefixed with non-existent folders</param>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="caseInsensitiveComparison">True by default</param>
        /// <returns>True if blob exists, false otherwise</returns>
        public async Task<bool> Exists(string relativePath, StorageBin storageBin, Bucket bucket, IDMID id, bool caseInsensitiveComparison = true)
        {
            if(string.IsNullOrWhiteSpace(relativePath))
                throw new ArgumentException($"{relativePath} must be provided", nameof(relativePath));

            string prefix = storageBin.BlobNamePrefix(bucket, id);
            string newBlobName = $"{prefix}/{relativePath}";
            string fileName = Path.GetFileName(newBlobName);
            string relativeDirectory = Path.GetDirectoryName(newBlobName)
                .Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
            //Gets either association or business container depending on IDMID.classFolder 'association' : 'static'
            CloudBlobContainer container = await GetOrCreateBusinessContainerAsync(id);
            CloudBlobDirectory directory = container.GetDirectoryReference(relativeDirectory);

            //Get IListBlobItems Segmented
            BlobResultSegment blobResultSegment = await directory.ListBlobsSegmentedAsync(true, BlobListingDetails.None, null, null, null, null);

            //Enumerate results to look for a match
            foreach (IListBlobItem listBlobItem in blobResultSegment.Results)
            {
                if (!(listBlobItem is CloudBlockBlob cloudBlockBlob)) continue;

                //Reduce blob path to filename and extension only
                var blobFileName = Path.GetFileName(cloudBlockBlob.Name);
                if(caseInsensitiveComparison)
                {
                    if (Path.GetFileName(blobFileName.ToLowerInvariant()) == fileName.ToLowerInvariant())
                        return true;
                }
                else
                {
                    if (blobFileName == fileName)
                        return true;
                }
            }

            return false;
        }

        #endregion

        #region public CRUD

        /// <summary>
        /// Uploads a stream to document storage
        /// </summary>
        /// <param name="stream">Make sure this is non null</param>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        /// <param name="contentType"></param>
        /// <param name="createdByID">Created by (user/employee/contact) ID.  Required for Document and Report buckets</param>
        /// <param name="createdByCTID">Created by Class Type ID.  Required for Document and Report buckets</param>
        /// <param name="fullyQualifyURLWithSAS">if True and storageBin==Temp, sets DMItem.URL to fully qualified URL w/SAS token</param>
        /// <param name="permissionOptions"></param>
        /// <param name="metadata">Additional Metadata to be appended to the blob</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">stream cannot be null</exception>
        public async Task<DMItem> AddFile(Stream stream, StorageBin storageBin, Bucket bucket, IDMID id, string fileName, string contentType, int? createdByID, int? createdByCTID, bool fullyQualifyURLWithSAS = false, DocumentPermissionOptions permissionOptions = null, Dictionary<string, string> metadata = null)
        {
            if (permissionOptions == null)
            {
                permissionOptions = DefaultDocumentPermissionOptions;
            }

            if (stream == null)
                throw new ArgumentNullException("stream", "Stream cannot be null");
            if (String.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("cannot be null, empty, or only whitespace", nameof(fileName));

            if (bucket != Bucket.Data)
            {
                if (!createdByID.HasValue || createdByID == default(int))
                    throw new ArgumentException("cannot be default value", nameof(createdByID));

                if (!createdByCTID.HasValue || createdByCTID == default(int))
                    throw new ArgumentException("cannot be default value", nameof(createdByCTID));
            }

            if (stream.Position != 0)
                stream.Position = 0;

            CloudBlobClient client = _storageAccount.CreateCloudBlobClient();

            CloudBlobContainer businessContainer = await GetOrCreateBusinessContainerAsync(id, client);

            string prefix = storageBin.BlobNamePrefix(bucket, id);
            string newBlobName = $"{prefix}/{fileName}";
            CloudBlockBlob blob = businessContainer.GetBlockBlobReference(newBlobName);

            string[] newFolders = GetFolders(newBlobName.Replace(prefix + "/", ""));

            if (newFolders != null)
            {
                foreach (string newFolder in newFolders)
                {
                    CloudBlockBlob makeIfNotExistFolder = businessContainer.GetBlockBlobReference($"{prefix}/{newFolder}/{SpecialFolderBlobName}");

                    if (!(await makeIfNotExistFolder.ExistsAsync()))
                        await makeIfNotExistFolder.UploadTextAsync("Endor Team.  2017-09. AR, BK, NS, RK, NH, MSC, JH");
                }
            }

            await TransferManager.UploadAsync(stream, blob, new UploadOptions(), CreateSingleTransferOverwriteContext());

            await blob.FetchAttributesAsync();

            if (!String.IsNullOrWhiteSpace(contentType))
                blob.Properties.ContentType = contentType;
            else
                contentType = blob.Properties.ContentType;

            await blob.SetPropertiesAsync();

            DateTime currentDT = DateTime.UtcNow;

            blob.SetMetadataValue(Constants.CreatedDT, currentDT);
            blob.SetMetadataValue(Constants.ModifiedByClasstypeID, createdByCTID);

            if (createdByID.HasValue)
            {
                blob.SetMetadataValue(Constants.ModifiedByID, createdByID.Value);
                blob.SetMetadataValue(Constants.CreatedByID, createdByID.Value);
            }

            blob.SetMetadataValue(Constants.CanDelete, permissionOptions.CanDelete);
            blob.SetMetadataValue(Constants.CanMove, permissionOptions.CanMove);
            blob.SetMetadataValue(Constants.CanRename, permissionOptions.CanRename);
            blob.SetMetadataValue(Constants.IsShared, permissionOptions.IsShared);

            if (metadata != null && metadata.Keys.Any())
            {
                foreach (string key in metadata.Keys)
                {
                    blob.SetMetadataValue(key, metadata[key]);
                }
            }

            await blob.SetMetadataAsync();

            MediaType mediaType = null;

            if (MediaTypes.Lookup.ContainsKey(contentType))
                mediaType = MediaTypes.Lookup[contentType];

            DMItem dmItem = new DMItem()
            {
                URL = newBlobName,
                Path = blob.RelativePath(storageBin.BlobNamePrefix(bucket, id)),
                Name = fileName,
                IsFolder = false,
                Size = stream.Length,
                MimeType = contentType,
                MediaType = mediaType,
                AssetType = mediaType?.AssetType,
                CreatedDT = currentDT,
                ModifiedDT = currentDT,
                ModifiedByClasstypeID = createdByCTID,
                ModifiedByID = createdByID,
                CreatedByID = createdByID,
                CanDelete = permissionOptions.CanDelete,
                CanRename = permissionOptions.CanRename,
                CanMove = permissionOptions.CanMove,
                IsShared = permissionOptions.IsShared,
            };

            if (fullyQualifyURLWithSAS && storageBin == StorageBin.Temp)
                dmItem.URL = newBlobName + blob.GetSharedAccessSignature(GetDefaultReadPolicy());

            return dmItem;
        }

        private static SingleTransferContext CreateSingleTransferOverwriteContext()
        {
            return new SingleTransferContext() { ShouldOverwriteCallback = (src, dest) => { return true; } };
        }

        /// <summary>
        /// Add folder by name to a bucket
        /// </summary>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public async Task<DMItem> AddFolder(StorageBin storageBin, Bucket bucket, IDMID id, string folderName, int createdByID, int createdByCTID, DocumentPermissionOptions permissionOptions = null)
        {
            if (permissionOptions == null)
            {
                permissionOptions = DefaultDocumentPermissionOptions;
            }

            CloudBlobContainer blobContainer = await GetOrCreateBusinessContainerAsync(id);

            using (MemoryStream stream = new MemoryStream())
            {
                StreamWriter sw = new StreamWriter(stream);
                await sw.WriteAsync("Endor Team.  2017-09. AR, BK, NS, RK, NH, MSC, JH");
                await sw.FlushAsync();

                stream.Position = 0;
                try
                {
                    DMItem result = await AddFile(stream, storageBin, bucket, id, $"{folderName}/{SpecialFolderBlobName}", MediaTypes.text_plain.MimeType, createdByID, createdByCTID, false, permissionOptions);

                    if (result != null)
                        result.IsFolder = true;

                    return result;
                }
                catch
                {
                    return null;
                }
            }
        }

        //DocumentList is a misnomer, it's a tree, but oh well it's well established already
        /// <summary>
        /// Returns a list of documents, folders, and folder contents from the root folder
        /// </summary>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="useFlatDocumentListing">if TRUE, return only blobs in a flat list, otherwise return folders and blobs in a tree</param>
        /// <param name="includeSpecialFolderBlobName">Set to true if you also need the special '_' file in the .thumbnails folder copied</param>
        /// <returns></returns>
        public async Task<List<DMItem>> GetDocumentList(StorageBin storageBin, Bucket bucket, IDMID id, bool useFlatDocumentListing = false, bool includeSpecialFolderBlobName = false)
        {
            return await GetDocumentList(storageBin, bucket, id, null, useFlatDocumentListing, includeSpecialFolderBlobName);
        }

        //DocumentList is a misnomer, it's a tree, but oh well it's well established already
        /// <summary>
        /// Returns a list of documents, folders, and folder contents from a relative path
        /// </summary>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="relativePath"></param>
        /// <param name="useFlatDocumentListing">if TRUE, return only blobs in a flat list, otherwise return folders and blobs in a tree</param>
        /// <param name="includeSpecialFolderBlobName">Set to true if you also need the special '_' file in the .thumbnails folder copied</param>
        /// <returns></returns>
        public async Task<List<DMItem>> GetDocumentList(StorageBin storageBin, Bucket bucket, IDMID id, string relativePath, bool useFlatDocumentListing = false, bool includeSpecialFolderBlobName = false)
        {
            CloudBlobContainer businessContainer = await GetOrCreateBusinessContainerAsync(id);

            if (relativePath == null || relativePath.Length < 1)
                return await GetChildDocumentItems(businessContainer, $"{storageBin.BlobNamePrefix(bucket, id)}{relativePath}", null, useFlatDocumentListing, includeSpecialFolderBlobName);
            else 
            {
                if (relativePath.Length > 0 && relativePath.EndsWith("/" + SpecialFolderBlobName))
                {
                    relativePath = relativePath.Substring(0, relativePath.Length - 1);
                }

                return await GetChildDocumentItems(businessContainer, $"{storageBin.BlobNamePrefix(bucket, id)}", relativePath, useFlatDocumentListing, includeSpecialFolderBlobName);
            }
        }

        /// <summary>
        /// Deletes a File
        /// </summary>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        public async Task<int> DeleteFile(StorageBin storageBin, Bucket bucket, IDMID id, string filename, bool caseInsensitiveComparison = false)
        {
            // if Bucket is not trash, move all affected files to trash
            // if Bucket is trash, delete all affected files
            // if target is a folder? then move affect all contents in and under it
            // if target is a single file, only affect that file.
            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync(id);

            string effectiveBlobName = $"{storageBin.BlobNamePrefix(bucket, id)}/{filename}";
            if (effectiveBlobName.EndsWith($"/{SpecialFolderBlobName}"))
            {
                CloudBlobDirectory cbd = cbc.GetDirectoryReference(effectiveBlobName.Substring(0, effectiveBlobName.Length - 1));
                return await DeleteFolder(cbd, storageBin);
            }
            else
            {
                if (caseInsensitiveComparison)
                {
                    string compareFileName = Path.GetFileName(effectiveBlobName);
                    string relativeDirectory = Path.GetDirectoryName(effectiveBlobName)
                        .Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                    CloudBlobDirectory directory = cbc.GetDirectoryReference(relativeDirectory);
                    BlobResultSegment blobResultSegment = await directory.ListBlobsSegmentedAsync(true, BlobListingDetails.None, null, null, null, null);

                    CloudBlockBlob foundBlob = null;
                    //Enumerate results to look for a match
                    foreach (IListBlobItem listBlobItem in blobResultSegment.Results)
                    {
                        if (!(listBlobItem is CloudBlockBlob cloudBlockBlob)) continue;

                        //Reduce blob path to filename and extension only
                        var blobFileName = Path.GetFileName(cloudBlockBlob.Name);
                        if (Path.GetFileName(blobFileName.ToLowerInvariant()) == compareFileName.ToLowerInvariant())
                            foundBlob = (CloudBlockBlob)listBlobItem;
                    }

                    return await DeleteBlob(foundBlob, storageBin, bucket, id, filename);
                }
                else
                {
                    CloudBlockBlob cbb = cbc.GetBlockBlobReference(effectiveBlobName);
                    return await DeleteBlob(cbb, storageBin, bucket, id, filename);
                }
            }
        }

        #endregion

        #region private crud helpers

        private async Task<int> DeleteFolder(CloudBlobDirectory cbd, StorageBin storageBin)
        {
            BlobContinuationToken bct = new BlobContinuationToken();
            BlobResultSegment brs = await cbd.ListBlobsSegmentedAsync(true, BlobListingDetails.None, null, bct, null, null);

            int totalAffected = 0;
            foreach (IListBlobItem item in brs.Results)
            {
                CloudBlockBlob blob = item as CloudBlockBlob;
                if (blob != null)
                {
                    totalAffected += await DeleteBlob(blob, storageBin);
                }
            }

            return totalAffected;
        }


        /// <summary>
        /// Deletes a blob with fully supplied blob storage schema
        /// </summary>
        /// <param name="blob"></param>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private async Task<int> DeleteBlob(CloudBlockBlob blob, StorageBin storageBin, Bucket bucket, IDMID id, string fileName)
        {
            if (storageBin == StorageBin.Trash)
            {
                if (await blob.DeleteIfExistsAsync())
                    return 1;
            }
            else
            {
                string trashBlobName = $"{StorageBin.Trash.BlobNamePrefix(bucket, id, DateTime.UtcNow)}/{fileName}";
                CloudBlockBlob targetBlob = blob.Container.GetBlockBlobReference(trashBlobName);
                if (!(await blob.ExistsAsync()))
                    return 0;

                await TransferManager.CopyAsync(blob, targetBlob, true,
                    new CopyOptions() { },
                    new SingleTransferContext() { ShouldOverwriteCallback = (src, dest) => { return true; } });

                if (await blob.DeleteIfExistsAsync())
                    return 1;
            }

            return 0;
        }

        /// <summary>
        /// Deletes a blob with unknown schema by parsing blob name.
        /// </summary>
        /// <param name="blob"></param>
        /// <param name="storageBin"></param>
        /// <returns></returns>
        private async Task<int> DeleteBlob(CloudBlockBlob blob, StorageBin storageBin)
        {
            if (storageBin == StorageBin.Trash)
            {
                if (await blob.DeleteIfExistsAsync())
                {
                    return 1;
                }
            }
            else
            {
                if (blob.TryParseBlobName(out StorageBin srcStorageBin, out Bucket srcBucket, out IDMID srcID, out string fileName))
                {
                    return await DeleteBlob(blob, srcStorageBin, srcBucket, srcID, fileName);
                }
            }

            return 0;
        }

        #endregion

        #region public action
        public async Task<int> Duplicate(StorageBin bin, Bucket bucket, IDMID ID, string[] files)
        {
            if (ID == null)
                throw new ArgumentNullException("Cannot duplicate from null entity");

            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync(ID);
            string prefix = bin.BlobNamePrefix(bucket, ID);
            CloudBlobDirectory folder = cbc.GetDirectoryReference(prefix);
            BlobContinuationToken bct = new BlobContinuationToken();

            //get existing blobs
            BlobResultSegment blobTree = await folder.ListBlobsSegmentedAsync(true, BlobListingDetails.None, null, bct, null, null);
            HashSet<string> existingFiles = new HashSet<string>();
            HashSet<string> existingFolders = new HashSet<string>();

            foreach (IListBlobItem blob in blobTree.Results)
            {
                var blockBlob = (blob as CloudBlockBlob);
                if (blockBlob != null)
                {
                    if (blockBlob.Name[blockBlob.Name.Length - 1] == SpecialFolderBlobName)
                    {
                        existingFolders.Add(blob.RelativePath(prefix));
                    }
                    else
                    {
                        existingFiles.Add(blob.RelativePath(prefix));
                    }
                }
            }

            var isFolderDictionary = new Dictionary<string, bool>();

            //when we loop through files, we'll reference this array for the name to dupe to
            string[] dupeNames = new string[files.Length];
            //get the duplicate name
            //and while we're looping,
            //validate that each file or folder exists
            for (int i = 0; i < files.Length; i++)
            {
                string toDupe = files[i];
                string fileMatch = existingFiles.Contains(toDupe) ? toDupe : null;
                string folderMatch = existingFolders.Contains(toDupe) ? toDupe : existingFolders.Contains(toDupe.TrimEnd('/')) ? toDupe.TrimEnd('/') : null;

                if (fileMatch == null && folderMatch == null)
                    throw new ArgumentException($"Cannot duplicate file or folder '{toDupe}' because there is no corresponding blob");
                else
                {
                    
                    bool isFolder = folderMatch != null;
                    isFolderDictionary[files[i]] = isFolder;
                    string candidateDuplicateName;
                    bool goodCandidate = false;
                    int dupeNumber = 2;
                    do
                    {
                        if (isFolder)
                        {
                            candidateDuplicateName = $"{toDupe} ({dupeNumber})";
                            goodCandidate = !existingFolders.Contains(candidateDuplicateName);
                        }
                        else
                        {
                            int dotIndex = toDupe.LastIndexOf('.');
                            string upToPeriod = toDupe.Substring(0, dotIndex);
                            string extension = toDupe.Substring(dotIndex + 1);
                            candidateDuplicateName = $"{upToPeriod} ({dupeNumber}).{extension}";
                            goodCandidate = !existingFiles.Contains(candidateDuplicateName);
                        }
                        dupeNumber++;
                    } while (!goodCandidate);

                    dupeNames[i] = candidateDuplicateName;
                }
            }

            uint blobsCopied = 0;
            uint blobsSkipped = 0;
            var cloneTasks = new List<Task<copyResult>>();
            //loop over files again, now that we have a to-name list and we know all the files are good to duplicate
            for (int i = 0; i < files.Length; i++)
            {
                string fromBlobName = files[i];
                string toBlobName = dupeNames[i];
                //copyResult cpCount;
                if (!isFolderDictionary[files[i]])
                {
                    cloneTasks.Add(this.CloneBlobFile(folder, fromBlobName, toBlobName, prefix, cbc));
                    //cpCount = await this.CloneBlobFile(folder, fromBlobName, toBlobName, prefix, cbc);
                }
                else
                {
                    cloneTasks.Add(this.CloneBlobDirectory(folder, fromBlobName, toBlobName, prefix, cbc));
                    //cpCount = await this.CloneBlobDirectory(folder, fromBlobName, toBlobName, prefix, cbc);
                }
                //blobsCopied += cpCount.blobsCopied;
                //blobsSkipped += cpCount.blobsSkipped;
            }

            var doneTasks = await Task.WhenAll(cloneTasks);

            blobsCopied = (uint)(doneTasks.Sum(x => x.blobsCopied));
            blobsSkipped = (uint)(doneTasks.Sum(x => x.blobsSkipped));

            return Convert.ToInt32(blobsCopied - blobsSkipped);
        }

        //-----------------------------------------------------------------------------------
        protected async Task<copyResult> CloneBlobDirectory(
        CloudBlobDirectory folder,
        string fromBlobName,
        string toBlobName,
        string prefix,
        CloudBlobContainer cbc)
        {
            uint blobsSkipped = 0;
            uint blobsCopied = 0;
            var item = folder.GetDirectoryReference(fromBlobName);
            CloudBlobDirectory fromBlob = item as CloudBlobDirectory;
            if (fromBlob != null)
            {
                string destBlobName = $"{prefix}/{toBlobName}";

                CloudBlobDirectory toBlob = cbc.GetDirectoryReference(destBlobName);
                try
                {
                    await TransferManager.CopyDirectoryAsync(fromBlob, toBlob, true,
                        new CopyDirectoryOptions() {
                            Recursive = true,
                            Delimiter = '/'
                        },
                        new DirectoryTransferContext() {
                            ShouldOverwriteCallback = (s, d) =>
                            {
                                blobsSkipped++;
                                return false;
                            }
                        });

                }
                //#warning skipping throws a whole exception that we have to catch
                catch (TransferSkippedException ex)
                {
                    var e = ex;
                    Console.WriteLine(e.Message);
                }
                catch (Exception ex)
                {
                    var e = ex;
                    Console.WriteLine(e.Message);
                }

                blobsCopied++;
            }

            return new copyResult() { blobsCopied = blobsCopied, blobsSkipped = blobsSkipped };
        }

        //-----------------------------------------------------------------------------------
        protected async Task<copyResult> CloneBlobFile(
                CloudBlobDirectory folder,
                string fromBlobName,
                string toBlobName,
                string prefix,
                CloudBlobContainer cbc)
        {
            uint blobsSkipped = 0;
            uint blobsCopied = 0;
            var item = folder.GetBlockBlobReference(fromBlobName);
            CloudBlockBlob fromBlob = item as CloudBlockBlob;
            if (fromBlob != null)
            {
                string destBlobName = $"{prefix}/{toBlobName}";

                CloudBlockBlob toBlob = cbc.GetBlockBlobReference(destBlobName);
                try
                {
                    await TransferManager.CopyAsync(fromBlob, toBlob, true,
                        new CopyOptions() { },
                        new SingleTransferContext()
                        {
                            ShouldOverwriteCallback = (s, d) =>
                            {
                                blobsSkipped++;
                                return false;
                            },
                        });
                }
                //#warning skipping throws a whole exception that we have to catch
                catch (TransferSkippedException ex)
                {
                    var e = ex;
                    Console.WriteLine(e.Message);
                }
                catch (Exception ex)
                {
                    var e = ex;
                    Console.WriteLine(e.Message);
                }

                blobsCopied++;
            }

            return new copyResult() { blobsCopied=blobsCopied, blobsSkipped = blobsSkipped };
        }
        
        protected struct copyResult
        {
            public uint blobsCopied { get; set; }
            public uint blobsSkipped { get; set; }
        }

        public async Task<int> Move(
            StorageBin fromBin, 
            Bucket fromBucket, 
            IDMID fromID, 
            string[] files, 
            StorageBin? toBin = null, 
            Bucket? toBucket = null, 
            IDMID toID = null, 
            string toFolder = null,
            string withFolder = null)
        {
            return await CopyInternal(fromBin, fromBucket, fromID, files, toBin, toBucket, toID, toFolder, withFolder: withFolder, deleteSrcAfterCopy: true);
        }

        /// <summary>
        /// Renames folder and contents of folder
        /// </summary>
        /// <param name="fromBin"></param>
        /// <param name="fromBucket"></param>
        /// <param name="fromID"></param>
        /// <param name="folderPath"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        public async Task<int> RenameFolder(
            StorageBin fromBin,
            Bucket fromBucket,
            IDMID fromID,
            string folderPath,
            string newName)
        {
            int renameCount = 0;
            if (folderPath.EndsWith("/" + SpecialFolderBlobName))
            {
                if (!folderPath.StartsWith("/"))
                    folderPath = '/' + folderPath;

                string newFolderName = GetRenamedName(newName, folderPath);
                
                List<DMItem> items = (await GetFlatFilesAndFolders(fromBin, fromBucket, fromID, folderPath)).Where(i => i.Name != folderPath).ToList();
                if (items != null && items.Count > 0)
                {
                    renameCount += await Move(
                        fromBin,
                        fromBucket,
                        fromID,
                        items.Select((item) =>
                        {
                            return item.Name;
                        }).ToArray(),
                        fromBin,
                        fromBucket,
                        fromID,
                        toFolder: newFolderName,
                        withFolder: folderPath);
                }

                renameCount += await CopyInternal(
                    fromBin, 
                    fromBucket, 
                    fromID, 
                    new string[] { folderPath }, 
                    fromBin, 
                    fromBucket, 
                    fromID,
                    renameName: newName, 
                    deleteSrcAfterCopy: true);
            }
            return renameCount;
        }

        /// <summary>
        /// Moves folders and contents of folders from one place to another
        /// </summary>
        /// <param name="fromBin"></param>
        /// <param name="fromBucket"></param>
        /// <param name="fromID"></param>
        /// <param name="folders"></param>
        /// <param name="toBin">optional</param>
        /// <param name="toBucket">optional</param>
        /// <param name="toID">optional</param>
        /// <param name="toFolder">optional</param>
        /// <returns></returns>
        public async Task<int> MoveFolders(
            StorageBin fromBin, 
            Bucket fromBucket, 
            IDMID fromID, 
            string[] folders, 
            StorageBin? toBin = null, 
            Bucket? toBucket = null, 
            IDMID toID = null, string toFolder = null)
        {
            if (folders == null || folders.Length < 1)
                return 0;

            int moveCount = 0;
            foreach(string originalFolderName in folders)
            {
                string folderName = originalFolderName;
                if (folderName.EndsWith("/"+SpecialFolderBlobName))
                {
                    if (!folderName.StartsWith("/"))
                        folderName = '/'+folderName;

                    List<DMItem> items = (await GetFlatFilesAndFolders(fromBin, fromBucket, fromID, folderName)).Where(i => i.Name != folderName).ToList();
                    if (items != null && items.Count > 0)
                    {
                        moveCount += await Move(
                            fromBin,
                            fromBucket,
                            fromID,
                            items.Select((item) =>
                            {
                                return item.Name;
                            }).ToArray(),
                            toBin,
                            toBucket,
                            toID,
                            toFolder,
                            withFolder: folderName);
                    }

                    moveCount += await CopyInternal(fromBin, fromBucket, fromID, new string[] { folderName }, toBin, toBucket, toID, toFolder, deleteSrcAfterCopy: true);
                }
            }
            return moveCount;
        }

        /// <summary>
        /// Rename a single blob to a new path and filename
        /// </summary>
        /// <param name="bin"></param>
        /// <param name="bucket"></param>
        /// <param name="ID"></param>
        /// <param name="pathAndFileName"></param>
        /// <param name="newPathAndFileName"></param>
        /// <returns></returns>
        public async Task<int> Rename(StorageBin bin, Bucket bucket, IDMID ID, string pathAndFileName, string newPathAndFileName)
        {
            return await CopyInternal(bin, bucket, ID, new string[] { pathAndFileName }, bin, bucket, ID, renameName: newPathAndFileName, deleteSrcAfterCopy: true);
        }

        public async Task<int> Copy(StorageBin fromBin, Bucket fromBucket, IDMID fromID, string[] files, StorageBin? toBin = null, Bucket? toBucket = null, IDMID toID = null, string toFolder = null)
        {
            return await CopyInternal(fromBin, fromBucket, fromID, files, toBin, toBucket, toID, toFolder);
        }

        /// <summary>
        /// Copies from ctid/template folder into ID
        /// </summary>
        /// <param name="storageBin"></param>
        /// <param name="srcid"></param>
        /// <returns>number of files copied and not skipped</returns>
        public async Task<int> Initialize(StorageBin storageBin, IDMID srcid)
        {
            if (srcid == null || !srcid.ctid.HasValue)
                throw new ArgumentException("Initialization requires CTID");

            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync(srcid);
            DMID source = new DMID()
            {
                id = null,
                guid = null,
                ctid = srcid.ctid,
                classFolder = "template"
            };
            switch (storageBin)
            {
                case StorageBin.Temp:
                    return await _initAll(cbc, source, StorageBin.Temp, new DMID()
                    {
                        guid = srcid.guid
                    });
                case StorageBin.Permanent:
                    return await _initAll(cbc, source, StorageBin.Permanent, new DMID()
                    {
                        ctid = srcid.ctid,
                        id = srcid.id
                    });
                default:
                    throw new ArgumentException("Can only initialize temp and permanent storage bins");
            }
        }

        public async Task<int> MakePermanent(Guid srcID, IDMID permID)
        {
            //if (srcID == null)
            //    throw new ArgumentNullException("srcID", "Cannot make null entity permanent");
            if (permID == null)
                throw new ArgumentNullException("permID", "Cannot make null entity permanent");

            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync(permID);
            int movedFiles = 0;
            if (!permID.id.HasValue || !permID.ctid.HasValue)
                return 0;

            CloudBlobDirectory tempEntityFolder = cbc.GetDirectoryReference($"temp/{srcID}/");
            BlobContinuationToken bct = new BlobContinuationToken();

            // Go through all blobs in temp
            // Copy it into the new targetdirectory
            BlobResultSegment blobList = await tempEntityFolder.ListBlobsSegmentedAsync(true, BlobListingDetails.None, null, bct, null, null);

            foreach (var item in blobList.Results)
            {
                if (item.GetType() == typeof(CloudBlockBlob))
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    bool successfulParse = blob.TryParseBlobName(out StorageBin tempStorageBin, out Bucket srcBucket, out IDMID tempID, out string srcFileName);

                    if (successfulParse)
                    {
                        string destBlobName = $"{StorageBin.Permanent.BlobNamePrefix(srcBucket, permID)}/{srcFileName}";

                        CloudBlockBlob destBlob = cbc.GetBlockBlobReference(destBlobName);
                        await TransferManager.CopyAsync((CloudBlockBlob)item, destBlob, true,
                            new CopyOptions() { },
                            new SingleTransferContext() { ShouldOverwriteCallback = (s, d) => { return true; } });
                    }
                }
            }

            List<DMItem> movedData = await GetDocumentList(StorageBin.Permanent, Bucket.Data, permID);
            movedFiles += movedData.Count;
            List<DMItem> movedReports = await GetDocumentList(StorageBin.Permanent, Bucket.Reports, permID);
            movedFiles += movedReports.Count;
            List<DMItem> movedDocuments = await GetDocumentList(StorageBin.Permanent, Bucket.Documents, permID);
            movedFiles += movedDocuments.Count;

            return movedFiles;
        }

        public async Task<int> DeleteAll(StorageBin storageBin, IDMID id)
        {
            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync(id);

            CloudBlobDirectory dataDirectory = cbc.GetDirectoryReference(storageBin.BlobNamePrefix(Bucket.Data, id));
            CloudBlobDirectory documentDirectory = cbc.GetDirectoryReference(storageBin.BlobNamePrefix(Bucket.Documents, id));
            CloudBlobDirectory reportDirectory = cbc.GetDirectoryReference(storageBin.BlobNamePrefix(Bucket.Reports, id));

            int affectedCount = 0;
            affectedCount += await DeleteFolder(dataDirectory, storageBin);
            affectedCount += await DeleteFolder(documentDirectory, storageBin);
            affectedCount += await DeleteFolder(reportDirectory, storageBin);

            return affectedCount;
        }

        public async Task<bool> DeleteBusinessBlobContainer(short bid)
        {
            try
            {
                CloudBlobClient client = _storageAccount.CreateCloudBlobClient();
                CloudBlobContainer businessContainer = client.GetContainerReference($"bid{bid}");

                bool containerExists = await businessContainer.ExistsAsync();
                if (containerExists)
                    await businessContainer.DeleteAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task EmptyTemp()
        {
            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync();
            CloudBlobDirectory cbd = cbc.GetDirectoryReference("temp");
            BlobContinuationToken bct = new BlobContinuationToken();

            BlobResultSegment brs = await cbd.ListBlobsSegmentedAsync(true, BlobListingDetails.Metadata, null, bct, null, null);
            DateTime minDateTime = DateTime.UtcNow - TimeSpan.FromDays(30);
            IEnumerable<CloudBlockBlob> expiredTempBlobs = brs.Results.Where(t => GetExpiredTempBlobs(t, minDateTime)).Select<IListBlobItem, CloudBlockBlob>(t => (CloudBlockBlob)t);
            List<Task> deleteTasks = new List<Task>();
            foreach (var item in expiredTempBlobs)
            {
                deleteTasks.Add(item.DeleteAsync());
            }

            await Task.WhenAll(deleteTasks);
        }

        public async Task EmptyTrash()
        {
            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync();
            CloudBlobDirectory cbd = cbc.GetDirectoryReference("trash");
            BlobContinuationToken bct = new BlobContinuationToken();

            BlobResultSegment brs = await cbd.ListBlobsSegmentedAsync(true, BlobListingDetails.None, null, bct, null, null);
            DateTime minDateTime = DateTime.UtcNow - TimeSpan.FromDays(30);
            IEnumerable<CloudBlockBlob> expiredTrashBlobs = brs.Results.Where(t => GetExpiredTrashBlobs(t, minDateTime)).Select<IListBlobItem, CloudBlockBlob>(t => (CloudBlockBlob)t);
            List<Task> deleteTasks = new List<Task>();
            foreach (var item in expiredTrashBlobs)
            {
                deleteTasks.Add(item.DeleteAsync());
            }

            await Task.WhenAll(deleteTasks);
        }

        private bool GetExpiredTrashBlobs(IListBlobItem t, DateTime minDate)
        {
            if (t is CloudBlockBlob cbb)
            {
                string dtFilter = cbb.Name.Substring(cbb.Name.IndexOf('/') + 1, DMExtensionMethods.TrashDirectoryDateFormat.Length);

                if (DateTime.TryParseExact(dtFilter, DMExtensionMethods.TrashDirectoryDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime result))
                {
                    return result <= minDate;
                }

                return false;
            }

            return false;
        }

        private bool GetExpiredTempBlobs(IListBlobItem t, DateTime minDateTime)
        {
            if (t is CloudBlockBlob cbb)
            {
                if (cbb.Metadata.TryGetValue(Constants.CreatedDT, out string createdDTString))
                {
                    if (DateTime.TryParse(createdDTString, out DateTime result))
                        return result <= minDateTime;
                }

                return true;
            }

            return false;
        }

        public async Task<long> ComputeUsage()
        {
            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync();
            BlobContinuationToken bct = new BlobContinuationToken();
            var blobResultSegment = await cbc.ListBlobsSegmentedAsync("", true, BlobListingDetails.None, null, bct, null, null);

            long totalUsage = 0;

            foreach (var item in blobResultSegment.Results)
            {
                if (item is CloudBlockBlob)
                {
                    totalUsage += ((CloudBlockBlob)item).Properties.Length;
                }
            }

            return totalUsage;
        }

        #endregion

        #region private action helpers

        /// <summary>
        /// Generic copy, with parameters for move and rename
        /// </summary>
        /// <param name="fromBin"></param>
        /// <param name="fromBucket"></param>
        /// <param name="fromID"></param>
        /// <param name="files"></param>
        /// <param name="toBin"></param>
        /// <param name="toBucket"></param>
        /// <param name="toID"></param>
        /// <param name="toFolder"></param>
        /// <param name="renameName">Renames *ALL* files passed to this name</param>
        /// <param name="deleteSrcAfterCopy">if TRUE deletes the from blob after the copy</param>
        /// <returns></returns>
        private async Task<int> CopyInternal(
            StorageBin fromBin,
            Bucket fromBucket,
            IDMID fromID,
            string[] files,
            StorageBin? toBin = null,
            Bucket? toBucket = null,
            IDMID toID = null,
            string toFolder = null,
            string renameName = null,
            string withFolder = null,
            bool deleteSrcAfterCopy = false)
        {
            if (fromID == null)
                throw new ArgumentNullException("Cannot copy from null entity");

            if (!toBin.HasValue)
                toBin = fromBin;
            if (!toBucket.HasValue)
                toBucket = fromBucket;
            if (toID == null)
                toID = fromID;

            CloudBlobContainer cbc = await GetOrCreateBusinessContainerAsync(toID);
            string fromPrefix = fromBin.BlobNamePrefix(fromBucket, fromID);
            string toPrefix = toBin.Value.BlobNamePrefix(toBucket.Value, toID);
            CloudBlobDirectory fromFolder = cbc.GetDirectoryReference(fromPrefix);
            BlobContinuationToken bct = new BlobContinuationToken();

            uint blobsCopied = 0;
            uint blobsSkipped = 0;

            if (files == null)
            {
                return 0;
            }

            foreach (string oldRelativeFullPath in files)
            {
                var item = fromFolder.GetBlockBlobReference(oldRelativeFullPath.TrimStart('/'));
                CloudBlockBlob fromBlob = item as CloudBlockBlob;
                if (fromBlob != null)
                {
                    string newRelativeFullPath = oldRelativeFullPath;
                    if (toFolder != null)
                        newRelativeFullPath = GetMovedName(oldRelativeFullPath, toFolder, withFolder);

                    if (renameName != null)
                    {
                        newRelativeFullPath = GetRenamedName(renameName, newRelativeFullPath);
                    }

                    if (!newRelativeFullPath.StartsWith("/"))
                        newRelativeFullPath = '/' + newRelativeFullPath;

                    string destBlobName = $"{toPrefix}{newRelativeFullPath}";

#warning copy/move/rename will not create intermediate folder _ blobs
                    CloudBlockBlob toBlob = cbc.GetBlockBlobReference(destBlobName);
                    try
                    {
                        await TransferManager.CopyAsync(fromBlob, toBlob, true,
                            new CopyOptions() { },
                            new SingleTransferContext()
                            {
                                ShouldOverwriteCallback = (s, d) =>
                                {
                                    blobsSkipped++;
                                    return false;
                                },
                            });

                        if (deleteSrcAfterCopy)
                        {
                            await fromBlob.DeleteAsync();
                        }
                    }
                    //#warning skipping throws a whole exception that we have to catch
                    catch (TransferSkippedException)
                    {
                        ; //noop
                    }
                    catch (Exception)
                    {
                        ;
                    }

                    blobsCopied++;
                }
            }

            return Convert.ToInt32(blobsCopied - blobsSkipped);
        }

        /// <summary>
        /// renames the last significant segment of a relative full path to renameName
        /// </summary>
        /// <param name="renameName"></param>
        /// <param name="relativeFullPath"></param>
        /// <returns></returns>
        internal static string GetRenamedName(string renameName, string relativeFullPath)
        {
            string[] segments = NormalizeToSegments(relativeFullPath).Split('/');
            bool isFolder = relativeFullPath.EndsWith("/"+SpecialFolderBlobName);

            renameName = renameName.TrimEnd('/', '_').TrimStart('/');

            string output = "";
            if (segments.Length > 1)
            {
                //we want to trim off the last segment
                int newSegmentCount = segments.Length - 1;
                string[] newSegments = new string[newSegmentCount];
                Array.Copy(segments, newSegments, newSegmentCount);
                output = String.Join("/", newSegments) + '/' + renameName;
            }
            else
            {
                output = renameName;
            }

            //if we started as a folder, add back the special folder identifier
            if (isFolder)
                output += "/" + SpecialFolderBlobName;

            if (!output.StartsWith("/"))
                output = '/' + output;

            return output;
        }

        private async Task<int> _initAll(CloudBlobContainer cbc, IDMID templateSourceID, StorageBin destinationBin, IDMID destinationID)
        {
            int numBlobCopied = 0;
            foreach (Bucket b in Enum.GetValues(typeof(Bucket)))
            {
                numBlobCopied += await _init(cbc, StorageBin.Permanent.BlobNamePrefix(b, templateSourceID), destinationBin.BlobNamePrefix(b, destinationID));
            }
            return numBlobCopied;
        }

        /// <summary>
        /// Copies blobs from source to destination, returns number of _files_ that were copied and not skipped
        /// </summary>
        /// <param name="cbc"></param>
        /// <param name="sourcePrefix"></param>
        /// <param name="destinationPrefix"></param>
        /// <returns></returns>
        private async Task<int> _init(CloudBlobContainer cbc, string sourcePrefix, string destinationPrefix)
        {
            CloudBlockBlob templateManifestRef = cbc.GetBlockBlobReference($"{destinationPrefix}/_TemplateFiles.json");
            bool templateWasInitilized = await templateManifestRef.ExistsAsync();
            if (templateWasInitilized)
            {
                return 0; // no-op
            }

            CloudBlobDirectory templateFolder = cbc.GetDirectoryReference(sourcePrefix);
            BlobContinuationToken bct = new BlobContinuationToken();

            BlobResultSegment blobList = await templateFolder.ListBlobsSegmentedAsync(true, BlobListingDetails.None, null, bct, null, null);

            uint filesCopied = 0;
            uint filesSkipped = 0;
            List<string> filesCopiedPath = new List<string>{ };
            TemplateFilesJson templateFilesJson = new TemplateFilesJson();

            foreach (var item in blobList.Results)
            {
                CloudBlockBlob templateBlob = item as CloudBlockBlob;
                if (templateBlob != null)
                {
                    string destBlobName = $"{destinationPrefix}{templateBlob.Name.Replace(sourcePrefix, "")}";
                    bool isFile = !destBlobName.EndsWith(SpecialFolderBlobName.ToString());

                    CloudBlockBlob destBlob = cbc.GetBlockBlobReference(destBlobName);
                    try
                    {
                        await TransferManager.CopyAsync(templateBlob, destBlob, true,
                            new CopyOptions() { },
                            new SingleTransferContext()
                            {
                                ShouldOverwriteCallback = (s, d) =>
                                {
                                    if (isFile)
                                        filesSkipped++;
                                    return false;
                                },
                            });
                    }
                    //#warning skipping throws a whole exception that we have to catch
                    catch (TransferSkippedException)
                    {
                        ; //noop
                    }

                    if (isFile)
                    {
                        filesCopied++;
                        filesCopiedPath.Add(templateBlob.Name);
                    }
                }
            }

            // create _TemplateFiles.json
            templateFilesJson.FilesCopied = filesCopiedPath;
            createTemplateFilesJson(templateManifestRef, templateFilesJson);

            return Convert.ToInt32(filesCopied - filesSkipped);
        }
        #endregion

        #region public util
        public async Task<string> GetSASTokenAsync()
        {
            return (await GetOrCreateBusinessContainerAsync())
                .GetSharedAccessSignature(GetDefaultReadPolicy());
        }

        public async Task<string> GetBlobSASTokenAsync(string filename)
        {
            return (await GetOrCreateBusinessContainerAsync()).GetBlobReference(filename)
                .GetSharedAccessSignature(GetDefaultReadPolicy());
        }

        public async Task<string> GetBlobSASToken(string uri)
        {
            return (await _client.GetBlobReferenceFromServerAsync(new Uri(uri)))
                .GetSharedAccessSignature(GetDefaultReadPolicy());
        }

        public async Task<Uri> GetCanonicalURLWithReadSASTokenAsync(DMItem item)
        {
            return await GetCanonicalURLWithReadSASTokenAsync(item, GetDefaultReadPolicy());
        }

        public async Task<Uri> GetCanonicalURLWithReadSASTokenAsync(DMItem item, SharedAccessBlobPolicy sharedAccessBlobPolicy)
        {
            if (item == null)
                throw new ArgumentNullException("Item cannot be null");
            if (item.URL.ToLowerInvariant().StartsWith("http"))
                throw new ArgumentException("DMItem seems to be a fully qualified URL (starts with http), this function only works on DMItems with relative URLs");

            var blob = (await GetOrCreateBusinessContainerAsync()).GetBlobReference(item.URL);
            string sas = blob.GetSharedAccessSignature(sharedAccessBlobPolicy);
            return new Uri(blob.Uri + sas);
        }

        public async Task<Stream> GetStream(StorageBin bin, Bucket bucket, IDMID id, string filename)
        {
            Stream stream = new MemoryStream();
            CloudBlockBlob blob = await GetBlockBlob(bin, bucket, id, filename);
            await blob.DownloadToStreamAsync(stream);
            stream.Position = 0;
            return stream;
        }

        public async Task<CloudBlockBlob> GetBlockBlob(StorageBin bin, Bucket bucket, IDMID id, string filename)
        {
            CloudBlockBlob blob = (await GetOrCreateBusinessContainerAsync(id)).GetBlockBlobReference($"{bin.BlobNamePrefix(bucket, id)}/{filename}");
            return blob;
        }

        #endregion

        #region private util

        async private void createTemplateFilesJson(CloudBlockBlob destination, TemplateFilesJson templateFiles)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                StreamWriter sw = new StreamWriter(stream);
                string jsonFile = JsonConvert.SerializeObject(templateFiles);
                await sw.WriteAsync(jsonFile);
                await sw.FlushAsync();

                stream.Position = 0;

                await TransferManager.UploadAsync(stream, destination, new UploadOptions(), CreateSingleTransferOverwriteContext());
            }
        }

        private static SharedAccessBlobPolicy GetDefaultReadPolicy()
        {
            return new SharedAccessBlobPolicy()
            {
                Permissions = SharedAccessBlobPermissions.Read,
                SharedAccessExpiryTime = DateTime.UtcNow.AddHours(DefaultSASReadExpirationHours)
            };
        }

        /// <summary>
        /// given /foo/bar/blue.txt
        /// returns ['foo', 'foo/bar']
        /// </summary>
        /// <param name="newBlobName"></param>
        /// <returns></returns>
        private string[] GetFolders(string newBlobName)
        {
            string[] allSegments = newBlobName.Split('/');
            if (allSegments.Length < 2)
                return null;
            string[] folders = allSegments.Take(allSegments.Length - 1).ToArray();
            //foo
            //bar
            string[] result = new string[folders.Length];
            for (int i = 0; i < folders.Length; i++)
            {
                result[i] = folders[i];
                if (i > 0)
                    result[i] = result[i - 1] + "/" + result[i];
            }

            return result;
        }

        /// <summary>
        /// Gets the relative path given an oldblobname and newfolder name, can handle files and folders, intended for Move/Rename/Copy
        /// </summary>
        /// <example>
        /// <code>
        /// GetMovedName("some/folder/file.txt", "other/_")
        /// //returns "other/file.txt"
        /// GetMovedName("some/folder/file.txt", "other/_", "some/_")
        /// //returns "other/folder/file.txt"
        /// </code>
        /// </example>
        /// <param name="oldBlobName">requires /_ for folder input</param>
        /// <param name="newFolder">can have / at beginning or end, can have /_ at end</param>
        /// <param name="withFolderFullPath">if supplied, folders in between withFolderFullPath and oldBlobName are kept, if not supplied, folders are not kept</param>
        /// <returns>a path guaranteed to start with /</returns>
        internal static string GetMovedName(string oldBlobName, string newFolder, string withFolderFullPath = null)
        {
            //sometimes we want to just move the file
            //sometimes we want to move a file with a folder

            bool sourceBlobIsFolder = oldBlobName.EndsWith("/" + SpecialFolderBlobName);
            string normalNewFolderPath = NormalizeToSegments(newFolder);
            
            string output = "";

            string[] normalOldSegments = NormalizeToSegments(oldBlobName).Split('/');
            if (normalOldSegments.Length == 0) //no name, just use new path
            {
                output = $"/{normalNewFolderPath}";
            }
            else if (normalNewFolderPath == "" && withFolderFullPath == null) //move to root, just move name
            {
                output = $"/{normalOldSegments[normalOldSegments.Length - 1]}";
            }
            else if (normalOldSegments.Length == 1) //no subfolders, just use newFolderPath/name
            {
                output = $"/{normalNewFolderPath}/{normalOldSegments[0]}";
            }
            else if (withFolderFullPath == null) //ignore in between folders
            {
                output = $"/{normalNewFolderPath}/{normalOldSegments[normalOldSegments.Length - 1]}";
            }
            else // use in between folders
            {
                string normalizedWithFolderPath = NormalizeToSegments(withFolderFullPath);
                //withFolderFullPath is now stripped of its leading / and trailing /_

                string[] newFolderSegments = normalNewFolderPath.Split('/');
                string[] withFolderSegments = normalizedWithFolderPath.Split('/');
                if (withFolderSegments == null || normalizedWithFolderPath.Length < 1)
                    throw new InvalidOperationException("can't move with folder that is unnamed");
                string withFolderName = withFolderSegments[withFolderSegments.Length - 1];


                //subFolderDepthStartIndex = index of folders from root for first subfolder of withFolder
                //example: blob /a/b/c/file.txt with folder /a/b/_
                //         has a subFolderDepthStartIndex of 2, our blob segments[2] is c
                //         which is the first subfolder of /a/b/_
                int subFolderDepthStartIndex = withFolderSegments.Length;
                
                if (String.IsNullOrWhiteSpace(normalNewFolderPath))
                {
                    output = $"/{withFolderName}";
                }
                else if (newFolderSegments.Length == withFolderSegments.Length)
                {
                    output = $"/{normalNewFolderPath}";
                    if(!output.EndsWith("/"+withFolderName)){
                        output = $"{output}/{withFolderName}";
                    }
                }
                else
                {
                    output = $"/{normalNewFolderPath}/{withFolderName}";
                }

                for (int i = subFolderDepthStartIndex; i < normalOldSegments.Length; i++)
                {
                    output += $"/{normalOldSegments[i]}";
                }
            }

            if (sourceBlobIsFolder)
                output += "/" + SpecialFolderBlobName;

            return output;
        }

        /// <summary>
        /// removes trailing /_ if it exists
        /// and trims / from start and end
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string NormalizeToSegments(string path)
        {
            if (path.EndsWith("/" + SpecialFolderBlobName))
                path = path.Substring(0, path.Length - 2).TrimStart('/');
            else
                path = path.Trim('/');
            return path;
        }

        /// <summary>
        /// Returns all files and folders from a given relative path
        /// </summary>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="relativePath"></param>
        /// <returns></returns>
        private async Task<List<DMItem>> GetFlatFilesAndFolders(StorageBin storageBin, Bucket bucket, IDMID id, string relativePath)
        {
            CloudBlobContainer businessContainer = await GetOrCreateBusinessContainerAsync(id);

            string prefix = storageBin.BlobNamePrefix(bucket, id);
            string relativeBlobDirectoryPath = prefix;
            if (relativePath != null && relativePath.Length > 1)
            {
                if (relativePath.EndsWith("/" + SpecialFolderBlobName))
                {
                    relativePath = relativePath.Substring(0, relativePath.Length - 1);
                }

                relativeBlobDirectoryPath += '/'+relativePath.TrimStart('/');
            }

            CloudBlobDirectory entityBucketDirectory = businessContainer.GetDirectoryReference(relativeBlobDirectoryPath);

            BlobContinuationToken blobContinuationToken = new BlobContinuationToken();

            BlobResultSegment entityBucketBlobListSegment = await entityBucketDirectory.ListBlobsSegmentedAsync(true, BlobListingDetails.Metadata, null, blobContinuationToken, new BlobRequestOptions() { }, new OperationContext() { });
            
            return entityBucketBlobListSegment.Results.Select(t => ((CloudBlockBlob)t).ToFlatListedDMItem(prefix)).ToList();
        }

        /// <summary>
        /// Recursively gather Document items/folders
        /// </summary>
        /// <param name="businessContainer"></param>
        /// <param name="relativeBlobDirectoryPath"></param>
        /// <param name="useFlatBlobListing">if TRUE, returns only blobs in a flat list, no nesting, otherwise this returns folders w/contents nested</param>
        /// <param name="relativeItemPath">DO NOT USE this is for recursive calls</param>
        /// <param name="includeSpecialFolderBlobName">Set to true if you also need the special '_' file in the .thumbnails folder copied</param>
        /// <returns></returns>
        private async Task<List<DMItem>> GetChildDocumentItems(CloudBlobContainer businessContainer, string relativeBlobDirectoryPath, string relativeItemPath, bool useFlatBlobListing = false, bool includeSpecialFolderBlobName = false)
        {
            CloudBlobDirectory entityBucketDirectory = businessContainer.GetDirectoryReference(relativeBlobDirectoryPath);

            BlobContinuationToken blobContinuationToken = new BlobContinuationToken();

            BlobResultSegment entityBucketBlobListSegment = await entityBucketDirectory.ListBlobsSegmentedAsync(useFlatBlobListing, BlobListingDetails.Metadata, null, blobContinuationToken, new BlobRequestOptions() { }, new OperationContext() { });

            if (useFlatBlobListing)
            {
                IEnumerable<IListBlobItem> listBlobItems = entityBucketBlobListSegment.Results
                    .Where(t => !t.Uri.AbsolutePath.EndsWith("/_TemplateFiles.json"));

                if (!includeSpecialFolderBlobName)
                {
                    listBlobItems = listBlobItems.Where(t => !t.Uri.AbsolutePath.EndsWith("/_"));
                }

                List<DMItem> childDocumentItems = listBlobItems.Select(t => ((CloudBlockBlob)t).ToFlatListedDMItem(entityBucketDirectory.Prefix)).ToList();

                return childDocumentItems;
            }

            List<DMItem> documentItems = entityBucketBlobListSegment.Results.Select(t => t.ToDMItem(relativeItemPath ?? "")).ToList();
            foreach (DMItem item in documentItems.Where(t => t.IsFolder))
            {
                CloudBlockBlob folderMetaBlob = businessContainer.GetBlockBlobReference($"{entityBucketDirectory.Prefix}{item.Name}/{SpecialFolderBlobName}");
                try
                {
                    await folderMetaBlob.FetchAttributesAsync();
                    if (folderMetaBlob.Metadata.TryGetValue(Constants.CanDelete, out string canDeleteValue) && bool.TryParse(canDeleteValue, out bool canDelete))
                        item.CanDelete = string.IsNullOrWhiteSpace(canDeleteValue) ? DefaultDocumentPermissionOptions.CanDelete : canDelete;

                    if (folderMetaBlob.Metadata.TryGetValue(Constants.CanRename, out string canRenameValue) && bool.TryParse(canRenameValue, out bool canRename))
                        item.CanRename = string.IsNullOrWhiteSpace(canRenameValue) ? DefaultDocumentPermissionOptions.CanRename : canRename;

                    if (folderMetaBlob.Metadata.TryGetValue(Constants.CanMove, out string canMoveValue) && bool.TryParse(canMoveValue, out bool canMove))
                        item.CanMove = string.IsNullOrWhiteSpace(canMoveValue) ? DefaultDocumentPermissionOptions.CanMove : canMove;

                    if (folderMetaBlob.Metadata.TryGetValue(Constants.IsShared, out string isSharedValue) && bool.TryParse(isSharedValue, out bool isShared))
                        item.IsShared = string.IsNullOrWhiteSpace(isSharedValue) ? DefaultDocumentPermissionOptions.IsShared : isShared;
                }
                catch
                {
                    // Item has no metadata...
                }

                item.Contents = await GetChildDocumentItems(businessContainer, $"{entityBucketDirectory.Prefix}{item.Name}/", $"{relativeItemPath ?? ""}{item.Name}/");
            }

            IEnumerable<DMItem> dmItems = documentItems.Where(t => t.IsFolder || (!t.IsFolder && !(t.Name == SpecialFolderBlobName.ToString() || t.Name == "_TemplateFiles.json")));

            if (!includeSpecialFolderBlobName)
                dmItems = dmItems.Where(t => t.Name != SpecialFolderBlobName.ToString());

            return dmItems.ToList();
        }

        /// <summary>
        /// Gets either association or business container depending on id.classFolder 'association' : 'static'
        /// </summary>
        /// <param name="id"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        private async Task<CloudBlobContainer> GetOrCreateBusinessContainerAsync(IDMID id = null, CloudBlobClient client = null)
        {
            if (id?.classFolder?.ToLower() == "association")
                return await GetOrCreateAssociationContainerAsync(client);

            client = client ?? _storageAccount.CreateCloudBlobClient();

            CloudBlobContainer businessContainer = client.GetContainerReference($"bid{_bid}");

            bool containerExists = await businessContainer.ExistsAsync();
            if (!containerExists)
                await businessContainer.CreateAsync();

            return businessContainer;
        }

        private async Task<CloudBlobContainer> GetOrCreateAssociationContainerAsync(CloudBlobClient client = null)
        {
            client = client ?? _storageAccount.CreateCloudBlobClient();

            CloudBlobContainer associationContainer = client.GetContainerReference($"aid{_aid}");

            bool containerExists = await associationContainer.ExistsAsync();
            if (!containerExists)
                await associationContainer.CreateAsync();

            return associationContainer;
        }

        private string GetExtensionFromContentType(string contentType)
        {
            if (String.IsNullOrWhiteSpace(contentType))
            {
                throw new ArgumentNullException("contentType");
            }

            return contentType.Substring(contentType.IndexOf("/") + 1);
        }
        #endregion

        /*
        private string GetHierarchy(IListBlobItem blob, HashSet<CloudBlobDirectory> existingDirectories)
        {
            string pathInput = (blob is CloudBlobDirectory) ? blob.Uri.AbsolutePath.TrimEnd('/') : blob.Uri.AbsolutePath;
            string name = pathInput.Substring(pathInput.LastIndexOf('/') + 1);
            if (blob.Parent != null && existingDirectories.Contains(blob.Parent))
            {
                name = $"{GetHierarchy(blob.Parent, existingDirectories)}/{name}";
            }
            return name;
        }
        */

        public static async Task ConfigureLocalDevelopmentCORS()
        {
            var blobClient = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudBlobClient();
            var tableClient = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudTableClient();
            var queueClient = CloudStorageAccount.DevelopmentStorageAccount.CreateCloudQueueClient();

            await Task.WhenAll(new Task[] {
                ConfigureServiceCORS(blobClient.GetServicePropertiesAsync, blobClient.SetServicePropertiesAsync),
                ConfigureServiceCORS(tableClient.GetServicePropertiesAsync, tableClient.SetServicePropertiesAsync),
                ConfigureServiceCORS(queueClient.GetServicePropertiesAsync, queueClient.SetServicePropertiesAsync)
            });
        }

        private static async Task ConfigureServiceCORS(Func<Task<ServiceProperties>> asyncGetter, Func<ServiceProperties, Task> asyncSetter)
        {
            ServiceProperties properties = await asyncGetter();
            ConfigureCors(properties);
            await asyncSetter(properties);
        }

        private static void ConfigureCors(ServiceProperties serviceProperties)
        {
            serviceProperties.Cors = new CorsProperties();
            serviceProperties.Cors.CorsRules.Add(new CorsRule()
            {
                AllowedHeaders = new List<string>() { "*" },
                AllowedMethods = CorsHttpMethods.Put | CorsHttpMethods.Get | CorsHttpMethods.Head | CorsHttpMethods.Post,
                AllowedOrigins = new List<string>() { "*" },
                ExposedHeaders = new List<string>() { "*" },
                MaxAgeInSeconds = 1800 // 30 minutes
            });
        }

    }
}
