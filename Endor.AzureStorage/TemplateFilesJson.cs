﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.AzureStorage
{
    public class TemplateFilesJson
    {
        public DateTime DateInitialized { get; set; }
        public List<string> FilesCopied { get; set; }

        public TemplateFilesJson()
        {
            this.DateInitialized = new DateTime();
            this.FilesCopied = new List<string>{ };
        }
    }
}
