﻿using Endor.DocumentStorage.Models;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Endor.AzureStorage
{
    public static class DMExtensionMethods
    {
        const string storageBinGroup = "storageBin";
        const string bucketGroup = "bucket";
        const string ctidGroup = "ctid";
        const string templatectidGroup = "templatectid";
        const string staticctidGroup = "staticctid";
        const string associationctidGroup = "associationctid";
        const string tempidGroup = "tempid";
        const string trashDTGroup = "trashDT";
        const string idGroup = "id";
        const string filenameGroup = "filename";
        const string templatefilenameGroup = "templatefilename";
        const string staticfilenameGroup = "staticfilename";
        const string associationfilenameGroup = "associationfilename";
        public const string TempRegexPattern = @"(?'" + storageBinGroup + @"')\/(?'" + tempidGroup + @"'[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})\/(?'" + bucketGroup + @"'Documents|Data|Reports)\/(?'" + filenameGroup + @"'.+)";
        public const string TrashRegexPattern = @"(?'" + storageBinGroup + @"'Trash)\/(?'" + trashDTGroup + @"'\d{4}-\d{2}-\d{2}-\d{2}:\d{2}:\d{2})\/(?'" + ctidGroup + @"'\d+)\/(?'" + idGroup + @"'\d+)\/(?'" + bucketGroup + @"'Documents|Data|Reports)\/(?'" + filenameGroup + @"'.+)";
        public const string PermanentRegexPattern = @"(?'" + bucketGroup + @"'Documents|Data|Reports)((\/(?'" + ctidGroup + @"'\d+)\/((?'" + idGroup + @"'\d+)\/(?'" + filenameGroup + @"'.+)))|(\/(?'" + staticctidGroup + @"'\d+)\/static\/(?'" + staticfilenameGroup + @"'.+)|(?'" + associationctidGroup + @"'\d+)\/association\/(?'" + associationfilenameGroup + @"'.+)|(\/(?'" + templatectidGroup + @"'\d+)\/template\/(?'" + templatefilenameGroup + @"'.+))))";
        public const string TrashDirectoryDateFormat = "yyyy-MM-dd_HH-mm-ss";
        public static Regex TempRegex = new Regex(TempRegexPattern, RegexOptions.Compiled);
        public static Regex TrashRegex = new Regex(TrashRegexPattern, RegexOptions.Compiled);
        public static Regex PermanentRegex = new Regex(PermanentRegexPattern, RegexOptions.Compiled);

        public static DMItem ToDMItem(this IListBlobItem self, string relativeItemPath)
        {
            if (self is CloudBlobDirectory dir)
            {
                return dir.ToDMItem(relativeItemPath);
            }
            else if (self is CloudBlockBlob blob)
            {
                return blob.ToDMItem(relativeItemPath);
            }
            else
            {
                throw new ArgumentException($"DMItem cannot be constructed from type {self.GetType().FullName}");
            }
        }
        
        internal static string RelativePath(this IListBlobItem self, string prefix)
        {
            CloudBlobDirectory dir = self as CloudBlobDirectory;
            if (dir != null)
            {
                return dir.Prefix.Replace(prefix, "").Trim('/');
            }
            CloudBlockBlob blob = self as CloudBlockBlob;
            if (blob != null)
            {
                string prefixless = blob.Name.Replace(prefix, "");
                if (blob.Name.EndsWith("/" + EntityStorageClient.SpecialFolderBlobName))
                {
                    return prefixless.TrimEnd(EntityStorageClient.SpecialFolderBlobName).Trim('/');
                }
                else
                {
                    return prefixless.Trim('/');
                }
            }
            else
            {
                throw new ArgumentException($"Relative path cannot be constructed from type {self.GetType().FullName}");
            }
        }

        internal static DMItem ToDMItem(this CloudBlobDirectory self, string relativeItemPath)
        {
            string directoryName = self.Prefix.TrimEnd('/').Split('/').ToList().Last();

            DMItem result = new DMItem()
            {
                Name = directoryName,
                Path = relativeItemPath,
                IsFolder = true,
                Contents = new List<DMItem>(),
                URL = self.Uri.AbsoluteUri,
            };

            return result;
        }

        internal static DMItem ToDMItem(this CloudBlockBlob self, string relativeItemPath)
        {
            return ToDMItem(self, self.Name.Substring(self.Name.LastIndexOf('/', self.Name.Length - 2) + 1), relativeItemPath);
        }

        public static DMItem ToFlatListedDMItem(this CloudBlockBlob self, string prefix)
        {
            return ToDMItem(self, self.Name.Remove(0, prefix.Length), "");
        }

        private static DMItem ToDMItem(CloudBlockBlob self, string name, string path)
        {
            MediaType media = GetMediaTypeFromMimeType(self.Properties.ContentType);

            DMItem result = new DMItem()
            {
                Name = name,
                Path = path,
                URL = self.Uri.AbsoluteUri,
                Size = self.Properties.Length,
                MimeType = self.Properties.ContentType,
                ModifiedDT = self.Properties.LastModified.Value.UtcDateTime,
                IsFolder = false,
                MediaType = media,
                //use mediaType's fontClass first, falling back to assetType's fontClass
                IconClass = String.IsNullOrWhiteSpace(media.FontClass) ? media.AssetType.FontClass : media.FontClass,
                AssetType = media.AssetType
            };

            result.SetMetadata(self);

            return result;

        }

        private static void SetMetadata(this DMItem self, CloudBlockBlob blob)
        { 

            if (blob.Metadata.TryGetValue(Constants.IconClass, out string iconClass))
                self.IconClass = iconClass;

            if (blob.Metadata.TryGetValue(Constants.CreatedDT, out string createDTValue) && DateTime.TryParse(createDTValue, out DateTime CreatedDT))
                self.CreatedDT = CreatedDT;

            if (blob.Metadata.TryGetValue(Constants.CreatedByID, out string createdByIDValue) && int.TryParse(createdByIDValue, out int CreatedByID))
                self.CreatedByID = CreatedByID;

            if (blob.Metadata.TryGetValue(Constants.ModifiedByID, out string modifiedByIDValue) && int.TryParse(modifiedByIDValue, out int ModifiedByID))
                self.ModifiedByID = ModifiedByID;

            if (blob.Metadata.TryGetValue(Constants.ModifiedByClasstypeID, out string modifiedByClasstypeIDValue) && int.TryParse(modifiedByClasstypeIDValue, out int ModifiedByClasstypeID))
                self.ModifiedByClasstypeID = ModifiedByClasstypeID;

            if (blob.Metadata.TryGetValue(Constants.HasThumbnail, out string hasThumbnailValue) && bool.TryParse(hasThumbnailValue, out bool hasThumbnail))
                self.HasThumbnail = hasThumbnail;
            if (blob.Metadata.TryGetValue(Constants.CanDelete, out string canDeleteValue) && bool.TryParse(canDeleteValue, out bool canDelete))
                self.CanDelete = canDelete;

            if (blob.Metadata.TryGetValue(Constants.CanRename, out string canRenameValue) && bool.TryParse(canRenameValue, out bool canRename))
                self.CanRename = canRename;

            if (blob.Metadata.TryGetValue(Constants.CanMove, out string canMoveValue) && bool.TryParse(canMoveValue, out bool canMove))
                self.CanMove = canMove;

            if (blob.Metadata.TryGetValue(Constants.IsShared, out string isSharedValue) && bool.TryParse(isSharedValue, out bool isShared))
                self.IsShared = isShared;
        }

        private static MediaType GetMediaTypeFromMimeType(string contentType)
        {
            if (MediaTypes.Lookup.TryGetValue(contentType, out MediaType mediaType))
                return mediaType;

            return GetUnknownMediaType(contentType);
        }

        private static MediaType GetUnknownMediaType(string contentType)
        {
            string partialType = contentType.Split('/')[0];
            switch (partialType)
            {
                case "document": return MediaTypes.application_x_cyr_unidentified_document;
                case "image": return MediaTypes.image_x_cyr_unidentified_image;
                case "artwork": return MediaTypes.application_x_cyr_unidentified_artwork;
                case "video": return MediaTypes.video_x_cyr_unidentified_video;
                case "audio": return MediaTypes.audio_x_cyr_unidentified_audio;
                case "text": return MediaTypes.text_x_cyr_unidentified_web;
                case "application": return MediaTypes.application_x_cyr_unidentified_binary;
                case "other": return MediaTypes.application_x_cyr_unidentified_other;
                case "":
                default: return MediaTypes.application_x_cyr_unidentified_unknown;
            }
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, double doubleValue)
        {
            self.Metadata[key] = doubleValue.ToString();
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, int intValue)
        {
            self.Metadata[key] = intValue.ToString();
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, DateTime dateTimeValue)
        {
            self.Metadata[key] = dateTimeValue.ToString();
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, short shortValue)
        {
            self.Metadata[key] = shortValue.ToString();
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, float floatValue)
        {
            self.Metadata[key] = floatValue.ToString();
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, byte byteValue)
        {
            self.Metadata[key] = byteValue.ToString();
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, string stringValue)
        {
            self.Metadata[key] = stringValue;
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, bool booleanValue)
        {
            self.Metadata[key] = booleanValue.ToString();
        }

        public static void SetMetadataValue(this CloudBlockBlob self, Dictionary<string, string> values)
        {
            foreach (KeyValuePair<string, string> kvp in values)
            {
                self.Metadata[kvp.Key] = kvp.Value;
            }
        }

        public static void SetMetadataValue(this CloudBlockBlob self, string key, object value)
        {
            self.Metadata[key] = Newtonsoft.Json.JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// Returns the prefix that all blobs in this bin/bucket/id should have. Results guaranteed not to have leading or trailing slashes.
        /// </summary>
        /// <param name="self"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="deleted"></param>
        /// <returns></returns>
        public static string BlobNamePrefix(this StorageBin self, Bucket bucket, IDMID id, DateTime? deleted = null)
        {
            if (id == null)
                throw new ArgumentException("id", "Null storage object is not supported");
            
            switch (self)
            {
                case StorageBin.Temp:
                    return $"temp/{id.ToString()}/{bucket}";
                case StorageBin.Trash:
                    {
                        if (deleted.HasValue)
                            return $"trash/{deleted.Value.ToString(TrashDirectoryDateFormat)}/{bucket}/{id.ToString()}";

                        throw new ArgumentNullException($"{nameof(deleted)} cannot be null when using the trash storageBin", nameof(deleted));
                    }
                case StorageBin.Permanent:
                    {
                        switch (id.classFolder)
                        {
                            case "static":
                                {
                                    return $"{bucket}/{id.ctid}/static";
                                }
                            case "association":
                                {
                                    return $"{bucket}/{id.ctid}/static";
                                    //return $"{bucket}/{id.ctid}/association";
                                }
                            default:
                                {
                                    return $"{bucket}/{id.ToString()}";
                                }
                        }
                    }
                    
                default:
                    throw new InvalidOperationException("Storage Context has unrecognizable storage bin");
            }
        }

        public static bool TryParseBlobName(this ICloudBlob self, out StorageBin storageBin, out Bucket bucket, out IDMID id, out string filename)
        {
            Match result = TrashRegex.Match(self.Name);
            if (result.Success)
            {
                storageBin = StorageBin.Trash;
                if (Enum.TryParse<Bucket>(result.Groups[bucketGroup].Value, out bucket) &&
                    int.TryParse(result.Groups[ctidGroup].Value, out int ctid) &&
                    int.TryParse(result.Groups[idGroup].Value, out int entityId))
                {
                    id = new DMID()
                    {
                        ctid = ctid,
                        id = entityId,
                    };

                    filename = result.Groups[filenameGroup].Value;

                    return true;
                }
                id = null;
                filename = null;
                return false;
            }

            result = TempRegex.Match(self.Name);

            if (result.Success)
            {
                storageBin = StorageBin.Temp;
                if (Enum.TryParse<Bucket>(result.Groups[bucketGroup].Value, out bucket) &&
                    Guid.TryParse(result.Groups[tempidGroup].Value, out Guid tempID))
                {
                    id = new DMID()
                    {
                        guid = tempID,
                    };

                    filename = result.Groups[filenameGroup].Value;

                    return true;
                }
                id = null;
                filename = null;
                return false;
            }

            result = PermanentRegex.Match(self.Name);

            if (result.Success)
            {
                storageBin = StorageBin.Permanent;
                if (Enum.TryParse<Bucket>(result.Groups[bucketGroup].Value, out bucket))
                {
                    if (int.TryParse(result.Groups[ctidGroup].Value, out int ctid))
                    {
                        if (int.TryParse(result.Groups[idGroup].Value, out int entityID))
                        {
                            id = new DMID()
                            {
                                ctid = ctid,
                                id = entityID,
                            };

                            filename = result.Groups[filenameGroup].Value;
                            return true;
                        }

                        id = null;
                        filename = null;

                        return false;
                    }

                    if (int.TryParse(result.Groups[staticctidGroup].Value, out int staticctid))
                    {
                        id = new DMID()
                        {
                            classFolder = "static",
                            ctid = staticctid,
                        };

                        filename = result.Groups[staticfilenameGroup].Value;

                        return true;
                    }

                    if (int.TryParse(result.Groups[associationctidGroup].Value, out int associationctid))
                    {
                        id = new DMID()
                        {
                            classFolder = "association",
                            ctid = associationctid,
                        };

                        filename = result.Groups[associationfilenameGroup].Value;

                        return true;
                    }

                    if (int.TryParse(result.Groups[templatectidGroup].Value, out int templatectid))
                    {
                        id = new DMID()
                        {
                            classFolder = "template",
                            ctid = templatectid
                        };

                        filename = result.Groups[templatefilenameGroup].Value;

                        return true;
                    }

                    id = null;
                    filename = null;

                    return false;
                }
            }

            storageBin = default(StorageBin);
            bucket = default(Bucket);
            id = null;
            filename = null;

            return false;
        }
    }
}
