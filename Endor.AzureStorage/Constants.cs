﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.AzureStorage
{
    public static class Constants
    {
        public const string CreatedDT = "CreatedDT";
        public const string CreatedByID = "CreatedByID";
        public const string ModifiedByID = "ModifiedByID";
        public const string IconClass = "IconClass";
        public const string ModifiedByClasstypeID = "ModifiedByClasstypeID";
        public const string HasThumbnail = "HasThumbnail";
        public const string CanDelete = "CanDelete";
        public const string CanRename = "CanRename";
        public const string CanMove = "CanMove";
        public const string IsShared = "IsShared";
    }
}
