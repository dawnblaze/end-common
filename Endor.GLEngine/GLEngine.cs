﻿using Endor.EF;
using Endor.GLEngine.Models;
using System;
using System.Linq;
using Endor.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Endor.GLEngine.Classes;
using System.Threading;
using System.Data;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using Endor.GLEngine.Queries;

namespace Endor.GLEngine
{
    public partial class GLEngine
    {
        public const short Undeposited_Funds_ID = 1100;
        public const short Cash_Drawer_ID = 1101;
        public const short Accounts_Receivable_ID = 1200;
        public const short Orders_In_WIP_ID = 1300;
        public const short Orders_In_Built_ID = 1400;
        public const short Customer_Deposits_ID = 2100;
        public const short Customer_Credits_ID = 2200;
        public const short Production_Deliverables_ID = 2300;
        public const short Sales_Tax_Payable_ID = 2400;
        public const short Credit_Adjustments_ID = 6100;
        public const short Bad_Debt_ID = 6200;
        public const short Shipping_Income_ID = 6200;

        private struct GLIncomeEntry
        {
            public int? GLAccountID;
            public decimal? AmountToUse;
            public int? OrderComponentID;
            public int? AssemblyID;
            public int? MaterialID;
            public int? LaborID;
            public int? MachineID;
            public int? PlaceID;
            public int? AssetID;
        }

        public GLEngine(short bid, ApiContext context)
        {
            this.BID = bid;
            this.Context = context;
        }

        private short BID { get; set; }
        private ApiContext Context { get; set; }

        public async Task<ActivityGlactivity> InsertGLActivity(string Name, string Subject, short? CompletedById, int? CompletedByContactId, DateTime completedDate, GLEntryType entryType, int objectID, List<GLData> glData, Func<ApiContext, short, int, Task<int>> IDGenerator)
        {
            ActivityGlactivity glActivity = await BuildGLActivity(Name, Subject, CompletedById, CompletedByContactId, completedDate, entryType, glData, IDGenerator);
            await this.Context.SaveChangesAsync();
            glActivity.GL = glData;
            return glActivity;
        }

        private async Task<ActivityGlactivity> BuildGLActivity(string Name, string Subject, short? CompletedById, int? CompletedByContactId, DateTime completedDate, GLEntryType entryType, List<GLData> glData, Func<ApiContext, short, int, Task<int>> IDGenerator)
        {
            var orderIDs = glData.Select(x => x.OrderID).Distinct();
            var orderID = orderIDs.Count() == 1 ? orderIDs.First() : null;
            var glActivity = new ActivityGlactivity()
            {
                BID = BID,
                ID = await IDGenerator(this.Context, BID, (int)ClassType.GLActivity),
                IsActive = true,
                Name = Name,
                ActivityType = (byte)ActivityType.Accounting_Entry,
                OrderID = orderID,
                CompanyID = glData.Any() ? glData.First().CompanyID : null,
                Subject = Subject,
                CompletedByID = CompletedById,
                CompletedByContactID = CompletedByContactId,
                CompletedDT = completedDate,
                GLEntryType = (byte)entryType,

            };
            Context.ActivityGlactivity.Add(glActivity);
            foreach (var glDataEntry in glData)
            {
                glDataEntry.BID = BID;
                glDataEntry.ID = await IDGenerator(this.Context, BID, (int)ClassType.GL);
                glDataEntry.ActivityID = glActivity.ID;
                Context.GLData.Add(glDataEntry);
            }

            return glActivity;
        }

        /// <summary>
        /// Create a GL Activity with GL Data to use and save later
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Subject"></param>
        /// <param name="CompletedById"></param>
        /// <param name="CompletedByContactId"></param>
        /// <param name="completedDate"></param>
        /// <param name="entryType"></param>
        /// <param name="objectID"></param>
        /// <param name="glData"></param>
        /// <returns></returns>
        public async Task<ActivityGlactivity> CreateGLActivity(string Name, string Subject, short? CompletedById, int? CompletedByContactId, DateTime completedDate, GLEntryType entryType, List<GLData> glData, Func<ApiContext, short, int, Task<int>> IDGenerator)
        {
            ActivityGlactivity glActivity = await BuildGLActivity(Name, Subject, CompletedById, CompletedByContactId, completedDate, entryType, glData, IDGenerator);
            glActivity.GL = glData;
            return glActivity;
        }

        private List<GLData> CalculateGLForOrder(TransactionHeaderData order)
        {
            //Check that order is not an Estimate
            if (order.TransactionType == 1)
            {
                throw new Exception("GLData will not be calculated for Estimate");
            }
            var incomeEntries = GetGLIncomeForOrder(order);
            //sum income entries and create offseting entry
            //Add up prices from Taxes
            List<OrderItemTaxAssessment> orderItemTaxAssessments = Context.OrderItemTaxAssessment.Where(p => p.OrderID == order.ID && p.BID == BID).Include(x => x.TaxItem).ToList();
            var taxEntries = new List<GLData>();
            foreach (var orderItemTaxAssessmsent in orderItemTaxAssessments)
            {
                taxEntries.Add(new GLData
                {
                    BID = order.BID,
                    OrderID = order.ID,
                    CompanyID = order.CompanyID,
                    LocationID = order.LocationID,
                    IsTaxable = !order.IsTaxExempt,
                    Amount = -orderItemTaxAssessmsent.TaxAmount,
                    GLAccountID = orderItemTaxAssessmsent.TaxItem.GLAccountID,
                    OrderItemID = orderItemTaxAssessmsent.OrderItemID,
                    TaxItemID = orderItemTaxAssessmsent.TaxItemID,
                    ItemComponentID = orderItemTaxAssessmsent.ItemComponentID,
                    ItemSurchargeID = orderItemTaxAssessmsent.ItemSurchargeID,
                    TaxCodeID = orderItemTaxAssessmsent.TaxCodeID,
                    TaxGroupID = orderItemTaxAssessmsent.TaxGroupID
                });
            }

            var glData = GetGLDataEntriesForOrder(order, incomeEntries, taxEntries);
            //Get Payment GL Info
            List<PaymentApplication> paymentApplications = Context.PaymentApplication.Where(p => p.OrderID == order.ID && p.BID == BID).Include(o => o.PaymentMethod).ToList();
            foreach (var paymentApplication in paymentApplications)
            {
                glData.AddRange(CalculateGLForPayment(paymentApplication));
            }
            //compress data and return
            return CompressGLForTransaciton(glData);
        }

        private List<GLData> GetGLDataEntriesForOrder(TransactionHeaderData order, List<GLData> incomeEntries, List<GLData> taxEntries)
        {
            var subTotalAmount = incomeEntries.Select(entry => entry.Amount).Sum();
            var totalAmount = subTotalAmount + taxEntries.Select(entry => entry.Amount).Sum();
            GLAccount gLAccount = Context.GLAccount.Where(gl => gl.ID == Orders_In_WIP_ID && gl.BID == BID).First(); //WIP
            List<GLData> glData = new List<GLData>();
            bool includeTaxAmount = false;
            bool isOffBS = false;
            switch (order.OrderStatusID)
            {
                case OrderOrderStatus.OrderBuilt:
                case OrderOrderStatus.OrderInvoicing:
                    gLAccount = Context.GLAccount.Where(gl => gl.ID == Orders_In_Built_ID && gl.BID == BID).First();
                    goto case OrderOrderStatus.OrderWIP;
                case OrderOrderStatus.OrderPreWIP:
                case OrderOrderStatus.OrderWIP:
                    GLData futureIncomeEntry = new GLData
                    {
                        BID = order.BID,
                        OrderID = order.ID,
                        CompanyID = order.CompanyID,
                        LocationID = order.LocationID,
                        IsTaxable = !order.IsTaxExempt,
                        Amount = includeTaxAmount ? totalAmount : subTotalAmount,
                        IsOffBS = isOffBS,
                        GLAccountID = Production_Deliverables_ID, //Production deliverables
                        GLAccount = Context.GLAccount.Where(gl => gl.ID == Production_Deliverables_ID && gl.BID == BID).First()
                    };
                    glData.Add(futureIncomeEntry);
                    break;
                case OrderOrderStatus.OrderClosed:
                case OrderOrderStatus.OrderInvoiced:
                    gLAccount = Context.GLAccount.Where(gl => gl.ID == Accounts_Receivable_ID && gl.BID == BID).First();
                    glData.AddRange(incomeEntries);
                    glData.AddRange(taxEntries);
                    includeTaxAmount = true;
                    break;
                case OrderOrderStatus.CreditMemoPosted:
                    gLAccount = Context.GLAccount.Where(gl => gl.ID == Accounts_Receivable_ID && gl.BID == BID).First();
                    glData.AddRange(incomeEntries.Select(this.NegateGLData));
                    glData.AddRange(taxEntries.Select(this.NegateGLData));
                    includeTaxAmount = true;
                    break;
                case OrderOrderStatus.OrderVoided:
                case OrderOrderStatus.CreditMemoVoided:
                case OrderOrderStatus.CreditMemoUnposted:
                    break;
            }
            if (order.OrderStatusID != OrderOrderStatus.OrderVoided &&
                order.OrderStatusID != OrderOrderStatus.CreditMemoVoided &&
                order.OrderStatusID != OrderOrderStatus.CreditMemoUnposted)
            {
                GLData incomeOffsetEntry = new GLData
                {
                    BID = order.BID,
                    OrderID = order.ID,
                    CompanyID = order.CompanyID,
                    LocationID = order.LocationID,
                    IsTaxable = !order.IsTaxExempt,
                    Amount = includeTaxAmount ? -totalAmount : -subTotalAmount,
                    IsOffBS = isOffBS,
                    GLAccountID = gLAccount.ID,
                    GLAccount = gLAccount
                };
                if (order.TransactionType == (byte)OrderTransactionType.Memo)
                {
                    incomeOffsetEntry = this.NegateGLData(incomeOffsetEntry);
                }
                glData.Add(incomeOffsetEntry);
            }
            return glData;
        }

        private GLData NegateGLData(GLData gLData)
        {
            gLData.Amount *= -1;
            return gLData;
        }

        public List<GLData> GetGLIncomeForOrder(TransactionHeaderData order)
        {
            //add up prices and accounts from children recursively
            List<GLData> incomeEntries = new List<GLData>();
            //1. OrderItems
            foreach (var orderItem in Context.OrderItemData
                .Include(x => x.Components)
                .Include(x => x.Surcharges)
                .Include(x => x.Order)
                .Where(oi => oi.OrderID == order.ID && oi.BID == BID).ToList())
            {
                var entries = CalculateIncomeGLForOrderItem(order, orderItem);
                incomeEntries.AddRange(entries);
            }
            //2. OrderDestinations 
            if (order.Destinations != null)
            {
                foreach (var orderDestination in order.Destinations)
                {
                    incomeEntries.Add(CalculateGLForOrderDestination(orderDestination));
                }
            }
            //Add LocationID to all entries
            incomeEntries.ForEach(entry => entry.LocationID = order.LocationID);
            return incomeEntries;
        }

        private List<GLData> CalculateIncomeGLForOrderItem(TransactionHeaderData order, OrderItemData orderItem)
        {
            List<GLData> entries = new List<GLData>();
            //Components
            foreach (var orderComponent in orderItem.Components.Where(x => !x.ParentComponentID.HasValue).ToList()) //Load only child components
            {
                if (!orderComponent.PricePreTax.HasValue) throw new Exception($"Order Component ${orderComponent.ID} does not have a price set");
                //check IncomeAllocationType - temporary workaround if IncomeAllocationType is null (but that shouldn't be the case)
                if (!orderComponent.AssemblyID.HasValue || orderComponent.IncomeAllocationType == AssemblyIncomeAllocationType.SingleIncomeAllocation || orderComponent.IncomeAllocationType == null)
                {
                    if (!orderComponent.IncomeAccountID.HasValue) throw new Exception($"Order Component ${orderComponent.ID} does not have a income account set");
                    GLData incomeEntry = new GLData
                    {
                        BID = orderItem.BID,
                        ItemComponentID = orderComponent.ID,
                        CompanyID = order.CompanyID,
                        OrderItemID = orderItem.ID,
                        OrderID = orderItem.OrderID,
                        Amount = -orderComponent.PricePreTax.Value,
                        GLAccountID = orderComponent.IncomeAccountID.Value,
                        GLAccount = orderComponent.IncomeAccount,
                        LocationID = order.LocationID,
                        AssemblyID = orderComponent.AssemblyID,
                        MaterialID = orderComponent.MaterialID,
                        LaborID = orderComponent.LaborID,
                        MachineID = orderComponent.MachineID,
                        IsTaxable = !order.IsTaxExempt
                    };
                    entries.Add(incomeEntry);
                }
                else
                {
                    //Must be Assembly - Look into children based upon income type
                    if (!orderComponent.IncomeAllocationType.HasValue) throw new Exception($"Order Component {orderComponent.ID} does not have a income allocation type set");

                    List<GLIncomeEntry> entriesToUseForIncomeAllotment =
                        SearchAssembliesForRollupData(orderComponent, orderComponent.IncomeAllocationType.Value)
                        .Where(entry => entry.AmountToUse.HasValue && entry.GLAccountID.HasValue).ToList();
                    //Recursively search assemblies
                    if (!entriesToUseForIncomeAllotment.Any() && entriesToUseForIncomeAllotment.Select(entry => entry.AmountToUse).Sum() != 0m)
                    {
                        //Nothing found so just use the existing info
                        GLData incomeEntry = new GLData
                        {
                            BID = orderItem.BID,
                            ItemComponentID = orderComponent.ID,
                            CompanyID = orderItem.Order.CompanyID,
                            OrderItemID = orderItem.ID,
                            OrderID = orderItem.OrderID,
                            Amount = -orderComponent.PricePreTax.Value,
                            GLAccountID = orderComponent.IncomeAccountID.Value,
                            GLAccount = orderComponent.IncomeAccount,
                            LocationID = order.LocationID,
                            AssemblyID = orderComponent.AssemblyID,
                            MaterialID = orderComponent.MaterialID,
                            LaborID = orderComponent.LaborID,
                            MachineID = orderComponent.MachineID,
                        };
                        entries.Add(incomeEntry);
                    }
                    else
                    {
                        //Allocate the amounts
                        var costTotal = entriesToUseForIncomeAllotment.Select(entry => entry.AmountToUse).Sum();
                        var summedEntries = entriesToUseForIncomeAllotment
                            .GroupBy(
                                entry => new
                                {
                                    entry.GLAccountID.Value,
                                    entry.AssemblyID,
                                    entry.MaterialID,
                                    entry.LaborID,
                                    entry.MachineID,
                                    entry.PlaceID,
                                    entry.AssetID
                                })
                                .SelectMany(g => g)
                            .Select(entry => new GLIncomeEntry
                            {
                                GLAccountID = entry.GLAccountID,
                                AmountToUse = Math.Round((orderComponent.PricePreTax.Value * (entry.AmountToUse.Value / costTotal.Value)), 2),
                                AssemblyID = entry.AssemblyID,
                                MaterialID = entry.MaterialID,
                                LaborID = entry.LaborID,
                                MachineID = entry.MachineID,
                                PlaceID = entry.PlaceID,
                                AssetID = entry.AssetID
                            }).ToList();
                        //check that the sums match
                        var roundedSum = summedEntries.Select(entry => entry.AmountToUse.Value).Sum();
                        var firstEntry = summedEntries.First();
                        firstEntry.AmountToUse += orderComponent.PricePreTax.Value - roundedSum;

                        summedEntries.ForEach(entry =>
                        {
                            GLData incomeEntry = new GLData
                            {
                                BID = orderItem.BID,
                                ItemComponentID = orderComponent.ID,
                                CompanyID = orderItem.Order.CompanyID,
                                OrderItemID = orderItem.ID,
                                OrderID = orderItem.OrderID,
                                Amount = -entry.AmountToUse.Value,
                                GLAccountID = entry.GLAccountID.Value,
                                LocationID = order.LocationID
                            };
                            entries.Add(incomeEntry);
                        });
                    }
                }
            }
            foreach (var surcharge in orderItem.Surcharges)
            {
                //Nothing found so just use the existing info
                var amount = surcharge.PricePreTax.GetValueOrDefault(0m); //- AppliedDiscount
                if (amount != 0)
                {
                    GLData incomeEntry = new GLData
                    {
                        BID = orderItem.BID,
                        //SurchargeID = surcharge.ID,
                        CompanyID = orderItem.Order.CompanyID,
                        OrderItemID = orderItem.ID,
                        OrderID = orderItem.OrderID,
                        Amount = -amount,
                        GLAccountID = surcharge.IncomeAccountID,
                        GLAccount = surcharge.IncomeAccount,
                        LocationID = order.LocationID
                    };
                    entries.Add(incomeEntry);
                }
            }
            return entries;
        }

        //Recursive function that rolls up any children, grandchildren,etc. matching the type requested
        private List<GLIncomeEntry> SearchAssembliesForRollupData(OrderItemComponent orderComponent, AssemblyIncomeAllocationType allocationType)
        {
            List<GLIncomeEntry> entriesToUseForIncomeAllotment = new List<GLIncomeEntry>();
            if (allocationType == AssemblyIncomeAllocationType.AllocationByMaterialIncomeAccount)
                entriesToUseForIncomeAllotment.AddRange(orderComponent.ChildComponents.Where(entry => entry.MaterialID.HasValue).Select(x => new GLIncomeEntry
                {
                    GLAccountID = x.IncomeAccountID,
                    AmountToUse = x.CostNet,
                    AssemblyID = x.AssemblyID,
                    MaterialID = x.MaterialID,
                    LaborID = x.LaborID,
                    MachineID = x.MachineID,
                }));
            if (allocationType == AssemblyIncomeAllocationType.AllocationByMachineIncomeAccount)
                entriesToUseForIncomeAllotment.AddRange(orderComponent.ChildComponents.Where(entry => entry.MachineID.HasValue).Select(x => new GLIncomeEntry
                {
                    GLAccountID = x.IncomeAccountID,
                    AmountToUse = x.CostNet,
                    AssemblyID = x.AssemblyID,
                    MaterialID = x.MaterialID,
                    LaborID = x.LaborID,
                    MachineID = x.MachineID,
                }));
            orderComponent.ChildComponents.Where(entry => entry.AssemblyID.HasValue && (entry.RollupLinkedPriceAndCost ?? false)).ToList()
                .ForEach(entry => entriesToUseForIncomeAllotment.AddRange(SearchAssembliesForRollupData(entry, allocationType)));
            return entriesToUseForIncomeAllotment;
        }

        private GLData CalculateGLForOrderDestination(OrderDestinationData orderDestination)
        {
            return new GLData
            {
                BID = orderDestination.BID,
                DestinationID = orderDestination.ID,
                CompanyID = orderDestination.Order.CompanyID,
                OrderID = orderDestination.OrderID,
                Amount = -orderDestination.PriceNet.Value,
                GLAccountID = Shipping_Income_ID
            };
        }

        public List<GLData> CalculateGL(EnumGLType type, int Id)
        {
            List<GLData> glData = new List<GLData>();
            switch (type)
            {
                case EnumGLType.Order:
                    TransactionHeaderData order = null;
                    try
                    {
                        order = Context.TransactionHeaderData.AsNoTracking().Include(o => o.Destinations).Where(o => o.ID == Id && o.BID == BID).First();
                    }
                    catch (Exception)
                    {
                        throw new Exception(String.Format("OrderID of {0} could not be found", Id));
                    }
                    glData = CalculateGLForOrder(order);
                    break;
                case EnumGLType.Payment:
                    PaymentApplication payment = null;
                    try
                    {
                        payment = Context.PaymentApplication.Where(o => o.ID == Id && o.BID == BID).Include(o => o.PaymentMethod).First();

                    }
                    catch (Exception)
                    {
                        throw new Exception(String.Format("PaymentApplication of {0} could not be found", Id));
                    }
                    glData = CalculateGLForPayment(payment);
                    break;
                case EnumGLType.OrderDestination:
                    OrderDestinationData destination = null;
                    try
                    {
                        destination = Context.OrderDestinationData.Where(o => o.ID == Id && o.BID == BID).First();
                        order = Context.OrderData.Include(o => o.Destinations).Where(o => o.ID == destination.OrderID).First();
                    }
                    catch (Exception)
                    {
                        throw new Exception(String.Format("OrderDestination of {0} could not be found", Id));
                    }
                    var incomeEntries = new List<GLData>();
                    incomeEntries.Add(CalculateGLForOrderDestination(destination));
                    glData = GetGLDataEntriesForOrder(order, incomeEntries, new List<GLData>());
                    break;
                case EnumGLType.OrderItem:
                    OrderItemData itemData = null;
                    try
                    {
                        itemData = Context.OrderItemData.Where(o => o.ID == Id && o.BID == BID).First();
                        order = Context.OrderData.Include(o => o.Destinations).Where(o => o.ID == itemData.OrderID && o.BID == BID).First();
                    }
                    catch (Exception)
                    {
                        throw new Exception(String.Format("OrderDestination of {0} could not be found", Id));
                    }
                    incomeEntries = CalculateIncomeGLForOrderItem(order, itemData);
                    List<OrderItemTaxAssessment> orderItemTaxAssessments = Context.OrderItemTaxAssessment.Where(p => p.OrderItemID == itemData.ID && p.BID == BID).ToList();
                    var taxEntries = new List<GLData>();
                    foreach (var orderItemTaxAssessmsent in orderItemTaxAssessments)
                    {
                        taxEntries.Add(new GLData
                        {
                            BID = order.BID,
                            OrderID = order.ID,
                            CompanyID = order.CompanyID,
                            LocationID = order.LocationID,
                            IsTaxable = !order.IsTaxExempt,
                            Amount = -orderItemTaxAssessmsent.TaxAmount,
                            GLAccountID = orderItemTaxAssessmsent.TaxItem.GLAccountID,
                            OrderItemID = orderItemTaxAssessmsent.OrderItemID,
                            TaxItemID = orderItemTaxAssessmsent.TaxItemID,
                            ItemComponentID = orderItemTaxAssessmsent.ItemComponentID,
                            ItemSurchargeID = orderItemTaxAssessmsent.ItemSurchargeID,
                            TaxCodeID = orderItemTaxAssessmsent.TaxCodeID,
                            TaxGroupID = orderItemTaxAssessmsent.TaxGroupID
                        });
                    }
                    glData = GetGLDataEntriesForOrder(order, incomeEntries, taxEntries);
                    break;
            }
            //Double Check that Debits == Credits
            if (glData.Select(entry => entry.Amount).Sum() != 0m)
            {
                throw new Exception("Error in GLEngine. Debits do not match Credits for current object");
            }
            return glData;
        }

        public List<GLData> LoadHistoricalGLData(EnumGLType type, int Id)
        {
            //Load Histical GL Information from DB
            List<GLData> historicalRecord = null;
            switch (type)
            {
                case EnumGLType.Order:
                    historicalRecord = Context.GLData.Where(entry => entry.OrderID == Id && entry.BID == BID).ToList();
                    break;
                case EnumGLType.Payment:
                    historicalRecord = Context.GLData.Where(entry => entry.PaymentID == Id && entry.BID == BID).ToList();
                    break;
                default: return null;
            }
            List<GLData> returnData = new List<GLData>();
            foreach (var entry in historicalRecord)
            {
                returnData.Add(new GLData
                {
                    BID = entry.BID,
                    GLAccountID = entry.GLAccountID,
                    CompanyID = entry.CompanyID,
                    LocationID = entry.LocationID,
                    OrderItemID = entry.OrderItemID,
                    ItemComponentID = entry.ItemComponentID,
                    LaborID = entry.LaborID,
                    MachineID = entry.MachineID,
                    MaterialID = entry.MaterialID,
                    AssemblyID = entry.AssemblyID,
                    AssetID = entry.AssetID,
                    DestinationID = entry.DestinationID,
                    OrderID = entry.OrderID,
                    PaymentID = entry.PaymentID,
                    Amount = entry.Amount,
                    IsTaxable = entry.IsTaxable
                });
            }
            return returnData;
        }

        public List<GLData> ComputeGLDiff(EnumGLType type, int Id)
        {
            //Load Historical GLInformation from DB
            var historicalGL = LoadHistoricalGLData(type, Id);
            if (historicalGL.Select(entry => entry.Amount).Sum() != 0m)
            {
                throw new Exception("Error in GLEngine. Debits do not match Credits for object loaded from DB");
            }
            //Generate current order GL
            var currentGLData = CalculateGL(type, Id);
            //Calculate Difference
            List<GLData> diffGL = CalculateDiffGL(currentGLData, historicalGL);
            return diffGL;

        }

        public List<GLData> CompressGLForTransaciton(List<GLData> glData)
        {
            return glData.Where(entry => entry.Amount != 0)
                .GroupBy(entry => new
                {
                    entry.BID,
                    entry.GLAccountID,
                    entry.CompanyID,
                    entry.LocationID,
                    entry.IsTaxable,
                    entry.OrderItemID,
                    entry.ItemComponentID,
                    entry.LaborID,
                    entry.MachineID,
                    entry.MaterialID,
                    entry.AssemblyID,
                    entry.AssetID,
                    entry.DestinationID,
                    entry.OrderID,
                    entry.PaymentID,
                    entry.TaxItemID,
                    entry.ItemSurchargeID,
                    entry.TaxGroupID,
                    entry.IsOffBS,
                    entry.TaxCodeID,
                    entry.CurrencyType,
                    entry.PaymentMethodID

                })
                .Select(entry => new GLData
                {
                    BID = entry.Key.BID,
                    GLAccountID = entry.Key.GLAccountID,
                    LocationID = entry.Key.LocationID,
                    OrderItemID = entry.Key.OrderItemID,
                    ItemComponentID = entry.Key.ItemComponentID,
                    LaborID = entry.Key.LaborID,
                    IsTaxable = entry.Key.IsTaxable,
                    CompanyID = entry.Key.CompanyID,
                    MachineID = entry.Key.MachineID,
                    MaterialID = entry.Key.MaterialID,
                    AssemblyID = entry.Key.AssemblyID,
                    AssetID = entry.Key.AssetID,
                    DestinationID = entry.Key.DestinationID,
                    OrderID = entry.Key.OrderID,
                    PaymentID = entry.Key.PaymentID,
                    TaxItemID = entry.Key.TaxItemID,
                    ItemSurchargeID = entry.Key.ItemSurchargeID,
                    TaxGroupID = entry.Key.TaxGroupID,
                    IsOffBS = entry.Key.IsOffBS,
                    TaxCodeID = entry.Key.TaxCodeID,
                    CurrencyType = entry.Key.CurrencyType,
                    PaymentMethodID = entry.Key.PaymentMethodID,
                    Amount = entry.Sum(e => e.Amount)
                }).ToList()
                .Where(gl => gl.Amount != 0).ToList(); //filter out zero entries
        }

        

        private DateTime truncateSeconds(DateTime date)
        {
            var roundTicks = TimeSpan.TicksPerSecond;
            return new DateTime(date.Ticks - date.Ticks % roundTicks, date.Kind);
        }

        private List<GLData> CalculateDiffGL(List<GLData> currentData, List<GLData> historicalGLData)
        {
            //negate the historical
            historicalGLData.ForEach(x => x.Amount = -x.Amount);
            List<GLData> glData = currentData;
            glData.AddRange(historicalGLData);
            return CompressGLForTransaciton(glData);
        }

        private List<GLData> CalculateGLForPayment(PaymentApplication payment)
        {
            List<GLData> glData = new List<GLData>();
            if (payment.PaymentMethod == null)
            {
                throw new Exception("No Payment Method found for payment with ID of:" + payment.ID);
            }
            var paymentMethodGLAccountId = payment.PaymentMethod.DepositGLAccountID;

            Nullable<OrderOrderStatus> orderStatusID;
            //AR/Liability/Expense Entry
            var entryAmount = -payment.Amount;
            if ((payment.PaymentTransactionType == (byte)PaymentTransactionType.Refund) ||
                (payment.PaymentTransactionType == (byte)PaymentTransactionType.Refund_to_Nonrefundable_Credit) ||
                (payment.PaymentTransactionType == (byte)PaymentTransactionType.Refund_to_Refundable_Credit))
            {
                entryAmount = payment.Amount;
            }
            switch (payment.PaymentTransactionType)
            {
                case (byte)PaymentTransactionType.Refund:
                case (byte)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                case (byte)PaymentTransactionType.Refund_to_Refundable_Credit:
                case (byte)PaymentTransactionType.Payment:
                case (byte)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                case (byte)PaymentTransactionType.Payment_from_Refundable_Credit:
                    var depositEntry = new GLData
                    {
                        BID = payment.BID,
                        OrderID = payment.OrderID,
                        CompanyID = payment.CompanyID,
                        LocationID = payment.LocationID,
                        PaymentID = payment.ID,
                        Amount = entryAmount
                    };
                    if (payment.OrderID.HasValue)
                    {
                        orderStatusID = Context.OrderData.Where(o => o.ID == payment.OrderID.Value && o.BID == BID).Select(o => o.OrderStatusID).First();
                        switch (orderStatusID)
                        {
                            case OrderOrderStatus.OrderBuilt:
                            case OrderOrderStatus.OrderInvoicing:
                            case OrderOrderStatus.OrderPreWIP:
                            case OrderOrderStatus.OrderWIP:
                                //customer deposits, undeposited funds/customer credit
                                depositEntry.GLAccountID = GLEngine.Customer_Deposits_ID;
                                depositEntry.GLAccount = Context.GLAccount.Where(gl => gl.ID == GLEngine.Customer_Deposits_ID && gl.BID == BID).First();
                                glData.Add(depositEntry);
                                break;
                            case OrderOrderStatus.OrderClosed:
                            case OrderOrderStatus.OrderInvoiced:
                                //ar, undeposited funds/customer credit
                                depositEntry.GLAccountID = GLEngine.Accounts_Receivable_ID;
                                depositEntry.GLAccount = Context.GLAccount.Where(gl => gl.ID == Accounts_Receivable_ID && gl.BID == BID).First();
                                glData.Add(depositEntry);
                                break;
                            case OrderOrderStatus.OrderVoided:
                                break;
                        }
                    }
                    else
                    {
                        depositEntry.GLAccountID = GLEngine.Customer_Credits_ID;
                        depositEntry.GLAccount = Context.GLAccount.Where(gl => gl.ID == GLEngine.Customer_Credits_ID && gl.BID == BID).First();
                        glData.Add(depositEntry);
                    }
                    break;
                case (byte)PaymentTransactionType.Credit_Adjustment:
                case (byte)PaymentTransactionType.Credit_Gift:
                    if (payment.OrderID == null)
                    {
                        var expenseEntry = new GLData
                        {
                            BID = payment.BID,
                            OrderID = payment.OrderID,
                            CompanyID = payment.CompanyID,
                            LocationID = payment.LocationID,
                            Amount = -entryAmount,
                            PaymentID = payment.ID,
                            GLAccountID = Credit_Adjustments_ID,
                        };
                        glData.Add(expenseEntry);
                    } else
                    {
                        var ARAdjustmentEntry = new GLData
                        {
                            BID = payment.BID,
                            OrderID = payment.OrderID,
                            CompanyID = payment.CompanyID,
                            LocationID = payment.LocationID,
                            PaymentID = payment.ID,
                            Amount = -entryAmount,
                            GLAccountID = Accounts_Receivable_ID,
                        };
                        glData.Add(ARAdjustmentEntry);
                    }
                    break;
            }

            //UndepositedFunds Entry
            switch (payment.PaymentTransactionType)
            {
                case (byte)PaymentTransactionType.Payment:
                case (byte)PaymentTransactionType.Refund:
                    var undepositedFundsEntry = new GLData
                    {
                        BID = payment.BID,
                        OrderID = payment.OrderID,
                        CompanyID = payment.CompanyID,
                        LocationID = payment.LocationID,
                        PaymentID = payment.ID,
                        Amount = -entryAmount,
                        GLAccountID = paymentMethodGLAccountId,
                    };
                    glData.Add(undepositedFundsEntry);
                    break;
                case (byte)PaymentTransactionType.Refund_to_Nonrefundable_Credit:
                case (byte)PaymentTransactionType.Refund_to_Refundable_Credit:
                case (byte)PaymentTransactionType.Payment_from_Nonrefundable_Credit:
                case (byte)PaymentTransactionType.Payment_from_Refundable_Credit:
                    entryAmount = -entryAmount;
                    var refundEntry = new GLData
                    {
                        BID = payment.BID,
                        OrderID = payment.OrderID,
                        CompanyID = payment.CompanyID,
                        LocationID = payment.LocationID,
                        PaymentID = payment.ID,
                        Amount = entryAmount,
                        GLAccountID = paymentMethodGLAccountId,
                    };
                    glData.Add(refundEntry);
                    break;
                case (byte)PaymentTransactionType.Credit_Adjustment:
                case (byte)PaymentTransactionType.Credit_Gift:
                        var customerCreditEntry = new GLData
                        {
                            BID = payment.BID,
                            OrderID = payment.OrderID,
                            CompanyID = payment.CompanyID,
                            LocationID = payment.LocationID,
                            PaymentID = payment.ID,
                            Amount = entryAmount,
                            GLAccountID = Customer_Credits_ID,
                        };
                        glData.Add(customerCreditEntry);
                    break;
            }
            //compress data and return
            return CompressGLForTransaciton(glData);
        }
    }
}
