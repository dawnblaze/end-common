﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.GLEngine.Models
{
    public enum EnumGLType
    {
        Order = 1,
        Payment = 2,
        OrderItem = 3,
        OrderDestination = 4
    }
}
