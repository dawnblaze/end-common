﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.GLEngine.Models
{
    public class ReconciliationsNeededResponse
    {
        public bool HasAdjustments { get; set; }

        public List<ReconciliationNeededResponse> Adjustments { get; set; }
    }

    public class ReconciliationNeededResponse
    {
        public int AdjustmentReconciliationID { get; set; }
        
        public string Suffix { get; set; }

        public DateTime StartingDT { get; set; }

        public DateTime EndingDT { get; set; }

        public int GLCount { get; set; }

        public int BatchCount { get; set; }

        public byte LocationID { get; set; }
    }
}
