﻿using Endor.EF;
using Endor.GLEngine.Models;
using System;
using System.Linq;
using Endor.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Endor.GLEngine.Classes;
using System.Threading;
using System.Data;
using Microsoft.Data.SqlClient;
using Newtonsoft.Json;
using Endor.GLEngine.Queries;

namespace Endor.GLEngine
{
    public partial class GLEngine
    {
        public async Task<ReconciliationsNeededResponse> GetAdjustmentsNeeded(byte? locationID)
        {
            //Find the Entries that need Adjustments
            var query = ReconciliationQueries.GetAdjustmentsNeeded;

            var conn = Context.Database.GetDbConnection();
            if (conn.State != ConnectionState.Open)
            {
                await conn.OpenAsync();
            }
            using var command = conn.CreateCommand();
            command.CommandText = query;
            var parameters = new[] {
                new SqlParameter("@BID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = BID,
                },
                new SqlParameter("@LocationID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = (locationID.HasValue ? locationID : (object)DBNull.Value),
                },
            };

            foreach (var p in parameters)
            {
                command.Parameters.Add(p);
            }

            using var reader = await command.ExecuteReaderAsync();
            ReconciliationsNeededResponse result = new ReconciliationsNeededResponse();
            result.HasAdjustments = false;
            result.Adjustments = new List<ReconciliationNeededResponse>();
            while (await reader.ReadAsync())
            {
                result.HasAdjustments = true;

                ReconciliationNeededResponse reconciliationToAdjustInfo = new ReconciliationNeededResponse()
                {
                    AdjustmentReconciliationID = reader.GetFieldValue<int>(0),
                    Suffix = reader.GetFieldValue<string>(1),
                    StartingDT = reader.GetFieldValue<DateTime>(2),
                    EndingDT = reader.GetFieldValue<DateTime>(3),
                    LocationID = reader.GetFieldValue<byte>(4),
                    GLCount = reader.GetFieldValue<int>(5),
                    BatchCount = reader.GetFieldValue<int>(6)
                };
                result.Adjustments.Add(reconciliationToAdjustInfo);
            }

            return result;
        }

        public async Task<List<Reconciliation>> GenerateReconciliationAdjustmentsPreview(byte? locationID, short? enteredByID)
        {
            var reconciliations = new List<Reconciliation>();

            // get the list of adjustments
            var response = await GetAdjustmentsNeeded(locationID);
            foreach (var adjustment in response.Adjustments)
            {
                var GLsForAnyAdjustment = (await GetAdjustmentGLs(adjustment.LocationID));
                var glIDsForThisAdjustment = GLsForAnyAdjustment.Where(x =>
                    (x.AccountingDT.CompareTo(adjustment.StartingDT) > 0)
                    && x.AccountingDT.CompareTo(adjustment.EndingDT) <= 0).Select(x => x.ID).ToArray();
                var glSummary = await GetGLSummary(adjustment.StartingDT, adjustment.EndingDT, locationID, glIDsForThisAdjustment);
                var pmSummary = await GetPaymentSummary(adjustment.StartingDT, adjustment.EndingDT, locationID);
                var glRange = new GLRange()
                {
                    BID = GLsForAnyAdjustment.First().BID,
                    LocationID = adjustment.LocationID,
                    StartingGLID = glIDsForThisAdjustment.Min(),
                    EndingGLID = glIDsForThisAdjustment.Max()
                };
                var reconciliation = GenerateReconciliation(adjustment.LocationID, adjustment.StartingDT, adjustment.EndingDT, glRange, glSummary, pmSummary, enteredByID);
                reconciliation.IsAdjustmentEntry = true;
                reconciliation.AdjustedReconciliationID = adjustment.AdjustmentReconciliationID;
                reconciliations.Add(reconciliation);
            }
            return reconciliations;
        }

        private async Task<List<GLData>> GetAdjustmentGLs(short? locationID)
        {
            var query = ReconciliationQueries.GetAdjustmentGLs;

            var conn = Context.Database.GetDbConnection();
            if (conn.State != ConnectionState.Open)
            {
                await conn.OpenAsync();
            }
            using var command = conn.CreateCommand();
            command.CommandText = query;
            var parameters = new[] {
                new SqlParameter("@BID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = BID,
                },
                new SqlParameter("@LocationID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = (locationID.HasValue ? locationID : (object)DBNull.Value),
                },
            };

            foreach (var p in parameters)
            {
                command.Parameters.Add(p);
            }

            using var reader = await command.ExecuteReaderAsync();
            List<GLData> summary = new List<GLData>();
            while (await reader.ReadAsync())
            {
                var pm = new GLData()
                {
                    BID = reader.GetFieldValue<short>(0),
                    ID = reader.GetFieldValue<int>(1),
                    AccountingDT = reader.GetFieldValue<DateTime>(2),
                    LocationID = (byte)reader.GetFieldValue<byte>(3)
                };
                summary.Add(pm);
            }

            return summary;

        }

        public async Task<List<Reconciliation>> GenerateReconciliationPreview(byte? locationID, short? enteredByID, DateTime? accountingDT)
        {
            var locationsToCheck = new List<byte>();
            var reconciliations = new List<Reconciliation>();

            // check if locationID exist
            if (locationID.HasValue)
            {
                if (!Context.LocationData.Any(t => t.BID == BID && t.ID == locationID))
                    throw new Exception("Invalid location ID");

                locationsToCheck.Add(locationID.Value);
            }
            else
                locationsToCheck = Context.LocationData.Where(a => a.BID == BID).Select(a => a.ID).ToList();

            //check if user is valid
            if (enteredByID.HasValue)
            {
                var employee = Context.EmployeeData.FirstOrDefault(t => t.BID == BID && t.ID == enteredByID);
                if (employee == null)
                    throw new Exception("Invalid employee id");
            }

            // defaults the accountingDT to now
            // truncate seconds in accountingDT
            accountingDT = truncateSeconds(accountingDT ?? DateTime.UtcNow);

            foreach (var checkingLocationID in locationsToCheck)
            {
                // set starting date to last known reconciliation
                var startDT = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

                var lastReconciliation = await GetLastReconciliation(locationID);

                if (lastReconciliation != null)
                    startDT = lastReconciliation.AccountingDT;

                var glSummary = await GetGLSummary(startDT, (DateTime)accountingDT, checkingLocationID);
                var pmSummary = await GetPaymentSummary(startDT, (DateTime)accountingDT, checkingLocationID);
                var glRange = await GetGLRange(startDT, (DateTime)accountingDT, checkingLocationID);

                reconciliations.Add(GenerateReconciliation(checkingLocationID, startDT, (DateTime)accountingDT, glRange, glSummary, pmSummary, enteredByID));
            }

            return reconciliations;
        }

        /// <summary>
        /// Saves the reconciliation
        /// </summary>
        /// <param name="locationID"></param>
        /// <param name="enteredByID"></param>
        /// <param name="accountingDT"></param>
        /// <returns></returns>
        public async Task<List<Reconciliation>> Reconcile(byte? locationID, short? enteredByID, DateTime? accountingDT, bool createAdjustments, IEnumerable<CashDrawerAdjustment> cashDrawerAdjustments, Func<ApiContext, short, int, Task<int>> IDGenerator)
        {
            var hasAdjustments = await this.GetAdjustmentsNeeded(locationID);
            var generatedReconciliations = new List<Reconciliation>();

            if (hasAdjustments.HasAdjustments && !createAdjustments)
                throw new Exception("Adjustment Reconciliations must be made before new reconciliations must be closed, or the CreateAdjustments parameter must be specified as true.");

            var cashDrawerAccountingDT = truncateSeconds(accountingDT ?? DateTime.UtcNow);
            cashDrawerAccountingDT = cashDrawerAccountingDT.AddMilliseconds(-50); //Need to make sure it falls inside reconciliation time period
            await HandleCashDrawerAdjustments(locationID, enteredByID, cashDrawerAccountingDT, cashDrawerAdjustments, IDGenerator);

            var lastReconciliation = await GetLastReconciliation(locationID);

            if (hasAdjustments.HasAdjustments)
            {
                var adjustments = await this.GenerateReconciliationAdjustmentsPreview(locationID, enteredByID);

                if (adjustments != null && adjustments.Any())
                    generatedReconciliations.AddRange(adjustments);
            }

            var newReconciliations = await this.GenerateReconciliationPreview(locationID, enteredByID, accountingDT);

            if (newReconciliations != null && newReconciliations.Any())
                generatedReconciliations.AddRange(newReconciliations);

            foreach (var generatedReconciliation in generatedReconciliations)
            {
                var glActivity = await this.CreateGLActivity("Reconciliation", generatedReconciliation.IsAdjustmentEntry ? "Reconciliation Adjustment" : "Reconciliation", enteredByID, null, DateTime.UtcNow, GLEntryType.Reconciliation, new List<GLData>(), IDGenerator);

                generatedReconciliation.ID = await IDGenerator(Context, this.BID, (int)ClassType.Reconciliation);
                generatedReconciliation.GLActivityID = glActivity.ID;
                //formatted number
                var locationAbbreviation = Context.LocationData.Where(x => x.BID == this.BID && x.ID == generatedReconciliation.LocationID).FirstOrDefault().Abbreviation;
                generatedReconciliation.FormattedNumber = locationAbbreviation + '-' + generatedReconciliation.ID.ToString();
                if (generatedReconciliation.IsAdjustmentEntry)
                {
                    var adjustmentNumber = Context.Reconciliation.Where(x => x.BID == this.BID && x.AdjustedReconciliationID == generatedReconciliation.AdjustedReconciliationID).Count() + 1;
                    generatedReconciliation.FormattedNumber += '-' + adjustmentNumber.ToString();
                }

                foreach (var item in generatedReconciliation.Items)
                    item.ID = await IDGenerator(Context, this.BID, (int)ClassType.ReconciliationItem);

                Context.Reconciliation.Add(generatedReconciliation);
            }

            if (lastReconciliation != null)
            {
                var currentReconciliation = await GetLastReconciliation(locationID);

                if (currentReconciliation.ID > lastReconciliation.ID || generatedReconciliations.All(a => a.AccountingDT < currentReconciliation.AccountingDT))
                    throw new ApplicationException("There is already a newer reconciliation created for this location.");
            }

            return generatedReconciliations;
        }

        private async Task HandleCashDrawerAdjustments(byte? locationID, short? enteredByID, DateTime accountingDT, IEnumerable<CashDrawerAdjustment> cashDrawerAdjustments, Func<ApiContext, short, int, Task<int>> IDGenerator)
        {
            if (cashDrawerAdjustments != null && cashDrawerAdjustments.Any())
            {
                if (locationID.HasValue &&
                    ((cashDrawerAdjustments.Count() > 1) || (cashDrawerAdjustments.First().LocationID != locationID.Value)))
                    throw new Exception("Cash Drawer adjustment location must match reconciliation location");
                int glActivityIDForAdjustment = 0;
                int glActivityIDForDeposit = 0;

                if (cashDrawerAdjustments.Any(a => a.AdjustmentsIn > 0 || a.AdjustmentsOut > 0))
                {
                    var glActivity = await this.CreateGLActivity("CashDrawer", "Cash Drawer Adjustment", enteredByID, null, DateTime.UtcNow, GLEntryType.CashDrawerAdjustment, new List<GLData>(), IDGenerator);
                    glActivityIDForAdjustment = glActivity.ID;
                }

                if (cashDrawerAdjustments.Any(a => a.DepositTransfer > 0))
                {
                    var glActivity = await this.CreateGLActivity("CashDrawer", "Cash Drawer Deposit", enteredByID, null, DateTime.UtcNow, GLEntryType.CashDrawerDeposit, new List<GLData>(), IDGenerator);
                    glActivityIDForDeposit = glActivity.ID;
                }

                var cashDrawerGlEntries = new List<GLData>();

                foreach (var cashDraw in cashDrawerAdjustments)
                {
                    if (cashDraw.AdjustmentsIn > 0 || cashDraw.AdjustmentsOut > 0)
                    {
                        cashDrawerGlEntries.Add(new GLData
                        {
                            AccountingDT = accountingDT,
                            Amount = Convert.ToDecimal(cashDraw.AdjustmentsIn),
                            ActivityID = glActivityIDForAdjustment,
                            GLAccountID = 1101,
                            LocationID = cashDraw.LocationID,
                            ID = await IDGenerator(Context, this.BID, (int)ClassType.GL),
                            BID = this.BID
                        });

                        cashDrawerGlEntries.Add(new GLData
                        {
                            AccountingDT = accountingDT,
                            Amount = Convert.ToDecimal(-cashDraw.AdjustmentsOut),
                            ActivityID = glActivityIDForAdjustment,
                            GLAccountID = 1101,
                            LocationID = cashDraw.LocationID,
                            ID = await IDGenerator(Context, this.BID, (int)ClassType.GL),
                            BID = this.BID
                        });

                        cashDrawerGlEntries.Add(new GLData
                        {
                            AccountingDT = accountingDT,
                            Amount = Convert.ToDecimal(cashDraw.AdjustmentsOut - cashDraw.AdjustmentsIn),
                            ActivityID = glActivityIDForAdjustment,
                            LocationID = cashDraw.LocationID,
                            GLAccountID = 6101,
                            ID = await IDGenerator(Context, this.BID, (int)ClassType.GL),
                            BID = this.BID
                        });
                    }

                    if (cashDraw.DepositTransfer > 0)
                    {
                        cashDrawerGlEntries.Add(new GLData
                        {
                            AccountingDT = accountingDT,
                            Amount = Convert.ToDecimal(-cashDraw.DepositTransfer),
                            ActivityID = glActivityIDForDeposit,
                            LocationID = cashDraw.LocationID,
                            GLAccountID = 1101,
                            ID = await IDGenerator(Context, this.BID, (int)ClassType.GL),
                            BID = this.BID
                        });

                        cashDrawerGlEntries.Add(new GLData
                        {
                            AccountingDT = accountingDT,
                            Amount = Convert.ToDecimal(cashDraw.DepositTransfer),
                            ActivityID = glActivityIDForDeposit,
                            LocationID = cashDraw.LocationID,
                            GLAccountID = 1100,
                            ID = await IDGenerator(Context, this.BID, (int)ClassType.GL),
                            BID = this.BID
                        });
                    }
                }

                if(cashDrawerGlEntries.Any())
                {
                    Context.GLData.AddRange(cashDrawerGlEntries);
                    await Context.SaveChangesAsync();
                }
            }
        }

        private async Task<Reconciliation> GetLastReconciliation(short? locationID) =>
            await Context.Reconciliation.Where(t => t.BID == BID && (locationID.HasValue ? t.LocationID == locationID : true)).OrderByDescending(a => a.ID).FirstOrDefaultAsync();

        private Reconciliation GenerateReconciliation(byte locationID, DateTime startDT, DateTime endDT, GLRange glRange, List<GLSummaryItem> glSummary, List<PaymentSummaryItem> pmSummary, short? enteredByID)
        {
            var result = new Reconciliation()
            {
                BID = BID,
                ID = 0,
                ClassTypeID = (int)ClassType.Reconciliation,
                LastAccountingDT = startDT,
                AccountingDT = endDT,
                StartingGLID = glRange.StartingGLID,
                EndingGLID = glRange.EndingGLID,
                LocationID = locationID,
                IsAdjustmentEntry = false,
                ExportedDT = null,
                WasExported = false,
                EnteredByID = enteredByID,
                GLActivityID = 0,
                Items = new List<ReconciliationItem>()
            };

            foreach (var item in glSummary)
                result.Items.Add(new ReconciliationItem()
                {
                    ID = 0,
                    BID = result.BID,
                    ClassTypeID = (int)ClassType.ReconciliationItem,
                    ReconciliationID = 0,
                    GLAccountID = item.GLAccountID,
                    Amount = item.Amount,
                    Balance = item.Balance,
                    IsPaymentSummary = false,
                });

            foreach (var item in pmSummary)
                result.Items.Add(new ReconciliationItem()
                {
                    ID = 0,
                    BID = result.BID,
                    ClassTypeID = (int)ClassType.ReconciliationItem,
                    ReconciliationID = 0,
                    PaymentMethodID = item.PaymentMethod,
                    PaymentMasterCount = (short)item.PaymentMasterCount,
                    PaymentApplicationCount = (short)item.PaymentApplicationCount,
                    Amount = item.Amount,
                    IsPaymentSummary = true,
                });

            var glIDs = result.Items.Where(x => x.GLAccountID != null).Select(x => x.GLAccountID.Value);
            var incomeAccountType = 40;
            var glIDsThatAreIncome = Context.GLAccount.Where(x => x.BID == this.BID && x.GLAccountType == incomeAccountType && glIDs.Contains(x.ID)).Select(x => x.ID).ToList();
            result.TotalIncome = -result.Items.Where(x => x.GLAccountID != null && glIDsThatAreIncome.Contains(x.GLAccountID.Value)).Sum(x => x.Amount).GetValueOrDefault();
            result.TotalPayments = result.Items.Where(x => x.PaymentMethodID != null).Sum(x => x.Amount).GetValueOrDefault();
            return result;
        }

        private async Task<GLRange> GetGLRange(DateTime startDT, DateTime endDT, short? locationID)
        {
            var query = ReconciliationQueries.GetGLRange;
            var conn = Context.Database.GetDbConnection();
            if (conn.State != ConnectionState.Open)
            {
                await conn.OpenAsync();
            }
            using var command = conn.CreateCommand();
            command.CommandText = query;
            var parameters = new[] {
                new SqlParameter("@BID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = BID,
                },
                new SqlParameter("@StartDT", SqlDbType.DateTime2) {
                    Direction = ParameterDirection.Input,
                    Size = 2,
                    Value = startDT,
                },
                new SqlParameter("@EndDT", SqlDbType.DateTime2) {
                    Direction = ParameterDirection.Input,
                    Size = 2,
                    Value = endDT,
                },
                new SqlParameter("@LocationID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = (locationID.HasValue ? locationID : (object)DBNull.Value),
                },
            };

            foreach (var p in parameters)
            {
                command.Parameters.Add(p);
            }

            using var reader = await command.ExecuteReaderAsync();
            GLRange result = new GLRange();
            if (await reader.ReadAsync())
            {
                result = new GLRange()
                {
                    BID = reader.GetFieldValue<short>(0),
                    LocationID = reader.GetFieldValue<byte>(1),
                    StartingGLID = reader.GetFieldValue<int>(2),
                    EndingGLID = reader.GetFieldValue<int>(3),
                };
            }

            return result;
        }

        private async Task<List<GLSummaryItem>> GetGLSummary(DateTime startDT, DateTime endDT, short? locationID, int[] glids = null)
        {
            //Use linq ?

            var query = ReconciliationQueries.GetGLSummary;
            var conn = Context.Database.GetDbConnection();
            if (conn.State != ConnectionState.Open)
            {
                await conn.OpenAsync();
            }
            using var command = conn.CreateCommand();
            command.CommandText = query;
            var parameters = new[] {
                new SqlParameter("@BID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = BID,
                },
                new SqlParameter("@StartDT", SqlDbType.DateTime2) {
                    Direction = ParameterDirection.Input,
                    Size = 2,
                    Value = startDT,
                },
                new SqlParameter("@EndDT", SqlDbType.DateTime2) {
                    Direction = ParameterDirection.Input,
                    Size = 2,
                    Value = endDT,
                },
                new SqlParameter("@LocationID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = (locationID.HasValue ? locationID : (object)DBNull.Value),
                },
                new SqlParameter("@GLIDs", SqlDbType.NVarChar) {
                    Direction = ParameterDirection.Input,
                    Value = (glids != null ? JsonConvert.SerializeObject(glids) : (object)DBNull.Value),
                },
            };

            foreach (var p in parameters)
            {
                command.Parameters.Add(p);
            }

            using var reader = await command.ExecuteReaderAsync();
            List<GLSummaryItem> summary = new List<GLSummaryItem>();
            while (await reader.ReadAsync())
            {
                var pm = new GLSummaryItem()
                {
                    BID = reader.GetFieldValue<short>(0),
                    LocationID = reader.GetFieldValue<byte>(1),
                    GLAccountID = reader.GetFieldValue<int>(2),
                    Amount = reader.GetFieldValue<decimal>(3),
                    Balance = reader.GetFieldValue<decimal>(4),
                };
                summary.Add(pm);
            }

            return summary;
        }

        private async Task<List<PaymentSummaryItem>> GetPaymentSummary(DateTime startDT, DateTime endDT, short? locationID)
        {
            var query = ReconciliationQueries.GetPaymentSummary;
            var conn = Context.Database.GetDbConnection();
            if (conn.State != ConnectionState.Open)
            {
                await conn.OpenAsync();
            }
            using var command = conn.CreateCommand();
            command.CommandText = query;
            var parameters = new[] {
                new SqlParameter("@BID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = BID,
                },
                new SqlParameter("@StartDT", SqlDbType.DateTime2) {
                    Direction = ParameterDirection.Input,
                    Size = 2,
                    Value = startDT,
                },
                new SqlParameter("@EndDT", SqlDbType.DateTime2) {
                    Direction = ParameterDirection.Input,
                    Size = 2,
                    Value = endDT,
                },
                new SqlParameter("@LocationID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = (locationID.HasValue ? locationID : (object)DBNull.Value),
                },
            };

            foreach (var p in parameters)
            {
                command.Parameters.Add(p);
            }

            using var reader = await command.ExecuteReaderAsync();
            List<PaymentSummaryItem> pmList = new List<PaymentSummaryItem>();
            while (await reader.ReadAsync())
            {
                var pm = new PaymentSummaryItem()
                {
                    BID = reader.GetFieldValue<short>(0),
                    LocationID = reader.GetFieldValue<byte>(1),
                    PaymentMethod = reader.GetFieldValue<int>(2),
                    Name = reader.GetFieldValue<string>(3),
                    Amount = reader.GetFieldValue<decimal>(4),
                    PaymentMasterCount = reader.GetFieldValue<int>(5),
                    PaymentApplicationCount = reader.GetFieldValue<int>(6),
                };
                pmList.Add(pm);
            }

            return pmList;
        }

        /// <summary>
        /// Retrieve the Cash Drawer balances for the current unreconciled period. An optional EndingDT can be supplied to ensure the returned data matches the initial preview information supplied to the client
        /// </summary>
        /// <param name="BID">BID value</param>
        /// <param name="locationID">Location of reconciliation</param>
        /// <param name="endingDT">Ending date, if not specified will get the current date UTC</param>
        /// <returns></returns>
        public async Task<List<CashDrawerBalance>> GetCashDrawerBalance(byte locationID, DateTime? endingDT = null)
        {
            var query = ReconciliationQueries.GetCashDrawerBalance;
            var conn = Context.Database.GetDbConnection();
            if (conn.State != ConnectionState.Open)
            {
                await conn.OpenAsync();
            }
            using var command = conn.CreateCommand();
            command.CommandText = query;
            var parameters = new[] {
                new SqlParameter("@BID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = this.BID,
                },
                new SqlParameter("@EndingDT", SqlDbType.DateTime2) {
                    Direction = ParameterDirection.Input,
                    Size = 2,
                    Value = endingDT ?? DateTime.UtcNow,
                },
                new SqlParameter("@LocationID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = locationID,
                },
            };

            foreach (var p in parameters)
                command.Parameters.Add(p);

            using var reader = await command.ExecuteReaderAsync();
            var cdbList = new List<CashDrawerBalance>();
            while (await reader.ReadAsync())
                cdbList.Add(new CashDrawerBalance()
                {
                    LocationID = (byte)reader.GetFieldValue<Int16>(0),
                    StartingBalance = reader.GetFieldValue<decimal>(1),
                    StartingDT = reader.GetFieldValue<DateTime>(2),
                    AdjustmentsIn = reader.GetFieldValue<decimal>(3),
                    AdjustmentsOut = reader.GetFieldValue<decimal>(4),
                    DepositTransfer = reader.GetFieldValue<decimal>(5),
                    EndingBalance = reader.GetFieldValue<decimal>(6),
                    EndingDT = reader.GetFieldValue<DateTime>(7),
                });

            return cdbList;
        }

        /// <summary>
        /// Retrieve the historical Cash Drawer balances for a previous reconciled period.
        /// </summary>
        /// <param name="BID">BID value</param>
        /// <param name="reconciliationID">Reconciliation ID needed to get the historical data</param>
        /// <returns></returns>
        public async Task<List<CashDrawerBalance>> GetHistoricalCashDrawerBalance(int reconciliationID)
        {
            var query = ReconciliationQueries.GetHistoricalCashDrawerBalance;

            var conn = Context.Database.GetDbConnection();
            if (conn.State != ConnectionState.Open)
            {
                await conn.OpenAsync();
            }
            using var command = conn.CreateCommand();
            command.CommandText = query;
            var parameters = new[] {
                new SqlParameter("@BID", SqlDbType.SmallInt) {
                    Direction = ParameterDirection.Input,
                    Value = this.BID,
                },
                new SqlParameter("@ReconciliationID", SqlDbType.Int) {
                    Direction = ParameterDirection.Input,
                    Size = 2,
                    Value = reconciliationID,
                },
            };

            foreach (var p in parameters)
                command.Parameters.Add(p);

            using var reader = await command.ExecuteReaderAsync();
            var cdbList = new List<CashDrawerBalance>();
            while (await reader.ReadAsync())
                cdbList.Add(new CashDrawerBalance()
                {
                    LocationID = (byte)reader.GetFieldValue<Int16>(0),
                    StartingBalance = reader.GetFieldValue<decimal>(1),
                    StartingDT = reader.GetFieldValue<DateTime>(2),
                    AdjustmentsIn = reader.GetFieldValue<decimal>(3),
                    AdjustmentsOut = reader.GetFieldValue<decimal>(4),
                    DepositTransfer = reader.GetFieldValue<decimal>(5),
                    EndingBalance = reader.GetFieldValue<decimal>(6),
                    EndingDT = reader.GetFieldValue<DateTime>(7),
                });

            return cdbList;
        }
    }
}
