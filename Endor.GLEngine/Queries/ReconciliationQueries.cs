﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.GLEngine.Queries
{
    public static class ReconciliationQueries
    {
        public const string GetAdjustmentGLs = @"
            WITH OrphanGLEntries AS 
            (
                SELECT BID, ID as GLID, LocationID, AccountingDT
                FROM [Accounting.GL.Data] GL
                WHERE GL.BID = @BID 
                AND NOT EXISTS(
                    SELECT ID AS ReconciliationID 
                    FROM [Accounting.Reconciliation.Data] R
                    WHERE R.BID = @BID
                        AND R.LocationID = GL.LocationID
                        AND GL.ID BETWEEN Coalesce(R.StartingGLID,-1000) AND Coalesce(R.EndingGLID,-1000)
                        AND (GL.AccountingDT > R.LastAccountingDT AND GL.AccountingDT <= R.AccountingDT)
                    )                
            ),
            AdjustmentEntries AS
            (
                -- Remove the Entries that are after the current last reconciliation for that location
                SELECT * 
                FROM OrphanGLEntries OE
                WHERE AccountingDT < (SELECT TOP(1) R.AccountingDT 
                                      FROM [Accounting.Reconciliation.Data] R 
                                      WHERE R.BID = OE.BID AND R.LocationID = OE.LocationID
                                        AND R.IsAdjustmentEntry = 0
                                      ORDER BY R.AccountingDT DESC)
            )
                SELECT A.BID, A.GLID, A.AccountingDT, A.LocationID
                FROM [Accounting.Reconciliation.Data] R
                JOIN AdjustmentEntries A ON A.BID = R.BID AND A.LocationID = R.LocationID AND (A.AccountingDT > R.LastAccountingDT AND A.AccountingDT <= R.AccountingDT) AND R.IsAdjustmentEntry = 0
                WHERE R.BID = @BID AND R.LocationID = @LocationID
            ;";

        public const string GetGLRange = @"
            SELECT BID, LocationID, MIN(ID) as StartingGLID, MAX(ID) as EndingGLID
            FROM [Accounting.GL.Data] GL
            WHERE GL.BID = @BID
                AND GL.AccountingDT > @StartDT 
                AND GL.AccountingDT <= @EndDT
                AND (@LocationID IS NULL or LocationID = @LocationID)
            GROUP BY BID, LocationID";

        public const string GetGLSummary = @"
            SELECT BID, LocationID
                    , GLAccountID
                    , SUM(CASE WHEN AccountingDT > @StartDT THEN Amount ELSE 0 END) Amount, SUM(Amount) Balance
            FROM [Accounting.GL.Data]
            WHERE BID = @BID
                AND AccountingDT <= @EndDT
                AND (@LocationID IS NULL or LocationID = @LocationID)
                AND (@GLIDs IS NULL or ID in (select value from OPENJSON(@GLIDs)))
            GROUP BY BID, GLAccountID, LocationID
            ORDER BY GLAccountID";

        public const string GetPaymentSummary = @"
            SELECT GL.BID, GL.LocationID,
                    PM.ID, PM.Name,
                    Sum(GL.Amount) Amount,
                    COUNT(DISTINCT PA.MasterID) As PaymentMasterCount,
                    COUNT(DISTINCT PA.ID) As PaymentApplicationCount
            FROM [Accounting.GL.Data] GL
            JOIN [Accounting.Payment.Application] PA on GL.BID = PA.BID AND GL.PaymentID = PA.ID
            JOIN [Accounting.Payment.Method] PM on PA.BID = PM.BID AND PA.PaymentMethodID = PM.ID
            WHERE GL.BID = @BID
               AND GL.AccountingDT > @StartDT 
               AND GL.AccountingDT <= @EndDT
               AND GL.GLAccountID = 1100
               AND (@LocationID IS NULL or GL.LocationID = @LocationID)
            GROUP BY GL.BID, PM.ID, PM.Name, GL.LocationID
            ORDER BY PM.ID, PM.Name";

        public const string GetAdjustmentsNeeded = @"
            WITH OrphanGLEntries AS 
            (
                SELECT BID, ID as GLID, LocationID, AccountingDT
                FROM [Accounting.GL.Data] GL
                WHERE GL.BID = @BID 
                AND NOT EXISTS(
                    SELECT ID AS ReconciliationID 
                    FROM [Accounting.Reconciliation.Data] R
                    WHERE R.BID = @BID
                        AND R.LocationID = GL.LocationID
                        AND GL.ID BETWEEN Coalesce(R.StartingGLID,-1000) AND Coalesce(R.EndingGLID,-1000)
                        AND (GL.AccountingDT > R.LastAccountingDT AND GL.AccountingDT <= R.AccountingDT)
                    )                
            ),
            AdjustmentEntries AS
            (
                -- Remove the Entries that are after the current last reconciliation for that location
                SELECT * 
                FROM OrphanGLEntries OE
                WHERE AccountingDT < (SELECT TOP(1) R.AccountingDT 
                                      FROM [Accounting.Reconciliation.Data] R 
                                      WHERE R.BID = OE.BID AND R.LocationID = OE.LocationID
                                        AND R.IsAdjustmentEntry = 0
                                      ORDER BY R.AccountingDT DESC)
            )
                SELECT R.ID as AdjustmentReconciliationID
                     , (SELECT CONCAT(R.ID, '-',COALESCE(COUNT(*),0)+1) FROM [Accounting.Reconciliation.Data] R2 WHERE R2.BID = R.BID AND R2.AdjustedReconciliationID = R.ID) AS Suffix
                     , R.LastAccountingDT AS StartingDT
                     , R.AccountingDT AS EndingDT
                     , R.LocationID, COUNT(*) AS GLEntries
                     , COUNT(DISTINCT A.AccountingDT) AS GLBatches
                FROM [Accounting.Reconciliation.Data] R
                JOIN AdjustmentEntries A ON A.BID = R.BID AND A.LocationID = R.LocationID AND (A.AccountingDT > R.LastAccountingDT AND A.AccountingDT <= R.AccountingDT) AND R.IsAdjustmentEntry = 0
                WHERE R.BID = @BID AND R.LocationID = COALESCE(@LocationID,R.LocationID)
                GROUP BY R.BID, R.LastAccountingDT, R.AccountingDT, R.LocationID, R.ID 
            ;";

        /// <summary>
        /// Vars @BID, @LocationID, @EndingDT
        /// </summary>
        public const string GetCashDrawerBalance = @"
                DECLARE @StartingDT DateTime2(2) = (SELECT IsNull(Max(AccountingDT), '1/1/1970')
                                                    FROM [Accounting.Reconciliation.Data] 
                                                    WHERE BID = @BID AND LocationID = @LocationID)
                      , @CashDrawerID INT = 1101;

                -- Retrieve the Data
                SELECT @LocationID as LocationID
                     , IsNull(SUM(CASE WHEN GL.AccountingDT < @StartingDT THEN GL.Amount ELSE 0 END),0) as StartingBalance
                     , @StartingDT as StartingDT
                     , IsNull(SUM(CASE WHEN GL.AccountingDT >= @StartingDT AND GL.AccountingDT < @EndingDT 
                                  AND A.GLEntryType <> 35
                                  AND GL.Amount > 0 THEN GL.Amount ELSE 0 END),0) as AdjustmentsIn
                     , IsNull(SUM(CASE WHEN GL.AccountingDT >= @StartingDT AND GL.AccountingDT < @EndingDT 
                                  AND A.GLEntryType <> 35
                                  AND GL.Amount < 0 THEN GL.Amount ELSE 0 END),0) as AdjustmentsOut
                     , IsNull(SUM(CASE WHEN GL.AccountingDT >= @StartingDT AND GL.AccountingDT < @EndingDT 
                                  AND A.GLEntryType = 35 THEN GL.Amount ELSE 0 END),0) as DepositTransfer
                     , IsNull(SUM(CASE WHEN GL.AccountingDT < @EndingDT THEN GL.Amount ELSE 0 END),0) as EndingBalance
                     , @EndingDT as EndingDT
                FROM [Accounting.GL.Data] GL
                JOIN [Activity.GLActivity] A ON GL.BID = A.BID AND GL.ActivityID = A.ID
                WHERE GL.BID = @BID AND GL.LocationID = @LocationID
                  AND GL.GLAccountID = @CashDrawerID;";

        /// <summary>
        /// Vars @BID, @ReconciliationID
        /// </summary>
        public const string GetHistoricalCashDrawerBalance = @"
                -- Working Variables
                DECLARE @CashDrawerID INT = 1101
                        , @LocationID SMALLINT 
                        , @EndingDT DateTime2(2)
                        , @StartingDT DateTime2(2)
                        , @EndingGLID int 
                        , @StartingGLID int
                ;

                SELECT  @LocationID = LocationID
                        , @EndingDT = AccountingDT
                        , @StartingDT = LastAccountingDT
                        , @EndingGLID = EndingGLID
                        , @StartingGLID = StartingGLID
                FROM [Accounting.Reconciliation.Data] 
                WHERE BID = @BID AND ID = @ReconciliationID
                ;

                -- Retrieve the Data
                SELECT @LocationID as LocationID
                     , IsNull(SUM(CASE WHEN GL.AccountingDT < @StartingDT AND GL.ID < @StartingGLID THEN GL.Amount ELSE 0 END),0) as StartingBalance
                     , @StartingDT as StartingDT
                     , IsNull(SUM(CASE WHEN GL.AccountingDT >= @StartingDT AND GL.AccountingDT < @EndingDT 
                                            AND GL.ID >= @StartingGLID AND GL.ID < @EndingGLID
                                            AND A.GLEntryType = 34 AND GL.Amount > 0 
                                       THEN GL.Amount ELSE 0 END),0) as AdjustmentsIn
                     , IsNull(-SUM(CASE WHEN GL.AccountingDT >= @StartingDT AND GL.AccountingDT < @EndingDT 
                                            AND GL.ID >= @StartingGLID AND GL.ID < @EndingGLID
                                            AND A.GLEntryType = 34 AND GL.Amount < 0 
                                        THEN GL.Amount ELSE 0 END),0) as AdjustmentsOut
                     , IsNull(SUM(CASE WHEN GL.AccountingDT >= @StartingDT AND GL.AccountingDT < @EndingDT 
                                            AND GL.ID >= @StartingGLID AND GL.ID < @EndingGLID
                                            AND A.GLEntryType = 35
                                       THEN GL.Amount ELSE 0 END),0) as DepositTransfer
                     , IsNull(SUM(CASE WHEN GL.AccountingDT < @EndingDT AND GL.ID < @EndingGLID THEN GL.Amount ELSE 0 END),0) as EndingBalance
                     , @EndingDT as EndingDT
                FROM [Accounting.GL.Data] GL 
                JOIN [Activity.GLActivity] A ON GL.BID = A.BID AND GL.ActivityID = A.ID
                WHERE GL.BID = @BID AND GL.LocationID = @LocationID
                  AND GL.GLAccountID = @CashDrawerID;";
    }
}
