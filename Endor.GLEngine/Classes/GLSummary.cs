﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.GLEngine.Classes
{
    class GLSummaryItem
    {
        public short BID { get; set; }
        public byte LocationID { get; set; }
        public int GLAccountID { get; set; }
        public decimal Amount { get; set; }
        public decimal Balance { get; set; }
    }
}
