﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.GLEngine.Classes
{
    /// <summary>
    /// Used for creating adjustments to the Cash Drawer
    /// </summary>
    public class CashDrawerAdjustment
    {
        /// <summary>
        /// The Location this adjustment affects
        /// </summary>
        public byte LocationID { get; set; }
        /// <summary>
        /// A value in AdjustmentsIn debits the Cash Drawer asset account (ID=1101)
        /// and credits the Dash Drawer Adjustment expense (ID=6101).
        /// If existing Adjustment Ins already occur, only the difference is posted.
        /// </summary>
        public decimal AdjustmentsIn { get; set; }
        /// <summary>
        /// Description of the Adjustments In
        /// </summary>
        public string AdjustmentsInDescription { get; set; }
        /// <summary>
        /// A value in AdjustmentsOut credits the Cash Drawer asset account (ID=1101)
        /// and debits the Dash Drawer Adjustment expense (ID=6101).
        /// If existing Adjustment Outs already occur, only the difference is posted.
        /// </summary>
        public decimal AdjustmentsOut { get; set; }
        /// <summary>
        /// Description of the Adjustments Out
        /// </summary>
        public string AdjustmentsOutDescription { get; set; }
        /// <summary>
        /// A value in DepositTransfer credits the Cash Drawer asset account (ID=1101)
        /// and debits the Bank Account asset account (ID=1100).
        /// If existing Adjustment Outs already occur, only the difference is posted.
        /// </summary>
        public decimal DepositTransfer { get; set; }
    }
}
