﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.GLEngine.Classes
{
    /// <summary>
    /// Cash Drawer Balance view model
    /// </summary>
    public class CashDrawerBalance
    {
        /// <summary>
        /// The Cash Drawer is location-specific
        /// </summary>
        public byte LocationID { get; set; }
        /// <summary>
        /// The Starting Cash Balance for that Location
        /// </summary>
        public decimal StartingBalance { get; set; }
        /// <summary>
        /// The DateTime the Starting Balance was computed at
        /// </summary>
        public DateTime StartingDT { get; set; }
        /// <summary>
        /// The total of all increases in the Cash Balance already recorded for that period.
        /// </summary>
        public decimal AdjustmentsIn { get; set; }
        /// <summary>
        /// The total of all decreases in the Cash Balance already recorded for that period.
        /// </summary>
        public decimal AdjustmentsOut { get; set; }
        /// <summary>
        /// The amount from the cash drawer that was deposited.
        /// </summary>
        public decimal DepositTransfer { get; set; }
        /// <summary>
        /// The Ending Cash Balance for that Location
        /// </summary>
        public decimal EndingBalance { get; set; }
        /// <summary>
        /// The DateTime the Ending Balance was computed at
        /// </summary>
        public DateTime EndingDT { get; set; }
    }
}
