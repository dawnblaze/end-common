﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.GLEngine.Classes
{
    class PaymentSummaryItem
    {
        public short BID { get; set; }
        public byte LocationID { get; set; }
        public int PaymentMethod { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public int PaymentMasterCount { get; set; }
        public int PaymentApplicationCount { get; set; }
    }
}
