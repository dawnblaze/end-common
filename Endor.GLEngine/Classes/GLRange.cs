﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.GLEngine.Classes
{
    class GLRange
    {
        public short? BID { get; set; }
        public byte? LocationID { get; set; }
        public int? StartingGLID { get; set; }
        public int? EndingGLID { get; set; }
    }
}
