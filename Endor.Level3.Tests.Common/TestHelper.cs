﻿using Endor.AzureStorage;
using Endor.Tasks;
using Endor.Tenant;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Level3.Tests.Common
{
    public static class TestHelper
    {
        public static async Task<EntityStorageClient> GetEntityStorageClient(short bid)
        {
            return new EntityStorageClient((await new MockTenantDataCache().Get(bid)).StorageConnectionString, bid);
        }

        public class MockTenantDataCache : ITenantDataCache
        {
            static MockTenantDataCache()
            {

            }

            private static Dictionary<string, string> _LocalSettings = null;
            public static string LocalSetting(string key)
            {
                if (_LocalSettings == null)
                {
                    /*
                        Example test-config.json
                        {
                            "BusinessDBConnectionString ="Data Source=.\\SQLEXPRESS;Initial Catalog=\"Dev.Endor.Business.DB1\";User ID=cyrious;Password=watankahani"
                        } 
                    */

                    string file = "..\\..\\..\\..\\test-config.json"; //put this inside end-common folder
                    if (File.Exists(file))
                    {
                        string text = File.ReadAllText(file);
                        _LocalSettings = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);
                    }
                }

                if (_LocalSettings != null)
                {
                    if (_LocalSettings.ContainsKey(key))
                        return _LocalSettings[key];
                }

                return null;
            }

            private static readonly TenantData Mock = new TenantData()
            {
                APIURL = "https://endorapi.localcyriousdevelopment.com:5002/",
                ReportingURL = "",
                MessagingURL = "https://endormessaging.localcyriousdevelopment.com:5004/",
                LoggingURL = "https://endorlog.localcyriousdevelopment.com:5003",
                BackgroundEngineURL = "https://endorbgengine.localcyriousdevelopment.com:5006/",
                //StorageURL = "http://127.0.0.1:10000/devstoreaccount1/",
                StorageConnectionString = LocalSetting("StorageConnectionString"),
                //IndexStorageConnectionString = "UseDevelopmentStorage=true",
                BusinessDBConnectionString = LocalSetting("BusinessDBConnectionString"),
                LoggingDBConnectionString = "",
                MessagingDBConnectionString = "",
                SystemReportBConnectionString = "",
                SystemDataDBConnectionString = "",
            };

            public Task<TenantData> Get(short bid)
            {
                if (bid == 1)
                    return Task.FromResult(Mock);
                else
                    return null;
            }

            public void InvalidateCache()
            {
            }

            public void InvalidateCache(short bid)
            {
            }
        }

        public class MockTaskQueuer : ITaskQueuer
        {
            public string Priority { get; set; }
            public string Schedule { get; set; }

            public MockTaskQueuer()
            {
            }

            public Task<string> EmptyTemp(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> RecomputeGL(short bid, byte type, int Id, string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> EmptyTrash(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> IndexClasstype(short bid, int ctid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> IndexModel(short bid, int ctid, int id)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> Test(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> CheckOrderStatus(short bid, int orderId, int status)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> OrderPricingCompute(short bid, int orderID)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> OrderPricingComputeTax(short bid, int orderID)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> IndexMultipleID(short bid, int ctid, ICollection<int> ids)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> DeleteExpiredDrafts(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> RepairUntrustedConstraint(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> RegenerateCBELForMachine(short bid, int machineId)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> ClearAllAssemblies(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> ClearAssembly(short bid, int ctid, int id)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> CreateDBBackup(short bid, DbType dbType)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> Reconciliate(short bid, byte? locationID, short? enteredById, DateTime accountingDT, bool createAdjustments, IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> cashDrawer)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }
            
            public Task<string> CreateViewCustomField(short bid, int appliesToClassTypeID, string sourceDataTable, string viewBaseName, string viewSchema = "dbo", string IDFieldName = "ID")
            {
                throw new NotImplementedException();
            }
        }
    }
}
