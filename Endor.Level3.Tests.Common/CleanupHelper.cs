﻿using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Level3.Common.Tests
{
    public static class CleanupHelper
    {

        public static async Task DeleteNegativeIDAssemblyAndMachineRelatedData(short BID, ApiContext ctx)
        {
            var _AssemblyVariableFormulas = ctx.AssemblyVariableFormula.Where(t => t.BID == BID && t.ID < 0 || t.VariableID < 0);
            var _AssemblyCategoryLinks = ctx.AssemblyCategoryLink.Where(t => t.CategoryID < 0 || t.PartID < 0);
            var _MachineCategoryLinks = ctx.MachineCategoryLink.Where(t => t.CategoryID < 0 || t.PartID < 0);
            var _AssemblyElements = ctx.AssemblyElement.Where(t => t.BID == BID && t.ID < 0 || t.VariableID < 0 || t.AssemblyID < 0 || t.ParentID < 0);
            var _AssemblyLayouts = ctx.AssemblyLayout.Where(t => t.BID == BID && t.ID < 0 || t.AssemblyID < 0);
            var _AssemblyTables = ctx.AssemblyTable.Where(t => t.BID == BID && t.ID < 0 || t.AssemblyID < 0 || t.ColumnVariableID < 0 || t.RowVariableID < 0);
            var _AssemblyCategories = ctx.AssemblyCategory.Where(t => t.BID == BID && t.ID < 0 || t.ParentID < 0);
            var _MachineCategories = ctx.MachineCategory.Where(t => t.BID == BID && t.ID < 0 || t.ParentID < 0);
            var _AssemblyVariables = ctx.AssemblyVariable.Where(t => t.BID == BID && t.ID < 0 || t.AssemblyID < 0 || t.LinkedAssemblyID < 0 || t.LinkedLaborID < 0 || t.LinkedMachineID < 0 || t.LinkedMaterialID < 0);
            var _AssemblyDatas = ctx.AssemblyData.Where(t => t.BID == BID && t.ID < 0 || t.TaxabilityCodeID < 0 || t.IncomeAccountID < 0);
            var _tables = ctx.MachineProfileTable.Where(t => t.BID == BID && t.ID < 0 || t.ProfileID < 0 || t.TableID < 0);
            var _profiles = ctx.MachineProfile.Where(t => t.BID == BID && t.ID < 0 || t.MachineID < 0 || t.MachineTemplateID < 0);
            var _machines = ctx.MachineData.Where(t => t.BID == BID && t.ID < 0);
            var _machineInstances = ctx.MachineInstance.Where(t => t.BID == BID && t.ID < 0 || t.MachineID < 0 || t.LocationID < 0);

            int[] machineDLLsToDelete = _machines.Select(x => (int)x.ID).ToArray();
            int[] assemblyDLLsToDelete = _AssemblyDatas.Select(x => x.ID).ToArray();

            ctx.RemoveRange(_AssemblyVariableFormulas);
            ctx.RemoveRange(_machineInstances);
            ctx.RemoveRange(_tables);
            ctx.RemoveRange(_AssemblyCategoryLinks);
            ctx.RemoveRange(_MachineCategoryLinks);
            ctx.RemoveRange(_AssemblyElements);
            ctx.SaveChanges();
            ctx.RemoveRange(_AssemblyTables);
            ctx.RemoveRange(_MachineCategories);
            ctx.RemoveRange(_profiles);
            ctx.RemoveRange(_AssemblyCategories);
            ctx.RemoveRange(_AssemblyLayouts);
            ctx.SaveChanges();
            ctx.RemoveRange(_AssemblyVariables);
            ctx.SaveChanges();
            ctx.RemoveRange(_machines);
            ctx.SaveChanges();
            ctx.RemoveRange(_AssemblyDatas);
            ctx.SaveChanges();

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            EntityStorageClient client = new EntityStorageClient((await cache.Get(BID)).StorageConnectionString, BID);

            if (machineDLLsToDelete.Length > 0)
            {
                await DeleteDLLAndCS(client, machineDLLsToDelete, ClassType.Machine, BID);
            }

            if (assemblyDLLsToDelete.Length > 0)
            {
                await DeleteDLLAndCS(client, assemblyDLLsToDelete, ClassType.Assembly, BID);
            }
        }

        private static async Task DeleteDLLAndCS(EntityStorageClient client, IEnumerable<int> ids, ClassType ct, short BID)
        {
            foreach (var id in ids)
            {
                var assemblyDoc = new DMID() { id = id, ctid = ct.ID() };
                string assemblyName = CBELAssemblyHelper.AssemblyName(BID, id, ct.ID(), 1);

                await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
            }
        }
    }
}
