# example usage:
# bash publish-non-dual.sh Endor.Lucene <api-key>
if [[ "$1" != "" ]]
then
    nuget setapikey $2 -Source https://www.myget.org/F/corebridgeshared/api/v2/package;
    nuget setapikey $2 -Source https://www.myget.org/F/corebridgeshared/symbols/api/v2/package;
    nuget pack ./$1/$1.csproj -Symbols -Build -OutputDirectory ./$1/bin/Debug;
    powershell ./pushPackage.ps1 ./$(ls $1/bin/Debug/*[0-9].nupkg -t1 | head -1);
else
    echo "missing argument project name";
fi
