﻿using System;

namespace Endor.Util
{
    public class DateRange : IComparable<DateRange>
    {
        public DateRange() : this(StandardDateRangeType.Today, DateTime.UtcNow) { }

        public DateRange(StandardDateRangeType range, DateTime localTime)
        {
            _LocalDT = localTime;
            _StandardDateRange = range;
            _IsComputed = false;
        }

        public DateRange(DateTime customStartDT, DateTime customEndDT) : this(StandardDateRangeType.Today, DateTime.UtcNow)
        {
            _StartDT = customStartDT;
            _EndDT = customEndDT;
        }

        public int CompareTo(DateRange other)
        {
            // returns 0 as this instance has the same date, time.  Other settings are ignored.
            // returns -Days as this instance is less than other
            // returns -Days as this instance is greater than other
            return ((this.StartDT > other.StartDT || this.EndDT > other.EndDT) ? 1
                     : ((this.StartDT < other.StartDT || this.EndDT < other.EndDT) ? -1
                        : 0));
        }

        protected DateTime _LocalDT;
        protected StandardDateRangeType _StandardDateRange;
        protected DateTime _StartDT;
        protected DateTime _EndDT;
        protected bool _IsComputed;

        public DateTime LocalTime
        {
            get => _LocalDT;
            set
            {
                _IsComputed = false;
                _LocalDT = value;
            }
        }

        public StandardDateRangeType StandardDateRange
        {
            get => _StandardDateRange;
            set
            {
                _IsComputed = false;
                _StandardDateRange = value;
            }
        }


        public DateTime StartDT
        {
            get
            {
                if (!_IsComputed) ComputeDateRange();
                return _StartDT;
            }
            set
            {
                _IsComputed = false;
                _StandardDateRange = StandardDateRangeType.Custom;
                _StartDT = value;
            }
        }

        public DateTime EndDT
        {
            get
            {
                if (!_IsComputed) ComputeDateRange();
                return _EndDT;
            }
            set
            {
                _IsComputed = false;
                _StandardDateRange = StandardDateRangeType.Custom;
                _EndDT = value;
            }
        }

        public virtual void ComputeDateRange()
        {
            // we are going to do this in two passes
            // 1. set the start date. 
            // 2. set the end date

            switch (StandardDateRange)
            {
                case StandardDateRangeType.Today:
                    StartDT = StartOfDay();
                    EndDT = EndOfDay();
                    break;

                case StandardDateRangeType.ThisWeek:
                    StartDT = StartOfWeek();
                    EndDT = EndOfWeek();
                    break;

                case StandardDateRangeType.ThisMonth:
                    StartDT = StartOfMonth();
                    EndDT = EndOfMonth();
                    break;

                case StandardDateRangeType.ThisQuarter:
                    StartDT = StartOfQuarter();
                    EndDT = EndOfQuarter();
                    break;

                case StandardDateRangeType.ThisYear:
                    StartDT = StartOfYear();
                    EndDT = EndOfYear();
                    break;

                case StandardDateRangeType.TodayToNow:
                    StartDT = StartOfDay();
                    EndDT = CurrentTime();
                    break;

                case StandardDateRangeType.ThisWeekToDate:
                    StartDT = StartOfWeek();
                    EndDT = CurrentTime();
                    break;

                case StandardDateRangeType.ThisMonthToDate:
                    StartDT = StartOfMonth();
                    EndDT = CurrentTime();
                    break;

                case StandardDateRangeType.ThisQuarterToDate:
                    StartDT = StartOfQuarter();
                    EndDT = CurrentTime();
                    break;

                case StandardDateRangeType.ThisYearToDate:
                    StartDT = StartOfYear();
                    EndDT = CurrentTime();
                    break;

                case StandardDateRangeType.ThisWeekFromDate:
                    StartDT = CurrentTime();
                    EndDT = EndOfWeek();
                    break;

                case StandardDateRangeType.ThisMonthFromDate:
                    StartDT = CurrentTime();
                    EndDT = EndOfMonth();
                    break;

                case StandardDateRangeType.ThisQuarterFromDate:
                    StartDT = CurrentTime();
                    EndDT = EndOfQuarter();
                    break;

                case StandardDateRangeType.ThisYearFromDate:
                    StartDT = CurrentTime();
                    EndDT = EndOfYear();
                    break;

                case StandardDateRangeType.Tomorrow:
                    StartDT = StartOfDay().AddDays(1);
                    EndDT = EndOfDay().AddDays(1);
                    break;

                case StandardDateRangeType.NextWeek:
                    StartDT = StartOfWeek().AddDays(7);
                    EndDT = EndOfWeek().AddDays(7);
                    break;

                case StandardDateRangeType.NextMonth:
                    StartDT = StartOfMonth().AddMonths(1);
                    EndDT = EndOfMonth().AddMonths(1);
                    break;

                case StandardDateRangeType.NextQuarter:
                    StartDT = StartOfQuarter().AddMonths(3);
                    EndDT = EndOfQuarter().AddMonths(3);
                    break;

                case StandardDateRangeType.NextYear:
                    StartDT = StartOfYear().AddYears(1);
                    EndDT = EndOfYear().AddYears(1);
                    break;

                case StandardDateRangeType.Next30Days:
                    StartDT = StartOfDay().AddDays(1);
                    EndDT = EndOfDay().AddDays(30);
                    break;

                case StandardDateRangeType.Next60Days:
                    StartDT = StartOfDay().AddDays(1);
                    EndDT = EndOfDay().AddDays(60);
                    break;

                case StandardDateRangeType.Next90Days:
                    StartDT = StartOfDay().AddDays(1);
                    EndDT = EndOfDay().AddDays(90);
                    break;

                case StandardDateRangeType.Next120Days:
                    StartDT = StartOfDay().AddDays(1);
                    EndDT = EndOfDay().AddDays(120);
                    break;

                case StandardDateRangeType.Yesterday:
                    StartDT = StartOfDay().AddDays(-1);
                    EndDT = EndOfDay().AddDays(-1);
                    break;

                case StandardDateRangeType.LastWeek:
                    StartDT = StartOfWeek().AddDays(-7);
                    EndDT = EndOfWeek().AddDays(-7);
                    break;

                case StandardDateRangeType.LastMonth:
                    StartDT = StartOfMonth().AddMonths(-1);
                    EndDT = EndOfMonth().AddMonths(-1);
                    break;

                case StandardDateRangeType.LastQuarter:
                    StartDT = StartOfMonth().AddMonths(-3);
                    EndDT = EndOfMonth().AddMonths(-3);
                    break;

                case StandardDateRangeType.LastYear:
                    StartDT = StartOfYear().AddYears(-1);
                    EndDT = EndOfYear().AddYears(-1);
                    break;

                case StandardDateRangeType.PriorWeekToDate:
                    StartDT = StartOfWeek().AddDays(-7);
                    EndDT = CurrentTime().AddDays(-7);
                    break;

                case StandardDateRangeType.PriorMonthToDate:
                    StartDT = StartOfMonth().AddMonths(-1);
                    EndDT = CurrentTime().AddMonths(-1);
                    break;

                case StandardDateRangeType.PriorQuarterToDate:
                    StartDT = StartOfQuarter().AddMonths(-3);
                    EndDT = CurrentTime().AddMonths(-3);
                    break;

                case StandardDateRangeType.PriorYearToDate:
                    StartDT = StartOfYear().AddYears(-1);
                    EndDT = CurrentTime().AddYears(-1);
                    break;

                case StandardDateRangeType.Last30Days:
                    StartDT = StartOfDay().AddDays(-29);
                    EndDT = EndOfDay();
                    break;

                case StandardDateRangeType.Last60Days:
                    StartDT = StartOfDay().AddDays(-59);
                    EndDT = EndOfDay();
                    break;

                case StandardDateRangeType.Last90Days:
                    StartDT = StartOfDay().AddDays(-89);
                    EndDT = EndOfDay();
                    break;

                case StandardDateRangeType.Last120Days:
                    StartDT = StartOfDay().AddDays(-119);
                    EndDT = EndOfDay();
                    break;

                case StandardDateRangeType.UpToNow:
                    StartDT = StartOfTime();
                    EndDT = CurrentTime();
                    break;

                case StandardDateRangeType.FromNowOn:
                    StartDT = StartOfDay();
                    EndDT = CurrentTime();
                    break;

                case StandardDateRangeType.BeforeToday:
                    StartDT = StartOfTime();
                    EndDT = EndOfDay().AddDays(-1);
                    break;

                case StandardDateRangeType.AfterToday:
                    StartDT = StartOfDay().AddDays(1);
                    EndDT = EndOfTime();
                    break;

                case StandardDateRangeType.Never:
                    StartDT = EndOfTime();
                    EndDT = EndOfTime();
                    break;

                case StandardDateRangeType.AllTime:
                    StartDT = StartOfTime();
                    EndDT = EndOfTime();
                    break;

                case StandardDateRangeType.Custom:
                    // nothign to do here because they should already have been set.
                    break;
            }

            _IsComputed = true;
        }



        protected DateTime CurrentTime()
        {
            return LocalTime;
        }

        protected DateTime StartOfYear()
        {
            return new DateTime(LocalTime.Year, 01, 01);
        }

        protected DateTime EndOfYear()
        {
            return new DateTime(LocalTime.Year, 12, 31, 23, 59, 59);
        }

        protected DateTime StartOfQuarter()
        {
            int month = LocalTime.Month;
            month = month - ((month - 1) % 3);
            return new DateTime(LocalTime.Year, month, 01);
        }

        protected DateTime EndOfQuarter()
        {
            int month = LocalTime.Month;
            month = month - ((month - 1) % 3) + 3;
            return new DateTime(LocalTime.Year, month, 01).AddSeconds(-1);
        }

        protected DateTime EndOfMonth()
        {
            return new DateTime(LocalTime.Year, LocalTime.Month, 01).AddMonths(1).AddSeconds(-1);
        }

        protected DateTime StartOfMonth()
        {
            return new DateTime(LocalTime.Year, LocalTime.Month, 01);
        }

        protected DateTime StartOfDay()
        {
            return LocalTime.Date;
        }

        protected DateTime EndOfWeek()
        {
            return StartOfWeek().AddDays(7).AddSeconds(-1);
        }

        protected DateTime StartOfWeek()
        {
            // we have to think about when the week starts --- different for different countries
            throw new NotImplementedException();
        }

        protected DateTime EndOfDay()
        {
            return LocalTime.Date.AddSeconds(24 * 60 * 60 - 1);
        }

        public static DateTime EndOfTime() => new DateTime(2180, 01, 01);

        public static DateTime StartOfTime() => new DateTime(1966, 04, 20);

    }
}
