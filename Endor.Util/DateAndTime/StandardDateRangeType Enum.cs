﻿namespace Endor.Util
{
    public enum StandardDateRangeType
    {
        // dates for this period = 0-49
        [DisplayTextAttribute("Today")]
        Today = 0,
        [DisplayTextAttribute("This Week")]
        ThisWeek = 1,
        [DisplayTextAttribute("This Month")]
        ThisMonth = 2,
        [DisplayTextAttribute("This Quarter")]
        ThisQuarter = 3,
        [DisplayTextAttribute("This Year")]
        ThisYear = 4,

        [DisplayTextAttribute("Today To Now")]
        TodayToNow = 10,
        [DisplayTextAttribute("This Week To Date")]
        ThisWeekToDate = 11,
        [DisplayTextAttribute("This Month To Date")]
        ThisMonthToDate = 12,
        [DisplayTextAttribute("This Quarter To Date")]
        ThisQuarterToDate = 13,
        [DisplayTextAttribute("This Year To Date")]
        ThisYearToDate = 14,

        [DisplayTextAttribute("This Week From Date")]
        ThisWeekFromDate = 21,
        [DisplayTextAttribute("This Month From Date")]
        ThisMonthFromDate = 22,
        [DisplayTextAttribute("This Quarter From Date")]
        ThisQuarterFromDate = 23,
        [DisplayTextAttribute("This Year From Date")]
        ThisYearFromDate = 24,

        // dates for the next period = 50-99
        [DisplayTextAttribute("Tomorrow")]
        Tomorrow = 50,
        [DisplayTextAttribute("Next Week")]
        NextWeek = 51,
        [DisplayTextAttribute("Next Month")]
        NextMonth = 52,
        [DisplayTextAttribute("Next Quarter")]
        NextQuarter = 53,
        [DisplayTextAttribute("Next Year")]
        NextYear = 54,

        [DisplayTextAttribute("Next 30 Days")]
        Next30Days = 71,
        [DisplayTextAttribute("Next 60 Days")]
        Next60Days = 72,
        [DisplayTextAttribute("Next 90 Days")]
        Next90Days = 73,
        [DisplayTextAttribute("Next 120 Days")]
        Next120Days = 74,

        // dates for the prior period = 100-149
        [DisplayTextAttribute("Yesterday")]
        Yesterday = 100,
        [DisplayTextAttribute("Last Week")]
        LastWeek = 101,
        [DisplayTextAttribute("Last Month")]
        LastMonth = 102,
        [DisplayTextAttribute("Last Quarter")]
        LastQuarter = 103,
        [DisplayTextAttribute("Last Year")]
        LastYear = 104,

        [DisplayTextAttribute("Prior Week To Date")]
        PriorWeekToDate = 111,
        [DisplayTextAttribute("Prior Month To Date")]
        PriorMonthToDate = 112,
        [DisplayTextAttribute("Prior Quarter To Date")]
        PriorQuarterToDate = 113,
        [DisplayTextAttribute("Prior Year To Date")]
        PriorYearToDate = 114,

        [DisplayTextAttribute("Last 30 Days")]
        Last30Days = 121,
        [DisplayTextAttribute("Last 60 Days")]
        Last60Days = 122,
        [DisplayTextAttribute("Last 90 Days")]
        Last90Days = 123,
        [DisplayTextAttribute("Last 120 Days")]
        Last120Days = 124,

        // Special Ranges = 200-255
        [DisplayTextAttribute("Up To Now")]
        UpToNow = 200,
        [DisplayTextAttribute("From Now On")]
        FromNowOn = 220,
        [DisplayTextAttribute("Before Today")]
        BeforeToday = 240,
        [DisplayTextAttribute("After Today")]
        AfterToday = 250,

        [DisplayTextAttribute("Never")]
        Never = 252,
        [DisplayTextAttribute("All Time")]
        AllTime = 253,

        [DisplayTextAttribute("Custom")]
        Custom = 255
    }
}
