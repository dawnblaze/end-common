﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TimeZoneConverter;

namespace Endor.Util
{

    public static class TimeZoneUtils
    {
        public static string IDToName(int ID) => TimeZoneList[ID];

        public static int NametoID(string Name) => TimeZoneList.FirstOrDefault(t => t.Value == Name).Key;

        public static TimeZoneInfo IDToTimeZoneInfo(int ID) => TZConvert.GetTimeZoneInfo(TimeZoneList[ID]);

        public static TimeZoneInfo NametoTimeZoneInfo(string Name) => TZConvert.GetTimeZoneInfo(Name);

        private static Dictionary<int, string> _TimeZoneList;

        public static Dictionary<int, string> TimeZoneList
        {
            get
            {
                if (_TimeZoneList == null)
                {
                    _TimeZoneList = new Dictionary<int, string>();
                    foreach (EndorTimeZone tz in (EndorTimeZone[])Enum.GetValues(typeof(EndorTimeZone)))
                    {
                        _TimeZoneList.Add((int)tz, tz.WindowsId());
                    }
                }
                return _TimeZoneList;
            }
        }

    }
}
