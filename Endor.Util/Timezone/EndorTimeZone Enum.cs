﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Util
{
    public enum EndorTimeZone
    {
        //[TimeZoneAttribute("TimeZoneID", "DisplayName", "StandardName", "DaylightName", UTCOffset)]
        //EnumName = EndorID,

        [TimeZoneAttribute("Dateline Standard Time", "(UTC-12:00) International Date Line West", "Dateline Standard Time", "Dateline Daylight Time", -12)]
        DatelineStandardTime = 0,

        [TimeZoneAttribute("Samoa Standard Time", "(UTC+13:00) Samoa", "Samoa Standard Time", "Samoa Daylight Time", 13)]
        SamoaStandardTime = 1,

        [TimeZoneAttribute("Hawaiian Standard Time", "(UTC-10:00) Hawaii", "Hawaiian Standard Time", "Hawaiian Daylight Time", -10)]
        HawaiianStandardTime = 2,

        [TimeZoneAttribute("Alaskan Standard Time", "(UTC-09:00) Alaska", "Alaskan Standard Time", "Alaskan Daylight Time", -9)]
        AlaskanStandardTime = 3,

        [TimeZoneAttribute("Pacific Standard Time", "(UTC-08:00) Pacific Time (US & Canada)", "Pacific Standard Time", "Pacific Daylight Time", -8)]
        PacificStandardTime = 4,

        [TimeZoneAttribute("Mountain Standard Time", "(UTC-07:00) Mountain Time (US & Canada)", "Mountain Standard Time", "Mountain Daylight Time", -7)]
        MountainStandardTime = 10,

        [TimeZoneAttribute("US Mountain Standard Time", "(UTC-07:00) Arizona", "US Mountain Standard Time", "US Mountain Daylight Time", -7)]
        USMountainStandardTime = 15,

        [TimeZoneAttribute("Central Standard Time", "(UTC-06:00) Central Time (US & Canada)", "Central Standard Time", "Central Daylight Time", -6)]
        CentralStandardTime = 20,

        [TimeZoneAttribute("Canada Central Standard Time", "(UTC-06:00) Saskatchewan", "Canada Central Standard Time", "Canada Central Daylight Time", -6)]
        CanadaCentralStandardTime = 25,

        [TimeZoneAttribute("Central America Standard Time", "(UTC-06:00) Central America", "Central America Standard Time", "Central America Daylight Time", -6)]
        CentralAmericaStandardTime = 33,

        [TimeZoneAttribute("Eastern Standard Time", "(UTC-05:00) Eastern Time (US & Canada)", "Eastern Standard Time", "Eastern Daylight Time", -5)]
        EasternStandardTime = 35,

        [TimeZoneAttribute("US Eastern Standard Time", "(UTC-05:00) Indiana (East)", "US Eastern Standard Time", "US Eastern Daylight Time", -5)]
        USEasternStandardTime = 40,

        [TimeZoneAttribute("SA Pacific Standard Time", "(UTC-05:00) Bogota, Lima, Quito, Rio Branco", "SA Pacific Standard Time", "SA Pacific Daylight Time", -5)]
        SAPacificStandardTime = 45,

        [TimeZoneAttribute("Atlantic Standard Time", "(UTC-04:00) Atlantic Time (Canada)", "Atlantic Standard Time", "Atlantic Daylight Time", -4)]
        AtlanticStandardTime = 50,

        [TimeZoneAttribute("SA Western Standard Time", "(UTC-04:00) Georgetown, La Paz, Manaus, San Juan", "SA Western Standard Time", "SA Western Daylight Time", -4)]
        SAWesternStandardTime = 55,

        [TimeZoneAttribute("Newfoundland Standard Time", "(UTC-03:30) Newfoundland", "Newfoundland Standard Time", "Newfoundland Daylight Time", -3.5)]
        NewfoundlandandLabradorStandardTime = 60,

        [TimeZoneAttribute("E. South America Standard Time", "(UTC-03:00) Brasilia", "E. South America Standard Time", "E. South America Daylight Time", -3)]
        ESouthAmericaStandardTime = 65,

        [TimeZoneAttribute("SA Eastern Standard Time", "(UTC-03:00) Cayenne, Fortaleza", "SA Eastern Standard Time", "SA Eastern Daylight Time", -3)]
        SAEasternStandardTime = 70,

        [TimeZoneAttribute("Greenland Standard Time", "(UTC-03:00) Greenland", "Greenland Standard Time", "Greenland Daylight Time", -3)]
        GreenlandStandardTime = 73,

        [TimeZoneAttribute("Mid-Atlantic Standard Time", "(UTC-02:00) Mid-Atlantic - Old", "Mid-Atlantic Standard Time", "Mid-Atlantic Daylight Time", -2)]
        MidAtlanticStandardTime = 75,

        [TimeZoneAttribute("Azores Standard Time", "(UTC-01:00) Azores", "Azores Standard Time", "Azores Daylight Time", -1)]
        AzoresStandardTime = 80,

        [TimeZoneAttribute("Cape Verde Standard Time", "(UTC-01:00) Cabo Verde Is.", "Cabo Verde Standard Time", "Cabo Verde Daylight Time", -1)]
        CapeVerdeStandardTime = 83,

        [TimeZoneAttribute("GMT Standard Time", "(UTC+00:00) Dublin, Edinburgh, Lisbon, London", "GMT Standard Time", "GMT Daylight Time", 0)]
        GMTStandardTime = 85,

        [TimeZoneAttribute("Greenwich Standard Time", "(UTC+00:00) Monrovia, Reykjavik", "Greenwich Standard Time", "Greenwich Daylight Time", 0)]
        GreenwichStandardTime = 90,

        [TimeZoneAttribute("Central Europe Standard Time", "(UTC+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague", "Central Europe Standard Time", "Central Europe Daylight Time", 1)]
        CentralEuropeStandardTime = 95,

        [TimeZoneAttribute("Central European Standard Time", "(UTC+01:00) Sarajevo, Skopje, Warsaw, Zagreb", "Central European Standard Time", "Central European Daylight Time", 1)]
        CentralEuropeanStandardTime = 100,

        [TimeZoneAttribute("Romance Standard Time", "(UTC+01:00) Brussels, Copenhagen, Madrid, Paris", "Romance Standard Time", "Romance Daylight Time", 1)]
        RomanceStandardTime = 105,

        [TimeZoneAttribute("W. Europe Standard Time", "(UTC+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna", "W. Europe Standard Time", "W. Europe Daylight Time", 1)]
        WEuropeStandardTime = 110,

        [TimeZoneAttribute("W. Central Africa Standard Time", "(UTC+01:00) West Central Africa", "W. Central Africa Standard Time", "W. Central Africa Daylight Time", 1)]
        WCentralAfricaStandardTime = 113,

        [TimeZoneAttribute("E. Europe Standard Time", "(UTC+02:00) Chisinau", "E. Europe Standard Time", "E. Europe Daylight Time", 2)]
        EEuropeStandardTime = 115,

        [TimeZoneAttribute("Egypt Standard Time", "(UTC+02:00) Cairo", "Egypt Standard Time", "Egypt Daylight Time", 2)]
        EgyptStandardTime = 120,

        [TimeZoneAttribute("FLE Standard Time", "(UTC+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius", "FLE Standard Time", "FLE Daylight Time", 2)]
        FLEStandardTime = 125,

        [TimeZoneAttribute("GTB Standard Time", "(UTC+02:00) Athens, Bucharest", "GTB Standard Time", "GTB Daylight Time", 2)]
        GTBStandardTime = 130,

        [TimeZoneAttribute("Israel Standard Time", "(UTC+02:00) Jerusalem", "Jerusalem Standard Time", "Jerusalem Daylight Time", 2)]
        IsraelStandardTime = 135,

        [TimeZoneAttribute("South Africa Standard Time", "(UTC+02:00) Harare, Pretoria", "South Africa Standard Time", "South Africa Daylight Time", 2)]
        SouthAfricaStandardTime = 140,

        [TimeZoneAttribute("Russian Standard Time", "(UTC+03:00) Moscow, St. Petersburg", "Russia TZ 2 Standard Time", "Russia TZ 2 Daylight Time", 3)]
        RussianStandardTime = 145,

        [TimeZoneAttribute("Arab Standard Time", "(UTC+03:00) Kuwait, Riyadh", "Arab Standard Time", "Arab Daylight Time", 3)]
        ArabStandardTime = 150,

        [TimeZoneAttribute("E. Africa Standard Time", "(UTC+03:00) Nairobi", "E. Africa Standard Time", "E. Africa Daylight Time", 3)]
        EAfricaStandardTime = 155,

        [TimeZoneAttribute("Arabic Standard Time", "(UTC+03:00) Baghdad", "Arabic Standard Time", "Arabic Daylight Time", 3)]
        ArabicStandardTime = 158,

        [TimeZoneAttribute("Iran Standard Time", "(UTC+03:30) Tehran", "Iran Standard Time", "Iran Daylight Time", 3.5)]
        IranStandardTime = 160,

        [TimeZoneAttribute("Arabian Standard Time", "(UTC+04:00) Abu Dhabi, Muscat", "Arabian Standard Time", "Arabian Daylight Time", 4)]
        ArabianStandardTime = 165,

        [TimeZoneAttribute("Caucasus Standard Time", "(UTC+04:00) Yerevan", "Caucasus Standard Time", "Caucasus Daylight Time", 4)]
        CaucasusStandardTime = 170,

        [TimeZoneAttribute("Afghanistan Standard Time", "(UTC+04:30) Kabul", "Afghanistan Standard Time", "Afghanistan Daylight Time", 4.5)]
        TransitionalIslamicStateofAfghanistanStandardTime = 175,

        [TimeZoneAttribute("Ekaterinburg Standard Time", "(UTC+05:00) Ekaterinburg", "Russia TZ 4 Standard Time", "Russia TZ 4 Daylight Time", 5)]
        EkaterinburgStandardTime = 180,

        [TimeZoneAttribute("West Asia Standard Time", "(UTC+05:00) Ashgabat, Tashkent", "West Asia Standard Time", "West Asia Daylight Time", 5)]
        WestAsiaStandardTime = 185,

        [TimeZoneAttribute("India Standard Time", "(UTC+05:30) Chennai, Kolkata, Mumbai, New Delhi", "India Standard Time", "India Daylight Time", 5.5)]
        IndiaStandardTime = 190,

        [TimeZoneAttribute("Nepal Standard Time", "(UTC+05:45) Kathmandu", "Nepal Standard Time", "Nepal Daylight Time", 5.75)]
        NepalStandardTime = 193,

        [TimeZoneAttribute("Central Asia Standard Time", "(UTC+06:00) Astana", "Central Asia Standard Time", "Central Asia Daylight Time", 6)]
        CentralAsiaStandardTime = 195,

        [TimeZoneAttribute("Sri Lanka Standard Time", "(UTC+05:30) Sri Jayawardenepura", "Sri Lanka Standard Time", "Sri Lanka Daylight Time", 5.5)]
        SriLankaStandardTime = 200,

        [TimeZoneAttribute("N. Central Asia Standard Time", "(UTC+07:00) Novosibirsk", "Novosibirsk Standard Time", "Novosibirsk Daylight Time", 7)]
        NCentralAsiaStandardTime = 201,

        [TimeZoneAttribute("Myanmar Standard Time", "(UTC+06:30) Yangon (Rangoon)", "Myanmar Standard Time", "Myanmar Daylight Time", 6.5)]
        MyanmarStandardTime = 203,

        [TimeZoneAttribute("SE Asia Standard Time", "(UTC+07:00) Bangkok, Hanoi, Jakarta", "SE Asia Standard Time", "SE Asia Daylight Time", 7)]
        SEAsiaStandardTime = 205,

        [TimeZoneAttribute("North Asia Standard Time", "(UTC+07:00) Krasnoyarsk", "Russia TZ 6 Standard Time", "Russia TZ 6 Daylight Time", 7)]
        NorthAsiaStandardTime = 207,

        [TimeZoneAttribute("China Standard Time", "(UTC+08:00) Beijing, Chongqing, Hong Kong, Urumqi", "China Standard Time", "China Daylight Time", 8)]
        ChinaStandardTime = 210,

        [TimeZoneAttribute("Singapore Standard Time", "(UTC+08:00) Kuala Lumpur, Singapore", "Malay Peninsula Standard Time", "Malay Peninsula Daylight Time", 8)]
        SingaporeStandardTime = 215,

        [TimeZoneAttribute("Taipei Standard Time", "(UTC+08:00) Taipei", "Taipei Standard Time", "Taipei Daylight Time", 8)]
        TaipeiStandardTime = 220,

        [TimeZoneAttribute("Aus Central W. Standard Time", "(UTC+08:45) Eucla", "Aus Central W. Standard Time", "Aus Central W. Daylight Time", 8.75)]
        WAustraliaStandardTime = 225,

        [TimeZoneAttribute("North Asia East Standard Time", "(UTC+08:00) Irkutsk", "Russia TZ 7 Standard Time", "Russia TZ 7 Daylight Time", 8)]
        NorthAsiaEastStandardTime = 227,

        [TimeZoneAttribute("Korea Standard Time", "(UTC+09:00) Seoul", "Korea Standard Time", "Korea Daylight Time", 9)]
        KoreaStandardTime = 230,

        [TimeZoneAttribute("Tokyo Standard Time", "(UTC+09:00) Osaka, Sapporo, Tokyo", "Tokyo Standard Time", "Tokyo Daylight Time", 9)]
        TokyoStandardTime = 235,

        [TimeZoneAttribute("Yakutsk Standard Time", "(UTC+09:00) Yakutsk", "Russia TZ 8 Standard Time", "Russia TZ 8 Daylight Time", 9)]
        YakutskStandardTime = 240,

        [TimeZoneAttribute("AUS Central Standard Time", "(UTC+09:30) Darwin", "AUS Central Standard Time", "AUS Central Daylight Time", 9.5)]
        CenAustraliaStandardTime = 245,

        [TimeZoneAttribute("AUS Eastern Standard Time", "(UTC+10:00) Canberra, Melbourne, Sydney", "AUS Eastern Standard Time", "AUS Eastern Daylight Time", 10)]
        EAustraliaStandardTime = 260,

        [TimeZoneAttribute("Tasmania Standard Time", "(UTC+10:00) Hobart", "Tasmania Standard Time", "Tasmania Daylight Time", 10)]
        TasmaniaStandardTime = 265,

        [TimeZoneAttribute("Vladivostok Standard Time", "(UTC+10:00) Vladivostok", "Russia TZ 9 Standard Time", "Russia TZ 9 Daylight Time", 10)]
        VladivostokStandardTime = 270,

        [TimeZoneAttribute("West Pacific Standard Time", "(UTC+10:00) Guam, Port Moresby", "West Pacific Standard Time", "West Pacific Daylight Time", 10)]
        WestPacificStandardTime = 275,

        [TimeZoneAttribute("Central Pacific Standard Time", "(UTC+11:00) Solomon Is., New Caledonia", "Central Pacific Standard Time", "Central Pacific Daylight Time", 11)]
        CentralPacificStandardTime = 280,

        [TimeZoneAttribute("Fiji Standard Time", "(UTC+12:00) Fiji", "Fiji Standard Time", "Fiji Daylight Time", 12)]
        FijiIslandsStandardTime = 285,

        [TimeZoneAttribute("New Zealand Standard Time", "(UTC+12:00) Auckland, Wellington", "New Zealand Standard Time", "New Zealand Daylight Time", 12)]
        NewZealandStandardTime = 290,

        [TimeZoneAttribute("Tonga Standard Time", "(UTC+13:00) Nuku'alofa", "Tonga Standard Time", "Tonga Daylight Time", 13)]
        TongaStandardTime = 300
    }
}
