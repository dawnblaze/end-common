﻿using System;

namespace Endor.Util
{
    public class DateRangeTZ : DateRange, IComparable<DateRangeTZ>
    {
        public DateRangeTZ() 
            : this(StandardDateRangeType.Today, DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time"))
        {
            _IsComputed = false;
        }

        public DateRangeTZ(StandardDateRangeType range, DateTime localTime, string LocalTimeZoneName)
            : this(range, localTime, TimeZoneUtils.NametoTimeZoneInfo(LocalTimeZoneName)) { }

        public DateRangeTZ(StandardDateRangeType range, DateTime localTime, int LocalTimeZoneID)
            : this(range, localTime, TimeZoneUtils.IDToTimeZoneInfo(LocalTimeZoneID)) { }

        public DateRangeTZ(StandardDateRangeType range, DateTime localTime, TimeZoneInfo LocalTimeZone) : base(range, localTime)
        {
            _LocalTimeZone = LocalTimeZone;
            _TargetTimeZone = TimeZoneInfo.Utc;
            _IsComputed = false;
        }

        public DateRangeTZ(DateTime customStartDT, DateTime customEndDT, string LocalTimeZoneName)
            : this(customStartDT, customEndDT, TimeZoneUtils.NametoTimeZoneInfo(LocalTimeZoneName)) { }

        public DateRangeTZ(DateTime customStartDT, DateTime customEndDT, int LocalTimeZoneID )
            : this(customStartDT, customEndDT, TimeZoneUtils.IDToTimeZoneInfo(LocalTimeZoneID)) { }

        public DateRangeTZ(DateTime customStartDT, DateTime customEndDT, TimeZoneInfo LocalTimeZone) : base(customStartDT, customEndDT)
        {
            _LocalTimeZone = LocalTimeZone;
            _TargetTimeZone = TimeZoneInfo.Utc;
            _IsComputed = false;
        }

        public int CompareTo(DateRangeTZ other)
        {
            // returns 0 as this instance has the same date, time.  Other settings are ignored.
            // returns -Days as this instance is less than other
            // returns -Days as this instance is greater than other
            return ((this.StartDT > other.StartDT || this.EndDT > other.EndDT) ? 1
                     : ((this.StartDT < other.StartDT || this.EndDT < other.EndDT) ? -1
                        : 0));
        }

        private TimeZoneInfo _LocalTimeZone;

        public TimeZoneInfo LocalTimeZone
        {
            get => _LocalTimeZone;
            set
            {
                _LocalTimeZone = value;
                _IsComputed = false;
            }
        }

        private TimeZoneInfo _TargetTimeZone;

        public TimeZoneInfo TargetTimeZone
        {
            get => _TargetTimeZone;
            set
            {
                _TargetTimeZone = value;
                _IsComputed = false;
            }
        }

        public override void ComputeDateRange()
        {
            // run the basic conversion
            base.ComputeDateRange();

            // set the kind to 'unspecified' so that it can be converted to the target time zone without errors
            _StartDT = DateTime.SpecifyKind(_StartDT, DateTimeKind.Unspecified);
            _EndDT = DateTime.SpecifyKind(_EndDT, DateTimeKind.Unspecified);


            // now translate the time from the local timezone to UTC
            _StartDT = TimeZoneInfo.ConvertTime(_StartDT, LocalTimeZone, TargetTimeZone);
            _EndDT = TimeZoneInfo.ConvertTime(_EndDT, LocalTimeZone, TargetTimeZone);

            //_StartDT = TimeZoneInfo.ConvertTime(_StartDT, LocalTimeZone);
            //_EndDT = TimeZoneInfo.ConvertTime(_EndDT, LocalTimeZone);
        }


    }
}
