﻿using System;
using System.Reflection;

namespace Endor.Util
{
    /// <summary>
    /// Attribute class for tagging Enums with Display Text
    /// </summary>
    public class DisplayTextAttribute : Attribute
    {
        public string DisplayText;

        public DisplayTextAttribute(string displayText)
        {
            DisplayText = displayText;
        }
    }

    /// <summary>
    /// Attribute class for tagging Enums with Display Text
    /// </summary>
    public class TimeZoneAttribute : Attribute
    {
        public string WindowsId; 
        public string DisplayText;
        public string StandardName;
        public string DaylightName;
        public double UTCOffset;

        public TimeZoneAttribute(string windowsId, string displayName, string standardName, string daylightName, double uTCOffset  )
        {
            WindowsId = windowsId;
            DisplayText = displayName;
            StandardName = standardName;
            DaylightName = daylightName;
            UTCOffset = uTCOffset;
        }
    }

    /// <summary>
    /// Class of Enum Helpers
    /// </summary>
    public static class EnumHelpers
    {
        /// <summary>
        /// Returns the attribute [WindowsID] on an enum if it exists, 
        /// else it returns the default enum.ToString() value.
        /// </summary>
        /// <param name="en"></param>
        /// <returns></returns>
        public static string WindowsId(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(TimeZoneAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((TimeZoneAttribute)attrs[0]).WindowsId;
            }

            // if not found, return the default
            return en.ToString();
        }

        /// <summary>
        /// Returns the attribute [FriendlyText] on an enum if it exists, 
        /// else it returns the default enum.ToString() value.
        /// </summary>
        /// <param name="en"></param>
        /// <returns></returns>
        public static string DisplayText(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(TimeZoneAttribute), false);
                if (attrs != null && attrs.Length > 0)
                {
                    return ((TimeZoneAttribute)attrs[0]).DisplayText;
                }
                else
                {
                    attrs = memInfo[0].GetCustomAttributes(typeof(DisplayTextAttribute), false);
                    if (attrs != null && attrs.Length > 0)
                        return ((DisplayTextAttribute)attrs[0]).DisplayText;
                }
            }

            // if not found, return the default
            return en.ToString();
        }

        /// <summary>
        /// Returns the attribute [StandardName] on an enum if it exists, 
        /// else it returns the default enum.ToString() value.
        /// </summary>
        /// <param name="en"></param>
        /// <returns></returns>
        public static string StandardName(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(TimeZoneAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((TimeZoneAttribute)attrs[0]).StandardName;
            }

            // if not found, return the default
            return en.ToString();
        }

        /// <summary>
        /// Returns the attribute [DaylightName] on an enum if it exists, 
        /// else it returns the default enum.ToString() value.
        /// </summary>
        /// <param name="en"></param>
        /// <returns></returns>
        public static string DaylightName(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(TimeZoneAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((TimeZoneAttribute)attrs[0]).DaylightName;
            }

            // if not found, return the default
            return en.ToString();
        }

        /// <summary>
        /// Returns the attribute [UTCOffset] on an enum if it exists, 
        /// else it returns the default enum.ToString() value.
        /// </summary>
        /// <param name="en"></param>
        /// <returns></returns>
        public static double UTCOffset(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(TimeZoneAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((TimeZoneAttribute)attrs[0]).UTCOffset;
            }

            // if not found, return the default
            double defaultValue = 0;
            double.TryParse(en.ToString(), out defaultValue);
            return defaultValue;
        }

    }


}
