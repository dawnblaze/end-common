using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.Units;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using DeepEqual.Syntax;
using DeepEqual.Formatting;

namespace Endor.Units.Tests
{
    [TestClass]
    public class TestUnits
    {
        [TestMethod]
        public void UnitInfoCounts()
        {
            int SortedTotalUnits = UnitInfo.InfoListSorted.Count;
            Assert.AreEqual(116, SortedTotalUnits);

            int TotalUnits = UnitInfo.InfoList.Count;
            Assert.AreEqual(116, TotalUnits);

            int MetricUnits = UnitInfo.InfoListBySystem(SystemOfUnits.Metric).Count;
            Assert.AreEqual(64, MetricUnits);

            int ImperialUnits = UnitInfo.InfoListBySystem(SystemOfUnits.Imperial).Count;
            Assert.AreEqual(74, ImperialUnits);

            int MetricAndImperialUnits = UnitInfo.InfoListBySystem(SystemOfUnits.Imperial | SystemOfUnits.Metric).Count;
            Assert.AreEqual(116, MetricAndImperialUnits);

            int MetricLengthUnits = UnitInfo.InfoListByUnitType(UnitType.Length, SystemOfUnits.Metric).Count;
            Assert.AreEqual(3, MetricLengthUnits);

            int ImperialAreaUnits = UnitInfo.InfoListByUnitType(UnitType.Area, SystemOfUnits.Imperial).Count;
            Assert.AreEqual(3, ImperialAreaUnits);

            int DigitalStorageUnits = UnitInfo.InfoListByUnitType(UnitType.DigitalStorage).Count;
            Assert.AreEqual(5, DigitalStorageUnits);


            string[] SqMMAliases = Unit.SquareMillimeter.Info().Names;
            Assert.AreEqual(14, SqMMAliases.Length);

        }

        [TestMethod]
        public void UnitTypeInfoCounts()
        {
            int TotalUnitTypes = UnitTypeInfo.InfoList.Count;
            Assert.AreEqual(17, TotalUnitTypes);

            int DigitalStorageUnits = UnitType.DigitalStorage.Units().Count;
            Assert.AreEqual(5, DigitalStorageUnits);
        }

        private decimal? RoundTo8(decimal? value) => value.HasValue ? Math.Round(value.Value, 8) : (decimal?)null;

        [TestMethod]
        public void UnitConversionTests()
        {
            Assert.AreEqual(RoundTo8(2.54m), RoundTo8(Unit.Inch.ConvertTo(1m, Unit.Centimeter)), "Inch to CM Conversion Incorrect");

            Assert.AreEqual(RoundTo8(2.0m), RoundTo8(Unit.Inch.ConvertTo(24m, Unit.Foot)), "Inch to FT Conversion Incorrect");

            Assert.AreEqual(RoundTo8((1.0m / 2.54m)), RoundTo8(Unit.Centimeter.ConvertTo(1m, Unit.Inch)), "CM to Inch Conversion Incorrect");

            Assert.AreEqual(RoundTo8(3250m), RoundTo8(Unit.Gigabyte.ConvertFrom(3.25m, Unit.Terabyte)), "GB from TB Conversion Incorrect");

            Assert.IsNull(Unit.Gigabyte.ConvertFrom((decimal?)null, Unit.Terabyte));
        }

        [TestMethod]
        public void UnitConversionExceptionTests()
        {
            Assert.ThrowsException<UnitConversionException>(() => Unit.Each.Info().ConvertTo(Unit.SquareFoot));

            Assert.ThrowsException<UnitConversionException>(() => Unit.Centimeter.ConvertTo(1m, Unit.SquareFoot));
        }

        [DataTestMethod]
        [DataRow("Centimeter", Unit.Centimeter)]
        [DataRow("Centimetre", Unit.Centimeter)]
        [DataRow("CentiMEtres", Unit.Centimeter)]
        [DataRow("CM", Unit.Centimeter)] // only this passes
        [DataRow("SF", Unit.SquareFoot)]
        [DataRow("Gigabyte", Unit.Gigabyte)]
        [DataRow("Foo", null)]
        public void UnitNameLookupTests(string UnitName, Unit? DesiredUnit)
        {
            if (DesiredUnit == null)
                Assert.IsNull(UnitInfo.UnitByName(UnitName));
            else
                Assert.AreEqual(DesiredUnit, UnitInfo.UnitByName(UnitName));
        }

        [TestMethod]
        public void TestSerializeDeserialize()
        {
            foreach(var ui in UnitInfo.InfoList)
            {
                var stringifiedObj = JsonConvert.SerializeObject(ui);
                Assert.IsNotNull(stringifiedObj);
                var deserializedObj = JsonConvert.DeserializeObject<UnitInfo>(stringifiedObj);
                Assert.IsNotNull(deserializedObj);

                Assert.IsTrue(ui.IsDeepEqual(deserializedObj));
            }

            var stringifiedList = JsonConvert.SerializeObject(UnitInfo.InfoList);
            Assert.IsNotNull(stringifiedList);
            var deserializedList = JsonConvert.DeserializeObject<List<UnitInfo>>(stringifiedList);
            Assert.IsNotNull(deserializedList);
        }
    }
}
