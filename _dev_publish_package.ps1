param (
   [Parameter(Mandatory)][string]$project,
   [Parameter(Mandatory)][string]$mygetkey,
   [Parameter()][bool]$dotnet = $true
)

if (!$folderName) { $folderName = $project }

$fullPath = (Get-Item -Path ./$folderName/$project.csproj -Verbose).FullName
			
Write-Host "| $project |"
Write-Host "|-- LATEST LIVE VERSION --|"
& nuget list $project -source "https://www.myget.org/F/corebridgeshared/auth/$mygetkey/api/v2" -prerelease
Write-Host "|-- LOCAL VERSION --|"
& ./_getAssemblyVersion.ps1 $fullPath
Write-Host "|-------------------------|"

if ($dotnet) {
	$newVersion = Read-Host -Prompt 'Input new version, including pre-release tag:'
	$yn = & .\_scripts\_readChoice 'Confirm Package Version' '&yes','&no' '&no' ('Confirm '+$project+' v'+$newVersion+'?')
	
	switch($yn) 
	{
		'&yes' 
		{
			& ./_setAssemblyVersion.ps1 $fullPath $newVersion
		}
		'&no'
		{
			return $null;
		}
	}
	& dotnet build ./$folderName/$project.csproj
	& ./dev_push_latest_package.ps1 $project $mygetkey $build "$project.$newVersion.nupkg"
	
	$tag = $project+' v'+$newVersion
	$tag | Clip
	Write-Host -Prompt "Commit message fragment $tag copied to clipboard, please paste into commit message:" -BackgroundColor DarkGreen -ForegroundColor White
	Read-Host -Prompt 'Press return to continue'
} else {
	Read-Host -Prompt 'Update version in VS, then press enter'
	Read-Host -Prompt 'Build project in VS, then press enter to publish'
	& ./dev_push_latest_package.ps1 $project $mygetkey $build
}

Write-Host ""