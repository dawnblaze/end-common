﻿param (
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [Parameter(Mandatory=$true)][string]$teamname,
    [string]$build = "Debug"
)

pushd .\_scripts
$valid = .\_team_validate
popd

if ($valid) {
	& .\_team_publish_package 'Endor.AEL' $mygetkey $teamname

	& .\_team_publish_package 'Endor.AzureStorage' $mygetkey $teamname

	& .\_team_publish_package 'Endor.EF' $mygetkey $teamname

	& .\_team_publish_package 'Endor.Logging.Client' $mygetkey $teamname

	& .\_team_publish_package 'Endor.Lucene' $mygetkey $teamname $false

	& .\_team_publish_package 'Endor.Tenant.Cache.Controller' $mygetkey $teamname

	& .\_team_publish_package 'Endor.Tenant.Cache.Controller.Net462' $mygetkey $teamname $false
}


