param (
    [Parameter(Mandatory=$true)][string]$mygetkey,
	
    [string]$build = "Debug"
)

pushd .\_scripts
$valid = .\_dev_validate
popd

if ($valid) {
	& .\_dev_publish_package 'Endor.AEL' $mygetkey

	& .\_dev_publish_package 'Endor.AzureStorage' $mygetkey

	& .\_dev_publish_package 'Endor.EF' $mygetkey

	& .\_dev_publish_package 'Endor.Logging.Client' $mygetkey

	& .\_dev_publish_package 'Endor.Lucene' $mygetkey $false

	& .\_dev_publish_package 'Endor.Tenant.Cache.Controller' $mygetkey

	& .\_dev_publish_package 'Endor.Tenant.Cache.Controller.Net462' $mygetkey $false
}

