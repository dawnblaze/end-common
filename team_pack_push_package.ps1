param (
    [Parameter(Mandatory=$true)][string]$project,
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [Parameter(Mandatory=$true)][string]$teamname,
    [string]$build = "Debug"
)

& nuget pack .\$project\$project.csproj -OutputDirectory .\$project\bin\$build -Symbols

& .\team_push_latest_package.ps1 $project $mygetkey $teamname $build