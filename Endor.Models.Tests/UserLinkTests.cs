using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.Models;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Endor.Models.Tests
{
    [TestClass]
    public class UserLinkTests
    {
        [TestMethod]
        public void UserLinkDataTypeSetterTest()
        {
            UserLink userLink = new UserLink();
            userLink.BID = 1;
            userLink.DisplayName = "testDisplayName";
            userLink.ID = 2;
            userLink.ClassTypeID = 3;
            userLink.EmployeeID = 4;
            userLink.ContactID = 5;
            userLink.UserName = "testUserName";
            Debug.Print(""+userLink.BID);
            Debug.Print("" + userLink.EmployeeID);

            //Assert.IsTrue();
            Assert.IsTrue(userLink.BID.Equals(1));
            Assert.IsTrue(userLink.DisplayName.Equals("testDisplayName"));
            Assert.IsTrue(userLink.UserName.Equals("testUserName"));
            Assert.IsTrue(userLink.ID.Equals(2));
            Assert.IsTrue(userLink.ClassTypeID.Equals(3));
            Assert.IsTrue(userLink.ContactID.Equals(5));
            Assert.IsTrue(userLink.EmployeeID.Value.Equals(4));
            
        }
    }
}
