using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.DNSManagement;
using System.IO;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace Endor.DNSManagement.Tests
{
    [TestClass]
    public class DNSManagementTests
    {

        protected const string TenantId = "tenantid";
        protected const string ClientId = "client_id";
        protected const string ClientSecret = "client_secret";
        protected const string SubscriptionId = "subscription_id";
        protected const string ResourceGroup = "resourceGroup";
        protected const string AppName = "appName";
        protected const string HostNameDNSTarget = "hostNameDNSTarget";
        protected const string SSLPassword = "sslPassword";
        protected const string Location = "location";

        protected IConfigurationRoot _config;
        protected IDNSManager _manager;

        [TestInitialize]
        public virtual void Init()
        {
            string file = "..\\..\\..\\client-secrets.json";
            Assert.IsTrue(File.Exists(file));

            string text = File.ReadAllText(file);
            var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);

            this._config = new ConfigurationBuilder().AddInMemoryCollection(result).Build();
            Assert.IsTrue(_config[TenantId] != null && _config[TenantId].Length > 0);
            Assert.IsTrue(_config[ClientId] != null && _config[ClientId].Length > 0);
            Assert.IsTrue(_config[ClientSecret] != null && _config[ClientSecret].Length > 0);
            Assert.IsTrue(_config[SubscriptionId] != null && _config[SubscriptionId].Length > 0);
            Assert.IsTrue(_config[ResourceGroup] != null && _config[ResourceGroup].Length > 0);
            Assert.IsTrue(_config[AppName] != null && _config[AppName].Length > 0);
            Assert.IsTrue(_config[HostNameDNSTarget] != null && _config[HostNameDNSTarget].Length > 0);
            Assert.IsTrue(_config[SSLPassword] != null && _config[SSLPassword].Length > 0);
            Assert.IsTrue(_config[Location] != null && _config[Location].Length > 0);

            this._manager = new DNSManager(_config[TenantId], _config[ClientId], _config[ClientSecret], _config[SubscriptionId], _config[ResourceGroup], _config[AppName], _config[HostNameDNSTarget], _config[Location]);
        }

        [TestMethod]
        public async Task TestGetHostNames()
        {
            Assert.Inconclusive();

            var result = await this._manager.GetHostNamesAsync();
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public async Task TestVerifyHostNames()
        {
            Assert.Inconclusive();

            var hostNames = new List<string>()
            {
                "endorwebdev-test.azurewebsites.net",
                "endorwebdev-test2.azurewebsites.net"
            };

            var result = await this._manager.CheckAvailabilityAsync(hostNames);
            Assert.IsNotNull(result);
            Assert.AreEqual(hostNames.Count, result.Count);
            Assert.IsFalse(result[hostNames[0]]); // should be false since it DOES exist already
            Assert.IsTrue(result[hostNames[1]]); // should be true since it does NOT exist already
        }

        [TestMethod]
        public async Task AddHostNameTest()
        {
            Assert.Inconclusive();

            const string hostName = "v3app-test2.cyriousapp.com";

            var hostNames = await _manager.GetHostNamesAsync();

            if (hostNames.Exists(s => s.ToLower() == hostName.ToLower()))
                await this._manager.DeleteAsync(hostName);

            hostNames = await _manager.GetHostNamesAsync();
            Assert.IsFalse(hostNames.Exists(s => s.ToLower() == hostName.ToLower()));

            var resp = await this._manager.AddHostNameAsync(hostName);
            Assert.IsTrue(resp.Success);

            hostNames = await _manager.GetHostNamesAsync();
            Assert.IsTrue(hostNames.Exists(s => s.ToLower() == hostName.ToLower()));

            // Check for failure
            try
            {
                resp = await this._manager.AddHostNameAsync("junk.cyrious.com");
                Assert.IsFalse(resp.Success);
            }
            catch
            {

            }
        }

        [TestMethod]
        public async Task AddMultipleHostNamesTest()
        {
            Assert.Inconclusive();

            var hostNames = new List<string>()
            {
                "v3app-test2.cyriousapp.com",
                "junk.cyrious.com"
            };

            var getHostNames = await _manager.GetHostNamesAsync();
            foreach(var hostName in hostNames)
            {
                if (getHostNames.Exists(s => s.ToLower() == hostName.ToLower()))
                    await this._manager.DeleteAsync(hostName);

                getHostNames = await _manager.GetHostNamesAsync();
                Assert.IsFalse(getHostNames.Exists(s => s.ToLower() == hostName.ToLower()));

            }

            var resp = await this._manager.AddHostNamesAsync(hostNames);
            Assert.IsNotNull(resp);
            Assert.AreEqual(2, resp.Count);
            Assert.IsTrue(resp[hostNames[0]].Success);
            Assert.IsFalse(resp[hostNames[1]].Success);

            getHostNames = await _manager.GetHostNamesAsync();
            Assert.IsTrue(hostNames.Exists(s => s.ToLower() == hostNames[0].ToLower()));
        }

        [TestMethod]
        public async Task TestDeleteHostName()
        {
            Assert.Inconclusive();

            //const string hostName = "endorwebdev-test.azurewebsites.net";
            const string hostName1 = "v3app-test.cyriousapp.com";
            const string hostName2 = "v3app-test1.cyriousapp.com";
            const string hostName3 = "v3app-test2.cyriousapp.com";

            //Test Single Delete...
            var hostNames = await _manager.GetHostNamesAsync();
            if (!hostNames.Exists(s => s.ToLower() == hostName1.ToLower()))
            {
                await this._manager.AddHostNameAsync(hostName1);
                hostNames = await _manager.GetHostNamesAsync();
                Assert.IsTrue(hostNames.Exists(s => s.ToLower() == hostName1.ToLower()));
            }
            
            await this._manager.DeleteAsync(hostName1);
            hostNames = await _manager.GetHostNamesAsync();
            Assert.IsFalse(hostNames.Exists(s => s.ToLower() == hostName1.ToLower()));

            //Test Delete All...
            hostNames = await _manager.GetHostNamesAsync();
            if (!hostNames.Exists(s => s.ToLower() == hostName1.ToLower()))
            {
                await this._manager.AddHostNameAsync(hostName1);
                hostNames = await _manager.GetHostNamesAsync();
                Assert.IsTrue(hostNames.Exists(s => s.ToLower() == hostName1.ToLower()));
            }
            if (!hostNames.Exists(s => s.ToLower() == hostName2.ToLower()))
            {
                await this._manager.AddHostNameAsync(hostName2);
                hostNames = await _manager.GetHostNamesAsync();
                Assert.IsTrue(hostNames.Exists(s => s.ToLower() == hostName2.ToLower()));
            }
            if (!hostNames.Exists(s => s.ToLower() == hostName3.ToLower()))
            {
                await this._manager.AddHostNameAsync(hostName3);
                hostNames = await _manager.GetHostNamesAsync();
                Assert.IsTrue(hostNames.Exists(s => s.ToLower() == hostName3.ToLower()));
            }

            await this._manager.DeleteAllAsync(); 
            hostNames = await _manager.GetHostNamesAsync();
            Assert.IsFalse(hostNames.Exists(s => s.ToLower() == hostName1.ToLower()));
            Assert.IsFalse(hostNames.Exists(s => s.ToLower() == hostName2.ToLower()));
            Assert.IsFalse(hostNames.Exists(s => s.ToLower() == hostName3.ToLower()));
        }

        [TestMethod]
        public async Task TestVerifyDNS()
        {
            Assert.Inconclusive();

            const string hostName = "v3app-test2.cyriousapp.com";

            var hostNames = await _manager.GetHostNamesAsync();
            if (!hostNames.Exists(s => s.ToLower() == hostName.ToLower()))
            {
                await this._manager.AddHostNameAsync(hostName);
                //Verify if hostname was added successfully...
                hostNames = await _manager.GetHostNamesAsync();
                Assert.IsTrue(hostNames.Exists(s => s.ToLower() == hostName.ToLower()));
            }

            Assert.IsTrue(await this._manager.VerifyDNSAsync(hostName));
        }

        [TestMethod]
        public async Task TestVerifySSL()
        {
            Assert.Inconclusive();

            await this.AddMultipleHostNamesTest();

            List<string> hostNames = new List<string>() { "endorwebdev-test.azurewebsites.net", "v3app-test2.cyriousapp.com" };
            var resp = await _manager.VerifySSLAsync(hostNames);
            Assert.IsNotNull(resp);
            Assert.AreEqual(2, resp.Count);
            Assert.IsNull(resp[1].Certificate);
            Assert.IsFalse(resp[1].IsValid);
            Assert.IsNotNull(resp[0].Certificate);
            Assert.IsTrue(resp[0].IsValid);
        }

        [TestMethod]
        public async Task TestRegisterSSL()
        {
            Assert.Inconclusive();

            try
            {
                var hostName = "v3app-test7.cyriousapp.com";
                var cert = File.ReadAllBytes("..\\..\\..\\Cyriousapp9432.pfx");
                var certPassword = _config[SSLPassword];
                var certName = "Cyriousapp9432";

                var resp = await _manager.RegisterSSLAsync(hostName, cert, certPassword, certName);
                Assert.IsFalse(resp.Success);
                Assert.IsTrue(resp.Message.Contains("Another certificate exists with same thumbprint"));

                certName = "F1FD2D7AB6D9FD06FDEF1AD5FB68D9158A7723C1-EndorDev-CentralUSwebspace";
                resp = await _manager.RegisterSSLAsync(hostName, cert, certPassword, certName);
                Assert.IsTrue(resp.Success);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
