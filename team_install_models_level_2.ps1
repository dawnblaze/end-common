param (
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [Parameter(Mandatory=$true)][string]$teamname,
    [Parameter(Mandatory=$true)][string]$version
)

pushd .\_scripts
$valid = .\_team_validate
popd

function Install(
   [Parameter(Mandatory)][string]$project,
   [Parameter(Mandatory)][string]$mygetkey,
   [Parameter(Mandatory)][string]$teamname,
   [Parameter(Mandatory)][string]$version,
   [Parameter()][AllowNull()][string]$folderName = $null
) {
	if (!$folderName) { $folderName = $project }
	& dotnet add ./$folderName/$project.csproj package Endor.Models --source https://www.myget.org/F/corebridgeteam$teamname/auth/$mygetkey/api/v2 --version $version
}

if ($valid) {
	Install 'Endor.AEL' $mygetkey $teamname $version

	Install 'Endor.AEL.Tests' $mygetkey $teamname $version

	#Install 'Endor.AzureStorage' $mygetkey $teamname $version

	#Install 'Endor.Common.Level2.Tests' $mygetkey $teamname $version 'Endor.Common.Tests'

	Install 'Endor.EF' $mygetkey $teamname $version

	Install 'Endor.EF.StartupEmulator' $mygetkey $teamname $version

	#Install 'Endor.Logging.Client' $mygetkey $teamname $version

	#Install 'Endor.Lucene' $mygetkey $teamname $version

	#Install 'Endor.Lucene.Tests' $mygetkey $teamname $version

	#Install 'Endor.Tenant.Cache.Controller' $mygetkey $teamname $version

	#Install 'Endor.Tenant.Cache.Controller.Net462' $mygetkey $teamname $version
	
	Read-Host -Prompt 'In Visual Studio, please install Models to Endor.Lucene and Endor.Lucene.Tests, then press the Enter key'
}
