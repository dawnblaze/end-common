﻿using Hangfire.Server;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface IOrderPricingCompute
    {
        Task OrderPricingCompute(short bid, int orderID, string priority, PerformContext context);
        Task OrderPricingComputeTax(short bid, int orderID, string priority, PerformContext context);
    }
}
