﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface ITaskQueuer
    {
        string Priority { get; set; }
        string Schedule { get; set; }
        Task<string> IndexClasstype(short bid, int ctid);
        Task<string> IndexMultipleID(short bid, int ctid, ICollection<int> ids);
        Task<string> IndexModel(short bid, int ctid, int id);
        Task<string> Test(short bid);
        Task<string> EmptyTrash(short bid);
        Task<string> EmptyTemp(short bid);
        Task<string> RecomputeGL(short bid, byte EnumGLType, int Id, string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType);
        Task<string> CheckOrderStatus(short bid, int orderId, int status);
        Task<string> ClearAllAssemblies(short bid);
        Task<string> ClearAssembly(short bid, int ctid, int id);
        Task<string> OrderPricingCompute(short bid, int orderID);
        Task<string> OrderPricingComputeTax(short bid, int orderID);
        Task<string> DeleteExpiredDrafts(short bid);
        Task<string> RepairUntrustedConstraint(short bid);
        Task<string> RegenerateCBELForMachine(short bid, int machineId);
        Task<string> CreateDBBackup(short bid, DbType dbType);
        Task<string> GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null);
        Task<string> GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null);
        Task<string> Reconciliate(short bid, byte? locationID, short? enteredById, DateTime accountingDT, bool createAdjustments, IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> cashDrawer);
        Task<string> CreateViewCustomField(short bid, int appliesToClassTypeID, string sourceDataTable, string viewBaseName, string viewSchema = "dbo", string IDFieldName = "ID");
    }
}
