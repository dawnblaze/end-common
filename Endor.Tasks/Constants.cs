﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public static class Constants
    {
        public static class RecurringJobs
        {
            public static string GetEmptyTrashID(short bid) => $"emptytrash{bid}";
            public static string GetEmptyTempID(short bid) => $"emptytemp{bid}";
        }
                
    }
}
