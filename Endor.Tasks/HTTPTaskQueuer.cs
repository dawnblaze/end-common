﻿using Hangfire;
using Hangfire.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web;

namespace Endor.Tasks
{
    /// <summary>
    /// Client level task scheduler
    /// </summary>
    public class HttpTaskQueuer: ITaskQueuer
    {
        private readonly string _BGEngineURL;
        private readonly string _TenantSecret;

        public string Priority { get; set; } = "low";
        public string Schedule { get; set; } = "Immediate";

        public HttpTaskQueuer(string bgEngineURL, string tenantSecret)
        {
            _BGEngineURL = bgEngineURL;
            _TenantSecret = tenantSecret;
        }

        public async Task<string> EmptyTemp(short bid)
        {
            return await Get(bid, $"task/emptytemp/{bid}");
        }

        public async Task<string> RecomputeGL(short bid, byte EnumGLType, int Id,string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType)
        {
            var requestURL = $"/task/recomputegl/{bid}/{EnumGLType}/{Id}/{name}/{Subject}/{glEntryType}/{this.Priority}";
            if (CompletedByContactId.HasValue || CompletedById.HasValue)
            {
                requestURL += "?";
                if (CompletedById.HasValue)
                {
                    requestURL += ("CompletedById=" + CompletedById.Value.ToString());
                    if (CompletedByContactId.HasValue)
                    {
                        requestURL += "&";
                    }
                }
                if (CompletedByContactId.HasValue)
                {
                    requestURL += ("CompletedByContactId=" + CompletedByContactId.Value.ToString());
                }
            }
            return await Get(bid, requestURL);
        }

        public async Task<string> EmptyTrash(short bid)
        {
            return await Get(bid, $"task/emptytrash/{bid}");
        }

        public async Task<string> IndexClasstype(short bid, int ctid)
        {
            return await Get(bid, $"/task/index/{bid}/{ctid}");
        }
        public async Task<string> IndexModel(short bid, int ctid, int id)
        {
            return await Get(bid, $"/task/index/{bid}/{ctid}/{id}");
        }
        public async Task<string> IndexMultipleID(short bid, int ctid, ICollection<int> ids)
        {
            string commaSeparatedIDs = String.Join(",", ids);
            return await Get(bid, $"/task/index/{bid}/{ctid}/multiple/{commaSeparatedIDs}");
        }

        public async Task<string> Test(short bid)
        {
            return await Get(bid, $"/task/test/{bid}");
        }

        public async Task<string> CheckOrderStatus(short bid, int orderId, int status)
        {
            return await Get(bid, $"/task/checkorderstatus/{bid}/{orderId}/{status}");
        }

        public async Task<string> OrderPricingCompute(short bid, int orderID)
        {
                return await Get(bid, $"/task/OrderPricingCompute/{bid}/{orderID}/{this.Priority}");
        }

        public async Task<string> OrderPricingComputeTax(short bid, int orderID)
        {
            return await Get(bid, $"/task/OrderPricingComputeTax/{bid}/{orderID}/{this.Priority}");
        }

        public async Task<string> DeleteExpiredDrafts(short bid)
        {
            return await Get(bid, $"/task/DeleteExpiredDrafts/{bid}");
        }

        public async Task<string> RepairUntrustedConstraint(short bid)
        {
            return await Get(bid, $"/task/RepairUntrustedConstraint/{bid}");
        }

        public async Task<string> RegenerateCBELForMachine(short bid, int machineId)
        {
            return await Get(bid, $"/task/RegenerateCBELForMachine/{bid}/{machineId}");
        }

        public async Task<string> ClearAllAssemblies(short bid)
        {
            return await Get(bid, $"/task/ClearAllAssemblies/{bid}");
        }

        public async Task<string> ClearAssembly(short bid, int ctid, int id)
        {
            return await Get(bid, $"/task/ClearAssembly/{bid}/{ctid}/{id}");
        }

        private async Task<string> GetAsyncRequest(short bid, string url)
        {

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Internal", $"{_TenantSecret}:{bid}");
                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsStringAsync();
                else
                    return null;
            }  
        }

        private Task<string> Get(short bid, string path)
        {
            string url = GetBaseURL(bid, path);
            return GetAsyncRequest(bid, url);
        }

        private async Task<string> Post(short bid, string path, string content)
        {
            var strContent = new StringContent(content, Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Internal", $"{_TenantSecret}:{bid}");

                string url = GetBaseURL(bid, path);
                var response = await client.PostAsync(url, strContent);

                if (response.IsSuccessStatusCode)
                    return await response.Content.ReadAsStringAsync();
                else
                    return null;
            }
        }

        private string GetBaseURL(short bid, string path)
        {
            //usually we'd do something like
            //string result = (await _cache.Get(bid)).BackgroundEngineURL;
            //but we have one BGEngine per node, so don't refer to the cache
            string result = this._BGEngineURL;
            result = result.TrimEnd('/') + path;
            return result;
        }

        public async Task<string> CreateDBBackup(short bid, DbType dbType)
        {            
            return await Get(bid, $"/task/CreateDBBackup/{bid}/{dbType}");
        }

        public async Task<string> GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            var queryString = BuildQueryString(this.Priority, this.Schedule, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath);
            return await Get(bid, $"/task/DocumentReport/{aid}/{bid}/{documentReportType}/menu/{MenuID}/{queryString}");
        }

        public async Task<string> GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            var queryString = BuildQueryString(this.Priority, this.Schedule, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath);
            return await Get(bid, $"/task/DocumentReport/{aid}/{bid}/{documentReportType}/template/{templateName}/{queryString}");
        }

        private static string BuildQueryString(string priority, string schedule, int[] dataID, int[] customFields, string orderStatusFilter, string optionsValue, string dmFilePath)
        {
            var queryString = "";
            if (dataID.Length > 0)
            {
                string param = "";
                foreach(var id in dataID)
                {
                    param += String.Format("{0}dataID={1}", param.Length == 0 ? "" : "&", id.ToString());
                }
                queryString += String.Format("{0}{1}", queryString.Length == 0 ? "?" : "&", param);
            }
            if (customFields.Length > 0)
            {
                string param = "";
                foreach (var cf in customFields)
                {
                    param += String.Format("{0}customFields={1}", param.Length == 0 ? "" : "&", cf.ToString());
                }
                queryString += String.Format("{0}{1}", queryString.Length == 0 ? "?" : "&", param);
            }
            
            if (orderStatusFilter != null) queryString += queryString.Length == 0 ? "?orderStatusFilter=" + orderStatusFilter : "&orderStatusFilter=" + orderStatusFilter;
            if (optionsValue != null) queryString += queryString.Length == 0 ? "?optionsValue=" + optionsValue : "&optionsValue=" + optionsValue;
            if (priority != null) queryString += queryString.Length == 0 ? "?priority=" + priority : "&priority=" + priority;
            if (schedule != null) queryString += queryString.Length == 0 ? "?schedule=" + schedule : "&schedule=" + schedule;
            if (dmFilePath != null) queryString += queryString.Length == 0 ? "?dmFilePath=" + dmFilePath : "&dmFilePath=" + dmFilePath;
            return queryString;
        }

        public async Task<string> Reconciliate(short bid, byte? locationID, short? enteredById, DateTime accountingDT, bool createAdjustments, IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> cashDrawer)
        {
            var cashDrawerSe = cashDrawer != null && cashDrawer.Any() ? 
                Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(cashDrawer))) : String.Empty;
            return await Get(bid, $"/task/reconciliate/{bid}/{locationID ?? 0}/{enteredById ?? 0}/{accountingDT}/{createAdjustments}/{cashDrawerSe}");
        }

        public async Task<string> CreateViewCustomField(short bid, int appliesToClassTypeID, string sourceDataTable, string viewBaseName, string viewSchema = "dbo", string IDFieldName = "ID")
        {
            return await Get(bid, $"/task/AnalyticReport/{bid}/{appliesToClassTypeID}/{sourceDataTable}/{viewBaseName}/?viewSchema={viewSchema}&IDFieldName={IDFieldName}");
        }
    }
}
