﻿
using Hangfire.Server;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface IReconciliation
    {
        Task Reconciliate(
            short bid, 
            byte? locationID, 
            short? enteredById, 
            DateTime accountingDT, 
            bool createAdjustments,
            IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> cashDrawer,
            string priority, 
            PerformContext context);
    }
}