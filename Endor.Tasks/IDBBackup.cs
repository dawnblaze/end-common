﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface IDBBackup
    {
        Task CreateDBBackup(short bid, DbType dbType);
    }
}
