﻿using Hangfire.Server;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface IRecomputeGL
    {
        Task RecomputeGL(short bid, byte EnumGLType, int Id, string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType, string priority, PerformContext context);
    }
}