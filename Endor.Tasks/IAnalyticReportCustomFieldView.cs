﻿using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface IAnalyticReportCustomFieldView
    {
        Task CreateViewCustomField(short bid, int appliesToClassTypeID, string sourceDataTable, string viewBaseName, string viewSchema = "dbo", string IDFieldName = "ID");
    }
}
