﻿using Hangfire.Server;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface ICheckOrderStatus
    {
        Task CheckOrderStatus(short bid, int orderId, int status, PerformContext context);
    }
}