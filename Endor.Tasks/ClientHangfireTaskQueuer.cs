﻿using Hangfire;
using Hangfire.Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    /// <summary>
    /// Client level task enqueuer that sets up Hangfire configuration
    /// DO NOT INSTANTIATE ON SERVER
    /// server has its own configuration
    /// </summary>
    public class ClientHangfireTaskQueuer: HangfireTaskQueuer
    {
        public ClientHangfireTaskQueuer(string sqlConnectionString)
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage(sqlConnectionString)
                .UseConsole();
        }
    }
}
