﻿using System;
using Hangfire.Server;
using System.Threading.Tasks;
namespace Endor.Tasks
{
    public interface IRegenerateCBELForMachine
    {
        Task RegenerateCBELForMachine(short bid, int machineId, PerformContext context);

    }
}
