﻿using Hangfire.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface ITester
    {
        Task Test(short bid, PerformContext context);
    }
}
