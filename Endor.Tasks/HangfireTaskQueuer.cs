﻿using Hangfire;
using Hangfire.Console;
using Hangfire.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    /// <summary>
    /// Base task enqueuer for Hangfire
    /// </summary>
    public class HangfireTaskQueuer : ITaskQueuer
    {
        public string Priority { get; set; } = "low";
        public string Schedule { get; set; } = "Immediate"; // default: Immediate, or Minutes, or DateTimeUTC

        public HangfireTaskQueuer() { }

        public Task<string> EmptyTemp(short bid)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IDocCleaner>(x => x.EmptyTemp(bid), state));
        }

        public Task<string> RecomputeGL(short bid, byte EnumGLType, int Id, string Name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IRecomputeGL>(x => x.RecomputeGL(bid, EnumGLType, Id, Name, Subject, CompletedById, CompletedByContactId, glEntryType, this.Priority, null), state));
        }

        public Task<string> EmptyTrash(short bid)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IDocCleaner>(x => x.EmptyTrash(bid), state));
        }

        public Task<string> IndexClasstype(short bid, int ctid)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IIndexer>(x => x.Index(bid, ctid), state));
        }

        public Task<string> IndexMultipleID(short bid, int ctid, ICollection<int> ids)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IIndexer>(x => x.Index(bid, ctid, ids), state));
        }

        public Task<string> IndexModel(short bid, int ctid, int id)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IIndexer>(x => x.Index(bid, ctid, id), state));
        }

        public Task<string> Test(short bid)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<ITester>(x => x.Test(bid, null), state));
        }

        public Task<string> CheckOrderStatus(short bid, int orderId, int status)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<ICheckOrderStatus>(x => x.CheckOrderStatus(bid, orderId, status, null), state));
        }

        public Task<string> OrderPricingCompute(short bid, int orderID)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IOrderPricingCompute>(x => x.OrderPricingCompute(bid, orderID, this.Priority, null), state));
        }

        public Task<string> OrderPricingComputeTax(short bid, int orderID)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IOrderPricingCompute>(x => x.OrderPricingComputeTax(bid, orderID, this.Priority, null), state));
        }

        public Task<string> DeleteExpiredDrafts(short bid)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IDeleteExpiredDrafts>(x => x.DeleteExpiredDrafts(bid), state));
        }

        public Task<string> RepairUntrustedConstraint(short bid)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<ISQLMaintenance>(x => x.RepairUntrustedConstraint(bid), state));
        }

        public Task<string> RegenerateCBELForMachine(short bid, int machineID)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IRegenerateCBELForMachine>(x => x.RegenerateCBELForMachine(bid, machineID, null), state));
        }

        public Task<string> CreateDBBackup(short bid, DbType dbType)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IDBBackup>(x => x.CreateDBBackup(bid, dbType), state));
        }

        public Task<string> ClearAllAssemblies(short bid)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IAssemblyCleaner>(x => x.ClearAllAssemblies(bid), state));
        }

        public Task<string> ClearAssembly(short bid, int ctid, int id)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IAssemblyCleaner>(x => x.ClearAssembly(bid, ctid, id), state));
        }

        public Task<string> GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            var client = new BackgroundJobClient();

            // trigger immediately by priority
            if (string.IsNullOrWhiteSpace(this.Schedule) || this.Schedule.ToLowerInvariant() == "immediate")
            {
                var state = new EnqueuedState(this.Priority);
                return Task.FromResult(client.Create<IDocumentReportGenerator>(x => x.GenerateDocumentReportByMenuID(aid, bid, documentReportType, MenuID, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath), state));
            }
            else
            {
                // trigger by minutes schedule
                if (int.TryParse(this.Schedule, out int minutes))
                {
                    DateTime dateTime = DateTime.UtcNow;
                    return Task.FromResult(client.Schedule<IDocumentReportGenerator>(x => x.GenerateDocumentReportByMenuID(aid, bid, documentReportType, MenuID, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath), dateTime.AddMinutes(minutes)));
                }
                else
                {
                    if (DateTime.TryParse(this.Schedule, out DateTime dateTime))
                    {
                        // trigger by date schedule
                        return Task.FromResult(client.Schedule<IDocumentReportGenerator>(x => x.GenerateDocumentReportByMenuID(aid, bid, documentReportType, MenuID, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath), dateTime.ToUniversalTime()));
                    }
                    else
                    {
                        // fallback to immediate
                        var state = new EnqueuedState(this.Priority);
                        return Task.FromResult(client.Create<IDocumentReportGenerator>(x => x.GenerateDocumentReportByMenuID(aid, bid, documentReportType, MenuID, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath), state));
                    }
                }
            }
        }

        public Task<string> GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
        {
            var client = new BackgroundJobClient();

            // trigger immediately by priority
            if (string.IsNullOrWhiteSpace(this.Schedule) || this.Schedule.ToLowerInvariant() == "immediate")
            {
                var state = new EnqueuedState(this.Priority);
                return Task.FromResult(client.Create<IDocumentReportGenerator>(x => x.GenerateDocumentReportByTemplateName(aid, bid, documentReportType, templateName, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath), state));
            }
            else
            {
                // trigger by minutes schedule
                if (int.TryParse(this.Schedule, out int minutes))
                {
                    DateTime dateTime = DateTime.UtcNow;
                    return Task.FromResult(client.Schedule<IDocumentReportGenerator>(x => x.GenerateDocumentReportByTemplateName(aid, bid, documentReportType, templateName, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath), dateTime.AddMinutes(minutes)));
                }
                else
                {
                    if (DateTime.TryParse(this.Schedule, out DateTime dateTime))
                    {
                        // trigger by date schedule
                        return Task.FromResult(client.Schedule<IDocumentReportGenerator>(x => x.GenerateDocumentReportByTemplateName(aid, bid, documentReportType, templateName, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath), dateTime.ToUniversalTime()));
                    }
                    else
                    {
                        // fallback to immediate
                        var state = new EnqueuedState(this.Priority);
                        return Task.FromResult(client.Create<IDocumentReportGenerator>(x => x.GenerateDocumentReportByTemplateName(aid, bid, documentReportType, templateName, DataID, CustomFields, OrderStatusFilter, OptionsValue, DMFilePath), state));
                    }
                }
            }
        }

        public Task<string> Reconciliate(short bid, byte? locationID, short? enteredById, DateTime accountingDT, bool createAdjustments, IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> cashDrawer)
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IReconciliation>(x => x.Reconciliate(bid, locationID, enteredById, accountingDT, createAdjustments, cashDrawer, this.Priority, null), state));
        }

        public Task<string> CreateViewCustomField(short bid, int appliesToClassTypeID, string sourceDataTable, string viewBaseName, string viewSchema = "dbo", string IDFieldName = "ID")
        {
            var client = new BackgroundJobClient();
            var state = new EnqueuedState(this.Priority);
            return Task.FromResult(client.Create<IAnalyticReportCustomFieldView>(x => x.CreateViewCustomField(bid, appliesToClassTypeID, sourceDataTable, viewBaseName, viewSchema, IDFieldName), state));
        }
    }
}
