﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface IDocCleaner
    {
        Task EmptyTemp(short bid);
        Task EmptyTrash(short bid);
    }
}
