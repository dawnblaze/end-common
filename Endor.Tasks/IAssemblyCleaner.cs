﻿using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface IAssemblyCleaner
    {
        Task ClearAllAssemblies(short bid);
        Task ClearAssembly(short bid, int ctid, int id);
    }
}
