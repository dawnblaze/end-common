﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tasks
{
    public interface IIndexer
    {
        Task Index(short bid, int ctid);
        Task Index(short bid, int ctid, int id);
        Task Index(short bid, int ctid, ICollection<int> ids);
    }
}
