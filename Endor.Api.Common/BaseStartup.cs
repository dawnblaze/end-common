﻿using Endor.Api.Common;
using Endor.Api.Common.Classes;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Security;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Endor.Security.Repositories.Interfaces;
using Endor.Security.Interfaces;

namespace Endor.Api.Common
{
    public abstract class BaseStartup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public BaseStartup(IWebHostEnvironment env)
        {
            IConfigurationBuilder builder = GetConfigurationBuilder(env);

            Configuration = builder.Build();
        }

        /// <summary>
        /// Gets the api name e.g. Endor API
        /// </summary>
        /// <returns></returns>
        public abstract string GetAPIName();
        /// <summary>
        /// Gets the api version e.g. v1
        /// </summary>
        /// <returns></returns>
        public abstract string GetAPIVersion();

        /// <summary>
        /// Returns a configuration builder
        /// </summary>
        /// <param name="env"></param>
        /// <returns></returns>
        public virtual IConfigurationBuilder GetConfigurationBuilder(IWebHostEnvironment env)
        {
            return new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
        }

        /// <summary>
        /// Configuration
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        private JsonSerializerSettings SetJsonOptions(JsonSerializerSettings settings)
        {
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver();

            return settings;
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            JsonConvert.DefaultSettings = () => SetJsonOptions(new JsonSerializerSettings());
            services.AddLogging(config =>
            {
                // clear out default configuration
                config.ClearProviders();

                config.AddConfiguration(Configuration.GetSection("Logging"));
                config.AddDebug();
                config.AddEventSourceLogger();

                if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == Environments.Development)
                {
                    config.AddConsole();
                }
            });
            // Add framework services.
            services.Configure<EndorOptions>(Configuration.GetSection("Endor"));
            services.AddMemoryCache();
            EndorOptions endorOptions = Configuration.GetSection("Endor").Get<EndorOptions>();
            services.AddSingleton<Tenant.IEnvironmentOptions>(endorOptions);
            services.AddSingleton<EndorOptions>(endorOptions);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            ConfigureExternalEndorServices(services);

            this.AddAuthentication(services, Configuration);

            //previously we used (options => options.UseSqlServer(Configuration.GetConnectionString("ApiContext")));
            //now ApiContext uses the ITenantDataCache (above) to get the connection string per-request
            services.AddDbContext<ApiContext>();

            services.AddCors();
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            })
            .AddNewtonsoftJson(options =>
            {
                SetJsonOptions(options.SerializerSettings);
            }).AddApplicationPart(typeof(Endor.Logging.Client.LogLevelController).GetTypeInfo().Assembly)
              .AddControllersAsServices();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(GetAPIVersion(), new OpenApiInfo()
                {
                    Title = GetAPIName(),
                    Version = GetAPIVersion()
                });
#pragma warning disable CS0618
                c.DescribeAllEnumsAsStrings();
#pragma warning enable CS0618
                c.IgnoreObsoleteActions();
                ApplyIgnoreRelationships(c);
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                c.OperationFilter<SwaggerSecurityRightsDocumentFilter>();
                string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                string commentsFileName = Assembly.GetExecutingAssembly().GetName().Name + ".XML";
                string commentsFile = System.IO.Path.Combine(baseDirectory, commentsFileName);

                if (System.IO.File.Exists(commentsFile))
                {
                    c.IncludeXmlComments(commentsFile);
                }

                c.SchemaFilter<SwaggerAddMissingEnums>();
            });
            services.AddRouting();
        }

        /// <summary>
        /// Configures External Endor Services
        /// </summary>
        /// <param name="services"></param>
        public virtual void ConfigureExternalEndorServices(IServiceCollection services)
        {
            services.AddSingleton<ITenantDataCache, NetworkTenantDataCache>();
            services.AddSingleton<ITaskQueuer>(new HttpTaskQueuer(Configuration["Endor:TasksAPIURL"], Configuration["Endor:TenantSecret"]));
            services.AddSingleton<RemoteLogger>();
            services.AddTransient<IRTMPushClient>((x) => new RealtimeMessagingPushClient(Configuration["Endor:MessagingServerURL"]));
            services.AddSingleton<IServerConfigurations>((x) =>
                new SecurityServerConfiguration(x.GetService<ITenantDataCache>(), Configuration["Endor:MessagingServerURL"], x.GetService<IHttpContextAccessor>()));
            services.AddSingleton<IRightsCache, RightsCache>();
        }

        private static void ApplyIgnoreRelationships(Swashbuckle.AspNetCore.SwaggerGen.SwaggerGenOptions c)
        {
            c.SchemaFilter<ApplyIgnoreRelationshipsInModelsNamespace>("Endor.Models");
        }

        /// <summary>
        /// This method configures the authentication service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public abstract void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration);

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline. 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="appLifetime"></param>
        public virtual void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            RemoteLoggingExtensions.ConfigureSystemRemoteLogging(Configuration, loggerFactory, appLifetime);
            app.UseAuthentication();
            app.UseRouting();

            app.UseSwagger(o =>
            {
                o.PreSerializeFilters.Add((swagger, httpReq) =>
                {
                    swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}" } };
                });
            })
            .UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/swagger/{GetAPIVersion()}/swagger.json", $"{GetAPIName()} {GetAPIVersion()}");
                c.DocExpansion(DocExpansion.None);
                c.RoutePrefix = "swagger";
            })
            .UseCors(t => t.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod())
            .UseMvc()
            .UseDeveloperExceptionPage();
        }
    }

}
