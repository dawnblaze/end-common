﻿using Swashbuckle.AspNetCore.SwaggerGen;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.Linq;
using Microsoft.OpenApi.Models;
using System.Collections;
using System.Reflection;
using Endor.Models;

namespace Endor.Api.Common
{
    internal class ApplyIgnoreRelationshipsInNamespace<TNsType> : ISchemaFilter
    {
        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (model.Properties == null || model.Properties.Count == 0)
                return;
            var excludeList = new List<string>();

            if (context.Type.Namespace == "Endor.Models")
            {
                var props = context.Type.GetProperties();
                var propsToExclude = props.Where(p => p.PropertyType.Namespace == "Endor.Models"
                    || (p.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(p.PropertyType))).ToList();
                excludeList.AddRange(propsToExclude.Select(p => p.Name));
            }

            foreach (var prop in excludeList)
            {
                if (model.Properties.Keys.Any(x => x.Equals(prop.ToLower())))
                    model.Properties.Remove(prop.ToLower());
            }
        }
    }

    internal class ApplyIgnoreRelationshipsInModelsNamespace : ISchemaFilter
    {
        string nameSpaceToIgnore;
        public ApplyIgnoreRelationshipsInModelsNamespace(string ns)
        {
            nameSpaceToIgnore = ns;
        }

        public void Apply(OpenApiSchema model, SchemaFilterContext context)
        {
            if (model.Properties == null || model.Properties.Count == 0)
                return;
            var excludeList = new List<string>();

            if (context.Type.Namespace == nameSpaceToIgnore)
            {
                var props = context.Type.GetProperties();
                var propsToExclude = props.Where(p => p.GetCustomAttribute<SwaggerIncludeAttribute>() == null
                    && !p.Name.Contains("Locator")).Where(
                        p => p.PropertyType.Namespace == nameSpaceToIgnore || p.Name == "BID"
                        || (p.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(p.PropertyType))).ToList();
                excludeList.AddRange(propsToExclude.Select(p => p.Name));
            }

            foreach (var prop in excludeList)
            {
                if (model.Properties.Keys.Any(x => x.Equals(prop.ToLower())))
                    model.Properties.Remove(prop.ToLower());
            }
        }
    }
}