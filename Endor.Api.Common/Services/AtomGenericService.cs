﻿using Endor.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Common.Services
{
    /// <summary>
    /// Generic service for base Atom Model
    /// </summary>
    public abstract class AtomGenericService<M, I> : BaseGenericService<M>
        where M : class
        where I : struct, IConvertible
    {
        /// <summary>
        /// Constructor for generic Atom model
        /// </summary>
        public AtomGenericService(ApiContext context, IMigrationHelper migrationHelper)
            : base(context, migrationHelper)
        {

        }

        /// <summary>
        /// Get valid includes for model
        /// </summary>
        public abstract string[] GetIncludes();

        /// <summary>
        /// Get objects that match the given search funciton
        /// </summary>
        public Task<List<M>> GetWhere(Expression<Func<M, bool>> predicate)
        {
            return ctx.Set<M>().IncludeAll(GetIncludes()).Where(predicate).ToListAsync();
        }

        /// <summary>
        /// Create Service
        /// </summary>
        public static Lazy<S> CreateService<S>(ApiContext ctx)
            where S : BaseGenericService<M>
        {
            return new Lazy<S>(() => Activator.CreateInstance(typeof(S), ctx) as S);
        }
    }
}
