﻿using Endor.EF;
using Endor.Tasks;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Services
{
    /// <summary>
    /// Base Generic Service typed to a Model class, initialized with a ApiContext
    /// </summary>
    /// <typeparam name="M"></typeparam>
    public abstract class BaseGenericService<M>
        where M : class
    {
        /// <summary>
        /// ApiContext
        /// </summary>
        protected readonly ApiContext ctx;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">ApiContext</param>
        protected BaseGenericService(ApiContext context, IMigrationHelper helper)
        {
            ctx = context;
            helper.MigrateDb(ctx);
        }
    }
}
