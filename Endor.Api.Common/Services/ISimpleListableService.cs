﻿using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Services
{
    /// <summary>
    /// A service that exposes a DbSet for simple lists
    /// most often used for simple list views
    /// </summary>
    /// <typeparam name="SLI"></typeparam>
    /// <typeparam name="I"></typeparam>
    public interface ISimpleListableViewService<SLI, I> where SLI : SimpleListItem<I> where I : struct, IConvertible
    {
        /// <summary>
        /// A DbSet for a simple list object
        /// </summary>
        DbSet<SLI> SimpleListSet { get; }
    }
}
