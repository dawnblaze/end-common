﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Services
{
    /// <summary>
    /// Defines methods that iterate over child objects during Create/Update/Delete operations
    /// </summary>
    /// <remarks>
    /// it's interfaced so that the generic service doesn't need to know the type parameters of each child service
    /// </remarks>
    /// <typeparam name="pM">the Parent Model type</typeparam>
    /// <typeparam name="pI">the Parent ID type</typeparam>
    public interface IChildServiceAssociation<pM, pI>
        where pM : class, IAtom<pI>
        where pI : struct, IConvertible
    {
        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being added
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        Task ForEachChildDoBeforeAddAsync(pM parentModel, Guid? tempGuid = null);
        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being updated
        /// </summary>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        Task ForEachChildDoBeforeUpdateAsync(pM parentModel);
        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being deleted
        /// </summary>
        /// <param name="parentModel"></param>
        Task ForEachChildDoBeforeDeleteAsync(pM parentModel);
        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being cloned
        /// </summary>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        Task ForEachChildDoBeforeCloneAsync(pM parentModel);
    }

    /// <summary>
    /// a class that represents parent-child relationships
    /// this exposes properties that allow one service to call another service
    /// <para>
    /// this is best illustrated by Employee and EmployeeLocators.
    /// an Employee save may save EmployeeLocators and should call the EmployeeLocator service's methods instead of managing another object
    /// </para>
    /// </summary>
    /// <typeparam name="cS">Child Service</typeparam>
    /// <typeparam name="pM">Parent Model</typeparam>
    /// <typeparam name="pI">Parent ID</typeparam>
    /// <typeparam name="cM">Child Model</typeparam>
    public abstract class ChildServiceAssociation<cS, pM, pI, cM> : IChildServiceAssociation<pM, pI>
        where cS : ICRUDService<cM>
        where pM : class, IAtom<pI>
        where cM : class
        where pI : struct, IConvertible
    {
        private readonly Lazy<cS> _svc;
        /// <summary>
        /// Service
        /// </summary>
        public cS Service { get { return _svc.Value; } }
        /// <summary>
        /// Children
        /// </summary>
        public readonly Func<pM, ICollection<cM>> ChildCollection;

        /// <summary>
        /// Child Service Association Constructor
        /// </summary>
        public ChildServiceAssociation(Lazy<cS> lazyService, Func<pM, ICollection<cM>> childCollection)
        {
            this._svc = lazyService;
            this.ChildCollection = childCollection;
        }

        private void ForEachChildDoBefore(pM parentModel, Action<cM> serviceCall)
        {
            
        }

        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being added
        /// </summary>
        /// <param name="parentModel"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        public async Task ForEachChildDoBeforeAddAsync(pM parentModel, Guid? tempGuid = null)
        {
            var children = ChildCollection(parentModel);
            if (children != null)
            {
                var myChildren = children.ToList();
                foreach(var child in myChildren)
                {
                    await Service.DoBeforeCreateAsync(child, tempGuid);
                }
            }
        }

        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being updated
        /// </summary>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        public async Task ForEachChildDoBeforeUpdateAsync(pM parentModel)
        {
            var children = ChildCollection(parentModel);
            if (children != null)
            {
                var myChildren = children.ToList();
                foreach (var child in myChildren)
                {
                    await Service.DoBeforeUpdateAsync(await Service.GetOldModelAsync(child), child);
                }                
            }
        }

        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being cloned
        /// </summary>
        /// <param name="parentModel"></param>
        /// <returns></returns>
        public async Task ForEachChildDoBeforeCloneAsync(pM parentModel)
        {
            if (Service is ICloneableChildCRUDService<pM, cM> cloneableService)
            {
                var children = ChildCollection(parentModel);
                if (children != null)
                {
                    IEnumerable<Task> myTasks = children.ToList().Select(a => cloneableService.DoBeforeCloneForParent(parentModel, a));

                    await Task.WhenAll(myTasks);
                }
            }
        }

        /// <summary>
        /// asynchronous function that iterates over child associations when the parent is being deleted
        /// </summary>
        /// <param name="parentModel"></param>
        public async Task ForEachChildDoBeforeDeleteAsync(pM parentModel)
        {
            ICollection<cM> children = ChildCollection(parentModel);

            if (children != null)
            {
                foreach (cM childModel in children)
                {
                    await Service.DoBeforeDeleteAsync(childModel);
                }
            }
        }
    }


    /// <summary>
    /// Concrete class for IChildServiceAssociation that iterates over child objects during Create/Update/Delete operations in order to cascade business logic
    /// </summary>
    /// <typeparam name="cS">Child Service type</typeparam>
    /// <typeparam name="pM">Parent Model type</typeparam>
    /// <typeparam name="pI">Parnet ID type</typeparam>
    /// <typeparam name="cM">Child Model type</typeparam>
    /// <typeparam name="cI">Child ID type</typeparam>
    public class ChildAtomServiceAssociation<cS, pM, pI, cM, cI> : ChildServiceAssociation<cS, pM, pI, cM>
        where cS : AtomCRUDService<cM, cI>
        where pM : class, IAtom<pI>
        where cM : class, IAtom<cI>
        where pI : struct, IConvertible
        where cI : struct, IConvertible
    {
        /// <summary>
        /// ChildAtomServiceAssociation Constructor
        /// </summary>
        public ChildAtomServiceAssociation(ApiContext ctx, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, AtomCRUDService<pM, pI> parent, Func<pM, ICollection<cM>> childCollectionAccessor, IMigrationHelper migrationHelper)
            : base(parent.GetLazyChildAtomService<cS, cM, cI>(ctx, taskQueuer, cache, logger, parent.BID, rtmClient, migrationHelper), childCollectionAccessor)
        {
        }
    }

    /// <summary>
    /// ChildAtomServiceAssociation Constructor
    /// </summary>
    public class ChildLinkServiceAssociation<cS, pM, pI, cM> : ChildServiceAssociation<cS, pM, pI, cM>
        where cS : LinkCRUDService<cM>
        where pM : class, IAtom<pI>
        where cM : class
        where pI : struct, IConvertible
    {
        /// <summary>
        /// ChildLinkServiceAssociation Constructor
        /// </summary>
        public ChildLinkServiceAssociation(ApiContext ctx, RemoteLogger logger, IRTMPushClient rtmClient, AtomCRUDService<pM, pI> parent, Func<pM, ICollection<cM>> childCollectionAccessor)
            : base(parent.GetLazyChildLinkService<cS, cM>(ctx, logger, parent.BID, rtmClient), childCollectionAccessor)
        {
        }
        /// <summary>
        /// ChildLinkServiceAssociation Constructor
        /// </summary>
        public ChildLinkServiceAssociation(ApiContext ctx, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, AtomCRUDService<pM, pI> parent, Func<pM, ICollection<cM>> childCollectionAccessor)
            : base(parent.GetLazyChildLinkService<cS, cM>(ctx, logger, parent.BID, rtmClient, taskQueuer, cache), childCollectionAccessor)
        {
        }

        /// <summary>
        /// ChildLinkServiceAssociation Constructor
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="logger"></param>
        /// <param name="rtmClient"></param>
        /// <param name="taskQueuer"></param>
        /// <param name="cache"></param>
        /// <param name="migrationHelper"></param>
        /// <param name="parent"></param>
        /// <param name="childCollectionAccessor"></param>
        public ChildLinkServiceAssociation(ApiContext ctx, RemoteLogger logger, IRTMPushClient rtmClient, ITaskQueuer taskQueuer, ITenantDataCache cache, AtomCRUDService<pM, pI> parent, Func<pM, ICollection<cM>> childCollectionAccessor, IMigrationHelper migrationHelper)
            : base(parent.GetLazyChildLinkService<cS, cM>(ctx, logger, parent.BID, rtmClient, taskQueuer, cache, migrationHelper), childCollectionAccessor)
        {
        }
    }
}
