﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.RTM;
using Endor.Tasks;
using Endor.Tenant;
using Endor.Api.Common.Classes;
using Microsoft.EntityFrameworkCore;

namespace Endor.Api.Common.Services
{
    /// <inheritdoc />
    /// <summary>
    /// UserDraftService
    /// </summary>
    public class UserDraftService : AtomCRUDService<UserDraft, int>
    {

        /// <inheritdoc />
        /// <summary>
        /// constructor
        /// </summary>
        public UserDraftService(ApiContext context, short bid, ITaskQueuer taskQueuer, ITenantDataCache cache, RemoteLogger logger, IRTMPushClient rtmClient, IMigrationHelper migrationHelper)
            : base(context, bid, taskQueuer, cache, logger, rtmClient, migrationHelper)
        {
        }

        /// <summary>
        /// Returns array of ChildServiceAssociations for the type
        /// </summary>
        /// <returns></returns>
        public override IChildServiceAssociation<UserDraft, int>[] GetChildAssociations()
        {
            return new IChildServiceAssociation<UserDraft, int>[]
            {

            };
        }

        public override void DoBeforeValidate(UserDraft newModel)
        {

        }

        /// <inheritdoc />
        /// <summary>
        /// GetIncludes
        /// </summary>
        public override string[] GetIncludes(IExpandIncludes includes = null)
        {
            return new string[] { };
        }

        /// <inheritdoc />
        /// <summary>
        /// WherePrimary
        /// </summary>
        public override IQueryable<UserDraft> WherePrimary(IQueryable<UserDraft> query, int ID)
        {
            return query.WherePrimary(this.BID, ID);
        }

        /// <summary>
        /// DoAfterAddAsync
        /// </summary>
        /// <param name="model"></param>
        /// <param name="tempGuid"></param>
        /// <returns></returns>
        protected override async Task DoAfterAddAsync(UserDraft model, Guid? tempGuid)
        {
            var docman = this.GetDocumentManager(model.ID, ClassType.UserDraft, BucketRequest.Data);
            await docman.UploadTextAsync("draft.json", model.ObjectJSON, model.ClassTypeID, model.ID, "text/json");
            await base.DoAfterAddAsync(model, tempGuid);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ct"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public async Task DeletePendingDrafts(int ct, int ID)
        {
            if (ct != (int)ClassType.UserDraft)
            {
                List<UserDraft> allPending = await this.ctx.UserDraft.Where(x => x.BID == this.BID && x.ObjectCTID == ct && x.ObjectID == ID).ToListAsync();
                foreach(UserDraft d in allPending)
                {
                    var docman = this.GetDocumentManager(d.ID, ClassType.UserDraft, BucketRequest.Data);
                    await docman.DeleteDocumentAsync("draft.json");
                    await this.DeleteAsync(d);
                }
            }
        }

        /// <summary>
        /// Returns the UserDraft for the given filters
        /// </summary>
        /// <param name="filters">UserDraft Filters</param>
        /// <param name="userID">The calling User's ID</param>
        /// <returns></returns>
        public async Task<List<UserDraft>> GetWithFiltersAsync(UserDraftFilters filters, int? userID)
        {
            if (filters != null)
                return await this.GetWhere(filters.WherePredicates(userID));
            else
                return await this.GetAsync(null);
        }
    }
}
