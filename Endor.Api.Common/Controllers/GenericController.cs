﻿using Endor.Api.Common.Services;
using Endor.EF;
using Endor.Logging.Client;
using Endor.RTM;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Controllers
{
    /// <summary>
    /// Generic Controller
    /// </summary>
    /// <typeparam name="M">Model object</typeparam>
    /// <typeparam name="S">Model Service</typeparam>
    /// <typeparam name="I">ID type</typeparam>
    public abstract class GenericController<M, S, I> : Controller
        where M : class
        where S : BaseGenericService<M>
        where I : struct, IConvertible
    {
        /// <summary>
        /// Lazy loaded service
        /// </summary>
        protected readonly Lazy<S> _lazyService;
        /// <summary>
        /// Remote Logger
        /// </summary>
        protected RemoteLogger _logger;
        /// <summary>
        /// RTM Client
        /// </summary>
        protected IRTMPushClient _rtmClient;
        /// <summary>
        /// Model Service
        /// </summary>
        protected S _service { get { return _lazyService.Value; } }

        /// <summary>
        /// Generic Controller
        /// </summary>
        /// <param name="context">API Context</param>
        /// <param name="logger">Remote Logger</param>
        /// <param name="rtmClient">RTM Client</param>
        public GenericController(ApiContext context, RemoteLogger logger, IRTMPushClient rtmClient)
        {
            this._lazyService = AtomGenericService<M, I>.CreateService<S>(context);
            this._logger = logger;
            this._rtmClient = rtmClient;
        }
    }
}
