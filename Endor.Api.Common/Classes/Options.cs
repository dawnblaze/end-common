﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// option
    /// </summary>
    public class Options
    {
        /// <summary>
        /// ID of an option
        /// </summary>
        public int? OptionID { get; set; }
        /// <summary>
        /// name of option
        /// </summary>
        public string OptionName { get; set; }
        /// <summary>
        /// value of option
        /// </summary>
        public string Value { get; set; }
    }
}
