﻿using Endor.Api.Common.Services;
using Endor.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// A filter object that exposes a way to get an array of predicates
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IQueryFilters<T>
    {
        /// <summary>
        /// An array of predicates used with <see cref="ServiceUtilities.WhereAll{T}(IQueryable{T}, Expression{Func{T, bool}}[])"/> to filter queries
        /// </summary>
        /// <returns></returns>
        Expression<Func<T, bool>>[] WherePredicates();
    }

    /// <summary>
    /// interface for signalling that this query filter object needs 
    /// some async prep, like pulling options
    /// </summary>
    public interface IQueryFilterAsyncPrepper
    {
        /// <summary>
        /// hook for before select
        /// used when clauses need extra information
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="ctx"></param>
        /// <returns></returns>
        Task DoBeforeSelectAsync(short bid, ApiContext ctx);
    }

    /// <summary>
    /// Mapping function from full to simple
    /// In order for EF to map correctly, we have to Select() and pass in an anonymous function in the same scope
    /// otherwise it doesn't work
    /// </summary>
    /// <typeparam name="T">The "full" entity</typeparam>
    /// <typeparam name="S">The simple entity</typeparam>
    public interface IQueryFilterMapper<T, S>
    {
        /// <summary>
        /// mapping function that changes an iqueryable to an ienumerable
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        IEnumerable<S> Map(IQueryable<T> source);
    }
}
