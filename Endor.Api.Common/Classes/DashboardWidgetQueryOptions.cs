﻿using Newtonsoft.Json;
using System;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// DashboardWidgetQuery Params
    /// </summary>
    public class DashboardWidgetQueryOptions
    {
        /// <summary>
        /// Employee ID to Filter on - Optional
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// The starting DateTime for the Query - Optional
        /// </summary>
        public DateTime? StartDT { get; set; }

        /// <summary>
        /// The ending DateTime for the Query - Optional
        /// </summary>
        public DateTime? EndDT { get; set; }

        /// <summary>
        /// The StatusID for the Query.  (This may refer to different Statuses for different queries.) - Optional
        /// </summary>
        public int? StatusID { get; set; }

        /// <summary>
        /// Instantiates a DashboardWidgetQueryOptions from a json string
        /// </summary>
        /// <param name="S"></param>
        /// <returns></returns>
        public static DashboardWidgetQueryOptions Deserialize(string S)
        {
            if (string.IsNullOrWhiteSpace(S))
                return null;

            return JsonConvert.DeserializeObject<DashboardWidgetQueryOptions>(S);
        }
    }
}
