namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// true(1), false(0), only(2)
    /// </summary>
    public enum TrueFalseOnly
    {
        /// <summary>
        /// 1
        /// </summary>
        True = 1,
        
        /// <summary>
        /// 0
        /// </summary>        
        False = 0,
        
        /// <summary>
        /// 2
        /// </summary>        
        Only = 2
    }
}