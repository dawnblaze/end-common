﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Services
{
    /// <summary>
    /// Service response 
    /// </summary>
    public class ServiceResponse
    {
        /// <summary>
        /// True if record is not found
        /// </summary>
        public bool IsNotFound { get; set; }
        /// <summary>
        /// True if service method was successful
        /// </summary>
        public bool Success { get; set; }
        /// <summary>
        /// True if service method has an error
        /// </summary>
        public bool HasError { get; set; }
        /// <summary>
        /// Filled with any message sent back on error
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Contains the response object from the service
        /// </summary>
        public object Value { get; set; }
    }
}
