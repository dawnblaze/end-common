﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// utility class for <see cref="string"/> extensions
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// takes a pascal case string and adds spaces between end of word and start of word
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string PascalCaseStringSpacer(this string self)
        {
            if (String.IsNullOrWhiteSpace(self))
                return self;
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < self.Length; i++)
            {
                // if first character always append
                if (i == 0)
                    sb.Append(self[i]);
                // if character is uppercase and previous character is lowercase append with space in front
                else if (self[i] >= 65 && self[i] <= 90 && self[i - 1] >= 97 && self[i - 1] <= 122)
                    sb.Append(" " + self[i]);
                // if character is lowercase append
                else
                    sb.Append(self[i]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns true if a string is not null, empty, or whitespace
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool HasValue(this string self)
        {
            return !string.IsNullOrWhiteSpace(self);
        }
    }
}
