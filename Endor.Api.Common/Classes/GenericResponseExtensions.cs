﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// extensions for <see cref="IGenericResponse"/> interface
    /// </summary>
    public static class GenericResponseExtensions
    {
        /// <summary>
        /// returns an <see cref="IActionResult"/> wrapping the IGenericResponse
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        public static IActionResult ToResult(this IGenericResponse response)
        {
            if (response == null)
                return new StatusCodeResult(500);

            if (response.Success)
                return new OkObjectResult(response);

            if (!string.IsNullOrWhiteSpace(response.ErrorMessage) && response.ErrorMessage.ToLower().Contains("not found"))
                return new NotFoundObjectResult(response);

            return new BadRequestObjectResult(response);
        }
    }
}
