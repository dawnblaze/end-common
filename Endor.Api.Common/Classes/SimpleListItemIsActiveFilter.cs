﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// An IsActive filter for simple list endpoints
    /// </summary>
    public class SimpleListItemIsActiveFilter<T, I> : IIsActiveQueryFilter<T>
        where T: SimpleListItem<I>
        where I : struct, IConvertible
    {
        /// <summary>
        /// When non-null, filters to value
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// returns an array of predicates, may be an array of length 0
        /// </summary>
        /// <returns></returns>
        public virtual Expression<Func<T, bool>>[] WherePredicates()
        {
            List<Expression<Func<T, bool>>> predicates = new List<Expression<Func<T, bool>>>();

            if (IsActive.HasValue)
            {
                predicates.Add(gla => gla.IsActive == IsActive.Value);
            }

            return predicates.ToArray();
        }
    }
}
