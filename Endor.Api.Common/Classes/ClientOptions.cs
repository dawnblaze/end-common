﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// Client Options
    /// </summary>
    public class ClientOptions
    {
        /// <summary>
        /// Google API Key
        /// </summary>
        public string GoogleAPIKey { get; set; }
        /// <summary>
        /// Used to define whether to use assisted address lookup
        /// </summary>
        public bool UseAssistedAddressLookup { get; set; }
    }
}
