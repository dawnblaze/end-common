﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// A Generic Response Interface
    /// </summary>
    public interface IGenericResponse
    {
        /// <summary>
        /// Response message
        /// </summary>
        string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        bool Success { get; set; }

        /// <summary>
        /// If there is an error
        /// </summary>
        bool HasError { get; }

        /// <summary>
        /// Error Message, if there was an error
        /// </summary>
        string ErrorMessage { get; set; }
    }
}
