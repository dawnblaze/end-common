﻿using Endor.DocumentStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common
{
    /// <summary>
    /// Context for Storage
    /// </summary>
    public class StorageContext : IStorageContext
    {
        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; private set; }

        /// <summary>
        /// Readonly Storage Bin
        /// </summary>
        public readonly StorageBin _bin;

        /// <summary>
        /// Storage Bin
        /// </summary>
        public StorageBin Bin { get { return _bin; } }

        /// <summary>
        /// Document Management Id
        /// </summary>
        public DMID ID { get; private set; }

        /// <summary>
        /// Bucket Request
        /// </summary>
        public BucketRequest BucketRequest { get; private set; }

        /// <summary>
        /// Is this Deleted
        /// </summary>
        public DateTime? Deleted { get; private set; }

        /// <summary>
        /// Constructor for Storage Context
        /// </summary>
        public StorageContext(short BID, BucketRequest bucket, DMID id)
        {
            this.BID = BID;
            this.BucketRequest = bucket;
            this.ID = id;
            this._bin = id.IsTemporary ? StorageBin.Temp : StorageBin.Permanent;
        }

        /// <summary>
        /// Constructor for Storage Context
        /// </summary>
        public StorageContext(short BID, BucketRequest bucket, DMID id, DateTime deleted)
        {
            this.BID = BID;
            this.BucketRequest = bucket;
            this.ID = id;
            this.Deleted = deleted;
            this._bin = StorageBin.Trash;
        }
    }
}
