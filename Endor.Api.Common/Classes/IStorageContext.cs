﻿using Endor.DocumentStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common
{
    /// <summary>
    /// Storage Context Interface
    /// </summary>
    public interface IStorageContext
    {
        /// <summary>
        /// Business ID
        /// </summary>
        short BID { get; }

        /// <summary>
        /// Storage Bin
        /// </summary>
        StorageBin Bin { get; }

        /// <summary>
        /// Document Management Id
        /// </summary>
        DMID ID { get; }

        /// <summary>
        /// Bucket Request
        /// </summary>
        BucketRequest BucketRequest { get; }

        /// <summary>
        /// Is this Deleted
        /// </summary>
        DateTime? Deleted { get; }
    }
}