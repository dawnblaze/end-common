﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// concrete Generic response object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class GenericResponse<T> : IGenericResponse 
        where T : class
    {
        /// <summary>
        /// Response data
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// If there is an error
        /// </summary>
        public bool HasError { get { return !this.Success; } }

        /// <summary>
        /// Error message, if there is an error
        /// </summary>
        public string ErrorMessage { get; set; }

    }
}
