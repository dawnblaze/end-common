﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// Response for Getting an Option Value
    /// </summary>
    public class GetOptionValueResponse : IGenericResponse
    {
        /// <summary>
        /// option value result
        /// </summary>
        public OptionValue Value { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// true if has error, otherwise false
        /// </summary>
        public bool HasError { get { return !this.Success; } }

        /// <summary>
        /// error message encountered for this operation
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// option as boolean
        /// </summary>
        public bool ValueAsBoolean(bool defaultValue = false)
        {
            if (Value == null)
                return defaultValue;

            string s = Value.Value;

            if (string.IsNullOrWhiteSpace(s))
                return false;

            var trueBeginnings = new List<char> { '1', 'T', 't', 'Y', 'y' };
            var firstLetter = s.TrimStart().First();

            return trueBeginnings.Contains(firstLetter);
        }
    }
}
