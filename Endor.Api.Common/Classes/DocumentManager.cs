﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.DocumentStorage.Models;
using Endor.AzureStorage;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.IO.Compression;
using System.Net.Http;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Png;
using SixLabors.ImageSharp.Formats.Jpeg;
using SixLabors.ImageSharp.Formats.Gif;
using SixLabors.ImageSharp.Formats.Bmp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Processing;
using System.Net;
using System.Net.Mime;
using System.Text;

namespace Endor.Api.Common
{
    /// <summary>
    /// Document manager for blob storage with entities
    /// </summary>
    public class DocumentManager : IDocumentManager
    {
        private readonly ITenantDataCache _cache;
        private readonly IStorageContext _ctx;
        private const string ZipContentType = "application/zip";
        private const string thumbnailPrefix = "thumb-";
        private const string thumbnailFolder = ".thumbnails";
        private const string defaultImageFileName = "Image";
        private const string knownImageTypes = "jpg,jpeg,png,bmp,gif,tiff,webp";
        private const string defaultImageThumbnailFileName = thumbnailFolder + "/" + defaultImageFileName;

        /// <summary>
        /// Document Management (Persistent Storage)
        /// </summary>
        public DocumentManager(ITenantDataCache cache, IStorageContext ctx)
        {
            this._cache = cache;
            this._ctx = ctx;
        }

        private async Task<EntityStorageClient> GetStorageClientAsync(short bid)
        {
            return new EntityStorageClient((await _cache.Get(bid)).StorageConnectionString, bid);
        }

        /// <summary>
        /// Tests if entity at bid/id has default image
        /// </summary>
        /// <returns></returns>
        public async Task<bool> HasDefaultImage()
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            return await client.HasDefaultImage(new DMID() { id = _ctx.ID.id, ctid = _ctx.ID.ctid });
        }


        /// <summary>
        /// ReadText of a blob
        /// </summary>
        /// <param name="dmItem"></param>
        /// <returns></returns>
        public async Task<string> ReadTextAsync(DMItem dmItem){
            string text = "";
            var client = await GetStorageClientAsync(_ctx.BID);
            using(var blobStream = await client.GetStream(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, Path.Combine(dmItem.Path, dmItem.Name))){
                using(var reader = new StreamReader(blobStream)){
                    text = reader.ReadToEnd();
                }
            }
            
            return text;
        }

        /// <summary>
        /// Copies documents into a target id/ctid
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="destination"></param>
        /// <param name="destinationID"></param>
        /// <param name="destinationCTID"></param>
        /// <returns></returns>
        public async Task<int> CopyDocumentsAsync(string[] documents, string destination, int? destinationID, int? destinationCTID)
        {
            var client = await GetStorageClientAsync(_ctx.BID);

            var fromBucket = _ctx.BucketRequest.ToBucket();
            DMID effectiveToID = _ctx.ID;
            if (destinationID.HasValue || destinationCTID.HasValue)
            {
                effectiveToID = new DMID()
                {
                    id = destinationID,
                    ctid = destinationCTID
                };
            }
            return await client.Copy(_ctx.Bin, fromBucket, _ctx.ID, documents, _ctx.Bin, fromBucket, effectiveToID, destination);
        }

        /// <summary>
        /// Deletes all documents in all buckets
        /// </summary>
        /// <returns></returns>
        public async Task<int> DeleteAllDocumentsAsync()
        {
            var client = await GetStorageClientAsync(_ctx.BID);

            return await client.DeleteAll(_ctx.Bin, _ctx.ID);
        }

        /// <summary>
        /// Delete a single document
        /// </summary>
        /// <param name="document"></param>
        /// <returns></returns>
        public async Task<int> DeleteDocumentAsync(string document)
        {
            if (_ctx.BucketRequest == BucketRequest.All)
                return await DeleteAllDocumentsAsync();

            return await DeleteFileWithThumbnail(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, document);
        }

        /// <summary>
        /// Delete multiple documents
        /// </summary>
        /// <param name="documents">Documents to Delete</param>
        /// <returns></returns>
        public async Task<int> DeleteDocumentsAsync(string[] documents)
        {
            if (_ctx.BucketRequest == BucketRequest.All)
                return await DeleteAllDocumentsAsync();

            var client = await GetStorageClientAsync(_ctx.BID);

            //if (!await FilesExist(client, documents)) {
            //    return -404;
            //}

            int results = 0;
            foreach (string document in documents)
            {
                results += await DeleteFileWithThumbnail(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, document);
            }
            return results;
        }

        /// <summary>
        /// Add a folder
        /// </summary>
        /// <param name="pathAndFileName"></param>
        /// <param name="uploaderID"></param>
        /// <param name="uploaderCTID"></param>
        /// <param name="permissionOptions"></param>
        /// <returns></returns>
        public async Task<DMItem> AddFolder(string pathAndFileName, int uploaderID, int uploaderCTID, DocumentPermissionOptions permissionOptions = null)
        {
            if (_ctx.BucketRequest == BucketRequest.All)
                throw new InvalidOperationException("Cannot add a folder to all buckets");

            var client = await GetStorageClientAsync(_ctx.BID);

            var tskThumbsFolder = client.AddFolder(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, thumbnailFolder+"/"+pathAndFileName.Trim('/', '\\'), uploaderID, uploaderCTID);
            var tskActualFolder = client.AddFolder(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, pathAndFileName.Trim('/', '\\'), uploaderID, uploaderCTID, permissionOptions);
            await Task.WhenAll(tskThumbsFolder, tskActualFolder);
            return await tskActualFolder;
        }

        /// <summary>
        /// Duplicate Multiple Documents
        /// </summary>
        /// <param name="documents"></param>
        /// <returns></returns>
        public async Task<int> DuplicateDocumentsAsync(string[] documents)
        {
            return await DuplicateFileWithThumbnail(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, documents);
        }

        /// <summary>
        /// Get all requested documents
        /// </summary>
        /// <returns></returns>
        public async Task<List<DMItem>> GetDocumentsAsync()
        {
            if(this._ctx.ID.IsEntityLevel){
                await this.InitializeAllAsync();
            }
            
            return await GetDocumentListWithThumbnail(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID);
        }

        /// <summary>
        /// Initialiaze Storage Client
        /// </summary>
        /// <returns></returns>
        public async Task<int> InitializeAllAsync()
        {
            var client = await GetStorageClientAsync(_ctx.BID);

            return await client.Initialize(_ctx.Bin, _ctx.ID);
        }

        /// <summary>
        /// Move a folder to a new destination
        /// </summary>
        /// <param name="document"></param>
        /// <param name="destination"></param>
        /// <param name="destinationCTID"></param>
        /// <param name="destinationID"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        public async Task<int> MoveFolderAsync(string document, string destination, int? destinationCTID, int? destinationID, string designation)
        {
            return await MoveFoldersAsync(new string[] { document }, destination, destinationCTID, destinationID, designation);
        }

        /// <summary>
        /// Move multiple folders to a new destination
        /// </summary>
        /// <remarks>
        /// Warning: API cannot Move between buckets or bins
        /// client does not allow designation on Move
        /// </remarks>
        /// <param name="documents"></param>
        /// <param name="destination"></param>
        /// <param name="destinationCTID"></param>
        /// <param name="destinationID"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        public async Task<int> MoveFoldersAsync(string[] documents, string destination, int? destinationCTID, int? destinationID, string designation)
        {
            Bucket fromBucket = _ctx.BucketRequest.ToBucket();
            DMID effectiveToID = GetEffectiveDMID(destinationCTID, destinationID);
            return await MoveFoldersWithThumbnail(_ctx.Bin, fromBucket, _ctx.ID, documents, _ctx.Bin, fromBucket, effectiveToID, destination);
        }

        private DMID GetEffectiveDMID(int? destinationCTID, int? destinationID)
        {
            DMID effectiveToID = _ctx.ID;
            if (destinationID.HasValue || destinationCTID.HasValue)
            {
                effectiveToID = new DMID()
                {
                    id = destinationID,
                    ctid = destinationCTID
                };
            }

            return effectiveToID;
        }

        /// <summary>
        /// Move a file to a new destination
        /// </summary>
        /// <param name="document"></param>
        /// <param name="destination"></param>
        /// <param name="destinationCTID"></param>
        /// <param name="destinationID"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        public async Task<int> MoveFileAsync(string document, string destination, int? destinationCTID, int? destinationID, string designation)
        {
            return await MoveFilesAsync(new string[] { document }, destination, destinationCTID, destinationID, designation);
        }

        /// <summary>
        /// Move files to a new destination
        /// </summary>
        /// <remarks>
        /// Warning: API cannot Move between buckets or bins
        /// client does not allow designation on Move
        /// </remarks>
        /// <param name="documents"></param>
        /// <param name="destination"></param>
        /// <param name="destinationCTID"></param>
        /// <param name="destinationID"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        public async Task<int> MoveFilesAsync(string[] documents, string destination, int? destinationCTID, int? destinationID, string designation)
        {
            Bucket fromBucket = _ctx.BucketRequest.ToBucket();
            DMID effectiveToID = GetEffectiveDMID(destinationCTID, destinationID);
            return await MoveWithThumbnail(_ctx.Bin, fromBucket, _ctx.ID, documents, _ctx.Bin, fromBucket, effectiveToID, destination);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pathAndFileName"></param>
        /// <param name="url"></param>
        /// <param name="designation"></param>
        /// <param name="uploaderID"></param>
        /// <param name="uploaderCTID"></param>
        /// <exception cref="System.ArgumentException">Invalid argument supplied</exception>
        /// <exception cref="System.FormatException">Url specified is not well-formed</exception>
        /// <returns></returns>
        public async Task<DMItem> AddDocumentFromUrlAsync(string pathAndFileName, string url, string designation, int uploaderID, int uploaderCTID)
        {
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                throw new FormatException("url must be a well-formed");

            HttpClient httpClient = new HttpClient();
            if (pathAndFileName.ToLower().EndsWith("null") || pathAndFileName.ToLower().EndsWith("undefined"))
            {
                Uri uri = new Uri(url);
                string predictedFileName = System.IO.Path.GetFileName(uri.LocalPath);
                if (!predictedFileName.Contains('.'))
                {
                    WebClient webclient = new WebClient();
                    var webstream = webclient.OpenRead(url);
                    string contentType = webclient.ResponseHeaders["Content-Type"];
                    predictedFileName = predictedFileName + "." + contentType.Substring(contentType.IndexOf('/')+1);
                    webclient.Dispose();
                }

                pathAndFileName = pathAndFileName + "__end__";
                pathAndFileName = pathAndFileName.Replace("null__end__", "").Replace("undefined__end__", "") + predictedFileName;
                
            }

            try
            {
                HttpResponseMessage getExternalResponse = await httpClient.GetAsync(url);

                if (!getExternalResponse.IsSuccessStatusCode)
                    throw new ArgumentException($"Invalid URL: Request for {url} returned {getExternalResponse.StatusCode}");

                long uploadSizeCap = 10000000;

                if (getExternalResponse.Content.Headers.ContentLength > uploadSizeCap)
                    throw new ArgumentException($"Document at URL is too large");

                var client = await GetStorageClientAsync(_ctx.BID);

                DMItem result = await AddFileWithThumbnail(await getExternalResponse.Content.ReadAsStreamAsync(),
                    _ctx.Bin,
                    _ctx.BucketRequest.ToBucket(),
                    _ctx.ID,
                    pathAndFileName,
                    getExternalResponse.Content.Headers.ContentType.MediaType,
                    uploaderID,
                    uploaderCTID);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Keeps any objects associated with the model
        /// </summary>
        /// <param name="temp"></param>
        /// <param name="destinationID"></param>
        /// <param name="destinationCTID"></param>
        /// <returns></returns>
        public async Task<int> PersistDocumentsAsync(Guid temp, int destinationID, int destinationCTID)
        {
            var client = await GetStorageClientAsync(_ctx.BID);

            return await client.MakePermanent(temp, new DMID()
            {
                id = destinationID,
                ctid = destinationCTID
            });
        }


        /// <summary>
        /// Renames document via Move
        /// </summary>
        /// <remarks>
        /// Warning: client does not allow designation on Rename
        /// </remarks>
        /// <param name="documentPathAndFile"></param>
        /// <param name="newDocumentPathAndFile"></param>
        /// <param name="designation"></param>
        /// <returns>number of records renamed</returns>
        public async Task<int> RenameFileAsync(string documentPathAndFile, string newDocumentPathAndFile, string designation)
        {
            return await RenameFileWithThumbnail(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, documentPathAndFile, newDocumentPathAndFile);
        }

        /// <summary>
        /// Rename a Folder in Storage
        /// </summary>
        /// <remarks>
        /// Warning: client does not allow designation on Rename
        /// </remarks>
        /// <param name="folderPathAndFile"></param>
        /// <param name="newFolderPathAndFile"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        public async Task<int> RenameFolderAsync(string folderPathAndFile, string newFolderPathAndFile, string designation)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            return await RenameFolderWithThumbnail(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, folderPathAndFile, newFolderPathAndFile);
        }

        /// <summary>
        /// Replaces a document via Upload
        /// </summary>
        /// <param name="document"></param>
        /// <param name="form"></param>
        /// <param name="designation"></param>
        /// <param name="uploaderID"></param>
        /// <param name="uploaderCTID"></param>
        /// <returns></returns>
        public async Task<DMItem> ReplaceDocumentAsync(string document, IFormCollection form, string designation, int uploaderID, int uploaderCTID)
        {
            return await UploadDocumentAsync(document, form, designation, uploaderID, uploaderCTID);
        }

        /// <summary>
        /// Change Document Metadata - Not Implemented
        /// </summary>
        /// <param name="document"></param>
        /// <param name="designation"></param>
        /// <returns></returns>
        public Task<DMItem> UpdateDocumentMetadataAsync(string document, string designation)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Uploads single file using folder/file of "document" parameter
        /// </summary>
        /// <remarks>
        /// Warning: client does not allow designation on upload
        /// </remarks>
        /// <param name="document"></param>
        /// <param name="form"></param>
        /// <param name="designation"></param>
        /// <param name="uploaderID"></param>
        /// <param name="uploaderCTID"></param>
        /// <returns></returns>
        public async Task<DMItem> UploadDocumentAsync(string document, IFormCollection form, string designation, int uploaderID, int uploaderCTID)
        {
            if (form.Files.Count < 1 || form.Files.Count > 1)
                throw new ArgumentException("Can only upload one file at a time");

            var file = form.Files.FirstOrDefault();
            var metadata = new Dictionary<string, string>();

            foreach (string key in form.Keys)
            {
                metadata.Add(key, form[key]);
            }

            using (var filestream = file.OpenReadStream())
            {
                var uploadItem = await AddFileWithThumbnail(
                    filestream,
                    _ctx.Bin,
                    _ctx.BucketRequest.ToBucket(),
                    _ctx.ID,
                    document,
                    file.ContentType,
                    uploaderID,
                    uploaderCTID,
                    false,
                    metadata);
                return uploadItem;
            }
        }

        /// <summary>
        /// UploadTextAsync
        /// </summary>
        /// <param name="pathAndFilename"></param>
        /// <param name="contents"></param>
        /// <param name="contentType"></param>
        /// <param name="uploaderID"></param>
        /// <param name="uploaderCTID"></param>
        /// <returns></returns>
        public async Task<DMItem> UploadTextAsync(string pathAndFilename, string contents, int uploaderID, int uploaderCTID, string contentType = "text/plaintext")
        {
            var formCollection = new FormCollection(new Dictionary<string, Microsoft.Extensions.Primitives.StringValues>(), null);
            // convert string to stream
            byte[] byteArray = Encoding.ASCII.GetBytes( contents );
            using (MemoryStream stream = new MemoryStream( byteArray ))
            {
                return await this.AddFileWithThumbnail(stream, _ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, pathAndFilename, contentType, uploaderID, uploaderCTID);
            }
        }

        /// <summary>
        /// Update the Image for the multiple files
        /// </summary>
        /// <param name="files"></param>
        /// <param name="uploaderID"></param>
        /// <param name="uploaderCTID"></param>
        /// <returns></returns>
        public async Task<DMItem> UploadDefaultImage(IFormFileCollection files, int uploaderID, int uploaderCTID)
        {
            if (files == null || files.Count == 0)
                throw new ArgumentException("No files were found in FormSubmit");
            else if (files.Count > 1)
                throw new ArgumentException("Can only set one image as default");

            var file = files.FirstOrDefault();

            var client = await GetStorageClientAsync(_ctx.BID);

            using (var fileStream = file.OpenReadStream())
            {
                DMItem defaultImage;
                if (_ctx.ID.IsTemporary)
                {
                    defaultImage = await client.StoreTemporaryImage(fileStream, _ctx.ID.guid.Value, file.ContentType, uploaderID, uploaderCTID);
                }
                else if (_ctx.ID.IsEntityLevel)
                {
                    defaultImage = await client.StoreDefaultImage(fileStream, _ctx.ID, file.ContentType, uploaderID, uploaderCTID);
                }
                else
                {
                    throw new InvalidOperationException("Invalid upload: must be to temporary or entity storage");
                }
                if (defaultImage != null)
                {
                    await AddThumbnail(fileStream, file.ContentType, uploaderID, uploaderCTID, client, thumbnailFileName: defaultImageThumbnailFileName, fileName: null);
                }
                return defaultImage;
            }
        }

        /// <summary>
        /// Delete the Default Image
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteDefaultImage()
        {
            var client = await GetStorageClientAsync(_ctx.BID);

            int deleted = await client.DeleteFile(StorageBin.Permanent, Bucket.Data, _ctx.ID, defaultImageFileName);
            deleted += await client.DeleteFile(StorageBin.Permanent, Bucket.Data, _ctx.ID, defaultImageThumbnailFileName);

            return deleted > 0;
        }

        /// <summary>
        /// Uploads multiple files to folder "destination" using each file's Name as the name of the file
        /// </summary>
        /// <param name="files"></param>
        /// <param name="destination"></param>
        /// <param name="designation"></param>
        /// <param name="uploaderCTID"></param>
        /// <param name="uploaderID"></param>
        /// <returns></returns>
        public async Task<List<DMItem>> UploadDocumentsAsync(IFormFileCollection files, string destination, string designation, int uploaderID, int uploaderCTID)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            List<DMItem> added = new List<DMItem>();

            foreach (var file in files)
            {
                string thisFileDocumentName = System.IO.Path.GetFileName(file.Name);
                string thisFilePath = System.IO.Path.Combine(destination, thisFileDocumentName);

                using (var filestream = file.OpenReadStream())
                {
                    added.Add(await AddFileWithThumbnail(
                        filestream,
                        _ctx.Bin,
                        _ctx.BucketRequest.ToBucket(),
                        _ctx.ID,
                        thisFilePath,
                        file.ContentType,
                        uploaderID,
                        uploaderCTID));
                }
            }

            return added;
        }

        /// <summary>
        /// Clones documents from all buckets
        /// </summary>
        /// <param name="destId">Destination ID</param>
        /// <param name="includeSpecialFolderBlobName">Set to true if you also need the special '_' file in the .thumbnails folder copied</param>
        /// <returns></returns>
        public async Task<int> CloneAllDocumentsAsync(int destId, bool includeSpecialFolderBlobName = false)
        {
            int documentsCopied = 0;
            documentsCopied += await CloneAllBucketBlobsAsync(destId, Bucket.Data, includeSpecialFolderBlobName);
            documentsCopied += await CloneAllBucketBlobsAsync(destId, Bucket.Documents, includeSpecialFolderBlobName);
            documentsCopied += await CloneAllBucketBlobsAsync(destId, Bucket.Reports, includeSpecialFolderBlobName);
            return documentsCopied;
        }

        /// <summary>
        /// Clones all blobs from a specific bucket
        /// </summary>
        /// <param name="destId">Destination ID</param>
        /// <param name="bucket">Bucket to Clone from</param>
        /// <param name="includeSpecialFolderBlobName">Set to true if you also need the special '_' file in the .thumbnails folder copied</param>
        /// <returns></returns>
        public async Task<int> CloneAllBucketBlobsAsync(int destId, Bucket bucket, bool includeSpecialFolderBlobName = false)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            DMID target = new DMID() { id = destId, ctid = _ctx.ID.ctid };
            List<DMItem> items = await client.GetDocumentList(StorageBin.Permanent, bucket, _ctx.ID, useFlatDocumentListing: true, includeSpecialFolderBlobName: true);

            int documentsCopied = 0;
            documentsCopied += await client.Copy(StorageBin.Permanent, bucket, _ctx.ID, items.Select(t => t.Name).ToArray(), toBin: StorageBin.Permanent, toBucket: bucket, toID: target);

            return documentsCopied;
        }

        private async Task ZipItem(EntityStorageClient client, ZipArchive fullZip, DMItem item, string root)
        {
            if (item.IsFolder)
            {
                if (item.Contents != null)
                {
                    foreach (DMItem child in item.Contents)
                    {
                        await ZipItem(client, fullZip, child, Path.Combine(root, item.Name));
                    }
                }
            }
            else
            {
                await AddBlobStreamToArchive(client, fullZip, Path.Combine(root, item.Name));
            }
        }

        private async Task ZipBucket(EntityStorageClient client, ZipArchive fullZip, Bucket b)
        {
            var all = await client.GetDocumentList(_ctx.Bin, b, _ctx.ID);
            foreach (var item in all)
            {
                await ZipItem(client, fullZip, item, "");
            }
        }

        /// <summary>
        /// Compress all the documents
        /// </summary>
        /// <param name="userLinkID"></param>
        /// <param name="userLinkCTID"></param>
        /// <returns></returns>
        public async Task<string> ZipAllDocumentsAsync(int userLinkID, int userLinkCTID)
        {
            var client = await GetStorageClientAsync(_ctx.BID);

            DMItem zipItem;

            using (var memoryStream = new MemoryStream())
            {
                using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    await ZipBucket(client, archive, Bucket.Documents);
                    await ZipBucket(client, archive, Bucket.Data);
                    await ZipBucket(client, archive, Bucket.Reports);
                }

                memoryStream.Seek(0, SeekOrigin.Begin);
                zipItem = await client.AddFile(
                    memoryStream,
                    _ctx.Bin,
                    Bucket.Data,
                    _ctx.ID,
                    Guid.NewGuid().ToString() + "-all.zip",
                    ZipContentType,
                    userLinkID,
                    userLinkCTID);
            }

            return (await client.GetCanonicalURLWithReadSASTokenAsync(zipItem)).ToString();
        }

        /// <summary>
        /// Compress the list of documents
        /// </summary>
        /// <param name="documents"></param>
        /// <param name="userLinkID"></param>
        /// <param name="userLinkCTID"></param>
        /// <param name="flatExport"></param>
        /// /// <returns></returns>
        public async Task<string> ZipDocumentsAsync(string[] documents, int userLinkID, int userLinkCTID, bool flatExport = false)
        {
            var client = await GetStorageClientAsync(_ctx.BID);

            DMItem zipItem;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    int entryCount = 0;
                    foreach (string doc in documents)
                    {
                        try
                        {
                            if (doc.EndsWith(EntityStorageClient.SpecialFolderBlobName))
                            {
                                DMItem folderItem = await GetMatchingFolderFromDocumentTreeAsync(client, doc);
                                if (folderItem != null)
                                {
                                    string parentPath = folderItem.Name.TrimEnd('/');
                                    await AddFolderToArchive(client, archive, folderItem, parentPath);
                                    entryCount++;
                                }
                            }
                            else
                            {
                                string archiveName = doc;
                                if (flatExport)
                                    archiveName = doc.Substring(doc.LastIndexOf('/'));

                                await AddBlobStreamToArchive(client, archive, doc, archiveName);
                                entryCount++;
                            }
                        }
                        catch
                        {
                            // blobs that fail to add to stream are caught here.
                           
                        }
                    }

                    if (entryCount == 0)
                        return null;
                }

                memoryStream.Seek(0, SeekOrigin.Begin);

                zipItem = await client.AddFile(memoryStream, _ctx.Bin, Bucket.Data, _ctx.ID, Guid.NewGuid().ToString() + ".zip", ZipContentType, userLinkID, userLinkCTID);
            }

            return (await client.GetCanonicalURLWithReadSASTokenAsync(zipItem)).ToString();
        }

        private async Task<DMItem> GetMatchingFolderFromDocumentTreeAsync(EntityStorageClient client, string folderPath)
        {
            List<DMItem> items = await client.GetDocumentList(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID);
            if (String.IsNullOrWhiteSpace(folderPath) || folderPath == "/" + EntityStorageClient.SpecialFolderBlobName || folderPath == "_")
                return new DMItem()
                {
                    Contents = items,
                    Name = "",
                    IsFolder = true
                };
            else
                return GetMatchingFolderFromDocumentTreeRecursive(items, folderPath);
        }

        private DMItem GetMatchingFolderFromDocumentTreeRecursive(List<DMItem> items, string folderPath)
        {
            foreach (var item in items)
            {
                if (item.IsFolder)
                {
                    if (folderPath.TrimStart('/') == item.AbsolutePath())
                        return item;
                    else if (item.Contents != null && item.Contents.Count > 0)
                    {
                        DMItem match = GetMatchingFolderFromDocumentTreeRecursive(item.Contents, folderPath);
                        if (match != null)
                            return match;
                    }
                }
            }

            return null;
        }

        private async Task AddFolderToArchive(EntityStorageClient client, ZipArchive archive, DMItem folderItem, string parentPath)
        {
            if (parentPath == null)
                parentPath = "";
            else if (!parentPath.EndsWith('/'))
                parentPath += '/';

            if (parentPath != "/")
                archive.CreateEntry(parentPath);

            foreach (DMItem childItem in folderItem.Contents)
            {
                string relativePath = $"{parentPath.TrimStart('/')}{childItem.Name.TrimStart('/')}";
                if (childItem.IsFolder)
                {
                    relativePath = relativePath.TrimEnd('/');
                    await AddFolderToArchive(client, archive, childItem, relativePath);
                }
                else
                {
                    await AddBlobStreamToArchive(client, archive, childItem.AbsolutePath(), relativePath);
                }
            }
        }

        private async Task AddBlobStreamToArchive(EntityStorageClient client, ZipArchive archive, string documentPathAndFilename, string archiveEntryPathAndFilename = null)
        {
            if (String.IsNullOrWhiteSpace(archiveEntryPathAndFilename))
            {
                archiveEntryPathAndFilename = documentPathAndFilename;
            }

            using (Stream blobContents = await client.GetStream(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, documentPathAndFilename))
            {
                var zipEntry = archive.CreateEntry(archiveEntryPathAndFilename.TrimStart('/'));

                using (var entryStream = zipEntry.Open())
                //using (var streamWriter = new StreamWriter(entryStream))
                {
                    await blobContents.CopyToAsync(entryStream);
                }
            }
        }

        /// <summary>
        /// Add a file to storage
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="fileName"></param>
        /// <param name="contentType"></param>
        /// <param name="createdByID"></param>
        /// <param name="createdByCTID"></param>
        /// <param name="fullyQualifyURLWithSAS"></param>
        /// <param name="metadata">Additional Metadata to be appended to blob</param>
        /// <returns></returns>
        protected async Task<DMItem> AddFileWithThumbnail(Stream stream, StorageBin storageBin, Bucket bucket, IDMID id, string fileName, string contentType, int createdByID, int createdByCTID, bool fullyQualifyURLWithSAS = false, Dictionary<string, string> metadata = null)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            var result = await client.AddFile(
                stream,
                _ctx.Bin,
                _ctx.BucketRequest.ToBucket(),
                _ctx.ID,
                fileName,
                contentType,
                createdByID,
                createdByCTID,
                false,
                null,
                metadata);
            if (result.IsImageMimeType())
            {
                await AddThumbnail(stream, contentType, createdByID, createdByCTID, client, fileName: fileName, thumbnailFileName: null);
            }
            return result;
        }

        internal async Task AddThumbnail(Stream stream, string contentType, int createdByID, int createdByCTID, EntityStorageClient client, string fileName, string thumbnailFileName)
        {
            if (fileName != null && thumbnailFileName == null)
            {
                thumbnailFileName = GetThumbnailName(fileName);
            }
            else if (fileName == null && thumbnailFileName == null)
            {
                throw new ArgumentException("Either filename or thumbnailfilename required");
            }
            //actual thumbnail generation
            stream.Seek(0, SeekOrigin.Begin);//make ImageSharp read from 0 position
            using (var origImage = Image.Load(stream))
            using (Stream thumbStream = new MemoryStream())
            {
                int targetHeight = 75;
                double aspectRatio = (double)origImage.Height / (double)targetHeight;
                int targetWidth = (int)((double)origImage.Width / aspectRatio);

                origImage.Mutate(img => img.Resize(targetWidth, targetHeight));

                string lowerContentType = contentType.ToLower();
                IImageEncoder encoder;

                if (lowerContentType.Contains("jpg") || lowerContentType.Contains("jpeg"))
                {
                    encoder = new JpegEncoder();
                }
                else if (lowerContentType.Contains("png"))
                {
                    encoder = new PngEncoder();
                }
                else if (lowerContentType.Contains("gif"))
                {
                    encoder = new GifEncoder();
                }
                else
                {
                    encoder = new BmpEncoder();//default MS encoder type
                }
                origImage.Save(thumbStream, encoder);

                //save thumbnail
                await client.AddFile(
                thumbStream,
                _ctx.Bin,
                _ctx.BucketRequest.ToBucket(),
                _ctx.ID,
                thumbnailFileName,
                contentType,
                createdByID,
                createdByCTID);
            }
        }

        //File Operations

        /// <summary>
        /// Move a file with the attached Thumbnail
        /// </summary>
        /// <param name="fromBin"></param>
        /// <param name="fromBucket"></param>
        /// <param name="fromID"></param>
        /// <param name="files"></param>
        /// <param name="toBin"></param>
        /// <param name="toBucket"></param>
        /// <param name="toID"></param>
        /// <param name="toFolder"></param>
        /// <param name="withFolder"></param>
        /// <returns></returns>
        protected async Task<int> MoveWithThumbnail(StorageBin fromBin, Bucket fromBucket, IDMID fromID, string[] files, StorageBin? toBin = null, Bucket? toBucket = null, IDMID toID = null, string toFolder = null, string withFolder = null)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            var thumbnailFiles = files.ToList().Select(file => GetThumbnailName(file)).ToArray();
            //----------------------------------------------thumbnails-----------------------------folder inside thumbnailFolder--
            await client.Move(fromBin, fromBucket, fromID, thumbnailFiles, toBin, toBucket, toID, $"{thumbnailFolder}/{toFolder}");
            return await client.Move(fromBin, fromBucket, fromID, files, toBin, toBucket, toID, toFolder);
        }

        /// <summary>
        /// Rename a file with the attached Thumbnail
        /// </summary>
        /// <param name="bin"></param>
        /// <param name="bucket"></param>
        /// <param name="ID"></param>
        /// <param name="pathAndFileName"></param>
        /// <param name="newPathAndFileName"></param>
        /// <returns></returns>
        protected async Task<int> RenameFileWithThumbnail(StorageBin bin, Bucket bucket, IDMID ID, string pathAndFileName, string newPathAndFileName)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            await client.Rename(bin, bucket, ID, GetThumbnailName(pathAndFileName), thumbnailPrefix + newPathAndFileName);
            return await client.Rename(bin, bucket, ID, pathAndFileName, newPathAndFileName);
        }

        /// <summary>
        /// Duplicate Multiple Files with Thumbnail
        /// </summary>
        /// <param name="bin"></param>
        /// <param name="bucket"></param>
        /// <param name="ID"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        protected async Task<int> DuplicateFileWithThumbnail(StorageBin bin, Bucket bucket, IDMID ID, string[] files)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            var filesAndFoldersToDup = files
                                        .Select(x => {
                                            if (x.EndsWith(EntityStorageClient.SpecialFolderBlobName))
                                            {
                                                return x.TrimEnd(EntityStorageClient.SpecialFolderBlobName).TrimEnd('/');
                                            }
                                            else
                                            {
                                                return x;
                                            }
                                        })
                                        .ToArray();

            var thumbnailsToDupe = files
                                    .Where(f=>
                                        f.EndsWith(EntityStorageClient.SpecialFolderBlobName) ||
                                        knownImageTypes.Contains(f.Substring(f.LastIndexOf(".")+1)))
                                    .Select(f=>this.GetThumbnailName(f)
                                    .TrimEnd('_').TrimEnd('/'))
                                    .ToArray();





            await client.Duplicate(bin, bucket, ID, thumbnailsToDupe);
            return await client.Duplicate(bin, bucket, ID, filesAndFoldersToDup);
        }

        /// <summary>
        /// Delete a file with the attached Thumbnail
        /// </summary>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        protected async Task<int> DeleteFileWithThumbnail(StorageBin storageBin, Bucket bucket, IDMID id, string filename)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            await client.DeleteFile(storageBin, bucket, id, GetThumbnailName(filename));
            return await client.DeleteFile(storageBin, bucket, id, filename);
        }

        //Folder Operations

        /// <summary>
        /// Move a folder with the attached Thumbnail
        /// </summary>
        /// <param name="fromBin"></param>
        /// <param name="fromBucket"></param>
        /// <param name="fromID"></param>
        /// <param name="folders"></param>
        /// <param name="toBin"></param>
        /// <param name="toBucket"></param>
        /// <param name="toID"></param>
        /// <param name="toFolder"></param>
        /// <returns></returns>
        protected async Task<int> MoveFoldersWithThumbnail(StorageBin fromBin, Bucket fromBucket, IDMID fromID, string[] folders, StorageBin? toBin = null, Bucket? toBucket = null, IDMID toID = null, string toFolder = null)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            var thumbnailFolders = folders.ToList().Select(folder => $"{thumbnailFolder}/{folder}").ToArray();
            //----------------------------------------------------thumbnailfolders------------------------thumbnailfolder+destinationFolder--
            await client.MoveFolders(fromBin, fromBucket, fromID, thumbnailFolders, toBin, toBucket, toID, $"{thumbnailFolder}/{toFolder}");
            return await client.MoveFolders(fromBin, fromBucket, fromID, folders, toBin, toBucket, toID, toFolder);
        }

        /// <summary>
        /// Rename a folder with the attached Thumbnail
        /// </summary>
        /// <param name="fromBin"></param>
        /// <param name="fromBucket"></param>
        /// <param name="fromID"></param>
        /// <param name="folderPath"></param>
        /// <param name="newName"></param>
        /// <returns></returns>
        protected async Task<int> RenameFolderWithThumbnail(StorageBin fromBin, Bucket fromBucket, IDMID fromID, string folderPath, string newName)
        {
            var client = await GetStorageClientAsync(_ctx.BID);
            await client.RenameFolder(fromBin, fromBucket, fromID, $"{thumbnailFolder}/{folderPath}", newName);
            return await client.RenameFolder(fromBin, fromBucket, fromID, folderPath, newName);
        }

        //get DMIItems


        /// <summary>
        /// Return a document list with thumbnails
        /// </summary>
        /// <param name="storageBin"></param>
        /// <param name="bucket"></param>
        /// <param name="id"></param>
        /// <param name="useFlatDocumentListing"></param>
        /// <returns></returns>
        protected async Task<List<DMItem>> GetDocumentListWithThumbnail(StorageBin storageBin, Bucket bucket, IDMID id, bool useFlatDocumentListing = false)
        {
            var client = await GetStorageClientAsync(_ctx.BID);

            var thumbnails = (await client.GetDocumentList(storageBin, bucket, id, true))
                                .Where(dm => dm.Name.Contains(thumbnailFolder) && dm.Name.Contains(thumbnailPrefix))
                                .ToList();
            var dmItemList = (await client.GetDocumentList(storageBin, bucket, id))
                                .Where(dmItem => !dmItem.Name.Contains(thumbnailFolder) && !dmItem.Name.Equals("_TemplateFiles.json"))
                                .ToList();
            SetThumbnails(dmItemList, thumbnails);
            return dmItemList;
        }

        /// <summary>
        /// Set Thumbnails for Items
        /// </summary>
        /// <param name="dmItemList"></param>
        /// <param name="thumbnails"></param>
        /// <returns></returns>
        protected void SetThumbnails(List<DMItem> dmItemList, List<DMItem> thumbnails)
        {
            foreach (var item in dmItemList)
            {
                if (item.IsFolder)
                {
                    SetThumbnails(item.Contents, thumbnails);
                }
                else if (item.MimeType.Contains("img") || item.MimeType.Contains("image"))
                {
                    var thumbNailItem = thumbnails.Where(thumbnail =>
                    {

                        var thumbnailName = Path.GetFileName(thumbnail.Name);
                        var itemName = Path.GetFileName(item.Name);
                        return thumbnailName.Contains($"{thumbnailPrefix}{itemName}");
                    }).FirstOrDefault();
                    if (thumbNailItem != null)
                    {
                        item.HasThumbnail = true;
                        item.ThumbnailURL = thumbNailItem.URL;
                    }
                }
            }
        }

        /// <summary>
        /// Get the Name of the Thumbnail for a file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        protected string GetThumbnailName(string fileName)
        {
            if (fileName.EndsWith(EntityStorageClient.SpecialFolderBlobName))
            {
                return $"{thumbnailFolder}/{fileName}";
            }
            else
            {
                string shortName = Path.GetFileName(fileName);
                string shortThumbNailName = thumbnailPrefix + shortName;
                string SpecialFolderBlobName = EntityStorageClient.SpecialFolderBlobName.ToString();
                shortThumbNailName = shortThumbNailName.Replace($"{thumbnailPrefix}{SpecialFolderBlobName}", SpecialFolderBlobName);
                return $"{thumbnailFolder}/{fileName.Replace(shortName, shortThumbNailName)}";
            }
        }

        /// <summary>
        /// Check whether files exist in a client
        /// </summary>
        /// <param name="client"></param>
        /// <param name="documents"></param>
        /// <returns></returns>
        public async Task<bool> FilesExist(EntityStorageClient client, string[] documents)
        {
            try
            {
                foreach (string doc in documents)
                {
                    var docStream = (await client.GetStream(_ctx.Bin, _ctx.BucketRequest.ToBucket(), _ctx.ID, doc));
                    docStream.Dispose();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message.ToLower();
                if (ex is Microsoft.WindowsAzure.Storage.StorageException && msg.Contains("does not exist"))
                {
                    return false;
                }
                else
                {
                    throw ex;
                }
            }
            return true;
        }
    }



    /// <summary>
    /// Document Management Extensions
    /// </summary>
    /// <returns></returns>
    public static class DocumentManagementExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Bucket ToBucket(this BucketRequest buckReq)
        {
            switch (buckReq)
            {
                case BucketRequest.Data:
                    return Bucket.Data;
                case BucketRequest.Documents:
                    return Bucket.Documents;
                case BucketRequest.Reports:
                    return Bucket.Reports;
                case BucketRequest.All:
                    throw new ArgumentException("BucketRequest of ALL must be handled in logic, cannot be mutated to a Bucket");
                default:
                    throw new ArgumentException("Unknown BucketRequest, cannot map to Bucket");
            }
        }

        /// <summary>
        /// Get the absolute path for an item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string AbsolutePath(this DMItem item)
        {
            if (item == null)
                throw new NullReferenceException();

            string path = item.Path ?? "";
            if (path.Length > 0 && path[path.Length - 1] != '/')
                path += '/';

            string name = item.Name ?? "";

            string @base = path + name.TrimStart('/');

            if (item.IsFolder)
                return $"{@base.TrimEnd('/')}/{EntityStorageClient.SpecialFolderBlobName}";
            else
                return @base;
        }

        internal static bool IsImageMimeType(this DMItem result)
        {
            return result.MimeType.ToLower().Contains("img") || result.MimeType.ToLower().Contains("image");
        }
    }
}
