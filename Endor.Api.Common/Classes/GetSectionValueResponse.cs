﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// Response for Getting an Option Value
    /// </summary>
    public class GetSectionValueResponse : IGenericResponse
    {
        /// <summary>
        /// system option values
        /// </summary>
        public SystemOptionSection Value { get; set; }

        /// <summary>
        /// Response message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// If the action was successful
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// true if has error, otherwise false
        /// </summary>
        public bool HasError { get { return !this.Success; } }

        /// <summary>
        /// error message encountered for this operation
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
