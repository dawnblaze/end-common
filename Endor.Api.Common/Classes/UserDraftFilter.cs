﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.Api.Common.Classes
{
    /// <summary>
    /// query filters for UserDraft entity
    /// </summary>>
    public class UserDraftFilters : IQueryFilters<UserDraft>
    {
        /// <summary>
        /// Required. The ClassTypeID of the draft object sought.
        /// </summary>
        public int ObjectClassTypeID { get; set; }

        /// <summary>
        /// Optional. The ID of the draft object sought.  If not supplied, all non-expired drafts for the specified ClassTypeID are returned.
        /// </summary>
        public int? ObjectID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool? AllUsers { get; set; }

        /// <summary>
        /// Query Filter for filters on UserDraft
        /// </summary>
        /// <returns></returns>
        [Obsolete("Use the overloaded method that takes a UserID", true)]
        public Expression<Func<UserDraft, bool>>[] WherePredicates() { return null; }

        /// <summary>
        /// query filter for filters on this object for <see cref="IQueryFilters{T}"/> interface
        /// </summary>
        /// <param name="userID">The calling User's ID</param>
        /// <returns></returns>
        public Expression<Func<UserDraft, bool>>[] WherePredicates(int? userID)
        {
            if (!this.AllUsers.HasValue)
                this.AllUsers = false;

            var predicates = new List<Expression<Func<UserDraft, bool>>>();

            predicates.Add(ud => ud.ObjectCTID == this.ObjectClassTypeID);

            if (this.ObjectID.HasValue)
                predicates.Add(ud => ud.ObjectID == this.ObjectID);
            else
                predicates.Add(ud => ud.ExpirationDT < DateTime.UtcNow);

            if ((!this.AllUsers.HasValue || !this.AllUsers.Value) && userID.HasValue)
                predicates.Add(ud => ud.UserLinkID == userID.Value);

            return predicates.ToArray();
        }
    }
}
