﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.WebUtilities;
using Endor.Api.Common;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Endor.Api.Common
{
    /// <summary>
    /// Startup Class
    /// </summary>
    public class Startup : BaseStartup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env) : base(env)
        {
        }

        public override string GetAPIName()
        {
            return "Endor API";
        }

        public override string GetAPIVersion()
        {
            return "v1";
        }

        /// <summary>
        /// This method configures the authentication service
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public override void AddAuthentication(IServiceCollection services, IConfigurationRoot configuration)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
            {
                option.Audience = Configuration["Auth:ValidAudience"];
                option.TokenValidationParameters = new TokenValidationParameters()
                {
                    NameClaimType = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier",
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["Auth:ValidIssuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(WebEncoders.Base64UrlDecode(Configuration["Auth:SymmetricKey"])),
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMilliseconds(2)
                };
            });
            //services.AddAuthorization();
        }
    }
}
