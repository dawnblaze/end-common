﻿using System.Collections.Generic;
using System.Linq;

namespace Endor.Api.Common
{
    /// <summary>
    /// Includes Extentions Class
    /// </summary>
    public static class IncludesExtensions
    {
        /// <summary>
        /// Takes an IExpandInclues and string[] of default includes and extracts an array of Includes to be passed to EF
        /// </summary>
        /// <param name="includes">IExpandIncludes interface built from query parameters</param>
        /// <param name="includeDefaults">Array of Includes included by default regardless of query parameters</param>
        /// <returns></returns>
        public static string[] ToIncludesArray(this IExpandIncludes includes, string[] includeDefaults)
        {
            string[] result = {};

            List<KeyValuePair<string, IncludeSettings>> activeIncludes = includes?.GetIncludes().Where(i => i.Value.Level != IncludesLevel.None).ToList();

            if (activeIncludes == null) return includeDefaults;

            List<string> outputIncludes = new List<string>();

            foreach (KeyValuePair<string, IncludeSettings> activeInclude in activeIncludes)
            {
                string activeIncludeKey = activeInclude.Value.AlternativeName;

                if (string.IsNullOrWhiteSpace(activeIncludeKey))
                    activeIncludeKey = activeInclude.Key;

                activeIncludeKey = activeIncludeKey.Replace("With", ".");

                if (activeIncludeKey[0] == '!')
                {
                    outputIncludes.Add(activeIncludeKey.Substring(1));
                    continue;
                }
                else
                {
                    switch (activeInclude.Value.Level)
                    {
                        case IncludesLevel.Simple:
                            outputIncludes.Add($"Simple{activeIncludeKey}");
                            break;
                        case IncludesLevel.Full:
                            outputIncludes.Add(activeIncludeKey);
                            break;
                    }
                }
            }

            if(includeDefaults != null)
                outputIncludes.AddRange(includeDefaults);

            return outputIncludes.ToArray();
        }
    }
}
