﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.Common.Tests;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class EstimateMemberInitializerTests
    {
        private readonly string[] includes = { "Company", "ContactRoles", "Dates", "EmployeeRoles", "Items", "Location", "Origin", "ProductionLocation" };

        [TestMethod]
        public async Task TestEstimateSimpleProperties()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);
            EstimateData nullEstimate = null;

            var testDateStr = DateTime.UtcNow.ToString();
            var testComp = GetTestCompany(timestamp: testDateStr);
            var testEstimate = GetTestEstimate(timestamp: testDateStr, companyID: testComp.ID);
            var taxGroup = new TaxGroup
            {
                BID = testBID,
                ID = -99,
                IsActive = true,
                IsTaxExempt = true,
                Name = "TaxGroup.1"
            };

            try
            {
                testEstimate.TaxGroup = taxGroup;
                ctx.TaxGroup.Add(taxGroup);
                ctx.CompanyData.Add(testComp);
                ctx.EstimateData.Add(testEstimate);
                Assert.AreEqual(3, await ctx.SaveChangesAsync());

                var estimate = await ctx.EstimateData.IncludeAll(includes)
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testComp.ID);

                Assert.IsNull(AELTestHelper.GetPropertyResult<List<OrderItemData>, EstimateData>(testBID, DataType.EstimateCategory, "Line Items", nullEstimate));
                var lineItems = AELTestHelper.GetPropertyResult<List<OrderItemData>, EstimateData>(testBID, DataType.EstimateCategory, "Line Items", estimate);
                Assert.AreEqual(estimate.Items?.ToList()?.GetType(), lineItems?.GetType());
                Assert.AreEqual(estimate.Items?.Count(), lineItems?.Count());

                Assert.IsNull(AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Contacts", nullEstimate));
                var contactRoles = AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Contacts", estimate);
                Assert.AreEqual(estimate.GetType(), contactRoles?.GetType());

                Assert.IsNull(AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Employees", nullEstimate));
                var employees = AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Employees", estimate);
                Assert.AreEqual(estimate.GetType(), employees?.GetType());

                Assert.IsNull(AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Locations", nullEstimate));
                var locationsCat = AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Locations", estimate);
                Assert.IsNotNull(locationsCat);

                Assert.IsNull(AELTestHelper.GetPropertyResult<CrmOrigin, EstimateData>(testBID, DataType.EstimateCategory, "Estimate Origin", nullEstimate));
                var origin = AELTestHelper.GetPropertyResult<CrmOrigin, EstimateData>(testBID, DataType.EstimateCategory, "Estimate Origin", estimate);
                Assert.AreEqual(estimate.Origin, origin);

                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, EstimateData>(testBID, DataType.EstimateCategory, "Estimate Number", nullEstimate));
                var estimateNumber = AELTestHelper.GetPropertyResult<decimal?, EstimateData>(testBID, DataType.EstimateCategory, "Estimate Number", estimate);
                Assert.AreEqual(estimate.Number, estimateNumber);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, EstimateData>(testBID, DataType.EstimateCategory, "Description", nullEstimate));
                var description = AELTestHelper.GetPropertyResult<string, EstimateData>(testBID, DataType.EstimateCategory, "Description", estimate);
                Assert.AreEqual(estimate.Description, description);

                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, EstimateData>(testBID, DataType.EstimateCategory, "Company", nullEstimate));
                var company = AELTestHelper.GetPropertyResult<CompanyData, EstimateData>(testBID, DataType.EstimateCategory, "Company", estimate);
                Assert.AreEqual(estimate.Company, company);

                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, EstimateData>(testBID, DataType.EstimateCategory, "Status", nullEstimate));
                var status = AELTestHelper.GetPropertyResult<decimal?, EstimateData>(testBID, DataType.EstimateCategory, "Status", estimate);
                Assert.AreEqual((decimal?)estimate.OrderStatusID, status);

                Assert.IsNull(AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Taxes", nullEstimate));
                var taxCat = AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Taxes", estimate);
                Assert.IsNotNull(taxCat);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, EstimateData>(testBID, DataType.EstimateCategory, "PO Number", nullEstimate));
                var poNumber = AELTestHelper.GetPropertyResult<string, EstimateData>(testBID, DataType.EstimateCategory, "PO Number", estimate);
                Assert.AreEqual(estimate.OrderPONumber, poNumber);

                Assert.IsNull(AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Tags", nullEstimate));
                var tags = AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Tags", estimate);
                Assert.AreEqual(estimate.ID, tags.ID);

                Assert.IsNull(AELTestHelper.GetPropertyResult<List<OrderNote>, EstimateData>(testBID, DataType.EstimateCategory, "Notes", nullEstimate));
                var notes = AELTestHelper.GetPropertyResult<List<OrderNote>, EstimateData>(testBID, DataType.EstimateCategory, "Notes", estimate);
                Assert.AreEqual(estimate.Notes?.GetType(), notes?.GetType());
                Assert.AreEqual(estimate.Notes?.Count(), notes?.Count());

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, EstimateData>(testBID, DataType.EstimateCategory, "Has Documents", nullEstimate));
                var hasDocuments = AELTestHelper.GetPropertyResult<bool?, EstimateData>(testBID, DataType.EstimateCategory, "Has Documents", estimate);
                Assert.AreEqual(estimate.HasDocuments, hasDocuments);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);
                ctx.EstimateData.Remove(testEstimate);
                ctx.CompanyData.Remove(testComp);
                ctx.TaxGroup.Remove(taxGroup);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task TestEstimateContactRolesProperty()
        {
            short testBID = 1;
            #region Null Estimate Check

            EstimateData nullEstimate = null;
            var nullContacts = AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Contacts", nullEstimate);
            Assert.IsNull(nullContacts);

            #endregion

            var ctx = AELTestHelper.GetMockCtx(testBID);

            #region Setup Data

            var testDateStr = DateTime.UtcNow.ToString();
            var testComp = GetTestCompany(timestamp: testDateStr);
            var testEstimate = GetTestEstimate(timestamp: testDateStr, companyID: testComp.ID);
            var taxGroup = new TaxGroup
            {
                BID = testBID, 
                ID = -99,
                IsActive = true,
                IsTaxExempt = true,
                Name = "TaxGroup.1"
            };

            testEstimate.TaxGroupID = taxGroup.ID;

            var testCont1 = new ContactData
            {
                BID = testBID,
                ID = -99,
                //CompanyID = testComp.ID, end-4814
                First = "ContactData.1",
                Last = testDateStr,
                LocationID = 1,
            };

            // salesperson
            var contactRole1 = new OrderContactRole()
            {
                BID = testBID,
                ID = -99,
                RoleType = OrderContactRoleType.Primary,
                ContactID = testCont1.ID,
                OrderID = testEstimate.ID
            };

            ctx.ContactData.Add(testCont1);
            ctx.TaxGroup.Add(taxGroup);
            ctx.OrderContactRole.Add(contactRole1);
            ctx.CompanyData.Add(testComp);
            ctx.EstimateData.Add(testEstimate);

            #endregion Setup Data

            try
            {
                Assert.AreEqual(5, await ctx.SaveChangesAsync());
                var estimate = await ctx.EstimateData
                    .Include("ContactRoles").Include("ContactRoles.Contact")
                    .FirstOrDefaultAsync(e => e.BID == testBID && e.ID == testEstimate.ID);
                Assert.IsNotNull(estimate);

                #region All Contacts

                var empData = estimate.ContactRoles;
                Assert.IsNotNull(empData);

                var propertyResultIsEstimateData =
                    AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory,
                        "Contacts", estimate);
                Assert.IsNotNull(propertyResultIsEstimateData);

                var primaryOrderContactPropertyResult =
                    AELTestHelper.GetPropertyResult<List<ContactData>, EstimateData>(testBID, DataType.TransactionHeaderContactRoleCategory,
                        "Primary", estimate);
                Assert.IsNotNull(primaryOrderContactPropertyResult);

                var shippingOrderContactPropertyResult =
                    AELTestHelper.GetPropertyResult<List<ContactData>, EstimateData>(testBID, DataType.TransactionHeaderContactRoleCategory,
                        "Shipping", estimate);
                Assert.IsNotNull(shippingOrderContactPropertyResult);

                #endregion All Contacts
            }
            catch (Exception ex)
            {
                if(ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                #region Cleanup Test Data
                ctx = AELTestHelper.GetMockCtx(testBID);
                ctx.OrderContactRole.Remove(contactRole1);
                ctx.ContactData.Remove(testCont1);
                ctx.EstimateData.Remove(testEstimate);
                ctx.TaxGroup.Remove(taxGroup);
                ctx.CompanyData.Remove(testComp);

                ctx.SaveChanges();

                #endregion
            }
        }

        [TestMethod]
        public async Task TestEstimateNotesProperty()
        {
            short testBID = 1;
            #region Null Estimate Check

            EstimateData nullEstimate = null;
            var nullNoteList = AELTestHelper.GetPropertyResult<List<OrderNote>, EstimateData>(testBID, DataType.EstimateCategory, "Notes", nullEstimate);
            Assert.IsNull(nullNoteList);
            #endregion

            var ctx = AELTestHelper.GetMockCtx(testBID);

            #region Setup Data

            var testDateStr = DateTime.UtcNow.ToString();
            var testComp = GetTestCompany(timestamp: testDateStr);
            var testEstimate = GetTestEstimate(timestamp: testDateStr, companyID: testComp.ID);
            var taxGroup = new TaxGroup
            {
                BID = testBID,
                ID = -99,
                IsActive = true,
                IsTaxExempt = true,
                Name = "TaxGroup.1"
            };

            testEstimate.TaxGroupID = taxGroup.ID;

            var testCont1 = new ContactData
            {
                BID = testBID,
                ID = -99,
                //CompanyID = testComp.ID, end-4814
                First = "ContactData.1",
                Last = testDateStr,
                LocationID = 1,
            };

            var note = new OrderNote
            {
                BID = testBID,
                ID = -99,
                Note = "Test Add Note",
                //NotePartXML = "<OrderNoteParts><OrderNotePart><Note>This is a note.</Note><CreatedDT>2018-01-01</CreatedDT><CreatedByID>1</CreatedByID><LastModifiedDT>2018-12-27</LastModifiedDT><LastModifiedByID>2</LastModifiedByID></OrderNotePart></OrderNoteParts>",
                OrderID = testEstimate.ID,
                ModifiedDT = DateTime.Now,
                NoteType = OrderNoteType.Other
            };

            var internalNote = new OrderNote
            {
                BID = testBID,
                ID = -98,
                Note = "Test Add Note",
                //NotePartXML = "<OrderNoteParts><OrderNotePart><Note>This is a internal note.</Note><CreatedDT>2018-01-01</CreatedDT><CreatedByID>1</CreatedByID><LastModifiedDT>2018-12-27</LastModifiedDT><LastModifiedByID>2</LastModifiedByID></OrderNotePart></OrderNoteParts>",
                OrderID = testEstimate.ID,
                ModifiedDT = DateTime.Now,
                NoteType = OrderNoteType.Production
            };

            var customerNote = new OrderNote
            {
                BID = testBID,
                ID = -97,
                Note = "Test Add Note",
                //NotePartXML = "<OrderNoteParts><OrderNotePart><Note>This is a customer note.</Note><CreatedDT>2018-01-01</CreatedDT><CreatedByID>1</CreatedByID><LastModifiedDT>2018-12-27</LastModifiedDT><LastModifiedByID>2</LastModifiedByID></OrderNotePart></OrderNoteParts>",
                OrderID = testEstimate.ID,
                ModifiedDT = DateTime.Now,
                NoteType = OrderNoteType.Customer
            };

            ctx.OrderNote.Add(note);
            ctx.OrderNote.Add(internalNote);
            ctx.OrderNote.Add(customerNote);
            testEstimate.Notes = new List<OrderNote>{ note, internalNote, customerNote };
            ctx.TaxGroup.Add(taxGroup);
            ctx.CompanyData.Add(testComp);
            ctx.EstimateData.Add(testEstimate);

            #endregion Setup Data

            try
            {
                Assert.AreEqual(6, await ctx.SaveChangesAsync());

                var estimate = await ctx.EstimateData
                    .Include("Notes")
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testComp.ID);

                #region All Notes

                var listNoteData = estimate.Notes.Where(n => n.IsOrderNote).ToList();
                Assert.IsNotNull(listNoteData);

                var listAelData = AELTestHelper.GetPropertyResult<IEnumerable<OrderNote>, EstimateData>(testBID, DataType.EstimateCategory, "Notes", estimate).ToList();
                Assert.IsNotNull(listAelData);

                for (int i = 0; i < listNoteData.Count; i++)
                    Assert.AreEqual(listNoteData[i], listAelData[i]);

                Assert.AreEqual(listNoteData.Count, listAelData.Count);

                #endregion All Notes
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                #region Cleanup Test Data
                ctx = AELTestHelper.GetMockCtx(testBID);
                ctx.OrderNote.Remove(note);
                ctx.OrderNote.Remove(internalNote);
                ctx.OrderNote.Remove(customerNote);
                ctx.EstimateData.Remove(testEstimate);
                ctx.TaxGroup.Remove(taxGroup);
                ctx.CompanyData.Remove(testComp);

                await ctx.SaveChangesAsync();

                #endregion
            }
        }

        [TestMethod]
        public void TestEstimateDatesProperty()
        {
            short testBID = 1;
            var date = DateTime.UtcNow;

            EstimateData nullEstimate = null;
            var estimate = GetTestEstimate();
            estimate.OrderStatusStartDT = date.AddDays(-10);
            var dates = new List<OrderKeyDate>()
            {
                new OrderKeyDate()
                {
                    BID = testBID,
                    ID = 1,
                    KeyDateType = OrderKeyDateType.Created,
                    KeyDate = date.AddDays(-7)
                },
                new OrderKeyDate()
                {
                    BID = testBID,
                    ID = 2,
                    KeyDateType = OrderKeyDateType.ProposalDue,
                    KeyDate = date.AddDays(-5)
                },
                new OrderKeyDate()
                {
                    BID = testBID,
                    ID = 3,
                    KeyDateType = OrderKeyDateType.Voided,
                    KeyDate = date.AddDays(-3)
                },
            };

            Assert.IsNull(AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Dates", nullEstimate));
            var estDatesCat = AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Dates", estimate);
            Assert.IsNotNull(estDatesCat);

            estimate.Dates = dates;
            estDatesCat = AELTestHelper.GetPropertyResult<EstimateData, EstimateData>(testBID, DataType.EstimateCategory, "Dates", estimate);
            Assert.IsNotNull(estDatesCat);

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, EstimateData>(testBID, DataType.EstimateDateCategory, "Current Status Date", null));
            var currentStatus = AELTestHelper.GetPropertyResult<DateTime?, EstimateData>(testBID, DataType.EstimateDateCategory, "Current Status Date", estDatesCat);
            Assert.AreEqual(estimate.OrderStatusStartDT, currentStatus);

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, EstimateData>(testBID, DataType.EstimateDateCategory, "Created Date", null));
            var created = AELTestHelper.GetPropertyResult<DateTime?, EstimateData>(testBID, DataType.EstimateDateCategory, "Created Date", estDatesCat);
            Assert.AreEqual(estimate.Dates.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.Created).KeyDate, created);

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, EstimateData>(testBID, DataType.EstimateDateCategory, "Estimate Send Date", null));
            var estimateSend = AELTestHelper.GetPropertyResult<DateTime?, EstimateData>(testBID, DataType.EstimateDateCategory, "Estimate Send Date", estDatesCat);
            Assert.AreEqual(estimate.Dates.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.ProposalDue).KeyDate, estimateSend);

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, EstimateData>(testBID, DataType.EstimateDateCategory, "Voided Date", null));
            var voided = AELTestHelper.GetPropertyResult<DateTime?, EstimateData>(testBID, DataType.EstimateDateCategory, "Voided Date", estDatesCat);
            Assert.AreEqual(estimate.Dates.FirstOrDefault(k => k.KeyDateType == OrderKeyDateType.Voided).KeyDate, voided);
        }

        private EstimateData GetTestEstimate(short bid = 1, int id = -99, string timestamp = "", int companyID = -99)
        {
            return new EstimateData()
            {
                BID = bid,
                ID = id,
                Description = $"Test.Estimate.{timestamp}",
                CompanyID = companyID,
                LocationID = 1,
                OrderStatusID = OrderOrderStatus.EstimatePending,
                Number = 9999,
                FormattedNumber = "EST-9999",
                PickupLocationID = 1,
                ProductionLocationID = 1,
                TaxGroupID = 1
            };
        }

        private CompanyData GetTestCompany(short bid = 1, int id = -99, string timestamp = "", int? teamId = null)
        {
            return new CompanyData()
            {
                BID = bid,
                ID = id,
                Name = $"CompanyData.{timestamp}",
                TeamID = teamId,
                LocationID = 1,
                StatusID = 1
            };
        }
    }
}
