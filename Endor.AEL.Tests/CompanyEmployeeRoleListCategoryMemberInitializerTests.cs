﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class CompanyEmployeeRoleListCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task TestCompanyEmployeeRoleListCategorySimpleProperties()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);

            short employeeID = ctx.EmployeeData.Min(e => e.ID);
            var employeeTeam = new EmployeeTeam()
            {
                BID = testBID,
                ID = -99,
                Name = "Test Team"
            };
            var employeeRole1 = new EmployeeTeamLink()
            {
                BID = testBID,
                TeamID = -99,
                EmployeeID = employeeID,
                RoleID = SystemIDs.EmployeeRole.AssignedTo,
            };
            var employeeRole2 = new EmployeeTeamLink()
            {
                BID = testBID,
                TeamID = -99,
                EmployeeID = employeeID,
                RoleID = SystemIDs.EmployeeRole.Salesperson,
            };

            var testCompany = new CompanyData()
            {
                BID = testBID,
                ID = -99,
                Name = "CompanyData_Test",
                LocationID = 1,
                StatusID = 1,
                IsVendor = false,
                IsProspect = false,
                IsClient = true,
                IsPORequired = false,
                DefaultPONumber = "blah blah blah",
                CreditLimit = 100,
                NonRefundableCredit = 50,
                RefundableCredit = 25,
                TeamID = -99,
            };

            try
            {
                ctx.RemoveRange(ctx.CompanyData.Where(c => c.ID == -99));
                ctx.RemoveRange(ctx.EmployeeTeamLink.Where(oi => oi.TeamID == -99));
                ctx.RemoveRange(ctx.EmployeeTeam.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.CompanyData.Add(testCompany);
                ctx.EmployeeTeam.Add(employeeTeam);
                ctx.EmployeeTeamLink.Add(employeeRole1);
                ctx.EmployeeTeamLink.Add(employeeRole2);
                Assert.AreEqual(4, await ctx.SaveChangesAsync());

                var company = ctx.CompanyData
                    .Include(oi => oi.EmployeeTeam)
                    .Include(oi => oi.EmployeeTeam.EmployeeTeamLinks)
                    .Include("EmployeeTeam.EmployeeTeamLinks.Employee")
                    .FirstOrDefault(e => e.BID == testBID && e.ID == -99);

                var RolesTest = AELTestHelper.GetPropertyResult<ICollection<EmployeeData>, CompanyData>(testBID, DataType.CompanyEmployeeRoleListCategory, "Salesperson", company);
                Assert.AreEqual(RolesTest.Count(), 1);
                Assert.IsNotNull(company);
            }
            finally
            {
                ctx.RemoveRange(ctx.CompanyData.Where(c => c.ID == -99));
                ctx.RemoveRange(ctx.EmployeeTeamLink.Where(oi => oi.TeamID == -99));
                ctx.RemoveRange(ctx.EmployeeTeam.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestCompanyEmployeeRoleListCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.CompanyEmployeeRoleListCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(1, memberInfo.Members.Where(x => x.RelatedID == 0).Count());
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.EmployeeCategory));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Salesperson"));

        }
    }
}
