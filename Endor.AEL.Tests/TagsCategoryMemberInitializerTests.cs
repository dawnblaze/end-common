﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;


namespace Endor.AEL.Tests
{
    [TestClass]
    public class TagsCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task TestCompanyTagsCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            ctx.RemoveRange(ctx.CompanyTagLink.Where(t => t.CompanyID == -99));
            ctx.RemoveRange(ctx.CompanyData.Where(t => t.ID == -99));
            ctx.RemoveRange(ctx.ListTag.Where(t => t.ID == -99));
            await ctx.SaveChangesAsync();

            var company = new CompanyData()
            {
                BID = testBID,
                ID = -99,
                Name = "Test Subject",
                LocationID = 1,
                IsAdHoc = true,
                StatusID = 1,
                TagLinks = new List<CompanyTagLink>()
                {
                    new CompanyTagLink()
                    {
                        BID = testBID,
                        CompanyID = -99,
                        TagID = -99,
                    }
                }
            };

            var testTag = new ListTag()
            {
                BID = testBID,
                ID = -99,
                Name = "TestName",
                RGB = "RGB",
                AssociatedClassTypeID = DataType.Company.ClassTypeID(),
            };

            ctx.ListTag.Add(testTag);
            ctx.CompanyData.Add(company);

            Assert.AreEqual(3, await ctx.SaveChangesAsync());

            try
            {
                var testCompany = await ctx.CompanyData
                    .FirstOrDefaultAsync(t => t.BID == testBID && t.ID == -99);

                Assert.IsNotNull(testCompany);

                var hasTagsTest = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTagsCategory, "Has Tags", testCompany);
                Assert.AreEqual(hasTagsTest, true);

                var tagNameTest = AELTestHelper.GetPropertyResult<List<string>, CompanyData>(testBID, DataType.CompanyTagsCategory, "Tag Name", testCompany);
                Assert.AreEqual(tagNameTest.FirstOrDefault(), "TestName");

                var tagColorTest = AELTestHelper.GetPropertyResult< List<string>, CompanyData>(testBID, DataType.CompanyTagsCategory, "Tag Color", testCompany);
                Assert.AreEqual(tagColorTest.FirstOrDefault(), "RGB");

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx.RemoveRange(ctx.CompanyTagLink.Where(t => t.CompanyID == -99));
                ctx.RemoveRange(ctx.CompanyData.Where(t => t.ID == -99));
                ctx.RemoveRange(ctx.ListTag.Where(t => t.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestCompanyTagsCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.CompanyTagsCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.StringList));
            

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Has Tags"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tag Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tag Color"));
            
        }

        [TestMethod]
        public async Task TestOrderTagsCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            ctx.RemoveRange(ctx.OrderTagLink.Where(t => t.OrderID == -99));
            ctx.RemoveRange(ctx.OrderData.Where(t => t.ID == -99));
            ctx.RemoveRange(ctx.ListTag.Where(t => t.ID == -99));
            await ctx.SaveChangesAsync();

            OrderData order = new OrderData()
            {
                BID = testBID,
                ID = -99,
                ClassTypeID = (int)ClassType.Order,
                ModifiedDT = DateTime.UtcNow,
                TransactionType = (byte)OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "INV-1000",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
                TagLinks = new List<OrderTagLink>()
                {
                    new OrderTagLink()
                    {
                        BID = testBID,
                        OrderID = -99,
                        TagID = -99,
                    }
                }
            };
            order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
            order.PickupLocationID = order.LocationID;
            order.ProductionLocationID = order.LocationID;
            order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
            order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

            var testTag = new ListTag()
            {
                BID = testBID,
                ID = -99,
                Name = "TestName",
                RGB = "RGB",
                AssociatedClassTypeID = DataType.Order.ClassTypeID(),
            };

            ctx.ListTag.Add(testTag);
            ctx.OrderData.Add(order);

            Assert.AreEqual(3, await ctx.SaveChangesAsync());

            try
            {
                var testOrder = await ctx.OrderData
                    .FirstOrDefaultAsync(t => t.BID == testBID && t.ID == -99);

                Assert.IsNotNull(testOrder);

                var hasTagsTest = AELTestHelper.GetPropertyResult<bool?, OrderData>(testBID, DataType.TransactionHeaderTagsCategory, "Has Tags", testOrder);
                Assert.AreEqual(hasTagsTest, true);

                var tagNameTest = AELTestHelper.GetPropertyResult<List<string>, OrderData>(testBID, DataType.TransactionHeaderTagsCategory, "Tag Name", testOrder);
                Assert.AreEqual(tagNameTest.FirstOrDefault(), "TestName");

                var tagColorTest = AELTestHelper.GetPropertyResult<List<string>, OrderData>(testBID, DataType.TransactionHeaderTagsCategory, "Tag Color", testOrder);
                Assert.AreEqual(tagColorTest.FirstOrDefault(), "RGB");

            }
            finally
            {
                ctx.RemoveRange(ctx.OrderTagLink.Where(t => t.OrderID == -99));
                ctx.RemoveRange(ctx.OrderData.Where(t => t.ID == -99));
                ctx.RemoveRange(ctx.ListTag.Where(t => t.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderTagsCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.TransactionHeaderTagsCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.StringList));


            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Has Tags"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tag Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tag Color"));

        }

        [TestMethod]
        public async Task TestOrderItemTagsCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            ctx.RemoveRange(ctx.OrderItemTagLink.Where(t => t.OrderItemID == -99));
            ctx.RemoveRange(ctx.OrderItemData.Where(t => t.ID == -99));
            ctx.RemoveRange(ctx.OrderData.Where(t => t.ID == -99));
            ctx.RemoveRange(ctx.ListTag.Where(t => t.ID == -99));
            await ctx.SaveChangesAsync();

            OrderData order = new OrderData()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.Order,
                ModifiedDT = DateTime.UtcNow,
                TransactionType = (byte)OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                Number = 1000,
                FormattedNumber = "INV-1000",
                PriceTaxRate = 0.01m,
                PaymentPaid = 0m,
            };
            order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
            order.PickupLocationID = order.LocationID;
            order.ProductionLocationID = order.LocationID;
            order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
            order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

            OrderItemData orderItem = new OrderItemData()
            {
                BID = order.BID,
                ID = -99,
                OrderID = order.ID,
                ItemNumber = 1,
                Quantity = 1,
                Name = "Test Order Item",
                IsOutsourced = false,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                TransactionType = (byte)OrderTransactionType.Order,
                ProductionLocationID = order.LocationID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroupID = order.TaxGroupID,
                TaxGroupOV = false,
                HasProof = false,
                HasDocuments = false,
                HasCustomImage = false,
                TagLinks = new List<OrderItemTagLink>()
                {
                    new OrderItemTagLink()
                    {
                        BID = testBID,
                        OrderItemID = -99,
                        TagID = -99,
                    }
                }
            };

            var testTag = new ListTag()
            {
                BID = testBID,
                ID = -99,
                Name = "TestName",
                RGB = "RGB",
                AssociatedClassTypeID = DataType.LineItem.ClassTypeID(),
            };

            ctx.ListTag.Add(testTag);
            ctx.OrderData.Add(order);
            ctx.OrderItemData.Add(orderItem);

            Assert.AreEqual(4, await ctx.SaveChangesAsync());

            try
            {
                var testOrderItem = await ctx.OrderItemData
                    .FirstOrDefaultAsync(t => t.BID == testBID && t.ID == -99);

                Assert.IsNotNull(testOrderItem);

                var hasTagsTest = AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.OrderItemTagsCategory, "Has Tags", testOrderItem);
                Assert.AreEqual(hasTagsTest, true);

                var tagNameTest = AELTestHelper.GetPropertyResult<List<string>, OrderItemData>(testBID, DataType.OrderItemTagsCategory, "Tag Name", testOrderItem);
                Assert.AreEqual(tagNameTest.FirstOrDefault(), "TestName");

                var tagColorTest = AELTestHelper.GetPropertyResult<List<string>, OrderItemData>(testBID, DataType.OrderItemTagsCategory, "Tag Color", testOrderItem);
                Assert.AreEqual(tagColorTest.FirstOrDefault(), "RGB");

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx.RemoveRange(ctx.OrderItemTagLink.Where(t => t.OrderItemID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(t => t.ID == -99));
                ctx.RemoveRange(ctx.OrderData.Where(t => t.ID == -99));
                ctx.RemoveRange(ctx.ListTag.Where(t => t.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemTagsCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.OrderItemTagsCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.StringList));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Has Tags"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tag Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tag Color"));

        }
    }
}
