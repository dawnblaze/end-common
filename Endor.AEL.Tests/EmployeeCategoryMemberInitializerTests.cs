﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Endor.EF;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Linq.Expressions;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class EmployeeCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task TestEmployeeCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var testEmployee = new EmployeeData()
            {
                BID = testBID,
                ID = -99,
                First = "TESTFirst",
                Last = "TESTLast",
                LocationID = (await ctx.LocationData.FirstOrDefaultAsync(x => x.BID == testBID)).ID
            };
            var phoneLocator = new EmployeeLocator()
            {
                BID = testBID,
                ID = -99,
                ParentID = testEmployee.ID,
                RawInput = "5554443210",
                LocatorType = (byte)LocatorType.Phone,
                SortIndex = -1,
                ModifiedDT = DateTime.UtcNow
            };
            var emailLocator = new EmployeeLocator()
            {
                BID = testBID,
                ID = -98,
                ParentID = testEmployee.ID,
                RawInput = "none@none.com",
                LocatorType = (byte)LocatorType.Email,
                SortIndex = -1,
                ModifiedDT = DateTime.UtcNow
            };

            try
            {
                ctx.EmployeeLocator.RemoveRange(ctx.EmployeeLocator.Where(oi => oi.ID == -99));
                ctx.EmployeeLocator.RemoveRange(ctx.EmployeeLocator.Where(oi => oi.ID == -98));
                ctx.EmployeeData.RemoveRange(ctx.EmployeeData.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();

                ctx.EmployeeData.Add(testEmployee);
                ctx.EmployeeLocator.Add(phoneLocator);
                ctx.EmployeeLocator.Add(emailLocator);
                Assert.AreEqual(3, await ctx.SaveChangesAsync());

                var employee = await ctx.EmployeeData
                    .Include("EmployeeLocators")
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testEmployee.ID);

                Assert.IsNotNull(employee);
                Assert.IsNotNull(employee.EmployeeLocators);
                Assert.AreEqual(2, employee.EmployeeLocators.Count);

                var employeeID = AELTestHelper.GetPropertyResult<decimal?, EmployeeData>(testBID, DataType.EmployeeCategory, "Name", employee);
                Assert.AreEqual(employee.ID, employeeID);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "Employee Name", null));
                var shortNameTest = AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "Employee Name", employee);
                Assert.AreEqual(employee.ShortName, shortNameTest);
                
                Assert.IsNull(AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "First Name", null));
                var firstNameTest = AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "First Name", employee);
                Assert.AreEqual(employee.First, firstNameTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "Last Name", null));
                var lastNameTest = AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "Last Name", employee);
                Assert.AreEqual(employee.Last, lastNameTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "Email", null));
                var emailTest = AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "Email", employee);
                Assert.AreEqual(emailLocator.RawInput, emailTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "Phone Number", null));
                var phoneTest = AELTestHelper.GetPropertyResult<string, EmployeeData>(testBID, DataType.EmployeeCategory, "Phone Number", employee);
                Assert.AreEqual(phoneLocator.RawInput, phoneTest);
            }
            finally
            {
                ctx.EmployeeLocator.Remove(emailLocator);
                ctx.EmployeeLocator.Remove(phoneLocator);
                ctx.EmployeeData.Remove(testEmployee);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestEmployeeCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.EmployeeCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(6, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Employee));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.String));
            
            // NO_DATATYPE like this as of the moment
            //Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.StringList));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Employee Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "First Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Last Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Email"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Phone Number"));

        }
    }
}
