﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class NotesCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task TestNotesCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var orderID = (await ctx.OrderData.FirstOrDefaultAsync()).ID;
            var testNote = new OrderNote()
            {
                BID = testBID,
                ID = -99,
                Note = "TestNote",
                NoteType = OrderNoteType.Design,
                OrderID = orderID
            };

            var testNote2 = new OrderNote()
            {
                BID = testBID,
                ID = -98,
                Note = "TestNote2",
                NoteType = OrderNoteType.Customer,
                OrderID = orderID
            };

            try
            {
                ctx.RemoveRange(ctx.OrderNote.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderNote.Where(oi => oi.ID == -98));
                await ctx.SaveChangesAsync();

                ctx.OrderNote.Add(testNote);
                ctx.OrderNote.Add(testNote2);
                Assert.AreEqual(2, await ctx.SaveChangesAsync());

                OrderData order = ctx.OrderData.FirstOrDefault(x => x.BID == testBID && x.ID == orderID);

                var hasTagsTest = AELTestHelper.GetPropertyResult<List<string>, OrderData>(testBID, DataType.TransactionHeaderNotesCategory, "Internal Notes", order);
                Assert.AreEqual(hasTagsTest.FirstOrDefault(), "TestNote");

                var tagNameTest = AELTestHelper.GetPropertyResult<List<string>, OrderData>(testBID, DataType.TransactionHeaderNotesCategory, "Customer Notes", order);
                Assert.AreEqual(tagNameTest.FirstOrDefault(), "TestNote2");

                var tagColorTest = AELTestHelper.GetPropertyResult<List<string>, OrderData>(testBID, DataType.TransactionHeaderNotesCategory, "Notes (Any)", order);
                Assert.AreEqual(tagColorTest.Count,2);

            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderNote.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderNote.Where(oi => oi.ID == -98));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestNotesCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.TransactionHeaderNotesCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.StringList));


            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Internal Notes"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Customer Notes"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Notes (Any)"));

        }
    }
}
