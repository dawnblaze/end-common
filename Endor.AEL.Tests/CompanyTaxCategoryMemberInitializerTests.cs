﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.AEL.MemberInitializers;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class CompanyTaxCategoryMemberInitializerTests
    {
        [TestMethod]
        public void TestCompanyTaxCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.CompanyTaxCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.TaxGroup));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.TaxExemptReason));


            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax Exempt"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax Exempt Reason"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax Group"));


        }
    }
}
