﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemPricesMemberInitializerTests
    {
        [TestMethod]
        public async Task TestOrderItemPricesSimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var testOrderItem = new OrderItemData()
            {
                BID = testBID,
                ID = -99,
                ItemNumber = 1,
                Description = "TestDescription",
                Quantity = 2,
                Name = "TestName",
                HasDocuments = true,
                HasProof = true,
                HasCustomImage = true,
                OrderID = ctx.OrderData.FirstOrDefault().ID,
                TransactionType = 2,
                OrderStatusID = OrderOrderStatus.OrderBuilt,
                ItemStatusID = ctx.OrderItemStatus.FirstOrDefault().ID,
                ProductionLocationID = ctx.LocationData.FirstOrDefault().ID,
                TaxGroupID = ctx.TaxGroup.FirstOrDefault().ID,
                IsTaxExempt = false,
                TaxGroupOV = false,
                PriceComponent = 1,
                PriceSurcharge = 2,
                PriceItemDiscountPercent = 50,
                PriceAppliedOrderDiscountPercent = 40,
                PriceItemDiscount = 3,
                PriceAppliedOrderDiscount = 4,
                PriceTax = 5
            };

            try
            {
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderItemData.Add(testOrderItem);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                var orderItemTag = await ctx.OrderItemData
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testOrderItem.ID);

                Assert.IsNotNull(orderItemTag);

                var pricePreTaxTest = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Price (Pre Tax)", testOrderItem);
                Assert.AreEqual(pricePreTaxTest, -4m);

                var priceAfterTaxTest = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Price (After Tax)", testOrderItem);
                Assert.AreEqual(priceAfterTaxTest, 1m);

                var priceUnitPriceTest = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Unit Price (Pre Tax)", testOrderItem);
                Assert.AreEqual(priceUnitPriceTest, -2m);

                var listItemDiscountPercent = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Line Item Discount Percent", testOrderItem);
                Assert.AreEqual(listItemDiscountPercent, 70m);

                var lineItemDiscountAmount = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Line Item Discount Amount", testOrderItem);
                Assert.AreEqual(lineItemDiscountAmount, 7m);

                var SetupFees = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Setup Fees", testOrderItem);
                Assert.AreEqual(SetupFees, 2m);


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderItemTagLink.Where(oi => oi.OrderItemID == -99));
                ctx.RemoveRange(ctx.ListTag.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemPricesMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.OrderItemPricesCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(6, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Number));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Price (Pre Tax)"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Price (After Tax)"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Unit Price (Pre Tax)"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Line Item Discount Percent"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Line Item Discount Amount"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Setup Fees"));

        }
    }
}
