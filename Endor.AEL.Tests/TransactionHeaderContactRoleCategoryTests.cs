﻿using Endor.AEL.Tests.Helper;
using Endor.Common.Tests;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class TransactionHeaderContactRoleCategoryTests
    {
        [TestMethod]
        public async Task TestPrimaryProperty()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);
            var order = ctx.OrderData.FirstOrDefault();
            var testSubject = new OrderContactRole()
            {
                BID = testBID,
                ID = -99,
                OrderID = order.ID,
                RoleType = OrderContactRoleType.Primary,
                IsAdHoc = false,
                ContactID = ctx.ContactData.FirstOrDefault().ID,
                IsOrderRole = true,
                IsOrderItemRole = false,
                IsDestinationRole = false,
            };
            try
            {
                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderContactRole.Add(testSubject);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                order = ctx.OrderData.IncludeAll(new string[] { "ContactRoles", "ContactRoles.Contact" }).FirstOrDefault();
                var result = AELTestHelper.GetPropertyResult<ICollection<ContactData>, TransactionHeaderData>(testBID, DataType.TransactionHeaderContactRoleCategory, "Primary", order);
                Assert.AreEqual(testSubject.Contact, result.FirstOrDefault());
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task TestShippingProperty()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);
            var order = ctx.OrderData.FirstOrDefault();
            var testSubject = new OrderContactRole()
            {
                BID = testBID,
                ID = -99,
                OrderID = order.ID,
                RoleType = OrderContactRoleType.ShipTo,
                IsAdHoc = false,
                ContactID = ctx.ContactData.FirstOrDefault().ID,
                IsOrderRole = true,
                IsOrderItemRole = false,
                IsDestinationRole = false,
            };
            try
            {
                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderContactRole.Add(testSubject);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                order = ctx.OrderData.IncludeAll(new string[] { "ContactRoles", "ContactRoles.Contact" }).FirstOrDefault();
                var result = AELTestHelper.GetPropertyResult<ICollection<ContactData>, TransactionHeaderData>(testBID, DataType.TransactionHeaderContactRoleCategory, "Shipping", order);
                Assert.AreEqual(testSubject.Contact, result.FirstOrDefault());
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();
            }
        }
    }
}
