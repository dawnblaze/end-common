﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemTaxesCategoryTests
    {
        [TestMethod]
        public async Task TestOrderItemTaxesCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);
            var order = ctx.OrderData.FirstOrDefault();
            var testOrderItem = new OrderItemData()
            {
                BID = testBID,
                ID = -99,
                ItemNumber = 1,
                Description = "TestDescription",
                Quantity = 2,
                Name = "TestName",
                HasDocuments = true,
                HasProof = true,
                HasCustomImage = true,
                OrderID = ctx.OrderData.FirstOrDefault().ID,
                TransactionType = 2,
                OrderStatusID = OrderOrderStatus.OrderBuilt,
                ItemStatusID = ctx.OrderItemStatus.FirstOrDefault().ID,
                ProductionLocationID = ctx.LocationData.FirstOrDefault().ID,
                TaxGroupID = ctx.TaxGroup.FirstOrDefault(x => x.ID != 1).ID,
                TaxExemptReasonID = ctx.FlatListItem.FirstOrDefault().ID,
                IsTaxExempt = false,
                TaxGroupOV = false,
            };

            try
            {
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderItemData.Add(testOrderItem);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                var taxRate = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Rate", testOrderItem);
                Assert.AreEqual(taxRate, order.PriceTaxRate);

                var taxExempt = AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Exempt", testOrderItem);
                Assert.AreEqual(taxExempt, testOrderItem.IsTaxExempt);

                var taxAmount = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Amount", testOrderItem);
                Assert.AreEqual(taxAmount, testOrderItem.PriceTax);

                var taxGroup = AELTestHelper.GetPropertyResult<TaxGroup, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Group", testOrderItem);
                Assert.AreEqual(taxGroup, testOrderItem.TaxGroup);

                var taxExemptReason = AELTestHelper.GetPropertyResult<FlatListItem, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Exempt Reason", testOrderItem);
                Assert.AreEqual(taxExemptReason, testOrderItem.TaxExemptReason);


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderItemTagLink.Where(oi => oi.OrderItemID == -99));
                ctx.RemoveRange(ctx.ListTag.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemTaxesCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.OrderItemTaxesCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(5, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Number));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.TaxGroup));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.TaxExemptReason));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax Rate"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax Exempt"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax Amount"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax Group"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax Exempt Reason"));

        }
    }
}
