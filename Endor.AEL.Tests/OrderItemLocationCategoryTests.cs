﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;


namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemLocationCategoryTests
    {
        [TestMethod]
        public async Task TestOrderItemLocationCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);
            var order = ctx.OrderData.FirstOrDefault();
            var testOrderItem = new OrderItemData()
            {
                BID = testBID,
                ID = -99,
                ItemNumber = 1,
                Description = "TestDescription",
                Quantity = 2,
                Name = "TestName",
                HasDocuments = true,
                HasProof = true,
                HasCustomImage = true,
                OrderID = ctx.OrderData.FirstOrDefault().ID,
                TransactionType = 2,
                OrderStatusID = OrderOrderStatus.OrderBuilt,
                ItemStatusID = ctx.OrderItemStatus.FirstOrDefault().ID,
                ProductionLocationID = ctx.LocationData.FirstOrDefault().ID,
                TaxGroupID = ctx.TaxGroup.FirstOrDefault().ID,
                IsTaxExempt = false,
                TaxGroupOV = false
            };

            try
            {
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderItemData.Add(testOrderItem);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                var salesLocationID = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemLocationCategory, "Sales Location", testOrderItem);
                Assert.AreEqual(salesLocationID, order.LocationID);

                var productionLocationID = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemLocationCategory, "Sales Location", testOrderItem);
                Assert.AreEqual(productionLocationID, order.ProductionLocationID);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderItemTagLink.Where(oi => oi.OrderItemID == -99));
                ctx.RemoveRange(ctx.ListTag.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemLocationCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.OrderItemLocationCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(2, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Location));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Sales Location"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Production Location"));

        }
    }
}
