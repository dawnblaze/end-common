﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemStatusMemberInitializerTests
    {
        [TestMethod]
        public async Task TestOrderItemStatusSimpleProperties()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);

            try
            {
                var existingOIC = await ctx.OrderItemComponent.Where(e => e.BID == testBID && (e.OrderID < 0 || e.OrderItemID < 0 || e.ID < 0)).ToListAsync();
                if (existingOIC != null && existingOIC.Count() > 0)
                {
                    ctx.OrderItemComponent.RemoveRange(existingOIC);
                    await ctx.SaveChangesAsync();
                }

                var existingOI = await ctx.OrderItemData.Where(e => e.BID == testBID && (e.OrderID < 0 || e.ID < 0)).ToListAsync();
                if (existingOI != null && existingOI.Count() > 0)
                {
                    ctx.OrderItemData.RemoveRange(existingOI);
                    await ctx.SaveChangesAsync();
                }

                var order = await ctx.OrderData.FirstOrDefaultAsync(e => e.BID == testBID && e.TransactionType == 2);
                var orderItemStatusID = (await ctx.OrderItemStatus.FirstOrDefaultAsync(e => e.OrderStatusID == order.OrderStatusID)).ID;
                var subStatusID = (await ctx.OrderItemSubStatus.FirstOrDefaultAsync()).ID;

                var testOrderItem = new OrderItemData()
                {
                    BID = testBID,
                    ID = -99,
                    ItemNumber = 1,
                    Description = "TestDescription",
                    Quantity = 2,
                    Name = "TestName",
                    HasDocuments = true,
                    HasProof = true,
                    HasCustomImage = true,
                    OrderID = order.ID,
                    TransactionType = 2,
                    OrderStatusID = order.OrderStatusID,
                    ItemStatusID = orderItemStatusID,
                    SubStatusID = subStatusID,
                    ProductionLocationID = ctx.LocationData.FirstOrDefault().ID,
                    TaxGroupID = ctx.TaxGroup.FirstOrDefault().ID,
                    IsTaxExempt = false,
                    TaxGroupOV = false,
                    PriceTax = 4
                };

                ctx.OrderItemData.Add(testOrderItem);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                var orderItemTag = await ctx.OrderItemData
                    .Include(oi => oi.OrderItemStatus).Include(oi => oi.SubStatus)
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testOrderItem.ID);

                Assert.IsNotNull(orderItemTag);

                var orderItemStatusIDResult = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemStatusCategory, "Item Status", testOrderItem);
                Assert.AreEqual(orderItemStatusIDResult, orderItemStatusID);

                var subStatusIDResult = AELTestHelper.GetPropertyResult<decimal?, OrderItemData >(testBID, DataType.LineItemStatusCategory, "Sub status", testOrderItem);
                Assert.AreEqual(subStatusIDResult, subStatusID);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemStatusMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.LineItemStatusCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(2, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.ItemStatus));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Substatus));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Item Status"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Sub status"));

        }
    }
}
