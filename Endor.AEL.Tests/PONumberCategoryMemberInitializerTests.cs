﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.AEL.MemberInitializers;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class PONumberCategoryMemberInitializerTests
    {
        [TestMethod]
        public void TestPONumberCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.PONumberCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(2, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.String));


            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Default PO Number"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "PO Required"));

        }
    }
}
