﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class PricesCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task TestPricesCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var testOrder = new OrderData()
            {
                BID = testBID,
                ID = -99,
                Description = $"Test.Order",
                CompanyID = ctx.CompanyData.FirstOrDefault().ID,
                LocationID = 1,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                Number = 9999,
                FormattedNumber = "ORD-9999",
                PickupLocationID = 1,
                ProductionLocationID = 1,
                TaxGroupID = ctx.TaxGroup.FirstOrDefault().ID,
                PriceProductTotal = 3m,
                PriceDiscount = 4m,
                PriceDestinationTotal = 5m,
                PriceFinanceCharge = 6m,
                PriceTax = 1m
            };

            try
            {
                ctx.OrderData.Add(testOrder);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                var orderItemTag = await ctx.OrderData
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testOrder.ID);

                Assert.IsNotNull(orderItemTag);

                var pricePreTaxTest = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.PricesCategory, "Price (Pre Tax)", orderItemTag);
                Assert.AreEqual(pricePreTaxTest, 10);

                var priceAfterTaxTest = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.PricesCategory, "Price (After Tax)", orderItemTag);
                Assert.AreEqual(priceAfterTaxTest, 11);

                var priceUnitPriceTest = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.PricesCategory, "Line Item Total", orderItemTag);
                Assert.AreEqual(priceUnitPriceTest, 3);

                var listItemDiscountPercent = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.PricesCategory, "Destination Total", orderItemTag);
                Assert.AreEqual(listItemDiscountPercent, 5);

                var lineItemDiscountAmount = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.PricesCategory, "Finance Charge", orderItemTag);
                Assert.AreEqual(lineItemDiscountAmount, 6);

                var SetupFees = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.PricesCategory, "Discount Amount", orderItemTag);
                Assert.AreEqual(SetupFees, 4);


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.OrderData.Remove(testOrder);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestPricesCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.PricesCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(6, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Number));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Price (Pre Tax)"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Price (After Tax)"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Line Item Total"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Destination Total"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Finance Charge"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Discount Amount"));

        }
    }
}
