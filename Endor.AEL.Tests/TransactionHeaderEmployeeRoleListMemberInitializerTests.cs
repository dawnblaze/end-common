﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class TransactionHeaderEmployeeRoleListMemberInitializerTests
    {
        [TestMethod]
        public async Task TestTransactionHeaderEmployeeRoleListSimpleProperties()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);

            var order = ctx.TransactionHeaderData.Include("EmployeeRoles").Where(t => !t.EmployeeRoles.Any()).FirstOrDefault();
            if (order == null)
                order = ctx.TransactionHeaderData.FirstOrDefault();
            if (order == null)
                Assert.Fail("No OrderData found. Cannot continue test without first creating OrderData.");

            var employeeRole1 = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -99,
                EmployeeID = ctx.EmployeeData.FirstOrDefault().ID,
                RoleID = SystemIDs.EmployeeRole.AssignedTo,
                OrderID = order.ID
            };
            var employeeRole2 = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -98,
                EmployeeID = ctx.EmployeeData.FirstOrDefault().ID,
                RoleID = SystemIDs.EmployeeRole.EnteredBy,
                OrderID = order.ID
            };

            try
            {
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -98));
                await ctx.SaveChangesAsync();

                ctx.OrderEmployeeRole.Add(employeeRole1);
                ctx.OrderEmployeeRole.Add(employeeRole2);
                Assert.AreEqual(2, await ctx.SaveChangesAsync());

                var RolesTest = AELTestHelper.GetPropertyResult<ICollection<EmployeeData>, TransactionHeaderData>(testBID, DataType.TransactionHeaderEmployeeRoleList, "Employees in Any Role", order );
                Assert.IsTrue(RolesTest.Count() > 0);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -98));
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestTransactionHeaderEmployeeRoleListMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.TransactionHeaderEmployeeRoleList);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(1, memberInfo.Members.Where(x => x.RelatedID == 0).Count());
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.EmployeeCategory));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Employees in Any Role"));

        }
    }
}
