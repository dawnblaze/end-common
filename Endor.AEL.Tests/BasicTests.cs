using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class BasicTests
    {
        const short testBID = 1;

        [TestMethod]
        public void TestAELStaticData()
        {
            Assert.IsNotNull(DataTypeDictionary.Instance);
            Assert.IsNotNull(DataTypeDictionary.Instance[DataType.Boolean]);
            Assert.IsNotNull(DataTypeDictionary.Instance[DataType.Order]);
            Assert.IsNotNull(DataTypeDictionary.Instance[DataType.OrderList]);

            Assert.IsNotNull(OperatorDictionary.Instance);
            Assert.IsNotNull(OperatorDictionary.Instance[OperatorType.EQ]);
        }

        [TestMethod]
        public void TestAELMemberData()
        {
            Assert.IsNotNull(AELTestHelper.GetDictionaryInstance(testBID));
            Assert.IsNotNull(AELTestHelper.GetDictionaryInstance(testBID));
            Assert.IsTrue(AELTestHelper.GetDictionaryInstance(testBID).Count > 0);

            foreach(KeyValuePair<DataType, DataTypeMemberInfo> kvp in AELTestHelper.GetDictionaryInstance(testBID))
            {
                Assert.IsNotNull(kvp.Key);
                Assert.IsNotNull(kvp.Value);

                if (kvp.Value.Members != null && kvp.Value.Members.Count > 0)
                    Assert.IsFalse(String.IsNullOrWhiteSpace(kvp.Value.Members[0].Text));
                break;
            }
        }

        [TestMethod]
        public void TestAELMemberDataSerialization()
        {
            string serialized = JsonConvert.SerializeObject(AELTestHelper.GetDictionaryInstance(testBID));
            Assert.IsFalse(String.IsNullOrWhiteSpace(serialized));
            Console.WriteLine(serialized);
        }

        [TestMethod]
        public void TestAELOperators()
        {
            var dictionary = AEL.OperatorDictionary.Instance;
            var json = JsonConvert.SerializeObject(dictionary);
            Console.WriteLine(json);
            //no assert needed. just need to see output.
        }
    }
}
