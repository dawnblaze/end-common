﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemEmployeeRoleListTests
    {
        [TestMethod]
        public async Task TestOrderItemEmployeeRoleListSimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);
            var lineItem = ctx.OrderItemData.Include(i => i.Order).FirstOrDefault();
            var employees = ctx.EmployeeData.Take(2);
            var employeeRole1 = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -99,
                EmployeeID = employees.FirstOrDefault().ID,
                RoleID = SystemIDs.EmployeeRole.AssignedTo,
                OrderID = lineItem.Order.ID,
                OrderItemID = lineItem.ID
            };
            var employeeRole2 = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -98,
                EmployeeID = employees.OrderByDescending(x => x.ID).First().ID,
                RoleID = SystemIDs.EmployeeRole.EnteredBy,
                OrderID = lineItem.Order.ID,
                OrderItemID = lineItem.ID
            };
            try
            {
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -99 || oi.ID == -98));
                await ctx.SaveChangesAsync();

                ctx.OrderEmployeeRole.Add(employeeRole1);
                ctx.OrderEmployeeRole.Add(employeeRole2);
                Assert.AreEqual(2, await ctx.SaveChangesAsync());

                var RolesTest = AELTestHelper.GetPropertyResult<ICollection<EmployeeData>, OrderItemData>(testBID, DataType.OrderItemEmployeeRoleList, "Employees in Any Role", lineItem);
                if (employeeRole1.EmployeeID != employeeRole2.ID)
                    Assert.AreEqual(RolesTest.Count(), 2);
                else
                    Assert.AreEqual(RolesTest.Count(), 1);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -99 || oi.ID == -98));
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemEmployeeRoleListMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.OrderItemEmployeeRoleList);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(1, memberInfo.Members.Where(x => x.RelatedID == 0).Count());
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.EmployeeCategory));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Employees in Any Role"));

        }
    }
}
