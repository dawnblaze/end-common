﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.Common.Tests;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemMemberInitializerTests
    {
        private readonly string[] includes = { "Order", "Notes", "TagLinks", "TagLinks.Tag", "Dates", "Components" };

        [TestMethod]
        public async Task TestOrderItemSimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var testOrderItem = new OrderItemData()
            {
                BID = testBID,
                ID = -99,
                ItemNumber = 1,
                Description = "TestDescription",
                Quantity = 2,
                Name = "TestName",
                HasDocuments = true,
                HasProof = true,
                HasCustomImage = true,
                OrderID = ctx.OrderData.FirstOrDefault().ID,
                TransactionType = 2,
                OrderStatusID = OrderOrderStatus.OrderBuilt,
                ItemStatusID = ctx.OrderItemStatus.FirstOrDefault().ID,
                ProductionLocationID = ctx.LocationData.FirstOrDefault().ID,
                TaxGroupID = ctx.TaxGroup.FirstOrDefault().ID,
                IsTaxExempt = false,
                TaxGroupOV = false
            };

            SurchargeDef surchargeDefTest = new SurchargeDef()
            {
                Name = "Test Surcharge",
                IsActive = true,
                ID = -99,
                BID = 1,
                IncomeAccountID = ctx.GLAccount.FirstOrDefault().ID,
                TaxCodeID = ctx.TaxabilityCodes.FirstOrDefault().ID
            };

            var surcharge = new OrderItemSurcharge()
            {
                BID = testBID,
                ID = -99,
                IncomeAccountID = ctx.GLAccount.FirstOrDefault().ID,
                Name = "Test Surcharge",
                OrderItemID = -99,
                Number = 1,
                PricePreTax = 0m,
                PriceIsOV = false,
                SurchargeDefID = -99,
                TaxCodeID = ctx.TaxabilityCodes.FirstOrDefault().ID,
                
            };

            var orderEmployeeRole = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -99,
                IsOrderItemRole = true,
                EmployeeName = "Test",
                EmployeeID = ctx.EmployeeData.FirstOrDefault().ID,
                OrderItemID = testOrderItem.ID,
                OrderID = testOrderItem.OrderID,
                RoleID = ctx.EmployeeRole.FirstOrDefault().ID
            };

            var orderContactRole = new OrderContactRole()
            {
                BID = testBID,
                ID = -99,
                IsOrderItemRole = true,
                ContactCompany = "Test",
                ContactName = "Test",
                ContactID = ctx.ContactData.FirstOrDefault().ID,
                RoleType = OrderContactRoleType.Primary,
                OrderItemID = testOrderItem.ID,
                OrderID = testOrderItem.OrderID,
                IsAdHoc = true,
            };

            var orderNote = new OrderNote()
            {
                BID = testBID,
                ID = -99,
                ContactID = ctx.ContactData.FirstOrDefault().ID,
                EmployeeID = ctx.EmployeeData.FirstOrDefault().ID,
                IsActive = true,
                IsOrderItemNote = true,
                Note = "Test",
                NoteType = OrderNoteType.Customer,
                OrderItemID = testOrderItem.ID,
                OrderID = testOrderItem.OrderID,
            };

            try
            {
                ctx.RemoveRange(ctx.OrderNote.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.SurchargeDef.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemSurcharge.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderNote.Add(orderNote);
                ctx.OrderContactRole.Add(orderContactRole);
                ctx.OrderEmployeeRole.Add(orderEmployeeRole);
                ctx.SurchargeDef.Add(surchargeDefTest);
                ctx.OrderItemData.Add(testOrderItem);
                ctx.OrderItemSurcharge.Add(surcharge);
                await ctx.SaveChangesAsync();

                var orderItem = await ctx.OrderItemData.IncludeAll(includes)
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testOrderItem.ID);

                Assert.IsNotNull(orderItem);

                Assert.IsNull(AELTestHelper.GetPropertyResult<ICollection<decimal?>, OrderItemData>(testBID, DataType.LineItemCategory, "Setup Fees", null));
                var surchargeTestResults = AELTestHelper.GetPropertyResult<ICollection<decimal?>, OrderItemData>(testBID, DataType.LineItemCategory, "Setup Fees", orderItem);
                Assert.AreEqual(surchargeTestResults.ToArray()[0], surcharge.PricePreTax);

                Assert.IsNull(AELTestHelper.GetPropertyResult<TransactionHeaderData, OrderItemData>(testBID, DataType.LineItemCategory, "Order", null));
                var orderTest = AELTestHelper.GetPropertyResult<TransactionHeaderData, OrderItemData>(testBID, DataType.LineItemCategory, "Order", orderItem);
                Assert.AreEqual(orderTest.ID, testOrderItem.OrderID);

                Assert.IsNull(AELTestHelper.GetPropertyResult<TransactionHeaderData, OrderItemData>(testBID, DataType.LineItemCategory, "Estimate", null));
                var estimateTest = AELTestHelper.GetPropertyResult<TransactionHeaderData, OrderItemData>(testBID, DataType.LineItemCategory, "Estimate", orderItem);
                Assert.AreEqual(orderTest.ID, testOrderItem.OrderID);

                Assert.IsNull(AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Employees", null));
                var orderItemEmployeeRolesTest = AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Employees", orderItem);
                Assert.AreEqual(orderItemEmployeeRolesTest.EmployeeRoles.FirstOrDefault().EmployeeID, orderEmployeeRole.EmployeeID);
                Assert.AreEqual(orderItemEmployeeRolesTest.EmployeeRoles.FirstOrDefault().EmployeeName, orderEmployeeRole.EmployeeName);

                Assert.IsNull(AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Contacts", null));
                var orderItemContactsTest = AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Contacts", orderItem);
                Assert.AreEqual(orderItemContactsTest.ContactRoles.FirstOrDefault().ContactID, orderContactRole.ContactID);
                Assert.AreEqual(orderItemContactsTest.ContactRoles.FirstOrDefault().ContactName, orderContactRole.ContactName);

                Assert.IsNull(AELTestHelper.GetPropertyResult<ICollection<OrderNote>, OrderItemData>(testBID, DataType.LineItemCategory, "Notes", null));
                var notesTest = AELTestHelper.GetPropertyResult<ICollection<OrderNote>, OrderItemData>(testBID, DataType.LineItemCategory, "Notes", orderItem);
                Assert.AreEqual(notesTest.FirstOrDefault().ID, orderNote.ID);
                Assert.AreEqual(notesTest.FirstOrDefault().Note, orderNote.Note);

                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemCategory, "Line Item Number", null));
                var lineItemNumberTest = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemCategory, "Line Item Number", orderItem);
                Assert.AreEqual(orderItem.ItemNumber, lineItemNumberTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, OrderItemData>(testBID, DataType.LineItemCategory, "Description", null));
                var descriptionTest = AELTestHelper.GetPropertyResult<string, OrderItemData>(testBID, DataType.LineItemCategory, "Description", orderItem);
                Assert.AreEqual(orderItem.Description, descriptionTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemCategory, "Quantity", null));
                var quantityTest = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemCategory, "Quantity", orderItem);
                Assert.AreEqual(orderItem.Quantity, quantityTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, OrderItemData>(testBID, DataType.LineItemCategory, "Name", null));
                var nameTest = AELTestHelper.GetPropertyResult<string, OrderItemData>(testBID, DataType.LineItemCategory, "Name", orderItem);
                Assert.AreEqual(orderItem.Name, nameTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Documents", null));
                var hasDocumentsTest = AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Documents", orderItem);
                Assert.AreEqual(orderItem.HasDocuments, hasDocumentsTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Proof", null));
                var hasProofTest = AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Proof", orderItem);
                Assert.AreEqual(orderItem.HasProof, hasProofTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Custom Image", null));
                var hasCustomImageTest = AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Custom Image", orderItem);
                Assert.AreEqual(orderItem.HasCustomImage, hasCustomImageTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Tags", null));
                var tagsTest = AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Tags", orderItem);

                Assert.IsNull(AELTestHelper.GetPropertyResult<ICollection<OrderItemComponent>, OrderItemData>(testBID, DataType.LineItemCategory, "Components", null));
                var componentsTest = AELTestHelper.GetPropertyResult<ICollection<OrderItemComponent>, OrderItemData>(testBID, DataType.LineItemCategory, "Components", orderItem);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderNote.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.SurchargeDef.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemSurcharge.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));
                
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.LineItemCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(20, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.NumberList));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.EstimateCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemEmployeeRoleListCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemContactRoleCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.LineItemNotesCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemLocationCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.String));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.LineItemStatusCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemTaxesCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemPricesCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemCostsCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemTagsCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemComponentCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Number));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Setup Fees"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Order"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Estimate"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Employees"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Contacts"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Notes"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Locations"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Line Item Number"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Description"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Quantity"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Status"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Taxes"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Prices"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Cost"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Has Documents"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Has Proof"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Has Custom Image"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tags"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Components"));
        }


    }
}
