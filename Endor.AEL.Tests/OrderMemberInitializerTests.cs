﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.Common.Tests;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderMemberInitializerTests
    {
        private readonly string[] includes = { "Company", "ContactRoles", "Dates", "EmployeeRoles", "Items", "Location", "Origin", "ProductionLocation" };

        [TestMethod]
        public async Task TestOrderSimpleProperties()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);
            OrderData nullOrder = null;

            var testDateStr = DateTime.UtcNow.ToString();
            var testComp = GetTestCompany(timestamp: testDateStr);
            var testOrder = GetTestOrder(timestamp: testDateStr, companyID: testComp.ID);
            var taxGroup = new TaxGroup
            {
                BID = testBID, 
                ID = -99,
                IsActive = true,
                IsTaxExempt = true,
                Name = "TaxGroup.1"
            };

            try
            {
                testOrder.TaxGroup = taxGroup;
                ctx.TaxGroup.Add(taxGroup);
                ctx.CompanyData.Add(testComp);
                ctx.OrderData.Add(testOrder);
                Assert.AreEqual(3, await ctx.SaveChangesAsync());

                var order = await ctx.OrderData.IncludeAll(includes)
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testComp.ID);

                //PROPERTY_NO_LONGER_EXIST
                //Assert.IsNull(AELTestHelper.GetPropertyResult<List<OrderItemData>, OrderData>(testBID, DataType.OrderCategory, "Line Items", nullOrder));
                //var lineItems = AELTestHelper.GetPropertyResult<List<OrderItemData>, OrderData>(testBID, DataType.OrderCategory, "Line Items", order);
                //Assert.AreEqual(order.Items?.ToList()?.GetType(), lineItems?.GetType());
                //Assert.AreEqual(order.Items?.Count(), lineItems?.Count());

                Assert.IsNull(AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Contacts", nullOrder));
                var contactRoles = AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Contacts", order);
                Assert.AreEqual(order.GetType(), contactRoles?.GetType());

                Assert.IsNull(AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Employees", nullOrder));
                var employeeRoles = AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Employees", order);
                Assert.AreEqual(order.GetType(), employeeRoles?.GetType());

                Assert.IsNull(AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Locations", nullOrder));
                var locationsCat = AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Locations", order);
                Assert.IsNotNull(locationsCat);

                Assert.IsNull(AELTestHelper.GetPropertyResult<CrmOrigin, OrderData>(testBID, DataType.OrderCategory, "Order Origin", nullOrder));
                var origin = AELTestHelper.GetPropertyResult<CrmOrigin, OrderData>(testBID, DataType.OrderCategory, "Order Origin", order);
                Assert.AreEqual(order.Origin, origin);

                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.OrderCategory, "Order Number", nullOrder));
                var orderNumber = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.OrderCategory, "Order Number", order);
                Assert.AreEqual(order.Number, orderNumber);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, OrderData>(testBID, DataType.OrderCategory, "Description", nullOrder));
                var description = AELTestHelper.GetPropertyResult<string, OrderData>(testBID, DataType.OrderCategory, "Description", order);
                Assert.AreEqual(order.Description, description);

                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, OrderData>(testBID, DataType.OrderCategory, "Company", nullOrder));
                var company = AELTestHelper.GetPropertyResult<CompanyData, OrderData>(testBID, DataType.OrderCategory, "Company", order);
                Assert.AreEqual(order.Company, company);

                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.OrderCategory, "Status", nullOrder));
                var status = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.OrderCategory, "Status", order);
                Assert.AreEqual((decimal?)order.OrderStatusID, status);

                Assert.IsNull(AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Taxes", nullOrder));
                var taxCat = AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Taxes", order);
                Assert.IsNotNull(taxCat);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, OrderData>(testBID, DataType.OrderCategory, "PO Number", nullOrder));
                var poNumber = AELTestHelper.GetPropertyResult<string, OrderData>(testBID, DataType.OrderCategory, "PO Number", order);
                Assert.AreEqual(order.OrderPONumber, poNumber);

                Assert.IsNull(AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Tags", nullOrder));
                var tags = AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Tags", order);
                Assert.AreEqual(order.ID, tags.ID);

                Assert.IsNull(AELTestHelper.GetPropertyResult<List<OrderNote>, OrderData>(testBID, DataType.OrderCategory, "Notes", nullOrder));
                var notes = AELTestHelper.GetPropertyResult<List<OrderNote>, OrderData>(testBID, DataType.OrderCategory, "Notes", order);
                Assert.AreEqual(order.Notes?.GetType(), notes?.GetType());
                Assert.AreEqual(order.Notes?.Count(), notes?.Count());

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, OrderData>(testBID, DataType.OrderCategory, "Has Documents", nullOrder));
                var hasDocuments = AELTestHelper.GetPropertyResult<bool?, OrderData>(testBID, DataType.OrderCategory, "Has Documents", order);
                Assert.AreEqual(order.HasDocuments, hasDocuments);                
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);
                ctx.OrderData.Remove(testOrder);
                ctx.CompanyData.Remove(testComp);
                ctx.TaxGroup.Remove(taxGroup);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task TestOrderContactRolesProperty()
        {
            short testBID = 1;
            #region Null Order Check

            OrderData nullOrder = null;
            var nullContacts = AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory, "Contacts", nullOrder);
            Assert.IsNull(nullContacts);

            #endregion

            var ctx = AELTestHelper.GetMockCtx(testBID);

            #region Setup Data

            var testDateStr = DateTime.UtcNow.ToString();
            var testComp = GetTestCompany(timestamp: testDateStr);
            var testOrder = GetTestOrder(timestamp: testDateStr, companyID: testComp.ID);
            var taxGroup = new TaxGroup
            {
                BID = testBID,
                ID = -99,
                IsActive = true,
                IsTaxExempt = true,
                Name = "TaxGroup.1"
            };

            testOrder.TaxGroupID = taxGroup.ID;

            var testCont1 = new ContactData
            {
                BID = testBID,
                ID = -99,
                //CompanyID = testComp.ID, end-4814
                First = "ContactData.1",
                Last = testDateStr,
                LocationID = 1,
            };

            // salesperson
            var contactRole1 = new OrderContactRole()
            {
                BID = testBID,
                ID = -99,
                RoleType = (OrderContactRoleType)1,
                ContactID = testCont1.ID,
                OrderID = testOrder.ID
            };

            ctx.ContactData.Add(testCont1);
            ctx.TaxGroup.Add(taxGroup);
            ctx.OrderContactRole.Add(contactRole1);
            ctx.CompanyData.Add(testComp);
            ctx.OrderData.Add(testOrder);

            #endregion Setup Data

            try
            {
                Assert.AreEqual(5, await ctx.SaveChangesAsync());
                var order = await ctx.OrderData
                    .Include("ContactRoles").Include("ContactRoles.Contact")
                    .FirstOrDefaultAsync(e => e.BID == testBID && e.ID == testOrder.ID);
                Assert.IsNotNull(order);

                #region All Contacts

                var empData = order.ContactRoles.Select(r => r.Contact);
                Assert.IsNotNull(empData);

                var propertyResultIsOrderData =
                    AELTestHelper.GetPropertyResult<OrderData, OrderData>(testBID, DataType.OrderCategory,
                        "Contacts", order);
                Assert.IsNotNull(propertyResultIsOrderData);

                var primaryOrderContactPropertyResult =
                    AELTestHelper.GetPropertyResult<ICollection<ContactData>, OrderData>(testBID, DataType.TransactionHeaderContactRoleCategory,
                        "Primary", order);
                Assert.IsNotNull(primaryOrderContactPropertyResult.FirstOrDefault());

                var shippingOrderContactPropertyResult =
                    AELTestHelper.GetPropertyResult<ICollection<ContactData>, OrderData>(testBID, DataType.TransactionHeaderContactRoleCategory,
                        "Shipping", order);
                Assert.IsNull(shippingOrderContactPropertyResult.FirstOrDefault());

                #endregion All Contacts
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                #region Cleanup Test Data
                ctx = AELTestHelper.GetMockCtx(testBID);
                ctx.OrderContactRole.Remove(contactRole1);
                ctx.ContactData.Remove(testCont1);
                ctx.OrderData.Remove(testOrder);
                ctx.TaxGroup.Remove(taxGroup);
                ctx.CompanyData.Remove(testComp);

                ctx.SaveChanges();

                #endregion
            }
        }

        private OrderData GetTestOrder(short bid = 1, int id = -99, string timestamp = "", int companyID = -99)
        {
            return new OrderData()
            {
                BID = bid,
                ID = id,
                Description = $"Test.Order.{timestamp}",
                CompanyID = companyID,
                LocationID = 1,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                Number = 9999,
                FormattedNumber = "ORD-9999",
                PickupLocationID = 1,
                ProductionLocationID = 1,
                TaxGroupID = 1
            };
        }

        private CompanyData GetTestCompany(short bid = 1, int id = -99, string timestamp = "", int? teamId = null)
        {
            return new CompanyData()
            {
                BID = bid,
                ID = id,
                Name = $"CompanyData.{timestamp}",
                TeamID = teamId,
                LocationID = 1,
                StatusID = 1
            };
        }
    }
}
