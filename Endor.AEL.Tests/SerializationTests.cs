﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Endor.AEL.Tests.Helper;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class SerializationTests
    {
        [TestMethod]
        public void SerializeDataTypeInfo()
        {
            var dti = new DataTypeInfo
            {
                DataType = DataType.String,
                Text = "string",
                PluralText = "strings",
                SystemType = typeof(String)
            };
            this.TestSerialization(dti, "SystemType");
        }

        [TestMethod]
        public void SerializeOperatorInfo()
        {
            var oi = new OperatorInfo
            {
                ASCIICode = 'a',
                DataType = DataType.Boolean,
                OperatorType = OperatorType.EQ,
                OperandTypes = new List<OperandType>(),
                Symbol = "aaa",
                Text = "aaa"
            };
            this.TestSerialization(oi, "OperandTypes");
        }

        [TestMethod]
        public void ConvertJsonToOperationGroup()
        {
            var reader = new StreamReader("..\\..\\..\\operation-group-data.json");
            string json = reader.ReadToEnd();

            var aelGroup = AELOperation.Deserialize(json);
            Assert.AreEqual(typeof(AELOperation), aelGroup.GetType());
            Assert.AreEqual(DataType.Boolean, aelGroup.DataType);
            Assert.AreEqual(OperatorType.OR, aelGroup.Operator);
            Assert.AreEqual(DataType.Boolean, aelGroup.LhsDataType);
            Assert.AreEqual(DataType.Boolean, aelGroup.RhsDataType);
            Assert.IsTrue(aelGroup.Terms.Count() != 0);

            string newJson = aelGroup.Serialize();

            var jsonObj = JObject.Parse(json);
            var newJsonObj = JObject.Parse(newJson);

            Assert.IsTrue(JToken.DeepEquals(jsonObj, newJsonObj));
        }

        public void TestSerialization<T>(T objectToSerialize, params string[] excludedProperties)
        {
            string jsonString = JsonConvert.SerializeObject(objectToSerialize);
            Console.WriteLine(jsonString);
            foreach (var prop in typeof(T).GetProperties())
            {
                if (excludedProperties.Contains(prop.Name))
                {
                    Assert.IsFalse(jsonString.Contains($"\"{prop.Name}\":"), $"\"{prop.Name}\" should not be serialized");
                }
                else
                {
                    Assert.IsTrue(jsonString.Contains($"\"{prop.Name}\":"), $"\"{prop.Name}\" should be serialized");
                }
            }
        }

        [TestMethod]
        public void OrderCreated_BoardTest()
        {
            const string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3,
    ""Operator"": 8,
    ""LhsDataType"": 3,
    ""RhsDataType"": 3,
    ""Terms"": [
    {
        ""$type"": ""AELOperation"",
        ""DataType"": 3,
        ""Operator"": 4,
        ""LhsDataType"": 1,
        ""RhsDataType"": -1,
        ""Terms"": [
        {
            ""$type"": ""AELMember"",
            ""DataType"": 9,
            ""Text"": ""Created Date"",
            ""ID"": 21261,
            ""MemberBase"": {
                ""$type"": ""AELMember"",
                ""DataType"": 20230,
                ""Text"": ""Order"",
                ""ID"": 10100,
                ""MemberBase"": null,
                ""RelatedID"": 0
            },
            ""RelatedID"": 0
        },
        {
            ""$type"": ""AELValues"",
            ""DataType"": 9,
            ""Values"": [""1-1-2019""],
            ""DisplayText"": ""1-1-2019""
        }
        ]
    }
    ]
}
";

            var ctx = AELTestHelper.GetMockCtx(1);

            var location = ctx.LocationData.OrderBy(x => x.ID).First(x => x.BID == 1);
            var company = ctx.CompanyData.OrderBy(x => x.ID).First(x => x.BID == 1);
            var origin = ctx.CrmOrigin.OrderBy(x => x.ID).First(x => x.BID == 1);
            var taxGroup = ctx.TaxGroup.OrderBy(x => x.ID).First(x => x.BID == 1);

            var order = new OrderData
            {
                ID = -999,
                BID = 1,
                Number = -999,
                Company = company,
                Location = location,
                PickupLocation = location,
                ProductionLocation = location,
                Description = "My test order",
                //TaxExemptReasonID = -99,
                Origin = origin,
                OrderPONumber = "PO Number",
                FormattedNumber = "",
                CostTotal = 1.23m,
                TaxGroup = taxGroup,
                PriceProductTotal = 0.00m,
                PriceDestinationTotal = 0.00m,
                PriceFinanceCharge = 0.00m,
                PriceNet = 1.23m,
                PriceDiscount = 0.00m,
                PriceDiscountPercent = 0.00m,
                PriceTaxRate = 1.0m,
                IsTaxExempt = false,
                PricePreTax = 1.23m,
                PriceTax = 0.00m,
                PriceTotal = 4.56m,
                PaymentTotal = 0.00m,
                PaymentBalanceDue = 0.00m,
                PaymentWriteOff = 0.00m,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                Dates = new List<OrderKeyDate>
                {
                    new OrderKeyDate
                    {
                        IsDestinationDate = false,
                        IsOrderItemDate = false,
                        IsFirmDate = false,
                        KeyDateType = OrderKeyDateType.Created,
                        KeyDT =  DateTime.UtcNow,
                    },
                },
            };

            ctx.OrderData.Add(order);
            ctx.SaveChanges();

            try
            {
                var withCondition = ctx.OrderData
                .Where(AELTestHelper.DataProvider(1), boardConditionFx)
                ;
                
                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == -999));
            }
            finally
            {
                ctx.OrderKeyDate.RemoveRange(order.Dates);
                ctx.OrderData.Remove(order);
                ctx.SaveChanges();
            }
        }
    }
}
