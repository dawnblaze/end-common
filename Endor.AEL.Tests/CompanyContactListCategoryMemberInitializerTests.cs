﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class CompanyContactListCategoryMemberInitializerTests
    {
        [TestMethod]
        public void TestCompanyContactCategorySimpleProperties()
        {
            short testBID = 1;

            var company = new CompanyData()
            {
                BID = testBID,
                ID = -99,
                Name = "TESTCo"
            };
            var contact1 = new ContactData()
            {
                BID = testBID,
                ID = -99,
                First = "TESTFirst",
                Last = "TESTLast",
                //IsDefault = true,
                LocationID = 1,
                //CompanyID = testCompany.ID
            };
            var contact2 = new ContactData()
            {
                BID = testBID,
                ID = -98,
                First = "TESTFirst",
                Last = "TESTLast",
                LocationID = 1,
                //CompanyID = testCompany.ID
            };

            company.CompanyContactLinks = new List<CompanyContactLink>()
            {
                new CompanyContactLink()
                {
                    BID = testBID,
                    Contact = contact1,
                    ContactID = contact1.ID,
                    CompanyID = company.ID,
                    IsPrimary = true
                },
                new CompanyContactLink()
                {
                    BID = testBID,
                    Contact = contact2,
                    ContactID = contact2.ID,
                    CompanyID = company.ID,
                    IsBilling = true
                },
            };

            var primaryContact = AELTestHelper.GetPropertyResult<ContactData, CompanyData>(testBID, DataType.CompanyContactListCategory, "Primary", company);
            Assert.AreEqual(contact1, primaryContact);

            var billingContact = AELTestHelper.GetPropertyResult<ContactData, CompanyData>(testBID, DataType.CompanyContactListCategory, "Billing", company);
            Assert.AreEqual(contact2, billingContact);

            var anyContacts = AELTestHelper.GetPropertyResult<ICollection<ContactData>, CompanyData>(testBID, DataType.CompanyContactListCategory, "Any", company);
            Assert.AreEqual(anyContacts.Count, 2);
        }

        [TestMethod]
        public void TestCompanyContactCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.CompanyContactListCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.ContactCategory));
            
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Primary"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Billing"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Any"));

        }
    }
}
