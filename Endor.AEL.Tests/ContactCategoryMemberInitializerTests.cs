﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Endor.EF;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Linq.Expressions;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class ContactCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task TestContactCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var testContact = new ContactData()
            {
                BID = testBID,
                ID = -99,
                First = "TESTFirst",
                Last = "TESTLast",
                LocationID = (await ctx.LocationData.FirstOrDefaultAsync(x => x.BID == testBID)).ID,
                //CompanyID = (await ctx.CompanyData.FirstOrDefaultAsync(x => x.BID == testBID)).ID end-4814
            };
            var phoneLocator = new ContactLocator()
            {
                BID = testBID,
                ID = -99,
                ParentID = testContact.ID,
                RawInput = "5554443210",
                LocatorType = (byte)LocatorType.Phone,
                SortIndex = -1,
                ModifiedDT = DateTime.UtcNow
            };
            var emailLocator = new ContactLocator()
            {
                BID = testBID,
                ID = -98,
                ParentID = testContact.ID,
                RawInput = "none@none.com",
                LocatorType = (byte)LocatorType.Email,
                SortIndex = -1,
                ModifiedDT = DateTime.UtcNow
            };

            try
            {
                ctx.ContactLocator.RemoveRange(ctx.ContactLocator.Where(oi => oi.ID == -99));
                ctx.ContactLocator.RemoveRange(ctx.ContactLocator.Where(oi => oi.ID == -98));
                ctx.ContactData.RemoveRange(ctx.ContactData.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();

                ctx.ContactData.Add(testContact);
                ctx.ContactLocator.Add(phoneLocator);
                ctx.ContactLocator.Add(emailLocator);
                Assert.AreEqual(3, await ctx.SaveChangesAsync());

                var contact = await ctx.ContactData
                    .Include("ContactLocators")
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testContact.ID);

                Assert.IsNotNull(contact);
                Assert.IsNotNull(contact.ContactLocators);
                Assert.AreEqual(2, contact.ContactLocators.Count);

                var contactID = AELTestHelper.GetPropertyResult<decimal?, ContactData>(testBID, DataType.ContactCategory, "Name", contact);
                Assert.AreEqual(contact.ID, contactID);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, ContactData>(testBID, DataType.ContactCategory, "Contact Name", null));
                var shortNameTest = AELTestHelper.GetPropertyResult<string, ContactData>(testBID, DataType.ContactCategory, "Contact Name", contact);
                Assert.AreEqual(contact.ShortName, shortNameTest);
                
                Assert.IsNull(AELTestHelper.GetPropertyResult<string, ContactData>(testBID, DataType.ContactCategory, "First Name", null));
                var firstNameTest = AELTestHelper.GetPropertyResult<string, ContactData>(testBID, DataType.ContactCategory, "First Name", contact);
                Assert.AreEqual(contact.First, firstNameTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, ContactData>(testBID, DataType.ContactCategory, "Last Name", null));
                var lastNameTest = AELTestHelper.GetPropertyResult<string, ContactData>(testBID, DataType.ContactCategory, "Last Name", contact);
                Assert.AreEqual(contact.Last, lastNameTest);

                Assert.IsNull(AELTestHelper.GetPropertyResult<List<string>, ContactData>(testBID, DataType.ContactCategory, "Email", null));
                var emailTest = AELTestHelper.GetPropertyResult<List<string>, ContactData>(testBID, DataType.ContactCategory, "Email", contact);
                Assert.AreEqual(emailLocator.RawInput, emailTest.FirstOrDefault());

                Assert.IsNull(AELTestHelper.GetPropertyResult<ICollection<string>, ContactData>(testBID, DataType.ContactCategory, "Phone Number", null));
                var phoneTest = AELTestHelper.GetPropertyResult<ICollection<string>, ContactData>(testBID, DataType.ContactCategory, "Phone Number", contact);
                Assert.AreEqual(phoneLocator.RawInput, phoneTest.FirstOrDefault());

                
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx.ContactLocator.Remove(emailLocator);
                ctx.ContactLocator.Remove(phoneLocator);
                ctx.ContactData.Remove(testContact);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestContactCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.ContactCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(7, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Contact));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.StringList));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Contact Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "First Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Last Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Email"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Phone Number"));

        }
    }
}
