﻿using Endor.EF;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Endor.AEL.Tests.Helper
{
    public static class AELTestHelper
    {
        public static readonly LoggerFactory MyLoggerFactory = new LoggerFactory(new[] { new DebugLoggerProvider() });
        public class MockTenantDataCache : ITenantDataCache
        {
            public static Dictionary<string, string> GetLocalSettings()
            {
                /*
                    Example test-config.json
                    {
                        "BusinessDBConnectionString ="Data Source=.\\SQLEXPRESS;Initial Catalog=\"Dev.Endor.Business.DB1\";User ID=cyrious;Password=watankahani"
                    } 
                */
                string file = "..\\..\\..\\..\\test-config.json"; //put this inside end-common folder
                if (File.Exists(file))
                {
                    string text = File.ReadAllText(file);
                    var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);
                    return result;
                }
                else
                {
                    return null;
                }
            }

            private static readonly TenantData Mock = new TenantData()
            {
                APIURL = "https://endorapi.localcyriousdevelopment.com:5002/",
                ReportingURL = "",
                MessagingURL = "https://endormessaging.localcyriousdevelopment.com:5004/",
                LoggingURL = "https://endorlog.localcyriousdevelopment.com:5003",
                BackgroundEngineURL = "https://endorbgengine.localcyriousdevelopment.com:5006/",
                StorageURL = "http://127.0.0.1:10000/devstoreaccount1/",
                StorageConnectionString = "UseDevelopmentStorage=true",
                IndexStorageConnectionString = "UseDevelopmentStorage=true",
                BusinessDBConnectionString = GetLocalSettings() != null ? GetLocalSettings()["BusinessDBConnectionString"] : "Data Source=.\\SQLEXPRESS;Initial Catalog=\"Dev.Endor.Business.DB1\";User ID=cyrious;Password=watankahani",
                LoggingDBConnectionString = "",
                MessagingDBConnectionString = "",
                SystemReportBConnectionString = "",
                SystemDataDBConnectionString = "",
            };

            public Task<TenantData> Get(short bid)
            {
                if (bid == 1)
                    return Task.FromResult(Mock);
                else
                    return null;
            }

            public void InvalidateCache()
            {
            }

            public void InvalidateCache(short bid)
            {
            }
        }
        public class TestApiContext : ApiContext
        {
            private readonly ILoggerFactory _loggerFactory;

            public TestApiContext(ILoggerFactory loggerFactory, DbContextOptions<ApiContext> options, ITenantDataCache iTenantCache, short bidScope) : base(options, iTenantCache, bidScope)
            {
                _loggerFactory = loggerFactory;
            }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                base.OnConfiguring(optionsBuilder);
                optionsBuilder.UseLoggerFactory(_loggerFactory);
            }
        }
        internal static ITaskQueuer GetMockTaskQueuer()
        {
            return new MockTaskQueuer(new MockTenantDataCache());
        }

        public static ApiContext GetMockCtx(short bid)
        {
            
            var opts = new DbContextOptions<ApiContext>()
            {
                
            };

            TestApiContext ctx = new TestApiContext(MyLoggerFactory, opts, new MockTenantDataCache(), bid);
            new MigrationHelper(GetMockTaskQueuer()).MigrateDb(ctx);

            return ctx;
        }

        public static bool DoGetOperationResult<LeftT, RightT>(Func<Expression, Expression, bool, Expression> expression, LeftT left, RightT right)
        {
            Expression expLeft = Expression.Constant(left, typeof(LeftT));
            Expression expRight = Expression.Constant(right, typeof(RightT));

            var exp = Expression.Lambda<Func<bool?>>(expression(expLeft, expRight, false));
            var func = exp.Compile();

            bool? result = func();
            return result.GetValueOrDefault(false);
        }

        public static OutputT GetPropertyResult<OutputT, InstanceT>(short bid, DataType dataType, string propertyName, InstanceT instance)
        {
            Expression expInstance = Expression.Constant(instance, typeof(InstanceT));

            PropertyInfo property = (PropertyInfo)dataType.FindMember(DataProvider(bid), propertyName);

            if (property == null)
                throw new Exception($"Property called \"{propertyName}\" not found for {dataType}.");
            try
            {
                var exp = Expression.Lambda<Func<OutputT>>(property.Expression(expInstance, false));
                var func = exp.Compile();

                return func();
            }
            catch (Exception e)
            {
                throw new Exception($"Exception throw in property call \"{propertyName}\" for {dataType}.", e);
            }
        }


        private static OutputT DoGetMethodResult<OutputT, InstanceT>(short bid, DataType dataType, string methodName, object instance, List<Expression> parameters = null)
        {
            Expression expInstance = Expression.Constant(instance, typeof(InstanceT));
            MethodInfo method = (MethodInfo)dataType.FindMember(DataProvider(bid), methodName);

            if (method == null)
                throw new Exception($"Method called \"{methodName}\" not found for {dataType}.");

            try
            {
                var exp = Expression.Lambda<Func<OutputT>>(method.Expression(expInstance, parameters, false));
                var func = exp.Compile();

                return func();
            }
            catch (Exception e)
            {
                throw new Exception($"Exception throw in Method call \"{methodName}\" for {dataType}.", e);
            }
        }

        public static OutputT GetMethodResult<OutputT, InstanceT>(short bid, DataType dataType, string methodName, object instance)
        {
            return DoGetMethodResult<OutputT, InstanceT>(bid, dataType, methodName, instance);
        }

        public static OutputT GetMethodResult<OutputT, InstanceT, Arg1T>(short bid, DataType dataType, string methodName, object instance, Arg1T arg1)
        {
            List<Expression> expParameters = null;

            expParameters = new List<Expression>
            {
                Expression.Constant(arg1, typeof(Arg1T))
            };

            return DoGetMethodResult<OutputT, InstanceT>(bid, dataType, methodName, instance, expParameters);
        }

        public static IDataProvider DataProvider(short bid)
        {
            return new DataProvider(bid, GetMockCtx(bid));
        }

        public static MemberDictionaryByDataType GetDictionaryInstance(short bid)
        {
            return BusinessMemberDictionary.MemberDictionaryByDataType(DataProvider(bid));
        }
    }
}
