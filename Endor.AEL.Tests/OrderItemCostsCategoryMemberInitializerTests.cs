﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemCostsCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task TestOrderItemCostsCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var testOrderItem = new OrderItemData()
            {
                BID = testBID,
                ID = -99,
                ItemNumber = 1,
                Description = "TestDescription",
                Quantity = 2,
                Name = "TestName",
                HasDocuments = true,
                HasProof = true,
                HasCustomImage = true,
                OrderID = ctx.OrderData.FirstOrDefault().ID,
                TransactionType = 2,
                OrderStatusID = OrderOrderStatus.OrderBuilt,
                ItemStatusID = ctx.OrderItemStatus.FirstOrDefault().ID,
                ProductionLocationID = ctx.LocationData.FirstOrDefault().ID,
                TaxGroupID = ctx.TaxGroup.FirstOrDefault().ID,
                IsTaxExempt = false,
                TaxGroupOV = false,
                CostLabor = 1,
                CostMachine = 2,
                CostMaterial = 3,
                CostOther = 4
            };

            try
            {
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderItemData.Add(testOrderItem);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                var orderItemTag = await ctx.OrderItemData
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testOrderItem.ID);

                Assert.IsNotNull(orderItemTag);

                var totalCost = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemCostsCategory, "Total Cost", testOrderItem);
                Assert.AreEqual(totalCost, 10);

                var materialCost = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemCostsCategory, "Material Cost", testOrderItem);
                Assert.AreEqual(materialCost, 3);

                var laborCost = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemCostsCategory, "Labor Cost", testOrderItem);
                Assert.AreEqual(laborCost, 1);

                var machineCost = AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemCostsCategory, "Machine Cost", testOrderItem);
                Assert.AreEqual(machineCost, 2);


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemCostsCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.OrderItemCostsCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(4, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Number));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Total Cost"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Material Cost"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Labor Cost"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Machine Cost"));

        }
    }
}
