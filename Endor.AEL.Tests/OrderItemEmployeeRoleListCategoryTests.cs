﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemEmployeeRoleListCategoryTests
    {
        [TestMethod]
        public async Task TestOrderItemEmployeeRoleListCategorySimpleProperties()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);
            var lineItem = ctx.OrderItemData.Include(i => i.Order).FirstOrDefault();
            var employees = ctx.EmployeeData.Take(2);
            var employeeRole1 = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -99,
                EmployeeID = employees.FirstOrDefault().ID,
                RoleID = SystemIDs.EmployeeRole.AssignedTo,
                OrderID = lineItem.Order.ID,
                OrderItemID = lineItem.ID
            };
            var employeeRole2 = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -98,
                EmployeeID = employees.OrderBy(x => x.ID).First().ID,
                RoleID = SystemIDs.EmployeeRole.EnteredBy,
                OrderID = lineItem.Order.ID,
                OrderItemID = lineItem.ID
            };
            try
            {
                ctx.RemoveRange(ctx.OrderEmployeeRole.Where(oi => oi.ID == -99 || oi.ID == -98));
                await ctx.SaveChangesAsync();

                ctx.OrderEmployeeRole.Add(employeeRole1);
                ctx.OrderEmployeeRole.Add(employeeRole2);
                Assert.AreEqual(2, await ctx.SaveChangesAsync());

                var RolesTest = AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.OrderItemEmployeeRoleListCategory, "Roles", lineItem);
                Assert.AreEqual(RolesTest.ID, lineItem.ID);

                var assignedToTest = AELTestHelper.GetPropertyResult<List<EmployeeData>, OrderItemData>(testBID, DataType.OrderItemEmployeeRoleListCategory, "Assigned To", lineItem);
                Assert.AreEqual(1, assignedToTest.Count);
                Assert.AreEqual(employeeRole1.EmployeeID, assignedToTest[0].ID);

                var enteredByTest = AELTestHelper.GetPropertyResult<List<EmployeeData>, OrderItemData>(testBID, DataType.OrderItemEmployeeRoleListCategory, "Entered By", lineItem);
                Assert.AreEqual(1, enteredByTest.Count);
                Assert.AreEqual(employeeRole2.EmployeeID, enteredByTest[0].ID);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99 || oi.ID == -98));
                await ctx.SaveChangesAsync();
            }

        }

        [TestMethod]
        public void TestOrderItemEmployeeRoleListCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.OrderItemEmployeeRoleListCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.OrderItemEmployeeRoleList));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.EmployeeCategory));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Roles"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Assigned To"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Entered By"));

        }
    }
}
