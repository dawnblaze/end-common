﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.Common.Tests;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class TransactionHeaderCompanyMemberInitializerTests
    {
        private readonly string[] includes = { "CompanyContactLinks", "CompanyContactLinks.Contact", "Location", "EmployeeTeam", "EmployeeTeam.EmployeeTeamLinks", "EmployeeTeam.EmployeeTeamLinks.Employee", "EmployeeTeam.EmployeeTeamLinks.Role", "Location", "CrmOrigin", "CrmIndustry", "TimeZone", "TaxExemptReason", "TaxGroup", "ParentCompany", "ChildCompanies" };
        const short testBID = 1;
        [TestMethod]
        public async Task TestTransactionHeaderCompanySimpleProperties()
        {
            var ctx = AELTestHelper.GetMockCtx(1);

            var testDateStr = DateTime.UtcNow.ToString();
            var testCompany = new CompanyData()
            {
                BID = testBID,
                ID = -99,
                Name = $"CompanyData.{testDateStr}",
                LocationID = 1,
                StatusID = 1,
                IsVendor = false,
                IsProspect = false,
                IsClient = true,
                IsPORequired = false,
                DefaultPONumber = "blah blah blah",
                CreditLimit = 100,
                NonRefundableCredit = 50,
                RefundableCredit = 25,
            };
            var testCompanyChild = new CompanyData()
            {
                BID = testBID,
                ID = -98,
                Name = $"CompanyData.{testDateStr}",
                LocationID = 1,
                StatusID = 1,
                IsVendor = false,
                IsProspect = false,
                IsClient = true,
                IsPORequired = false,
                ParentID = -99,
                DefaultPONumber = "blah blah blah",
                CreditLimit = 100,
                NonRefundableCredit = 50,
                RefundableCredit = 25,
            };

            try
            {
                var orderToDelete = ctx.OrderData.Where(oi => oi.CompanyID == -99);
                var orderItemsToDelete = ctx.OrderItemData.Where(oi => orderToDelete.Select(o => o.ID).Contains(oi.OrderID));
                var orderItemComponentsToDelete = ctx.OrderItemComponent.Where(oi => orderItemsToDelete.Select(o => o.ID).Contains(oi.OrderItemID));
                ctx.OrderItemComponent.RemoveRange(orderItemComponentsToDelete);
                await ctx.SaveChangesAsync();

                ctx.OrderItemData.RemoveRange(orderItemsToDelete);
                await ctx.SaveChangesAsync();

                ctx.OrderEmployeeRole.RemoveRange(ctx.OrderEmployeeRole.Where(oi => orderToDelete.Select(o => o.ID).Contains(oi.OrderID)));
                ctx.OrderKeyDate.RemoveRange(ctx.OrderKeyDate.Where(oi => orderToDelete.Select(o => o.ID).Contains(oi.OrderID)));
                await ctx.SaveChangesAsync();

                ctx.OrderData.RemoveRange(orderToDelete);
                await ctx.SaveChangesAsync();

                ctx.EmployeeTeamLink.RemoveRange(ctx.EmployeeTeamLink.Where(oi => oi.EmployeeID == -99));
                ctx.EmployeeData.RemoveRange(ctx.EmployeeData.Where(oi => oi.ID == -99));
                ctx.ContactData.RemoveRange(ctx.ContactData.Where(oi => oi.ID == -99));
                ctx.CompanyData.RemoveRange(ctx.CompanyData.Where(oi => oi.ID == -98));
                ctx.CompanyData.RemoveRange(ctx.CompanyData.Where(oi => oi.ID == -99));
                ctx.EmployeeTeam.RemoveRange(ctx.EmployeeTeam.Where(oi => oi.ID == -99));
                ctx.TaxGroup.RemoveRange(ctx.TaxGroup.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();

                ctx.CompanyData.Add(testCompany);
                ctx.CompanyData.Add(testCompanyChild);
                Assert.AreEqual(2, await ctx.SaveChangesAsync());

                CompanyData nullCompany = null;
                var company = await ctx.CompanyData.IncludeAll(includes)
                    .FirstOrDefaultAsync(c => c.BID == 1 && c.ID == testCompany.ID);
                var child = await ctx.CompanyData.IncludeAll(includes)
                    .FirstOrDefaultAsync(c => c.BID == 1 && c.ID == testCompanyChild.ID);

                // Origin
                Assert.IsNull(AELTestHelper.GetPropertyResult<CrmOrigin, CompanyData>(testBID, DataType.CompanyCategory, "Origin", nullCompany));
                var origin = AELTestHelper.GetPropertyResult<CrmOrigin, CompanyData>(testBID, DataType.CompanyCategory, "Origin", company);
                Assert.AreEqual(company.CrmOrigin, origin);

                // Industry
                Assert.IsNull(AELTestHelper.GetPropertyResult<CrmIndustry, CompanyData>(testBID, DataType.CompanyCategory, "Industry", nullCompany));
                var industry = AELTestHelper.GetPropertyResult<CrmIndustry, CompanyData>(testBID, DataType.CompanyCategory, "Industry", company);
                Assert.AreEqual(company.CrmIndustry, industry);

                // Name
                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, CompanyData>(testBID, DataType.CompanyCategory, "Name", nullCompany));
                var compObj = AELTestHelper.GetPropertyResult<decimal?, CompanyData>(testBID, DataType.CompanyCategory, "Name", company);
                Assert.AreEqual(company.ID, compObj);

                // Company Name
                Assert.IsNull(AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.CompanyCategory, "Company Name", nullCompany));
                var companyName = AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.CompanyCategory, "Company Name", company);
                Assert.AreEqual(company.Name, companyName);

                // Subsidiaries
                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Subsidiaries", nullCompany));
                var subsidiary = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Subsidiaries", company);
                Assert.IsNotNull(subsidiary);
            }
            catch (Exception ex)
            {
                ctx.CompanyData.Remove(testCompanyChild);
                ctx.CompanyData.Remove(testCompany);
                await ctx.SaveChangesAsync();

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx.CompanyData.Remove(testCompanyChild);
                ctx.CompanyData.Remove(testCompany);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestTransactionHeaderCompanyMemberInitializer()
        {
            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.CompanyCategory);
            Assert.IsNotNull(memberInfo);
            Assert.IsTrue(memberInfo.Members.Count > 0);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.String));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CompanyCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Origin));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Industry));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.SubsidiariesCategory));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Origin"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Industry"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Company Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Subsidiaries"));
        }
    }
}
