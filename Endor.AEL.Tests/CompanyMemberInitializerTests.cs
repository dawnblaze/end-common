﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.Common.Tests;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class CompanyMemberInitializerTests
    {
        private readonly string[] includes = { "CompanyContactLinks", "CompanyContactLinks.Contact", "Location", "EmployeeTeam", "EmployeeTeam.EmployeeTeamLinks", "EmployeeTeam.EmployeeTeamLinks.Employee", "EmployeeTeam.EmployeeTeamLinks.Role", "Location", "CrmOrigin", "CrmIndustry", "TimeZone", "TaxExemptReason", "TaxGroup", "ParentCompany", "ChildCompanies", "Payments", "Orders", "Orders.Dates" };
        const short testBID = 1;

        [TestMethod]
        public async Task TestCompanySimpleProperties()
        {
            var ctx = AELTestHelper.GetMockCtx(1);

            var testDateStr = DateTime.UtcNow.ToString();
            var testTeam = GetTestTeam(timestamp: testDateStr);
            var testTaxGroup = GetTestTaxGroup(timestamp: testDateStr);
            var testComp = GetTestCompany(timestamp: testDateStr);
            var testCompChild = GetTestCompany(id: -98, timestamp: $"{testDateStr}.1", parentCompanyId: -99);
            var testContact = GetTestContact(timestamp: testDateStr);
            var testCompanyContactLink = new CompanyContactLink()
            {
                BID = 1,
                CompanyID = testComp.ID,
                ContactID = testContact.ID,
                Contact = testContact,
                Roles = (ContactRole.Primary | ContactRole.Billing)
            };
            var testEmp = GetTestEmployee(timestamp: testDateStr);
            var testTeamLink = GetTestTeamLink();
           
            try
            {
                var orderToDelete = ctx.OrderData.Where(oi => oi.CompanyID == -99);
                var orderItemsToDelete = ctx.OrderItemData.Where(oi => orderToDelete.Select(o => o.ID).Contains(oi.OrderID));
                var orderItemComponentsToDelete = ctx.OrderItemComponent.Where(oi => orderItemsToDelete.Select(o => o.ID).Contains(oi.OrderItemID));
                ctx.OrderItemComponent.RemoveRange(orderItemComponentsToDelete);
                await ctx.SaveChangesAsync();

                ctx.OrderItemData.RemoveRange(orderItemsToDelete);
                await ctx.SaveChangesAsync();

                ctx.OrderEmployeeRole.RemoveRange(ctx.OrderEmployeeRole.Where(oi => orderToDelete.Select(o => o.ID).Contains(oi.OrderID)));
                ctx.OrderKeyDate.RemoveRange(ctx.OrderKeyDate.Where(oi => orderToDelete.Select(o => o.ID).Contains(oi.OrderID)));
                await ctx.SaveChangesAsync();

                ctx.OrderData.RemoveRange(orderToDelete);
                await ctx.SaveChangesAsync();

                ctx.EmployeeTeamLink.RemoveRange(ctx.EmployeeTeamLink.Where(oi => oi.EmployeeID == -99));
                ctx.EmployeeData.RemoveRange(ctx.EmployeeData.Where(oi => oi.ID == -99));
                ctx.CompanyContactLink.RemoveRange(ctx.CompanyContactLink.Where(l => l.ContactID == -99 || l.CompanyID == -99));
                ctx.ContactData.RemoveRange(ctx.ContactData.Where(oi => oi.ID == -99));
                ctx.CompanyData.RemoveRange(ctx.CompanyData.Where(oi => oi.ID == -98));
                ctx.CompanyData.RemoveRange(ctx.CompanyData.Where(oi => oi.ID == -99));
                ctx.EmployeeTeam.RemoveRange(ctx.EmployeeTeam.Where(oi => oi.ID == -99));
                ctx.TaxGroup.RemoveRange(ctx.TaxGroup.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();

                ctx.TaxGroup.Add(testTaxGroup);
                ctx.EmployeeTeam.Add(testTeam);
                ctx.CompanyData.Add(testComp);
                ctx.CompanyData.Add(testCompChild);
                ctx.ContactData.Add(testContact);
                ctx.CompanyContactLink.Add(testCompanyContactLink);
                ctx.EmployeeData.Add(testEmp);
                ctx.EmployeeTeamLink.Add(testTeamLink);
                Assert.AreEqual(8, await ctx.SaveChangesAsync());

                CompanyData nullCompany = null;
                var company = await ctx.CompanyData.IncludeAll(includes)
                    .FirstOrDefaultAsync(c => c.BID == 1 && c.ID == testComp.ID);
                var child = await ctx.CompanyData.IncludeAll(includes)
                    .FirstOrDefaultAsync(c => c.BID == 1 && c.ID == testCompChild.ID);

                // Contacts
                Assert.IsNull(AELTestHelper.GetPropertyResult<ICollection<ContactData>, CompanyData>(testBID, DataType.CompanyContactListCategory, "Any", nullCompany));
                var contacts = AELTestHelper.GetPropertyResult<ICollection<ContactData>, CompanyData>(testBID, DataType.CompanyContactListCategory, "Any", company);

                Assert.IsNull(AELTestHelper.GetPropertyResult<ContactData, CompanyData>(testBID, DataType.CompanyContactListCategory, "Primary", null));
                var primaryContact = AELTestHelper.GetPropertyResult<ContactData, CompanyData>(testBID, DataType.CompanyContactListCategory, "Primary", company);
                Assert.IsNotNull(primaryContact);
                Assert.AreEqual(company.CompanyContactLinks.FirstOrDefault(c => c.IsPrimary == true).Contact, primaryContact);

                Assert.IsNull(AELTestHelper.GetPropertyResult<ContactData, CompanyData>(testBID, DataType.CompanyContactListCategory, "Billing", null));
                var billingContact = AELTestHelper.GetPropertyResult<ContactData, CompanyData>(testBID, DataType.CompanyContactListCategory, "Billing", company);
                Assert.IsNotNull(billingContact);
                Assert.AreEqual(company.CompanyContactLinks.FirstOrDefault(c => c.IsBilling == true).Contact, billingContact);

                // Employees
                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Employees", nullCompany));
                var empTeamLinks = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Employees", company);
                Assert.IsNotNull(empTeamLinks);

                // Time Zone
                Assert.IsNull(AELTestHelper.GetPropertyResult<EnumTimeZone, CompanyData>(testBID, DataType.CompanyCategory, "Time Zone", nullCompany));
                var timeZone = AELTestHelper.GetPropertyResult<EnumTimeZone, CompanyData>(testBID, DataType.CompanyCategory, "Time Zone", company);
                Assert.AreEqual(company.TimeZone, timeZone);

                // Location
                Assert.IsNull(AELTestHelper.GetPropertyResult<LocationData, CompanyData>(testBID, DataType.CompanyCategory, "Default Location", nullCompany));
                var location = AELTestHelper.GetPropertyResult<LocationData, CompanyData>(testBID, DataType.CompanyCategory, "Default Location", company);
                Assert.AreEqual(company.Location, location);

                // Origin
                Assert.IsNull(AELTestHelper.GetPropertyResult<CrmOrigin, CompanyData>(testBID, DataType.CompanyCategory, "Origin", nullCompany));
                var origin = AELTestHelper.GetPropertyResult<CrmOrigin, CompanyData>(testBID, DataType.CompanyCategory, "Origin", company);
                Assert.AreEqual(company.CrmOrigin, origin);

                // Industry
                Assert.IsNull(AELTestHelper.GetPropertyResult<CrmIndustry, CompanyData>(testBID, DataType.CompanyCategory, "Industry", nullCompany));
                var industry = AELTestHelper.GetPropertyResult<CrmIndustry, CompanyData>(testBID, DataType.CompanyCategory, "Industry", company);
                Assert.AreEqual(company.CrmIndustry, industry);

                // Name
                Assert.IsNull(AELTestHelper.GetPropertyResult<Decimal?, CompanyData>(testBID, DataType.CompanyCategory, "Name", nullCompany));
                var name = AELTestHelper.GetPropertyResult<Decimal?, CompanyData>(testBID, DataType.CompanyCategory, "Name", company);
                Assert.AreEqual(company.ID, name);

                // Company Name
                Assert.IsNull(AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.CompanyCategory, "Company Name", nullCompany));
                var companyName = AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.CompanyCategory, "Company Name", company);
                Assert.AreEqual(company.Name, companyName);

                // Type
                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Type", nullCompany));
                var type = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Type", company);
                Assert.IsNotNull(type);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTypeCategory, "Is Prospect", null));
                var isProspect = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTypeCategory, "Is Prospect", type);
                Assert.IsNotNull(isProspect);
                Assert.AreEqual(company.IsProspect, isProspect.Value);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTypeCategory, "Is Customer", null));
                var isCustomer = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTypeCategory, "Is Customer", type);
                Assert.AreEqual(company.Orders?.Any(), isCustomer);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTypeCategory, "Is Vendor", null));
                var isVendor = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTypeCategory, "Is Vendor", type);
                Assert.IsNotNull(isVendor);
                Assert.AreEqual(company.IsVendor.Value, isVendor.Value);

                // Has Credit
                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyCategory, "Has Credit Account", nullCompany));
                var hasCredit = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyCategory, "Has Credit Account", company);
                Assert.IsNotNull(hasCredit);
                Assert.AreEqual(company.HasCreditAccount, hasCredit);

                // Has Image
                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyCategory, "Has Image", nullCompany));
                var hasImage = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyCategory, "Has Image", company);
                Assert.IsNotNull(hasImage);
                Assert.AreEqual(company.HasImage, hasImage);

                // Tax
                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Tax", nullCompany));
                var tax = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Tax", company);
                Assert.IsNotNull(tax);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTaxCategory, "Tax Exempt", null));
                var taxExempt = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CompanyTaxCategory, "Tax Exempt", tax);
                Assert.IsNotNull(taxExempt);
                Assert.AreEqual(company.IsTaxExempt, taxExempt);

                Assert.IsNull(AELTestHelper.GetPropertyResult<FlatListItem, CompanyData>(testBID, DataType.CompanyTaxCategory, "Tax Exempt Reason", null));
                var taxExemptReason = AELTestHelper.GetPropertyResult<FlatListItem, CompanyData>(testBID, DataType.CompanyTaxCategory, "Tax Exempt Reason", tax);
                Assert.IsNotNull(taxExempt);
                Assert.AreEqual(company.TaxExemptReason, taxExemptReason);

                Assert.IsNull(AELTestHelper.GetPropertyResult<TaxGroup, CompanyData>(testBID, DataType.CompanyTaxCategory, "Tax Group", null));
                var taxGroup = AELTestHelper.GetPropertyResult<TaxGroup, CompanyData>(testBID, DataType.CompanyTaxCategory, "Tax Group", tax);
                Assert.IsNotNull(taxGroup);
                Assert.AreEqual(company.TaxGroup, taxGroup);

                // Payment Terms
                Assert.IsNull(AELTestHelper.GetPropertyResult<PaymentTerm, CompanyData>(testBID, DataType.CompanyCategory, "Payment Terms", nullCompany));
                var paymentTerm = AELTestHelper.GetPropertyResult<PaymentTerm, CompanyData>(testBID, DataType.CompanyCategory, "Payment Terms", company);
                Assert.AreEqual(company.DefaultPaymentTerms, paymentTerm);

                // Subsidiaries
                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Subsidiaries", nullCompany));
                var subsidiary = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Subsidiaries", company);
                Assert.IsNotNull(subsidiary);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.SubsidiariesCategory, "Has Parent Company", null));
                var hasParentComp = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.SubsidiariesCategory, "Has Parent Company", subsidiary);
                Assert.IsNotNull(hasParentComp);
                Assert.AreEqual(company.ParentID.HasValue, hasParentComp);

                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.SubsidiariesCategory, "Parent Company", null));
                var parentComp = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.SubsidiariesCategory, "Parent Company", subsidiary);
                Assert.AreEqual(company.ParentCompany, parentComp);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.SubsidiariesCategory, "Parent Company Name", null));
                var parentCompName = AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.SubsidiariesCategory, "Parent Company Name", subsidiary);
                Assert.AreEqual(company.ParentCompany?.Name, parentCompName);

                Assert.IsNull(AELTestHelper.GetPropertyResult<List<CompanyData>, CompanyData>(testBID, DataType.SubsidiariesCategory, "Subsidiary", null));
                var subComp = AELTestHelper.GetPropertyResult<List<CompanyData>, CompanyData>(testBID, DataType.SubsidiariesCategory, "Subsidiary", subsidiary);
                Assert.AreEqual(company.ChildCompanies?.FirstOrDefault(), subComp?.FirstOrDefault());

                Assert.IsNull(AELTestHelper.GetPropertyResult<List<string>, CompanyData>(testBID, DataType.SubsidiariesCategory, "Subsidiary Name", null));
                var subCompName = AELTestHelper.GetPropertyResult<List<string>, CompanyData>(testBID, DataType.SubsidiariesCategory, "Subsidiary Name", subsidiary);
                Assert.AreEqual(company.ChildCompanies?.FirstOrDefault()?.Name, subCompName?[0]);

                subsidiary = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Subsidiaries", child);
                Assert.IsNotNull(subsidiary);

                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.SubsidiariesCategory, "Parent Company", null));
                parentComp = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.SubsidiariesCategory, "Parent Company", subsidiary);
                Assert.AreEqual(child.ParentCompany, parentComp);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.SubsidiariesCategory, "Parent Company Name", null));
                parentCompName = AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.SubsidiariesCategory, "Parent Company Name", subsidiary);
                Assert.AreEqual(child.ParentCompany?.Name, parentCompName);

                // Customer Credit
                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Customer Credit", nullCompany));
                var creditCat = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Customer Credit", company);
                Assert.IsNotNull(creditCat);

                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, CompanyData>(testBID, DataType.CustomerCreditCategory, "Customer Credit Limit", null));
                var creditLimit = AELTestHelper.GetPropertyResult<decimal?, CompanyData>(testBID, DataType.CustomerCreditCategory, "Customer Credit Limit", creditCat);
                Assert.AreEqual(company.CreditLimit, creditLimit);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CustomerCreditCategory, "Has Customer Credit", null));
                hasCredit = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.CustomerCreditCategory, "Has Customer Credit", creditCat);
                Assert.AreEqual(company.HasCreditAccount, hasCredit);

                Assert.IsNull(AELTestHelper.GetPropertyResult<decimal?, CompanyData>(testBID, DataType.CustomerCreditCategory, "Credit Available", null));
                var creditAvailable = AELTestHelper.GetPropertyResult<decimal?, CompanyData>(testBID, DataType.CustomerCreditCategory, "Credit Available", creditCat);
                decimal? compCreditAvailable = company.CreditLimit - (company.NonRefundableCredit + company.RefundableCredit);
                Assert.AreEqual(compCreditAvailable, creditAvailable);

                // PO Number
                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "PO Number", nullCompany));
                var poCategory = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "PO Number", company);
                Assert.IsNotNull(poCategory);

                Assert.IsNull(AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.PONumberCategory, "PO Required", null));
                var poRequired = AELTestHelper.GetPropertyResult<bool?, CompanyData>(testBID, DataType.PONumberCategory, "PO Required", poCategory);
                Assert.IsNotNull(poRequired);
                Assert.AreEqual(company.IsPORequired, poRequired);

                Assert.IsNull(AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.PONumberCategory, "Default PO Number", null));
                var defaultPO = AELTestHelper.GetPropertyResult<string, CompanyData>(testBID, DataType.PONumberCategory, "Default PO Number", poCategory);
                Assert.IsNotNull(defaultPO);
                Assert.AreEqual(company.DefaultPONumber, defaultPO);

                // Tags
                Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Tags", null));
                var tagLinks = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Tags", company);
            }
            finally
            {
                ctx.CompanyContactLink.Remove(testCompanyContactLink);
                ctx.ContactData.Remove(testContact);
                ctx.CompanyData.Remove(testCompChild);
                ctx.CompanyData.Remove(testComp);
                ctx.EmployeeTeamLink.Remove(testTeamLink);
                ctx.EmployeeTeam.Remove(testTeam);
                ctx.EmployeeData.Remove(testEmp);
                ctx.TaxGroup.Remove(testTaxGroup);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestCompanyDatesProperty()
        {
            CompanyData nullCompany = null;
            CompanyData company = GetTestCompany();
            var currentDate = DateTime.UtcNow;

            company.Orders = new List<TransactionHeaderData>()
            {
                new OrderData()
                {
                    BID = 1,
                    ID = -99,
                    Dates = new List<OrderKeyDate>()
                    {
                        new OrderKeyDate()
                        {
                            OrderItemID = -99,
                            KeyDateType = OrderKeyDateType.Created,
                            KeyDate = currentDate.AddDays(-5)
                        },
                        new OrderKeyDate()
                        {
                            KeyDateType = OrderKeyDateType.Invoiced,
                            KeyDate = currentDate.AddDays(5)
                        },
                    }
                },
                new OrderData()
                {
                    BID = 1,
                    ID = -98,
                    Dates = new List<OrderKeyDate>()
                    {
                        new OrderKeyDate()
                        {
                            KeyDateType = OrderKeyDateType.Created,
                            KeyDate = currentDate.AddDays(-4)
                        },
                        new OrderKeyDate()
                        {
                            OrderItemID = -99,
                            KeyDateType = OrderKeyDateType.Invoiced,
                            KeyDate = currentDate.AddDays(4)
                        },
                    }
                },
                new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    Dates = new List<OrderKeyDate>()
                    {
                        new OrderKeyDate()
                        {
                            KeyDateType = OrderKeyDateType.Created,
                            KeyDate = currentDate.AddDays(-3)
                        },
                        new OrderKeyDate()
                        {
                            OrderItemID = -99,
                            KeyDateType = OrderKeyDateType.Invoiced,
                            KeyDate = currentDate.AddDays(3)
                        },
                    }
                },
            };
            company.Payments = new List<PaymentMaster>()
            {
                new PaymentMaster()
                {
                    BID = 1,
                    ID = 1,
                    AccountingDT = currentDate.AddDays(-8)
                },
                new PaymentMaster()
                {
                    BID = 1,
                    ID = 2,
                    AccountingDT = currentDate.AddDays(-5)
                },
                new PaymentMaster()
                {
                    BID = 1,
                    ID = 3,
                    AccountingDT = currentDate.AddDays(-1)
                },
            };

            Assert.IsNull(AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Dates", nullCompany));
            var companyDates = AELTestHelper.GetPropertyResult<CompanyData, CompanyData>(testBID, DataType.CompanyCategory, "Dates", company);
            Assert.IsNotNull(companyDates);

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "Created Date", null));
            var createdDate = AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "Created Date", companyDates);
            Assert.IsNotNull(createdDate);
            Assert.AreEqual(company.CreatedDate.ToString(), createdDate.Value.ToString());

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "Last Payment Date", null));
            var lastPaymentDate = AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "Last Payment Date", companyDates);
            Assert.IsNotNull(lastPaymentDate);
            Assert.AreEqual(company.Payments.Max(p => p.AccountingDT).ToString(), lastPaymentDate.Value.ToString());

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "Last Order Date", null));
            var lastOrderDate = AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "Last Order Date", companyDates);
            Assert.IsNotNull(lastOrderDate);
            Assert.AreEqual(company.Orders.Max(o => o.Dates.Where(d => d.KeyDateType == OrderKeyDateType.Created && d.OrderItemID == null).Max(k => k.KeyDate)).ToString(), lastOrderDate.Value.ToString());

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "First Order Date", null));
            var firstOrderDate = AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "First Order Date", companyDates);
            Assert.IsNotNull(firstOrderDate);
            Assert.AreEqual(company.Orders.Min(o => o.Dates.Where(d => d.KeyDateType == OrderKeyDateType.Created && d.OrderItemID == null).Min(k => k.KeyDate)).ToString(), firstOrderDate.Value.ToString());

            Assert.IsNull(AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "Last Invoiced Date", null));
            var lastInvoicedDate = AELTestHelper.GetPropertyResult<DateTime?, CompanyData>(testBID, DataType.CompanyDatesCategory, "Last Invoiced Date", companyDates);
            Assert.IsNotNull(lastInvoicedDate);
            Assert.AreEqual(company.Orders.Max(o => o.Dates.Where(d => d.KeyDateType == OrderKeyDateType.Invoiced && d.OrderItemID == null).Max(k => k.KeyDate)).ToString(), lastInvoicedDate.Value.ToString());
        }

        [TestMethod]
        public void TestCompanyMemberInitializer()
        {
            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.CompanyCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(19, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CompanyContactListCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CompanyEmployeeListCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CompanyDatesCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.TimeZone));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Location));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Origin));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Industry));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.String));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CompanyTypeCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CompanyTaxCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.PaymentTerm));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.SubsidiariesCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CustomerCreditCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.PONumberCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CompanyTagsCategory));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.CompanyCustomField));


            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Contacts"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Employees"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Dates"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Time Zone"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Default Location"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Origin"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Industry"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Company Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Type"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Has Credit Account"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tax"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Has Image"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Payment Terms"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Subsidiaries"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Customer Credit"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "PO Number"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Tags"));
        }

        #region Private Handlers

        private TaxGroup GetTestTaxGroup(short bid = 1, short id = -99, string timestamp = "")
        {
            return new TaxGroup()
            {
                BID = bid,
                ID = id,
                Name = $"Tax.Group.{timestamp}"
            };
        }

        private CompanyData GetTestCompany(short bid = 1, int id = -99, string timestamp = "", int teamId = -99, short? taxGroupId = -99, int? parentCompanyId = null)
        {
            return new CompanyData()
            {
                BID = bid,
                ID = id,
                Name = $"CompanyData.{timestamp}",
                TeamID = teamId,
                LocationID = 1,
                StatusID = 1,
                IsVendor = false,
                IsProspect = false,
                IsClient = true,
                TaxGroupID = taxGroupId,
                ParentID = parentCompanyId,
                IsPORequired = false,
                DefaultPONumber = "blah blah blah",
                CreditLimit = 100,
                NonRefundableCredit = 50,
                RefundableCredit = 25,
            };
        }

        private ContactData GetTestContact(short bid = 1, int id = -99, string timestamp = "", int companyId = -99)
        {
            return new ContactData()
            {
                BID = bid,
                ID = id,
                First = "TEST",
                Last = timestamp
            };
        }

        private EmployeeData GetTestEmployee(short bid = 1, short id = -99, string timestamp = "")
        {
            return new EmployeeData()
            {
                BID = bid,
                ID = id,
                First = "TEST",
                Last = timestamp,
                LocationID = 1
            };
        }

        private EmployeeTeam GetTestTeam(short bid = 1, int id = -99, string timestamp = "")
        {
            return new EmployeeTeam()
            {
                BID = bid,
                ID = id,
                Name = $"Test.Team.{timestamp}"
            };
        }

        private EmployeeTeamLink GetTestTeamLink(short bid = 1, int teamId = -99, short empId = -99, short roleId = 1)
        {
            return new EmployeeTeamLink()
            {
                BID = bid,
                TeamID = teamId,
                EmployeeID = empId,
                RoleID = roleId
            };
        }

        #endregion
    }
}
