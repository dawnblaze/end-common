﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class PaymentsCategoryMemberInitializerTests
    {
        [TestInitialize]
        public void Initialize()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);
            OrderData testOrder = ctx.Find<OrderData>(testBID, -99);
            if (testOrder != null)
            {
                ctx.OrderData.Remove(testOrder);

                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public async Task TestPaymentsCategorySimplePropertiesTests()//TestPricesCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var testOrder = new OrderData()
            {
                BID = testBID,
                ID = -99,
                Description = $"Test.Order",
                CompanyID = ctx.CompanyData.FirstOrDefault().ID,
                LocationID = 1,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                Number = 9999,
                FormattedNumber = "ORD-9999",
                PickupLocationID = 1,
                ProductionLocationID = 1,
                TaxGroupID = ctx.TaxGroup.FirstOrDefault().ID,
                PriceProductTotal = 3m,
                PriceDiscount = 4m,
                PriceDestinationTotal = 5m,
                PriceFinanceCharge = 6m,
                PriceTax = 1m,
                PaymentBalanceDue = 20m,
                PaymentTotal = 40,
                PriceTotal = 60m,
            };

            try
            {
                ctx.OrderData.Add(testOrder);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                var orderDataFromDb = await ctx.OrderData
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == testOrder.ID);

                var paymentTotal = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.PaymentCategory, "Payment Amount", orderDataFromDb);
                Assert.AreEqual(testOrder.PaymentTotal, paymentTotal);

                var balanceDue = AELTestHelper.GetPropertyResult<decimal?, OrderData>(testBID, DataType.PaymentCategory, "Balance Due", orderDataFromDb);
                Assert.AreEqual(testOrder.PaymentBalanceDue, balanceDue);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.OrderData.Remove(testOrder);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestPaymentsCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.PaymentCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(2, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Number));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Payment Amount"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Balance Due"));

        }
    }
}
