﻿using Endor.AEL.Tests.Helper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using Endor.EF;
using Endor.Models;
using System;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class MergeFieldTests
    {
        private const short bid = 1;
        private readonly ApiContext ctx = AELTestHelper.GetMockCtx(bid);
        private readonly IDataProvider dataProvider = AELTestHelper.DataProvider(bid);

        private void AssertMergeLookupEqual(IMergeLookup mergeLookup, string mergeField, bool isValid, dynamic result, bool isNull, string resultAsString = null)
        {
            Assert.AreEqual(mergeField?.ToLower(), mergeLookup.MergeField?.ToLower());
            Assert.AreEqual(isValid, mergeLookup.IsValid);
            Assert.AreEqual(result, mergeLookup.Result);
            Assert.AreEqual(isNull, mergeLookup.IsNull);
            Assert.AreEqual(resultAsString ?? result?.ToString(), mergeLookup.ToString());
        }

        private void CheckSingleLookup(MergeFieldHelper mergeFieldHelper, string mergeField, bool isValid, dynamic result, bool isNull, string resultAsString = null)
        {
            AssertMergeLookupEqual(mergeFieldHelper.Lookup(mergeField), mergeField, isValid, result, isNull, resultAsString);
        }

        [TestMethod]
        [DataRow("Company.Company name", true, false, "Test Company")]
        [DataRow("Company name", true, false, "Test Company")]
        [DataRow("Company.Companyname", true, false, "Test Company")]
        [DataRow("CompanyName", true, false, "Test Company")]
        [DataRow("Company.Parties", false, true, null)]
        public void Lookup_Validity_Test(string mergeField, bool isValid, bool isNull, string expectedResult)
        {
            int minID = -999;
            var companyLinq = ctx.CompanyData.Where(x => x.BID == bid && x.ID < 0);
            if (companyLinq.Any())
                minID = companyLinq.Min(x => x.ID);

            int companyID = minID - 1;

            LocationData location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            string companyName = "Test Company";

            CompanyData testCompany = new CompanyData()
            {
                BID = 1,
                ID = companyID,
                IsActive = true,
                Name = companyName,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer,
                IsPORequired = false,
                HasImage = false,
                IsTaxExempt = false
            };

            ctx.CompanyData.Add(testCompany);
            ctx.SaveChanges();

            try
            {
                MergeFieldHelper mergeFieldHelper = new MergeFieldHelper(dataProvider, ClassType.Company.ID(), companyID);

                IMergeLookup result = mergeFieldHelper.Lookup(mergeField);

                AssertMergeLookupEqual(result, mergeField, isValid, expectedResult, isNull);
            }

            finally
            {
                ctx.CompanyData.Remove(testCompany);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        [DataRow(2, 1, "Estimate.Employees", "Company.Employees", "Company.Contacts")]
        [DataRow(1, 3, "Company.Parties", "Company.Employees", "Company.Perks", "Company.FreeFood")]
        public void LookupList_Validity_Test(int expectedValid, int expectedInvalid, params string[] mergeFields)
        {
            int minID = -999;
            var companyLinq = ctx.CompanyData.Where(x => x.BID == bid && x.ID < 0);
            if (companyLinq.Any())
                minID = companyLinq.Min(x => x.ID);

            int companyID = minID - 1;

            LocationData location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            string companyName = "Test Company";

            CompanyData testCompany = new CompanyData()
            {
                BID = 1,
                ID = companyID,
                IsActive = true,
                Name = companyName,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer,
                IsPORequired = false,
                HasImage = false,
                IsTaxExempt = false
            };

            ctx.CompanyData.Add(testCompany);
            ctx.SaveChanges();

            try
            {
                MergeFieldHelper mergeFieldHelper = new MergeFieldHelper(dataProvider, ClassType.Company.ID(), companyID);

                IEnumerable<IMergeLookup> result = mergeFieldHelper.Lookup(mergeFields);

                Assert.AreEqual(expectedValid, result.Where(r => r.IsValid).Count());
                Assert.AreEqual(expectedInvalid, result.Where(r => !r.IsValid).Count());
            }

            finally
            {
                ctx.CompanyData.Remove(testCompany);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        [DataRow("<h1>{Company.Company Name}<span>my {Company.Company Name}</span></h1>", "Test Company", null, "<h1>Test Company<span>my Test Company</span></h1>")]
        [DataRow("I have a company called {Company.Company name} {Company.Company name} {Company.company name}", "Test Company", null, "I have a company called Test Company Test Company Test Company")]
        [DataRow("We got a good event on {Company.Event dates} that will be held on {Company.Company name}", "Test Company", null, "We got a good event on {Company.Event dates} that will be held on Test Company")]
        [DataRow("Open bracket test 1 {{company name}.", "Test Company", null, "Open bracket test 1 {Test Company.")]
        [DataRow("Close bracket test 1 }{company name}.", "Test Company", null, "Close bracket test 1 }Test Company.")]
        [DataRow("Open bracket test 2 {company name}{.", "Test Company", null, "Open bracket test 2 Test Company{.")]
        [DataRow("Close bracket test 2 {company name}}.", "Test Company", null, "Close bracket test 2 Test Company}.")]
        [DataRow("Hi {User.name}", "Test Company", 1, "Hi AlphaAA LastName")]
        [DataRow("Hi I'm {User.name}, and my company's named {Company name}", "Test Company", 1, "Hi I'm AlphaAA LastName, and my company's named Test Company")]
        public async Task Merge_Test(string document, string companyName, int? userID, string expectedOutput)
        {
            int minCompanyID = -999;
            var companyLinq = ctx.CompanyData.Where(x => x.BID == bid && x.ID < 0);
            if (companyLinq.Any())
                minCompanyID = companyLinq.Min(x => x.ID);

            int companyID = minCompanyID - 1;

            short minUserLinkID = -999;
            var userLinkLinq = ctx.UserLink.Where(x => x.BID == bid && x.ID < 0);
            if (userLinkLinq.Any())
                minUserLinkID = userLinkLinq.Min(x => x.ID);

            short userLinkID = (short)(minUserLinkID - 1);

            UserLink testUserLink = new UserLink()
            {
                BID = bid,
                ID = userLinkID,
                AuthUserID = 1,
                UserName = "User@email.com",
                DisplayName = "AlphaAA LastName",
            };

            ctx.UserLink.Add(testUserLink);

            LocationData location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            CompanyData testCompany = new CompanyData()
            {
                BID = 1,
                ID = companyID,
                IsActive = true,
                Name = companyName,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer,
                IsPORequired = false,
                HasImage = false,
                IsTaxExempt = false
            };

            ctx.CompanyData.Add(testCompany);
            ctx.SaveChanges();

            try
            {
                MergeFieldHelper mergeFieldHelper = new MergeFieldHelper(dataProvider, ClassType.Company.ID(), companyID, userID);

                string result = await mergeFieldHelper.Merge(document);

                Assert.AreEqual(expectedOutput, result);
            }

            finally
            {
                ctx.UserLink.Remove(testUserLink);
                ctx.CompanyData.Remove(testCompany);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        [DataRow("<h1>{Company.CustomFields.CustomFieldText}<span>my {Company.Custom Fields.Custom Field Text}</span></h1>", "Test Company", null, "<h1>Custom Field Text<span>my Custom Field Text</span></h1>")]        
        public async Task CompanyCustomFieldValueFilter_Test(string document, string companyName, int? userID, string expectedOutput)
        {
            int minCompanyID = -999;
            var companyLinq = ctx.CompanyData.Where(x => x.BID == bid && x.ID < 0);
            if (companyLinq.Any())
                minCompanyID = companyLinq.Min(x => x.ID);

            int companyID = minCompanyID - 1;

            short minUserLinkID = -999;
            var userLinkLinq = ctx.UserLink.Where(x => x.BID == bid && x.ID < 0);
            if (userLinkLinq.Any())
                minUserLinkID = userLinkLinq.Min(x => x.ID);

            short userLinkID = (short)(minUserLinkID - 1);


            short minCustomFieldID = -999;
            var userCustomLinq = ctx.CustomFieldDefinition.Where(x => x.BID == bid && x.ID < 0);
            if (userCustomLinq.Any())
                minCustomFieldID = userCustomLinq.Min(x => x.ID);

            short customFieldID = (short)(minCustomFieldID - 1);

            UserLink testUserLink = new UserLink()
            {
                BID = bid,
                ID = userLinkID,
                AuthUserID = 1,
                UserName = "User@email.com",
                DisplayName = "AlphaAA LastName",
            };

            ctx.UserLink.Add(testUserLink);

            LocationData location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            CompanyData testCompany = new CompanyData()
            {
                BID = 1,
                ID = companyID,
                IsActive = true,
                Name = companyName,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer,
                IsPORequired = false,
                HasImage = false,
                IsTaxExempt = false
            };

            CustomFieldDefinition customFieldDefinition_string = new CustomFieldDefinition()
            {
                BID = 1,
                ID = customFieldID,
                AppliesToClassTypeID = ClassType.Company.ID(),
                DataType = DataType.String,
                Name = "Custom Field Text",
            };
                        
            CompanyCustomData companyCustomData = new CompanyCustomData()
            {
                BID = 1,
                ID = companyID,
                Company = testCompany,
                DataXML = $@"
                <data>
                    <ID>{customFieldID}</ID>
                    <V>Custom Field Text</V>
                </data>"
            };

            try
            {
                ctx.CustomFieldDefinition.Add(customFieldDefinition_string);
                ctx.CompanyData.Add(testCompany);
                ctx.CompanyCustomData.Add(companyCustomData);
                ctx.SaveChanges();

                MergeFieldHelper mergeFieldHelper = new MergeFieldHelper(dataProvider, ClassType.Company.ID(), companyID, userID);

                string result = await mergeFieldHelper.Merge(document);

                Assert.AreEqual(expectedOutput, result);

            }
            finally
            {
                ctx.CustomFieldDefinition.RemoveRange(customFieldDefinition_string);
                ctx.CompanyCustomData.Remove(companyCustomData);
                ctx.CompanyData.Remove(testCompany);
                ctx.UserLink.Remove(testUserLink);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void SelectCompanyDataTest()
        {
            int minID = -999;
            var companyLinq = ctx.CompanyData.Where(x => x.BID == bid && x.ID < 0);
            if (companyLinq.Any())
                minID = companyLinq.Min(x => x.ID);

            int parentCompanyID = minID - 1;
            int childCompanyID = minID - 2;
            int grandchildCompanyID = minID - 3;

            LocationData location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            string parentCompanyName = "Parent Company";
            string childCompanyName = "Child Company";
            string grandchildCompanyName = "Grandchild Company";

            CompanyData parentCompany = new CompanyData()
            {
                BID = 1,
                ID = parentCompanyID,
                IsActive = true,
                Name = parentCompanyName,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer,
                IsPORequired = false,
                HasImage = false,
                IsTaxExempt = false
            };

            CompanyData childCompany = new CompanyData()
            {
                BID = 1,
                ID = childCompanyID,
                IsActive = true,
                Name = childCompanyName,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer,
                IsPORequired = false,
                HasImage = false,
                IsTaxExempt = false,
                ParentCompany = parentCompany
            };

            CompanyData grandchildCompany = new CompanyData()
            {
                BID = 1,
                ID = grandchildCompanyID,
                IsActive = true,
                Name = grandchildCompanyName,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer,
                IsPORequired = false,
                HasImage = false,
                IsTaxExempt = false,
                ParentCompany = childCompany
            };

            ctx.CompanyData.Add(parentCompany);
            ctx.CompanyData.Add(childCompany);
            ctx.CompanyData.Add(grandchildCompany);
            ctx.SaveChanges();

            try
            {
                MergeFieldHelper parentMergeFieldHelper = new MergeFieldHelper(dataProvider, ClassType.Company.ID(), parentCompanyID);
                MergeFieldHelper childMergeFieldHelper = new MergeFieldHelper(dataProvider, ClassType.Company.ID(), childCompanyID);
                MergeFieldHelper grandchildMergeFieldHelper = new MergeFieldHelper(dataProvider, ClassType.Company.ID(), grandchildCompanyID);

                CheckSingleLookup(parentMergeFieldHelper, " ", false, null, true);
                CheckSingleLookup(parentMergeFieldHelper, null, false, null, true);
                CheckSingleLookup(parentMergeFieldHelper, "blah", false, null, true);

                CheckSingleLookup(parentMergeFieldHelper, "Company Name", true, parentCompanyName, false);
                CheckSingleLookup(childMergeFieldHelper, "Company Name", true, childCompanyName, false);
                CheckSingleLookup(grandchildMergeFieldHelper, "Company Name", true, grandchildCompanyName, false);

                CheckSingleLookup(parentMergeFieldHelper, "Company.Company Name", true, parentCompanyName, false);
                CheckSingleLookup(childMergeFieldHelper, "Company.Company Name", true, childCompanyName, false);
                CheckSingleLookup(grandchildMergeFieldHelper, "Company.Company Name", true, grandchildCompanyName, false);

                CheckSingleLookup(parentMergeFieldHelper, "Company.Parent Company.Company Name", true, null, true);
                CheckSingleLookup(childMergeFieldHelper, "Company.Parent Company.Company Name", true, parentCompanyName, false);
                CheckSingleLookup(grandchildMergeFieldHelper, "Company.Parent Company.Company Name", true, childCompanyName, false);

                CheckSingleLookup(parentMergeFieldHelper, "Company.Parent Company.Parent Company.Company Name", true, null, true);
                CheckSingleLookup(childMergeFieldHelper, "Company.Parent Company.Parent Company.Company Name", true, null, true);
                CheckSingleLookup(grandchildMergeFieldHelper, "Company.Parent Company.Parent Company.Company Name", true, parentCompanyName, false);

                List<IMergeLookup> manyResult;

                manyResult = grandchildMergeFieldHelper.Lookup(new string[] { "Company Name", "Company.Parent Company.Company Name", "Company.Parent Company.Parent Company.Company Name" }).ToList();

                Assert.AreEqual(3, manyResult.Count);
                AssertMergeLookupEqual(manyResult[0], "Company Name", true, grandchildCompanyName, false);
                AssertMergeLookupEqual(manyResult[1], "Company.Parent Company.Company Name", true, childCompanyName, false);
                AssertMergeLookupEqual(manyResult[2], "Company.Parent Company.Parent Company.Company Name", true, parentCompanyName, false);

                manyResult = childMergeFieldHelper.Lookup(new string[] { "Company Name", "Company.Parent Company.Company Name", "Company.Parent Company.Parent Company.Company Name" }).ToList();

                Assert.AreEqual(3, manyResult.Count);
                AssertMergeLookupEqual(manyResult[0], "Company Name", true, childCompanyName, false);
                AssertMergeLookupEqual(manyResult[1], "Company.Parent Company.Company Name", true, parentCompanyName, false);
                AssertMergeLookupEqual(manyResult[2], "Company.Parent Company.Parent Company.Company Name", true, null, true);

                manyResult = parentMergeFieldHelper.Lookup(new string[] { "Company Name", "Company.Parent Company.Company Name", "Company.Parent Company.Parent Company.Company Name" }).ToList();

                Assert.AreEqual(3, manyResult.Count);
                AssertMergeLookupEqual(manyResult[0], "Company Name", true, parentCompanyName, false);
                AssertMergeLookupEqual(manyResult[1], "Company.Parent Company.Company Name", true, null, true);
                AssertMergeLookupEqual(manyResult[2], "Company.Parent Company.Parent Company.Company Name", true, null, true);

                manyResult = parentMergeFieldHelper.Lookup(new string[] { "Company Name", "blah" }).ToList();

                Assert.AreEqual(2, manyResult.Count);
                AssertMergeLookupEqual(manyResult[0], "Company Name", true, parentCompanyName, false);
                AssertMergeLookupEqual(manyResult[1], "blah", false, null, true);
            }

            finally
            {
                ctx.CompanyData.Remove(grandchildCompany);
                ctx.CompanyData.Remove(childCompany);
                ctx.CompanyData.Remove(parentCompany);
                ctx.SaveChanges();
            }
        }


        [TestMethod]
        [DataRow("Description", "My test order", "My test order")]
        [DataRow("Tax Amount", 1.23, "$1.23")]
        [DataRow("Tax Rate", 1.00, "1")]
        [DataRow("OrderNumber", 1234, "1234")]
        public void SelectOrderDataTest(string mergeField, object _expectedResult, string expectedResultAsString)
        {
            dynamic expectedResult;

            if (_expectedResult is double)
                expectedResult = Convert.ToDecimal((double)_expectedResult);
            else
                expectedResult = _expectedResult;

            string testOrderDescription = "My test order";
            string testCompanyName = "Test Company";

            LocationData location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            TaxGroup taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            int minOrderID = -999;
            var orderLinq = ctx.OrderData.Where(x => x.BID == bid && x.ID < 0);
            if (orderLinq.Any())
                minOrderID = orderLinq.Min(x => x.ID);

            int testOrderID = minOrderID - 1;

            int minCompanyID = -999;
            var companyLinq = ctx.CompanyData.Where(x => x.BID == bid && x.ID < 0);
            if (companyLinq.Any())
                minCompanyID = companyLinq.Min(x => x.ID);

            int testCompanyID = minCompanyID - 1;

            CompanyData testCompany = new CompanyData()
            {
                BID = bid,
                ID = testCompanyID,
                IsActive = true,
                Name = testCompanyName,
                LocationID = location.ID,
                IsAdHoc = false,
                StatusID = (byte)CompanyStatus.Customer,
                IsPORequired = false,
                HasImage = false,
                IsTaxExempt = false
            };

            ctx.CompanyData.Add(testCompany);

            var testOrder = new OrderData
            {
                ID = testOrderID,
                BID = bid,
                Number = 1234,
                Company = testCompany,
                Location = location,
                PickupLocation = location,
                ProductionLocation = location,
                Description = testOrderDescription,
                OrderPONumber = "PO Number",
                FormattedNumber = "",
                CostTotal = 1.23m,
                PriceProductTotal = 0.00m,
                PriceDestinationTotal = 0.00m,
                PriceFinanceCharge = 0.00m,
                PriceNet = 1.23m,
                PriceDiscount = 0.00m,
                PriceDiscountPercent = 0.00m,
                PriceTaxRate = 1.0m,
                IsTaxExempt = true,
                TaxGroup = taxGroup,
                PricePreTax = 1.23m,
                PriceTax = 1.23m,
                PriceTotal = 4.56m,
                PaymentTotal = 0.00m,
                PaymentBalanceDue = 0.00m,
                PaymentWriteOff = 0.00m,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
            };

            ctx.OrderData.Add(testOrder);
            ctx.SaveChanges();


            try
            {
                MergeFieldHelper mergeFieldHelper = new MergeFieldHelper(dataProvider, ClassType.Order.ID(), testOrderID);

                CheckSingleLookup(mergeFieldHelper, mergeField, true, expectedResult, false, expectedResultAsString);
            }

            finally
            {
                ctx.OrderData.Remove(testOrder);
                ctx.CompanyData.Remove(testCompany);
                ctx.SaveChanges();
            }
        }
    }
}
