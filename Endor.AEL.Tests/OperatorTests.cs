using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OperatorTests
    {
        private static OperandType AssertOperatorHasFunction(DataType leftOperandType, OperatorType operatorType, DataType rightOperandType)
        {
            OperatorInfo op = OperatorDictionary.Instance.GetValueOrDefault(operatorType);

            Assert.IsNotNull(op, $"Operator {operatorType} not found.");

            var operand = op.OperandTypes.Find(x => x.LeftDataType == leftOperandType && x.RightDataType == rightOperandType);
            Assert.IsNotNull(operand, $"Operator {leftOperandType} {operatorType} {rightOperandType} not found.");

            Assert.IsNotNull(operand.LinqFx, $"Operator {leftOperandType} {operatorType} {rightOperandType} does not have a Linq function defined.");
            Assert.IsNotNull(operand.Expression, $"Operator {leftOperandType} {operatorType} {rightOperandType} does not have a Expression defined.");

            return operand;
        }

        [TestMethod]
        public void TestStringOperators()
        {
            void AssertFunction_S(bool expectedResult, OperandType op, string v1)
            {
                bool actualResult = op.LinqFx(v1, null);
                Assert.AreEqual(expectedResult, actualResult);

                //actualResult = AELTestHelper.DoGetOperationResult<string, object>(op.Expression, v1, null);
                //Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_S_S(bool expectedResult, OperandType op, string v1, string v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                //actualResult = AELTestHelper.DoGetOperationResult<string, string>(op.Expression, v1, v2);
                //Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_S_SL(bool expectedResult, OperandType op, string v1, List<string> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                //actualResult = AELTestHelper.DoGetOperationResult<string, List<string>>(op.Expression, v1, v2);
                //Assert.AreEqual(expectedResult, actualResult);
            }

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.EQ, DataType.String);
            AssertFunction_S_S(true, myFunc, "A", "A");
            AssertFunction_S_S(true, myFunc, "A", "a");
            AssertFunction_S_S(false, myFunc, "A", "B");
            AssertFunction_S_S(false, myFunc, "A", null);
            AssertFunction_S_S(true, myFunc, "", null);
            AssertFunction_S_S(true, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.NEQ, DataType.String);
            AssertFunction_S_S(false, myFunc, "A", "A");
            AssertFunction_S_S(false, myFunc, "A", "a");
            AssertFunction_S_S(true, myFunc, "A", "B");
            AssertFunction_S_S(true, myFunc, "A", null);
            AssertFunction_S_S(false, myFunc, "", null);
            AssertFunction_S_S(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.ISNULL, DataType.None);
            AssertFunction_S(false, myFunc, "A");
            AssertFunction_S(true, myFunc, "");
            AssertFunction_S(true, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.ISNOTNULL, DataType.None);
            AssertFunction_S(true, myFunc, "A");
            AssertFunction_S(false, myFunc, "");
            AssertFunction_S(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.IN, DataType.StringList);
            AssertFunction_S_SL(true, myFunc, "B", new List<string> { "A", "B", "C", null });
            AssertFunction_S_SL(true, myFunc, "B", new List<string> { "a", "b", "c", null });
            AssertFunction_S_SL(true, myFunc, "", new List<string> { "a", "b", "c", null });
            AssertFunction_S_SL(true, myFunc, null, new List<string> { "a", "b", "c", null });
            AssertFunction_S_SL(false, myFunc, "X", new List<string> { "A", "B", "C", null });

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.NOTIN, DataType.StringList);
            AssertFunction_S_SL(false, myFunc, "B", new List<string> { "A", "B", "C", null });
            AssertFunction_S_SL(false, myFunc, "B", new List<string> { "a", "b", "c", null });
            AssertFunction_S_SL(false, myFunc, "", new List<string> { "a", "b", "c", null });
            AssertFunction_S_SL(false, myFunc, null, new List<string> { "a", "b", "c", null });
            AssertFunction_S_SL(true, myFunc, "X", new List<string> { "A", "B", "C", null });

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.LT, DataType.String);
            AssertFunction_S_S(true, myFunc, "A", "B");
            AssertFunction_S_S(false, myFunc, "A", "A");
            AssertFunction_S_S(false, myFunc, "A", null);
            AssertFunction_S_S(false, myFunc, "A", "");
            AssertFunction_S_S(false, myFunc, "", null);
            AssertFunction_S_S(true, myFunc, "Aaron", "Adam");
            AssertFunction_S_S(false, myFunc, "Adam", "Aaron");
            AssertFunction_S_S(true, myFunc, "A", "b");
            AssertFunction_S_S(false, myFunc, "A", "a");
            AssertFunction_S_S(true, myFunc, "a", "B");
            AssertFunction_S_S(false, myFunc, "a", "A");
            AssertFunction_S_S(true, myFunc, "aaron", "Adam");
            AssertFunction_S_S(false, myFunc, "adam", "Aaron");

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.GT, DataType.String);
            AssertFunction_S_S(false, myFunc, "A", "B");
            AssertFunction_S_S(false, myFunc, "A", "A");
            AssertFunction_S_S(true, myFunc, "A", null);
            AssertFunction_S_S(true, myFunc, "A", "");
            AssertFunction_S_S(false, myFunc, "", null);
            AssertFunction_S_S(false, myFunc, "Aaron", "Adam");
            AssertFunction_S_S(true, myFunc, "Adam", "Aaron");
            AssertFunction_S_S(false, myFunc, "A", "b");
            AssertFunction_S_S(false, myFunc, "A", "a");
            AssertFunction_S_S(false, myFunc, "a", "B");
            AssertFunction_S_S(false, myFunc, "a", "A");
            AssertFunction_S_S(false, myFunc, "aaron", "Adam");
            AssertFunction_S_S(true, myFunc, "adam", "Aaron");

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.LTE, DataType.String);
            AssertFunction_S_S(true, myFunc, "A", "B");
            AssertFunction_S_S(true, myFunc, "A", "A");
            AssertFunction_S_S(false, myFunc, "A", null);
            AssertFunction_S_S(false, myFunc, "A", "");
            AssertFunction_S_S(true, myFunc, "", null);
            AssertFunction_S_S(true, myFunc, "Aaron", "Adam");
            AssertFunction_S_S(false, myFunc, "Adam", "Aaron");
            AssertFunction_S_S(true, myFunc, "A", "b");
            AssertFunction_S_S(true, myFunc, "A", "a");
            AssertFunction_S_S(true, myFunc, "a", "B");
            AssertFunction_S_S(true, myFunc, "a", "A");
            AssertFunction_S_S(true, myFunc, "aaron", "Adam");
            AssertFunction_S_S(false, myFunc, "adam", "Aaron");

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.GTE, DataType.String);
            AssertFunction_S_S(false, myFunc, "A", "B");
            AssertFunction_S_S(true, myFunc, "A", "A");
            AssertFunction_S_S(true, myFunc, "A", null);
            AssertFunction_S_S(true, myFunc, "A", "");
            AssertFunction_S_S(true, myFunc, "", null);
            AssertFunction_S_S(false, myFunc, "Aaron", "Adam");
            AssertFunction_S_S(true, myFunc, "Adam", "Aaron");
            AssertFunction_S_S(false, myFunc, "A", "b");
            AssertFunction_S_S(true, myFunc, "A", "a");
            AssertFunction_S_S(false, myFunc, "a", "B");
            AssertFunction_S_S(true, myFunc, "a", "A");
            AssertFunction_S_S(false, myFunc, "aaron", "Adam");
            AssertFunction_S_S(true, myFunc, "adam", "Aaron");

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.CONTAINSTEXT, DataType.String);
            AssertFunction_S_S(true, myFunc, "ABC", "B");
            AssertFunction_S_S(true, myFunc, "abc", "B");
            AssertFunction_S_S(false, myFunc, "ABC", "X");
            AssertFunction_S_S(false, myFunc, "ABC", "");
            AssertFunction_S_S(false, myFunc, "ABC", null);

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.NOTCONTAINSTEXT, DataType.String);
            AssertFunction_S_S(false, myFunc, "ABC", "B");
            AssertFunction_S_S(false, myFunc, "abc", "B");
            AssertFunction_S_S(true, myFunc, "ABC", "X");
            AssertFunction_S_S(true, myFunc, "ABC", "");
            AssertFunction_S_S(true, myFunc, "ABC", null);

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.CONTAINSTEXT, DataType.StringList);
            AssertFunction_S_SL(true, myFunc, "Banana", new List<string> { "A", "B", "C", null });
            AssertFunction_S_SL(true, myFunc, "Banana", new List<string> { "a", "b", "c", null });
            AssertFunction_S_SL(false, myFunc, "Lemon", new List<string> { "A", "B", "C", null });
            AssertFunction_S_SL(false, myFunc, null, new List<string> { "A", "B", "C", null });

            myFunc = AssertOperatorHasFunction(DataType.String, OperatorType.NOTCONTAINSTEXT, DataType.StringList);
            AssertFunction_S_SL(false, myFunc, "Banana", new List<string> { "A", "B", "C", null });
            AssertFunction_S_SL(false, myFunc, "Banana", new List<string> { "a", "b", "c", null });
            AssertFunction_S_SL(true, myFunc, "Lemon", new List<string> { "A", "B", "C", null });
            AssertFunction_S_SL(true, myFunc, null, new List<string> { "A", "B", "C", null });
        }

        [TestMethod]
        public void TestStringListOperators()
        {
            void AssertFunction_SL_S(bool expectedResult, OperandType op, List<string> v1, string v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                //actualResult = AELTestHelper.DoGetOperationResult<List<string>, string>(op.Expression, v1, v2);
                //Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_SL_SL(bool expectedResult, OperandType op, List<string> v1, List<string> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                //actualResult = AELTestHelper.DoGetOperationResult<List<string>, List<string>>(op.Expression, v1, v2);
                //Assert.AreEqual(expectedResult, actualResult);
            }

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.StringList, OperatorType.IN, DataType.StringList);
            AssertFunction_SL_SL(true, myFunc, new List<string> { "B" }, new List<string> { "A", "B", "C" });
            AssertFunction_SL_SL(true, myFunc, new List<string> { "B" }, new List<string> { "a", "b", "c" });
            AssertFunction_SL_SL(false, myFunc, new List<string> { "X", "A" }, new List<string> { "A", "B", "C" });

            myFunc = AssertOperatorHasFunction(DataType.StringList, OperatorType.NOTIN, DataType.StringList);
            AssertFunction_SL_SL(false, myFunc, new List<string> { "B" }, new List<string> { "A", "B", "C" });
            AssertFunction_SL_SL(false, myFunc, new List<string> { "B" }, new List<string> { "a", "b", "c" });
            AssertFunction_SL_SL(true, myFunc, new List<string> { "X", "A" }, new List<string> { "A", "B", "C" });

            myFunc = AssertOperatorHasFunction(DataType.StringList, OperatorType.INCLUDES, DataType.String);
            AssertFunction_SL_S(true, myFunc, new List<string> { "A", "B", "C" }, "B");
            AssertFunction_SL_S(true, myFunc, new List<string> { "a", "b", "c" }, "B");
            AssertFunction_SL_S(false, myFunc, new List<string> { "A", "B", "C" }, "X" );

            myFunc = AssertOperatorHasFunction(DataType.StringList, OperatorType.NOTINCLUDES, DataType.String);
            AssertFunction_SL_S(false, myFunc, new List<string> { "A", "B", "C" }, "B");
            AssertFunction_SL_S(false, myFunc, new List<string> { "a", "b", "c" }, "B");
            AssertFunction_SL_S(true, myFunc, new List<string> { "A", "B", "C" }, "X");
        }

        [TestMethod]
        public void TestBooleanOperators()
        {
            void AssertFunction_B_B(bool expectedResult, OperandType op, bool? v1, bool? v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                //actualResult = AELTestHelper.DoGetOperationResult<bool?, bool?>(op.Expression, v1, v2);
                //Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_B(bool expectedResult, OperandType op, bool? v1)
            {
                bool actualResult = op.LinqFx(v1, null);
                Assert.AreEqual(expectedResult, actualResult);

                //actualResult = AELTestHelper.DoGetOperationResult<bool?, object>(op.Expression, v1, null);
                //Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_B_BL(bool expectedResult, OperandType op, bool? v1, List<bool?> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                //actualResult = AELTestHelper.DoGetOperationResult<bool?, List<bool?>>(op.Expression, v1, v2);
                //Assert.AreEqual(expectedResult, actualResult);
            }

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.EQ, DataType.Boolean);
            AssertFunction_B_B(true, myFunc, true, true);
            AssertFunction_B_B(true, myFunc, false, false);
            AssertFunction_B_B(false, myFunc, false, true);
            AssertFunction_B_B(false, myFunc, true, false);
            AssertFunction_B_B(false, myFunc, true, null);
            AssertFunction_B_B(false, myFunc, false, null);
            AssertFunction_B_B(true, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.NEQ, DataType.Boolean);
            AssertFunction_B_B(false, myFunc, true, true);
            AssertFunction_B_B(false, myFunc, false, false);
            AssertFunction_B_B(true, myFunc, false, true);
            AssertFunction_B_B(true, myFunc, true, false);
            AssertFunction_B_B(true, myFunc, true, null);
            AssertFunction_B_B(true, myFunc, false, null);
            AssertFunction_B_B(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.ISNULL, DataType.None);
            AssertFunction_B(false, myFunc, true);
            AssertFunction_B(false, myFunc, false);
            AssertFunction_B(true, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.ISNOTNULL, DataType.None);
            AssertFunction_B(true, myFunc, true);
            AssertFunction_B(true, myFunc, false);
            AssertFunction_B(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.IN, DataType.BooleanList);
            AssertFunction_B_BL(true, myFunc, true, new List<bool?> { true, false, null });
            AssertFunction_B_BL(true, myFunc, false, new List<bool?> { true, false });
            AssertFunction_B_BL(false, myFunc, false, new List<bool?> { true, null });
            AssertFunction_B_BL(false, myFunc, true, new List<bool?> { false, null });
            AssertFunction_B_BL(false, myFunc, false, new List<bool?> { true });
            AssertFunction_B_BL(false, myFunc, null, new List<bool?> { true, false });
            AssertFunction_B_BL(true, myFunc, null, new List<bool?> { true, null });
            AssertFunction_B_BL(false, myFunc, null, new List<bool?> { true });

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.NOTIN, DataType.BooleanList);
            AssertFunction_B_BL(false, myFunc, true, new List<bool?> { true, false, null });
            AssertFunction_B_BL(false, myFunc, false, new List<bool?> { true, false });
            AssertFunction_B_BL(true, myFunc, false, new List<bool?> { true, null });
            AssertFunction_B_BL(true, myFunc, true, new List<bool?> { false, null });
            AssertFunction_B_BL(true, myFunc, false, new List<bool?> { true });
            AssertFunction_B_BL(true, myFunc, null, new List<bool?> { true, false });
            AssertFunction_B_BL(false, myFunc, null, new List<bool?> { true, null });
            AssertFunction_B_BL(true, myFunc, null, new List<bool?> { true });

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.AND, DataType.Boolean);
            AssertFunction_B_B(true, myFunc, true, true);
            AssertFunction_B_B(false, myFunc, false, false);
            AssertFunction_B_B(false, myFunc, false, true);
            AssertFunction_B_B(false, myFunc, true, false);
            AssertFunction_B_B(false, myFunc, null, false);
            AssertFunction_B_B(false, myFunc, false, null);
            AssertFunction_B_B(false, myFunc, null, null);
            AssertFunction_B_B(true, myFunc, null, true);
            AssertFunction_B_B(true, myFunc, true, null);


            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.OR, DataType.Boolean);
            AssertFunction_B_B(true, myFunc, true, true);
            AssertFunction_B_B(false, myFunc, false, false);
            AssertFunction_B_B(true, myFunc, false, true);
            AssertFunction_B_B(true, myFunc, true, false);
            AssertFunction_B_B(false, myFunc, null, false);
            AssertFunction_B_B(false, myFunc, false, null);
            AssertFunction_B_B(false, myFunc, null, null);
            AssertFunction_B_B(true, myFunc, null, true);
            AssertFunction_B_B(true, myFunc, true, null);

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.NOT, DataType.None);
            AssertFunction_B(false, myFunc, true);
            AssertFunction_B(true, myFunc, false);
            AssertFunction_B(true, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.ISTRUE, DataType.None);
            AssertFunction_B(true, myFunc, true);
            AssertFunction_B(false, myFunc, false);
            AssertFunction_B(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.Boolean, OperatorType.ISFALSE, DataType.None);
            AssertFunction_B(false, myFunc, true);
            AssertFunction_B(true, myFunc, false);
            AssertFunction_B(true, myFunc, null);
        }

        [TestMethod]
        public void TestBooleanListOperators()
        {
            void AssertFunction_BL_BL(bool expectedResult, OperandType op, List<bool?> v1, List<bool?> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<List<bool?>, List<bool?>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_BL_B(bool expectedResult, OperandType op, List<bool?> v1, bool? v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<List<bool?>, bool?>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.BooleanList, OperatorType.IN, DataType.BooleanList);
            AssertFunction_BL_BL(true, myFunc, new List<bool?> { true }, new List<bool?> { true, false, null });
            AssertFunction_BL_BL(true, myFunc, new List<bool?> { false }, new List<bool?> { true, false });
            AssertFunction_BL_BL(false, myFunc, new List<bool?> { false }, new List<bool?> { true, null });
            AssertFunction_BL_BL(false, myFunc, new List<bool?> { true }, new List<bool?> { false, null });
            AssertFunction_BL_BL(false, myFunc, new List<bool?> { false }, new List<bool?> { true });
            AssertFunction_BL_BL(false, myFunc, new List<bool?> { null }, new List<bool?> { true, false });
            AssertFunction_BL_BL(true, myFunc, new List<bool?> { null }, new List<bool?> { true, null });
            AssertFunction_BL_BL(false, myFunc, new List<bool?> { null }, new List<bool?> { true });

            myFunc = AssertOperatorHasFunction(DataType.BooleanList, OperatorType.NOTIN, DataType.BooleanList);
            AssertFunction_BL_BL(false, myFunc, new List<bool?> { true }, new List<bool?> { true, false, null });
            AssertFunction_BL_BL(false, myFunc, new List<bool?> { false }, new List<bool?> { true, false });
            AssertFunction_BL_BL(true, myFunc, new List<bool?> { false }, new List<bool?> { true, null });
            AssertFunction_BL_BL(true, myFunc, new List<bool?> { true }, new List<bool?> { false, null });
            AssertFunction_BL_BL(true, myFunc, new List<bool?> { false }, new List<bool?> { true });
            AssertFunction_BL_BL(true, myFunc, new List<bool?> { null }, new List<bool?> { true, false });
            AssertFunction_BL_BL(false, myFunc, new List<bool?> { null }, new List<bool?> { true, null });
            AssertFunction_BL_BL(true, myFunc, new List<bool?> { null }, new List<bool?> { true });

            myFunc = AssertOperatorHasFunction(DataType.BooleanList, OperatorType.INCLUDES, DataType.Boolean);
            AssertFunction_BL_B(true, myFunc, new List<bool?> { true, false, null }, true);
            AssertFunction_BL_B(true, myFunc, new List<bool?> { true, false }, false);
            AssertFunction_BL_B(false, myFunc, new List<bool?> { true, null }, false);
            AssertFunction_BL_B(false, myFunc, new List<bool?> { false, null }, true);
            AssertFunction_BL_B(false, myFunc, new List<bool?> { true }, false);
            AssertFunction_BL_B(false, myFunc, new List<bool?> { true, false }, null);
            AssertFunction_BL_B(true, myFunc, new List<bool?> { true, null }, null);
            AssertFunction_BL_B(false, myFunc, new List<bool?> { true }, null);

            myFunc = AssertOperatorHasFunction(DataType.BooleanList, OperatorType.NOTINCLUDES, DataType.Boolean);
            AssertFunction_BL_B(false, myFunc, new List<bool?> { true, false, null }, true);
            AssertFunction_BL_B(false, myFunc, new List<bool?> { true, false }, false);
            AssertFunction_BL_B(true, myFunc, new List<bool?> { true, null }, false);
            AssertFunction_BL_B(true, myFunc, new List<bool?> { false, null }, true);
            AssertFunction_BL_B(true, myFunc, new List<bool?> { true }, false);
            AssertFunction_BL_B(true, myFunc, new List<bool?> { true, false }, null);
            AssertFunction_BL_B(false, myFunc, new List<bool?> { true, null }, null);
            AssertFunction_BL_B(true, myFunc, new List<bool?> { true }, null);
        }

        [TestMethod]
        public void TestNumberOperators()
        {
            void AssertFunction_N_N(bool expectedResult, OperandType op, decimal? v1, decimal? v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<decimal?, decimal?>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_N(bool expectedResult, OperandType op, decimal? v1)
            {
                bool actualResult = op.LinqFx(v1, null);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<decimal?, object>(op.Expression, v1, null);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_N_NL(bool expectedResult, OperandType op, decimal? v1, List<decimal?> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<decimal?, List<decimal?>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.EQ, DataType.Number);
            AssertFunction_N_N(true, myFunc, 5.0m, 5m);
            AssertFunction_N_N(true, myFunc, 0m, 0m);
            AssertFunction_N_N(false, myFunc, 5m, 0m);
            AssertFunction_N_N(false, myFunc, 5m, null);
            AssertFunction_N_N(false, myFunc, 0m, null);
            AssertFunction_N_N(true, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.NEQ, DataType.Number);
            AssertFunction_N_N(false, myFunc, 5.0m, 5m);
            AssertFunction_N_N(false, myFunc, 0m, 0m);
            AssertFunction_N_N(true, myFunc, 5m, 0m);
            AssertFunction_N_N(true, myFunc, 5m, null);
            AssertFunction_N_N(true, myFunc, 0m, null);
            AssertFunction_N_N(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.ISNULL, DataType.None);
            AssertFunction_N(false, myFunc, 5m);
            AssertFunction_N(false, myFunc, 0m);
            AssertFunction_N(true, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.ISNOTNULL, DataType.None);
            AssertFunction_N(true, myFunc, 5m);
            AssertFunction_N(true, myFunc, 0m);
            AssertFunction_N(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.IN, DataType.NumberList);
            AssertFunction_N_NL(true, myFunc, 5m, new List<decimal?> { 1m, 3m, 5m, null });
            AssertFunction_N_NL(true, myFunc, null, new List<decimal?> { 1m, 3m, 5m, null });
            AssertFunction_N_NL(false, myFunc, 2m, new List<decimal?> { 1m, 3m, 5m, null });

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.NOTIN, DataType.NumberList);
            AssertFunction_N_NL(false, myFunc, 5m, new List<decimal?> { 1m, 3m, 5m, null });
            AssertFunction_N_NL(false, myFunc, null, new List<decimal?> { 1m, 3m, 5m, null });
            AssertFunction_N_NL(true, myFunc, 2m, new List<decimal?> { 1m, 3m, 5m, null });

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.LT, DataType.Number);
            AssertFunction_N_N(false, myFunc, 5m, 5m);
            AssertFunction_N_N(false, myFunc, 5m, 0m);
            AssertFunction_N_N(true, myFunc, 0m, 5m);
            AssertFunction_N_N(false, myFunc, 5m, null);
            AssertFunction_N_N(false, myFunc, null, 5m);
            AssertFunction_N_N(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.GT, DataType.Number);
            AssertFunction_N_N(false, myFunc, 5m, 5m);
            AssertFunction_N_N(true, myFunc, 5m, 0m);
            AssertFunction_N_N(false, myFunc, 0m, 5m);
            AssertFunction_N_N(false, myFunc, 5m, null);
            AssertFunction_N_N(false, myFunc, null, 5m);
            AssertFunction_N_N(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.LTE, DataType.Number);
            AssertFunction_N_N(true, myFunc, 5m, 5m);
            AssertFunction_N_N(false, myFunc, 5m, 0m);
            AssertFunction_N_N(true, myFunc, 0m, 5m);
            AssertFunction_N_N(false, myFunc, 5m, null);
            AssertFunction_N_N(false, myFunc, null, 5m);
            AssertFunction_N_N(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.Number, OperatorType.GTE, DataType.Number);
            AssertFunction_N_N(true, myFunc, 5m, 5m);
            AssertFunction_N_N(true, myFunc, 5m, 0m);
            AssertFunction_N_N(false, myFunc, 0m, 5m);
            AssertFunction_N_N(false, myFunc, 5m, null);
            AssertFunction_N_N(false, myFunc, null, 5m);
            AssertFunction_N_N(false, myFunc, null, null);
        }

        [TestMethod]
        public void TestNumberListOperators()
        {
            void AssertFunction_NL_NL(bool expectedResult, OperandType op, List<decimal?> v1, List<decimal?> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<List<decimal?>, List<decimal?>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_NL_N(bool expectedResult, OperandType op, List<decimal?> v1, decimal? v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<List<decimal?>, decimal?>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.NumberList, OperatorType.IN, DataType.NumberList);
            AssertFunction_NL_NL(true, myFunc, new List<decimal?> { 5m }, new List<decimal?> { 0m, 5m, null });
            AssertFunction_NL_NL(false, myFunc, new List<decimal?> { 2m }, new List<decimal?> { 0m, 5m, null });
            AssertFunction_NL_NL(false, myFunc, new List<decimal?> { null }, new List<decimal?> { 0m, 5m });
            AssertFunction_NL_NL(true, myFunc, new List<decimal?> { null }, new List<decimal?> { 0m, 5m, null });

            myFunc = AssertOperatorHasFunction(DataType.NumberList, OperatorType.NOTIN, DataType.NumberList);
            AssertFunction_NL_NL(false, myFunc, new List<decimal?> { 5m }, new List<decimal?> { 0m, 5m, null });
            AssertFunction_NL_NL(true, myFunc, new List<decimal?> { 2m }, new List<decimal?> { 0m, 5m, null });
            AssertFunction_NL_NL(true, myFunc, new List<decimal?> { null }, new List<decimal?> { 0m, 5m });
            AssertFunction_NL_NL(false, myFunc, new List<decimal?> { null }, new List<decimal?> { 0m, 5m, null });

            myFunc = AssertOperatorHasFunction(DataType.NumberList, OperatorType.INCLUDES, DataType.Number);
            AssertFunction_NL_N(true, myFunc, new List<decimal?> { 1m, 3m, 5m, null }, 5m);
            AssertFunction_NL_N(true, myFunc, new List<decimal?> { 1m, 3m, 5m, null }, null);
            AssertFunction_NL_N(false, myFunc, new List<decimal?> { 1m, 3m, 5m, null }, 2m);

            myFunc = AssertOperatorHasFunction(DataType.NumberList, OperatorType.NOTINCLUDES, DataType.Number);
            AssertFunction_NL_N(false, myFunc, new List<decimal?> { 1m, 3m, 5m, null }, 5m);
            AssertFunction_NL_N(false, myFunc, new List<decimal?> { 1m, 3m, 5m, null }, null);
            AssertFunction_NL_N(true, myFunc, new List<decimal?> { 1m, 3m, 5m, null }, 2m);
        }

        [TestMethod]
        public void TestDateTimeOperators()
        {
            void AssertFunction_D_D(bool expectedResult, OperandType op, DateTime? v1, DateTime? v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<DateTime?, DateTime?>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_D(bool expectedResult, OperandType op, DateTime? v1)
            {
                bool actualResult = op.LinqFx(v1, null);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<DateTime?, object>(op.Expression, v1, null);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_D_DL(bool expectedResult, OperandType op, DateTime? v1, List<DateTime?> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<DateTime?, List<DateTime?>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.EQ, DataType.DateTime);
            AssertFunction_D_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 00, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), null);
            AssertFunction_D_D(true, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.NEQ, DataType.DateTime);
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 00, 0));
            AssertFunction_D_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), null);
            AssertFunction_D_D(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.LT, DataType.DateTime);
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(true, myFunc, new DateTime(2016, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 00, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), null);
            AssertFunction_D_D(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.LTE, DataType.DateTime);
            AssertFunction_D_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(true, myFunc, new DateTime(2016, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 00, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), null);
            AssertFunction_D_D(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.GT, DataType.DateTime);
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2016, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 00, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), null);
            AssertFunction_D_D(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.GTE, DataType.DateTime);
            AssertFunction_D_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2016, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new DateTime(2018, 1, 1, 3, 00, 0));
            AssertFunction_D_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), null);
            AssertFunction_D_D(false, myFunc, null, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.ISNULL, DataType.None);
            AssertFunction_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D(true, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.ISNOTNULL, DataType.None);
            AssertFunction_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.ISFUTURE, DataType.None);
            AssertFunction_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D(true, myFunc, DateTime.UtcNow.AddDays(30));
            AssertFunction_D(false, myFunc, DateTime.UtcNow.AddDays(-30));
            AssertFunction_D(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.INPAST, DataType.None);
            AssertFunction_D(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D(false, myFunc, DateTime.UtcNow.AddDays(30));
            AssertFunction_D(true, myFunc, DateTime.UtcNow.AddDays(-30));
            AssertFunction_D(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.ISTODAY, DataType.None);
            AssertFunction_D(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_D(true, myFunc, DateTime.UtcNow.Date.AddHours(6));
            AssertFunction_D(true, myFunc, DateTime.UtcNow.Date);
            AssertFunction_D(false, myFunc, DateTime.UtcNow.Date.AddSeconds(-6));
            AssertFunction_D(true, myFunc, DateTime.UtcNow.Date.AddSeconds(6));
            AssertFunction_D(false, myFunc, DateTime.UtcNow.AddDays(30));
            AssertFunction_D(false, myFunc, DateTime.UtcNow.AddDays(-30));
            AssertFunction_D(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.IN, DataType.DateTimeList);
            AssertFunction_D_DL(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_D_DL(true, myFunc, null, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_D_DL(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 5), null });

            myFunc = AssertOperatorHasFunction(DataType.DateTime, OperatorType.NOTIN, DataType.DateTimeList);
            AssertFunction_D_DL(false, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_D_DL(false, myFunc, null, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_D_DL(true, myFunc, new DateTime(2018, 1, 1, 3, 30, 0), new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 5), null });
        }

        [TestMethod]
        public void TestDateTimeListOperators()
        {
            void AssertFunction_DL_D(bool expectedResult, OperandType op, List<DateTime?> v1, DateTime? v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<List<DateTime?>, DateTime?>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_DL_DL(bool expectedResult, OperandType op, List<DateTime?> v1, List<DateTime?> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<List<DateTime?>, List<DateTime?>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.DateTimeList, OperatorType.IN, DataType.DateTimeList);
            AssertFunction_DL_DL(true, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0) }, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_DL_DL(true, myFunc, new List<DateTime?> { null }, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_DL_DL(false, myFunc, null, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_DL_DL(false, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0) }, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 5), null });

            myFunc = AssertOperatorHasFunction(DataType.DateTimeList, OperatorType.NOTIN, DataType.DateTimeList);
            AssertFunction_DL_DL(false, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0) }, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_DL_DL(false, myFunc, new List<DateTime?> { null }, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_DL_DL(true, myFunc, null, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null });
            AssertFunction_DL_DL(true, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0) }, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 5), null });

            myFunc = AssertOperatorHasFunction(DataType.DateTimeList, OperatorType.INCLUDES, DataType.DateTime);
            AssertFunction_DL_D(true, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null }, new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_DL_D(true, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null }, null);
            AssertFunction_DL_D(false, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 5), null }, new DateTime(2018, 1, 1, 3, 30, 0));

            myFunc = AssertOperatorHasFunction(DataType.DateTimeList, OperatorType.NOTINCLUDES, DataType.DateTime);
            AssertFunction_DL_D(false, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null }, new DateTime(2018, 1, 1, 3, 30, 0));
            AssertFunction_DL_D(false, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 0), null }, null);
            AssertFunction_DL_D(true, myFunc, new List<DateTime?> { new DateTime(2018, 1, 1, 3, 30, 5), null }, new DateTime(2018, 1, 1, 3, 30, 0));
        }

        [TestMethod]
        public void TestObjectOperators()
        {
            void AssertFunction_O_O(bool expectedResult, OperandType op, AtomValue<short> v1, AtomValue<short> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<AtomValue<short>, AtomValue<short>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_O(bool expectedResult, OperandType op, AtomValue<short> v1)
            {
                bool actualResult = op.LinqFx(v1, null);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<AtomValue<short>, object>(op.Expression, v1, null);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_O_OL(bool expectedResult, OperandType op, AtomValue<short> v1, List<AtomValue<short>> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<AtomValue<short>, List<AtomValue<short>>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            var atomValue1 = new AtomValue<short>
            {
                BID = 1,
                ID = 1,
                DataType = DataType.Employee,
            };
            var atomValue1_Copy = new AtomValue<short>
            {
                BID = 1,
                ID = 1,
                DataType = DataType.Employee,
            };
            var atomValue2 = new AtomValue<short>
            {
                BID = 1,
                ID = 2,
                DataType = DataType.Employee,
            };

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectScalar, OperatorType.EQ, DataType.AnyObjectScalar);
            AssertFunction_O_O(true, myFunc, atomValue1, atomValue1_Copy);
            AssertFunction_O_O(false, myFunc, atomValue1, atomValue2);
            AssertFunction_O_O(false, myFunc, atomValue1, null);

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectScalar, OperatorType.NEQ, DataType.AnyObjectScalar);
            AssertFunction_O_O(false, myFunc, atomValue1, atomValue1_Copy);
            AssertFunction_O_O(true, myFunc, atomValue1, atomValue2);
            AssertFunction_O_O(true, myFunc, atomValue1, null);

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectScalar, OperatorType.ISNULL, DataType.None);
            AssertFunction_O(false, myFunc, atomValue1);
            AssertFunction_O(true, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectScalar, OperatorType.ISNOTNULL, DataType.None);
            AssertFunction_O(true, myFunc, atomValue1);
            AssertFunction_O(false, myFunc, null);

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectScalar, OperatorType.IN, DataType.AnyObjectList);
            AssertFunction_O_OL(true, myFunc, null, new List<AtomValue<short>> { atomValue1_Copy, null });
            AssertFunction_O_OL(true, myFunc, null, new List<AtomValue<short>> { atomValue1, null });
            AssertFunction_O_OL(false, myFunc, atomValue1, new List<AtomValue<short>> { atomValue2, null });

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectScalar, OperatorType.NOTIN, DataType.AnyObjectList);
            AssertFunction_O_OL(false, myFunc, atomValue1, new List<AtomValue<short>> { atomValue1_Copy, null });
            AssertFunction_O_OL(false, myFunc, null, new List<AtomValue<short>> { atomValue1, null });
            AssertFunction_O_OL(true, myFunc, atomValue1, new List<AtomValue<short>> { atomValue2, null });
        }

        [TestMethod]
        public void TestObjectListOperators()
        {
            void AssertFunction_OL_O(bool expectedResult, OperandType op, List<AtomValue<short>> v1, AtomValue<short> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<List<AtomValue<short>>, AtomValue<short>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            void AssertFunction_OL_OL(bool expectedResult, OperandType op, List<AtomValue<short>> v1, List<AtomValue<short>> v2)
            {
                bool actualResult = op.LinqFx(v1, v2);
                Assert.AreEqual(expectedResult, actualResult);

                actualResult = AELTestHelper.DoGetOperationResult<List<AtomValue<short>>, List<AtomValue<short>>>(op.Expression, v1, v2);
                Assert.AreEqual(expectedResult, actualResult);
            }

            var atomValue1 = new AtomValue<short>
            {
                BID = 1,
                ID = 1,
                DataType = DataType.Employee,
            };
            var atomValue1_Copy = new AtomValue<short>
            {
                BID = 1,
                ID = 1,
                DataType = DataType.Employee,
            };
            var atomValue2 = new AtomValue<short>
            {
                BID = 1,
                ID = 2,
                DataType = DataType.Employee,
            };

            OperandType myFunc;

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectList, OperatorType.IN, DataType.AnyObjectList);
            AssertFunction_OL_OL(true, myFunc, new List<AtomValue<short>> { atomValue1 }, new List<AtomValue<short>> { atomValue1_Copy, null });
            AssertFunction_OL_OL(true, myFunc, new List<AtomValue<short>> { null }, new List<AtomValue<short>> { atomValue1, null });
            AssertFunction_OL_OL(false, myFunc, null, new List<AtomValue<short>> { atomValue1, null });
            AssertFunction_OL_OL(false, myFunc, new List<AtomValue<short>> { atomValue1 }, new List<AtomValue<short>> { atomValue2, null });

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectList, OperatorType.NOTIN, DataType.AnyObjectList);
            AssertFunction_OL_OL(false, myFunc, new List<AtomValue<short>> { atomValue1 }, new List<AtomValue<short>> { atomValue1_Copy, null });
            AssertFunction_OL_OL(false, myFunc, new List<AtomValue<short>> { null }, new List<AtomValue<short>> { atomValue1, null });
            AssertFunction_OL_OL(false, myFunc, null, new List<AtomValue<short>> { atomValue1, null });
            AssertFunction_OL_OL(true, myFunc, new List<AtomValue<short>> { atomValue1 }, new List<AtomValue<short>> { atomValue2, null });

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectList, OperatorType.INCLUDES, DataType.AnyObjectScalar);
            AssertFunction_OL_O(true, myFunc, new List<AtomValue<short>> { atomValue1, null }, atomValue1_Copy);
            AssertFunction_OL_O(true, myFunc, new List<AtomValue<short>> { atomValue1, null }, null);
            AssertFunction_OL_O(false, myFunc, new List<AtomValue<short>> { atomValue1, null }, atomValue2);

            myFunc = AssertOperatorHasFunction(DataType.AnyObjectList, OperatorType.NOTINCLUDES, DataType.AnyObjectScalar);
            AssertFunction_OL_O(false, myFunc, new List<AtomValue<short>> { atomValue1, null }, atomValue1_Copy);
            AssertFunction_OL_O(false, myFunc, new List<AtomValue<short>> { atomValue1, null }, null);
            AssertFunction_OL_O(true, myFunc, new List<AtomValue<short>> { atomValue1, null }, atomValue2);
        }
    }
}
