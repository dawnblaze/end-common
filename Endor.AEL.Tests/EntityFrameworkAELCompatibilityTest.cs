﻿using Endor.AEL.Tests.Helper;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    public class AELDataProvider : IAELDataProvider
    {
        private readonly ApiContext _ApiContext;

        public AELDataProvider(short bid, ApiContext ctx)
        {
            this.BID = bid;
            this._ApiContext = ctx;
        }

        public short BID { get; private set; }

        public IQueryable<T> GetData<T>() where T : class, IAtom
        {
                    return this._ApiContext.Set<T>().Where(x => x.BID.Equals(this.BID));
                }
    }

    [TestClass]
    public class EntityFrameworkAELCompatibilityTest
    {
        [TestMethod]
        public async Task TestEstimateSimpleProperties()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);

            var boardDefinition = await ctx.BoardDefinitionData.Include(b => b.RoleLinks)
               .Where(b => b.BID == testBID && b.ID == 1125).FirstOrDefaultAsync();

            AELOperation aelOp = AELOperation.Deserialize(boardDefinition.ConditionFx);

            var result = ctx.OrderItemData.Where(new AELDataProvider(testBID, ctx), aelOp);

            Assert.IsNotNull(result);
        }
    }
}
