﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class BoardDataTests
    {
        const short bid = 1;

        [TestMethod]
        public void OrderTaxExempt_BoardTest()
        {
            const string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3, // boolean
    ""Operator"": 8, // OR
    ""LhsDataType"": 3, // boolean
    ""RhsDataType"": 3, // boolean
    ""Terms"": [{
            ""$type"": ""AELOperation"",
            ""DataType"": 3, // boolean
            ""Operator"": 6, // AND
            ""LhsDataType"": 3, // boolean
            ""RhsDataType"": 3, // boolean
            ""Terms"": [{
                    ""$type"": ""AELOperation"",
                    ""DataType"": 3, // boolean
                    ""Operator"": 0,
                    ""LhsDataType"": 1, // string
                    ""RhsDataType"": -1, // liststring
                    ""Terms"": [{
                            ""$type"": ""AELMember"",
                            ""DataType"": 3, // boolean
                            ""Text"": ""Tax Exempt"",
                            ""ID"": 20051,
                            ""MemberBase"": {
                                ""$type"": ""AELMember"",
                                ""DataType"": 20050,
                                ""Text"": ""Taxes"",
                                ""ID"": 10011,
                                ""MemberBase"": {
                                    ""$type"": ""AELMember"",
                                    ""DataType"": 20010,
                                    ""Text"": ""Order"",
                                    ""ID"": 0,
                                    ""MemberBase"": null
                                },
                            },
                        }, {
                            ""$type"": ""AELValues"",
                            ""DataType"": 3, // boolean
                            ""Values"": [true],
                            ""DisplayText"": true
                        }
                    ]
                }
            ]
        }
    ]
}
";

            var ctx = AELTestHelper.GetMockCtx(bid);

            var location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            ctx.OrderEmployeeRole.RemoveRange(ctx.OrderEmployeeRole.Where(o => o.OrderID == -999));
            ctx.OrderData.RemoveRange(ctx.OrderData.Where(o => o.ID == -999));
            ctx.SaveChanges();
            var order = new OrderData
            {
                ID = -999,
                BID = bid,
                Number = -999,
                Company = company,
                Location = location,
                PickupLocation = location,
                ProductionLocation = location,
                Description = "My test order",
                //TaxExemptReasonID = -99,
                Origin = origin,
                OrderPONumber = "PO Number",
                FormattedNumber = "",
                CostTotal = 1.23m,
                TaxGroup = taxGroup,
                PriceProductTotal = 0.00m,
                PriceDestinationTotal = 0.00m,
                PriceFinanceCharge = 0.00m,
                PriceNet = 1.23m,
                PriceDiscount = 0.00m,
                PriceDiscountPercent = 0.00m,
                PriceTaxRate = 1.0m,
                IsTaxExempt = true,
                PricePreTax = 1.23m,
                PriceTax = 0.00m,
                PriceTotal = 4.56m,
                PaymentTotal = 0.00m,
                PaymentBalanceDue = 0.00m,
                PaymentWriteOff = 0.00m,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                Dates = new List<OrderKeyDate>
                {
                    new OrderKeyDate
                    {
                        IsDestinationDate = false,
                        IsOrderItemDate = false,
                        IsFirmDate = false,
                        KeyDateType = OrderKeyDateType.Created,
                        KeyDT =  DateTime.UtcNow,
                    },
                },
            };

            ctx.OrderData.Add(order);
            ctx.SaveChanges();

            try
            {
                var withCondition = ctx.OrderData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionFx);

                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == -999));
            }
            finally
            {
                ctx.OrderKeyDate.RemoveRange(order.Dates);
                ctx.OrderData.Remove(order);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public async Task OrderLineItemLevel_BoardTest()
        {
            var ctx = AELTestHelper.GetMockCtx(bid);
            short testID = -99;
            try
            {
                var dirPath = Assembly.GetExecutingAssembly().Location;
                dirPath = Path.GetDirectoryName(dirPath);
                var path = dirPath + @"\";

                var payloadJson = File.ReadAllText(path + "payloads\\boarddefinition_orderitemlevel_payload.json");
                var newBoardDefinition = JsonConvert.DeserializeObject<BoardDefinitionData>(payloadJson);
                newBoardDefinition.BID = bid;
                newBoardDefinition.ID = testID;
                await ctx.BoardDefinitionData.AddAsync(newBoardDefinition);
                await ctx.SaveChangesAsync();

                var boardDefinition = await ctx.BoardDefinitionData.Include(b => b.RoleLinks)
                    .Where(b => b.BID == bid && b.ID == newBoardDefinition.ID).FirstOrDefaultAsync();
                var ids = new int[] { };
                AELOperation aelOp = AELOperation.Deserialize(boardDefinition.ConditionFx);
                var result = ctx.OrderItemData
                .Include(x => x.Order)
                .Include(x => x.Order.Company)
                .Include(x => x.Order.Items)
                .Include(x => x.SubStatus)
                .Include(x => x.OrderItemStatus)
                .Include(x => x.Dates)
                .Include(x => x.EmployeeRoles).ThenInclude(x => x.Employee)
                .Include(x => x.TagLinks).ThenInclude(x => x.Tag)
                .Where(AELTestHelper.DataProvider(bid), aelOp)
                .Where(x => ids.Length == 0 || ids.Contains(x.ID))
                .OrderByDescending(x => x.IsUrgent ?? false)
                .ThenBy(x => x.Priority ?? 0)
                .ThenBy(x => x.Order.Dates.FirstOrDefault(z => z.KeyDateType.Equals(OrderKeyDateType.ProductionDue)).KeyDate)
                .ThenByDescending(x => x.Order == null ? "" : (x.Order.FormattedNumber ?? null));
                result.ToList();
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            } 
            finally
            {
                var boardDefinition = await ctx.BoardDefinitionData.FirstOrDefaultAsync(x => x.ID == testID);
                if (boardDefinition != null)
                {
                    ctx.BoardDefinitionData.Remove(boardDefinition);
                    await ctx.SaveChangesAsync();
                }
            }
            
        }

        [TestMethod]
        public async Task OrderLevel_BoardTest()
        {
            var ctx = AELTestHelper.GetMockCtx(bid);
            short testID = -98;
            try
            {
                var dirPath = Assembly.GetExecutingAssembly().Location;
                dirPath = Path.GetDirectoryName(dirPath);
                var path = dirPath + @"\";
                var payloadJson = File.ReadAllText(path + "payloads\\boarddefinition_orderlevel.json");
                var newBoardDefinition = JsonConvert.DeserializeObject<BoardDefinitionData>(payloadJson);
                newBoardDefinition.BID = bid;
                newBoardDefinition.ID = testID;
                await ctx.BoardDefinitionData.AddAsync(newBoardDefinition);
                await ctx.SaveChangesAsync();

                var boardDefinition = await ctx.BoardDefinitionData.Include(b => b.RoleLinks)
                    .Where(b => b.BID == bid && b.ID == testID).FirstOrDefaultAsync();

                var ids = new int[] { };
                AELOperation aelOp = AELOperation.Deserialize(boardDefinition.ConditionFx);
                var result = ctx.OrderData
                .Include(x => x.Company)
                .Include(x => x.Location)
                .Include(x => x.Dates)
                .Include(x => x.TagLinks).ThenInclude(x => x.Tag)
                .Include(x => x.EmployeeRoles).ThenInclude(x => x.Employee)
                .Include(x => x.Items)
                .Include(x => x.Company).ThenInclude(x => x.DefaultPaymentTerms)
                .Where(AELTestHelper.DataProvider(bid), aelOp)
                .Where(x => ids.Length == 0 || ids.Contains(x.ID))
                .OrderByDescending(x => x.IsUrgent ?? false)
                .ThenBy(x => x.Priority ?? 0)
                .ThenBy(x => x.Dates.FirstOrDefault(d => d.KeyDateType == OrderKeyDateType.ProductionDue).KeyDT)
                .ThenByDescending(x => x.FormattedNumber ?? null);
                result.ToList();

                Assert.IsTrue(true);
            } 
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            finally
            {
                var boardDefinition = await ctx.BoardDefinitionData.FirstOrDefaultAsync(x => x.ID == testID);
                if (boardDefinition != null)
                {
                    ctx.BoardDefinitionData.Remove(boardDefinition);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        [TestMethod]
        public void OrderAssignedToFilter_BoardTest()
        {
            // Orders assigned to a specified employee

            const string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3,
    ""Operator"": 8,
    ""LhsDataType"": 3,
    ""RhsDataType"": 3,
    ""Terms"": [{
        ""$type"": ""AELOperation"",
        ""DataType"": 3,
        ""Operator"": 6,
        ""LhsDataType"": 3,
        ""RhsDataType"": 3,
        ""Terms"": [{
            ""$type"": ""AELOperation"",
            ""DataType"": 3,
            ""Operator"": 0,
            ""LhsDataType"": 1,
            ""RhsDataType"": -1,
            ""Terms"": [{
                ""$type"": ""AELMember"",
                ""DataType"": 5000,
                ""Text"": ""Name"",
                ""ID"": 20220,
                ""MemberBase"": {
                    ""$type"": ""AELMember"",
                    ""DataType"": 20220,
                    ""Text"": ""Assigned To"",
                    ""ID"": 20201,
                    ""MemberBase"": {
                        ""$type"": ""AELMember"",
                        ""DataType"": 20200,
                        ""Text"": ""Order"",
                        ""ID"": 10002,
                        ""MemberBase"": {
                            ""$type"": ""AELMember"",
                            ""DataType"": 20010,
                            ""Text"": ""Order"",
                            ""ID"": -1,
                        },
                    },
                },
            },
            {
                ""$type"": ""AELValues"",
                ""DataType"": 5000,
                ""Values"": [<<EmployeeID>>]
            }]
        }]
    }]
}
";

            int testid = -999;
            var ctx = AELTestHelper.GetMockCtx(bid);
            var foundOrder = ctx.OrderData.Find(bid, testid);
            if (foundOrder != null)
            {
                ctx.Entry(foundOrder).Collection(t => t.EmployeeRoles).Load();
                ctx.OrderEmployeeRole.RemoveRange(foundOrder.EmployeeRoles);
                ctx.OrderData.Remove(foundOrder);
                ctx.SaveChanges();
            }

            var location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var employee = ctx.EmployeeData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            var order = new OrderData
            {
                ID = testid,
                BID = bid,
                Number = testid,
                Company = company,
                Location = location,
                PickupLocation = location,
                ProductionLocation = location,
                Description = "My test order",
                //TaxExemptReasonID = -99,
                Origin = origin,
                OrderPONumber = "PO Number",
                FormattedNumber = "",
                CostTotal = 1.23m,
                TaxGroup = taxGroup,
                PriceProductTotal = 0.00m,
                PriceDestinationTotal = 0.00m,
                PriceFinanceCharge = 0.00m,
                PriceNet = 1.23m,
                PriceDiscount = 0.00m,
                PriceDiscountPercent = 0.00m,
                PriceTaxRate = 1.0m,
                IsTaxExempt = true,
                PricePreTax = 1.23m,
                PriceTax = 0.00m,
                PriceTotal = 4.56m,
                PaymentTotal = 0.00m,
                PaymentBalanceDue = 0.00m,
                PaymentWriteOff = 0.00m,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
                EmployeeRoles = new List<OrderEmployeeRole>
                {
                    new OrderEmployeeRole
                    {
                        Employee = employee,
                        RoleID = SystemIDs.EmployeeRole.AssignedTo,
                    }
                },
            };


            try
            {
                ctx.OrderData.Add(order);
                ctx.SaveChanges();


                var withCondition = ctx.OrderData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionFx.Replace("<<EmployeeID>>", employee.ID.ToString()))
                ;

                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == testid));
            }
            finally
            {
                ctx.OrderEmployeeRole.RemoveRange(order.EmployeeRoles);
                ctx.OrderData.Remove(order);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void OrderMultipleAssignedToFilter_BoardTest()
        {
            const string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3,
    ""Operator"": 8,
    ""LhsDataType"": 3,
    ""RhsDataType"": 3,
    ""Terms"": [{
        ""$type"": ""AELOperation"",
        ""DataType"": 3,
        ""Operator"": 6,
        ""LhsDataType"": 3,
        ""RhsDataType"": 3,
        ""Terms"": [{
            ""$type"": ""AELOperation"",
            ""DataType"": 3,
            ""Operator"": 14,
            ""LhsDataType"": 1,
            ""RhsDataType"": -1,
            ""Terms"": [{
                ""$type"": ""AELMember"",
                ""DataType"": 5000,
                ""Text"": ""Name"",
                ""ID"": 20220,
                ""MemberBase"": {
                    ""$type"": ""AELMember"",
                    ""DataType"": 20220,
                    ""Text"": ""Assigned To"",
                    ""ID"": 20201,
                    ""MemberBase"": {
                        ""$type"": ""AELMember"",
                        ""DataType"": 20200,
                        ""Text"": ""Order"",
                        ""ID"": 10002,
                        ""MemberBase"": {
                            ""$type"": ""AELMember"",
                            ""DataType"": 20010,
                            ""Text"": ""Order"",
                            ""ID"": -1,
                            ""MemberBase"": null
                        },
                    },
                },
            },
            {
                ""$type"": ""AELValues"",
                ""DataType"": -5000,
                ""Values"": [<<EmployeeIDs>>]
            }]
        }]
    }]
}
";

            var ctx = AELTestHelper.GetMockCtx(bid);

            var location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var employees = ctx.EmployeeData.Where(x => x.BID == bid).OrderByDescending(x => x.ID).Take(2).ToList();

            var orders = new List<OrderData>
            {
                new OrderData
                {
                    ID = -999,
                    BID = bid,
                    Number = -999,
                    Company = company,
                    Location = location,
                    PickupLocation = location,
                    ProductionLocation = location,
                    Description = "My test order",
                    //TaxExemptReasonID = -99,
                    Origin = origin,
                    OrderPONumber = "PO Number",
                    FormattedNumber = "",
                    CostTotal = 1.23m,
                    TaxGroup = taxGroup,
                    PriceProductTotal = 0.00m,
                    PriceDestinationTotal = 0.00m,
                    PriceFinanceCharge = 0.00m,
                    PriceNet = 1.23m,
                    PriceDiscount = 0.00m,
                    PriceDiscountPercent = 0.00m,
                    PriceTaxRate = 1.0m,
                    IsTaxExempt = true,
                    PricePreTax = 1.23m,
                    PriceTax = 0.00m,
                    PriceTotal = 4.56m,
                    PaymentTotal = 0.00m,
                    PaymentBalanceDue = 0.00m,
                    PaymentWriteOff = 0.00m,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    EmployeeRoles = new List<OrderEmployeeRole>
                    {
                        new OrderEmployeeRole
                        {
                            BID = bid,
                            ID = -999,
                            Employee = employees.FirstOrDefault(),
                            RoleID = SystemIDs.EmployeeRole.AssignedTo,
                        }
                    },
                },
                new OrderData
                {
                    ID = -998,
                    BID = bid,
                    Number = -998,
                    Company = company,
                    Location = location,
                    PickupLocation = location,
                    ProductionLocation = location,
                    Description = "My test order2",
                    //TaxExemptReasonID = -99,
                    Origin = origin,
                    OrderPONumber = "PO Number",
                    FormattedNumber = "",
                    CostTotal = 1.23m,
                    TaxGroup = taxGroup,
                    PriceProductTotal = 0.00m,
                    PriceDestinationTotal = 0.00m,
                    PriceFinanceCharge = 0.00m,
                    PriceNet = 1.23m,
                    PriceDiscount = 0.00m,
                    PriceDiscountPercent = 0.00m,
                    PriceTaxRate = 1.0m,
                    IsTaxExempt = true,
                    PricePreTax = 1.23m,
                    PriceTax = 0.00m,
                    PriceTotal = 4.56m,
                    PaymentTotal = 0.00m,
                    PaymentBalanceDue = 0.00m,
                    PaymentWriteOff = 0.00m,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    EmployeeRoles = new List<OrderEmployeeRole>
                    {
                        new OrderEmployeeRole
                        {
                            BID = bid,
                            ID = -998,
                            Employee = employees.LastOrDefault(),
                            RoleID = SystemIDs.EmployeeRole.AssignedTo,
                        }
                    },
                }
            };

            ctx.OrderData.AddRange(orders);
            ctx.SaveChanges();

            try
            {
                var withCondition = ctx.OrderData
                    .Where(AELTestHelper.DataProvider(bid)
                        , boardConditionFx.Replace("<<EmployeeIDs>>"
                            , string.Join(',', employees.Select(x => x.ID)))
                    );

                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == -999));
                Assert.IsTrue(result.Any(x => x.ID == -998));
            }
            finally
            {
                ctx.OrderEmployeeRole.RemoveRange(orders.SelectMany(x => x.EmployeeRoles));
                ctx.OrderData.RemoveRange(orders);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void OrderMultipleSaleAmountFilter_BoardTest()
        {
            const string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3,
    ""Operator"": 8,
    ""LhsDataType"": 3,
    ""RhsDataType"": 3,
    ""Terms"": [{
        ""$type"": ""AELOperation"",
        ""DataType"": 3,
        ""Operator"": 6,
        ""LhsDataType"": 3,
        ""RhsDataType"": 3,
        ""Terms"": [{
            ""$type"": ""AELOperation"",
            ""DataType"": 3,
            ""Operator"": 14,
            ""LhsDataType"": 1,
            ""RhsDataType"": -1,
            ""Terms"": [{
                ""$type"": ""AELMember"",
                ""DataType"": 2,
                ""Text"": ""Price (Pre Tax)"",
                ""ID"": 20071,
                ""MemberBase"": {
                    ""$type"": ""AELMember"",
                    ""DataType"": 20060,
                    ""Text"": ""Order"",
                    ""ID"": -1,
                    ""MemberBase"": null
                },
            },
            {
                ""$type"": ""AELValues"",
                ""DataType"": -2,
                ""Values"": [<<Prices>>]
            }]
        }]
    }]
}
";

            var ctx = AELTestHelper.GetMockCtx(bid);

            var location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            var orders = new List<OrderData>
            {
                new OrderData
                {
                    ID = -999,
                    BID = bid,
                    Number = -999,
                    Company = company,
                    Location = location,
                    PickupLocation = location,
                    ProductionLocation = location,
                    Description = "My test order",
                    //TaxExemptReasonID = -99,
                    Origin = origin,
                    OrderPONumber = "PO Number",
                    FormattedNumber = "",
                    CostTotal = 1.23m,
                    TaxGroup = taxGroup,
                    PriceProductTotal = 3.21m,
                    PriceDestinationTotal = 0.00m,
                    PriceFinanceCharge = 0.00m,
                    PriceDiscount = 0.00m,
                    PriceDiscountPercent = 0.00m,
                    PriceTaxRate = 1.0m,
                    IsTaxExempt = true,
                    PriceTax = 0.00m,
                    PaymentWriteOff = 0.00m,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                },
                new OrderData
                {
                    ID = -998,
                    BID = bid,
                    Number = -998,
                    Company = company,
                    Location = location,
                    PickupLocation = location,
                    ProductionLocation = location,
                    Description = "My test order2",
                    //TaxExemptReasonID = -99,
                    Origin = origin,
                    OrderPONumber = "PO Number",
                    FormattedNumber = "",
                    CostTotal = 1.23m,
                    TaxGroup = taxGroup,
                    PriceProductTotal = 1.23m,
                    PriceDestinationTotal = 0.00m,
                    PriceFinanceCharge = 0.00m,
                    PriceDiscount = 0.00m,
                    PriceDiscountPercent = 0.00m,
                    PriceTaxRate = 1.0m,
                    IsTaxExempt = true,
                    PriceTax = 0.00m,
                    PaymentWriteOff = 0.00m,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                }
            };

            ctx.OrderData.AddRange(orders);
            ctx.SaveChanges();

            try
            {
                var withCondition = ctx.OrderData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionFx.Replace("<<Prices>>", "\"1.23\",\"3.21\""));
                ;

                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == -999));
                Assert.IsTrue(result.Any(x => x.ID == -998));
            }
            finally
            {
                ctx.OrderData.RemoveRange(orders);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void OrderMultipleDescriptionFilter_BoardTest()
        {
            const string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3,
    ""Operator"": 8,
    ""LhsDataType"": 3,
    ""RhsDataType"": 3,
    ""Terms"": [{
        ""$type"": ""AELOperation"",
        ""DataType"": 3,
        ""Operator"": 6,
        ""LhsDataType"": 3,
        ""RhsDataType"": 3,
        ""Terms"": [{
            ""$type"": ""AELOperation"",
            ""DataType"": 3,
            ""Operator"": 14,
            ""LhsDataType"": 1,
            ""RhsDataType"": -1,
            ""Terms"": [{
                ""$type"": ""AELMember"",
                ""DataType"": 1,
                ""Text"": ""Description"",
                ""ID"": 10007,
                ""MemberBase"": {
                    ""$type"": ""AELMember"",
                    ""DataType"": 20010,
                    ""Text"": ""Order"",
                    ""ID"": -1,
                    ""MemberBase"": null
                },
            },
            {
                ""$type"": ""AELValues"",
                ""DataType"": -1,
                ""Values"": [""test order -999"",""test order -998""]
            }]
        }]
    }]
}
";

            var ctx = AELTestHelper.GetMockCtx(bid);

            var location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            var orders = new List<OrderData>
            {
                new OrderData
                {
                    ID = -999,
                    BID = bid,
                    Number = -999,
                    Company = company,
                    Location = location,
                    PickupLocation = location,
                    ProductionLocation = location,
                    Description = "Test Order -999",
                    //TaxExemptReasonID = -99,
                    Origin = origin,
                    OrderPONumber = "PO Number",
                    FormattedNumber = "",
                    CostTotal = 1.23m,
                    TaxGroup = taxGroup,
                    PriceProductTotal = 3.21m,
                    PriceDestinationTotal = 0.00m,
                    PriceFinanceCharge = 0.00m,
                    PriceDiscount = 0.00m,
                    PriceDiscountPercent = 0.00m,
                    PriceTaxRate = 1.0m,
                    IsTaxExempt = true,
                    PriceTax = 0.00m,
                    PaymentWriteOff = 0.00m,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                },
                new OrderData
                {
                    ID = -998,
                    BID = bid,
                    Number = -998,
                    Company = company,
                    Location = location,
                    PickupLocation = location,
                    ProductionLocation = location,
                    Description = "Test Order -998",
                    //TaxExemptReasonID = -99,
                    Origin = origin,
                    OrderPONumber = "PO Number",
                    FormattedNumber = "",
                    CostTotal = 1.23m,
                    TaxGroup = taxGroup,
                    PriceProductTotal = 1.23m,
                    PriceDestinationTotal = 0.00m,
                    PriceFinanceCharge = 0.00m,
                    PriceDiscount = 0.00m,
                    PriceDiscountPercent = 0.00m,
                    PriceTaxRate = 1.0m,
                    IsTaxExempt = true,
                    PriceTax = 0.00m,
                    PaymentWriteOff = 0.00m,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                }
            };

            ctx.OrderData.AddRange(orders);
            ctx.SaveChanges();

            try
            {
                var withCondition = ctx.OrderData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionFx);
                ;

                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == -999));
                Assert.IsTrue(result.Any(x => x.ID == -998));
            }
            finally
            {
                ctx.OrderData.RemoveRange(orders);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void OrderItemDescriptionIsBlankFilter_BoardTest()
        {
            const string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3, // boolean
    ""Operator"": 8, // AND
    ""LhsDataType"": 3, // boolean
    ""RhsDataType"": 3, // boolean
    ""Terms"": [
    {
        ""$type"": ""AELOperation"",
        ""DataType"": 3, // boolean
        ""Operator"": 6, // OR
        ""LhsDataType"": 3, // boolean
        ""RhsDataType"": 3, // boolean
        ""Terms"": [
        {
            ""$type"": ""AELOperation"",
            ""DataType"": 3, // boolean
            ""Operator"": 12, // ISNULL
            ""LhsDataType"": 1, // string
            ""RhsDataType"": 0, // none
            ""Terms"": [
            {
                ""$type"": ""AELMember"",
                ""DataType"": 1, // string
                ""Text"": ""Description"",
                ""ID"": 10028,
                ""MemberBase"": {
                    ""$type"": ""AELMember"",
                    ""DataType"": 20040,
                    ""Text"": ""Line Item"",
                    ""ID"": 0,
                    ""MemberBase"": null
                },
            },
            {
                ""$type"": ""AELValues"",
                ""DataType"": 0,
                ""Values"": [0],
                ""DisplayText"": 0
            }
            ]
        }
        ]
    }
    ]
}
";

            var ctx = AELTestHelper.GetMockCtx(bid);

            var location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            var order = new OrderData
            {
                ID = -999,
                BID = bid,
                Number = -999,
                Company = company,
                Location = location,
                PickupLocation = location,
                ProductionLocation = location,
                Description = "Test Order -999",
                //TaxExemptReasonID = -99,
                Origin = origin,
                OrderPONumber = "PO Number",
                FormattedNumber = "",
                CostTotal = 1.23m,
                TaxGroup = taxGroup,
                PriceProductTotal = 3.21m,
                PriceDestinationTotal = 0.00m,
                PriceFinanceCharge = 0.00m,
                PriceDiscount = 0.00m,
                PriceDiscountPercent = 0.00m,
                PriceTaxRate = 1.0m,
                IsTaxExempt = true,
                PriceTax = 0.00m,
                PaymentWriteOff = 0.00m,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
            };

            order.Items = new List<OrderItemData>
                {
                    new OrderItemData
                    {
                        BID = bid,
                        ID = -999,
                        Name = "Test Item -999",
                        TransactionType = 2,
                        OrderStatusID =  OrderOrderStatus.OrderPreWIP,
                        ItemStatusID = 23,
                        TaxGroup = taxGroup,
                        ProductionLocation = location,
                    },
                    new OrderItemData
                    {
                        BID = bid,
                        ID = -998,
                        Name = "Test Item -998",
                        TransactionType = 2,
                        OrderStatusID =  OrderOrderStatus.OrderPreWIP,
                        ItemStatusID = 23,
                        TaxGroup = taxGroup,
                        ProductionLocation = location,
                        Description = "Test Item -998",
                    }
                };

            ctx.OrderData.Add(order);
            ctx.SaveChanges();

            try
            {
                var withCondition = ctx.OrderItemData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionFx)
                ;

                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == -999));
                Assert.IsFalse(result.Any(x => x.ID == -998));
            }
            finally
            {
                ctx.OrderItemData.RemoveRange(order.Items);
                ctx.OrderData.Remove(order);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void OrderItemSubStatusHasValueFilter_BoardTest()
        {
            const string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3, // boolean
    ""Operator"": 8, // AND
    ""LhsDataType"": 3, // boolean
    ""RhsDataType"": 3, // boolean
    ""Terms"": [{
            ""$type"": ""AELOperation"",
            ""DataType"": 3, // boolean
            ""Operator"": 6, // OR
            ""LhsDataType"": 3, // boolean
            ""RhsDataType"": 3, // boolean
            ""Terms"": [{
                    ""$type"": ""AELOperation"",
                    ""DataType"": 3, // boolean
                    ""Operator"": 13, // ISNOTNULL
                    ""LhsDataType"": 1, // string
                    ""RhsDataType"": 0, // none
                    ""Terms"": [{
                            ""$type"": ""AELMember"",
                            ""DataType"": 10023, // Substatus 
                            ""Text"": ""Sub status"",
                            ""ID"": 20131, // Substatus
                            ""MemberBase"": {
                                ""$type"": ""AELMember"",
                                ""DataType"": 20130, // LineItemStatusCategory
                                ""Text"": ""LineItem"",
                                ""ID"": 10031, // Status
                                ""MemberBase"": {
                                    ""$type"": ""AELMember"",
                                    ""DataType"": 20040, // LineItemCategory
                                    ""Text"": ""Line Item"",
                                    ""ID"": 0, // Status
                                    ""MemberBase"": null
                                },
                            },
                        }, {
                            ""$type"": ""AELValues"",
                            ""DataType"": 0, // none
                            ""Values"": [],
                            ""DisplayText"": 0
                        }
                    ]
                }
            ]
        }
    ]
}
";

            var ctx = AELTestHelper.GetMockCtx(bid);

            var location = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var itemSubStatus = ctx.OrderItemSubStatus.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            var order = new OrderData
            {
                ID = -999,
                BID = bid,
                Number = -999,
                Company = company,
                Location = location,
                PickupLocation = location,
                ProductionLocation = location,
                Description = "Test Order -999",
                //TaxExemptReasonID = -99,
                Origin = origin,
                OrderPONumber = "PO Number",
                FormattedNumber = "",
                CostTotal = 1.23m,
                TaxGroup = taxGroup,
                PriceProductTotal = 3.21m,
                PriceDestinationTotal = 0.00m,
                PriceFinanceCharge = 0.00m,
                PriceDiscount = 0.00m,
                PriceDiscountPercent = 0.00m,
                PriceTaxRate = 1.0m,
                IsTaxExempt = true,
                PriceTax = 0.00m,
                PaymentWriteOff = 0.00m,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
            };

            order.Items = new List<OrderItemData>
                {
                    new OrderItemData
                    {
                        BID = bid,
                        ID = -999,
                        Name = "Test Item -999",
                        TransactionType = 2,
                        OrderStatusID =  OrderOrderStatus.OrderPreWIP,
                        ItemStatusID = 23,
                        SubStatus = itemSubStatus,
                        TaxGroup = taxGroup,
                        ProductionLocation = location,
                    },
                    new OrderItemData
                    {
                        BID = bid,
                        ID = -998,
                        Name = "Test Item -998",
                        TransactionType = 2,
                        OrderStatusID =  OrderOrderStatus.OrderPreWIP,
                        ItemStatusID = 23,
                        TaxGroup = taxGroup,
                        ProductionLocation = location,
                    }
                };

            ctx.OrderData.Add(order);
            ctx.SaveChanges();

            try
            {
                var withCondition = ctx.OrderItemData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionFx)
                ;

                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == -999));
                Assert.IsFalse(result.Any(x => x.ID == -998));
            }
            finally
            {
                ctx.OrderItemData.RemoveRange(order.Items);
                ctx.OrderData.Remove(order);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void OrderItemProductionLocationEqualsFilter_BoardTest()
        {
            string boardConditionFx = @"
{
    ""$type"": ""AELOperation"",
    ""DataType"": 3, // boolean
    ""Operator"": 8, // AND
    ""LhsDataType"": 3, // boolean
    ""RhsDataType"": 3, // boolean
    ""Terms"": [{
            ""$type"": ""AELOperation"",
            ""DataType"": 3, // boolean
            ""Operator"": 6, // OR
            ""LhsDataType"": 3, // boolean
            ""RhsDataType"": 3, // boolean
            ""Terms"": [{
                    ""$type"": ""AELOperation"",
                    ""DataType"": 3, // boolean
                    ""Operator"": 0, // EQ
                    ""LhsDataType"": 1005, // Location
                    ""RhsDataType"": 1005, // Location
                    ""Terms"": [{
                            ""$type"": ""AELMember"",
                            ""DataType"": 1005, // Location 
                            ""Text"": ""Production Location"",
                            ""ID"": 20381, // ProductionLocation
                            ""MemberBase"": {
                                ""$type"": ""AELMember"",
                                ""DataType"": 20380, // OrderItemLocationCategory
                                ""Text"": ""Locations"",
                                ""ID"": 10026, // Locations
                                ""MemberBase"": {
                                    ""$type"": ""AELMember"",
                                    ""DataType"": 20040, // LineItemCategory
                                    ""Text"": ""Line Item"",
                                    ""ID"": 0,
                                    ""MemberBase"": null
                                },
                            },
                        }, {
                            ""$type"": ""AELValues"",
                            ""DataType"": 1005, // Location
                            ""Values"": [ {{locationID}} ],
                            ""DisplayText"": """"
                        }
                    ]
                }
            ]
        }
    ]
}
";

            var ctx = AELTestHelper.GetMockCtx(bid);

            var locationLast = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var locationFirst = ctx.LocationData.FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var itemSubStatus = ctx.OrderItemSubStatus.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            var order = new OrderData
            {
                ID = -999,
                BID = bid,
                Number = -999,
                Company = company,
                Location = locationLast,
                PickupLocation = locationLast,
                ProductionLocation = locationLast,
                Description = "Test Order -999",
                //TaxExemptReasonID = -99,
                Origin = origin,
                OrderPONumber = "PO Number",
                FormattedNumber = "",
                CostTotal = 1.23m,
                TaxGroup = taxGroup,
                PriceProductTotal = 3.21m,
                PriceDestinationTotal = 0.00m,
                PriceFinanceCharge = 0.00m,
                PriceDiscount = 0.00m,
                PriceDiscountPercent = 0.00m,
                PriceTaxRate = 1.0m,
                IsTaxExempt = true,
                PriceTax = 0.00m,
                PaymentWriteOff = 0.00m,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
            };

            order.Items = new List<OrderItemData>
                {
                    new OrderItemData
                    {
                        BID = bid,
                        ID = -999,
                        Name = "Test Item -999",
                        TransactionType = 2,
                        OrderStatusID =  OrderOrderStatus.OrderPreWIP,
                        ItemStatusID = 23,
                        SubStatus = itemSubStatus,
                        TaxGroup = taxGroup,
                        ProductionLocation = locationLast,
                    },
                    new OrderItemData
                    {
                        BID = bid,
                        ID = -998,
                        Name = "Test Item -998",
                        TransactionType = 2,
                        OrderStatusID =  OrderOrderStatus.OrderPreWIP,
                        ItemStatusID = 23,
                        TaxGroup = taxGroup,
                        ProductionLocation = locationFirst,
                    }
                };

            ctx.OrderData.Add(order);
            ctx.SaveChanges();

            try
            {
                boardConditionFx = boardConditionFx.Replace("{{locationID}}", locationLast.ID.ToString());
                var withCondition = ctx.OrderItemData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionFx)
                ;

                var result = withCondition.ToList();

                Assert.IsTrue(result.Any(x => x.ID == -999));
                if (locationFirst.ID != locationLast.ID)
                    Assert.IsFalse(result.Any(x => x.ID == -998));
                else
                    Assert.IsTrue(result.Any(x => x.ID == -998));
            }
            finally
            {
                ctx.OrderItemData.RemoveRange(order.Items);
                ctx.OrderData.Remove(order);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void OrderPaymentTotalGreaterThanZero()
        {
            int testid = -1000;
            var ctx = AELTestHelper.GetMockCtx(bid);
            var foundOrder = ctx.OrderData.Find(bid, testid);
            if (foundOrder != null)
            {
                ctx.OrderData.Remove(foundOrder);
                ctx.SaveChanges();
            }

            var locationLast = ctx.LocationData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var locationFirst = ctx.LocationData.FirstOrDefault(x => x.BID == bid);
            var company = ctx.CompanyData.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var origin = ctx.CrmOrigin.FirstOrDefault(x => x.BID == bid);
            var taxGroup = ctx.TaxGroup.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);
            var itemSubStatus = ctx.OrderItemSubStatus.OrderByDescending(x => x.ID).FirstOrDefault(x => x.BID == bid);

            var order = new OrderData
            {
                ID = testid,
                BID = bid,
                Number = testid,
                Company = company,
                Location = locationLast,
                PickupLocation = locationLast,
                ProductionLocation = locationLast,
                Description = $"Test Order {testid}",
                //TaxExemptReasonID = -99,
                Origin = origin,
                OrderPONumber = "PO Number",
                FormattedNumber = "",
                CostTotal = 1.23m,
                TaxGroup = taxGroup,
                PriceProductTotal = 3.21m,
                PriceDestinationTotal = 0.00m,
                PriceFinanceCharge = 0.00m,
                PriceDiscount = 0.00m,
                PriceDiscountPercent = 0.00m,
                PriceTaxRate = 1.0m,
                IsTaxExempt = true,
                PriceTax = 0.00m,
                PaymentWriteOff = 0.00m,
                PaymentBalanceDue = 60m,
                PaymentTotal = 40m,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                OrderStatusStartDT = DateTime.UtcNow,
            };

            ctx.OrderData.Add(order);
            ctx.SaveChanges();

            var rootOrOp = new AELOperation();
            rootOrOp.DataType = DataType.Boolean;
            rootOrOp.LhsDataType = DataType.Boolean;
            rootOrOp.RhsDataType = DataType.Boolean;
            rootOrOp.Operator = OperatorType.OR;
            rootOrOp.Terms = new List<AELTerm>();

            var rootAndOp = new AELOperation();
            rootAndOp.DataType = DataType.Boolean;
            rootAndOp.LhsDataType = DataType.Boolean;
            rootAndOp.RhsDataType = DataType.Boolean;
            rootAndOp.Operator = OperatorType.AND;
            rootAndOp.Terms = new List<AELTerm>();

            var gtNumberOp = new AELOperation();
            gtNumberOp.DataType = DataType.Boolean;
            gtNumberOp.LhsDataType = DataType.Number;
            gtNumberOp.RhsDataType = DataType.Number;
            gtNumberOp.Operator = OperatorType.GT;
            gtNumberOp.Terms = new List<AELTerm>();

            var numberMemberGetter = new AELMember();
            numberMemberGetter.DataType = DataType.Number;
            numberMemberGetter.ID = 20092;
            numberMemberGetter.Text = "Balance Due";

            var paymentsMemberGetter = new AELMember();
            paymentsMemberGetter.DataType = DataType.PaymentCategory;
            paymentsMemberGetter.Text = "Payments";
            paymentsMemberGetter.ID = 20090;

            numberMemberGetter.MemberBase = paymentsMemberGetter;

            var orderMemberGetter = new AELMember();
            orderMemberGetter.DataType = DataType.OrderCategory;
            orderMemberGetter.Text = "Order";

            paymentsMemberGetter.MemberBase = orderMemberGetter;

            var constZeroMember = new AELValues();
            constZeroMember.DataType = DataType.Number;
            constZeroMember.Values = new List<object>() { 0m };

            gtNumberOp.Terms.Add(numberMemberGetter);
            gtNumberOp.Terms.Add(constZeroMember);

            rootAndOp.Terms.Add(gtNumberOp);

            rootOrOp.Terms.Add(rootAndOp);

            string boardConditionFx = JsonConvert.SerializeObject(rootOrOp, AELOperation.jsonSettings);


            try
            {
                var withCondition = ctx.OrderData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionFx)
                ;

                var result = withCondition.ToList();


                Assert.IsTrue(result.Any(t => t.PaymentBalanceDue > 0));
                Assert.IsFalse(result.Any(t => t.PaymentBalanceDue == 0));
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.ToString());
            }
            finally
            {
                ctx.OrderData.Remove(order);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void CompanyCustomFieldValueFilter_Test()
        {
            var ctx = AELTestHelper.GetMockCtx(bid);

            CustomFieldDefinition customFieldDefinition_string = new CustomFieldDefinition()
            {
                BID = bid,
                ID = -999,
                AppliesToClassTypeID = ClassType.Company.ID(),
                DataType = DataType.String,
                Name = "CustomFieldTest_String",
            };

            CustomFieldDefinition customFieldDefinition_number = new CustomFieldDefinition()
            {
                BID = bid,
                ID = -998,
                AppliesToClassTypeID = ClassType.Company.ID(),
                DataType = DataType.Number,
                Name = "CustomFieldTest_Number",
            };

            CustomFieldDefinition customFieldDefinition_boolean = new CustomFieldDefinition()
            {
                BID = bid,
                ID = -997,
                AppliesToClassTypeID = ClassType.Company.ID(),
                DataType = DataType.Boolean,
                Name = "CustomFieldTest_Boolean",
            };

            CompanyData company = new CompanyData
            {
                BID = bid,
                ID = -999,
                Name = "Test Company",
                LocationID = 1,
                IsAdHoc = true,
                StatusID = 1
            };

            CompanyCustomData companyCustomData = new CompanyCustomData()
            {
                Company = company,
                DataXML = @"
<data>
    <ID>-999</ID>
    <V>String_Value</V>
</data>
<data>
    <ID>-998</ID>
    <V>1</V>
</data>
<data>
    <ID>-997</ID>
    <V>True</V>
</data>"
            };

            try
            {
                ctx.CustomFieldDefinition.Add(customFieldDefinition_string);
                ctx.CustomFieldDefinition.Add(customFieldDefinition_number);
                ctx.CustomFieldDefinition.Add(customFieldDefinition_boolean);
                ctx.CompanyData.Add(company);
                ctx.CompanyCustomData.Add(companyCustomData);
                ctx.SaveChanges();


                // Test Boolean Custom Field
                AELOperation boardConditionForBoolean = new AELOperation()
                {
                    DataType = DataType.Boolean,
                    Operator = OperatorType.EQ,
                    LhsDataType = DataType.Boolean,
                    RhsDataType = DataType.Boolean,
                    Terms = new List<AELTerm>()
                {
                    new AELMember()
                    {
                        ID = 20330,
                        DataType = DataType.Boolean,
                        Text = "CustomFieldTest_Boolean",
                        RelatedID = -997,
                        MemberBase = new AELMember()
                        {
                            DataType = DataType.CompanyCustomField,
                            Text = "Custom Fields"
                        }
                    },
                    new AELValues()
                    {
                        DataType = DataType.Boolean,
                        DisplayText = "true",
                        Values = new List<object>{ true }
                    }
                }
                };

                IQueryable<CompanyData> withCondition = ctx.CompanyData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionForBoolean)
                ;

                List<CompanyData> result = withCondition.ToList();

                Assert.AreEqual(1, result.Count);
                Assert.AreEqual(-999, result[0].ID);


                // Test Number Custom Field
                AELOperation boardConditionForNumber = new AELOperation()
                {
                    DataType = DataType.Boolean,
                    Operator = OperatorType.EQ,
                    LhsDataType = DataType.Number,
                    RhsDataType = DataType.Number,
                    Terms = new List<AELTerm>()
                {
                    new AELMember()
                    {
                        ID = 20330,
                        DataType = DataType.Number,
                        Text = "CustomFieldTest_Number",
                        RelatedID = -998,
                        MemberBase = new AELMember()
                        {
                            DataType = DataType.CompanyCustomField,
                            Text = "Custom Fields"
                        }
                    },
                    new AELValues()
                    {
                        DataType = DataType.Number,
                        DisplayText = "1",
                        Values = new List<object>{ 1 }
                    }
                }
                };

                withCondition = ctx.CompanyData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionForNumber)
                ;

                result = withCondition.ToList();

                Assert.AreEqual(1, result.Count);
                Assert.AreEqual(-999, result[0].ID);

                AELOperation boardConditionForString = new AELOperation()
                {
                    DataType = DataType.Boolean,
                    Operator = OperatorType.EQ,
                    LhsDataType = DataType.String,
                    RhsDataType = DataType.String,
                    Terms = new List<AELTerm>()
                {
                    new AELMember()
                    {
                        ID = 20330,
                        DataType = DataType.String,
                        Text = "CustomFieldTest_String",
                        RelatedID = -999,
                        MemberBase = new AELMember()
                        {
                            DataType = DataType.CompanyCustomField,
                            Text = "Custom Fields"
                        }
                    },
                    new AELValues()
                    {
                        DataType = DataType.String,
                        DisplayText = "String_Value",
                        Values = new List<object>{ "String_Value" }
                    }
                }
                };

                withCondition = ctx.CompanyData
                    .Where(AELTestHelper.DataProvider(bid), boardConditionForString)
                ;

                result = withCondition.ToList();

                Assert.AreEqual(1, result.Count);
                Assert.AreEqual(-999, result[0].ID);
            }
            finally
            {
                ctx.CustomFieldDefinition.RemoveRange(customFieldDefinition_string, customFieldDefinition_number, customFieldDefinition_boolean);
                ctx.CompanyCustomData.Remove(companyCustomData);
                ctx.CompanyData.Remove(company);
                ctx.SaveChanges();
            }
        }
    }
}
