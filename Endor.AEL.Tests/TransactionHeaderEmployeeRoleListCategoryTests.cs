﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class TransactionHeaderEmployeeRoleListCategoryMemberInitializerTests
    {
        [TestMethod]
        public void TestTransactionHeaderEmployeeRoleListCategorySimpleProperties()
        {
            short testBID = 1;

            var order = new OrderData()
            {
                BID = testBID,
                ID = -99
            };
            var employee = new EmployeeData()
            {
                BID = testBID,
                ID = -99,
                First = "TEST",
                Last = "TEST"
            };
            var employeeRole1 = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -99,
                EmployeeID = employee.ID,
                Employee = employee,
                RoleID = SystemIDs.EmployeeRole.AssignedTo,
                OrderID = order.ID
            };
            var employeeRole2 = new OrderEmployeeRole()
            {
                BID = testBID,
                ID = -98,
                EmployeeID = employee.ID,
                Employee = employee,
                RoleID = SystemIDs.EmployeeRole.EnteredBy,
                OrderID = order.ID
            };
            order.EmployeeRoles = new List<OrderEmployeeRole>() { employeeRole1, employeeRole2 };

            var RolesTest = AELTestHelper.GetPropertyResult<TransactionHeaderData, TransactionHeaderData>(testBID, DataType.TransactionHeaderEmployeeRoleListCategory, "Roles", order);
            Assert.AreEqual(RolesTest.ID, order.ID);

            var assignedToTest = AELTestHelper.GetPropertyResult<List<EmployeeData>, TransactionHeaderData>(testBID, DataType.TransactionHeaderEmployeeRoleListCategory, "Assigned To", order);
            Assert.AreEqual(1, assignedToTest.Count);
            Assert.AreEqual(employeeRole1.EmployeeID, assignedToTest[0].ID);

            var enteredByTest = AELTestHelper.GetPropertyResult<List<EmployeeData>, TransactionHeaderData>(testBID, DataType.TransactionHeaderEmployeeRoleListCategory, "Entered By", order);
            Assert.AreEqual(1, enteredByTest.Count);
            Assert.AreEqual(employeeRole2.EmployeeID, enteredByTest[0].ID);
        }

        [TestMethod]
        public void TestTransactionHeaderEmployeeRoleListCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.TransactionHeaderEmployeeRoleListCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderEmployeeRoleList));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.EmployeeCategory));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Roles"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Assigned To"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Entered By"));

        }
    }
}
