﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemContactRoleCategoryTests
    {
        [TestMethod]
        public async Task TestOrderItemContactPrimaryProperty()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);
            var lineItem = ctx.OrderItemData.Include(i => i.Order).FirstOrDefault();
            var testSubject = new OrderContactRole()
            {
                BID = testBID,
                ID = -99,
                OrderItemID = lineItem.ID,
                OrderID = lineItem.Order.ID,
                RoleType = OrderContactRoleType.Primary,
                IsAdHoc = false,
                ContactID = ctx.ContactData.FirstOrDefault().ID,
                IsOrderRole = true,
                IsOrderItemRole = false,
                IsDestinationRole = false,
            };
            try
            {
                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderContactRole.Add(testSubject);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                lineItem = ctx.OrderItemData.Include(i => i.ContactRoles).FirstOrDefault();
                var result = AELTestHelper.GetPropertyResult<ContactData, OrderItemData>(testBID, DataType.OrderItemContactRoleCategory, "Primary", lineItem);
                Assert.AreEqual(testSubject.Contact, result);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task TestOrderItemContactShippingProperty()
        {
            short testBID = 1;
            var ctx = AELTestHelper.GetMockCtx(testBID);
            var lineItem = ctx.OrderItemData.Include(i => i.Order).Include(i => i.ContactRoles).FirstOrDefault();
            var testSubject = new OrderContactRole()
            {
                BID = testBID,
                ID = -99,
                OrderItemID = lineItem.ID,
                OrderID = lineItem.Order.ID,
                RoleType = OrderContactRoleType.ShipTo,
                IsAdHoc = false,
                ContactID = ctx.ContactData.FirstOrDefault().ID,
                IsOrderRole = true,
                IsOrderItemRole = false,
                IsDestinationRole = false,
            };
            try
            {
                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderContactRole.Add(testSubject);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                var result = AELTestHelper.GetPropertyResult<ContactData, OrderItemData>(testBID, DataType.OrderItemContactRoleCategory, "Shipping", lineItem);
                Assert.AreEqual(testSubject.Contact, result);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderContactRole.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();
            }
        }
    }
}
