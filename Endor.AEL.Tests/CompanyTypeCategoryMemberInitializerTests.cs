﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.AEL.MemberInitializers;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class CompanyTypeCategoryMemberInitializerTests
    {
        [TestMethod]
        public void TestCompanyTypeCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.CompanyTypeCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(3, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            


            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Is Prospect"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Is Customer"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Is Vendor"));


        }
    }
}
