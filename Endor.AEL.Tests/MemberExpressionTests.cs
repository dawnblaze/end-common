﻿using Endor.AEL.MemberInitializers;
using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class MemberExpressionTests
    {
        [TestMethod]
        public void TestOrderItem()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var material = ctx.MaterialData.OrderBy(x => x.ID).First(x => x.BID == testBID);
            var labor = ctx.LaborData.OrderBy(x => x.ID).First(x => x.BID == testBID);
            var machine = ctx.MachineData.OrderBy(x => x.ID).First(x => x.BID == testBID);
            var taxGroup = ctx.TaxGroup.OrderBy(x => x.ID).First(x => x.BID == testBID);
            var orderItemStatus = ctx.OrderItemStatus.First(x => x.BID == testBID);
            var orderItemSubStatus = ctx.OrderItemSubStatus.First(x => x.BID == testBID);
            var productionLocation = ctx.LocationData.First(x => x.BID == testBID);

            var order = new OrderData
            {
                ID = -999,
                BID = testBID,
                Number = -999,
            };

            var employees = ctx.EmployeeData.Take(2).Where(x => x.BID == testBID).ToList();

            var orderItem = new OrderItemData
            {
                ID = -999,
                BID = testBID,
                Name = "Test Item 1",
                Order = order,
                Description = "This is a description!",
                TransactionType = (byte)OrderTransactionType.Order,
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                ItemNumber = 1,
                IsOutsourced = false,
                OrderItemStatus = orderItemStatus,
                ItemStatusID = orderItemStatus.ID,
                SubStatus = orderItemSubStatus,
                SubStatusID = orderItemSubStatus.ID,
                ProductionLocation = productionLocation,
                ProductionLocationID = productionLocation.ID,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                TaxGroup = taxGroup,
                TaxGroupOV = false,
                HasProof = false,
                HasDocuments = false,
                HasCustomImage = false,
                Quantity = 5m,
                PricePreTax = 5.5m,
                PriceTaxable = 5.5m,
                PriceDiscount = 0m,
                PriceDiscountPercent = 0m,
                PriceItemDiscount = 0m,
                PriceItemDiscountPercent = 0m,
                PriceTax = 0m,
                PriceTotal = 5.5m,
                CostTotal = 3m,
                PriceUnitPreTax = 1.1m,
                Notes = new List<OrderNote>
                {
                    new OrderNote
                    {
                        Note = "Random Note",
                        NoteType = OrderNoteType.Other,
                    },
                    new OrderNote
                    {
                        Note = "Production Note",
                        NoteType = OrderNoteType.Production,
                    },
                    new OrderNote
                    {
                        Note = "Customer Note",
                        NoteType = OrderNoteType.Customer,
                    },
                },
                EmployeeRoles = new List<OrderEmployeeRole>
                {
                    new OrderEmployeeRole
                    {
                        RoleID = SystemIDs.EmployeeRole.Designer,
                        Employee = employees[0],
                    },
                    new OrderEmployeeRole
                    {
                        RoleID = SystemIDs.EmployeeRole.Designer,
                        Employee = employees[1],
                    },
                    new OrderEmployeeRole
                    {
                        RoleID = SystemIDs.EmployeeRole.CSR,
                        Employee = employees[0],
                    },
                    new OrderEmployeeRole
                    {
                        RoleID = SystemIDs.EmployeeRole.AssignedTo,
                        Employee = employees[0],
                    },
                }
            };

            // Order
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<TransactionHeaderData, OrderItemData>(testBID, DataType.LineItemCategory, "Order", null));
            Assert.AreEqual(order, AELTestHelper.GetPropertyResult<TransactionHeaderData, OrderItemData>(testBID, DataType.LineItemCategory, "Order", orderItem));

            // Employees
//            CollectionAssert.AreEqual(null, AELTestHelper.GetPropertyResult<List<EmployeeData>, OrderItemData>(DataType.LineItemCategory, "Employees", null));
//            CollectionAssert.AreEqual(employees, AELTestHelper.GetPropertyResult<List<EmployeeData>, OrderItemData>(DataType.LineItemCategory, "Employees", orderItem));
//
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Employees", null));
            Assert.AreEqual(orderItem, AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Employees", orderItem));

            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Contacts", null));
            Assert.AreEqual(orderItem, AELTestHelper.GetPropertyResult<OrderItemData, OrderItemData>(testBID, DataType.LineItemCategory, "Contacts", orderItem));

            // Notes
            var notesList = orderItem.Notes.ToList();
            CollectionAssert.AreEqual(null, AELTestHelper.GetPropertyResult<List<OrderNote>, OrderItemData>(testBID, DataType.LineItemCategory, "Notes", null));
            CollectionAssert.AreEqual(notesList, AELTestHelper.GetPropertyResult<List<OrderNote>, OrderItemData>(testBID, DataType.LineItemCategory, "Notes", orderItem));

            // Production Location
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemLocationCategory, "Production Location", null));
            Assert.AreEqual(productionLocation.ID, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemLocationCategory, "Production Location", orderItem));

            // Item Number
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemCategory, "Line Item Number", null));
            Assert.AreEqual(1, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemCategory, "Line Item Number", orderItem));

            // Description
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<string, OrderItemData>(testBID, DataType.LineItemCategory, "Description", null));
            Assert.AreEqual("This is a description!", AELTestHelper.GetPropertyResult<string, OrderItemData>(testBID, DataType.LineItemCategory, "Description", orderItem));

            // Quantity
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemCategory, "Quantity", null));
            Assert.AreEqual(5m, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemCategory, "Quantity", orderItem));

            // Name
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<string, OrderItemData>(testBID, DataType.LineItemCategory, "Name", null));
            Assert.AreEqual("Test Item 1", AELTestHelper.GetPropertyResult<string, OrderItemData>(testBID, DataType.LineItemCategory, "Name", orderItem));

            // Item Status
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemStatusCategory, "Item Status", null));
            Assert.AreEqual(orderItemStatus.ID, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemStatusCategory, "Item Status", orderItem));

            // Substatus
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemStatusCategory, "Sub status", null));
            Assert.AreEqual(orderItemSubStatus.ID, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.LineItemStatusCategory, "Sub status", orderItem));

            // Discount Percent
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Line Item Discount Percent", null));
            Assert.AreEqual(0m, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Line Item Discount Percent", orderItem));

            // Discount Amount
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Line Item Discount Amount", null));
            Assert.AreEqual(0m, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Line Item Discount Amount", orderItem));

            // Price (Pre Tax)
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Price (Pre Tax)", null));
            Assert.AreEqual(5.5m, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Price (Pre Tax)", orderItem));

            // Unit Price (Pre Tax)
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Unit Price (Pre Tax)", null));
            Assert.AreEqual(1.1m, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Unit Price (Pre Tax)", orderItem));

            // Tax Exempt
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Exempt", null));
            Assert.AreEqual(false, AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Exempt", orderItem));

            // Tax Amount
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Amount", null));
            Assert.AreEqual(0.00m, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Amount", orderItem));

            // Price (After Tax)
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Price (After Tax)", null));
            Assert.AreEqual(5.50m, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemPricesCategory, "Price (After Tax)", orderItem));

            // Tax Group
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<TaxGroup, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Group", null));
            Assert.AreEqual(taxGroup, AELTestHelper.GetPropertyResult<TaxGroup, OrderItemData>(testBID, DataType.OrderItemTaxesCategory, "Tax Group", orderItem));

            // Cost
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemCostsCategory, "Total Cost", null));
            Assert.AreEqual(3.00m, AELTestHelper.GetPropertyResult<decimal?, OrderItemData>(testBID, DataType.OrderItemCostsCategory, "Total Cost", orderItem));

            // Has Documents
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Documents", null));
            Assert.AreEqual(false, AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Documents", orderItem));

            // Has Proof
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Proof", null));
            Assert.AreEqual(false, AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Proof", orderItem));

            // Has Custom Image
            Assert.AreEqual(null, AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Custom Image", null));
            Assert.AreEqual(false, AELTestHelper.GetPropertyResult<bool?, OrderItemData>(testBID, DataType.LineItemCategory, "Has Custom Image", orderItem));
            
        }
    }
}
