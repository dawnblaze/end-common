using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Endor.EF;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class MemberTests
    {
        const short testBID = 1;

        [TestMethod]
        public void TestForDuplicateMemberIDs()
        {
            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            List<IMemberInfo> allMembers = instance
                                           .Where(x => x.Value.Members != null)
                                           .SelectMany(x => x.Value.Members).ToList();

            Dictionary<(int, int), List<IMemberInfo>> memberDictionary = new Dictionary<(int, int), List<IMemberInfo>>();

            allMembers.ForEach(
                x =>
                {
                    if (!memberDictionary.ContainsKey((x.ID, x.RelatedID)))
                        memberDictionary.Add((x.ID, x.RelatedID), new List<IMemberInfo>());

                    memberDictionary[(x.ID, x.RelatedID)].Add(x);
                });

            var dupes = memberDictionary.Where(x => x.Value.Count > 1);
            string dupeIDs = string.Join(", ", dupes.Select(x => $"{x.Key.Item1}{(x.Key.Item2 == 0 ? "" : ":" + x.Key.Item2.ToString())}"));

            Assert.AreEqual(0, dupes.Count(), dupeIDs);
        }

        [TestMethod]
        public void TestOrderMemberInitializer()
        {
            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var orderMemberInfo = instance.GetValueOrDefault(DataType.OrderCategory);
            Assert.IsNotNull(orderMemberInfo);
            Assert.AreEqual(19, orderMemberInfo.Members.Count);
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.DestinationCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderContactRoleCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderEmployeeRoleListCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.LocationCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.Origin));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.Number));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.String));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.CompanyCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.OrderStatus));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.OrderDateCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.TaxesCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.PricesCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.CostCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderTagsCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderNotesCategory));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.DataType == DataType.Boolean));

            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Destinations"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Contacts"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Employees"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Locations"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Order Origin"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Order Number"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Description"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Company"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Status"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Dates"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Taxes"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Prices"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Costs"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "PO Number"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Tags"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Notes"));
            Assert.IsNotNull(orderMemberInfo.Members.Find(x => x.Text == "Has Documents"));
        }

        [TestMethod]
        public void TestEstimateMemberInitializer()
        {
            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var estimateMemberInfo = instance.GetValueOrDefault(DataType.EstimateCategory);
            Assert.IsNotNull(estimateMemberInfo);
            Assert.AreEqual(18, estimateMemberInfo.Members.Count);
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.LineItemCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderContactRoleCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderEmployeeRoleListCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.LocationCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.Origin));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.Number));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.String));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.CompanyCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.OrderStatus));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.EstimateDateCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.TaxesCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.PricesCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.CostCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderTagsCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.TransactionHeaderNotesCategory));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.DataType == DataType.Boolean));

            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Line Items"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Contacts"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Employees"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Locations"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Estimate Origin"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Estimate Number"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Description"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Company"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Status"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Dates"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Taxes"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Prices"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Costs"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "PO Number"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Tags"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Notes"));
            Assert.IsNotNull(estimateMemberInfo.Members.Find(x => x.Text == "Has Documents"));
        }
    }
}
