﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Endor.AEL.MemberInitializers;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class CostCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task CostCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var order = new OrderData()
            {
                BID = testBID,
                ID = -99,
                CostTotal = 1m,
                CostMachine = 2m,
                CostMaterial = 3m,
                CostLabor = 4m
            };


            try
            {
                var totalCostTest = AELTestHelper.GetPropertyResult<decimal?, TransactionHeaderData>(testBID, DataType.CostCategory, "Total Cost", order);
                Assert.AreEqual(totalCostTest, 1);

                var MaterialCostTest = AELTestHelper.GetPropertyResult<decimal?, TransactionHeaderData>(testBID, DataType.CostCategory, "Material Cost", order);
                Assert.AreEqual(MaterialCostTest, 3);

                var laborCostTest = AELTestHelper.GetPropertyResult<decimal?, TransactionHeaderData>(testBID, DataType.CostCategory, "Labor Cost", order);
                Assert.AreEqual(laborCostTest, 4);

                var MachineCostTest = AELTestHelper.GetPropertyResult<decimal?, TransactionHeaderData>(testBID, DataType.CostCategory, "Machine Cost", order);
                Assert.AreEqual(MachineCostTest, 2);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderItemTagLink.Where(oi => oi.OrderItemID == -99));
                ctx.RemoveRange(ctx.ListTag.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestCostCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.CostCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(4, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Number));


            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Total Cost"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Material Cost"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Labor Cost"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Machine Cost"));


        }
    }
}
