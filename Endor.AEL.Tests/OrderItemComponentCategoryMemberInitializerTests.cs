﻿using Endor.AEL.Tests.Helper;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.AEL.Tests
{
    [TestClass]
    public class OrderItemComponentCategoryMemberInitializerTests
    {
        [TestMethod]
        public async Task TestOrderItemComponentCategorySimpleProperties()
        {
            short testBID = 1;

            var ctx = AELTestHelper.GetMockCtx(testBID);

            var testOrderItem = new OrderItemData()
            {
                BID = testBID,
                ID = -99,
                ItemNumber = 1,
                Description = "TestDescription",
                Quantity = 2,
                Name = "TestName",
                HasDocuments = true,
                HasProof = true,
                HasCustomImage = true,
                OrderID = ctx.OrderData.FirstOrDefault().ID,
                TransactionType = 2,
                OrderStatusID = OrderOrderStatus.OrderBuilt,
                ItemStatusID = ctx.OrderItemStatus.FirstOrDefault().ID,
                ProductionLocationID = ctx.LocationData.FirstOrDefault().ID,
                TaxGroupID = ctx.TaxGroup.FirstOrDefault().ID,
                IsTaxExempt = false,
                TaxGroupOV = false
            };

            var component2 = new OrderItemComponent()
            {
                BID = testBID,
                ID = -99,
                OrderID = ctx.OrderData.FirstOrDefault().ID,
                Name = "2",
                OrderItemID = -99,
                PriceUnit = 1,
                PriceUnitOV = true,
                TotalQuantity = 2,
                AssemblyQuantity = 2
            };

            try
            {
                ctx.RemoveRange(ctx.OrderItemComponent.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));
                await ctx.SaveChangesAsync();

                ctx.OrderItemData.Add(testOrderItem);
                ctx.OrderItemComponent.Add(component2);
                Assert.AreEqual(2, await ctx.SaveChangesAsync());

                var orderItem = await ctx.OrderItemComponent
                    .Include(oi => oi.Order)
                    .FirstOrDefaultAsync(c => c.BID == testBID && c.ID == component2.ID);

                Assert.IsNotNull(orderItem);

                var nameTest = AELTestHelper.GetPropertyResult<string, OrderItemComponent>(testBID, DataType.OrderItemComponentCategory, "Component Name", orderItem);
                Assert.AreEqual(nameTest, component2.Name);

                var unitPriceTest = AELTestHelper.GetPropertyResult<decimal?, OrderItemComponent>(testBID, DataType.OrderItemComponentCategory, "Unit Price", orderItem);
                Assert.AreEqual(unitPriceTest, 1);

                var priceTest = AELTestHelper.GetPropertyResult<decimal?, OrderItemComponent>(testBID, DataType.OrderItemComponentCategory, "Price", orderItem);
                Assert.AreEqual(priceTest, 2);

                var isOverridenTest = AELTestHelper.GetPropertyResult<bool?, OrderItemComponent>(testBID, DataType.OrderItemComponentCategory, "Is Overridden", orderItem);
                Assert.AreEqual(isOverridenTest, true);

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                    Assert.Fail(ex.InnerException.Message);

                Assert.Fail(ex.Message);
            }
            finally
            {
                ctx = AELTestHelper.GetMockCtx(testBID);

                ctx.RemoveRange(ctx.OrderItemComponent.Where(oi => oi.ID == -99));
                ctx.RemoveRange(ctx.OrderItemData.Where(oi => oi.ID == -99));

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void TestOrderItemComponentCategoryMemberInitializer()
        {
            short testBID = 1;

            var instance = AELTestHelper.GetDictionaryInstance(testBID);
            var memberInfo = instance.GetValueOrDefault(DataType.OrderItemComponentCategory);
            Assert.IsNotNull(memberInfo);
            Assert.AreEqual(6, memberInfo.Members.Count);
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.String));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Boolean));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.DataType == DataType.Number));

            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Component Name"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Quantity"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Unit Price"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Price"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Is Overridden"));
            Assert.IsNotNull(memberInfo.Members.Find(x => x.Text == "Component Type"));
           
        }
    }
}
