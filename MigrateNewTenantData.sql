  alter table [Business.Data] add StorageURL varchar(255) null
  alter table [Business.Data] add IndexStorageConnectionString varchar(512) null
  alter table [Business.Data] add StorageConnectionString varchar(512) null
  
  go
  
  alter table [Business.Data] drop column BusinessDocumentRootURL
  alter table [Business.Data] drop column SharedAssetRootURL
  alter table [Business.Data] drop column SearchIndexRootURL
  
  go
  
  update [Business.Data] set StorageConnectionString = 'UseDevelopmentStorage=true'
  update [Business.Data] set IndexStorageConnectionString = 'UseDevelopmentStorage=true'
  update [Business.Data] set StorageURL = 'http://127.0.0.1:10000/devstoreaccount1/'
  
  go

  alter table [Business.Data] Alter Column StorageURL varchar(255) not null
  alter table [Business.Data] Alter Column IndexStorageConnectionString varchar(512) not null
  alter table [Business.Data] Alter Column StorageConnectionString varchar(512) not null