﻿using Endor.EF;
using Endor.GLEngine.Classes;
using Endor.GLEngine.Models;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.GLEngine.Tests
{
    [TestClass]
    public class CashDrawerTests
    {
        ApiContext context;

        [TestInitialize]
        public void Setup()
        {
            context = GLTestHelper.GetMockCtx(1);
            Cleanup();
        }

        [TestCleanup]
        public void Cleanup()
        {
            GLTestHelper.DeleteTables(context);
        }

        [TestMethod]
        public async Task Test_Cash_Drawer()
        {
            // Setup
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            var lastKnownReconcilation = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var endDate = DateTime.UtcNow.AddDays(1);
            var enteredByID = context.EmployeeData.First(t => t.BID == DBCreationFunctions.BID).ID;

            // create order
            var location = DBCreationFunctions.CreateLocationData(context);

            var ID = -200;

            var results = await glEngine.Reconcile(location.ID, enteredByID, endDate, false, null, (context, bid, val) => {
                var ids = ID--;
                return Task.FromResult(ids);
            });

            await context.SaveChangesAsync();

            var newEndDate = endDate.AddMinutes(1);
            var result1 = await glEngine.GetCashDrawerBalance(location.ID, newEndDate);
            var cashAdjustment = new CashDrawerAdjustment()
            {
                AdjustmentsIn = 3m,
                AdjustmentsOut = 2m,
                DepositTransfer = 1m,
                LocationID = location.ID
            };
            var results2 = await glEngine.Reconcile(location.ID, enteredByID, endDate, false, null, (context, bid, val) => {
                var ids = ID--;
                return Task.FromResult(ids);
            });
            await context.SaveChangesAsync();

            result1 = await glEngine.GetCashDrawerBalance(location.ID, newEndDate);
            Assert.AreEqual(result1.Count, 1);
            var reconciliationResult = await glEngine.GetHistoricalCashDrawerBalance(++ID);
            Assert.AreEqual(reconciliationResult.Count, 1);
        }
    }
}
