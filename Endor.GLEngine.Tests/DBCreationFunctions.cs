﻿using Endor.EF;
using Endor.GLEngine.Models;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Endor.GLEngine.Tests
{
    public static class DBCreationFunctions
    {
        public const byte BID = 1;

        public static LocationData CreateLocationData(ApiContext context)
        {
            byte nextID = 200;
            var IDs = context.LocationData.Where(x => x.BID == BID && x.ID == nextID).ToList();
            if (IDs.Any())
            {
                return IDs.FirstOrDefault();
            }
            else
            {
                var _location = new LocationData()
                {
                    BID = BID,
                    ID = nextID,
                    Name = GLTestHelper.TestLocationName
                };
                context.Add(_location);
                context.SaveChanges();
                return _location;
            }           
        }

        public static OrderItemComponent CreateComponentWithMachineDataInDb(ApiContext context, OrderItemData orderItem, GLAccount incomeAccount)
        {
            short nextID = -99;
            var IDs = context.MachineData.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
                if (IDs.Any())
                {
                    nextID = (short)(IDs.Min() - 1);
                }
                var _machine = new MachineData()
                    {
                        BID = BID,
                        ID = nextID,
                        Name = "Test.Machine"
                    };
                    context.Add(_machine);

            var orderItemComponent = CreateOrderItemComponentInDb(context, orderItem, incomeAccount);
            orderItemComponent.ComponentID = _machine.ID;
            orderItemComponent.ComponentType = OrderItemComponentType.Machine;
            context.SaveChanges();
            return orderItemComponent;
        }

        public static OrderItemComponent CreateComponentWithLaborDataInDb(ApiContext context, OrderItemData orderItem, GLAccount incomeAccount, GLAccount expenseAccount)
        {
            short nextID = -99;
            var IDs = context.LaborData.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
                {
                    nextID = (short)(IDs.Min() - 1);
                }

                var _labor = new LaborData()
                    {
                        BID = BID,
                        ID = nextID,
                        Name = "Test.Labor",
                        InvoiceText = "Test",
                        ExpenseAccountID = expenseAccount.ID,
                        IncomeAccountID = incomeAccount.ID
                    };
                    context.Add(_labor);

            var orderItemComponent = CreateOrderItemComponentInDb(context, orderItem, incomeAccount);
            orderItemComponent.ComponentID = _labor.ID;
            orderItemComponent.ComponentType = OrderItemComponentType.Labor;
            context.SaveChanges();
            return orderItemComponent;
        }

        public static OrderItemComponent CreateComponentWithMaterialDataInDb(ApiContext context, OrderItemData orderItem, GLAccount incomeAccount, GLAccount expenseAccount, GLAccount assetAccount)
        {
            short nextID = -99;
            var IDs = context.MaterialData.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = (short)(IDs.Min() - 1);
            }

            var _material = new MaterialData()
                    {
                        BID = BID,
                        ID = nextID,
                        Name = "Test.Material",
                        InvoiceText = "Test",
                        ExpenseAccountID = expenseAccount.ID,
                        IncomeAccountID = incomeAccount.ID,
                        InventoryAccountID = assetAccount.ID
                    };
                    context.Add(_material);

            var orderItemComponent = CreateOrderItemComponentInDb(context, orderItem, incomeAccount);
            orderItemComponent.ComponentID = _material.ID;
            orderItemComponent.ComponentType = OrderItemComponentType.Material;
            context.SaveChanges();
            return orderItemComponent;
        }

        public static OrderItemSurcharge CreateSurchargeInDb(ApiContext context, OrderItemData orderItem, GLAccount incomeAccount, GLAccount expenseAccount)
        {
            short nextID = -99;
            var IDs = context.SurchargeDef.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = (short)(IDs.Max() + 1);
            }

            var _surcharge = new SurchargeDef()
            {
                BID = BID,
                ID = nextID,
                Name = "Test.Material",
                IncomeAccountID = incomeAccount.ID
            };
            context.Add(_surcharge);
            var nextIDB = -99;
            var IDBs = context.OrderItemSurcharge.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            if (IDBs.Any())
            {
                nextIDB = (short)(IDBs.Max() + 1);
            }
            var orderItemSurcharge = new OrderItemSurcharge()
            {
                BID = BID,
                ID = nextIDB,
                OrderItemID = orderItem.ID,
                SurchargeDefID = nextID,
                PricePreTax = 10m,
                Name = "Test",
                IncomeAccountID = incomeAccount.ID
            };
            context.Add(orderItemSurcharge);
            context.SaveChanges();
            return orderItemSurcharge;
        }
        
        public static OrderItemTaxAssessment CreateTaxAssessmentDataInDb(ApiContext context, OrderData order, OrderItemData orderItem, OrderItemComponent orderItemComponent)
        {
            short nextID = -99;
            var IDs = context.OrderItemTaxAssessment.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = (short)(IDs.Min() - 1);
            }
            OrderItemTaxAssessment orderItemTaxAssessment = new OrderItemTaxAssessment()
            {
                TaxGroupID = context.TaxGroup.Where(x => x.BID == BID).First().ID,
                TaxItemID = context.TaxItem.Where(x => x.BID == BID).First().ID,
                TaxCodeID = context.TaxabilityCodes.Where(x => x.BID == BID).First().ID,
                TaxAmount = 1m,
                OrderItemID = orderItem.ID,
                OrderID = order.ID,
                BID = BID,
                ID = nextID,
                ItemComponentID = orderItemComponent.ID
            };

            context.Add(orderItemTaxAssessment);

            context.SaveChanges();
            return orderItemTaxAssessment;
        }

        public static OrderItemComponent CreateComponentWithAssemblySimpleDataInDb(ApiContext context, OrderItemData orderItem, GLAccount incomeAccount, GLAccount incomeAccount2, GLAccount expenseAccount, GLAccount assetAccount)
        {
            var machine = CreateComponentWithMachineDataInDb(context, orderItem, incomeAccount2);
            var labor = CreateComponentWithLaborDataInDb(context, orderItem, incomeAccount, expenseAccount);
            var material1 = CreateComponentWithMaterialDataInDb(context, orderItem, incomeAccount, expenseAccount, assetAccount);
            var material2 = CreateComponentWithMaterialDataInDb(context, orderItem, incomeAccount2, expenseAccount, assetAccount);

            short nextID = -99;
            var _assembly = context.AssemblyData.FirstOrDefault(t => t.BID == BID && t.ID == nextID);

            _assembly = new AssemblyData()
                    {
                        BID = BID,
                        ID = nextID,
                        Name = "Test.Assembly",                     
                    };
                    context.Add(_assembly);

            var orderItemComponent = CreateOrderItemComponentInDb(context, orderItem, incomeAccount);
            orderItemComponent.ComponentID = _assembly.ID;
            orderItemComponent.ComponentType = OrderItemComponentType.Assembly;
            machine.ParentComponentID = orderItemComponent.ID;
            labor.ParentComponentID = orderItemComponent.ID;
            material1.ParentComponentID = orderItemComponent.ID;
            material2.ParentComponentID = orderItemComponent.ID;
            context.SaveChanges();
            return orderItemComponent;
        }

        public static OrderItemComponent CreateOrderItemComponentInDb(ApiContext context, OrderItemData orderItem, GLAccount incomeAccount)
        {
            int nextID = -99;
            var IDs = context.OrderItemComponent.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }

            var _orderItemComponent = new OrderItemComponent()
                    {
                        BID = BID,
                        ID = nextID,
                        OrderID = orderItem.OrderID,
                        OrderItemID = orderItem.ID,
                        Name = "New Component",
                        TotalQuantity = 1,
                        PriceUnit = CompoundActionTests.OrderProductTotal,
                        CostUnit = CompoundActionTests.OrderProductTotal / 2,
                        IncomeAccountID = incomeAccount.ID,
                        IncomeAllocationType = AssemblyIncomeAllocationType.SingleIncomeAllocation
                    };
                    context.Add(_orderItemComponent);
                    context.SaveChanges();

            return _orderItemComponent;
        }

        public static OrderDestinationData CreateOrderDestinationInDb(ApiContext context, OrderData order)
        {
            int nextID = -99;
            var IDs = context.OrderDestinationData.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }
            OrderDestinationData orderDestination = new OrderDestinationData()
            {
                BID = order.BID,
                ID = nextID,
                OrderID = order.ID,
                Name = "Test Order Destination",
                OrderStatusID = OrderOrderStatus.OrderPreWIP,
                ItemStatusID = context.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                TransactionType = (byte)OrderTransactionType.Order,
                PriceIsLocked = false,
                PriceNetOV = false,
                PriceTaxableOV = false,
                TaxGroupID = order.TaxGroupID,
                TaxGroupOV = false,
                HasDocuments = false,
                PriceNet = CompoundActionTests.OrderDestinationTotal
            };
            context.Add(orderDestination);
            context.SaveChanges();
            return orderDestination;
        }

        public static OrderItemData CreateOrderItemInDb(ApiContext context, OrderData order)
        {
            int nextID = -99;
            var IDs = context.OrderItemData.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }

            var _orderItemData = new OrderItemData()
                    {
                        BID = BID,
                        ID = nextID,
                        OrderID = order.ID,
                        ItemNumber = 1,
                        Quantity = 1,
                        Name = "Test Order Item",
                        IsOutsourced = false,
                        OrderStatusID = OrderOrderStatus.OrderPreWIP,
                        ItemStatusID = context.OrderItemStatus.FirstOrDefault(t => t.BID == BID).ID,
                        TransactionType = (byte)OrderTransactionType.Order,
                        ProductionLocationID = order.LocationID,
                        PriceIsLocked = false,
                        PriceTaxableOV = false,
                        TaxGroupID = order.TaxGroupID,
                        TaxGroupOV = false,
                        HasProof = false,
                        HasDocuments = false,
                        HasCustomImage = false,
                    };
                    context.Add(_orderItemData);
                    context.SaveChanges();

            return _orderItemData;
        }

        public static OrderData CreateOrderInDb(ApiContext context, byte locationId, int CompanyID, bool taxable, short taxGroupID)
        {
            int nextID = -99;
            var IDs = context.OrderData.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }
           
            var _orderData = new OrderData()
                    {
                        BID = BID,
                        ID = nextID,
                        ClassTypeID = (int)ClassType.Order,
                        ModifiedDT = DateTime.UtcNow,
                        LocationID = locationId,
                        PickupLocationID = locationId,
                        ProductionLocationID = locationId,
                        TransactionType = (byte)OrderTransactionType.Order,
                        OrderStatusID = OrderOrderStatus.OrderPreWIP,
                        OrderStatusStartDT = DateTime.UtcNow,
                        Number = 1000,
                        FormattedNumber = "INV-1000",
                        PriceTaxRate = 0.01m,
                        PaymentPaid = 0m,
                        CompanyID = CompanyID,
                        TaxGroupID = taxGroupID,
                        PriceProductTotal = CompoundActionTests.OrderProductTotal,
                        PriceTax = 0
                    };
                    context.Add(_orderData);
                    context.SaveChanges();

            return _orderData;
        }

        public static CreditMemoData SaveOrderAsCreditMemo(ApiContext context, int orderID, CreditMemoData creditMemo)
        {
            OrderData order = context.OrderData.Where(t => t.ID == orderID && t.BID == BID).First();
            order.ClassTypeID = creditMemo.ClassTypeID;
            order.TransactionType = creditMemo.TransactionType;
            order.OrderStatusID = creditMemo.OrderStatusID;
            context.Update(order);
            context.SaveChanges();

            return creditMemo;
        }

        public static OrderItemTaxAssessment CreateOrderItemTaxAssessmentInDb(ApiContext context, OrderData order, TaxItem taxItem, decimal Amount)
        {
            int nextID = -99;
            var IDs = context.OrderItemTaxAssessment.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }
            OrderItemTaxAssessment orderTaxItem = new OrderItemTaxAssessment()
            {
                BID = order.BID,
                ID = nextID,
                OrderID = order.ID,
                TaxAmount = Amount,
                TaxCodeID = context.TaxabilityCodes.First(x => x.BID == BID).ID,
                TaxItemID = taxItem.ID
            };
            context.Add(orderTaxItem);
            order.PriceTax += Amount;
            context.SaveChanges();
            return orderTaxItem;
        }

        public static TaxItem CreateTaxGroupInDb(ApiContext context)
        {
            short taxGroupNextID = -99;
            var _taxGroup = context.TaxGroup.FirstOrDefault(t => t.BID == BID && t.ID == taxGroupNextID);

                        if (_taxGroup == null)
                        {
                            _taxGroup = new TaxGroup()
                            {
                                BID = BID,
                                ID = taxGroupNextID,
                                Name = "Tax Group Name"
                            };
                            context.Add(_taxGroup);
                        }

            var salesTaxPayable = context.GLAccount.First(entry => entry.BID == BID && entry.ID == GLEngine.Sales_Tax_Payable_ID);
            var _taxItem = context.TaxItem.FirstOrDefault(t => t.BID == BID && t.ID == taxGroupNextID);
            if (_taxItem == null)
            {
                _taxItem = new TaxItem
                    {
                        BID = BID,
                        ID = taxGroupNextID,
                        IsActive = true,
                        AccountNumber = "9999",
                        AgencyName = "AgencyName",
                        InvoiceText = "InvoiceText",
                        LookupCode = "LookupCode",
                        GLAccount = salesTaxPayable,
                        GLAccountID = salesTaxPayable.ID,
                        ModifiedDT = DateTime.UtcNow,
                        Name = "tax item name",
                        TaxRate = 2.5m,
                        TaxableSalesCap = 0m,
                    };

                    context.Add(_taxItem);
                    context.SaveChanges();
                }

            return _taxItem;
        }

        public static PaymentMethod CreateCustomPaymentMethodInDb(ApiContext context)
        {
            var nextID = -99;
            var _paymentMethod = context.PaymentMethod.FirstOrDefault(t => t.BID == BID && t.ID == nextID);

            if (_paymentMethod == null)
            {
                _paymentMethod = new PaymentMethod()
                {
                    BID = BID,
                    ID = nextID,
                    Name = "Custom",
                    PaymentMethodType = PaymentMethodType.Custom,
                    DepositGLAccountID = GLEngine.Undeposited_Funds_ID
                };
                context.Add(_paymentMethod);
                context.SaveChanges();
            }

            return _paymentMethod;
        }

        public static PaymentMaster createMasterPaymentTransaction(ApiContext context, byte locationId, int CompanyID, decimal Amount, PaymentTransactionType transactionType = PaymentTransactionType.Payment)
        {
            int nextID = -99;
            var IDs = context.PaymentMaster.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }
            PaymentMaster payment = new PaymentMaster()
            {
                BID = 1,
                ID = nextID,
                LocationID = locationId,
                DisplayNumber = "Test Payment",
                Amount = Amount,
                CurrencyType = (byte)AccountingCurrencyType.USDollar,
                CompanyID = CompanyID,
                PaymentTransactionType = (byte)transactionType
            };
            context.Add(payment);
            context.SaveChanges();
            return payment;
        }

        public static PaymentApplication createApplicationPaymentTransaction(ApiContext context, int? orderId, PaymentMethodType paymentMethodType, byte locationId, int CompanyID, decimal Amount, int masterPaymentId, PaymentTransactionType transactionType = PaymentTransactionType.Payment)
        {
            int nextID = -99;
            var IDs = context.PaymentApplication.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }
            var PaymentMethodId = context.PaymentMethod.First(entry => entry.BID == BID && entry.PaymentMethodType == paymentMethodType).ID;
            PaymentApplication payment = new PaymentApplication()
            {
                BID = 1,
                ID = nextID,
                LocationID = locationId,
                DisplayNumber = "Test Payment",
                Amount = Amount,
                CurrencyType = (byte)AccountingCurrencyType.USDollar,
                PaymentMethodID = PaymentMethodId,
                PaymentType = paymentMethodType,
                CompanyID = CompanyID,
                MasterID = masterPaymentId,
                OrderID = orderId != 0 ? orderId : null,
                PaymentTransactionType = (byte)transactionType,
                RefBalance = 0
            };
            context.Add(payment);
            context.SaveChanges();
            return payment;
        }

        public static PaymentApplication createNonRefundableISCTransaction(ApiContext context, PaymentTransactionType transactionType, byte locationId, int companyId, decimal Amount, int? orderId = null, int masterPaymentTransactionID = 0)
        {
            if (masterPaymentTransactionID == 0)
            {
                //credit gift needs a new master
                PaymentMaster master = createMasterPaymentTransaction(context, locationId, companyId, Amount, transactionType);
                masterPaymentTransactionID = master.ID;
            }
            return createApplicationPaymentTransaction(context, null, PaymentMethodType.NonRefundableCredit, locationId, companyId, Amount, masterPaymentTransactionID, transactionType);
        }

        public static PaymentApplication createRefundTransaction(ApiContext context, System.Nullable<int> orderId, PaymentMethodType paymentMethodType, byte locationId, int CompanyID, decimal Amount, int refundedPaymentID, PaymentTransactionType transactionType = PaymentTransactionType.Refund)
        {
            var masterPaymentID = context.PaymentApplication.First(x => x.BID == BID && x.ID == refundedPaymentID).MasterID;
            var masterPayment = context.PaymentMaster.First(x => x.BID == BID && x.ID == masterPaymentID);

            int nextID = -99;
            var IDs = context.PaymentApplication.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }
            var refBalance = 0m;
            if ((transactionType == PaymentTransactionType.Refund_to_Nonrefundable_Credit) ||
                (transactionType == PaymentTransactionType.Refund_to_Refundable_Credit))
                refBalance = Amount;
            PaymentApplication paymentApplication = new PaymentApplication()
            {
                BID = 1,
                ID = nextID,
                LocationID = locationId,
                DisplayNumber = "Test Refund",
                Amount = Amount,
                PaymentMethodID = (byte)paymentMethodType,
                PaymentType = paymentMethodType,
                CompanyID = CompanyID,
                MasterID = masterPayment.ID,
                OrderID = orderId != 0 ? orderId : null,
                PaymentTransactionType = (byte)transactionType,
                AdjustedApplicationID = refundedPaymentID,
                RefBalance = refBalance
            };

            context.Add(paymentApplication);

            //Adjust Ref Balance
            var originalTransaction = context.PaymentApplication.First(x => x.BID == BID && x.ID == refundedPaymentID);
            originalTransaction.RefBalance -= Amount;
            context.SaveChanges();

            return paymentApplication;
        }

        public static ActivityGlactivity createGLActivity(ApiContext context, GLEntryType entryType)
        {
            int nextID = -99;
            var IDs = context.ActivityGlactivity.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }

            var glActivity = new ActivityGlactivity()
            {
                BID = BID,
                ID = nextID,
                IsActive = true,
                Name = "Test",
                ActivityType = (byte)ActivityType.Accounting_Entry,
                OrderID = null,
                CompanyID = null,
                Subject = "Test",
                CompletedByID = null,
                CompletedByContactID = null,
                CompletedDT = DateTime.UtcNow.AddDays(0),
                GLEntryType = (byte)entryType
            };
            context.ActivityGlactivity.Add(glActivity);
            context.SaveChanges();
            return glActivity;
        }

        public static Reconciliation createEmptyReconciliation(ApiContext context, byte locationID)
        {
            var glActivity = createGLActivity(context, GLEntryType.Reconciliation);
            int nextID = -99;
            var IDs = context.Reconciliation.Where(x => x.BID == BID && x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Min() - 1;
            }
            var result = new Reconciliation()
            {
                BID = BID,
                ID = nextID,
                ClassTypeID = (int)ClassType.Reconciliation,
                LastAccountingDT = DateTime.UtcNow.AddDays(-5010),
                AccountingDT = DateTime.UtcNow.AddDays(-5000),
                StartingGLID = null,
                EndingGLID = null,
                LocationID = locationID,
                IsAdjustmentEntry = false,
                ExportedDT = null,
                WasExported = false,
                EnteredByID = null,
                GLActivityID = glActivity.ID,
                Items = null
            };
            context.Reconciliation.Add(result);

            context.SaveChanges();
            return result;
        }
    }
}
