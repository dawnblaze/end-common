using Endor.EF;
using Endor.GLEngine.Models;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.GLEngine.Tests
{
    [TestClass]
    public class CompoundActionTests
    {
        #region variables
        public static decimal OrderProductTotal = 10m;
        public static decimal OrderDestinationTotal = 2m;
        public static decimal OrderTaxTotal = 1m;
        DateTime currentTime;

        ApiContext context;
        byte[] LocationID = new byte[2];
        int[] companyID = new int[2];
        TaxItem taxItem;
        List<GLAccount> incomeAccounts;

        decimal[] StartingCash = new decimal[2];
        decimal[] calculatedCashTotal = new decimal[2];
        decimal[] checkAmount = new decimal[2];
        decimal[] refundAmount = new decimal[2];
        decimal[] eftAmount = new decimal[2];
        decimal[] visaAmount = new decimal[2];
        decimal[] mcAmount = new decimal[2];
        decimal[] DiscoverAmount = new decimal[2];
        decimal[] amexAmount = new decimal[2];
        decimal[] otherAmount = new decimal[2];
        decimal[] iscAmount = new decimal[2];
        decimal[] badDebtAmount = new decimal[2];
        decimal[] undepositedFundsChange = new decimal[2];
        decimal[] arChange = new decimal[2];
        decimal[] ordersInWIPChange = new decimal[2];
        decimal[] orderInBuiltChange = new decimal[2];
        decimal[] salesTaxChange = new decimal[2];
        decimal[] iscChange = new decimal[2];
        decimal[] depositsChange = new decimal[2];
        decimal[] ordersNotCompleteChange = new decimal[2];
        decimal[] arCurrent = new decimal[2];
        decimal[] incomeChange = new decimal[2];
        decimal[] ordersInWIPCurrent = new decimal[2];
        decimal[] orderInBuiltCurrent = new decimal[2];
        decimal[] iscCurrent = new decimal[2];
        decimal[] depositsCurrent = new decimal[2];
        decimal[] ordersNotCompleteCurrent = new decimal[2];
        decimal[] badDebtExpense = new decimal[2];
        decimal[] iscExpense = new decimal[2];
        decimal[] prCash = new decimal[2];
        decimal[] prCheck = new decimal[2];
        decimal[] prEFT = new decimal[2];
        decimal[] prCC = new decimal[2];
        decimal[] prOther = new decimal[2];
        decimal[] prRefunds = new decimal[2];
        decimal[] prISC = new decimal[2];
        #endregion variables

        #region setup
        [TestInitialize]
        public void Setup()
        {
            context = GLTestHelper.GetMockCtx(1);
            // reconciliationList = new ArrayList();
            //DailyReconcile currentReconciliation = new DailyReconcile();
            currentTime = new DateTime(2000, 1, 1, 12, 00, 00);
            
            GLTestHelper.DeleteTables(context);
            var locationArray = context.LocationData.Select(a => a.ID).ToArray();
            LocationID[0] = locationArray[0];
            LocationID[1] = locationArray[1];
            var companyArray = context.CompanyData.ToArray();
            var companyArrayDifferentLocation = context.CompanyData.Where(entry => entry.LocationID != companyArray[0].LocationID).Select(a => a.ID).ToArray();
            companyID[0] = companyArray[0].ID;
            companyID[1] = companyArrayDifferentLocation[0];
            //FinancialUtils.Instance.DoSetup();
            //revenueRecognition = new List<KeyValuePair<DateTime, decimal>>();

            //clear everything out for checking
            ZeroOutCurrentValues(0);
            ZeroOutCurrentValues(1);
            ZeroOutChangeValues(0);
            ZeroOutChangeValues(1);
            taxItem = DBCreationFunctions.CreateTaxGroupInDb(context);
            DBCreationFunctions.CreateCustomPaymentMethodInDb(context);
            //beginTime = new DateTime[2];
            //beginTime[0] = getTime();
            //beginTime[1] = getTime();
            incomeAccounts = context.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income


        }

        [TestCleanup]
        public void Cleanup()
        {
            GLTestHelper.DeleteTables(context);
        }

        private void ZeroOutCurrentValues(int location)
        {
            arCurrent[location] = 0m;
            ordersInWIPCurrent[location] = 0m;
            orderInBuiltCurrent[location] = 0m;
            iscCurrent[location] = 0m;
            depositsCurrent[location] = 0m;
            ordersNotCompleteCurrent[location] = 0m;

        }

        private void ZeroOutChangeValues(int location)
        {
            StartingCash[location] = 0m;
            calculatedCashTotal[location] = 0m;
            checkAmount[location] = 0m;
            eftAmount[location] = 0m;
            visaAmount[location] = 0m;
            mcAmount[location] = 0m;
            DiscoverAmount[location] = 0m;
            amexAmount[location] = 0m;
            otherAmount[location] = 0m;
            iscAmount[location] = 0m;
            badDebtAmount[location] = 0m;
            undepositedFundsChange[location] = 0m;
            arChange[location] = 0m;
            ordersInWIPChange[location] = 0m;
            orderInBuiltChange[location] = 0m;
            salesTaxChange[location] = 0m;
            iscChange[location] = 0m;
            depositsChange[location] = 0m;
            ordersNotCompleteChange[location] = 0m;
            badDebtExpense[location] = 0m;
            iscExpense[location] = 0m;
            prCash[location] = 0m;
            prCheck[location] = 0m;
            prEFT[location] = 0m;
            prCC[location] = 0m;
            prOther[location] = 0m;
            prRefunds[location] = 0m;
            prISC[location] = 0m;
            badDebtExpense[location] = 0m;
            refundAmount[location] = 0m;
            depositsChange[location] = 0m;
            incomeChange[location] = 0m;
        }

        #endregion teardown

        #region Add/Reduce Credit

        [TestMethod]
        public void Add_In_Store_Credit_same_location()
        {
            //create Transaction
            var payment = createISCTransaction(PaymentTransactionType.Credit_Gift, 0, companyID[0], 100);
            checkGLForPayment(payment.ID);
        }

        [TestMethod]
        public void Reduce_In_Store_Credit_same_location()
        {
            //create Transaction
            var payment = createISCTransaction(PaymentTransactionType.Credit_Gift, 0, companyID[0], 100);
            checkGLForPayment(payment.ID);
            var payment2 = createISCTransaction(PaymentTransactionType.Credit_Adjustment, 0, companyID[0], -100,null,payment.ID);
            checkGLForPayment(payment.ID);
        }

        [TestMethod]
        public void Add_In_Store_Credit_different_location()
        {
            //create Transaction
            var payment = createISCTransaction(PaymentTransactionType.Credit_Gift, 1, companyID[0], 100);
            checkGLForPayment(payment.ID);
        }

        [TestMethod]
        public void Reduce_In_Store_Credit_different_location()
        {
            //create Transaction
            var payment = createISCTransaction(PaymentTransactionType.Credit_Gift, 1, companyID[0], 100);
            checkGLForPayment(payment.ID);
            var payment2 = createISCTransaction(PaymentTransactionType.Credit_Adjustment, 1, companyID[0], -100, null, payment.ID);
            checkGLForPayment(payment.ID);

        }

        #endregion

        #region ISC Payments
        [TestMethod]
        public void Add_In_Store_Credit_same_location_pay_invoice()
        {
            //create Transaction
            var payment = createISCTransaction(PaymentTransactionType.Credit_Gift, 0, companyID[0], 100);
            checkGLForPayment(payment.ID);
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            var payment2 = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.NonRefundableCredit, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal));
            checkGLForPayment(payment2.ID);
        }

        [TestMethod]
        public void Add_In_Store_Credit_different_location_pay_invoice()
        {
            //create Transaction
            var payment = createISCTransaction(PaymentTransactionType.Credit_Gift, 1, companyID[0], 100);
            checkGLForPayment(payment.ID);
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            var payment2 = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.NonRefundableCredit, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal));
            checkGLForPayment(payment2.ID);
        }

        #endregion

        #region Order payments
        [TestMethod]
        public void Order_same_w_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_check_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Check, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_visa_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_mc_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.MasterCard, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_amex_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.AmEx, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_discover_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Discover, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_custom_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Custom, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_baddebt_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Writeoff, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_wire_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.WireTransfer, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        #endregion

        #region Order payments with different location
        [TestMethod]
        public void Order_different_w_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_check_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Check, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_visa_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_mc_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.MasterCard, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_amex_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.AmEx, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_discover_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Discover, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_custom_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Custom, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_baddebt_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Writeoff, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_wire_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.WireTransfer, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
        }

        #endregion

        #region Master Payment
        [TestMethod]
        public void Order_same_w_master_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            OrderData order2 = createOrder(0, companyID[0], false);
            checkGLForOrder(order2.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(order2.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_master_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            OrderData order2 = createOrder(1, companyID[0], false);
            checkGLForOrder(order2.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(order2.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Check, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_master_check_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            OrderData order2 = createOrder(0, companyID[0], false);
            checkGLForOrder(order2.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(order2.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal));
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_master_check_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            OrderData order2 = createOrder(1, companyID[0], false);
            checkGLForOrder(order2.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(order2.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Check, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal));
            checkGLForPayment(master.ID);
        }

        #endregion

        #region Overpayments and applications
        [TestMethod]
        public void Basic_Overpayment_cash_same()
        {
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], 100);
            checkGLForPayment(master.ID);
        }
        [TestMethod]
        public void Basic_Overpayment_check_same()
        {
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Check, 0, companyID[0], 100);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Basic_Overpayment_cc_same()
        {
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 100);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Basic_Overpayment_custom_same()
        {
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Custom, 0, companyID[0], 100);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Basic_Overpayment_different()
        {
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 1, companyID[0], 100);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_master_cash_payment_and_overpayment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], 105);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_master_cash_payment_and_overpayment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 1, companyID[0], 105);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_master_cc_payment_and_overpayment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 105);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_different_w_master_cc_payment_and_overpayment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 1, companyID[0], 105);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_master_cash_payment_and_overpayment_and_apply_overpayment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 105);
            checkGLForPayment(master.ID);
            PaymentApplication overpaymentApplication = createPaymentTransaction(order.ID, PaymentMethodType.RefundableCredit, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), master.ID, false, PaymentTransactionType.Payment_from_Refundable_Credit);
            checkGLForPayment(overpaymentApplication.ID, false);
        }
        #endregion

        #region Payment as order progresses

        [TestMethod]
        public void Progress_Order_with_payments()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            advanceOrder(order);
            checkGLForOrder(order.ID);
            advanceOrder(order);
            checkGLForOrder(order.ID);
            PaymentMaster master2 = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], 5);
            checkGLForPayment(master2.ID);
        }
        #endregion

        #region Refunds on orders
        [TestMethod]
        public void Refund_payment_same_on_completed_order()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            advanceOrder(order);
            checkGLForOrder(order.ID);
            advanceOrder(order);
            checkGLForOrder(order.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Check, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Refund_payment_same_on_WIP_order()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Check, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Refund_payment_different_on_completed_order()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 1, companyID[0], 5);
            checkGLForPayment(master.ID);
            advanceOrder(order);
            checkGLForOrder(order.ID);
            advanceOrder(order);
            checkGLForOrder(order.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Check, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Refund_payment_different_on_WIP_order()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 1, companyID[0], 5);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Check, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Refund_to_isc_on_wip_order()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.RefundableCredit, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID, PaymentTransactionType.Refund_to_Refundable_Credit);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Refund_to_isc_on_completed_order()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            advanceOrder(order);
            checkGLForOrder(order.ID);
            advanceOrder(order);
            checkGLForOrder(order.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.RefundableCredit, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID, PaymentTransactionType.Refund_to_Refundable_Credit);
            checkGLForPayment(master.ID);
        }
        #endregion

        #region Refunds on overpayments

        [TestMethod]
        public void Order_same_w_master_cash_payment_and_overpayment_and_refund_order()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == order.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(refund.MasterID);
        }

        [TestMethod]
        public void Order_different_w_master_cash_payment_and_overpayment_and_refund_order()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 1, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == order.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Cash, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_master_cc_payment_and_overpayment_and_refund_order()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == order.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Visa, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(refund.MasterID);
        }

        [TestMethod]
        public void Order_same_w_master_cc_payment_and_overpayment_and_refund_order_as_cash()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == order.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(refund.MasterID);
        }

        [TestMethod]
        public void Order_different_w_master_cc_payment_and_overpayment_and_refund_order()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 1, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == order.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Check, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_master_cash_payment_and_overpayment_and_refund_overpayment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == null).First();
            PaymentApplication refund = createRefundTransaction(0, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(refund.MasterID);
        }

        [TestMethod]
        public void Order_different_w_master_cash_payment_and_overpayment_and_refund_overpayment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 1, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == null).First();
            PaymentApplication refund = createRefundTransaction(0, PaymentMethodType.Check, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        [TestMethod]
        public void Order_same_w_master_cc_payment_and_overpayment_and_refund_overpayment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == null).First();
            PaymentApplication refund = createRefundTransaction(0, PaymentMethodType.Visa, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(refund.MasterID);
        }

        [TestMethod]
        public void Order_different_w_master_cc_payment_and_overpayment_and_refund_overpayment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            paymentDict.Add(0, 100);
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 1, companyID[0], 105);
            checkGLForPayment(master.ID);
            //process refund
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID && p.OrderID == null).First();
            PaymentApplication refund = createRefundTransaction(0, PaymentMethodType.Check, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        #endregion

        #region Edit Order with payments
        [TestMethod]
        public void Edit_order_up_same_w_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            //edit order
            editOrder(order, 20, 0);
            checkGLForOrder(order.ID);
        }

        [TestMethod]
        public void Edit_order_down_same_w_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            //edit order
            editOrder(order, -20, 0);
            checkGLForOrder(order.ID);

        }

        [TestMethod]
        public void Edit_order_down_same_w_edit_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            //edit order
            editOrder(order, -80, 0);
            checkGLForOrder(order.ID);
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        #endregion

        #region Voiding Orders
        [TestMethod]
        public void  Void_order_same_w_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 0, companyID[0], 5);
            checkGLForPayment(master.ID);
            //void order
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
            voidOrder(order, 0);
            checkGLForOrder(order.ID);

        }

        [TestMethod]
        public void Void_order_different_w_cash_payment()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Visa, 1, companyID[0], 5);
            checkGLForPayment(master.ID);
            //void order
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Cash, 1, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
            voidOrder(order, 1);
            checkGLForOrder(order.ID);
        }

        #endregion

        #region Normal Orders
        [TestMethod]
        public void Order_WIP_same_location_as_account()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
        }


        [TestMethod]
        public void Order_Built_same_location_as_account()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            advanceOrder(order);
            checkGLForOrder(order.ID);
        }


        [TestMethod]
        public void Order_Complete_same_location_as_account()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            advanceOrder(order);
            advanceOrder(order);
            checkGLForOrder(order.ID);
        }

        [TestMethod]
        public void Order_WIP_other_location_as_account()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[1], false);
            checkGLForOrder(order.ID);
        }

        [TestMethod]
        public void Order_Built_other_location_as_account()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[1], false);
            advanceOrder(order);
            checkGLForOrder(order.ID);
        }

        [TestMethod]
        public void Order_Complete_other_location_as_account()
        {
            //create Order 
            OrderData order = createOrder(1, companyID[1], false);
            advanceOrder(order);
            advanceOrder(order);
            checkGLForOrder(order.ID);
        }

        #endregion

        #region Taxes
        [TestMethod]
        public void Order_with_taxes()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], true);
            checkGLForOrder(order.ID);
            advanceOrder(order);
            advanceOrder(order);
            checkGLForOrder(order.ID);
        }

        #endregion
        
        #region NSF
        [TestMethod]
        public void Order_with_nsf()
        {
            //create Order 
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(CompoundActionTests.OrderProductTotal / 2));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Check, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2));
            checkGLForPayment(master.ID);
            PaymentApplication paymentToRefund = context.PaymentApplication.Where(p => p.MasterID == master.ID).First();
            PaymentApplication refund = createRefundTransaction(order.ID, PaymentMethodType.Check, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal / 2), paymentToRefund.ID);
            checkGLForPayment(master.ID);
        }

        #endregion 

        #region Error Cases
        [TestMethod]
        public void TestInCorrectOrderId()
        {
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            try
            {
                glEngine.ComputeGLDiff(EnumGLType.Order, -999);
                Assert.IsTrue(false); //should never be exercised
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message.Contains("could not be found"));
            }

        }

        #endregion

        #region create functions
        private PaymentApplication createISCTransaction(PaymentTransactionType TransactionTypeID, int locationIndex, int companyId, decimal Amount, int? orderId = null, int creditGiftPaymentID = 0)
        {
            PaymentApplication payment = null;
            payment = DBCreationFunctions.createNonRefundableISCTransaction(context, TransactionTypeID, this.LocationID[locationIndex], companyId, Amount, orderId,creditGiftPaymentID);

            if ((TransactionTypeID == PaymentTransactionType.Credit_Gift)
                || (TransactionTypeID == PaymentTransactionType.Credit_Adjustment))
            {
                iscExpense[locationIndex] += Amount;
                iscAmount[locationIndex] += Amount;
            }
            else if (orderId != null)
            {
                iscAmount[locationIndex] -= Amount;
                prISC[locationIndex] += Amount;
                createOrderPayment((int)orderId, Amount, locationIndex);
            }

            if ((TransactionTypeID == PaymentTransactionType.Payment))
            {
                iscChange[locationIndex] -= Amount;
                iscCurrent[locationIndex] -= Amount;
            }
            else
            {
                iscChange[locationIndex] += Amount;
                iscCurrent[locationIndex] += Amount;
            }
            return payment;
        }

        private OrderData createOrder(byte locationIndex, int CompanyID, bool taxable)
        {
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, this.LocationID[locationIndex], CompanyID, taxable, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMachineDataInDb(context, orderItemData, incomeAccounts[0]);
            if (taxable)
            {
                OrderItemTaxAssessment taxAssessment = DBCreationFunctions.CreateOrderItemTaxAssessmentInDb(context, order, taxItem, OrderTaxTotal);
            }
            //Order total is 10
            decimal subTotal = CompoundActionTests.OrderProductTotal;
            //update values for checking
            ordersInWIPCurrent[locationIndex] += subTotal;
            ordersInWIPChange[locationIndex] += subTotal;
            ordersNotCompleteCurrent[locationIndex] += subTotal;
            ordersNotCompleteChange[locationIndex] += subTotal;
            return order;
        }

        private PaymentApplication createRefundTransaction(int orderId, PaymentMethodType paymentMethodType, int locationIndex, int accountId, decimal Amount, int paymentId, PaymentTransactionType transactionType = PaymentTransactionType.Refund)
        {
            //payment Id is used for refunds of overpayments
            PaymentApplication refund = DBCreationFunctions.createRefundTransaction(context, orderId, paymentMethodType, this.LocationID[locationIndex], accountId, Amount, paymentId, transactionType);
            if (orderId != 0)
            {
                createOrderPayment((int)orderId, -Amount, locationIndex);
            } else
            {
                iscAmount[locationIndex] -= Amount;
                iscChange[locationIndex] -= Amount;
                iscCurrent[locationIndex] -= Amount;
            }
            switch (paymentMethodType)
            {
                case PaymentMethodType.Cash:
                    {
                        calculatedCashTotal[locationIndex] += Amount;
                        break;
                    }
                case PaymentMethodType.RefundableCredit:
                    {
                        //refunds to ISC
                        prISC[locationIndex] -= Amount;
                        iscAmount[locationIndex] += Amount;
                        iscChange[locationIndex] += Amount;
                        iscCurrent[locationIndex] += Amount;
                        break;
                    }
            }
            if (paymentMethodType != PaymentMethodType.RefundableCredit) 
            {
                undepositedFundsChange[locationIndex] -= Amount;
                prRefunds[locationIndex] += Amount;
                refundAmount[locationIndex] += -Amount;
            }
            return refund;

        }

        private void advanceOrder(OrderData order)
        {
            if ((order.OrderStatusID == OrderOrderStatus.OrderPreWIP)
                || (order.OrderStatusID == OrderOrderStatus.OrderWIP))
            {
                //update order table
                order.OrderStatusID = OrderOrderStatus.OrderBuilt;
                context.SaveChanges();
                //update values for checking
                int location = Array.IndexOf(LocationID, order.LocationID);
                ordersInWIPCurrent[location] -= (decimal)(order.PriceTotal - order.PriceTax);
                ordersInWIPChange[location] -= (decimal)(order.PriceTotal - order.PriceTax);
                orderInBuiltCurrent[location] += (decimal)(order.PriceTotal - order.PriceTax);
                orderInBuiltChange[location] += (decimal)(order.PriceTotal - order.PriceTax);
            }
            else if ((order.OrderStatusID == OrderOrderStatus.OrderBuilt)
                || (order.OrderStatusID == OrderOrderStatus.OrderInvoicing))
            {
                //update order table
                order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
                context.SaveChanges();
                //update values for checking
                int location = Array.IndexOf(LocationID, order.LocationID);
                orderInBuiltCurrent[location] -= (decimal)(order.PriceTotal - order.PriceTax);
                orderInBuiltChange[location] -= (decimal)(order.PriceTotal - order.PriceTax);
                ordersNotCompleteCurrent[location] -= (decimal)(order.PriceTotal - order.PriceTax);
                ordersNotCompleteChange[location] -= (decimal)(order.PriceTotal - order.PriceTax);
                incomeChange[location] += (decimal)(order.PriceTotal - order.PriceTax);
                arChange[location] += (decimal)(order.PriceTotal);
                arCurrent[location] += (decimal)(order.PriceTotal);
                salesTaxChange[location] += (decimal)(order.PriceTax);

                //update payments
                foreach (PaymentApplication trans in context.PaymentApplication.Where(p => p.OrderID == order.ID))
                {
                    var tmpAmount = trans.Amount;
                    if (trans.PaymentTransactionType == (byte)PaymentTransactionType.Payment)
                    {
                        depositsChange[location] -= trans.Amount;
                        depositsCurrent[location] -= trans.Amount;
                        arChange[location] -= trans.Amount;
                        arCurrent[location] -= trans.Amount;
                    }
                }
            }
        }

        private PaymentMaster createMasterPaymentTransaction(Dictionary<int, decimal> subPaymentInformation, PaymentMethodType paymentMethodType, byte locationIndex, int CompanyID, decimal Amount, bool overrideSourcedFromOverpayment = false, PaymentTransactionType transactionType = PaymentTransactionType.Payment)
        {
            byte locationId = this.LocationID[locationIndex];
            PaymentMaster payment = DBCreationFunctions.createMasterPaymentTransaction(context, locationId, CompanyID, Amount, transactionType);
            try
            {
                if ((paymentMethodType != PaymentMethodType.NonRefundableCredit) &&
                    (paymentMethodType != PaymentMethodType.Writeoff))
                {
                    undepositedFundsChange[locationIndex] += Amount;
                }
                foreach (KeyValuePair<int, decimal> item in subPaymentInformation)
                {
                    createPaymentTransaction(item.Key, paymentMethodType, locationIndex, CompanyID, item.Value, payment.ID, overrideSourcedFromOverpayment, transactionType);
                }
                switch (paymentMethodType)
                {
                    case PaymentMethodType.Cash:
                        {
                            prCash[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.Check:
                        {
                            prCheck[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.Visa:
                        {
                            prCC[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.MasterCard:
                        {
                            prCC[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.Discover:
                        {
                            prCC[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.AmEx:
                        {
                            prCC[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.Custom:
                        {
                            prOther[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.WireTransfer:
                        {
                            prEFT[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.Writeoff:
                        {
                            break;
                        }
                }
                return payment;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private PaymentApplication createPaymentTransaction(int? orderId, PaymentMethodType paymentMethodType, int locationIndex, int accountId, decimal Amount, int masterPaymentID = 0, bool overrideSourcedFromOverpayment = false, PaymentTransactionType transactionType = PaymentTransactionType.Payment)
        {
            byte locationId = this.LocationID[locationIndex];
            PaymentApplication trans = DBCreationFunctions.createApplicationPaymentTransaction(context, orderId, paymentMethodType, locationId, accountId, Amount, masterPaymentID, transactionType);
            try
            {
                /*if ((masterPayment == 0) 
                    && ((CbTransactionTypeEnum)TransactionTypeId != CbTransactionTypeEnum.BadDebt) 
                    && ((CbTransactionTypeEnum)TransactionTypeId != CbTransactionTypeEnum.CustomerCredit))
                {
                    undepositedFundsChange[locationIndex] += Amount;
                }*/
                if (orderId != 0)
                {
                    //depositsChange[location] += Amount;
                    //depositsCurrent[location] += Amount;
                    createOrderPayment((int)orderId, Amount, locationIndex);
                }
                else
                {
                    iscAmount[locationIndex] += Amount;
                    iscChange[locationIndex] += Amount;
                    iscCurrent[locationIndex] += Amount;
                    //FinancialUtils.Instance.createAccountInStoreCreditEntry(trans);
                }
                switch (paymentMethodType)
                {
                    case PaymentMethodType.Cash:
                        {
                            calculatedCashTotal[locationIndex] += Amount;
                            if (masterPaymentID == 0) { prCash[locationIndex] += Amount; }
                            break;
                        }
                    case PaymentMethodType.Check:
                        {
                            checkAmount[locationIndex] += Amount;
                            if (masterPaymentID == 0) { prCheck[locationIndex] += Amount; }
                            break;
                        }
                    case PaymentMethodType.Visa:
                        {
                            visaAmount[locationIndex] += Amount;
                            if (masterPaymentID == 0) { prCC[locationIndex] += Amount; }
                            break;
                        }
                    case PaymentMethodType.MasterCard:
                        {
                            mcAmount[locationIndex] += Amount;
                            if (masterPaymentID == 0) { prCC[locationIndex] += Amount; }
                            break;
                        }
                    case PaymentMethodType.Discover:
                        {
                            DiscoverAmount[locationIndex] += Amount;
                            if (masterPaymentID == 0) { prCC[locationIndex] += Amount; }
                            break;
                        }
                    case PaymentMethodType.AmEx:
                        {
                            amexAmount[locationIndex] += Amount;
                            if (masterPaymentID == 0) { prCC[locationIndex] += Amount; }
                            break;
                        }
                    case PaymentMethodType.Custom:
                        {
                            otherAmount[locationIndex] += Amount;
                            if (masterPaymentID == 0) { prOther[locationIndex] += Amount; }
                            break;
                        }
                    case PaymentMethodType.WireTransfer:
                        {
                            eftAmount[locationIndex] += Amount;
                            if (masterPaymentID == 0) { prEFT[locationIndex] += Amount; }
                            break;
                        }
                    case PaymentMethodType.Writeoff:
                        {
                            badDebtAmount[locationIndex] += Amount;
                            badDebtExpense[locationIndex] += Amount;
                            break;
                        }
                    case PaymentMethodType.NonRefundableCredit:
                    case PaymentMethodType.RefundableCredit:
                        {
                            if (orderId != 0)
                            {
                                iscAmount[locationIndex] -= Amount;
                                iscCurrent[locationIndex] -= Amount;
                                iscChange[locationIndex] -= Amount;
                                prISC[locationIndex] += Amount;
                                //fix the origional overpayment transaction
                                //update payments
                                /*foreach (Transaction tmpTrans in FinancialUtils.Instance.transInfo.ToArray())
                                {
                                    if ((tmpTrans.MasterTransactionId == masterPayment) && (tmpTrans.OrderId == null))
                                    {
                                        tmpTrans.Amount -= Amount;
                                        FinancialUtils.Instance.SubmitChanges();
                                    }
                                }*/
                            }
                            break;
                        }
                }
                return trans;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private OrderData createOrderPayment(int orderId, decimal amount, int locationIndex)
        {
            var order = context.OrderData.Where(o => o.ID == orderId).First();
            if (order.OrderStatusID == OrderOrderStatus.OrderClosed ||
                order.OrderStatusID == OrderOrderStatus.OrderInvoiced)
            {
                arChange[locationIndex] -= amount;
                arCurrent[locationIndex] -= amount;
            }
            else
            {
                depositsChange[locationIndex] += amount;
                depositsCurrent[locationIndex] += amount;
            }
            return order;
            /*foreach (Order order in FinancialUtils.Instance.orderInfo)
            {
                if (order.Id == orderId)
                {
                    order.BalanceDue -= amount;
                    int location = FinancialUtils.Instance.getLocationIndex(order.Location.Id);

                    if (order.OrderStatusId == (int)FinancialUtils.OrderState.Completed)
                    {
                        page2_arChange[location] -= amount;
                        page2_arCurrent[location] -= amount;
                        //page2_depositsChange[location] -= amount;
                        //page2_depositsCurrent[location] -= amount;
                    }
                    else
                    {
                        page2_depositsChange[location] += amount;
                        page2_depositsCurrent[location] += amount;
                    }
                    try
                    {
                        FinancialUtils.Instance.SubmitChanges();
                        return order;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }*/
        }

        private void editOrder(OrderData order, decimal changePercentage, int locationIndex)
        {
            //decimal startTotalAmount = (decimal)order.TotalPrice;
            //decimal startBalanceDue = (decimal)order.BalanceDue;
            //decimal startSubTotalAmount = (decimal)order.TotalPrice - (decimal)order.TaxesPrice;
            decimal adjustmentAmount = 0m;
            foreach (OrderItemComponent component in context.OrderItemComponent.Where(oi => oi.OrderID == order.ID).ToList())
            {
                decimal tmpAdjustmentamount = component.PriceUnit.Value * (1 + changePercentage);
                adjustmentAmount += tmpAdjustmentamount;
                component.PriceUnit = component.PriceUnit + tmpAdjustmentamount;
            }
            context.SaveChanges();
            if ((order.OrderStatusID == OrderOrderStatus.OrderPreWIP)
                || (order.OrderStatusID == OrderOrderStatus.OrderWIP))
            {
                //update values for checking
                ordersInWIPCurrent[locationIndex] += adjustmentAmount;
                ordersInWIPChange[locationIndex] += adjustmentAmount;
                ordersNotCompleteCurrent[locationIndex] += adjustmentAmount;
                ordersNotCompleteChange[locationIndex] += adjustmentAmount;
            }
            else if ((order.OrderStatusID == OrderOrderStatus.OrderBuilt)
                || (order.OrderStatusID == OrderOrderStatus.OrderInvoicing))
            {
                //update values for checking
                orderInBuiltCurrent[locationIndex] += adjustmentAmount;
                orderInBuiltChange[locationIndex] += adjustmentAmount;
                ordersNotCompleteCurrent[locationIndex] += adjustmentAmount;
                ordersNotCompleteChange[locationIndex] += adjustmentAmount;
            }
        }

        private void voidOrder(OrderData order, int locationIndex)
        {
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            var incomeEntries = glEngine.GetGLIncomeForOrder(order);
            var subTotal = incomeEntries.Select(entry => entry.Amount).Sum();

            switch (order.OrderStatusID)
            {
                case OrderOrderStatus.OrderWIP:
                case OrderOrderStatus.OrderPreWIP:
                    //update values for checking
                    ordersInWIPCurrent[locationIndex] += subTotal;
                    ordersInWIPChange[locationIndex] += subTotal;
                    ordersNotCompleteCurrent[locationIndex] += subTotal;
                    ordersNotCompleteChange[locationIndex] += subTotal;
                    break;
                case OrderOrderStatus.OrderBuilt:
                case OrderOrderStatus.OrderInvoicing:
                    //update values for checking
                    orderInBuiltChange[locationIndex] += subTotal;
                    orderInBuiltCurrent[locationIndex] += subTotal;
                    ordersNotCompleteCurrent[locationIndex] += subTotal;
                    ordersNotCompleteChange[locationIndex] += subTotal;
                    break;
                case OrderOrderStatus.OrderInvoiced:
                case OrderOrderStatus.OrderClosed:
                    arChange[locationIndex] -= subTotal;
                    arChange[locationIndex] -= subTotal;
                    incomeChange[locationIndex] -= subTotal;
                    break;
            }
            order.OrderStatusID = OrderOrderStatus.OrderVoided;
            context.SaveChanges();

            
        }

        #endregion

        #region
        [TestMethod]
        public async Task Reconciliation_Generate_Preview()
        {
            // Setup
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            var lastKnownReconcilation = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var endDate = DateTime.UtcNow.AddDays(1);
            var enteredByID = context.EmployeeData.First(t => t.BID == DBCreationFunctions.BID).ID;
            var activityIDs = context.ActivityGlactivity.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            var nextActivityID = activityIDs.Any() ? activityIDs.Max() + 1 : -99;


            // create order
            var location = DBCreationFunctions.CreateLocationData(context);
            LocationID[0] = location.ID;
            var lastReconciliation = context.Reconciliation.FirstOrDefault(t => t.BID == DBCreationFunctions.BID && t.LocationID == location.ID);
            if (lastReconciliation != null)
            {
                lastKnownReconcilation = lastReconciliation.AccountingDT;
            }
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);
            var results = await glEngine.GenerateReconciliationPreview(order.LocationID, enteredByID, endDate);

            foreach (var result in results)
            {
                Assert.AreEqual(DBCreationFunctions.BID, result.BID);
                Assert.AreEqual(0, result.ID);
                Assert.AreEqual(lastKnownReconcilation, result.LastAccountingDT);
                Assert.IsNotNull(result.AccountingDT);
                Assert.AreEqual(false, result.IsAdjustmentEntry);
                Assert.AreEqual(order.LocationID, result.LocationID);
                Assert.AreEqual(0, result.GLActivityID);
                Assert.AreEqual(-99, result.StartingGLID);
                Assert.AreEqual(-98, result.EndingGLID);
                Assert.AreEqual(enteredByID, result.EnteredByID);
                Assert.AreEqual(false, result.WasExported);
                Assert.AreEqual(2, result.Items.Count);

                // verify GLSummaryItem items
                foreach (var summary in result.Items)
                {
                    Assert.AreEqual(0, summary.ReconciliationID);
                    Assert.IsNotNull(summary.GLAccountID);
                    Assert.IsNull(summary.PaymentMasterCount);
                    Assert.IsNull(summary.PaymentApplicationCount);
                    Assert.IsNotNull(summary.Amount);
                    Assert.IsNotNull(summary.Balance);
                    Assert.IsNull(summary.CurrencyType);
                    Assert.IsFalse(summary.IsPaymentSummary);
                }
            }

            // pay order
            Dictionary<int, decimal> paymentDict = new Dictionary<int, decimal>();
            paymentDict.Add(order.ID, (decimal)(10m));
            PaymentMaster master = this.createMasterPaymentTransaction(paymentDict, PaymentMethodType.Cash, 0, companyID[0], (decimal)(CompoundActionTests.OrderProductTotal));
            checkGLForPayment(master.ID);

            results = (await glEngine.GenerateReconciliationPreview(order.LocationID, enteredByID, endDate));

            foreach (var result in results)
            {
                Assert.AreEqual(4, result.Items.Count);

                // verify GLSummaryItem items
                foreach (var summary in result.Items.Where(t => t.IsPaymentSummary))
                {
                    Assert.AreEqual(0, summary.ReconciliationID);
                    Assert.IsNull(summary.GLAccountID);
                    Assert.AreEqual((int)PaymentMethodType.Cash, summary.PaymentMethodID);
                    Assert.AreEqual((short)1, summary.PaymentMasterCount);
                    Assert.AreEqual((short)1, summary.PaymentApplicationCount);
                    Assert.AreEqual(10m, summary.Amount);
                    Assert.IsNull(summary.Balance);
                    Assert.IsNull(summary.CurrencyType);
                    Assert.IsTrue(summary.IsPaymentSummary);
                }
            }
        }

        [TestMethod]
        public async Task Reconciliation_Reconcile()
        {
            // Setup
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            var lastKnownReconcilation = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var endDate = DateTime.UtcNow.AddDays(1);
            var enteredByID = context.EmployeeData.First(t => t.BID == DBCreationFunctions.BID).ID;

            // create order
            var location = DBCreationFunctions.CreateLocationData(context);
            LocationID[0] = location.ID;
            OrderData order = createOrder(0, companyID[0], false);
            advanceOrder(order);
            advanceOrder(order);
            checkGLForOrder(order.ID);

            var lastReconciliation = context.Reconciliation.Where(t => t.BID == DBCreationFunctions.BID && t.LocationID == order.LocationID).OrderByDescending(a => a.ID).FirstOrDefault();
            if (lastReconciliation != null)
            {
                lastKnownReconcilation = lastReconciliation.AccountingDT;
            }

            var ID = -200;

            var results = await glEngine.Reconcile(order.LocationID, enteredByID, endDate, false, null, (context, bid, val) => {
                var ids = ID--;
                return Task.FromResult(ids); 
            });

            await context.SaveChangesAsync();

            foreach (var result in results)
            {
                Assert.AreEqual(DBCreationFunctions.BID, result.BID);
                Assert.IsTrue(result.ID == -201);
                Assert.AreEqual(lastKnownReconcilation, result.LastAccountingDT);
                Assert.IsNotNull(result.AccountingDT);
                Assert.AreEqual(false, result.IsAdjustmentEntry);
                Assert.AreEqual(order.LocationID, result.LocationID);
                Assert.AreEqual(-99, result.StartingGLID);
                Assert.AreEqual(-98, result.EndingGLID);
                Assert.AreEqual(enteredByID, result.EnteredByID);
                Assert.AreEqual(false, result.WasExported);
                Assert.AreEqual(2, result.Items.Count);
                Assert.AreEqual(10, result.TotalIncome);
                Assert.AreEqual(0, result.TotalPayments);

                var locationAbbrev = context.LocationData.Where(x => x.BID == result.BID && x.ID == result.LocationID).FirstOrDefault().Abbreviation;
                Assert.AreEqual(locationAbbrev+'-'+result.ID.ToString(), result.FormattedNumber);
                // verify GLSummaryItem items
                foreach (var summary in result.Items)
                {
                    Assert.AreEqual(-201, summary.ReconciliationID);
                    Assert.IsNotNull(summary.GLAccountID);
                    Assert.IsNull(summary.PaymentMasterCount);
                    Assert.IsNull(summary.PaymentApplicationCount);
                    Assert.IsNotNull(summary.Amount);
                    Assert.IsNotNull(summary.Balance);
                    Assert.IsNull(summary.CurrencyType);
                    //Assert.IsFalse(summary.IsPaymentSummary);
                }
            }
        }

        [TestMethod]
        public async Task Reconciliation_ReconcileWithAdjustments()
        {
            // Setup
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            var lastKnownReconcilation = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var endDate = DateTime.UtcNow.AddDays(1);
            var enteredByID = context.EmployeeData.First(t => t.BID == DBCreationFunctions.BID).ID;
            var companyArray = context.CompanyData.Select(a => a.ID).ToArray();
            var CompanyID = companyArray[0];
            var reconciliation = DBCreationFunctions.createEmptyReconciliation(context, 1);
            int nextGLId = -99;

            // Generate adjustments reconciliations
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, 1, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMachineDataInDb(context, orderItemData, incomeAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            glEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            glEntries.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(glEntries);
            reconciliation.EndingGLID = glEntries.ToList().Last().ID;
            context.SaveChanges();

            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            var newglEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            newglEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-1));
            newglEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddMinutes(1));
            newglEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            nextGLId = -80;
            newglEntries.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(newglEntries);
            context.SaveChanges();

            var existingInTimeFrame = context.Reconciliation.FirstOrDefault(x => x.LastAccountingDT < new DateTime(2006,1,1) && x.AccountingDT > new DateTime(2006, 1, 1));
            var result = await glEngine.GetAdjustmentsNeeded(1);
            Assert.AreEqual(true, result.HasAdjustments);
            if (existingInTimeFrame != null)
                Assert.AreEqual(2, result.Adjustments.Count);
            else
                Assert.AreEqual(1, result.Adjustments.Count);
            Assert.AreEqual(1, result.Adjustments.First().LocationID);
            Assert.AreEqual(reconciliation.LastAccountingDT.ToShortTimeString(), result.Adjustments.First().StartingDT.ToShortTimeString());
            Assert.AreEqual(reconciliation.AccountingDT.ToShortTimeString(), result.Adjustments.First().EndingDT.ToShortTimeString());

            var result2 = await glEngine.GenerateReconciliationAdjustmentsPreview(1, enteredByID);
            if (existingInTimeFrame != null)
                Assert.AreEqual(2, result2.Count);
            else
                Assert.AreEqual(1, result2.Count);
            Assert.AreEqual(2, result2.First().Items.Count);
            
            var lastReconciliation = context.Reconciliation.Where(t => t.BID == DBCreationFunctions.BID && t.LocationID == order.LocationID).OrderByDescending(a => a.ID).FirstOrDefault();
            if (lastReconciliation != null)
            {
                lastKnownReconcilation = lastReconciliation.AccountingDT;
            }

            var ID = -200;

            // should not reconcile when adjustment is needed
            await Assert.ThrowsExceptionAsync<Exception>(async () => await glEngine.Reconcile(order.LocationID, enteredByID, endDate, false, null, (context, bid, val) => Task.FromResult(0)));

            var results = await glEngine.Reconcile(order.LocationID, enteredByID, endDate, true, null, (context, bid, val) => {
                var ids = ID--;
                return Task.FromResult(ids);
            });

            await context.SaveChangesAsync();

            Assert.IsTrue(results.Any(a => a.IsAdjustmentEntry));
            if (existingInTimeFrame != null)
                Assert.AreEqual(3, results.Count);
            else
                Assert.AreEqual(2, results.Count);
        }

        [TestMethod]
        public async Task Reconciliation_Reconcile_FailDueLastReconciliationAccountingDT()
        {
            // Setup
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            var lastKnownReconcilation = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var endDate = DateTime.UtcNow.AddDays(1);
            var enteredByID = context.EmployeeData.First(t => t.BID == DBCreationFunctions.BID).ID;

            // create order
            OrderData order = createOrder(0, companyID[0], false);
            checkGLForOrder(order.ID);

            var hasAdjustments = await glEngine.GetAdjustmentsNeeded(order.LocationID);
            byte i = 0;
            while(hasAdjustments.HasAdjustments == true)
            {
                order = createOrder(i, companyID[0], false);
                checkGLForOrder(order.ID);
                hasAdjustments = await glEngine.GetAdjustmentsNeeded(order.LocationID);
                i++;
            }

            var ID = -200;

            var result1s = await glEngine.Reconcile(order.LocationID, enteredByID, endDate, false, null, (context, bid, val) => {
                ID = val == (int)ClassType.Reconciliation ? int.MaxValue : Math.Min(-200, ID);
                var ids = ID--;
                return Task.FromResult(ids);
            });

            await context.SaveChangesAsync();

            foreach (var result1 in result1s)
            {
                result1.AccountingDT = DateTime.UtcNow.AddDays(20);
                await context.SaveChangesAsync();
            }

            ID = -150;

            await Assert.ThrowsExceptionAsync<ApplicationException>(async () => await glEngine.Reconcile(order.LocationID, enteredByID, endDate, false, null, (context, bid, val) => {
                var ids = ID--;
                return Task.FromResult(ids);
            }));
        }

        #endregion

        private void checkCurrentForLocation(List<GLData> currentGL, int locationIndex)
        {
            var locationID = LocationID[locationIndex];
            //Balance Sheet
            Assert.AreEqual(currentGL.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), ordersInWIPCurrent[locationIndex]);
            Assert.AreEqual(currentGL.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), this.orderInBuiltCurrent[locationIndex]);
            Assert.AreEqual(currentGL.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), -ordersNotCompleteCurrent[locationIndex]);
            Assert.AreEqual(currentGL.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), this.arCurrent[locationIndex]);
            Assert.AreEqual(currentGL.Where(entry => entry.GLAccountID == GLEngine.Customer_Credits_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), -this.iscCurrent[locationIndex]);
            Assert.AreEqual(currentGL.Where(entry => entry.GLAccountID == GLEngine.Customer_Deposits_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), -this.depositsCurrent[locationIndex]);
        }

        private void checkChangeForLocation(List<GLData> changeGL, int locationIndex)
        {
            if (changeGL.Count < 1)
            {
                return;
            }

            var locationID = LocationID[locationIndex];

            //P&L
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccount != null && entry.GLAccount.GLAccountType == 40 && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), -this.incomeChange[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Credit_Adjustments_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), this.iscExpense[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Bad_Debt_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), this.badDebtExpense[locationIndex]);

            //Balance Sheet
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), ordersInWIPChange[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), orderInBuiltChange[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), -ordersNotCompleteChange[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), this.arChange[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => (entry.GLAccountID == GLEngine.Undeposited_Funds_ID || entry.GLAccountID == GLEngine.Cash_Drawer_ID) && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), this.undepositedFundsChange[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Customer_Credits_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), -this.iscChange[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Customer_Deposits_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), -this.depositsChange[locationIndex]);
            Assert.AreEqual(changeGL.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID && entry.LocationID == locationID).Select(entry => entry.Amount).Sum(), -this.salesTaxChange[locationIndex]);

        }

        private void checkGLForPayment(int paymentId, bool IsMaster = true)
        {
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            var compositeGL = new List<GLData>();
            if (IsMaster)
            {
                var applicationPayments = context.PaymentApplication.Where(o => o.MasterID == paymentId).ToList();
                Assert.IsTrue(applicationPayments.Count() > 0);
                foreach (var payment in applicationPayments)
                {
                    var paymentID = payment.ID;
                    //calculate total
                    //var paymentCurrentGL = glEngine.CalculateGL(EnumGLType.Payment, payment.ID);
                    //calculate diff
                    var paymentChangeGL = glEngine.ComputeGLDiff(EnumGLType.Payment, paymentID);
                    compositeGL.AddRange(paymentChangeGL);
                }
            }
            else
            {
                compositeGL = glEngine.ComputeGLDiff(EnumGLType.Payment, paymentId);
            }
            var changeGL = glEngine.CompressGLForTransaciton(compositeGL);
            SaveNewGLData(changeGL);
            //check total
            //checkCurrentForLocation(currentGL, 0);
            //checkCurrentForLocation(currentGL, 1);

            //check diff
            checkChangeForLocation(changeGL, 0);
            checkChangeForLocation(changeGL, 1);

            //Zero out Change values
            ZeroOutChangeValues(0);
            ZeroOutChangeValues(1);

        }

        private void checkGLForOrder(int orderId)
        {
            var glEngine = new GLEngine(DBCreationFunctions.BID, context);
            //calculate total
            //var currentGL = glEngine.CalculateGL(EnumGLType.Order, orderId);
            //calculate diff
            var changeGL = glEngine.ComputeGLDiff(EnumGLType.Order, orderId);
            SaveNewGLData(changeGL);
            //check total
            //checkCurrentForLocation(currentGL, 0);
            //checkCurrentForLocation(currentGL, 1);

            //check diff
            checkChangeForLocation(changeGL, 0);
            checkChangeForLocation(changeGL, 1);

            //Zero out Change values
            ZeroOutChangeValues(0);
            ZeroOutChangeValues(1);
            
        }

        private void SaveNewGLData(List<GLData> changeGL)
        {
            int nextID = -99;
            var IDs = context.ActivityGlactivity.Where(x => x.ID < 0).Select(x => x.ID).ToList();
            if (IDs.Any())
            {
                nextID = IDs.Max() + 1;
            }
            var GLActivity = new ActivityGlactivity
            {
                BID = 1,
                ID = nextID,
                Name = "Test",
                Subject = "Test",
                CompletedDT = new DateTime(),

            };
            context.Add(GLActivity);
            context.SaveChanges();
            foreach (var glData in changeGL)
            {
                nextID = -99;
                IDs = context.GLData.Where(x => x.ID < 0).Select(x => x.ID).ToList();
                if (IDs.Any())
                {
                    nextID = IDs.Max() + 1;
                }
                glData.ID = nextID;
                glData.ActivityID = GLActivity.ID;
                context.Add(glData);
                context.SaveChanges();
            }
        }
    }
}
