﻿using Endor.EF;
using Endor.Models;
using Endor.Tasks;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.GLEngine.Tests
{
    public class GLTestHelper
    {
        public class MockTaskQueuer : ITaskQueuer
        {
            public string Priority { get; set; }
            public string Schedule { get; set; }

            public MockTaskQueuer()
            {
            }

            public Task<string> EmptyTemp(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> RecomputeGL(short bid, byte type, int Id, string name, string Subject, short? CompletedById, int? CompletedByContactId, byte glEntryType)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> EmptyTrash(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> IndexClasstype(short bid, int ctid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> IndexModel(short bid, int ctid, int id)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> Test(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> CheckOrderStatus(short bid, int orderId, int status)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> OrderPricingCompute(short bid, int orderID)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> OrderPricingComputeTax(short bid, int orderID)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> IndexMultipleID(short bid, int ctid, ICollection<int> ids)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> DeleteExpiredDrafts(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> RepairUntrustedConstraint(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> RegenerateCBELForMachine(short bid, int machineId)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> ClearAllAssemblies(short bid)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> ClearAssembly(short bid, int ctid, int id)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> CreateDBBackup(short bid, DbType dbType)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> GenerateDocumentReportByMenuID(byte aid, short bid, byte documentReportType, int MenuID, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> GenerateDocumentReportByTemplateName(byte aid, short bid, byte documentReportType, string templateName, int[] DataID = null, int[] CustomFields = null, string OrderStatusFilter = null, string OptionsValue = null, string DMFilePath = null)
            {
                return Task.FromResult(Guid.NewGuid().ToString());
            }

            public Task<string> Reconciliate(short bid, byte? locationID, short? enteredById, DateTime accountingDT, bool createAdjustments, IEnumerable<(decimal CashIn, decimal CashOut, decimal CashDeposit, byte LocationID)> cashDrawer)
            {
                throw new NotImplementedException();
            }

            public Task<string> CreateViewCustomField(short bid, int appliesToClassTypeID, string sourceDataTable, string viewBaseName, string viewSchema = "dbo", string IDFieldName = "ID")
            {
                throw new NotImplementedException();
            }
        }

        public class MockTenantDataCache : ITenantDataCache
        {
            public static Dictionary<string, string> GetLocalSettings()
            {
                /*
                    Example test-config.json
                    {
                        "BusinessDBConnectionString ="Data Source=.\\SQLEXPRESS;Initial Catalog=\"Dev.Endor.Business.DB1\";User ID=cyrious;Password=watankahani"
                    } 
                */
                string file = "..\\..\\..\\..\\test-config.json"; //put this inside end-common folder
                if (File.Exists(file))
                {
                    string text = File.ReadAllText(file);
                    var result = JsonConvert.DeserializeObject<Dictionary<string, string>>(text);
                    return result;
                }
                else
                {
                    return null;
                }
            }

            private static readonly TenantData Mock = new TenantData()
            {
                APIURL = "https://endorapi.localcyriousdevelopment.com:5002/",
                ReportingURL = "",
                MessagingURL = "https://endormessaging.localcyriousdevelopment.com:5004/",
                LoggingURL = "https://endorlog.localcyriousdevelopment.com:5003",
                BackgroundEngineURL = "https://endorbgengine.localcyriousdevelopment.com:5006/",
                StorageURL = "http://127.0.0.1:10000/devstoreaccount1/",
                StorageConnectionString = "UseDevelopmentStorage=true",
                IndexStorageConnectionString = "UseDevelopmentStorage=true",
                BusinessDBConnectionString = GetLocalSettings() != null ? GetLocalSettings()["BusinessDBConnectionString"] : "Data Source=.\\SQLEXPRESS;Initial Catalog=\"Dev.Endor.Business.DB1\";User ID=cyrious;Password=watankahani",
                LoggingDBConnectionString = "",
                MessagingDBConnectionString = "",
                SystemReportBConnectionString = "",
                SystemDataDBConnectionString = "",
            };

            public Task<TenantData> Get(short bid)
            {
                if (bid == 1)
                    return Task.FromResult(Mock);
                else
                    return null;
            }

            public void InvalidateCache()
            {
            }

            public void InvalidateCache(short bid)
            {
            }
        }

        public static ApiContext GetMockCtx(short bid)
        {
            var opts = new DbContextOptions<ApiContext>()
            {
            };
            ApiContext ctx = new ApiContext(opts, new MockTenantDataCache(), bid);
            new MigrationHelper(new MockTaskQueuer()).MigrateDb(ctx);
            return ctx;
        }

        public static string TestLocationName = "TestLocation";
        public static void DeleteTables(ApiContext ctx)
        {
            try
            {
                var reconciliations = ctx.Reconciliation.Where(o => o.ID <= 0 || o.ID == int.MaxValue || o.StartingGLID < 0).ToList();
                ctx.Set<ReconciliationItem>().RemoveRange(ctx.Set<ReconciliationItem>().Where(o => reconciliations.Select(o => o.ID).Contains(o.ReconciliationID)).ToList());
                var activities = reconciliations.Select(a => a.GLActivityID).ToArray();
                //reconciliations.Reverse();
                ctx.Reconciliation.RemoveRange(reconciliations);
                ctx.SaveChanges();
                ctx.GLData.RemoveRange(ctx.GLData.Where(o => o.ID <= 0));
                ctx.SaveChanges();
                ctx.ActivityGlactivity.RemoveRange(ctx.ActivityGlactivity.Where(a => activities.Contains(a.ID)).ToList());

                ctx.ActivityGlactivity.RemoveRange(ctx.ActivityGlactivity.Where(o => o.ID <= 0));
                ctx.SaveChanges();

                DisableSystemVersioningIfEnabled(ctx, "Order.Data");
                DisableSystemVersioningIfEnabled(ctx, "Order.Item.Data");
                DisableSystemVersioningIfEnabled(ctx, "Order.Item.Component");
                DisableSystemVersioningIfEnabled(ctx, "Order.Item.Surcharge");
                DisableSystemVersioningIfEnabled(ctx, "Order.Destination.Data");
                DisableSystemVersioningIfEnabled(ctx, "Order.Tax.Item.Assessment");
                DisableSystemVersioningIfEnabled(ctx, "Accounting.Payment.Application");

                ctx.SaveChanges();
                ctx.OrderItemTaxAssessment.RemoveRange(ctx.OrderItemTaxAssessment.Where(o => o.ID <= 0));
                ctx.SaveChanges();
                ctx.OrderItemComponent.RemoveRange(ctx.OrderItemComponent.Where(o => o.ID <= 0));
                DeleteTestRowsFromHistoricTable(ctx, "Order.Item.Component");
                ctx.SaveChanges();
                ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(o => o.SurchargeDefID <= 0));
                DeleteTestRowsFromHistoricTable(ctx, "Order.Item.Surcharge", "SurchargeDefID <= 0");
                ctx.SaveChanges();
                ctx.MachineProfileTable.RemoveRange(ctx.MachineProfileTable.Where(o => o.ID <= 0 || o.ProfileID <= 0));
                ctx.MachineProfile.RemoveRange(ctx.MachineProfile.Where(o => o.ID <= 0 || o.MachineID <= 0));
                ctx.SaveChanges(); 
                ctx.MachineData.RemoveRange(ctx.MachineData.Where(o => o.ID <= 0));

                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(o => o.AssemblyID <= 0));
                ctx.SaveChanges();

                ctx.LaborData.RemoveRange(ctx.LaborData.Where(o => o.ID <= 0));
                ctx.LaborCategoryLink.RemoveRange(ctx.LaborCategoryLink.Where(o => o.PartID <= 0));

                ctx.SaveChanges();
                ctx.SurchargeDef.RemoveRange(ctx.SurchargeDef.Where(o => o.ID <= 0));
                ctx.MaterialData.RemoveRange(ctx.MaterialData.Where(o => o.ID <= 0));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(o => o.AssemblyID <= 0));
                ctx.AssemblyData.RemoveRange(ctx.AssemblyData.Where(o => o.ID <= 0));
                ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(o => o.ID <= 0));
                ctx.PaymentApplication.RemoveRange(ctx.PaymentApplication.Where(o => o.MasterID <= 0));
                ctx.SaveChanges();
                ctx.PaymentMaster.RemoveRange(ctx.PaymentMaster.Where(o => o.ID <= 0));
                ctx.OrderItemData.RemoveRange(ctx.OrderItemData.Where(o => o.ID <= 0 || o.OrderID <= 0));
                ctx.OrderItemComponent.RemoveRange(ctx.OrderItemComponent.Where(o => o.OrderItemID <= 0));
                ctx.PaymentMethod.RemoveRange(ctx.PaymentMethod.Where(o => o.ID <= 0));

                ctx.OrderEmployeeRole.RemoveRange(ctx.OrderEmployeeRole.Where(o => o.OrderID <= 0));
                ctx.OrderCustomData.RemoveRange(ctx.OrderCustomData.Where(o => o.Order.ID <= 0));
                ctx.SaveChanges();
                ctx.OrderItemData.RemoveRange(ctx.OrderItemData.Where(o => o.OrderID <= 0));
                ctx.OrderDestinationData.RemoveRange(ctx.OrderDestinationData.Where(o => o.OrderID <= 0));
                ctx.TransactionHeaderData.RemoveRange(ctx.TransactionHeaderData.Where(o => o.ID <= 0));
                ctx.SaveChanges();
                ctx.OrderDestinationData.RemoveRange(ctx.OrderDestinationData.Where(o => o.ID <= 0));
                //ctx.PaymentData.RemoveRange(ctx.PaymentData.Where(o => o.ID <= 0));
                ctx.TaxGroup.RemoveRange(ctx.TaxGroup.Where(o => o.ID <= 0));
                ctx.TaxItem.RemoveRange(ctx.TaxItem.Where(o => o.ID <= 0));
                ctx.TaxabilityCodeItemExemptionLinks.RemoveRange(ctx.TaxabilityCodeItemExemptionLinks.Where(o => o.TaxItemID <= 0));
                ctx.OrderDestinationTagLink.RemoveRange(ctx.OrderDestinationTagLink.Where(o => o.OrderDestinationID <= 0));
                ctx.LocationData.RemoveRange(ctx.LocationData.Where(o => o.Name == GLTestHelper.TestLocationName));
                ctx.SaveChanges();
            }
            catch(Exception) { throw; }
            finally {
                EnableSystemVersioning(ctx, "Order.Item.Data");
                EnableSystemVersioning(ctx, "Order.Item.Component");
                EnableSystemVersioning(ctx, "Order.Item.Surcharge");
                EnableSystemVersioning(ctx, "Order.Destination.Data");
                EnableSystemVersioning(ctx, "Order.Data");
                EnableSystemVersioning(ctx, "Order.Tax.Item.Assessment");
                EnableSystemVersioning(ctx, "Accounting.Payment.Application");
            }

        }

        /// <summary>
        /// Disables System Versioning on a table if it is already enabled
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        public static void DisableSystemVersioningIfEnabled(ApiContext ctx, string tableName)
        {
            string s = $@"
                    IF EXISTS (SELECT temporal_type FROM sys.tables WHERE object_id = OBJECT_ID('dbo.[{tableName}]', 'u') AND temporal_type = 2)
                    BEGIN
                        ALTER TABLE dbo.[{tableName}] SET(SYSTEM_VERSIONING = OFF);
                    END
                ";
            ctx.Database.ExecuteSqlRaw(s);
        }

        /// <summary>
        /// Enables System Versioning on a table
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        public static void EnableSystemVersioning(ApiContext ctx, string tableName)
        {
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentException($"A value for parameter {nameof(tableName)} must be supplied!");

            string s = $@"ALTER TABLE [{tableName}] SET(SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.[Historic.{tableName}]))";

            ctx.Database.ExecuteSqlRaw(s);
        }

        /// <summary>
        /// Delete Test Rows From Historic Table
        /// </summary>
        /// <param name="migrationBuilder"></param>
        /// <param name="tableName">Name of the table without square brackets</param>
        public static void DeleteTestRowsFromHistoricTable(ApiContext ctx, string tableName, string whereCondition = "ID < 0")
        {
            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentException($"A value for parameter {nameof(tableName)} must be supplied!");

            string s = $@"Delete [Historic.{tableName}] where {whereCondition}";

            ctx.Database.ExecuteSqlRaw(s);
        }

    }
}
