﻿using Endor.EF;
using Endor.GLEngine.Models;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.GLEngine.Tests
{
    [TestClass]
    public class PreviewAdjustmentsTest
    {
        const byte BID = 1;
        ApiContext context;
        byte LocationID;
        int CompanyID;
        TaxItem taxItem;
        List<GLAccount> incomeAccounts;
        List<GLAccount> expenseAccounts;
        List<GLAccount> assetAccounts;
        Reconciliation reconciliation;
        int nextGLId = -99;

        [TestInitialize]
        public void Setup()
        {
            context = GLTestHelper.GetMockCtx(1);

            GLTestHelper.DeleteTables(context);
            var location = DBCreationFunctions.CreateLocationData(context);
            LocationID = location.ID;
            var companyArray = context.CompanyData.Select(a => a.ID).ToArray();
            CompanyID = companyArray[0];
            taxItem = DBCreationFunctions.CreateTaxGroupInDb(context);
            incomeAccounts = context.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income
            expenseAccounts = context.GLAccount.Where(account => account.GLAccountType == 50).ToList(); //Expense
            assetAccounts = context.GLAccount.Where(account => account.GLAccountType == 13).ToList(); //Asset
            reconciliation = DBCreationFunctions.createEmptyReconciliation(context, LocationID);
        }

        [TestCleanup]
        public void Cleanup()
        {
            GLTestHelper.DeleteTables(context);

        }

        [TestMethod]
        public async Task TestNoAdjustmentsNeeded()
        {
            var glEngine = new GLEngine(BID, context);
            var enteredByID = context.EmployeeData.First(t => t.BID == DBCreationFunctions.BID).ID;
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMachineDataInDb(context, orderItemData, incomeAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            glEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            glEntries.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(glEntries);
            reconciliation.EndingGLID = glEntries.ToList().Last().ID;
            context.SaveChanges();

            var result = await glEngine.GenerateReconciliationAdjustmentsPreview(LocationID, enteredByID);
            Assert.AreEqual(0, result.Count);

            var result2 = await glEngine.GetAdjustmentsNeeded(LocationID);
            Assert.AreEqual(false, result2.HasAdjustments);
            Assert.AreEqual(0, result2.Adjustments.Count);
            
        }

        [TestMethod]
        public async Task TestOneAdjustmentsNeeded()
        {
            var glEngine = new GLEngine(BID, context);
            var enteredByID = context.EmployeeData.First(t => t.BID == DBCreationFunctions.BID).ID;
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMachineDataInDb(context, orderItemData, incomeAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            glEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            glEntries.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(glEntries);
            reconciliation.EndingGLID = glEntries.ToList().Last().ID;
            context.SaveChanges();

            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            var newglEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            newglEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-1));
            newglEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddMinutes(1));
            newglEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            nextGLId = -80;
            newglEntries.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(newglEntries);
            context.SaveChanges();

            var result = await glEngine.GetAdjustmentsNeeded(LocationID);
            Assert.AreEqual(true, result.HasAdjustments);
            Assert.AreEqual(1, result.Adjustments.Count);
            Assert.AreEqual(LocationID, result.Adjustments.First().LocationID);
            Assert.AreEqual(reconciliation.LastAccountingDT.ToShortTimeString(), result.Adjustments.First().StartingDT.ToShortTimeString());
            Assert.AreEqual(reconciliation.AccountingDT.ToShortTimeString(), result.Adjustments.First().EndingDT.ToShortTimeString());

            var result2 = await glEngine.GenerateReconciliationAdjustmentsPreview(LocationID, enteredByID);
            Assert.AreEqual(1, result2.Count);
            Assert.AreEqual(2, result2.First().Items.Count);
        }


        [TestMethod]
        public async Task TestMultipleAdjustmentsNeeded()
        {
            var glEngine = new GLEngine(BID, context);
            var enteredByID = context.EmployeeData.First(t => t.BID == DBCreationFunctions.BID).ID;
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMachineDataInDb(context, orderItemData, incomeAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            glEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddMinutes(-2));
            glEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            glEntries.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(glEntries);
            reconciliation.EndingGLID = glEntries.ToList().Last().ID;
            context.SaveChanges();

            Reconciliation secondReconciliation = DBCreationFunctions.createEmptyReconciliation(context, LocationID);
            secondReconciliation.LastAccountingDT = reconciliation.AccountingDT;
            secondReconciliation.AccountingDT = reconciliation.AccountingDT.AddDays(1);
            context.SaveChanges();

            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            var newglEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            newglEntries.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddMinutes(-11));
            newglEntries.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddDays(2));
            newglEntries.ForEach(gl => gl.ActivityID = reconciliation.GLActivityID);
            nextGLId = -80;
            newglEntries.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(newglEntries);
            context.SaveChanges();

            nextGLId = -70;
            order.OrderStatusID = OrderOrderStatus.OrderWIP;
            context.SaveChanges();
            var glEntries2 = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            glEntries2.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddDays(1).AddMinutes(-2));
            glEntries2.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddDays(2));
            glEntries2.ForEach(gl => gl.ActivityID = secondReconciliation.GLActivityID);
            glEntries2.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(glEntries2);
            secondReconciliation.EndingGLID = reconciliation.EndingGLID;
            context.SaveChanges();

            nextGLId = -60;
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            var glEntries3 = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            glEntries3.ForEach(gl => gl.AccountingDT = reconciliation.AccountingDT.AddDays(2).AddMinutes(-2));
            glEntries3.ForEach(gl => gl.RecordedDT = reconciliation.AccountingDT.AddDays(2));
            glEntries3.ForEach(gl => gl.ActivityID = secondReconciliation.GLActivityID);
            glEntries3.ForEach(gl => gl.ID = nextGLId++);
            context.GLData.AddRange(glEntries3);
            
            var result = await glEngine.GenerateReconciliationAdjustmentsPreview(LocationID, enteredByID);
            
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(2, result.First().Items.Count);
            Assert.AreEqual(2, result.Last().Items.Count);

            var result2 = await glEngine.GetAdjustmentsNeeded(LocationID);
            Assert.AreEqual(true, result2.HasAdjustments);
            Assert.AreEqual(2, result2.Adjustments.Count);
            Assert.AreEqual(LocationID, result2.Adjustments.First().LocationID);
            Assert.AreEqual(reconciliation.LastAccountingDT.ToShortTimeString(), result2.Adjustments.First().StartingDT.ToShortTimeString());
            Assert.AreEqual(reconciliation.AccountingDT.ToShortTimeString(), result2.Adjustments.First().EndingDT.ToShortTimeString());

        }
    }
}
