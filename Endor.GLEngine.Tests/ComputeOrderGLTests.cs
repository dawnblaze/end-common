﻿using Endor.EF;
using Endor.GLEngine.Models;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.GLEngine.Tests
{
    [TestClass]
    public class ComputeOrderGLTests
    {
        const byte BID = 1;
        ApiContext context;
        byte LocationID;
        int CompanyID;
        TaxItem taxItem;
        List<GLAccount> incomeAccounts;
        List<GLAccount> expenseAccounts;
        List<GLAccount> assetAccounts;

        [TestInitialize]
        public void Setup()
        {
            context = GLTestHelper.GetMockCtx(1);
            GLTestHelper.DeleteTables(context);
            var locationArray = context.LocationData.Select(a => a.ID).ToArray();
            LocationID = locationArray[0];
            var companyArray = context.CompanyData.Select(a => a.ID).ToArray();
            CompanyID = companyArray[0];
            taxItem = DBCreationFunctions.CreateTaxGroupInDb(context);
            incomeAccounts = context.GLAccount.Where(account => account.GLAccountType == 40).ToList(); //Income
            expenseAccounts = context.GLAccount.Where(account => account.GLAccountType == 50).ToList(); //Expense
            assetAccounts = context.GLAccount.Where(account => account.GLAccountType == 13).ToList(); //Asset

        }

        [TestCleanup]
        public void Cleanup()
        {
            GLTestHelper.DeleteTables(context);
        }

        [TestMethod]
        public void SimpleOrderMachine()
        {
            var glEngine = new GLEngine(BID, context);
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMachineDataInDb(context, orderItemData, incomeAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == 10);


        }

        [TestMethod]
        public void SimpleOrderLabor()
        {
            var glEngine = new GLEngine(BID, context);
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithLaborDataInDb(context, orderItemData, incomeAccounts[0], expenseAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == 10);

        }

        [TestMethod]
        public void SimpleOrderMaterial()
        {
            var glEngine = new GLEngine(BID, context);
            
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMaterialDataInDb(context, orderItemData, incomeAccounts[0], expenseAccounts[0], assetAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == 10);

        }

        [TestMethod]
        public void SimpleOrderSurcharge()
        {
            var glEngine = new GLEngine(BID, context);
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemSurcharge orderItemSurchargeData = DBCreationFunctions.CreateSurchargeInDb(context, orderItemData, incomeAccounts[0], expenseAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == 10);

        }

        [TestMethod]
        public void SimpleOrderAssembly()
        {
            var glEngine = new GLEngine(BID, context);
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithAssemblySimpleDataInDb(context, orderItemData, incomeAccounts[0], incomeAccounts[1], expenseAccounts[0], assetAccounts[0]);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.AreEqual(2, glEntries.Count);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

        }

        [TestMethod]
        public void SimpleOrderAssemblyNoIncomeAllocationType()
        {
            var glEngine = new GLEngine(BID, context);
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithAssemblySimpleDataInDb(context, orderItemData, incomeAccounts[0], incomeAccounts[1], expenseAccounts[0], assetAccounts[0]);
            var orderItemComponent = context.OrderItemComponent.First(x => x.BID == BID && x.ID == orderItemComponentData.ID);
            orderItemComponent.IncomeAllocationType = null;
            context.Entry(orderItemComponent).Property(x => x.IncomeAllocationType).IsModified = true;
            context.SaveChanges();

            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.AreEqual(2, glEntries.Count);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

        }

        [TestMethod]
        public void AllocateByMachineOrderAssembly()
        {
            var glEngine = new GLEngine(BID, context);
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithAssemblySimpleDataInDb(context, orderItemData, incomeAccounts[0], incomeAccounts[1], expenseAccounts[0], assetAccounts[0]);
            orderItemComponentData.IncomeAllocationType = AssemblyIncomeAllocationType.AllocationByMachineIncomeAccount;
            context.SaveChanges();
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[1].ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

        }

        [TestMethod]
        public void AllocateByMaterialOrderAssembly()
        {
            var glEngine = new GLEngine(BID, context);
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithAssemblySimpleDataInDb(context, orderItemData, incomeAccounts[0], incomeAccounts[1], expenseAccounts[0], assetAccounts[0]);
            orderItemComponentData.IncomeAllocationType = AssemblyIncomeAllocationType.AllocationByMaterialIncomeAccount;
            context.SaveChanges();
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 3);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal / 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[1].ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderProductTotal / 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderProductTotal);
        }

        [TestMethod]
        public void SimpleOrderDestination()
        {
            var glEngine = new GLEngine(BID, context);
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderDestinationData orderDestinationData = DBCreationFunctions.CreateOrderDestinationInDb(context, order);

            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderDestinationTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderDestinationTotal);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderDestinationTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderDestinationTotal);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Shipping_Income_ID).Select(entry => entry.Amount).Sum() == -CompoundActionTests.OrderDestinationTotal);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == CompoundActionTests.OrderDestinationTotal);
        }

        [TestMethod]
        public async Task InsertGLActivity()
        {
            context.RemoveRange(context.ActivityGlactivity.Where(x => x.ID == -99));
            await context.SaveChangesAsync();
            var glEngine = new GLEngine(BID, context);
            var result = await glEngine.InsertGLActivity("Test", "Test", null, null, DateTime.Now, GLEntryType.Credit_Adjusted, 1, new List<GLData>(), RequestIDIntegerAsync);
            Assert.IsNotNull(result);
            context.RemoveRange(context.ActivityGlactivity.Where(x => x.ID == -99));
            await context.SaveChangesAsync();


        }

        [TestMethod]
        public void SimpleOrderTaxAssessments()
        {
            var glEngine = new GLEngine(BID, context);
            
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMaterialDataInDb(context, orderItemData, incomeAccounts[0], expenseAccounts[0], assetAccounts[0]);
            OrderItemTaxAssessment orderItemTaxAssessment = DBCreationFunctions.CreateTaxAssessmentDataInDb(context, order, orderItemData, orderItemComponentData);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_WIP_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to built
            order.OrderStatusID = OrderOrderStatus.OrderBuilt;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 2);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Production_Deliverables_ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Orders_In_Built_ID).Select(entry => entry.Amount).Sum() == 10);

            //Update to Completed
            order.OrderStatusID = OrderOrderStatus.OrderInvoiced;
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, order.ID);
            Assert.IsTrue(glEntries.Count == 3);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == -10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == 11);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).Select(entry => entry.Amount).Sum() == -1);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().ItemComponentID.GetValueOrDefault() , orderItemComponentData.ID);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().TaxCodeID.GetValueOrDefault(), context.TaxabilityCodes.First().ID);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().TaxItemID.GetValueOrDefault(), context.TaxItem.Where(x => x.BID == BID).First().ID);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().TaxGroupID.GetValueOrDefault(), context.TaxGroup.Where(x => x.BID == BID).First().ID);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().ItemSurchargeID, null);
        }

        [TestMethod]
        public void SimpleCreditMemo()
        {
            var glEngine = new GLEngine(BID, context);
            
            OrderData order = DBCreationFunctions.CreateOrderInDb(context, LocationID, CompanyID, false, taxItem.ID);
            OrderItemData orderItemData = DBCreationFunctions.CreateOrderItemInDb(context, order);
            OrderItemComponent orderItemComponentData = DBCreationFunctions.CreateComponentWithMaterialDataInDb(context, orderItemData, incomeAccounts[0], expenseAccounts[0], assetAccounts[0]);
            OrderItemTaxAssessment orderItemTaxAssessment = DBCreationFunctions.CreateTaxAssessmentDataInDb(context, order, orderItemData, orderItemComponentData);
            CreditMemoData creditMemo = OrderToCreditMemo(order, OrderOrderStatus.CreditMemoPosted);
            creditMemo = DBCreationFunctions.SaveOrderAsCreditMemo(context, order.ID, creditMemo);
            var glEntries = glEngine.CalculateGL(EnumGLType.Order, creditMemo.ID);
            Assert.AreEqual(3, glEntries.Count);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == incomeAccounts[0].ID).Select(entry => entry.Amount).Sum() == 10);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Accounts_Receivable_ID).Select(entry => entry.Amount).Sum() == -11);
            Assert.IsTrue(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).Select(entry => entry.Amount).Sum() == 1);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().ItemComponentID.GetValueOrDefault(), orderItemComponentData.ID);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().TaxCodeID.GetValueOrDefault(), context.TaxabilityCodes.First().ID);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().TaxItemID.GetValueOrDefault(), context.TaxItem.Where(x => x.BID == BID).First().ID);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().TaxGroupID.GetValueOrDefault(), context.TaxGroup.Where(x => x.BID == BID).First().ID);
            Assert.AreEqual(glEntries.Where(entry => entry.GLAccountID == GLEngine.Sales_Tax_Payable_ID).First().ItemSurchargeID, null);

            TransactionHeaderData creditMemo2 = context.TransactionHeaderData.Where(t => t.ID == creditMemo.ID && t.BID == BID).First();
            creditMemo2.OrderStatusID = OrderOrderStatus.CreditMemoVoided;
            context.TransactionHeaderData.Update(creditMemo2);
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, creditMemo2.ID);
            Assert.AreEqual(0, glEntries.Count);

            TransactionHeaderData creditMemo3 = context.TransactionHeaderData.Where(t => t.ID == creditMemo.ID && t.BID == BID).First();
            creditMemo3.OrderStatusID = OrderOrderStatus.CreditMemoUnposted;
            context.TransactionHeaderData.Update(creditMemo3);
            context.SaveChanges();
            glEntries = glEngine.CalculateGL(EnumGLType.Order, creditMemo3.ID);
            Assert.AreEqual(0, glEntries.Count);
        }


        private Task<int> RequestIDIntegerAsync(ApiContext ctx, short bid, int classtypeID)
        {
            return Task.FromResult(-99);
        }

        private CreditMemoData OrderToCreditMemo(OrderData order, OrderOrderStatus orderStatus = OrderOrderStatus.CreditMemoUnposted)
        {
            return new CreditMemoData()
            {
                ID = order.ID,
                BID = order.BID,
                ClassTypeID = ClassType.CreditMemo.ID(),
                Company = order.Company,
                CompanyID = order.CompanyID,
                ContactRoles = order.ContactRoles,
                CostLabor = order.CostLabor,
                CostMachine = order.CostMachine,
                CostMaterial = order.CostMaterial,
                CostOther = order.CostOther,
                CostTotal = order.CostTotal,
                CreditMemoApplied = order.CreditMemoApplied,
                CreditMemoBalance = order.CreditMemoBalance,
                CreditMemoCredit = order.CreditMemoCredit,
                CreditMemoHasBalance = order.CreditMemoHasBalance,
                CreditMemoUsed = order.CreditMemoUsed,
                Dates = order.Dates,
                Description = order.Description,
                Destinations = order.Destinations,
                DestinationType = order.DestinationType,
                EmployeeRoles = order.EmployeeRoles,
                EnumOrderDestinationType = order.EnumOrderDestinationType,
                EnumOrderOrderStatus = order.EnumOrderOrderStatus,
                FormattedNumber = order.FormattedNumber,
                FromQuickItemID = order.FromQuickItemID,
                HasDocuments = order.HasDocuments,
                HasOrderLinks = order.HasOrderLinks,
                HasSingleDestination = order.HasSingleDestination,
                InvoiceNumber = order.InvoiceNumber,
                IsTaxExempt = order.IsTaxExempt,
                IsUrgent = order.IsUrgent,
                ItemCount = order.ItemCount,
                Items = order.Items,
                Links = order.Links,
                Location = order.Location,
                LocationID = order.LocationID,
                ModifiedDT = order.ModifiedDT,
                Notes = order.Notes,
                Number = order.Number,
                Opportunity = order.Opportunity,
                OpportunityID = order.OpportunityID,
                OrderPONumber = order.OrderPONumber,
                OrderStatusID = orderStatus,
                OrderStatusStartDT = order.OrderStatusStartDT,
                Origin = order.Origin,
                OriginID = order.OriginID,
                PaymentAuthorized = order.PaymentAuthorized,
                PaymentBalanceDue = order.PaymentBalanceDue,
                PaymentPaid = order.PaymentPaid,
                Payments = order.Payments,
                PaymentTotal = order.PaymentTotal,
                PaymentWriteOff = order.PaymentWriteOff,
                PickupLocation = order.PickupLocation,
                PickupLocationID = order.PickupLocationID,
                PriceDestinationTotal = order.PriceDestinationTotal,
                PriceDiscount = order.PriceDiscount,
                PriceDiscountPercent = order.PriceDiscountPercent,
                PriceFinanceCharge = order.PriceFinanceCharge,
                PriceIsLocked = order.PriceIsLocked,
                PriceNet = order.PriceNet,
                PricePreTax = order.PricePreTax,
                PriceProductTotal = order.PriceProductTotal,
                PriceTax = order.PriceTax,
                PriceTaxable = order.PriceTaxable,
                PriceTaxableOV = order.PriceTaxableOV,
                PriceTaxRate = order.PriceTaxRate,
                PriceTotal = order.PriceTotal,
                Priority = order.Priority,
                ProductionLocation = order.ProductionLocation,
                ProductionLocationID = order.ProductionLocationID,
                SimpleDestinations = order.SimpleDestinations,
                SimpleItems = order.SimpleItems,
                TagLinks = order.TagLinks,
                TaxExemptReason = order.TaxExemptReason,
                TaxExemptReasonID = order.TaxExemptReasonID,
                TaxGroup = order.TaxGroup,
                TaxGroupID = order.TaxGroupID,
                TaxGroupOV = order.TaxGroupOV,
                TransactionType = (byte)OrderTransactionType.Memo,
            };
        }
    }
}
