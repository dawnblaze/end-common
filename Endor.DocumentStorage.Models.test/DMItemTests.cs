using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;

namespace Endor.DocumentStorage.Models.test
{
    [TestClass]
    public class DMItemTests
    {
        [TestMethod]
        public void DMItemSerializationTest()
        {
            DMItem item1 = new DMItem
            {
                Name = "yo",
                URL = "",
                Size = 10L,
                MediaType = MediaTypes.application_octet_stream,
                ModifiedDT = DateTime.UtcNow,
                Path = "/"
            };
            string serializedItem1 = JsonConvert.SerializeObject(item1);

            Assert.IsFalse(serializedItem1.ToLower().Contains("assettype"));
        }
    }
}
