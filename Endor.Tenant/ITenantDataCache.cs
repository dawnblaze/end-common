﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tenant
{
    public interface ITenantDataCache
    {
        Task<TenantData> Get(short bid);

        void InvalidateCache();

        void InvalidateCache(short bid);
    }
}
