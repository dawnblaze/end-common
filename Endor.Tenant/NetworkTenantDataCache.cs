﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Tenant
{
    public class NetworkTenantDataCache: TenantDataCache
    {
        private const string AuthServerTenantEndpoint = "/tenant/";
        private readonly string authServerURLBase, tenantSecretBase64;

        public NetworkTenantDataCache(IMemoryCache cache, IEnvironmentOptions options, int expirationMilliseconds = DefaultExpirationMilliseconds) : base(cache, expirationMilliseconds)
        {
            if (String.IsNullOrWhiteSpace(options.AuthOrigin))
                throw new ArgumentException("Options supplied to NetworkTenantDataCache contains an invalid empty AuthOrigin property");

            this.authServerURLBase = options.AuthOrigin.Trim('/');
            this.tenantSecretBase64 = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(options.TenantSecret));
        }

        protected override async Task<TenantData> GetTenantData(short bid)
        {
            using (var client = new HttpClient())
            {
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, this.authServerURLBase + AuthServerTenantEndpoint + bid);
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", tenantSecretBase64);
                HttpResponseMessage response = await client.SendAsync(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<TenantData>(await response.Content.ReadAsStringAsync());
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
