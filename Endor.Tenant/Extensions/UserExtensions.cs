﻿using System;
using System.Security.Claims;
using System.Security.Principal;

namespace Endor.Tenant
{
    /// <summary>
    /// Claim Names Constants
    /// </summary>
    public static class ClaimNameConstants
    {
        public const string Rights = "rights";
        public const string BID = "bid";
        public const string UserID = "userid";
        public const string EmployeeID = "employeeid";
        public const string TenantSecret = "tenantsecret";
        public const string UserLinkID = "userlinkid";
        public const string UserName = "username";
        public const string AID = "aid";
        public const string AccessType = "accesstype";
    }

    /// <summary>
    /// User Extensions for getting properties from the IPrincipal User
    /// </summary>
    public static class UserExtensions
    {
        /// <summary>
        /// Gets the Business ID from the IPrincipal user
        /// </summary>
        /// <param name="user">IPrincipal user</param>
        /// <returns></returns>
        public static short? BID(this IPrincipal user)
        {
            return GetFromClaim<short>(user, ClaimNameConstants.BID, short.TryParse);
        }

        /// <summary>
        /// Gets the Association ID from the IPrincipal user
        /// </summary>
        /// <param name="user">IPrincipal user</param>
        /// <returns></returns>
        public static byte? AID(this IPrincipal user)
        {
            return GetFromClaim<byte>(user, ClaimNameConstants.AID, byte.TryParse);
        }

        /// <summary>
        /// Gets the User ID from the IPrincipal user
        /// </summary>
        /// <param name="user">IPrincipal user</param>
        /// <returns></returns>
        public static int? UserID(this IPrincipal user)
        {
            return GetFromClaim<int>(user, ClaimNameConstants.UserID, int.TryParse);
        }

        /// <summary>
        /// Gets the User Name from the Identity identity
        /// </summary>
        /// <param name="identity">Identity identity</param>
        /// <returns></returns>
        public static string UserName(this IIdentity identity)
        {
            return (identity as ClaimsIdentity).FindFirst(ClaimNameConstants.UserName)?.Value;
        }

        /// <summary>
        /// Gets the User Name from the IPrincipal user
        /// </summary>
        /// <param name="user">IPrincipal user</param>
        /// <returns></returns>
        public static string UserName(this IPrincipal user)
        {
            if (user?.Identity is ClaimsIdentity)
            {
                ClaimsIdentity claimsIdentity = (ClaimsIdentity)user.Identity;
                return claimsIdentity.FindFirst(ClaimNameConstants.UserName)?.Value;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the Employee ID from the IPrincipal user
        /// </summary>
        /// <param name="user">IPrincipal user</param>
        /// <returns></returns>
        public static short? EmployeeID(this IPrincipal user)
        {
            return GetFromClaim<short>(user, ClaimNameConstants.EmployeeID, short.TryParse);
        }

        /// <summary>
        /// Gets the User Link ID from the IPrincipal user
        /// </summary>
        /// <param name="user">IPrincipal user</param>
        /// <returns></returns>
        public static short? UserLinkID(this IPrincipal user)
        {
            return GetFromClaim<short>(user, ClaimNameConstants.UserLinkID, short.TryParse);
        }

        delegate bool TryParser<T>(string rawInput, out T output);
        private static T? GetFromClaim<T>(IPrincipal user, string claimName, TryParser<T> tryParse)
            where T : struct
        {
            return GetFromClaim(user?.Identity, claimName, tryParse);
        }
        private static T? GetFromClaim<T>(IIdentity identity, string claimName, TryParser<T> tryParse)
            where T : struct
        {
            ClaimsIdentity ident = identity as ClaimsIdentity;
            if (ident != null)
            {
                return GetFromClaim(ident, claimName, tryParse);
            }
            else
            {
                return null;
            }
        }
        private static T? GetFromClaim<T>(ClaimsIdentity claimIdentity, string claimName, TryParser<T> tryParse)
            where T : struct
        {
            string rawShort = claimIdentity.FindFirst(claimName)?.Value;
            T result;
            if (tryParse(rawShort, out result))
                return result;
            else
                return (T?)null;
        }
    }
}
