﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Tenant
{
    public class ServerLink
    {
        public short ServerType { get; set; }
        public string URL { get; set; }
    }

    public class BaseServerData
    {
        public short ID { get; set; }
        public ServerType ServerType { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
    }
}

