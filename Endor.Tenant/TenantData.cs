﻿using System;
using System.Collections.Generic;

namespace Endor.Tenant
{
    public class TenantData
    {
        public string APIURL { get; set; }
        public string ReportingURL { get; set; }
        public string MessagingURL { get; set; }
        public string LoggingURL { get; set; }
        public string BackgroundEngineURL { get; set; }
        public string SearchURL { get; set; }
        public string BoardURL { get; set; }
        public string AnalyticsURL { get; set; }
        public string PaymentURL { get; set; }
        public string ExternalTaxEngineURL { get; set; }
        public string StorageURL { get; set; }

        public List<ServerLink> ServerLinks { get; set; }

        public string StorageConnectionString { get; set; }
        public string IndexStorageConnectionString { get; set; }
        public string BusinessDBConnectionString { get; set; }
        public string LoggingDBConnectionString { get; set; }
        public string MessagingDBConnectionString { get; set; }
        public string SystemReportBConnectionString { get; set; }
        public string SystemDataDBConnectionString { get; set; }
		
    }
}
