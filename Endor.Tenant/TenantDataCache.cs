﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Endor.Tenant
{
    public abstract class TenantDataCache: ITenantDataCache
    {
        public const int DefaultExpirationHours = 12;
        public const int DefaultExpirationMilliseconds = 1000 * 60 * 60 * DefaultExpirationHours;
        protected readonly IMemoryCache cache;
        public int ExpirationMilliseconds { get; private set; }
        private HashSet<short> requestedBIDs = new HashSet<short>();

        public TenantDataCache(IMemoryCache cache, int expirationMilliseconds = DefaultExpirationMilliseconds)
        {
            this.cache = cache;
            this.ExpirationMilliseconds = expirationMilliseconds;
        }

        public async Task<TenantData> Get(short bid)
        {
            TenantData cacheEntry;

            if (this.cache == null)
            {
                throw new InvalidOperationException("No underlying MemoryCache instance was provided at constructor time.");
            }
            else if (!this.cache.TryGetValue(bid, out cacheEntry) || cacheEntry == null)
            {
                // Key not in cache, so get data.
                cacheEntry = await GetTenantData(bid);

                if (cacheEntry != null)
                {
                    // Set cache options.
                    var cacheEntryOptions = new MemoryCacheEntryOptions()
                        .SetAbsoluteExpiration(TimeSpan.FromMilliseconds(this.ExpirationMilliseconds));

                    // Save data in cache.
                    this.cache.Set(bid, cacheEntry, cacheEntryOptions);

                    if (!this.requestedBIDs.Contains(bid))
                        this.requestedBIDs.Add(bid);
                }
            }

            return cacheEntry;
        }

        protected abstract Task<TenantData> GetTenantData(short bid);

        public void InvalidateCache()
        {
            foreach (short bid in requestedBIDs)
            {
                this.InvalidateCache(bid);
            }
        }

        public void InvalidateCache(short bid)
        {
            this.cache.Remove(bid);
        }
    }
}
