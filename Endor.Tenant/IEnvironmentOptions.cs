﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Tenant
{
    public interface ITenantSecretOptions
    {
        string TenantSecret { get; }
    }

    public interface IEnvironmentOptions: ITenantSecretOptions
    {
        string AuthOrigin { get; }
    }
}
