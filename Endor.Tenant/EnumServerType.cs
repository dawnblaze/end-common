﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Endor.Tenant
{
    public enum ServerType : byte
    {
        Web = 0,
        API = 1,
        BGEngine = 2,
        DocumentReports = 3,
        AnalyticReports = 4,
        Search = 5,
        Auth = 6,
        Log = 7,
        Board = 8,
        APE = 21,
        ATE = 22,
        APSA = 23,
        Storage = 31,
        RTC = 32,
        MessageEmail = 33,
        MergeFields = 34
    }
}
