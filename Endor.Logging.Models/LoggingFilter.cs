﻿using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Logging.Models
{
    public static class LoggingFilter
    {
        //public enum NamespaceFilter
        //{
        //    EndorOnly,
        //    All
        //}

        //public static NamespaceFilter Namespace = LoggingFilter.NamespaceFilter.EndorOnly;

        //public static Func<LogEvent, bool> ExclusionFunction = (log) =>
        //{
        //    switch (Namespace)
        //    {
        //        case NamespaceFilter.All:
        //            return false;
        //        default:
        //        case NamespaceFilter.EndorOnly:
        //            return !log.Properties.ContainsKey("IsEndor");
        //    }
        //};

        public static LoggingLevelSwitch EndorLevelSwitch = new LoggingLevelSwitch();
        public static LoggingLevelSwitch SystemLevelSwitch = new LoggingLevelSwitch(LogEventLevel.Error);
    }
}
