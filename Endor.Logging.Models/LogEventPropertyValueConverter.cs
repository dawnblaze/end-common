﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Serilog.Events;
using System.Reflection;

namespace Endor.Logging.Models
{
    public class LogEventPropertyValueConverter : JsonConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.String)
                return new ScalarValue(serializer.Deserialize(reader));
            else if (reader.TokenType == JsonToken.Integer || reader.TokenType == JsonToken.Float)
                return new ScalarValue(serializer.Deserialize(reader));
            else if (reader.TokenType == JsonToken.Null)
                return new ScalarValue(null);
            else if (reader.TokenType == JsonToken.StartArray)
            {
                return new SequenceValue(serializer.Deserialize<LogEventPropertyValue[]>(reader));
            }

            JObject jObject = JObject.Load(reader);

            if (FieldExists("Elements", jObject) && jObject["Elements"] is JArray)
            {
                var logEventPropertyValues = serializer.Deserialize<LogEventPropertyValue[]>(jObject["Elements"].CreateReader());
                return new SequenceValue(logEventPropertyValues);
            }

            if (FieldExists("Elements", jObject) && jObject["Elements"] is JObject)
            {
                var dictionary = serializer.Deserialize<Dictionary<string, LogEventPropertyValue>>(jObject["Elements"].CreateReader());
                return new DictionaryValue(dictionary.Select(x => new KeyValuePair<ScalarValue, LogEventPropertyValue>(new ScalarValue(x.Key), x.Value)));
            }

            if (FieldExists("Value", jObject))
            {
                return new ScalarValue(serializer.Deserialize(jObject["Value"].CreateReader()));
            }

            if (FieldExists("Properties", jObject))
            {
                var logEventProperties = serializer.Deserialize<LogEventProperty[]>(jObject["Properties"].CreateReader());
                string typeTag = null;
                if (FieldExists("TypeTag", jObject))
                {
                    typeTag = serializer.Deserialize<string>(jObject["TypeTag"].CreateReader());
                }
                return new StructureValue(logEventProperties, typeTag);
            }

            return new ScalarValue(jObject);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(LogEventPropertyValue).IsAssignableFrom(objectType);
        }

        private bool FieldExists(string fieldName, JObject jObject)
        {
            return jObject[fieldName] != null;
        }
    }
}
