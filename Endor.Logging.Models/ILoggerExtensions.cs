﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Logging.Models
{
    public static class ILoggerExtensions
    {
        public static void Billing(this Serilog.ILogger logger, BillingEntryType type, short bid, string machineName, int userID, string summary)
        {
            logger.Information(Constants.TypeAndBIDTemplate + "{MachineName} {EntryType} {UserID} {Summary}", LoggerType.Billing, bid, machineName, Convert.ToInt16(type), userID, summary);
        }

        public static void ClientAction(this Serilog.ILogger logger, ClientActionEntryType type, short bid, int userID, string clientID, string summary)
        {
            logger.Information(Constants.TypeAndBIDTemplate + "{EntryType} {UserID} {ClientID} {Summary}", LoggerType.ClientAction, bid, Convert.ToInt16(type), userID, clientID, summary);
        }

        public static void ClientConnection(this Serilog.ILogger logger, short bid, int userID, string clientID, int? deviceID, DateTime entryDTUtc)
        {
            logger.Information(Constants.TypeAndBIDTemplate + "{UserID} {ClientID} {DeviceID} {ConnectDT} {LastActivityDT}", LoggerType.ClientConnection, bid, userID, clientID, deviceID, entryDTUtc, entryDTUtc);
        }
    }
}
