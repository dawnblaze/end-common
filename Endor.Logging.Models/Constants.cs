﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Logging.Models
{
    public static class Constants
    {
        public const string TenantSecretConfigName = "Endor:TenantSecret";
        public const string RemoteDiagnosticLoggingURLConfigName = "Logging:RemoteURL";
        public const string BIDPropertyName = "BID";
        public const string LoggerTypePropertyName = "LoggerType";

        public const string TypeAndBIDTemplate = "{" + Constants.LoggerTypePropertyName + "} {" + Constants.BIDPropertyName + "} ";
    }
}
