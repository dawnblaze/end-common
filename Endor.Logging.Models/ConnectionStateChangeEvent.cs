﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Logging.Models
{
    public enum ConnectionState
    {
        Connected,
        Reconnected,
        Disconnected
    }

    public class ConnectionStateChangeEvent
    {
        public short BID { get; set; }
        public int UserID { get; set; }
        public string ConnectionID { get; set; }
        public ConnectionState State { get; set; }
        public DateTime DateTimeUTC { get; set; }

        public ConnectionStateChangeEvent() { }
    }
}
