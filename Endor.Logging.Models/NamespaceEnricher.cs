﻿using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Logging.Models
{
    public class NamespaceEnricher : ILogEventEnricher
    {
        private readonly string _namespace;

        public NamespaceEnricher(string @namespace)
        {
            this._namespace = @namespace;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory lepf)
        {
            logEvent.AddPropertyIfAbsent(
              lepf.CreateProperty("Namespace", this._namespace));
        }
    }
}
