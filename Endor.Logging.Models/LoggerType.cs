﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Logging.Models
{
    public enum LoggerType
    {
        Meta = -1,
        Diagnostic = 0,
        Billing,
        ClientAction,
        ClientConnection,
        Event,
        PricingError,
    }
}
