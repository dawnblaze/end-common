﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Serilog.Events;
using Serilog.Parsing;
using System.Reflection;

namespace Endor.Logging.Models
{
    public class LogEventJsonConverter : JsonConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObject = JObject.Load(reader);

            DateTimeOffset timestamp = serializer.Deserialize<DateTimeOffset>(jObject["Timestamp"].CreateReader());
            LogEventLevel level = serializer.Deserialize<LogEventLevel>(jObject["Level"].CreateReader());

            Exception exception = null;
            JToken rawException = jObject["Exception"];
            if (rawException != null)
            {
                try
                {
                    exception = serializer.Deserialize<Exception>(rawException.CreateReader());
                }
                catch
                {
                    exception = new Exception(rawException.ToString());                    
                }
            }

            string messageTemplateText;
            JToken rawMessageTemplate = jObject["MessageTemplate"];
            if (rawMessageTemplate.HasValues)
                messageTemplateText = rawMessageTemplate["Text"].Value<string>();
            else
                messageTemplateText = rawMessageTemplate.Value<string>();

            MessageTemplateParser messageTemplateParser = new MessageTemplateParser();
            MessageTemplate messageTemplate = messageTemplateParser.Parse(messageTemplateText);
            Dictionary<string, LogEventPropertyValue> logEventPropertyValues = serializer.Deserialize<Dictionary<string, LogEventPropertyValue>>(jObject["Properties"].CreateReader());

            return new LogEvent(timestamp, level, exception, messageTemplate, logEventPropertyValues.Select(x => new LogEventProperty(x.Key, x.Value)));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(LogEvent).IsAssignableFrom(objectType);
        }
    }
}
