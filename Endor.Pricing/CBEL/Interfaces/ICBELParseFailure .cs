﻿namespace Endor.CBEL.Interfaces
{
    public interface IParseFailure
    {
        string Message { get; }

        string Formula { get; }

        int Position { get; }
    }
}