﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    public interface IHasLayoutComputationComponent : IHasComponent<ICBELLayoutComputation>
    {
    }
}
