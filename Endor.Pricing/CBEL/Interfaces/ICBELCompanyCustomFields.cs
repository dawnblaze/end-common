﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("CompanyCustomFields")]
    public interface ICBELCompanyCustomFields : ICBELCustomFields
    {
    }
}