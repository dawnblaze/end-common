﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("EmployeeObject")]
    public interface ICBELEmployeeObject :ICBELObject
    {
        ICBELEmployeeCustomFields CustomFields { get; }
    }
}