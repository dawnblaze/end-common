﻿using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("LaborComponent")]
    public interface ICBELLaborComponent : ICBELComponent
    {
        string NameOnInvoice { get; }

        string SKU { get; }

        string Description { get; }

        decimal? SetupPrice { get; }

        decimal? HourlyPrice { get; }

        Measurement BillingIncrement { get; }

        Measurement MinimumBillingTime { get; }

        /// <summary>
        /// The fixed cost of the labor.
        /// </summary>
        decimal? DefinedFixedCost { get; }

        /// <summary>
        /// The fixed price of the assembly.
        /// </summary>
        decimal? DefinedFixedPrice { get; }

        /// <summary>
        /// The fixed price of the assembly.
        /// </summary>
        byte? MinimumTimeInMin { get; }

        /// <summary>
        /// The fixed price of the assembly.
        /// </summary>
        byte? BillingIncrementInMin { get; }
    }
}