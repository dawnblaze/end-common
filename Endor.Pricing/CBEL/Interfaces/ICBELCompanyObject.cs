﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("CompanyObject")]
    public interface ICBELCompanyObject :ICBELObject
    {
        ICBELCompanyCustomFields CustomFields { get; }
        string Tier { get; }
    }
}