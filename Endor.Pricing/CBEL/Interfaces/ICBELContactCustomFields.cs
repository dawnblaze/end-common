﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("ContactCustomFields")]
    public interface ICBELContactCustomFields : ICBELCustomFields
    {
    }
}