﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("OrderItemObject")]
    public interface ICBELOrderItemObject : ICBELObject
    {
    }
}