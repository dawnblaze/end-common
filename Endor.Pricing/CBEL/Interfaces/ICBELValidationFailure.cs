﻿namespace Endor.CBEL.Interfaces
{
    public interface ICBELValidationFailure
    {
        ICBELAssembly AssemblyComponent { get; }
        ICBELVariable ElementComponent { get; }
        string Message { get; }
    }
}