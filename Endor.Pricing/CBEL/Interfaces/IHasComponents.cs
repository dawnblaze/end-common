﻿using System.Collections.Generic;

namespace Endor.CBEL.Interfaces
{
    public interface IHasComponents
    {
        /// <summary>
        /// A dictionary of all of the components on the Assembly
        /// </summary>
        List<ICBELComponentVariable> Components { get; }
    }
}