﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("OpportunityObject")]
    public interface ICBELOpportunityObject :ICBELObject
    {
        ICBELOpportunityCustomFields CustomFields { get; }
    }
}