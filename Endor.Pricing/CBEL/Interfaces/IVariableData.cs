﻿using Endor.Models;
using Endor.Units;

namespace Endor.CBEL.Interfaces
{
    public interface IVariableData
    {
        DataType DataType { get; set; }

        string VariableName { get; set; }

        bool IsOV { get; set; }

        string Value { get; set; }

        string  Alt { get; set; }

        bool? IsIncluded { get; set; }
        bool? IsIncludedOV { get; set; }

        decimal? CostUnit { get; set; }
        bool? CostUnitOV { get; set; }

        Unit Unit { get; set; }
    }
}