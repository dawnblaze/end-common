﻿using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("MaterialComponent")]
    public interface ICBELMaterialComponent : ICBELComponent
    {
        string NameOnInvoice { get; }

        string SKU { get; }

        string Description { get; }

        //Use Enum Text Value
        string MaterialType { get; }

        Measurement Length { get; }

        //Returns same as Length
        Measurement Height { get; }

        Measurement Width { get; }

        Measurement Depth { get; }

        //Returns same as Depth
        Measurement Thickness { get; }

        Measurement Weight { get; }

        decimal? QuantityInSet { get; }
    }
}