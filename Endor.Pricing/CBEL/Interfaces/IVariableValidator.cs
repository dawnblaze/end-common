﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    public interface IVariableValidator<T>
    {
        bool Validate(ICBELVariableComputed<T> element, out ICBELValidationFailure vf);
    }
}
