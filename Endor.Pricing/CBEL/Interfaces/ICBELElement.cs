﻿using Endor.CBEL.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    public interface ICBELElement
    {
        /// <summary>
        /// The ID of the Element Type, based on the enum AssemblyElementType.
        /// 0	Group
        /// 1	Single Line Label
        /// 2	Multi-Line Label
        /// 3	URL Label
        /// 7	Spacer
        /// 11	Single Line Text
        /// 12	Multi-Line String
        /// 13	Drop-Down
        /// 21	Number
        /// 22	Measurement
        /// 31	Checkbox
        /// 102 	Show/Hide Group
        /// 106 	Linked Assembly
        /// 107 	Linked Machine
        /// </summary>
        int ElementType { get; }

        /// <summary>
        /// The text name for the Element Type (e.g., Checkbox, Dropdown, etc.)
        /// This metadata is useful for debugging.
        /// </summary>
        string ElementTypeName { get; }
    }
}
