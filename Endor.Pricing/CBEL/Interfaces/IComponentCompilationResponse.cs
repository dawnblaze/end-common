﻿using Endor.CBEL.Common;
using System.Collections.Generic;

namespace Endor.CBEL.Interfaces
{
    public interface IComponentCompilationResponse : ICompilationResponse
    {
        List<PricingErrorMessage> Errors { get; set; }
        List<string> ExternalDependencies { get; set; }
    }
}