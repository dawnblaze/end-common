﻿using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("ComponentBase")]
    public interface ICBELComponent : ICBELObject
    {
        /// <summary>
        /// The quantity of the component.  
        /// Except for assemblies, this is the same as the ConsumptionValue.
        /// </summary>
        decimal? QuantityValue { get; }

        /// <summary>
        /// Is the quantity of the component overridden.  
        /// </summary>
        bool? QuantityOV { get; }

        /// <summary>
        /// The unit Price of the assembly.
        /// Computed as the Price / ComponentQuantity
        /// </summary>
        decimal? ComputedUnitPrice { get; }

        /// <summary>
        /// The unit cost of the component is overridden.
        /// </summary>
        public bool? UnitCostIsOverridden { get; set; }

        /// <summary>
        /// The defined unit price of the component.
        /// </summary>
        decimal? DefinedUnitPrice { get; set; }

        /// <summary>
        /// The defined unit cost of the component.
        /// </summary>
        decimal? DefinedUnitCost { get; set;  }

        /// <summary>
        /// The overridden unit cost of the component.
        /// </summary>
        decimal? OverriddenUnitCost { get; set;  }

        /// <summary>
        /// GL ID for Income, required in order to fill <see cref="Endor.Pricing.ComponentPriceResult.IncomeAccountID"/>
        /// </summary>
        int? IncomeAccountID { get; set; }
        /// <summary>
        /// GL ID for Expense, required in order to fill <see cref="Endor.Pricing.ComponentPriceResult.ExpenseAccountID"/>
        /// </summary>
        int? ExpenseAccountID { get; set; }
        /// <summary>
        /// Allocation Type for Income, required in order to fill <see cref="Endor.Pricing.ComponentPriceResult.IncomeAllocationType"/>
        /// </summary>
        public AssemblyIncomeAllocationType? IncomeAllocationType { get; set; }
    }
}