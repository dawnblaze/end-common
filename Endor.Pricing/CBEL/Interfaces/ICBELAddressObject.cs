﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("AddressObject")]
    public interface ICBELAddressObject :ICBELObject
    {
    }
}