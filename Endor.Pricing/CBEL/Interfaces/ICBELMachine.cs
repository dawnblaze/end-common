﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("MachineBase")]
    public interface ICBELMachine : ICBELAssembly
    {

        string Description { get; set; }

        //For right now this is the ID of the Machine Template
        string MachineType { get; set; }

        short ActiveProfileCount { get; }

        short ActiveInstanceCount { get; }

        decimal? MachineCostPerHour { get; }
    }
}