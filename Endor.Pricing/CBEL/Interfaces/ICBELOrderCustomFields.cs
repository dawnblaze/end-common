﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("OrderCustomFields")]
    public interface ICBELOrderCustomFields : ICBELCustomFields
    {
    }
}