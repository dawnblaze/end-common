﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("MaterialObject")]
    public interface ICBELMaterialObject :ICBELObject
    {
        ICBELMaterialCustomFields CustomFields { get; }
        string NameOnInvoice { get; set; }
        string SKU { get; set; }
        string Description { get; set; }
        string MaterialType { get; set; }
        decimal? QuantityInSets { get; set; }
    }
}