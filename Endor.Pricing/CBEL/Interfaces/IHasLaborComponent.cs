﻿using Endor.CBEL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    public interface IHasLaborComponent : IHasComponent<ICBELLaborComponent>
    {
    }
}
