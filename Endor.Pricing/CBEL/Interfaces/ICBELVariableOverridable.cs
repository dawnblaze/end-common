﻿namespace Endor.CBEL.Interfaces
{
    /// <summary>
    /// This interface is used for computed elements that support overridding.  
    /// This shiould be applied to user-input types (e.g. Single Line Text Elements), but not label types (e.g. Single Line Label Elements).
    /// </summary>
    internal interface ICBELVariableOverridable
    {
        /// <summary>
        /// IsOV indicates if the value has been manually overridden by the user.
        /// IsOV = false means the value is determined based on the default formula for the variable
        /// </summary>
        bool IsOV { get; set; }
    }
}