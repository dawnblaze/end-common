﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    /// <summary>
    /// This interface builds on IAssemblyElementComputed to add the data-type specific Value and DefaultValue fields.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICBELVariableComputed<T> : ICBELVariableComputed
    {
        /// <summary>
        /// Value stores the last computed value.  On access, if the data is dirty, the Compute() function is called
        /// and the Value is updated before being returned.  When value changes (either during the computation or because it was OV)
        /// the function calls all of the DependedOnBy variables and sets them as IsDirty=true so they know they need to recalculate.
        /// </summary>
        T Value { get; set; }

        /// <summary>
        /// Evaulates and returns the value for this variable if not overridden 
        /// </summary>
        T DefaultValue();

        /// <summary>
        /// A list of Validators for this Element.
        /// </summary>
        List<IVariableValidator<T>> Validators { get;  }
    }
}
