﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("EstimateObject")]
    public interface ICBELEstimateObject : ICBELObject
    {
        ICBELEstimateCustomFields CustomFields { get; }
    }
}