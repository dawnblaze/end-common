﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("LaborCustomFields")]
    public interface ICBELLaborCustomFields : ICBELCustomFields
    {
    }
}