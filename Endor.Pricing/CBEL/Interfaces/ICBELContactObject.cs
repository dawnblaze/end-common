﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("ContactObject")]
    public interface ICBELContactObject :ICBELObject
    {
        ICBELContactCustomFields CustomFields { get; }
    }
}