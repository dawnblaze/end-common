﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("EmployeeCustomFields")]
    public interface ICBELEmployeeCustomFields : ICBELCustomFields
    {
    }
}