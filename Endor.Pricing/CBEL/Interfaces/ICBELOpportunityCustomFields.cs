﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("OpportunityCustomFields")]
    public interface ICBELOpportunityCustomFields : ICBELCustomFields
    {
    }
}