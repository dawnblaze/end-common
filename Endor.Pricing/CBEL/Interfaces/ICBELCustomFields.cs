﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("CustomFields")]
    public interface ICBELCustomFields
    {
    }
}