﻿namespace Endor.CBEL.Interfaces
{
    public interface ICompilationResponse
    {
        int ErrorCount { get;}
        bool Success { get; }
    }
}