﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("LaborObject")]
    public interface ICBELLaborObject :ICBELObject
    {
        ICBELLaborCustomFields CustomFields { get; }
        string NameOnInvoice { get; set; }
        string SKU { get; set; }
        string Description { get; set; }
        decimal SetupPrice { get; set; }
        decimal HourlyPrice { get; set; }
        decimal DefinedFixedCost { get; set; }
        decimal DefinedFixedPrice { get; set; }
        decimal MinimumTimeInMin { get; set; }
        decimal BillingIncrementInMin { get; set; }
    }
}