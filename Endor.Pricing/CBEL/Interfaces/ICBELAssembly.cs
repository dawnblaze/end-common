﻿using Endor.CBEL.Common;
using Endor.CBEL.Exceptions;
using Endor.EF;
using System.Collections.Generic;
using Endor.Tenant;
using Endor.CBEL.Elements;
using Endor.Models.Autocomplete;
using Endor.Logging.Client;
using System;
using Endor.Models;

namespace Endor.CBEL.Interfaces
{

    [Autocomplete("AssemblyBase")]
    public interface ICBELAssembly : ICBELComponent, IHasComponents
    {
        ITenantDataCache Cache { get;}
        RemoteLogger Logger { get; }

        ApiContext Context { get; }
        short BID { get; }

        /// <summary>
        /// A dictionary of all of the variables on  the Assembly
        /// </summary>
        VariableDictionary Variables { get; }

        /// <summary>
        /// A dictionary of just of the material on the Assembly.
        /// This is a subset of the Components list.
        /// </summary>
        IEnumerable<ICBELComponentVariable> Materials { get; }

        /// <summary>
        /// A dictionary of just of the material on the Assembly
        /// This is a subset of the Components list.
        /// </summary>
        IEnumerable<ICBELComponentVariable> Labor { get; }

        /// <summary>
        /// A dictionary of just of the material on the Assembly
        /// This is a subset of the Components list.
        /// </summary>
        IEnumerable<ICBELComponentVariable> Machines { get; }

        /// <summary>
        /// A dictionary of just of the material on the Assembly
        /// This is a subset of the Components list.
        /// </summary>
        IEnumerable<ICBELComponentVariable> Subassemblies { get; }

        /// <summary>
        /// Initialize is called after the data is loaded but before it is computed.
        /// </summary>
        void InitializeVariables();

        /// <summary>
        /// This method is used to set all of the initial fields and properties.
        /// Thie routine is overridden in the concrete class created for each assembly.
        /// It is generated based on the contents of the Setters list.
        /// </summary>
        void InitializeData();

        /// <summary>
        /// Compute the value for an assembly and return a Compute Result.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        ICBELComputeResult Compute(ICBELOverriddenValues ovValues = null);

        /// <summary>
        /// Compute the value for an assembly and return a Compute Result.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        ICBELComputeResult Compute(List<VariableOVData> ovValues);

        /// <summary>
        /// Property that indicates if the assembly is fully computed.  This is set to false when any underlying field changes.
        /// </summary>
        bool IsCalculated { get; }

        /// <summary>
        /// Property that indicates if that the assembly component is in the process of being computed.
        /// </summary>
        bool IsCalculating { get; }

        /// <summary>
        /// This property is a reference to the Line Item Object this assembly is on.
        /// This is not the full OrderItem object but a limited model with only a few properties.
        /// </summary>
        ICBELOrderItemObject LineItem { get; }

        /// <summary>
        /// This property is a reference to the Company Object for this assembly.  It is NULL if no company is yet assigned.
        /// This is not the full Company object but a limited model with only a few properties.
        /// This property will be NULL if the company is not assigned to the order yet.
        /// </summary>
        ICBELCompanyObject Company { get; }

        /// <summary>
        /// Boolean method indicating if a company is assigned to the order.  This should be checked before the Company property is accessed.
        /// </summary>
        /// <returns></returns>
        bool HasCompany();

        // run time error handling

        /// <summary>
        /// Helper function to link a dictionary of element names to the underlying elements they represent
        /// </summary>
        /// <param name="dict"></param>
        void LinkElementsByName(VariableLinkDictionary dict);


        // ================================
        // Exception Handling
        // ================================

        /// <summary>
        /// A function to accumulate errors during computation.
        /// </summary>
        /// <param name="ex">Exception to be logged</param>
        void LogException(CBELRuntimeException ex);

        /// <summary>
        /// Method indicating if any computation exceptions exist on the assembly.
        /// </summary>
        bool HasError();

        /// <summary>
        /// Property that gives a concatenated message of all of the exceptions for the assembly.
        /// </summary>
        string ErrorText();

        /// <summary>
        /// A list of all of the exceptions produced during computation.
        /// </summary>
        List<CBELRuntimeException> Errors { get; }

        // ================================
        // Validation Handling
        // ================================

        /// <summary>
        /// A function to accumulate errors during computation.
        /// </summary>
        /// <param name="vf">Validation Failure to be logged</param>
        void LogValidationFailure(ICBELValidationFailure vf);

        /// <summary>
        /// Method indicating if any computation exceptions exist on the assembly.
        /// </summary>
        bool HasValidationFailure();

        /// <summary>
        /// Property that gives a concatenated message of all of the exceptions for the assembly.
        /// </summary>
        string ValidationFailureText();

        /// <summary>
        /// A list of all of the exceptions produced during computation.
        /// </summary>
        List<ICBELValidationFailure> ValidationFailures { get; }

        /// <summary>
        /// The name of the company tier.  
        /// Returns "Default" if Company not assigned.
        /// </summary>
        string TierName { get; }

        /// <summary>
        /// The total cost of the Assembly
        /// Computed as the sum of all of the costs of the Assembly
        /// </summary>
        decimal? TotalCost { get; }

        /// <summary>
        /// The unit cost of the assembly.
        /// Computed as the Cost / ComponentQuantity
        /// </summary>
        decimal? ComputedUnitCost { get; }

        /// <summary>
        /// The total MaterialCost of the Assembly
        /// </summary>
        decimal? MaterialCost { get; }

        /// <summary>
        /// The total MaterialCost of the Assembly
        /// </summary>
        decimal? LaborCost { get; }

        /// <summary>
        /// The total MaterialCost of the Assembly
        /// </summary>
        decimal? MachineCost { get; }

        decimal? FixedPrice { get; set; }
        decimal? FixedMargin { get; set; }
        decimal? FixedMarkup { get; set; }
        decimal? TierPriceTable { get; } // always return 2.00 ($2) for now
        decimal? TierDiscountTable { get; } // always return 0.10 (10%) for now
        decimal? TierMarginTable { get; } // always return 0.50 (50%) for now
        decimal? TierMarkupTable { get; } // always return 0.50 (50%) for now

        /// <summary>
        /// The total Price of the Component
        /// </summary>
        decimal? TotalPrice { get; set; }

        int? CompanyID { get; set; }

        decimal? ParentQuantity { get; set; }
        //decimal? Quantity { get; set; }

        decimal? LineItemQuantity { get; set; }

        bool IsVended { get; set; }

        /// <summary>
        /// Overriden values
        /// </summary>
        List<VariableOVData> UserOVValues { get; set; }
        List<VariableOVData> FormulaOVValues { get; set; }
        void SetOvValues();

        ConsumptionStringVariable ParentVariable { get; set; }
    }
}
