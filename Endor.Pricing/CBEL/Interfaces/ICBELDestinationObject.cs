﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("DestinationObject")]
    public interface ICBELDestinationObject : ICBELObject
    {
    }
}