﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("MaterialCustomFields")]
    public interface ICBELMaterialCustomFields : ICBELCustomFields
    {
    }
}