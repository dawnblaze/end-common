﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("CreditMemoObject")]
    public interface ICBELCreditMemoObject : ICBELObject
    {
    }
}