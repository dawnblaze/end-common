﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("EstimateCustomFields")]
    public interface ICBELEstimateCustomFields : ICBELCustomFields
    {
    }
}