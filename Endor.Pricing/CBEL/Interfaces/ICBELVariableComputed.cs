﻿using Endor.CBEL.Common;
using Endor.Models;
using Endor.Units;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    /// <summary>
    /// This interface is a base type for IAssemblyElementComputed.  
    /// It contains all of the properties that are not data specific and can be used in typecasting 
    /// computed elements for those elements.
    /// </summary>
    public interface ICBELVariableComputed : ICBELVariable
    {
        /// <summary>
        /// The DataType ID the variable returns.
        /// </summary>
        DataType DataType { get; }

        /// <summary>
        /// The text name for the DataType (e.g., String, Number, Material, etc.)
        /// This metadata is useful for debugging.
        /// </summary>
        string DataTypeName { get; }

        /// <summary>
        /// The original formula (in CBEL) for the variable.
        /// This metadata is useful for debugging.
        /// </summary>
        string FormulaText { get; }

        /// <summary>
        /// Recompute the value and set it as the current value.  
        /// If the value changes, this also triggers the PropertyChanged event on the Assembly.
        /// Passes any computation errors to the assembly LogException();
        /// Validate() is called automatically at the end of the routine.
        /// Returns true if no errors or validations fail.
        /// </summary>
        void Compute();

        VariableOVData UserOVValues { get; set; }
        VariableOVData FormulaOVValues { get; set; }

        /// <summary>
        /// Validates the current value against the validation rules.
        /// Passes any validation error to the assembly LogValidationException();
        /// Returns true if all validations pass.
        /// </summary>
        bool Validate();

        /// <summary>
        /// A dictionary of Variables on the Assembly that the formula needs in it's evaluation.
        /// </summary>
        VariableLinkDictionary ReliesOn { get; }

        /// <summary>
        /// A dictionary of Variables on the Assembly that reference this variable.
        /// </summary>
        VariableLinkDictionary ReferencedBy { get; }

        /// <summary>
        /// Property that indicates if the field IsRequired.
        /// </summary>
        bool IsRequired { get; set; }

        /// <summary>
        /// A list of external dependencies that are referenced by the formula.
        /// E.g. (Company.Name,  Order.Salesperson.FirstName, etc.)
        /// The value for all external references are stored in parent the Assembly component.
        /// </summary>
        List<string> ExternalDependencies { get; }
   

        /// <summary>
        /// Returns the value of the variable as a Dynamic type.
        /// </summary>
        dynamic AsDynamic { get; set; }  

        /// <summary>
        /// Returns the value as a decimal.  
        /// If value == null, then null is returned without error.
        /// Throws an error if value can't be converted to a number.  
        /// </summary>
        /// <returns>decimal?</returns>
        decimal? AsNumber { get; set; }

        /// <summary>
        /// Gets and Sets the value as a string.  
        /// If value == null, then null is returned without error.
        /// For numbers, a general format is used.
        /// For booleans, the value "true" or "false" is returned.
        /// For objects, the Name property of the object is returned.
        /// </summary>
        /// <returns>string</returns>
        string AsString { get; set; }

        /// <summary>
        /// Gets and Sets value as string for alt text
        /// </summary>
        string AltAsString { get; }

        /// <summary>
        /// Returns the value as a boolean.  
        /// If value == null, then null is returned without error.
        /// For numbers, 0 == false, non-zero == true.
        /// For strings, the first non-white space character is checked.
        ///    true == 'TtYy1',  false == 'FfNn0',  other values throw an error
        /// Throws an error if value can't be converted to a number.  
        /// </summary>
        /// <returns>bool</returns>
        bool? AsBoolean { get; set; }

        /// <summary>
        /// Returns the value as an ICBELObject.
        /// Throws an error if the DataType is not an Object.
        /// If value == null, then null is returned without error.
        /// </summary>
        /// <returns>ICBELObject</returns>
        ICBELObject AsObject();

        /// <summary>
        /// Indicates whether there is a value (default of computed)
        /// It is the negative of IsNull.  Either can be used based on the logic.
        /// </summary>
        bool HasValue { get; }

        /// <summary>
        /// Indicates whether the value is NULL
        /// It is the negative of HasValue.  Either can be used based on the logic.
        /// </summary>
        bool IsNull { get; }

        /// <summary>
        /// IsCalculated indicates that the variable's value needs to be recomputed.  This can be because it has
        /// never been computed or because a variable that it depends on in it's formula has changed.
        /// Setting IsCalculated does not automatically trigger a recompute.This is done on the Value getter when
        /// IsCalculated = false.
        /// </summary>
        bool IsCalculated { get; set;  }

        /// <summary>
        /// Indicates if the variable is in the middle of calculating it's value.
        /// This is used to prevent recursion.
        /// </summary>
        bool IsCalculating { get; }

        bool IsOV { get; set; }

        bool? IsIncluded { get; set; }
        bool? IsIncludedOV { get; set; }

        void ResetValue();

        Unit ValueUnit();
        string GetResultValue();
    }
}
