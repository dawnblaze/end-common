﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("OrderObject")]
    public interface ICBELOrderObject : ICBELObject
    {
        ICBELOrderCustomFields CustomFields { get; }
    }
}