﻿using Endor.CBEL.Common;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    /// <summary>
    /// This interface is a base type for IAssemblyElementComputed.  
    /// It contains all of the properties that are not data specific and can be used in typecasting 
    /// computed elements for those elements.
    /// </summary>
    public interface ICBELVariableTable<T, RowDataType, ColumnDataType> 
    {
        /// <summary>
        /// The name of the variable that specifies the Row values.
        /// </summary>
        string RowVariableName { get; }

        /// <summary>
        /// The name of the variable that specifies the Column values.
        /// </summary>
        string ColumnVariableName { get; }

        /// <summary>
        /// This function is used to look up a table value with a custom set of options.  
        /// </summary>
        /// <param name="table">The table variable</param>
        /// <param name="RowValue">The value of the Row Variable</param>
        /// <param name="ColValue">The value of the Column Variable</param>
        /// <param name="rowmatch">Enum indicating how non-exact matches are handled for the Row Variable</param>
        /// <param name="colmatch">Enum indicating how non-exact matches are handled for the Column Variable</param>
        /// <returns></returns>
        T TableLookup(RowDataType RowValue, ColumnDataType ColValue, AssemblyTableMatchType rowmatch = AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType colmatch = AssemblyTableMatchType.ExactMatch);

        /// <summary>
        /// This function is used to look up a table value with the default table values.
        /// </summary>
        /// <returns></returns>
        T TableLookup();


        /// <summary>
        /// Enum value specifying the type of rounding to use when the Row Variable value does 
        /// not exactly match any value in the table.
        ///  0 = Exact Match Required
        ///  1 = Use Next Lower Value
        ///  2 = Use Next Higher Value
        ///  3 = Interpolate (only valid for number types)
        /// </summary>
        AssemblyTableMatchType RowMatchOption { get; }

        /// <summary>
        /// Enum value specifying the type of rounding to use when the Column Variable value does 
        /// not exactly match any value in the table.
        ///  0 = Exact Match Required
        ///  1 = Use Next Lower Value
        ///  2 = Use Next Higher Value
        ///  3 = Interpolate (only valid for number types)
        /// </summary>
        AssemblyTableMatchType ColumnMatchOption { get; }


        // ==============================================================
        // The following properties exist in the ICBELVariableComputed
        // but are not used or have fixed values for Table Variables
        // ==============================================================

        /// <summary>
        /// The formula (in CBEL) for the variable table.
        /// Note, for a table this is always
        ///       $"=TableLookup({Name}, {RowVariable}, {ColumnVariable}, {TableOptions})"
        /// </summary>
        /// string FormulaText { get; }

        /// <summary>
        /// A dictionary of Variables on the Assembly that the formula needs in it's evaluation.
        /// This should contain Row and Column Variable Names
        /// </summary>
        /// ElementDictionary ReliesOn { get; }

        /// <summary>
        /// A list of external dependencies that are referenced by the formula.
        /// For a Table, this will always be empty/null.
        /// </summary>
        /// List<string> ExternalDependencies { get; }
    }
}
