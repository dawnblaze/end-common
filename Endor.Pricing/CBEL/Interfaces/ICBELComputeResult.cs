﻿using Endor.CBEL.Exceptions;
using System.Collections.Generic;

namespace Endor.CBEL.Interfaces
{

    public interface ICBELComputeResult
    {
        ICBELAssembly Assembly { get; }

        List<IVariableData> VariableData { get; }

        bool HasErrors { get; }

        string ErrorText { get; }

        List<CBELRuntimeException> Errors { get; }


        bool HasValidationFailures { get; }

        string ValidationFailureText { get; }

        List<ICBELValidationFailure> ValidationFailures { get; }

        List<ICBELComputeResult> Results { get; }
    }
}