﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("CBELObject")]
    public interface ICBELObject
    {
        /// <summary>
        /// The ID of the object.  
        /// This is NULL if it has not been saved or if it is saved as JSON data in the parent object
        /// </summary>
        int? ID { get; }

        /// <summary>
        /// The ClassTypeID of the object.
        /// </summary>
        [AutocompleteIgnore]
        int? ClassTypeID { get; }

        /// <summary>
        /// The Parent object this object is a part of.
        /// </summary>
        ICBELObject ParentObject { get; }

        /// <summary>
        /// The display name of the object.
        /// </summary>
        string Name { get; }
    }
}