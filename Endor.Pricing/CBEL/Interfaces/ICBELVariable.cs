﻿using Endor.CBEL.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    public interface ICBELVariable
    {
        /// <summary>
        /// Reference to the Assembly that contains this variable.
        /// </summary>
        ICBELAssembly Parent { get; }

        /// <summary>
        /// The display label for the variable.
        /// </summary>
        string Label { get; }

        /// <summary>
        /// The name of variable used in formulas. This is also the name of the property on 
        /// the Assembly object that references this variable.  It is defined on the Assembly as:
        ///    public {ElementPropertyClass} {name} {get; private set;}
        /// </summary>
        string Name { get; }
    }
}
