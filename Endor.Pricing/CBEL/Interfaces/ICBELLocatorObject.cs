﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("LocatorObject")]
    public interface ICBELLocatorObject : ICBELObject
    {
    }
}