﻿using Endor.Models.Autocomplete;

namespace Endor.CBEL.Interfaces
{
    [Autocomplete("PhoneObject")]
    public interface ICBELPhoneObject : ICBELObject
    {
    }
}