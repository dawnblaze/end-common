﻿using Endor.Models;
using Endor.Units;

namespace Endor.CBEL.Interfaces
{
    /// <summary>
    /// This Interface describes the formula for a variable.  This may be used for it's own consumption or 
    /// for overrides to the Linked Component variables. The FormulaType determines how it is used.
    /// 
    /// See https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/979009618/AssemblyVariableFormula+Object
    /// </summary>
    public interface ICBELVariableFormula
    {

        /// <summary>
        /// Reference to the parent variable.  All overrides set the values on the linked component for this variable.
        /// The ParentVariable must be one of the following types: Dropdown List of Machines or Assemblies, Directly Linked Assembly, Directly Linked Machine.
        /// </summary>
        ICBELVariable ParentVariable { get; set; }

        /// <summary>
        /// Enum describing how the formula/value is used.
        ///     public enum AssemblyFormulaUseType : byte
        ///     {
        ///        DefaultValue = 0,
        ///        Consumption = 1,
        ///        IsEnabled = 2,
        ///        IsRequired = 3,
        ///        IsVisible = 4,
        ///        IsLinked = 10,
        ///        LinkedVariableOV = 11
        ///     }
        /// </summary>
        AssemblyFormulaUseType FormulaUseType { get; set; }

        /// <summary>
        /// Enum describing the method by which the formula is to be evaluated.
        ///   public enum AssemblyFormulaEvalType : byte
        ///   {
        ///       Constant = 0,
        ///       MappedVariable = 1,
        ///       CBEL = 2,
        ///       CSharp = 3,
        ///       CFL = 4
        ///   }
        /// </summary>
        AssemblyFormulaEvalType FormulaEvalType { get; set; }

        /// <summary>
        /// The datatype of the value or formula.  
        /// </summary>
        DataType DataType { get; set; }

        /// <summary>
        /// The value or formula the child variable is being set to.  This can be a constant or a formula, and follows the same rules
        /// and logic as other variable formulas.
        /// </summary>
        string FormulaText { get; set; }

        /// <summary>
        /// The Unit associated with the DefaultValue.  This only applies when the ChildVariable is a measurement, and should be null otherwise.
        /// Defaults to the same unit as ChildVariable (or null, if the ChildVariable is not a measurement).
        /// </summary>
        Unit? Unit { get; set; }

        /// <summary>
        /// A flag indicating if the DefaultValue is a formula or constant. This is a computed field based on the FormulaEvalType
        /// </summary>
        bool IsFormula { get; }

        /// <summary>
        /// The property name of the child variable being set, if applicable.
        /// </summary>
        string ChildVariableName { get; set; }

        string ValueFunction { get; set; }
    }
}