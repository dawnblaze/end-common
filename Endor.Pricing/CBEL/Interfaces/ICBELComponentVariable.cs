﻿using Endor.CBEL.Elements;
using Endor.Models;
using Endor.Pricing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    public interface ICBELComponentVariable
    {
        OrderItemComponentType ComponentClassType { get; }

        NumberVariable ConsumptionQuantity { get;  }

        ICBELComponent Component { get; set; }
       
        /// <summary>
        /// The total cost of the Component
        /// Computed ConsumptionDefaultValue * Component.Cost
        /// </summary>
        decimal? Cost { get; }

        /// <summary>
        /// The unit cost of the component.
        /// Computed as the Cost / ConsumptionValue
        /// </summary>
        decimal? ComputedUnitCost { get; }

        /// <summary>
        /// Unit cost of the component is overridden.
        /// </summary>
        bool? UnitCostIsOverridden { get; }

        /// <summary>
        /// The unit price of the component.
        /// </summary>
        decimal? ComputedUnitPrice { get; }

        /// <summary>
        /// Price
        /// </summary>
        decimal? Price { get; }
        
        void LoadLinkedComponent();

        List<VariableFormula> Formulas { get; set; }

        /// <summary>
        /// roll up linked price and cost
        /// </summary>
        bool RollupLinkedPriceAndCost { get; set; }
    }
}
