﻿using System.Collections.Generic;

namespace Endor.CBEL.Interfaces
{
    public interface ICBELOverriddenValues
    {
        List<IVariableData> VariableData { get; }
        List<ICBELOverriddenValues> OVChildValues { get; }
        int? ComponentID { get; }
        string VariableName { get; }
        decimal? Quantity { get; set; }
        bool? QuantityOV { get; set; }
        bool? IsIncluded { get; set; }
        bool? IsIncludedOV { get; set; }
        decimal? ParentQuantity { get; set; }  // Default to NULL
        bool IsVended { get; set; }   // Default to FALSE
        int? CompanyID { get; set; } // Default to NULL
        decimal? LineItemQuantity { get; set; }
        decimal? CostUnit { get; set; }
        bool? CostUnitOV { get; set; }
    }
}