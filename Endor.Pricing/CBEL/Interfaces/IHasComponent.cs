﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Interfaces
{
    public interface IHasComponent<T>
    {
        T TypedComponent { get; }
    }
}
