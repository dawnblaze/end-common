﻿using Endor.CBEL.Metadata;

namespace Endor.CBEL.Operators
{
    public class CBELOperatorInfo : CBELMemberInfo
    {
        public override string MemberType => "Operator";
    }
}
