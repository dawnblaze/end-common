﻿using Endor.CBEL.Interfaces;
using System;

namespace Endor.CBEL.Validation
{
    /// <summary>
    /// A validation failure class generated when the IsRequiredValidation fails.
    /// </summary>
    [Serializable]
    public class IsRequiredValidationFailure : ValidationFailureBase
    {
        public IsRequiredValidationFailure(ICBELVariable element) : base(element) { }

        public override string Message
        {
            get
            {
                if(ElementComponent != null)
                    return $"Value Required for Element {ElementComponent.Name}.";

                else
                    return "Value Required but not found.";
            }
        }
    }
}