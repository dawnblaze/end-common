﻿using Endor.CBEL.Interfaces;

namespace Endor.CBEL.Validation
{
    /// <summary>
    /// This class validates that the variable has a non-null/empty value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IsRequiredValidator<T> : IVariableValidator<T>
    {
        public bool Validate(ICBELVariableComputed<T> element, out ICBELValidationFailure vf)
        {
            bool isEmptyString = false;
            if(element != null)
            {
               isEmptyString = typeof(T) == typeof(System.String) && string.IsNullOrEmpty(element.Value as string);
            }

            if (element == null)
            {
                vf = new IsRequiredValidationFailure(element);
                return false;
            }
            else if (!element.HasValue || isEmptyString)
            {
                vf = new IsRequiredValidationFailure(element);
                return false;
            }
            else
            {
                vf = null;
                return true;
            }
        }

    }
}
