﻿using Endor.CBEL.Interfaces;
using System;

namespace Endor.CBEL.Validation
{
    // The base class for all CBEL Validation Exceptions.
    [Serializable]
    public abstract class ValidationFailureBase : ICBELValidationFailure
    {
        public ValidationFailureBase(ICBELVariable element)
        {
            ElementComponent = element;
            AssemblyComponent = element?.Parent;
        }

        public ValidationFailureBase(ICBELAssembly assemblycomponent)
        {
            AssemblyComponent = assemblycomponent;
        }

        /// <summary>
        /// Gets a message that describes the current exception.
        /// </summary>
        public abstract string Message { get; }

        /// <summary>
        /// This property adds the Assembly Component to the exception if available.
        /// </summary>
        public ICBELAssembly AssemblyComponent { get; private set; }

        /// <summary>
        /// This property adds the Element/Variable Component to the exception if available.
        /// </summary>
        public ICBELVariable ElementComponent { get; private set; }
    }
}