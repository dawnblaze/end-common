﻿using System;
using System.Collections.Generic;
using System.Linq;
using Endor.CBEL.Metadata;
using Irony.Ast;
using Irony.Parsing;

namespace Endor.CBEL.Grammar
{
    public class TAssemblyPropertyNode : TASTNodeBase, IASTNodeExtensions
    {
        private string[] GetTokenText(ParseTreeNode node)
        {
            if (node.Token == null)
                return node.ChildNodes.SelectMany(n => GetTokenText(n)).ToArray();

            return new string[] { node.Token?.Text };
        }

        private void ParseKnownProperties(ParseTreeNode treeNode, string[] variableToken)
        {
            CBELGrammar grammar = (CBELGrammar)treeNode.Term.Grammar;
            SortedDictionary<string, ICBELMember> memberList = grammar.KnownMemberList;
            ICBELMember member = null;

            foreach (var _token in _tokens.Select((value, index) => new { index, value }))
            {
                if (memberList == null) return;

                member = memberList
                            .Where(
                                n => n.Value.Name.ToLower() == _token.value.ToLower()
                                && (
                                       (n.Value.MemberObjectType & CBELMemberObjectType.Variable) == 0 
                                )
                            ).FirstOrDefault().Value;

                if (member == null) return;

                variableToken[_token.index] = member.Name;

                memberList = member.Members;
            }
        }

        private string _tokenText;
        private string[] _tokens;

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            _tokens = treeNode.ChildNodes.SelectMany(n => GetTokenText(n)).ToArray();
            ParseKnownProperties(treeNode, _tokens);
            _tokenText = string.Join(".", _tokens);

            base.Init(context, treeNode);

            AsString = _tokenText;
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCBEL(TPrettyPrinter printer)
        {
            printer.Write(_tokenText);
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCS(TPrettyPrinter printer)
        {
            printer.Write(_tokenText);
        }
    }
}
