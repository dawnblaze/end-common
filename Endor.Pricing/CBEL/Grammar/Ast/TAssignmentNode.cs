﻿using Irony.Ast;
using Irony.Parsing;
using Irony.Interpreter.Ast;

// <copyright file="TAssignmentNode.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{
    /// <summary>
    /// Adds ToCBEL and ToCS methods to Irony Assignment AST Node.
    /// </summary>
    public class TAssignmentNode : AssignmentNode, IASTNodeExtensions
    {
        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <returns>String representation of node as CFL source</returns>
        public string ToCBEL()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCBEL(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public virtual void ToCBEL(TPrettyPrinter printer)
        {
            (this.Target as IASTNodeExtensions).ToCBEL(printer);
            printer.Write(string.Format(" {0} ", this.AssignmentOp));
            (this.Expression as IASTNodeExtensions).ToCBEL(printer);
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <returns>String representation of node as C# source</returns>
        public string ToCS()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCS(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public virtual void ToCS(TPrettyPrinter printer)
        {
            string operatorCSharp = this.AssignmentOp;
            ////if (this.AssignmentOp.Equals("/="))
            ////{
            ////    CSharpOp = "/= (double)";
            ////}

            // output Left Side
            (this.Target as IASTNodeExtensions).ToCS(printer);

            // output operator
            printer.Write(string.Format(" {0} ", operatorCSharp));

            // output right side
            (this.Expression as IASTNodeExtensions).ToCS(printer);
        }
    }
}
