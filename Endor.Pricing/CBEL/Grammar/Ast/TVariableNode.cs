﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endor.CBEL.Grammar;
using Endor.CBEL.Metadata;
using Endor.Models;
using Irony.Ast;
using Irony.Parsing;

namespace Endor.CBEL.Grammar
{
    public class TVariableNode : TStatementListNode, IASTNodeExtensions
    {
        private string _variableTokenText;
        private string[] _variableTokens;

        private string[] GetTokenText(ParseTreeNode node)
        {
            if (node.Token == null)
                return node.ChildNodes.SelectMany(n => GetTokenText(n)).ToArray();

            return new string[] { node.Token?.Text };
        }

        private void ParseVariable(ParseTreeNode treeNode, string[] variableToken)
        {
            CBELGrammar grammar = (CBELGrammar)treeNode.Term.Grammar;
            SortedDictionary<string, ICBELMember> memberList = grammar.KnownMemberList;
            ICBELMember member = null;

            string _name;
            CBELMemberObjectType _memberObjectType;

            foreach (var _token in _variableTokens.Select((value, index) => new { index, value }))
            {
                if (memberList == null) return;

                _name = _token.value;
                _memberObjectType = _token.index == 0 ? CBELMemberObjectType.Variable : 0;

                member = memberList
                            .Where(
                                n => n.Value.Name.ToLower() == _name.ToLower()
                                && ( 
                                        n.Value.MemberObjectType == (_memberObjectType | CBELMemberObjectType.Scalar)
                                    || n.Value.MemberObjectType == (_memberObjectType | CBELMemberObjectType.Object)
                                )
                            ).FirstOrDefault().Value;

                if (member == null) return;

                variableToken[_token.index] = member.MemberType == CBELMemberType.Function ? $"{member.Name}()" : member.Name;

                memberList = member.Members;
            }
        }

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            _variableTokens = treeNode.ChildNodes.SelectMany(n => GetTokenText(n)).ToArray();

            ParseVariable(treeNode, _variableTokens);

            _variableTokenText = string.Join(".", _variableTokens);

            base.Init(context, treeNode);

            AsString = _variableTokenText;
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCBEL(TPrettyPrinter printer)
        {
            printer.Write(_variableTokenText);
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCS(TPrettyPrinter printer)
        {
            List<string> tokens = new List<string>(_variableTokens);

            if (tokens.Count > 0)
            {
                CBELGrammar grammar = (CBELGrammar)Term.Grammar;

                ICBELMember baseMember = grammar.KnownMemberList
                                                .Where(n => n.Value.Name.ToLower() == tokens[0].ToLower()
                                                            &&
                                                            (n.Value.DataType == DataType.Assembly || n.Value.DataType == DataType.MachinePart)
                                                            && (
                                                                    n.Value.MemberObjectType == (CBELMemberObjectType.Variable | CBELMemberObjectType.Scalar)
                                                                    || n.Value.MemberObjectType == (CBELMemberObjectType.Variable | CBELMemberObjectType.Object)
                                                               )
                                                      )
                                                .FirstOrDefault()
                                                .Value;

                if (baseMember != null)
                {
                    if (baseMember.DataType == DataType.Assembly)
                        tokens.Insert(1, "AssemblyComponent");
                    if (baseMember.DataType == DataType.MachinePart)
                        tokens.Insert(1, "Component");
                }
            }

            printer.Write(string.Join("?.", tokens));
        }
    }
}
