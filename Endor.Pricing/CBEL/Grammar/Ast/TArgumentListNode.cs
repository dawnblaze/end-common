﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace Endor.CBEL.Grammar
{
    public class TArgumentListNode : TStatementListNode, IASTNodeExtensions
    {
        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);

            string argumentsText = $"({string.Join(",", treeNode.ChildNodes.Select(n => n.Token?.Text))})";

            AsString = argumentsText;
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCBEL(TPrettyPrinter printer)
        {
            bool isFirst = true;
            foreach (var childNode in ChildNodes)
            {
                string nodeAsCBEL = null;
                if (childNode is IASTNodeExtensions astNodeExtensions)
                    nodeAsCBEL = astNodeExtensions.ToCBEL();
                else if (childNode is LiteralValueNode node)
                    nodeAsCBEL = node.AsString;

                if (!string.IsNullOrWhiteSpace(nodeAsCBEL))
                {
                    if (isFirst)
                    {
                        printer.Write("(");
                        isFirst = false;
                    }
                    else
                        printer.Write(",");

                    printer.Write(nodeAsCBEL);
                }
            }

            if (!isFirst)
                printer.Write(")");
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCS(TPrettyPrinter printer)
        {
            bool isFirst = true;
            foreach (var childNode in ChildNodes)
            {
                string nodeAsCS = null;
                if (childNode is IASTNodeExtensions astNodeExtensions)
                    nodeAsCS = astNodeExtensions.ToCS();
                else if (childNode is LiteralValueNode node)
                    nodeAsCS = node.AsString;

                if (!string.IsNullOrWhiteSpace(nodeAsCS))
                {
                    if (isFirst)
                    {
                        printer.Write("(");
                        isFirst = false;
                    }
                    else
                        printer.Write(",");

                    printer.Write(nodeAsCS);
                }
            }

            if (!isFirst)
                printer.Write(")");
        }
    }
}
