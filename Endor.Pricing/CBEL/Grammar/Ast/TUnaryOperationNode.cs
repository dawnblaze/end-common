﻿using Irony.Ast;
using Irony.Parsing;
using Irony.Interpreter.Ast;

// <copyright file="TUnaryOperationNode.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{
    using Irony.Ast;

    /// <summary>
    /// Adds ToCBEL and ToCS methods to Irony Unary Operation AST Node.
    /// </summary>
    public class TUnaryOperationNode : UnaryOperationNode, IASTNodeExtensions
    {
        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <returns>String representation of node as CFL source</returns>
        public string ToCBEL()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCBEL(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public virtual void ToCBEL(TPrettyPrinter printer)
        {
            printer.Write(this.OpSymbol);
            if (this.OpSymbol.Equals("not"))
            {
                printer.Write(" ");
            }

            (this.Argument as IASTNodeExtensions).ToCBEL(printer);
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <returns>String representation of node as C# source</returns>
        public string ToCS()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCS(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public virtual void ToCS(TPrettyPrinter printer)
        {
            // unaryOperator.Rule = ToTerm("-") | "+" | "!" | "not";
            if (this.OpSymbol.Equals("not"))
            {
                printer.Write("!");
            }
            else
            {
                printer.Write(this.OpSymbol);
            }

            (this.Argument as IASTNodeExtensions).ToCS(printer);
        }
    }
}
