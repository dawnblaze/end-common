﻿using System;
using System.Collections.Generic;
using System.Text;
using Irony.Ast;
using Irony.Parsing;

namespace Endor.CBEL.Grammar
{
    public class TBooleanLiteral : TASTNodeBase, IASTNodeExtensions
    {
        private string _booleanLiteralValue = null;
        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            _booleanLiteralValue = treeNode.ChildNodes[0].Token.Text.ToLower();
            this.AddChild("boolean", treeNode.ChildNodes[0]);
            AsString = _booleanLiteralValue;
        }

        public override void ToCBEL(TPrettyPrinter printer)
        {
            printer.Write(_booleanLiteralValue);
        }

        public override void ToCS(TPrettyPrinter printer)
        {
            printer.Write(_booleanLiteralValue);
        }
    }
}
