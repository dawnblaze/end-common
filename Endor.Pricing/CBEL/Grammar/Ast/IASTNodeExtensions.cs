﻿// <copyright file="IASTNodeExtensions.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{
    /// <summary>
    /// Defines Cryious AST Node extensions.
    /// </summary>
    public interface IASTNodeExtensions
    {
        /// <summary>
        /// Formats the node (and any children) as CFL code..
        /// </summary>
        /// <returns>String representation of node as CFL source</returns>
        string ToCBEL();

        /// <summary>
        /// Formats the node (and any children) as CFL code..
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        void ToCBEL(TPrettyPrinter printer);

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <returns>String representation of node as C# source</returns>
        string ToCS();

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        void ToCS(TPrettyPrinter printer);
    }
}
