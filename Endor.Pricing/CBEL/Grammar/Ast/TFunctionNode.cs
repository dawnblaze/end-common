﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.CBEL.Metadata;
using Irony.Ast;
using Irony.Parsing;

namespace Endor.CBEL.Grammar
{
    public class TFunctionNode : TASTNodeBase, IASTNodeExtensions
    {
        private string _token = null;

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);

            _token = treeNode.ChildNodes[0].Term.Name;

            AsString = _token;
        }

        public override void ToCBEL(TPrettyPrinter printer)
        {
            printer.Write(this._token);
        }

        public override void ToCS(TPrettyPrinter printer)
        {
            //Check against the KnownMemberList (caseInsenstive) for methods
            var sortedKnownMemberList = Metadata.MetadataHelper.KnownMemberList();
            if (sortedKnownMemberList.ContainsKey(this._token.ToLower()))
            {
                //Get the real name from the member list
                ICBELMember caseFixedToken = sortedKnownMemberList.GetValueOrDefault(this._token.ToLower());
                if (caseFixedToken.Members != null && caseFixedToken.Members.GetValueOrDefault("extensionAttribute") != null)
                    printer.Write("this." + caseFixedToken.Name);
                else if (string.IsNullOrWhiteSpace(caseFixedToken.MethodClass))
                    printer.Write(caseFixedToken.Name);
                else
                    printer.Write($"{caseFixedToken.MethodClass}.{caseFixedToken.Name}");
            }
            else
            {
                //TODO: Not sure where to log this exception or return as exception
                //      For now just returning empty string as this exceeption will be
                //      caught while compiling
            }
            
        }
    }
}
