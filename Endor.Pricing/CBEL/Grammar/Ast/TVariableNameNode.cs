﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Ast;
using Irony.Parsing;

namespace Endor.CBEL.Grammar
{
    public class TVariableNameNode : TASTNodeBase, IASTNodeExtensions
    {
        private string _token = null;

        private string GetTokenText(ParseTreeNode node)
        {
            if (node.Token == null)
                return string.Join(".", node.ChildNodes.Select(n => GetTokenText(n)));

            return node.Token?.Text;
        }
        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);

            _token = string.Join(".", treeNode.ChildNodes.Select(n => GetTokenText(n)));

            AsString = _token;
        }

        public override void ToCBEL(TPrettyPrinter printer)
        {
            printer.Write(this._token);
        }

        public override void ToCS(TPrettyPrinter printer)
        {
            printer.Write(this._token);
        }
    }
}
