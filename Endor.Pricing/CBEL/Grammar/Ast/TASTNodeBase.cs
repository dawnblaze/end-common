﻿using Irony.Ast;
using Irony.Parsing;
using Irony.Interpreter.Ast;

// <copyright file="TASTNodeBase.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{

    /// <summary>
    /// Base node for custom AST nodes.
    /// </summary>
    public abstract class TASTNodeBase : AstNode, IASTNodeExtensions
    {
        /// <summary>
        /// Formats the node (and any children) as CBEL code.
        /// </summary>
        /// <returns>String of node as CFL source.</returns>
        public string ToCBEL()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCBEL(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code..
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public abstract void ToCBEL(TPrettyPrinter printer);

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <returns>String of node as C# source.</returns>
        public string ToCS()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCS(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public abstract void ToCS(TPrettyPrinter printer);
    }
}

