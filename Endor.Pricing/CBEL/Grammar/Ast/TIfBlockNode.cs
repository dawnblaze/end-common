﻿using System.Linq;
using Irony.Ast;
using Irony.Parsing;
using Irony.Interpreter.Ast;

// <copyright file="TIfBlockNode.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{
    /// <summary>
    /// AST Node representing an 'if' block.
    /// </summary>
    public class TIfBlockNode : TASTNodeBase
    {

        /// <summary>
        /// If Condition for block to be executed.
        /// </summary>
        private AstNode fCondition;

        /// <summary>
        /// Optional Else statement.
        /// </summary>
        private AstNode fIfTrue;

        /// <summary>
        /// Optional list of Elseif statements.
        /// </summary>
        private AstNode fIfFalse;

        /// <summary>
        /// Initializes the For AST node.
        /// </summary>
        /// <param name="context">The parsing context.</param>
        /// <param name="treeNode">The Parse Tree Node to be converted to an AST Node.</param>
        /// 
        // public override void Init(Irony.Parsing.ParsingContext context, Irony.Parsing.ParseTreeNode treeNode)
        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            ParseTreeNode node0 = treeNode.ChildNodes[0]; //IF Keyword
            ParseTreeNode node1 = treeNode.ChildNodes[1]; //Condition
            ParseTreeNode node2 = treeNode.ChildNodes[2]; //IF True
            ParseTreeNode node3 = treeNode.ChildNodes[3]; //IF False

            fCondition = AddChild("condition", treeNode.ChildNodes[1]);
            fIfTrue = AddChild("iftrue", treeNode.ChildNodes[2]);
            fIfFalse = AddChild("iffalse", treeNode.ChildNodes[3]);

            this.AsString = "If Block";
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCBEL(TPrettyPrinter printer)
        {
            string cond, ifTrue, ifFalse = null;

            if (this.fCondition is IASTNodeExtensions fConditionExt)
            {
                cond = fConditionExt.ToCBEL();
            }
            else if (this.fCondition is LiteralValueNode literalValueNode)
            {
                cond = literalValueNode.AsString;
            }
            else
            {
                cond = "";
            }

            if (this.fIfTrue != null && this.fIfTrue is IASTNodeExtensions fIfTrueExt)
            {
                ifTrue = fIfTrueExt.ToCBEL();
            }
            else if (this.fIfTrue is LiteralValueNode literalValueNode)
            {
                ifTrue = literalValueNode.AsString;
            }
            else
            {
                ifTrue = "";
            }

            if (this.fIfFalse != null && this.fIfFalse is IASTNodeExtensions fIfFalseExt)
            {
                ifFalse = fIfFalseExt.ToCBEL();
            }
            else if (this.fIfFalse is LiteralValueNode literalValueNode)
            {
                ifFalse = literalValueNode.AsString;
            }
            else
            {
                ifFalse = "";
            }

            printer.Write($"IF({cond},{ifTrue},{ifFalse})");
        }

        /// <summary>
        /// Formats the node (and any children) as C-Sharp code
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCS(TPrettyPrinter printer)
        {
            string cond, ifTrue, ifFalse = null;

            if (this.fCondition is IASTNodeExtensions fConditionExt)
            {
                cond = fConditionExt.ToCS();
            }
            else if (this.fCondition is LiteralValueNode literalValueNode)
            {
                cond = literalValueNode.AsString;
            }
            else
            {
                cond = "";
            }

            if (this.fIfTrue != null && this.fIfTrue is IASTNodeExtensions fIfTrueExt)
            {
                ifTrue = fIfTrueExt.ToCS();
            }
            else if (this.fIfTrue is LiteralValueNode literalValueNode)
            {
                ifTrue = literalValueNode.AsString;
            }
            else
            {
                ifTrue = "";
            }

            if (this.fIfFalse != null && this.fIfFalse is IASTNodeExtensions fIfFalseExt)
            {
                ifFalse = fIfFalseExt.ToCS();
            }
            else if (this.fIfFalse is LiteralValueNode literalValueNode)
            {
                ifFalse = literalValueNode.AsString;
            }
            else
            {
                ifFalse = "";
            }
            printer.Write($"(({cond}) ? ({ifTrue}) : ({ifFalse}))");
        }
    }
}
