﻿// <copyright file="TPrettyPrinter.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{
    using System.Text;

    /// <summary>
    /// Helper class to pretty print source code from AST tree.
    /// </summary>
    public class TPrettyPrinter
    {
        /// <summary>
        /// StringBuilder used to build source.
        /// </summary>
        private StringBuilder fPrettyString;

        /// <summary>
        /// Current count of padding.  
        /// This is how many levels of indentation we are in the source.
        /// </summary>
        private int fPadCount;

        /// <summary>
        /// Value used for padding.
        /// </summary>
        private string fPadValue; // "\t"

        /// <summary>
        /// Current padding.
        /// </summary>
        private string fPadding;

        /// <summary>
        /// Current status of padding.  
        /// If on a new line, then pad.
        /// Else if we are inline do not pad.
        /// </summary>
        private bool fToPad;

        /// <summary>
        /// Current status of string
        /// If Uppercase is true format the string to Uppercase
        /// Else return without any changes to the string.
        /// </summary>
        private bool forceToUpper;

        /// <summary>
        /// Initializes a new instance of the <see cref="TPrettyPrinter"/> class.
        /// </summary>
        public TPrettyPrinter()
        {
            this.fPrettyString = new StringBuilder();
            this.fPadCount = 0;
            this.fPadValue = "    ";
            this.fPadding = string.Empty;
            this.fToPad = false;
            this.forceToUpper = false;
        }

        /// <summary>
        /// Tree of Tokens - this property is set by the calling object
        /// and, if supplied, the output of tokens is matched to this list and the Casing 
        /// from the list is used in the output.
        /// </summary>
        //public Tree<TokenTypes> Tots { get; set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that is source.
        /// </returns>
        public override string ToString()
        {
            return this.fPrettyString.ToString();
        }

        /// <summary>
        /// Indents the source code.
        /// </summary>
        public void Indent()
        {
            this.fPadCount++;
            this.UpdatePad();
        }

        /// <summary>
        /// Unindents the source code.
        /// </summary>
        public void Unindent()
        {
            this.fPadCount--;
            this.UpdatePad();
        }

        /// <summary>
        /// Writes the specified str to source with appropriate padding.
        /// </summary>
        /// <param name="str">The string to be written to source.</param>
        public void Write(string str)
        {
            if (this.fToPad)
            {
                str = this.fPadding + str;
            }

            this.fPrettyString.Append(str);
            this.fToPad = false;
        }       

        /// <summary>
        /// Convenience method - calls Write with str and WriteLine.
        /// </summary>
        /// <param name="str">The string to be written to source.</param>
        public void WriteLine(string str)
        {
            this.Write(str);
            this.WriteLine();
        }

        /// <summary>
        /// Writes a newline to source.
        /// </summary>
        public void WriteLine()
        {
            this.fPrettyString.AppendLine();
            this.fToPad = true;
        }

        /// <summary>
        /// Updates the pad count.
        /// </summary>
        private void UpdatePad()
        {
            this.fPadding = string.Empty;
            for (int i = 0; i < this.fPadCount; i++)
            {
                this.fPadding += this.fPadValue;
            }
        }

        /// <summary>
        /// force the characters to Uppercase
        /// </summary>
        /// <param name="str">The string to be written to source.</param>
        public void WRITE(string str)
        {
            if (this.forceToUpper)
            {
                str.ToUpper();
            }
            this.fPrettyString.Append(str);
            this.forceToUpper = true;
        }

        /// <summary>
        /// return the characters without any changes.
        /// </summary>
        /// <param name="str"></param>
        public void WriteLiteral(string str)
        {
            this.fPrettyString.Append(str);
            this.forceToUpper = false;
        }
    }
}
