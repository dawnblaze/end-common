﻿using Endor.CBEL.Grammar;
using Irony.Ast;
using Irony.Interpreter;
using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Grammar
{
    public class TNumericLiteral : TASTNodeBase, IASTNodeExtensions
    {
        private string _numericLiteralValue = null;

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            base.Init(context, treeNode);
            _numericLiteralValue = treeNode.Token.ValueString;
        }

        public override void ToCBEL(TPrettyPrinter printer)
        {
            printer.Write(_numericLiteralValue);
        }

        public override void ToCS(TPrettyPrinter printer)
        {
            printer.Write(_numericLiteralValue+"m");
        }
    }
}
