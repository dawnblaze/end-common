﻿using System;
using System.Linq;
using System.Collections.Generic;
using Endor.CBEL.Common;
using Endor.Models;
using Irony.Ast;
using Irony.Parsing;
using Endor.CBEL.Metadata;

namespace Endor.CBEL.Grammar
{
    public class TKnownObjectReference : TStatementListNode, IASTNodeExtensions
    {
        private string[] GetTokenText(ParseTreeNode node)
        {
            if (node.Token == null)
                return node.ChildNodes.SelectMany(n => GetTokenText(n)).ToArray();

            return new string[] { node.Token?.Text };
        }

        private void ParseKnownObjects(ParseTreeNode treeNode, string[] variableToken)
        {
            CBELGrammar grammar = (CBELGrammar)treeNode.Term.Grammar;
            SortedDictionary<string, ICBELMember> memberList = grammar.KnownMemberList;
            ICBELMember member = null;

            string _name;
            CBELMemberObjectType _memberObjectType;

            foreach (var _token in _tokens.Select((value, index) => new { index, value }))
            {
                if (memberList == null) return;

                _name = _token.value;
                _memberObjectType = _token.index == 0 ? CBELMemberObjectType.Object : 0;

                var test = (int)_memberObjectType + CBELMemberObjectType.Object;

                member = memberList
                            .Where(
                                n => n.Value.Name.ToLower() == _name.ToLower()
                                && (
                                       n.Value.MemberObjectType == (_memberObjectType | CBELMemberObjectType.Scalar)
                                    || n.Value.MemberObjectType == (_memberObjectType | CBELMemberObjectType.Object)
                                    || n.Value.MemberObjectType == CBELMemberObjectType.Object
                                )
                            ).FirstOrDefault().Value;

                if (member == null) return;

                variableToken[_token.index] = member.Name;

                memberList = member.Members;
            }
        }

        private string _tokenText;
        private string[] _tokens;

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            _tokens = treeNode.ChildNodes.SelectMany(n => GetTokenText(n)).ToArray();
            ParseKnownObjects(treeNode, _tokens);
            _tokenText = string.Join(".", _tokens);

            base.Init(context, treeNode);

            AsString = _tokenText;
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCBEL(TPrettyPrinter printer)
        {
            printer.Write(_tokenText);
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCS(TPrettyPrinter printer)
        {
            printer.Write(_tokenText);
        }
    }
}
