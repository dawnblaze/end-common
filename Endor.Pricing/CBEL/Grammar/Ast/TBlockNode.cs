﻿using Irony.Ast;
using Irony.Parsing;
using Irony.Interpreter.Ast;

// <copyright file="TBlockNode.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{
    using Irony.Ast;

    /// <summary>
    /// Adds ToCBEL and ToCS methods to Irony Block AST Node.
    /// </summary>
    public class TBlockNode : TStatementListNode, IASTNodeExtensions
    {

        public override void Init(AstContext context, ParseTreeNode treeNode)
        {
            if (treeNode.ChildNodes.Count > 0)
            {
                base.Init(context, treeNode.ChildNodes[0]);
            }
            else
            {
                base.Init(context, treeNode);
            }
            AsString = "Block";
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCBEL(TPrettyPrinter printer)
        {
            printer.WriteLine("{");
            printer.Indent();
            foreach (IASTNodeExtensions child in ChildNodes)
            {
                child.ToCBEL(printer);
                printer.WriteLine();
            }

            printer.Unindent();
            printer.Write("}");
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public override void ToCS(TPrettyPrinter printer)
        {
            printer.WriteLine("{");
            printer.Indent();
            foreach (IASTNodeExtensions child in ChildNodes)
            {
                child.ToCS(printer);
                printer.WriteLine();
            }

            printer.Unindent();
            printer.WriteLine("}");
        }
    }
}
