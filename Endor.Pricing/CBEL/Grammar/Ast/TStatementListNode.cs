﻿using Irony.Ast;
using Irony.Parsing;
using Irony.Interpreter.Ast;
// <copyright file="TStatementListNode.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{
    using Irony.Ast;
    
    /// <summary>
    /// Adds ToCBEL and ToCS methods to Irony Statement List AST Node.
    /// </summary>
    public class TStatementListNode : StatementListNode, IASTNodeExtensions
    {
        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <returns>String representation of node as CFL source</returns>
        public string ToCBEL()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCBEL(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public virtual void ToCBEL(TPrettyPrinter printer)
        {
            foreach (AstNode child in ChildNodes)
            {
                if (child is IASTNodeExtensions childAstExt)
                {
                    childAstExt.ToCBEL(printer);
                    printer.WriteLine();
                }
            }
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <returns>String representation of node as C# source</returns>
        public string ToCS()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCS(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public virtual void ToCS(TPrettyPrinter printer)
        {
            foreach ( AstNode child in ChildNodes)
            {
                if (child is IASTNodeExtensions childAstExt)
                {
                    childAstExt.ToCS(printer);
                }
                else if (child is LiteralValueNode)
                {
                    printer.Write(child.AsString);
                }
            }
        }
    }
}
