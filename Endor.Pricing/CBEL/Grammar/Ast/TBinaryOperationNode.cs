﻿using Irony.Ast;
using Irony.Parsing;
using Irony.Interpreter.Ast;

// <copyright file="TBinaryOperationNode.cs" company="Cyrious">
//     Copyright (c) Cyrious Software.  All rights reserved.
// </copyright>

namespace Endor.CBEL.Grammar
{
    using Irony.Ast;

    /// <summary>
    /// Adds ToCBEL and ToCS methods to Irony Binary Operation AST Node.
    /// </summary>
    public class TBinaryOperationNode : BinaryOperationNode, IASTNodeExtensions
    {
        /// <summary>
        /// Formats the node (and any children) as CFL code.
        /// </summary>
        /// <returns>String representation of node as CFL source</returns>
        public string ToCBEL()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCBEL(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as CFL code..
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public virtual void ToCBEL(TPrettyPrinter printer)
        {
            (this.Left as IASTNodeExtensions).ToCBEL(printer);
            if (this.Op.Equals("^"))
            {
                printer.Write(this.OpSymbol);
            }
            else
            {
                printer.Write(string.Format(" {0} ", this.Op));
            }

            (this.Right as IASTNodeExtensions).ToCBEL(printer);
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <returns>String representation of node as C# source</returns>
        public string ToCS()
        {
            TPrettyPrinter printer = new TPrettyPrinter();
            this.ToCS(printer);
            return printer.ToString();
        }

        /// <summary>
        /// Formats the node (and any children) as C# code.
        /// </summary>
        /// <param name="printer">StringBuilder used to build source code.</param>
        public virtual void ToCS(TPrettyPrinter printer)
        {
            // binaryOperator.Rule = ToTerm("+") | "-" | "*" | "/" | "^" | "%" 
            //                     | "mod" | "==" | "!=" | "<>" | "<" | ">" | "<=" | ">="
            //                     | "and" | "or" | "xor" | "&&" | "||" | "per" | "in";
            string leftOperand = null;
            string rightOperand = null;

            if (this.Left is IASTNodeExtensions leftNodeExtensions)
            {
                leftOperand = leftNodeExtensions.ToCS();
            }
            else if (this.Left is LiteralValueNode node)
            {
                leftOperand = node.AsString;
            }

            if (this.Right is IASTNodeExtensions rightNodeExtensions)
            {
                rightOperand = rightNodeExtensions.ToCS();
            }
            else if (this.Right is LiteralValueNode node)
            {
                rightOperand = node.AsString;
            }

            string binaryExpression;
            string operatorCSharp;

            
            if (this.OpSymbol.Equals("^"))
            {
                binaryExpression = string.Format("Math.Pow({0}, {1})", leftOperand, rightOperand);
            }
            else
            {
                if (this.OpSymbol.Equals("mod"))
                {
                    operatorCSharp = "%";
                }
                else if (this.OpSymbol.Equals("="))
                {
                    operatorCSharp = "==";
                }
                else if (this.OpSymbol.Equals("<>"))
                {
                    operatorCSharp = "!=";
                }
                else if (this.OpSymbol.Equals("and"))
                {
                    operatorCSharp = "&&";
                }
                else if (this.OpSymbol.Equals("or"))
                {
                    operatorCSharp = "||";
                }
                else if (this.OpSymbol.Equals("xor"))
                {
                    operatorCSharp = "^";
                }
                else if (this.OpSymbol.Equals("per"))
                {
                    //operatorCSharp = "/";
                    //This is needed to prevent incorrect integer division
                    operatorCSharp = "/ (decimal)";
                }
                else if (this.OpSymbol.Equals("in"))
                {
                    //operatorCSharp = "/";
                    //This is needed to prevent incorrect integer division
                    operatorCSharp = "/ (decimal)";
                }
                else if (this.OpSymbol.Equals("/"))
                {
                    //operatorCSharp = "/";
                    //This is needed to prevent incorrect integer division
                    operatorCSharp = "/ (decimal)";
                }
                else if (this.OpSymbol.Equals("&"))
                {
                    operatorCSharp = "+";
                }
                else
                {
                    operatorCSharp = this.OpSymbol;
                }

                binaryExpression = $"({leftOperand} {operatorCSharp} {rightOperand})";
            }

            printer.Write(binaryExpression);
        }
    }
}
