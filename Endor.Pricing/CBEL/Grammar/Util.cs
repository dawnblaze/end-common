﻿using Endor.CBEL.Common;
using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.CBEL.Grammar
{
    //public static class Util
    //{
    //    public static string SafeFormat(this string template, params object[] args)
    //    {
    //        if (args == null || args.Length == 0) return template;
    //        try
    //        {
    //            template = string.Format(template, args);
    //        }
    //        catch (Exception ex)
    //        {
    //            template = template + "(message formatting failed: " + ex.Message + " Args: " + string.Join(",", args) + ")";
    //        }
    //        return template;
    //    }//method

    //    public static void Check(bool condition, string messageTemplate, params object[] args)
    //    {
    //        if (condition) return;
    //        throw new Exception(messageTemplate.SafeFormat(args));
    //    }

    //}//class

    public static class Util
    {
        private static readonly List<string> SpecialTokens = new List<string>()
        {
            "UnitPrice",
            "Cost",
            "UnitCost",
            "MaterialCost",
            "LaborCost",
            "MachineCost",
        };

        private static readonly Dictionary<string, string> TranslatedTokens = new Dictionary<string, string>()
        {            
        };

        public static bool IsSpecialToken(string token)
        {
            return SpecialTokens.Contains(token);
        }

        public static string TranslateToken(string token)
        {
            if (TranslatedTokens.ContainsKey(token))
                return TranslatedTokens[token];

            return token;
        }

        public static string SafeFormat(this string template, params object[] args)
        {
            if (args == null || args.Length == 0) return template;
            try
            {
                template = string.Format(template, args);
            }
            catch (Exception ex)
            {
                template = template + "(message formatting failed: " + ex.Message + " Args: " + string.Join(",", args) + ")";
            }
            return template;
        }//method

        public static void Check(bool condition, string messageTemplate, params object[] args)
        {
            if (condition) return;
            throw new Exception(messageTemplate.SafeFormat(args));
        }

        public static List<string> ExternalDependencies(this ParseTree self)
        {
            List<string> results = new List<string>();
            foreach (var token in self.Tokens)
            {
                if (token.IsVariable() || token.IsObjectIdentifier())
                {
                    string tokenText = token.Text;

                    if (!results.Contains(tokenText))
                        results.Add(tokenText);
                }
            }

            return results;
        }

        public static ParseResponse CheckParseErrors(ParseTree tree)
        {
            ParseResponse response = new ParseResponse()
            {
                Formula = tree.SourceText,
                Failures = new List<ParseFailuresResponse>()
            };

            foreach (Irony.LogMessage log in tree.ParserMessages)
            {
                ParseFailuresResponse failure = new ParseFailuresResponse()
                {
                    Message = FormatParseErrorMessage(log.Message),
                    Position = log.Location.Position,
                    Line = log.Location.Line
                };
                response.Failures.Add(failure);
            }
            
            return response;
        }

        private static string FormatParseErrorMessage(string OldMessage)
        {
            string NewMessage = OldMessage;

            const string SyntaxError = "Syntax Error.";
            const string ParenthesisExpected = "Parenthesis expected.";
            const string UnterminatedString = "Unterminated String.";
            const string InvalidToken = "Invalid Token Name.";

            if (OldMessage.IndexOf("Syntax error, expected: ") == 0)
            {
                NewMessage = SyntaxError;
            }
            if (OldMessage.IndexOf("Syntax error, expected: CloseParen") == 0)
            {
                NewMessage = ParenthesisExpected;
            }
            if (OldMessage.IndexOf("Mal-formed  string literal - cannot find termination symbol.") == 0)
            {
                NewMessage = UnterminatedString;
            }
            if (OldMessage.IndexOf("Number cannot be followed by a letter.") == 0)
            {
                NewMessage = InvalidToken;
            }

            return NewMessage;
        }

    }//class

}
