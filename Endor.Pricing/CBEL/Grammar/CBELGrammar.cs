﻿using System;
using Irony.Parsing;
using Irony.Ast;
using Irony.Interpreter.Ast;
using System.Collections.Generic;
using Endor.CBEL.Metadata;
using System.Linq;
using Endor.Models;
using Endor.CBEL.Common;
using Irony;

namespace Endor.CBEL.Grammar
{
    [Language("CBEL", "0.51", "CoreBridge Expression Language")]
    public class CBELGrammar : Irony.Parsing.Grammar
    {
        public SortedDictionary<string, ICBELMember> KnownMemberList { get; private set; }

        public CBELGrammar(AssemblyData assemblyData = null) : base(caseSensitive: false)
        {
            this.GrammarComments = "CoreBridge Expression Language";

            // Get all known member list
            SortedDictionary<string, ICBELMember> memberList = MetadataHelper.KnownMemberList(assemblyData);
            this.KnownMemberList = memberList;

            //--------------------------------------------------------------------------------------------------------
            // 0. Comments
            //--------------------------------------------------------------------------------------------------------
            CommentTerminal blockComment = new CommentTerminal("block-comment", "/*", "*/");
            CommentTerminal lineComment = new CommentTerminal("line-comment", "//",
                "\r", "\n", "\u2085", "\u2028", "\u2029");
            NonGrammarTerminals.Add(blockComment);
            NonGrammarTerminals.Add(lineComment);


            //--------------------------------------------------------------------------------------------------------
            // 1. Terminals
            //--------------------------------------------------------------------------------------------------------
            NumberLiteral NumberLiteral = new NumberLiteral("NumericLiteral", NumberOptions.Default, typeof(TNumericLiteral));
            StringLiteral StringLiteral = new StringLiteral("StringLiteral", "\"", StringOptions.AllowsAllEscapes);

            IdentifierTerminal ObjectIdentifier = new IdentifierTerminal("ObjectIdentifier");
            IdentifierTerminal MemberIdentifier = new IdentifierTerminal("MemberIdentifier");

            // These terminals are defined in our parent Grammar
            //Terminal Empty = Grammar.Empty;
            //Terminal NEWLINE = Grammar.NewLine;
            //Terminal INDENT = Grammar.Indent;
            //Terminal DEDENT = Grammar.Dedent;
            //Terminal EOF = Grammar.Eof;

            KeyTerm Dot = new KeyTerm(".", "Dot");
            KeyTerm Hash = new KeyTerm("#", "Hash");
            KeyTerm DoubleHash = new KeyTerm("##", "DoubleHash");
            KeyTerm OpenParen = new KeyTerm("(", "OpenParen");
            KeyTerm CloseParen = new KeyTerm(")", "CloseParen");
            KeyTerm OpenSquareBrace = new KeyTerm("[", "OpenSquareBrace");
            KeyTerm CloseSquareBrace = new KeyTerm("]", "CloseSquareBrace");
            KeyTerm OpenCurlyBrace = new KeyTerm("{", "OpenCurlyBrace");
            KeyTerm CloseCurlyBrace = new KeyTerm("}", "CloseCurlyBrace");
            KeyTerm Colon = new KeyTerm(":", "Colon");
            KeyTerm Semicolon = new KeyTerm(";", "Semicolon");
            KeyTerm Comma = new KeyTerm(",", "Comma");

            // Set the ASTConfig.NodeType
            //StringLiteral.AddStartEnd("'", StringOptions.AllowsAllEscapes | StringOptions.IsTemplate);
            //StringLiteral.AstConfig.NodeType = typeof(StringTemplateNode);

            //StringTemplateSettings templateSettings = new StringTemplateSettings(); //by default set to Ruby-style settings 
            //templateSettings.ExpressionRoot = Expression; //this defines how to evaluate expressions inside template
            //this.SnippetRoots.Add(Expression);
            //StringLiteral.AstConfig.Data = templateSettings;

            //--------------------------------------------------------------------------------------------------------
            // 2. Non-terminals
            //--------------------------------------------------------------------------------------------------------
            Type DefaultNodeType = typeof(TBlockNode);  // StatementListNode
            Type KeywordNodeType = typeof(TKeywordNode);
            Type AssemblyPropertyNodeType = typeof(TAssemblyPropertyNode);

            NonTerminal Program = new NonTerminal("Program", typeof(TStatementListNode));
            NonTerminal Statement = new NonTerminal("Statement", typeof(TStatementListNode));

            NonTerminal Expression = new NonTerminal("Expression", typeof(TStatementListNode));
            NonTerminal ParentheticExpression = new NonTerminal("ParentheticExpression", typeof(TStatementListNode));

            // Non-Terminals for All Types
            NonTerminal BinaryExpression = new NonTerminal("BinaryExpression", typeof(TBinaryOperationNode));
            NonTerminal UnaryPrefixExpression = new NonTerminal("UnaryPrefixExpression", typeof(TUnaryOperationNode));
            NonTerminal UnaryPostfixExpression = new NonTerminal("UnaryPostfixExpression", typeof(TUnaryOperationNode));

            NonTerminal BinaryOperator = new NonTerminal("BinaryOperator", typeof(TBinaryOperationNode));
            NonTerminal PrefixOperator = new NonTerminal("PrefixOperator", typeof(TUnaryOperationNode));
            NonTerminal PostfixOperator = new NonTerminal("PostfixOperator", typeof(TUnaryOperationNode));

            NonTerminal Function = new NonTerminal("Function", typeof(TStatementListNode));
            NonTerminal FunctionName = new NonTerminal("FunctionName", typeof(TFunctionNode));

            // Non-Terminals for String types
            NonTerminal ConcatenationOperator = new NonTerminal("ConcatenationOperator", DefaultNodeType);

            // Non-Terminals for Boolean Types
            NonTerminal BooleanLiteral = new NonTerminal("BooleanLiteral", typeof(TBooleanLiteral));

            NonTerminal ReservedKeyword = new NonTerminal("ReservedKeyword", KeywordNodeType);

            // Non-Terminals for Objects types
            NonTerminal DynamicLiteral = new NonTerminal("DynamicLiteral", KeywordNodeType);

            NonTerminal KnownObjectReference = new NonTerminal("KnownObjectReference", typeof(TKnownObjectReference));
            NonTerminal KnownAssemblyProperty = new NonTerminal("KnownAssemblyProperty", AssemblyPropertyNodeType);
            NonTerminal KnownObjectName = new NonTerminal("KnownObjectName", KeywordNodeType);
            NonTerminal KnownAssemblyPropertyName = new NonTerminal("KnownAssemblyPropertyName", KeywordNodeType);

            NonTerminal ObjectReference = new NonTerminal("ObjectReference", KeywordNodeType);
            NonTerminal ObjectMemberReference = new NonTerminal("ObjectMemberReference", KeywordNodeType);
            NonTerminal ObjectMemberAccessor = new NonTerminal("ObjectMemberAccessor", KeywordNodeType);
            NonTerminal ObjectIndexedProperty = new NonTerminal("ObjectIndexedProperty", KeywordNodeType);
            NonTerminal ObjectFunctionProperty = new NonTerminal("ObjectFunctionProperty", KeywordNodeType);
            NonTerminal ObjectScalarProperty = new NonTerminal("ObjectScalarProperty", KeywordNodeType);

            // Non-Terminals for List types
            //NonTerminal ListExpression = new NonTerminal("ListExpression", DefaultNodeType);
            //NonTerminal List = new NonTerminal("List", typeof(TStatementListNode));

            //NonTerminal ListFunction = new NonTerminal("ListFunction", typeof(TStatementListNode));
            //NonTerminal ListFunctionName = new NonTerminal("ListFunctionName", typeof(TKeywordNode));

            // Non-Terminals for Dynamic types
            NonTerminal IFFunction = new NonTerminal("IFFunction", typeof(TIfBlockNode));

            NonTerminal Argument = new NonTerminal("Argument", DefaultNodeType);
            NonTerminal Arguments = new NonTerminal("Arguments", typeof(TArgumentListNode));

            //--------------------------------------------------------------------------------------------------------
            // 3. BNF rules
            //--------------------------------------------------------------------------------------------------------
            this.Root
                                    = Program;

            Program.Rule
                                    = Expression; // MakePlusRule(Program, Statement);

            Statement.Rule
                                    = Expression
                                    | Expression + Semicolon;

            Expression.Rule
                                    = NumberLiteral     // 3.14
                                    | StringLiteral     // "Red"
                                    | BooleanLiteral    // true
                                    | BinaryExpression
                                    | ParentheticExpression
                                    | UnaryPrefixExpression
                                    | UnaryPostfixExpression
                                    | ReservedKeyword
                                    | Function
                                    | KnownObjectReference
                                    | ObjectReference
                                    | DynamicLiteral
                                    | KnownAssemblyProperty
                                    ;

            KnownObjectReference.Rule = KnownObjectName | KnownObjectName + Dot + ObjectMemberAccessor;
            KnownAssemblyProperty.Rule = KnownAssemblyPropertyName | KnownAssemblyPropertyName + Dot + ObjectMemberAccessor;

            ObjectReference.Rule = ObjectIdentifier + Dot + ObjectMemberAccessor;
            ObjectMemberReference.Rule = MemberIdentifier | MemberIdentifier + Dot + ObjectMemberAccessor;

            ObjectMemberAccessor.Rule =
                                     ObjectScalarProperty
                                   | ObjectFunctionProperty
                                   | ObjectIndexedProperty
                                   | ObjectMemberReference
                                   ;

            ObjectScalarProperty.Rule = ObjectIdentifier;

            ObjectFunctionProperty.Rule
                                    = ObjectIdentifier + PreferShiftHere() + OpenParen + Arguments + CloseParen;

            ObjectIndexedProperty.Rule
                                    = ObjectIdentifier + PreferShiftHere() + OpenSquareBrace + Arguments + CloseSquareBrace;

            ReservedKeyword.Rule = ToTerm("BID") | "Assembly"
                                    | "Cache" | "Context"
                                    ;

            ParentheticExpression.Rule
                                    = OpenParen + Expression + CloseParen;


            BinaryExpression.Rule
                                    = Expression + BinaryOperator + Expression;

            UnaryPrefixExpression.Rule
                                     = PrefixOperator + Expression;

            UnaryPostfixExpression.Rule
                                    = Expression + PostfixOperator;

            Function.Rule
                                    = IFFunction | DynamicLiteral + OpenParen + CloseParen
                                    | FunctionName + OpenParen + Arguments + CloseParen | FunctionName + OpenParen + CloseParen + Semicolon | FunctionName + OpenParen + CloseParen;

            BinaryOperator.Rule
                                    = ToTerm("+") | "-" | "*" | "/" | "^" | "&"
                                    | "=" | ">" | "<" | ">=" | "<=" | "<>" | "&&" | "||"
                                    ;

            PrefixOperator.Rule
                                    = ToTerm("+") | "-" | "$" | "!";

            PostfixOperator.Rule
                                    = ToTerm("%");

            IFFunction.Rule
                                    = ToTerm("IF") + OpenParen + Expression + Comma + Expression + Comma + Expression + CloseParen;

            // Get all function list
            var variableList = memberList.Where(x => x.Value.MemberType == CBELMemberType.Property && (x.Value.MemberObjectType & CBELMemberObjectType.Variable) != 0);

            if (variableList.Count() != 0)
            {
                NonTerminal Variable = new NonTerminal("Variable", typeof(TVariableNode));
                NonTerminal VariableName = new NonTerminal("VariableName", typeof(TVariableNameNode));
                //Variable.Rule = VariableName | VariableName + Dot + ObjectMemberAccessor;

                Expression.Rule |= Variable;

                Variable.Rule = VariableName + Dot + ObjectMemberAccessor;

                // Set Functionname.Rule
                foreach (var variable in variableList)
                {
                    if (VariableName.Rule == null)
                        VariableName.Rule = ToTerm(variable.Value.Name, $"Variable<{variable.Value.Name}>");
                    else
                        VariableName.Rule |= ToTerm(variable.Value.Name, $"Variable<{variable.Value.Name}>");
                }
            }

            FunctionName.Rule = ToTerm("CompanyTier") | ToTerm("NULL()");

            
            // Get all function list
            var funcList = memberList.Where(x => x.Value.MemberType == CBELMemberType.Function);
            
            // Set Functionname.Rule
            foreach (var func in funcList)
            {
                 FunctionName.Rule |= ToTerm(func.Value.Name);
            }

            BooleanLiteral.Rule
                                    = ToTerm("false") | "true";

            DynamicLiteral.Rule
                                     = ToTerm("null");


            //
            // Object Data Type Rules
            //

            KnownAssemblyPropertyName.Rule = ToTerm("ID");

            // Get all properties list
            var propList = memberList.Where(x => x.Value.MemberType == CBELMemberType.Property && ((x.Value.MemberObjectType.GetValueOrDefault(0) & CBELMemberObjectType.Variable) == 0));

            // Set KnownAssemblyPropertyName.Rule
            foreach (var prop in propList)
            {
                KnownAssemblyPropertyName.Rule |= ToTerm(prop.Value.Name);
            }


            KnownObjectName.Rule = ToTerm("Company") | "Variables"
                                    | "Materials" | "Labor" | "Machines" | "Subassemblies"
                                    ;

            Arguments.Rule
                                    = MakeStarRule(Arguments, Comma, Argument);

            Argument.Rule
                                    = Expression ;




            //--------------------------------------------------------------------------------------------------------
            // 4. Operators precedence
            //--------------------------------------------------------------------------------------------------------


            RegisterOperators(10, "&&", "||");
            RegisterOperators(20, "=", "<", "<=", ">", ">=", "<>");
            RegisterOperators(30, "+", "-", "&");
            RegisterOperators(40, "*", "/");
            RegisterOperators(50, Associativity.Right, "^");
            RegisterOperators(60, Associativity.Left, "%");
            RegisterOperators(70, Associativity.Right, "!", "@");

            //--------------------------------------------------------------------------------------------------------
            // 5. Punctuation and transient terms
            //--------------------------------------------------------------------------------------------------------
            // Punctuation is removed from the parser output (nodes are not created)

            MarkPunctuation(OpenParen, CloseParen
                           , OpenCurlyBrace, CloseCurlyBrace
                           , OpenSquareBrace, CloseSquareBrace
                           , Comma, Hash, Dot, DoubleHash);

            RegisterBracePair("(", ")");
            RegisterBracePair("{", "}");
            RegisterBracePair("[", "]");

            // Transient Non-Terminals are not emitted in the output
            // A transient can only have single child nodes under it (No "+" in the construction, only "|")
            MarkTransient(Expression, ParentheticExpression
                          , BinaryOperator, PrefixOperator, PostfixOperator
                          , Argument
                          , ObjectMemberAccessor
                          );

            //8. Console
            ConsoleTitle = "CoreBridge Expression Evaluator";
            ConsoleGreeting = "CoreBridge Expression Evaluation ... Press Ctrl-C to exit the program at any time.";
            ConsolePrompt = "= ";
            ConsolePromptMoreInput = "?";

            //9. Language flags. 
            // Automatically add NewLine before EOF so that our BNF rules work correctly when there's no final line break in source
            this.LanguageFlags
                        = LanguageFlags.NewLineBeforeEOF | LanguageFlags.CreateAst;

            // 10. Built AST
        }

        public override void BuildAst(LanguageData language, ParseTree parseTree)
        {
            var opHandler = new OperatorHandler(language.Grammar.CaseSensitive);
            Util.Check(!parseTree.HasErrors(), "ParseTree has errors, cannot build AST.");
            var astContext = new InterpreterAstContext(language, opHandler);
            var astBuilder = new AstBuilder(astContext);
            astBuilder.BuildAst(parseTree);
        }

        public override string ConstructParserErrorMessage(ParsingContext context, StringSet expectedTerms)
        {
            if (expectedTerms.Count == 1 && expectedTerms.First() == "Dot")
            {
                if (context.PreviousToken.IsVariable())
                    return $"{context.PreviousToken.Text} variable reference does not include a property.";

                if (context.PreviousToken.IsObjectIdentifier())
                    return $"Unknown property {context.PreviousToken.Text} or {context.PreviousToken.Text} is an object reference without a property.";
            }

            return base.ConstructParserErrorMessage(context, expectedTerms);
        }
    }
}

