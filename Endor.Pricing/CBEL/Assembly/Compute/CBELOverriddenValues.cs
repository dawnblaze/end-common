﻿using Endor.CBEL.Interfaces;
using System.Collections.Generic;

namespace Endor.CBEL.Compute
{
    public class CBELOverriddenValues : ICBELOverriddenValues
    {
        public CBELOverriddenValues()
        {
            VariableData = new List<IVariableData>();
            OVChildValues = new List<ICBELOverriddenValues>();
        }

        public List<IVariableData> VariableData { get; set; }
        public List<ICBELOverriddenValues> OVChildValues { get; set; }

        public decimal? Quantity { get; set; }
        public bool? QuantityOV { get; set; }
        public bool? IsIncluded { get; set; }
        public bool? IsIncludedOV { get; set; }
        public int? ComponentID { get; set; }
        public string VariableName { get; set; }
        public decimal? ParentQuantity { get; set; }
        public decimal? LineItemQuantity { get; set; }
        public bool IsVended { get; set; }
        public int? CompanyID { get; set; }
        public decimal? CostUnit { get; set; }
        public bool? CostUnitOV { get; set; }
    }
}
