﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Units;

namespace Endor.CBEL.Compute
{
    public class VariableData : IVariableData
    {
        public DataType DataType { get; set; }
        public string VariableName { get; set; }
        public bool IsOV { get; set; }
        public string Value { get; set; }
        public string Alt { get; set; }
        public bool? IsIncluded { get; set; }
        public bool? IsIncludedOV { get; set; }
        public decimal? CostUnit { get; set; }
        public bool? CostUnitOV { get; set; }
        public Unit Unit { get; set; }
    }
}
