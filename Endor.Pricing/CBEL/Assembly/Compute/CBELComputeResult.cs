﻿using System.Collections.Generic;
using System.Linq;
using Endor.CBEL.Common;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;

namespace Endor.CBEL.Compute
{
    public class CBELComputeResult : ICBELComputeResult
    {
        public CBELComputeResult(ICBELAssembly assembly)
        {
            Assembly = assembly;

            Errors = new List<CBELRuntimeException>();
            Errors.AddRange(assembly.Errors);

            ValidationFailures = new List<ICBELValidationFailure>();
            ValidationFailures.AddRange(assembly.ValidationFailures);

            VariableData = new List<IVariableData>();
            
            foreach (ICBELVariable variable in assembly.Variables.OrderedList())
            {
                if (variable is ICBELVariableComputed variableComputed)
                {
                    if (variableComputed.IsCalculated)
                    {
                        bool isOV = false;
                        decimal? costUnit = null;
                        bool? costUnitOV = null;

                        if (variable is ICBELVariableOverridable variableOverridable)
                            isOV = variableOverridable.IsOV;

                        if (variable is ICBELComponentVariable componentVariable)
                        {
                            costUnit = componentVariable.ComputedUnitCost;
                            costUnitOV = componentVariable.UnitCostIsOverridden;
                        }

                        VariableData.Add(new VariableData()
                        {
                            DataType = variableComputed.DataType,
                            VariableName = variableComputed.Name,
                            IsOV = isOV,
                            Value = variableComputed.GetResultValue(),
                            Alt = variableComputed.AltAsString,
                            IsIncluded = variableComputed.IsIncluded,
                            IsIncludedOV = variableComputed.IsIncludedOV,
                            CostUnit = costUnit,
                            CostUnitOV = costUnitOV,
                            Unit = variableComputed.ValueUnit()
                        });
                    }
                }
            }

            Results = new List<ICBELComputeResult>();
            // Why do we check UserOVValues to see if there are children?
            if (assembly.UserOVValues != null)
            {
                // iterate all of this computeRequest's subrequests
                foreach (VariableOVData ovChildValue in assembly.UserOVValues)
                {
                    var linkedComponent = assembly.Components.FirstOrDefault(t => t.Component?.ID == ovChildValue.ComponentID && t.Component?.Name == ovChildValue.VariableName)?.Component;

                    // If the selectedComponent is an AssemblyComponent
                    if (linkedComponent is ICBELAssembly subAssembly)
                    {
                        // Compute the subAssembly and its direct children and add the output to this compute request's results
                        Results.Add(new CBELComputeResult(subAssembly));
                    }
                }
            }
        }

        public ICBELAssembly Assembly { get; private set; }

        public List<IVariableData> VariableData { get; private set; }

        public bool HasErrors => Errors.Count != 0;

        public string ErrorText
        {
            get
            {
                string Results = "";
                Errors.ForEach(x => Results += $"{x.Message}; " + Constants.CR);
                return Results;
            }
        }

        public List<CBELRuntimeException> Errors { get; private set; }

        public bool HasValidationFailures => ValidationFailures.Count != 0;

        public string ValidationFailureText
        {
            get
            {
                string Results = "";
                ValidationFailures.ForEach(x => Results += $"{x.Message}; " + Constants.CR);
                return Results;
            }
        }

        public List<ICBELValidationFailure> ValidationFailures { get; private set; }

        public List<ICBELComputeResult> Results { get; private set; }
    }
}
