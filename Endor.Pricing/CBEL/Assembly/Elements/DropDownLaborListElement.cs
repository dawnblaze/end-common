﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using Endor.Pricing.CBEL.Common;
using System.Collections.Generic;

namespace Endor.CBEL.Elements
{
    [Autocomplete("DropDownLaborVariable")]
    public class DropDownLaborListElement : ConsumptionStringVariable, ICBELVariableOverridable, ICBELElement, IHasLaborComponent
    {
        public DropDownLaborListElement(ICBELAssembly parent) : base(parent) { }

        [AutocompleteIgnore]
        public override OrderItemComponentType ComponentClassType => OrderItemComponentType.Labor;
        public int ElementType => (int)AssemblyElementType.DropDown;
        public string ElementTypeName => "Drop-Down";

        public ICBELLaborComponent TypedComponent => (Component as ICBELLaborComponent);

        [AutocompleteIgnore]
        public List<(string Category, int ID)> IncludedCategories { get; private set; }
        [AutocompleteIgnore]
        public List<(string Component, int ID)> IncludedComponents { get; private set; }
        [AutocompleteIgnore]
        public List<(string Component, int ID)> ListValues { get; private set; }

        public string NameOnInvoice => TypedComponent?.NameOnInvoice;

        public string SKU => TypedComponent?.SKU;

        public string Description => TypedComponent?.Description;

        public decimal? SetupPrice => TypedComponent?.SetupPrice;

        public decimal? HourlyPrice => TypedComponent?.HourlyPrice;

        public Measurement BillingIncrement => TypedComponent?.BillingIncrement;
        public Measurement MinimumBillingTime => TypedComponent?.MinimumBillingTime;


        /// <summary>
        /// Labor has a specialized cost function. It equals fixed cost plus variable cost
        /// </summary>
        protected override decimal? RoundedCalculatedConsumption()
        {
            if (ConsumptionQuantity.IsOV)
                return ConsumptionQuantity.Value;

            return LaborQuantityHelper.ComputeQuantity(BillingIncrement, MinimumBillingTime, ConsumptionQuantity.Value);
        }

    }
}
