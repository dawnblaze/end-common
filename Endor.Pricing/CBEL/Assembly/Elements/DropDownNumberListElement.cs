﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using System.Collections.Generic;

namespace Endor.CBEL.Elements
{
    [Autocomplete("DropDownNumberVariable")]
    public class DropDownNumberListElement : NumberVariable, ICBELVariableOverridable, ICBELElement
    {
        public DropDownNumberListElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.DropDown;
        public string ElementTypeName => "Drop-Down";

        [AutocompleteIgnore]
        public List<(string name, decimal? number)> ListValues { get; set; }

        public override decimal? AsNumber
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;
            }
        }
    }
}
