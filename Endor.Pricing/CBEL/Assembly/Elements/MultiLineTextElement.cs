﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("MultiLineTextVariable")]
    public class MultiLineTextElement : StringVariable, ICBELVariableOverridable, ICBELElement
    {
        public MultiLineTextElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.MultiLineString;
        public string ElementTypeName => "Multi Line Text";
    }
}
