﻿using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.Models;

namespace Endor.CBEL.Elements
{
    public class GroupElement : VoidVariable, ICBELElement
    {
        public GroupElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.Group;
        public string ElementTypeName => "Group";
    }
}
