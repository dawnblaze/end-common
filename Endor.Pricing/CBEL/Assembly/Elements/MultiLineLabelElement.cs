﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("MultiLineLabelVariable")]
    public class MultiLineLabelElement : StringVariable, ICBELElement
    {
        public MultiLineLabelElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.MultiLineLabel;
        public string ElementTypeName => "Multi-Line Label";
    }
}