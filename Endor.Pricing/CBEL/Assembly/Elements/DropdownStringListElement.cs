﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using System.Collections.Generic;

namespace Endor.CBEL.Elements
{
    [Autocomplete("DropDownStringVariable")]
    public class DropDownStringListElement : StringVariable, ICBELVariableOverridable, ICBELElement
    {
        public DropDownStringListElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.DropDown;
        public string ElementTypeName => "Drop-Down";

        [AutocompleteIgnore]
        public List<string> ListValues { get; set; }
    }
}
