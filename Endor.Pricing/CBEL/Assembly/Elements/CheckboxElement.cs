﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("CheckboxVariable")]
    public class CheckboxElement : BooleanVariable, ICBELVariableOverridable, ICBELElement
    {
        public CheckboxElement(ICBELAssembly parent) : base(parent) { }

        /// <summary>
        /// The ID of the Element Type
        /// </summary>
        public int ElementType => (int)AssemblyElementType.Checkbox;

        /// <summary>
        /// The text name for the Element Type (e.g., Checkbox, Dropdown, etc.)
        /// This metadata is useful for debugging.
        /// </summary>
        public string ElementTypeName => "Checkbox";
    }
}
