﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("URLVariable")]
    public class URLLabelElement : StringVariable, ICBELVariableOverridable, ICBELElement
    {
        public URLLabelElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.UrlLabel;
        public string ElementTypeName => "URL Label";

        public string DisplayText { get; }
    }
}