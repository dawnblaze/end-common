﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("ShowHideGroupVariable")]
    public class ShowHideGroupElement : BooleanVariable, ICBELVariableOverridable, ICBELElement
    {
        public ShowHideGroupElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.ShowHideGroup;
        public string ElementTypeName => "Show/Hide Group";
    }
}
