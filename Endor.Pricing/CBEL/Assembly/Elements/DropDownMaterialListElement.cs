﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using System.Collections.Generic;

namespace Endor.CBEL.Elements
{
    [Autocomplete("DropDownMaterialVariable")]
    public class DropDownMaterialListElement : ConsumptionStringVariable, ICBELVariableOverridable, ICBELElement, IHasMaterialComponent, ICBELMaterialComponent
    {
        public DropDownMaterialListElement(ICBELAssembly parent) : base(parent) { }

        [AutocompleteIgnore]
        public override OrderItemComponentType ComponentClassType => OrderItemComponentType.Material;
        public int ElementType => (int)AssemblyElementType.DropDown;
        public string ElementTypeName => "Drop-Down";

        public ICBELMaterialComponent TypedComponent => ( Component as ICBELMaterialComponent );

        [AutocompleteIgnore]
        public List<(string Category, int ID)> IncludedCategories { get; private set; }
        [AutocompleteIgnore]
        public List<(string Component, int ID)> IncludedComponents { get; private set; }
        [AutocompleteIgnore]
        public List<(string Component, int ID)> ListValues { get; private set; }

        public string NameOnInvoice => TypedComponent?.NameOnInvoice;
        public string SKU => TypedComponent?.SKU;
        public string Description => TypedComponent?.Description;
        public string MaterialType => TypedComponent?.MaterialType;
        public Measurement Length => TypedComponent?.Length;
        public Measurement Height => TypedComponent?.Height;
        public Measurement Width => TypedComponent?.Width;
        public Measurement Depth => TypedComponent?.Depth;
        public Measurement Thickness => TypedComponent?.Thickness;
        public Measurement Weight => TypedComponent?.Weight;

        public decimal? QuantityInSet => TypedComponent?.QuantityInSet;

        public decimal? QuantityValue => TypedComponent?.QuantityValue;

        public decimal? UnitPrice => TypedComponent?.ComputedUnitPrice;

        public bool? QuantityOV => TypedComponent?.QuantityOV;

        public int? ID => TypedComponent?.ID;

        [AutocompleteIgnore]
        public int? ClassTypeID => TypedComponent?.ClassTypeID;

        public ICBELObject ParentObject => TypedComponent.ParentObject;

        public int? IncomeAccountID
        {
            get
            {
                return TypedComponent.IncomeAccountID;
            }
            set
            {
                TypedComponent.IncomeAccountID = value;
            }
        }
        public int? ExpenseAccountID
        {
            get
            {
                return TypedComponent.ExpenseAccountID;
            }
            set
            {
                TypedComponent.ExpenseAccountID = value;
            }
        }
        public AssemblyIncomeAllocationType? IncomeAllocationType
        {
            get
            {
                return TypedComponent.IncomeAllocationType;
            }
            set
            {
                TypedComponent.IncomeAllocationType = value;
            }
        }
    }
}
