﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("SingleLineTextVariable")]
    public class SingleLineTextElement : StringVariable, ICBELVariableOverridable, ICBELElement
    {
        public SingleLineTextElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.SingleLineText;
        public string ElementTypeName => "Single Line Text";
    }
}
