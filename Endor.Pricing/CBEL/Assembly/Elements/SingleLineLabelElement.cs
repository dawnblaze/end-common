﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("SingleLineLabelVariable")]
    public class SingleLineLabelElement : StringVariable, ICBELElement
    {
        public SingleLineLabelElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.SingleLineLabel;
        public string ElementTypeName => "Single Line Label";
    }
}