﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("NumberEditVariable")]
    public class NumberEditElement : NumberVariable, ICBELVariableOverridable, ICBELElement
    {
        public NumberEditElement(ICBELAssembly parent) : base(parent) { }

        public int ElementType => (int)AssemblyElementType.Number;
        public string ElementTypeName => "Number Edit";
    }
}
