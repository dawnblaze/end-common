﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.CBEL.Elements;
using Endor.CBEL.Interfaces;

namespace Endor.CBEL.Elements
{
    public class MarkupTable<ColumnDataType> : TierDataTable<ColumnDataType>
    {
        public MarkupTable(ICBELAssembly parent) : base(parent) { }
    }
}
