﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    // Implement the Assembly Property for Boolean Types
    public abstract class BooleanVariable : CBELComputedVariableBase<bool?>
    {
        public BooleanVariable(ICBELAssembly parent) : base(parent)
        {
            // Set other default values ... these should not be reassigned in descendents
            NullResult = null;
            DefaultValueConstant = false;
        }

        [JsonIgnore] public override DataType DataType => DataType.Boolean;
        [JsonIgnore] public override string DataTypeName => "boolean";

        public override bool? AsBoolean
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;
            }
        }

        public override decimal? AsNumber
        {

            get
            {
                bool? v = Value;
                if (v == null)
                    return (decimal?)null;
                else
                    return ((bool)v ? 1 : 0);
            }
            set
            {
                if (!value.HasValue)
                    Value = null;
                else
                    Value = (value.Value != 0);
            }
        }

        public override ICBELObject AsObject() => null;

        public override string AsString
        {
            get
            {
                bool? v = Value;
                if (v == null)
                    return null;
                else
                    return ((bool)v ? "true" : "false");
            }
            set
            {
                if (value == null)
                    Value = null;
                else if (value.Trim() == "")
                    Value = false;
                else
                    Value = "YyTt1".Contains(value.Trim()[0]);
            }
        }

        protected override bool? ToT(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null as bool? : bool.Parse(value);
        }
    }
}



