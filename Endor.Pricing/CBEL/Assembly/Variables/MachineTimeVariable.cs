﻿using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Pricing;

namespace Endor.CBEL.Elements
{
    public class MachineTimeVariable : ConsumptionStringVariable, ICBELVariableOverridable, IHasMachineComponent
    {
        public MachineTimeVariable(ICBELAssembly parent) : base(parent) { }

        public ICBELMachine TypedComponent => Component as ICBELMachine;
        public override OrderItemComponentType ComponentClassType => OrderItemComponentType.MachineTime;

        public string Description => TypedComponent?.Description;

        public string MachineType => TypedComponent?.MachineType;

        public short? ActiveProfileCount => TypedComponent?.ActiveProfileCount;

        public short? ActiveInstanceCount => TypedComponent?.ActiveInstanceCount;

        public decimal? MachineCostPerHour => TypedComponent?.MachineCostPerHour;

        public int? ID => TypedComponent?.ID;

        public int? ClassTypeID => TypedComponent?.ClassTypeID;

        public override decimal? Cost
        {
            get
            {
                return Parent?.ParentVariable?.Cost;
            }
        }

        public override decimal? ComputedUnitCost
        {
            get
            {
                return Parent?.ParentVariable?.ComputedUnitCost;
            }
        }

        public override bool? UnitCostIsOverridden
        {
            get
            {
                return Parent?.ParentVariable?.UnitCostIsOverridden;
            }
        }
    }
}
