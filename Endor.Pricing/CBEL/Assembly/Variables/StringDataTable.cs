﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    [Autocomplete("StringTableVariable")]
    public class StringDataTable<RowDataType, ColumnDataType> : CBELDataTable<string, RowDataType, ColumnDataType>
    {
        public StringDataTable(ICBELAssembly parent) : base(parent) { }

        [JsonIgnore] public override DataType DataType => DataType.String;
        [JsonIgnore] public override string DataTypeName => "string";

        public override bool? AsBoolean
        {

            get

            {
                var v = Value.TrimStart();

                if (v.Length == 0)
                    return null;
                else
                    return "YyTt1".Contains(v[0]);
            }
            set
            {
                if (!value.HasValue)
                    Value = null;
                else
                    Value = value.Value ? "true" : "false";
            }
        }

        public override decimal? AsNumber
        {
            get
            {
                var v = Value;
                if (v == null)
                    return null;
                else
                    return (decimal.TryParse(v, out decimal Result)) ? Result : (decimal?)null;
            }
            set
            {
                if (!value.HasValue)
                    Value = null;
                else
                    Value = value.ToString();
            }
        }

        public override ICBELObject AsObject() => null;

        protected override string ToT(string value)
        {
            return value;
        }

        public override string AsString
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;

            }
        }
    }
}
