﻿using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using Endor.Units;

namespace Endor.CBEL.Elements
{
    [Autocomplete("LinkedMaterialVariableBase")]
    public class LinkedMaterialVariable : ConsumptionStringVariable, ICBELVariableOverridable, IHasMaterialComponent, IMeasurement, ICBELMaterialComponent
    {
        public LinkedMaterialVariable(ICBELAssembly parent) : base(parent) { }

        public ICBELMaterialComponent TypedComponent => Component as ICBELMaterialComponent;

        public override OrderItemComponentType ComponentClassType => OrderItemComponentType.Material;

        public string NameOnInvoice => TypedComponent?.NameOnInvoice;
        public string SKU => TypedComponent?.SKU;
        public string Description => TypedComponent?.Description;
        public string MaterialType => TypedComponent?.MaterialType;
        public Measurement Length => TypedComponent?.Length;
        public Measurement Height => TypedComponent?.Height;
        public Measurement Width => TypedComponent?.Width;
        public Measurement Depth => TypedComponent?.Depth;
        public Measurement Thickness => TypedComponent?.Thickness;
        public Measurement Weight => TypedComponent?.Weight;

        public decimal? QuantityInSet => TypedComponent?.QuantityInSet;

        public decimal? QuantityValue => TypedComponent?.QuantityValue;

        public decimal? UnitPrice => TypedComponent?.ComputedUnitPrice;

        public bool? QuantityOV => TypedComponent?.QuantityOV;

        public int? ID => TypedComponent?.ID;

        [AutocompleteIgnore]
        public int? ClassTypeID => TypedComponent?.ClassTypeID;

        public ICBELObject ParentObject => TypedComponent.ParentObject;

        public int? IncomeAccountID
        {
            get
            {
                return TypedComponent.IncomeAccountID;
            }
            set
            {
                TypedComponent.IncomeAccountID = value;
            }
        }
        public int? ExpenseAccountID
        {
            get
            {
                return TypedComponent.ExpenseAccountID;
            }
            set
            {
                TypedComponent.ExpenseAccountID = value;
            }
        }
        public AssemblyIncomeAllocationType? IncomeAllocationType
        {
            get
            {
                return TypedComponent.IncomeAllocationType;
            }
            set
            {
                TypedComponent.IncomeAllocationType = value;
            }
        }
        #region Units
        /// <summary>
        /// Returns the UnitType of the number.
        /// </summary>
        [AutocompleteIgnore]
        public Unit Unit { get; set; }

        [AutocompleteIgnore]
        public UnitType UnitType => this.UnitInfo().UnitType;

        [AutocompleteIgnore]
        public decimal? ValueInUnits => QuantityValue;

        [AutocompleteIgnore]
        public void LogConversionError(string message)
        {
            if (Parent != null)
                Parent.LogException(new CBELRuntimeException(message));
        }

        public string UnitName => Unit.Name();
        public string UnitTypeName => UnitType.Name();
        public string UnitSystemName => this.UnitInfo().UnitSystem.ToString();
        public string UnitClassificationName => this.UnitTypeInfo().Name;
        public int UnitDimension => MeasurementHelper.UnitDimension(this);
        #endregion Units
    }
}
