﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.CBEL.Elements;
using Endor.CBEL.Interfaces;

namespace Endor.CBEL.Elements
{
    public class DiscountTable<ColumnDataType> : TierDataTable<ColumnDataType>
    {
        public DiscountTable(ICBELAssembly parent) : base(parent) { }
    }
}
