﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    [Autocomplete("NumberTableVariable")]
    public class NumberDataTable<RowDataType, ColumnDataType> : CBELDataTable<decimal?, RowDataType, ColumnDataType>
    {
        public NumberDataTable(ICBELAssembly parent) : base(parent) { }

        [JsonIgnore] public override DataType DataType => DataType.Number;
        [JsonIgnore] public override string DataTypeName => "number";

        public override bool? AsBoolean
        {
            get
            {
                decimal? v = Value;
                if (v == null)
                    return null;
                else
                    return ((decimal)v != 0);
            }
            set
            {
                if (!value.HasValue)
                    Value = null;
                else
                    Value = value.Value ? 1 : 0;
            }
        }

        public override decimal? AsNumber
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;
            }
        }

        public override ICBELObject AsObject() => null;

        protected override decimal? ToT(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null as decimal? : decimal.Parse(value);
        }

        public override string AsString
        {

            get
            {
                decimal? v = Value;
                if (v == null)
                    return null;
                else
                    return v.ToString();
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    Value = null;
                else
                {
                    if (decimal.TryParse(value, out decimal d))
                        Value = d;
                    else
                        Value = null;
                }
            }
        }
    }
}
