﻿using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using Endor.Units;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    // Implement the Assembly Property for Number Types
    public class NumberVariable : CBELComputedVariableBase<decimal?>, IMeasurement
    {
        public NumberVariable(ICBELAssembly parent) : base(parent)
        {
            // Set other default values ... these should not be reassigned in descendents
            NullResult = null;
        }

        [JsonIgnore] public override DataType DataType => DataType.Number;
        [JsonIgnore] public override string DataTypeName => "number";

        public override bool? AsBoolean
        {
            get
            {
                decimal? v = Value;
                if (v == null)
                    return null;
                else
                    return ((decimal)v != 0);
            }
            set
            {
                if (!value.HasValue)
                    Value = null;
                else
                    Value = value.Value ? 1 : 0;
            }
        }

        public override decimal? AsNumber
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;
            }
        }
        public override ICBELObject AsObject() => null;

        protected override decimal? ToT(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null as decimal? : decimal.Parse(value);
        }

        public override string AsString
        {
            get
            {
                decimal? v = Value;
                if (v == null)
                    return null;
                else
                    return v.ToString();
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    Value = null;
                else
                {
                    if (decimal.TryParse(value, out decimal d))
                        Value = d;
                    else
                        Value = null;
                }
            }
        }

        private Unit? _ValueUnit;
        public override Unit ValueUnit()
        {
            return _ValueUnit ?? Unit;
        }
        public override string GetResultValue()
        {
            if (_ValueUnit.HasValue && _ValueUnit.Value != Unit)
                return this.ConvertTo(_ValueUnit.Value).ToString();

            return AsString;
        }

        protected override void SetValueOV()
        {
            base.SetValueOV();
            if (UserOVValues.Unit != Unit.None
                && Unit != Unit.None
                && UnitInfo.UnitInfoByUnit(UserOVValues.Unit).UnitType == UnitInfo.UnitInfoByUnit(Unit).UnitType
                && (decimal.TryParse(UserOVValues.Value, out decimal numberValue))
                )
            {
                _ValueUnit = UserOVValues.Unit;
                AsString = Unit.ConvertFrom(numberValue, UserOVValues.Unit).ToString();
            }
        }

        #region Units
        /// <summary>
        /// Returns the UnitType of the number.
        /// </summary>
        [AutocompleteIgnore]
        public Unit Unit { get; set; }

        [AutocompleteIgnore]
        public UnitType UnitType => this.UnitInfo().UnitType;

        [AutocompleteIgnore]
        public decimal? ValueInUnits => Value;

        [AutocompleteIgnore]
        public void LogConversionError(string message)
        {
            if (Parent != null)
                Parent.LogException(new CBELRuntimeException(message));
        }

        public string UnitName => Unit.Name();
        public string UnitTypeName => UnitType.Name();
        public string UnitSystemName => this.UnitInfo().UnitSystem.ToString();
        public string UnitClassificationName => this.UnitTypeInfo().Name;
        public int UnitDimension => MeasurementHelper.UnitDimension(this);
        #endregion Units
    }
}