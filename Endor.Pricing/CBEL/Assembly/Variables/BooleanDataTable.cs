﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    [Autocomplete("BooleanTableVariable")]
    public class BooleanDataTable<RowDataType, ColumnDataType> : CBELDataTable<bool?, RowDataType, ColumnDataType>
    {
        public BooleanDataTable(ICBELAssembly parent) : base(parent) { }

        [JsonIgnore] public override DataType DataType => DataType.Boolean;
        [JsonIgnore] public override string DataTypeName => "boolean";

        public override bool? AsBoolean
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;
            }
        }

        public override decimal? AsNumber
        {
            get
            {
                bool? v = Value;
                if (v == null)
                    return (decimal?)null;
                else
                    return (v.Value ? 1 : 0);
            }
            set
            {
                if (!value.HasValue)
                    Value = null;
                else
                    Value = (value.Value != 0);
            }
        }


        public override ICBELObject AsObject() => null;

        protected override bool? ToT(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? null as bool? : bool.Parse(value);
        }

        public override string AsString
        {
            get
            {
                bool? v = Value;
                if (v == null)
                    return null;
                else
                    return ((bool)v ? "true" : "false");
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    Value = null;
                else
                    Value = "YyTt1".Contains(value[0]);
            }
        }
    }
}
