﻿using Endor.CBEL.Common;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Newtonsoft.Json;
using System;

namespace Endor.CBEL.Elements
{
    public class TierDataTable<ColumnDataType> : NumberDataTable<string, ColumnDataType>
    {
        public TierDataTable(ICBELAssembly parent) : base(parent) { }


        public override string FormulaText { get => $"={Name}.TableLookup({ColumnVariableName}.Value, {ColumnMatchOption} )"; }

        /// <summary>
        /// Lookup the table's values given the default definition/variables
        /// </summary>
        /// <returns></returns>
        public override decimal? TableLookup()
        {
            return (ColumnVariable == null)
                    ? null
                    : TableLookup(ColumnVariable.AsDynamic, ColumnMatchOption);
        }

        /// <summary>
        /// Lookup a table value specifying the Row Value, Column Value, and Match conditions
        /// </summary>
        /// <param name="ColValue">The value of the Column Variable</param>
        /// <param name="colmatch">Enum indicating how non-exact matches are handled for the Column Variable</param>
        /// <returns></returns>
        public decimal? TableLookup(dynamic ColValue, AssemblyTableMatchType colmatch = AssemblyTableMatchType.ExactMatch)
        {
            InitializeTable();

            var RowValue = Parent.TierName;
            int RowNo = RowHeadings.IndexOf(RowValue);
            RowValue = (RowNo == -1) ? RowHeadings[0] : RowValue;
            return TableLookup(RowValue, ColValue, AssemblyTableMatchType.ExactMatch, colmatch);
        }
    }
}
