﻿using Endor.CBEL.Assembly.Components;
using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using Endor.Units;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.CBEL.Elements
{
    public class ConsumptionStringVariable : CBELComponentVariableBase<string>
    {
        public ConsumptionStringVariable(ICBELAssembly parent) : base(parent)
        {
            // Set other default values ... these should not be reassigned in descendents
            NullResult = null;
        }

        [JsonIgnore]
        public override DataType DataType => DataType.String;
        [JsonIgnore]
        public override string DataTypeName => "string";

        public override void ResetValue()
        {
            base.ResetValue();
            _Component = null;
        }
        protected ICBELComponent _Component;
        public override ICBELComponent Component
        {
            get
            {
                if (_Component == null
                    && !IsCalculating
                    && !IsCalculated)
                    Compute();

                return _Component;
            }
            set
            {
                _Component = value;
            }
        }

        public override bool? AsBoolean
        {

            get
            {
                var v = Value.TrimStart();

                if (v.Length == 0)
                    return null;
                else
                    return "YyTt1".Contains(v[0]);
            }
            set
            {
                if (!value.HasValue)
                    Value = null;
                else
                    Value = value.Value ? "true" : "false";
            }
        }

        public override decimal? AsNumber
        {
            get
            {
                var v = Value;
                if (v == null)
                    return null;
                else
                    return (decimal.TryParse(v, out decimal Result)) ? Result : (decimal?)null;
            }
            set
            {
                if (!value.HasValue)
                    Value = null;
                else
                    Value = value.ToString();
            }
        }

        public override ICBELObject AsObject() => null;
        public override string AsString
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value;
            }
        }

        /// <summary>
        /// LinkedMaterialID
        /// </summary>
        [AutocompleteIgnore]
        public int? LinkedMaterialID { get; set; }

        /// <summary>
        /// LinkedLaborID
        /// </summary>
        [AutocompleteIgnore]
        public int? LinkedLaborID { get; set; }

        /// <summary>
        /// LinkedMachineID
        /// </summary>
        [AutocompleteIgnore]
        public short? LinkedMachineID { get; set; }

        /// <summary>
        /// LinkedAssemblyID
        /// </summary>
        [AutocompleteIgnore]
        public int? LinkedAssemblyID { get; set; }

        [AutocompleteIgnore]
        public override void LoadLinkedComponent()
        {
            if (_Component != null)
                return;

            string componentName = Value;
            // check for overridden variable
            if (Parent?.Variables != null && Parent.Variables.TryGetValue(Name, out ICBELVariable variableValue))
            {
                if (variableValue is ICBELVariableComputed computedVariable)
                    componentName = computedVariable.AsString;
            }

            if (!string.IsNullOrWhiteSpace(componentName))
            {
                OrderItemComponentType componentClassType = ComponentClassType;

                bool? costUnitOV = null;
                decimal? costUnit = null;

                if (UserOVValues != null && UserOVValues.CostUnitOV.GetValueOrDefault(false))
                {
                    costUnitOV = true;
                    costUnit = UserOVValues.CostUnit;
                }


                if (componentClassType == OrderItemComponentType.Labor)
                {
                    CBELLaborComponent Projection(LaborData t) => new CBELLaborComponent()
                    {
                        ID = t.ID,
                        ClassTypeID = ClassType.Labor.ID(),
                        Name = t.Name,
                        NameOnInvoice = t.InvoiceText,
                        SKU = t.SKU,
                        Description = t.Description,
                        SetupPrice = t.EstimatingPriceFixed,
                        HourlyPrice = t.EstimatingPricePerHour,
                        BillingIncrement = new Measurement(t.BillingIncrementInMin, Unit.Minute),
                        MinimumBillingTime = new Measurement(t.MinimumTimeInMin, Unit.Minute),
                        UnitCostIsOverridden = costUnitOV,
                        OverriddenUnitCost = costUnit,
                        DefinedUnitCost = t.EstimatingCostPerHour / 60m,
                        DefinedFixedCost = t.EstimatingCostFixed,
                        DefinedUnitPrice = t.EstimatingPricePerHour / 60m,
                        DefinedFixedPrice = t.EstimatingPriceFixed,
                        IncomeAccountID = t.IncomeAccountID,
                        ExpenseAccountID = t.ExpenseAccountID
                    };

                    LaborData laborData = Parent.Context.LaborData
                        .FirstOrDefault(t => t.Name == componentName && t.BID == Parent.BID);

                    // When the name lookup returns null, get the LinkedLaborID and look it up from that.
                    if (laborData == null && LinkedLaborID.HasValue)
                    {
                        laborData = Parent.Context.LaborData
                            .FirstOrDefault(t => t.ID == LinkedLaborID.Value && t.BID == Parent.BID);
                    }

                    CBELLaborComponent laborComponent = Projection(laborData);

                    Component = laborComponent;
                }

                else if (componentClassType == OrderItemComponentType.Material)
                {
                    CBELMaterialComponent Projection(MaterialData t) => new CBELMaterialComponent()
                    {
                        ID = t.ID,
                        ClassTypeID = ClassType.Material.ID(),
                        Name = t.Name,
                        NameOnInvoice = t.InvoiceText,
                        SKU = t.SKU,
                        Description = t.Description,
                        MaterialType = t.PhysicalMaterialType.ToString(),
                        Length = t.Length,
                        Width = t.Width,
                        Weight = t.Weight,
                        QuantityInSet = t.QuantityInSet,
                        UnitCostIsOverridden = costUnitOV,
                        OverriddenUnitCost = costUnit,
                        DefinedUnitCost = t.EstimatingCost,
                        DefinedUnitPrice = t.EstimatingPrice,
                        IncomeAccountID = t.IncomeAccountID,
                        ExpenseAccountID = t.ExpenseAccountID
                    };

                    MaterialData materialData = Parent.Context.MaterialData
                        .FirstOrDefault(t => t.Name == componentName && t.BID == Parent.BID);

                    // When the name lookup returns null, get the LinkedMaterialID and look it up from that.
                    if (materialData == null && LinkedMaterialID.HasValue)
                    {
                        materialData = Parent.Context.MaterialData
                            .FirstOrDefault(t => t.ID == LinkedMaterialID.Value && t.BID == Parent.BID);
                    }

                    CBELMaterialComponent materialComponent = Projection(materialData);

                    Component = materialComponent;
                }

                else if (componentClassType == OrderItemComponentType.Assembly)
                {
                    AssemblyData assemblyData = Parent.Context.AssemblyData.FirstOrDefault(t => t.Name == componentName && t.BID == Parent.BID);

                    //load assembly component
                    if (assemblyData == null && LinkedAssemblyID.HasValue)
                    {
                        assemblyData = Parent.Context.AssemblyData.FirstOrDefault(t => t.ID == LinkedAssemblyID.Value && t.BID == Parent.BID);
                    }

                    if (assemblyData != null)
                    {
                        Task<AssemblyCompilationResponse> tskAssembly = CBELAssemblyHelper.LoadAssembly(Parent.Context, assemblyData.ID, ClassType.Assembly.ID(), 1, Parent.Cache, Parent.Logger, Parent.BID);
                        AssemblyCompilationResponse resp = Task.Run(() => tskAssembly).Result;
                        ICBELAssembly cbelAssembly = resp?.Assembly;

                        cbelAssembly.OverriddenUnitCost = costUnit;
                        cbelAssembly.UnitCostIsOverridden = costUnitOV;
                        cbelAssembly.IncomeAccountID = assemblyData.IncomeAccountID;
                        cbelAssembly.IncomeAllocationType = assemblyData.IncomeAllocationType;
                        cbelAssembly.ParentVariable = this;

                        if (UserOVValues != null)
                        {
                            cbelAssembly.UserOVValues = UserOVValues.ChildOVValues;
                            cbelAssembly.SetOvValues();
                        }

                        Component = cbelAssembly;

                        if (resp != null && resp.Errors != null && Parent != null && Parent.Errors != null)
                            Parent.Errors.AddRange(resp.Errors.Select(x => new Exceptions.CBELRuntimeException(x.Message, this)));
                    }

                }

                else if (componentClassType == OrderItemComponentType.Machine)
                {
                    ICBELMachine Projection(MachineData machine)
                    {
                        if (machine.MachineTemplateID != null)
                        {
                            Task<AssemblyCompilationResponse> tskAssembly = CBELAssemblyHelper.LoadAssembly(Parent.Context, machine.ID, ClassType.Machine.ID(), 1, Parent.Cache, Parent.Logger, Parent.BID);
                            AssemblyCompilationResponse resp = Task.Run(() => tskAssembly).Result;
                            ICBELAssembly cbelAssembly = resp?.Assembly;

                            if (decimal.TryParse(machine.EstimatingCostPerHourFormula, out decimal definedUnitCost))
                                cbelAssembly.DefinedUnitCost = definedUnitCost;

                            cbelAssembly.OverriddenUnitCost = costUnit;
                            cbelAssembly.UnitCostIsOverridden = costUnitOV;

                            if (resp != null && resp.Errors != null && Parent != null && Parent.Errors != null)
                                Parent.Errors.AddRange(resp.Errors.Select(x => new Exceptions.CBELRuntimeException(x.Message, this)));

                            if (cbelAssembly is ICBELMachine result)
                            {
                                result.Description = machine.Description;
                                result.MachineType = machine.MachineTemplateID.ToString();
                                result.IncomeAccountID = machine.IncomeAccountID;
                                result.ExpenseAccountID = machine.ExpenseAccountID;
                                result.ParentVariable = this;
                                return result;
                            }
                        }

                        return null;
                    }

                    MachineData machineData = Parent.Context.MachineData
                        .FirstOrDefault(t => t.Name == componentName && t.BID == Parent.BID);

                    // When the name lookup returns null, get the LinkedLaborID and look it up from that.
                    if (machineData == null && LinkedMachineID.HasValue)
                    {
                        machineData = Parent.Context.MachineData
                            .FirstOrDefault(t => t.ID == LinkedMachineID.Value && t.BID == Parent.BID);
                    }

                    ICBELMachine machineComponent = Projection(machineData);

                    Component = machineComponent;
                }

                else if (componentClassType == OrderItemComponentType.MachineTime)
                {
                    CBELMachineTimeComponent Projection()
                    {
                        if (Parent is ICBELMachine)
                        {
                            return new CBELMachineTimeComponent
                            {
                                ParentMachine = Parent as ICBELMachine,
                                QuantityValue = Parent.QuantityValue,
                            };
                        }

                        return null;
                    }


                    Component = Projection();
                }
            }

        }

        protected override string ToT(string value)
        {
            return value;
        }
    }
}
