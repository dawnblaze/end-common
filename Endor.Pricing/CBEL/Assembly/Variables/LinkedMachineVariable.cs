﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("LinkedMachineVariableBase")]
    public class LinkedMachineVariable : ConsumptionStringVariable, ICBELVariableOverridable, IHasMachineComponent
    {
        public LinkedMachineVariable(ICBELAssembly parent) : base(parent) { }

        public ICBELMachine TypedComponent => Component as ICBELMachine;
        public override OrderItemComponentType ComponentClassType => OrderItemComponentType.Machine;

        public string Description => TypedComponent?.Description;

        public string MachineType => TypedComponent?.MachineType;

        public short? ActiveProfileCount => TypedComponent?.ActiveProfileCount;

        public short? ActiveInstanceCount => TypedComponent?.ActiveInstanceCount;

        public decimal? MachineCostPerHour => TypedComponent?.MachineCostPerHour;

        public int? ID => TypedComponent?.ID;

        [AutocompleteIgnore]
        public int? ClassTypeID => TypedComponent?.ClassTypeID;
    }
}
