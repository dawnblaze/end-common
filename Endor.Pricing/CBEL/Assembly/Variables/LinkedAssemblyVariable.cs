﻿using Endor.Models.Autocomplete;
using Endor.CBEL.Interfaces;

namespace Endor.CBEL.Elements
{
    [Autocomplete("LinkedAssemblyVariableBase")]
    public class LinkedAssemblyVariable : ConsumptionStringVariable, ICBELVariableOverridable
    {
        public LinkedAssemblyVariable(ICBELAssembly parent) : base(parent) { }
        public ICBELAssembly AssemblyComponent => ( Component as ICBELAssembly );

        protected override void UpdateUserOVValues()
        {
            if (_Component != null)
            {
                ((ICBELAssembly)_Component).UserOVValues = UserOVValues.ChildOVValues;
                ((ICBELAssembly)_Component).SetOvValues();
            }
        }

        public override bool RollupLinkedPriceAndCost { get; set; }

        public override decimal? Cost
        {
            get
            {
                if (AssemblyComponent == null)
                    return null;

                return AssemblyComponent.TotalCost;
            }
        }

        public override decimal? Price
        {
            get
            {
                if (AssemblyComponent == null)
                    return null;

                return AssemblyComponent.TotalPrice;
            }
        }
    }
}
