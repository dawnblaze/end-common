﻿using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.Pricing;
using System;

namespace Endor.CBEL.Elements
{
    public class LayoutComputationAssemblyVariable : ConsumptionStringVariable, ICBELVariableOverridable, IHasLayoutComputationComponent
    {
        public LayoutComputationAssemblyVariable(ICBELAssembly parent) : base(parent)
        {
            ConsumptionQuantity.Value = 0m;
        }

        public ICBELLayoutComputation TypedComponent => Component as ICBELLayoutComputation;

        public override ICBELComponent Component
        {
            get
            {
                if (base.Component == null)
                    base.Component = new LayoutComputingAssembly(this.Value, Parent.Context, Parent.BID, Parent.Cache, Parent.Logger);

                return base.Component;
            }
            set => base.Component = value;
        }

        public decimal? Bleed => TypedComponent?.Bleed.Value;
        public decimal? BleedBottom => TypedComponent?.BleedBottom.Value;
        public decimal? BleedLeft => TypedComponent?.BleedLeft.Value;
        public decimal? BleedRight => TypedComponent?.BleedRight.Value;
        public decimal? BleedTop => TypedComponent?.BleedTop.Value;
        public decimal? ColorBarLength => TypedComponent?.ColorBarLength.Value;
        public string ColorBarPosition => TypedComponent?.ColorBarPosition.Value;
        public decimal? ColorBarThickness => TypedComponent?.ColorBarThickness.Value;
        public decimal? ColorsBack => TypedComponent?.ColorsBack.Value;
        public decimal? ColorsFront => TypedComponent?.ColorsFront.Value;
        public decimal? Gutter => TypedComponent?.Gutter.Value;
        public decimal? GutterHorizontal => TypedComponent?.GutterHorizontal.Value;
        public decimal? GutterVertical => TypedComponent?.GutterVertical.Value;
        public string ImageBackUniquenessRule => TypedComponent?.ImageBackUniquenessRule.Value;
        public string ImageItemUniquenessRule => TypedComponent?.ImageItemUniquenessRule.Value;
        public string ImageOrderUniquenessRule => TypedComponent?.ImageOrderUniquenessRule.Value;
        public string ImpositionType => TypedComponent?.ImpositionType.Value;
        public bool? IncludeColorBar => TypedComponent?.IncludeColorBar.Value;
        public bool? IncludeCuts => TypedComponent?.IncludeCuts.Value;
        public bool? IncludeVisualization => TypedComponent?.IncludeVisualization.Value;
        public bool? Is2Sided => TypedComponent?.Is2Sided.Value;
        public decimal? ItemHeight => TypedComponent?.ItemHeight.Value;
        public decimal? ItemQuantity => TypedComponent?.ItemQuantity.Value;
        public decimal? ItemWidth => TypedComponent?.ItemWidth.Value;
        public string LayoutType => TypedComponent?.LayoutType.Value;
        public decimal? LeaderBottom => TypedComponent?.LeaderBottom.Value;
        public decimal? LeaderLeft => TypedComponent?.LeaderLeft.Value;
        public decimal? LeaderRight => TypedComponent?.LeaderRight.Value;
        public decimal? LeaderTop => TypedComponent?.LeaderTop.Value;
        public decimal? MaterialHeight => TypedComponent?.MaterialHeight.Value;
        public decimal? MaterialMargin => TypedComponent?.MaterialMargin.Value;
        public decimal? MaterialMarginBottom => TypedComponent?.MaterialMarginBottom.Value;
        public decimal? MaterialMarginLeft => TypedComponent?.MaterialMarginLeft.Value;
        public decimal? MaterialMarginRight => TypedComponent?.MaterialMarginRight.Value;
        public decimal? MaterialMarginTop => TypedComponent?.MaterialMarginTop.Value;
        public decimal? MaterialRunningWastePercentage => TypedComponent?.MaterialRunningWastePercentage.Value;
        public decimal? MaterialSetupWasteCount => TypedComponent?.MaterialSetupWasteCount.Value;
        public decimal? TotalMaterialWasteCount => TypedComponent?.TotalMaterialWasteCount.Value;
        public decimal? MaterialWidth => TypedComponent?.MaterialWidth.Value;
        public decimal? MaxMaterialsPerCut => TypedComponent?.MaxMaterialsPerCut.Value;
        public decimal? OrderedQuantity => TypedComponent?.OrderedQuantity.Value;
        public bool? RotateItem => TypedComponent?.RotateItem.Value;
        public bool? RotateMaterial => TypedComponent?.RotateMaterial.Value;
        public bool? HideBleed => TypedComponent?.HideBleed.Value;
        public bool? HideColorBar => TypedComponent?.HideColorBar.Value;
        public bool? HideCutLines => TypedComponent?.HideCutLines.Value;
        public bool? HideFoldLines => TypedComponent?.HideFoldLines.Value;
        public bool? HideGutter => TypedComponent?.HideGutter.Value;
        public bool? HideLeader => TypedComponent?.HideLeader.Value;
        public bool? HideMargin => TypedComponent?.HideMargin.Value;
        public bool? HideMaterialDimensions => TypedComponent?.HideMaterialDimensions.Value;
        public bool? HideRegistrationMarks => TypedComponent?.HideRegistrationMarks.Value;
        public bool? HideWatermark => TypedComponent?.HideWatermark.Value;
        public bool? UseVisualization => TypedComponent?.UseVisualization.Value;
        public string Machine => TypedComponent?.Machine.Value;
        public string Material => TypedComponent?.Material.Value;
        public decimal? MachineMaxHorizontal => TypedComponent?.MachineMaxHorizontal.Value;
        public decimal? MachineMaxVertical => TypedComponent?.MachineMaxVertical.Value;
        public bool? HidePlaceholders => TypedComponent?.HidePlaceholders.Value;
        public decimal? CutsPerMaterial => TypedComponent?.CutsPerMaterial.Value;
        public decimal? ImageCount => TypedComponent?.ImageCount.Value;
        public bool? IsPaneled => TypedComponent?.IsPaneled.Value;
        public decimal? ItemsHorizontal => TypedComponent?.ItemsHorizontal.Value;
        public decimal? ItemsVertical => TypedComponent?.ItemsVertical.Value;
        public decimal? MaterialRunCount => TypedComponent?.MaterialRunCount.Value;
        public decimal? SignatureCount => TypedComponent?.SignatureCount.Value;
        public decimal? TotalCutCount => TypedComponent?.TotalCutCount.Value;
        public decimal? TotalImpressionCount => TypedComponent?.TotalImpressionCount.Value;
        public decimal? TotalItemArea => TypedComponent?.TotalItemArea.Value;
        public decimal? TotalItemFaceArea => TypedComponent?.TotalItemFaceArea.Value;
        public decimal? TotalMaterialCount => TypedComponent?.TotalMaterialCount.Value;
        public decimal? TotalWasteArea => TypedComponent?.TotalWasteArea.Value;
        public string VisualizationData => TypedComponent?.VisualizationData.Value;
        public decimal? ImpressionCountBack => TypedComponent?.ImpressionCountBack;
        public decimal? ImpressionCountFront => TypedComponent?.ImpressionCountFront;
        public bool? IsPaneledHorizontally => TypedComponent?.IsPaneledHorizontally;
        public bool? IsPaneledVertically => TypedComponent?.IsPaneledVertically;
        public decimal? ItemArea => TypedComponent?.ItemArea;
        public decimal? ItemsOnFirstPage => TypedComponent?.ItemsOnFirstPage;
        public decimal? ItemsOnLastPage => TypedComponent?.ItemsOnLastPage;
        public decimal? MaterialArea => TypedComponent?.MaterialArea;
        public decimal? RotatedItemHeight => TypedComponent?.RotatedItemHeight;
        public decimal? RotatedItemWidth => TypedComponent?.RotatedItemWidth;
        public decimal? RotatedMaterialHeight => TypedComponent?.RotatedMaterialHeight;
        public decimal? RotatedMaterialWidth => TypedComponent?.RotatedMaterialWidth;
        public decimal? TotalItemCount => TypedComponent?.TotalItemCount;
        public decimal? TotalMaterialArea => TypedComponent?.TotalMaterialArea;
        public decimal? LeftItemOffset => TypedComponent?.LeftItemOffset; //Max(NonPrintableSpace+bleed,Gutter)
        public decimal? EffectiveItemWidth => TypedComponent?.EffectiveItemWidth;
        public decimal? TopItemOffset => TypedComponent?.TopItemOffset;//Max(NonPrintableSpace+bleed,Gutter)
        public decimal? EffectiveItemHeight => TypedComponent?.EffectiveItemHeight;


        protected override void DoCompute()
        {
            this.Value = this.Name;
            base.DoCompute();
        }
    }
}
