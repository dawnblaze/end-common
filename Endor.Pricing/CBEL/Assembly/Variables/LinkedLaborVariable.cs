﻿using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;
using Endor.Pricing.CBEL.Common;
using Endor.Units;

namespace Endor.CBEL.Elements
{
    [Autocomplete("LinkedLaborVariableBase")]
    public class LinkedLaborVariable : ConsumptionStringVariable, ICBELVariableOverridable, IHasLaborComponent, IMeasurement
    {
        public LinkedLaborVariable(ICBELAssembly parent) : base(parent) { }

        public ICBELLaborComponent TypedComponent => Component as ICBELLaborComponent;

        [AutocompleteIgnore]
        public override OrderItemComponentType ComponentClassType => OrderItemComponentType.Labor;

        public string NameOnInvoice => TypedComponent?.NameOnInvoice;

        public string SKU => TypedComponent?.SKU;

        public string Description => TypedComponent?.Description;

        public decimal? SetupPrice => TypedComponent?.SetupPrice;

        public decimal? HourlyPrice => TypedComponent?.HourlyPrice;

        public Measurement BillingIncrement => TypedComponent?.BillingIncrement;
        public Measurement MinimumBillingTime => TypedComponent?.MinimumBillingTime;



        /// <summary>
        /// Labor has a specialized cost function. It equals fixed cost plus variable cost
        /// </summary>
        protected override decimal? RoundedCalculatedConsumption()
        {
            if (ConsumptionQuantity.IsOV)
                return ConsumptionQuantity.Value;

            return LaborQuantityHelper.ComputeQuantity(BillingIncrement, MinimumBillingTime, ConsumptionQuantity.Value);
        }

        #region Units
        /// <summary>
        /// Returns the UnitType of the number.
        /// </summary>
        [AutocompleteIgnore]
        public Unit Unit { get; set; }

        [AutocompleteIgnore]
        public UnitType UnitType => this.UnitInfo().UnitType;

        [AutocompleteIgnore]
        public decimal? ValueInUnits => RoundedCalculatedConsumption();

        [AutocompleteIgnore]
        public void LogConversionError(string message)
        {
            if (Parent != null)
                Parent.LogException(new CBELRuntimeException(message));
        }

        public string UnitName => Unit.Name();
        public string UnitTypeName => UnitType.Name();
        public string UnitSystemName => this.UnitInfo().UnitSystem.ToString();
        public string UnitClassificationName => this.UnitTypeInfo().Name;
        public int UnitDimension => MeasurementHelper.UnitDimension(this);
        #endregion Units
    }
}
