﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.CBEL.Elements;
using Endor.CBEL.Interfaces;

namespace Endor.CBEL.Elements
{
    public class PriceTable<ColumnDataType> : TierDataTable<ColumnDataType>
    {
        public PriceTable(ICBELAssembly parent) : base(parent) { }
    }
}
