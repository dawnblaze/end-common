﻿using Endor.CBEL.Elements;
using Endor.CBEL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Elements
{
    public class MarginTable<ColumnDataType> : TierDataTable<ColumnDataType>
    {
        public MarginTable(ICBELAssembly parent) : base(parent) { }
    }
}
