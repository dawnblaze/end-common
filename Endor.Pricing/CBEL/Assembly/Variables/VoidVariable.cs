﻿using Endor.CBEL.Interfaces;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    /// <summary>
    /// Implement the Assembly Property for Elements that don't have a value
    /// </summary>
    /// <param name="parent"></param>
    public class VoidVariable : ICBELVariable
    {
        /// <summary>
        /// We require the Parent Assembly be set during construction
        /// </summary>
        /// <param name="parent">The Assembly parent</param>
        public VoidVariable(ICBELAssembly parent)
        {
            Parent = parent;
        }

        /// <summary>
        /// Make the default constructor private so it isn't used.
        /// </summary>
        private VoidVariable() { }

        /// <summary>
        /// Reference to the Assembly that contains this variable.
        /// </summary>
        [JsonIgnore]
        public ICBELAssembly Parent { get; private set; }

        /// <summary>
        /// The display label for the variable.
        /// </summary>
        [JsonIgnore]
        public string Label { get; set; }

        /// <summary>
        /// The name of variable used in formulas. This is also the name of the property on 
        /// the Assembly object that references this variable.  It is defined on the Assembly as:
        ///    public {ElementPropertyClass} {name} {get; private set;}
        /// </summary>
        public string Name { get; set; }
    }
}


