﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.CBEL.Interfaces;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    /// <summary>
    /// This function computes the layout of an Image/Sign/Print on a Sheet/Board.
    /// It returns True if there are no errors or validation failures in the computation.
    /// </summary>
    public class CalculateLayoutOnRoll : CalculateLayoutBase
    {
        public CalculateLayoutOnRoll(ICBELAssembly parent) : base(parent) { }

        public override bool FormulaFunction()
        {
            bool Result = true;

            /* Control Pascal Code
                procedure TMethodCall.FxCalculateLayoutOnRoll;
                var
                  NonRotatedResults, RotatedResults, ActualResults: TLayoutOnRollRec;
                  OutputVariable, ItemHeight, ItemWidth, ItemQuantity,
                  RollWidth,
                  GapWidth, GapHeight,
                  RollMarginLeft, RollMarginRight,
                  ItemMarginTop, ItemMarginBottom, ItemMarginLeft, ItemMarginRight,
                  RollLeader, RollTrailer,
                  OverlapAmount,
                  CanRotateStatement, AvoidPanelingStatement, CanTileStatement: TSimpleStatement;
                  CanRotate, AvoidPaneling, CanTile: Boolean;
                  ErrorMessage: string;
                  ReturnValue: TValueRec;
                  ItemMarginLeftValue, ItemMarginTopValue,
                  ItemMarginRightValue, ItemMarginBottomValue,
                  GapHeightValue, GapWidthValue,
                  RollLeaderValue, RollTrailerValue,
                  RollMarginLeftValue, RollMarginRightValue, OverlapAmountValue: Extended;
                  TotalPanels, PanelsHigh, PanelsWideOnRoll, Quantity: Integer;

                  function CheckForMissingInformation : string;
                  begin
                    Result := '';

                    if ( ItemWidth = nil ) or ( ItemWidth.ValueAsNumber = 0 ) then
                        Result := Result + 'The Item Width cannot be zero.' + #13#10;
                    if ( ItemHeight = nil ) or ( ItemHeight.ValueAsNumber = 0 ) then
                        Result := Result + 'The Item Height cannot be zero.' + #13#10;
                    if ( RollWidth = nil ) or ( RollWidth.ValueAsNumber = 0 ) then
                        Result := Result + 'The Roll Width cannot be zero.'+ #13#10;
                  end;

                  function DoCalculation(const aIsRotated: Boolean): TLayoutOnRollRec;
                  var
                    WorkingRollWidth, WorkingItemHeight, WorkingItemWidth: Extended;
                  begin
                    Result.HasError := False;
                    Result.ErrorMessage := '';

                    Result.IsRotated          := aIsRotated;
                    Result.IsTiled            := False;
                    Result.IsPaneled          := False;

                    Result.LayoutHeight       := 0.00;
                    Result.LayoutWidth        := 0.00;
                    Result.PrintsHighPerPanel := 0   ;
                    Result.PrintsWide         := 0.00;
                    Result.PanelsWide         := 0   ;
                    Result.TotalProducedArea  := 0.00;
                    Result.TotalRollLength    := 0.00;
                    Result.TotalRollArea      := 0.00;
                    Result.TotalScrapArea     := 0.00;

                    try
                      // Next, Calculate the material usage
                      WorkingRollWidth  := RollWidth.ValueAsNumber  - RollMarginLeftValue  - RollMarginRightValue ;

                      // Detemine the effective height and width of the item
                      if aIsRotated then
                      begin
                        Result.LayoutHeight  := ItemWidth.ValueAsNumber;
                        Result.LayoutWidth   := ItemHeight.ValueAsNumber;

                        WorkingItemHeight  := ItemMarginLeftValue  + ItemWidth.ValueAsNumber  + ItemMarginRightValue ;
                        WorkingItemWidth   := ItemMarginTopValue  + ItemHeight.ValueAsNumber  + ItemMarginBottomValue ;
                      end
                      else
                      begin
                        Result.LayoutHeight  := ItemHeight.ValueAsNumber;
                        Result.LayoutWidth   := ItemWidth.ValueAsNumber;

                        WorkingItemHeight  := ItemMarginTopValue  + ItemHeight.ValueAsNumber  + ItemMarginBottomValue ;
                        WorkingItemWidth   := ItemMarginLeftValue  + ItemWidth.ValueAsNumber  + ItemMarginRightValue ;
                      end;

                      // Calculate the PrintsWide
                      if TEqualityUtils.IsGreaterThan( WorkingItemWidth, WorkingRollWidth ) then
                      begin
                        // If the print's width is greater than the roll's width, we must panel
                        Result.PanelsWide  := RoundUp( (WorkingItemWidth - OverlapAmountValue) / (WorkingRollWidth - OverlapAmountValue) );
                        Result.PrintsWide  := TNumberUtils.RoundToFactor( 1 / Result.PanelsWide, 0.0001 );
                        PanelsWideOnRoll   := 1;
                      end
                      else if not CanTile then
                      begin
                        // If the print's width is less than the roll's width and tiling is not allowed, then there is one print wide per roll
                        Result.PanelsWide  := 1;
                        Result.PrintsWide  := 1;
                        PanelsWideOnRoll   := 1;
                      end
                      else
                      begin
                        // If the print's width is less than the roll's width and tiling is allowed, then calculate the number of prints wide
                        Result.PanelsWide  := 1;
                        Result.PrintsWide  := Trunc( (WorkingRollWidth + GapWidthValue ) / (WorkingItemWidth + GapWidthValue ) + 0.00005 );
                        PanelsWideOnRoll   := Trunc( Result.PrintsWide );
                      end;

                      TotalPanels  := Result.PanelsWide * Quantity;
                      PanelsHigh   := RoundUp( TotalPanels / PanelsWideOnRoll );

                      Result.PrintsHighPerPanel := PanelsHigh;

                      Result.TotalRollLength   := RollLeaderValue
                                                  + ( WorkingItemHeight * PanelsHigh )
                                                  + ( GapHeightValue * ( PanelsHigh-1 ) )
                                                  + RollTrailerValue;
                      Result.TotalProducedArea := TNumberUtils.RoundToFactor( Quantity * ( ItemHeight.ValueAsNumber * ItemWidth.ValueAsNumber ), 0.0001 );
                      Result.TotalRollArea     := TNumberUtils.RoundToFactor( Result.TotalRollLength * RollWidth.ValueAsNumber, 0.0001 );
                      Result.TotalScrapArea    := TNumberUtils.RoundToFactor( Result.TotalRollArea - Result.TotalProducedArea, 0.0001 );
                      Result.IsTiled           := ( TotalPanels > 1 ) and ( PanelsWideOnRoll > 1 );
                      Result.IsPaneled         := (Result.PanelsWide > 1);
                    except
                      on E:Exception do
                      begin
                        Result.HasError := True;
                        Result.ErrorMessage := E.Message;
                      end;
                    end;
                  end;

                begin
                  ReturnValue.DataType := dtNumber;

                {$REGION 'Get Passed Parameters'}
                    // Get the required parameters
                    OutputVariable     := Parameters[0]; // None - Required
                    ItemHeight         := Parameters[1]; // None - Required
                    ItemWidth          := Parameters[2]; // None - Required
                    RollWidth          := Parameters[4]; // None - Required

                    if Assigned( ItemHeight ) then
                    begin
                      ItemHeight.Evaluate;
                      FFirmlyEvaluated := ItemHeight.FirmlyEvaluated;
                    end;

                    if Assigned( RollWidth ) then
                    begin
                      RollWidth.Evaluate;
                      FFirmlyEvaluated := FFirmlyEvaluated and RollWidth.FirmlyEvaluated;
                    end;

                    if Assigned( ItemWidth ) then
                    begin
                      ItemWidth.Evaluate;
                      FFirmlyEvaluated := FFirmlyEvaluated and ItemWidth.FirmlyEvaluated;
                    end;

                    // Get the passed parameters and use the default value if not passed
                    ItemQuantity           := RetrieveParameter( FParameters, 3  , 1 );
                    GapHeight              := RetrieveParameter( FParameters, 5  , 0 );
                    GapWidth               := RetrieveParameter( FParameters, 6  , 0 );
                    ItemMarginTop          := RetrieveParameter( FParameters, 7  , 0 );
                    ItemMarginBottom       := RetrieveParameter( FParameters, 8  , 0 );
                    ItemMarginLeft         := RetrieveParameter( FParameters, 9  , 0 );
                    ItemMarginRight        := RetrieveParameter( FParameters, 9  , 0 );
                    RollLeader             := RetrieveParameter( FParameters, 11 , 0 );
                    RollTrailer            := RetrieveParameter( FParameters, 12 , 0 );
                    RollMarginLeft         := RetrieveParameter( FParameters, 13 , 0 );
                    RollMarginRight        := RetrieveParameter( FParameters, 14 , 0 );
                    OverlapAmount          := RetrieveParameter( FParameters, 15 , 0 );
                    CanRotateStatement     := RetrieveParameter( FParameters, 16 , 1 );
                    CanTileStatement       := RetrieveParameter( FParameters, 17 , 1 );
                    AvoidPanelingStatement := RetrieveParameter( FParameters, 18 , 1 );

                    Quantity      := Trunc( ItemQuantity.ValueAsNumber );
                    CanRotate     := ( CanRotateStatement     = nil ) or  CanRotateStatement.ValueAsBoolean;
                    CanTile       := ( CanTileStatement       = nil ) or  CanTileStatement.ValueAsBoolean;
                    AvoidPaneling := ( AvoidPanelingStatement = nil ) or  AvoidPanelingStatement.ValueAsBoolean;

                    if Assigned(  GapHeight ) then
                      GapHeightValue := GapHeight.ValueAsNumber
                    else
                      GapHeightValue := 0.00;

                    if Assigned(  GapWidth ) then
                      GapWidthValue := GapWidth.ValueAsNumber
                    else
                      GapWidthValue := 0.00;

                    if Assigned(  ItemMarginTop ) then
                      ItemMarginTopValue := ItemMarginTop.ValueAsNumber
                    else
                      ItemMarginTopValue := 0.00;

                    if Assigned(  ItemMarginBottom ) then
                      ItemMarginBottomValue := ItemMarginBottom.ValueAsNumber
                    else
                      ItemMarginBottomValue := 0.00;

                    if Assigned(  ItemMarginLeft ) then
                      ItemMarginLeftValue := ItemMarginLeft.ValueAsNumber
                    else
                      ItemMarginLeftValue := 0.00;

                    if Assigned(  ItemMarginRight ) then
                      ItemMarginRightValue := ItemMarginRight.ValueAsNumber
                    else
                      ItemMarginRightValue := 0.00;

                    if Assigned(  RollLeader ) then
                      RollLeaderValue := RollLeader.ValueAsNumber
                    else
                      RollLeaderValue := 0.00;

                    if Assigned(  RollTrailer ) then
                      RollTrailerValue := RollTrailer.ValueAsNumber
                    else
                      RollTrailerValue := 0.00;

                    if Assigned(  RollMarginLeft ) then
                      RollMarginLeftValue := RollMarginLeft.ValueAsNumber
                    else
                      RollMarginLeftValue := 0.00;

                    if Assigned(  RollMarginRight ) then
                      RollMarginRightValue := RollMarginRight.ValueAsNumber
                    else
                      RollMarginRightValue := 0.00;

                    if Assigned(  OverlapAmount ) then
                      OverlapAmountValue := OverlapAmount.ValueAsNumber
                    else
                      OverlapAmountValue := 0.00;
                {$ENDREGION}

                  ErrorMessage := CheckForMissingInformation;
                  if Length(ErrorMessage) > 0 then
                  begin
                    SetVariableProperty( OutputVariable, 'ErrorMessage'            , ErrorMessage );
                    ReturnValue.NumberValue := 0;
                    Value := ReturnValue;
                    exit;
                  end;

                {$REGION 'Perform Calculations'}
                  // Perform Calculations on Sheet Layout
                  NonRotatedResults := DoCalculation(False);

                  if NonRotatedResults.HasError then
                  begin
                    ActualResults := NonRotatedResults;
                    ErrorMessage  := NonRotatedResults.ErrorMessage;
                  end

                  else if CanRotate then
                  begin
                    RotatedResults := DoCalculation(True);
                    if RotatedResults.HasError then
                    begin
                      ActualResults := RotatedResults;
                      ErrorMessage  := RotatedResults.ErrorMessage;
                    end

                    else
                    begin
                      // Determine which orientation minimizes scrap.
                      if TEqualityUtils.IsLessThan( RotatedResults.TotalScrapArea, NonRotatedResults.TotalScrapArea ) then
                        ActualResults := RotatedResults
                      else
                        ActualResults := NonRotatedResults;

                      if AvoidPaneling then
                      begin
                        // If Avoiding Paneling, determine which orientation minimizes panels
                        if RotatedResults.PanelsWide > NonRotatedResults.PanelsWide then
                          ActualResults := NonRotatedResults
                        else if RotatedResults.PanelsWide < NonRotatedResults.PanelsWide then
                          ActualResults := RotatedResults;

                        // If panel counts are equal, stay with the orientation that minimizes scrap
                      end;
                    end;
                  end
                  else
                    ActualResults := NonRotatedResults;
                {$ENDREGION}

                {$REGION 'Set Variable properties'}
                    // Set the variable properties, if passed into the function
                    SetVariableProperty( OutputVariable, 'LayoutHeight'       , FloatToStr( ActualResults.LayoutHeight             ));
                    SetVariableProperty( OutputVariable, 'LayoutWidth'        , FloatToStr( ActualResults.LayoutWidth              ));
                    SetVariableProperty( OutputVariable, 'PrintsHighPerPanel' , FloatToStr( ActualResults.PrintsHighPerPanel       ));
                    SetVariableProperty( OutputVariable, 'PrintsWide'         , FloatToStr( ActualResults.PrintsWide               ));
                    SetVariableProperty( OutputVariable, 'PanelsWide'         , FloatToStr( ActualResults.PanelsWide               ));
                    SetVariableProperty( OutputVariable, 'TotalProducedArea'  , FloatToStr( ActualResults.TotalProducedArea        ));
                    SetVariableProperty( OutputVariable, 'TotalRollLength'    , FloatToStr( ActualResults.TotalRollLength          ));
                    SetVariableProperty( OutputVariable, 'TotalRollArea'      , FloatToStr( ActualResults.TotalRollArea            ));
                    SetVariableProperty( OutputVariable, 'TotalScrapArea'     , FloatToStr( ActualResults.TotalScrapArea           ));
                    SetVariableProperty( OutputVariable, 'ErrorMessage'       , ErrorMessage                                        );

                    SetVariableProperty( OutputVariable, 'IsRotated'          , IfThen( ActualResults.IsRotated, '1' , '0'         ));
                    SetVariableProperty( OutputVariable, 'IsTiled'            , IfThen( ActualResults.IsTiled,   '1' , '0'         ));
                    SetVariableProperty( OutputVariable, 'IsPaneled'          , IfThen( ActualResults.IsPaneled, '1' , '0'         ));
                {$ENDREGION}

                  // return a boolean value indicating whether the functin was successful
                  // or not
                  ReturnValue.NumberValue := 1;
                  Value := ReturnValue;
                end;

           */

            return Result;
        }
    }
}
