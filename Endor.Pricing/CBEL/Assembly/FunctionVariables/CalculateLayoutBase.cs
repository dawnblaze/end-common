﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    /// <summary>
    /// This function computes the layout of an Image/Sign/Print on a Roll
    /// It returns True if there are no errors or validation failures in the computation.
    /// </summary>
    public class CalculateLayoutBase : CBELFunctionVariableBase<bool>
    {
        public CalculateLayoutBase(ICBELAssembly parent) : base(parent) { }

        [JsonIgnore] public override DataType DataType => DataType.Boolean;
        [JsonIgnore] public override string DataTypeName => "boolean";

        public override bool FormulaFunction()
        {
            bool Result = true;



            return Result;
        }


        public override bool? AsBoolean
        {
            get
            {
                return Value;
            }
            set
            {
                Value = value.GetValueOrDefault(false);
            }
        }

        public override decimal? AsNumber
        {
            get
            {
                bool? v = Value;
                if (v == null)
                    return null;
                else
                    return (v.Value ? 1 : 0);
            }
            set
            {
                if (!value.HasValue)
                    Value = false;
                else
                    Value = (value.Value != 0);
            }
        }

        public override ICBELObject AsObject() => null;

        public override string AsString
        {
            get

            {
                bool? v = Value;
                if (v == null)
                    return null;
                else
                    return ((bool)v ? "true" : "false");
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    Value = false;
                else
                    Value = "YyTt1".Contains(value[0]);
            }
        }

        protected override bool ToT(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? false : bool.Parse(value);
        }
    }
}
