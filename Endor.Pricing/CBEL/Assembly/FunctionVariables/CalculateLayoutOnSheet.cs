﻿using System;
using System.Collections.Generic;
using System.Text;
using Endor.CBEL.Interfaces;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    /// <summary>
    /// This function computes the layout of an Image/Sign/Print on a Sheet/Board.
    /// It returns True if there are no errors or validation failures in the computation.
    /// </summary>
    public class CalculateLayoutOnSheet : CalculateLayoutBase
    {
        public CalculateLayoutOnSheet(ICBELAssembly parent) : base(parent) { }

        public override bool FormulaFunction()
        {
            bool Result = true;

            /* Control Pascal Code
                procedure TMethodCall.FxCalculateLayoutOnSheet;
                var
                  NonRotatedResults, RotatedResults, ActualResults: TLayoutOnSheetRec;
                  OutputVariable, ItemHeight, ItemWidth, ItemQuantity,
                  SheetHeight, SheetWidth,
                  DuplicateTwoSided,
                  GapWidth, GapHeight,
                  ItemMarginLeft, ItemMarginRight, ItemMarginTop, ItemMarginBottom,
                  SheetMarginLeft, SheetMarginRight, SheetMarginTop, SheetMarginBottom,
                  OverlapAmount,
                  CanRotateStatement, AvoidPanelingStatement, CanTileStatement: TSimpleStatement;
                  CanRotate, AvoidPaneling, CanTile: Boolean;
                  SheetMarginTopValue, SheetMarginLeftValue,
                  ItemMarginTopValue, ItemMarginBottomValue,
                  ItemMarginLeftValue, ItemMarginRightValue,
                  SheetMarginBottomValue, SheetMarginRightValue,
                  GapHeightValue, GapWidthValue, OverlapAmountValue: Extended;
                  ErrorMessage: string;
                  ReturnValue: TValueRec;
                  Quantity: Integer;
                  PanelsWidePerSheet, PanelsHighPerSheet, PanelsPerSheet, TotalPanels: Integer;

                  function CheckForMissingInformation : string;
                  begin
                    Result := '';

                    if ( ItemWidth = nil ) or ( ItemWidth.ValueAsNumber = 0 ) then
                        Result := Result + 'The Item Width cannot be zero.' + #13#10;
                    if ( ItemHeight = nil ) or ( ItemHeight.ValueAsNumber = 0 ) then
                        Result := Result + 'The Item Height cannot be zero.' + #13#10;

                    if ( SheetWidth = nil ) or ( SheetWidth.ValueAsNumber = 0 ) then
                        Result := Result + 'The Sheet Width cannot be zero.'+ #13#10;
                    if ( SheetHeight = nil ) or ( SheetHeight.ValueAsNumber = 0 ) then
                        Result := Result + 'The Sheet Height cannot be zero.'+ #13#10;
                  end;

                  function DoCalculation(const aIsRotated: Boolean): TLayoutOnSheetRec;
                  var
                    WorkingSheetHeight, WorkingSheetWidth,
                    WorkingItemHeight, WorkingItemWidth: Extended;
                  begin
                    Result.IsRotated                 := aIsRotated;
                    Result.IsTiled                   := False;
                    Result.IsPaneled                 := False;

                    Result.LayoutHeight              := 0.00;
                    Result.LayoutWidth               := 0.00;
                    Result.PrintsHigh                := 0.00;
                    Result.PanelsHigh                := 0   ;
                    Result.PrintsWide                := 0.00;
                    Result.PanelsWide                := 0   ;
                    Result.PrintsPerSheet            := 0.00;
                    Result.LayoutSheets              := 0   ;
                    Result.TotalProducedArea         := 0.00;
                    Result.TotalProducedSurfaceArea  := 0.00;
                    Result.TotalSheetArea            := 0.00;
                    Result.TotalScrapArea            := 0.00;

                    try
                      WorkingSheetHeight := SheetHeight.ValueAsNumber  - SheetMarginTopValue  - SheetMarginBottomValue ;
                      WorkingSheetWidth  := SheetWidth.ValueAsNumber  - SheetMarginLeftValue  - SheetMarginRightValue ;

                      if aIsRotated then
                      begin
                        Result.LayoutHeight  := ItemWidth.ValueAsNumber;
                        Result.LayoutWidth   := ItemHeight.ValueAsNumber;

                        WorkingItemHeight  := ItemMarginLeftValue  + ItemWidth.ValueAsNumber  + ItemMarginRightValue ;
                        WorkingItemWidth   := ItemMarginTopValue  + ItemHeight.ValueAsNumber  + ItemMarginBottomValue ;
                      end
                      else
                      begin
                        Result.LayoutHeight  := ItemHeight.ValueAsNumber;
                        Result.LayoutWidth   := ItemWidth.ValueAsNumber;

                        WorkingItemHeight  := ItemMarginTopValue  + ItemHeight.ValueAsNumber  + ItemMarginBottomValue;
                        WorkingItemWidth   := ItemMarginLeftValue  + ItemWidth.ValueAsNumber  + ItemMarginRightValue;
                      end;

                      // Calculate the PrintsWide
                      if TEqualityUtils.IsGreaterThan( WorkingItemWidth, WorkingSheetWidth ) then
                      begin
                        // If the print's width is greater than the sheet's width, we must panel
                        Result.PanelsWide  := RoundUp( (WorkingItemWidth - OverlapAmountValue) / (WorkingSheetWidth - OverlapAmountValue) );
                        Result.PrintsWide  := TNumberUtils.RoundToFactor( 1 / Result.PanelsWide, 0.0001 );
                        PanelsWidePerSheet := 1;
                      end
                      else if not CanTile then
                      begin
                        // If the print's width is less than the sheet's width and tiling is not allowed, then there is one print wide per sheet
                        Result.PanelsWide  := 1;
                        Result.PrintsWide  := 1;
                        PanelsWidePerSheet := 1;
                      end
                      else
                      begin
                        // If the print's width is less than the sheet's width and tiling is allowed, then calculate the number of prints wide
                        Result.PanelsWide  := 1;
                        Result.PrintsWide  := Trunc( (WorkingSheetWidth + GapWidthValue ) / (WorkingItemWidth + GapWidthValue ) + 0.00005 );
                        PanelsWidePerSheet := Trunc( Result.PrintsWide );
                      end;

                      // Calculate the PrintsHigh
                      if TEqualityUtils.IsGreaterThan( WorkingItemHeight , WorkingSheetHeight ) then
                      begin
                        // If the print's height is greater than the sheet's height, we must panel
                        Result.PanelsHigh  := RoundUp( (WorkingItemHeight - OverlapAmountValue) / (WorkingSheetHeight - OverlapAmountValue) );
                        Result.PrintsHigh  := TNumberUtils.RoundToFactor( 1 / Result.PanelsHigh, 0.0001 );
                        PanelsHighPerSheet := 1;
                      end
                      else if not CanTile then
                      begin
                        // If the print's height is less than the sheet's height and tiling is not allowed, then there is one print high per sheet
                        Result.PanelsHigh  := 1;
                        Result.PrintsHigh  := 1;
                        PanelsHighPerSheet := 1;
                      end
                      else
                      begin
                        // If the print's height is less than the sheet's height and tiling is allowed, then calculate the number of prints high
                        Result.PanelsHigh  := 1;
                        Result.PrintsHigh  := Trunc( (WorkingSheetHeight + GapHeightValue ) / (WorkingItemHeight + GapHeightValue ) + 0.00005 );
                        PanelsHighPerSheet := Trunc( Result.PrintsHigh );
                      end;

                      Result.PanelsPerPrint := Result.PanelsWide * Result.PanelsHigh;
                      Result.PrintsPerSheet := Trunc( (Result.PrintsWide * Result.PrintsHigh)  + 0.00005 );

                      PanelsPerSheet      := PanelsWidePerSheet * PanelsHighPerSheet;
                      TotalPanels         := Result.PanelsPerPrint * Quantity;
                      Result.LayoutSheets := RoundUp( TotalPanels / PanelsPerSheet );

                      Result.TotalProducedArea := TNumberUtils.RoundToFactor( Quantity * ( ItemHeight.ValueAsNumber * ItemWidth.ValueAsNumber ), 0.0001 );
                      Result.TotalSheetArea    := TNumberUtils.RoundToFactor( Result.LayoutSheets * ( SheetHeight.ValueAsNumber * SheetWidth.ValueAsNumber ), 0.0001 );
                      Result.TotalScrapArea    := TNumberUtils.RoundToFactor( Result.TotalSheetArea - Result.TotalProducedArea, 0.0001 );
                      Result.IsTiled           := ( TotalPanels > 1 ) and ( PanelsPerSheet > 1 );
                      Result.IsPaneled         := ( Result.PanelsPerPrint > 1 );

                      if ( DuplicateTwoSided.ValueAsBoolean ) then
                        Result.TotalProducedSurfaceArea := Result.TotalProducedArea * 2
                      else
                        Result.TotalProducedSurfaceArea := Result.TotalProducedArea;
                    except
                      on E:Exception do
                      begin
                        Result.HasError := True;
                        Result.ErrorMessage := E.Message;
                      end;
                    end;
                  end;

                begin
                  ReturnValue.DataType := dtNumber;

                {$REGION 'Get Passed Parameters'}
                    // Get the required parameters
                    OutputVariable     := Parameters[0]; // None - Required
                    ItemHeight         := Parameters[1]; // None - Required
                    ItemWidth          := Parameters[2]; // None - Required
                    SheetHeight        := Parameters[5]; // None - Required
                    SheetWidth         := Parameters[6]; // None - Required

                    if Assigned( ItemHeight ) then
                    begin
                      ItemHeight.Evaluate;
                      FFirmlyEvaluated := ItemHeight.FirmlyEvaluated;
                    end;

                    if Assigned( ItemWidth ) then
                    begin
                      ItemWidth.Evaluate;
                      FFirmlyEvaluated := FFirmlyEvaluated and ItemWidth.FirmlyEvaluated;
                    end;

                    if Assigned( SheetHeight ) then
                    begin
                      SheetHeight.Evaluate;
                      FFirmlyEvaluated := FFirmlyEvaluated and SheetHeight.FirmlyEvaluated;
                    end;

                    if Assigned( SheetWidth ) then
                    begin
                      SheetWidth.Evaluate;
                      FFirmlyEvaluated := FFirmlyEvaluated and SheetWidth.FirmlyEvaluated;
                    end;

                    // Get the passed parameters and use the default value is not passed
                    ItemQuantity           := RetrieveParameter( FParameters, 3 , 1 );
                    DuplicateTwoSided      := RetrieveParameter( FParameters, 4 , 0 );
                    GapHeight              := RetrieveParameter( FParameters, 7 , 0 );
                    GapWidth               := RetrieveParameter( FParameters, 8 , 0 );
                    ItemMarginTop          := RetrieveParameter( FParameters, 9 , 0 );
                    ItemMarginBottom       := RetrieveParameter( FParameters, 10, 0 );
                    ItemMarginLeft         := RetrieveParameter( FParameters, 11, 0 );
                    ItemMarginRight        := RetrieveParameter( FParameters, 12, 0 );
                    SheetMarginTop         := RetrieveParameter( FParameters, 13, 0 );
                    SheetMarginBottom      := RetrieveParameter( FParameters, 14, 0 );
                    SheetMarginLeft        := RetrieveParameter( FParameters, 15, 0 );
                    SheetMarginRight       := RetrieveParameter( FParameters, 16, 0 );
                    OverlapAmount          := RetrieveParameter( FParameters, 17, 0 );
                    CanRotateStatement     := RetrieveParameter( FParameters, 18, 1 );
                    CanTileStatement       := RetrieveParameter( FParameters, 19, 1 );
                    AvoidPanelingStatement := RetrieveParameter( FParameters, 20, 1 );

                    Quantity      := Trunc( ItemQuantity.ValueAsNumber );
                    CanRotate     := ( CanRotateStatement     = nil ) or  CanRotateStatement.ValueAsBoolean;
                    CanTile       := ( CanTileStatement       = nil ) or  CanTileStatement.ValueAsBoolean;
                    AvoidPaneling := ( AvoidPanelingStatement = nil ) or  AvoidPanelingStatement.ValueAsBoolean;

                    if Assigned( GapHeight ) then
                      GapHeightValue := GapHeight.ValueAsNumber
                    else
                      GapHeightValue := 0.00;

                    if Assigned( GapWidth ) then
                      GapWidthValue := GapWidth.ValueAsNumber
                    else
                      GapWidthValue := 0.00;

                    if Assigned( ItemMarginTop ) then
                      ItemMarginTopValue := ItemMarginTop.ValueAsNumber
                    else
                      ItemMarginTopValue := 0.00;

                    if Assigned( ItemMarginBottom ) then
                      ItemMarginBottomValue := ItemMarginBottom.ValueAsNumber
                    else
                      ItemMarginBottomValue := 0.00;

                    if Assigned( ItemMarginLeft ) then
                      ItemMarginLeftValue := ItemMarginLeft.ValueAsNumber
                    else
                      ItemMarginLeftValue := 0.00;

                    if Assigned( ItemMarginRight ) then
                      ItemMarginRightValue := ItemMarginRight.ValueAsNumber
                    else
                      ItemMarginRightValue := 0.00;

                    if Assigned( SheetMarginTop ) then
                      SheetMarginTopValue := SheetMarginTop.ValueAsNumber
                    else
                      SheetMarginTopValue := 0.00;

                    if Assigned( SheetMarginBottom ) then
                      SheetMarginBottomValue := SheetMarginBottom.ValueAsNumber
                    else
                      SheetMarginBottomValue := 0.00;

                    if Assigned( SheetMarginLeft ) then
                      SheetMarginLeftValue := SheetMarginLeft.ValueAsNumber
                    else
                      SheetMarginLeftValue := 0.00;

                    if Assigned( SheetMarginRight ) then
                      SheetMarginRightValue := SheetMarginRight.ValueAsNumber
                    else
                      SheetMarginRightValue := 0.00;

                    if Assigned( OverlapAmount ) then
                      OverlapAmountValue := OverlapAmount.ValueAsNumber
                    else
                      OverlapAmountValue := 0.00;
                {$ENDREGION}

                  ErrorMessage := CheckForMissingInformation;
                  if Length(ErrorMessage) > 0 then
                  begin
                    SetVariableProperty( OutputVariable, 'ErrorMessage'            , ErrorMessage );
                    ReturnValue.NumberValue := 0;
                    Value := ReturnValue;
                    exit;
                  end;

                {$REGION 'Perform Calculations'}
                  // Perform Calculations on Sheet Layout
                  NonRotatedResults := DoCalculation(False);

                  if NonRotatedResults.HasError then
                  begin
                    ActualResults := NonRotatedResults;
                    ErrorMessage  := NonRotatedResults.ErrorMessage;
                  end

                  else if not CanRotate then
                    ActualResults := NonRotatedResults

                  else
                  begin
                    RotatedResults := DoCalculation(True);
                    if RotatedResults.HasError then
                    begin
                      ActualResults := RotatedResults;
                      ErrorMessage  := RotatedResults.ErrorMessage;
                    end

                    else
                    begin
                      // Determine which orientation minimizes scrap.
                      if TEqualityUtils.IsLessThan( RotatedResults.TotalScrapArea, NonRotatedResults.TotalScrapArea ) then
                        ActualResults := RotatedResults
                      else
                        ActualResults := NonRotatedResults;

                      if AvoidPaneling then
                      begin
                        // If Avoiding Paneling, determine which orientation minimizes panels
                        if RotatedResults.PanelsPerPrint > NonRotatedResults.PanelsPerPrint then
                          ActualResults := NonRotatedResults
                        else if RotatedResults.PanelsPerPrint < NonRotatedResults.PanelsPerPrint then
                          ActualResults := RotatedResults;

                        // If panel counts are equal, stay with the orientation that minimizes scrap
                      end;
                    end;
                  end;
                {$ENDREGION}

                {$REGION 'Set Variable Properties'}
                    // Set the variable properties, if passed into the function
                    SetVariableProperty( OutputVariable, 'LayoutHeight'            , FloatToStr( ActualResults.LayoutHeight             ));
                    SetVariableProperty( OutputVariable, 'LayoutWidth'             , FloatToStr( ActualResults.LayoutWidth              ));
                    SetVariableProperty( OutputVariable, 'PrintsHigh'              , FloatToStr( ActualResults.PrintsHigh               ));
                    SetVariableProperty( OutputVariable, 'PrintsWide'              , FloatToStr( ActualResults.PrintsWide               ));
                    SetVariableProperty( OutputVariable, 'PrintsPerSheet'          , FloatToStr( ActualResults.PrintsPerSheet           ));
                    SetVariableProperty( OutputVariable, 'LayoutSheets'            , FloatToStr( ActualResults.LayoutSheets             ));
                    SetVariableProperty( OutputVariable, 'TotalProducedArea'       , FloatToStr( ActualResults.TotalProducedArea        ));
                    SetVariableProperty( OutputVariable, 'TotalProducedSurfaceArea', FloatToStr( ActualResults.TotalProducedSurfaceArea ));
                    SetVariableProperty( OutputVariable, 'TotalSheetArea'          , FloatToStr( ActualResults.TotalSheetArea           ));
                    SetVariableProperty( OutputVariable, 'TotalScrapArea'          , FloatToStr( ActualResults.TotalScrapArea           ));

                    SetVariableProperty( OutputVariable, 'IsRotated'          , IfThen( ActualResults.IsRotated, '1' , '0'         ));
                    SetVariableProperty( OutputVariable, 'IsTiled'            , IfThen( ActualResults.IsTiled,   '1' , '0'         ));
                    SetVariableProperty( OutputVariable, 'IsPaneled'          , IfThen( ActualResults.IsPaneled, '1' , '0'         ));

                    SetVariableProperty( OutputVariable, 'ErrorMessage'            , ErrorMessage );
                {$ENDREGION}

                  // return a boolean value indicating whether the functin was successful
                  // or not
                  ReturnValue.NumberValue := 1;
                  Value := ReturnValue;
                end;
            */

            return Result;
        }
    }
}
