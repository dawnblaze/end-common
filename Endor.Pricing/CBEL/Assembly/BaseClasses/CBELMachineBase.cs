﻿using Endor.CBEL.Interfaces;
using Endor.EF;
using System.Collections.Generic;
using Endor.Tenant;
using Endor.Models.Autocomplete;
using Endor.Logging.Client;
using Endor.Models;

namespace Endor.CBEL.Elements
{
    [Autocomplete("MachineBase")]
    public abstract class CBELMachineBase : CBELAssemblyBase, ICBELMachine
    {
        public CBELMachineBase(ApiContext ctx, short bid, ITenantDataCache cache, RemoteLogger logger) : base(ctx, bid, cache, logger)
        {
            // The ProfileList needs to be filled in by the MachineGenerator Class
            ProfileList = new List<string>();

            // The Instance needs to be filled in by the MachineGenerator Class
            InstanceList = new List<string>();

            InitializeProfileList();
            InitializeInstanceList();
        }

        /// <summary>
        /// The ClassTypeID of the Machine
        /// </summary>
        /// <returns></returns>
        [AutocompleteIgnore]
        public override int? ClassTypeID => ClassType.Machine.ID();

        private DropDownStringListElement _Profile;
        /// <summary>
        /// The currently selected Profile for the Machine.
        /// Every Machine Template Must Have the Profile drop-down, even if it only has one profile
        /// </summary>
        public DropDownStringListElement Profile
        {
            get
            {
                if (_Profile == null)
                {
                    _Profile = new DropDownStringListElement(this)
                    {
                        Name = "Profile",
                        Label = "Profile",
                        IsRequired = true
                    };
                    _Profile.ListValues = ProfileList;
                }

                return _Profile;
            }
        }

        private DropDownStringListElement _Instance;
        /// <summary>
        /// The currently selected Instance for the Machine.
        /// Every Machine Template Must Have the Instance drop-down, even if it only has one instance
        /// </summary>
        public DropDownStringListElement Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new DropDownStringListElement(this)
                    {
                        Name = "Instance",
                        Label = "Instance",
                        IsRequired = false
                    };
                    _Instance.ListValues = InstanceList;
                }

                return _Instance;
            }
        }

        private DropDownStringListElement _LayoutType;
        /// <summary>
        /// The MachineLayoutType for the currently selected Profile
        /// </summary>
        public DropDownStringListElement LayoutType
        {
            get
            {
                if (_LayoutType == null)
                {
                    _LayoutType = new DropDownStringListElement(this)
                    {
                        Name = "LayoutType",
                        Label = "LayoutType",
                        IsRequired = true,
                    };
                    _LayoutType.ListValues = new List<string>()
                        {
                            "None",
                            "Roll",
                            "Sheet"
                        };
                }

                return _LayoutType;
            }
        }

        [AutocompleteIgnore]
        public List<string> ProfileList { get; private set; }

        [AutocompleteIgnore]
        public List<string> InstanceList { get; private set; }


        private MachineTimeVariable _MachineTime;
        public MachineTimeVariable MachineTime
        {
            get
            {
                if (_MachineTime == null)
                {
                    _MachineTime = new MachineTimeVariable(this)
                    {
                        Name = "MachineTime",
                        Label = "Machine Time",
                        IsRequired = false,
                        LinkedMachineID = (short?)ID,
                        DefaultValueConstant = "Machine Time",
                    };
                }

                return _MachineTime;
            }
        }


        public override void InitializeVariables()
        {
            Variables.Add("Profile", Profile);
            Variables.Add("Instance", Instance);
            Variables.Add("LayoutType", LayoutType);
            Variables.Add("MachineTime", MachineTime);
        }

        public string Description { get; set; }
        public string MachineType { get; set; }
        public short ActiveProfileCount => (short)ProfileList.Count;
        public short ActiveInstanceCount => (short)InstanceList.Count;
        public decimal? MachineCostPerHour { get; set; }
        [AutocompleteIgnore]
        public virtual void InitializeProfileList() { }

        [AutocompleteIgnore]
        public virtual void InitializeInstanceList() { }
    }
}
