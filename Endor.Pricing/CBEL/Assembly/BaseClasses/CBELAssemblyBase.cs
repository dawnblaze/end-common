﻿using Endor.CBEL.Assembly.BaseClasses;
using Endor.CBEL.Common;
using Endor.CBEL.Compute;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Endor.EF;
using Endor.Pricing.CBEL.Assembly.BaseClasses;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Endor.Tenant;
using Endor.Models;
using System.Reflection;
using Endor.Models.Autocomplete;
using Endor.Logging.Client;

namespace Endor.CBEL.Elements
{
    [Autocomplete("AssemblyBase")]
    public abstract class CBELAssemblyBase : CBELBaseComponent, ICBELAssembly
    {
        public CBELAssemblyBase(ApiContext ctx, short bid, ITenantDataCache cache, RemoteLogger logger)
        {
            // create the lists objects.
            this.Errors = new List<CBELRuntimeException>();
            this.ValidationFailures = new List<ICBELValidationFailure>();
            this.Variables = new VariableDictionary();

            this.Context = ctx;
            this.BID = bid;
            this.Cache = cache;
            this.Logger = logger;
            this._lazyCompany = new Lazy<CBELCompany>(() => GetCBELCompany());

            // Call the Initialize Data to set the Fields and Properties
            InitializeData();

            // call the InitializeElements in the concrete class to define all the element properties!
            InitializeVariables();
        }

        [AutocompleteIgnore]
        public ITenantDataCache Cache { get; private set; }
        [AutocompleteIgnore]
        public RemoteLogger Logger { get; private set; }
        [AutocompleteIgnore]
        public ApiContext Context { get; private set; }
        [AutocompleteIgnore]
        public short BID { get; private set; }

        public int CurrencyRoundingDecimals = 2;

        /// <summary>
        /// A dictionary of all of the variables on the Assembly
        /// </summary>
        [AutocompleteIgnore]
        public VariableDictionary Variables { get; private set; }

        private List<ICBELComponentVariable> _Components;
        /// <summary>
        /// A dictionary of all of the components on the Assembly
        /// </summary>
        [AutocompleteIgnore]
        public List<ICBELComponentVariable> Components
        {
            get
            {
                if (_Components == null)
                {
                    _Components = Variables.Select(t => t.Value).OfType<ICBELComponentVariable>().ToList();
                }

                return _Components;
            }
        }

        /// <summary>
        /// A dictionary of just of the material on the Assembly.
        /// This is a subset of the Components list.
        /// </summary>
        [AutocompleteIgnore]
        public IEnumerable<ICBELComponentVariable> Materials => Components.Where(x => x.ComponentClassType == OrderItemComponentType.Material);

        /// <summary>
        /// A dictionary of just of the material on the Assembly
        /// This is a subset of the Components list.
        /// </summary>
        [AutocompleteIgnore]
        public IEnumerable<ICBELComponentVariable> Labor => Components.Where(x => x.ComponentClassType == OrderItemComponentType.Labor);

        /// <summary>
        /// A dictionary of just of the material on the Assembly
        /// This is a subset of the Components list.
        /// </summary>
        [AutocompleteIgnore]
        public IEnumerable<ICBELComponentVariable> Machines => Components.Where(x => x.ComponentClassType == OrderItemComponentType.Machine);

        /// <summary>
        /// A dictionary of just of the material on the Assembly
        /// This is a subset of the Components list.
        /// </summary>
        [AutocompleteIgnore]
        public IEnumerable<ICBELComponentVariable> Subassemblies => Components.Where(x => x.ComponentClassType == OrderItemComponentType.Assembly);

        /// <summary>
        /// This method creates the Element Properties, assigns them to the private fields, and adds them to the dictionary.
        /// Thie routine is overridden in the concrete class created for each assembly.
        /// It is generated based on the contents of the Elements dictionary.
        /// </summary>
        [AutocompleteIgnore]
        public abstract void InitializeVariables();

        /// <summary>
        /// This method is used to set all of the initial fields and properties.
        /// Thie routine is overridden in the concrete class created for each assembly.
        /// It is generated based on the contents of the Setters list.
        /// </summary>
        [AutocompleteIgnore]
        public abstract void InitializeData();

        [AutocompleteIgnore]
        public List<VariableOVData> UserOVValues { get; set; }
        [AutocompleteIgnore]
        public List<VariableOVData> FormulaOVValues { get; set; }

        /// <summary>
        /// Compute is called to compute the variables.  
        /// Returns true if there are no errors or validation failures.
        /// Following the compute, the Errors and Validations must be checked to know if it was successful.
        /// </summary>
        [AutocompleteIgnore]
        public ICBELComputeResult Compute(ICBELOverriddenValues OVValues = null)
        {
            if (OVValues == null) OVValues = new CBELOverriddenValues();
            IsVended = OVValues.IsVended;
            ParentQuantity = OVValues.ParentQuantity;
            LineItemQuantity = OVValues.LineItemQuantity;
            CompanyID = OVValues.CompanyID;
            OverriddenUnitCost = OVValues.CostUnit;
            UnitCostIsOverridden = OVValues.CostUnitOV;
            return Compute(CBELAssemblyHelper.OVValuesToOVDataList(OVValues));
        }

        /// <summary>
        /// Compute is called to compute the variables.  
        /// Returns true if there are no errors or validation failures.
        /// Following the compute, the Errors and Validations must be checked to know if it was successful.
        /// </summary>
        public ICBELComputeResult Compute(List<VariableOVData> OVValues)
        {
            UserOVValues = OVValues;

            IsCalculating = true;
            IsCalculated = false;

            // Clear any previous erors and validation failures
            ValidationFailures.Clear();
            Errors.Clear();

            try
            {
                DoCompute();
            }
            catch (Exception e)
            {
                LogException(new CBELRuntimeException($"Error During Evaluation in {Name}", this, e));
            }

            IsCalculating = false;
            IsCalculated = true;

            //bool HasComputeError = !(HasValidationFailure() || HasError());

            return new CBELComputeResult(this);
        }

        [AutocompleteIgnore]
        public virtual void SetOvValues()
        {
            // Set all variables that are not OV and have a formula to need computation
            foreach (ICBELVariable variable in Variables.OrderedList())
            {
                if (variable is ICBELVariableComputed computedVariable)
                {
                    computedVariable.ResetValue();
                    computedVariable.UserOVValues = UserOVValues?.FirstOrDefault(x => x.VariableName == computedVariable.Name);
                    computedVariable.FormulaOVValues = FormulaOVValues?.FirstOrDefault(x => x.VariableName == computedVariable.Name);
                }
            }
        }

        protected virtual void ComputeVariables()
        {
            // Run through all the variables that still need to be calculated and compute them
            // Note: Because this is an iterative process, many will be computed before they come up in this list.  That's OK.
            foreach (ICBELVariable variable in Variables.OrderedList())
            {
                if (variable is ICBELVariableComputed computedVariable)
                    if (!computedVariable.IsCalculated)
                        computedVariable.Compute();
            }
        }

        /// <summary>
        /// Compute is called to compute the variables.  
        /// Returns true if there are no errors or validation failures.
        /// Following the compute, the Errors and Validations must be checked to know if it was successful.
        /// </summary>
        protected virtual void DoCompute()
        {
            SetOvValues();
            OverrideFormulas();
            ComputeVariables();
        }

        protected virtual void OverrideFormulas() { }

        /// <summary>
        /// Property that indicates if the assembly is fully computed.  This is set to false when any underlying field changes.
        /// </summary>
        [AutocompleteIgnore]
        public bool IsCalculated { get; private set; }

        /// <summary>
        /// Property that indicates if that the assembly component is in the process of being computed.
        /// </summary>
        public bool IsCalculating { get; private set; }

        /// <summary>
        /// This property is a reference to the Line Item Object this assembly is on.
        /// This is not the full OrderItem object but a limited model with only a few properties.
        /// </summary>
        public ICBELOrderItemObject LineItem { get; private set; }

        /// <summary>
        /// This property is a reference to the Company Object for this assembly.  It is NULL if no company is yet assigned.
        /// This is not the full Company object but a limited model with only a few properties.
        /// This property will be NULL if the company is not assigned to the order yet.
        /// </summary>
        public ICBELCompanyObject Company
        {
            get
            {
                return _lazyCompany.Value;
            }
        }

        private readonly Lazy<CBELCompany> _lazyCompany = null;

        private CBELCompany GetCBELCompany()
        {
            try
            {

                var company = (this.CompanyID == null) ? null : this.Context.CompanyData
                    .Include(c => c.PricingTier)
                    .Include(c => c.ParentCompany)
                    .Where(c => c.BID == this.BID && c.ID == this.CompanyID)?
                    .Select(c => new CBELCompany()
                    {
                        ID = c.ID,
                        Name = c.Name,
                        Tier = c.PricingTierID.HasValue ? c.PricingTier.Name :
                            (c.ParentCompany.PricingTierID.HasValue ? c.ParentCompany.PricingTier.Name :
                            this.Context.FlatListItem.First(x => x.BID == this.BID && x.ID == 100 && x.FlatListType == Models.FlatListType.PricingTiers).Name)
                    }).FirstOrDefault();
                if (company == null)
                {
                    return new CBELCompany()
                    {
                        ID = null,
                        Name = null,
                        Tier = this.Context.FlatListItem.First(x => x.BID == this.BID && x.ID == 100 && x.FlatListType == Models.FlatListType.PricingTiers)?.Name
                    };
                }
                else return company;
            }
            catch (InvalidOperationException ioe)
            {
                throw new CBELException("Missing default pricing tier", ioe);
            }
        }
        /// <summary>
        /// Boolean method indicating if a company is assigned to the order.  This should be checked before the Company property is accessed.
        /// </summary>
        /// <returns></returns>
        public bool HasCompany() => CompanyID != null;

        /// <summary>
        /// The CompanyID is set if the Company is assigned to the order before being computed.
        /// </summary>
        [JsonIgnore]
        public int? CompanyID { get; set; }

        /// <summary>
        /// This property contains the Line Item Quantity for top-level Assemblies, 
        /// or the Quantity of the Parent Assemblies for Child-level Assemblies; 
        /// </summary>
        public decimal? ParentQuantity { get; set; }

        public decimal? LineItemQuantity { get; set; }

        /// <summary>
        /// This property indicates if the Line Item flag indicating the order Is Outsourced (Vended) was set.
        /// </summary>
        public bool IsVended { get; set; }


        public string CompanyTier => HasCompany() ? Company.Tier : null;

        [AutocompleteIgnore]
        public string NULL => "null";

        /// <summary>
        /// The tier of the Company.
        /// If not company is set, the Default Tier will be used.
        /// </summary>
        public string TierName => HasCompany() ? Company.Tier ??
           this.Context.FlatListItem.First(x => x.BID == this.BID && x.ID == 100 && x.FlatListType == Models.FlatListType.PricingTiers).Name
            : this.Context.FlatListItem.First(x => x.BID == this.BID && x.ID == 100 && x.FlatListType == Models.FlatListType.PricingTiers).Name;


        /// <summary>
        /// Helper function to link a dictionary of element names to the underlying elements they represent
        /// </summary>
        /// <param name="dict"></param>
        [AutocompleteIgnore]
        public void LinkElementsByName(VariableLinkDictionary dict)
        {
            List<string> unLinked = dict.Where(d => d.Value.Variable == null).Select(d => d.Key).ToList();

            foreach (var s in unLinked)
            {
                string varName = dict[s].VariableName;
                if (Variables.ContainsKey(varName))
                {
                    (string, ICBELVariable) newValue = (varName, Variables[varName]);
                    dict[s] = newValue;
                }
            }
        }

        // ================================
        // Exception Handling
        // ================================

        /// <summary>
        /// A function to accumulate errors during computation.
        /// </summary>
        /// <param name="ex">Exception to be logged</param>
        public void LogException(CBELRuntimeException ex)
        {
            Errors.Add(ex);
            Logger.PricingError(BID, ex.Message, ex);
        }

        /// <summary>
        /// Method indicating if any computation exceptions exist on the assembly.
        /// </summary>
        public bool HasError() => (Errors.Count > 0);

        /// <summary>
        /// Property that gives a concatenated message of all of the exceptions for the assembly.
        /// </summary>
        public string ErrorText()
        {
            string Results = "";
            Errors.ForEach(x => Results += $"{x.Message};{Constants.CR}");
            return Results.ToString();
        }

        /// <summary>
        /// A list of all of the exceptions produced during computation.
        /// </summary>
        [AutocompleteIgnore]
        public List<CBELRuntimeException> Errors { get; private set; }

        // ================================
        // Validation Handling
        // ================================

        /// <summary>
        /// A function to accumulate errors during computation.
        /// </summary>
        /// <param name="vf">Validation Failure to be logged</param>
        public void LogValidationFailure(ICBELValidationFailure vf) => ValidationFailures.Add(vf);

        /// <summary>
        /// Method indicating if any computation exceptions exist on the assembly.
        /// </summary>
        public bool HasValidationFailure() => ValidationFailures.Count > 0;

        /// <summary>
        /// Property that gives a concatenated message of all of the exceptions for the assembly.
        /// </summary>
        public string ValidationFailureText()
        {
            string Results = "";
            ValidationFailures.ForEach(x => Results += $"{x.Message};{Constants.CR}");
            return Results.ToString();
        }

        /// <summary>
        /// The ClassTypeID of the Assembly Pricer Object
        /// </summary>
        /// <returns></returns>
        [AutocompleteIgnore]
        public override int? ClassTypeID => ClassType.Assembly.ID();

        /// <summary>
        /// A list of all of the exceptions produced during computation.
        /// </summary>
        [AutocompleteIgnore]
        public List<ICBELValidationFailure> ValidationFailures { get; private set; }

        public virtual NumberEditElement TotalQuantity => null;
        [AutocompleteIgnore]
        public override decimal? QuantityValue
        {
            get
            {
                if (TotalQuantity != null)
                    return TotalQuantity.Value;

                return null;
            }
            set
            {
                if (TotalQuantity != null)
                    TotalQuantity.Value = value;
            }
        }

        [AutocompleteIgnore]
        public override bool? QuantityOV
        {
            get
            {
                if (TotalQuantity != null)
                    return TotalQuantity.IsOV;

                return null;
            }
        }

        public virtual NumberEditElement Price => null;
        public decimal? TotalPrice
        {
            get
            {
                if (Price != null)
                    return Price.Value;

                return null;
            }
            set
            {
                if (Price != null)
                    Price.Value = value;
            }
        }

        /// <summary>
        /// The unit price of the assembly.
        /// Computed as the Price / Quantity
        /// </summary>
        [JsonIgnore]
        public override decimal? ComputedUnitPrice
        {
            get
            {
                decimal? price = TotalPrice;
                decimal? quantity = QuantityValue;

                return (quantity == 0.0m) ? null : price / quantity;
            }
        }

        /// <summary>
        /// The total cost of the Assembly
        /// Computed as the sum of all of the costs of the Assembly
        /// </summary>
        public decimal? TotalCost
        {
            get
            {
                return Components.Where(x => x.RollupLinkedPriceAndCost).Sum(x => x.Cost).Round(CurrencyRoundingDecimals);
            }
        }

        /// <summary>
        /// The unit cost of the assembly.
        /// Computed as the Cost / ComponentQuantity
        /// </summary>
        [JsonIgnore]
        public decimal? ComputedUnitCost
        {
            get
            {
                if (UnitCostIsOverridden.GetValueOrDefault(false))
                    return OverriddenUnitCost;

                return (QuantityValue.GetValueOrDefault(0.00m) == 0.0m) ? null : TotalCost / QuantityValue;
            }
        }

        /// <summary>
        /// The unit cost of the assembly.
        /// This is the same as ComputedUnitCost, but people friendly name for CBEL formulas
        /// </summary>
        public decimal? UnitCost => ComputedUnitCost;

        public ConsumptionStringVariable ParentVariable { get; set; }
        /// <summary>
        /// The unit price of the assembly.
        /// This is the same as ComputedUnitPrice, but people friendly name for CBEL formulas
        /// </summary>
        public decimal? UnitPrice => ComputedUnitPrice;

        public decimal? FixedPrice
        {
            get
            {
                return DefinedFixedPrice;
            }
            set
            {
                DefinedFixedPrice = value;
            }
        }
        public decimal? FixedMargin
        {
            get
            {
                return DefinedFixedMargin;
            }
            set
            {
                DefinedFixedMargin = value;
            }
        }
        public decimal? FixedMarkup
        {
            get
            {
                return DefinedFixedMarkup;
            }
            set
            {
                DefinedFixedMarkup = value;
            }
        }
        public decimal? MaterialCost
        {
            get
            {
                decimal? result;
                IEnumerable<ICBELAssembly> assemblyComponents = Components.Where(x => x.RollupLinkedPriceAndCost && x.Component is ICBELAssembly).Select(x => (ICBELAssembly)x.Component);
                result = assemblyComponents.Sum(x => x.MaterialCost);
                result += Components.Where(x => x.ComponentClassType == OrderItemComponentType.Material).Sum(x => x.Cost).Round(CurrencyRoundingDecimals);

                return result;
            }
        }

        public decimal? LaborCost
        {
            get
            {
                decimal? result;

                IEnumerable<ICBELAssembly> assemblyComponents = Components.Where(x => x.RollupLinkedPriceAndCost && x.Component is ICBELAssembly).Select(x => (ICBELAssembly)x.Component);
                result = assemblyComponents.Sum(x => x.LaborCost);

                result += Components.Where(x => x.ComponentClassType == OrderItemComponentType.Labor).Sum(x => x.Cost).Round(CurrencyRoundingDecimals);

                return result;
            }
        }

        public decimal? MachineCost
        {
            get
            {
                decimal? result;

                IEnumerable<ICBELAssembly> assemblyComponents = Components.Where(x => x.RollupLinkedPriceAndCost && x.Component is ICBELAssembly).Select(x => (ICBELAssembly)x.Component);
                result = assemblyComponents.Sum(x => x.MachineCost);

                result += Components.Where(x => x.ComponentClassType == OrderItemComponentType.Machine).Sum(x => x.Cost).Round(CurrencyRoundingDecimals);

                return result;
            }
        }

        public override decimal? TierPriceTable
        {
            get
            {
                ICBELVariable tableVariable = Variables.GetValueOrDefault("PriceTable");

                if (tableVariable is ICBELVariableComputed<decimal?> numberVariable)
                    return numberVariable.Value;

                return null;
            }
        }

        public override decimal? TierDiscountTable
        {
            get
            {
                ICBELVariable tableVariable = Variables.GetValueOrDefault("DiscountTable");

                if (tableVariable is ICBELVariableComputed<decimal?> numberVariable)
                    return numberVariable.Value;

                return null;
            }
        }
        public override decimal? TierMarginTable
        {
            get
            {
                ICBELVariable tableVariable = Variables.GetValueOrDefault("MarginTable");

                if (tableVariable is ICBELVariableComputed<decimal?> numberVariable)
                    return numberVariable.Value;

                return null;
            }
        }
        public override decimal? TierMarkupTable
        {
            get
            {
                ICBELVariable tableVariable = Variables.GetValueOrDefault("MarkupTable");

                if (tableVariable is ICBELVariableComputed<decimal?> numberVariable)
                    return numberVariable.Value;

                return null;
            }
        }
    }

}