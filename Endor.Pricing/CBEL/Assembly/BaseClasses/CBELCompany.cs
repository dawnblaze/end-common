﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Pricing.CBEL.Assembly.BaseClasses
{
    public class CBELCompany : ICBELCompanyObject
    {
        /// <summary>
        /// The pricing tier of the company
        /// </summary>
        public string Tier { get; set; }
        /// <summary>
        /// The ID of the company 
        /// This is NULL if it is not known
        /// </summary>
        public int? ID { get; set; }
        public int? ClassTypeID {  get { return (int)ClassType.Company; } }
        /// <summary>
        /// The parent company (Null if no parent exists
        /// </summary>
        public ICBELObject ParentObject { get; set; }
        /// <summary>
        /// The name of the company (Empty string if not known)
        /// </summary>
        public string Name { get; set; }

        public ICBELCompanyCustomFields CustomFields { get; set; }
    }
}
