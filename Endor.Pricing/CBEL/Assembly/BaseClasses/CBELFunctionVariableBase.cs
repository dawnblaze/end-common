﻿using Endor.CBEL.Exceptions;
using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.CBEL.Validation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Endor.CBEL.Grammar;

namespace Endor.CBEL.Elements
{

    /// <summary>
    /// This is the base class for custom function variables in Endor.
    /// These variables are used to add complex functionality to the system.
    /// </summary>
    /// <param name="parent"></param>
    public abstract class CBELFunctionVariableBase<T> : CBELComputedVariableBase<T>
    {
        /// <summary>
        /// We require the Parent Assembly be set during construction
        /// </summary>
        /// <param name="parent">The Assembly parent</param>
        public CBELFunctionVariableBase(ICBELAssembly parent) : base(parent)
        {
            _DefaultValueFunction = FormulaFunction;
        }

        /// <summary>
        /// The formula function/computation is done here.
        /// </summary>
        /// <returns></returns>
        public abstract T FormulaFunction();

        /// <summary>
        /// The fomrula
        /// </summary>
        [JsonIgnore]
        public override string FormulaText => $"CBEL Function";

        // Functions don't support default values being constant, so don't allow this
        [JsonIgnore]
        public override bool DefaultValueIsConstant => false;

    }
}



