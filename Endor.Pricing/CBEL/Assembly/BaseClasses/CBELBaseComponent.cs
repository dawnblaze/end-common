﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Assembly.BaseClasses
{
    [Autocomplete("ComponentBase")]
    public abstract class CBELBaseComponent : ICBELComponent
    {
        public virtual decimal? QuantityValue { get; set; }

        public virtual bool? QuantityOV => null;

        [AutocompleteIgnore]
        public virtual decimal? DefinedUnitPrice { get; set; }
        [AutocompleteIgnore]
        public virtual decimal? DefinedFixedPrice { get; set; }

        public virtual decimal? ComputedUnitPrice => QuantityValue.GetValueOrDefault(1) != 0 ? DefinedFixedPrice / QuantityValue.GetValueOrDefault(1) : null;

        public bool? UnitCostIsOverridden { get; set; }
        public decimal? OverriddenUnitCost { get; set; }
        [AutocompleteIgnore]
        public decimal? DefinedUnitCost { get; set; }
        [AutocompleteIgnore]
        public decimal? DefinedFixedCost { get; set; }

        public int? ID { get; set; }

        [AutocompleteIgnore]
        public virtual int? ClassTypeID { get; set; }

        [AutocompleteIgnore]
        public ICBELObject ParentObject { get; set; }

        public string Name { get; set; }

        /// <summary>
        /// GL ID for Income, required in order to fill <see cref="Endor.Pricing.ComponentPriceResult.IncomeAccountID"/>
        /// </summary>
        [AutocompleteIgnore]
        public int? IncomeAccountID { get; set; }
        /// <summary>
        /// GL ID for Expense, required in order to fill <see cref="Endor.Pricing.ComponentPriceResult.ExpenseAccountID"/>
        /// </summary>
        [AutocompleteIgnore]
        public int? ExpenseAccountID { get; set; }
        /// <summary>
        /// Allocation Type for Income, required in order to fill <see cref="Endor.Pricing.ComponentPriceResult.IncomeAllocationType"/>
        /// </summary>
        [AutocompleteIgnore]
        public AssemblyIncomeAllocationType? IncomeAllocationType { get; set; }

        public virtual decimal? DefinedFixedMargin { get; set; }
        public virtual decimal? DefinedFixedMarkup { get; set; }
        public virtual decimal? TierPriceTable { get { return 2.00m; } } // always return 2.00 ($2) for now
        public virtual decimal? TierDiscountTable { get { return 0.10m; } } // always return 0.10 (10%) for now
        public virtual decimal? TierMarginTable { get { return 0.50m; } } // always return 0.50 (50%) for now
        public virtual decimal? TierMarkupTable { get { return 0.50m; } } // always return 0.50 (50%) for now
    }
}
