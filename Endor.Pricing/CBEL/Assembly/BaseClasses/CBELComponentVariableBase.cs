﻿using Endor.CBEL.Exceptions;
using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using System.Collections.Generic;
using System;
using Endor.Models;
using System.Linq;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{
    [Autocomplete("VariableBase")]
    public abstract class CBELComponentVariableBase<T> : CBELComputedVariableBase<T>, ICBELComponentVariable
        where T : class
    {
        public CBELComponentVariableBase(ICBELAssembly parent) : base(parent)
        {
            _ConsumptionQuantity = new NumberEditElement(parent)
            {
                Name = $"{this.Name}_ConsumptionQuantity"
            };
        }

        public NumberVariable ConsumptionQuantity => _ConsumptionQuantity;

        private readonly NumberEditElement _ConsumptionQuantity;

        public abstract ICBELComponent Component { get; set; }

        [AutocompleteIgnore]
        public virtual OrderItemComponentType ComponentClassType => (Component != null) ? (OrderItemComponentType)Component.ClassTypeID : OrderItemComponentType.Assembly;

        [AutocompleteIgnore]
        public virtual void LoadLinkedComponent()
        {

        }

        public virtual decimal? Cost
        {
            get
            {
                if (UnitCostIsOverridden.GetValueOrDefault(false))
                    return OverriddenUnitCost * RoundedCalculatedConsumption();

                return FixedCost.GetValueOrDefault() + (RoundedCalculatedConsumption() * DefinedUnitCost);
            }
        }

        /// <summary>
        /// Unit cost of the component.
        /// This is always Cost / QuantityConsumed.
        /// </summary>
        public virtual decimal? ComputedUnitCost
        {
            get
            {
                if (UnitCostIsOverridden.GetValueOrDefault(false))
                    return OverriddenUnitCost;

                decimal calcConsumption = RoundedCalculatedConsumption().GetValueOrDefault(0m);

                if (calcConsumption == 0m)
                    return null;

                return Math.Round((FixedCost.GetValueOrDefault(0m) + (calcConsumption * DefinedUnitCost.GetValueOrDefault(0m))) / calcConsumption, 10);
            }                    
        }

        /// <summary>
        /// Unit cost of the component is overridden.
        /// </summary>
        public virtual bool? UnitCostIsOverridden
        {
            get
            {
                return Component?.UnitCostIsOverridden;
            }
            set
            {
                throw new Exception("CostUnitOV can not be overridden");
            }
        }

        /// <summary>
        /// Defined Fixed Cost
        /// </summary>
        public decimal? FixedCost => (Component is ICBELLaborComponent laborComponent) ?
            laborComponent.DefinedFixedCost : 0m;

        /// <summary>
        /// Defined Unit Cost
        /// </summary>
        public decimal? DefinedUnitCost
        {
            get
            {
                return Component?.DefinedUnitCost;
            }
            set
            {
                throw new Exception("DefinedUnitCost can not be overridden");
            }
        }
        /// <summary>
        /// Overrridden Unit Cost
        /// </summary>
        public decimal? OverriddenUnitCost
        {
            get
            {
                return Component?.OverriddenUnitCost;
            }
            set
            {
                throw new Exception("OverriddenUnitCost can not be overridden");
            }
        }

        /// <summary>
        /// Unit price of the component.
        /// </summary>
        public decimal? ComputedUnitPrice
        {
            get
            {
                decimal calcConsumption = RoundedCalculatedConsumption().GetValueOrDefault(0m);

                if (calcConsumption == 0m)
                    return null;

                return Math.Round((DefinedFixedPrice.GetValueOrDefault(0m) + (calcConsumption * DefinedUnitPrice.GetValueOrDefault(0m))) / calcConsumption, 10);
            }
        }

        /// <summary>
        /// Defined Unit Cost
        /// </summary>
        public decimal? DefinedUnitPrice
        {
            get
            {
                return Component?.DefinedUnitPrice;
            }
            set
            {
                throw new Exception("DefinedUnitPrice can not be overridden");
            }
        }

        /// <summary>
        /// Fixed Base Price
        /// </summary>
        public decimal? DefinedFixedPrice => (Component is ICBELLaborComponent laborComponent) ?
            laborComponent.DefinedFixedPrice : 0m;

        /// <summary>
        /// Component Price
        /// </summary>
        public virtual decimal? Price 
        {
            get
            {
                return DefinedFixedPrice.GetValueOrDefault(0m) + (RoundedCalculatedConsumption() * DefinedUnitPrice.GetValueOrDefault(0m));
            }
        }

        protected virtual decimal? RoundedCalculatedConsumption()
        {
            return ConsumptionQuantity.Value;
        }

        /// <summary>
        /// A list of all linked formulas
        /// </summary>
        /// <returns>list of all linked variable formulas</returns>
        [AutocompleteIgnore]
        public List<VariableFormula> Formulas { get; set; }

        /// <summary>
        /// roll up linked price and cost flag
        /// </summary>
        public virtual bool RollupLinkedPriceAndCost { get { return true; } set { } }

        protected override void DoCompute()
        {
            IsIncludedOV = null;

            if (UserOVValues != null)
            {
                if (UserOVValues.IsIncludedOV.GetValueOrDefault(false))
                {
                    IsIncluded = UserOVValues.IsIncluded.GetValueOrDefault(true);
                    IsIncludedOV = true;
                }
            }

            if (!IsIncludedOV.GetValueOrDefault(false))
                IsIncluded = CheckIsIncluded();

            if (IsIncluded.GetValueOrDefault(true))
            {
                _ConsumptionQuantity.IsOV = false;
                
                base.DoCompute();

                VariableOVData ovChildValue = UserOVValues;

                if (ovChildValue == null || !ovChildValue.QuantityOV.GetValueOrDefault(false))
                    if (FormulaOVValues != null)
                        ovChildValue = FormulaOVValues;

                if (ovChildValue != null && ovChildValue.QuantityOV.GetValueOrDefault(false) && ovChildValue.Quantity.HasValue)
                    _ConsumptionQuantity.Value = ovChildValue.Quantity.Value;

                LoadLinkedComponent();
                ICBELComponent component = Component;

                _ConsumptionQuantity.Compute();

                if (component != null)
                {
                    if (component is ICBELAssembly childAssembly)
                    {
                        childAssembly.LineItemQuantity = Parent?.LineItemQuantity ?? ovChildValue?.LineItemQuantity;
                        childAssembly.IsVended = Parent?.IsVended ?? ovChildValue?.IsVended ?? false;
                        childAssembly.CompanyID = Parent?.CompanyID ?? ovChildValue?.CompanyID;

                        childAssembly.ParentQuantity = ovChildValue?.Quantity;
                        childAssembly.OverriddenUnitCost = ovChildValue?.CostUnit;
                        childAssembly.UnitCostIsOverridden = ovChildValue?.CostUnitOV;

                        VariableOVData ovChild = ovChildValue?.ChildOVValues?.FirstOrDefault(ov => ov.VariableName == "MachineTime");

                        if (ovChild != null && ovChild.CostUnitOV.GetValueOrDefault(false))
                        { 
                            childAssembly.OverriddenUnitCost = ovChild?.CostUnit;
                            childAssembly.UnitCostIsOverridden = ovChild?.CostUnitOV;
                            if(ovChild != null)
                            {
                                if((bool)ovChild?.CostUnitOV.GetValueOrDefault(false))
                                {
                                    childAssembly.OverriddenUnitCost = ovChild?.CostUnit;
                                    childAssembly.UnitCostIsOverridden = ovChild?.CostUnitOV;
                                }
                                
                            }
                            
                        }

                        if (Formulas != null && Formulas.Count > 0)
                        {
                            childAssembly.FormulaOVValues = new List<VariableOVData>();
                            foreach (VariableFormula formula in Formulas)
                            {
                                childAssembly.FormulaOVValues.Add(
                                    new VariableOVData()
                                    {
                                        VariableName = formula.ChildVariableName,
                                        IsOV = true,
                                        Value = formula.ValueFunction == null ? null : formula.ValueFunction()?.ToString(),
                                    }
                                    );
                            }
                        }

                        ICBELComputeResult childResult = childAssembly.Compute(UserOVValues?.ChildOVValues);
                        if (childResult.HasValidationFailures)
                            foreach (ICBELValidationFailure err in childResult.ValidationFailures)
                                Parent.ValidationFailures.Add(err);

                        if (childResult.HasErrors)
                            foreach (CBELRuntimeException err in childResult.Errors)
                                Parent.Errors.Add(err);
                    }

                    else if (component is ICBELVariableComputed computedVariable)
                        computedVariable.Compute();
                }
            }
            else
                IsCalculated = true;
        }
    }
}
