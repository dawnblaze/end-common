﻿using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Endor.Models;
using Endor.Models.Autocomplete;

namespace Endor.CBEL.Elements
{

    /// <summary>
    /// This is the base class for most Assembly Properties (Variables).
    /// Each element is added as a property to the Assembly component.
    /// </summary>
    /// <param name="parent"></param>
    public abstract class CBELDataTable<T, RowDataType, ColumnDataType> : CBELComputedVariableBase<T>, ICBELVariableTable<T, RowDataType, ColumnDataType>
    {
        /// <summary>
        /// We require the Parent Assembly be set during construction
        /// </summary>
        /// <param name="parent">The Assembly parent</param>
        public CBELDataTable(ICBELAssembly parent) : base(parent) {
            RowHeadings = new List<RowDataType>();
            ColumnHeadings = new List<ColumnDataType>();
            DefaultValueFunction = TableValueFunction;
        }

        /// <summary>
        /// For a table, the formula is fixed.
        /// </summary>
        [JsonIgnore]
        public override string FormulaText { get => $"=TableLookup( {Name}, {RowVariableName}.Value, {ColumnVariableName}.Value, {RowMatchOption}, {ColumnMatchOption} )"; }

        // Tables don't support default values being constant, so don't allow this
        [JsonIgnore]
        public override bool DefaultValueIsConstant => false;

        /// <summary>
        /// A list of the Row Headings for this Table
        /// </summary>
        [AutocompleteIgnore]
        public List<RowDataType> RowHeadings { get; }

        /// <summary>
        /// A list of the Column Headings for this Table
        /// </summary>
        [AutocompleteIgnore]
        public List<ColumnDataType> ColumnHeadings { get; }


        /// <summary>
        /// The underlying data store for the cells.  This is set in the O
        /// </summary>
        protected T[,] Cells { get; set;  }

        /// <summary>
        /// This stores the function that is used to evaluate the default value.
        /// Note: The scope of the function that is passed in must be on the Assembly - not the element.
        /// </summary>
        [AutocompleteIgnore]
        public Func<T> TableValueFunction
        {
            get {
                return (Func<T>) (() => TableLookup());
            }
        }


        /// <summary>
        /// Lookup the table's values given the default definition/variables
        /// </summary>
        /// <returns></returns>
        public virtual T TableLookup()
        {
            return TableLookup( ( (ICBELVariableComputed<RowDataType>)RowVariable ).Value,  
                                ( (ICBELVariableComputed<ColumnDataType>)ColumnVariable ).Value, 
                                RowMatchOption, 
                                ColumnMatchOption );
        }

        private bool _TableInitialized = false;
        /// <summary>
        /// Initializes the tables content
        /// </summary>
        [AutocompleteIgnore]
        public void InitializeTable()
        {
            if (!_TableInitialized)
            {
                FillTable?.Invoke(this);
                _TableInitialized = true;
            }
        }

        [AutocompleteIgnore]
        public Action<CBELDataTable<T, RowDataType, ColumnDataType>> FillTable { get; set; }

        /// <summary>
        /// Lookup a table value specifying the Row Value, Column Value, and Match conditions
        /// </summary>
        /// <param name="RowValue">The value of the Row Variable</param>
        /// <param name="ColValue">The value of the Column Variable</param>
        /// <param name="rowmatch">Enum indicating how non-exact matches are handled for the Row Variable</param>
        /// <param name="colmatch">Enum indicating how non-exact matches are handled for the Column Variable</param>
        /// <returns></returns>
        public T TableLookup(RowDataType RowValue, ColumnDataType ColValue, AssemblyTableMatchType rowmatch = AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType colmatch = AssemblyTableMatchType.ExactMatch)
        {
            InitializeTable();

            if ((typeof(RowDataType) != typeof(decimal?)) && (rowmatch != AssemblyTableMatchType.ExactMatch))
                Parent.LogException(new TableLookupException($"Row has incorrect match type {rowmatch.ToString()} for Row Data Type of {typeof(RowDataType)}.", this));

            if ((typeof(ColumnDataType) != typeof(decimal?)) && (colmatch != AssemblyTableMatchType.ExactMatch))
                Parent.LogException(new TableLookupException($"Column has incorrect match type {colmatch.ToString()} for Column Data Type of {typeof(ColumnDataType)}.", this));

            //Check if entries exist
            int ColNo = ColumnHeadings.IndexOf(ColValue);
            int RowNo = RowHeadings.IndexOf(RowValue);

            if ((RowNo == -1) && ((typeof(RowDataType) != typeof(decimal?)) || rowmatch == AssemblyTableMatchType.ExactMatch))
            {
                if(rowmatch != AssemblyTableMatchType.UseHigherValue && rowmatch != AssemblyTableMatchType.UseLowerValue)
                {
                    Parent.LogException(new TableLookupException("Exact match must be found row variable: " + RowVariableName + ". RowValue is: " + RowValue.ToString()));
                    return default(T);
                }                
            }

            if ((ColNo == -1) && ((typeof(ColumnDataType) != typeof(decimal?)) || colmatch == AssemblyTableMatchType.ExactMatch))
            {
                Parent.LogException(new TableLookupException("Exact match must be found row variable: " + ColumnVariableName + ". ColValue is: " + ColValue.ToString()));
                return default(T);
            }

            //decimal error checking
            bool rowDecimalError = (typeof(RowDataType) == typeof(decimal?)) ? performDecimalErrorChecking(
                "Row", 
                rowmatch, 
                GetValue<decimal?>(RowValue).GetValueOrDefault(),
                RowHeadings.Cast<decimal?>().ToList()) 
                : false;
            bool colDecimalError = (typeof(ColumnDataType) == typeof(decimal?)) ? performDecimalErrorChecking(
                "Column",
                rowmatch,
                GetValue<decimal?>(ColValue).GetValueOrDefault(),
                ColumnHeadings.Cast<decimal?>().ToList())
                : false;

            if (rowDecimalError || colDecimalError)
                return default(T);

            //check that we aren't using interpolation if all the types aren't decimal
            if (
                 (
                   (rowmatch == AssemblyTableMatchType.Interpolate)
                   ||
                   (colmatch == AssemblyTableMatchType.Interpolate)
                 )
                 &&
                 !(
                   (typeof(ColumnDataType) == typeof(decimal?))
                   &&
                   (typeof(RowDataType) == typeof(decimal?))
                   && (typeof(T) == typeof(decimal?))
                 )
               )
            {
                Parent.LogException(new TableLookupException($"When using match type of Interpolation, all data types on the table must be a number.", this));
                return default(T);
            }

            if (RowNo == -1 && rowmatch != AssemblyTableMatchType.Interpolate)
            {
                decimal rowValue = 0;
                try
                {
                    rowValue = GetValue<decimal?>(RowValue).GetValueOrDefault();
                }
                catch (Exception ex)
                {
                    Parent.Logger.PricingError(Parent.BID, "Failed to Get RowValue of Table.", ex);
                }

                //need to find the appropriate values
                int RowMatchLT = 0;
                int RowMatchGT = 0;

                try
                {
                    RowMatchLT = RowHeadings.ConvertAll(v => Convert.ToDecimal(v)).Where(var => var < rowValue).Count() - 1;
                }
                catch (Exception ex)
                {
                    Parent.Logger.PricingError(Parent.BID, "Failed to Get RowMatchLT of Table.", ex);
                }

                RowMatchGT = RowMatchLT + 1;

                RowNo = (rowmatch == AssemblyTableMatchType.ExactMatch) ? throw new Exception("This should never be hit because exception was hit above")
                    : (rowmatch == AssemblyTableMatchType.UseHigherValue) ? RowMatchGT : RowMatchLT;

            }

            if (ColNo == -1 && colmatch != AssemblyTableMatchType.Interpolate)
            {
                decimal colValue = GetValue<decimal?>(ColValue).GetValueOrDefault();
                //need to find the appropriate values
                int ColMatchLT = ColumnHeadings.Cast<decimal?>().Where(var => var.Value < colValue).Count() - 1;
                int ColMatchGT = ColMatchLT + 1;
                ColNo = (colmatch == AssemblyTableMatchType.ExactMatch) ? throw new Exception("This should never be hit because exception was hit above")
                    : (colmatch == AssemblyTableMatchType.UseHigherValue) ? ColMatchGT : ColMatchLT;
            }

            if (RowNo == -1 && (rowmatch == AssemblyTableMatchType.Interpolate)
                && ColNo == -1 && (colmatch == AssemblyTableMatchType.Interpolate))
                {
                var rowHeadingDecimal = RowHeadings.Cast<decimal?>().ToList();
                decimal rowValue = GetValue<decimal?>(RowValue).GetValueOrDefault();
                int RowMatchLT = rowHeadingDecimal.Where(var => var.Value < rowValue).Count() - 1;
                decimal RowMatchLTVal = rowHeadingDecimal[RowMatchLT].GetValueOrDefault();
                int RowMatchGT = RowMatchLT + 1;
                decimal RowMatchGTVal = rowHeadingDecimal[RowMatchGT].GetValueOrDefault();

                var colHeadingDecimal = ColumnHeadings.Cast<decimal?>().ToList();
                decimal colValue = GetValue<decimal?>(ColValue).GetValueOrDefault();
                int ColMatchLT = colHeadingDecimal.Where(var => var.Value < colValue).Count() - 1;
                decimal ColMatchLTVal = colHeadingDecimal[ColMatchLT].GetValueOrDefault();
                int ColMatchGT = ColMatchLT + 1;
                decimal ColMatchGTVal = colHeadingDecimal[ColMatchGT].GetValueOrDefault();

                decimal? RowDelta = GetValue<decimal?>(Cells[RowMatchGT, ColMatchLT]) - GetValue<decimal?>(Cells[RowMatchLT, ColMatchLT]);
                decimal RowFactorOverLT = (rowValue - RowMatchLTVal) / (RowMatchGTVal - RowMatchLTVal);
                decimal? ColDelta = GetValue<decimal?>(Cells[RowMatchLT, ColMatchGT]) - GetValue<decimal?>(Cells[RowMatchLT, ColMatchLT]);
                decimal ColFactorOverLT = (colValue - ColMatchLTVal) / (ColMatchGTVal - ColMatchLTVal);

                if (!RowDelta.HasValue || !ColDelta.HasValue)
                    return GetValue<T>(null);

                decimal? result = GetValue<decimal?>(Cells[RowMatchLT, ColMatchLT])
                                  + decimal.Round(RowDelta.Value * RowFactorOverLT, 10)
                                  + decimal.Round(ColDelta.Value * ColFactorOverLT, 10);

                return GetValue<T>(result);
            }

            if (ColNo != -1 && RowNo == -1 && (rowmatch == AssemblyTableMatchType.Interpolate))
            {
                var rowHeadingDecimal = RowHeadings.Cast<decimal?>().ToList();
                decimal rowValue = GetValue<decimal?>(RowValue).GetValueOrDefault();
                int RowMatchLT = rowHeadingDecimal.Where(var => var.Value < rowValue).Count() - 1;
                decimal RowMatchLTVal = rowHeadingDecimal[RowMatchLT].GetValueOrDefault();
                int RowMatchGT = RowMatchLT + 1;
                decimal RowMatchGTVal = rowHeadingDecimal[RowMatchGT].GetValueOrDefault();

                decimal? RowDelta = GetValue<decimal?>(Cells[RowMatchGT, ColNo]) - GetValue<decimal?>(Cells[RowMatchLT, ColNo]);
                decimal RowFactorOverLT = (rowValue - RowMatchLTVal) / (RowMatchGTVal - RowMatchLTVal);

                if (!RowDelta.HasValue)
                    return GetValue<T>(null);

                decimal? result = GetValue<decimal?>(Cells[RowMatchLT, ColNo])
                                  + decimal.Round(RowDelta.Value * RowFactorOverLT, 10);

                return GetValue<T>(result);
            }

            if (RowNo != -1 && ColNo == -1 && (colmatch == AssemblyTableMatchType.Interpolate))
            {
                var colHeadingDecimal = ColumnHeadings.Cast<decimal?>().ToList();
                decimal colValue = GetValue<decimal?>(ColValue).GetValueOrDefault();
                int ColMatchLT = colHeadingDecimal.Where(var => var.Value < colValue).Count() - 1;
                decimal ColMatchLTVal = colHeadingDecimal[ColMatchLT].GetValueOrDefault();
                int ColMatchGT = ColMatchLT + 1;
                decimal ColMatchGTVal = colHeadingDecimal[ColMatchGT].GetValueOrDefault();

                decimal? ColDelta = GetValue<decimal?>(Cells[RowNo, ColMatchGT]) - GetValue<decimal?>(Cells[RowNo, ColMatchLT]);
                decimal ColFactorOverLT = (colValue - ColMatchLTVal) / (ColMatchGTVal - ColMatchLTVal);

                if (!ColDelta.HasValue)
                    return GetValue<T>(null);

                decimal? result = GetValue<decimal?>(Cells[RowNo, ColMatchLT])
                                  + decimal.Round(ColDelta.Value * ColFactorOverLT, 10);

                return GetValue<T>(result);
            }

            return Cells[RowNo, ColNo];
        }

        private bool performDecimalErrorChecking(string headingType, AssemblyTableMatchType matchType, decimal value, List<decimal?> headings) 
        {
            var maxValue = headings.Max();
            var minValue = headings.Min();
            if ((matchType == AssemblyTableMatchType.UseLowerValue)&&(value<minValue))
            {
                Parent.LogException(new TableLookupException($"{headingType} is of match type {matchType.ToString()} and value of {value.ToString()} is outside the bounds for the heading values.", this));
                return true;
            }
            if ((matchType == AssemblyTableMatchType.UseHigherValue) && (value > maxValue))
            {
                Parent.LogException(new TableLookupException($"{headingType} is of match type {matchType.ToString()} and value of {value.ToString()} is outside the bounds for the heading values.", this));
                return true;
            }
            if ((matchType == AssemblyTableMatchType.Interpolate) && ((value > maxValue) || (value < minValue)))
            {
                Parent.LogException(new TableLookupException($"{headingType} is of match type {matchType.ToString()} and value of {value.ToString()} is outside the bounds for the heading values.", this));
                return true;
            }
            return false;
        }

        private static Tp GetValue<Tp>(Object value )
        {
            Type t = typeof(Tp);
            t = Nullable.GetUnderlyingType(t) ?? t;
            var val = (value == null) ? default(Tp) : (Tp)Convert.ChangeType(value, t);
            return val;
        }

        /// <summary>
        /// The name of the variable that is used for the Row
        /// </summary>
        private string _RowVariableName;
        public string RowVariableName
        {
            get => _RowVariableName;
            set
            {
                if (_RowVariableName != value)
                {
                    _RowVariableName = value;
                    _RowVariable = null;
                }
            }
        }

        /// <summary>
        /// This property retrieves the RowVariable based on the name.
        /// </summary>
        private ICBELVariableComputed<RowDataType> _RowVariable;
        public ICBELVariableComputed<RowDataType> RowVariable 
        {
            get
            {
                if (_RowVariable == null && !string.IsNullOrEmpty(RowVariableName))
                {
                    _RowVariable = (ICBELVariableComputed<RowDataType>)Parent.Variables[RowVariableName];
                }
                return _RowVariable;
            }

            private set { _RowVariable = value; }
        }


        /// <summary>
        /// Enum value specifying the type of rounding to use when the Row Variable value does 
        /// not exactly match any value in the table.
        ///  0 = Exact Match Required
        ///  1 = Use Next Lower Value
        ///  2 = Use Next Higher Value
        ///  3 = Interpolate (only valid for number types)
        /// </summary>
        [AutocompleteIgnore]
        public AssemblyTableMatchType RowMatchOption { get; set; }

        /// <summary>
        /// The name of the variable that is used for the Column
        /// </summary>
        private string _ColumnVariableName;
        public string ColumnVariableName
        {
            get => _ColumnVariableName;
            set
            {
                if(_ColumnVariableName != value)
                {
                    _ColumnVariableName = value;
                    _ColumnVariable = null;
                }
            }
        }

        /// <summary>
        /// This property retrieves the ColumnVariable based on the name.
        /// </summary>
        private ICBELVariableComputed<ColumnDataType> _ColumnVariable;
        public ICBELVariableComputed<ColumnDataType> ColumnVariable
        {
            get
            {
                if(_ColumnVariable == null && !string.IsNullOrEmpty(ColumnVariableName))
                {
                    _ColumnVariable = (ICBELVariableComputed<ColumnDataType>) Parent.Variables[ColumnVariableName];
                }
                return _ColumnVariable;
            }

            private set { _ColumnVariable = value; }
        }

        /// <summary>
        /// Enum value specifying the type of rounding to use when the Column Variable value does 
        /// not exactly match any value in the table.
        ///  0 = Exact Match Required
        ///  1 = Use Next Lower Value
        ///  2 = Use Next Higher Value
        ///  3 = Interpolate (only valid for number types)
        /// </summary>
        [AutocompleteIgnore]
        public AssemblyTableMatchType ColumnMatchOption { get; set; }

        /// <summary>
        /// Add cell data to table
        /// </summary>
        public void AddCells(T[,] newCells)
        {
            Cells = newCells;
        }
    }

}



