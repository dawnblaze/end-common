﻿using Endor.CBEL.Exceptions;
using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.CBEL.Validation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Endor.Models;
using System.ComponentModel;
using Endor.Models.Autocomplete;
using Endor.Units;

namespace Endor.CBEL.Elements
{

    /// <summary>
    /// This is the base class for most Assembly Properties (Variables).
    /// Each element is added as a property to the Assembly component.
    /// </summary>
    /// <param name="parent"></param>
    /// 
    [Autocomplete("ComputedVariableBase")]
    public abstract class CBELComputedVariableBase<T> : ICBELVariableComputed<T>
    {
        /// <summary>
        /// We require the Parent Assembly be set during construction
        /// </summary>
        /// <param name="parent">The Assembly parent</param>
        public CBELComputedVariableBase(ICBELAssembly parent)
        {
            Parent = parent;
            Validators = new List<IVariableValidator<T>>();
            _ReliesOn = new VariableLinkDictionary();
            _ReferencedBy = new VariableLinkDictionary();
        }

        /// <summary>
        /// Make the default constructor private so it isn't used.
        /// </summary>
        private CBELComputedVariableBase() { }

        // protected members
        /// <summary>
        /// CalcDepth is used to track the level of recursive calls.  
        /// The maximum depth is set in the MaxStackDepth value of the assembly, which defaults to 1 (no recursion allowed).
        /// </summary>
        [JsonIgnore]
        protected int CalcDepth = 0;

        /// <summary>
        /// NullResult should be set to Null when T is defined.
        /// C# won't let us return Null when T's type isn't known (and may not be a nullable type)
        /// and there is no way to force it, so this hack will let us just assign once the 
        /// actual type T is set.
        /// </summary>
        [JsonIgnore]
        protected T NullResult;

        /// <summary>
        /// The name of variable used in formulas. This is also the name of the property on 
        /// the Assembly object that references this variable.  It is defined on the Assembly as:
        ///    public {ElementPropertyClass} {name} {get; private set;}
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The display label for the variable.
        /// </summary>
        [JsonIgnore]
        public string Label { get; set; }

        /// <summary>
        /// Reference to the Assembly that contains this element.
        /// </summary>
        [JsonIgnore]
        public virtual ICBELAssembly Parent { get; private set; }

        /// <summary>
        /// The DataType ID the element returns.
        /// </summary>
        [JsonIgnore]
        [AutocompleteIgnore]
        public abstract DataType DataType { get; }

        /// <summary>
        /// The text name for the DataType (e.g., String, Number, Material, etc.)
        /// This metadata is useful for debugging.
        /// </summary>
        [JsonIgnore]
        public abstract string DataTypeName { get; }

        /// <summary>
        /// The original formula (in CBEL) for the variable.
        /// This metadata is useful for debugging.
        /// </summary>
        [JsonIgnore]
        public virtual string FormulaText { get; set; }

        /// <summary>
        /// IsOV indicates if the value has been manually overridden by the user.
        /// IsOV = false means the value is determined based on the default formula for the variable
        /// </summary>
        private bool _IsOV;
        public virtual bool IsOV
        {
            get => _IsOV;
            set
            {
                if(this is ICBELVariableOverridable)
                    _IsOV = value;
                else
                    Parent.LogException(new InvalidLabelAssignmentException(this));
            }
        }

        /// <summary>
        /// IsCalculated indicates that the variable's value needs to be recomputed.  This can be because it has
        /// never been computed or because a variable that it depends on in it's formula has changed.
        /// Setting IsCalculated does not automatically trigger a recompute.This is done on the Value getter when
        /// IsCalculated = false.
        /// </summary>
        private bool _IsCalculated;
        [JsonIgnore]
        public bool IsCalculated
        {
            get => _IsCalculated;
            // if we don't have a formula or are OV, there is never a need to change the _IsCalculated to false.
            set
            {
                if (value)
                    _IsCalculated = true;
                else if (!IsOV)
                    _IsCalculated = false;
            }
        }

        /// <summary>
        /// Indicates if the variable is in the middle of calculating it's value.
        /// This is used to prevent recursion.
        /// </summary>
        [JsonIgnore]
        public bool IsCalculating => ( CalcDepth > 0 );

        private VariableOVData _UserOVValues;
        [AutocompleteIgnore]
        public VariableOVData UserOVValues
        {
            get
            {
                return _UserOVValues;
            }
            set
            {
                _UserOVValues = value;

                if (value != null)
                    UpdateUserOVValues();
            }
        }

        protected virtual void UpdateUserOVValues()
        {

        }

        [AutocompleteIgnore]
        public VariableOVData FormulaOVValues { get; set; }

        protected virtual void DoCompute()
        {
            if (IsCalculated)
                return;

            if (HasDefaultValue)
                _value = DefaultValue();
            else
                _value = NullResult;

            _IsCalculated = true;
        }

        protected virtual void SetValueOV()
        {
            AsString = UserOVValues.Value;
        }

        public virtual Unit ValueUnit()
        {
            return Unit.None;
        }

        public virtual string GetResultValue()
        {
            return AsString;
        }

        public void Compute()
        {
            CalcDepth++;

            // check for recursion exceeding the stackdepth allowed
            if(CalcDepth > Constants.MaxStackDepth)
                Parent.LogException(new CircularReferenceFormulaException($"Recursive Formula Error in variable {Name}.", this));
            else
            {
                try
                {
                    if (UserOVValues != null && UserOVValues.IsOV)
                        SetValueOV();
                    else if (FormulaOVValues != null)
                        DefaultValueConstant = ToT(FormulaOVValues.Value);

                    // Now go ahead and compute our value
                    DoCompute();
                }

                // catch known exception types
                catch (CBELRuntimeException ex)
                {
                    if (ex.ElementComponent == null)
                        ex.ElementComponent = this;

                    Parent.LogException(ex);
                }

                // handle unknown exception types
                catch (Exception ex)
                {
                    // create a CBEL Exception
                    Parent.LogException(new CBELRuntimeException("CBEL Exception: " + ex.Message, this, ex));
                }
            }

            CalcDepth--;

            // Now run our Validation checks.
            Validate();
        }

        protected abstract T ToT(string value);

        /// <summary>
        /// Validates the current value against the validation rules.
        /// Passes any validation error to the assembly LogValidationException();
        /// Returns true is all validations pass
        /// </summary>
        public bool Validate()
        {
            bool Result = true;

            foreach(IVariableValidator<T> v in Validators)
            {
                v.Validate(this, out ICBELValidationFailure vf);
                if(vf != null)
                {
                    Result = false;
                    Parent?.LogValidationFailure(vf);
                }
            }

            return Result;
        }

        /// <summary>
        /// A list of Validators on the Assembly.
        /// </summary>
        [JsonIgnore]
        [AutocompleteIgnore]
        public List<IVariableValidator<T>> Validators { get; private set; }

        /// <summary>
        /// Property that indicates if the IsRequired Validator applied to this variable.
        /// </summary>
        private bool _IsRequired;
        public bool IsRequired
        {
            get => _IsRequired;
            set
            {
                _IsRequired = value;

                // Remove the IsRequiredValidator if it is present
                if(!value)
                    Validators.RemoveAll(x => ( x is IsRequiredValidator<T> ));

                // else Add the IsRequiredValidator if it is not present
                else if(!Validators.Exists(x => ( x is IsRequiredValidator<T> )))
                    Validators.Add(new IsRequiredValidator<T>());
            }
        }

        /// <summary>
        /// A dictionary of Variables on the Assembly that the formula needs in it's evaluation.
        /// </summary>
        [JsonIgnore]
        private readonly VariableLinkDictionary _ReliesOn;
        [AutocompleteIgnore]
        public VariableLinkDictionary ReliesOn
        {
            get
            {
                // see if we need to relink variables
                if(_ReliesOnUpdated)
                {
                    Parent.LinkElementsByName(_ReliesOn);
                    _ReliesOnUpdated = false;
                }

                return _ReliesOn;
                ;
            }
        }

        private bool _ReliesOnUpdated = false;

        private void DoAddReliesOn(string elementName)
        {
            if (!Endor.CBEL.Grammar.Util.IsSpecialToken(elementName))
            {
                string translatedElementName = Endor.CBEL.Grammar.Util.TranslateToken(elementName);
                if (!_ReliesOn.ContainsKey(translatedElementName))
                    _ReliesOn.Add(translatedElementName, (elementName, null));
            }
        }
        public void AddReliesOn(IEnumerable<string> list)
        {
            // Create a dictionary entry for each variable name passed in
            foreach(string s in list)
                DoAddReliesOn(s);

            // Flag that the list needs to be updated
            _ReliesOnUpdated = true;
        }

        public void AddReliesOn(string elementName)
        {
            // Create a dictionary entry for each variable name passed in
            DoAddReliesOn(elementName);

            // Flag that the list needs to be updated
            _ReliesOnUpdated = true;
        }


        /// <summary>
        /// A dictionary of Variables on the Assembly that reference this variable.
        /// </summary>
        [JsonIgnore]
        private readonly VariableLinkDictionary _ReferencedBy;
        [AutocompleteIgnore]
        public VariableLinkDictionary ReferencedBy
        {
            get
            {
                // see if we need to relink variables
                if(_ReferencedByUpdated)
                {
                    Parent.LinkElementsByName(_ReferencedBy);
                    _ReferencedByUpdated = false;
                }

                return _ReferencedBy;
            }
        }

        private bool _ReferencedByUpdated = false;

        private void DoAddReferencedBy(string elementName)
        {
            if (!Endor.CBEL.Grammar.Util.IsSpecialToken(elementName))
            {
                string translatedElementName = Endor.CBEL.Grammar.Util.TranslateToken(elementName);
                if (!_ReferencedBy.ContainsKey(translatedElementName))
                    _ReferencedBy.Add(translatedElementName, (elementName, null));
            }
        }

        public void AddReferencedBy(IEnumerable<string> list)
        {
            // Create a dictionary entry for each variable name passed in
            foreach(string s in list)
                DoAddReferencedBy(s);

            // Flag that the list needs to be updated
            _ReferencedByUpdated = true;
        }


        public void AddReferencedBy(string elementName)
        {
            // Create a dictionary entry for each variable name passed in
            DoAddReferencedBy(elementName);

            // Flag that the list needs to be updated
            _ReferencedByUpdated = true;
        }


        /// <summary>
        /// A list of external dependencies that are referenced by the formula.
        /// E.g. (Company.Name,  Order.Salesperson.FirstName, etc.)
        /// The value for all external references are stored in parent the Assembly component.
        /// </summary>
        [JsonIgnore]
        [AutocompleteIgnore]
        public List<string> ExternalDependencies { get; }


        /// <summary>
        /// This stores the function that is used to evaluate the default value.
        /// Note: The scope of the function that is passed in must be on the Assembly - not the element.
        /// </summary>
        protected Func<T> _DefaultValueFunction;
        [JsonIgnore]
        [AutocompleteIgnore]
        public Func<T> DefaultValueFunction
        {
            get => _DefaultValueFunction;
            set
            {
                DefaultValueIsConstant = false;
                HasDefaultValue = true;
                _DefaultValueFunction = value;

                // If we aren't overridden, set IsDirty to true
                // so that we know we need to compute to get a starting value
                // but don't compute until it is needed.
                if (!IsOV)
                    _IsCalculated = false;
            }
        }

        // Stores the Constant as the DefaultValue for this object
        private T _DefaultValueConstant;
        [JsonIgnore]
        public T DefaultValueConstant
        {
            get => _DefaultValueConstant;
            set
            {
                DefaultValueIsConstant = true;
                HasDefaultValue = true;
                _DefaultValueConstant = value;

                // if we aren't overridden, go ahead and set the value
                // and turn IsCalculated true so we don't need re-calculate
                if (!IsOV)
                {
                    _value = value;
                    _IsCalculated = true;
                }
            }
        }

        // This field is used to indicate if a formula or constant value is set for the default
        // It's value is automatically updated when the DefaultValueConstant or DefaultValueFunction is set.
        [JsonIgnore]
        public virtual bool DefaultValueIsConstant { get; private set; }

        // This field is used to indicate if there is a default
        [JsonIgnore]
        public bool HasDefaultValue { get; private set; }

        // This field is used to indicate if there is a value
        // It is the negative of IsNull.  Either can be used based on the logic.
        [JsonIgnore]
        public bool HasValue => !EqualityComparer<T>.Default.Equals(_value, NullResult);

        // This field is used to indicate if the value is NULL (not zero)
        // It is the negative of HasValue.  Either can be used based on the logic.
        [JsonIgnore]
        public bool IsNull => EqualityComparer<T>.Default.Equals(_value, NullResult);

        /// <summary>
        /// Value stores the last computed value.  On access, if the data is dirty, the Compute() function is called
        /// and the Value is updated before being returned.  When value changes (either during the computation or because it was OV)
        /// the function calls all of the DependedOnBy variables and sets them as IsDirty=true so they know they need to recalculate.
        /// </summary>
        private T _value;
        public  T Value
        {
            get
            {
                if (!IsCalculated && !IsOV)
                    Compute();
                return _value;
            }

            set
            {
                // check that we are dealing with an element that can be overridden first
                if (this is ICBELVariableOverridable)
                {
                    IsOV = true;
                    _IsCalculated = true;
                    _value = value;
                }
                else
                    Parent.LogException(new InvalidLabelAssignmentException(this));
            }
        }

        public dynamic AsDynamic
        {
            get => Value;
            set { Value = value; }
        }

        /// <summary>
        /// Evaulates and returns the value for this variable if not overridden 
        /// </summary>
        public virtual T DefaultValue()
        {
            return DefaultValueIsConstant ? DefaultValueConstant : DefaultValueFunction();
        }

        /// <summary>
        /// Returns the value as a decimal.  
        /// If value == null, then null is returned without error.
        /// Throws an error if value can't be converted to a number.  
        /// </summary>
        /// <returns>decimal?</returns>
        public abstract decimal? AsNumber { get; set; }

        /// <summary>
        /// Returns the value as a string.  
        /// If value == null, then null is returned without error.
        /// For numbers, a general format is used.
        /// For booleans, the value "true" or "false" is returned.
        /// For objects, the Name property of the object is returned.
        /// </summary>
        /// <returns>string</returns>
        public abstract string AsString { get; set; }

        /// <summary>
        /// Returns the value as a boolean.  
        /// If value == null, then null is returned without error.
        /// For numbers, 0 == false, non-zero == true.
        /// For strings, the first non-white space character is checked.
        ///    true == 'TtYy1',  false == 'FfNn0',  other values throw an error
        /// Throws an error if value can't be converted to a number.  
        /// </summary>
        /// <returns>bool</returns>
        public abstract bool? AsBoolean { get; set; }

        /// <summary>
        /// Returns the value as an ICBELObject.
        /// Throws an error if the DataType is not an Object.
        /// If value == null, then null is returned without error.
        /// </summary>
        /// <returns>ICBELObject</returns>
        [AutocompleteIgnore]
        public abstract ICBELObject AsObject();

        [AutocompleteIgnore]
        public virtual void ResetValue()
        {
            _IsOV = false;
            _IsCalculated = false;
            _value = NullResult;
        }

        private string _altConstant;
        private Func<string> _altFunction;

        [JsonIgnore]
        public virtual string AltFormulaText { get; set; }
        [JsonIgnore]
        public virtual bool AltIsFormula { get; private set; }
        [JsonIgnore]
        public string AltConstant
        {
            set
            {
                AltIsFormula = false;
                _altConstant = value;
            }
            get
            {
                return _altConstant;
            }
        }
        [JsonIgnore]
        [AutocompleteIgnore]
        public Func<string> AltFunction
        {
            set
            {
                AltIsFormula = true;
                _altFunction = value;
            }
            get
            {
                return _altFunction;
            }
        }
        public virtual string Alt()
        {
            return AltIsFormula ? AltFunction() : AltConstant;
        }
        public string AltAsString
        {
            get => Alt();
        }
        public bool? IsIncluded { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(false)]
        public bool? IsIncludedOV { get; set; }
        [JsonIgnore]
        public virtual string InclusionFormulaText { get; set; }
        [JsonIgnore]
        [AutocompleteIgnore]
        public Func<bool?> InclusionFunction { get; set; }
        public bool CheckIsIncluded()
        {
            if (InclusionFunction != null)
                return InclusionFunction().GetValueOrDefault(true);

            return true;
        }
    }

}