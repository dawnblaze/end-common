﻿using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Units;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Elements
{
    /// <summary>
    /// This class is contains the formula for a variable.  This may be used for it's own consumption or 
    /// for overrides to the Linked Component variables. The FormulaType determines how it is used.
    /// 
    /// See https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/979009618/AssemblyVariableFormula+Object
    /// </summary>
    public class CBELVariableFormula : ICBELVariableFormula
    {
        public CBELVariableFormula()
        {

        }

        public CBELVariableFormula(ICBELVariable parentVariable)
        {
            ParentVariable = parentVariable;
        }

        
        /// <summary>
        /// ID of the Link ... not used externally. Only for database and EF tracking.
        /// </summary>
        public int? ID { get; set; }

        /// <summary>
        /// Reference to the parent variable.  All overrides set the values on the linked component for this variable.
        /// The ParentVariable must be one of the following types: Dropdown List of Machines or Assemblies, Directly Linked Assembly, Directly Linked Machine.
        /// </summary>
        public ICBELVariable ParentVariable { get; set; }

        /// <summary>
        /// The property name of the child variable being set.
        /// </summary>
        public string ChildVariableName { get; set; }

        /// <summary>
        /// The datatype of the value or formula.  This should always be the same as the DataType of the ChildVariable.
        /// </summary>
        public DataType DataType { get; set; }

        /// <summary>
        /// The value or formula the child variable is being set to.  This can be a constant or a formula, and follows the same rules
        /// and logic as other variable formulas.
        /// </summary>
        public string FormulaText { get; set; }

        /// <summary>
        /// The Unit associated with the DefaultValue.  This only applies when the ChildVariable is a measurement, and should be null otherwise.
        /// Defaults to the same unit as ChildVariable (or null, if the ChildVariable is not a measurement).
        /// </summary>
        public Unit? Unit { get; set; }

        /// <summary>
        /// A flag indicating if the DefaultValue is a formula or constant. This follows the same rules
        /// and logic the IsFormula on variable formulas.
        /// </summary>
        public bool IsFormula { get; }

        /// <summary>
        /// Enum describing how the formula/value is used.
        ///     public enum AssemblyFormulaUseType : byte
        ///     {
        ///        DefaultValue = 0,
        ///        Consumption = 1,
        ///        IsEnabled = 2,
        ///        IsRequired = 3,
        ///        IsVisible = 4,
        ///        IsLinked = 10,
        ///        LinkedVariableOV = 11
        ///     }
        /// </summary>

        public AssemblyFormulaUseType FormulaUseType { get; set; }

        /// <summary>
        /// Enum describing the method by which the formula is to be evaluated.
        ///   public enum AssemblyFormulaEvalType : byte
        ///   {
        ///       Constant = 0,
        ///       MappedVariable = 1,
        ///       CBEL = 2,
        ///       CSharp = 3,
        ///       CFL = 4
        ///   }
        /// </summary>
        public AssemblyFormulaEvalType FormulaEvalType { get; set; }

        public string ValueFunction { get; set; }
    }
}
