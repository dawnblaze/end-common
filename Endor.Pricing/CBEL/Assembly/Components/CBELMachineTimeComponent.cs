﻿using Endor.CBEL.Assembly.BaseClasses;
using Endor.CBEL.Interfaces;
using Endor.Models;

namespace Endor.CBEL.Assembly.Components
{
    public class CBELMachineTimeComponent : CBELBaseComponent
    {
        public override int? ClassTypeID => ClassType.MachineTime.ID();
        public ICBELMachine ParentMachine { get; set; }
    }
}
