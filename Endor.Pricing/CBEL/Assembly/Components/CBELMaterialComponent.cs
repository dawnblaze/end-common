﻿using Endor.CBEL.Interfaces;
using Endor.CBEL.Assembly.BaseClasses;
using System;
using System.Collections.Generic;
using System.Text;
using Endor.CBEL.Common;
using Endor.Models;

namespace Endor.CBEL.Assembly.Components
{
    public class CBELMaterialComponent : CBELBaseComponent, ICBELMaterialComponent
    {
        public string NameOnInvoice { get; set; }

        public string SKU { get; set; }

        public string Description { get; set; }

        public string MaterialType { get; set; }

        public Measurement Length { get; set; }

        public Measurement Height {
            get { return Length; }
        }

        public Measurement Width { get; set; }

        public Measurement Depth {
            get { return Width; }
        }

        public Measurement Thickness
        {
            get { return Height; }
        }

        public Measurement Weight { get; set; }

        public decimal? QuantityInSet { get; set; }
    }
}
