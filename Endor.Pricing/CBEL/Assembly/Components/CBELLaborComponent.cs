﻿using Endor.CBEL.Assembly.BaseClasses;
using Endor.CBEL.Interfaces;
using Endor.CBEL.Common;
using System;
using System.Collections.Generic;
using System.Text;
using Endor.Models;

namespace Endor.CBEL.Assembly.Components
{
    public class CBELLaborComponent : CBELBaseComponent, ICBELLaborComponent
    {
        public string NameOnInvoice { get; set; }

        public string SKU { get; set; }

        public string Description { get; set; }

        public decimal? SetupPrice { get; set; }

        public decimal? HourlyPrice { get; set; }

        public Measurement BillingIncrement { get; set; }

        public Measurement MinimumBillingTime { get; set; }

        /// <summary>
        /// The fixed price of the assembly.
        /// </summary>
        public byte? MinimumTimeInMin { get; set; }

        /// <summary>
        /// The fixed price of the assembly.
        /// </summary>
        public byte? BillingIncrementInMin { get; set; }
    }
}
