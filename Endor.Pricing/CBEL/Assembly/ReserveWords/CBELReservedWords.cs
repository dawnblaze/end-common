﻿using Endor.CBEL.Assembly.BaseClasses;
using Endor.CBEL.Common;
using Endor.CBEL.Compute;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Endor.EF;
using Endor.Pricing;
using Endor.Pricing.CBEL.Assembly.BaseClasses;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Endor.Tenant;

namespace Endor.CBEL.ReserveWords
{
    public class CBELReservedWords
    {   
        public List<string> reservedWords()
        {
            List<string> listofReservedWords = new List<string>();

            
            listofReservedWords.Add("abstract");
            listofReservedWords.Add("as");
            listofReservedWords.Add("base");
            listofReservedWords.Add("bool");
            listofReservedWords.Add("break");
            listofReservedWords.Add("byte");
            listofReservedWords.Add("case");
            listofReservedWords.Add("catch");
            listofReservedWords.Add("char");
            listofReservedWords.Add("checked");
            listofReservedWords.Add("class");
            listofReservedWords.Add("const");
            listofReservedWords.Add("continue");
            listofReservedWords.Add("decimal");
            listofReservedWords.Add("default");
            listofReservedWords.Add("delegate");
            listofReservedWords.Add("do");
            listofReservedWords.Add("double");
            listofReservedWords.Add("else");
            listofReservedWords.Add("enum");
            listofReservedWords.Add("event");
            listofReservedWords.Add("explicit");
            listofReservedWords.Add("extern");
            listofReservedWords.Add("false");
            listofReservedWords.Add("finally");
            listofReservedWords.Add("fixed");
            listofReservedWords.Add("float");
            listofReservedWords.Add("for");
            listofReservedWords.Add("foreach");
            listofReservedWords.Add("goto");
            listofReservedWords.Add("if");
            listofReservedWords.Add("implicit");
            listofReservedWords.Add("in");
            listofReservedWords.Add("int");
            listofReservedWords.Add("interface");
            listofReservedWords.Add("internal");
            listofReservedWords.Add("is");
            listofReservedWords.Add("lock");
            listofReservedWords.Add("long");
            listofReservedWords.Add("namespace");
            listofReservedWords.Add("new");
            listofReservedWords.Add("null");
            listofReservedWords.Add("object");
            listofReservedWords.Add("operator");
            listofReservedWords.Add("out");
            listofReservedWords.Add("override");
            listofReservedWords.Add("params");
            listofReservedWords.Add("private");
            listofReservedWords.Add("protected");
            listofReservedWords.Add("public");
            listofReservedWords.Add("readonly");
            listofReservedWords.Add("ref");
            listofReservedWords.Add("return");
            listofReservedWords.Add("sbyte");
            listofReservedWords.Add("sealed");
            listofReservedWords.Add("short");
            listofReservedWords.Add("sizeof");
            listofReservedWords.Add("stackalloc");
            listofReservedWords.Add("static");
            listofReservedWords.Add("string");
            listofReservedWords.Add("struct");
            listofReservedWords.Add("switch");
            listofReservedWords.Add("this");
            listofReservedWords.Add("throw");
            listofReservedWords.Add("true");
            listofReservedWords.Add("try");
            listofReservedWords.Add("typeof");
            listofReservedWords.Add("uint");
            listofReservedWords.Add("ulong");
            listofReservedWords.Add("unchecked");
            listofReservedWords.Add("unsafe");
            listofReservedWords.Add("ushort");
            listofReservedWords.Add("using");
            listofReservedWords.Add("virtual");
            listofReservedWords.Add("void");
            listofReservedWords.Add("volatile");
            listofReservedWords.Add("while");

            return listofReservedWords;
        }
    }
}