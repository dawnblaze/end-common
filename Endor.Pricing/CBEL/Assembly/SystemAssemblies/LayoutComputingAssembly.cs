﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Endor.CBEL.Common;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Endor.CBEL.Elements
{
    public class LayoutComputingAssembly : CBELAssemblyBase, ICBELLayoutComputation
    {
        public LayoutComputingAssembly(string instanceName, ApiContext ctx, short bid, ITenantDataCache cache, RemoteLogger logger) : base(ctx, bid, cache, logger)
        {
            this.ID = 1;
            this.Name = instanceName;

            var optionCategory = ctx.SystemOptionCategory.Include(t => t.SystemOptionDefinitions).FirstOrDefault(t => t.ID == 910);
            if (optionCategory == null)
            {
                throw new Exception("Layout Visualization Options not found. Please run the latest migration.");
            }

            foreach (SystemOptionDefinition systemOption in optionCategory.SystemOptionDefinitions)
            {
                var propertyName = systemOption.Name.Replace("LayoutVisualization.", "Option");
                var propertyValue = Convert.ToBoolean(systemOption.DefaultValue);
                Type layoutType = this.GetType();

                FieldInfo propertyInfo = layoutType.GetField(propertyName);
                propertyInfo.SetValue(this, propertyValue);
            }
        }

        #region Declare Option Variables
        public bool OptionHideBleed;
        public bool OptionHideColorbar;
        public bool OptionHideCutlines;
        public bool OptionHideFoldlines;
        public bool OptionHideGutter;
        public bool OptionHideLeader;
        public bool OptionHideMargin;
        public bool OptionHideMaterialDimensions;
        public bool OptionHidePlaceholders;
        public bool OptionHideRegistration;
        public bool OptionHideWatermark;
        #endregion

        public override void InitializeData()
        {
            // do nothing here
        }

        #region Declare Option Variables

        #endregion

        #region Initialize Variables

        public override void InitializeVariables()
        {
            // Define the Elements
            // =====================================================================

            _BleedBottom = CreateNumericEditElement(nameof(BleedBottom), "Bleed-Bottom", false);
            _Bleed = CreateNumericEditElement(nameof(Bleed), "Bleed", false);
            _BleedLeft = CreateNumericEditElement(nameof(BleedLeft), "Bleed-Left", false);
            _BleedRight = CreateNumericEditElement(nameof(BleedRight), "Bleed-Right", false);
            _BleedTop = CreateNumericEditElement(nameof(BleedTop), "Bleed-Top", false);
            _ColorBarLength = CreateNumericEditElement(nameof(ColorBarLength), "ColorBarLength", false);
            _ColorBarPosition = CreateDropDownStringListElement(nameof(ColorBarPosition), "ColorBarPosition", false, "Top;Bottom;Left;Right");
            _ColorBarThickness = CreateNumericEditElement(nameof(ColorBarThickness), "ColorBarThickness", false);
            _ColorsFront = CreateNumericEditElement(nameof(ColorsFront), "Colors on Front", false);
            _ColorsBack = CreateNumericEditElement(nameof(ColorsBack), "Colors on Back", false);
            _CutsPerMaterial = CreateNumericEditElement(nameof(CutsPerMaterial), "Cuts per Material", false);
            _TotalCutCount = CreateNumericEditElement(nameof(TotalCutCount), "Total Number of Cuts", false);
            _Gutter = CreateNumericEditElement(nameof(Gutter), "Gutter", false);
            _GutterHorizontal = CreateNumericEditElement(nameof(GutterHorizontal), "Gutter-Left", false);
            _GutterVertical = CreateNumericEditElement(nameof(GutterVertical), "Gutter-Right", false);
            _ImageCount = CreateNumericEditElement(nameof(ImageCount), "Images Count", false);
            _ImageBackUniquenessRule = CreateDropDownStringListElement(nameof(ImageBackUniquenessRule), "Rule for Second Side Images", false, "Duplicate;Unique");
            _ImageItemUniquenessRule = CreateDropDownStringListElement(nameof(ImageItemUniquenessRule), "Rule for Determining Uniqueness of Images within an Ordered Quantity", false, "Duplicate;Unique");
            _ImageOrderUniquenessRule = CreateDropDownStringListElement(nameof(ImageOrderUniquenessRule), "Rule for Determining Uniqueness of Images between Ordered Quantities", false, "Duplicate;Unique");
            _ImpositionType = CreateDropDownStringListElement(nameof(ImpositionType), "Imposition Type", false, "None;Sheetwise;WorkAndTurn;WorkAndTumble");
            _IncludeColorBar = CreateCheckboxElement(nameof(IncludeColorBar), "Include ColorBar", false);
            _IncludeCuts = CreateCheckboxElement(nameof(IncludeCuts), "Include Cuts", false);
            _IncludeVisualization = CreateCheckboxElement(nameof(IncludeVisualization), "Include Visualization Data", false);
            _Is2Sided = CreateCheckboxElement(nameof(Is2Sided), "Is 2 Sided", false);
            _IsPaneled = CreateCheckboxElement(nameof(IsPaneled), "Is Paneled", false);
            _LayoutType = CreateDropDownStringListElement(nameof(LayoutType), "Layout Type", false, "None;Sheet;Roll");
            _LeaderBottom = CreateNumericEditElement(nameof(LeaderBottom), "Leader-Bottom", false);
            _LeaderLeft = CreateNumericEditElement(nameof(LeaderLeft), "Leader-Left", false);
            _LeaderRight = CreateNumericEditElement(nameof(LeaderRight), "Leader-Right", false);
            _LeaderTop = CreateNumericEditElement(nameof(LeaderTop), "Leader-Top", false);
            _MaterialHeight = CreateNumericEditElement(nameof(MaterialHeight), "Material Height", false);
            _MaterialMarginBottom = CreateNumericEditElement(nameof(MaterialMarginBottom), "Material Margin-Bottom", false);
            _MaterialMargin = CreateNumericEditElement(nameof(MaterialMargin), "Material Margin", false);
            _MaterialMarginLeft = CreateNumericEditElement(nameof(MaterialMarginLeft), "Material Margin-Left", false);
            _MaterialMarginRight = CreateNumericEditElement(nameof(MaterialMarginRight), "Material Margin-Right", false);
            _MaterialMarginTop = CreateNumericEditElement(nameof(MaterialMarginTop), "Material Margin-Top", false);
            _MaterialRunCount = CreateNumericEditElement(nameof(MaterialRunCount), "Material Run Count", false);
            _MaterialWidth = CreateNumericEditElement(nameof(MaterialWidth), "Material Width", false);
            _MaxMaterialsPerCut = CreateNumericEditElement(nameof(MaxMaterialsPerCut), "Materials Per Cut", false);
            _MaterialSetupWasteCount = CreateNumericEditElement(nameof(MaterialSetupWasteCount), "Material Setup Waste Count", false);
            _MaterialRunningWastePercentage = CreateNumericEditElement(nameof(MaterialRunningWastePercentage), "Material Running Waste Percentage", false);
            _ItemHeight = CreateNumericEditElement(nameof(ItemHeight), "Item Height", true);
            _ItemsHorizontal = CreateNumericEditElement(nameof(ItemsHorizontal), "Items Horizontal", false);
            _ItemQuantity = CreateNumericEditElement(nameof(ItemQuantity), "Item Quantity", true);
            _ItemsVertical = CreateNumericEditElement(nameof(ItemsVertical), "Items Vertical", false);
            _ItemWidth = CreateNumericEditElement(nameof(ItemWidth), "Item Width", true);
            _RotateItem = CreateCheckboxElement(nameof(RotateItem), "Rotate Item", false);
            _RotateMaterial = CreateCheckboxElement(nameof(RotateMaterial), "Rotate Material", false);
            _OrderedQuantity = CreateNumericEditElement(nameof(OrderedQuantity), "Set Count", false);
            _SignatureCount = CreateNumericEditElement(nameof(SignatureCount), "Signature Count", false);
            _HideBleed = CreateCheckboxElement(nameof(HideBleed), "Hide Bleed", false);
            _HideColorBar = CreateCheckboxElement(nameof(HideColorBar), "Hide Color Bar", false);
            _HideCutLines = CreateCheckboxElement(nameof(HideCutLines), "Hide Cut Lines", false);
            _HideFoldLines = CreateCheckboxElement(nameof(HideFoldLines), "Hide Fold Lines", false);
            _HideGutter = CreateCheckboxElement(nameof(HideGutter), "Hide Gutter", false);
            _HideLeader = CreateCheckboxElement(nameof(HideLeader), "Hide Leader", false);
            _HideMargin = CreateCheckboxElement(nameof(HideMargin), "Hide Margin", false);
            _HideMaterialDimensions = CreateCheckboxElement(nameof(HideMaterialDimensions), "Hide Material Dimensions", false);
            _HidePlaceholders = CreateCheckboxElement(nameof(HidePlaceholders), "Hide Placeholders", false);
            _HideRegistrationMarks = CreateCheckboxElement(nameof(HideRegistrationMarks), "Hide Registration Marks", false);
            _HideWatermark = CreateCheckboxElement(nameof(HideWatermark), "Hide Watermark", false);
            _TotalImpressionCount = CreateNumericEditElement(nameof(TotalImpressionCount), "Total Impressions", false);
            _TotalItemArea = CreateNumericEditElement(nameof(TotalItemArea), "TotalItemArea", false);
            _TotalItemFaceArea = CreateNumericEditElement(nameof(TotalMaterialArea), "Total Printed Area", false);
            _TotalMaterialCount = CreateNumericEditElement(nameof(TotalMaterialCount), "Total Material Count with Scrap", false);
            _TotalMaterialWasteCount = CreateNumericEditElement(nameof(TotalMaterialWasteCount), "Total Material Waste Count", false);
            _TotalWasteArea = CreateNumericEditElement(nameof(TotalWasteArea), "TotalScrapArea", false);
            _UseVisualization = CreateCheckboxElement(nameof(UseVisualization), "Use Visualizations", false);
            _VisualizationData = CreateMultiLineTextElement(nameof(VisualizationData), "Visualization Data", false);
            _Machine = CreateSingleLineTextElement(nameof(Machine), "Machine", false);
            _Material = CreateSingleLineTextElement(nameof(Material), "Material", false);
            _MachineMaxHorizontal = CreateNumericEditElement(nameof(MachineMaxHorizontal), "Machine Max Horizontal", false);
            _MachineMaxVertical = CreateNumericEditElement(nameof(MachineMaxVertical), "Machine Max Vertical", false);

            // Now call the routine that defines the formulas and default values
            DefineFormulasAndDefaults();
        }

        #endregion Initialize Variabless

        #region Declare Input Variables
        // =====================================================================
        // Input Variables
        // =====================================================================
        private NumberEditElement _Bleed;
        public NumberEditElement Bleed => _Bleed;

        private NumberEditElement _BleedBottom;
        public NumberEditElement BleedBottom => _BleedBottom;

        private NumberEditElement _BleedLeft;
        public NumberEditElement BleedLeft => _BleedLeft;

        private NumberEditElement _BleedRight;
        public NumberEditElement BleedRight => _BleedRight;

        private NumberEditElement _BleedTop;
        public NumberEditElement BleedTop => _BleedTop;

        private NumberEditElement _ColorBarLength;
        public NumberEditElement ColorBarLength => _ColorBarLength;

        private DropDownStringListElement _ColorBarPosition;
        public DropDownStringListElement ColorBarPosition => _ColorBarPosition;

        private NumberEditElement _ColorBarThickness;
        public NumberEditElement ColorBarThickness => _ColorBarThickness;

        private NumberEditElement _ColorsBack;
        public NumberEditElement ColorsBack => _ColorsBack;

        private NumberEditElement _ColorsFront;
        public NumberEditElement ColorsFront => _ColorsFront;

        private NumberEditElement _Gutter;
        public NumberEditElement Gutter => _Gutter;

        private NumberEditElement _GutterHorizontal;
        public NumberEditElement GutterHorizontal => _GutterHorizontal;

        private NumberEditElement _GutterVertical;
        public NumberEditElement GutterVertical => _GutterVertical;


        private DropDownStringListElement _ImageBackUniquenessRule;
        public DropDownStringListElement ImageBackUniquenessRule => _ImageBackUniquenessRule;


        private DropDownStringListElement _ImageItemUniquenessRule;
        public DropDownStringListElement ImageItemUniquenessRule => _ImageItemUniquenessRule;


        private DropDownStringListElement _ImageOrderUniquenessRule;
        public DropDownStringListElement ImageOrderUniquenessRule => _ImageOrderUniquenessRule;



        private DropDownStringListElement _ImpositionType;
        public DropDownStringListElement ImpositionType => _ImpositionType;

        private CheckboxElement _IncludeColorBar;
        public CheckboxElement IncludeColorBar => _IncludeColorBar;

        private CheckboxElement _IncludeCuts;
        public CheckboxElement IncludeCuts => _IncludeCuts;

        private CheckboxElement _IncludeVisualization;
        public CheckboxElement IncludeVisualization => _IncludeVisualization;
        
        private CheckboxElement _Is2Sided;
        public CheckboxElement Is2Sided => _Is2Sided;

        private NumberEditElement _ItemHeight;
        public NumberEditElement ItemHeight => _ItemHeight;

        private NumberEditElement _ItemQuantity;
        public NumberEditElement ItemQuantity => _ItemQuantity;

        private NumberEditElement _ItemWidth;
        public NumberEditElement ItemWidth => _ItemWidth;

        private DropDownStringListElement _LayoutType;
        public DropDownStringListElement LayoutType => _LayoutType;

        private NumberEditElement _LeaderBottom;
        public NumberEditElement LeaderBottom => _LeaderBottom;

        private NumberEditElement _LeaderLeft;
        public NumberEditElement LeaderLeft => _LeaderLeft;

        private NumberEditElement _LeaderRight;
        public NumberEditElement LeaderRight => _LeaderRight;

        private NumberEditElement _LeaderTop;
        public NumberEditElement LeaderTop => _LeaderTop;

        private NumberEditElement _MaterialHeight;
        public NumberEditElement MaterialHeight => _MaterialHeight;

        private NumberEditElement _MaterialMargin;
        public NumberEditElement MaterialMargin => _MaterialMargin;

        private NumberEditElement _MaterialMarginBottom;
        public NumberEditElement MaterialMarginBottom => _MaterialMarginBottom;

        private NumberEditElement _MaterialMarginLeft;
        public NumberEditElement MaterialMarginLeft => _MaterialMarginLeft;

        private NumberEditElement _MaterialMarginRight;
        public NumberEditElement MaterialMarginRight => _MaterialMarginRight;

        private NumberEditElement _MaterialMarginTop;
        public NumberEditElement MaterialMarginTop => _MaterialMarginTop;

        private NumberEditElement _MaterialRunningWastePercentage;
        public NumberEditElement MaterialRunningWastePercentage => _MaterialRunningWastePercentage;

        private NumberEditElement _MaterialSetupWasteCount;
        public NumberEditElement MaterialSetupWasteCount => _MaterialSetupWasteCount;

        private NumberEditElement _TotalMaterialWasteCount;
        public NumberEditElement TotalMaterialWasteCount => _TotalMaterialWasteCount;

        private NumberEditElement _MaterialWidth;
        public NumberEditElement MaterialWidth => _MaterialWidth;

        private NumberEditElement _MaxMaterialsPerCut;
        public NumberEditElement MaxMaterialsPerCut => _MaxMaterialsPerCut;

        private NumberEditElement _OrderedQuantity;
        public NumberEditElement OrderedQuantity => _OrderedQuantity;

        private CheckboxElement _RotateItem;
        public CheckboxElement RotateItem => _RotateItem;

        private CheckboxElement _RotateMaterial;
        public CheckboxElement RotateMaterial => _RotateMaterial;

        private CheckboxElement _HideBleed;
        public CheckboxElement HideBleed => _HideBleed;

        private CheckboxElement _HideColorBar;
        public CheckboxElement HideColorBar => _HideColorBar;

        private CheckboxElement _HideCutLines;
        public CheckboxElement HideCutLines => _HideCutLines;

        private CheckboxElement _HideFoldLines;
        public CheckboxElement HideFoldLines => _HideFoldLines;

        private CheckboxElement _HideGutter;
        public CheckboxElement HideGutter => _HideGutter;

        private CheckboxElement _HideLeader;
        public CheckboxElement HideLeader => _HideLeader;

        private CheckboxElement _HideMargin;
        public CheckboxElement HideMargin => _HideMargin;

        private CheckboxElement _HideMaterialDimensions;
        public CheckboxElement HideMaterialDimensions => _HideMaterialDimensions;

        private CheckboxElement _HideRegistrationMarks;
        public CheckboxElement HideRegistrationMarks => _HideRegistrationMarks;

        private CheckboxElement _HideWatermark;
        public CheckboxElement HideWatermark => _HideWatermark;

        private CheckboxElement _UseVisualization;
        public CheckboxElement UseVisualization => _UseVisualization;

        private SingleLineTextElement _Machine;
        public SingleLineTextElement Machine => _Machine;
        
        private SingleLineTextElement _Material;
        public SingleLineTextElement Material => _Material;

        private NumberEditElement _MachineMaxHorizontal;
        public NumberEditElement MachineMaxHorizontal => _MachineMaxHorizontal;

        private NumberEditElement _MachineMaxVertical;
        public NumberEditElement MachineMaxVertical => _MachineMaxVertical;

        private CheckboxElement _HidePlaceholders;
        public CheckboxElement HidePlaceholders => _HidePlaceholders;

        #endregion Input Variables

        #region Declare Computed Variables
        // =====================================================================
        // Computed Variables
        // These computations are stored as variables, but are private so that the 
        // user may not set access the underlyine variable.
        // The Elements are private so they are not shown or OV by the using assembly
        // =====================================================================
        private NumberEditElement _CutsPerMaterial;
        public NumberEditElement CutsPerMaterial => _CutsPerMaterial;

        private NumberEditElement _ImageCount;
        public NumberEditElement ImageCount => _ImageCount;

        private CheckboxElement _IsPaneled;
        public CheckboxElement IsPaneled => _IsPaneled;

        private NumberEditElement _ItemsHorizontal;
        public NumberEditElement ItemsHorizontal => _ItemsHorizontal;

        private NumberEditElement _ItemsVertical;
        public NumberEditElement ItemsVertical => _ItemsVertical;

        private NumberEditElement _MaterialRunCount;
        public NumberEditElement MaterialRunCount => _MaterialRunCount;

        private NumberEditElement _SignatureCount;
        public NumberEditElement SignatureCount => _SignatureCount;

        private NumberEditElement _TotalCutCount;
        public NumberEditElement TotalCutCount => _TotalCutCount;

        private NumberEditElement _TotalImpressionCount;
        public NumberEditElement TotalImpressionCount => _TotalImpressionCount;

        private NumberEditElement _TotalItemArea;
        public NumberEditElement TotalItemArea => _TotalItemArea;

        private NumberEditElement _TotalItemFaceArea;
        public NumberEditElement TotalItemFaceArea => _TotalItemFaceArea;

        private NumberEditElement _TotalMaterialCount;
        public NumberEditElement TotalMaterialCount => _TotalMaterialCount;
        
        private NumberEditElement _TotalWasteArea;
        public NumberEditElement TotalWasteArea => _TotalWasteArea;

        private MultiLineTextElement _VisualizationData;
        public MultiLineTextElement VisualizationData => _VisualizationData;

        #endregion Computed Variables

        #region Declare Computed Properties

        // =====================================================================
        // Computed Properties 
        // These values are primarily used for Intermediate logic and Visualilzation Data
        // =====================================================================
        public decimal? ImpressionCountBack => Is2Sided.Value.GetValueOrDefault(false) ? ColorsBack.Value * TotalMaterialCount.Value : 0;
        public decimal? ImpressionCountFront => ColorsFront.Value * TotalMaterialCount.Value;
        public bool? IsPaneledHorizontally =>
            (RotatedMaterialWidth < (RotatedItemWidth + Max(BleedLeft.Value + MaterialMarginLeft.Value, GutterHorizontal.Value) + Max(BleedRight.Value + MaterialMarginRight.Value, GutterHorizontal.Value)));
        public bool? IsPaneledVertically =>
            (RotatedMaterialHeight < (RotatedItemHeight + Max(BleedTop.Value + MaterialMarginTop.Value, GutterVertical.Value) + Max(BleedBottom.Value + MaterialMarginBottom.Value, GutterVertical.Value)));
        public decimal? ItemArea => ItemHeight.Value * ItemWidth.Value;
        public decimal? ItemsOnFirstPage => Min(TotalItemCount, ItemsVertical.Value * ItemsHorizontal.Value);
        public decimal? ItemsOnLastPage => (TotalItemCount == 0) ? 0 : (ItemsOnFirstPage == 0) ? null : (1 + ((TotalItemCount - 1) % ItemsOnFirstPage));
        public decimal? MaterialArea => MaterialHeight.Value * MaterialWidth.Value;
        public decimal? RotatedItemHeight => (RotateItem.Value.GetValueOrDefault(false) ? ItemWidth.Value : ItemHeight.Value);
        public decimal? RotatedItemWidth => (RotateItem.Value.GetValueOrDefault(false) ? ItemHeight.Value : ItemWidth.Value);
        public decimal? RotatedMaterialHeight => (RotateMaterial.Value.GetValueOrDefault(false) ? MaterialWidth.Value : MaterialHeight.Value);
        public decimal? RotatedMaterialWidth => (RotateMaterial.Value.GetValueOrDefault(false) ? MaterialHeight.Value : MaterialWidth.Value);
        public decimal? TotalItemCount => ItemQuantity.Value * OrderedQuantity.Value;
        public decimal? TotalMaterialArea => TotalMaterialCount.Value * MaterialHeight.Value * MaterialWidth.Value;
        public decimal? LeftItemOffset => Max(Max(MaterialMarginLeft.Value.Value, LeaderLeft.Value.Value) + BleedLeft.Value, GutterHorizontal.Value); //Max(NonPrintableSpace+bleed,Gutter)
        public decimal? EffectiveItemWidth => RotatedItemWidth + Max(GutterHorizontal.Value, BleedLeft.Value + BleedRight.Value);
        public decimal? TopItemOffset => Max(Max(MaterialMarginTop.Value, LeaderTop.Value) + BleedTop.Value, GutterVertical.Value);//Max(NonPrintableSpace+bleed,Gutter)
        public decimal? EffectiveItemHeight => RotatedItemHeight + Max(GutterVertical.Value, BleedTop.Value + BleedBottom.Value);
        #endregion Computed Properties - Internal Use Only

        #region Define the Formulas and Defaults
        // =====================================================================
        // Define the Formulas and Default Values
        // =====================================================================
        private void DefineFormulasAndDefaults()
        {
            _Bleed.DefaultValueConstant = 0m;
            _BleedLeft.DefaultValueFunction = () => Bleed.Value;
            _BleedRight.DefaultValueFunction = () => Bleed.Value;
            _BleedTop.DefaultValueFunction = () => Bleed.Value;
            _BleedBottom.DefaultValueFunction = () => Bleed.Value;
            _ColorBarLength.DefaultValueFunction = () => (int)((MaterialHeight.Value ?? 0m) * 0.75m);
            _ColorBarPosition.DefaultValueConstant = "Right";
            _ColorBarThickness.DefaultValueConstant = 0.1m;
            _Gutter.DefaultValueConstant = 0m;
            _GutterHorizontal.DefaultValueFunction = () => Gutter.Value;
            _GutterVertical.DefaultValueFunction = () => Gutter.Value;
            _ImageBackUniquenessRule.DefaultValueConstant = "Duplicate";
            _ImageCount.DefaultValueFunction = () =>
                ((StringToImageRule(ImageOrderUniquenessRule.Value) == ImageRule.Unique) ? OrderedQuantity.Value : 1)
                * ((StringToImageRule(ImageItemUniquenessRule.Value) == ImageRule.Unique) ? ItemQuantity.Value : 1)
                * ((Is2Sided.Value.GetValueOrDefault(false) && (StringToImageRule(ImageBackUniquenessRule.Value) == ImageRule.Unique)) ? 2 : 1);
            _ImageItemUniquenessRule.DefaultValueConstant = "Duplicate";
            _ImageOrderUniquenessRule.DefaultValueConstant = "Duplicate";
            _ImpositionType.DefaultValueConstant = "None";
            _IncludeColorBar.DefaultValueConstant = false;
            _IncludeCuts.DefaultValueConstant = false;
            _IncludeVisualization.DefaultValueConstant = false;
            _Is2Sided.DefaultValueConstant = false;
            _IsPaneled.DefaultValueConstant = false;
            _IsPaneled.DefaultValueFunction = () => OR(IsPaneledHorizontally, IsPaneledVertically);
            _ItemsHorizontal.DefaultValueConstant = 0.1m;
            _ItemsVertical.DefaultValueConstant = 0.1m;
            _ItemQuantity.DefaultValueConstant = 1.0m;
            _LayoutType.DefaultValueFunction = () => "None";
            _LeaderBottom.DefaultValueConstant = 0m;
            _LeaderLeft.DefaultValueConstant = 0m;
            _LeaderRight.DefaultValueConstant = 0m;
            _LeaderTop.DefaultValueConstant = 0m;
            _MaterialHeight.DefaultValueFunction = () => ItemHeight.Value + Max(BleedTop.Value + MaterialMarginTop.Value, GutterVertical.Value) + Max(BleedBottom.Value + MaterialMarginBottom.Value, GutterVertical.Value) + ((ColorBarPosition.Value == "Top" || ColorBarPosition.Value == "Bottom") ? ColorBarThickness.Value : 0m);
            _MaterialMarginBottom.DefaultValueFunction = () => MaterialMargin.Value;
            _MaterialMargin.DefaultValueConstant = 0m;
            _MaterialMarginLeft.DefaultValueFunction = () => MaterialMargin.Value;
            _MaterialMarginRight.DefaultValueFunction = () => MaterialMargin.Value;
            _MaterialMarginTop.DefaultValueFunction = () => MaterialMargin.Value;
            _MaterialWidth.DefaultValueFunction = () => ItemWidth.Value + Max(BleedLeft.Value + MaterialMarginLeft.Value, GutterHorizontal.Value) + Max(BleedRight.Value + MaterialMarginRight.Value, GutterHorizontal.Value) + ((ColorBarPosition.Value == "Left" || ColorBarPosition.Value == "Right") ? ColorBarThickness.Value : 0m);
            _OrderedQuantity.DefaultValueConstant = 1;
            _RotateItem.DefaultValueConstant = false;
            _RotateMaterial.DefaultValueConstant = false;

            _HideBleed.DefaultValueFunction = () => OptionHideBleed || ((BleedTop.Value ?? 0m) == 0m && (BleedBottom.Value ?? 0m) == 0m && (BleedRight.Value ?? 0m) == 0m && (BleedLeft.Value ?? 0m) == 0m);
            _HideColorBar.DefaultValueFunction = () => OptionHideColorbar || !IncludeColorBar.Value.Value || ((ColorBarThickness.Value ?? 0m) == 0m);
            _HideCutLines.DefaultValueFunction = () => OptionHideCutlines;
            _HideFoldLines.DefaultValueFunction = () => OptionHideFoldlines;
            _HideGutter.DefaultValueFunction = () => OptionHideGutter;
            _HideLeader.DefaultValueFunction = () => OptionHideLeader || ((LeaderTop.Value ?? 0m) == 0m && (LeaderBottom.Value ?? 0m) == 0m && (LeaderRight.Value ?? 0m) == 0m && (LeaderLeft.Value ?? 0m) == 0m);
            _HideMargin.DefaultValueFunction = () => OptionHideMargin || ((MaterialMarginTop.Value ?? 0m) == 0m && (MaterialMarginBottom.Value ?? 0m) == 0m && (MaterialMarginRight.Value ?? 0m) == 0m && (MaterialMarginLeft.Value ?? 0m) == 0m);
            _HideMaterialDimensions.DefaultValueFunction = () => OptionHideMaterialDimensions;
            _HidePlaceholders.DefaultValueFunction = () => OptionHidePlaceholders;
            _HideRegistrationMarks.DefaultValueFunction = () => OptionHideRegistration;
            _HideWatermark.DefaultValueFunction = () => OptionHideWatermark;

            _TotalCutCount.DefaultValueFunction = () => CutsPerMaterial.Value * Ceiling(TotalMaterialCount.Value / MaxMaterialsPerCut.Value);
            _TotalImpressionCount.DefaultValueFunction = () => ImpressionCountFront + ImpressionCountBack;
            _TotalItemArea.DefaultValueFunction = () => OrderedQuantity.Value * ItemQuantity.Value * ItemHeight.Value * ItemWidth.Value;
            _TotalItemFaceArea.DefaultValueFunction = () => TotalItemArea.Value * (Is2Sided.Value ?? false ? 2.0m : 1.0m);
            _TotalMaterialWasteCount.DefaultValueFunction = () => MaterialSetupWasteCount.Value + Ceiling(MaterialRunCount.Value * MaterialRunningWastePercentage.Value);
            _TotalWasteArea.DefaultValueFunction = () => TotalMaterialArea - TotalItemArea.Value;
            _MachineMaxHorizontal.DefaultValueConstant = 0m;
            _MachineMaxVertical.DefaultValueConstant = 0m;
            _Machine.DefaultValueConstant = "None";
            _Material.DefaultValueConstant = "None";
            _VisualizationData.DefaultValueFunction = () => JsonConvert.SerializeObject(new LayoutComputationVisualizationModel(this));

            // the following formulas are not defines and are computed in the Comnpute() formula
            /*
            _CutsPerMaterial.DefaultValueFunction = () => null;
            _ItemsHorizontal.DefaultValueFunction = () => null;
            _ItemsVertical.DefaultValueFunction = () => null;
            _MaterialRunCount.DefaultValueFunction = () => null
            _SignatureCount.DefaultValueFunction = () => null;
            _VisualizationData.DefaultValueFunction = () => null;\
            */
        }

        #endregion Define the Formulas and Defaults


        #region Computation Calculations
        // =====================================================================
        // Methods that do the Work!
        // =====================================================================

        protected override void OverrideFormulas()
        {
            if (LayoutType.Value == "Sheet")
                ComputeSheetLayout();

            else if (LayoutType.Value == "Roll")
                ComputeRollLayout();

            else if (LayoutType.Value == "None")
                ComputeNoLayout();

            else LogException(new CBELRuntimeException($"Invalid Machine Layout Type: {LayoutType}", this));
        }

        private void ComputeSheetLayout()
        {
            this._ItemsVertical.DefaultValueFunction = () =>
            {
                var usableMaterialHeight = RotatedMaterialHeight;
                usableMaterialHeight -= Max(LeaderTop.Value, MaterialMarginTop.Value);
                usableMaterialHeight -= Max(LeaderBottom.Value, MaterialMarginBottom.Value);

                // if color bar is top or bottom, it needs to remove the usable material height
                if (IncludeColorBar.Value.GetValueOrDefault(false) && (ColorBarPosition.Value == "Top" || ColorBarPosition.Value == "Bottom"))
                    usableMaterialHeight -= ColorBarThickness.Value;

                var topItemOffset = TopItemOffset;
                var effectiveItemHeight = EffectiveItemHeight;
                int calculatedItemsVertical = 0;

                while (topItemOffset + effectiveItemHeight <= usableMaterialHeight)
                {
                    topItemOffset += effectiveItemHeight;
                    calculatedItemsVertical++;
                }

                return Floor(calculatedItemsVertical);
            };
            this._ItemsHorizontal.DefaultValueFunction = () => CalculateItemsHorizontal();

            this._MaterialRunCount.DefaultValueFunction = this.ComputeMaterialRunCount;
        }

        private decimal? CalculateItemsHorizontal()
        {
            var usableMaterialWidth = RotatedMaterialWidth;
            usableMaterialWidth -= Max(LeaderLeft.Value, MaterialMarginLeft.Value);
            usableMaterialWidth -= Max(LeaderRight.Value, MaterialMarginRight.Value);

            //- Max(GutterHorizontal, MaterialMarginLeft + MaterialMarginRight);
            if (IncludeColorBar.Value.GetValueOrDefault(false) && (ColorBarPosition.Value == "Left" || ColorBarPosition.Value == "Right"))
                usableMaterialWidth -= ColorBarThickness.Value;
            usableMaterialWidth -= Max(LeaderLeft.Value, MaterialMarginLeft.Value);

            var leftItemOffset = LeftItemOffset;
            var effectiveItemWidth = EffectiveItemWidth;

            int calculatedItemsHorizontal = 0;
            while (leftItemOffset + effectiveItemWidth <= usableMaterialWidth)
            {
                leftItemOffset += effectiveItemWidth;
                calculatedItemsHorizontal++;
            }

            return Floor(calculatedItemsHorizontal);
        }

        private decimal? ComputeMaterialRunCount()
        {
            if (!this.ItemsVertical.HasValue || !this.ItemsHorizontal.HasValue || !this.TotalItemCount.HasValue)
                return null;

            decimal? itemsPerPage = this.ItemsVertical.Value * this.ItemsHorizontal.Value;

            if (itemsPerPage == 0)
                return null;

            return (Ceiling(this.TotalItemCount / itemsPerPage));
        }

        private void ComputeRollLayout()
        {
            this._ItemsVertical.DefaultValueFunction = () => 1;
            this._ItemsHorizontal.DefaultValueFunction = () => CalculateItemsHorizontal();
            this._MaterialRunCount.DefaultValueFunction = this.ComputeMaterialRunCount;
        }

        private void ComputeNoLayout()
        {
            //base.Compute();
        }

        #endregion Computation Calculations

        #region Helper Functions
        protected NumberEditElement CreateNumericEditElement(string name, string label, bool isRequired)
        {
            NumberEditElement result = new NumberEditElement(this)
            {
                Name = name,
                Label = label,
                IsRequired = isRequired,
            };
            this.Variables.Add(name, result);
            return result;
        }

        protected CheckboxElement CreateCheckboxElement(string name, string label, bool isRequired)
        {
            CheckboxElement result = new CheckboxElement(this)
            {
                Name = name,
                Label = label,
                IsRequired = isRequired,
            };
            this.Variables.Add(name, result);
            return result;
        }

        protected DropDownStringListElement CreateDropDownStringListElement(string name, string label, bool isRequired, string listValues)
        {
            DropDownStringListElement result = new DropDownStringListElement(this)
            {
                Name = name,
                Label = label,
                IsRequired = isRequired,
                ListValues = new List<string>()
            };
            result.ListValues.AddRange(listValues.Split(";").ToList());
            this.Variables.Add(name, result);
            return result;
        }

        protected SingleLineTextElement CreateSingleLineTextElement(string name, string label, bool isRequired)
        {
            SingleLineTextElement result = new SingleLineTextElement(this)
            {
                Name = name,
                Label = label,
                IsRequired = isRequired,
            };
            this.Variables.Add(name, result);
            return result;
        }

        protected MultiLineTextElement CreateMultiLineTextElement(string name, string label, bool isRequired)
        {
            MultiLineTextElement result = new MultiLineTextElement(this)
            {
                Name = name,
                Label = label,
                IsRequired = isRequired,
            };
            this.Variables.Add(name, result);
            return result;
        }

        /// <summary>
        ///  Convert a string to a ImageRule
        /// </summary>
        /// <param name="Position"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected ImageRule StringToImageRule(string Rule, ImageRule defaultValue = ImageRule.None)
            => Enum.TryParse(Rule, true, out ImageRule result) ? result : defaultValue;


        /// <summary>
        ///  Convert a string to a single LayoutPosition
        /// </summary>
        /// <param name="Position"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected LayoutPosition StringToPosition(string Position, LayoutPosition defaultValue = LayoutPosition.None)
            => Enum.TryParse(Position, true, out LayoutPosition result) ? result : defaultValue;

        /// <summary>
        /// Convert a semi-colon delimited string to a Flag collection of LayoutPositions
        /// </summary>
        /// <param name="Position"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        protected LayoutPosition StringToPositions(string Positions, LayoutPosition defaultValue = LayoutPosition.None)
        {
            LayoutPosition result = LayoutPosition.None;

            foreach (string lp in Positions.Split(";").ToList())
            {
                if (Enum.TryParse(Positions, true, out LayoutPosition innerresult))
                    result |= innerresult;
            }

            return result;
        }

        public static decimal? Max(decimal? A, decimal? B)
        {
            return !(A.HasValue && B.HasValue) ? (decimal?)null : Math.Max(A.Value, B.Value);
        }

        protected decimal? Min(decimal? A, decimal? B)
        {
            return !(A.HasValue && B.HasValue) ? (decimal?)null : Math.Min(A.Value, B.Value);
        }

        protected decimal? Ceiling(decimal? A)
        {
            return (A.HasValue) ? Math.Ceiling(A.Value) : (decimal?)null;
        }

        protected decimal? Floor(decimal? A)
        {
            return (A.HasValue) ? Math.Floor(A.Value) : (decimal?)null;
        }

        protected bool? OR(bool? A, bool? B)
        {
            return (A.HasValue && B.HasValue) ? (A.Value || B.Value) : (bool?)null;
        }

        protected bool? AND(bool? A, bool? B)
        {
            return (A.HasValue && B.HasValue) ? (A.Value && B.Value) : (bool?)null;
        }

        #endregion Helper Functions

    }

}
