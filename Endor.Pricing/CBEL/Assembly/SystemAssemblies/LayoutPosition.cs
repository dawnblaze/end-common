﻿using System;

namespace Endor.CBEL.Elements
{
    /// <summary>
    /// This is an internal enum used by the Layout Computing Assembly.
    /// </summary>
    [Flags]
    public enum LayoutPosition
    {
        None = 0,
        Top = 1,
        Bottom = 2,
        Left = 4,
        Right = 8,
        Horizontal = 16,
        Vertical = 32,
        Front = 64,
        Back = 128
    };

}
