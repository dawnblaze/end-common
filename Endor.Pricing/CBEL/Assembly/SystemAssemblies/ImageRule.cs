﻿namespace Endor.CBEL.Elements
{
    public enum ImageRule
    {
        None = 0,
        Duplicate = 1,
        Unique = 2,
        NextItem = 3
    }
}
