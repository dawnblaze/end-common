﻿using System;

namespace Endor.CBEL.Metadata
{
    [Flags]
    public enum CBELMemberObjectType
    {
        Scalar = 1,
        Object = 2,
        Array = 4, 
        Variable = 8,
    }
}