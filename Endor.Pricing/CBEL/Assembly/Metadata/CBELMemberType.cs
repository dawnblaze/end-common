﻿using System;

namespace Endor.CBEL.Metadata
{
    [Flags]
    public enum CBELMemberType
    {
        Property = 1,
        Function = 2,
        Keyword = 64,
        ReservedWord = 128
    }
} 