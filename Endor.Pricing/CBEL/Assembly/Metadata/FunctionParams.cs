﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Metadata
{
    public class FunctionParams
    {
        public FunctionParams() { }
        public string Name { get; set; }
        public string MemberType { get; set; }
        public bool Required { get; set; }
        public string Hint { get; set; }
    }
}
