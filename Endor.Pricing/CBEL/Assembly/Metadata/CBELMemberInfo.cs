﻿namespace Endor.CBEL.Metadata
{
    public class CBELMemberInfo
    {
        public string Name { get; set; }
        public string InsertedText { get; set; }
        public int DataType { get; set; }
        public string Hint { get; set; }
        public string Description { get; set; }
        public string LongDescription { get; set; }
        public virtual string MemberType => "Member";
    }
}
