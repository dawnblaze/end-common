﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Metadata
{
    /// <summary>
    /// This interface is used to document the members that can be used in a CBEL formula.
    /// </summary>
    public interface ICBELMember
    {
        /// <summary>
        /// The name of the member
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The Type of Member
        ///     Property = 1,
        ///     Function = 2,
        ///     Keyword = 64,
        ///     ReservedWord = 128
        /// </summary>
        CBELMemberType MemberType { get; }

        /// <summary>
        /// If the type is a Property or a function, this enum gives characteristics of the property. 
        /// It is a FLAG enum as multiple characteristics may apply.
        ///         Scalar = 1,
        ///         Object = 2,
        ///         Array = 4, 
        ///         Variable = 8,
        /// </summary>
        CBELMemberObjectType? MemberObjectType { get; }

        /// <summary>
        /// The DataType that the Member returns.
        /// </summary>
        DataType DataType { get; }

        /// <summary>
        /// The Class the a method belongs to.
        /// </summary>
        string MethodClass { get; set; }

        /// <summary>
        /// A list of the child member for object references.  This is null if the member is not an object type.
        /// </summary>
        SortedDictionary<string, ICBELMember> Members { get; set; }

    }
}
