﻿namespace Endor.CBEL.Metadata
{
    public class CBELFunctionInfo : CBELMemberInfo
    {
        public override string MemberType => "Function";
        public int MinParams { get; set; }
        public int MaxParams { get; set; }
        public CBELFunctionParamInfo[] Params { get; set; }

    }
}
