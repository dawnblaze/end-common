﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Endor.Models;
using Endor.CBEL.Common;
using System.Runtime.CompilerServices;
using Endor.CBEL.Elements;
using Endor.CBEL.Interfaces;
using Endor.Units;

namespace Endor.CBEL.Metadata
{
    public static class MetadataHelper
    {
        public static DataType GetDataTypeFromSystemType(Type returnType)
        {
            string TypeName = returnType.ToString().ToLower();
            if (TypeName.Contains("decimal"))
            {
                return DataType.Number;
            }
            if (TypeName.Contains("double"))
            {
                return DataType.Number;
            }
            if (TypeName.Contains("string"))
            {
                return DataType.String;
            }
            if (TypeName.Contains("bool"))
            {
                return DataType.Boolean;
            }
            if (TypeName.Contains("datetime"))
            {
                return DataType.DateTime;
            }
            return DataType.AnyObjectScalar;
        }

        /// <summary>
        /// Fills the memberList with the CBEL (Excel) functions
        /// </summary>
        /// <param name="memberList"></param>
        private static void FillCBELFunctions(SortedDictionary<string, ICBELMember> memberList)
        {
            Type[] types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes();
            foreach (Type type in types)
            {
                if (type.Name != nameof(ExcelFunctions) && type.Name != nameof(CBELFunctions)) continue;

                MethodInfo[] methodInfos = type.GetMethods(BindingFlags.Public | BindingFlags.Static);
                foreach (MethodInfo method in methodInfos)
                {
                    if (!memberList.ContainsKey(method.Name.ToLower()))
                        memberList.Add(method.Name.ToLower(), new CBELMember(method));
                }
            }
        }

        /// <summary>
        /// Populates the child variable members of the known member list of the Assembly Data
        /// </summary>
        /// <param name="memberList">Know members list to be populated</param>
        /// <param name="assemblyData">Assembly Data</param>
        private static void FillVariables(SortedDictionary<string, ICBELMember> memberList, AssemblyData assemblyData)
        {
            // End if Variables are null
            if (assemblyData?.Variables == null || !assemblyData.Variables.Any()) return;
            ICollection<AssemblyVariable> assemblyVariables = assemblyData.Variables;

            //Variables
            foreach (AssemblyVariable variable in assemblyVariables)
            {
                // Exclude if DataType is DataType.None or Variable already existed
                if (variable.DataType == DataType.None || memberList.ContainsKey(variable.Name.ToLower())) continue;

                // Check variable if it is Scalar or Object
                bool isScalar = (short) variable.DataType < 1000 && (short) variable.DataType > 0;
                ICBELMember member = new CBELMember
                {
                    DataType = variable.DataType,
                    MemberObjectType = isScalar
                        ? (int) CBELMemberObjectType.Variable + CBELMemberObjectType.Scalar
                        : (int) CBELMemberObjectType.Variable + CBELMemberObjectType.Object,
                    MemberType = CBELMemberType.Property,
                    Name = variable.Name
                };

                FillMembers(member, variable.ElementType);

                memberList.Add(member.Name.ToLower(), member);
            }
        }

        /// <summary>
        /// Fill parent members with child elements
        /// </summary>
        /// <param name="parent">Parent Variable/Element</param>
        /// <param name="elementType"></param>
        private static void FillMembers(ICBELMember parent, AssemblyElementType elementType)
        {
            Type elementClassType = null;

            switch (elementType)
            {
                case AssemblyElementType.SingleLineText:
                case AssemblyElementType.MultiLineString:
                    elementClassType = typeof(StringVariable);
                    break;

                case AssemblyElementType.DropDown:
                    elementClassType = typeof(ConsumptionStringVariable);
                    break;

                case AssemblyElementType.Number:
                    elementClassType = typeof(NumberVariable);
                    break;

                case AssemblyElementType.Checkbox:
                    elementClassType = typeof(BooleanVariable);
                    break;

                case AssemblyElementType.LinkedMaterial:
                    elementClassType = typeof(LinkedMaterialVariable);
                    break;

                case AssemblyElementType.LinkedLabor:
                    elementClassType = typeof(LinkedLaborVariable);
                    break;

                case AssemblyElementType.LinkedAssembly:
                    elementClassType = typeof(LinkedAssemblyVariable);
                    break;

                case AssemblyElementType.LinkedMachine:
                    elementClassType = typeof(LinkedMachineVariable);
                    break;

                case AssemblyElementType.Group:
                    break;
                case AssemblyElementType.SingleLineLabel:
                    break;
                case AssemblyElementType.MultiLineLabel:
                    break;
                case AssemblyElementType.UrlLabel:
                    break;
                case AssemblyElementType.Spacer:
                    break;
                case AssemblyElementType.ShowHideGroup:
                    break;
                case AssemblyElementType.SplitColumn:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(elementType), elementType, null);
            }

            if (elementClassType == null) return;

            FillMembers(parent, elementClassType);
        }

        /// <summary>
        /// Fill parent members with child elements
        /// </summary>
        /// <param name="parent">Parent Variable/Element</param>
        /// <param name="elementType"></param>
        private static void FillMembers(ICBELMember parent, Type elementType)
        {
            PropertyInfo[] properties = elementType.GetProperties();

            MethodInfo[] specialMethods = null;

            if (elementType.GetInterfaces().Contains(typeof(IMeasurement)))
                specialMethods = typeof(MeasurementExtension).GetMethods().Where(x => x.IsStatic && x.GetParameters().Length == 1).ToArray();

            if (properties == null && specialMethods == null) return;
            if (parent.Members == null) parent.Members = new SortedDictionary<string, ICBELMember>();

            if (properties != null)
            {
                foreach (PropertyInfo property in properties)
                {
                    if (!parent.Members.ContainsKey(property.Name.ToLower()))
                    {
                        CBELMember newMember = new CBELMember(property);
                        parent.Members.Add(property.Name.ToLower(), newMember);

                        if (property.PropertyType.GetInterfaces().Contains(typeof(ICBELVariable))
                            || property.PropertyType.GetInterfaces().Contains(typeof(IMeasurement)))
                        {
                            FillMembers(newMember, property.PropertyType);
                        }
                    }
                }
            }

            if (specialMethods != null)
            {
                foreach (MethodInfo method in specialMethods)
                {
                    if (!parent.Members.ContainsKey(method.Name.ToLower()))
                        parent.Members.Add(method.Name.ToLower(), new CBELMember(method));
                }
            }
        }

        /// <summary>
        /// Adds the properties of CBELAssemblyBase to the known member list
        /// </summary>
        /// <param name="memberList">The top level member list</param>
        private static void FillAssemblyProperties(SortedDictionary<string, ICBELMember> memberList, AssemblyType? assemblyType)
        {
            Type t;
            if (assemblyType.GetValueOrDefault(AssemblyType.Product) == AssemblyType.MachineTemplate)
                t = Type.GetType($"Endor.CBEL.Elements.{nameof(CBELMachineBase)}");
            else
                t = Type.GetType($"Endor.CBEL.Elements.{nameof(CBELAssemblyBase)}");

            PropertyInfo[] properties = t.GetProperties();

            if (properties == null) return;
            foreach (PropertyInfo property in properties)
            {
                if (memberList.ContainsKey(property.Name.ToLower())) continue;
                CBELMember _property = new CBELMember(property);

                PropertyInfo[] childProperties = GetPublicProperties(property.PropertyType);
                if (childProperties.Any())
                {
                    _property.Members = new SortedDictionary<string, ICBELMember>();
                    foreach (var childProperty in childProperties)
                    {
                        if (!memberList.ContainsKey(childProperty.Name.ToLower()))
                            _property.Members.Add(childProperty.Name.ToLower(), new CBELMember(childProperty));
                    }
                }

                memberList.Add(property.Name.ToLower(), _property);
            }
        }

        /// <summary>
        /// Adds the functions of CBELAssemblyBase to the known member list
        /// </summary>
        /// <param name="memberList">The top level member list</param>
        private static void FillAssemblyMethods(SortedDictionary<string, ICBELMember> memberList)
        {
            Type t = Type.GetType("Endor.CBEL.Elements.CBELAssemblyBase");
            MethodInfo[] methods = t.GetMethods().Where(m => !m.IsSpecialName).ToArray();

            if (methods == null) return;
            foreach (MethodInfo method in methods)
            {
                if (memberList.ContainsKey(method.Name.ToLower())) continue;
                CBELMember _method = new CBELMember(method);
                memberList.Add(method.Name.ToLower(), _method);
            }
        }

        /// <summary>
        /// Helper method to get the public properties of a type and its inherited properties
        /// </summary>
        /// <param name="type">CBELAssemblyBase property</param>
        /// <returns></returns>
        private static PropertyInfo[] GetPublicProperties(Type type)
        {
            if (type.IsInterface)
            {
                var propertyInfos = new List<PropertyInfo>();

                var considered = new List<Type>();
                var queue = new Queue<Type>();
                considered.Add(type);
                queue.Enqueue(type);
                while (queue.Count > 0)
                {
                    var subType = queue.Dequeue();
                    foreach (var subInterface in subType.GetInterfaces())
                    {
                        if (considered.Contains(subInterface)) continue;

                        considered.Add(subInterface);
                        queue.Enqueue(subInterface);
                    }

                    var typeProperties = subType.GetProperties(
                        BindingFlags.FlattenHierarchy
                        | BindingFlags.Public
                        | BindingFlags.Instance);

                    var newPropertyInfos = typeProperties
                        .Where(x => !propertyInfos.Contains(x));

                    propertyInfos.InsertRange(0, newPropertyInfos);
                }

                return propertyInfos.ToArray();
            }

            return type.GetProperties(BindingFlags.FlattenHierarchy
                                      | BindingFlags.Public | BindingFlags.Instance);
        }

        static IEnumerable<MethodInfo> GetExtensionMethods(System.Reflection.Assembly assembly, Type extendedType)
        {
            var query = from type in assembly.GetTypes()
                        from method in type.GetMethods()
                        where method.IsDefined(typeof(ExtensionAttribute), false)
                        where method.GetParameters()[0].ParameterType == extendedType
                        select method;
            return query;
        }

        /// <summary>
        /// <param name="memberList"></param>
        /// </summary>
        private static void FillAssemblyExtensionMethods(SortedDictionary<string, ICBELMember> memberList)
        {

            Type t = Type.GetType("Endor.CBEL.Interfaces.ICBELAssembly");
            System.Reflection.Assembly thisAssembly = t.Assembly;
            IList<MethodInfo> methods = new List<MethodInfo>();
            foreach (MethodInfo method in GetExtensionMethods(thisAssembly, t))
            {
                Console.WriteLine(method);
                //FOR TESTING: 
                //ParameterInfo[] p = method.GetParameters();
                methods.Add(method);
            }

            if (methods != null)
            {
                foreach (MethodInfo method in methods)
                {
                    if (memberList.ContainsKey(method.Name.ToLower()))
                        continue;

                    CBELMember cm = new CBELMember();
                    cm.Name = method.Name;
                    cm.MemberType = CBELMemberType.Function;
                    cm.MemberObjectType = CBELMemberObjectType.Object;
                    cm.DataType = GetDataTypeFromSystemType(method.GetType());
                    cm.Members = new SortedDictionary<string, ICBELMember>();
                    //Create and set a property to identify Extension Methods
                    CBELMember extensionProperty = new CBELMember();
                    extensionProperty.Name = "ExtensionAttribute";
                    extensionProperty.MemberType = CBELMemberType.Property;
                    
                    cm.Members.Add("extensionAttribute", extensionProperty);
                    memberList.Add(method.Name.ToLower(), cm);
                }
            }
        }

        /// <summary>
        /// TODO: Add summary
        /// </summary>
        private static void FillCSReservedWords(SortedDictionary<string, ICBELMember> memberList)
        {
            Type t = Type.GetType("Endor.CBEL.ReserveWords.CBELReservedWords");
            ConstructorInfo reservedConstructor = t.GetConstructor(Type.EmptyTypes);
            object classObject = reservedConstructor.Invoke(new object[] { });
            MethodInfo methodInfo = t.GetMethod("reservedWords");
            List<string> reservedWords = (List<string>)methodInfo.Invoke(classObject, new object[] { });
            if (!reservedWords.Any()) return;
            foreach (var reservedWord in reservedWords)
            {
                if (!memberList.ContainsKey(reservedWord.ToLower()))
                    memberList.Add(reservedWord.ToLower(), new CBELMember(reservedWord));
            }
        }

        /// <summary>
        /// Retrieves the member list of the assembly used for code-complete and case-correction.  If the ID is not specifid, the method returns the list of all Members on a generic assembly (without any variables).
        /// </summary>
        /// <param name="assemblyData"></param>
        /// <returns></returns>
        public static SortedDictionary<string, ICBELMember> KnownMemberList(AssemblyData assemblyData = null)
        {
            SortedDictionary<string, ICBELMember> memberList = new SortedDictionary<string, ICBELMember>();

            FillCBELFunctions(memberList);
            FillVariables(memberList, assemblyData);
            FillAssemblyProperties(memberList, assemblyData?.AssemblyType);
            FillAssemblyMethods(memberList);
            FillAssemblyExtensionMethods(memberList);
            FillCSReservedWords(memberList);

            return memberList;
        }

    }
}
