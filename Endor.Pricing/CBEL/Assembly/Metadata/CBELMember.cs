﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Endor.Models;

namespace Endor.CBEL.Metadata
{
    /// <summary>
    /// This interface is used to document the members that can be used in a CBEL formula.
    /// </summary>
    public class CBELMember : ICBELMember
    {
        public CBELMember(){}

        public CBELMember(PropertyInfo propertyInfo)
        {
            Name = propertyInfo.Name;
            MemberType = CBELMemberType.Property;
            MemberObjectType = CBELMemberObjectType.Object;
            DataType = MetadataHelper.GetDataTypeFromSystemType(propertyInfo.GetType());
            MethodClass = null;
        }

        public CBELMember(MethodInfo methodInfo)
        {
            Name = methodInfo.Name;
            MemberType = CBELMemberType.Function;
            MemberObjectType = CBELMemberObjectType.Scalar;
            DataType = MetadataHelper.GetDataTypeFromSystemType(methodInfo.ReturnType);
            MethodClass = methodInfo.DeclaringType.Name;
        }

        public CBELMember(string reservedWord)
        {
            Name = reservedWord;
            MemberType = CBELMemberType.ReservedWord;
            MemberObjectType = CBELMemberObjectType.Scalar;
            DataType = MetadataHelper.GetDataTypeFromSystemType(reservedWord.GetType());
            MethodClass = null;
        }

        /// <summary>
        /// The name of the member
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Type of Member
        ///     Property = 1,
        ///     Function = 2,
        ///     Keyword = 64,
        ///     ReservedWord = 128
        /// </summary>
        public CBELMemberType MemberType { get; set; }

        /// <summary>
        /// If the type is a Property or Function, this enum gives characteristics of the property. 
        /// It is a FLAG enum as multiple characteristics may apply.
        ///         Scalar = 1,
        ///         Object = 2,
        ///         Array = 4, 
        ///         Variable = 8,
        /// </summary>
        public CBELMemberObjectType? MemberObjectType { get; set; }

        /// <summary>
        /// The DataType that the Member returns.
        /// </summary>
        public DataType DataType { get; set; }

        /// <summary>
        /// The Class the a method belongs to.
        /// </summary>
        public string MethodClass { get; set; }

        /// <summary>
        /// A list of the child member for object references.  This is null if the member is not an object type.
        /// </summary>
        public SortedDictionary<string, ICBELMember> Members { get; set; }
    }
}
