﻿namespace Endor.CBEL.Metadata
{
    public class CBELFunctionParamInfo : CBELMemberInfo
    {
        public override string MemberType => "Parameter";
        public bool Required { get; set; }

    }
}
