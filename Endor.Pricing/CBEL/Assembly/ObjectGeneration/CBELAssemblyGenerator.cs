﻿using Endor.CBEL.Common;
using Endor.Models;
using Endor.CBEL.Interfaces;
using Endor.EF;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Immutable;
using Endor.Tenant;
using Endor.Units;

namespace Endor.CBEL.ObjectGeneration
{
    /// <summary>
    /// This class is used to emit an assembly pricing object
    /// </summary>
    public partial class CBELAssemblyGenerator : CBELBaseAssemblyGenerator
    {
        public CBELAssemblyGenerator(short bid, int id, int classtypeid, string name, int version) 
            : base(bid, id, classtypeid, name, version)
        {
        }

        public override void CheckDefaultVariables()
        {
            // If TotalQuantity is not Defined Add it
            if (!_Variables.Exists(x => x.Name == "TotalQuantity"))
                _Variables.Add(new CBELElementGenerator(this)
                {
                    VariableClassName = "NumberEditElement",
                    VariableDataType = Models.DataType.Number,
                    Name = "TotalQuantity",
                    FormulaText = "= IF(TotalQuantityOV, 0, AssemblyQuantity.Value * LineItemQuantity)",
                    IsRequired = true,
                    Label = "Total Quantity",
                    DefaultValueConstant = "1",
                    UnitType = UnitType.Discrete,
                    UnitID = Unit.Each
                });

            // If AssemblyQuantity is not Defined Add it
            if (!_Variables.Exists(x => x.Name == "AssemblyQuantity"))
                _Variables.Add(new CBELElementGenerator(this)
                {
                    VariableClassName = "NumberEditElement",
                    VariableDataType = Models.DataType.Number,
                    Name = "AssemblyQuantity",
                    FormulaText = "= IF( AssemblyQuantityOV, 0, TotalQuantity.Value / IF(LineItemQuantity = 0, 1, LineItemQuantity) )",
                    IsRequired = true,
                    Label = "Assembly Quantity",
                    DefaultValueConstant = "1",
                    UnitType = UnitType.Discrete,
                    UnitID = Unit.Each
                });

            // If AssemblyPrice is not Defined Add it
            if (!_Variables.Exists(x => x.Name == "Price"))
                _Variables.Add(new CBELElementGenerator(this)
                {
                    VariableClassName = "NumberEditElement",
                    VariableDataType = Models.DataType.Number,
                    Name = "Price",
                    FormulaText = null,
                    IsRequired = false,
                    Label = "Price",
                });

        }

        protected override void AddDeclareVariablesCode(CodeBuilder Results)
        {
            _Variables.ForEach(x => x.AddDeclareVariableCode(Results));
            //_Variables.ForEach(x => x.OverrideDefaultVariables(Results));
        }
        protected override string CBELAssemblyBaseClass => "CBELAssemblyBase";

        protected override string AssemblyTypeName => "Assembly";
    }
}