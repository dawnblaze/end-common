﻿using Endor.CBEL.Common;
using System.Text;

namespace Endor.CBEL.ObjectGeneration
{
    /// <summary>
    /// This class is used to emit an element pricing object
    /// </summary>
    public class CBELElementGenerator : CBELVariableGenerator
    {
        /// <summary>
        /// The default constructor.
        /// </summary>
        public CBELElementGenerator(CBELBaseAssemblyGenerator assemblyGenerator) : base(assemblyGenerator) { }
    }
}
