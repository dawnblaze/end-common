﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.CBEL.Elements;
using Endor.CBEL.Grammar;
using Endor.CBEL.Interfaces;
using Endor.CBEL.Metadata;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tenant;
using Endor.Units;
using Irony.Parsing;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.EntityFrameworkCore;

namespace Endor.CBEL.ObjectGeneration
{
    public abstract partial class CBELBaseAssemblyGenerator
    {
        protected const string TierTableVariableName = "TierTable";
        public const string NoDefaultUnitErrorMessage = "No Default Unit:";

        public async Task<AssemblyCompilationResponse> Save(ITenantDataCache tenantData)
        {
            // Before we go deleting anything, lets make sure the new thing compiles;
            AssemblyCompilationResponse compileTestResult = await TestAssemblyCompilation();
            if (!compileTestResult.Success)
            {
                if (Errors.Count == 0)
                    Errors.AddRange(compileTestResult.Errors);
                return compileTestResult;
            }

            EntityStorageClient client = new EntityStorageClient((await tenantData.Get(BID)).StorageConnectionString, BID);
            var assemblyDoc = new DMID() { id = ID, ctid = ClassTypeID };

            //Delete existing
            await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{AssemblyClassName}.cs");
            await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{AssemblyClassName}.dll");


            //Save CS
            string assemblyCode = compileTestResult.AssemblyCode;

            using (MemoryStream assemblyCodeStream = new MemoryStream())
            using (StreamWriter sw = new StreamWriter(assemblyCodeStream))
            {
                sw.Write(assemblyCode);
                sw.Flush();

                await client.AddFile(assemblyCodeStream, StorageBin.Permanent, Bucket.Data, assemblyDoc,
                    $"source/{AssemblyClassName}.cs", "text/plain", null, null);
            }

            using (MemoryStream assemblyDLLStream = new MemoryStream())
            { 
                Compilation compilation = compileTestResult.Script.GetCompilation();
                compilation.Emit(assemblyDLLStream);
                if (assemblyDLLStream == null)
                {
                    compileTestResult.Errors.Add(
                        new PricingErrorMessage()
                        {
                            ErrorType = "Error",
                            Message = "DLL failed to generate."
                        });
                }
                else
                {
                    await client.AddFile(assemblyDLLStream, StorageBin.Permanent, Bucket.Data, assemblyDoc,
                        $"bin/{AssemblyClassName}.dll", "application/x-msdownload", null, null);
                }
            }
            //Save DLL
            return compileTestResult;
        }

        public async Task<ICBELAssembly> GetAssemblyInstance(ApiContext ctx, ITenantDataCache cache, RemoteLogger logger, Type assemblyType = null)
        {
            if (assemblyType == null)
            {
                AssemblyCompilationResponse scriptResult = await InternalGetAssemblyScript();

                if (scriptResult == null)
                    return null;

                if (!scriptResult.Success)
                {
                    if (Errors.Count == 0)
                        Errors.AddRange(scriptResult.Errors);
                    return null;
                }

                assemblyType = scriptResult.AssemblyType;
            }

            ICBELAssembly result = (ICBELAssembly)Activator.CreateInstance(assemblyType, ctx, BID, cache, logger);
            return result;
        }

        public async Task<AssemblyCompilationResponse> TestAssemblyCompilation()
        {
            AssemblyCompilationResponse scriptResponse = await InternalGetAssemblyScript();

            if (scriptResponse == null)
                return new AssemblyCompilationResponse(Name)
                {
                    Errors = new List<PricingErrorMessage>() { new PricingErrorMessage() { ErrorType = "Error", Message = "No assembly returned" } },
                    ExternalDependencies = scriptResponse.ExternalDependencies,
                };

            return scriptResponse;
        }

        public static ScriptOptions scriptOptions = ScriptOptions.Default.WithReferences(System.Reflection.Assembly.GetExecutingAssembly(), typeof(CBELAssemblyGenerator).Assembly, typeof(Models.AssemblyElementType).Assembly);
        
        private async Task<AssemblyCompilationResponse> InternalGetAssemblyScript()
        {
            AssemblyCompilationResponse result = new AssemblyCompilationResponse(Name);

            if (Errors.Count > 0)
            {
                result.Script = null;
                result.Errors = new List<PricingErrorMessage>(Errors);
                result.ExternalDependencies = _ExternalDependencies;
            }

            else
            {
                result.AssemblyCode = GetAssemblyCode();
                string assemblyCode = $"{result.AssemblyCode}{Common.Constants.CR}return typeof({AssemblyClassName});";

                CBELAssemblyHelper.MakeLocalCopy(this, assemblyCode);

                Script<Type> script = CSharpScript.Create<Type>(assemblyCode, scriptOptions);
                
                ImmutableArray<Diagnostic> compileResults = script.Compile();
                result.Script = script;

                if (compileResults.Length == 0)
                    result.AssemblyType = (await script.RunAsync()).ReturnValue;// scriptRunner();
                else
                {
                    result.Script = null;
                    result.Errors = PricingErrorMessage.FromDiagnostics(compileResults, Variables);
                    result.ExternalDependencies = _ExternalDependencies;
                }

                GC.Collect();
            }

            return result;
        }

        protected void FillVariables(ApiContext context, AssemblyData assemblyData, Parser parser)
        {
            if (assemblyData.Variables != null)
            {
                foreach (var variable in assemblyData.Variables)
                {
                    if (!CSharpIdentifiers.IsValidParsedIdentifier(variable.Name))
                    {
                        Errors.Add(
                            new PricingErrorMessage()
                            {
                                ErrorType = "Error",
                                VariableName = variable.Name,
                                Message = "Invalid variable name"
                            }
                            );
                        continue;
                    }

                    switch (variable.ElementType)
                    {
                        case AssemblyElementType.Group:
                        case AssemblyElementType.Spacer:
                        case AssemblyElementType.SplitColumn:
                            // These should not be added as elements
                            break;
                        case AssemblyElementType.SingleLineLabel:
                            AddElementGenerator<SingleLineLabelElement>(variable, parser, DataType.String);
                            break;
                        case AssemblyElementType.MultiLineLabel:
                            AddElementGenerator<MultiLineLabelElement>(variable, parser, DataType.String);
                            break;
                        case AssemblyElementType.UrlLabel:
                            AddElementGenerator<URLLabelElement>(variable, parser, DataType.String);
                            break;
                        case AssemblyElementType.SingleLineText:
                            AddElementGenerator<SingleLineTextElement>(variable, parser, DataType.String);
                            break;
                        case AssemblyElementType.MultiLineString:
                            AddElementGenerator<MultiLineTextElement>(variable, parser, DataType.String);
                            break;
                        case AssemblyElementType.DropDown:
                            AddDropdownElementGenerator(context, variable, parser);
                            break;
                        case AssemblyElementType.Number:
                            AddElementGenerator<NumberEditElement>(variable, parser, DataType.Number);
                            break;
                        case AssemblyElementType.Checkbox:
                            AddElementGenerator<CheckboxElement>(variable, parser, DataType.Boolean);
                            break;
                        case AssemblyElementType.ShowHideGroup:
                            AddElementGenerator<ShowHideGroupElement>(variable, parser, DataType.Boolean);
                            break;
                        case AssemblyElementType.LinkedMaterial:
                            AddComponentGenerator<LinkedMaterialVariable>(context, variable, parser, DataType.MaterialPart);
                            break;
                        case AssemblyElementType.LinkedLabor:
                            AddComponentGenerator<LinkedLaborVariable>(context, variable, parser, DataType.LaborPart);
                            break;
                        case AssemblyElementType.LinkedAssembly:
                            AddComponentGenerator<LinkedAssemblyVariable>(context, variable, parser, DataType.Assembly, true);
                            break;
                        case AssemblyElementType.LinkedMachine:
                            AddComponentGenerator<LinkedMachineVariable>(context, variable, parser, DataType.MachinePart, true);
                            break;
                        case AssemblyElementType.LayoutVisualizer:
                        case AssemblyElementType.LayoutComputationAssembly:
                            AddComponentGenerator<LayoutComputationAssemblyVariable>(context, variable, parser, DataType.Assembly);
                            break;
                    }
                }
            }
        }

        protected void FillTables(AssemblyData assemblyData)
        {
            if (assemblyData.Tables != null)
            {
                foreach (var table in assemblyData.Tables)
                {
                    if (table.IsTierTable)
                        AddTableGenerator(assemblyData.Variables, table);

                    else
                    {
                        switch (table.TableType)
                        {
                            case AssemblyTableType.Custom:
                                AddTableGenerator(assemblyData.Variables, table);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        private T_Generator CreateGenerator<T_Generator, T_Variable>(AssemblyVariable variable, Parser parser, DataType? variableDataType = null)
            where T_Generator : CBELVariableGenerator
            where T_Variable : ICBELVariable
        {
            T_Generator generator = (T_Generator)Activator.CreateInstance(typeof(T_Generator), this);
            generator.Name = variable.Name;
            generator.VariableClassName = typeof(T_Variable).Name;
            generator.Label = variable.Label;
            generator.VariableDataType = variableDataType.GetValueOrDefault(DataType.None);
            generator.IsRequired = variable.IsRequired;
            generator.UnitID = variable.UnitID;
            generator.UnitType = variable.UnitType;
            generator.LinkedAssemblyID = variable.LinkedAssemblyID;
            generator.LinkedLaborID = variable.LinkedLaborID;
            generator.LinkedMaterialID = variable.LinkedMaterialID;
            generator.LinkedMachineID = variable.LinkedMachineID;
            generator.InclusionFormula = variable.InclusionFormula;            

            generator.SetFormulaOrConstantValue(this, variable, parser);
            generator.SetAltFormulaOrConstantValue(this, variable, parser);

            return generator;
        }

        public CBELElementGenerator AddElementGenerator<T>(AssemblyVariable variable, Parser parser, DataType? variableDataType = null)
            where T : ICBELVariable
        {
            CBELElementGenerator elementGenerator = CreateGenerator<CBELElementGenerator, T>(variable, parser, variableDataType);

            AddVariable(elementGenerator);

            return elementGenerator;
        }

        public CBELVariableGenerator AddDropdownElementGenerator(ApiContext context, AssemblyVariable variable, Parser parser)
        {
            switch (variable.ListDataType)
            {
                case DataType.MaterialPart:
                    return AddComponentGenerator<DropDownMaterialListElement>(context, variable, parser, DataType.MaterialPart);
                case DataType.LaborPart:
                    return AddComponentGenerator<DropDownLaborListElement>(context, variable, parser, DataType.LaborPart);
                case DataType.String:
                    return AddElementGenerator<DropDownStringListElement>(variable, parser, variable.DataType);
                case DataType.Number:
                    return AddElementGenerator<DropDownNumberListElement>(variable, parser, variable.DataType);
                default:
                    Errors.Add(
                        new PricingErrorMessage()
                        {
                            ErrorType = "Error",
                            VariableName = variable.Name,
                            Message = "Variable is not of a valid dropdown type",
                        }
                        );
                    return null;
            }
        }

        public CBELComponentGenerator AddComponentGenerator<T>(ApiContext context, AssemblyVariable variable, Parser parser, DataType? variableDataType = null, bool useDynamicType = false)
            where T : ICBELVariable
        {
            CBELComponentGenerator generator = CreateGenerator<CBELComponentGenerator, T>(variable, parser, variableDataType);

            generator.UseDynamicType = useDynamicType;

            if (variable.LinkedAssemblyID.HasValue)
                generator.RollupLinkedPriceAndCost = variable.RollupLinkedPriceAndCost;

            generator.SetConsumptionFormulaOrConstantValue(this, variable, parser);
            generator.SetInclusionFormulaOrConstantValue(this, variable, parser);

            if (variable.Formulas != null)
            {
                generator.Formulas = new List<ICBELVariableFormula>();

                AssemblyData linkedAssembly;
                if (context == null)
                    linkedAssembly = null;
                else if (variable.LinkedAssemblyID.HasValue)
                    linkedAssembly = context.AssemblyData
                                            .Include(x => x.Variables)
                                            .FirstOrDefault(x => x.BID == BID && x.ID == variable.LinkedAssemblyID);
                else if (variable.LinkedMachineID.HasValue)
                {
                    MachineData linkedMachine = context.MachineData
                                                       .Include(x => x.MachineTemplate)
                                                       .ThenInclude(x => x.Variables)
                                                       .FirstOrDefault(x => x.BID == BID && x.ID == variable.LinkedMachineID);
                    linkedAssembly = linkedMachine?.MachineTemplate;
                }
                else
                    linkedAssembly = null;

                string fxText;
                Unit? varUnit;
                AssemblyVariable linkedVariable;

                foreach (AssemblyVariableFormula formula in variable.Formulas)
                {
                    if (linkedAssembly != null)
                        linkedVariable = linkedAssembly.Variables.FirstOrDefault(x => x.Name == formula.ChildVariableName);
                    else
                        linkedVariable = null;

                    varUnit = linkedVariable?.UnitID;

                    if (string.IsNullOrWhiteSpace(formula.FormulaText))
                        fxText = null;
                    else if (formula.FormulaEvalType != AssemblyFormulaEvalType.MappedVariable)
                        fxText = formula.FormulaText;
                    else
                    {
                        string convertProperty = "Value";

                        if (varUnit.HasValue)
                        {
                            UnitInfo unitInfo = UnitInfo.UnitInfoByUnit(varUnit.Value);

                            if (unitInfo != null
                                && unitInfo.UnitType != UnitType.Custom
                                && unitInfo.UnitType != UnitType.Discrete
                                && unitInfo.UnitType != UnitType.General
                                )
                            {
                                convertProperty = $"In{unitInfo.NamePluralUS}";
                            }
                        }

                        fxText = $"={formula.FormulaText}.{convertProperty}";
                    }

                    CBELVariableFormula varFormula = new CBELVariableFormula()
                    {
                        ID = formula.ID,
                        ChildVariableName = formula.ChildVariableName,
                        DataType = formula.DataType,
                        FormulaText = fxText,
                        FormulaEvalType = formula.FormulaEvalType,
                        FormulaUseType = formula.FormulaUseType
                    };

                    SetLinkedFormula(variable, varFormula, parser);

                    generator.Formulas.Add(varFormula);
                }
            }

            AddVariable(generator);

            return generator;
        }

        public CBELTableGenerator AddTableGenerator(ICollection<AssemblyVariable> variables, AssemblyTable table, MachineProfileTable profileTable = null)
        {
            if (!CSharpIdentifiers.IsValidParsedIdentifier(table.VariableName))
            {
                Errors.Add(
                    new PricingErrorMessage()
                    {
                        ErrorType = "Error",
                        VariableName = table.VariableName,
                        Message = "Invalid table variable name"
                    }
                    );

                return null;
            }

            //get table data
            DataType tableDataType = table.CellDataType;
            string tableDataTypeStr = tableDataType.ToString();

            AssemblyVariable rowVariable = null;
            string rowDataTypeStr = "string";

            if (table.RowVariableID.HasValue)
            {
                rowVariable = variables.FirstOrDefault(t => t.BID == table.BID && t.ID == table.RowVariableID);
                if (rowVariable == null)
                {
                    Errors.Add(
                        new PricingErrorMessage()
                        {
                            ErrorType = "Error",
                            VariableName = table.VariableName,
                            Message = "Specified row variable does not exist on the assembly",
                        }
                        );
                    return null;
                }

                DataType rowDataType = table.RowDataType;
                switch (rowDataType)
                {
                    case (DataType.Boolean):
                        rowDataTypeStr = "bool?";
                        break;
                    case (DataType.Number):
                        rowDataTypeStr = "decimal?";
                        break;
                }
            }

            else
            {
                if (table.IsTierTable)
                {
                    rowVariable = variables.FirstOrDefault(t => t.BID == table.BID && t.Name == "Tier");

                    if (rowVariable == null)
                    {
                        Errors.Add(
                            new PricingErrorMessage()
                            {
                                ErrorType = "Error",
                                VariableName = table.VariableName,
                                Message = "Specified row variable does not exist on the assembly",
                            }
                            );
                        return null;
                    }

                    table.RowVariableID = rowVariable.ID;
                }
            }

            //get col variable data 
            AssemblyVariable colVariable = null;
            string colDataTypeStr = "string";
            if (table.ColumnVariableID.HasValue)
            {

                colVariable = GetColumnVariable(variables, table);
                if (colVariable == null) return null;
                colDataTypeStr = GetColumnDataType(table);
            }

            string variableClassName;

            if (!table.IsTierTable)
                variableClassName = tableDataTypeStr + "DataTable<" + rowDataTypeStr + "," + colDataTypeStr + ">";
            else
                variableClassName = table.VariableName + "<" + colDataTypeStr + ">";

            CBELTableGenerator generator = new CBELTableGenerator(this)
            {
                Name = table.VariableName,
                VariableClassName = variableClassName,
                Label = table.Label,
                VariableDataType = tableDataType,
                RowDataType = rowDataTypeStr,
                ColumnDataType = colDataTypeStr,
                RowMatchType = table.RowMatchType,
                ColumnMatchType = table.ColumnMatchType,
                RowVariableName = rowVariable?.Name,
                ColumnVariableName = colVariable?.Name,
                RowHeadersJSON = table.RowValuesJSON,
                ColumnHeadersJSON = table.ColumnValuesJSON,
                CellValuesJSON = table.CellDataJSON
            };

            if (profileTable != null && profileTable.OverrideDefault)
            {
                generator.RowHeadersJSON = profileTable.RowValuesJSON;
                generator.ColumnHeadersJSON = profileTable.ColumnValuesJSON;
                generator.CellValuesJSON = profileTable.CellDataJSON;
            }

            if (profileTable == null)
            {
                if (rowVariable != null)
                {
                    //Add Relies On
                    generator.ReliesOn.Add(rowVariable.Name);

                    //Add Referenced By
                    Variables.Where(var => var.Name == rowVariable.Name).First().ReferencedBy.Add(table.VariableName);
                }

                if (colVariable != null)
                {
                    //Add Relies On
                    generator.ReliesOn.Add(colVariable.Name);

                    //Add Referenced By
                    Variables.Where(var => var.Name == colVariable.Name).First().ReferencedBy.Add(table.VariableName);
                }

                AddVariable(generator);
            }

            return generator;
        }

        private AssemblyVariable GetColumnVariable(ICollection<AssemblyVariable> variables, AssemblyTable table)
        {
            AssemblyVariable colVariable = variables.FirstOrDefault(t => t.BID == table.BID && t.ID == table.ColumnVariableID);

            if (colVariable == null)
            {
                Errors.Add(
                    new PricingErrorMessage()
                    {
                        ErrorType = "Error",
                        VariableName = table.VariableName,
                        Message = "Specified column variable does not exist on the assembly",
                    }
                    );
                return null;
            }

            return colVariable;
        }

        private string GetColumnDataType(AssemblyTable table)
        {
            DataType colDataType = table.ColumnDataType.GetValueOrDefault(DataType.String);
            string colDataTypeStr = "string";
            switch (colDataType)
            {
                case (DataType.Boolean):
                    colDataTypeStr = "bool?";
                    break;
                case (DataType.Number):
                    colDataTypeStr = "decimal?";
                    break;
            }
            return colDataTypeStr;
        }

        protected void ValidateAllVariableAndTableNames()
        {
            //Loop through all variable and tables and collect a list of names
            var variableNames = Variables.Where(x => x.GetType() != typeof(CBELTableGenerator)).Select(v => v.Name).ToList();
            var tableNames = Variables.Where(x => x.GetType() == typeof(CBELTableGenerator)).Select(v => v.Name).ToList();

            //Check if all unique
            var duplicateVariableNames = variableNames.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => new { Name = y.Key, Counter = y.Count() })
              .ToList();

            var duplicateTableNames = tableNames.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .Select(y => new { Name = y.Key, Counter = y.Count() })
              .ToList();

            List<string> variableTableDuplicates = variableNames.Intersect(tableNames).ToList();
            //add errors for non-unique
            duplicateVariableNames.ForEach(x =>
                Errors.Add(
                    new PricingErrorMessage()
                    {
                        ErrorType = "Error",
                        VariableName = x.Name,
                        Message = "Non unique variable name:" + x.Name
                    }
                    ));
            duplicateTableNames.ForEach(x =>
                Errors.Add(
                    new PricingErrorMessage()
                    {
                        ErrorType = "Error",
                        VariableName = x.Name,
                        Message = "Non unique table name:" + x.Name
                    }
                    ));
            variableTableDuplicates.ForEach(x =>
                Errors.Add(
                    new PricingErrorMessage()
                    {
                        ErrorType = "Error",
                        VariableName = x,
                        Message = "Non unique table/variable name:" + x
                    }
                    ));

            // Check Measurement if it has UnitID
            List<CBELVariableGenerator>  measurements = Variables.Where(x => x.VariableClassName == nameof(NumberEditElement) && x.UnitType != null && x.UnitType > UnitType.Custom).ToList();
            measurements.ForEach(x =>
            {
                string message;
                if (!x.UnitID.HasValue)
                    message = NoDefaultUnitErrorMessage+x.Name;
                // Calling ToString() on an invalid unit value will return the number as string
                else if (((byte)x.UnitID.Value).ToString() == x.UnitID.Value.ToString())
                    message = $"Invalid Unit ({x.UnitID.Value})";
                else
                    message = null;

                if (message != null)
                    Errors.Add(
                        new PricingErrorMessage()
                        {
                            ErrorType = "Error",
                            VariableName = x.Name,
                            Message = message
                        }
                        );
            });


            CheckVariableAndTableNamesForReservedWords(variableNames, tableNames);
        }

        private void CheckVariableAndTableNamesForReservedWords(List<string> variableNames, List<string> tableNames)
        {
            HashSet<string> reservedWords = MetadataHelper.KnownMemberList().Select(x => x.Key).ToHashSet();
            reservedWords.RemoveWhere(rw => OverrideVariable(rw));

            List<string> variableDuplicates = variableNames.Where(s => reservedWords.Contains(s.ToLower())).ToList();

            List<string> tableDuplicates = tableNames.Where(s => reservedWords.Contains(s.ToLower())).ToList();

            variableDuplicates.ForEach(x =>
                Errors.Add(
                    new PricingErrorMessage()
                    {
                        ErrorType = "Error",
                        VariableName = x,
                        Message = "Variable name of:" + x + " is a reserved variable in Assemblies"
                    }
                    ));
            tableDuplicates.ForEach(x =>
                Errors.Add(
                    new PricingErrorMessage()
                    {
                        ErrorType = "Error",
                        VariableName = x,
                        Message = "Table name of:" + x + " is a reserved variable in Assemblies"
                    }
                    ));

        }

        private void SetLinkedFormula(AssemblyVariable variable, CBELVariableFormula variableFormula, Parser parser)
        {
            if (!string.IsNullOrWhiteSpace(variableFormula.FormulaText))
            {
                ParseFxResult result = CBELAssemblyHelper.ParseFormula(this, null, variable.Name, DataType.None, variableFormula.FormulaText, parser);

                if (result.FxFunction != null)
                    variableFormula.ValueFunction = result.FxFunction;
            }
        }

        protected virtual void InitSettings(ApiContext context, AssemblyData assemblyData, string name, int id, object parentObject = null)
        {
            AddUsingReference("System.Threading.Tasks");
            AddUsingReference("System.Collections.Generic");
            // now set any default fields
            AddSetter("Name", name.ToStringLiteral());
            AddSetter("CompanyID", null);
            AddSetter("ID", id.ToString());

            // Fill Variables
            FillVariables(context, assemblyData, new Parser(new CBELGrammar(assemblyData)));

            // Fill Tables
            FillTables(assemblyData);

            ValidateAllVariableAndTableNames();
        }
    }
}