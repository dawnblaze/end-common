﻿using Endor.CBEL.Common;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.Linq;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using Endor.Models;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using DataType = Endor.Models.DataType;

namespace Endor.CBEL.ObjectGeneration
{
    /// <summary>
    /// This class is used to emit an element pricing object
    /// </summary>
    public class CBELTableGenerator : CBELVariableGenerator
    {
        protected class IndexValueObject
        {
            public string index { get; set; }
            public string value { get; set; }
        }

        /// <summary>
        /// The default constructor.
        /// </summary>
        public CBELTableGenerator(CBELBaseAssemblyGenerator assemblyGenerator) : base(assemblyGenerator) {
            ProfileTableOverrides = new Dictionary<string, CBELTableGenerator>();
        }

        /// <summary>
        /// This stores the row data type
        /// </summary>
        public string RowDataType { get; set; }

        /// <summary>
        /// This stores the column data type
        /// </summary>
        public string ColumnDataType { get; set; }

        /// <summary>
        /// This stores the row match type
        /// </summary>
        public AssemblyTableMatchType RowMatchType { get; set; }

        /// <summary>
        /// This stores the column match type
        /// </summary>
        public AssemblyTableMatchType ColumnMatchType { get; set; }

        /// <summary>
        /// This stores the row variable name
        /// </summary>
        public string RowVariableName { get; set; }

        /// <summary>
        /// This stores the column variable name
        /// </summary>
        public string ColumnVariableName { get; set; }

        /// <summary>
        /// This stores the row headers values
        /// </summary>
        public string RowHeadersJSON { get; set; }

        /// <summary>
        /// This stores the column headers values
        /// </summary>
        public string ColumnHeadersJSON { get; set; }

        /// <summary>
        /// This stores the cell values JSON
        /// </summary>
        public string CellValuesJSON { get; set; }

        public Dictionary<string, CBELTableGenerator> ProfileTableOverrides { get; set; }
        
        private void AddDeclareVariablesValueCode(CodeBuilder Results, string tableElementName, int? index = null)
        {
            //Parse JSON Values
            if (!string.IsNullOrWhiteSpace(RowHeadersJSON))
            {
                var rowHeadings = JsonConvert.DeserializeObject<List<IndexValueObject>>(RowDataType.Contains("decimal") ?  AddDecimalSuffixForHeader(RowHeadersJSON) : RowHeadersJSON);

                // generate Headings List
                IEnumerable<string> myRowHeadings;

                switch (RowDataType)
                {
                    case "string":
                        myRowHeadings = rowHeadings.OrderBy(o => o.index).Select(i => $"\"{i.value}\"");
                        break;
                    case "bool?":
                        myRowHeadings = rowHeadings.OrderBy(o => o.index).Select(i => i.value == "0" ? "false" : "true");
                        break;
                    default:
                        myRowHeadings = rowHeadings.OrderBy(o => o.index).Select(i => i.value);
                        break;
                }

                string rowHeadingsList = string.Join(",", myRowHeadings);
                Results.Append($"{tableElementName}.RowHeadings.AddRange(new {RowDataType}[] {{{rowHeadingsList}}});");
            }

            if (!string.IsNullOrWhiteSpace(ColumnHeadersJSON))
            {
                var columnHeadings = JsonConvert.DeserializeObject<List<IndexValueObject>>(ColumnDataType.Contains("decimal") ? AddDecimalSuffixForHeader(ColumnHeadersJSON) : ColumnHeadersJSON);

                // generate Columns List
                IEnumerable<string> myColumnHeadings;

                switch (ColumnDataType)
                {
                    case "string":
                        myColumnHeadings = columnHeadings.OrderBy(o => o.index).Select(i => $"\"{i.value}\"");
                        break;
                    case "bool?":
                        myColumnHeadings = columnHeadings.OrderBy(o => o.index).Select(i => i.value == "0" ? "false" : "true");
                        break;
                    default:
                        myColumnHeadings = columnHeadings.OrderBy(o => o.index).Select(i => i.value);
                        break;
                }

                string columnHeadingsList = string.Join(",", myColumnHeadings);
                Results.Append($"{tableElementName}.ColumnHeadings.AddRange(new {ColumnDataType}[] " + "{" + columnHeadingsList + "});");
            }

            if (!string.IsNullOrWhiteSpace(CellValuesJSON))
            {
                object[][] jaggedCellValues = JsonConvert.DeserializeObject<object[][]>(CellValuesJSON);

                if (jaggedCellValues.Length > 0)
                {
                    // generate Cell data array
                    string cubicalCellValuesJson = JsonConvert.SerializeObject(ToCubicalFromJagged(jaggedCellValues));
                    cubicalCellValuesJson = (this.VariableDataType == DataType.Number) ?  AddDecimalSuffixForCellData(cubicalCellValuesJson) : cubicalCellValuesJson.Replace("[", "{").Replace("]", "}");
                    string arrayName = $"{Name}_Array{(index == null ? string.Empty : index.ToString())}";
                    Results.Append($"{VariableDataType.ToValueType()}[,] {arrayName} = {cubicalCellValuesJson};");
                    Results.Append($"{tableElementName}.AddCells({arrayName});");
                }
            }
        }

        private string AddDecimalSuffixForHeader(string jsonValue)
        {
            dynamic rowValuesJson = JsonConvert.DeserializeObject(jsonValue);
            foreach (var item in rowValuesJson.Children())
            {
                var key = "value";
                var value = item[key];
                if (value == null)
                {
                    key = "Value";
                }
                value = item[key];
                if (value != null)
                {
                    string strValue = value.ToString();
                    if (strValue.Contains(".") && !strValue.Contains("m"))
                    {
                        item[key] = strValue + "m";
                    }
                }
            }
            return rowValuesJson.ToString();
        }
        private string AddDecimalSuffixForCellData(string jsonValue)
        {
            dynamic cellDataJson = JsonConvert.DeserializeObject(jsonValue);

            var convertedString = "";
            foreach (var item in cellDataJson.Children())
            {

                var strItem = "";

                for (int i = 0; i < item.Count; i++)
                {
                    var strSubItem = item[i].ToString();
                    if (item[i].Value == null)
                    {
                        strSubItem = "null";
                    }

                    if (item[i].Value is double)
                    {
                        strSubItem = item[i].ToString() + "m";
                    }

                    strItem = strItem == "" ? strSubItem : strItem + "," + strSubItem;
                }

                strItem = "{" + strItem + "}";
                convertedString = convertedString == "" ? strItem : convertedString + "," + strItem;
            }

            return "{" + convertedString + "}";

        }
        /// <summary>
        /// Override this method call for table data
        /// </summary>
        /// <param name="Results"></param>
        protected override void AddVariableInitCode(CodeBuilder Results)
        {
            Results.Append($"{VariableFieldName}.RowMatchOption = AssemblyTableMatchType.{RowMatchType.ToString()};");

            Results.Append($"{VariableFieldName}.ColumnMatchOption = AssemblyTableMatchType.{ColumnMatchType.ToString()};");

            Results.Append($"{VariableFieldName}.RowVariableName = \"{RowVariableName}\";");

            Results.Append($"{VariableFieldName}.ColumnVariableName = \"{ColumnVariableName}\";");

            Results.AppendCR();

            Results.Append($"{VariableFieldName}.FillTable = (t) => {{");
            Results.IncIndent();

            if (ProfileTableOverrides.Count > 0)
            {
                // switch(Profile)
                Results.Append($"switch (Profile.Value)");
                Results.Append("{");
                Results.IncIndent();

                int i = 0;
                foreach (var profile in ProfileTableOverrides)
                {
                    i++;
                    Results.Append($"case \"{profile.Key}\":");
                    Results.IncIndent();

                    profile.Value.AddDeclareVariablesValueCode(Results, "t", i);
                    Results.AppendCR();
                    Results.Append($"break;");

                    Results.AppendCR();

                    Results.DecIndent();
                }

                Results.Append("default:");
                Results.IncIndent();

                AddDeclareVariablesValueCode(Results, "t");
                Results.AppendCR();
                Results.Append($"break;");

                Results.DecIndent();

                Results.DecIndent();
                Results.Append("}");
            }
            else
                AddDeclareVariablesValueCode(Results, "t");

            Results.DecIndent();
            Results.Append("};");
        }

        public static T[,] ToCubicalFromJagged<T>(T[][] source) where T : class
        {
            var FirstDim = source.Length;
            var SecondDim =
                source.Select(row => row.Length).Max();

            var result = new T[FirstDim, SecondDim];
            for (var i = 0; i < FirstDim; i++)
                for (var j = 0; j < SecondDim; j++)
                    if (j > source[i].Length - 1)
                        result[i, j] = null;
                    else
                        result[i, j] = source[i][j];

            return result;
        }

    }
}
