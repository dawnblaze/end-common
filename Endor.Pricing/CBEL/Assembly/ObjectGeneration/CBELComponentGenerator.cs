﻿using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.ObjectGeneration
{
    /// <summary>
    /// This class is used to emit an element pricing object
    /// </summary>
    public class CBELComponentGenerator : CBELVariableGenerator
    {
        /// <summary>
        /// The default constructor.
        /// </summary>
        public CBELComponentGenerator(CBELBaseAssemblyGenerator assemblyGenerator) : base(assemblyGenerator) { }


        /// <summary>
        /// Returns list of linked variable formulas.
        /// </summary>
        /// <returns>list of linked variable formulas</returns>
        public List<ICBELVariableFormula> Formulas { get; set; }

        public override void AddLinkedFormulas(CodeBuilder Results)
        {
            if (Formulas == null || Formulas.Count == 0)
                return;

            Results.AppendCR();

            Results.Append($"{VariableFieldName}.Formulas = new List<VariableFormula>();");
            Results.AppendCR();

            foreach (ICBELVariableFormula vf in Formulas)
            {
                Results.Append($"// Variable Formula for {vf.ChildVariableName}");
                // add that formula to the new 
                Results.Append($"{VariableFieldName}.Formulas.Add(");
                Results.IncIndent();
                Results.Append("new VariableFormula()");
                Results.Append("{");

                Results.IncIndent();
                Results.Append($"ChildVariableName = {vf.ChildVariableName.ToStringLiteral()},");
                Results.Append($"DataType = DataType.{vf.DataType},");
                Results.Append($"FormulaEvalType = AssemblyFormulaEvalType.{vf.FormulaEvalType},");
                Results.Append($"FormulaText = {vf.FormulaText.ToStringLiteral()},");
                Results.Append($"FormulaUseType = AssemblyFormulaUseType.{vf.FormulaUseType},");
                Results.Append($"Unit = null,");
                if (!string.IsNullOrWhiteSpace(vf.ValueFunction))
                    Results.Append($"ValueFunction = () => {vf.ValueFunction}");
                Results.DecIndent();

                Results.Append("}");
                Results.DecIndent();

                Results.Append(");");

                // add a blank line for nice spacing!
                Results.AppendCR();
            }
        }

        protected override void AddVariableInitCode(CodeBuilder Results)
        {
            base.AddVariableInitCode(Results);

            if (RollupLinkedPriceAndCost.GetValueOrDefault(false))
                Results.Append($"{VariableFieldName}.RollupLinkedPriceAndCost = true;");
        }
    }
}
