﻿using Endor.CBEL.Common;
using Endor.Models;
using Irony.Parsing;

namespace Endor.CBEL.ObjectGeneration
{
    public partial class CBELVariableGenerator
    {
        private ParseFxResult SetFx(CBELBaseAssemblyGenerator assemblyGenerator, string variableName, bool isFormula, DataType dataType, string valueType, string valueText, Parser parser)
        {
            if (!isFormula)
            {
                string valueConstant = valueText;

                switch (dataType)
                {
                    case DataType.Number:
                        if (string.IsNullOrWhiteSpace(valueConstant))
                            valueConstant = null;
                        else if (!decimal.TryParse(valueConstant, out _))
                        {
                            assemblyGenerator.Errors.Add(new PricingErrorMessage() { ErrorType = "Invalid formula", VariableName = variableName, FormulaText = valueText, IsValidation = false, Message = $"Invalid data in {valueType} value constant" });
                            valueConstant = null;
                        }
                        break;
                    case DataType.Boolean:
                        if (string.IsNullOrWhiteSpace(valueConstant))
                            valueConstant = null;
                        else if (CBELAssemblyHelper.TryStringToBool(valueConstant, out bool boolValue))
                            valueConstant = boolValue ? "true" : "false";
                        else
                        {
                            assemblyGenerator.Errors.Add(new PricingErrorMessage() { ErrorType = "Invalid formula", VariableName = variableName, FormulaText = valueText, IsValidation = false, Message = $"Invalid data in {valueType} value constant" });
                            valueConstant = null;
                        }
                        break;
                    default:
                        break;
                }

                return new ParseFxResult()
                {
                    IsConstant = true,
                    FormulaText = valueText,
                    FxFunction = null,
                    ValueConstant = valueConstant
                };
            }

            if (!string.IsNullOrWhiteSpace(valueText))
                return CBELAssemblyHelper.ParseFormula(assemblyGenerator, this, variableName, dataType, valueText, parser);

            return new ParseFxResult()
            {
                IsConstant = false,
                FormulaText = null,
                FxFunction = null,
                ValueConstant = null
            };
        }

        public void SetFormulaOrConstantValue(CBELBaseAssemblyGenerator assemblyGenerator, AssemblyVariable variable, Parser parser)
        {
            ParseFxResult result = SetFx(assemblyGenerator, variable.Name, variable.IsFormula, variable.DataType, "default", variable.DefaultValue, parser);

            if (result.IsConstant)
                DefaultValueConstant = result.ValueConstant;
            else
                DefaultValueFunction = result.FxFunction;

            FormulaText = result.FormulaText;
        }

        public void SetAltFormulaOrConstantValue(CBELBaseAssemblyGenerator assemblyGenerator, AssemblyVariable variable, Parser parser)
        {
            ParseFxResult result = SetFx(assemblyGenerator, variable.Name, variable.IsAltTextFormula, DataType.String, "alt", variable.AltText, parser);

            if (result.IsConstant)
                AltConstant = result.ValueConstant;
            else
                AltFunction = result.FxFunction;

            AltFormulaText = result.FormulaText;
        }

        public void SetInclusionFormulaOrConstantValue(CBELBaseAssemblyGenerator assemblyGenerator, AssemblyVariable variable, Parser parser)
        {
            ParseFxResult result = SetFx(assemblyGenerator, variable.Name, true, DataType.Boolean, "inclusion", variable.InclusionFormula, parser);

            InclusionFunction = result.FxFunction;

            InclusionFormulaText = result.FormulaText;
        }

        public void SetConsumptionFormulaOrConstantValue(CBELBaseAssemblyGenerator assemblyGenerator, AssemblyVariable variable, Parser parser)
        {
            ParseFxResult result = SetFx(assemblyGenerator, variable.Name, variable.IsConsumptionFormula, DataType.Number, "consumption", variable.ConsumptionDefaultValue, parser);

            if (result.IsConstant)
            {
                if (string.IsNullOrWhiteSpace(result.ValueConstant))
                    DefaultConsumptionValueConstant = null;
                else
                    DefaultConsumptionValueConstant = decimal.Parse(result.ValueConstant);
            }
            else
                DefaultConsumptionValueFunction = result.FxFunction;

            ConsumptionFormulaText = result.FormulaText;
        }
    }
}
