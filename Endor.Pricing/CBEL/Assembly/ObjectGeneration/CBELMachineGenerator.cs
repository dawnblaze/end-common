﻿using Endor.CBEL.Common;
using Endor.Models;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.ObjectGeneration
{
    public partial class CBELMachineGenerator: CBELBaseAssemblyGenerator
    {

        public CBELMachineGenerator(short bid, int id, int classtypeid, string name, int version) 
            : base(bid, id, classtypeid, name, version)
        {
            _ProfileList = new List<(string profileName, MachineLayoutType layoutType)>();

            _InstanceList = new List<string>();
        }

        protected List<(string profileName, MachineLayoutType layoutType)> _ProfileList;

        public List<(string profileName, MachineLayoutType layoutType)> ProfileList => _ProfileList;

        public string DefaultLayoutType { get; set; }
        public string DefaultProfile { get; set; }
        public decimal? MachineCostPerHour { get; set; }
        public void AddProfile(string profileName, MachineLayoutType layoutType) => _ProfileList.Add((profileName, layoutType));

        protected List<string> _InstanceList;
        public List<string> InstanceList => _InstanceList;
        public string DefaultInstance { get; set; }
        public void AddInstance(string instanceName) => _InstanceList.Add(instanceName);


        // ==============================================================
        // Generate Assembly C# Code
        // ==============================================================

        protected void AddInitializeProfileListCode(CodeBuilder Results)
        {
            bool addCR = false;

            if (ProfileList.Count > 0)
            {
                ProfileList.ForEach(x => Results.Append($"ProfileList.Add(\"{x.profileName}\");"));
                addCR = true;
            }

            if (DefaultProfile != null)
            {
                if (addCR)
                    Results.AppendCR();

                Results.Append($"Profile.DefaultValueConstant = \"{DefaultProfile}\";");
                addCR = true;
            }

            if (MachineCostPerHour != null)
            {
                if (addCR)
                    Results.AppendCR();

                Results.Append($"MachineCostPerHour = {MachineCostPerHour}m;");
                addCR = true;
            }

            if (addCR)
                Results.AppendCR();

            Results.Append($"LayoutType.DefaultValueFunction = () =>");
            Results.IncIndent();

            bool isFirst = true;

            foreach ((string profileName, MachineLayoutType layoutType) in ProfileList)
            {
                if (!isFirst)
                    Results.Append(" :", indent: false);

                Results.Append($"(Profile.Value == \"{profileName}\") ? \"{layoutType}\"", addCR: false);
                isFirst = false;
            }

            if (!isFirst)
                Results.Append(" :", indent: false);

            if (DefaultLayoutType != null)
                Results.Append($"\"{DefaultLayoutType}\"");
            else
                Results.Append("null");

            Results.Append(";");
            Results.DecIndent();
        }

        protected override void AddInitializeVariablesCode(CodeBuilder Results)
        {
            Results.Append("base.InitializeVariables();");
            Results.AppendCR();

            base.AddInitializeVariablesCode(Results);
        }

        protected void AddInitializeInstanceListCode(CodeBuilder Results)
        {
            bool addCR = false;

            if (InstanceList.Count > 0)
            {
                InstanceList.ForEach(instance => Results.Append($"InstanceList.Add(\"{instance}\");"));
                addCR = true;
            }

            if (DefaultInstance != null)
            {
                if (addCR)
                    Results.AppendCR();

                Results.Append($"Instance.DefaultValueConstant = \"{DefaultInstance}\";");
            }
        }

        protected override void AddOverriddenMethods(CodeBuilder Results)
        {
            base.AddOverriddenMethods(Results);

            Results.AppendCR();

            Results.AppendMethodCode("InitializeProfileList",
                                     AddInitializeProfileListCode,
                                     comments: "Override the InitializeProfileList() method to pass the profile names to ProfileList",
                                     isOverridden: true);

            Results.AppendMethodCode("InitializeInstanceList",
                                     AddInitializeInstanceListCode,
                                     comments: "Override the InitializeInstanceList() method to pass the instance names to InstanceList",
                                     isOverridden: true);
        }

        protected override string CBELAssemblyBaseClass => "CBELMachineBase";
        protected override string AssemblyTypeName => "Machine";

        public override bool IsVariablePredefined(string variableName)
        {
            return (variableName.ToLower() == "profile") || (variableName.ToLower() == "instance");
        }
    }
}
