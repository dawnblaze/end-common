﻿using Endor.CBEL.Common;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using Endor.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Endor.Units;

namespace Endor.CBEL.ObjectGeneration
{
    /// <summary>
    /// This class is used to emit an element pricing object
    /// </summary>
    public partial class CBELVariableGenerator : ICBELVariable, IComparable<CBELVariableGenerator>
    {
        /// <summary>
        /// The default constructor.
        /// </summary>
        public CBELVariableGenerator(CBELBaseAssemblyGenerator assemblyGenerator)
        {
            // Create a List to hold the ReliesOn in this Assembly
            ReliesOn = new List<string>();
            // Create a List to hold the ReferencedBy in this Assembly
            ReferencedBy = new List<string>();

            PropertyOverrides = new Dictionary<string, string>();
            ProfileOverrides = new Dictionary<string, string>();

            AssemblyGenerator = assemblyGenerator;
        }

        /// <summary>
        /// The Generator for the Parent Assembly
        /// </summary>
        public CBELBaseAssemblyGenerator AssemblyGenerator { get; private set; }
        
        /// <summary>
        /// If this is set to true, the public Getter will use type dynamic
        /// </summary>
        public bool UseDynamicType { get; set; }

        /// <summary>
        /// The DataType of the Element
        /// </summary>
        public DataType VariableDataType { get; set; }

        /// <summary>
        /// The ClassType Name of the element.
        /// </summary>
        public string VariableClassName { get; set; }

        protected string VariableName => Name;
        protected string VariableFieldName => $"_{Name}";

        /// <summary>
        /// The name of variable used in formulas. This is also the name of the property on 
        /// the Assembly object that references this variable.  It is defined on the Assembly as:
        ///    public {ElementPropertyClass} {name} {get; private set;}
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The display label for the variable.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// The original formula (in CBEL) for the variable.
        /// This metadata is useful for debugging.
        /// </summary>
        public string FormulaText { get; set; }

        /// <summary>
        /// Property that indicates if the IsRequired Validator applied to this variable.
        /// </summary>
        public bool? IsRequired { get; set; }

        /// <summary>
        /// optional unit
        /// </summary>
        public Unit? UnitID { get; set; }

        /// <summary>
        /// optional unit type
        /// </summary>
        public UnitType? UnitType { get; set; }

        /// <summary>
        /// A list of Variables on the Assembly that the formula needs in it's evaluation.
        /// </summary>
        public List<string> ReliesOn { get; }

        /// <summary>
        /// A list of Variables on the Assembly that the formula needs in it's evaluation.
        /// </summary>
        public List<string> ReferencedBy { get; }

        /// <summary>
        /// This stores the function that is used to evaluate the default value.
        /// </summary>
        public string DefaultValueFunction { get; set; }

        /// <summary>
        /// Stores the Constant as the DefaultValue for this object
        /// </summary>
        public string DefaultValueConstant { get; set; }

        public decimal? Cost { get; set; }

        public bool HasConsumption => !string.IsNullOrWhiteSpace(ConsumptionFormulaText);

        /// <summary>
        /// The original formula (in CBEL) for the variable.
        /// This metadata is useful for debugging.
        /// </summary>
        public string ConsumptionFormulaText { get; set; }

        /// <summary>
        /// This stores the function 
        /// </summary>
        public string DefaultConsumptionValueFunction { get; set; }

        /// <summary>
        /// Stores the Constant 
        /// </summary>
        public decimal? DefaultConsumptionValueConstant { get; set; }

        /// <summary>
        /// The original formula (in CBEL) for the variable alt.
        /// This metadata is useful for debugging.
        /// </summary>
        public string AltFormulaText { get; set; }

        /// <summary>
        /// This stores the function that is used to evaluate the alt value.
        /// </summary>
        public string AltFunction { get; set; }

        /// <summary>
        /// Stores the Constant as the Alt for this object
        /// </summary>
        public string AltConstant { get; set; }

        /// <summary>
        /// LinkedMaterialID
        /// </summary>
        public int? LinkedMaterialID { get; set; }

        /// <summary>
        /// LinkedLaborID
        /// </summary>
        public int? LinkedLaborID { get; set; }

        /// <summary>
        /// LinkedMachineID
        /// </summary>
        public short? LinkedMachineID { get; set; }

        /// <summary>
        /// LinkedAssemblyID
        /// </summary>
        public int? LinkedAssemblyID { get; set; }

        /// <summary>
        /// Inclusion Formula
        /// </summary>
        public string InclusionFormula { get; set; }

        /// <summary>
        /// Inclusion Formula Text
        /// </summary>
        public string InclusionFormulaText { get; set; }

        /// <summary>
        /// Inclusion Function
        /// </summary>
        public string InclusionFunction { get; set; }


        /// <summary>
        /// roll up linked price and cost
        /// </summary>
        public bool? RollupLinkedPriceAndCost { get; set; }

        public ICBELAssembly Parent { get; set; }


        public Dictionary<string, string> ProfileOverrides { get; private set; }
        public Dictionary<string, string> PropertyOverrides { get; private set; }


        // ==============================================================
        // Generate Element C# Code
        // ==============================================================

        private void AddFunction(CodeBuilder Results, string functionPropertyName, string functionCode)
        {
            if (functionCode != null)
            {
                string[] codeSplit = functionCode.Split("\n");

                if (codeSplit.Length < 2)
                    Results.Append($"{VariableFieldName}.{functionPropertyName} = () => {functionCode};");
                else
                {
                    Results.Append($"{VariableFieldName}.{functionPropertyName} = () =>");
                    Results.IncIndent();

                    foreach (string s in codeSplit)
                        Results.Append(s);

                    Results.Append(";");
                    Results.DecIndent();
                }
            }
        }
        protected virtual void AddVariableInitCode(CodeBuilder Results)
        {
            if (HasConsumption)
            {
                Results.Append($"{VariableFieldName}.ConsumptionQuantity.FormulaText = {ConsumptionFormulaText.ToStringLiteral()};");
                if (DefaultConsumptionValueFunction != null)
                    Results.Append($"{VariableFieldName}.ConsumptionQuantity.DefaultValueFunction= () => {DefaultConsumptionValueFunction};");
                if (DefaultConsumptionValueConstant.HasValue)
                    Results.Append($"{VariableFieldName}.ConsumptionQuantity.DefaultValueConstant = {DefaultConsumptionValueConstant}m;");
            }

            AddFunction(Results, "DefaultValueFunction", GetDefaultValueFunctionCode());
            AddFunction(Results, "AltFunction", AltFunction);
            AddFunction(Results, "InclusionFunction", InclusionFunction);

            // Calling ToString() on an invalid unit value will return the number as string
            if (UnitID.HasValue && ((byte)UnitID.Value).ToString() != UnitID.Value.ToString())
                Results.Append($"{VariableFieldName}.Unit = Unit.{UnitID.Value};");

            string DefaultValueConstantCode = GetDefaultValueConstantCode();

            if (DefaultValueConstantCode != null)
                Results.Append($"{VariableFieldName}.DefaultValueConstant = {DefaultValueConstantCode};");

            string AltConstantCode = GetConstantCode(AltConstant);

            if (AltConstantCode != null)
                Results.Append($"{VariableFieldName}.AltConstant = {AltConstantCode};");

            if (IsRequired.HasValue)
                Results.Append($"{VariableFieldName}.IsRequired = {(IsRequired.Value ? "true" : "false")};");

            if (LinkedAssemblyID.HasValue)
                Results.Append($"{VariableFieldName}.LinkedAssemblyID = {LinkedAssemblyID.Value};");

            if (LinkedLaborID.HasValue)
                Results.Append($"{VariableFieldName}.LinkedLaborID = {LinkedLaborID.Value};");

            if (LinkedMachineID.HasValue)
                Results.Append($"{VariableFieldName}.LinkedMachineID = {LinkedMachineID.Value};");

            if (LinkedMaterialID.HasValue)
                Results.Append($"{VariableFieldName}.LinkedMaterialID = {LinkedMaterialID.Value};");
        }

        public void AddDeclareVariableCode(CodeBuilder Results)
        {
            // Prevent from redeclaring the required variables
            if (AssemblyGenerator.IsVariablePredefined(Name))
                return;

            if (DefaultValueFunction != null && DefaultValueConstant != null)
                throw new CBELCompilationException("Cannot have both a DefaultValueFunction and DefaultValueConstant.");
            string variableTypeHelper = UseDynamicType ? "dynamic" : VariableClassName;

            Results.Append($"// Variable:  {Name}");
            Results.Append($"private {VariableClassName} {VariableFieldName};");
            Results.Append($"public {(AssemblyGenerator.OverrideVariable(Name) ? "override " : string.Empty)}{variableTypeHelper} {VariableName} ");
            Results.Append("{");
            Results.IncIndent();
            Results.Append($"get");
            Results.Append("{");
            Results.IncIndent();
            Results.Append($"if ( {VariableFieldName} == null)");
            Results.Append("{ ");
            Results.IncIndent();

            Results.Append($"{VariableFieldName} = new {VariableClassName}(this);");
            Results.Append($"{VariableFieldName}.Name = {Name.ToStringLiteral()};");

            if (Label != null)
                Results.Append($"{VariableFieldName}.Label = {Label.ToStringLiteral()};");

            if (FormulaText != null)
                Results.Append($"{VariableFieldName}.FormulaText = {FormulaText.ToStringLiteral()};");

            if (AltFormulaText != null)
                Results.Append($"{VariableFieldName}.AltFormulaText = {AltFormulaText.ToStringLiteral()};");

            AddVariableInitCode(Results);

            // Add any Formula
            AddLinkedFormulas(Results);

            // Add referenced by code
            AddReferencedByCode(Results);

            // Add relieson code
            AddReliesOnCode(Results);

            Results.DecIndent();
            Results.Append("}");
            Results.AppendCR();

            Results.Append($"return {VariableFieldName};");

            Results.DecIndent();
            Results.Append("}");
            Results.DecIndent();
            Results.Append("}");
            Results.AppendCR();
        }

        public void OverrideDefaultVariables(CodeBuilder Results)
        {
            // overrides the default getter
            var tierTables = new List<string>() { "MarginTable", "DiscountTable", "MarkupTable", "PriceTable" };
            var variableTypeHelper = "decimal?";
            var variableName = $"Tier{Name}";

            if (tierTables.Contains(Name))
            {
                Results.Append($"// Variable:  Tier{Name}");
                Results.Append($"public override {variableTypeHelper} {variableName} ");
                Results.Append("{");
                Results.IncIndent();
                Results.Append($"get");
                Results.Append("{");
                Results.IncIndent();
                Results.Append($"return {Name}.Value;");
                Results.DecIndent();
                Results.Append("}");
                Results.DecIndent();
                Results.Append("}");
                Results.AppendCR();
            }

        }
        private string GetConstantCode(string value)
        {
            if (value == null)
                return null;

            switch (VariableDataType)
            {
                case DataType.Number: // number
                    if (string.IsNullOrWhiteSpace(value))
                        return null;
                    return $"{value}m";

                case DataType.Boolean: // boolean
                    return value;

                case DataType.DateTime: // datetime
#warning Needs to be resolved still
                    return value.ToStringLiteral(false);

                case DataType.String: // string
                default:
                    return $"\"{value.Replace("\"", "\\\"")}\"";
            }
        }

        private string GetDefaultValueConstantCode()
        {
            if (ProfileOverrides != null && ProfileOverrides.Count > 0)
                return null;

            return GetConstantCode(DefaultValueConstant);
        }

        private string GetDefaultValueFunctionCode()
        {
            StringBuilder sb = new StringBuilder(4000);
            string ovValue;
            bool isFirst = true;

            if (ProfileOverrides != null && ProfileOverrides.Count > 0)
            {

                foreach (var ov in ProfileOverrides)
                {
                    ovValue = GetConstantCode(ov.Value);

                    if (ovValue != null)
                    {
                        if (!isFirst)
                            sb.Append(" :\n");

                        sb.Append($"(Profile.Value == \"{ov.Key}\") ? {ovValue}");
                        isFirst = false;
                    }
                }

                if (PropertyOverrides != null && PropertyOverrides.Count > 0)
                {
                    foreach (var ov in PropertyOverrides)
                    {
                        ovValue = GetConstantCode(ov.Value);
                        if (ovValue != null)
                        {
                            if (!isFirst)
                                sb.Append(" :\n");

                            sb.Append(ovValue);
                            isFirst = false;
                        }
                    }
                }

                else
                {
                    ovValue = GetConstantCode(DefaultValueConstant);

                    if (ovValue == null)
                        ovValue = DefaultValueFunction;

                    if (ovValue != null)
                    {
                        if (!isFirst)
                            sb.Append(" :\n");

                        sb.Append(ovValue);
                        isFirst = false;
                    }

                    else if (!isFirst)
                    {
                        sb.Append(" :\n");
                        sb.Append($"({VariableDataType.ToValueType()})null");
                        isFirst = false;
                    }
                }                

                if (isFirst)
                    return null;

                return sb.ToString();
            }
            else
            {
                if (PropertyOverrides != null && PropertyOverrides.Count > 0)
                {
                    foreach (var ov in PropertyOverrides)
                    {
                        ovValue = GetConstantCode(ov.Value);
                        if (ovValue != null)
                        {
                            if (!isFirst)
                                sb.Append(" :\n");

                            sb.Append(ovValue);
                            isFirst = false;
                        }
                    }   
                    
                    return sb.ToString();
                }
                else
                {
                    ovValue = GetConstantCode(DefaultValueConstant);

                    if (ovValue == null)
                        ovValue = DefaultValueFunction;

                    if (ovValue != null)
                    {
                        if (!isFirst)
                            sb.Append(" :\n");

                        sb.Append(ovValue);
                        isFirst = false;
                    }

                    else if (!isFirst)
                    {
                        sb.Append(" :\n");
                        sb.Append($"({VariableDataType.ToValueType()})null");
                        isFirst = false;
                    }
                }

                if (isFirst)
                    return null;


            }

            return DefaultValueFunction;
        }

        /// <summary>
        /// This method returns the C# Code for the Element 
        /// It should be called after all other properties are set.
        /// </summary>
        public virtual void AddVariableDefinitionCode(CodeBuilder Results)
        {
            // Prevent from overriding the required variables
            if (AssemblyGenerator.IsVariablePredefined(Name))
                return;

            // Add it to the Elements dictionary
            Results.Append($"Variables.Add( \"{Name}\", {VariableName} );");
        }

        // ==============================================================
        // ReliesOn and ReferencedBy Elements
        // ==============================================================

        /// <summary>
        /// This method fills the ReliesOn dictionary with 
        /// </summary>
        /// <returns></returns>
        public virtual void AddReliesOnCode(CodeBuilder Results)
        {
            if (ReliesOn.Count > 0)
            {
                Results.AppendCR();
                ReliesOn.ForEach(str => Results.Append($"{this.VariableFieldName}.AddReliesOn(\"{str}\");"));
            }
        }

        /// <summary>
        /// Get a string of all of the Using References
        /// </summary>
        /// <returns></returns>
        public virtual void AddReferencedByCode(CodeBuilder Results)
        {
            if (ReferencedBy.Count > 0)
            {
                Results.AppendCR();
                ReferencedBy.ForEach(str => Results.Append($"{this.VariableFieldName}.AddReferencedBy(\"{str}\");"));
            }
        }

        public virtual void AddLinkedFormulas(CodeBuilder Results)
        {
        }

        public int CompareTo(CBELVariableGenerator other)
        {
            return (this.Name.CompareTo(other.Name));
        }
    }
}
