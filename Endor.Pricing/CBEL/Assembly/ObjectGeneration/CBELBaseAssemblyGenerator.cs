﻿using Endor.CBEL.Common;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Scripting;
using System;
using System.Collections.Generic;
using System.IO;
using Endor.Tenant;
using Endor.Logging.Client;

namespace Endor.CBEL.ObjectGeneration
{
    /// <summary>
    /// This class is used to emit an assembly pricing object
    /// </summary>
    public abstract partial class CBELBaseAssemblyGenerator
    {
        /// <summary>
        /// The Constructor for the Pricer Generator.
        /// </summary>
        /// <param name="bid">The BusinessID of the Assembly Object</param>
        /// <param name="classtypeid">The ClassTypeID of the Assembly Object, normally 12040</param>
        /// <param name="id">The ID of the Generated Pricer Assembly Object</param>
        /// <param name="name">The name of the assembly, machine, or machine template</param>
        /// <param name="version">The Version of the Assembly Object</param>
        public CBELBaseAssemblyGenerator(short bid, int id, int classtypeid, string name, int version)
        {
            BID = bid;
            ClassTypeID = classtypeid;
            ID = id;
            Version = version;
            Name = name;

            // Create a List to hold the Variables in this Assembly
            _Variables = new List<CBELVariableGenerator>();
            Errors = new List<PricingErrorMessage>();
            _ExternalDependencies = new List<string>();

            // Create a List to hold the Setters in this Assembly
            Setters = new List<(string Name, string Value)>();

            // Create a List to hold the Using References and populate the initial list
            Usings = new List<(string Reference, bool IsStatic)>();
            AddUsingReference("Endor.CBEL");
            AddUsingReference("Endor.CBEL.Common");
            AddUsingReference("Endor.CBEL.Elements");
            AddUsingReference("Endor.EF");
            AddUsingReference("Endor.CBEL.Exceptions");
            AddUsingReference("Endor.CBEL.Interfaces");
            AddUsingReference("Endor.Tenant");
            AddUsingReference("Endor.Logging.Client");
            AddUsingReference("Endor.Models");
            AddUsingReference("Endor.Units");
        }

        /// <summary>
        /// The Business ID of the Assembly Object
        /// </summary>
        public short BID { get; set; }
        public List<PricingErrorMessage> Errors { get; }
        private readonly List<string> _ExternalDependencies;

        public void AddExternalDependencies(string referenceName)
        {
            if (!_ExternalDependencies.Contains(referenceName))
                _ExternalDependencies.Add(referenceName);
        }

        /// <summary>
        /// The ID of the Assembly Object
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// The ClassTypeID of the Assembly Object, normally 12040
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The Version Number of the Assembly.  
        /// This is auto-incremented each time the Assembly is modified so that 
        /// historic versions of the pricing may be maintained.
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// The Name of the Assembly, Machine, or Machine Template
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The name of the new class.
        /// This defaults to "Assembly_BID{BID}_CT{ClassTypeID}_ID{ID}_V{Version}" but may be overridden.
        /// </summary>
        private string _AssemblyClassName;
        public string AssemblyClassName
        {
            // Use default value unless overridden
            get => (!string.IsNullOrWhiteSpace(_AssemblyClassName)) ? _AssemblyClassName : CBELAssemblyHelper.AssemblyName(BID, ID, ClassTypeID, Version);
            set => _AssemblyClassName = value;
        }

        // ==============================================================
        // Generate Assembly C# Code
        // ==============================================================

        protected virtual void AddOverriddenMethods(CodeBuilder Results)
        {
            Results.AppendMethodCode("InitializeVariables",
                                     AddInitializeVariablesCode,
                                     comments: "Override the InitializeVariables() method to create the actual Variables",
                                     isOverridden: true);

            Results.AppendCR();

            Results.AppendMethodCode("InitializeData",
                                     AddInitializeDataCode,
                                     comments: "Override the InitializeData() method to set data fields",
                                     isOverridden: true);
        }

        /// <summary>
        /// This method returns the Assembly Code file as a string.
        /// It should be called after the Variables, usings, and other properties are set.
        /// </summary>
        /// <returns></returns>
        public string GetAssemblyCode()
        {
            CheckDefaultVariables();
            _Variables.Sort();

            // Return the Code to create the C# Unit for this assembly.

            CodeBuilder Results = new CodeBuilder();

            AddUsingReferencesCode(Results);

            Results.AppendCR();

            Results.Append($"// {AssemblyTypeName} Created on {DateTime.UtcNow} UTC.");

            Results.Append($"public class {AssemblyClassName} : {CBELAssemblyBaseClass}");
            Results.Append("{");

            Results.IncIndent();

            Results.AppendMethodCode(AssemblyClassName,
                                     null,
                                     returnType: null,
                                     parameters: $"ApiContext ctx, short bid, {nameof(ITenantDataCache)} cache, {nameof(RemoteLogger)} logger",
                                     baseParameters: "ctx, bid, cache, logger");

            Results.AppendCR();
            Results.Append("// Declare Properties for each Variable");
            Results.AppendCR();
            AddDeclareVariablesCode(Results);

            AddOverriddenMethods(Results);

            Results.DecIndent();
            Results.Append("}");

            return Results.ToString();
        }

        public virtual void CheckDefaultVariables() { }

    
        // ==============================================================
        // Field and Property Setters
        // ==============================================================

        /// <summary>
        /// List of Field and Property Setters to set on initialize.
        /// </summary>
        protected List<(string Name, string Value)> Setters;

        /// <summary>
        /// This method is used to add each of the Variables to the assembly.
        /// </summary>
        public void AddSetter(string Name, dynamic Value) => Setters.Add((Name, Value));

        /// <summary>
        /// This method creates the code that goes in the InitializeData() method.
        /// It should assign all of the values to the default fields and properties.
        /// </summary>
        protected void AddInitializeDataCode(CodeBuilder Results)
        {
            Setters.ForEach(x => Results.Append($"{x.Name} = {x.Value ?? "null"};"));
        }

        // ==============================================================
        // Variables
        // ==============================================================

        /// <summary>
        /// This method declares the public property and private backing field for each element.
        /// </summary>
        /// <returns></returns>
        protected virtual void AddDeclareVariablesCode(CodeBuilder Results)
        {
            _Variables.ForEach(x => x.AddDeclareVariableCode(Results));
        }

        /// <summary>
        /// This method creates the code that goes inside the InitializeVariables() method.
        /// It created the element and assigns it property and adds it to the Variables array.
        /// </summary>
        /// <returns></returns>
        protected virtual void AddInitializeVariablesCode(CodeBuilder Results)
        {
            _Variables.ForEach(x => x.AddVariableDefinitionCode(Results));
        }

        /// <summary>
        /// This will create the declaration of the base class i.e. CBELMachineBase, CBELaborBase etc.
        /// </summary>
        /// <returns></returns>
        protected virtual string CBELAssemblyBaseClass => null;

        /// <summary>
        /// The common name of the assembly type (Assembly, Machine)
        /// </summary>
        /// <returns></returns>
        protected virtual string AssemblyTypeName => null;

        /// <summary>
        /// List of Variables to create.
        /// </summary>
        protected List<CBELVariableGenerator> _Variables;

        /// <summary>
        /// List of Variables to create.
        /// </summary>
        public List<CBELVariableGenerator> Variables => _Variables;

        /// <summary>
        /// This method is used to add each of the Variables to the assembly.
        /// </summary>
        /// <param name="variable"></param>
        public void AddVariable(CBELVariableGenerator variable) => _Variables.Add(variable);


        // ==============================================================
        // References
        // ==============================================================

        /// <summary>
        /// Get a string of all of the Using References
        /// </summary>
        /// <returns></returns>
        protected void AddUsingReferencesCode(CodeBuilder Results)
        {
            Usings.ForEach(x => Results.Append($"using {(x.IsStatic ? "static " : "")}{x.Reference};"));
        }

        protected List<(string Reference, bool IsStatic)> Usings;

        /// <summary>
        /// This method is used to add Using References.  Do not add the word "using".
        /// By default, the following references are automatically added:
        /// * Endor.CBEL.Assembly;
        /// * Endor.CBEL.ExcelFunctions
        /// </summary>
        /// <param name="reference"></param>
        public void AddUsingReference(string reference, bool isStatic = false) => Usings.Add((reference, isStatic));

        public virtual bool IsVariablePredefined(string variableName)
        {
            return false;
        }

        public virtual bool OverrideVariable(string variableName)
        {
            return (variableName.ToLower() == "totalquantity" || variableName.ToLower() == "price");
        }
    }
}