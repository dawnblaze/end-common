﻿using Endor.EF;
using Endor.Models;
using Irony.Parsing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Endor.CBEL.ObjectGeneration
{
    public partial class CBELAssemblyGenerator
    {
        public static CBELAssemblyGenerator Create(ApiContext context, short bid, int id, int classTypeID, int version = 1)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context), "ApiContext cannot be null");

            if (classTypeID != ClassType.Assembly.ID())
                throw new ArgumentException("Invalid Class Type ID", nameof(classTypeID));

            var workingAssemblyData = context.AssemblyData
                .Include(t => t.Variables).ThenInclude(v => v.Formulas)
                .Include(t => t.Tables)
                .FirstOrDefault(t => t.BID == bid && t.ID == id);

            if (workingAssemblyData == null)
                throw new ArgumentException($"No AssemblyData found with ID={id}");

            if (workingAssemblyData.AssemblyType != AssemblyType.Product && 
                workingAssemblyData.AssemblyType != AssemblyType.MachineTemplate)
                throw new ArgumentException($"AssemblyData with ID={id} has an invalid AssemblyType");

            return Create(context, workingAssemblyData, version);
        }

        public static CBELAssemblyGenerator Create(ApiContext context, AssemblyData assemblyData, int version = 1)
        {
            CBELAssemblyGenerator gen = new CBELAssemblyGenerator(assemblyData.BID, assemblyData.ID, ClassType.Assembly.ID(), $"Assembly: {assemblyData.Name}", version);
            gen.InitSettings(context, assemblyData, assemblyData.Name, assemblyData.ID);

            return gen;
        }

        protected override void InitSettings(ApiContext context, AssemblyData assemblyData, string name, int id, object parentObject = null)
        {
            // rename the variable names for the tier tables
           if (assemblyData.Tables != null && assemblyData.Tables.Count > 0)
           {
               var tierTables = new List<string>(){"TierMarginTable", "TierDiscountTable", "TierMarkupTable", "TierPriceTable"};
               foreach (var assemblyDataTable in assemblyData.Tables)
               {
                   if (tierTables.Contains(assemblyDataTable.VariableName))
                   {
                       assemblyDataTable.VariableName = assemblyDataTable.VariableName.Substring(4);
                   }
               }
           }

            base.InitSettings(context, assemblyData, name, id);

            AddSetter("FixedPrice", assemblyData.FixedPrice.HasValue ? assemblyData.FixedPrice.ToString() + "m" : null);
            AddSetter("FixedMargin", assemblyData.FixedMargin.HasValue ? assemblyData.FixedMargin.ToString() + "m" : null);
            AddSetter("FixedMarkup", assemblyData.FixedMarkup.HasValue ? assemblyData.FixedMarkup.ToString() + "m" : null);
        }
    }
}
