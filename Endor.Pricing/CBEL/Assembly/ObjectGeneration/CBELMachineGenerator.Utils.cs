﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Irony.Parsing;
using Endor.EF;
using Endor.Models;
using Endor.CBEL.Common;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Endor.CBEL.ObjectGeneration
{
    public partial class CBELMachineGenerator
    {
        public static CBELMachineGenerator Create(ApiContext context, short bid, int id, int classTypeID, int version = 1)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context), "ApiContext cannot be null");

            if (classTypeID != ClassType.Machine.ID() && classTypeID != ClassType.Assembly.ID())
                throw new ArgumentException("Invalid Class Type ID", nameof(classTypeID));

            if (classTypeID == ClassType.Machine.ID())
            {
                var machineData = context.MachineData
                    .Include(t => t.Profiles)
                    .Include(t => t.Instances)
                    .Include(t => t.MachineTemplate).ThenInclude(mt => mt.Variables).ThenInclude(v => v.Formulas)
                    .Include(t => t.MachineTemplate).ThenInclude(mt => mt.Tables)
                    .FirstOrDefault(t => t.BID == bid && t.ID == id);

                if (machineData == null)
                    throw new ArgumentException($"No MachineData found with ID={id}");

                if (machineData.MachineTemplate == null)
                    throw new ArgumentException($"No MachineTemplate found for MachineData with ID={id}");

                if (machineData.MachineTemplate.AssemblyType != AssemblyType.MachineTemplate)
                    throw new ArgumentException($"MachineTemplate for MachineData with ID={id} has an invalid AssemblyType");

                return Create(context, machineData: machineData, version: version);
            }

            var machineTemplate = context.AssemblyData
                .Include(t => t.Variables)
                .Include(t => t.Tables)
                .FirstOrDefault(t => t.BID == bid && t.ID == id);

            if (machineTemplate == null)
                throw new ArgumentException($"No MachineTemplate found with ID={id}");

            if (machineTemplate.AssemblyType != AssemblyType.MachineTemplate)
                throw new ArgumentException($"MachineTemplate with ID={id} has an invalid AssemblyType");

            return Create(context, machineTemplate: machineTemplate, version: version);
        }

        public static CBELMachineGenerator Create(ApiContext context, MachineData machineData, int version = 1)
        {
            CBELMachineGenerator gen = new CBELMachineGenerator(machineData.BID, machineData.ID, ClassType.Machine.ID(), $"Machine:  {machineData.Name}", version);
            gen.InitSettings(context, machineData.MachineTemplate, machineData.Name, machineData.ID, machineData);

            return gen;
        }

        public static CBELMachineGenerator Create(ApiContext context, AssemblyData machineTemplate, int version = 1)
        {
            CBELMachineGenerator gen = new CBELMachineGenerator(machineTemplate.BID, machineTemplate.ID, ClassType.Assembly.ID(), $"Machine Template:  {machineTemplate.Name}", version);
            gen.InitSettings(context, machineTemplate, machineTemplate.Name, machineTemplate.ID);

            return gen;
        }

        protected override void InitSettings(ApiContext context, AssemblyData machineTemplate, string name, int id, object parentObject = null)
        {
            base.InitSettings(context, machineTemplate, name, id);

            MachineData machine = null;
            if (parentObject is MachineData)
                machine = (MachineData)parentObject;

            // Add the Instances
            if (machine?.Instances != null)
            {
                machine.Instances.ToList().ForEach(i =>
                {
                    if (i.IsActive) AddInstance(i.Name);
                });

                // Set default if there's only 1 active Instance, otherwise it is blank
                DefaultInstance = InstanceList.Count == 1 ? InstanceList[0] : null;
            }

            if (machine?.EstimatingCostPerHourFormula != null && decimal.TryParse(machine.EstimatingCostPerHourFormula,out decimal machineCostPerHour))
            {
                MachineCostPerHour = machineCostPerHour;
            }

            // Add the profiles
            if (machine?.Profiles != null)
            {
                foreach (var profile in machine.Profiles)
                {
                    AddProfile(profile.Name, profile.MachineLayoutType);

                    // Add the Profile's table to table variable
                    if (profile.MachineProfileTables != null)
                    {
                        foreach (var profileTable in profile.MachineProfileTables)
                        {
                            if (profileTable.Table != null)
                            {
                                CBELTableGenerator genTable = AddTableGenerator(machineTemplate.Variables, profileTable.Table, profileTable);
                                CBELTableGenerator TableOV = (CBELTableGenerator)Variables.FirstOrDefault(t => t.Name == profileTable.Table.VariableName);
                                TableOV.ProfileTableOverrides.Add(profile.Name, genTable);
                            }
                        }
                    }

                    if (profile.IsDefault)
                    {
                        DefaultProfile = profile.Name;
                        DefaultLayoutType = profile.MachineLayoutType.ToString();
                    }

                    if (!string.IsNullOrWhiteSpace(profile.AssemblyOVDataJSON))
                    {
                        Dictionary<string, VariableValue> ovData = JsonConvert.DeserializeObject<Dictionary<string, VariableValue>>(profile.AssemblyOVDataJSON);

                        if ((ovData?.Keys?.Count ?? 0) > 0)
                        {
                            foreach (KeyValuePair<string, VariableValue> kvp in ovData)
                            {
                                if (kvp.Value.ValueOV)
                                {
                                    CBELVariableGenerator VariableToOV = Variables.FirstOrDefault(t => t.Name == kvp.Key);

                                    if (VariableToOV != null)
                                        VariableToOV.ProfileOverrides.Add(profile.Name, kvp.Value.Value);
                                }
                            }
                        }
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(machine?.AssemblyOVDataJSON))
            {
                Dictionary<string, VariableValue> ovData = JsonConvert.DeserializeObject<Dictionary<string, VariableValue>>(machine.AssemblyOVDataJSON);

                //if (ovData?.VariableData != null)
                if ((ovData?.Keys?.Count ?? 0) > 0)
                {
                    foreach (KeyValuePair<string, VariableValue> ov in ovData)
                    {
                        if (ov.Value.ValueOV)
                        {
                            CBELVariableGenerator VariableOV = Variables.FirstOrDefault(t => t.Name == ov.Key);

                            if (VariableOV != null)
                                VariableOV.PropertyOverrides.Add(ov.Key, ov.Value.Value);
                        }
                    }
                }
            }
        }
    }
}
