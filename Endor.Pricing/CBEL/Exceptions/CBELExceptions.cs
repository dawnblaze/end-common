﻿using Endor.CBEL.Interfaces;
using Endor.CBEL.Common;
using System;

namespace Endor.CBEL.Exceptions
{
    // The base class for all CBEL Exceptions.
    public class CBELException : Exception
    {
        public CBELException(string message) : base(message) { }

        public CBELException(string message, Exception innerException) : base(message, innerException) { }
    }

    public class CBELCompilationException : CBELException
    {
        public CBELCompilationException(string message) : base(message) { }

        public CBELCompilationException(string message, Exception innerException) : base(message, innerException) { }

        public CBELCompilationException(string message, AssemblyCompilationResponse assemblyResponse) : base(message)
        {
            this.AssemblyResponse = assemblyResponse;
        }

        public CBELCompilationException(string message, AssemblyCompilationResponse assemblyResponse, Exception innerException) : base(message, innerException)
        {
            this.AssemblyResponse = assemblyResponse;
        }

        public AssemblyCompilationResponse AssemblyResponse { get; set; }
    }


    public class CBELRuntimeException : CBELException
    {
        /// <summary>
        /// This property adds the Assembly Component to the exception if available.
        /// </summary>
        public ICBELAssembly AssemblyComponent { get; set; }

        /// <summary>
        /// This property adds the Element/Variable Component to the exception if available.
        /// </summary>
        public ICBELVariable ElementComponent { get; set; }

        /// <summary>
        /// Implement the exception handler where the Element is passed in.  The Assembly is pulled from the Element.Parent.
        /// The inner exception may also be passed if available.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="inner"></param>
        public CBELRuntimeException(ICBELVariable element, Exception inner = null) : base("CBEL Exception in Element " + ( element == null ? "undefined" : element.Name
                                                                                    + " in Assembly " + ( element.Parent == null ? "undefined" : element.Parent.Name ) ), inner)
        {
            ElementComponent = element;
            AssemblyComponent = ( element == null ? null : element.Parent );
        }

        /// <summary>
        /// Implement the exception handler where the Assembly is passed in.
        /// The inner exception may also be passed if available.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="inner"></param>
        public CBELRuntimeException(ICBELAssembly assemblycomponent, Exception inner = null) : base("CBEL Exception in Assembly "
                                                                                                + ( assemblycomponent == null ? "undefined" : assemblycomponent.Name ), inner)
        {
            AssemblyComponent = assemblycomponent;
        }
        /// <summary>
        /// Implement the exception handler where a customer message and the Element are passed in.  The Assembly is pulled from the Element.Parent.
        /// The inner exception may also be passed if available.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="element"></param>
        /// <param name="inner"></param>
        public CBELRuntimeException(string message, ICBELVariable element, Exception inner = null) : base(message + " in Element " + (element == null ? "undefined" :
                                                                 element.Name + " in Assembly " + (element.Parent == null ? "undefined" : element.Parent.Name)), inner)
        {
            ElementComponent = element;
            AssemblyComponent = (element == null ? null : element.Parent);
        }

        /// <summary>
        /// Implement the exception handler where a custom message and the Assembly are passed in.
        /// The inner exception may also be passed if available.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="element"></param>
        /// <param name="inner"></param>
        public CBELRuntimeException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message + "CBEL Exception in Assembly " 
                                                                                                                + (assemblycomponent == null ? "undefined" : assemblycomponent.Name), inner)
        {
            AssemblyComponent = assemblycomponent;
        }


        /// <summary>
        /// Implement the base Rxception handler.  
        /// </summary>
        /// <param name="message"></param>
        public CBELRuntimeException(string message) : base(message) { }

        public CBELRuntimeException(string message, Exception innerException) : base(message, innerException) { }
    };

    // General Exceptions        
    [Serializable]
    public class FormulaSyntaxException : CBELRuntimeException
    {
        public FormulaSyntaxException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public FormulaSyntaxException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public FormulaSyntaxException(ICBELVariable element, Exception inner = null) : base("Formula Syntax Exception", element, inner) { }
        public FormulaSyntaxException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Formula Syntax Exception", assemblycomponent, inner) { }
        public FormulaSyntaxException(string message) : base(message) { }
    }
    [Serializable]
    public class FormulaExecutionException : CBELRuntimeException
    {
        public FormulaExecutionException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public FormulaExecutionException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public FormulaExecutionException(ICBELVariable element, Exception inner = null) : base("Formula Execution Exception", element, inner) { }
        public FormulaExecutionException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Formula Execution Exception", assemblycomponent, inner) { }
        public FormulaExecutionException(string message) : base(message) { }
        public FormulaExecutionException() : base("Formula Execution Exception") { }
        public FormulaExecutionException(string message, Exception inner) : base(message, inner) { }
    }

    [Serializable]
    public class CircularReferenceFormulaException : FormulaExecutionException
    {
        public CircularReferenceFormulaException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public CircularReferenceFormulaException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public CircularReferenceFormulaException(ICBELVariable element, Exception inner = null) : base("Recursive Formula Exception", element, inner) { }
        public CircularReferenceFormulaException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Recursive Formula Exception", assemblycomponent, inner) { }
        public CircularReferenceFormulaException(string message) : base(message) { }
        public CircularReferenceFormulaException() : base("Recursive Formula Exception") { }
        public CircularReferenceFormulaException(string message, Exception inner) : base(message, inner) { }
    }

    [Serializable]
    public class InvalidLabelAssignmentException : FormulaExecutionException
    {
        public InvalidLabelAssignmentException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public InvalidLabelAssignmentException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public InvalidLabelAssignmentException(ICBELVariable element, Exception inner = null) : base("Invalid Attempt to Override a Label.", element, inner) { }
        public InvalidLabelAssignmentException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Invalid Attempt to Override a Label.", assemblycomponent, inner) { }
        public InvalidLabelAssignmentException(string message) : base(message) { }
    }

    // String Exceptions
    [Serializable]
    public class StringException : CBELRuntimeException
    {
        public StringException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public StringException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public StringException(ICBELVariable element, Exception inner = null) : base("String Exception", element, inner) { }
        public StringException(ICBELAssembly assemblycomponent, Exception inner = null) : base("String Exception", assemblycomponent, inner) { }
        public StringException(string message) : base(message) { }
        public StringException() : base("String Exception") { }
        public StringException(string message, Exception inner) : base(message, inner) { }
    }


    // Numeric Exceptions
    public class NumberException : CBELRuntimeException
    {
        public NumberException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public NumberException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public NumberException(ICBELVariable element, Exception inner = null) : base("Number Exception", element, inner) { }
        public NumberException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Number Exception", assemblycomponent, inner) { }
        public NumberException(string message) : base(message) { }
    }

    // Boolean Exceptions
    public class BooleanException : CBELRuntimeException
    {
        public BooleanException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public BooleanException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public BooleanException(ICBELVariable element, Exception inner = null) : base("Boolean Formula Exception", element, inner) { }
        public BooleanException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Boolean Formula Exception", assemblycomponent, inner) { }
        public BooleanException(string message) : base(message) { }
    }

    // DateTime Exception
    public class DateTimeException : CBELRuntimeException
    {
        public DateTimeException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public DateTimeException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public DateTimeException(ICBELVariable element, Exception inner = null) : base("Date/Time Formula Exception", element, inner) { }
        public DateTimeException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Date/Time Formula Exception", assemblycomponent, inner) { }
        public DateTimeException(string message) : base(message) { }
    }

    public class DateConversionException : DateTimeException
    {
        public DateConversionException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public DateConversionException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public DateConversionException(ICBELVariable element, Exception inner = null) : base("Invalid Date/Time Conversion", element, inner) { }
        public DateConversionException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Invalid Date/Time Conversion", assemblycomponent, inner) { }
        public DateConversionException(string message) : base(message) { }
    }

    public class DateComparisonException : DateTimeException
    {
        public DateComparisonException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public DateComparisonException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public DateComparisonException(ICBELVariable element, Exception inner = null) : base("Invalid Date/Time Comparison", element, inner) { }
        public DateComparisonException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Invalid Date/Time Comparison", assemblycomponent, inner) { }
        public DateComparisonException(string message) : base(message) { }
    }

    public class VariableNotFoundException : CBELRuntimeException
    {
        public VariableNotFoundException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public VariableNotFoundException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public VariableNotFoundException(ICBELVariable element, Exception inner = null) : base("Variable not found", element, inner) { }
        public VariableNotFoundException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Variable not found", assemblycomponent, inner) { }
        public VariableNotFoundException(string message) : base(message) { }
    }

    public class TableLookupException : CBELRuntimeException
    {
        public TableLookupException(string message, ICBELVariable element, Exception inner = null) : base(message, element, inner) { }
        public TableLookupException(string message, ICBELAssembly assemblycomponent, Exception inner = null) : base(message, assemblycomponent, inner) { }
        public TableLookupException(ICBELVariable element, Exception inner = null) : base("Table Lookup Exception", element, inner) { }
        public TableLookupException(ICBELAssembly assemblycomponent, Exception inner = null) : base("Table Lookup Exception", assemblycomponent, inner) { }
        public TableLookupException(string message) : base(message) { }
    }

}