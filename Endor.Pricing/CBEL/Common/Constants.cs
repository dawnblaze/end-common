﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Common
{
    public static class Constants
    {
        // MaxStackDepth is used to flag the maximum number of recursive passes before aborting
        // This is used when the value of a variable converges through repetition.
        public const int MaxStackDepth = 1;

#if !PLATFORM_UNIX
        public const string CR = "\r\n";
#else
        public const string CR ="\n";
#endif // !PLATFORM_UNIX
    }
}
