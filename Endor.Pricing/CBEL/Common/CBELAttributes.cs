﻿using System;

namespace Endor.CBEL.Elements
{
    internal class ReadOnlyVariable : Attribute
    {
        public ReadOnlyVariable(bool isReadOnly = true)
        {
            IsReadOnly = isReadOnly;
        }

        public bool IsReadOnly { get; set; }
    }
}