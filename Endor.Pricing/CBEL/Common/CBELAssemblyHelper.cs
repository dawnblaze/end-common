﻿using Endor.AzureStorage;
using Endor.CBEL.Compute;
using Endor.CBEL.Elements;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Grammar;
using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing;
using Endor.Tenant;
using Irony.Parsing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.CBEL.Common
{
    public static class CBELAssemblyHelper
    {
        public static bool SaveCopyToFile = false;
        public static string SaveToDirectory = null;
        public static bool SaveCopyToUniqueFileName = false;

        internal static void MakeLocalCopy(CBELBaseAssemblyGenerator gen, string assemblyCode)
        {
            if (SaveCopyToFile)
            {
                if (string.IsNullOrWhiteSpace(SaveToDirectory))
                {
                    // Log the failure
                }
                else
                {
                    try
                    {
                        string fileDir = Path.Combine(SaveToDirectory, $"bid{gen.BID}", $"{gen.ClassTypeID}", $"{gen.ID}");
                        string fileName = gen.AssemblyClassName;

                        if (!Directory.Exists(fileDir))
                            Directory.CreateDirectory(fileDir);

                        string fullFileName = Path.Combine(fileDir, $"{fileName}.cs");

                        if (SaveCopyToUniqueFileName)
                        {
                            int fileCount = 0;

                            while (File.Exists(fullFileName))
                            {
                                fileCount++;
                                fullFileName = Path.Combine(fileDir, $"{fileName}_{fileCount:00}.cs");
                            }
                        }

                        File.WriteAllText(fullFileName, assemblyCode);
                    }
                    catch
                    {
                        // Log Error
                    }
                }
            }
        }

        public static string AssemblyName(short bid, int assemblyID, int assemblyClassTypeID, int version)
        {
            string assemblyIDStr;

            if (assemblyID >= 0)
                assemblyIDStr = assemblyID.ToString();
            else
                // Test assemblies will fail if a dash is in the class name.
                assemblyIDStr = "_" + Math.Abs(assemblyID).ToString();

            // If ClassType is Machine
            if (assemblyClassTypeID == ClassType.Machine.ID())
                return $"Machine_BID{bid}_CT{assemblyClassTypeID}_ID{assemblyIDStr}_V{version}";
            else
                return $"Assembly_BID{bid}_CT{assemblyClassTypeID}_ID{assemblyIDStr}_V{version}";
        }

        private static void AddComponentPriceResult(ComponentPriceResult result, ICBELComponentVariable component, decimal? lineItemQuantity, ComponentPriceRequest request = null)
        {
            if (component?.Component != null)
            {
                OrderItemComponentType componentType = component.Component.ClassTypeID.ToComponentType();

                var subComponentPriceResult = new ComponentPriceResult()
                {
                    ChildComponents = new List<ComponentPriceResult>(),
                    ComponentID = component.Component.ID,
                    ComponentType = componentType,
                    VariableName = ((ICBELVariable)component).Name,
                    TotalQuantity = component.Component.QuantityValue,
                    TotalQuantityOV = component.Component.QuantityOV.GetValueOrDefault(false),
                    PricePreTax = component.RollupLinkedPriceAndCost ? (decimal?)null : component.Price ?? 0,
                    PriceTotal = component.RollupLinkedPriceAndCost ? (decimal?)null : component.Price ?? 0,
                    CostLabor = componentType == OrderItemComponentType.Labor ? component.Cost : 0,
                    CostMachine = componentType == OrderItemComponentType.Machine || componentType == OrderItemComponentType.MachineTime ? component.Cost : 0,
                    CostMaterial = componentType == OrderItemComponentType.Material ? component.Cost : 0,
                    CostNet = component.Cost,
                    CostUnit = component.ComputedUnitCost,
                    CostOV = component.UnitCostIsOverridden.GetValueOrDefault(false),
                    Name = componentType == OrderItemComponentType.MachineTime ? ((ICBELVariable)component).Name : component.Component.Name,
                    RollupLinkedPriceAndCost = component.RollupLinkedPriceAndCost,
                    LineItemQuantity = lineItemQuantity,
                    // these three values are the defaults from the Assembly/Labor/Machine/Material, they may be OV'd by request in AssignPassthroughRequestValues below
                    IncomeAccountID = component.Component.IncomeAccountID,
                    IncomeAllocationType = component.Component.IncomeAllocationType,
                    ExpenseAccountID = component.Component.ExpenseAccountID,
                };

                subComponentPriceResult.PriceUnit = subComponentPriceResult.PricePreTax / (subComponentPriceResult.TotalQuantity.GetValueOrDefault(0m) == 0m ? 1 : subComponentPriceResult.TotalQuantity);

                if (request != null)
                {
                    subComponentPriceResult.AssemblyQuantityOV = request.AssemblyQuantityOV.GetValueOrDefault(false);
                    subComponentPriceResult.QuantityUnit = request.QuantityUnit;
                }

                var nonAssemblyTypes = new OrderItemComponentType[]{ OrderItemComponentType.Labor, OrderItemComponentType.Material };
                if(nonAssemblyTypes.Contains(subComponentPriceResult.ComponentType))
                {
                    subComponentPriceResult.TotalQuantity = component.ConsumptionQuantity.Value;
                    subComponentPriceResult.TotalQuantityOV = component.ConsumptionQuantity.IsOV;
                    subComponentPriceResult.QuantityUnit = component.ConsumptionQuantity.Unit;
                }


                if (component.Component is IHasComponents componentParent)
                {
                    foreach (ICBELComponentVariable subComponent in componentParent.Components)
                    {
                        AddComponentPriceResult(subComponentPriceResult, subComponent, lineItemQuantity, request?.GetChildComponent(subComponent));
                    }

                    if ((subComponentPriceResult?.ChildComponents?.Count ?? 0) > 0)
                    {
                        subComponentPriceResult.CostLabor = subComponentPriceResult.ChildComponents
                                                                .Where(lvl3Comp => lvl3Comp.ComponentType == OrderItemComponentType.Labor)
                                                                .Sum(lvl3Comp => lvl3Comp.CostLabor);
                        subComponentPriceResult.CostMachine = subComponentPriceResult.ChildComponents
                                                                .Where(lvl3Comp => lvl3Comp.ComponentType == OrderItemComponentType.Machine
                                                                || lvl3Comp.ComponentType == OrderItemComponentType.MachineTime)
                                                                .Sum(lvl3Comp => lvl3Comp.CostMachine);
                        subComponentPriceResult.CostMaterial = subComponentPriceResult.ChildComponents
                                                                .Where(lvl3Comp => lvl3Comp.ComponentType == OrderItemComponentType.Material)
                                                                .Sum(lvl3Comp => lvl3Comp.CostMaterial);

                        subComponentPriceResult.CostNet = subComponentPriceResult.ChildComponents
                                                           .Sum(lvl3Comp => lvl3Comp.CostNet);

                        subComponentPriceResult.CostUnit = subComponentPriceResult.ChildComponents
                                                           .Sum(lvl3Comp => lvl3Comp.CostUnit);
                    }

                    if (componentType != OrderItemComponentType.Machine)
                        subComponentPriceResult.CostNet = subComponentPriceResult.CostUnit * subComponentPriceResult.TotalQuantity;

                    else if((subComponentPriceResult?.ChildComponents?.Count ?? 0) > 0)
                        subComponentPriceResult.CostNet = subComponentPriceResult.ChildComponents
                                                           .Sum(lvl3Comp => lvl3Comp.CostNet);

                    if (component.Component is ICBELAssembly assemblyComponent)
                    {
                        subComponentPriceResult.Variables = assemblyComponent.Variables
                            .Select(x => (ICBELVariableComputed)x.Value)
                            .ToDictionary(x => x.Name, x => new VariableValue()
                            {
                                DataType = x.DataType,
                                Value = x.GetResultValue(),
                                ValueOV = x.IsOV,
                                Alt = x.AltAsString,
                                Unit = x.ValueUnit()
                                
                            });

                        subComponentPriceResult.AssemblyQuantity = assemblyComponent.QuantityValue;
                        subComponentPriceResult.AssemblyQuantityOV = assemblyComponent.QuantityOV.GetValueOrDefault(false);
                    }
                }

                result.ChildComponents.Add(subComponentPriceResult);
            }
        }

        public static ComponentPriceResult ComputePriceResultToComponentPriceResult(ComponentPriceResult result, ICBELComputeResult computeResult, ComponentPriceRequest request = null)
        {
            result.ComponentType = OrderItemComponentType.Assembly;
            result.AssignPassthroughRequestValues(request);

            if (request != null)
            {
                result.ExpenseAccountID = request.ExpenseAccountID;
                result.IncomeAccountID = request.IncomeAccountID;
                result.IncomeAllocationType = request.IncomeAllocationType;
                result.TotalQuantityOV = request.TotalQuantityOV.GetValueOrDefault(false);
                result.QuantityUnit = request.QuantityUnit;
            }

            var priceVar = computeResult.VariableData?.FirstOrDefault(t => t.VariableName == "Price");

            if (decimal.TryParse(priceVar?.Value, out decimal priceDecimal))
            {
                result.PricePreTax = priceDecimal;
                result.PriceUnitOV = priceVar.IsOV;
            }
            else
            {
                result.PricePreTax = computeResult.Assembly.TotalPrice;
            }

            var totalQuantityVar = computeResult.VariableData?.FirstOrDefault(t => t.VariableName == "TotalQuantity");
            if (decimal.TryParse(totalQuantityVar?.Value, out decimal totalQuantityDecimal))
            {
                result.TotalQuantity = totalQuantityDecimal;
                result.TotalQuantityOV = totalQuantityVar.IsOV;
                result.QuantityUnit = totalQuantityVar.Unit;
            }

            var assemblyQuantityVar = computeResult.VariableData?.FirstOrDefault(t => t.VariableName == "AssemblyQuantity");
            if (decimal.TryParse(assemblyQuantityVar?.Value, out decimal assemblyQuantityDecimal))
            {
                result.AssemblyQuantity = assemblyQuantityDecimal;
                result.AssemblyQuantityOV = assemblyQuantityVar.IsOV;
            }
            else
            {
                result.AssemblyQuantity = computeResult.Assembly.QuantityValue;
                result.AssemblyQuantityOV = computeResult.Assembly.QuantityOV.GetValueOrDefault(false);
            }

            if (request != null)
                result.LineItemQuantity = request.LineItemQuantity;

            result.Name = computeResult.Assembly.Name;
            result.CostLabor = computeResult.Assembly.LaborCost;
            result.CostMachine = computeResult.Assembly.MachineCost;
            result.CostMaterial = computeResult.Assembly.MaterialCost;
            result.CostNet = computeResult.Assembly.TotalCost;
            result.CostUnit = computeResult.Assembly.ComputedUnitCost;
            result.CostOV = computeResult.Assembly.UnitCostIsOverridden.GetValueOrDefault(false);
            result.AssignPassthroughRequestValues(request);

            result.ChildComponents = new List<ComponentPriceResult>();
            // This needs to recursively fill out the 
            // componentPriceResult with assembly components (mat/labor) as well as sub assembly components (assembly/machines)

            foreach (ICBELComponentVariable component in computeResult.Assembly.Components)
            {
                AddComponentPriceResult(result, component, request?.LineItemQuantity, request?.GetChildComponent(component));
            }

            result.Variables = computeResult.VariableData.ToDictionary(x => x.VariableName, x => new VariableValue()
            {
                DataType = x.DataType,
                Value = x.Value,
                ValueOV = x.IsOV,
                Alt = x.Alt,
                IsIncluded = x.IsIncluded,
                IsIncludedOV = x.IsIncludedOV,
                Unit = x.Unit
            });

            if (computeResult.HasErrors)
            {
                result.Errors = result.Errors ?? new List<PricingErrorMessage>();

                foreach (var item in computeResult.Errors)
                {
                    var errorMsg = new PricingErrorMessage();

                    if (item is CircularReferenceFormulaException circularReferenceException)
                    {
                        errorMsg.ErrorType = nameof(CircularReferenceFormulaException);
                        errorMsg.VariableName = circularReferenceException.ElementComponent.Name;
                        if (circularReferenceException.ElementComponent is ICBELVariableComputed computedElement)
                        {
                            errorMsg.FormulaText = computedElement.FormulaText;
                        }
                        errorMsg.IsValidation = false;
                        errorMsg.Message = circularReferenceException.Message;

                    }
                    else if (item is CBELRuntimeException cbelException && cbelException.InnerException is DivideByZeroException divideByZeroException)
                    {
                        errorMsg.ErrorType = nameof(DivideByZeroException);

                        errorMsg.VariableName = cbelException.ElementComponent.Name;
                        if (cbelException.ElementComponent is ICBELVariableComputed computedElement)
                        {
                            errorMsg.FormulaText = computedElement.FormulaText;
                        }
                        errorMsg.IsValidation = false;
                        errorMsg.Message = cbelException.Message;
                    }
                    else
                    {
                        errorMsg.ErrorType = item.GetType().Name;
                        errorMsg.IsValidation = false;
                        errorMsg.Message = item.Message;
                    }

                    result.Errors.Add(errorMsg);
                }
            }

            if (computeResult.HasValidationFailures)
            {
                result.Errors = result.Errors ?? new List<PricingErrorMessage>();

                foreach (var item in computeResult.ValidationFailures)
                {
                    var errorMsg = new PricingErrorMessage()
                    {
                        IsValidation = true,
                        ErrorType = "Validation",
                        VariableName = item.ElementComponent.Name,
                        Message = item.Message,
                    };

                    result.Errors.Add(errorMsg);
                }
            }

            return result;
        }

        public static CBELOverriddenValues ComponentPriceRequestToComputeRequest(ComponentPriceRequest request)
        {
            bool? quantityOV;

            if (!request.TotalQuantityOV.HasValue)
                quantityOV = request.AssemblyQuantityOV;
            else if (!request.AssemblyQuantityOV.HasValue)
                quantityOV = request.TotalQuantityOV;
            else
                quantityOV = request.TotalQuantityOV.Value || request.AssemblyQuantityOV.Value;

            var computeRequest = new CBELOverriddenValues()
            {
                ComponentID = request.ComponentID,
                VariableName = request.VariableName,
                Quantity = request.TotalQuantity,
                QuantityOV = quantityOV,
                IsIncluded = request.IsIncluded,
                IsIncludedOV = request.IsIncludedOV,
                IsVended = request.IsVended,
                ParentQuantity = request.ParentQuantity,
                LineItemQuantity = request.LineItemQuantity,
                CompanyID = request.CompanyID,
                CostUnit = request.CostUnit,
                CostUnitOV = request.CostUnitOV,
            };

            computeRequest.VariableData = request.Variables == null ? new List<IVariableData>() :
                request.Variables.Select(v => new VariableData()
                {
                    DataType = v.Value.DataType,
                    IsOV = v.Value.ValueOV,
                    VariableName = v.Key,
                    Value = v.Value.Value,
                    Alt = v.Value.Alt,
                    IsIncluded = v.Value.IsIncluded.GetValueOrDefault(true),
                    IsIncludedOV = v.Value.IsIncludedOV.GetValueOrDefault(false),
                    Unit = v.Value.Unit,
                }).ToList<IVariableData>();

            IVariableData quantityVar = computeRequest.VariableData.FirstOrDefault(t => t.VariableName == "TotalQuantity");
            IVariableData assemblyQuantityVar = computeRequest.VariableData.FirstOrDefault(t => t.VariableName == "AssemblyQuantity");

            if (quantityVar != null && request.TotalQuantityOV.GetValueOrDefault(false))
            {
                quantityVar.Value = request.TotalQuantity.ToString();
                quantityVar.IsOV = true;
            }
            else
            {
                if(quantityVar == null)
                    quantityVar = new VariableData()
                    {
                        DataType = DataType.Number,
                        IsOV = computeRequest.QuantityOV.GetValueOrDefault(false),
                        Value = computeRequest.Quantity.ToString(),
                        VariableName = "TotalQuantity",
                    };

                computeRequest.VariableData.Add(quantityVar);
            }

            if (assemblyQuantityVar != null && request.AssemblyQuantityOV.GetValueOrDefault(false))
            {
                assemblyQuantityVar.Value = request.AssemblyQuantity.ToString();
                assemblyQuantityVar.IsOV = true;
            }
            else
            {
                if(assemblyQuantityVar == null)
                    assemblyQuantityVar = new VariableData()
                    {
                        DataType = DataType.Number,
                        IsOV = computeRequest.QuantityOV.GetValueOrDefault(false),
                        Value = computeRequest.Quantity.ToString(),
                        VariableName = "AssemblyQuantity",
                    };

                computeRequest.VariableData.Add(assemblyQuantityVar);
            }

            if (request.ChildComponents != null)
            {
                foreach (var subComponentRequest in request.ChildComponents)
                {
                    computeRequest.OVChildValues.Add(ComponentPriceRequestToComputeRequest(subComponentRequest));
                }
            }

            return computeRequest;
        }

        public static async Task<(ICBELAssembly assembly, string assemblyCode)> LoadAssemblyFromStorage(int assemblyID, int assemblyClassTypeID, int assemblyVersion, ITenantDataCache cache, RemoteLogger logger, ApiContext ctx, short bid)
        {
            EntityStorageClient client = new EntityStorageClient((await cache.Get(bid)).StorageConnectionString, bid);

            var assemblyDoc = new DMID() { id = assemblyID, ctid = assemblyClassTypeID };

            string className = CBELAssemblyHelper.AssemblyName(bid, assemblyID, assemblyClassTypeID, assemblyVersion);

            ICBELAssembly assembly = null;
            string assemblyCode = null;

            if (await client.Exists($"source/{className}.cs", StorageBin.Permanent, Bucket.Data, assemblyDoc))
            {
                var stream = await client.GetStream(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{className}.cs");
                using (stream)
                {
                    using StreamReader reader = new StreamReader(stream);
                    assemblyCode = reader.ReadToEnd();
                }
            }

            if (await client.Exists($"bin/{className}.dll", StorageBin.Permanent, Bucket.Data, assemblyDoc))
            {
                var stream = await client.GetStream(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{className}.dll");
                using (stream)
                {
                    try
                    {
                        byte[] data = new byte[stream.Length];
                        stream.Read(data, 0, data.Length);
                        System.Reflection.Assembly asm = System.Reflection.Assembly.Load(data);
                        List<Type> results = asm.GetTypes()
                                                .Where(type => typeof(ICBELAssembly).IsAssignableFrom(type))
                                                .ToList();

                        assembly = (ICBELAssembly)Activator.CreateInstance(results.First(), ctx, bid, cache, logger);
                    }

                    catch (Exception ex)
                    {
                        await logger.PricingError(bid, "Error Loading Assembly from Storage.", ex);
                        assembly = null;
                    }
                }
            }

            return (assembly, assemblyCode);
        }

        public static async Task<AssemblyCompilationResponse> LoadAssembly(ApiContext context, int id, int classtypeid, int version, ITenantDataCache cache, RemoteLogger logger, short bid, bool createIfNotFound = true, bool isMachineTemplate = false)
        {
            (ICBELAssembly assembly, string assemblyCode) = await LoadAssemblyFromStorage(id, classtypeid, version, cache, logger, context, bid);

            string fallbackName;

            if (classtypeid == ClassType.Machine.ID())
                fallbackName = $"Machine:  (ID: {id},  ClassTypeID: {classtypeid})";
            else if (isMachineTemplate)
                fallbackName = $"Machine Template:  (ID: {id},  ClassTypeID: {classtypeid})";
            else
                fallbackName = $"Assembly:  (ID: {id},  ClassTypeID: {classtypeid})";


            if (assembly == null && createIfNotFound)
            {
                CBELBaseAssemblyGenerator gen;

                try
                {
                    if (classtypeid == ClassType.Machine.ID() || isMachineTemplate)
                    {
                        //no assembly found so we create CBELMachineGenerator from an MachineData
                        gen = CBELMachineGenerator.Create(context, bid, id, classtypeid, version);
                    }
                    else
                    {
                        //no assembly found so we create CBELAssemblyGenerator from an AssemblyData
                        gen = CBELAssemblyGenerator.Create(context, bid, id, classtypeid, version);
                    }

                    AssemblyCompilationResponse compilationResponse = await gen.Save(cache);

                    if (compilationResponse.Success)
                        compilationResponse.Assembly = await gen.GetAssemblyInstance(context, cache, logger, compilationResponse.AssemblyType);

                    return compilationResponse;
                }

                catch (Exception err)
                {
                    await logger.PricingError(bid, $"Failed to generate {fallbackName}", err);

                    return new AssemblyCompilationResponse(fallbackName)
                    {
                        Errors = new List<PricingErrorMessage>()
                        {
                            new PricingErrorMessage()
                            {
                                ErrorType = "Exception",
                                Message = err.Message,
                            }
                        }
                    };
                }
            }

            return new AssemblyCompilationResponse(fallbackName) { Assembly = assembly, AssemblyCode = assemblyCode };
        }

        internal static List<VariableOVData> OVValuesToOVDataList(ICBELOverriddenValues OVValues)
        {
            if (OVValues == null)
                return null;

            List<VariableOVData> result = new List<VariableOVData>();
            VariableOVData ovData;

            foreach (IVariableData variableData in OVValues.VariableData)
            {
                ovData = new VariableOVData()
                {
                    VariableName = variableData.VariableName,
                    Value = variableData.Value,
                    IsOV = variableData.IsOV,
                    CostUnit = variableData.CostUnit,
                    CostUnitOV = variableData.CostUnitOV,
                    Unit = variableData.Unit
                };

                result.Add(ovData);
            }

            foreach (ICBELOverriddenValues ovChildValue in OVValues.OVChildValues)
            {
                ovData = result.FirstOrDefault(x => x.VariableName == ovChildValue.VariableName);

                if (ovData == null)
                {
                    ovData = new VariableOVData()
                    {
                        VariableName = ovChildValue.VariableName,
                        IsOV = false,
                    };
                    result.Add(ovData);
                }

                ovData.ComponentID = ovChildValue.ComponentID;
                ovData.Quantity = ovChildValue.Quantity;
                ovData.QuantityOV = ovChildValue.QuantityOV;
                ovData.IsIncluded = ovChildValue.IsIncluded;
                ovData.IsIncludedOV = ovChildValue.IsIncludedOV;
                ovData.IsVended = ovChildValue.IsVended;
                ovData.CompanyID = ovChildValue.CompanyID;
                ovData.ParentQuantity = OVValues.Quantity;
                ovData.LineItemQuantity = OVValues.LineItemQuantity;
                ovData.CostUnit = ovChildValue.CostUnit;
                ovData.CostUnitOV = ovChildValue.CostUnitOV;

                ovData.ChildOVValues = OVValuesToOVDataList(ovChildValue);
            }

            if (result.Count == 0)
                return null;

            return result;
        }

        internal static ParseFxResult ParseFormula(CBELBaseAssemblyGenerator assemblyGenerator, CBELVariableGenerator variableGenerator, string variableName, DataType dataType, string fxText, Parser parser)
        {
            // We remove the = here from the start of the value text
            string fx = fxText.Trim();

            if (fx[0] == '=')
                fx = fx.Substring(1).TrimStart();

            if (string.IsNullOrWhiteSpace(fx))
                return new ParseFxResult()
                {
                    IsConstant = false,
                    FormulaText = null,
                    FxFunction = null,
                    ValueConstant = null
                };

            switch (dataType)
            {
                case DataType.String:
                    fx = $"ToString({fx})";
                    break;
                case DataType.Number:
                    fx = $"ToDecimal({fx})";
                    break;
                case DataType.Boolean:
                    fx = $"ToBool({fx})";
                    break;
            }

            ParseTree tree = parser.Parse(fx);
            if (tree.ParserMessages.Count > 0)
            {
                foreach (var parserMessage in tree.ParserMessages)
                {
                    assemblyGenerator.Errors.Add(
                        new PricingErrorMessage()
                        {
                            ErrorType = "Error",
                            VariableName = variableName,
                            FormulaText = fxText,
                            IsValidation = false,
                            Message = $"Location:{parserMessage.Location}-{parserMessage.Message}",
                        }
                    );
                }

                return new ParseFxResult()
                {
                    IsConstant = false,
                    FormulaText = fxText,
                    FxFunction = null,
                    ValueConstant = null
                };
            }

            if (variableGenerator != null)
            {
                List<string> unknownTokens = tree.ExternalDependencies();

                foreach (string tokenName in unknownTokens)
                {
                    variableGenerator.ReliesOn.Add(tokenName.Replace(".Value", ""));

                    if (!assemblyGenerator.Variables.Any(x => x.Name.ToLower() == tokenName.ToLower()))
                        assemblyGenerator.AddExternalDependencies(tokenName);
                }
            }

            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;

            return new ParseFxResult()
            {
                IsConstant = false,
                FormulaText = fxText,
                FxFunction = program.ToCS(),
                ValueConstant = null
            };
        }

        internal static Boolean TryStringToBool(string value, out bool result)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                string trimValue = value.Trim();

                if (int.TryParse(trimValue, out int intValue))
                {
                    result = intValue != 0;
                    return true;
                }

                var trueBeginnings = new List<char> { 'T', 't', 'Y', 'y' };
                var firstLetter = trimValue.First();

                result = trueBeginnings.Contains(firstLetter);
                return true;
            }

            result = false;
            return false;
        }
    }
}
