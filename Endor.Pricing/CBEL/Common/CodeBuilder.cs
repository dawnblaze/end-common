﻿using Endor.CBEL.Common;
using System;
using System.Linq;
using System.Text;

namespace Endor.CBEL.Common
{
    public class CodeBuilder
    {
        private readonly StringBuilder _stringBuilder = new StringBuilder(16000);

        private short _IndentIndex = 0;

        public int Length
        {
            get
            {
                return _stringBuilder.Length;
            }
        }

        public void IncIndent()
        {
            _IndentIndex++;
        }
        public void DecIndent()
        {
            _IndentIndex--;

            if (_IndentIndex < 0)
                throw new Exception("Indention index can not be less than zero.");
        }

        public override string ToString()
        {
            return _stringBuilder.ToString();
        }

        public void Append(string s, bool indent = true, bool addCR = true)
        {
            if (indent && _IndentIndex > 0)
                _stringBuilder.Append(new string(' ', _IndentIndex*2));

            _stringBuilder.Append(s);

            if (addCR)
                _stringBuilder.Append(Constants.CR);
        }

        public void AppendCR()
        {
            _stringBuilder.Append(Constants.CR);
        }

        public void AppendMethodCode(string methodName,
                                            Action<CodeBuilder> addCodeMethod,
                                            string visibility = "public",
                                            string returnType = "void",
                                            string parameters = null,
                                            string baseParameters = null,
                                            string comments = null,
                                            bool isOverridden = false)
        {
            if (!string.IsNullOrWhiteSpace(comments))
                comments.Split("\r\n").ToList().ForEach((s) => Append($"// {s}"));

            _stringBuilder.Append(new string(' ', _IndentIndex * 2));
            _stringBuilder.Append($"{visibility} ");

            if (isOverridden)
                _stringBuilder.Append("override ");

            if (!string.IsNullOrWhiteSpace(returnType))
                _stringBuilder.Append($"{returnType} ");

            _stringBuilder.Append($"{methodName}({(string.IsNullOrWhiteSpace(parameters) ? string.Empty : parameters)})");

            // baseParameters of null - base not called
            // baseParameters of empty string - base called with no parameters
            if (baseParameters != null)
                _stringBuilder.Append($": base({baseParameters})");

            if (addCodeMethod == null)
            {
                _stringBuilder.Append("{ }");
                AppendCR();
            }
            else
            {
                AppendCR();
                Append("{");
                IncIndent();
                addCodeMethod(this);
                DecIndent();
                Append("}");
            }
        }
    }
}
