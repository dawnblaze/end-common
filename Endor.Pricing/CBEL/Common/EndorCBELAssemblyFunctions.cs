﻿using Endor.CBEL.Exceptions;
using Endor.CBEL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.CBEL.Common
{
    
    public static class EndorCBELAssemblyFunctions
    {

        public static string ERROR(this ICBELAssembly assembly, string errorMessageText)
        {
            assembly.LogException(new CBELRuntimeException(errorMessageText));
            return "true";
        }

    }
}
