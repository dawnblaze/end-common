﻿using Endor.CBEL.Interfaces;
using Microsoft.CodeAnalysis.Scripting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Endor.CBEL.Common
{
    public class AssemblyCompilationResponse : IComponentCompilationResponse
    {
        public AssemblyCompilationResponse(string name)
        {
            this.Name = name;
        }

        public string Name { get; private set; }
        public List<PricingErrorMessage> Errors { get; set; }
        public bool Success => ErrorCount == 0;
        public List<string> ExternalDependencies { get; set; }
        public int ErrorCount => Errors?.Count ?? 0;

        [JsonIgnore]
        public ICBELAssembly Assembly { get; set; }

        [JsonIgnore]
        public string AssemblyCode { get; set; }

        [JsonIgnore]
        public Type AssemblyType { get; set; }

        [JsonIgnore]
        public Script Script { get; set; }
    }
}