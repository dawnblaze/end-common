﻿using Endor.CBEL.Autocomplete;
using Endor.CBEL.Exceptions;
using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Endor.CBEL.Common
{
    public static class ExcelFunctions
    {
        private static bool TryParseDate(string s, out DateTime dt)
        {
            var culture = CultureInfo.CreateSpecificCulture("en-US");
            var styles = DateTimeStyles.None;

            if (!DateTime.TryParse(s, culture, styles, out DateTime result))
            {
                dt = DateTime.MinValue;
                return false;
            }

            dt = result;
            return true;
        }

        #region Functions Named A through I

        /// <summary>
        /// Returns the absolute value or a number without its sign.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>        
        [CBELFunctionInfo(
             Name = "ABS",
             InsertedText = "ABS(|Value|)",
             DataType = 2,
             Hint = "The absolute value or a number without its sign.",
             Description = "The absolute value of a number is just the number without its sign.  If number is negative,this function simply removes the sign, returning a positive number. If number is not numeric, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The real number of which you want the absolute value.")]
        public static decimal? ABS(decimal? number)
        {
            if (!number.HasValue)
                return null;

            return Math.Abs(number.Value);
        }

        /// <summary>
        /// The logical AND for any number of arguments.
        /// </summary>
        /// <param name="Conditions"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "AND",
             InsertedText = "AND(|Value|)",
             DataType = 3,
             Hint = "The logical AND for any number of arguments.",
             Description = "This function returns either True or False. This function will return True if all the logical arguments are True. This function will return False if all the logical arguments are False. The logical values can be constants, logical equations, cell references or named ranges. Any empty cells are ignored. If any argument does not evaluate to either True or False, then #VALUE! is returned. This function can be used to check if values meet certain criteria.",
             MinParams = 2,
             MaxParams = 255,
             Param1Name = "Value1",
             Param1DataType = 3,
             Param1Hint = "The first logical value. ",
             Param2Name = "Value2",
             Param2DataType = 3,
             Param2Hint = "The second logical value.")]
        public static bool? AND(params bool?[] Conditions)
        {
            if (Conditions == null || Conditions.Any(c => !c.HasValue))
                return false;

            return Conditions.All(c => c.Value);
        }

        /// <summary>
        /// Returns the arc-cosine of a number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ACOS",
             InsertedText = "ACOS(|Value|)",
             DataType = 2,
             Hint = "The arc-cosine of a number.",
             Description = "The arc-cosine is the inverse of the COS function. This function is also called the inverse cosine function. The number is the cosine of an angle in radians. If number is not numeric, then #VALUE! is returned. The angle returned is in radians between 0 and PI.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the arc-cosine of. ")]
        public static decimal? ACOS(decimal? number)
        {
            if (!number.HasValue)
                return null;

            double d = (double)number.Value;
            return (decimal?)Math.Acos(d);
        }

        /// <summary>
        /// Returns the arc-cotangent of a number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ACOT",
             InsertedText = "ACOT(|Value|)",
             DataType = 2,
             Hint = "(2013) The arc-cotangent of a number.",
             Description = "The arc-cotangent is the inverse of the COT function. The angle returned is in radians between 0 and PI. If number is not numeric, then #VALUE is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the inverse cotangent of. ")]
        public static decimal? ACOT(decimal? number)
        {
            if (!number.HasValue)
                return null;

            double d = (double)number.Value;
            //2 * Atan(1) - Atan(x)

            decimal atan1 = (decimal)Math.Atan(1);
            decimal atanx = (decimal)Math.Atan(d);

            return 2 * atan1 - atanx;
        }

        /// <summary>
        /// Returns the arc-sine of a number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ASIN",
             InsertedText = "ASIN(|Value|)",
             DataType = 2,
             Hint = "The arc-sine of a number.",
             Description = "The arc-sine is the inverse of the SIN function. This function is also called the inverse sine function. The number is the sine of an angle in radians. If number is not numeric, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the arcsine of. ")]
        public static decimal? ASIN(decimal? number)
        {
            if (!number.HasValue)
                return null;

            double d = (double)number.Value;
            return (decimal?)Math.Asin(d);
        }

        /// <summary>
        /// Returns the arc-tangent of a number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ATAN",
             InsertedText = "ATAN(|Value|)",
             DataType = 2,
             Hint = "The arc-tangent of a number.",
             Description = "The arc-tangent is the inverse of the TAN function. This function is also called the inverse tangent function. The number is the angle in radians. If number is not numeric, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the arc-tangent of.")]
        public static decimal? ATAN(decimal? number)
        {
            if (!number.HasValue)
                return null;

            double d = (double)number.Value;
            return (decimal?)Math.Atan(d);
        }

        /// <summary>
        /// Returns the arc-tangent of the specified x and y co-ordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ATAN2",
             InsertedText = "ATAN(|X|,|Y|)",
             DataType = 2,
             Hint = "The arc-tangent of the specified x and y co-ordinates.",
             Description = "The arc-tangent of an angle is the inverse of the TAN function. ATAN2(x_num , y_num) equals ATAN(y_num / x_num), except that x_num can equal 0 in ATAN2. The arc-tangent is the angle from the x-axis to a line containing the origin (0, 0) and a point with coordinates (x_num, y_num). If x_num = 0 and y_num = 0, then #DIV/0! is returned.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The x-coordinate of the point. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The y-coordinate of the point. ")]
        public static decimal? ATAN2(decimal? x, decimal? y)
        {
            if (!x.HasValue || !y.HasValue)
                return null;

            double dx = (double)x.Value;
            double dy = (double)y.Value;

            return (decimal?)Math.Atan2(dx, dy);
        }

        /// <summary>
        /// Returns the arithmetic mean of non null values in an array.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "AVERAGE",
             InsertedText = "AVERAGE(|VALUES|)",
             DataType = 2,
             Hint = "The numeric average of the input numbers.",
             Description = "The arithmetic mean is the most common measure of central tendency. This function includes hidden rows. Arguments that are numbers are included. Arguments that are zero are included. Arguments that are dates are included. Arguments that are logical values are excluded from the denominator. Arguments that are text are excluded from the denominator. If any cell references are empty, then these are excluded. If any cell references contain an error, then that error is returned.",
             MinParams = 2,
             MaxParams = 255,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The first number.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The second number. ")]
        public static decimal? AVERAGE(params decimal?[] values)
        {
            if (values == null || values.Length == 0)
                return null;

            return values.Average();
        }

        /// <summary>
        /// Returns the bitwise AND of two numbers.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "BITAND",
             InsertedText = "BITAND(|X|,|Y|)",
             DataType = 2,
             Hint = "(2013) The bitwise AND of two numbers.",
             Description = "This function returns a decimal. The value of each bit position is counted only if both parameters have a 1 at that position. If number1 < 0, then #NUM is returned. If number2 < 0, then #NUM is returned. If number1 is not an integer, then #NUM is returned. If number2 is not an integer, then #NUM is returned.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The first number.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The second number. ")]
        public static decimal? BITAND(decimal? x, decimal? y)
        {
            if (!x.HasValue || !y.HasValue)
                return null;

            return (int)x.Value & (int)y.Value;
        }

        /// <summary>
        /// Returns the bitwise OR of two numbers.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "BITOR",
             InsertedText = "BITOR(|X|,|Y|)",
             DataType = 2,
             Hint = "(2013) The bitwise OR of two numbers.",
             Description = "The value of each bit position is counted if either of the parameters has a 1 is that position. If number1 < 0, then #NUM is returned. If number2 < 0, then #NUM is returned. If number1 is not an integer, then #NUM is returned. If number2 is not an integer, then #NUM is returned. If number1 > (2^48)-1, then #NUM is returned. If number2 > (2^48)-1, then #NUM is returned.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The first number.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The second number. ")]
        public static decimal? BITOR(decimal? x, decimal? y)
        {
            if (!x.HasValue || !y.HasValue)
                return null;

            return (int)x.Value | (int)y.Value;
        }

        /// <summary>
        /// Returns the bitwise Exclusive OR of two numbers.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "BITXOR",
             InsertedText = "BITXOR(|X|,|Y|)",
             DataType = 2,
             Hint = "(2013) The bitwise Exclusive OR of two numbers.",
             Description = "The value of each bit position is 1 when the bit positions of the parameters are different. If number1 < 0, then #NUM is returned. If number2 < 0, then #NUM is returned. If number1 is not an integer, then #NUM is returned. If number2 is not an integer, then #NUM is returned. If number1 > (2^48)-1, then #NUM is returned. If number2 > (2^48)-1, then #NUM is returned.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The first number.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The second number. ")]
        public static decimal? BITXOR(decimal? x, decimal? y)
        {
            if (!x.HasValue || !y.HasValue)
                return null;

            return (int)x.Value ^ (int)y.Value;
        }

        /// <summary>
        /// Returns the number rounded up to the nearest integer or significant figure.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "CEILING",
             InsertedText = "CEILING(|VALUE|)",
             DataType = 2,
             Hint = "(CEILING.MATH) The number rounded up to the nearest integer or significant figure.",
             Description = "This function is available for backwards compatibility. If any of the arguments are not numeric, then #VALUE! is returned. If significance < 0, then the absolute value is taken. If significance = 0, then 0 is returned. If numberis positive and significance is negative, then #NUM! is returned. If number is negative and significance is positive then the direction of the rounding is reversed. Positive decimal numbers are rounded up to the nearest integer. Negative decimal numbers are rounded up to the nearest integer (away from zero).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number to be rounded up. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The multiple you want the number rounded to. ")]
        public static decimal? CEILING(decimal? number)
        {
            if (!number.HasValue)
                return null;

            return Math.Ceiling(number.Value);
        }

        /// <summary>
        /// Returns the character with the corresponding ANSI number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "CHAR",
             InsertedText = "CHAR(|VALUE|)",
             DataType = 1,
             Hint = "The character with the corresponding ANSI number.",
             Description = "This function is useful when you want to specify characters that are difficult or impossible to type directly. The number argument can be with or without leading zeros.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The number specifying which character you want between 1 and 255.")]
        public static string CHAR(decimal? number)
        {
            if (!number.HasValue)
                return null;

            int i = (int)number.Value;

            return ((char)i).ToString();
        }

        /// <summary>
        /// Returns the text string with all the non-printable characters removed.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "CLEAN",
             InsertedText = "CLEAN(|VALUE|)",
             DataType = 1,
             Hint = "The text string with all the non-printable characters removed.",
             Description = "This function removes any non printable characters include tabs and program specific codes. This function is useful when you want to remove characters that will not be printed. For example this function can be used to remove line breaks. The text can consist of other worksheet functions. This can be a useful function to use when you import data from another program.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string you want cleaned.")]
        public static string CLEAN(string text)
        {
            if (text == null)
                return null;

            StringBuilder result = new StringBuilder();

            foreach (char c in text)
            {
                if ((int)c > 31)
                    result.Append(c);
            }

            return result.ToString();
        }

        /// <summary>
        /// Returns the ANSI number for the first character in a text string.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "CODE",
             InsertedText = "CODE(|VALUE|)",
             DataType = 2,
             Hint = "The ANSI number for the first character in a text string.",
             Description = "The text can be a text string, a cell reference or a named range. If text contains more than one character, then the first character is used",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string you want the first character code for. ")]
        public static decimal? CODE(string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            return (int)text.First();
        }

        /// <summary>
        /// Returns the text string that is a concatenation of several strings.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "CONCAT",
             InsertedText = "CONCAT(|VALUES|)",
             DataType = 1,
             Hint = "(2016 Jan) The text string that is a concatenation of several strings.",
             Description = "This function replaces the CONCATENATE function. The text can be a range of cells (as opposed to a single cell reference). The text can be a text string or number. If the resulting strings has more than 32, 767 characters, then #VALUE! is returned. You can have a maximum of 253 text arguments.",
             MinParams = 1,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The first text string.",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "(Optional) The second text string. ")]
        public static string CONCAT(params string[] values)
        {
            return CONCATENATE(values);
        }

        /// <summary>
        /// Returns the text string that is a concatenation of several strings.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "CONCATENATE",
             InsertedText = "CONCATENATE(|VALUES|)",
             DataType = 1,
             Hint = "(CONCAT) The text string that is a concatenation of several strings.",
             Description = "This function is equivalent to the & character. The text can be single cell reference. The text can be a text string or number. If you concatenate a cell that contains a numerical value, the result is converted to text. If you concatenate a cell that is formatted as a date, the date serial number will be used. You can have a maximum of 255 arguments. You can use the TEXTJOIN function to combine text from multiple ranges and add a delimiter. You can use the TEXT function to avoid the date serial number problem and convert the date into a recognisable date format before it gets concatenated.",
             MinParams = 1,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The first text string.",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "(Optional) The second text string. ")]
        public static string CONCATENATE(params string[] values)
        {
            if (values == null || values.Length == 0)
                return null;

            return string.Concat(values);
        }

        /// <summary>
        /// Returns the cosine of a number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "COS",
             InsertedText = "COS(|VALUE|)",
             DataType = 2,
             Hint = "The cosine of a number.",
             Description = "This function is periodic with a period of 2PI. The number is the angle in radians. If number is not numeric, the #VALUE! is returned. Example 15. If your argument is in degrees, multiply it by PI/180 to convert it to radians. If your argument is in radians, multiply it by 180/PI to convert it to degrees.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the cosine of. ")]
        public static decimal? COS(decimal? number)
        {
            if (!number.HasValue)
                return null;

            double d = (double)number.Value;
            return (decimal?)Math.Cos(d);
        }

        /// <summary>
        /// Returns the cotangent of a number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "COT",
             InsertedText = "COT(|VALUE|)",
             DataType = 2,
             Hint = "(2013) The cotangent of a number.",
             Description = "The number is the angle in radians. If number is not numeric, then #VALUE is returned. If number is zero, then #DIV/0! is returned. If number < -2 E10, then #NUM is returned. If number > 2 E10, then #NUM is returned. If your argument is in degrees, multiply it by PI/180 to convert it to radians.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the cotangent of. ")]
        public static decimal? COT(decimal? number)
        {
            if (!number.HasValue)
                return null;

            double d = (double)number.Value;

            decimal tan = (decimal)Math.Tan(d);

            if (tan == 0)
                return 0;

            return 1 / tan;
        }

        /// <summary>
        /// Returns the cosecant of a number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "CSC",
             InsertedText = "CSC(|VALUE|)",
             DataType = 2,
             Hint = "(2013) The cosecant of a number.",
             Description = "This function does not have an inverse. The number is the angle in radians. If number is not numeric, the #VALUE is returned. If Abs(number) > 2^27, then #NUM is returned. If your argument is in degrees, multiply it by PI/180 to convert it to radians.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the cosecant of. ")]
        public static decimal? CSC(decimal? number)
        {
            if (!number.HasValue)
                return null;

            double d = (double)number.Value;

            decimal sin = (decimal)Math.Sin(d);

            if (sin == 0)
                return 0;

            return 1 / sin;
        }

        /// <summary>
        /// Returns the date given a year, month, day.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATE",
             InsertedText = "DATE(|YEAR|,|MONTH|,|DAY|)",
             DataType = 9,
             Hint = "The date as a date serial number given a year, month, day.",
             Description = "If year >= 0 and year <= 1899 then year is added to 1900 to calculate the year. If year >= 1900 and year <= 9999 then that value is used as the year. If year < 0, then #NUM! is returned. If year > 10000, then #NUM! is returned. If month > 12, then month is added to the first month in the given year. If month <= 0, then ABS(month)+1 is taken away from the first month in the given year. If day > the number of days in month, then day is added to the first day in the given month. If day < 0, then ABS(day)+1 is taken away from the first day of the given month. If day = 0, then the last day of the previous month is used. If the cell has the General number format, then this cell is formatted automatically to dd/mm/yyyy.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The year component, between 0 and 9999.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The month component, between 1 and 12.",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The day component between 1 and 31. ")]
        public static DateTime? DATE(decimal? year, decimal? month, decimal? day)
        {
            if (!year.HasValue || !month.HasValue || !day.HasValue)
                return null;

            int iYear = (int)year;
            int iMonth = (int)month;
            int iDay = (int)day;

            if (iYear < 1 || iYear > 9999)
                throw new DateConversionException($"Invalid year value in DATE({iYear}, {iMonth}, {iDay})");

            if (iMonth < 1 || iMonth > 12)
                throw new DateConversionException($"Invalid month value in DATE({iYear}, {iMonth}, {iDay})");

            if (iDay < 1 || iDay > DateTime.DaysInMonth(iYear, iMonth))
                throw new DateConversionException($"Invalid day value in DATE({iYear}, {iMonth}, {iDay})");

            return new DateTime(iYear, iMonth, iDay);
        }

        #region DATEDIF
        private static decimal? DoDATEDIF(DateTime startDT, DateTime endDT, string unit)
        {
            if (startDT > endDT)
                throw new DateComparisonException($"startDT is after the endDT in DATEDIF({startDT}, {endDT}, {unit}).");

            DateTime diffDT = DateTime.MinValue + (endDT - startDT);

            switch (unit.ToUpper())
            {
                case "D":
                    return (endDT - startDT).Days;
                case "M":
                    return (diffDT.Month - 1) + ((diffDT.Year - 1) * 12);
                case "Y":
                    return (diffDT.Year - 1);
                case "MD":
                    if (endDT.Day < startDT.Day)
                        return (new DateTime(2001, 2, endDT.Day) - new DateTime(2001, 1, startDT.Day)).Days;

                    return (new DateTime(2001, 1, endDT.Day) - new DateTime(2001, 1, startDT.Day)).Days;
                case "YD":
                    if (endDT.Month < startDT.Month)
                        return (new DateTime(2001, endDT.Month, endDT.Day) - new DateTime(2002, startDT.Month, startDT.Day)).Days;

                    return (new DateTime(2001, endDT.Month, endDT.Day) - new DateTime(2001, startDT.Month, startDT.Day)).Days;
                case "YM":
                    if (endDT.Month < startDT.Month)
                        return (endDT.Month - startDT.Month) + 12;

                    return (endDT.Month - startDT.Month);

                default:
                    throw new DateComparisonException($"Invalid unit in DATEDIF({startDT}, {endDT}, {unit}).");
            }
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(DateTime? startDT, DateTime? endDT, string unit)
        {
            if (!startDT.HasValue || !endDT.HasValue)
                return null;

            return DoDATEDIF(startDT.Value, endDT.Value, unit);
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(DateTime? startDT, decimal? endDT, string unit)
        {
            if (!startDT.HasValue || !endDT.HasValue)
                return null;

            DateTime end = DateTime.FromOADate((double)endDT.Value);
            
            return DoDATEDIF(startDT.Value, end, unit);
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(DateTime? startDT, string endDT, string unit)
        {
            if (!startDT.HasValue || endDT == null)
                return null;

            if (!TryParseDate(endDT, out DateTime end))
                throw new DateConversionException($"Invalid End Date format in DATEDIF({startDT}, '{endDT}', {unit}).");

            return DoDATEDIF(startDT.Value, end, unit);
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(decimal? startDT, DateTime? endDT, string unit)
        {
            if (!startDT.HasValue || !endDT.HasValue)
                return null;

            DateTime start = DateTime.FromOADate((double)startDT.Value);

            return DoDATEDIF(start, endDT.Value, unit);
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(decimal? startDT, decimal? endDT, string unit)
        {
            if (!startDT.HasValue || !endDT.HasValue)
                return null;

            DateTime start = DateTime.FromOADate((double)startDT.Value);
            DateTime end = DateTime.FromOADate((double)endDT.Value);

            return DoDATEDIF(start, end, unit);
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(decimal? startDT, string endDT, string unit)
        {
            if (!startDT.HasValue || endDT == null)
                return null;

            DateTime start = DateTime.FromOADate((double)startDT.Value);

            if (!TryParseDate(endDT, out DateTime end))
                throw new DateConversionException($"Invalid End Date format in DATEDIF({startDT}, '{endDT}', {unit}).");

            return DoDATEDIF(start, end, unit);
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(string startDT, DateTime? endDT, string unit)
        {
            if (startDT == null || !endDT.HasValue)
                return null;

            if (!TryParseDate(startDT, out DateTime start))
                throw new DateConversionException($"Invalid Start Date format in DATEDIF('{startDT}', {endDT}, {unit}).");

            return DoDATEDIF(start, endDT.Value, unit);
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(string startDT, decimal? endDT, string unit)
        {
            if (startDT == null || !endDT.HasValue)
                return null;

            DateTime end = DateTime.FromOADate((double)endDT.Value);

            if (!TryParseDate(startDT, out DateTime start))
                throw new DateConversionException($"Invalid Start Date format in DATEDIF('{startDT}', {endDT}, {unit}).");

            return DoDATEDIF(start, end, unit);
        }

        /// <summary>
        /// Returns the number of units between two dates.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <param name="unit">
        /// "D" = number of days between "start_date" and "end_date"
        /// "M" = number of months (whole months) between "start_date" and "end_date"
        /// "Y" = number of years (whole years) between "start_date" and "end_date"
        /// "MD" = number of days between "start_date" and "end_date" (ignoring months and years)
        /// "YD" = number of days between "start_date" and "end_date" (ignoring years)
        /// "YM" = number of months between "start_date" and "end_date" (ignoring days and years)
        /// </param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEDIF",
             InsertedText = "DATEDIF(|START|,|END|,|UNIT|)",
             DataType = 2,
             Hint = "The number of units between two dates.",
             Description = "If start_date > end_date, then #NUM! is returned. If start_date is not a valid date, then #VALUE! is returned. If end_date is not a valid date, then #VALUE! is returned. If interval is not one of the above then #NUM! is returned. The interval is not case sensitive (you can use: d, m, y, td, ym, md). If you are including the interval directly then it must be enclosed in double quotes. The following are leap years: 1992, 1996, 2000, 2004, 2008, 2012, 2016, 2020. When calculating leap years the year from the 'start_date' is used (and the year from the 'end_date' is ignored) when you are using yd, ym and md).",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the starting date of the period.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the finishing date of the period. ")]
        public static decimal? DATEDIF(string startDT, string endDT, string unit)
        {
            if (startDT == null || endDT == null)
                return null;

            if (!TryParseDate(startDT, out DateTime start))
                throw new DateConversionException($"Invalid Start Date format in DATEDIF('{startDT}', {endDT}, {unit}).");

            if (!TryParseDate(endDT, out DateTime end))
                throw new DateConversionException($"Invalid End Date format in DATEDIF({startDT}, '{endDT}', {unit}).");

            return DoDATEDIF(start, end, unit);
        }
        #endregion DATEDIF

        #region DATEVALUE
        private static decimal? DoDATEVALUE(DateTime valueDT)
        {
            TimeSpan timeSince = (valueDT - new DateTime(1899, 12, 30));

            return timeSince.Days;
        }

        /// <summary>
        /// Returns the date serial number given a date.
        /// 12/30/1899 is the zero date.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEVALUE",
             InsertedText = "DATEVALUE(|VALUE|)",
             DataType = 9,
             Hint = "The date serial number given a date in text format.",
             Description = "If date_value contains any time information, it is ignored. If date_value is not a valid date, then #VALUE! is returned. If date_value is a cell reference, the value in the cell must be text. If the year portion of date_text is left blank, the current year is used. Acceptable date forms will vary from system to system and will also depend on the users settings. Most functions will automatically convert date strings to serial numbers. The date format can be any of those found on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The text string that represents the date. ")]
        public static decimal? DATEVALUE(DateTime? valueDT)
        {
            if (!valueDT.HasValue)
                return null;

            return DoDATEVALUE(valueDT.Value);
        }

        /// <summary>
        /// Returns the date serial number given a date.
        /// 12/30/1899 is the zero date.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEVALUE",
             InsertedText = "DATEVALUE(|VALUE|)",
             DataType = 9,
             Hint = "The date serial number given a date in text format.",
             Description = "If date_value contains any time information, it is ignored. If date_value is not a valid date, then #VALUE! is returned. If date_value is a cell reference, the value in the cell must be text. If the year portion of date_text is left blank, the current year is used. Acceptable date forms will vary from system to system and will also depend on the users settings. Most functions will automatically convert date strings to serial numbers. The date format can be any of those found on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The text string that represents the date. ")]
        public static decimal? DATEVALUE(decimal? valueDT)
        {
            return valueDT;
        }

        /// <summary>
        /// Returns the date serial number given a date.
        /// 12/30/1899 is the zero date.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DATEVALUE",
             InsertedText = "DATEVALUE(|VALUE|)",
             DataType = 9,
             Hint = "The date serial number given a date in text format.",
             Description = "If date_value contains any time information, it is ignored. If date_value is not a valid date, then #VALUE! is returned. If date_value is a cell reference, the value in the cell must be text. If the year portion of date_text is left blank, the current year is used. Acceptable date forms will vary from system to system and will also depend on the users settings. Most functions will automatically convert date strings to serial numbers. The date format can be any of those found on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The text string that represents the date. ")]
        public static decimal? DATEVALUE(string valueDT)
        {
            if (valueDT == null)
                return null;

            if (!TryParseDate(valueDT, out DateTime dt))
                throw new DateConversionException($"Invalid Date format in DATEVALUE('{valueDT}').");

            return DoDATEVALUE(dt);
        }
        #endregion DATEVALUE

        #region DAY
        private static decimal? DoDAY(DateTime valueDT)
        {
            return valueDT.Day;
        }

        /// <summary>
        /// The day as an integer given a date.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DAY",
             InsertedText = "DAY(|VALUE|)",
             DataType = 9,
             Hint = "The day as an integer given a date serial number.",
             Description = "This function returns a number between 1 and 31. The serial_number can be a date, value, cell reference or a text string in a date format. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned. The date format can be any of those found on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the day you want to find. ")]
        public static decimal? DAY(DateTime? valueDT)
        {
            if (!valueDT.HasValue)
                return null;

            return DoDAY(valueDT.Value);
        }

        /// <summary>
        /// The day as an integer given a date.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DAY",
             InsertedText = "DAY(|VALUE|)",
             DataType = 9,
             Hint = "The day as an integer given a date serial number.",
             Description = "This function returns a number between 1 and 31. The serial_number can be a date, value, cell reference or a text string in a date format. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned. The date format can be any of those found on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the day you want to find. ")]
        public static decimal? DAY(decimal? valueDT)
        {
            if (!valueDT.HasValue)
                return null;

            DateTime dt = DateTime.FromOADate((double)valueDT.Value);

            return DoDAY(dt);
        }

        /// <summary>
        /// The day as an integer given a date.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DAY",
             InsertedText = "DAY(|VALUE|)",
             DataType = 9,
             Hint = "The day as an integer given a date serial number.",
             Description = "This function returns a number between 1 and 31. The serial_number can be a date, value, cell reference or a text string in a date format. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned. The date format can be any of those found on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the day you want to find. ")]
        public static decimal? DAY(string valueDT)
        {
            if (valueDT == null)
                return null;

            if (!TryParseDate(valueDT, out DateTime dt))
                throw new DateConversionException($"Invalid Date format in DAY('{valueDT}').");

            return DoDAY(dt);
        }
        #endregion DAY

        /// <summary>
        /// Returns the number of days between two dates.
        /// </summary>
        /// <param name="endDT"></param>
        /// <param name="startDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DAYS",
             InsertedText = "DAYS(|START|,|END|)",
             DataType = 2,
             Hint = "(2013) The number of days between two dates.",
             Description = "If end_date contains any time information, it is ignored. If end_date is not a valid date, then #VALUE! is returned. If end_date is not a valid serial number, then #NUM! is returned. If start_date contains any time information, it is ignored. If start_date is not a valid date, then #VALUE! is returned. If start_date is not a valid serial number, then #NUM! is returned. This function can handle dates in text format.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that represents the finishing date.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The date that represents the starting date. ")]
        public static decimal? DAYS(DateTime? endDT, DateTime? startDT)
        {
            if (!startDT.HasValue || !endDT.HasValue)
                return null;

            DateTime start = startDT.Value;
            DateTime end = endDT.Value;

            if (start > end)
                throw new DateComparisonException($"startDT is after the endDT in DAYS({endDT}, {startDT}).");

            return (end - start).Days;
        }

        /// <summary>
        /// Returns the text string of a number with the currency formatting $0,000.00.
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="decimals"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "DOLLAR",
             InsertedText = "DOLLAR(|AMOUNT|,|DECIMALS|)",
             DataType = 1,
             Hint = "The text string of a number with the dollar formatting $0, 000.00.",
             Description = "The number can be a number, a reference to a cell containing a number, or a formula that evaluates to a number. The resulting number is rounded when necessary. If decimals < 0, then number is rounded to the left of the decimal point.  If decimals is left blank, then 2 is used. If decimal is empty (i.e. you just include the comma separator), then no decimal places are used. The major difference between formatting a cell using (Format > Cells) and formatting it with this function is that using this function the result is converted to text. Using (Format > Cells) leaves the number as a number. Any numbers formatted with this function can still be used in numerical formulas because numbers entered as text are automatically converted to numbers when performing calculations.",
             MinParams = 1,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The number you want formatted.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "(Optional) The number of digits to the right of the decimal place.")]
        public static string DOLLAR(decimal? amount, decimal? decimals = null)
        {
            if (!amount.HasValue)
                return null;


            if (decimals.HasValue)
                return string.Format(new CultureInfo("en-US"), $"{{0:c{decimals.Value}}}", amount.Value);

            return string.Format(new CultureInfo("en-US"), "{0:c}", amount.Value);
        }

        /// <summary>
        /// Returns the last day of a month before or after a date.
        /// </summary>
        /// <param name="startDT"></param>
        /// <param name="months"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "EOMONTH",
             InsertedText = "EOMONTH(|START|,|MONTHS|)",
             DataType = 9,
             Hint = "The serial number of the last day of a month before or after a date.",
             Description = "This function can be used to calculate maturity dates or due dates that fall on the same day on the month as the date of issue. If start_date is not a valid date, then #NUM! is returned. If start_date plus months yields an invalid date, then #NUM! is returned. If months is not an integer, it is truncated. If months > 0, then a date in the future is returned. If months > 12, then additional years are added on. If months < 0, then a date in the past is returned. If months = 0, then the end of the start_date month is returned. If months is left blank, then #N/A is returned. If no arguments are submitted, then #NA! is returned",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The start date.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number of months before or after the \"start date\".")]
        public static DateTime? EOMONTH(DateTime? startDT, decimal? months = null)
        {
            if (!startDT.HasValue)
                return null;

            int iMonths = (int)months.GetValueOrDefault(0m);

            DateTime nextMonth = startDT.Value.AddMonths(iMonths+1);
            DateTime dt = new DateTime(nextMonth.Year, nextMonth.Month, 1).AddDays(-1);

            return dt;
        }

        /// <summary>
        /// Returns the number rounded up to the nearest even integer.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "EVEN",
             InsertedText = "EVEN(|VALUE|)",
             DataType = 2,
             Hint = "The number rounded up to the nearest even integer.",
             Description = "If number is not numeric, then #VALUE! is returned. If number is an even integer then no rounding occurs. Negative numbers are rounded down.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param2Hint = "The number you want to round up. ")]
        public static decimal? EVEN(decimal? number)
        {
            if (!number.HasValue)
                return null;

            bool isNeg = (number.Value < 0);
            decimal result = decimal.Ceiling(Math.Abs(number.Value) / 2) * 2;

            return isNeg ? -result : result;
        }

        /// <summary>
        /// Returns the value True or False based on whether two strings match exactly.
        /// </summary>
        /// <param name="text1"></param>
        /// <param name="text2"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "EXACT",
             InsertedText = "EXACT(|T1|,|T2|)",
             DataType = 3,
             Hint = "The value True or False based on whether two strings match exactly.",
             Description = "This function is case sensitive. This function is similar to using the = character in a formula except this is not case sensitive. See Row 10. If the arguments are cell references then any formatting differences are ignored. The arguments can be literal strings enclosed in quotation marks, formulas evaluating to numerical values or cell references containing text, numerical values.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The first text string.",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "The second text string. ")]
        public static bool? EXACT(string text1, string text2)
        {
            if (text1 == null || text2 == null)
                return false;

            return (text1 == text2);
        }

        /// <summary>
        /// Returns the exponential number raised to a particular power.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "EXP",
             InsertedText = "EXP(|VALUE|)",
             DataType = 2,
             Hint = "The exponential number raised to a particular power.",
             Description = "The e number raised to a particular power is the inverse of the LN function. The constant e equals 2.71828182845904, the base of the natural logarithm. To calculate powers of other bases, use the exponentiation operator (^).",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number to use as the exponent applied to the base e. ")]
        public static decimal? EXP(decimal? number)
        {
            if (!number.HasValue)
                return null;

            double d = (double)number.Value;
            return (decimal?)Math.Exp(d);
        }

        /// <summary>
        /// Returns the factorial of a number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "FACT",
             InsertedText = "FACT(|VALUE|)",
             DataType = 2,
             Hint = "The factorial of a number.",
             Description = "The factorial of a number is equal to 1*2*3* to * number. If number < 0, then #NUM! is returned. If number is not an integer, it is truncated. If number is not numeric, then #VALUE! is returned. The factorial of 1 is 0.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the factorial of. ")]
        public static decimal? FACT(decimal? number)
        {
            if (!number.HasValue)
                return null;

            if (number.Value < 0)
                throw new ArgumentOutOfRangeException("number", $"Number must be greater than or equal to zero in FACT({number})");

            decimal result = 1;
            int n = (int)number.Value;

            while (n > 1)
            {
                result = result * n;
                n = n - 1;
            }
            return result;
        }

        /// <summary>
        /// Returns the logical value False.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "FALSE",
             DataType = 3,
             Hint = "The logical value False.",
             Description = "This is one of the few functions that does not require any arguments. You can also type the word FALSE directly into a cell.",
             MinParams = 0,
             MaxParams = 0)]
        public static bool? FALSE()
        {
            return false;
        }

        /// <summary>
        /// Returns the logical value False.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "FIND",
             InsertedText = "FIND(|FIND|,|WITHIN|,|START|)",
             DataType = 2,
             Hint = "The position of a substring within a larger text string.",
             Description = "This function is case sensitive. This function will always return the first instance of the text you want to find (reading from left to right). This function returns a #VALUE when the text does not appear (ie. find_text does not appear in within_text). The find_text can either be a literal string enclosed in quotation marks or a cell reference. The within_text can either be a literal string enclosed in quotation marks or a cell reference. If start_num is greater than the position of the last occurrence of find_text, then #VALUE! is returned. If start_num > the length of within_text, then #VALUE! is returned. If start_num < 1, then #VALUE! is returned. If start_num is left blank, then 1 is used. Any blank spaces or punctuation marks count as individual characters.",
             MinParams = 2,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text you want to find.",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "The text containing the text you want to find. ",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "(Optional) The character at which to start the search. ")]
        public static decimal? FIND(string findText, string withinText, decimal? startNum = null)
        {
            if (findText == null || withinText == null)
                return null;

            int index;

            if (startNum.HasValue)
            {
                int start = (int)startNum.Value-1;

                if (start < 0)
                    return null;

                if (start > withinText.Length)
                    return null;

                index = withinText.IndexOf(findText, start);
            }

            else
                index = withinText.IndexOf(findText);

            if (index == -1)
                return null;

            return index + 1;
        }

        /// <summary>
        /// Returns the text string of a number rounded to a fixed number of decimal places.
        /// </summary>
        /// <param name="number"></param>
        /// <param name="decimals"></param>
        /// <param name="no_commas"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "FIXED",
             InsertedText = "FIXED(|VALUE|,|DECIMAL|,|COMMAS|)",
             DataType = 1,
             Hint = "The text string of a number rounded to a fixed number of decimal places.",
             Description = "This function returns numbers as text strings (left cell aligned) however they can still be used in formulas. If decimals < 0, then number is rounded to the left of the decimal point. If decimals is left blank, then 2 is used. If no_commas = True, then the returned text doesn't include commas. If no_commas = False, then the returned text includes commas. If no_commas is left blank, then False is used. Any numbers formatted with this function can still be used in numerical formulas because numbers entered as text are automatically converted to numbers when performing calculations.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The number you want to round and convert to text. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number of digits to the right of the decimal point.",
             Param3Name = "Value3",
             Param3DataType = 1,
             Param3Hint = "True of False to specify whether to include commas. ")]
        public static string FIXED(decimal? number, decimal? decimals = null, bool? no_commas = false)
        {
            if (!number.HasValue)
                return null;

            int iDecimals = 2;
            if (decimals.HasValue)
                iDecimals = (int)decimals.Value;

            bool bNoCommas = no_commas.GetValueOrDefault(false);

            string fmt;

            if (bNoCommas)
                fmt = "0";
            else
                fmt = "#,##0";

            decimal myNumber = number.Value;
            if (iDecimals > 0)
                fmt += "." + new String('0', iDecimals);
            else if (iDecimals < 0)
            {
                int places = (int)Math.Pow(10, -iDecimals);
                myNumber = Math.Round(myNumber / places) * places;
            }

            return myNumber.ToString(fmt);
        }

        /// <summary>
        /// Returns the number rounded down to the nearest integer or significant figure.
        /// </summary>
        /// <param name="number"></param>
        /// <param name="significance"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "FLOOR",
             InsertedText = "FLOOR(|VALUE|,|SIGNIFICANCE|)",
             DataType = 2,
             Hint = "(FLOOR.MATH) The number rounded down to the nearest integer or significant figure.",
             Description = "This function is available for backwards compatibility. If any of the arguments are not numeric, then #VALUE! is returned. If number or significance have different signs, then #NUM! is returned (last example ?) Regardless of the sign of number, a value is rounded down when adjusted away from zero. If number is an exact multiple of significance, no rounding occurs.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number to be rounded down. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The multiple you want the number rounded to. ")]
        public static decimal? FLOOR(decimal? number, decimal? significance)
        {
            if (!number.HasValue || !significance.HasValue)
                return null;

            if (number.Value == 0)
                return 0;

            if (significance.Value == 0)
                throw new ArgumentOutOfRangeException("significance", $"Significance can not zero in FLOOR({number}, {significance}).");

            decimal dNumber = number.Value;
            decimal dSignificance = significance.Value;

            if ((dNumber > 0) != (dSignificance > 0) || (dNumber < 0) != (dSignificance < 0))
                throw new ArgumentOutOfRangeException("significance", $"Number and Significance can not be difference signs in FLOOR({number}, {significance}).");

            if (dSignificance == 1)
                return decimal.Floor(dNumber);

            return decimal.Floor(dNumber / dSignificance) * dSignificance;
        }

        /// <summary>
        /// Returns the value based on a condition you specify.
        /// </summary>
        /// <param name="logical_test"></param>
        /// <param name="value_if_true"></param>
        /// <param name="value_if_false"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "IF",
             InsertedText = "IF(|LOGIC|,|TRUE|,|FALSE|)",
             DataType = 99,
             Hint = "The value based on a condition you specify.",
             Description = "This function is not case sensitive. You can nest up to 64 IF functions. If value_if_true is left blank, then. If value_if_false is left blank, then. If you use text arguments, the match must be exact, except for case. This function can return an array formula.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value or expression that can be evaluated to True of False. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The value that is returned if \"logical_test\" is True.",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The value that is returned if \"logical_test\" is False. ")]
        public static bool? IF(bool? logical_test, bool? value_if_true, bool? value_if_false)
        {
            if (logical_test.GetValueOrDefault(false))
                return value_if_true;

            return value_if_false;
        }

        /// <summary>
        /// Returns the value based on a condition you specify.
        /// </summary>
        /// <param name="logical_test"></param>
        /// <param name="value_if_true"></param>
        /// <param name="value_if_false"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "IF",
             InsertedText = "IF(|LOGIC|,|TRUE|,|FALSE|)",
             DataType = 99,
             Hint = "The value based on a condition you specify.",
             Description = "This function is not case sensitive. You can nest up to 64 IF functions. If value_if_true is left blank, then. If value_if_false is left blank, then. If you use text arguments, the match must be exact, except for case. This function can return an array formula.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value or expression that can be evaluated to True of False. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The value that is returned if \"logical_test\" is True.",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The value that is returned if \"logical_test\" is False. ")]
        public static decimal? IF(bool? logical_test, decimal? value_if_true, decimal? value_if_false)
        {
            if (logical_test.GetValueOrDefault(false))
                return value_if_true;

            return value_if_false;
        }

        /// <summary>
        /// Returns the value based on a condition you specify.
        /// </summary>
        /// <param name="logical_test"></param>
        /// <param name="value_if_true"></param>
        /// <param name="value_if_false"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "IF",
             InsertedText = "IF(|LOGIC|,|TRUE|,|FALSE|)",
             DataType = 99,
             Hint = "The value based on a condition you specify.",
             Description = "This function is not case sensitive. You can nest up to 64 IF functions. If value_if_true is left blank, then. If value_if_false is left blank, then. If you use text arguments, the match must be exact, except for case. This function can return an array formula.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value or expression that can be evaluated to True of False. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The value that is returned if \"logical_test\" is True.",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The value that is returned if \"logical_test\" is False. ")]
        public static string IF(bool? logical_test, string value_if_true, string value_if_false)
        {
            if (logical_test.GetValueOrDefault(false))
                return value_if_true;

            return value_if_false;
        }

        /// <summary>
        /// Returns the value based on a condition you specify.
        /// </summary>
        /// <param name="logical_test"></param>
        /// <param name="value_if_true"></param>
        /// <param name="value_if_false"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "IF",
             InsertedText = "IF(|LOGIC|,|TRUE|,|FALSE|)",
             DataType = 99,
             Hint = "The value based on a condition you specify.",
             Description = "This function is not case sensitive. You can nest up to 64 IF functions. If value_if_true is left blank, then. If value_if_false is left blank, then. If you use text arguments, the match must be exact, except for case. This function can return an array formula.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value or expression that can be evaluated to True of False. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The value that is returned if \"logical_test\" is True.",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The value that is returned if \"logical_test\" is False. ")]
        public static DateTime? IF(bool? logical_test, DateTime? value_if_true, DateTime? value_if_false)
        {
            if (logical_test.GetValueOrDefault(false))
                return value_if_true;

            return value_if_false;
        }

        /// <summary>
        /// Returns the number rounded down to the nearest integer.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "INT",
             InsertedText = "INT(|VALUE|)",
             DataType = 2,
             Hint = "The number rounded down to the nearest integer.",
             Description = "Negative numbers are rounded down. ",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want rounded down to an integer. ")]
        public static decimal? INT(decimal? number)
        {
            if (!number.HasValue)
                return null;

            return decimal.Floor(number.Value);
        }

        /// <summary>
        /// Returns the value True or False depending if the value is null.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ISBLANK",
             InsertedText = "ISBLANK(|VALUE|)",
             DataType = 3,
             Hint = "The value True or False depending if the value is blank.",
             Description = "The value can refer to a value, cell reference, formula or named range.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value you want to test. ")]
        public static bool? ISBLANK(string text)
        {
            return (text == null);
        }

        /// <summary>
        /// Returns the value True or False depending if the value is an even number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ISEVEN",
             InsertedText = "ISEVEN(|VALUE|)",
             DataType = 3,
             Hint = "The value True or False depending if the value is an even number.",
             Description = "If number is not an integer, it is truncated. If number is not numeric, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want to test. ")]
        public static bool? ISEVEN(decimal? number)
        {
            if (!number.HasValue)
                return false;

            int iNumber = (int)number.Value;
            return ((iNumber % 2) == 0);
        }

        /// <summary>
        /// Returns the value True or False depending if the value is non text.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ISNONTEXT",
             InsertedText = "ISNONTEXT(|VALUE|)",
             DataType = 3,
             Hint = "The value True or False depending if the value is non text.",
             Description = "The value can refer to a value, cell reference, formula or a named range. If value is a cell reference to an empty cell, then True is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value you want to test. ")]
        public static bool? ISNONTEXT(string text)
        {
            if (text == null)
                return false;

            if (string.IsNullOrWhiteSpace(text))
                return true;

            return decimal.TryParse(text, out decimal d);
        }

        /// <summary>
        /// Returns the value True or False depending if the value is a number.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ISNUMBER",
             InsertedText = "ISNUMBER(|VALUE|)",
             DataType = 3,
             Hint = "The value True or False depending if the value is a number.",
             Description = "The value can refer to a value, cell reference, formula or a named range.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value you want to test. ")]
        public static bool? ISNUMBER(string text)
        {
            if (text == null)
                return false;

            if (string.IsNullOrWhiteSpace(text))
                return false;

            return decimal.TryParse(text, out decimal d);
        }

        /// <summary>
        /// Returns the value True or False depending if the value is text.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ISTEXT",
             InsertedText = "ISTEXT(|VALUE|)",
             DataType = 3,
             Hint = "The value True or False depending if the value is text.",
             Description = "The value can refer to a value, cell reference, formula or a named range.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value you want to test. ")]
        public static bool? ISTEXT(string text)
        {
            if (text == null)
                return false;

            if (string.IsNullOrWhiteSpace(text))
                return true;

            return !decimal.TryParse(text, out decimal d);
        }

        #endregion Functions Named A through I

        #region Functions J - O

        /// <summary>
        /// Returns the first or left most characters in a text string.
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="num_chars">Num of characters from the left</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "LEFT",
             InsertedText = "LEFT(|VALUE|,|CHARS|)",
             DataType = 1,
             Hint = "The first or left most characters in a text string.",
             Description = "If num_chars < 0, then #VALUE! is returned. If num_chars > the length of the text, then text is returned. If num_chars is left blank, then 1 is used. Any blank spaces are counted as individual characters.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string that contains the characters you want to extract. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number of characters you want to extract. ")]
        public static string LEFT(string text, decimal? num_chars = null)
        {
            if (text == null)
                return null;

            int numChars = (int)num_chars.GetValueOrDefault(1);

            if (numChars > text.Length)
                return text;

            return text.Substring(0, numChars);
        }

        /// <summary>
        /// The number of characters in a text string.
        /// </summary>
        /// <param name="text">Text</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "LEN",
             InsertedText = "LEN(|VALUE|)",
             DataType = 2,
             Hint = "The number of characters in a text string.",
             Description = "This function will count how many characters there are in a text string. Any blank spaces are counted as individual characters. Number formatting is not included. Any trailing zeros are counted. This function returns the length of the displayed text or value and not the underlying value.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string you want to find the length of. ")]
        public static decimal? LEN(string text)
        {
            if (text != null)
                return text.Length;
            else
                return null;
        }

        /// <summary>
        /// Returns the natural logarithm of a number.
        /// </summary>
        /// <param name="num">The number you want the logarithm of.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "LN",
             InsertedText = "LN(|VALUE|)",
             DataType = 2,
             Hint = "The natural logarithm of a number.",
             Description = "The natural logarithm of a number is the inverse of the EXP function. Natural logarithms are based on the constant e (2.71828182845904). If number is not numeric, then #VALUE! is returned. A logarithm is an exponent used in calculations to show the perceived levels of variable quantities. This function can be used in problems that calculate exponential growth. This function can be used to estimate population growth over a period of time.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the natural logarithm of. ")]
        public static decimal? LN(decimal? num)
        {
            if (!num.HasValue)
                return null;

            return (decimal)Math.Log(Convert.ToDouble(num.Value));
        }

        /// <summary>
        /// The logarithm of a number to any base.
        /// </summary>
        /// <param name="num">The number you want the logarithm of.</param>
        /// <param name="_base">(Optional) The base of the logarithm.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "LOG",
             InsertedText = "LOG(|VALUE|,|BASE|)",
             DataType = 2,
             Hint = "The logarithm of a number to any base.",
             Description = "If number < 0, then #NUM! is returned. If number is not an integer, it is truncated. If base is left blank, then 10 is used. If any of the arguments are not numeric, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the logarithm of. ")]
        public static decimal? LOG(decimal? num, decimal? _base = null)
        {
            if (!num.HasValue)
                return null;
            if (num < 0)
                throw new ArgumentOutOfRangeException("num");

            double logBase = (double)_base.GetValueOrDefault(10m);
            double numValue = (double)num.Value;

            return (decimal)Math.Log(numValue, logBase);
        }

        /// <summary>
        /// Returns the logarithm of a number to the base 10.
        /// </summary>
        /// <param name="num">The number you want the base-10 logarithm of.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "LOG10",
             InsertedText = "LOG10(|VALUE|)",
             DataType = 2,
             Hint = "The logarithm of a number to the base 10.",
             Description = "If number < 0, then #NUM! is returned. If number is not an integer, it is truncated. If number is not numeric, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the base-10 logarithm of.")]
        public static decimal? LOG10(decimal? num)
        {
            if (!num.HasValue)
                return null;

            return (decimal)Math.Log10(Convert.ToDouble(num.Value));
        }

        /// <summary>
        /// The text string with all the characters converted to lowercase.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "LOWER",
             InsertedText = "LOWER(|VALUE|)",
             DataType = 1,
             Hint = "The text string with all the characters converted to lowercase.",
             Description = "The text can be a cell reference or a text string. Any characters that are numbers will not be changed. Any characters that are symbols or special characters will not be changed. Any characters that are not text will not altered.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string you want to convert to lowercase.")]
        public static string LOWER(string text)
        {
            if (text != null)
                return text.ToLower();
            else
                return null;

        }

        /// <summary>
        /// Returns the largest value in a list or array of numbers.
        /// </summary>
        /// <param name="nums">Array of numbers to parse</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "MAX",
             InsertedText = "MAX(|VALUES|)",
             DataType = 2,
             Hint = "The largest value in a list or array of numbers.",
             Description = "This function includes hidden rows. Arguments that are numbers are included. Arguments that are dates are included. Arguments that are zero are included. Arguments that are logical values are excluded. Arguments that are text are excluded. If any cell references are empty, then these are excluded. If any cell references contain an error, then that error is returned.",
             MinParams = 2,
             MaxParams = 255,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The first number.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The second number. ")]
        public static decimal? MAX(decimal?[] nums)
        {
            if (nums == null || nums.Any(t => !t.HasValue))
                return null;

            if (nums.Count() == 0)
                return null;

            return nums.Max();
        }

        /// <summary>
        /// The text string which is a substring of a given string.
        /// </summary>
        /// <param name="text">The text string containing the characters you want to extract.</param>
        /// <param name="start_num">The position of the first character you want to extract in text.</param>
        /// <param name="num_chars">The number of characters you want to return</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "MID",
             InsertedText = "MID(|VALUE|,|START|,|CHARS|)",
             DataType = 1,
             Hint = "The text string which is a substring of a given string.",
             Description = "The first character in text has a start_num of 1, and so on. If num_chars > the length of text, then a blank string is returned If num_chars < 0, then #VALUE! is returned. If num_chars = 0, then #VALUE! is returned.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string containing the characters you want to extract.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The position of the first character you want to extract in text.",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The number of characters you want to return.")]
        public static string MID(string text, decimal? start_num = null, decimal? num_chars = null)
        {
            if (text == null)
                return null;

            int numChars = (int)num_chars.GetValueOrDefault(1);
            int startNum = (int)start_num.GetValueOrDefault(1)-1;

            return text.Substring(startNum, numChars);
        }

        /// <summary>
        /// Returns the smallest value in a list or array of numbers.
        /// </summary>
        /// <param name="nums">Array of numbers to parse</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "MIN",
             InsertedText = "MIN(|VALUES|)",
             DataType = 2,
             Hint = "The smallest value in a list or array of numbers.",
             Description = "This function includes hidden rows. Arguments that are numbers are included. Arguments that are dates are included. Arguments that are zero are included. Arguments that are logical values are excluded. Arguments that are text are excluded. If any cell references are empty, then these are excluded. If any cell references contain an error, then that error is returned.",
             MinParams = 2,
             MaxParams = 255,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The first number.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The second number. ")]
        public static decimal? MIN(decimal?[] nums)
        {
            if (nums == null || nums.Any(t => !t.HasValue))
                return null;

            if (nums.Count() == 0)
                return null;

            return nums.Min();
        }

        /// <summary>
        /// Returns the remainder after division.
        /// </summary>
        /// <param name="num">The number for which you want to find the remainder of.</param>
        /// <param name="divisor">The number you want to divide by.</param>
        /// <returns></returns>        
        [CBELFunctionInfo(
             Name = "MOD",
             InsertedText = "MOD(|VALUE|,|DIVISOR|)",
             DataType = 2,
             Hint = "The remainder after division.",
             Description = "The result of this function is the remainder when number is divided by divisor. If divisor = 0, then #DIV/0! is returned. If number < divisor, then numbers is returned. If number is exactly divisible by divisor, then 0 is returned. This function can be expressed in terms of the INT function: MOD(n, d) = n - (INT(n/d)*d). The result of this function has the same sign as the sign of the divisor. A known bug with this function is when [ (divisor * 134217728) <= number ]. In this case the function will return #NUM! instead of the correct remainder and you should use the equivalent formula above.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number for which you want to find the remainder of. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number you want to divide by. ")]
        public static decimal? MOD(decimal? num, decimal? divisor)
        {
            if (!num.HasValue)
                return null;

            decimal myDivisor = divisor.GetValueOrDefault(0m);

            if (myDivisor == 0)
                return null;

            return num.Value % myDivisor;
        }

        /// <summary>
        /// Returns the month as an integer given a date serial number.
        /// </summary>
        /// <param name="serial_number">The date that contains the month you want to find.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "MONTH",
             InsertedText = "MONTH(|VALUE|)",
             DataType = 2,
             Hint = "The month as an integer given a date serial number.",
             Description = "This function returns a number between 1 and 12. The serial_number can be a date, value, cell reference or a text string in a date format. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the month you want to find. ")]
        public static decimal? MONTH(decimal? serial_number)
        {
            if (serial_number == null)
                return null;

            if (serial_number < 0)
                return null;

            if (serial_number < 12)
                return serial_number + 1;

            return DateTime.FromOADate(Convert.ToDouble(serial_number)).Month;
        }

        /// <summary>
        /// Returns the month as an integer given a date serial number.
        /// </summary>
        /// <param name="serial_number">Serial Number as a DateTime object</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "MONTH",
             InsertedText = "MONTH(|VALUE|)",
             DataType = 2,
             Hint = "The month as an integer given a date serial number.",
             Description = "This function returns a number between 1 and 12. The serial_number can be a date, value, cell reference or a text string in a date format. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the month you want to find. ")]
        public static decimal? MONTH(DateTime? serial_number)
        {
            if (serial_number.HasValue)
                return serial_number.Value.Month;
            else
                return null;
        }

        /// <summary>
        /// Returns the month as an integer given a date serial number.
        /// </summary>
        /// <param name="serial_number">Serial Number as a string object</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "MONTH",
             InsertedText = "MONTH(|VALUE|)",
             DataType = 2,
             Hint = "The month as an integer given a date serial number.",
             Description = "This function returns a number between 1 and 12. The serial_number can be a date, value, cell reference or a text string in a date format. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the month you want to find. ")]
        public static decimal? MONTH(string serial_number)
        {
            if (serial_number == null)
                return null;

            if (!TryParseDate(serial_number, out DateTime dt))
                throw new DateConversionException($"Invalid Date format in MONTH('{serial_number}').");

            return dt.Month;
        }

        /// <summary>
        /// Returns the opposite of a True or False value.
        /// </summary>
        /// <param name="logical">The value or expression that can be evaluated to True or False.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "NOT",
             InsertedText = "NOT(|VALUE|)",
             DataType = 3,
             Hint = "The opposite of a True or False value.",
             Description = "This function returns either True or False. This function can be used when you want to return the opposite logical value. The logical can be a conditional test, an array or a cell reference. If logical evaluates to False, then True is returned. If logical evaluates to True, then False is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 3,
             Param1Hint = "The value or expression that can be evaluated to True of False. ")]
        public static bool? NOT(bool? logical)
        {
            if (!logical.HasValue)
                return null;

            return !logical.Value;
        }

        /// <summary>
        /// Returns the serial number of the current system date and time.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "NOW",
             DataType = 9,
             Hint = "The serial number of the current system date and time.",
             Description = "This is one of few functions that does not require any arguments. This function is volatile and will change everytime a cell on the worksheet is calculated. This function will update when the workbook is recalculated. You must include the empty parentheses after the function name. The serial number is the date-time code used by Excel for date and time calculations. If the cell has the General number format, then this cell is formatted automatically to dd/mm/yyyy hh:mm. If you want to enter a static date that will not change or update, use the shortcut key (Ctrl + ;). If you want to enter a static time that will not change or update, use the shortcut key (Ctrl + Shift + ;).",
             MinParams = 0,
             MaxParams = 0)]
        public static DateTime? NOW()
        {
            return DateTime.UtcNow;
        }

        /// <summary>
        /// Returns the text to number in a locale independent manner.
        /// </summary>
        /// <param name="text">The text to convert to a number.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "NUMBERVALUE",
             InsertedText = "NUMBERVALUE(|VALUE|)",
             DataType = 2,
             Hint = "(2013) The text to number in a locale independent manner.",
             Description = "If decimal_separator is left blank, then the current locale is used. If group_separator is left blank, then the current locale is used. If the group_separator occurs before the decimal separator, the group separator is ignored. If the group_separator occurs after the decimal separator, then #NUM is returned. If text contains any spaces, they are ignored. If decimal_separator contains more than 1 character, only the first character is used. If group_separator contains more than 1 character, only the first character is used.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text to convert to a number.",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "The character used to separate integer and fraction. ",
             Param3Name = "Value3",
             Param3DataType = 1,
             Param3Hint = "The character used to separate thousands and millions. ")]
        public static decimal? NUMBERVALUE(string text)
        {
            if (text == null)
                return null;

            if (text.Contains('%'))
            {
                int percentCount = text.Count(c => c == '%');
                text = text.Replace('%', ' ');
                if (decimal.TryParse(text, out decimal result))
                {
                    for (int i = 0; i < percentCount; i++)
                        result = result / 100;

                    return result;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                if (decimal.TryParse(text, out decimal result))
                    return result;
                else
                    return null;
            }
        }

        /// <summary>
        /// Returns the number rounded up to the nearest odd integer.
        /// </summary>
        /// <param name="num">The number you want to round down.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ODD",
             InsertedText = "ODD(|VALUE|)",
             DataType = 2,
             Hint = "The number rounded up to the nearest odd integer.",
             Description = "If number is not numeric, then #VALUE! is returned. If number < 0, then the number is rounded down. If number is an odd number, then no rounding takes place.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want to round down. ")]
        public static decimal? ODD(decimal? num)
        {
            if (!num.HasValue)
                return null;

            if (num.Value % 2 == 0)
            {
                if (num.Value < 0)
                    return num - 1;
                else
                    return num + 1;
            }
            else
            {
                return num;
            }
        }

        /// <summary>
        /// Returns the logical OR for any number of arguments.";
        /// See https://bettersolutions.com/excel/functions/or-function.htm for additional information.
        /// </summary>
        /// <param name="Conditions"></param>
        /// <returns>Boolean value of Result</returns>
        [CBELFunctionInfo(
             Name = "OR",
             InsertedText = "OR(|VALUES|)",
             DataType = 3,
             Hint = "The logical OR for any number of arguments.",
             Description = "This function returns either True or False. The logical values can be constants, logical equations, cell references or named ranges. The arguments must evaluate to logical values such as True or False, or in arrays or references that contain logical values. If an array or reference argument contains text or empty cells, those values are ignored. If the specified range contains no logical values, OR returns the #VALUE! error value.",
             MinParams = 2,
             MaxParams = 255,
             Param1Name = "Value1",
             Param1DataType = 3,
             Param1Hint = "The first logical value. ",
             Param2Name = "Value2",
             Param2DataType = 3,
             Param2Hint = "The second logical value")]
        public static bool? OR(params bool?[] Conditions)
        {
            if (Conditions == null || Conditions.Any(c => !c.HasValue))
                return false;

            bool result = false;
            foreach (var p in Conditions)
            {
                if (p.Value)
                {
                    result = true;
                    break;
                }
            }
            return (result);
        }

        #endregion

        #region Functions P through Z

        /// <summary>
        /// Returns the number PI (3.141592).
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "PI",
             DataType = 2,
             Hint = "The number PI (3.141592).",
             Description = "This is one of the few functions that does not require any arguments. You must include the empty parentheses after the function name. This constant is used to calculate the area of a circle.",
             MinParams = 0,
             MaxParams = 0)]
        public static decimal? PI()
        {
            return (decimal?)Math.PI;
        }

        /// <summary>
        /// Returns the random number more than or equal to 0 and less than 1.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "RAND",
             DataType = 2,
             Hint = "The random number >=0 and <1.",
             Description = "This function returns a number greater than or equal to zero and less than one. This function is volatile and will change everytime a cell on the worksheet is calculated. This function returns random values based on a uniform distribution. This function will update when the workbook is recalculated by pressing F9. This is one of the few functions that does not require any arguments. You must include the empty parentheses after the function name. The result from this function will change every time that the worksheet is calculated. If you have your calculation set to automatic then this will change every time you make an entry on the worksheet. To generate a random real number between a and b, use: RAND()*(b-a)+a If you want to generate a random number but don't want the number to change every time the cell is calculated, you can enter =RAND() in the formula bar, and press F9 to replace the formula with the value.",
             MinParams = 0,
             MaxParams = 0)]
        public static decimal? RAND()
        {
            Random random = new Random();
            return (decimal?)random.NextDouble();
        }

        /// <summary>
        /// Returns The text string with a number of characters replaced.
        /// </summary>
        /// <param name="old_text">The text you want to replace.</param>
        /// <param name="start_num">The position of the character in "old_text" that is to be replaced.</param>
        /// <param name="num_chars">The number of characters in "old_text" that you want replaced.</param>
        /// <param name="new_text">The text you want to replace "old_text" with.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "REPLACE",
             InsertedText = "REPLACE(|OLD|,|START|,|CHARS|,|NEW|)",
             DataType = 1,
             Hint = "The text string with a number of characters replaced.",
             Description = "This function uses the position to replace characters, so uppercase or lowercase is not relevant. This function should be used when you want to replace text that occurs in a specific location in a text string.",
             MinParams = 4,
             MaxParams = 4,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text you want to replace.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The position of the character in \"old text\" that is to be replaced. ",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The number of characters in \"old text\" that you want replaced.",
             Param4Name = "Value4",
             Param4DataType = 1,
             Param4Hint = "The text you want to replace \"old text\" with.")]
        public static string REPLACE(string old_text, decimal? start_num, decimal? num_chars, string new_text)
        {
            if (old_text == null)
                return null;
            if (new_text == null)
                return null;

            int startNum = (int)start_num.GetValueOrDefault(1m);
            int numChars = (int)num_chars.GetValueOrDefault(1m);

            var firstPart = old_text.Substring(0, startNum - 1);
            var secondPart = old_text.Substring(startNum - 1 + numChars);
            return firstPart + new_text + secondPart;
        }

        /// <summary>
        /// Returns the text string repeated a number of times.
        /// </summary>
        /// <param name="text">The text you want to repeat.</param>
        /// <param name="number_times">	The number of times to repeat the text.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "REPT",
             InsertedText = "REPLACE(|VALUE|,|TIMES|)",
             DataType = 1,
             Hint = "The text string repeated a number of times.",
             Description = "You can use this function to create a simple histogram on your worksheet. If number_times = 0, then an empty string () is returned. If number_times is not an integer, it is truncated. If number_times > 32, 767, then #VALUE! is returned. If number_times < 0, then #VALUE! is returned. If number_times is not numeric, then #VALUE! is returned.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text you want to repeat.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number of times to repeat the text. ")]
        public static string REPT(string text, decimal? number_times)
        {
            if (text == null)
                return null;

            if (text == "")
                return "";

            string returnVal = "";
            for (int i=0; i<number_times; i++)
                returnVal += text;

            return returnVal;
        }

        /// <summary>
        /// Returns the last or right most characters in a text string.
        /// </summary>
        /// <param name="text">The text string that contains the characters you want to extract.</param>
        /// <param name="num_chars">(Optional) The number of characters you want to extract.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "RIGHT",
             InsertedText = "RIGHT(|VALUE|,|CHARS|)",
             DataType = 1,
             Hint = "The last or right most characters in a text string.",
             Description = "If text is a text string then it must be enclosed in speech marks. If num_chars > the length of the text, then text is returned. If num_chars is left blank, then 1 is used. If num_chars is not numerical, then #VALUE! is returned. If num_chars < 0, then #VALUE! is returned. Any blank spaces are counted as individual characters.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string that contains the characters you want to extract. ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number of characters you want to extract. ")]
        public static string RIGHT(string text, decimal? num_chars = null)
        {
            if (text == null)
                return null;

            int numChars = (int)num_chars.GetValueOrDefault(1m);

            if (numChars >= text.Length)
                return text;

            return text.Substring(text.Length - (numChars + 1));
        }

        /// <summary>
        /// The number rounded to a specified number of digits.
        /// </summary>
        /// <param name="number">The number you want to round.</param>
        /// <param name="digits">The number of digits to which you want to round to.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ROUND",
             InsertedText = "ROUND(|VALUE|,|DIGITS|)",
             DataType = 2,
             Hint = "The number rounded to a specified number of digits.",
             Description = "Any digits less than 5 are rounded down. Any digits greater than or equal to 5 are rounded up. The number can be a number, a cell reference or a named range. If num_digits > 0, then number is rounded to the specified number of decimal places. If num_digits = 0, then number is rounded to the nearest integer. If num_digits < 0, then number is rounded to the left of the decimal point. This function operates on the actual value in the cell and rounds them so they are consistent with the value that is displayed. Using this function will change the underlying value and not just the value that is displayed.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want to round.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number of digits to which you want to round to. ")]
        public static decimal? ROUND(decimal? number, decimal? digits)
        {
            if (!number.HasValue)
                return null;

            int numDigits = (int)digits.GetValueOrDefault(0m);

            decimal places = (decimal)Math.Pow(10, -numDigits);
            return Math.Round(number.Value / places) * places;
        }

        /// <summary>
        /// The number rounded down to the nearest integer.
        /// </summary>
        /// <param name="number">The number you want to round.</param>
        /// <param name="digits">The number of digits you want to round the number down to.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ROUNDDOWN",
             InsertedText = "ROUNDDOWN(|VALUE|,|DIGITS|)",
             DataType = 2,
             Hint = "The number rounded down to the nearest integer.",
             Description = "This function is identical to the ROUND function, except that it always rounds a number down. If num_digits > 0, then number is rounded down to the specified number of decimal places. If num_digits = 0 then number is rounded down to the nearest integer. If num_digits < 0, then number is rounded down to the left of the decimal point.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want to round down (towards zero). ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number of digits you want to round the number down to. ")]
        public static decimal? ROUNDDOWN(decimal? number, decimal? digits)
        {
            if (!number.HasValue)
                return null;

            int numDigits = (int)digits.GetValueOrDefault(0m);

            decimal places = (decimal)Math.Pow(10, -numDigits);
            /*
            decimal myNumber = Math.Round(number.Value / places) * places;

            return Math.Truncate(myNumber);
            */
            return Math.Floor(number.Value / places) * places;
        }

        /// <summary>
        /// The number rounded up to the nearest integer.
        /// </summary>
        /// <param name="number">The number you want to round.</param>
        /// <param name="digits">The number of digits you want to round the number up to.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "ROUNDUP",
             InsertedText = "ROUNDUP(|VALUE|,|DIGITS|)",
             DataType = 2,
             Hint = "The number rounded up to the nearest integer.",
             Description = "This function is identical to the ROUND function, except that it always rounds a number up. If num_digits > 0, then number is rounded up to the specified number of decimal places. If num_digits = 0, then number is rounded up to the nearest integer. If num_digits is left blank, then number is rounded up to the nearest integer. If num_digits < 0, then number is rounded up to the left of the decimal point.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want to round up (away from zero). ",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number of digits you want to round the number to. ")]
        public static decimal? ROUNDUP(decimal? number, decimal? digits)
        {
            if (!number.HasValue)
                return null;

            int numDigits = (int)digits.GetValueOrDefault(0m);

            decimal places = (decimal)Math.Pow(10, -numDigits);

            return Math.Ceiling(number.Value / places) * places;
        }

        /// <summary>
        /// The position of a substring within a larger text string.
        /// </summary>
        /// <param name="find_text">The text string you want to find.</param>
        /// <param name="within_text">The text in which you want to search for "find_text".</param>
        /// <param name="start_num">(Optional) The character number in "within_text" at which to start searching.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SEARCH",
             InsertedText = "SEARCH(|FIND|,|WITHIN|,|START|)",
             DataType = 2,
             Hint = "The position of a substring within a larger text string.",
             Description = "This function is not case sensitive. This function does support wildcards. This function will always return the first instance of the text you want to find, reading from left to right. If find_text does not appear in within_text, then #VALUE! is returned. The find_text can include wildcards: ? = a single character, * = multiple characters. If start_num > the length of within_text, then #VALUE is returned. If start_num < 0, then #VALUE! is returned. If start_num = 0, then #VALUE! is returned. If start_num is left blank, then 1 is used. If you want to find an actual question mark or asterisk, type a tilde (~) before the character. Any blank spaces or punctuation marks count as individual characters.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string you want to find. ",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "The text in which you want to search for \"find_text\".",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The character number in \"within_text\" at which to start searching. ")]
        public static decimal? SEARCH(string find_text, string within_text, decimal? start_num = null)
        {
            if (find_text == null)
                return null;
            if (within_text == null)
                return null;

            int startNum = (int)start_num.GetValueOrDefault(0m);

            return within_text.IndexOf(find_text, startNum);
        }

        /// <summary>
        /// Returns the seconds as an integer given a date serial number.
        /// </summary>
        /// <param name="serial_number">The date that contains the seconds you want to find.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SECOND",
             InsertedText = "SECOND(|VALUE|)",
             DataType = 9,
             Hint = "The number of seconds as an integer given a date serial number.",
             Description = "This function returns a number between 0 and 59. The serial_number can be a time, value, cell reference or a text string in a time format. Times may be entered as text strings within quotation marks, as decimal numbers or as results of other formulas or functions. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid time or date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The time that contains the seconds you want to find. ")]
        public static decimal? SECOND(decimal? serial_number)
        {
            if (serial_number == null)
                return null;

            if (serial_number < 0)
                return null;

            if (serial_number < 12)
                return serial_number + 1;

            return DateTime.FromOADate(Convert.ToDouble(serial_number)).Second;
        }

        /// <summary>
        /// Returns the seconds as an integer given a date serial number.
        /// </summary>
        /// <param name="serial_number">The date that contains the seconds you want to find.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SECOND",
             InsertedText = "SECOND(|VALUE|)",
             DataType = 9,
             Hint = "The number of seconds as an integer given a date serial number.",
             Description = "This function returns a number between 0 and 59. The serial_number can be a time, value, cell reference or a text string in a time format. Times may be entered as text strings within quotation marks, as decimal numbers or as results of other formulas or functions. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid time or date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The time that contains the seconds you want to find. ")]
        public static decimal? SECOND(DateTime? serial_number)
        {
            if (serial_number.HasValue)
                return serial_number.Value.Second;
            else
                return null;
        }

        /// <summary>
        /// Returns the seconds as an integer given a date serial number.
        /// </summary>
        /// <param name="serial_number">The date that contains the seconds you want to find.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SECOND",
             InsertedText = "SECOND(|VALUE|)",
             DataType = 9,
             Hint = "The number of seconds as an integer given a date serial number.",
             Description = "This function returns a number between 0 and 59. The serial_number can be a time, value, cell reference or a text string in a time format. Times may be entered as text strings within quotation marks, as decimal numbers or as results of other formulas or functions. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid time or date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The time that contains the seconds you want to find. ")]
        public static decimal? SECOND(string serial_number)
        {
            if (serial_number == null)
                return null;

            if (!TryParseDate(serial_number, out DateTime dt))
                throw new DateConversionException($"Invalid Date format in SECOND('{serial_number}').");

            return dt.Second;
        }

        /// <summary>
        /// The numerical value indicating if a number is positive or negative.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SIGN",
             InsertedText = "SIGN(|VALUE|)",
             DataType = 2,
             Hint = "The numerical value indicating if a number is positive or negative.",
             Description = "If number > 0, then 1 is returned. If number = 0, then 0 is returned. If number < 0, then -1 is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the sign of. ")]
        public static decimal? SIGN(decimal? number)
        {
            if (!number.HasValue)
                return null;

            return Math.Sign(number.Value);
        }

        /// <summary>
        /// The sine of a number.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SIN",
             InsertedText = "SIN(|VALUE|)",
             DataType = 2,
             Hint = "The sine of a number.",
             Description = "This function is periodical with a period of 2PI. The number is the angle in radians. If number is not numeric, then #VALUE! is returned. If your argument is in degrees, multiply it by PI/180 to convert it to radians.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the sine of. ")]
        public static decimal? SIN(decimal? number)
        {
            if (!number.HasValue)
                return null;

            return (decimal)Math.Sin(Convert.ToDouble(number.Value));
        }

        /// <summary>
        /// The positive square root of a number.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SQRT",
             InsertedText = "SQRT(|VALUE|)",
             DataType = 2,
             Hint = "The positive square root of a number.",
             Description = "If number < 0, then #NUM! is returned. If number is not numeric, then #VALUE! is returned. You can also calculate the square root of a number by raising it the power of one half, for example 16^0.5 = 4.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want to find the square root of. ")]
        public static decimal? SQRT(decimal? number)
        {
            if (!number.HasValue)
                return null;

            return (decimal?)Math.Sqrt(Convert.ToDouble(number.Value));
        }

        /// <summary>
        /// The text string with a substring substituted for another substring.
        /// </summary>
        /// <param name="text">The text for which you want to substitute characters.</param>
        /// <param name="old_text">The text you want to replace.</param>
        /// <param name="new_text">	The text you want to replace old_text with.</param>
        /// <param name="instance_num">(Optional) The occurrence number you want to replace.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SUBSTITUTE",
             InsertedText = "SUBSTITUTE(|VALUE|,|OLD|,|NEW|,|INSTANCE|)",
             DataType = 1,
             Hint = "The text string with a substring substituted for another substring.",
             Description = "This function is case sensitive. This function does not support wildcards. This function should be used when you want to replace all the instances of some text regardless of their location in the text. The text can be a cell reference. If you specify instance_num, only that instance of old_text is replaced. If instance_num = [] you will replace every occurrence of old_text. If instance_num is left blank, every occurrence of old_text is changed to new_text.",
             MinParams = 4,
             MaxParams = 4,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text for which you want to substitute characters. ",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "The text you want to replace.",
             Param3Name = "Value3",
             Param3DataType = 1,
             Param3Hint = "The text you want to replace old_text with. ",
             Param4Name = "Value4",
             Param4DataType = 2,
             Param4Hint = "The occurrence number you want to replace. ")]
        public static string SUBSTITUTE(string text, string old_text, string new_text, decimal? instance_num = null)
        {
            if (text == null)
                return null;
            if (old_text == null)
                return null;
            if (new_text == null)
                return null;

            int instanceNum = (int)instance_num.GetValueOrDefault(0m);

            if (instanceNum == 0)
                return text.Replace(old_text, new_text);

            int count = 0;
            int cursor = 0;

            while (cursor != -1)
            {
                cursor = text.IndexOf(old_text, cursor);
                count++;

                if (count == instanceNum)
                {
                    string start = text.Substring(0, cursor);
                    string end = text.Substring(cursor + old_text.Length, text.Length - cursor - old_text.Length);
                    return start + new_text + end;
                }
                cursor = cursor + old_text.Length;
            }

            return text;
        }

        /// <summary>
        /// The total value of the numbers in a list, table or cell range.
        /// </summary>
        /// <param name="nums">Array of numbers to parse</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "SUM",
             InsertedText = "SUM(|VALUES|)",
             DataType = 2,
             Hint = "The total value of the numbers in a list, table or cell range.",
             Description = "This function includes hidden rows. Arguments that are logical values are excluded. Arguments that are text are excluded. If any cell references are empty, then these are excluded. If any cell references contain an error, then that error is returned.",
             MinParams = 2,
             MaxParams = 255,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The first number.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The second number.")]
        public static decimal? SUM(decimal?[] nums)
        {
            if (nums == null || nums.Count() == 0)
                return null;

            return nums.Sum();
        }

        /// <summary>
        /// The text string of the value given.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "T",
             InsertedText = "T(|VALUE|)",
             DataType = 1,
             Hint = "The text string of the value given.",
             Description = "If value is text, then value is returned. If value is a number, (empty string) is returned. This function is provided for compatibility with other spreadsheet programs. This function is not used a lot as Microsoft Excel automatically converts values when necessary.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The value you want to test. ")]
        public static string T(decimal? number)
        {
            if (!number.HasValue)
                return null;

            return number.ToString();
        }

        /// <summary>
        /// The tangent of a number.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TAN",
             InsertedText = "TAN(|VALUE|)",
             DataType = 2,
             Hint = "The tangent of a number.",
             Description = "This function is periodic with a period of 2PI. The number is the angle in radians. If number is not numeric, then #VALUE! is returned. If your argument is in degrees, multiply it by PI/180 to convert it to radians. If you want the result to be in degrees, multiply the result by 180/PI.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want the tangent of.")]
        public static decimal? TAN(decimal? number)
        {
            if (!number.HasValue)
                return null;

            return (decimal)Math.Tan(Convert.ToDouble(number.Value));
        }

        /// <summary>
        /// The number as a formatted text string.
        /// </summary>
        /// <param name="value">The value representing the number you want converted to text.</param>
        /// <param name="format_text">The display format.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TEXT",
             InsertedText = "TEXT(|VALUE|,|FORMAT|)",
             DataType = 1,
             Hint = "The number as a formatted text string.",
             Description = "The value can be a numeric value, a cell reference or a formula that evaluates to a numeric value. The format_text argument must be enclosed in speech marks. The format_text cannot contain an asterisk (*) and cannot be the General number format. Formatting a cell using the (Format > Cells) dialog box only changes how the value is displayed. Using this function will actually change the value to text, and the actual value might have changed.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text value of the number you want to format",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "The format string indicating how the number should be formatted.")]
        public static string TEXT(decimal? value, string format_text)
        {
            if (!value.HasValue)
                return null;

            return value.Value.ToString(format_text);
        }

        /// <summary>
        /// The number as a formatted text string.
        /// </summary>
        /// <param name="value">The value representing the string you want converted to text.</param>
        /// <param name="format_text">The display format.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TEXT",
             InsertedText = "TEXT(|VALUE|,|FORMAT|)",
             DataType = 1,
             Hint = "The number as a formatted text string.",
             Description = "The value can be a numeric value, a cell reference or a formula that evaluates to a numeric value. The format_text argument must be enclosed in speech marks. The format_text cannot contain an asterisk (*) and cannot be the General number format. Formatting a cell using the (Format > Cells) dialog box only changes how the value is displayed. Using this function will actually change the value to text, and the actual value might have changed.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text value of the number you want to format",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "The format string indicating how the number should be formatted.")]
        public static string TEXT(string value, string format_text)
        {
            if (value == null)
                return null;

            if (TryParseDate(value, out DateTime dtResult))
                return dtResult.ToString(format_text);

            if (decimal.TryParse(value, out decimal dResult))
                return dResult.ToString(format_text);
            
            return value;
        }

        /// <summary>
        /// The number as a formatted text string.
        /// </summary>
        /// <param name="value">The value representing the Date/Time you want converted to text.</param>
        /// <param name="format_text">The display format.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TEXT",
             InsertedText = "TEXT(|VALUE|,|FORMAT|)",
             DataType = 1,
             Hint = "The number as a formatted text string.",
             Description = "The value can be a numeric value, a cell reference or a formula that evaluates to a numeric value. The format_text argument must be enclosed in speech marks. The format_text cannot contain an asterisk (*) and cannot be the General number format. Formatting a cell using the (Format > Cells) dialog box only changes how the value is displayed. Using this function will actually change the value to text, and the actual value might have changed.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text value of the number you want to format",
             Param2Name = "Value2",
             Param2DataType = 1,
             Param2Hint = "The format string indicating how the number should be formatted.")]
        public static string TEXT(DateTime? value, string format_text)
        {
            if (!value.HasValue)
                return null;

            return value.Value.ToString(format_text);
        }

        /// <summary>
        /// The time as a decimal given an hour, minute, second.
        /// </summary>
        /// <param name="hour">The number between 0 and 23 representing the hour.</param>
        /// <param name="minute">The number between 0 and 59 representing the minute.</param>
        /// <param name="second">The number between 0 and 59 representing the second.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TIME",
             InsertedText = "TIME(|HOUR|,|MINUTE|,|SECOND|)",
             DataType = 9,
             Hint = "The time as a decimal given an hour, minute, second.",
             Description = " If hour > 23, then hour Mod 24 is used. If minute > 59, then minute is converted to hours and minutes. If second > 59, then second is converted to minutes and seconds. If hour < 0, then #NUM! is returned. If minute < 0, then the minutes are subtracted. If second < 0, then the seconds are subtracted.",
             MinParams = 3,
             MaxParams = 3,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number between 0 and 23 representing the hour.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number between 0 and 59 representing the minute.",
             Param3Name = "Value3",
             Param3DataType = 2,
             Param3Hint = "The number between 0 and 59 representing the second. ")]
        public static DateTime? TIME(decimal? hour, decimal? minute, decimal? second)
        {
            if (!hour.HasValue || !minute.HasValue || !second.HasValue)
                return null;

            int iHour = (int)hour;
            int iMinute = (int)minute;
            int iSecond = (int)second;

            if (iHour < 0 || iHour >= 24)
                throw new DateConversionException($"Invalid hour value in TIME({iHour}, {iMinute}, {iSecond})");

            if (iMinute < 0 || iMinute >= 60)
                throw new DateConversionException($"Invalid minute value in TIME({iHour}, {iMinute}, {iSecond})");

            if (iSecond < 1 || iSecond >= 60)
                throw new DateConversionException($"Invalid second value in TIME({iHour}, {iMinute}, {iSecond})");

            return new DateTime(1900, 1, 1, iHour, iMinute, iSecond);
        }

        #region TIMEVALUE
        private static decimal? DoTIMEVALUE(DateTime valueDT)
        {
            return (decimal)valueDT.TimeOfDay.TotalDays;
        }

        /// <summary>
        /// Returns the time serial number given a datetime.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TIMEVALUE",
             InsertedText = "TIMEVALUE(|VALUE|)",
             DataType = 9,
             Hint = "The time as a decimal given a time in text format.",
             Description = "In Excel times are represented as decimals. If time_text contains any date information, it is ignored. If time_text is not a valid time, then #VALUE! is returned. The decimal number is a value ranging from 0 (zero) to 0.99999999, representing the times from 0:00:00 (12:00:00 AM) to 23:59:59 (11:59:59 P.M.). Time values are a portion of a date value and represented by a decimal number (for example, 12:00 PM is represented as 0.5 because it is half of a day). The time format can be any of those on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The text string that represents the time. ")]
        public static decimal? TIMEVALUE(DateTime? valueDT)
        {
            if (!valueDT.HasValue)
                return null;

            return DoTIMEVALUE(valueDT.Value);
        }

        /// <summary>
        /// Returns the time serial number given a datetime.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TIMEVALUE",
             InsertedText = "TIMEVALUE(|VALUE|)",
             DataType = 9,
             Hint = "The time as a decimal given a time in text format.",
             Description = "In Excel times are represented as decimals. If time_text contains any date information, it is ignored. If time_text is not a valid time, then #VALUE! is returned. The decimal number is a value ranging from 0 (zero) to 0.99999999, representing the times from 0:00:00 (12:00:00 AM) to 23:59:59 (11:59:59 P.M.). Time values are a portion of a date value and represented by a decimal number (for example, 12:00 PM is represented as 0.5 because it is half of a day). The time format can be any of those on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The text string that represents the time. ")]
        public static decimal? TIMEVALUE(decimal? valueDT)
        {
            return valueDT;
        }

        /// <summary>
        /// Returns the time serial number given a datetime.
        /// </summary>
        /// <param name="valueDT"></param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TIMEVALUE",
             InsertedText = "TIMEVALUE(|VALUE|)",
             DataType = 9,
             Hint = "The time as a decimal given a time in text format.",
             Description = "In Excel times are represented as decimals. If time_text contains any date information, it is ignored. If time_text is not a valid time, then #VALUE! is returned. The decimal number is a value ranging from 0 (zero) to 0.99999999, representing the times from 0:00:00 (12:00:00 AM) to 23:59:59 (11:59:59 P.M.). Time values are a portion of a date value and represented by a decimal number (for example, 12:00 PM is represented as 0.5 because it is half of a day). The time format can be any of those on the Format Cells dialog box.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The text string that represents the time. ")]
        public static decimal? TIMEVALUE(string valueDT)
        {
            if (valueDT == null)
                return null;

            if (!TryParseDate(valueDT, out DateTime dt))
                throw new DateConversionException($"Invalid Date format in TIMEVALUE('{valueDT}').");

            return DoTIMEVALUE(dt);
        }
        #endregion TIMEVALUE

        /// <summary>
        /// The time as a decimal given a time in text format.
        /// </summary>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TODAY",
             DataType = 9,
             Hint = "The serial number representing today's date.",
             Description = "This is one of the few functions that does not require any arguments. This function is volatile and will change everytime a cell on the worksheet is calculated. This function will update when the workbook is recalculated. You must include the empty parentheses after the function name. The serial number is the date-time code used by Microsoft Excel for date and time calculations. If the cell has the General number format, then this cell is automatically formatted dd/mm/yyyy. If you want to enter a static date that will not change or update, use the shortcut key (Ctrl + ;). If you want to enter a static time that will not change or update, use the shortcut key (Ctrl + Shift + ;).",
             MinParams = 0,
             MaxParams = 0)]
        public static DateTime? TODAY()
        {
            return DateTime.UtcNow.Date;
        }

        /// <summary>
        /// The text string with all extra spaces removed from the beginning, middle and end.
        /// </summary>
        /// <param name="text">The text from which you want spaces removed.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TRIM",
             InsertedText = "TRIM(|VALUE|)",
             DataType = 1,
             Hint = "The text string with all extra spaces removed from the beginning, middle and end.",
             Description = "Extra spaces can prevent your formulas from working correctly. The only spaces left will be a single space between words. If words have more than one space between them, they are replaced with a single space. This function will only remove the ASCII space character (32) and not any other characters.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text from which you want spaces removed. ")]
        public static string TRIM(string text)
        {
            if (text == null)
                return null;

            return text.Trim();
        }

        /// <summary>
        /// The number with any decimal places removed.
        /// </summary>
        /// <param name="number">The number you want to truncate.</param>
        /// <param name="num_digits">(Optional) The number specifying the precision of the truncation.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "TRUNC",
             InsertedText = "TRIM(|VALUE|,|DIGITS|)",
             DataType = 2,
             Hint = "The number with any decimal places removed.",
             Description = "This function is similar to the INT function. If the num_digits is left blank then 0 is used.",
             MinParams = 2,
             MaxParams = 2,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The number you want to truncate.",
             Param2Name = "Value2",
             Param2DataType = 2,
             Param2Hint = "The number specifying the precision of the truncation. ")]
        public static decimal? TRUNC(decimal? number, decimal? num_digits = null)
        {
            if (!number.HasValue)
                return null;

            int numDigits = (int)num_digits.GetValueOrDefault(0m);

            int decimalPlaces = 1;
            for (int x = 1; x <= num_digits; x++)
                decimalPlaces = decimalPlaces * 10;

            return Math.Truncate(number.Value * decimalPlaces) / decimalPlaces;
        }

        /// <summary>
        /// The text string with all the characters converted to uppercase.
        /// </summary>
        /// <param name="text">The text string you want to convert to uppercase.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "UPPER",
             InsertedText = "UPPER(|VALUE|)",
             DataType = 1,
             Hint = "The text string with all the characters converted to uppercase.",
             Description = "The text can be a cell reference or a text string. Any characters that are not text will not be altered.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string you want to convert to uppercase. ")]
        public static string UPPER(string text)
        {
            if (text == null)
                return null;

            return text.ToUpper();
        }

        /// <summary>
        /// The number that a text string represents.
        /// </summary>
        /// <param name="text">The text string that represents a number.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "VALUE",
             InsertedText = "VALUE(|VALUE|)",
             DataType = 2,
             Hint = "The number that a text string represents.",
             Description = "Text can be a number constant, date, or time format. If text is not in one of these formats, then #VALUE! is returned. The text enclosed in quotation marks or a reference to a cell containing the text you want to convert.  This function is provided for backwards compatibility with other spreadsheet programs.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 1,
             Param1Hint = "The text string that represents a number. ")]
        public static decimal? VALUE(string text)
        {
            if (text == null)
                return null;

            if (decimal.TryParse(text, out decimal dValue))
                return dValue;
            else
                return null;
        }

        #region WEEKDAY
        private static decimal? DoWEEKDAY(DateTime date, decimal? return_type)
        {
            int returnType = (int)return_type.GetValueOrDefault(1m);

            switch (returnType)
            {
                case 2: // Monday is the start of week
                case 11:
                    if (date.DayOfWeek == DayOfWeek.Sunday)
                        return 7;
                    else
                        return (int)date.DayOfWeek;
                case 3: // 0 based w/ Monday as start of week
                    if (date.DayOfWeek == DayOfWeek.Sunday)
                        return 6;
                    else
                        return (int)date.DayOfWeek - 1;
                case 12: // Tuesday is the start of the week
                    if ((int)date.DayOfWeek < 2)
                        return ((int)date.DayOfWeek - 1) + 7;
                    else
                        return (int)date.DayOfWeek - 1;
                case 13: // Wednesday is the start of the week
                    if ((int)date.DayOfWeek < 3)
                        return ((int)date.DayOfWeek - 2) + 7;
                    else
                        return (int)date.DayOfWeek - 2;
                case 14: // Thursday is the start of the week
                    if ((int)date.DayOfWeek < 4)
                        return ((int)date.DayOfWeek - 3) + 7;
                    else
                        return (int)date.DayOfWeek - 3;
                case 15: // Friday is the start of the week
                    if ((int)date.DayOfWeek < 5)
                        return ((int)date.DayOfWeek - 4) + 7;
                    else
                        return (int)date.DayOfWeek - 4;
                case 16: // Saturday is the start of the week
                    if ((int)date.DayOfWeek < 6)
                        return ((int)date.DayOfWeek - 5) + 7;
                    else
                        return (int)date.DayOfWeek - 5;
                case 1:
                case 17:
                default:
                    return (int)date.DayOfWeek + 1;
            }
        }

        /// <summary>
        /// The day of the week for a given date.
        /// </summary>
        /// <param name="date">The date</param>
        /// <param name="return_type">(Optional) The number that specifies on what day the week starts</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "WEEKDAY",
             InsertedText = "WEEKDAY(|VALUE|,|TYPE|)",
             DataType = 2,
             Hint = "The day of the week for a given date.",
             Description = "This function returns a number between 0 and 7. The serial_number can be a date value, a serial number or a reference to a cell containing a date. The serial_number cannot be a text string. If return_type is left blank, the 1 is used. If return_type = 1, then Sun = 1, Mon = 2, Tue = 3, Wed = 4, Thu = 5, Fri = 6 and Sat = 7. If return_type = 2, then Mon = 1, Tue = 2, Wed = 3, Thu = 4, Fri = 5, Sat = 6 and Sun = 7. If return_type = 3, then Mon = 0, Tue = 1, Wed = 2, Thu = 3, Fri = 4, Sat = 5 and Sun = 6. If return_type = 11, then Mon = 1, Tue = 2, Wed = 3, Thu = 4, Fri = 5, Sat = 6 and Sun = 7.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date as a serial number. ")]
        public static decimal? WEEKDAY(DateTime? date, decimal? return_type = null)
        {
            if (!date.HasValue)
                return null;

            return DoWEEKDAY(date.Value, return_type);
        }

        /// <summary>
        /// The day of the week for a given date.
        /// </summary>
        /// <param name="date">The date</param>
        /// <param name="return_type">(Optional) The number that specifies on what day the week starts</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "WEEKDAY",
             InsertedText = "WEEKDAY(|VALUE|,|TYPE|)",
             DataType = 2,
             Hint = "The day of the week for a given date.",
             Description = "This function returns a number between 0 and 7. The serial_number can be a date value, a serial number or a reference to a cell containing a date. The serial_number cannot be a text string. If return_type is left blank, the 1 is used. If return_type = 1, then Sun = 1, Mon = 2, Tue = 3, Wed = 4, Thu = 5, Fri = 6 and Sat = 7. If return_type = 2, then Mon = 1, Tue = 2, Wed = 3, Thu = 4, Fri = 5, Sat = 6 and Sun = 7. If return_type = 3, then Mon = 0, Tue = 1, Wed = 2, Thu = 3, Fri = 4, Sat = 5 and Sun = 6. If return_type = 11, then Mon = 1, Tue = 2, Wed = 3, Thu = 4, Fri = 5, Sat = 6 and Sun = 7.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date as a serial number. ")]
        public static decimal? WEEKDAY(decimal? date, decimal? return_type = null)
        {
            if (!date.HasValue)
                return null;

            DateTime dt = DateTime.FromOADate((double)date.Value);
            return DoWEEKDAY(dt, return_type);
        }

        /// <summary>
        /// The day of the week for a given date.
        /// </summary>
        /// <param name="date">The date</param>
        /// <param name="return_type">(Optional) The number that specifies on what day the week starts</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "WEEKDAY",
             InsertedText = "WEEKDAY(|VALUE|,|TYPE|)",
             DataType = 2,
             Hint = "The day of the week for a given date.",
             Description = "This function returns a number between 0 and 7. The serial_number can be a date value, a serial number or a reference to a cell containing a date. The serial_number cannot be a text string. If return_type is left blank, the 1 is used. If return_type = 1, then Sun = 1, Mon = 2, Tue = 3, Wed = 4, Thu = 5, Fri = 6 and Sat = 7. If return_type = 2, then Mon = 1, Tue = 2, Wed = 3, Thu = 4, Fri = 5, Sat = 6 and Sun = 7. If return_type = 3, then Mon = 0, Tue = 1, Wed = 2, Thu = 3, Fri = 4, Sat = 5 and Sun = 6. If return_type = 11, then Mon = 1, Tue = 2, Wed = 3, Thu = 4, Fri = 5, Sat = 6 and Sun = 7.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date as a serial number. ")]
        public static decimal? WEEKDAY(string date, decimal? return_type = null)
        {
            if (date == null)
                return null;

            if (!TryParseDate(date, out DateTime dt))
                throw new DateConversionException($"Invalid Date format in WEEKDAY('{date}', {return_type}).");

            return DoWEEKDAY(dt, return_type);
        }
        #endregion WEEKDAY

        #region WEEKNUM
        private static decimal? DoWEEKNUM(DateTime date, decimal? return_type)
        {
            int returnType = (int)return_type.GetValueOrDefault(1m);

            Calendar cal = CultureInfo.InvariantCulture.Calendar;
            switch (returnType)
            {
                case 2:
                case 11:
                    return cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday);
                case 12:
                    return cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Tuesday);
                case 13:
                    return cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Wednesday);
                case 14:
                    return cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Thursday);
                case 15:
                    return cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Friday);
                case 16:
                    return cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Saturday);
                case 21:
                    return cal.GetWeekOfYear(date, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                case 1:
                case 17:
                default:
                    return cal.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
            }
        }


        /// <summary>
        /// The week number in the year for a given date.
        /// </summary>
        /// <param name="date">The date</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "WEEKNUM",
             InsertedText = "WEEKNUM(|VALUE|,|TYPE|)",
             DataType = 2,
             Hint = "The week number in the year for a given date.",
             Description = "This function returns a number between 1 and 54. If return_type is left blank, then 1 is used. If return_type = 1, then weekdays are numbered Sunday = 1 to Saturday = 7. If return_type = 2, then weekdays are numbered Monday = 1 to Sunday = 7. This function is based on the American standard which considers the week containing January 1st to be the first week of the year. The European convention however defines the first week as the first one containing four or more days. If return_type = 21, then the weekdays according to the European Standard are returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date within the week. ")]
        public static decimal? WEEKNUM(DateTime? date, decimal? return_type = null)
        {
            if (!date.HasValue)
                return null;

            return DoWEEKNUM(date.Value, return_type);
        }

        /// <summary>
        /// The week number in the year for a given date.
        /// </summary>
        /// <param name="date">The date</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "WEEKNUM",
             InsertedText = "WEEKNUM(|VALUE|,|TYPE|)",
             DataType = 2,
             Hint = "The week number in the year for a given date.",
             Description = "This function returns a number between 1 and 54. If return_type is left blank, then 1 is used. If return_type = 1, then weekdays are numbered Sunday = 1 to Saturday = 7. If return_type = 2, then weekdays are numbered Monday = 1 to Sunday = 7. This function is based on the American standard which considers the week containing January 1st to be the first week of the year. The European convention however defines the first week as the first one containing four or more days. If return_type = 21, then the weekdays according to the European Standard are returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date within the week. ")]
        public static decimal? WEEKNUM(decimal? date, decimal? return_type = null)
        {
            if (!date.HasValue)
                return null;

            DateTime dt = DateTime.FromOADate((double)date.Value);
            return DoWEEKNUM(dt, return_type);
        }

        /// <summary>
        /// The week number in the year for a given date.
        /// </summary>
        /// <param name="date">The date</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "WEEKNUM",
             InsertedText = "WEEKNUM(|VALUE|,|TYPE|)",
             DataType = 2,
             Hint = "The week number in the year for a given date.",
             Description = "This function returns a number between 1 and 54. If return_type is left blank, then 1 is used. If return_type = 1, then weekdays are numbered Sunday = 1 to Saturday = 7. If return_type = 2, then weekdays are numbered Monday = 1 to Sunday = 7. This function is based on the American standard which considers the week containing January 1st to be the first week of the year. The European convention however defines the first week as the first one containing four or more days. If return_type = 21, then the weekdays according to the European Standard are returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date within the week. ")]
        public static decimal? WEEKNUM(string date, decimal? return_type = null)
        {
            if (date == null)
                return null;

            if (!TryParseDate(date, out DateTime dt))
                throw new DateConversionException($"Invalid Date format in WEEKNUM('{date}', {return_type}).");

            return DoWEEKNUM(dt, return_type);
        }
        #endregion WEEKNUM

        /// <summary>
        /// The logical exclusive OR for any number of arguments.
        /// </summary>
        /// <param name="tests">Array containing the tests.</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "XOR",
             InsertedText = "XOR(|VALUES|)",
             DataType = 3,
             Hint = "(2013) The logical exclusive OR for any number of arguments.",
             Description = "This function returns either True or False. This function returns True when the number of True parameters is odd. This function returns False when the number of True parameters is even. The logical values can be constants, logical equations, cell references or named ranges. You can use this function to see if a value occurs in an array. All the arguments must evaluate to logical values (either true or false).",
             MinParams = 2,
             MaxParams = 255,
             Param1Name = "Value1",
             Param1DataType = 3,
             Param1Hint = "The first logical value. ",
             Param2Name = "Value2",
             Param2DataType = 3,
             Param2Hint = "The second logical value. ")]
        public static bool? XOR(bool?[] Conditions)
        {
            if (Conditions == null || Conditions.Any(c => !c.HasValue))
                return false;

            int trueCount = 0;

            for (int i = 0; i < Conditions.Length; i++)
            {
                if (Conditions[i].Value)
                    trueCount++;
            }

            if (trueCount % 2 == 0)
                return false;
            else
                return true;
        }

        #region YEAR
        private static decimal? DoYEAR(DateTime date)
        {
            return date.Year;
        }

        /// <summary>
        /// The year as an integer given a date serial number.
        /// </summary>
        /// <param name="date">The date</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "YEAR",
             InsertedText = "YEAR(|VALUE|)",
             DataType = 2,
             Hint = "The year as an integer given a date serial number.",
             Description = "The serial_number can be a date value, a serial number or a reference to a cell containing a date. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the year you want to find. ")]
        public static decimal? YEAR(DateTime? date)
        {
            if (!date.HasValue)
                return null;

            return DoYEAR(date.Value);
        }

        /// <summary>
        /// The year as an integer given a date serial number.
        /// </summary>
        /// <param name="date">The date</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "YEAR",
             InsertedText = "YEAR(|VALUE|)",
             DataType = 2,
             Hint = "The year as an integer given a date serial number.",
             Description = "The serial_number can be a date value, a serial number or a reference to a cell containing a date. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the year you want to find. ")]
        public static decimal? YEAR(decimal? date)
        {
            if (!date.HasValue)
                return null;

            DateTime dt = new DateTime(1900 + (int)date, 1, 1);
            return DoYEAR(dt);
        }

        /// <summary>
        /// The year as an integer given a date serial number.
        /// </summary>
        /// <param name="date">The date</param>
        /// <returns></returns>
        [CBELFunctionInfo(
             Name = "YEAR",
             InsertedText = "YEAR(|VALUE|)",
             DataType = 2,
             Hint = "The year as an integer given a date serial number.",
             Description = "The serial_number can be a date value, a serial number or a reference to a cell containing a date. If serial_number < 0, then #NUM! is returned. If serial_number is not a valid date, then #VALUE! is returned.",
             MinParams = 1,
             MaxParams = 1,
             Param1Name = "Value1",
             Param1DataType = 2,
             Param1Hint = "The date that contains the year you want to find. ")]
        public static decimal? YEAR(string date)
        {
            if (date == null)
                return null;

            if (!TryParseDate(date, out DateTime dt))
                throw new DateConversionException($"Invalid Date format in YEAR('{date}').");

            return DoYEAR(dt);
        }
        #endregion YEAR

        #endregion

    }
}
