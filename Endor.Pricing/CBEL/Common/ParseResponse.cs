﻿using System.Collections.Generic;

namespace Endor.CBEL.Common
{
    /// <summary>
    /// The Parse Response to be returned
    /// </summary>
    public class ParseResponse
    {
        /// <summary>
        /// Formula Parsed
        /// </summary>
        public string Formula { get; set; }

        /// <summary>
        /// List of parse failures encountered
        /// </summary>
        public List<ParseFailuresResponse> Failures { get; set; }

    }

    /// <summary>
    /// The Parse Failure Response to be returned
    /// </summary>
    public class ParseFailuresResponse
    {
        /// <summary>
        /// Error message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Line of the error
        /// </summary>
        public int Line { get; set; }


        /// <summary>
        /// Position of the error
        /// </summary>
        public int Position { get; set; }
    }
}
