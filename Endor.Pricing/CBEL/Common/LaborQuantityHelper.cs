﻿using Endor.CBEL.Common;
using Endor.Models;
using Endor.Units;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Pricing.CBEL.Common
{
    public static class LaborQuantityHelper
    {
        /// <summary>
        /// determine the appropriate quantity given the interval and minimum restrictions
        /// </summary>
        /// <param name="IncrementTime">when supplied, rounds quantities up to be a multiple of this value</param>
        /// <param name="MinimumTime">when supplied, ensures this value is used as a minimum</param>
        /// <param name="quantityInMinutes">user-supplied or computed quantity to be restricted</param>
        /// <exception cref="DivideByZeroException">if IncrementTime is zero</exception>
        /// <returns></returns>
        public static decimal? ComputeQuantity(Measurement IncrementTime, Measurement MinimumTime, decimal? quantityInMinutes)
        {
            if (!quantityInMinutes.HasValue) return null;

            if ((IncrementTime != null) && (IncrementTime.Value.HasValue))
            {
                if (IncrementTime.InMinutes().Value == 0)
                    throw new DivideByZeroException("Labor Increment supplied cannot be zero");

                quantityInMinutes = ((int)Math.Ceiling(quantityInMinutes.Value / IncrementTime.InMinutes().Value)) * IncrementTime.InMinutes().Value;
            }

            return Math.Max(quantityInMinutes.GetValueOrDefault(), MinimumTime?.InMinutes().GetValueOrDefault() ?? 0);
        }
    }
}
