﻿using System;
using System.Text;
using Endor.Models;
using Endor.Pricing;
using Irony.Parsing;

namespace Endor.CBEL.Common
{
    public static class CBELExtensions
    {
        public static string ToValueType(this DataType self)
        {
            switch (self)
            {
                case DataType.Number:
                    return "decimal?";
                case DataType.Boolean:
                    return "bool?";
                case DataType.String:
                case DataType.MaterialPart:
                case DataType.LaborPart:
                case DataType.MachinePart:
                case DataType.Assembly:
                    return "string";
                default:
                    throw new ArgumentException($"DataType {self.ToString()} does not have a valid object type mapping");
            }
        }

        public static OrderItemComponentType ToComponentType(this int? self)
        {
            switch (self)
            {
                case (int)ClassType.Material:
                    return OrderItemComponentType.Material;
                case (int)ClassType.Labor:
                    return OrderItemComponentType.Labor;
                case (int)ClassType.Machine:
                    return OrderItemComponentType.Machine;
                case (int)ClassType.Assembly:
                    return OrderItemComponentType.Assembly;
                case (int)ClassType.MachineTime:
                    return OrderItemComponentType.MachineTime;
            }

            throw new ArgumentException("This componentType cannot be converted yet.");
        }

        public static int ToClassTypeID(this OrderItemComponentType self)
        {
            switch (self)
            {
                case OrderItemComponentType.Assembly:
                    return ClassType.Assembly.ID();
                case OrderItemComponentType.Labor:
                    return ClassType.Labor.ID();
                case OrderItemComponentType.Machine:
                    return ClassType.Machine.ID();
                case OrderItemComponentType.Material:
                    return ClassType.Material.ID();
                case OrderItemComponentType.MachineTime:
                    return ClassType.MachineTime.ID();
            }

            throw new ArgumentException("This componentType does not have a matching ClassTypeID");
        }

        public static string ToStringLiteral(this string input, bool wrapInQuotes = true)
        {
            if (input == null)
                return wrapInQuotes ? "\"\"" : "";

            StringBuilder literal = new StringBuilder(input.Length + 2);
            if (wrapInQuotes)
                literal.Append("\"");

            foreach (var c in input)
            {
                switch (c)
                {
                    case '\'': literal.Append(@"\'"); break;
                    case '\"': literal.Append("\\\""); break;
                    case '\\': literal.Append(@"\\"); break;
                    case '\0': literal.Append(@"\0"); break;
                    case '\a': literal.Append(@"\a"); break;
                    case '\b': literal.Append(@"\b"); break;
                    case '\f': literal.Append(@"\f"); break;
                    case '\n': literal.Append(@"\n"); break;
                    case '\r': literal.Append(@"\r"); break;
                    case '\t': literal.Append(@"\t"); break;
                    case '\v': literal.Append(@"\v"); break;
                    default:
                        // ASCII printable character
                        if (c >= 0x20 && c <= 0x7e)
                        {
                            literal.Append(c);
                            // As UTF16 escaped character
                        }
                        else
                        {
                            literal.Append(@"\u");
                            literal.Append(((int)c).ToString("x4"));
                        }
                        break;
                }
            }

            if (wrapInQuotes)
                literal.Append("\"");

            return literal.ToString();
        }

        public static decimal? Round(this decimal? self, int decimalPlacesToRoundTo)
        {
            if (!self.HasValue)
                return self;

            return (decimal?)Decimal.Round(self.Value, decimalPlacesToRoundTo);
        }

        public static bool IsVariable(this Token self)
        {
            return self?.Terminal?.Name?.StartsWith("Variable<") ?? false;
        }
        public static bool IsObjectIdentifier(this Token self)
        {
            return self?.Terminal?.Name?.Equals("ObjectIdentifier") ?? false;
        }
    }
}
