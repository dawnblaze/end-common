﻿using Endor.CBEL.Interfaces;

using System.Collections.Generic;
using System.Linq;

namespace Endor.CBEL.Common
{
    public class VariableDictionary : Dictionary<string, ICBELVariable> 
    {
        private class VariableComparer : IComparer<ICBELVariable>
        {
            public int Compare(ICBELVariable x, ICBELVariable y)
            {
                if (x.Name.ToLower() == "profile")
                    return -1;

                if (y.Name.ToLower() == "profile")
                    return 1;

                if (x.Name.ToLower() == "instance")
                    return -1;

                if (y.Name.ToLower() == "instance")
                    return 1;

                if (x.Name.ToLower() == "totalquantity")
                    return -1;

                if (y.Name.ToLower() == "totalquantity")
                    return 1;

                if (x.Name.ToLower() == "price")
                    return 1;

                if (y.Name.ToLower() == "price")
                    return -1;

                return string.Compare(x.Name, y.Name, true);
            }
        }
        public IOrderedEnumerable<ICBELVariable> OrderedList()
        {
            return this.Values.OrderBy(v => v, new VariableComparer());
        }
    }

    public class VariableLinkDictionary : Dictionary<string, (string VariableName, ICBELVariable Variable)>
    {
    }
}
