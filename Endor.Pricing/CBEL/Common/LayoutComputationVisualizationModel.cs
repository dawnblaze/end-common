﻿using Endor.CBEL.Elements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.CBEL.Common
{
    public class LayoutComputationVisualizationModel
    {
        [JsonConstructor]
        public LayoutComputationVisualizationModel()
        {

        }

        public LayoutComputationVisualizationModel(LayoutComputingAssembly layoutComputingAssembly)
        {
            if (layoutComputingAssembly == null)
                throw new Exception("Layout Computation Assembly must be defined");

            DefaultOptions = new LayoutComputationDefaultOptions()
            {
                HideBleed = layoutComputingAssembly.HideBleed.Value.GetValueOrDefault(false),
                HideColorbar = layoutComputingAssembly.HideColorBar.Value.GetValueOrDefault(false),
                HideCutlines = layoutComputingAssembly.HideCutLines.Value.GetValueOrDefault(false),
                HideFoldlines = layoutComputingAssembly.HideFoldLines.Value.GetValueOrDefault(false),
                HideGutter = layoutComputingAssembly.HideGutter.Value.GetValueOrDefault(false),
                HideLeader = layoutComputingAssembly.HideLeader.Value.GetValueOrDefault(false),
                HideMargin = layoutComputingAssembly.HideMargin.Value.GetValueOrDefault(false),
                HideMaterialDimensions = layoutComputingAssembly.HideMaterialDimensions.Value.GetValueOrDefault(false),
                HideRegistration = layoutComputingAssembly.HideRegistrationMarks.Value.GetValueOrDefault(false),
                HideWatermark = layoutComputingAssembly.HideWatermark.Value.GetValueOrDefault(false),
            };

            Machine = new LayoutComputationMachine(layoutComputingAssembly);
            Material = new LayoutComputationMaterial(layoutComputingAssembly);
            ItemCount = layoutComputingAssembly.TotalItemCount.GetValueOrDefault(0);
            Sides = layoutComputingAssembly.Is2Sided.Value.GetValueOrDefault(false) ? 2 : 1;

            Layouts = GenerateLayouts(layoutComputingAssembly);
            LayoutCount = Layouts.Count;
            LayoutType = layoutComputingAssembly.LayoutType.Value;

            Pages = GeneratePages(layoutComputingAssembly);
            PageCount = Pages.Count;

        }

        public LayoutComputationDefaultOptions DefaultOptions;
        public string LayoutType;
        // match the types based on the Wiki for itemcount and page count
        public decimal ItemCount;
        public decimal PageCount;
        public int LayoutCount;
        public int Sides;
        public LayoutComputationMaterial Material;
        public LayoutComputationMachine Machine;
        public List<LayoutComputationPage> Pages;
        public List<LayoutComputationLayout> Layouts;

        public static string ColorBar = "ColorBar";

        private List<LayoutComputationPage> GeneratePages(LayoutComputingAssembly layoutComputingAssembly)
        {
            var pageList = new List<LayoutComputationPage>();
            int pageCount = 1;
            bool is2Sided = layoutComputingAssembly.Is2Sided.Value.GetValueOrDefault(false);
            int nextPageFirstItemNumber = 1;

            for (int i = 0; i < layoutComputingAssembly.MaterialRunCount.Value; i++)
            {
                bool NotOnLastPage = (i < (layoutComputingAssembly.MaterialRunCount.Value - 1));

                var lastAndFirstDiffer = layoutComputingAssembly.ItemsOnLastPage != layoutComputingAssembly.ItemsOnFirstPage;
                var layoutBaseID = !is2Sided ? (NotOnLastPage ? 0 : (lastAndFirstDiffer ? 1 : 0)) :
                    (NotOnLastPage ? 0 : (lastAndFirstDiffer ? 2 : 0));

                var page = new LayoutComputationPage()
                {
                    PageNumber = pageCount++,
                    Side = 1,
                    FirstItemNumber = nextPageFirstItemNumber,
                    LayoutID = layoutBaseID
                };

                pageList.Add(page);

                if (is2Sided)
                {
                    var backSidedPage = new LayoutComputationPage()
                    {
                        PageNumber = pageCount++,
                        Side = 2,
                        FirstItemNumber = nextPageFirstItemNumber,
                        LayoutID = layoutBaseID + 1
                    };

                    pageList.Add(backSidedPage);
                }

                nextPageFirstItemNumber += this.Layouts[page.LayoutID].Items.Count;
            }
            return pageList;

        }

        private List<LayoutComputationLayout> GenerateLayouts(LayoutComputingAssembly layoutComputingAssembly)
        {
            var ItemCount = (int)layoutComputingAssembly.ItemsOnFirstPage;

            var layoutList = new List<LayoutComputationLayout>();
            int layoutID = 0;
            var baseLayout = GetBaseLayout(layoutComputingAssembly, layoutID++, ItemCount);

            layoutList.Add(baseLayout);

            LayoutComputationLayout baseSecondSideLayout = null;
            if (layoutComputingAssembly.Is2Sided.Value.GetValueOrDefault() && layoutComputingAssembly.TotalItemCount > ItemCount)
            {
                baseSecondSideLayout = baseLayout.Clone() as LayoutComputationLayout;
                baseSecondSideLayout.ID = layoutID++;
                //append f to label for front side items and b to label for back side items
                //flip the x position for the items on back
                for (int x = 0; x < ItemCount; x++)
                {
                    baseLayout.Items[x].Label = baseLayout.Items[x].Label + 'F';
                    baseSecondSideLayout.Items[x].Label = baseSecondSideLayout.Items[x].Label + 'B';
                    baseSecondSideLayout.Items[x].Left = layoutComputingAssembly.MaterialWidth.Value.GetValueOrDefault(0) - baseSecondSideLayout.Items[x].Left - layoutComputingAssembly.ItemWidth.Value.GetValueOrDefault();
                }

                layoutList.Add(baseSecondSideLayout);
            }

            if (layoutComputingAssembly.ItemsOnLastPage != layoutComputingAssembly.ItemsOnFirstPage)
            {
                //Need to add last pages
                ItemCount = (int)layoutComputingAssembly.ItemsOnLastPage.GetValueOrDefault(0);
                var lastBaseLayout = GetBaseLayout(layoutComputingAssembly, layoutID++, ItemCount);
                layoutList.Add(lastBaseLayout);
                if (layoutComputingAssembly.Is2Sided.Value.GetValueOrDefault())
                {
                    var baseLastSecondSideLayout = lastBaseLayout.Clone() as LayoutComputationLayout;
                    baseLastSecondSideLayout.ID = layoutID++;
                    //append f to label for front side items and b to label for back side items
                    //flip the x position for the items on back
                    for (int x = 0; x < ItemCount; x++)
                    {
                        lastBaseLayout.Items[x].Label = lastBaseLayout.Items[x].Label + 'F';
                        baseLastSecondSideLayout.Items[x].Label = baseLastSecondSideLayout.Items[x].Label + 'B';
                        baseLastSecondSideLayout.Items[x].Left = layoutComputingAssembly.MaterialWidth.Value.GetValueOrDefault(0) - baseLastSecondSideLayout.Items[x].Left - layoutComputingAssembly.ItemWidth.Value.GetValueOrDefault();
                    }

                    layoutList.Add(baseLastSecondSideLayout);
                }
            }

            return layoutList;
        }


        private LayoutComputationLayout GetBaseLayout(LayoutComputingAssembly layoutComputingAssembly, int layoutID, int ItemCount)
        {
            var printerMarks = new List<LayoutComptationLayoutPrinterMark>();
            if (layoutComputingAssembly.IncludeColorBar.Value.GetValueOrDefault(false))
            {
                printerMarks.Add(new LayoutComptationLayoutPrinterMark(ColorBar, layoutComputingAssembly));
            }
            var baseLayout = new LayoutComputationLayout()
            {
                ID = layoutID,
                ItemCount = ItemCount,
                LayoutSize = new LayoutComputationSize()
                {
                    Horizontal = layoutComputingAssembly.MaterialWidth.Value,
                    Vertical = layoutComputingAssembly.MaterialHeight.Value
                },
                ItemSize = new LayoutComputationSize()
                {
                    Horizontal = layoutComputingAssembly.ItemWidth.Value,
                    Vertical = layoutComputingAssembly.ItemHeight.Value
                },
                Bleed = new LayoutComputationBorder()
                {
                    Top = layoutComputingAssembly.BleedTop.Value,
                    Bottom = layoutComputingAssembly.BleedBottom.Value,
                    Left = layoutComputingAssembly.BleedLeft.Value,
                    Right = layoutComputingAssembly.BleedRight.Value,
                },
                Gutter = new LayoutComputationSize()
                {
                    Horizontal = layoutComputingAssembly.GutterHorizontal.Value,
                    Vertical = layoutComputingAssembly.GutterVertical.Value,
                },
                Items = GetItemsForMaterial(layoutComputingAssembly, ItemCount),
                PrinterMarks = printerMarks,
                Annotations = new List<LayoutComptationLayoutAnnotation>()

            };
            return baseLayout;
        }
        private List<LayoutComputationLayoutItem> GetItemsForMaterial(LayoutComputingAssembly layoutComputingAssembly, int ItemCount)
        {
            var itemList = new List<LayoutComputationLayoutItem>();
            for (int yItem = 0; yItem < layoutComputingAssembly.ItemsVertical.Value; yItem++)
            {
                for (int xItem = 0; xItem < layoutComputingAssembly.ItemsHorizontal.Value; xItem++)
                {
                    var colorbarThickness = layoutComputingAssembly.IncludeColorBar.Value.GetValueOrDefault(false) ? layoutComputingAssembly.ColorBarThickness.Value.GetValueOrDefault(0) : 0;
                    var colorbarPos = layoutComputingAssembly.ColorBarPosition;
                    var index = (int)(layoutComputingAssembly.ItemsHorizontal.Value * yItem) + xItem;
                    var left = layoutComputingAssembly.LeftItemOffset + (layoutComputingAssembly.EffectiveItemWidth * xItem);
                    var top = layoutComputingAssembly.TopItemOffset + (layoutComputingAssembly.EffectiveItemHeight * yItem);

                    if (colorbarPos.Value == "Left")
                    {
                        left += colorbarThickness;
                    }
                    if (colorbarPos.Value == "Top")
                    {
                        top += colorbarThickness;
                    }

                    var item = new LayoutComputationLayoutItem()
                    {
                        ItemIndex = index,
                        Label = index >= ItemCount ? "X" : "{ItemNumber}",
                        Left = (left ?? 0m),
                        Top = (top ?? 0m),
                        LabelRotation = 0,
                        Placeholder = index >= ItemCount ? true : false,
                        URL = ""
                    };
                    itemList.Add(item);
                }
            }
            return itemList;
        }
    }

    public class LayoutComputationDefaultOptions
    {
        public bool HideMaterialDimensions;
        public bool HideMargin;
        public bool HideBleed;
        public bool HideLeader;
        public bool HideGutter;
        public bool HideColorbar;
        public bool HideCutlines;
        public bool HideFoldlines;
        public bool HideWatermark;
        public bool HideRegistration;
    }

    public class LayoutComputationSize : ICloneable
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Horizontal;

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Vertical;

        public object Clone()
        {
            return new LayoutComputationSize()
            {
                Horizontal = this.Horizontal,
                Vertical = this.Vertical
            };
        }
    }

    public class LayoutComputationBorder : ICloneable
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Top;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Bottom;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Left;
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Right;
        public object Clone()
        {
            return new LayoutComputationBorder()
            {
                Top = this.Top,
                Bottom = this.Bottom,
                Left = this.Left,
                Right = this.Right,
            };
        }
    }

    public class LayoutComputationMaterial
    {
        public string Name;
        public LayoutComputationSize Size;
        public string Units;

        [JsonConstructor]
        public LayoutComputationMaterial()
        {

        }

        public LayoutComputationMaterial(LayoutComputingAssembly layoutComputingAssembly)
        {
            Name = layoutComputingAssembly.Material.Value;
            Size = new LayoutComputationSize()
            {
                Horizontal = layoutComputingAssembly.MaterialWidth.Value,
                Vertical = layoutComputingAssembly.MaterialHeight.Value
            };
            Units = "\"";
        }
    }

    public class LayoutComputationMachine
    {
        public string Name;
        public LayoutComputationBorder MaterialMargin;
        public LayoutComputationBorder Leader;
        public LayoutComputationSize MaxSize;
        public string Units;

        [JsonConstructor]
        public LayoutComputationMachine()
        {

        }

        public LayoutComputationMachine(LayoutComputingAssembly layoutComputingAssembly)
        {
            Name = layoutComputingAssembly.Machine.Value;
            MaterialMargin = new LayoutComputationBorder()
            {
                Top = layoutComputingAssembly.MaterialMarginTop.Value,
                Bottom = layoutComputingAssembly.MaterialMarginBottom.Value,
                Left = layoutComputingAssembly.MaterialMarginLeft.Value,
                Right = layoutComputingAssembly.MaterialMarginRight.Value
            };
            Leader = new LayoutComputationBorder()
            {
                Top = layoutComputingAssembly.LeaderTop.Value,
                Bottom = layoutComputingAssembly.LeaderBottom.Value,
                Left = layoutComputingAssembly.LeaderLeft.Value,
                Right = layoutComputingAssembly.LeaderRight.Value,
            };
            MaxSize = new LayoutComputationSize()
            {
                Horizontal = layoutComputingAssembly.MachineMaxHorizontal.Value,
                Vertical = layoutComputingAssembly.MachineMaxVertical.Value
            };
            Units = "\"";
        }
    }

    public class LayoutComputationPage
    {
        public int PageNumber;
        public int Side;
        public int FirstItemNumber;
        public int LayoutID;
    }

    public class LayoutComputationLayout : ICloneable
    {
        public int ID;
        public int ItemCount;
        public LayoutComputationSize LayoutSize;
        public LayoutComputationSize ItemSize;
        public LayoutComputationBorder Bleed;
        public LayoutComputationSize Gutter;
        public List<LayoutComputationLayoutItem> Items;
        public List<LayoutComptationLayoutPrinterMark> PrinterMarks;
        public List<LayoutComptationLayoutAnnotation> Annotations;

        public object Clone()
        {
            return new LayoutComputationLayout()
            {
                ID = this.ID,
                ItemCount = this.ItemCount,
                LayoutSize = this.LayoutSize.Clone() as LayoutComputationSize,
                ItemSize = this.ItemSize.Clone() as LayoutComputationSize,
                Bleed = this.Bleed.Clone() as LayoutComputationBorder,
                Gutter = this.Gutter.Clone() as LayoutComputationSize,
                Items = this.Items.Select(item => (LayoutComputationLayoutItem)item.Clone()).ToList(),
                PrinterMarks = this.PrinterMarks.Select(item => (LayoutComptationLayoutPrinterMark)item.Clone()).ToList(),
                Annotations = this.Annotations.Select(item => (LayoutComptationLayoutAnnotation)item.Clone()).ToList(),
            };

        }
    }

    public class LayoutComputationLayoutItem : ICloneable
    {
        public int ItemIndex;
        public string Label;
        public decimal Left;
        public decimal Top;
        public decimal LabelRotation;
        public bool Placeholder;
        public string URL;

        public object Clone()
        {
            return new LayoutComputationLayoutItem()
            {
                ItemIndex = this.ItemIndex,
                Label = this.Label,
                Left = this.Left,
                Top = this.Top,
                LabelRotation = this.LabelRotation,
                Placeholder = this.Placeholder,
                URL = this.URL
            };
        }
    }

    public class LayoutComptationLayoutPrinterMark : ICloneable
    {
        public decimal Rotation;
        public decimal Width;
        public decimal Height;
        public decimal Length;
        public decimal Thickness;
        public decimal Top;
        public decimal Left;
        public string URL;

        [JsonConstructor]
        public LayoutComptationLayoutPrinterMark()
        {

        }

        public LayoutComptationLayoutPrinterMark(string Type, LayoutComputingAssembly layoutComptationAssembly)
        {
            if (Type == LayoutComputationVisualizationModel.ColorBar)
            {
                this.Length = layoutComptationAssembly.ColorBarLength.Value.GetValueOrDefault(0);
                this.Thickness = layoutComptationAssembly.ColorBarThickness.Value.GetValueOrDefault(0);
                URL = "";
                switch (layoutComptationAssembly.ColorBarPosition.Value)
                {
                    case "Top":
                        Rotation = 0;
                        Height = this.Thickness;
                        Width = this.Length;
                        Top = LayoutComputingAssembly.Max(layoutComptationAssembly.MaterialMarginTop.Value, layoutComptationAssembly.LeaderTop.Value).GetValueOrDefault(0);
                        Left = LayoutComputingAssembly.Max(layoutComptationAssembly.MaterialMarginLeft.Value, layoutComptationAssembly.LeaderLeft.Value).GetValueOrDefault(0);
                        break;
                    case "Bottom":
                        Rotation = 180;
                        Height = this.Thickness;
                        Width = this.Length;
                        Top = layoutComptationAssembly.MaterialHeight.Value.GetValueOrDefault(0) - LayoutComputingAssembly.Max(layoutComptationAssembly.MaterialMarginBottom.Value, layoutComptationAssembly.LeaderBottom.Value).GetValueOrDefault(0);
                        Left = layoutComptationAssembly.MaterialWidth.Value.GetValueOrDefault(0) - LayoutComputingAssembly.Max(layoutComptationAssembly.MaterialMarginBottom.Value, layoutComptationAssembly.LeaderBottom.Value).GetValueOrDefault(0);
                        break;
                    case "Left":
                        Rotation = 90;
                        Height = this.Length;
                        Width = this.Thickness;
                        Top = (layoutComptationAssembly.MaterialHeight.Value.GetValueOrDefault(0) - layoutComptationAssembly.ColorBarLength.Value.GetValueOrDefault(0)) / 2m;
                        Left = (layoutComptationAssembly.MaterialWidth.Value.GetValueOrDefault(0) - Width) / 2m;
                        break;
                    case "Right":
                        Rotation = 270;
                        Height = this.Length;
                        Width = this.Thickness;
                        Top = (layoutComptationAssembly.MaterialHeight.Value.GetValueOrDefault(0) - layoutComptationAssembly.ColorBarLength.Value.GetValueOrDefault(0)) / 2m;
                        Left = (layoutComptationAssembly.MaterialWidth.Value.GetValueOrDefault(0) - Width) / 2m;
                        break;

                    default: throw new Exception("Could not properly parse position of " + layoutComptationAssembly.ColorBarPosition + " in LayoutComptationLayoutPrinterMark.");
                }

            }
        }

        public object Clone()
        {
            return new LayoutComptationLayoutPrinterMark()
            {
                Rotation = this.Rotation,
                Width = this.Width,
                Height = this.Height,
                Length = this.Length,
                Thickness = this.Thickness,
                Top = this.Top,
                Left = this.Left,
                URL = this.URL
            };
        }
    }

    public class LayoutComptationLayoutAnnotation : ICloneable
    {
        public decimal Rotation;
        public decimal Width;
        public decimal Height;
        public decimal Length;
        public decimal Thickness;
        public decimal Left;
        public decimal Top;
        public string URL;

        public object Clone()
        {
            return new LayoutComptationLayoutAnnotation()
            {
                Rotation = this.Rotation,
                Width = this.Width,
                Height = this.Height,
                Length = this.Length,
                Thickness = this.Thickness,
                Top = this.Top,
                Left = this.Left,
                URL = this.URL
            };
        }
    }
}
