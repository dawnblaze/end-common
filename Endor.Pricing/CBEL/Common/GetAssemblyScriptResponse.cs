﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;

namespace Endor.CBEL.Common
{
    public class GetAssemblyScriptResponse
    {
        public bool Success { get; set; }
        public List<PricingErrorMessage> Errors { get; set; }
        public List<string> ExternalDependencies { get; set; }
    }
}
