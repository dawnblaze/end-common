﻿namespace Endor.CBEL.Common
{
    internal class ParseFxResult
    {
        public bool IsConstant { get; set; }
        public string ValueConstant { get; set; }
        public string FormulaText { get; set; }
        public string FxFunction { get; set; }
    }
}
