﻿using Endor.Models;
using Endor.Units;
using System.Collections.Generic;

namespace Endor.CBEL.Common
{
    public class VariableOVData
    {
        public string VariableName { get; set; }
        public bool IsOV { get; set; }
        public string Value { get; set; }
        public int? ComponentID { get; set; }
        public decimal? Quantity { get; set; }
        public bool? QuantityOV { get; set; }
        public bool? IsIncluded { get; set; }
        public bool? IsIncludedOV { get; set; }
        public int? CompanyID { get; set; }
        public decimal? ParentQuantity { get; set; }
        public bool IsVended { get; set; }
        public decimal? LineItemQuantity { get; set; }
        public decimal? CostUnit { get; set; }
        public bool? CostUnitOV { get; set; }
        public Unit Unit { get; set; }

        public List<VariableOVData> ChildOVValues { get; set; }
    }
}
