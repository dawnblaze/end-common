﻿using System.Collections.Generic;
using System.Linq;

namespace Endor.CBEL.Common
{
    public static class CBELFunctions
    {
        private static string ConvertToString(decimal? value)
        {
            if (value.HasValue)
                return value.Value.ToString();

            return null;
        }
        private static string ConvertToString(bool? value)
        {
            if (value.HasValue)
                return value.Value ? "True" : "False";

            return null;
        }
        public static string ToString(string value)
        {
            return value;
        }
        public static string ToString(object value)
        {
            if (value == null)
                return null;

            if (value is string)
                return (string)value;

            if (value is bool?)
                return ConvertToString((bool?)value);

            if (value is decimal?)
                return ConvertToString((decimal?)value);

            return null;
        }

        private static decimal? ConvertToDecimal(string value)
        {
            if (value != null && decimal.TryParse(value, out decimal result))
                return result;

            return null;
        }
        private static decimal? ConvertToDecimal(bool? value)
        {
            if (value.HasValue)
                return value.Value ? 1 : 0;

            return null;
        }
        public static decimal? ToDecimal(object value)
        {
            if (value == null)
                return null;

            if (value is decimal?)
                return (decimal?)value;

            if (value is string)
                return ConvertToDecimal((string)value);

            if (value is bool?)
                return ConvertToDecimal((bool?)value);

            return null;
        }
        public static decimal? ToDecimal(decimal? value)
        {
            return value;
        }

        private static bool? ConvertToBool(string value)
        {
            if (CBELAssemblyHelper.TryStringToBool(value, out bool result))
                return result;

            return null;
        }
        private static bool? ConvertToBool(decimal? value)
        {
            if (value.HasValue)
                return (value.Value != 0);

            return null;
        }
        public static bool? ToBool(object value)
        {
            if (value == null)
                return null;

            if (value is bool?)
                return (bool?)value;

            if (value is string)
                return ConvertToBool((string)value);

            if (value is decimal?)
                return ConvertToBool((decimal?)value);

            return null;
        }
        public static bool? ToBool(bool? value)
        {
            return value;
        }
    }
}
