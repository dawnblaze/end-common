﻿using Endor.CBEL.ObjectGeneration;
using Microsoft.CodeAnalysis;
using System.Collections.Generic;
using System.Linq;

namespace Endor.CBEL.Common
{   

    public class PricingErrorMessage
    {
        public string Message { get; set; }
        public string ReferenceID { get; set; }
        public string VariableName { get; set; }
        public string FormulaText { get; set; }
        public bool IsValidation { get; set; }
        public string ErrorType { get; set; }

        public static List<PricingErrorMessage> FromDiagnostics(ICollection<Diagnostic> compileResults, List<CBELVariableGenerator> variables)
        {
            List<PricingErrorMessage> result = new List<PricingErrorMessage>();
            if (compileResults == null)
                return result;

            foreach (Diagnostic err in compileResults)
            {
                string variableName = null;
                string fxText = null;
                CBELVariableGenerator variable = null;

                var sourceText = err.Location?.SourceTree?.GetText();

                if (sourceText != null)
                {
                    var lineSpan = err.Location.GetLineSpan();

                    string sourceLine = sourceText.Lines[lineSpan.StartLinePosition.Line].ToString();
                    int startPos = sourceLine.IndexOf("_");
                    if (startPos >= 0)
                    {
                        int endPos = sourceLine.IndexOf(".", startPos);

                        if (endPos >= 0)
                        {
                            variableName = sourceLine.Substring(startPos + 1, endPos - startPos - 1);
                            variable = variables.FirstOrDefault(t => t.Name == variableName);
                        }
                    }

                    if (variable != null)
                        fxText = variable.FormulaText;
                    else
                        fxText = sourceLine;
                }

                result.Add(
                    new PricingErrorMessage()
                    {
                        ErrorType = err.Severity.ToString(),
                        IsValidation = false,
                        ReferenceID = err.Id,
                        Message = err.GetMessage(),
                        VariableName = variableName,
                        FormulaText = fxText,
                    }
                    );
            }

            return result;
        }
    }
}
