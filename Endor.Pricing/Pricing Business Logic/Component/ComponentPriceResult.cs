﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Units;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Endor.Pricing
{
    public class ComponentPriceResult : IComponentCompilationResponse, IItemComponent<ComponentPriceResult>
    {
        /// <summary>
        /// ID of the OrderItemComponent if known.  Used for remapping results to the client.
        /// Only returned for top-level Components.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int ID { get; set; }

        /// <summary>
        /// TempID of the OrderItemComponent.  Used for remapping results to the client.
        /// Only returned for top-level Components.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Guid? TempID { get; set; }

        /// <summary>
        /// ClassTypeID of the Component (Material|Labor|Machine|Assembly)
        /// </summary>
        public OrderItemComponentType ComponentType { get; set; }

        /// <summary>
        /// The ID of the Component
        /// </summary>
        public int? ComponentID { get; set; }

        /// <summary>
        /// Includes the Component name.  
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Contains a list of variable values, indicating the value and which ones are Overridden.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, VariableValue> Variables { get; set; }

        /// <summary>
        /// The Unit of measure for the Quantity
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Unit? QuantityUnit { get; set; }

        /// <summary>
        /// The Quantity of the Component.  Null is treated as 0 for computation purposes.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TotalQuantity { get; set; }

        /// <summary>
        /// Flag indicating if the QuantityUnit was overridden.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool TotalQuantityOV { get; set; }

        /// <summary>
        /// The Assembly Quantity of the Component.  Null is treated as 0 for computation purposes.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AssemblyQuantity { get; set; }

        /// <summary>
        /// Flag indicating if the Assembly QuantityUnit was overridden.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool AssemblyQuantityOV { get; set; }

        /// <summary>
        /// Is the component included
        /// </summary>
        public bool? IsIncluded { get; set; }
        /// <summary>
        /// Is IsIncluded overridden
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsIncludedOV { get; set; }

        /// <summary>
        /// The total price of the component.  This includes the price of all child components.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceUnit { get; set; }

        /// <summary>
        /// Boolean Flag indicating if this component or any child component prices or quantities were overridden.
        /// Overriding variable values in Assemblies or Machines does not count as overridden for purposes here.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool PriceUnitOV { get; set; } //was PricePreTaxOV

        /// <summary>
        /// The Unit Cost. Null is treated as 0 for computation purposes.
        /// </summary>
        public decimal? CostUnit { get; set; }

        /// <summary>
        /// Flag indicating if the Unit Cost was overridden.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool CostOV { get; set; } // Was CostUnitOV

        /// <summary>
        /// Discounts applied from line item
        /// </summary>
        public decimal PriceAppliedDiscount { get; set; }

        /// <summary>
        /// Taxable Price
        /// </summary>
        public decimal PriceTaxable => PricePreTax.GetValueOrDefault(0m) - PriceAppliedDiscount;

        /// <summary>
        /// A list of child component Price Results.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<ComponentPriceResult> ChildComponents { get; set; }

        /// <summary>
        /// The amount of sales tax for the line item.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTax { get; set; } // Was TaxAmount

        /// <summary>
        /// The total price for the order, computed as PricePreTax + TaxAmount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTotal { get; set; }

        /// <summary>
        /// The cost of this part (if it is a Material Part) plus the cost of all child material components.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMaterial { get; set; }

        /// <summary>
        /// The cost of this part (if it is a Labor Part) plus the cost of all child labor components.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostLabor { get; set; }

        /// <summary>
        /// The cost of this part (if it is a Machine Part) plus the cost of all child Machine components.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMachine { get; set; }

        /// <summary>
        /// The cost of this component plus all of its child components.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostNet { get; set; }

        /// <summary>
        /// A Dictionary of (NexusID, Nexus) that apply (or might apply) to the line item.
        /// 
        /// This is set automatically by the Order when the order is computed,
        /// but must be passed in by the caller when a line item is computed by itself.
        /// 
        /// Required for tax computations when the order is not tax exempt.
        /// </summary>
        public Dictionary<string, TaxAssessmentNexus> TaxNexusList { get; set; }

        /// <summary>
        /// A (compressed) collection of all of the Tax Assessments applied to line items and destinations,
        /// totaled by Tax Assessment Item.
        /// </summary>
        public ICollection<TaxAssessmentResult> TaxInfoList { get; set; }

        /// <summary>
        /// The ID of the Company
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }
        public string VariableName { get; set; }

        public int? IncomeAccountID { get; set; }

        public int? ExpenseAccountID { get; set; }

        /// <summary>
        /// The IncomeAllocationType for this object.
        /// </summary>
        public AssemblyIncomeAllocationType? IncomeAllocationType { get; set; }

        /// <summary>
        /// Determines Whether a subcomponent's price/cost is rolled up to the parent component
        /// </summary>
        public bool? RollupLinkedPriceAndCost { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<PricingErrorMessage> Errors { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<string> ExternalDependencies { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int ErrorCount => Errors?.Count ?? 0;

        public bool Success => ErrorCount == 0;

        /// <summary>
        /// this is actually a setter for <see cref="AssemblyQuantity"/> which requires LineItemQuantity to be computed
        /// </summary>
        internal decimal? LineItemQuantity {
            set
            {
                if (value.HasValue && value.Value != 0 && TotalQuantity.HasValue)
                    if(TotalQuantityOV)
                    {
                        AssemblyQuantity = TotalQuantity / value.Value;
                    }
                    else if(AssemblyQuantityOV)
                    {
                        TotalQuantity = AssemblyQuantity * value.Value;
                    }
            }
        }

        public bool ShouldSerializeErrorCount()
        {
            return !Success;
        }

        /// <summary>
        /// fill in pass-through properties that must be persisted from request to result
        /// </summary>
        /// <param name="request"></param>
        public void AssignPassthroughRequestValues(ComponentPriceRequest request)
        {
            if (request != null)
            {
                ExpenseAccountID = request.ExpenseAccountID;
                IncomeAccountID = request.IncomeAccountID;
                IncomeAllocationType = request.IncomeAllocationType;
                RollupLinkedPriceAndCost = request.RollupLinkedPriceAndCost;
                TotalQuantityOV = request.TotalQuantityOV.GetValueOrDefault();
                AssemblyQuantityOV = request.AssemblyQuantityOV.GetValueOrDefault();
                QuantityUnit = request.QuantityUnit;
            }
        }
    }
}