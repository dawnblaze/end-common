﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Units;
using System.Linq;

namespace Endor.Pricing
{
    public static class ComponentPriceEngine_Machine
    {
        public static void Compute(ComponentPriceRequest request, PricingEngineType engineType, ApiContext ctx, RemoteLogger logger, short bid)
        {
            ComponentPriceResult result = request.Result;

            MachineData machine = ctx.MachineData.FirstOrDefault(c => c.BID == bid && c.ID == request.ComponentID);

            if (machine == null)
            {
                result.PricePreTax = null;
                result.CostLabor = null;
            }
            else
            {
                result.Name = machine.Name;
                result.TotalQuantity = request.TotalQuantity;
                result.QuantityUnit = request.QuantityUnit ?? Unit.Hour;
                result.LineItemQuantity = request.LineItemQuantity;


                if (request.ComponentType != OrderItemComponentType.Machine)
                {
                    if (request.PriceUnitOV.GetValueOrDefault(false))
                        result.PricePreTax = request.PriceUnit.GetValueOrDefault(0m) * result.TotalQuantity.GetValueOrDefault(0m);
                    else
                        result.PricePreTax = 0m;

                    if (request.CostUnitOV.GetValueOrDefault(false))
                        result.CostMachine = request.CostUnit.GetValueOrDefault(0m) * result.TotalQuantity.GetValueOrDefault(0m);
                    else
                        result.CostMachine = 0m;

                    if (result.TotalQuantity.GetValueOrDefault(0) != 0m)
                        result.PriceUnit = result.PricePreTax / result.TotalQuantity;
                }
                else
                {
                    foreach (var component in request.ChildComponents)
                    {
                        result.PricePreTax = result.PricePreTax.GetValueOrDefault(0)
                            + (component.PriceUnit.GetValueOrDefault(0m) * component.TotalQuantity.GetValueOrDefault(0m));
                        
                        result.CostMachine = result.CostMachine.GetValueOrDefault(0)
                            + component.CostUnit.GetValueOrDefault(0m) * component.TotalQuantity.GetValueOrDefault(0m);
                    }
                }
            }
        }
    }
}
