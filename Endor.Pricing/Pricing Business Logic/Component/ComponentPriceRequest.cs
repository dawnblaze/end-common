﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.Units;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Endor.Pricing
{
    public class ComponentPriceRequest
    {
        /// <summary>
        /// OrderItemComponent.TempID only used for top level components
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Guid? TempID { get; set; }

        /// <summary>
        /// OrderItemComponent.ID only used for top level components
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ID { get; set; }

        /// <summary>
        /// ClassTypeID of the Component (Material|Labor|Machine|Assembly)
        /// </summary>
        public OrderItemComponentType ComponentType { get; set; }

        /// <summary>
        /// The ID of the Component
        /// </summary>
        public int ComponentID { get; set; }

        /// <summary>
        /// The Quantity of the Component.  Null is treated as 0 for computation purposes.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TotalQuantity { get; set; }

        /// <summary>
        /// Flag indicating if the QuantityUnit was overridden.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? TotalQuantityOV { get; set; }

        /// <summary>
        /// The Assembly Quantity of the Component.  Null is treated as 0 for computation purposes.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AssemblyQuantity { get; set; }

        /// <summary>
        /// Flag indicating if the Assembly QuantityUnit was overridden.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? AssemblyQuantityOV { get; set; }

        /// <summary>
        /// The Unit of measure for the Quantity
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Unit? QuantityUnit { get; set; }

        /// <summary>
        /// Is the component included
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsIncluded { get; set; }
        /// <summary>
        /// Is IsIncluded overridden
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsIncludedOV { get; set; }

        /// <summary>
        /// The Unit Price. Null is treated as 0 for computation purposes.
        /// </summary>
        public decimal? PriceUnit { get; set; }
        
        /// <summary>
        /// Flag indicating if the Unit Price was overridden.
        /// </summary>
        public bool? PriceUnitOV { get; set; }

        /// <summary>
        /// The Unit Cost. Null is treated as 0 for computation purposes.
        /// </summary>
        public decimal? CostUnit { get; set; }

        /// <summary>
        /// Flag indicating if the Unit Cost was overridden.
        /// </summary>
        public bool? CostUnitOV { get; set; }

        /// <summary>
        /// Contains a list of variable values, indicating the value and which ones are Overridden.
        /// </summary>
        public Dictionary<string, VariableValue> Variables { get; set; }

        /// <summary>
        /// A list of contained child Component Price Requests
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ComponentPriceRequest> ChildComponents { get; set; }

        /// <summary>
        /// A Dictionary of (NexusID, Nexus) that apply (or might apply) to the line item.
        /// 
        /// This is set automatically by the Order when the order is computed,
        /// but must be passed in by the caller when a line item is computed by itself.
        /// 
        /// Required for tax computations when the order is not tax exempt.
        /// </summary>
        public Dictionary<string, TaxAssessmentNexus> TaxNexusList { get; set; }

        /// <summary>
        /// List of Tax Assessment Requests
        /// </summary>
        public List<TaxAssessmentRequest> TaxInfoList { get; set; }

        /// <summary>
        /// The ID of the Company
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }

        /// <summary>
        /// The result property stores an internal ComponentPriceResult record.
        /// This is used to store interim and final results in the computation.
        /// </summary>
        [JsonIgnore]
        internal ComponentPriceResult Result { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string VariableName { get; set; }

        /// <summary>
        /// This property contains the Line Item Quantity for top-level Assemblies, 
        /// or the Quantity of the Parent Assemblies for Child-level Assemblies; 
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ParentQuantity { get; set; }  // Default to NULL

        /// <summary>
        /// Quantity of LineItem
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? LineItemQuantity { get; set; }  // Default to NULL

        /// <summary>
        /// This property indicates if the Line Item flag indicating the order Is Outsourced (Vended) was set.
        /// </summary>
        public bool IsVended { get; set; }   // Default to FALSE

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? IncomeAccountID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ExpenseAccountID { get; set; }

        /// <summary>
        /// The IncomeAllocationType for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AssemblyIncomeAllocationType? IncomeAllocationType { get; set; }

        /// <summary>
        /// Determines Whether a subcomponent's price/cost is rolled up to the parent component
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? RollupLinkedPriceAndCost { get; set; }

        internal ComponentPriceRequest GetChildComponent(ICBELComponentVariable component)
        {
            if (component is ICBELVariable var)
            {
                return ChildComponents?.FirstOrDefault( x => x.VariableName == var.Name );
            }
            else
            {
                return null;
            }
        }
    }
}
