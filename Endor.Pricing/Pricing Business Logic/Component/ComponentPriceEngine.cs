﻿using Endor.CBEL.Common;
using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Tenant;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Pricing
{
    public static class ComponentPriceEngine
    {
        public async static Task Compute(ComponentPriceRequest request, PricingEngineType engineType, ApiContext ctx, ITenantDataCache cache, RemoteLogger logger, short bid)
        {
            switch (request.ComponentType)
            {
                case OrderItemComponentType.Labor:
                    ComponentPriceEngine_Labor.Compute(request, engineType, ctx, logger, bid);
                    break;
                case OrderItemComponentType.MachineTime:
                case OrderItemComponentType.Machine:
                    ComponentPriceEngine_Machine.Compute(request, engineType, ctx, logger, bid);
                    break;
                case OrderItemComponentType.Material:
                    ComponentPriceEngine_Material.Compute(request, engineType, ctx, logger, bid);
                    break;
                case OrderItemComponentType.Assembly:
                    await ComponentPriceEngine_Assembly.Compute(request, ctx, cache, logger, bid);
                    break;
                default:
                    if (request.Result.Errors == null)
                        request.Result.Errors = new List<PricingErrorMessage>();

                    await logger.PricingError(bid, $"Unknown Component Type Specified - {request.ComponentType}.", null);

                    request.Result.Errors.Add(
                            new PricingErrorMessage()
                            {
                                ErrorType = "Error",
                                Message = $"Unknown Component Type Specified - {request.ComponentType}."
                            }
                        );
                    break;
            }


        }
    }
}
