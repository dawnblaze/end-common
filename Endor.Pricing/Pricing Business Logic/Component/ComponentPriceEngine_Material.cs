﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using System.Linq;

namespace Endor.Pricing
{
    public static class ComponentPriceEngine_Material
    {
        public static void Compute(ComponentPriceRequest request, PricingEngineType engineType, ApiContext ctx, RemoteLogger logger, short bid)
        {
            ComponentPriceResult result = request.Result;

            MaterialData material = ctx.MaterialData.FirstOrDefault(c => c.BID == bid && c.ID == request.ComponentID);

            if (!request.TotalQuantityOV.GetValueOrDefault(false))
            {
                request.TotalQuantity = request.LineItemQuantity;
            }

            if (material == null)
            {
                result.PricePreTax = null;
                result.CostLabor = null;
            }
            else
            {
                result.Name = material.Name;
                result.TotalQuantity = request.TotalQuantity;
                result.QuantityUnit = request.QuantityUnit ?? material.ConsumptionUnit;
                result.LineItemQuantity = request.LineItemQuantity;

                if (request.PriceUnitOV.GetValueOrDefault(false))
                {
                    result.PriceUnit = request.PriceUnit;
                    result.PricePreTax = request.PriceUnit.GetValueOrDefault(0m) * result.TotalQuantity.GetValueOrDefault(0m);
                }
                else
                {
                    result.PricePreTax = material.EstimatingPrice.GetValueOrDefault(0m) * result.TotalQuantity.GetValueOrDefault(0m);

                    if (result.TotalQuantity.GetValueOrDefault(0m) != 0)
                        result.PriceUnit = result.PricePreTax / result.TotalQuantity.Value;
                }

                if (request.CostUnitOV.GetValueOrDefault(false))
                    result.CostMaterial = request.CostUnit.GetValueOrDefault(0m) * result.TotalQuantity.GetValueOrDefault(0m);
                else
                    result.CostMaterial = material.EstimatingCost.GetValueOrDefault(0m) * result.TotalQuantity.GetValueOrDefault(0m);
            }
        }
    }
}
