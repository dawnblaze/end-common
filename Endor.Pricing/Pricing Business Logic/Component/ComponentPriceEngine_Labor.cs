﻿using Endor.EF;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.CBEL.Common;
using Endor.Units;
using System.Linq;

namespace Endor.Pricing
{
    public static class ComponentPriceEngine_Labor
    {
        public static void Compute(ComponentPriceRequest request, PricingEngineType engineType, ApiContext ctx, RemoteLogger logger, short bid)
        {
            ComponentPriceResult result = request.Result;

            LaborData labor = ctx.LaborData.FirstOrDefault(c => c.BID == bid && c.ID == request.ComponentID);

            if (labor == null)
            {
                result.PricePreTax = null;
                result.CostLabor = null;
            }
            else
            {
                result.Name = labor.Name;

                if (!request.TotalQuantityOV.GetValueOrDefault(false) && !request.AssemblyQuantityOV.GetValueOrDefault(false))
                {
                    result.AssemblyQuantity = 1m;

                    result.TotalQuantity = LaborQuantityHelper.ComputeQuantity(
                       new Measurement(labor.BillingIncrementInMin, Unit.Minute),
                       new Measurement(labor.MinimumTimeInMin, Unit.Minute),
                       result.AssemblyQuantity.GetValueOrDefault(1) * request.LineItemQuantity.GetValueOrDefault(1));

                    result.QuantityUnit = request.QuantityUnit ?? Unit.Hour;
                }

                else if (!request.TotalQuantityOV.GetValueOrDefault(false) && request.AssemblyQuantityOV.GetValueOrDefault(false))
                {
                    result.AssemblyQuantity = request.AssemblyQuantity;
                    result.TotalQuantity = LaborQuantityHelper.ComputeQuantity(
                       new Measurement(labor.BillingIncrementInMin, Unit.Minute),
                       new Measurement(labor.MinimumTimeInMin, Unit.Minute),
                       request.AssemblyQuantity.GetValueOrDefault(1) * request.LineItemQuantity.GetValueOrDefault(1));
                }

                else
                {
                    result.TotalQuantity = request.TotalQuantity;

                    result.AssemblyQuantity = request.TotalQuantity.GetValueOrDefault(1) / request.LineItemQuantity.GetValueOrDefault(1);
                    result.QuantityUnit = request.QuantityUnit ?? Unit.Hour;
                }

                result.LineItemQuantity = request.LineItemQuantity;

                if (request.PriceUnitOV.GetValueOrDefault(false))
                {
                    result.PriceUnit = request.PriceUnit;

                    result.PricePreTax = request.PriceUnit.GetValueOrDefault(0m) * result.TotalQuantity.GetValueOrDefault(0m);
                }
                else
                {
                    result.PricePreTax = labor.EstimatingPriceFixed.GetValueOrDefault(0m) + (labor.EstimatingPricePerHour.GetValueOrDefault(0m) / 60m * result.TotalQuantity.GetValueOrDefault(0m) );
                    if (result.TotalQuantity.GetValueOrDefault(0m) != 0)
                        result.PriceUnit = result.PricePreTax / result.TotalQuantity;
                }

                if (request.CostUnitOV.GetValueOrDefault(false))
                    result.CostLabor = request.CostUnit.GetValueOrDefault(0m) * result.TotalQuantity.GetValueOrDefault(0m);
                else
                    result.CostLabor = labor.EstimatingCostFixed.GetValueOrDefault(0m) + (labor.EstimatingCostPerHour.GetValueOrDefault(0m) / 60m * result.TotalQuantity.GetValueOrDefault(0m));
            }
        }
    }
}
