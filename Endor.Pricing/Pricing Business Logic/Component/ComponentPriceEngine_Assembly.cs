﻿using Endor.CBEL.Common;
using Endor.EF;
using System.Threading.Tasks;
using Endor.Tenant;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Endor.CBEL.ObjectGeneration;
using Irony.Parsing;
using Endor.CBEL.Grammar;
using Endor.CBEL.Compute;
using System;
using Endor.CBEL.Elements;
using Endor.Logging.Client;

namespace Endor.Pricing
{
    public static class ComponentPriceEngine_Assembly
    {
        public async static Task Compute(ComponentPriceRequest request, ApiContext ctx, ITenantDataCache cache, RemoteLogger logger, short bid)
        {
            var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, request.ComponentID, ClassType.Assembly.ID(), 1, cache, logger, bid);
            
            if (compilationResponse.Assembly != null)
            {
                ((CBELAssemblyBase)compilationResponse.Assembly).CompanyID = request.CompanyID;
                CBELOverriddenValues computeRequest = CBELAssemblyHelper.ComponentPriceRequestToComputeRequest(request);
                ICBELComputeResult computeResult = compilationResponse.Assembly.Compute(computeRequest);
                CBELAssemblyHelper.ComputePriceResultToComponentPriceResult(request.Result, computeResult, request);

                if (request.Result != null)
                {
                    var quantity = request.Result.TotalQuantity.GetValueOrDefault();

                    if (request.PriceUnitOV.GetValueOrDefault(false))
                        request.Result.PricePreTax = request.PriceUnit.GetValueOrDefault(0m) * quantity;
                    

                    if (request.CostUnitOV.GetValueOrDefault(false))
                        request.Result.CostNet = request.CostUnit.GetValueOrDefault(0m) * quantity;

                    request.Result.PriceUnit = request.Result.PricePreTax / quantity;
                }
            }
            else
            {
                request.Result.Errors = compilationResponse.Errors;
                request.Result.CostLabor = null;
                request.Result.CostMachine = null;
                request.Result.CostMaterial = null;
                request.Result.CostNet = null;
                request.Result.PricePreTax = null;
                request.Result.PriceTotal = null;
            }
        }
    }
}
