﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Pricing
{
    public enum PricingEngineType
    {
        /// <summary>
        /// Manual pricing, the price must be supplied. (The calculated price will always == null.)
        /// </summary>
        Supplied = 0,
        /// <summary>
        /// Pricing = sum of Component Pricing, but not the computation of components
        /// </summary>
        SimplePart = 1,
        /// <summary>
        /// Pricing involves complex assemblies that can add new components
        /// </summary>
        Computed = 2,
        /// <summary>
        /// Pricing is looked up from a master product list.  This feature is not currently supported.
        /// </summary>
        OnlineLookup = 3
    }
}
