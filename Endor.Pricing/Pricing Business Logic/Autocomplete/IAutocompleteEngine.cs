﻿using Endor.CBEL.Metadata;
using Endor.CBEL.Operators;
using System.Collections.Generic;
using System.Threading.Tasks;
using Endor.Models.Autocomplete;
using Endor.Models;

namespace Endor.CBEL.Autocomplete
{
    public interface IAutocompleteEngine
    {
        /// <summary>
        /// common definition
        /// </summary>
        Task<string> CommonDefinition();
        /// <summary>
        /// common definitions of the given class type id
        /// </summary>
        /// <param name="classtypeid"></param>
        /// <returns></returns>
        Task<string> CommonDefinition(int classtypeid);
        /// <summary>
        /// class definition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<string> ClassDefinition(int id, int classtypeid);
        /// <summary>
        /// local definition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<string> LocalDefinition(int id, int classtypeid);
        /// <summary>
        /// server definition
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<string> ServerDefinition(int id, int classtypeid);
        /// <summary>
        /// hint
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        Task<string> Hint(string query);
        /// <summary>
        /// local definition of variables
        /// </summary>
        /// <param name="assemblyVariables"></param>
        /// <returns></returns>
        Task<string> LocalVariableDefinitions(List<AssemblyVariableAutocompleteBrief> assemblyVariables);
        /// <summary>
        /// static operators
        /// </summary>
        /// <returns></returns>
        Task<CBELOperatorInfo[]> Operators();
        /// <summary>
        /// functions given
        /// </summary>
        /// <returns></returns>
        Task<CBELFunctionInfo[]> Functions();

        /// <summary>
        /// Assembly ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        Task<string> GetAssemblyByID(int id, string path);

        /// <summary>
        /// Assembly variables
        /// </summary>
        /// <param name="assemblyVariables"></param>
        /// <returns></returns>
        Task<string> AssemblyVariables(List<AssemblyVariableAutocompleteBrief> assemblyVariables, string path);
    }
}
