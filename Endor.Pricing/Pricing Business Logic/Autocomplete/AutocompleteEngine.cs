﻿using Endor.CBEL.Common;
using Endor.EF;
using Endor.Tenant;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Endor.CBEL.Interfaces;
using Endor.CBEL.Elements;
using Endor.CBEL.Metadata;
using Endor.CBEL.Assembly.BaseClasses;
using Endor.Models;
using System.Text.RegularExpressions;
using Endor.CBEL.Operators;
using Endor.Models.Autocomplete;
using Endor.Units;
using Endor.Logging.Client;
using Endor.CBEL.Exceptions;
using System.Text;

namespace Endor.CBEL.Autocomplete
{
    public class AutocompleteEngine : IAutocompleteEngine
    {
        private string[] excludeGettersSetters = { "get_", "set_" };
        private class AutoCompleteMethodInfo
        {

            public AutoCompleteMethodInfo(MethodInfo method)
            {
                this.Method = method;
                this.MethodName = method.Name;
                this.ReturnType = GetTSType(method.ReturnType);
                this.Parameters = GetParameters(method.GetParameters());
            }

            public MethodInfo Method { get; private set; }
            public string MethodName { get; private set; }
            public string ReturnType { get; private set; }
            public List<(string Name, string ParameterType)> Parameters { get; private set; }

            private List<(string Name, string ParameterType)> GetParameters(ParameterInfo[] parameters)
            {
                if (parameters.Length == 0)
                    return null;

                List<(string Name, string ParameterType)> results = new List<(string Name, string ParameterType)>();

                bool isArray;
                Type paramType;
                foreach (ParameterInfo parameterInfo in parameters)
                {
                    paramType = parameterInfo.ParameterType;
                    isArray = false;

                    if (paramType.IsArray)
                    {
                        paramType = paramType.GetElementType();
                        isArray = true;
                    }

                    if (isArray)
                    {
                        results.Add((parameterInfo.Name + "01", GetTSType(paramType)));
                        results.Add((parameterInfo.Name + "02", GetTSType(paramType)));
                    }

                    else
                        results.Add((parameterInfo.Name, GetTSType(paramType)));
                }

                return results;
            }

            public void NewInstance(MethodInfo method)
            {
                if (ReturnType != "any" && ReturnType != GetTSType(method.ReturnType))
                    ReturnType = "any";

                if (Parameters != null)
                {
                    List<(string Name, string ParameterType)> newParameters = GetParameters(method.GetParameters());

                    if (newParameters != null)
                    {
                        for (int i = 0; i < Parameters.Count; i++)
                        {
                            if (i < newParameters.Count)
                            {
                                if (Parameters[i].Name == newParameters[i].Name)
                                {
                                    if (Parameters[i].ParameterType != "any" && Parameters[i].ParameterType != newParameters[i].ParameterType)
                                        Parameters[i] = (Parameters[i].Name, "any");
                                }
                            }
                        }
                    }
                }
            }
        }

        private class GetAssemblyResult
        {
            private GetAssemblyResult()
            {
            }
            public bool IsSuccess { get; private set; }
            public ICBELAssembly Assembly { get; private set; }
            public string AssemblyName { get; private set; }
            public string AssemblyBaseName { get; private set; }
            public string ErrorMsg { get; private set; }
            public CBELCompilationException Exception { get; private set; }

            static public GetAssemblyResult Success(ICBELAssembly assembly, string assemblyName, string assemblyBaseName)
            {
                return new GetAssemblyResult
                {
                    IsSuccess = true,
                    Assembly = assembly,
                    AssemblyName = assemblyName,
                    AssemblyBaseName = assemblyBaseName
                };
            }
            static public GetAssemblyResult Failure(string errorMsg)
            {
                return new GetAssemblyResult
                {
                    IsSuccess = false,
                    ErrorMsg = errorMsg
                };
            }
            static public GetAssemblyResult Failure(AssemblyCompilationResponse compilationResponse)
            {
                return new GetAssemblyResult
                {
                    IsSuccess = false,
                    Exception = new CBELCompilationException($"Error loading {compilationResponse.Name}", compilationResponse)
                };
            }
        }

        public AutocompleteEngine(short bid, ApiContext context, ITenantDataCache cache, RemoteLogger logger)
        {
            BID = bid;
            Context = context;
            this.Cache = cache;
            this.Logger = logger;
        }

        private short BID { get; set; }
        private ApiContext Context { get; set; }
        private ITenantDataCache Cache { get; set; }
        private RemoteLogger Logger { get; set; }

        public AssemblyType GetAssemblyType(int id)
        {
            AssemblyData assemblyData = Context.AssemblyData.FirstOrDefault(x => x.BID == BID && x.ID == id);

            if (assemblyData == null)
                return AssemblyType.Product;

            return assemblyData.AssemblyType;
        }

        public async Task<string> CommonDefinition()
        {
            CodeBuilder result = new CodeBuilder();

            result.Append("// This document contains the class structure for");
            result.Append("//");
            result.Append("//  1. Global Functions");
            result.Append("//  2. Object Classes");
            result.Append("//  3. Variable Classes");
            result.Append("//  4. Measurement Classes");
            result.Append("//  5. Custom Field Classes");
            result.Append("//");
            result.AppendCR();
            result.AppendCR();

            result.Append("// ======================================================================");
            result.Append("// ===================== Global Functions ===============================");
            result.Append("// ======================================================================");
            result.AppendCR();
            result.Append("type DateTime = number;");
            result.AppendCR();
            FillGlobalMethods(result
                        , typeof(ExcelFunctions)
                        , typeof(CBELFunctions)
                        );
            result.AppendCR();
            result.AppendCR();
            result.AppendCR();


            result.Append("// ======================================================================");
            result.Append("// ===================== Object Classes =================================");
            result.Append("// ======================================================================");
            result.AppendCR();

            FillClasses(result
                        , typeof(CBELBaseComponent)
                        , typeof(CBELAssemblyBase)
                        , typeof(CBELMachineBase)
                        , typeof(ICBELObject)
                        , typeof(ICBELOrderObject)
                        , typeof(ICBELEstimateObject)
                        , typeof(ICBELCreditMemoObject)
                        , typeof(ICBELOrderItemObject)
                        , typeof(ICBELDestinationObject)
                        , typeof(ICBELCompanyObject)
                        , typeof(ICBELContactObject)
                        , typeof(ICBELMaterialObject)
                        , typeof(ICBELLaborObject)
                        , typeof(ICBELEmployeeObject)
                        , typeof(ICBELOpportunityObject)
                        , typeof(ICBELAddressObject)
                        , typeof(ICBELPhoneObject)
                        , typeof(ICBELLocatorObject)
                        , typeof(ICBELMaterialComponent)
                        , typeof(ICBELLaborComponent)
                        , typeof(ICBELLayoutComputation)
                        );


            result.AppendCR();
            result.AppendCR();
            result.AppendCR();

            result.Append("// ======================================================================");
            result.Append("// ===================== Variable Classes ===============================");
            result.Append("// ======================================================================");
            result.AppendCR();

            FillClasses(result
                        , typeof(CBELComputedVariableBase<string>)
                        , typeof(CBELComputedVariableBase<decimal?>)
                        , typeof(CBELComputedVariableBase<bool?>)
                        , typeof(CBELComponentVariableBase<string>)
                        );

            result.AppendCR();
            result.AppendCR();
            result.AppendCR();

            FillEnum(result, typeof(UnitType));

            FillClasses(result,
                customCode: null,
                excludeNameContains: "In*,UnitType",
                typeof(NumberVariable));

            FillUnitTypes(result, "Numeric", "Variable", "NumberVariable");

            FillClasses(result
                        , typeof(StringVariable)
                        , typeof(DropDownStringListElement)
                        , typeof(MultiLineLabelElement)
                        , typeof(MultiLineTextElement)
                        , typeof(SingleLineLabelElement)
                        , typeof(SingleLineTextElement)
                        , typeof(URLLabelElement)
                        , typeof(DropDownNumberListElement)
                        , typeof(NumberEditElement)
                        , typeof(BooleanVariable)
                        , typeof(CheckboxElement)
                        , typeof(ShowHideGroupElement)
                        , typeof(ConsumptionStringVariable)
                        , typeof(LinkedMachineVariable)
                        , typeof(LinkedMaterialVariable)
                        , typeof(DropDownLaborListElement)
                        , typeof(DropDownMaterialListElement)
                        , typeof(LayoutComputationAssemblyVariable)
                        , typeof(LinkedAssemblyVariable)
                        , typeof(LinkedLaborVariable)
                        , typeof(CBELDataTable<string, string, string>)
                        , typeof(CBELDataTable<decimal, decimal, decimal>)
                        , typeof(CBELDataTable<bool, bool, bool>)
                        , typeof(StringDataTable<string, string>)
                        , typeof(NumberDataTable<decimal, decimal>)
                        , typeof(BooleanDataTable<bool, bool>)
                        );



            result.AppendCR();
            result.AppendCR();
            result.AppendCR();


            result.Append("// ======================================================================");
            result.Append("// ===================== Measurement Classes ============================");
            result.Append("// ======================================================================");
            result.AppendCR();

            FillClasses(result
                        , customCode: null
                        , excludeNameContains: "In*"
                        , typeof(Measurement)
                        );

            result.AppendCR();

            FillUnitTypes(result, "", "Measurement", "MeasurementBase");

            result.AppendCR();

            result = await FillCustomClasses(result);

            return await Task.FromResult(result.ToString());
        }

        public Task<string> CommonDefinition(int classtypeid)
        {
            CodeBuilder result = new CodeBuilder();

            // now only expects assembly and machine type
            if (classtypeid == ClassType.Assembly.ID())
                FillDefinitions(result
                    , typeof(CBELAssemblyBase)
                    );
            else
                FillDefinitions(result
                    , typeof(CBELMachineBase)
                    );

            return Task.FromResult(result.ToString());
        }

        public async Task<string> ClassDefinition(int id, int classtypeid)
        {
            AssemblyCompilationResponse compilationResponse = await GetCompilationResponse(id, classtypeid);

            if (!compilationResponse.Success)
                throw new CBELCompilationException($"Error loading {compilationResponse.Name}", compilationResponse);

            CodeBuilder result = new CodeBuilder();

            string stringType = classtypeid == ClassType.Assembly.ID() ? "Assembly" : "Machine";

            FillClass(result
                      , compilationResponse.Assembly.GetType()
                      , assembly: compilationResponse.Assembly
                      , assemblyName: FormatTSName(compilationResponse.Assembly.Name) + stringType
                      , assemblyBaseName: null
                      );

            return result.ToString();

        }

        public async Task<string> LocalDefinition(int id, int classtypeid)
        {
            AssemblyCompilationResponse compilationResponse = await GetCompilationResponse(id, classtypeid);

            if (!compilationResponse.Success)
                throw new CBELCompilationException($"Error loading {compilationResponse.Name}", compilationResponse);

            CodeBuilder result = new CodeBuilder();

            FillDefinitions(result
                        , compilationResponse.Assembly
                        );

            return result.ToString();
        }

        public async Task<string> ServerDefinition(int id, int classtypeid)
        {
            AssemblyCompilationResponse compilationResponse = await GetCompilationResponse(id, classtypeid);

            if (!compilationResponse.Success)
                throw new CBELCompilationException($"Error loading {compilationResponse.Name}", compilationResponse);

            return compilationResponse.AssemblyCode;
        }

        public async Task<string> Hint(string query)
        {
            CodeBuilder result = new CodeBuilder();

            FillClassValues(result
                        , query
                        , suffix: "_Hint"
                        , typeof(ExcelFunctions)
                        , typeof(CBELFunctions)
                        );

            if (result.Length == 0)
                return null;

            return await Task.FromResult(result.ToString());
        }

        public Task<CBELFunctionInfo[]> Functions()
        {
            return Task.FromResult(
                FillFunctionInfosAsArray(
                    typeof(ExcelFunctions),
                    typeof(CBELFunctions))
                );
        }

        public Task<CBELOperatorInfo[]> Operators()
        {
            return Task.FromResult(FillOperators<CBELOperatorInfo[]>());
        }

        private async Task<AssemblyCompilationResponse> GetCompilationResponse(int id, int classtypeid)
        {
            bool isMachineTemplate = GetAssemblyType(id) == AssemblyType.MachineTemplate;
            try
            {
                // try to get or generate assembly
                return await CBELAssemblyHelper.LoadAssembly(Context, id, classtypeid, 1, Cache, Logger, BID, isMachineTemplate: isMachineTemplate);
            }
            catch (Exception e)
            {
                string name = $"{(isMachineTemplate ? "Machine Template" : "Assembly")}:  (ID: {id},  ClassTypeID: {classtypeid})";
                // create an error response if the load assembly throws an exception
                return new AssemblyCompilationResponse(name)
                {
                    Errors = new List<PricingErrorMessage>()
                    {
                        new PricingErrorMessage{ Message = e.Message }
                    }
                };
            }
        }

        public Task<string> LocalVariableDefinitions(List<AssemblyVariableAutocompleteBrief> assemblyVariables)
        {
            CodeBuilder result = new CodeBuilder();

            if (assemblyVariables.Count > 0)
            {
                FillVariableDefinitions(result
                        , assemblyVariables
                        , "declare var"
                        , excludeContains: "Capacity,Count"
                        );
            }

            return Task.FromResult(result.ToString());
        }

        /// <summary>
        /// Assembly ID and path 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<string> GetAssemblyByID(int id, string path)
        {
            AssemblyCompilationResponse compilationResponse = await GetCompilationResponse(id, ClassType.Assembly.ID());

            if (!compilationResponse.Success)
                throw new CBELCompilationException($"Error loading {compilationResponse.Name}", compilationResponse);

            PropertyInfo[] properties = compilationResponse.Assembly.GetType().GetProperties();

            GetAssemblyResult validPathResult = await IsValidPath(path.Split('.'), properties, compilationResponse.Assembly);

            if (!validPathResult.IsSuccess)
            {
                if (validPathResult.Exception != null)
                    throw validPathResult.Exception;

                if (validPathResult.ErrorMsg == "Path is not an assembly")
                    return "";

                throw new KeyNotFoundException(validPathResult.ErrorMsg);
            }

            CodeBuilder result = new CodeBuilder();

            FillClass(result
                    , validPathResult.Assembly.GetType()
                    , assembly: validPathResult.Assembly
                    , assemblyName: validPathResult.AssemblyName
                    , assemblyBaseName: validPathResult.AssemblyBaseName
                    );

            return result.ToString();
        }

        private async Task<GetAssemblyResult> IsValidPath(string[] path, PropertyInfo[] propertyInfos, object reference)
        {
            PropertyInfo[] valuePropertyInfos = null;
            GetAssemblyResult assemblyEquivalent = null;

            if (path.Length == 0) 
                return GetAssemblyResult.Failure("Path is not defined");

            PropertyInfo propertyPath = propertyInfos.Where(p => p.Name.ToLower() == path[0].ToLower()).FirstOrDefault();

            if (propertyPath == null)
                return GetAssemblyResult.Failure($"Invalid Path {string.Join('.', path)}");
            
            object propertyValue = propertyPath.GetValue(reference);            

            if (propertyValue != null)
                valuePropertyInfos = propertyValue.GetType().GetProperties();

            if (path.Length == 1)
            {
                if (propertyValue == null)
                    return GetAssemblyResult.Failure("Path is not an assembly");

                return await GetAssemblyEquivalent(valuePropertyInfos, propertyValue);
            }
            else
                assemblyEquivalent = await GetAssemblyEquivalent(valuePropertyInfos, propertyValue);

            valuePropertyInfos = assemblyEquivalent.Assembly?.GetType().GetProperties();
            return await IsValidPath(path.Skip(1).ToArray(), valuePropertyInfos, assemblyEquivalent.Assembly);            
        }

        private void GetPropertyMethodInfo(ICBELAssembly assembly, out PropertyInfo[] valuePropertyInfos, out MethodInfo[] valueMethodInfos)
        {
            Type type = assembly.GetType();
            valuePropertyInfos = type.GetProperties();
            valueMethodInfos = type.GetMethods()
                    .Where(x => x.DeclaringType != typeof(Object)
                        && (!excludeGettersSetters.Contains(x.Name.ToLower().Substring(0, 4))))
                    .ToArray();
        }

        private bool IsValidProperty(PropertyInfo[] valuePropertyInfos, string propertyName = "")
        {
            if (propertyName.Length > 0)
            {
                PropertyInfo propInfo = valuePropertyInfos.Where(p => p.Name.ToLower() == propertyName.ToLower()).FirstOrDefault();
                if (propInfo == null)
                    return false;

                AutocompleteIgnoreAttribute attr = propInfo.GetCustomAttributes<AutocompleteIgnoreAttribute>().FirstOrDefault();
                if (attr != null)
                    return false;
            }

            if (propertyName.Length == 0 && valuePropertyInfos.Where(p => p.Name.ToLower() == "componentclasstype").FirstOrDefault() == null)
                return false;

            return true;
        }

        private bool IsValidMethod(MethodInfo[] methodInfos, string methodName)
        {
            MethodInfo methodInfo = methodInfos.Where(m => m.Name.ToLower() == methodName.ToLower()).FirstOrDefault();
            if (methodInfo == null)
                return false;

            AutocompleteIgnoreAttribute attr = methodInfo.GetCustomAttributes<AutocompleteIgnoreAttribute>().FirstOrDefault();
            if (attr != null)
                return false;

            return true;
        }

        private async Task<GetAssemblyResult> GetAssemblyEquivalent(PropertyInfo[] valuePropertyInfos, object componentObj)
        {
            AssemblyCompilationResponse compilationResponse;
            string getID = "";
            int classTypeID = ClassType.Assembly.ID();
            string stringType = "Assembly";

            PropertyInfo classType = valuePropertyInfos
                                .Where(p => p.Name.ToLower() == "componentclasstype")
                                .FirstOrDefault();

            if (classType == null)
                return GetAssemblyResult.Failure("Path is not an assembly");

            OrderItemComponentType componentType = (OrderItemComponentType)classType.GetValue(componentObj);

            switch (componentType)
            {
                case OrderItemComponentType.Assembly:
                    getID = "LinkedAssemblyID";
                    break;
                case OrderItemComponentType.Machine:
                    getID = "LinkedMachineID";
                    classTypeID = (int)ClassType.Machine.ID();
                    stringType = "Machine";
                    break;
                case OrderItemComponentType.Labor:
                    getID = "LinkedLaborID";
                    break;
                case OrderItemComponentType.Material:
                    getID = "LinkedMaterialID";
                    break;
            }

            int id = Convert.ToInt32(valuePropertyInfos
                        .Where(p => p.Name.ToLower() == getID.ToLower())
                        .FirstOrDefault()
                        .GetValue(componentObj));

            compilationResponse = await GetCompilationResponse(id, classTypeID);

            if (!compilationResponse.Success)
            {
                return GetAssemblyResult.Failure(compilationResponse);
            }

            return GetAssemblyResult.Success(compilationResponse.Assembly
                                             , FormatTSName(compilationResponse.Assembly.Name) + stringType
                                             , $"Linked{stringType}VariableBase");
        }

        /// <summary>
        /// AssemblyVariables and path
        /// </summary>
        /// <param name="assemblyVariables"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public async Task<string> AssemblyVariables(List<AssemblyVariableAutocompleteBrief> assemblyVariables, string path)
        {
            CodeBuilder result = new CodeBuilder();
            string[] pathInfo = path.Split('.');
            if (pathInfo.Length == 0)
                return "";

            AssemblyCompilationResponse compilationResponse = null;

            AssemblyVariableAutocompleteBrief assembly = assemblyVariables.Where(x => x.Name.ToLower() == pathInfo[0].ToLower()).FirstOrDefault();
            if (assembly == null)
                throw new KeyNotFoundException($"Invalid Path {path}");
            
            assembly = assemblyVariables.Where(x => (x.LinkedAssemblyID != null || x.LinkedMachineID != null) && x.Name.ToLower() == pathInfo[0].ToLower()).FirstOrDefault();

            if (assembly == null)
                return "";

            int assemblyID = (assembly.LinkedAssemblyID != null ? (int)assembly.LinkedAssemblyID : (int)assembly.LinkedMachineID); 
            int classTypeID = (assembly.LinkedAssemblyID != null ? ClassType.Assembly.ID() : ClassType.Machine.ID());
            string stringType = (assembly.LinkedAssemblyID != null ? "Assembly" : "Machine");

            compilationResponse = await GetCompilationResponse(assemblyID, classTypeID);

            if (!compilationResponse.Success)
                throw new CBELCompilationException($"Error loading {compilationResponse.Name}", compilationResponse);

            if (pathInfo.Length == 1)
            {
                FillClass(result
                    , (compilationResponse.Assembly as ICBELAssembly).GetType()
                    , assembly: compilationResponse.Assembly as ICBELAssembly
                    , assemblyName: FormatTSName(compilationResponse.Assembly.Name) + stringType
                    , assemblyBaseName: $"Linked{stringType}VariableBase"
                    );

                return result.ToString();
            }

            PropertyInfo[] properties = compilationResponse.Assembly.GetType().GetProperties();

            pathInfo = pathInfo.Skip(1).ToArray();
            GetAssemblyResult validPathResult = await IsValidPath(pathInfo, properties, compilationResponse.Assembly);

            if (!validPathResult.IsSuccess)
            {
                if (validPathResult.Exception != null)
                    throw validPathResult.Exception;

                if (validPathResult.ErrorMsg == "Path is not an assembly")
                    return "";

                throw new KeyNotFoundException(validPathResult.ErrorMsg);
            }

            FillClass(result
                , validPathResult.Assembly.GetType()
                , assembly: validPathResult.Assembly
                , assemblyName: validPathResult.AssemblyName
                , assemblyBaseName: validPathResult.AssemblyBaseName
            );
            FillDefinitions(result, validPathResult.Assembly);

            return result.ToString();
        }

        private static string GetTSType(Type type, AssemblyElementType elementType = AssemblyElementType.Spacer)
        {
            if (type == null) return "any";

            try
            {
                Type baseType = type;

                if (baseType == typeof(void))
                    return "void";

                if (baseType.IsGenericType && baseType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    baseType = Nullable.GetUnderlyingType(baseType);

                if (baseType == typeof(string))
                    return "string";

                if (baseType == typeof(int)
                    || baseType == typeof(decimal)
                    || baseType == typeof(byte)
                    || baseType == typeof(short)
                    )
                    return "number";

                if (baseType == typeof(bool))
                    return "Boolean";

                if (baseType == typeof(DateTime))
                    return "DateTime";

                // generic class renaming
                if (baseType == typeof(ICBELObject)
                    || baseType.GetInterfaces().Contains(typeof(ICBELObject))
                    || baseType == typeof(ICBELCustomFields)
                    || baseType.GetInterfaces().Contains(typeof(ICBELCustomFields))
                    || baseType == typeof(ICBELVariableComputed)
                    || baseType.GetInterfaces().Contains(typeof(ICBELVariableComputed))
                    || baseType.GetInterfaces().Contains(typeof(ICBELComponent))
                    || baseType == typeof(Measurement)
                    || baseType == typeof(UnitType)
                    || baseType == typeof(AssemblyVariableAutocompleteBrief)
                    )
                    return GetTypeName(baseType);

                if(baseType == typeof(AssemblyElementType))
                {
                    string typeName = "";

                    switch (elementType)
                    {
                        case AssemblyElementType.SingleLineText:
                        case AssemblyElementType.SingleLineLabel:
                        case AssemblyElementType.UrlLabel:
                        case AssemblyElementType.MultiLineString:
                        case AssemblyElementType.MultiLineLabel:
                            typeName = "StringVariable";
                            break;
                        case AssemblyElementType.LinkedAssembly:
                        case AssemblyElementType.LinkedLabor:
                        case AssemblyElementType.LinkedMachine:
                        case AssemblyElementType.LinkedMaterial:
                            typeName = $"{elementType.ToString()}VariableBase";
                            break;
                        default:
                            typeName = "VariableBase";
                            break;
                    }

                    return typeName;
                }
            }
            catch (Exception)
            {
                return "any";
            }

            return "any";
        }

        private static string GetTypeName(Type type)
        {
            if (type == null) return "undefined";

            string result;
            string genTypeString = "";

            if (type.IsGenericType)
            {
                Type genType = type.GetGenericArguments().First();

                genTypeString = GetTSType(genType);
            }

            AutocompleteAttribute attr = type.GetCustomAttributes<AutocompleteAttribute>(false).FirstOrDefault();

            if (attr != null && !string.IsNullOrWhiteSpace(attr.Alias))
                result = attr.Alias;
            else
            {
                result = type.Name;

                int index = result.IndexOf("`");
                if (index >= 0)
                    result = result.Substring(0, index);
            }

            if (result.ToLower().Contains(genTypeString.ToLower()))
                return result;
            else
                return $"{result}_{genTypeString}";
        }

        private static void AddToMethodDictionary(Dictionary<string, AutoCompleteMethodInfo> methodDictionary, MethodInfo method)
        {
            if (methodDictionary.ContainsKey(method.Name.ToLower()))
            {
                AutoCompleteMethodInfo methodInfo = methodDictionary[method.Name.ToLower()];
                methodInfo.NewInstance(method);
            }
            else
                methodDictionary.Add(method.Name.ToLower(), new AutoCompleteMethodInfo(method));

        }

        private static void FillGlobalMethods(CodeBuilder result, params Type[] types)
        {
            Dictionary<string, AutoCompleteMethodInfo> methodDictionary = new Dictionary<string, AutoCompleteMethodInfo>();

            foreach (Type type in types)
            {
                MethodInfo[] methodInfos = type.GetMethods(BindingFlags.Public | BindingFlags.Static).Where(x => x.DeclaringType == type).ToArray();

                foreach (MethodInfo method in methodInfos)
                    AddToMethodDictionary(methodDictionary, method);
            }

            foreach (AutoCompleteMethodInfo methodInfo in methodDictionary.Values.OrderBy(x => x.MethodName.ToLower()))
            {
                result.Append($"declare var {methodInfo.MethodName}: ( ", addCR: false);

                List<string> paramList = new List<string>();

                try
                {

                    if (methodInfo.Parameters != null)
                    {
                        foreach ((string Name, string ParameterType) paramInfo in methodInfo.Parameters)
                            paramList.Add($"{paramInfo.Name}: {paramInfo.ParameterType}");

                        result.Append(string.Join(", ", paramList), addCR: false, indent: false);
                        result.Append(" ", addCR: false, indent: false);
                    }
                }
                catch (Exception e)
                {
                    result.AppendCR();
                    result.Append("/**");
                    result.Append("Error Generating Code");
                    result.Append($"FillGlobalMethods - {methodInfo.MethodName}");
                    result.Append(e.Message);
                    result.Append("**/");
                }

                result.Append($") => {methodInfo.ReturnType};", indent: false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="type"></param>
        /// <param name="prefix"></param>
        /// <param name="excludePropertyContains">add "*" to the string to make it wild card</param>
        private void FillPropertiesFromType(CodeBuilder result, Type type, string prefix = "", string excludePropertyContains = null, bool includeInherited = false, ICBELAssembly assembly = null)
        {
            PropertyInfo[] propInfos;
            if (includeInherited)
                propInfos = type.GetProperties().Where(x => x.DeclaringType != typeof(Object)).ToArray();
            else
                propInfos = type.GetProperties().Where(x => x.DeclaringType == type).ToArray();

            // to handle no space prefix
            if (prefix.Length > 0)
                if (!prefix.EndsWith(' '))
                    prefix += " ";

            foreach (PropertyInfo propInfo in propInfos)
            {
                bool isExcluded = false;
                string propertyName = propInfo.Name;
                string typeString = "";
                Type propertyType;


                AutocompleteIgnoreAttribute attr = propInfo.GetCustomAttributes<AutocompleteIgnoreAttribute>().FirstOrDefault();

                if (attr != null)
                    isExcluded = true;
                else if (!string.IsNullOrWhiteSpace(excludePropertyContains))
                {
                    try
                    {
                        string[] excludes = excludePropertyContains.Split(",");

                        foreach (string exclude in excludes)
                        {
                            if (exclude.EndsWith("*") && propInfo.Name.StartsWith(exclude.Replace("*", "")))
                            {
                                isExcluded = true;
                                break;
                            }
                            else if (propInfo.Name == exclude)
                            {
                                isExcluded = true;
                                break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        result.Append("/**");
                        result.Append("Error Generating Code");
                        result.Append($"FillPropertiesFromType - {propInfo.Name}");
                        result.Append(e.Message);
                        result.Append("**/");
                    }
                }

                if (!isExcluded)
                {
                    if (propInfo.PropertyType != typeof(Object))
                        typeString = GetTSType(propInfo.PropertyType);
                    else
                    {
                        object propertyValue = null;

                        if (assembly != null)
                        {
                            propertyValue = propInfo.GetValue(assembly);
                            propertyType = propertyValue.GetType();
                        }
                        else
                            propertyType = propInfo.PropertyType;

                        switch (propertyType.Name)
                        {
                            case "LinkedAssemblyVariable":
                                LinkedAssemblyVariable linkedAssembly = propertyValue as LinkedAssemblyVariable;
                                AssemblyData parentAssembly = Context.AssemblyData.FirstOrDefault(x => x.BID == BID && x.ID == linkedAssembly.LinkedAssemblyID);

                                typeString = $"{parentAssembly.Name}Assembly";
                                break;
                            case "LinkedMachineVariable":
                                LinkedMachineVariable linkedMachine = propertyValue as LinkedMachineVariable;
                                MachineData parentMachine = Context.MachineData.FirstOrDefault(x => x.BID == BID && x.ID == linkedMachine.LinkedMachineID);

                                typeString = $"{parentMachine.Name}Machine";
                                break;
                            case "LinkedLaborVariable":
                                LinkedLaborVariable linkedLabor = propertyValue as LinkedLaborVariable;
                                LaborData parentLabor = Context.LaborData.FirstOrDefault(x => x.BID == BID && x.ID == linkedLabor.LinkedLaborID);

                                typeString = $"{parentLabor.Name}Labor";
                                break;
                            case "LinkedMaterialVariable":
                                LinkedMaterialVariable linkedMaterial = propertyValue as LinkedMaterialVariable;
                                MaterialData parentMaterial = Context.MaterialData.FirstOrDefault(x => x.BID == BID && x.ID == linkedMaterial.LinkedMaterialID);

                                typeString = $"{parentMaterial.Name}Material";
                                break;
                            default:
                                typeString = GetTSType(propertyType);
                                break;
                        }
                        typeString = FormatTSName(typeString);
                    }

                    // to handle unit specific properties
                    if (typeString == "NumberEditVariable")
                    {
                        try
                        {
                            if (assembly != null)
                            {
                                object value = propInfo.GetValue(assembly);
                                NumberEditElement field = (NumberEditElement)Convert.ChangeType(value, typeof(NumberEditElement));

                                if (field != null)
                                {
                                    UnitTypeInfo unitType = field.UnitTypeInfo();

                                    // remove unecessary characters
                                    Regex pattern = new Regex("[()/ ]");
                                    string className = pattern.Replace($"{unitType.Name}", "");

                                    typeString = $"Numeric{className}Variable";
                                }
                                else
                                {
                                    typeString = "NumberVariable";
                                }

                                result.AppendCR();
                                result.Append("/**");
                                result.Append($"* @type {{{typeString}}}");
                                result.Append("*/");

                                result.Append($"{prefix}{propertyName}: {typeString};");
                            }
                        }
                        catch (Exception e)
                        {
                            result.Append("/**");
                            result.Append("Error Generating Code");
                            result.Append($"FillPropertiesFromType - {propInfo.Name}");
                            result.Append(e.Message);
                            result.Append("**/");
                        }
                    }
                    else
                    {
                        result.AppendCR();
                        result.Append("/**");
                        result.Append($"* @type {{{typeString}}}");
                        result.Append("*/");

                        result.Append($"{prefix}{propertyName}: {typeString};");
                    }
                }
            }
        }

        private void FillVariableProperties(CodeBuilder result, AssemblyVariableAutocompleteBrief assemblyVariable, string prefix = "", string excludePropertyContains = null)
        {
            // to handle no space prefix
            if (prefix.Length > 0)
                if (!prefix.EndsWith(' '))
                    prefix += " ";

            bool isExcluded = false;
            
            if (!string.IsNullOrWhiteSpace(excludePropertyContains))
            {
                try
                {
                    string[] excludes = excludePropertyContains.Split(",");

                    foreach (var exclude in excludes)
                    {
                        if (exclude.EndsWith("*") && assemblyVariable.Name.StartsWith(exclude.Replace("*", "")))
                        {
                            isExcluded = true;
                            break;
                        }
                        else if (assemblyVariable.Name == exclude)
                        {
                            isExcluded = true;
                            break;
                        }
                    }
                }
                catch (Exception e)
                {
                    result.Append("/**");
                    result.Append("Error Generating Code");
                    result.Append($"FillPropertiesFromType - {assemblyVariable.Name}");
                    result.Append(e.Message);
                    result.Append("**/");
                }
            }

            if (!isExcluded)
            {
                // to handle unit specific properties
                try
                {
                    string typeString;

                    // handle only number variables for now
                    if (assemblyVariable.ElementType == AssemblyElementType.Number)
                    {
                        if (assemblyVariable.UnitType != null && assemblyVariable.UnitType != UnitType.General)
                        {
                            var unitType = assemblyVariable.UnitType.Value.Info();

                            // remove unecessary characters
                            Regex pattern = new Regex("[()/ ]");
                            string className = pattern.Replace($"{unitType.Name}", "");

                            typeString = $"Numeric{className}Variable";
                        }
                        else
                        {
                            typeString = "NumberVariable";
                        }
                    }
                    else if (assemblyVariable.ElementType == AssemblyElementType.LinkedAssembly)
                    {
                        AssemblyData parentAssembly = Context.AssemblyData.FirstOrDefault(x => x.BID == BID && x.ID == assemblyVariable.LinkedAssemblyID);
                        typeString = FormatTSName($"{parentAssembly.Name}Assembly");
                    }
                    else if (assemblyVariable.ElementType == AssemblyElementType.LinkedMachine)
                    {
                        MachineData parentMachine = Context.MachineData.FirstOrDefault(x => x.BID == BID && x.ID == assemblyVariable.LinkedMachineID);

                        typeString = FormatTSName($"{parentMachine.Name}Machine");
                    }
                    else
                        typeString = GetTSType(assemblyVariable.ElementType.GetType(), assemblyVariable.ElementType);

                    result.AppendCR();
                    result.Append("/**");
                    result.Append($"* @type {{{typeString}}}");
                    result.Append("*/");

                    result.Append($"{prefix}{assemblyVariable.Name}: {typeString};");
                }
                catch (Exception e)
                {
                    result.Append("/**");
                    result.Append("Error Generating Code");
                    result.Append($"FillPropertiesFromType - {assemblyVariable.Name}");
                    result.Append(e.Message);
                    result.Append("**/");
                }
            }            
        }

        private static void FillValuesFromObject(CodeBuilder result, object _object, string name = null)
        {
            bool skipName = false;

            if (!string.IsNullOrWhiteSpace(name))
            {
                result.Append($"Name: \"{name}\",");
                skipName = true;
            }

            // fill the class with values
            foreach (PropertyInfo propInfo in _object.GetType().GetProperties())
            {
                try
                {
                    object value = propInfo.GetValue(_object);
                    // set the name
                    if (!(skipName && propInfo.Name == "Name") && value != null)
                    {
                        // adds the quotes to strings
                        if (propInfo.PropertyType.IsArray)
                        {
                            result.Append($"{propInfo.Name}: [] = [");
                            result.IncIndent();

                            foreach (object item in (Array)value)
                            {
                                result.Append("{");
                                result.IncIndent();

                                FillValuesFromObject(result, item);

                                result.DecIndent();
                                result.Append("},");
                            }

                            result.DecIndent();
                            result.Append($"] as {propInfo.PropertyType.Name},");
                        }
                        else if (propInfo.PropertyType == typeof(string))
                        {
                            if (!string.IsNullOrWhiteSpace((string)value))
                                result.Append($"{propInfo.Name}: \"{value}\",");
                        }
                        else if (propInfo.PropertyType == typeof(bool))
                        {
                            if ((bool)value)
                                result.Append($"{propInfo.Name}: true,");
                        }
                        else if (propInfo.PropertyType == typeof(int))
                        {
                            if ((int)value != 0)
                                result.Append($"{propInfo.Name}: {value},");
                        }
                        else
                        {
                            result.Append($"{propInfo.Name}: {value},");
                        }

                    }
                }
                catch (Exception e)
                {
                    result.Append("/**");
                    result.Append("Error Generating Code");
                    result.Append($"FillPropertiesWithValueFromType - {propInfo.Name}");
                    result.Append(e.Message);
                    result.Append("**/");
                    result.AppendCR();
                }
            }
        }

        private static bool FillMembersWithValueFromMethod(CodeBuilder result, MethodInfo method, string prefix = null, bool includeStartingComma = false)
        {
            CBELFunctionInfoAttribute attr = method.GetCustomAttributes<CBELFunctionInfoAttribute>(false).FirstOrDefault();

            if (attr == null)
                return false;

            if (includeStartingComma)
                result.Append(",", indent: false);

            if (!string.IsNullOrWhiteSpace(prefix))
                result.Append(prefix);

            result.Append("{");
            result.IncIndent();

            FillValuesFromObject(result, attr, method.Name);

            result.DecIndent();
            result.Append("}", addCR: false);
            return true;
        }

        private static CBELFunctionInfo FillFunctionInfos(MethodInfo method)
        {
            CBELFunctionInfoAttribute attr = method.GetCustomAttributes<CBELFunctionInfoAttribute>(false).FirstOrDefault();

            if (attr == null)
                return null;

            return new CBELFunctionInfo()
            {
                Name = attr.Name,
                InsertedText = attr.InsertedText,
                DataType = attr.DataType,
                Hint = attr.Hint,
                Description = attr.Description,
                LongDescription = attr.LongDescription,
                MinParams = attr.MinParams,
                MaxParams = attr.MaxParams,
                Params = attr.Params
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="type"></param>
        /// <param name="prefix"></param>
        /// <param name="excludeMethodContains">add "*" to the string to make it wild card</param>
        private static void FillMethodsFromType(CodeBuilder result, Type type, string prefix = "", string excludeMethodContains = null, bool includeInherited = false)
        {
            Dictionary<string, AutoCompleteMethodInfo> methodDictionary = new Dictionary<string, AutoCompleteMethodInfo>();

            MethodInfo[] methodInfos;

            if (includeInherited)
                methodInfos = type.GetMethods().Where(x => !x.IsSpecialName && x.DeclaringType != typeof(Object)).ToArray();
            else
                methodInfos = type.GetMethods().Where(x => !x.IsSpecialName && x.DeclaringType == type).ToArray();

            // to handle no space prefix
            if (prefix.Length > 0)
                if (!prefix.EndsWith(' '))
                    prefix += " ";

            foreach (MethodInfo method in methodInfos)
                AddToMethodDictionary(methodDictionary, method);

            foreach (AutoCompleteMethodInfo methodInfo in methodDictionary.Values.OrderBy(x => x.MethodName.ToLower()))
            {
                bool isExcluded = false;

                AutocompleteIgnoreAttribute attr = methodInfo.Method.GetCustomAttributes<AutocompleteIgnoreAttribute>().FirstOrDefault();

                if (attr != null)
                    isExcluded = true;
                else if (!string.IsNullOrWhiteSpace(excludeMethodContains))
                {
                    try
                    {
                        string[] excludes = excludeMethodContains.Split(",");

                        // checking the string needed to be excluded
                        foreach (string exclude in excludes)
                        {
                            if (exclude.EndsWith("*") && methodInfo.MethodName.StartsWith(exclude.Replace("*", "")))
                            {
                                isExcluded = true;
                                break;
                            }
                            else if (methodInfo.MethodName == exclude)
                            {
                                isExcluded = true;
                                break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        result.Append("/**");
                        result.Append("Error Generating Code");
                        result.Append($"FillMethodsFromType - {methodInfo.MethodName}");
                        result.Append(e.Message);
                        result.Append("**/");
                    }
                }

                if (!isExcluded)
                {
                    List<string> paramList = new List<string>();

                    result.AppendCR();
                    result.Append("/**");

                    if (methodInfo.Parameters != null)
                    {
                        // add the parameter to list for appending later
                        foreach ((string Name, string ParameterType) paramInfo in methodInfo.Parameters)
                        {
                            result.Append($"* @param {{{paramInfo.ParameterType}}} {paramInfo.Name}");
                            paramList.Add($"{paramInfo.Name}: {paramInfo.ParameterType}");
                        }
                    }

                    result.Append($"* @type {{{methodInfo.ReturnType}}}");

                    result.Append("*/");

                    result.Append($"{prefix}{methodInfo.MethodName}: ( ", addCR: false);


                    try
                    {
                        if (methodInfo.Parameters != null)
                        {
                            result.Append(string.Join(", ", paramList), addCR: false, indent: false);
                            result.Append(" ", addCR: false, indent: false);
                        }
                    }
                    catch (Exception e)
                    {
                        result.AppendCR();
                        result.Append("/**");
                        result.Append("Error Generating Code");
                        result.Append($"FillMethodsFromType - {methodInfo.MethodName}");
                        result.Append(e.Message);
                        result.Append("**/");
                    }

                    result.Append($") => {methodInfo.ReturnType};", indent: false);
                }
            }
        }

        private static string FormatTSName(string assemblyName)
        {
            assemblyName = Regex.Replace(assemblyName, @"[^\w\d]", "");
            assemblyName = (Regex.IsMatch(assemblyName, @"^\d") ? "_" : "") + assemblyName;
            return assemblyName;
        }
        private void FillClass(CodeBuilder result, Type type, Action<CodeBuilder, Type> customCode = null, string excludeNameContains = null, ICBELAssembly assembly = null, string assemblyName = null, string assemblyBaseName = null)
        {
            string typeName;

            if (!string.IsNullOrWhiteSpace(assemblyName))
                typeName = assemblyName;
            else
                typeName = GetTypeName(type);

            result.Append($"class {typeName}", addCR: false);

            Type ancestorType;

            if (!string.IsNullOrWhiteSpace(assemblyBaseName))
                result.Append($" extends {assemblyBaseName}", addCR: false);
            else
            {
                if (type.IsInterface)
                    ancestorType = type.GetInterfaces().FirstOrDefault();
                else
                    ancestorType = type.BaseType;

                if (ancestorType != null && ancestorType != typeof(object))
                    result.Append($" extends {GetTypeName(ancestorType)}", addCR: false);
            }


            result.AppendCR();

            result.Append("{");

            result.IncIndent();

            try
            {
                FillPropertiesFromType(result, type, excludePropertyContains: excludeNameContains, includeInherited: assembly != null, assembly: assembly);
                FillMethodsFromType(result, type, excludeMethodContains: excludeNameContains, includeInherited: assembly != null);

                customCode?.Invoke(result, type);
            }
            catch (Exception e)
            {
                result.Append("/**");
                result.Append("Error Generating Code");
                result.Append($"FillClasses - {type}");
                result.Append(e.Message);
                result.Append("**/");
            }
            result.DecIndent();
            result.Append("}");

            result.AppendCR();

        }

        private void FillClasses(CodeBuilder result, params Type[] types)
        {
            FillClasses(result, null, null, types);
        }

        private void FillClasses(CodeBuilder result, Action<CodeBuilder, Type> customCode = null, string excludeNameContains = null, params Type[] types)
        {
            foreach (Type type in types)
                FillClass(result, type
                          , customCode: customCode
                          , excludeNameContains: excludeNameContains);
        }

        private static void FillEnum(CodeBuilder result, params Type[] enumTypes)
        {
            result.Append("// ======================================================================");
            result.Append("// ======================= UnitType Enum ================================");
            result.Append("// ======================================================================");

            result.AppendCR();
            result.AppendCR();
            result.AppendCR();

            foreach (Type type in enumTypes)
            {
                result.Append($"enum {type.Name}");
                result.Append("{");
                result.IncIndent();

                foreach (object value in Enum.GetValues(type))
                {
                    try
                    {
                        result.Append($"{value.ToString()},");
                    }
                    catch (Exception e)
                    {
                        result.AppendCR();
                        result.Append("/**");
                        result.Append("Error Generating Code");
                        result.Append($"FillEnum - {value.ToString()}");
                        result.Append(e.Message);
                        result.Append("**/");
                    }
                }

                result.DecIndent();
                result.Append("}");
                result.AppendCR();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="append"></param>
        /// <param name="prefix">string inserted in index [0] of class name</param>
        /// <param name="suffix">string inserted in index [last] of class name</param>
        /// <param name="types"></param>
        private static void FillEnumsToClass(CodeBuilder result, Action<CodeBuilder, object> append, string prefix, string suffix, string parentClass, params Type[] types)
        {
            foreach (Type type in types)
            {
                foreach (var value in Enum.GetValues(type))
                {
                    try
                    {
                        var unitType = ((UnitType)value).Info();

                        Regex pattern = new Regex("[()/ ]");
                        string className = pattern.Replace($"{prefix}{unitType.Name}{suffix}", "");

                        if (!string.IsNullOrWhiteSpace(parentClass))
                            className += $" extends {parentClass}";

                        GenerateGenericClass(
                            result,
                            className: className,
                            objectValue: value,
                            customAppend: append
                        );
                    }
                    catch (Exception e)
                    {
                        result.Append("/**");
                        result.Append("Error Generating Code");
                        result.Append($"FillEnumsToClass - {value.ToString()}");
                        result.Append(e.Message);
                        result.Append("**/");
                    }
                }
            }
        }

        private static void GenerateGenericClass<T>(CodeBuilder result, string className, T objectValue, Action<CodeBuilder, T> customAppend = null, bool isUS = true)
        {
            result.Append($"class {className}");

            result.Append("{");

            result.IncIndent();

            try
            {
                customAppend(result, objectValue);
            }
            catch (Exception e)
            {
                result.Append("/**");
                result.Append("Error Generating Code");
                result.Append("GenerateGenericClass");
                result.Append(e.Message);
                result.Append("**/");
            }

            result.DecIndent();
            result.Append("}");

            result.AppendCR();

        }

        private static void FillClassValues(CodeBuilder result, string query, string suffix = "", params Type[] types)
        {
            MethodInfo method = null;
            foreach (Type type in types)
            {
                method = type.GetMethods().FirstOrDefault(p => p.Name.ToLower() == query.ToLower());

                if (method != null)
                    break;
            }

            if (method != null)
            {
                try
                {
                    FillMembersWithValueFromMethod(result, method, prefix: $"let {method.Name}{suffix} : {nameof(CBELFunctionInfo)} =");
                }
                catch (Exception e)
                {
                    result.Append("/**");
                    result.Append("Error Generating Code");
                    result.Append($"FillClassValues - {method.Name}");
                    result.Append(e.Message);
                    result.Append("**/");
                }
            }
        }

        private static void FillClassValuesAsArray(CodeBuilder result, params Type[] types)
        {
            bool hasContent = false;
            foreach (Type type in types)
            {
                foreach (MethodInfo method in type.GetMethods())
                {
                    try
                    {
                        hasContent = FillMembersWithValueFromMethod(result, method, includeStartingComma: hasContent) || hasContent;
                    }
                    catch (Exception e)
                    {
                        result.Append("/**");
                        result.Append("Error Generating Code");
                        result.Append($"FillClassValuesAsArray - {method.Name}");
                        result.Append(e.Message);
                        result.Append("**/");
                    }
                }
            }
            result.AppendCR();
        }
        private static CBELFunctionInfo[] FillFunctionInfosAsArray(params Type[] types)
        {
            List<CBELFunctionInfo> functionInfos = new List<CBELFunctionInfo>();

            foreach (Type type in types)
            {
                foreach (MethodInfo method in type.GetMethods())
                {
                    var info = FillFunctionInfos(method);

                    if(info != null)
                        functionInfos.Add(info);
                }
            }

            return functionInfos.ToArray();
        }

        private void FillDefinitions(CodeBuilder result, Type type)
        {
            try
            {
                FillPropertiesFromType(result, type, "declare var", includeInherited: true);
                FillMethodsFromType(result, type, "declare var", includeInherited: true);
            }
            catch (Exception e)
            {
                result.Append("/**");
                result.Append("Error Generating Code");
                result.Append($"FillDefinitions - {type.Name}");
                result.Append(e.Message);
                result.Append("**/");
            }
        }

        private void FillDefinitions(CodeBuilder result, ICBELAssembly assembly)
        {
            try
            {
                FillPropertiesFromType(result, assembly.GetType(), "declare var", includeInherited: true, assembly: assembly);
                FillMethodsFromType(result, assembly.GetType(), "declare var", includeInherited: true);
            }
            catch (Exception e)
            {
                result.Append("/**");
                result.Append("Error Generating Code");
                result.Append($"FillDefinitions - {assembly.Name}");
                result.Append(e.Message);
                result.Append("**/");
            }
        }

        private void FillVariableDefinitions(CodeBuilder result, List<AssemblyVariableAutocompleteBrief> assemblyVariables, string prefix = "", string excludeContains = "")
        {
            foreach (AssemblyVariableAutocompleteBrief assemblyVariable in assemblyVariables)
            {               

                try
                {
                    FillVariableProperties(result, assemblyVariable as AssemblyVariableAutocompleteBrief, prefix, excludePropertyContains: excludeContains);                    
                }
                catch (Exception e)
                {
                    result.Append("/**");
                    result.Append("Error Generating Code");
                    result.Append("FillVariableDefinitions");
                    result.Append(e.Message);
                    result.Append("**/");
                }

            }
        }

        private Task<CodeBuilder> FillCustomClasses(CodeBuilder result)
        {
            result.Append("// ======================================================================");
            result.Append("// ===================== Custom Field Classes ===========================");
            result.Append("// ======================================================================");
            result.AppendCR();

            try
            {
                FillClasses(result,
                    typeof(ICBELCustomFields)
                );

                IEnumerable<CustomFieldDefinition> customCFs = Context.CustomFieldDefinition.Where(c => c.BID == BID && c.IsActive);
                FillClasses(result,
                    customCode: (code, type) =>
                    {
                        IEnumerable<CustomFieldDefinition> cfs;
                        if (type == typeof(ICBELCompanyCustomFields))
                            cfs = customCFs.Where(x => x.AppliesToClassTypeID == ClassType.Company.ID());
                        else if (type == typeof(ICBELOrderCustomFields))
                            cfs = customCFs.Where(x => x.AppliesToClassTypeID == ClassType.Order.ID());
                        else if (type == typeof(ICBELContactCustomFields))
                            cfs = customCFs.Where(x => x.AppliesToClassTypeID == ClassType.Contact.ID());
                        else if (type == typeof(ICBELOpportunityCustomFields))
                            cfs = customCFs.Where(x => x.AppliesToClassTypeID == ClassType.Opportunity.ID());
                        else if (type == typeof(ICBELEmployeeCustomFields))
                            cfs = customCFs.Where(x => x.AppliesToClassTypeID == ClassType.Employee.ID());
                        else if (type == typeof(ICBELMaterialCustomFields))
                            cfs = customCFs.Where(x => x.AppliesToClassTypeID == ClassType.Material.ID());
                        else if (type == typeof(ICBELLaborCustomFields))
                            cfs = customCFs.Where(x => x.AppliesToClassTypeID == ClassType.Labor.ID());
                        else if (type == typeof(ICBELEstimateCustomFields))
                            cfs = customCFs.Where(x => x.AppliesToClassTypeID == ClassType.Estimate.ID());
                        else
                            cfs = null;

                        if (cfs != null)
                        {
                            foreach (CustomFieldDefinition cf in cfs)
                            {
                                string typeString = cf.DataType.ToString();

                                code.Append($"{cf.Name} : {typeString};");
                            }
                        }

                    },
                    excludeNameContains: null,
                    typeof(ICBELOrderCustomFields),
                    typeof(ICBELContactCustomFields),
                    typeof(ICBELOpportunityCustomFields),
                    typeof(ICBELCompanyCustomFields),
                    typeof(ICBELEmployeeCustomFields),
                    typeof(ICBELMaterialCustomFields),
                    typeof(ICBELLaborCustomFields),
                    typeof(ICBELEstimateCustomFields)
                    );
            }
            catch (Exception e)
            {
                result.Append("/**");
                result.Append("Error Generating Code");
                result.Append("FillCustomClasses");
                result.Append(e.Message);
                result.Append("**/");
            }

            return Task.FromResult(result);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="result"></param>
        /// <param name="prefix">string inserted in index [0] of class name</param>
        /// <param name="suffix">string inserted in index [last] of class name</param>
        private static void FillUnitTypes(CodeBuilder result, string prefix, string suffix, string parentClass)
        {
            FillEnumsToClass(
                result,
                (code, item) =>
                {
                    // custom code to add localized unit names
                    var unitType = (UnitType)item;

                    foreach (var info in unitType.Info().Units)
                    {
                        try
                        {
                            // include localized text
                            string unitNameUS = "", unitNameUK = "", unitNamePluralUS = "", unitNamePluralUK = "";
                            unitNameUS = info.NameUS.ToString().Replace("/", "Per").Replace(" ", "");
                            unitNameUK = info.NameUK.ToString().Replace("/", "Per").Replace(" ", "");
                            unitNamePluralUS = info.NamePluralUS.ToString().Replace("/", "Per").Replace(" ", "");
                            unitNamePluralUK = info.NamePluralUK.ToString().Replace("/", "Per").Replace(" ", "");

                            if (unitNameUS == "None")
                                continue;

                            code.Append($"In{unitNameUS} : number;");

                            if (unitNameUS != unitNameUK)
                                code.Append($"In{unitNameUK} : number;");

                            if (unitNameUS != unitNamePluralUS)
                                code.Append($"In{unitNamePluralUS} : number;");

                            if (unitNameUK != unitNamePluralUK && unitNamePluralUS != unitNamePluralUK)
                                code.Append($"In{unitNamePluralUK} : number;");
                        }
                        catch (Exception e)
                        {
                            result.Append("/**");
                            result.Append("Error Generating Code");
                            result.Append($"FillUnitTypes - {info.NameUS}");
                            result.Append(e.Message);
                            result.Append("**/");
                        }
                    }
                },
                prefix,
                suffix,
                parentClass,
                typeof(UnitType));
        }

        private static List<CBELOperatorInfo> _operators = new List<CBELOperatorInfo>();
        /// <summary>
        /// fill operator variable and returns variable depending on type given
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>default returns type List<CBELOperatorInfo></returns>
        private static T FillOperators<T>() where T : class
        {
            if (_operators.Count < 1)
            {

                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "+",
                        InsertedText = "+",
                        DataType = 99,
                        Hint = "Add two numbers or combine two strings",
                        Description = "The plus sign is used to add two numbers e.g. <noformat>3+4</noformat> or combine two string <noformat>\"Red,White,\" + \" and Blue\"</noformat>"
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "-",
                        InsertedText = "-",
                        DataType = 99,
                        Hint = "Subtract two numbers",
                        Description = "The minus sign is used to subtract two numbers e.g. <noformat>3-4</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "*",
                        InsertedText = "*",
                        DataType = 99,
                        Hint = "Multiplies two numbers",
                        Description = "The multiplication sign is used to multiply two numbers e.g. <noformat>3*4</noformat>"
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "/",
                        InsertedText = "/",
                        DataType = 99,
                        Hint = "Divides two numbers",
                        Description = "The obelus sign is used to divide two numbers e.g. <noformat>3/4</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "^",
                        InsertedText = "^",
                        DataType = 99,
                        Hint = "Exponentiates the first number by the second number",
                        Description = "The caret sign is used as the exponentiation operator e.g. <noformat>3^4</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "%",
                        InsertedText = "%",
                        DataType = 99,
                        Hint = "Indicates a percentage",
                        Description = "The percent sign is the symbol used to indicate a percentage, a number or ratio as a fraction of 100 e.g. <noformat>90%</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "(",
                        InsertedText = "(||)",
                        DataType = 99,
                        Hint = "Indicates that an expression will be inserted after the open parenthesis and a vertical bar",
                        Description = "The open parethesis sign is used when inserting a variable. It will insert two vertical bars (pipes) and a closing parenthesis to indicate that the user can insert an expression between the vertical bars e.g. <noformat>(|VALUE|)</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = ")",
                        InsertedText = ")",
                        DataType = 99,
                        Hint = "Indicates that an expression is being ended",
                        Description = "The close parenthesis sign is used when ending an expression e.g. <noformat>(|VALUE|)</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = ">",
                        InsertedText = ">",
                        DataType = 99,
                        Hint = "Check if left expression is greater than the right expression",
                        Description = "The greater than sign is used when comparing two expressions. A condition to check whether left expression is  greater than the right expression e.g. <noformat>3>4</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "<",
                        InsertedText = "<",
                        DataType = 99,
                        Hint = "Check if left expression is less than the right expression",
                        Description = "The less than sign is used when comparing two expressions. A condition to check whether left expression is  less than the right expression e.g. <noformat>3<4</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "=",
                        InsertedText = "=",
                        DataType = 99,
                        Hint = "Sets the value",
                        Description = "The equal sign is used to store the right expression to the left. Makes left's expression equal to right's expression e.g. <noformat>variable=4</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = ">=",
                        InsertedText = ">=",
                        DataType = 99,
                        Hint = "Check if left expression is greater or equal than the right expression",
                        Description = "The greater than or equal sign is used when comparing two expressions. A condition to check whether left expression is greater than or equal to the right expression e.g. <noformat>3>=4</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "<>",
                        InsertedText = "<>",
                        DataType = 99,
                        Hint = "Checks if left expression is not equal to the right expression",
                        Description = "The not equal sign is used when comparing two expressions. A condition to check whether left expression is not equal to the right expression e.g. <noformat>3<>4</noformat> "
                    });
                _operators.Add(
                    new CBELOperatorInfo()
                    {
                        Name = "<=",
                        InsertedText = "<=",
                        DataType = 99,
                        Hint = "Check if left expression is less than or equal the right expression",
                        Description = "The less than or equal sign is used when comparing two expressions. A condition to check whether left expression is less than or equal to the right expression e.g. <noformat>3<=4</noformat> "
                    });
            }

            if(typeof(T) == typeof(string))
            {
                return ListToString<CBELOperatorInfo>(
                    _operators,
                    typeof(CBELOperatorInfo),
                    declareName: "FunctionInfoList",
                    declareType: "CBELFunctionInfo[]"
                ) as T;
            }
            else
            {
                return _operators.ToArray() as T;
            }
        }

        private static string ListToString<T>(List<T> list, Type type, string declareName = "", string declareType = "")
        {
            CodeBuilder result = new CodeBuilder();

            result.Append($"let {declareName}: {declareType} = [");
            result.IncIndent();

            foreach (T item in list)
            {
                result.Append("{");
                result.IncIndent();

                foreach (PropertyInfo property in item.GetType().GetProperties())
                {
                    if(property.PropertyType == typeof(string))
                        result.Append($"{property.Name} : \"{property.GetValue(item)}\",");

                    else if (property.PropertyType == typeof(bool))
                        result.Append($"{property.Name} : {property.GetValue(item).ToString().ToLower()},");

                    else
                        result.Append($"{property.Name} : {property.GetValue(item)},");
                }

                foreach (MethodInfo method in item.GetType().GetMethods().Where(x => !x.IsSpecialName && x.DeclaringType == type))
                {
                    if (method.ReturnType == typeof(string))
                    {
                        result.Append($"{method.Name}()", addCR: false);
                        result.Append("{", addCR: false);
                        result.Append($"return \"{method.Invoke(item, null)}\";", addCR: false);
                        result.Append("},");
                    }
                    else if (method.ReturnType == typeof(bool))
                    {
                        string boolValue = ((bool)method.Invoke(item, null) == true) ? "true" : "false";

                        result.Append($"{method.Name}()", addCR: false);
                        result.Append("{", addCR: false);
                        result.Append($"return {boolValue};", addCR: false);
                        result.Append("},");
                    }
                    else
                    {
                        result.Append($"{method.Name}()", addCR: false);
                        result.Append("{", addCR: false);
                        result.Append($"return {method.Invoke(item, null)};", addCR: false);
                        result.Append("},");
                    }
                }

                result.DecIndent();
                result.Append("},");
            }

            result.DecIndent();
            result.Append($"] as {declareType};");
            

            return result.ToString();
        }
        // for unit test
    }
}
