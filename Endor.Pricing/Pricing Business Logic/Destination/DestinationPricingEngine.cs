﻿using System;

namespace Endor.Pricing
{
    public static class DestinationPricingEngine
    {
        public static DestinationPriceResult Compute(DestinationPriceRequest request)
        {
            // Switch the pricing engine based on the Destination Engine Type
            // 
            switch (request.EngineType)
            {
                case DestinationEngineType.Other:
                    return DestinationPricingEngine_Other.Compute(request);

                case DestinationEngineType.Pickedup:
                    return DestinationPricingEngine_Pickedup.Compute(request);

                case DestinationEngineType.ElectronicDelivery:
                    return DestinationPricingEngine_ElectronicDelivery.Compute(request);

                case DestinationEngineType.Delivered:
                    return DestinationPricingEngine_Delivered.Compute(request);

                case DestinationEngineType.Installed:
                    return DestinationPricingEngine_Installed.Compute(request);

                case DestinationEngineType.OnsiteService:
                    return DestinationPricingEngine_OnsiteService.Compute(request);

                case DestinationEngineType.Shipped:
                    return DestinationPricingEngine_Shipped.Compute(request);

                default:
                    throw new InvalidOperationException("Unknown Destination Pricing Engine Specified.");
            }
        }
    }
}
