﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Pricing
{
    public enum DestinationEngineType
    {
        Other = 0,
        Pickedup = 1,
        ElectronicDelivery = 2,
        Delivered = 3,
        Installed = 4,
        OnsiteService = 5,
        Shipped = 11
    }
}
