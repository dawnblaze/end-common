﻿using Endor.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;


namespace Endor.Pricing
{
    public class DestinationPriceResult
    {
        // Dictionary for Variables (user input and computed data fields)
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, VariableValue> Variables { get; set; }

        // Dictionaries to hold ALL Parts (added or computed)
        // public Dictionary<string, Material> Materials { get; set; }
        // public Dictionary<string, Labor> Labor { get; set; }
        // public Dictionary<string, Machine> Machine { get; set; }

        // List of Tax Assessment Results
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TaxAssessmentResult> TaxInfoList { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ComputedNet { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceNet { get; set; }

        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool PriceNetOV { get; set; }

        public decimal? PriceDiscountPercent { get; set; }
        public decimal? PriceDiscountAmount { get; set; }

        // Discount Amount
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceDiscount
        {
            get
            {
                decimal? itemDiscount;

                if (!PriceDiscountPercent.HasValue)
                    itemDiscount = 0;
                else if (PriceNet.HasValue)
                    itemDiscount = decimal.Round((PriceDiscountPercent.Value / 100m) * PriceNet.Value, 2);
                else
                    itemDiscount = null;

                if (PriceDiscountAmount.HasValue)
                    itemDiscount += PriceDiscountAmount.Value;

                return itemDiscount;
            }
        }

        /// <summary>
        /// The computed pretax value ignoring any overrides.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ComputedPreTax { get { return (ComputedNet - PriceDiscount.GetValueOrDefault(0m)); } }

        /// <summary>
        /// The pretax price for the order, after discounts are applied.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax { get { return (PriceNet - PriceDiscount.GetValueOrDefault(0m)); } }

        /// <summary>
        /// The amount of sales tax for the line item.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The total price for the order, computed as PricePreTax + TaxAmount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTotal { get { return (PricePreTax + TaxAmount); } }

        // Some additional data fields for computation
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }
    }

    internal class DestinationPriceTotals
    {
        public DestinationPriceTotals()
        {
            ComputedNet = 0m;
            PriceNet = 0m;
            PriceDiscount = 0m;
            TaxAmount = 0m;
        }

        public decimal? ComputedNet { get; set; }
        public decimal? PriceNet { get; set; }
        public decimal? PriceDiscount { get; set; }
        public decimal? TaxAmount { get; set; }

        public List<TaxAssessmentResult> TaxInfoList { get; set; }

        public List<DestinationPriceResult> DestinationPriceResults { get; private set; } //results for each line item

        internal void AddDestination(DestinationPriceResult destinationPriceResult)
        {
            if (DestinationPriceResults == null)
                DestinationPriceResults = new List<DestinationPriceResult>();

            DestinationPriceResults.Add(destinationPriceResult);

            ComputedNet += destinationPriceResult.ComputedNet;
            PriceNet += destinationPriceResult.PriceNet;
            PriceDiscount += destinationPriceResult.PriceDiscount;
            TaxAmount += destinationPriceResult.TaxAmount;

            if (destinationPriceResult.TaxInfoList != null && destinationPriceResult.TaxInfoList.Count > 0)
            {
                if (TaxInfoList == null)
                    TaxInfoList = new List<TaxAssessmentResult>();

                TaxInfoList.AddRange(destinationPriceResult.TaxInfoList);
            }
        }
    }
}
