﻿using System;

namespace Endor.Pricing
{
    static class DestinationPricingEngine_Pickedup
    {
        public static DestinationPriceResult Compute(DestinationPriceRequest request)
        {
            DestinationPriceResult result = request.Result;

            result.CompanyID = request.CompanyID;
            result.ComputedNet = 0m;

            return result;
        }
    }
}
