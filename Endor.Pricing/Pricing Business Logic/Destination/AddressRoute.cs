﻿namespace Endor.Pricing
{
    public class AddressRoute
    {
        public Address FromAddress { get; set; }

        public bool UseSameToFromAddress
        {
            get { return UseSameToFromAddress; }
            set
            {
                if (value)
                    ToAddress = null;
                UseSameToFromAddress = value;
            }
        }

        public Address ToAddress
        {
            get
            {
                return (UseSameToFromAddress ? FromAddress : ToAddress);
            }

            set
            {
                UseSameToFromAddress = (value == FromAddress);

                if (!UseSameToFromAddress)
                    ToAddress = value;
            }
        }
    }
}