﻿using Endor.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


namespace Endor.Pricing
{
    public class DestinationPriceRequest
    {
        /// <summary>
        /// The TransactionType of the price request.
        /// </summary>
        [DefaultValue((byte)OrderTransactionType.Order)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public byte TransactionType { get; set; } = (byte)OrderTransactionType.Order;


        // Specifies the type of destination
        public DestinationEngineType EngineType { get; set; }

        // Dictionary for Variables (user input and computed data fields)
        public Dictionary<string, VariableValue> Variables { get; set; }

        // Dictionary to hold Parts
        // public Dictionary<string, Material> Materials { get; private set; }
        // public Dictionary<string, Labor> Labor { get; private set; }
        // public Dictionary<string, Machine> Machine { get; private set; }

        // The Tax Group associated with a Destination.  A destination can have only one Tax Group.
        public TaxAssessmentRequest TaxInfo { get; set; }

        /// <summary>
        /// A List of Tax Nexus that apply to the Destination.  This should only be supplied when 
        /// a single destination is being recomputed.  When recomputing the entire order, this value
        /// is filled in from the Order-Level property of the same name.
        /// </summary>
        public Dictionary<string, TaxAssessmentNexus> TaxNexusList { get; set; }

        public decimal Quantity { get; set; }

        // Only send the Net price if Overridden
        //      PriceNetOV is set to true if PriceNet != null
        public decimal? PriceNet { get; set; }

        public decimal? PriceDiscountPercent { get; set; }
        public decimal? PriceDiscountAmount { get; set; }

        // Some additional data fields for computation
        public int? CompanyID { get; set; }

        /// <summary>
        /// A place holder for the post-calculated results.
        /// </summary>
        [JsonIgnore]
        internal DestinationPriceResult Result { get; set; }
    }
}
