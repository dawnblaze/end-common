﻿using System;

namespace Endor.Pricing
{
    public class Address : IComparable
    {

        public string County { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Street { get; set; }


        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
    }
}