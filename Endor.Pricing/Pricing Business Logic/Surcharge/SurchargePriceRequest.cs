﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.ComponentModel;

namespace Endor.Pricing
{
    /// <summary>
    /// The SurchargePriceRequest is used to request the pricing for a Setup Charge.
    /// </summary>
    public class SurchargePriceRequest
    {
        private decimal? _quantity;

        /// <summary>
        /// The Surcharge ID
        /// </summary>
        public short SurchargeDefID { get; set; }

        /// <summary>
        /// The Quantity of the Component.  Null is treated as 0 for computation purposes.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Quantity
        {
            get => _quantity ?? 0m;
            set => _quantity = value;
        }

        /// <summary>
        /// The Taxability Code of the Surcharge.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TaxabilityCode { get; set; }
        /// <summary>
        /// The fixed amount of the surcharge.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceFixedAmount { get; set; }
        /// <summary>
        /// The per-unit amount of the surcharge.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePerUnitAmount { get; set; }
        /// <summary>
        /// The PreTax Price for the Surcharge
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax { get; set; }
        /// <summary>
        /// Flag indicating if the PreTax Price for the Surcharge is Overridden.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? PricePreTaxOV { get; set; }
        /// <summary>
        /// A Dictionary of (NexusID, Nexus) that apply (or might apply) to the line item.
        /// This is set automatically by the Order when the order is computed, but must be passed in by the caller when a line item is computed by itself.
        /// Required for tax computations when the order is not tax exempt.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, TaxAssessmentNexus> TaxNexusList { get; set; }
        /// <summary>
        /// TaxInfoList
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TaxAssessmentRequest> TaxInfoList { get; set; }
        /// <summary>
        /// CompanyID
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }
        /// <summary>
        /// The result property stores an internal SurchargePriceResult record.  This is used to store interim and final results in the computation.
        /// </summary>
        [JsonIgnore]
        internal SurchargePriceResult Result { get; set; }
    }
}
