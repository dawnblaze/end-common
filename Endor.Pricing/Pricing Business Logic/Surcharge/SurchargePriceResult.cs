﻿using Endor.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Endor.Pricing
{
    /// <summary>
    /// The SurchargePriceResult is returned to the client from the SurchargePriceRequest.
    /// </summary>
    public class SurchargePriceResult
    {
        private decimal? _quantity;

        /// <summary>
        /// The Quantity of the Surcharges.  Null is treated as 0 for computation purposes.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Quantity
        {
            get => _quantity ?? 0m;
            set => _quantity = value;
        }

        /// <summary>
        /// The Surcharge ID
        /// </summary>
        public short SurchargeDefID { get; set; }

        /// <summary>
        /// The total price of the Surcharge.  
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax { get; set; }
        /// <summary>
        /// Flag indicating if the PreTax Price for the Surcharge is Overridden.
        /// </summary>
        public bool PricePreTaxOV { get; set; }

        /// <summary>
        /// Discounts applied from line item
        /// </summary>
        public decimal PriceAppliedDiscount { get; set; }

        /// <summary>
        /// Taxable Price
        /// </summary>
        public decimal PriceTaxable => PricePreTax.GetValueOrDefault(0m) - PriceAppliedDiscount;

        /// <summary>
        /// A Dictionary of(NexusID, Nexus) that apply(or might apply) to the line item.
        /// This is set automatically by the Order when the order is computed, but must be passed in by the caller when a line item is computed by itself.
        /// Required for tax computations when the order is not tax exempt.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, TaxAssessmentNexus> TaxNexusList { get; set; }
        /// <summary>
        /// A (compressed) collection of all of the Tax Assessments applied to line items and destinations, totaled by Tax Assessment Item.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TaxAssessmentResult> TaxInfoList { get; set; }
        /// <summary>
        /// CompanyID
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }
    }
}
