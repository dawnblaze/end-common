﻿using Endor.CBEL.Metadata;
using System;
using System.Collections.Generic;

namespace Endor.CBEL.Autocomplete
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public sealed class CBELFunctionInfoAttribute : Attribute
    {
        public CBELFunctionInfoAttribute() { }

        public string Name { get; set; }
        public string InsertedText { get; set; }
        public int DataType { get; set; }
        public string Hint { get; set; }
        public string Description { get; set; }
        public string LongDescription { get; set; }
        public int MinParams { get; set; }
        public int MaxParams { get; set; }

        private readonly List<CBELFunctionParamInfo> paramInfos = new List<CBELFunctionParamInfo>();
        public CBELFunctionParamInfo[] Params => paramInfos.Count == 0 ? null : paramInfos.ToArray();

        public string Param1Name 
        {
            get => null;
            set
            {
                while (paramInfos.Count < 1)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[0].Name = value;
            }
        }
        public int Param1DataType
        {
            get => 0;
            set
            {
                while (paramInfos.Count < 1)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[0].DataType = value;
            }
        }
        public bool Param1IsRequired
        {
            get => false;
            set
            {
                while (paramInfos.Count < 1)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[0].Required = value;
            }
        }
        public string Param1Hint
        {
            get => null;
            set
            {
                while (paramInfos.Count< 1)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[0].Hint = value;
            }
        }

        public string Param2Name
        {
            get => null;
            set
            {
                while (paramInfos.Count < 2)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[1].Name = value;
            }
        }
        public int Param2DataType
        {
            get => 0;
            set
            {
                while (paramInfos.Count < 2)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[1].DataType = value;
            }
        }
        public bool Param2IsRequired
        {
            get => false;
            set
            {
                while (paramInfos.Count < 2)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[1].Required = value;
            }
        }
        public string Param2Hint
        {
            get => null;
            set
            {
                while (paramInfos.Count < 2)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[1].Hint = value;
            }
        }

        public string Param3Name
        {
            get => null;
            set
            {
                while (paramInfos.Count < 3)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[2].Name = value;
            }
        }
        public int Param3DataType
        {
            get => 0;
            set
            {
                while (paramInfos.Count < 3)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[2].DataType = value;
            }
        }
        public bool Param3IsRequired
        {
            get => false;
            set
            {
                while (paramInfos.Count < 3)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[2].Required = value;
            }
        }
        public string Param3Hint
        {
            get => null;
            set
            {
                while (paramInfos.Count < 3)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[2].Hint = value;
            }
        }

        public string Param4Name
        {
            get => null;
            set
            {
                while (paramInfos.Count < 4)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[3].Name = value;
            }
        }
        public int Param4DataType
        {
            get => 0;
            set
            {
                while (paramInfos.Count < 4)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[3].DataType = value;
            }
        }
        public bool Param4IsRequired
        {
            get => false;
            set
            {
                while (paramInfos.Count < 4)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[3].Required = value;
            }
        }
        public string Param4Hint
        {
            get => null;
            set
            {
                while (paramInfos.Count < 4)
                    paramInfos.Add(new CBELFunctionParamInfo());

                paramInfos[3].Hint = value;
            }
        }
    }
}