﻿using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace Endor.Pricing
{
    public class OrderPriceRequest
    {
        /// <summary>
        /// The List of Order Items to be Priced/Summed
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ItemPriceRequest> Items { get; set; }

        /// <summary>
        /// The List of Destinations to be Prices/Summed
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<DestinationPriceRequest> Destinations { get; set; }

        /// <summary>
        /// A List of Tax Nexus that apply (or might apply) to the order.  This is used for all line items and destinations
        /// and does not need to be supplied at those levels if supplied at the Order-Level.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, TaxAssessmentNexus> TaxNexusList { get; set; }

        /// <summary>
        /// A list of Order-Level Discounts that can be applied to the order.  
        /// Each discount may have it's own logic or conditions for applicability.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<OrderDiscount> DiscountList { get; set; }

        /// <summary>
        /// Order Level Finance Charge (positive) or Early Payment Discounts (negative) to Apply.  
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? FinanceCharge { get; set; }

        /// <summary>
        /// A place holder for the post-calculated results.
        /// </summary>
        [JsonIgnore]
        internal OrderPriceResult Result { get; set; }
    }
}
