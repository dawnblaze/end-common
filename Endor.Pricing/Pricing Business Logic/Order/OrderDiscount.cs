﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Endor.Pricing
{
    public class OrderDiscount
    {
        /// <summary>
        /// The name of the discount.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Amount of the Discount (e.g., 5.00 for $5) or the Percent of the discount (e.g.  10.00 for 10%)
        /// </summary>
        public decimal Discount { get; set; }

        /// <summary>
        /// Flag indicating if the specified Discount is a currency amount (when false) or a percent (when true).
        /// </summary>
        public bool IsPercentBased { get; set; }
    }
}
