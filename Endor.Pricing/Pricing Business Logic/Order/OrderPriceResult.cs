﻿using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Endor.Pricing
{
    /// <summary>
    /// Data structure to hold the result of an Order Price Computation.
    /// </summary>
    public class OrderPriceResult
    {
        /// <summary>
        /// Create an OrderPriceResult using the OrderPriceRequest, ItemPriceResult, and DestinationPriceResult as a starting template.
        /// </summary>
        /// <param name="request"></param>
        internal OrderPriceResult(OrderPriceRequest request, ItemPriceTotals itemTotal, DestinationPriceTotals destTotal)
        {
            Items = itemTotal.ItemPriceResults;
            Destinations = destTotal.DestinationPriceResults;

            if (itemTotal.TaxInfoList != null || destTotal.TaxInfoList != null)
            {
                List<TaxAssessmentResult> tempTaxInfoList = new List<TaxAssessmentResult>();

                if (itemTotal.TaxInfoList != null)
                    tempTaxInfoList.AddRange(itemTotal.TaxInfoList);

                if (destTotal.TaxInfoList != null)
                    tempTaxInfoList.AddRange(destTotal.TaxInfoList);

                TaxInfoList = tempTaxInfoList.Compress();

                if (TaxInfoList.Count == 0)
                    TaxInfoList = null;
            }

            TaxAmount = itemTotal.TaxAmount + destTotal.TaxAmount;
        }

        /// <summary>
        /// Intializes an empty Order Price Result, For Testing purposes
        /// </summary>
        public OrderPriceResult() { }

        /// <summary>
        /// List of the resulting OrderItem price results.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ItemPriceResult> Items { get; set; }

        /// <summary>
        /// List of the resulting Destination price results.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<DestinationPriceResult> Destinations { get; set; }

        /// <summary>
        /// List of the resulting order-level taxes.  
        /// These taxes are computed by combining all of the item-level and destination-level taxes.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TaxAssessmentResult> TaxInfoList { get; set; }

        /// <summary>
        /// The total pre-tax price for the Order Items = SUM( Items.PricePreTax )
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ItemPreTaxTotal
        {
            get
            {
                if (Items == null)
                    return 0m;

                return Items.Sum(item => item.PricePreTax);
            }
        }

        /// <summary>
        /// The total pre-tax price for the Destination Items = SUM( Destinations.PricePreTax )
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? DestPreTaxTotal
        {
            get
            {
                if (Destinations == null)
                    return 0m;

                return Destinations.Sum(item => item.PricePreTax);
            }
        }
        /// <summary>
        /// The total of any finance charges (positive) or early payment discounts (negative) applied to the order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? FinanceCharge { get; set; }

        /// <summary>
        /// The Net Price for an order is the sum of all the item and distination pricing, less the applied order level discounts
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceNet
        {
            get
            {
                return ItemPreTaxTotal 
                    + DestPreTaxTotal 
                    + FinanceCharge.GetValueOrDefault(0m) 
                    + PriceOrderDiscountTotal.GetValueOrDefault(0m);
            }
        }

        /// <summary>
        /// The total of all applied order-level discounts.  This is already deducted from the ItemTotal.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceOrderDiscountTotal
        {
            get
            {
                if (Items == null)
                    return 0m;

                return Items.Sum(item => item.AppliedOrderDiscountAmount.GetValueOrDefault(0m));
            }
        }
        /// <summary>
        /// The pretax price for the order, after discounts are applied.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax
        {
            get
            {
                return PriceNet - PriceOrderDiscountTotal.GetValueOrDefault(0m);
            }
        }

        /// <summary>
        /// The taxable amount of the order = SUM( Item.TaxablePrice ) + SUM( Dest.TaxablePrice )
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTaxable { get; set; }

        /// <summary>
        /// The amount of sales tax for the order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The total price for the order, computed as PricePreTax + TaxAmount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTotal => (PricePreTax + TaxAmount);

        /// <summary>
        /// The cost of all of the materials included in the order = Sum(LineItem.CostMaterial)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMaterial => Items?.Where(i => i.CostMaterial != null).Sum(i => i.CostMaterial);

        /// <summary>
        /// The cost of all of the Labor included in the order = Sum(LineItem.CostLabor)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostLabor => Items?.Where(i => i.CostLabor != null).Sum(i => i.CostLabor);

        /// <summary>
        /// The cost of all of the Machine included in the order = Sum(LineItem.CostMachine)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMachine => Items?.Where(i => i.CostMachine != null).Sum(i => i.CostMachine);

        /// <summary>
        /// The cost of all of the component in the order = Sum(LineItem.CostTotal)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostTotal => Items?.Where(i => i.CostTotal != null).Sum(i => i.CostTotal);
    }
}
