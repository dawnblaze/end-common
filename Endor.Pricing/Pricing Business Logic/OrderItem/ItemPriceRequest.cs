﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.ComponentModel;
using Endor.Models;

namespace Endor.Pricing
{
    public class ItemPriceRequest
    {
        
        /// <summary>
        /// The TransactionType of the price request.
        /// </summary>
        [DefaultValue((byte)OrderTransactionType.Order)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)] 
        public byte TransactionType { get; set; } = (byte)OrderTransactionType.Order;

        /// <summary>
        /// The pricing engine type for this line item.
        /// </summary>
        public PricingEngineType EngineType { get; set; }

        /// <summary>
        /// Quantity
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// A flag indicating if the SinglePart is outsourced.
        /// </summary>
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool IsOutsourced { get; set; }

        /// <summary>
        /// A list of Component Price Request
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ComponentPriceRequest> Components { get; set; }

        /// <summary>
        /// A list of Surcharge Price Requests
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<SurchargePriceRequest> Surcharges { get; set; }

        /// <summary>
        /// If the Price Discount Percentage is set, the Discount Amount is always computed.
        /// If the Price Discount Percentage is not set and the Discount Amount have a value, that is a manually entered discount amount.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ItemDiscountPercent { get; set; }

        /// <summary>
        /// If the Price Discount Percentage is set, the Discount Amount is always computed.
        /// If the Price Discount Percentage is not set and the Discount Amount have a value, that is a manually entered discount amount.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ItemDiscountAmount { get; set; }

        /// <summary>
        /// If UnitPriceOV is true, this is the overridden unit price.  Otherwise the Unit Price is calculated
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// Unit Price is overridden.  When null, the unit price is not overridden
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? UnitPriceOV { get; set; }


        /// <summary>
        /// A List of Tax Nexus that apply to the Line Item.  This should only be supplied when 
        /// a single line item is being recomputed.  When recomputing the entire order, this value
        /// is filled in from the Order-Level property of the same name.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Dictionary<string, TaxAssessmentNexus> TaxNexusList { get; set; }

        /// <summary>
        /// A list of the tax Groups (associated with Destinations, but that information isn't needed here)
        /// and the number of items subject to each tax group.
        /// SUM( Taxes.TaxQuantity ) must equal Quantity.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TaxAssessmentRequest> TaxInfoList { get; set; }

        /// <summary>
        /// CompanyID
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }

        /// <summary>
        /// A place holder for the post-calculated results.
        /// </summary>
        [JsonIgnore]
        internal ItemPriceResult Result { get; set; }
    }
}