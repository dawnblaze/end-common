﻿using Endor.CBEL.Common;
using Endor.CBEL.Interfaces;
using Endor.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel;

namespace Endor.Pricing
{
    public class ItemPriceResult : ICompilationResponse
    {
        /// <summary>
        /// A (compressed) collection of all of the Tax Assessments applied to line items and destinations, totaled by Tax Assessment Item.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<TaxAssessmentResult> TaxInfoList { get; set; }

        /// <summary>
        /// A list of Component Price Results
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<ComponentPriceResult> Components { get; set; }
        /// <summary>
        /// The total of all the Components on the Order
        /// </summary>

        public decimal? PriceComponent { get; set; }
        /// <summary>
        /// Boolean Flag indicating if any of the Labor, Materials, or Machine prices or quantities were overidden.
        /// Overrridding variable values in Assemblies or Machines does not count as overrridden for purposes here.
        /// </summary>
        public bool PriceComponentOV { get; set; }

        /// <summary>
        /// A list of Surcharge Price Results
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<SurchargePriceResult> Surcharges { get; set; }
        /// <summary>
        /// The total of all the Surcharges on the Order
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceSurcharge { get; set; }
        /// <summary>
        /// Boolean Flag indicating if any of the Surcharge elements were manually entered or overridden.
        /// </summary>
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool PriceSurchargeOV { get; set; }
        /// <summary>
        /// The Price before any Discounts or Taxes
        /// ( = PriceComponent + PriceSurcharge ).
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceList { get { return (!PriceComponent.HasValue && UnitPriceOV ? 0m : PriceComponent) + PriceSurcharge; } }

        /// <summary>
        /// Unit Price
        /// </summary>
        public decimal? UnitPrice { get; set; }

        /// <summary>
        /// Unit Price is overridden.
        /// </summary>
        public bool UnitPriceOV { get; set; }

        /// <summary>
        /// If the Price Discount Percentage is set, the Discount Amount is always computed. 
        /// If the Price Discount Percentage is not set and the Discount Amount have a value, 
        /// that is a manually entered discount amount.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ItemDiscountPercent { get; set; }
        /// <summary>
        /// If the Price Discount Percentage is set, the Discount Amount is always computed. 
        /// If the Price Discount Percentage is not set and the Discount Amount have a value, 
        /// that is a manually entered discount amount.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ItemDiscountAmount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AppliedOrderDiscountPercent { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AppliedOrderDiscountAmount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AppliedItemDiscountAmount { get; set; }

        /// <summary>
        /// The PriceDiscount is the combination of the Item Discount and the AppliedOrder Discount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceDiscount 
        { 
            get 
            {
                decimal? percentAmount = 0m;
                if (ItemDiscountPercent.GetValueOrDefault(0m) != 0m)
                {
                    if (PriceList.HasValue)
                        percentAmount = decimal.Round(PriceList.Value * ItemDiscountPercent.Value, 2);
                    else
                        return null;
                }
                        
                return percentAmount + ItemDiscountAmount + AppliedOrderDiscountAmount; 
            } 
        }

        /// <summary>
        /// The pretax price for the order, after discounts are applied.
        /// ( = PriceList - PriceDiscount )
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax { get { return PriceList - PriceDiscount; } }

        /// <summary>
        /// The amount of sales tax for the line item.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TaxAmount { get; set; }

        /// <summary>
        /// The total price for the order, computed as PricePreTax + TaxAmount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTotal { get { return PricePreTax + TaxAmount; } }

        /// <summary>
        /// Company ID
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }

        /// <summary>
        /// The cost of all of the materials included in the order = Sum(LineItem.CostMaterial)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMaterial { get; set; }
        /// <summary>
        /// The cost of all of the Labor included in the order = Sum(LineItem.CostLabor)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostLabor { get; set; }
        /// <summary>
        /// The cost of all of the Machine included in the order = Sum(LineItem.CostMachine)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMachine { get; set; }
        /// <summary>
        /// The cost of all of the component in the order = Sum(LineItem.CostTotal)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostTotal { get; set; }
        
        public int ErrorCount { get; set; }
        
        public bool Success => ErrorCount == 0;
    }

    internal class ItemPriceTotals
    {
        public ItemPriceTotals()
        {
            ComputedNet = 0m;
            PriceNet = 0m;
            ItemDiscountAmount = 0m;
            AppliedOrderDiscountAmount = 0m;
            TaxAmount = 0m;
        }

        public decimal? ComputedNet { get; set; }
        public decimal? PriceNet { get; set; }
        public decimal? ItemDiscountAmount { get; set; }
        public decimal? AppliedOrderDiscountAmount { get; set; }
        public decimal? TaxAmount { get; set; }
        public List<TaxAssessmentResult> TaxInfoList { get; set; }

        public List<ItemPriceResult> ItemPriceResults { get; private set; }

        internal void AddItem(ItemPriceResult itemPriceResult)
        {
            if (ItemPriceResults == null)
                ItemPriceResults = new List<ItemPriceResult>();

            ItemPriceResults.Add(itemPriceResult);

            //ComputedNet += itemPriceResult.ComputedNet;
            //PriceNet += itemPriceResult.PriceNet;
            ItemDiscountAmount += itemPriceResult.ItemDiscountAmount;
            AppliedOrderDiscountAmount += itemPriceResult.AppliedOrderDiscountAmount;
            TaxAmount += itemPriceResult.TaxAmount;

            if (itemPriceResult.TaxInfoList != null && itemPriceResult.TaxInfoList.Count > 0)
            {
                if (TaxInfoList == null)
                    TaxInfoList = new List<TaxAssessmentResult>();

                TaxInfoList.AddRange(itemPriceResult.TaxInfoList);
            }
        }
    }
}

