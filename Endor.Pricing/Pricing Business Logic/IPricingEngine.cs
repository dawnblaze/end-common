﻿using System.Threading.Tasks;

namespace Endor.Pricing
{
    public interface IPricingEngine
    {
        Task<OrderPriceResult> Compute(OrderPriceRequest request, bool ComputeTaxes = true);

        Task<ItemPriceResult> Compute(ItemPriceRequest request, bool ComputeTaxes = true);

        DestinationPriceResult Compute(DestinationPriceRequest request, bool ComputeTaxes = true);
    }
}
