﻿using Endor.EF;
using Endor.Models;
using Endor.Tenant;
using Endor.Logging.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Pricing
{
    public class PricingEngine : IPricingEngine
    {
        public PricingEngine(short bid, ApiContext context, ITenantDataCache cache, RemoteLogger logger)
        {
            this.BID = bid;
            this.Context = context;
            this.Cache = cache;
            this.Logger = logger;
        }

        private short BID { get; set; }
        private ApiContext Context { get; set; }
        private ITenantDataCache Cache { get; set; }
        private RemoteLogger Logger { get; set; }

        public async Task<OrderPriceResult> Compute(OrderPriceRequest request, bool ComputeTaxes = true)
        {
            ItemPriceTotals itemTotal = new ItemPriceTotals();
            DestinationPriceTotals destTotal = new DestinationPriceTotals(); 
            
            try
            {                

                // Set a threshold for parallel computation ... if small enough, the overhead is higher than the savings.
                bool ComputeParallel = ((request?.Items?.Count ?? 0) > 3) || ((request?.Destinations?.Count ?? 0) > 3);

                // figure out if we'll have to adjust the order after the computation.  If so, skip the taxes until
                // afterwards since the items must be adjusted after computation.
                bool AdjustForOrderDiscounts = ((request.DiscountList?.Count ?? 0) > 0);

                // We want to parallelize the operation if we have a lot of line items
                // but if fewer that that avoid the thread cost
                if (!ComputeParallel)
                {
                    if (request.Items != null)
                    {
                        foreach (ItemPriceRequest item in request.Items)
                        {
                            if (item.TaxNexusList == null)
                                item.TaxNexusList = request.TaxNexusList;

                            await Compute(item, ComputeTaxes && !AdjustForOrderDiscounts);
                            itemTotal.AddItem(item.Result);
                        }
                    }

                    if (request.Destinations != null)
                    {
                        foreach (DestinationPriceRequest dest in request.Destinations)
                        {
                            if (dest.TaxNexusList == null)
                                dest.TaxNexusList = request.TaxNexusList;

                            Compute(dest, ComputeTaxes);
                            destTotal.AddDestination(dest.Result);
                        }
                    }
                }
                // run parallel
                else if (request.Items != null && request.Destinations != null)
                {
                    // We can recompute the totals, but adding to itemTotal is not threadsafe so we need to lock for that.
                    // Also, we can do the OrderItem and Destination computation in parallel, in addition to each of its children

                    Parallel.Invoke(
                        // Task 1 - Compute the Line Items
                        () => Parallel.ForEach(request.Items.Cast<ItemPriceRequest>(), async (item) =>
                        {
                            if (item.TaxNexusList == null)
                                item.TaxNexusList = request.TaxNexusList;

                            await Compute(item, ComputeTaxes && !AdjustForOrderDiscounts);
                            lock (itemTotal)
                            {
                                itemTotal.AddItem(item.Result);
                            }
                        }),

                        // Task 2 - Compute the Destinations
                        () => Parallel.ForEach(request.Destinations.Cast<DestinationPriceRequest>(), (dest) =>
                        {
                            if (dest.TaxNexusList == null)
                                dest.TaxNexusList = request.TaxNexusList;

                            Compute(dest, ComputeTaxes);
                            lock (destTotal)
                            {
                                destTotal.AddDestination(dest.Result);
                            }
                        })
                    );
                }
                else if (request.Items != null)
                {
                    // We can recompute the totals, but adding to itemTotal is not threadsafe so we need to lock for that.
                    // Also, we can do the OrderItem and Destination computation in parallel, in addition to each of its children

                    Parallel.ForEach(request.Items.Cast<ItemPriceRequest>(), async (item) =>
                    {
                        if (item.TaxNexusList == null)
                            item.TaxNexusList = request.TaxNexusList;

                        await Compute(item, ComputeTaxes && !AdjustForOrderDiscounts);
                        lock (itemTotal)
                        {
                            itemTotal.AddItem(item.Result);
                        }
                    });
                }
                else if (request.Destinations != null)
                {
                    // We can recompute the totals, but adding to itemTotal is not threadsafe so we need to lock for that.
                    // Also, we can do the OrderItem and Destination computation in parallel, in addition to each of its children

                    Parallel.ForEach(request.Destinations.Cast<DestinationPriceRequest>(), (dest) =>
                    {
                        if (dest.TaxNexusList == null)
                            dest.TaxNexusList = request.TaxNexusList;

                        Compute(dest, ComputeTaxes);
                        lock (destTotal)
                        {
                            destTotal.AddDestination(dest.Result);
                        }
                    });
                }

                // Adjust the totals for each line item (before taxing) if there are order level discounts or finance charges
                if (AdjustForOrderDiscounts && request.Items != null)
                {
                    decimal OrderDiscountPercent = 0.0M;
                    decimal OrderDiscountAmount = 0.0M;

                    foreach (OrderDiscount d in request.DiscountList)
                    {
                        if (d.IsPercentBased)
                            OrderDiscountPercent += d.Discount;
                        else
                            OrderDiscountAmount += d.Discount;
                    }


                    if (OrderDiscountPercent != 0 || OrderDiscountAmount != 0)
                    {
                        decimal OrderPreDiscountTotal = 0.0M;

                        foreach (ItemPriceRequest item in request.Items)
                        {
                            ItemPriceResult itemResult = item.Result;

                            // handle the OrderDiscountPercent if applicable, and compute the total excluding that
                            if (OrderDiscountPercent != 0)
                            {
                                itemResult.AppliedOrderDiscountPercent = OrderDiscountPercent;

                                #warning END-4373 needs rework

                                if (itemResult.PriceList.HasValue)
                                    itemResult.AppliedOrderDiscountAmount = decimal.Round((OrderDiscountPercent / 100m) * (itemResult.PriceList.Value - itemResult.AppliedItemDiscountAmount.Value), 2);
                                else
                                    itemResult.AppliedOrderDiscountAmount = null;
                            }

                            else
                                itemResult.AppliedOrderDiscountAmount = 0m;

                            OrderPreDiscountTotal += itemResult.PriceList.GetValueOrDefault(0m);
                        }

                        if (OrderDiscountAmount != 0)
                        {
                            // now we can distribute the discount proportionately
                            decimal RemainingOrderPreDiscountAmount = OrderPreDiscountTotal;
                            decimal RemainingDiscountAmount = OrderDiscountAmount;

                            foreach (ItemPriceRequest item in request.Items)
                            {
                                ItemPriceResult itemResult = item.Result;

                                if (itemResult.PriceList < 0.01M)
                                    continue;

                                decimal ThisDiscountAmount = Math.Min(itemResult.PriceList.Value,
                                                                    decimal.Round((OrderDiscountAmount * (itemResult.PriceList.GetValueOrDefault(0m) / OrderPreDiscountTotal)), 2));

                                RemainingOrderPreDiscountAmount -= itemResult.PriceList.Value;

                                // when the discount is less than 5 cents, apply it if possible
                                if ((RemainingDiscountAmount - ThisDiscountAmount) < 0.05M)
                                {
                                    if (itemResult.PriceList.Value > RemainingDiscountAmount)
                                        ThisDiscountAmount = RemainingDiscountAmount;
                                    else
                                        ThisDiscountAmount = itemResult.PriceList.Value;
                                }

                                RemainingDiscountAmount -= ThisDiscountAmount;

                                // Apply the discount, but check that we don't apply more than the actual amount.
                                itemResult.AppliedOrderDiscountAmount += ThisDiscountAmount;

                                ApplyPriceDiscountToItemResult(itemResult);
                            }
                        }
                    }

                    if (ComputeTaxes)
                    {
                        // Now we have to compute the taxes
                        itemTotal.TaxAmount = 0m;
                        foreach (ItemPriceRequest item in request.Items)
                        {
                            var te = new TaxEngine(this.BID, this.Context);
                            te.Compute(item, ComputeTaxes);

                            itemTotal.TaxAmount += item.Result.TaxAmount;

                            if (item.Result.TaxInfoList != null)
                            {
                                if (itemTotal.TaxInfoList == null)
                                    itemTotal.TaxInfoList = new List<TaxAssessmentResult>();

                                itemTotal.TaxInfoList.AddRange(item.Result.TaxInfoList);
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                await Logger.PricingError(this.BID, "Error in Order Compute", e);
                throw;
            }
            // now build the result
            OrderPriceResult result = new OrderPriceResult(request, itemTotal, destTotal);
            request.Result = result;

            // and return it
            return result;
        }

        private List<ComponentPriceResult> FillNonRolledComponents(List<ComponentPriceResult> components)
        {
            static void FillResults(ICollection<ComponentPriceResult> components, bool assembliesOnly, List<ComponentPriceResult> result)
            {
                foreach (ComponentPriceResult component in components)
                {
                    if (!assembliesOnly || (component.ComponentType == OrderItemComponentType.Assembly && !component.RollupLinkedPriceAndCost.GetValueOrDefault(true)))
                        result.Add(component);

                    if (component.ChildComponents != null)
                        FillResults(component.ChildComponents, true, result);
                }
            }

            List<ComponentPriceResult> result = new List<ComponentPriceResult>();
            FillResults(components, false, result);

            return result;
        }

        public async Task<ItemPriceResult> Compute(ItemPriceRequest request, bool ComputeTaxes = true)
        {
            ItemPriceResult result = request.Result ?? new ItemPriceResult();
            request.Result = result;

            try
            {
                result.CompanyID = request.CompanyID;
                result.AppliedItemDiscountAmount = 0m;
                result.AppliedOrderDiscountAmount = 0m;
                result.AppliedOrderDiscountPercent = 0m;
                result.UnitPriceOV = request.UnitPriceOV.GetValueOrDefault(false);
                result.ErrorCount = 0;

                result.ItemDiscountAmount = request.ItemDiscountAmount.GetValueOrDefault(0m);
                result.ItemDiscountPercent = request.ItemDiscountPercent;

                if (request.Surcharges != null && request.Surcharges.Count > 0)
                {
                    if (result.Surcharges == null)
                        result.Surcharges = new List<SurchargePriceResult>();

                    if (result.PriceSurcharge == null)
                        result.PriceSurcharge = 0m;

                    foreach (SurchargePriceRequest surchargeRequest in request.Surcharges)
                    {
                        // Adjustment Surcharges are not computed or returned in the results.
                        if (surchargeRequest.SurchargeDefID != 1)
                            result.Surcharges.Add(Compute(surchargeRequest));
                    }

                    if (result.Surcharges.Any(s => s.PricePreTaxOV))
                        result.PriceSurchargeOV = true;

                    result.PriceSurcharge = result.Surcharges.Sum(c => c.PricePreTax);
                }
                else
                {
                    result.Surcharges = null;
                    result.PriceSurcharge = 0m;
                }

                decimal? totalOVAmount = null;

                if (request.UnitPriceOV.GetValueOrDefault(false))
                {
                    totalOVAmount = Math.Round(request.UnitPrice.GetValueOrDefault(0.00m) * request.Quantity, 2);

                    if (request.ItemDiscountPercent.GetValueOrDefault(0m) != 0m)
                        totalOVAmount = decimal.Round(totalOVAmount.Value / (1m - request.ItemDiscountPercent.Value), 2);

                    totalOVAmount -= result.PriceSurcharge.GetValueOrDefault(0m);
                    totalOVAmount += result.ItemDiscountAmount.GetValueOrDefault(0m);
                    totalOVAmount += result.AppliedOrderDiscountAmount.GetValueOrDefault(0m);
                }

                decimal? remainingOVAmount = totalOVAmount;

                if (request.Components != null && request.Components.Count > 0)
                {
                    if (result.Components == null)
                        result.Components = new List<ComponentPriceResult>();
                    if (result.PriceComponent == null)
                        result.PriceComponent = 0m;

                    foreach (ComponentPriceRequest componentRequest in request.Components)
                    {
                        componentRequest.IsVended = request.IsOutsourced;
                        componentRequest.ParentQuantity = request.Quantity;
                        componentRequest.CompanyID = request.CompanyID;
                        componentRequest.LineItemQuantity = request.Quantity;

                        ComponentPriceResult componentResult = await Compute(componentRequest, request.EngineType);

                        if (componentResult.IsIncluded.GetValueOrDefault(true))
                            result.Components.Add(componentResult);

                        if (!componentResult.Success)
                            result.ErrorCount += componentResult.ErrorCount;
                    }

                    List<ComponentPriceResult> componentsNotRolledUp = FillNonRolledComponents(result.Components);

                    if (totalOVAmount.HasValue)
                    {
                        decimal componentsOVTotal = componentsNotRolledUp
                                                          .Where(x => x.PriceUnitOV && x.PricePreTax.GetValueOrDefault(0.00m) != 0.00m)
                                                          .Sum(x => x.PricePreTax.GetValueOrDefault(0.00m));

                        totalOVAmount -= componentsOVTotal;

                        List<ComponentPriceResult> componentsToOV = componentsNotRolledUp
                                                                          .Where(x => !x.PriceUnitOV && x.PricePreTax.HasValue)
                                                                          .ToList();
                        decimal priceTotal = componentsToOV.Sum(x => x.PricePreTax.Value);
                        bool allZeros = componentsToOV.All(x => x.PricePreTax.Value == 0m);
                        int componentCount = componentsToOV.Count;
                        remainingOVAmount = totalOVAmount;

                        ComponentPriceResult firstComponent = null;

                        foreach (ComponentPriceResult componentResult in componentsToOV)
                        {
                            if (componentResult.PricePreTax != 0m || allZeros)
                            {
                                decimal newPrice;

                                if (allZeros)
                                    newPrice = decimal.Round(totalOVAmount.Value / componentCount, 2);
                                else
                                    newPrice = decimal.Round(totalOVAmount.Value * (componentResult.PricePreTax.Value / priceTotal), 2);

                                remainingOVAmount -= newPrice;

                                componentResult.PricePreTax = newPrice;
                                componentResult.PriceTotal = newPrice;

                                if (firstComponent == null)
                                    firstComponent = componentResult;
                            }
                        }

                        if (remainingOVAmount.Value != 0m && firstComponent != null)
                        {
                            firstComponent.PricePreTax += remainingOVAmount;
                            firstComponent.PriceTotal += remainingOVAmount;

                            remainingOVAmount = 0m;
                        }
                    }

                    if (componentsNotRolledUp.Any(c => c.PriceUnitOV))
                        result.PriceComponentOV = true;

                    if (componentsNotRolledUp.Any(c => !c.PriceTotal.HasValue))
                        result.PriceComponent = null;
                    else
                        result.PriceComponent = componentsNotRolledUp.Sum(c => c.PriceTotal);

                    if (componentsNotRolledUp.Any(c => !c.CostLabor.HasValue))
                        result.CostLabor = null;
                    else
                        result.CostLabor = componentsNotRolledUp.Sum(c => c.CostLabor);

                    if (componentsNotRolledUp.Any(c => !c.CostMachine.HasValue))
                        result.CostMachine = null;
                    else
                        result.CostMachine = componentsNotRolledUp.Sum(c => c.CostMachine);

                    if (componentsNotRolledUp.Any(c => !c.CostMaterial.HasValue))
                        result.CostMaterial = null;
                    else
                        result.CostMaterial = componentsNotRolledUp.Sum(c => c.CostMaterial);

                    if (componentsNotRolledUp.Any(c => !c.CostNet.HasValue))
                        result.CostTotal = null;
                    else
                        result.CostTotal = componentsNotRolledUp.Sum(c => c.CostNet);
                }
                else
                {
                    result.Components = null;
                    result.PriceComponent = 0m;
                    result.CostLabor = 0m;
                    result.CostMachine = 0m;
                    result.CostMaterial = 0m;
                    result.CostTotal = 0m;
                }

                if (remainingOVAmount.HasValue && remainingOVAmount.Value != 0m)
                {
                    if (result.Surcharges == null)
                        result.Surcharges = new List<SurchargePriceResult>();

                    result.Surcharges.Add(new SurchargePriceResult
                    {
                        SurchargeDefID = 1,
                        CompanyID = request.CompanyID,
                        PricePreTax = remainingOVAmount.Value,
                        PricePreTaxOV = false,
                        Quantity = 1,
                    });

                    result.PriceSurcharge += remainingOVAmount.Value;
                }

                if (result.PricePreTax.HasValue && request.Quantity != 0)
                    result.UnitPrice = decimal.Round(result.PricePreTax.Value / request.Quantity, 6);
                else
                    result.UnitPrice = null;

                ApplyPriceDiscountToItemResult(result);

                if (ComputeTaxes)
                {
                    var te = new TaxEngine(this.BID, this.Context);
                    te.Compute((ItemPriceRequest)request, ComputeTaxes);
                }
            }
            catch (Exception e)
            {
                await Logger.PricingError(this.BID, "Error in Item Compute", e);
                throw;
            }

            return result;
        }

        private void ApplyPriceDiscountToItemResult(ItemPriceResult result)
        {
            try
            {
                if (result.PriceDiscount.GetValueOrDefault(0m) != 0m)
                {
                    decimal discountTotal = result.PriceDiscount.Value;
                    decimal discountRemaining = discountTotal;
                    decimal priceTotal = result.PriceList.GetValueOrDefault(0m);
                    decimal priceRemaining = priceTotal;

                    ComponentPriceResult firstComponent = null;
                    SurchargePriceResult firstSurcharge = null;

                    if (result.Components != null)
                    {
                        foreach (ComponentPriceResult componentResult in result.Components)
                        {
                            componentResult.PriceAppliedDiscount = Math.Round(discountTotal * (priceTotal == 0m ? 0m : componentResult.PricePreTax.GetValueOrDefault(0m) / priceTotal), 2);
                            discountRemaining -= componentResult.PriceAppliedDiscount;
                            priceRemaining -= componentResult.PricePreTax.GetValueOrDefault(0m);

                            if (firstComponent == null && componentResult.PricePreTax.GetValueOrDefault(0m) != 0m)
                                firstComponent = componentResult;
                        }
                    }

                    if (result.Surcharges != null)
                    {
                        foreach (SurchargePriceResult surchargeResult in result.Surcharges)
                        {
                            surchargeResult.PriceAppliedDiscount = Math.Round(discountTotal * (priceTotal == 0m ? 0m : surchargeResult.PricePreTax.GetValueOrDefault(0m) / priceTotal), 2);
                            discountRemaining -= surchargeResult.PriceAppliedDiscount;
                            priceRemaining -= surchargeResult.PricePreTax.GetValueOrDefault(0m);

                            if (firstSurcharge == null && surchargeResult.PricePreTax.GetValueOrDefault(0m) != 0m)
                                firstSurcharge = surchargeResult;
                        }
                    }

                    if (discountRemaining != 0m)
                    {
                        if (firstComponent != null)
                            firstComponent.PriceAppliedDiscount += discountRemaining;
                        else if (firstSurcharge != null)
                            firstSurcharge.PriceAppliedDiscount += discountRemaining;
                        else
                        {
                            if (result.Surcharges == null)
                                result.Surcharges = new List<SurchargePriceResult>();

                            result.Surcharges.Add(new SurchargePriceResult
                            {
                                SurchargeDefID = 1,
                                CompanyID = result.CompanyID,
                                PricePreTax = 0m,
                                PricePreTaxOV = false,
                                Quantity = 1,
                                PriceAppliedDiscount = discountRemaining
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.PricingError(this.BID, "Error in ApplyPriceDiscountToItemResult", e);
                throw;
            }
        }

        public async Task<ComponentPriceResult> Compute(ComponentPriceRequest request, PricingEngineType engineType)
        {
            ComponentPriceResult result = request.Result ?? new ComponentPriceResult();
            request.Result = result;

            try
            {
                result.CompanyID = request.CompanyID;
                result.ComponentType = request.ComponentType;
                result.ComponentID = request.ComponentID;
                result.TotalQuantity = request.TotalQuantity;
                result.TotalQuantityOV = request.TotalQuantityOV.GetValueOrDefault(false);
                result.QuantityUnit = request.QuantityUnit;
                result.AssemblyQuantity = request.AssemblyQuantity;
                result.AssemblyQuantityOV = request.AssemblyQuantityOV.GetValueOrDefault(false);
                result.IsIncludedOV = request.IsIncludedOV;
                result.PriceUnitOV = request.PriceUnitOV.GetValueOrDefault();
                result.CostOV = request.CostUnitOV.GetValueOrDefault();
                result.ExpenseAccountID = request.ExpenseAccountID;
                result.IncomeAccountID = request.IncomeAccountID;
                result.IncomeAllocationType = request.IncomeAllocationType;
                result.Variables = request.Variables;

                //only assign ID/TempID at this top level
                if (request.ID.HasValue)
                    result.ID = request.ID.Value;
                result.TempID = request.TempID;
                result.VariableName = request.VariableName;

                result.CostLabor = 0m;
                result.CostMachine = 0m;
                result.CostMaterial = 0m;

                await ComponentPriceEngine.Compute(request, engineType, Context, Cache, Logger, BID);

                result.PriceTotal = result.PricePreTax;

                if (!result.CostOV || new OrderItemComponentType[] { OrderItemComponentType.Material, OrderItemComponentType.Labor }.Contains(result.ComponentType))
                {
                    result.CostNet = result.CostLabor + result.CostMachine + result.CostMaterial;
                }

                if (result.CostNet.HasValue && result.TotalQuantity.GetValueOrDefault(0m) != 0)
                    result.CostUnit = decimal.Round(result.CostNet.Value / result.TotalQuantity.Value, 6);
                else
                    result.CostUnit = null;

            }
            catch (Exception e)
            {
                await Logger.PricingError(this.BID, "Error in Component Compute", e);
                throw;
            }
            return result;
        }

        public SurchargePriceResult Compute(SurchargePriceRequest request)
        {
            SurchargePriceResult result = request.Result ?? new SurchargePriceResult();
            request.Result = result;

            try
            {
                result.SurchargeDefID = request.SurchargeDefID;
                result.CompanyID = request.CompanyID;
                result.PricePreTaxOV = request.PricePreTaxOV.GetValueOrDefault(false);
                result.Quantity = request.Quantity;

                if (request.PricePreTaxOV.GetValueOrDefault(false))
                    result.PricePreTax = request.PricePreTax;
                else
                    result.PricePreTax = request.PriceFixedAmount.GetValueOrDefault(0m) + (request.Quantity * request.PricePerUnitAmount.GetValueOrDefault(0m));

            }
            catch (Exception e)
            {
                Logger.PricingError(this.BID, "Error in Surcharge Compute", e);
                throw;
            }

            return result;
        }

        public DestinationPriceResult Compute(DestinationPriceRequest request, bool ComputeTaxes = true)
        {
            DestinationPriceResult result = request.Result ?? new DestinationPriceResult();
            request.Result = result;

            try
            {
                DestinationPricingEngine.Compute(request);

                result.PriceNetOV = request.PriceNet.HasValue;

                result.PriceDiscountPercent = request.PriceDiscountPercent;
                result.PriceDiscountAmount = request.PriceDiscountAmount;

                if (result.PriceNetOV)
                    result.PriceNet = request.PriceNet.Value;
                else
                    result.PriceNet = result.ComputedNet;

                if (ComputeTaxes)
                {
                    var te = new TaxEngine(this.BID, this.Context);
                    te.Compute(request, ComputeTaxes);
                }

            }
            catch (Exception e)
            {
                Logger.PricingError(this.BID, "Error in Destination Compute", e);
                throw;
            }
            return result;
        }

        public OrderPriceResult SumOrder(OrderPriceRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
