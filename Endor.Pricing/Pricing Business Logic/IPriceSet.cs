﻿using System;

namespace Endor.Pricing
{
    public interface IPriceSet
    {
        decimal? Net { get; set; }
        decimal? Discount { get; set; }
        decimal? PreTax { get; }
        decimal? Taxable { get; set; }
        decimal? Tax { get; set; }
        decimal? Total { get; }
    }
}
