﻿using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Endor.Pricing
{
    public static class TaxEngine_Exempt
    {
        public static TaxAssessmentResult Compute(short BID, ApiContext ctx, TaxAssessmentRequest request, TaxAssessmentNexus nexus)
        {
            // For orders marked non-taxable, we don't need to compute the detail.
            // Just create an appropriate response and return it.
            TaxAssessmentResult result = new TaxAssessmentResult()
            {
                NexusID = request.NexusID,
                Quantity = request.Quantity,
                Price = request.Price.GetValueOrDefault(0m),
                InvoiceText = nexus.InvoiceText, 
                IsTaxExempt = request.IsTaxExempt,
                PriceTaxable = 0,
                PriceTaxableOV = request.PriceTaxable.HasValue,
                TaxEngine = nexus.EngineType,
                TaxGroupID = nexus.TaxGroupID,
            };

            TaxGroup taxGroup = ctx.TaxGroup.Include(x => x.TaxGroupItemLinks).ThenInclude(x => x.TaxItem).FirstOrDefault(tg => tg.ID == nexus.TaxGroupID && tg.BID == BID);

            if (taxGroup == null)
                throw new Exception($"Tax group ID={nexus.TaxGroupID} not found.");

            foreach (TaxGroupItemLink itemLink in taxGroup.TaxGroupItemLinks)
            {
                TaxAssessmentItemResult itemResult = new TaxAssessmentItemResult()
                {
                    TaxItemID = itemLink.TaxItem.ID,
                    InvoiceText = itemLink.TaxItem.InvoiceText,
                    TaxRate = itemLink.TaxItem.TaxRate,
                    TaxAmount = 0,
                    GLAccountID = itemLink.TaxItem.GLAccountID,
                };
                result.AddItem(itemResult);
            }

            return result;
        }
    }
}
