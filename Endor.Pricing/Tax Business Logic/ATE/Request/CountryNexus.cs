﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Xml.Serialization;

namespace ATE.Models
{
    /// <summary>
    /// Jurisdiction which a company has Nexus for a particular country.
    /// </summary>
    [XmlRoot("CountryNexus")]
    public class CountryNexus
    {

        /// <summary>
        /// Deserializes the Nexuses from XML
        /// </summary>
        /// <param name="XML"></param>
        /// <returns></returns>
        public static List<CountryNexus> Deserialize(string XML)
        {
            if (string.IsNullOrWhiteSpace(XML))
                return null;

            XmlSerializer serializer = new XmlSerializer(typeof(List<CountryNexus>), new XmlRootAttribute("Nexuses"));
            using (TextReader reader = new StringReader(XML))
            {
                return (List<CountryNexus>)serializer.Deserialize(reader);
            }
        }

        /// <summary>
        /// The two digit country code for the address.
        /// </summary>
        [Required]
        public string Country { get; set; }

        /// <summary>
        /// An array of two or three digit standard region abbreviations (aka provinces or regions) to which Jurisdiction applies.  
        /// If this is not provided, all Regions are considered to be taxable jurisdictions.
        /// </summary>
        [XmlArrayItem("Region")]
        public List<string> Region { get; set; }

    }
}