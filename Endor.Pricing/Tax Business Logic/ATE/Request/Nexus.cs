﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace ATE.Models
{
    /// <summary>
    /// Address information to determine taxation
    /// </summary>
    public class Nexus
    {

        /// <summary>
        /// Addresses correspond to the locations where the company has a physical presence (Physical Nexus) and are sent along with 
        /// the CountryNexus to tax providers. 
        /// </summary>
        [XmlArrayItem("PhysicalAddresses")]
        public List<Address> PhysicalAddresses { get; set; }

        /// <summary>
        /// Economic Regions correspond to the locations where the company has Economic Nexus, and is an array of CountryRegions 
        /// where the customer has Nexus.  Each CountryRegion contains a 2-digit Country Code and an array of Regions in which the
        /// business has Nexus. 
        /// </summary>
        [XmlArrayItem("EconomicRegions")]
        public List<CountryNexus> EconomicRegions { get; set; }


    }
}
