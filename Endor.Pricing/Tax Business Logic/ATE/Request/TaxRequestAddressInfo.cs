﻿using System.ComponentModel.DataAnnotations;

namespace ATE.Models
{
    /// <summary>
    /// Contains Shipping to/from information for an item and is used to determine the applicable nexus and tax rates for a transaction
    /// </summary>
    public class TaxRequestAddressInfo
    {
        /// <summary>
        /// The Key/ID for this Nexus.  This needs to be unique within an order.
        /// </summary>
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// The destination address.  
        /// </summary>
        [Required]
        public Address Address { get; set; }

        /// <summary>
        /// The source shipping address.  If not supplied, the Address is used as the source also (i.e., local pickup)
        /// </summary>
        public Address FromAddress { get; set; }
    }
}