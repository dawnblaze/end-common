﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ATE.Models
{
    /// <summary>
    /// Tax computation request.
    /// </summary>
    public class OrderTaxRequest
    {
        /// <summary>
        /// An array of Line Items for the Order.  This may also include Destinations if there are fees associated with them.
        /// </summary>
        [Required]
        public List<ItemTaxRequest> Items { get; set; }

        /// <summary>
        /// An array of Tax Ship To/From Addresses pairs for the Order.
        /// </summary>
        [Required]
        public List<TaxRequestAddressInfo> AddressInfoList { get; set; }

        /// <summary>
        /// Can be used by some Tax Providers (Avalara) to alter tax rules based on CompanyName.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Can be used by some Tax Providers (TaxJar, Avalara) to alter tax rules based on CompanyID
        /// </summary>
        public string CompanyID { get; set; }

        /// <summary>
        /// Order Number for information / tracking purposes.
        /// </summary>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Order Description for information / tracking purposes.
        /// </summary>
        public string Description { get; set; }
    }
}
