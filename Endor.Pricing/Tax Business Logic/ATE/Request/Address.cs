﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ATE.Models
{
    /// <summary>
    /// Address used in the lookup for nexus.
    /// </summary>
    public class Address
    {
        /// <summary>
        /// The two digit country code for the address.
        /// </summary>
        [Required]
        public string Country { get; set; }

        /// <summary>
        /// A strings corresponding to the street address.  If the street address contains multiple lines, each line by /n in the input string.
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// The name of the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// The two or three digit standard abbreviation for the state, province, or district.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// The postal code if applicable.  For US, may be the 5 or 9 digit postal code.  Omit spaces, dashes, or other formatting.
        /// </summary>
        [Required]
        public string Zip { get; set; }

        /// <summary>
        /// FormattedAddress
        /// </summary>
        [JsonIgnore]
        public string FormattedAddress
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(Street))
                {
                    sb.Append(Street);
                    sb.Append("\r\n");
                }

                if (!string.IsNullOrWhiteSpace(City))
                {
                    sb.Append(City);
                    sb.Append(", ");
                }

                if (!string.IsNullOrWhiteSpace(State))
                {
                    sb.Append(State);
                    sb.Append("  ");
                }

                if (!string.IsNullOrWhiteSpace(Zip))
                    sb.Append(Zip);

                if (!string.IsNullOrWhiteSpace(Country))
                {
                    sb.Append("  ");
                    sb.Append(Country);
                }

                return sb.ToString();
            }
        }
    }
}