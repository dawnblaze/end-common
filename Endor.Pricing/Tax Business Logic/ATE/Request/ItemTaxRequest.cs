﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ATE.Models
{
    /// <summary>
    /// Contains the information about a single line item or pricing attribute (shipping charges) in a tax computation request.
    /// </summary>
    public class ItemTaxRequest
    {
        private Decimal? _priceNet = null;

        /// <summary>
        /// Line Item Number or Key
        /// </summary>
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// The name of the Product
        /// </summary>
        public string Product { get; set; }

        /// <summary>
        /// The Quantity of units for this item
        /// </summary>
        [Required]
        public Decimal Quantity { get; set; }

        /// <summary>
        /// The Unit Price of the Item.  Defaults to PriceNet/Quantity if not supplied.
        /// </summary>
        public Decimal? PriceUnit { get; set; }

        /// <summary>
        /// The (net) amount of discount
        /// </summary>
        public Decimal? ItemDiscountAmount { get; set; }

        /// <summary>
        /// The Next Price of the Item.  Defaults to ( PriceUnit == null ? 0.00 : PriceUnit*Quantity ) - ItemDiscountAmount
        /// </summary>
        public Decimal? PriceNet
        {
            get
            {
                if (_priceNet == null)
                    return this.PriceUnit != null ? PriceUnit * Quantity - (this.ItemDiscountAmount ?? 0m) : 0m;

                return _priceNet;
            }
            set => this._priceNet = value;
        }

        /// <summary>
        /// The Key/ID associated with the ATE.TaxRequestAddressInfo Object List for this tax item.
        /// </summary>
        [Required]
        public string AddressInfoKey { get; set; }

        /// <summary>
        /// An enum used to identify the tax types for pricing attributes.
        /// This will automatically set the Taxability Code for non-zero values.
        /// Valid values are found in the TaxabilityType enum.
        /// </summary>
        public TaxabilityType TaxabilityType { get; set; }

        /// <summary>
        /// The taxability code for this Nexus lookup.
        /// This value cannot be supplied when TaxabilityType is supplied and non-zero.
        /// </summary>
        public string TaxabilityCode { get; set; }
    }
}
