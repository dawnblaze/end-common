﻿using System;
using System.Collections.Generic;

namespace ATE.Models
{
    /// <summary>
    /// Registration request class used when adding a new registration
    /// </summary>
    public class RegistrationRequest
    {
        /// <summary>
        /// The Key of the Business 
        /// </summary>
        public Guid BusinessKey { get; set; }
        /// <summary>
        /// Application the created the registration
        /// </summary>
        public string AppType { get; set; }
        /// <summary>
        /// If true, the sandbox credentials are used
        /// </summary>
        public bool IsSandboxed { get; set; }
        /// <summary>
        /// The id of the tax processor to use
        /// </summary>
        public byte ProcessorType { get; set; }
        /// <summary>
        /// Uses shared account
        /// </summary>
        public bool UseSharedAccount { get; set; }
        /// <summary>
        /// List of credentials used by the processor
        /// </summary>
        public Dictionary<string, string> Credentials { get; set; }
        /// <summary>
        /// The friendly name of the registration
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The ID of the client's TimeZone
        /// </summary>
        public int TimeZone { get; set; }
        /// <summary>
        /// Currency code of the taxes
        /// </summary>
        public string Currency { get; set; }
        /// <summary>
        /// Tax Nexus
        /// </summary>
        public Nexus Nexus { get; set; }
    }
}
