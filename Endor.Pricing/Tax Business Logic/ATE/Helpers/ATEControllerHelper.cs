﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Pricing.Tax_Business_Logic.ATE.Helpers
{
    public class ATEControllerHelper
    {
        async public static Task<HttpResponseMessage> GET(string endpoint, Guid? registrationId = null)
        {
            using (HttpClient client = new HttpClient())
            {
                if (registrationId.HasValue)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Registration", registrationId.Value.ToString());

                return await client.GetAsync(endpoint);
            }
        }
        async public static Task<HttpResponseMessage> POST(string endpoint, object body, Guid? registrationId = null)
        {
            using (HttpClient client = new HttpClient())
            {
                if (registrationId.HasValue)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Registration", registrationId.Value.ToString());

                var bodyStr = JsonConvert.SerializeObject(body);
                return await client.PostAsync(endpoint, new StringContent(bodyStr, Encoding.UTF8, "application/json"));
            }
        }
        async public static Task<HttpResponseMessage> PUT(string endpoint, object body, Guid? registrationId = null)
        {
            using (HttpClient client = new HttpClient())
            {
                if (registrationId.HasValue)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Registration", registrationId.Value.ToString());

                return await client.PutAsync(endpoint, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json"));
            }
        }
        async public static Task<HttpResponseMessage> DELETE(string endpoint, Guid? registrationId = null)
        {
            using (HttpClient client = new HttpClient())
            {
                if (registrationId.HasValue)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Registration", registrationId.Value.ToString());

                return await client.DeleteAsync(endpoint);
            }
        }
    }
}
