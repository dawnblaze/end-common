﻿namespace ATE.Models
{
    /// <summary>
    /// Tax processors supported by ATE
    /// </summary>
    public enum Processor : byte
    {
        /// <summary>
        /// Avalara
        /// </summary>
        Avalara = 3,
        /// <summary>
        /// TaxJar
        /// </summary>
        TaxJar = 4,
        /// <summary>
        /// Test
        /// </summary>
        Test = 99,
    }
}
