﻿using System;

namespace ATE.Models
{
    /// <summary>
    /// Registration Response class
    /// </summary>
    public class RegistrationResponse
    {
        /// <summary>
        /// When successful, this specifies the registration ID.
        /// </summary>
        public Guid RegistrationID { get; set; }
    }
}
