﻿using System;

namespace ATE.Models
{
    /// <summary>
    /// Business Key Registration Response class
    /// </summary>
    public class BusinessKeyResponse
    {
        /// <summary>
        /// When successful, this specifies the business key.
        /// </summary>
        public Guid BusinessKey { get; set; }
    }
}
