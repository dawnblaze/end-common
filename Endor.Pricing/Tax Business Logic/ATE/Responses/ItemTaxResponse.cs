﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ATE.Models.Responses
{
    /// <summary>
    /// Tax computation for a single line item in response to a tax computation request.
    /// </summary>
    public class ItemTaxResponse
    {
        /// <summary>
        /// The Line Item Number or key passed into the Tax Request.  This can be used to match the returned taxes with the original request.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Indicates whether nexus exists or not. True = nexus exists (and the computation was passed to the tax service) False = nexus does not exist (and the computation was NOT passed to the tax service)
        /// </summary>
        public bool HasNexus { get; set; }

        /// <summary>
        /// The amount of the price that was subject to taxes, computed as the MAX() of the TaxAssessments[].PriceTaxable.  This is 0.00 if the item is not taxable.
        /// </summary>
        public Decimal? PriceTaxable
        {
            get
            {
                if (IsTaxable == null) return null;

                if(IsTaxable.GetValueOrDefault(false))
                {
                    return Assessments.Any() ? Assessments.Max(x => x.PriceTaxable) : 0.00m;
                }

                //Return 0 if IsTaxable == false
                return 0.00m;
            }
        }

        /// <summary>
        /// The net rate of taxes.  This is the SUM() of TaxAssessments for this line item where TaxAmount != 0.00.
        /// </summary>
        public Decimal? TaxRate => Assessments.Any() ? Assessments.Where(x => x.TaxAmount != 0m).Sum(x => x.TaxRate) : 0m;

        /// <summary>
        /// The amount of the taxes due for the item, computed as the SUM() of the TaxAssessments[].TaxAmount.
        /// </summary>
        public Decimal? TaxAmount => Assessments.Any() ? Assessments.Sum(x => x.TaxAmount) : 0m;

        /// <summary>
        /// An array of the individual taxes that make up the combined tax rates.
        /// </summary>
        public List<TaxAssessment> Assessments { get; set; }

        /// <summary>
        /// A flag indicating if the item or order was taxable, indicated by a TaxAmount > 0.00.  If the TaxAmount is NULL, IsTaxable is NULL.
        /// </summary>
        public bool? IsTaxable => (TaxAmount == null) ? null : (bool?)(TaxAmount > 0);
    }
}
