﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ATE.Models.Responses
{
    /// <summary>
    /// Details the tax rate and amount computed for an individual item and nexus.
    /// </summary>
    public class TaxAssessment
    {
        /// <summary>
        /// The name of the level of tax applied. 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The name of the region corresponding to the tax. 
        /// </summary>
        public string TaxRegion { get; set; }

        /// <summary>
        /// The amount of the price that was subject to this Tax.
        /// </summary>
        public Decimal? PriceTaxable { get; set; }

        /// <summary>
        /// The net rate of taxes.
        /// </summary>
        public Decimal? TaxRate { get; set; }

        /// <summary>
        /// The amount of the taxes due for this item and Tax.
        /// </summary>
        public Decimal? TaxAmount { get; set; }

        /// <summary>
        /// Json string breakdown of the taxes by region
        /// </summary>
        public string Breakdown { get; set; }


    }
}
