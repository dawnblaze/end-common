﻿using ATE.Models.Responses;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ATE.Models
{
    /// <summary>
    /// Response to a tax computation request.
    /// </summary>
    public class OrderTaxResponse
    {
        /// <summary>
        /// A unique identifier associated with the recording of this transactions in the underlying tax engine.  It is used for GET, PUT, and DELETE actions on the recorded tax document.
        /// </summary>
        public string DocID { get; set; }

        /// <summary>
        /// An array of Line Items responses for the Order.  The Key field can be used to match the returned array with the Request.
        /// </summary>
        public List<ItemTaxResponse> Items { get; set; }

        /// <summary>
        /// The total pre-taxable amount of the order, based on the SUM() of the initial request, ItemTaxRequest[].PriceNet
        /// </summary>
        public Decimal? PreTaxPrice { get; set; }

        /// <summary>
        /// The total taxes due for the order, computed as the sum of Items[]. TaxAmount.
        /// </summary>
        public Decimal? TaxAmount { get; set; }

        /// <summary>
        /// Computed value = PreTaxPrice + TaxAmount
        /// </summary>
        public Decimal? PriceTotal { get; set; }

        /// <summary>
        /// The total taxable sales for the order, computed as the sum of Items[]. PriceTaxable.
        /// </summary>
        public Decimal? PriceTaxable { get; set; }

        /// <summary>
        /// The highest tax rate leveraged against any item in the order, computed as MAX() of Items.TaxRate.
        /// </summary>
        public Decimal? HighestTaxRate => Items.Any() ? Items.Where(x => x.TaxRate != null).Max(x => x.TaxRate) : 0m;

        /// <summary>
        /// An array of the resulting order-level taxes, computed by combining all of the item-level tax assessments with the same name and tax region.
        /// </summary>
        public List<TaxAssessment> Assessments { get; set; }

        /// <summary>
        /// A flag indicating if the item or order was taxable, indicated by a TaxAmount > 0.00.  If the TaxAmount is NULL, IsTaxable is NULL.
        /// </summary>
        public bool? IsTaxable => TaxAmount.GetValueOrDefault(0m) > 0m;

        /// <summary>
        /// Composite Breakdown for the order
        /// </summary>
        public string breakdown;

    }
}
