﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ATE.Models
{
    /// <summary>
    /// TaxabilityType model.
    /// </summary>
    public class TaxabilityType
    {
        /// <summary>
        /// ID of the TaxabilityType
        /// </summary>
        public short ID { get; set; }
        /// <summary>
        /// TaxabilityType Name
        /// /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// TaxabilityType TaxJarCode
        /// /// </summary>
        public string TaxJarCode { get; set; }
        /// <summary>
        /// TaxabilityType AvalaraCode
        /// /// </summary>
        public string AvalaraCode { get; set; }

    }
}
