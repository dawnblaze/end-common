﻿using ATE.Models;
using Endor.EF;
using Endor.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Pricing
{
    public static class TaxEngine_TaxJar
    {
        public static OrderTaxRequest TaxAssessmentToOrderTaxRequest(byte TransactionType, TaxAssessmentRequest request, TaxAssessmentNexus nexus) => new OrderTaxRequest()
        {
            CompanyName = nexus.CompanyName,
            CompanyID = nexus.CompanyID.ToString(),
            AddressInfoList = new List<TaxRequestAddressInfo>()
                {
                     new TaxRequestAddressInfo()
                     {
                        Key = "Address1",
                        Address = GetAddress(nexus.ToAddress),
                        FromAddress = nexus.UseSameToFromAddress.GetValueOrDefault() ? GetAddress(nexus.ToAddress) : GetAddress(nexus.FromAddress)
                     },
                },
            Items = new List<ItemTaxRequest>()
                {
                    new ItemTaxRequest //not taxable in LA
                    {
                        Key = "Item1",
                        AddressInfoKey = "Address1",
                        Product = "Item1",
                        PriceUnit = request.Quantity != 0 ? request.Price/request.Quantity : 0,
                        Quantity = ((TransactionType==(byte)OrderTransactionType.Memo) ? -1 : 1) * request.Quantity,
                        TaxabilityCode = nexus.TaxabilityCode
                    },
                }
        };

        public static ATE.Models.Address GetAddress(Endor.Pricing.Address address)
        {
            if (address == null) return null;
            return new ATE.Models.Address()
            {
                Country = address.Country,
                Street = address.Street,
                City = address.Street,
                State = address.State,
                Zip = address.PostalCode
            };
        }
        public static async Task<T> SendRequest<T>(string url, object body, KeyValuePair<string, string>[] headers = null)
        {
            using HttpClient client = new HttpClient();
            var content = new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json");
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            var response = await client.PostAsync(url, content);
            var result = await response.Content.ReadAsStringAsync();
            try
            {
                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception)
            {
                throw new Exception("Error parsing ATE result:" + result);
            }
        }

        public static async Task<T> SendGetRequest<T>(string url, KeyValuePair<string, string>[] headers = null)
        {
            using HttpClient client = new HttpClient();
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            var response = await client.GetAsync(url);
            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(result);
        }

        static UInt64 CalculateHash(string read)
        {
            UInt64 hashedValue = 3074457345618258791ul;
            for (int i = 0; i < read.Length; i++)
            {
                hashedValue += read[i];
                hashedValue *= 3074457345618258799ul;
            }
            return hashedValue;
        }

        public static TaxAssessmentResult Compute(byte TransactionType, short BID, ApiContext ctx, TaxAssessmentRequest request, TaxAssessmentNexus nexus)
        {
            var headers = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("Authorization", "registration " + nexus.ATERegistrationID)
            };

            if (nexus.ToAddress==null)
            {
                throw new Exception("To Address of Nexus must be defined");
            }
            /*var addressQuery = "";
            if (!String.IsNullOrWhiteSpace(nexus.ToAddress.Street)) addressQuery += $"street1={nexus.ToAddress.Street}&";
            if (!String.IsNullOrWhiteSpace(nexus.ToAddress.City)) addressQuery += $"city={nexus.ToAddress.City}&";
            if (!String.IsNullOrWhiteSpace(nexus.ToAddress.State)) addressQuery += $"region={nexus.ToAddress.State}&";
            if (!String.IsNullOrWhiteSpace(nexus.ToAddress.PostalCode)) addressQuery += $"postalcode={nexus.ToAddress.PostalCode}&";
            if (!String.IsNullOrWhiteSpace(nexus.ToAddress.Country)) addressQuery += $"street1={nexus.ToAddress.Country}&";
            var addressResult = Task.Run(() => SendGetRequest<Address>(nexus.ATEApiUrl + "/ate/address/validate?" + addressQuery, headers.ToArray()));
            addressResult.Wait();
            var addressResponse = addressResult.Result;
            */
            var orderTaxRequest = TaxAssessmentToOrderTaxRequest(TransactionType, request, nexus);
            var result = Task.Run(() => SendRequest<OrderTaxResponse>(nexus.ATEApiUrl+ "/ate/tax/compute", orderTaxRequest, headers.ToArray()));
            result.Wait();
            var computeResponse = result.Result;
            if (computeResponse == null)
                throw new Exception("Error communicating with ATE server");
            //{ year}\{ country}\{ state or province}\{ county}\{ city}\{ zip - 9}\{ rate}\#{Group Hash}
            var taxRate = 100*computeResponse.Assessments.Sum(x => x.TaxRate.GetValueOrDefault());
            var yearStr = DateTime.UtcNow.Year.ToString();
            var taxGroupName = yearStr
                + '/' + nexus.ToAddress.Country
                + '/' + nexus.ToAddress.State
                + '/' + nexus.ToAddress.County
                + '/' + nexus.ToAddress.City
                + '/' + nexus.ToAddress.PostalCode
                + '/' + taxRate.ToString("0.###")
                + "/#";
            var hashCode = CalculateHash(taxGroupName); 

            taxGroupName += hashCode.ToString();

            List<TaxAssessmentItemResult> taxItemResults = new List<TaxAssessmentItemResult>();
            foreach (var assessment in computeResponse.Assessments)
            {
                var itemTaxRate = 100*assessment.TaxRate.GetValueOrDefault();
                var itemTaxRateStr = itemTaxRate.ToString("0.###");
                string Name = "";
                switch (assessment.Name.ToLower())
                {
                    case "country":
                        Name = yearStr
                            + '/' + assessment.TaxRegion;
                        break;
                    case "state":
                        Name = yearStr
                            + '/' + nexus.ToAddress.Country
                            + '/' + assessment.TaxRegion;
                        break;
                    case "county":
                        Name = yearStr
                            + '/' + nexus.ToAddress.Country
                            + '/' + nexus.ToAddress.State
                            + '/' + assessment.TaxRegion;
                        break;
                    case "city":
                        Name = yearStr
                            + '/' + nexus.ToAddress.Country
                            + '/' + nexus.ToAddress.State
                            + '/' + assessment.TaxRegion;
                        break;
                    case "district":
                        Name = yearStr
                            + '/' + nexus.ToAddress.Country
                            + '/' + nexus.ToAddress.State
                            + '/' + assessment.TaxRegion;
                        break;
                    case "gst":
                        Name = yearStr
                            + '/' + nexus.ToAddress.Country
                            + "/GST";
                        break;
                    case "pst":
                        Name = yearStr
                            + '/' + nexus.ToAddress.Country
                            + "/PST";
                        break;
                    case "qst":
                        Name = yearStr
                            + '/' + nexus.ToAddress.Country
                            + "/QST";
                        break;
                    default:
                        Name = yearStr
                            + '/' +assessment.TaxRegion;
                        break;

                }
                Name += ("/" + itemTaxRateStr + "/#");
                var itemHashCode = CalculateHash(Name);
                Name += itemHashCode.ToString();

                var taxItemResult = new TaxAssessmentItemResult()
                {
                    TaxRate = itemTaxRate,
                    TaxAmount = ((TransactionType == (byte)OrderTransactionType.Memo) ? -1 : 1) * assessment.TaxAmount.GetValueOrDefault(),
                    GLAccountID = 2400, //Sales Tax Payable
                    InvoiceText = Name
                };

                taxItemResults.Add(taxItemResult);
            }
            var taxCodeID = ctx.TaxabilityCodes.FirstOrDefault(x => x.BID == BID && x.TaxCode == nexus.TaxabilityCode)?.ID;
            TaxAssessmentResult taxAssessmentResult = new TaxAssessmentResult()
            {
                TaxGroupID = 1,
                TaxEngine = TaxEngineType.TaxJar,
                TaxAmount = computeResponse.TaxAmount.GetValueOrDefault(),
                PriceTaxable = computeResponse.PriceTaxable.GetValueOrDefault(),
                IsTaxExempt = !computeResponse.IsTaxable ?? true,
                TaxRate = taxRate,
                InvoiceText = taxGroupName,
                Price = computeResponse.PriceTotal.GetValueOrDefault(),
                Quantity = request.Quantity,
                Items = taxItemResults,
                NexusID = "1",
                TaxCodeID = taxCodeID
            };
            return taxAssessmentResult;
        }
    }
}
