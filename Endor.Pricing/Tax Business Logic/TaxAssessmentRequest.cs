﻿using System.Collections.Generic;


namespace Endor.Pricing
{
    /// <summary>
    /// A Tax Assessment Request
    /// </summary>
    public class TaxAssessmentRequest
    {
        /// <summary>
        /// The quantity of items taxed.  For line items taxed by destination, this is the destination quantity.
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// The price that tax is being computed on
        /// </summary>
        public decimal? Price { get; set; }

        /// <summary>
        /// Flag that indicates the products
        /// </summary>
        public bool IsTaxExempt { get; set; }

        /// <summary>
        /// Only send the PriceTaxable if it is overridden.
        /// PriceTaxableOV is set to true if PriceTaxable != null 
        /// </summary>
        public decimal? PriceTaxable { get; set; }

        public string NexusID { get; set; }

        
    }
}
