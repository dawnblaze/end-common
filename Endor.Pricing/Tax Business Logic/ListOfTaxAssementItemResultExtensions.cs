﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Endor.Pricing
{
    public static class ListOfTaxAssementItemResultExtensions
    {

        /// <summary>
        /// This method is used to compress a List<> of TaxAssessmentResults.
        /// <para /> It combines Assessments with the same NexusID ... 
        /// </summary>
        /// <param name="assessments">List of TaxAssessmentResults</param>
        /// <returns></returns>
        public static List<TaxAssessmentResult> Compress(this List<TaxAssessmentResult> assessments)
        {
            var dupeNexusID = (from a in assessments
                               group a by a.NexusID into g
                               where g.Count() > 1
                               select g.Key).ToArray();

            var duplicates = assessments.FindAll(a => dupeNexusID.Contains(a.NexusID));
            duplicates.ForEach(d => assessments.Remove(d));

            var combinedDuplicates = new List<TaxAssessmentResult>();
            for (int i = 0; i < dupeNexusID.Count(); i++)
            {
                var sameIDs = duplicates.Where(d => d.NexusID.Equals(dupeNexusID[i])).ToArray();
                var firstDupe = sameIDs.First();

                List<TaxAssessmentItemResult> dupesItems = new List<TaxAssessmentItemResult>();
                foreach (TaxAssessmentResult same in sameIDs)
                    dupesItems.AddRange(same.Items);

                firstDupe.Items.Clear();
                firstDupe.Items.AddRange(dupesItems);
                combinedDuplicates.Add(firstDupe);
            }

            assessments.AddRange(combinedDuplicates);
            assessments.ForEach(d => d.Items.Compress());

            return assessments;
        }

        /// <summary>
        /// Combines and Tax Assessment Items that are for the same TaxItemID or Invoice Text if TaxGroupID is null
        /// </summary>
        /// <param name="items">List of TaxAssessmentItemResults</param>
        /// <returns></returns>
        public static List<TaxAssessmentItemResult> Compress(this List<TaxAssessmentItemResult> items)
        {
            var dupeInvoiceText = (from i in items
                                  group i by i.InvoiceText into g
                                  where g.Count() > 1
                                  select g.Key).ToArray();

            var duplicates = items.FindAll(a => dupeInvoiceText.Contains(a.InvoiceText));
            duplicates.ForEach(d => items.Remove(d));

            var combinedDuplicates = new List<TaxAssessmentItemResult>();
            for (int i = 0; i < dupeInvoiceText.Count(); i++)
            {
                var sameIDs = duplicates.Where(d => d.InvoiceText.Equals(dupeInvoiceText[i]));
                TaxAssessmentItemResult firstDupe = sameIDs.First();

                decimal sumTaxAmount = 0.0m;
                foreach (TaxAssessmentItemResult same in sameIDs)
                    sumTaxAmount += same.TaxAmount;

                firstDupe.TaxAmount = sumTaxAmount;
                combinedDuplicates.Add(firstDupe);
            }

            items.AddRange(combinedDuplicates);
            return items;
        }

        ///// <summary>
        ///// This method is used to cast the result to a particular type.  
        ///// Unlike System.IConvertible, it supports nullable types.
        ///// </summary>
        ///// <typeparam name="T">Specified Type</typeparam>
        ///// <param name="obj"></param>
        ///// <returns>Value cast to Specified Type</returns>
        //public static T To<T>(this IConvertible obj)
        //{
        //    Type t = typeof(T);
        //    Type u = Nullable.GetUnderlyingType(t);

        //    if (u != null)
        //    {
        //        return (obj == null) ? default(T) : (T)Convert.ChangeType(obj, u);
        //    }
        //    else
        //    {
        //        return (T)Convert.ChangeType(obj, t);
        //    }
        //}
    }
}
