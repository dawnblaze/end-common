﻿

namespace Endor.Pricing
{
    /// <summary>
    /// The TaxAssessmentItemRequest is only used when the tax rates are being manually supplied (and not looked up from the database or TaxJar).
    /// </summary>
    public class TaxAssessmentNexusItem
    {
        // optional TaxItemID
        public short? TaxItemID { get; set; }

        // The text displayed for this tax assessment on the invoice
        public string InvoiceText { get; set; }

        // The Tax Rate, stored as a percent  (e.g 7.25% would be 7.25)
        public decimal TaxRate { get; set; }
    }
}