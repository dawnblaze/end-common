﻿using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Pricing
{
    public class TaxEngine
    {
        /// <summary>
        /// Instantiates a TaxEngine
        /// </summary>
        /// <param name="bid">Business ID</param>
        /// <param name="context">API Context</param>
        public TaxEngine(short bid, ApiContext context)
        {
            this.BID = bid;
            this.Context = context;
        }

        private short BID { get; set; }
        private ApiContext Context { get; set; }

        public List<TaxAssessmentResult> Compute(OrderPriceRequest request, bool ComputeTaxes)
        {
            List<TaxAssessmentResult> result = new List<TaxAssessmentResult>();

            if (request.Items != null)
            {
                foreach (ItemPriceRequest itemRequest in request.Items)
                {
                    if (itemRequest.TaxNexusList == null)
                        itemRequest.TaxNexusList = request.TaxNexusList;

                    result.AddRange(Compute(itemRequest, ComputeTaxes));
                }
            }

            if (request.Destinations != null)
            {
                foreach (DestinationPriceRequest destRequest in request.Destinations)
                {
                    if (destRequest.TaxNexusList == null)
                        destRequest.TaxNexusList = request.TaxNexusList;

                    result.Add(Compute(destRequest, ComputeTaxes));
                }
            }

            return result.Compress();
        }

        public List<TaxAssessmentResult> Compute(ItemPriceRequest request, bool ComputeTaxes)
        {
            List<TaxAssessmentResult> result = new List<TaxAssessmentResult>();
          
            if (request.TaxInfoList != null)
            {
                var taxInfoQuantitySum = request.TaxInfoList.Sum(x => x.Quantity);
                if (taxInfoQuantitySum != request.Quantity)
                    throw new Exception($"Quantity mismatch exception in tax calculation");

                foreach (TaxAssessmentRequest tarequest in request.TaxInfoList)
                {
                    if (string.IsNullOrWhiteSpace(tarequest.NexusID))
                        throw new Exception($"NexusID not specified.");

#warning END-4373 needs rework
                    if (request.Result != null)
                        tarequest.Price = request.Result.PricePreTax;

                    if (request.TaxNexusList == null)
                        throw new Exception($"Specified NexusID {tarequest.NexusID} not found while Computing Order Item Taxes.");

                    request.TaxNexusList.TryGetValue(tarequest.NexusID, out TaxAssessmentNexus Nexus);
                    if (Nexus == null)
                        throw new Exception($"Specified NexusID {tarequest.NexusID} not found while Computing Order Item Taxes.");

                    if(ComputeTaxes && Nexus.EngineType == TaxEngineType.TaxJar)
                    {
                        //Nexus.ATERegistrationID = 
                    }

                    result.AddRange(Compute(request, tarequest, Nexus));
                }
            }

            //result = result.Compress();

            request.Result.TaxAmount = result.Sum(t => t.TaxAmount);
            request.Result.TaxInfoList = result;
            return result;
        }

        public TaxAssessmentResult Compute(DestinationPriceRequest request, bool ComputeTaxes)
        {
            if (string.IsNullOrWhiteSpace(request.TaxInfo?.NexusID))
                throw new Exception($"NexusID not specified.");

            if (request.TaxNexusList == null)
                throw new Exception($"Specified NexusID {request.TaxInfo.NexusID} not found while Computing Destination Taxes.");

            request.TaxNexusList.TryGetValue((request.TaxInfo.NexusID), out TaxAssessmentNexus Nexus);

            if (Nexus == null)
                throw new Exception($"Specified NexusID {request.TaxInfo.NexusID} not found while Computing Destination Taxes.");

            if (request.Result == null)
                request.TaxInfo.Price = request.PriceNet;
            else
                request.TaxInfo.Price = request.Result.PricePreTax;

            TaxAssessmentResult result = Compute(request.TransactionType, request.TaxInfo, Nexus);
            request.Result.TaxAmount = result.TaxAmount;
            request.Result.TaxInfoList = new List<TaxAssessmentResult>()
            {
                result
            };
            return result;
        }

        public List<TaxAssessmentResult> Compute(ItemPriceRequest itemRequest, TaxAssessmentRequest request, TaxAssessmentNexus nexus)
        {
            List<TaxAssessmentResult> result = new List<TaxAssessmentResult>();
            if (itemRequest.Result.Components != null && itemRequest.Result.Components.Count > 0)
            {
                foreach (var componentResult in itemRequest.Result.Components)
                {
                    var SubAssessmentRequest = new TaxAssessmentRequest()
                    {
                        Quantity = componentResult.TotalQuantity.GetValueOrDefault(),
                        Price = componentResult.PriceTaxable,
                        IsTaxExempt = request.IsTaxExempt,
                        NexusID = request.NexusID
                    };
                    result.AddRange(Compute(itemRequest.TransactionType, componentResult, SubAssessmentRequest, nexus));
                }
            }
            if (itemRequest.Result.Surcharges != null && itemRequest.Result.Surcharges.Count > 0)
            {
                foreach (var surchargeResult in itemRequest.Result.Surcharges)
                {
                    var SubAssessmentRequest = new TaxAssessmentRequest()
                    {
                        Quantity = itemRequest.Quantity,
                        Price = surchargeResult.PriceTaxable,
                        IsTaxExempt = request.IsTaxExempt,
                        NexusID = request.NexusID
                    };
                    nexus.TaxabilityCode = Context.SurchargeDef.Include(x => x.TaxCode).First(x => x.BID == BID && x.ID == surchargeResult.SurchargeDefID).TaxCode.TaxCode;
                    var taxResult = Compute(itemRequest.TransactionType, SubAssessmentRequest, nexus);
                    surchargeResult.TaxInfoList = new List<TaxAssessmentResult>() { taxResult };
                    result.Add(taxResult);
                }
            }


            return result;
        }

        private List<TaxAssessmentResult> Compute(byte TransactionType, ComponentPriceResult componentResult, TaxAssessmentRequest request, TaxAssessmentNexus nexus)
        {
            List<TaxAssessmentResult> result = new List<TaxAssessmentResult>();
            if (componentResult.ChildComponents!=null && componentResult.ChildComponents.Where(x => x.ComponentType == OrderItemComponentType.Assembly).Count() > 0)
            {
                foreach (ComponentPriceResult componentSubResult in componentResult.ChildComponents.Where(x => x.ComponentType == OrderItemComponentType.Assembly))
                {
                    if (componentSubResult.RollupLinkedPriceAndCost.GetValueOrDefault(true))
                    {
                        var taxResult = ComponentComputeTax(TransactionType, componentResult, request, nexus);
                        result.Add(taxResult);
                    }
                    else
                    {
                        if (componentSubResult.ComponentType == OrderItemComponentType.Assembly)
                        {
                            var SubAssessmentRequest = new TaxAssessmentRequest()
                            {
                                Quantity = request.Quantity * componentSubResult.TotalQuantity.GetValueOrDefault(),
                                Price = componentSubResult.PricePreTax - componentSubResult.PricePreTax,
                                IsTaxExempt = request.IsTaxExempt,
                                NexusID = request.NexusID
                            };
                            result.AddRange(Compute(TransactionType, componentSubResult, SubAssessmentRequest, nexus));
                        };
                    }
                }
            } else
            {
                var taxResult = ComponentComputeTax(TransactionType,componentResult, request, nexus);
                result.Add(taxResult);
            }
            return result;
        }

        private TaxAssessmentResult ComponentComputeTax(byte TransactionType, ComponentPriceResult componentResult, TaxAssessmentRequest request, TaxAssessmentNexus nexus)
        {
            short? taxCodeID = null;
            switch (componentResult.ComponentType)
            {
                case OrderItemComponentType.Assembly:
                    taxCodeID = Context.AssemblyData.First(x => x.BID == BID && x.ID == componentResult.ComponentID.GetValueOrDefault()).TaxabilityCodeID;                   
                    break;
                case OrderItemComponentType.Labor:
                    taxCodeID = Context.LaborData.First(x => x.BID == BID && x.ID == componentResult.ComponentID.GetValueOrDefault()).TaxCodeID;
                    break;
                case OrderItemComponentType.Machine:
                    taxCodeID = Context.MachineData.First(x => x.BID == BID && x.ID == componentResult.ComponentID.GetValueOrDefault()).TaxCodeID;
                    break;
                case OrderItemComponentType.Material:
                    taxCodeID = Context.MaterialData.First(x => x.BID == BID && x.ID == componentResult.ComponentID.GetValueOrDefault()).TaxCodeID;
                    break;
            }
            if (taxCodeID != null)
            {
                nexus.TaxabilityCode = Context.TaxabilityCodes.First(x => x.BID == BID && x.ID == taxCodeID.GetValueOrDefault()).TaxCode;
            }
            var taxResult = Compute(TransactionType, request, nexus);
            componentResult.TaxInfoList = new List<TaxAssessmentResult>() { taxResult };
            componentResult.PriceTax = componentResult.TaxInfoList.Sum(x => x.TaxAmount);
            return taxResult;
        }

        public TaxAssessmentResult Compute(byte TransactionType, TaxAssessmentRequest request, TaxAssessmentNexus nexus)
        {
            if (request.IsTaxExempt)
                return TaxEngine_Exempt.Compute(BID, Context, request, nexus);

            else
                switch (nexus.EngineType)
                {
                    case TaxEngineType.Exempt:
                        return TaxEngine_Exempt.Compute(BID, Context, request, nexus);
                    case TaxEngineType.Supplied:
                        return TaxEngine_Supplied.Compute(request, nexus);
                    case TaxEngineType.Internal:
                        return TaxEngine_Internal.Compute(BID, Context, request, nexus);
                    case TaxEngineType.TaxJar:
                        var result = TaxEngine_TaxJar.Compute(TransactionType, BID, Context, request, nexus);
                        return result;
                    default:
                        throw new InvalidOperationException("Unknown Tax Engine Specified.");
                }
        }
    }
}
