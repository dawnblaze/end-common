﻿using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;

using System.Linq;

namespace Endor.Pricing
{
    /// <summary>
    /// TaxEngine to use when pulling tax data directly from DB for tax computations
    /// </summary>
    public static class TaxEngine_Internal
    {
        /// <summary>
        /// Computes the Taxes for a TaxAssessmentRequest
        /// </summary>
        /// <param name="BID">Business ID</param>
        /// <param name="ctx">API Context</param>
        /// <param name="request">TaxAssessmentRequest object</param>
        /// <returns>TaxAssessmentResult object</returns>
        public static TaxAssessmentResult Compute(short BID, ApiContext ctx, TaxAssessmentRequest request, TaxAssessmentNexus nexus)
        {
            decimal taxableAmount = request.Price.GetValueOrDefault(0m);

            if (request.PriceTaxable.HasValue)
                taxableAmount = Math.Min(taxableAmount, request.PriceTaxable.Value);

            TaxAssessmentResult result = new TaxAssessmentResult()
            {
                NexusID = request.NexusID,
                Quantity = request.Quantity,
                Price = request.Price.GetValueOrDefault(0m),
                InvoiceText = nexus.InvoiceText, 
                IsTaxExempt = request.IsTaxExempt,
                PriceTaxable = taxableAmount,
                PriceTaxableOV = request.PriceTaxable.HasValue,
                TaxEngine = nexus.EngineType,
                TaxGroupID = nexus.TaxGroupID,
            };

            TaxGroup taxGroup = ctx.TaxGroup.Include(x => x.TaxGroupItemLinks).ThenInclude(x => x.TaxItem).FirstOrDefault(tg => tg.ID == nexus.TaxGroupID && tg.BID == BID);

            if (taxGroup == null)
                throw new Exception($"Tax group ID={nexus.TaxGroupID} not found.");

            foreach (TaxGroupItemLink itemLink in taxGroup.TaxGroupItemLinks)
            {
                TaxAssessmentItemResult itemResult = new TaxAssessmentItemResult()
                {
                    TaxItemID = itemLink.TaxItem.ID,
                    InvoiceText = itemLink.TaxItem.InvoiceText,
                    TaxRate = itemLink.TaxItem.TaxRate,
                    TaxAmount = Math.Round(((itemLink.TaxItem.TaxRate/100) * taxableAmount), 2),
                    GLAccountID = itemLink.TaxItem.GLAccountID,
                };
                result.AddItem(itemResult);
            }

            return result;
        }
    }
}
