﻿using Endor.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace Endor.Pricing
{
    public class TaxAssessmentNexus
    {
        // Specify the tax engine used for calculating the taxes
        //      Supplied    = 0 --> All tax item rates must be supplied.
        //      TaxGroup    = 1 --> Compute tax rates based on the TaxGroupID
        //      TaxJar     = 2 --> Use TaxJar service 
        [JsonConverter(typeof(StringEnumConverter))]
        public TaxEngineType EngineType { get; set; }

        // The text displayed for this tax assessment on the invoice
        public string InvoiceText { get; set; }

        // Field for TaxEngine = Supplied (0)
        // ==============================================
        public List<TaxAssessmentNexusItem> TaxAssessmentItems { get; set; }

        // Field for TaxEngine = Internal (1)
        // ==============================================
        public short? TaxGroupID { get; set; }

        // Field for TaxEngine = TaxJar (2)
        // ==============================================
        //public AddressRoute Address { get; set; }
        public Address FromAddress { get; set; }
        public string TaxabilityCode {get; set;}  // the products taxability code.

        // Set UseSameToFromAddress = True for local pickup, Otherwise the ToAddress must be supplied 
        public bool? UseSameToFromAddress { get; set; }
        public Address ToAddress { get; set; }

        // External Tax Service can use the company name or ID for specialized tax lookup
        public string CompanyName { get; set; }
        public int? CompanyID { get; set; }

        /// <summary>
        /// Registration ID
        /// </summary>
        public Guid ATERegistrationID { get; set; }

        /// <summary>
        /// ATE api URL
        /// </summary>
        public string ATEApiUrl { get; set; }

    }
}
