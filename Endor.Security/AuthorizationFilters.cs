﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Endor.Security.Interfaces;

using Endor.Security.Extensions;
using Endor.Tenant;

namespace Endor.Security.AuthorizationFilters
{
    /// <summary>
    /// Used to check if the user has all of the Security Rights needed
    /// </summary>
    public class HasAllSecurityRights : AuthorizeAttribute, IAuthorizationFilter​
    {
        private readonly SecurityRight[] _securityRights;

        /// <summary>
        /// Used to check if the user has all of the Security Rights needed
        /// </summary>
        /// <param name="securityRights">Security Rights to check the user has</param>
        public HasAllSecurityRights(SecurityRight[] securityRights)
        {
            _securityRights = securityRights;
        }

        /// <summary>
        /// Actions to perform when trying to Authorize the user.
        /// </summary>
        /// <param name="context">Authorization Filter Context</param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var rightsCache = context.HttpContext.RequestServices.GetService<IRightsCache>();
            var userBID = context.HttpContext.User.BID().Value;
            var userLinkID = context.HttpContext.User.UserLinkID().Value;

            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                bool hasClaim = rightsCache.HasAllRights(userBID, userLinkID, _securityRights);
                if (!hasClaim)
                {
                    context.Result = new ForbidResult();
                }
            }
        }
    }

    /// <summary>
    /// Used to check if the user has any of the Security Rights needed
    /// </summary>
    public class HasAnySecurityRight : AuthorizeAttribute, IAuthorizationFilter​
    {
        private readonly SecurityRight[] _securityRights;
        /// <summary>
        /// Used to check if the user has any of the Security Rights needed
        /// </summary>
        /// <param name="securityRights">Security Rights to check the user has</param>
        public HasAnySecurityRight(SecurityRight[] securityRights)
        {
            _securityRights = securityRights;
        }

        /// <summary>
        /// Actions to perform when trying to Authorize the user.
        /// </summary>
        /// <param name="context"></param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var rightsCache = context.HttpContext.RequestServices.GetService<IRightsCache>();
            var userBID = context.HttpContext.User.BID().Value;
            var userLinkID = context.HttpContext.User.UserLinkID().Value;

            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                bool hasClaim = rightsCache.HasAnyRight(userBID, userLinkID, _securityRights);
                if (!hasClaim)
                {
                    context.Result = new ForbidResult();
                }
            }
        }
    }

    /// <summary>
    /// Used to check if the user has the Security Right needed
    /// </summary>
    public class HasSecurityRight : AuthorizeAttribute, IAuthorizationFilter​
    {
        private readonly SecurityRight _securityRight;
        /// <summary>
        /// Used to check if the user has the Security Right needed
        /// </summary>
        /// <param name="securityRight">Security Right to check the user has</param>
        public HasSecurityRight(SecurityRight securityRight)
        {
            _securityRight = securityRight;
        }

        /// <summary>
        /// Actions to perform when trying to Authorize the user.
        /// </summary>
        /// <param name="context"></param>
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var rightsCache = context.HttpContext.RequestServices.GetService<IRightsCache>();
            var userBID = context.HttpContext.User.BID().Value;
            var userLinkID = context.HttpContext.User.UserLinkID().Value;

            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                bool hasClaim = rightsCache.HasRight(userBID, userLinkID, _securityRight);
                if (!hasClaim)
                {
                    context.Result = new ForbidResult();
                }
            }
        }
    }

}
