﻿using System.Collections.Generic;

namespace Endor.Security.Interfaces
{
    public interface IRightsCache
    {
        void ClearCache(short BID);
        void ClearCacheByPermission(short BID, int permissionGroupID);
        void ClearCacheByUser(short BID, short userLinkID);
        IEnumerable<SecurityRight> GetRights(short BID, short userLinkID);
        bool HasAllRights(short BID, short userLinkID, SecurityRight[] securityRights);
        bool HasAnyRight(short BID, short userLinkID, SecurityRight[] securityRights);
        bool HasRight(short BID, short userLinkID, SecurityRight securityRight);
    }
}