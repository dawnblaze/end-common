﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Security.Interfaces
{
    public interface ICache<TKey, TCacheType>
    {
        TCacheType Get(TKey key);
        void Set(TKey key, TCacheType value);
        TKey[] GetAllKeys();
        void Remove(TKey key);
        void ClearCache();
    }
}
