﻿using System.Threading.Tasks;

namespace Endor.Security.Interfaces
{
    public interface IServerConfigurations
    {
        Task<string> ConnectionString(short BID);
        string AuthorizationToken();
        string RTCMessagingURL();
    }
}
