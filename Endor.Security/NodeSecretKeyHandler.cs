﻿#if NETCOREAPP1_1 || NETCOREAPP2_0
using Microsoft.AspNetCore.Authorization;
#endif
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Security
{
#if NETCOREAPP1_1 || NETCOREAPP2_0
    public class NodeSecretKeyHandler : AuthorizationHandler<NodeSecretKeyRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, NodeSecretKeyRequirement requirement)
        {
            if (context.User.HasClaim(c => c.Type == Common.ClaimNameConstants.TenantSecret && c.Value == requirement.SecretKey))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
#endif
}
