﻿using System;

namespace Endor.Common
{
    public static class ClaimNameConstants
    {
        public const string Rights = "rights";
        public const string BID = "bid";
        public const string UserID = "userid";
        public const string EmployeeID = "employeeid";
        public const string TenantSecret = "tenantsecret";
        public const string UserLinkID = "userlinkid";
        public const string UserName = "username";
        public const string AID = "aid";
        public const string AccessType = "accesstype";
    }
}
