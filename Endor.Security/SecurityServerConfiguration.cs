﻿using Endor.Security.Interfaces;
using Endor.Tenant;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace Endor.Security
{
    /// <summary>
    /// Security Server Configuration
    /// </summary>
    public class SecurityServerConfiguration : IServerConfigurations
    {
        private readonly ITenantDataCache tenantDataCache;
        private readonly string messagingServerUrl;
        private readonly IHttpContextAccessor httpContextAccessor;

        /// <summary>
        /// Security Server Configuration
        /// </summary>
        /// <param name="tenantDataCache">Tenant Data Cache</param>
        /// <param name="options">Endor Options</param>
        /// <param name="httpContextAccessor">Http Context Accessor</param>
        public SecurityServerConfiguration(ITenantDataCache tenantDataCache, string messagingServerUrl, IHttpContextAccessor httpContextAccessor)
        {
            this.tenantDataCache = tenantDataCache ?? throw new ArgumentNullException(nameof(tenantDataCache));
            this.messagingServerUrl = messagingServerUrl ?? throw new ArgumentNullException(nameof(messagingServerUrl));
            this.httpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
        }

        /// <summary>
        /// Gets the Authorization Token from the Headers
        /// </summary>
        /// <returns></returns>
        public string AuthorizationToken() =>
            httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString();

        /// <summary>
        /// Gets the DB Connection String for the given Business
        /// </summary>
        /// <param name="BID">Business ID</param>
        /// <returns></returns>
        public async Task<string> ConnectionString(short BID) =>
            (await tenantDataCache.Get(BID)).BusinessDBConnectionString;

        /// <summary>
        /// Gets the RTC Messaging URL from the Endor Options
        /// </summary>
        /// <returns></returns>
        public string RTCMessagingURL() => this.messagingServerUrl;
    }
}
