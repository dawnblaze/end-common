﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Security.Extensions
{
    public static class ServiceProviderExtensions
    {
        public static TServiceImplementation GetService<TServiceImplementation>(this IServiceProvider provider) =>
            (TServiceImplementation)provider.GetService(typeof(TServiceImplementation));
    }
}
