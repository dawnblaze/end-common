﻿using Endor.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;

namespace Endor.Security
{
    public static class UserExtensions
    {
        public static short? BID(this IPrincipal user)
        {
            return GetFromClaim<short>(user, ClaimNameConstants.BID, short.TryParse);
        }

        public static byte? AID(this IPrincipal user)
        {
            return GetFromClaim<byte>(user, ClaimNameConstants.AID, byte.TryParse);
        }

        [Obsolete]
        public static short? BID(this IIdentity identity)
        {
            return GetFromClaim<short>(identity, ClaimNameConstants.BID, short.TryParse);
        }

        public static int? UserID(this IPrincipal user)
        {
            return GetFromClaim<int>(user, ClaimNameConstants.UserID, int.TryParse);
        }

        public static string UserName(this IIdentity identity)
        {
            return (identity as ClaimsIdentity).FindFirst(ClaimNameConstants.UserName)?.Value;
        }

        public static string UserName(this IPrincipal user)
        {
            if (user?.Identity is ClaimsIdentity)
            {
                ClaimsIdentity claimsIdentity = (ClaimsIdentity)user.Identity;
                return claimsIdentity.FindFirst(ClaimNameConstants.UserName)?.Value;
            }
            else
            {
                return null;
            }
        }


        [Obsolete]
        public static int? UserID(this IIdentity identity)
        {
            return GetFromClaim<int>(identity, ClaimNameConstants.UserID, int.TryParse);
        }

        public static short? EmployeeID(this IPrincipal user)
        {
            return GetFromClaim<short>(user, ClaimNameConstants.EmployeeID, short.TryParse);
        }

        public static short? UserLinkID(this IPrincipal user)
        {
            return GetFromClaim<short>(user, ClaimNameConstants.UserLinkID, short.TryParse);
        }

        public static BitArray Rights(this IPrincipal user)
        {
            if (user?.Identity is ClaimsIdentity)
            {
                string rawBase64 = (user.Identity as ClaimsIdentity).FindFirst(ClaimNameConstants.Rights)?.Value;
                return Endor.Security.Rights.FromBase64(rawBase64);
            }
            else
            {
                return null;
            }
        }

        [Obsolete]
        public static BitArray Rights(this IIdentity identity)
        {
            return (identity is ClaimsIdentity) ? Endor.Security.Rights.FromBase64((identity as ClaimsIdentity).FindFirst(ClaimNameConstants.Rights).Value) : null;
        }

        public static bool IsAuthorized(this IPrincipal user, SecurityRight right)
        {
            BitArray rights = user.Rights();
            return rights == null ? false : right.Authorized(rights);
        }

        [Obsolete]
        public static bool IsAuthorized(this IIdentity identity, SecurityRight right)
        {
            BitArray rights = identity.Rights();
            return rights == null ? false : right.Authorized(rights);
        }
        
        public static string RightsBitString(this IPrincipal user)
        {
            BitArray rights = user.Rights();

            if (rights == null)
                return null;
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach (bool bit in rights)
                {
                    sb.Append(Convert.ToInt32(bit).ToString());
                }
                return sb.ToString();
            }
        }

        [Obsolete]
        public static string RightsBitString(this IIdentity identity)
        {
            BitArray rights = identity.Rights();

            if (rights == null)
                return null;
            else
            {
                StringBuilder sb = new StringBuilder();
                foreach(bool bit in rights)
                {
                    sb.Append(Convert.ToInt32(bit).ToString());
                }
                return sb.ToString();
            }
        }

        delegate bool TryParser<T>(string rawInput, out T output);
        private static T? GetFromClaim<T>(IPrincipal user, string claimName, TryParser<T> tryParse)
            where T: struct
        {
            return GetFromClaim(user?.Identity, claimName, tryParse);
        }
        private static T? GetFromClaim<T>(IIdentity identity, string claimName, TryParser<T> tryParse)
            where T : struct
        {
            ClaimsIdentity ident = identity as ClaimsIdentity;
            if (ident != null)
            {
                return GetFromClaim(ident, claimName, tryParse);
            }
            else
            {
                return null;
            }
        }
        private static T? GetFromClaim<T>(ClaimsIdentity claimIdentity, string claimName, TryParser<T> tryParse)
            where T : struct
        {
            string rawShort = claimIdentity.FindFirst(claimName)?.Value;
            T result;
            if (tryParse(rawShort, out result))
                return result;
            else
                return (T?)null;
        }
    }
}
