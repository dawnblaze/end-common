﻿using Endor.Security.Interfaces;
using Endor.Security.Repositories;
using Endor.Security.Repositories.Interfaces;
using Endor.Security.RTC;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Endor.Security
{
    /// <summary>
    /// This class should be added to a depedency injection as singleton to make the caching possible
    /// </summary>
    public sealed class RightsCache : ConcurrentCache<short, BusinessRightCache>, IRightsCache
    {
        /// <summary>
        /// This gets the connection string of the current tenant bid
        /// </summary>
        private readonly IServerConfigurations _serverConfigurations;
        private readonly IUserRightsWithSourceRepository _userRightsWithSourceRepository;

        /// <summary>
        /// The server must implement IServerConfigurations to grab server configurations
        /// </summary>
        /// <param name="serverConfigurations"></param>
        public RightsCache(IServerConfigurations serverConfigurations, IUserRightsWithSourceRepository userRightsWithSourceRepository = null)
        {
            _serverConfigurations = serverConfigurations ?? throw new ArgumentNullException(nameof(serverConfigurations));
            _userRightsWithSourceRepository = userRightsWithSourceRepository ?? new UserRightsWithSourceRepository();

            UserLinkRefreshListener.Init(serverConfigurations, 
                (parameter) => ReloadBusinessCache(parameter.BID, parameter.UserLinkID),
                (parameter) => ClearBusinessCache(parameter.BID, parameter.UserLinkID)
            );
        }

        private BusinessRightCache CreateBusinessAndUserLinkIDForCache(short BID, short userLinkID)
        {
            var businessCacheForUserLink = new BusinessRightCache();
            businessCacheForUserLink.SetRight(userLinkID, CreateUserRightsForCache(BID, userLinkID));
            return businessCacheForUserLink;
        }

        private UserRights CreateUserRightsForCache(short BID, short userLinkID)
        {
            var connString = _serverConfigurations.ConnectionString(BID).GetAwaiter().GetResult();

            _userRightsWithSourceRepository.SetConnectionString(connString);

            var userRights = _userRightsWithSourceRepository.GetRightsBy(BID, userLinkID);

            return new UserRights
            {
                BID = BID,
                RightsGroup = userRights.Select(a => a.GroupID).Distinct(),
                SecurityRights = userRights.Select(a => (SecurityRight)a.RightID)
            };
        }

        private BusinessRightCache ReloadBusinessCache(short BID, short userLinkID)
        {
            var businessCache = CreateBusinessAndUserLinkIDForCache(BID, userLinkID);
            Set(BID, businessCache);
            return businessCache;
        }

        private void ClearBusinessCache(short BID, short userLinkID)
        {
            try
            {
                var businessCache = Get(BID);
                businessCache.ClearCacheByUser(userLinkID);
            }
            catch (KeyNotFoundException)
            {
                //Creates the cache when the current BID is not even loaded
                ReloadBusinessCache(BID, userLinkID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool HasRight(short BID, short userLinkID, SecurityRight securityRight)
        {
            try
            {
                var currentBusinessCache = Get(BID);

                var hasRight = currentBusinessCache.HasRight(userLinkID, securityRight);

                // ONLY reloads cache if there is no UserLinkId on cache already
                if (!hasRight && !currentBusinessCache.GetRights(userLinkID).Any())
                    return ReloadBusinessCache(BID, userLinkID).HasRight(userLinkID, securityRight);

                return hasRight;
            }
            catch (KeyNotFoundException)
            {
                //Creates the cache when the current BID is not even loaded
                return ReloadBusinessCache(BID, userLinkID).HasRight(userLinkID, securityRight);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool HasAnyRight(short BID, short userLinkID, SecurityRight[] securityRights)
        {
            try
            {
                var currentBusinessCache = Get(BID);

                var hasRight = currentBusinessCache.HasAnyRight(userLinkID, securityRights);

                // ONLY reloads cache if there is no UserLinkId on cache already
                if (!hasRight && !currentBusinessCache.GetRights(userLinkID).Any())
                    return ReloadBusinessCache(BID, userLinkID).HasAnyRight(userLinkID, securityRights);

                return hasRight;
            }
            catch (KeyNotFoundException)
            {
                //Creates the cache when the current BID is not even loaded
                return ReloadBusinessCache(BID, userLinkID).HasAnyRight(userLinkID, securityRights);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool HasAllRights(short BID, short userLinkID, SecurityRight[] securityRights)
        {
            try
            {
                var currentBusinessCache = Get(BID);

                var hasRight = currentBusinessCache.HasAllRights(userLinkID, securityRights);

                // ONLY reloads cache if there is no UserLinkId on cache already
                if (!hasRight && !currentBusinessCache.GetRights(userLinkID).Any())
                    return ReloadBusinessCache(BID, userLinkID).HasAllRights(userLinkID, securityRights);

                return hasRight;
            }
            catch (KeyNotFoundException)
            {
                //Creates the cache when the current BID is not even loaded
                return ReloadBusinessCache(BID, userLinkID).HasAllRights(userLinkID, securityRights);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<SecurityRight> GetRights(short BID, short userLinkID) =>
            Get(BID).GetRights(userLinkID);

        public void ClearCache(short BID) =>
            Get(BID).ClearCache();

        public void ClearCacheByUser(short BID, short userLinkID) =>
            Get(BID).ClearCacheByUser(userLinkID);

        public void ClearCacheByPermission(short BID, int permissionGroupID) =>
            Get(BID).ClearCacheByPermission(permissionGroupID);
    }
}
