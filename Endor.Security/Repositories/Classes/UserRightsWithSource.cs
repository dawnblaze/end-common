﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Security.Repositories.Classes
{
    /// <summary>
    /// Àvailable RightID	RightName	GroupID	GroupName	Sources
    /// </summary>
    public class UserRightsWithSourceResult
    {
        public int RightID { get; set; }
        public int GroupID { get; set; }
    }

    public class UserRightsWithSourceQuery
    {
        public int BID { get; set; }
        public int UserLinkID { get; set; }

    }
}
