﻿using Endor.Security.Repositories.Classes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Security.Repositories.Interfaces
{
    public interface IUserRightsWithSourceRepository : IRepository
    {
        IEnumerable<UserRightsWithSourceResult> GetRightsBy(short BID, short userLinkID);
    }
}
