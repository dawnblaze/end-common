﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Security.Repositories.Interfaces
{
    public interface IRepository
    {
        void SetConnectionString(string connectionString);
    }
}
