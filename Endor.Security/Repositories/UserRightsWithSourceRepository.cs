﻿using Dapper;
using Endor.Security.Repositories.Classes;
using Endor.Security.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Security.Repositories
{
    internal class UserRightsWithSourceRepository : IUserRightsWithSourceRepository
    {
        private string ConnectionString { get; set; }

        IEnumerable<UserRightsWithSourceResult> IUserRightsWithSourceRepository.GetRightsBy(short BID, short userLinkID)
        {
            if (string.IsNullOrWhiteSpace(ConnectionString))
                throw new NullReferenceException("The connection string is empty");

            using (var connection = new System.Data.SqlClient.SqlConnection(ConnectionString))
            {
                var query = @"SELECT RightID, GroupID FROM [dbo].[User.Security.RightsBySource] (@BID,@UserLinkID)";

                return connection.Query<UserRightsWithSourceResult>(query, new UserRightsWithSourceQuery
                {
                    BID = BID,
                    UserLinkID = userLinkID
                });
            }
        }

        void IRepository.SetConnectionString(string connectionString) => ConnectionString = connectionString;
    }
}
