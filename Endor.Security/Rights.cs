﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Endor.Security
{
    public enum SecurityRight
    {
        AccessModuleManagement = 1,
        AccessModuleAccounting = 2,
        AccessModuleSales = 4,
        AccessModuleProduction = 8,
        AccessModulePurchasing = 16,
        AccessModuleEcommerce = 32,
        AccessModuleUser = 128,
        AccessModuleDevOps = 256,
        AccessLocationSetup = 301,
        CanCreateNewLocations = 302,
        CanEditLocations = 303,
        AccessEmployeeSetup = 304,
        CanManageEmployees = 305,
        AccessAdjustTimeCards = 306,
        AccessTeamSetup = 307,
        CanManageTeams = 308,
        AccessPermissionSetup = 309,
        CanManagePermissionGroups = 310,
        AccessSalesGoalsSetup = 311,
        CanManageSalesGoals = 312,
        AccessAlertSetup = 313,
        CanManageAlerts = 314,
        AccessAutomationSetup = 315,
        CanManageAutomations = 316,
        AccessAutomationHistory = 317,
        CanManageAutomationHistory = 318,
        AccessBoardSetup = 319,
        CanManageBoardSetup = 320,
        CanFavoriteOthersBoards = 321,
        AccessMaterialSetup = 322,
        CanEditMaterialSetup = 323,
        AccessLaborSetup = 324,
        CanEditLaborSetup = 325,
        AccessMachineSetup = 326,
        CanEditMachineSetup = 327,
        AccessMachineTemplateSetup = 328,
        CanEditMachineTemplateSetup = 329,
        AccessAssemblySetup = 330,
        CanEditAssemblySetup = 331,
        AccessAccountingOptionsSetup = 332,
        CanEditAccountingOptionsSetup = 333,
        AccessPurchasingOptionsSetup = 334,
        CanEditPurchasingOptionsSetup = 335,
        AccessCustomFieldSetup = 336,
        CanEditCustomFieldSetup = 337,
        AccessReasonListSetup = 338,
        CanEditReasonListSetup = 339,
        AccessCustomerOptionsSetup = 340,
        CanEditCustomerOptionsSetup = 341,
        AccessCustomerListSetup = 342,
        CanEditCustomerListSetup = 343,
        AccessVendorOptionSetup = 344,
        CanEditVendorOptionSetup = 345,
        AccessOrderOptionsSetup = 346,
        CanEditOrderOptionsSetup = 347,
        AccessEstimateOptionsSetup = 348,
        CanEditEstimateOptionsSetup = 349,
        AccessStatusSetup = 350,
        CanEditStatusSetup = 351,
        AccessOtherEmployeeOptions = 352,
        CanEditOtherEmployeeOptions = 353,
        AccessTimeClockOptions = 354,
        CanEditTimeClockOptions = 355,
        AccessPricingTierSetup = 356,
        CanEditPricingTierSetup = 357,
        AccessCommissionPlanSetup = 358,
        CanEditCommissionPlanSetup = 359,
        AccessRoyaltyPlanSetup = 360,
        CanEditRoyaltyPlanSetup = 361,
        Access3rdPartyIntegrationSetup = 362,
        CanEdit3rdPartyIntegrationSetup = 363,
        AccessSystemOptionSetup = 364,
        CanEditSystemOptionSetup = 365,
        AccessGlobalSettings = 366,
        CanEditMySalesDashboard = 401,
        CanViewWidgetGoals = 402,
        CanViewWidgetCompanySales = 403,
        CanViewSalesBoards = 404,
        CanMoveItemsonSalesBoards = 405,
        CanFavoriteMySalesBoards = 406,
        CanTagItemsonSalesBoards = 407,
        CanViewMyOrders = 408,
        CanViewAllOrders = 409,
        CanViewMyEstimates = 410,
        CanViewAllEstimates = 411,
        CanViewPrices = 412,
        CanViewPartCosts = 413,
        AccessOrders = 414,
        CanCreateandEditMyOrders = 415,
        CanViewOthersOrders = 416,
        CanCreateandEditOthersOrders = 417,
        CanChangeItemStatus = 418,
        CanChangeOrderStatustoBuilt = 419,
        CanChangeOrderStatustoInvoicing = 420,
        CanChangeOrderStatustoInvoiced = 421,
        CanConvertEstimate = 422,
        CanVoidorCloseEstimate = 423,
        AccessEstimates = 424,
        CanCreateandEditMyEstimates = 425,
        CanViewOthersEstimates = 426,
        CanCreateandEditOthersEstimates = 427,
        AccessGlobalQuickProducts = 428,
        CanVoidOrders = 429,
        CanChangeTaxGroups = 430,
        CanChangeSalesLocation = 431,
        CanOverridePrices = 432,
        CanOverrideandAddParts = 433,
        CanOverridePartsCosts = 434,
        CanOverrideSurcharges = 435,
        CanApplyOrderDiscounts = 436,
        CanOverrideOrderValidations = 437,
        CanChangeProductStatusesandSubstatuses = 438,
        CanOverrideCustomerPayments = 439,
        CanEditInvoicedandClosedOrders = 440,
        CanChangeSalespersonOnOrder = 441,
        CanEditMyQuickProducts = 442,
        CanViewCustomerQuickProducts = 443,
        CanEditCustomerQuickProducts = 444,
        CanViewGlobalQuickProducts = 445,
        CanEditGlobalQuickProducts = 446,
        AccessCompanies = 447,
        CanEditMyCompanies = 448,
        CanViewOthersCompanies = 449,
        CanEditOthersCompanies = 450,
        AccessContacts = 451,
        CanEditMyContacts = 452,
        CanViewOthersContacts = 453,
        CanEditOthersContacts = 454,
        AccessCustomerPortal = 455,
        CanSetupCustomerPortalAccess = 456,
        CanAdjustCustomerSpecificPricing = 457,
        CanChangeCustomerSalesperson = 458,
        CanGiveCustomerCredit = 459,
        AccessOpportunties = 460,
        CanViewOthersOpportunities = 461,
        CanCreateandEditMyOpportunities = 462,
        CanCreateandEditOthersOpportunities = 463,
        AccessCalendars = 464,
        CanViewOthersCalendars = 465,
        CanCreateTasksforOthers = 466,
        CanEditTasksforOthers = 467,
        CanCreatePurchaseOrder = 468,
        CanEditMyProductionDashboard = 501,
        CanViewWidgetsforProduction = 502,
        CanViewWidgetsforDesign = 503,
        AccessProductionBoards = 504,
        CanMoveItemsonProductionBoards = 505,
        CanAdjustMachineSchedule = 506,
        CanEditMyAccountingDashboard = 550,
        CanViewWidgetsforGL = 551,
        CanViewWidgetsforPayments = 552,
        AccessAccountingBoards = 553,
        CanMoveItemsonAccountingBoards = 554,
        AccessPayments = 555,
        CanApplyPayments = 556,
        CanApplyCustomerCredit = 557,
        CanVoidandRefundPayments = 558,
        AccessReconciliations = 559,
        CanPostReconcilliation = 560,
        CanReopenReconciliation = 561,
        CanInitiateAccountingSync = 562,
        CanExportAccountingData = 563,
        CanEditMyPurchasingDashboard = 601,
        CanViewWidgetsforPurchaseOrder = 602,
        CanViewWidgetsforVendors = 603,
        AccessPurchasingBoards = 604,
        CanMoveItemsonPurchasingBoards = 605,
        AccessVendors = 606,
        AccessVendorCatalogs = 607,
        CanEditVendorCatalogs = 608,
        AccessPurchaseOrders = 609,
        CanApprovePurchaseOrders = 610,
        CanOrderPurchaseOrders = 611,
        CanVoidPurchaseOrders = 612,
        AccessInventory = 613,
        CanEditInventory = 614,
        CanEditEcommerceOrders = 651,
        CanCreateEcommerceOrders = 652,
        AccessMyProfile = 701,
        CanEditMyProfile = 702,
        AccessTimeClock = 703,
        CanClockInandOut = 704,
        AccessAlerts = 705,
        CanCreatePersonalAlerts = 706,
        AccessMessaging = 707,
        CanSendMessages = 708,
        AccessMyCalendar = 709,
        CanInteractwithMyCalendar = 710,
        AccessMyDocuments = 711,
        CanAddtoMyDocuments = 712,
        CanDeletefromMyDocuments = 713
    }

    public static class SecurityRightsExtensions
    {
        public static bool Authorized(this SecurityRight right, BitArray rights)
        {
            return rights[(int)right];
        }
    }

    public static class Rights
    {
        public static bool HasRight(BitArray userRights, SecurityRight right)
        {
            return userRights[(int)right];
        }
        public static bool HasAnyRight(BitArray userRights, SecurityRight[] rights) 
        {
            bool returnVal = false;
            for (int i=0;i<rights.Length;i++)
            {
                returnVal = returnVal || HasRight(userRights, rights[i]);
            }
            return returnVal;
        }

        public static bool HasAllRights(BitArray userRights, SecurityRight[] rights)
        {
            bool returnVal = true;
            for (int i = 0; i < rights.Length; i++)
            {
                returnVal = returnVal && HasRight(userRights, rights[i]);
            }
            return returnVal;
        }

        public static BitArray FromBase64(string base64)
        {
            if (String.IsNullOrWhiteSpace(base64))
            {
                return null;
            }
            else
            {
                switch (base64.Length % 4)
                {
                    case 2: base64 += "=="; break;
                    case 3: base64 += "="; break;
                }
                return new BitArray(Convert.FromBase64String(base64));
            }
        }
    }
}
