﻿using System;
using System.Collections.Generic;
using System.Text;
#if NETCOREAPP1_1 || NETCOREAPP2_0
using Microsoft.AspNetCore.Authorization;
#endif

namespace Endor.Security
{
#if NETCOREAPP1_1 || NETCOREAPP2_0
    public class NodeSecretKeyRequirement : IAuthorizationRequirement
    {
        private readonly string _secretKey;

        public NodeSecretKeyRequirement(string secretKey)
        {
            _secretKey = secretKey;
        }

        public string SecretKey { get { return _secretKey; } }
    }
#endif
}
