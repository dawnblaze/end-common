﻿using Endor.Security.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Security
{
    public abstract class ConcurrentCache<TKey, TCacheType> : ICache<TKey, TCacheType>
    {
        protected readonly ConcurrentDictionary<TKey, TCacheType> _cache;

        public ConcurrentCache()
        {
            _cache = new ConcurrentDictionary<TKey, TCacheType>();
        }

        public TCacheType Get(TKey key)
        {
            if (_cache.TryGetValue(key, out TCacheType cached) && cached is TCacheType)
                return cached;
            throw new KeyNotFoundException($"The key '{key.ToString()}' does not exists on caching dictionary.");
        }

        public void Remove(TKey key)
        {
            while (!_cache.TryRemove(key, out _));
        }

        public TKey[] GetAllKeys() => _cache.Keys.ToArray();

        public void Set(TKey key, TCacheType value) => _cache.AddOrUpdate(key, value, (s, o) => value);

        public void ClearCache() => _cache.Clear();
    }
}
