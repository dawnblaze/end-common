﻿using Endor.Security.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Security
{
    public class BusinessRightCache : ConcurrentCache<short, UserRights>, ICache<short, UserRights>
    {
        public void SetRight(short userLinkID, UserRights userRights) =>
            Set(userLinkID, userRights);

        public bool HasRight(short userLinkID, SecurityRight securityRight) => 
            Get(userLinkID).SecurityRights.Contains(securityRight);

        public bool HasAnyRight(short userLinkID, SecurityRight[] securityRights) => 
            Get(userLinkID).SecurityRights.Any(userRight => securityRights.Contains(userRight));

        public bool HasAllRights(short userLinkID, SecurityRight[] securityRights) =>
            Get(userLinkID).SecurityRights.All(userRight => securityRights.Contains(userRight));

        public IEnumerable<SecurityRight> GetRights(short userLinkID) =>
            Get(userLinkID).SecurityRights;

        public void ClearCacheByUser(short userLinkID) => 
            Remove(userLinkID);

        public void ClearCacheByPermission(int permissionGroupID)
        {
            var availableKeys = GetAllKeys();
            foreach (var key in availableKeys)
                if (Get(key).RightsGroup.Any(a => a == permissionGroupID))
                    Remove(key);
        }
    }
}
