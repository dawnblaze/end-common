﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Security
{
    public class UserRights
    {
        public short BID { get; set; }
        public short UserLinkID { get; set; }
        public IEnumerable<int> RightsGroup { get; set; }
        public IEnumerable<SecurityRight> SecurityRights { get; set; }
    }
}
