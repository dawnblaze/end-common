﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Security.RTC
{
    internal class RefreshMessage
    {
        [JsonProperty("refreshEntities")]
        public List<RefreshEntity> RefreshEntities { get; set; }
    }

    internal class RefreshEntity
    {
        [JsonProperty("bid")]
        public short BID { get; set; }
        [JsonProperty("id")]
        public int ID { get; set; }
        [JsonProperty("classtypeid")]
        public int ClasstypeID { get; set; }
        [JsonProperty("ctid")]
        public int ctid { get; set; }
        [JsonProperty("datetime")]
        public DateTime DateTime { get; set; }
        [JsonProperty("data")]
        public string Data { get; set; }
        [JsonProperty("refreshmessagetype")]
        public RefreshMessageType RefreshMessageType { get; set; }
        [JsonProperty("headingHash", NullValueHandling = NullValueHandling.Ignore)]
        public string HeadingHash { get; set; }
    }

    internal enum RefreshMessageType
    {
        Change, //= Existing Record Changed
        New, //= New Record of Specified Type
        Delete, // = Existing Record Deleted
        CollectionChange, // = Container contents changes, but container unaffected
        Command, // = message to client to take some action (e.g., Logout)
        Information, // = Non - refresh related message (e.g., Toast Message)
    }
}