﻿using Endor.Security.Interfaces;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Security.RTC
{
    internal static class UserLinkRefreshListener
    {
        internal static void Init(IServerConfigurations serverConfigurations, Action<(short BID, short UserLinkID)> reloadBusinessCacheAction, Action<(short BID, short UserLinkID)> deleteBusinessCacheAction)
        {
            var refreshClassTypeIDs = new int[] { 1012 };
            var (RTCMessagingURL, BearerToken) = (serverConfigurations.RTCMessagingURL(), serverConfigurations.AuthorizationToken());

            if (String.IsNullOrWhiteSpace(RTCMessagingURL) || String.IsNullOrWhiteSpace(BearerToken))
                return;

            if (!Uri.IsWellFormedUriString(RTCMessagingURL, UriKind.Absolute))
                throw new ArgumentException("Invalid RTC url", "RTCMessagingURL");
            else if (!RTCMessagingURL.EndsWith("/message/"))
            {
                var builder = new UriBuilder(RTCMessagingURL)
                {
                    Path = "/message",
                    Query = "access_token=" + BearerToken.Replace("Bearer ", "")
                };
                RTCMessagingURL = builder.ToString();
            }

            var Connection = new HubConnectionBuilder().WithUrl(RTCMessagingURL).Build();

            Connection.On<RefreshMessage>("refresh", data =>
            {
                if (data != null && data.RefreshEntities.Any())
                    foreach (var refreshEntity in data.RefreshEntities //Actually is impossible to know if we get ctid or ClasstypeID, there is a issue with JsonProperty, its a big fix
                        .Where(a => refreshClassTypeIDs.Any(ctid => ctid == Math.Max(a.ClasstypeID, a.ctid))))
                    {
                        switch (refreshEntity.RefreshMessageType)
                        {
                            case RefreshMessageType.Change:
                            case RefreshMessageType.New:
                            case RefreshMessageType.CollectionChange:
                            case RefreshMessageType.Command:
                            case RefreshMessageType.Information:
                                reloadBusinessCacheAction((refreshEntity.BID, (short)refreshEntity.ID));
                                break;
                            case RefreshMessageType.Delete:
                                deleteBusinessCacheAction((refreshEntity.BID, (short)refreshEntity.ID));
                                break;
                            default:
                                break;
                        }
                    }
            });

            Connection.Closed += async (e) =>
            {
                await Connection.DisposeAsync();
                await Task.Delay(1000);
                UserLinkRefreshListener.Init(serverConfigurations, reloadBusinessCacheAction, deleteBusinessCacheAction);
            };

            try
            {
                Connection.StartAsync().GetAwaiter().GetResult();
                foreach (var ctid in refreshClassTypeIDs)
                    Connection.InvokeAsync("joinGroup", ctid).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not connect to Endor.RTC", ex);
            }
        }
    }
}
