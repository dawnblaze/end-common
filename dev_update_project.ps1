param (
    [Parameter(Mandatory=$true)][string]$project,
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [string]$build = "Debug"
)

pushd .\_scripts
$valid = .\_dev_validate
popd

if ($valid) {
	& .\_dev_publish_package $project $mygetkey
}


