﻿using Endor.EF;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonUtils = Endor.Common.Tests.CommonEntityFrameworkTests;

namespace Endor.Common.Level2.Tests
{
    [TestClass]
    public class MyTestClass
    {
        protected ApiContext ctx;
        protected short bid;

        [TestInitialize]
        public void Initialize()
        {            
        }

        [TestMethod]
        public void TestMigration_END_8259FixupRequiredDataThatGotRenamed()
        {
            bid = 1;
            ctx = CommonUtils.GetMockCtx(bid, "20190819184132_END-8259FixupRequiredDataThatGotRenamed");

            var requiredOrderItemStatus = ctx.OrderItemStatus.Where(t => t.BID == -1).Select(t => new { t.ID, t.Name }).ToList();
            List<short> requiredOrderItemStatusIDs = requiredOrderItemStatus.Select(t => t.ID).ToList();
            List<string> requiredOrderItemStatusNames = requiredOrderItemStatus.Select(t => t.Name).ToList();

            List<OrderItemStatus> businessInstancesOfRequiredOrderItemStatus = ctx.OrderItemStatus.Where(t => requiredOrderItemStatusIDs.Contains(t.ID) && t.BID == bid).ToList();
            List<short> businessInstanceOfRequiredOrderItemStatusIDs = businessInstancesOfRequiredOrderItemStatus.Select(t => t.ID).ToList();
            List<string> businessInstanceOfRequiredOrderItemStatusNames = businessInstancesOfRequiredOrderItemStatus.Select(t => t.Name).ToList();

            CollectionAssert.AreEquivalent(requiredOrderItemStatusIDs, businessInstanceOfRequiredOrderItemStatusIDs);
            CollectionAssert.AreEquivalent(requiredOrderItemStatusNames, businessInstanceOfRequiredOrderItemStatusNames);            
        }

        [TestMethod]
        public void TestMigration_MachineInstanceUsedIncorrectTypeForLocationID()
        {
            bid = 1;
            ctx = CommonUtils.GetMockCtx(bid, "20190819223528_MachineInstanceUsedIncorrectTypeForLocationID");
        }

        [TestCleanup]
        public void Cleanup()
        {

        }

    }
}