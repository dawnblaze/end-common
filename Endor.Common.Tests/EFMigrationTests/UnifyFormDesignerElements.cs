﻿using Endor.Common.Tests;
using Endor.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Common.Level2.Tests
{
    [TestClass]
    public class UnifyFormDesignerElements : CommonEntityFrameworkTests
    {
        [ClassInitialize]
        public static void InitializeAssembly(TestContext context)
        {
        }


        [TestMethod]
        public async Task CustomFieldContainerMigratedToElement()
        {
            string requiresMigration = "20191009214320_UnifyCustomFieldAndAssembly";

            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                using (var command = ctx.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = @"
SELECT
TableName
FROM 
TablesAndColumns
WHERE
TableName = 'CustomField.Layout.Container'";

                    try
                    {
                        ctx.Database.OpenConnection();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            Assert.AreEqual(false, reader.HasRows);
                        }
                    }
                    finally
                    {
                        ctx.Database.CloseConnection();
                    }
                }

                Assert.IsFalse(
                    ctx.CustomFieldLayoutElement.Any(x => x.ParentID == null && x.ElementType != Models.AssemblyElementType.Section)
                );
                Assert.IsFalse(
                    ctx.CustomFieldLayoutElement.Any(x => x.ID == x.ParentID),
                    "no elements should be parented to themselves"
                );
            }
        }
    }
}
