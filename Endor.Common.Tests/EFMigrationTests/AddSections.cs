﻿using Endor.Common.Tests;
using Endor.EF;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level2.Tests
{
    [TestClass]
    public class AddSections : CommonEntityFrameworkTests
    {
        [ClassInitialize]
        public static void InitializeAssembly(TestContext context)
        {
            Initialize();
        }

        [TestMethod]
        public void SectionTest()
        {
            string requiresMigration = "20190912193945_AddSectionAssemblyElement";

            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                Assert.IsFalse(
                    ctx.AssemblyElement.Any(x => x.ParentID == null && x.ElementType != Models.AssemblyElementType.Section)
                );
            }
        }

    }
}
