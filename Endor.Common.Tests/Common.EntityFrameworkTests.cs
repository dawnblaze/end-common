﻿using Endor.Common.Tests;
using Endor.EF;
using Endor.Models;
using Endor.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Common.Tests
{
    public class CommonEntityFrameworkTests
    {
        protected static void Initialize()
        {
            ApiContext ctx = GetMockCtx(1);
        }
        
        internal static ITaskQueuer GetMockTaskQueuer()
        {
            return new MockTaskQueuer(new MockTenantDataCache());
        }

        internal static ApiContext GetMockCtx(short bid, string migrationRequiredName = null)
        {
            var opts = new DbContextOptions<ApiContext>()
            {

            };
            
            ApiContext ctx = new ApiContext(opts, new MockTenantDataCache(), bid);
            new MigrationHelper(GetMockTaskQueuer()).MigrateDb(ctx);

            if (!String.IsNullOrWhiteSpace(migrationRequiredName) && !ctx.Database.GetAppliedMigrations().Contains(migrationRequiredName))
                Assert.Fail($"Requires migration {migrationRequiredName}");

            return ctx;
        }

        public async Task TestEntityCrud<T, I, U>(T initialEntity, string propertyToUpdate, U updateTestValue) where T : class, IAtom<I> where I : struct, IConvertible
        {
            var type = typeof(T);
            var IDProp = type.GetProperty("ID");
            var UpdateProp = type.GetProperty(propertyToUpdate);
            using (ApiContext ctx = GetMockCtx(1))
            {
                T existingID = await ctx.Set<T>().Where(e => e.ID.Equals(initialEntity.ID)).FirstOrDefaultAsync();

                if (existingID != null)
                    ctx.Set<T>().Remove(existingID);

                ctx.Set<T>().Add(initialEntity);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());//test create
                var insertedEntity = await ctx.Set<T>().Where(e => e.ID.Equals(initialEntity.ID)).FirstOrDefaultAsync();
                Assert.IsNotNull(insertedEntity);//test read

                UpdateProp.SetValue(insertedEntity, updateTestValue);
                await ctx.SaveChangesAsync();
                var updatedEntity = await ctx.Set<T>().Where(e => e.ID.Equals(initialEntity.ID)).FirstOrDefaultAsync();
                Assert.AreEqual(updateTestValue, UpdateProp.GetValue(updatedEntity));//test update

                ctx.Set<T>().Remove(updatedEntity);
                await ctx.SaveChangesAsync();
                var deletedEntity = await ctx.Set<T>().Where(e => e.ID.Equals(initialEntity.ID)).FirstOrDefaultAsync();
                Assert.IsNull(deletedEntity);//test delete

            }
        }

    }
}
