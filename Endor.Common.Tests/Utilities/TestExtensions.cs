﻿using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace Endor.Common.Tests
{
    public static class Extensions
    {
        public static IQueryable<T> IncludeAll<T>(this IQueryable<T> query, string[] includes) where T : class
        {
            for (int i = 0; i < includes.Length; i++)
            {
                query = query.Include(includes[i]);
            }

            return query;
        }

        public static IEnumerable<T> SelectFromReader<T>(this DbDataReader reader, Func<DbDataReader, T> selector)
        {
            while (reader.Read())
            {
                yield return selector(reader);
            }
        }

        public static string DbGetStringOrNull(this DbDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? null : reader.GetString(ordinal);
        }

        public static int? DbGetIntOrNull(this DbDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? (int?)null : reader.GetInt32(ordinal);
        }

        public static short? DbGetShortOrNull(this DbDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal) ? (short?)null : reader.GetInt16(ordinal);
        }

        public static void RemoveWhereNegativeInt<T>(this DbSet<T> set)
            where T : class, IAtom<int>
        {
            var list = set.Where(p => p.ID < 0).ToList();
            if (list != null)
            {
                set.RemoveRange(list);
            }
        }

        public static void RemoveWhereNegativeShort<T>(this DbSet<T> set)
            where T: class, IAtom<short>
        {
            var list = set.Where(p => p.ID < 0).ToList();
            if (list != null)
            {
                set.RemoveRange(list);
            }
        }

        public static void RemoveWhereNegativeByte<T>(this DbSet<T> set)
            where T : class, IAtom<byte>
        {
            var list = set.Where(p => p.ID < 0).ToList();
            if (list != null)
            {
                set.RemoveRange(list);
            }
        }
    }
}
