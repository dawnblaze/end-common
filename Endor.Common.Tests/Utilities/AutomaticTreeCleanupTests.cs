﻿using Endor.Common.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Common.Level2.Tests
{
    public abstract class AutomaticTreeCleanupTests: CommonEntityFrameworkTests
    {
        [TestInitialize]
        public virtual void InitializeEntityTree()
        {
            this.DoCleanupEntityTree();
        }

        [TestCleanup]
        public virtual void CleanupEntityTree()
        {
            this.DoCleanupEntityTree();
        }

        protected abstract void DoCleanupEntityTree();
    }
}
