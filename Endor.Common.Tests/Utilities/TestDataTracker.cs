﻿using Endor.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Common.Level2.Tests
{
    public class TestDataTracker : IDisposable
    {
        List<IAtom> entities = new List<IAtom>();

        public readonly DbContext Ctx;

        public TestDataTracker(DbContext _ctx) { Ctx = _ctx; }

        public void Add<T>(T entity) where T : class, IAtom
        {
            entities.Add(entity);
            Ctx.Add(entity);
        }

        public int SaveChanges()
        {
            try
            {
                return this.Ctx.SaveChanges();
            }
            catch (Exception e)
            {
                entities = new List<IAtom>();

                throw new InvalidOperationException("Bad batch of test data", e);
            }
        }

        public void Dispose()
        {
            if (entities.Count > 0)
            {
                foreach (var e in entities)
                {
                    Ctx.Remove(e);
                }

                this.Ctx.SaveChanges();
            }
        }
    }
}
