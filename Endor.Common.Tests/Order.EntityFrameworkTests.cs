﻿using Endor.EF;
using Endor.Models;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json;
using Endor.Common.Level2.Tests;

namespace Endor.Common.Tests
{
    [TestClass]
    public class OrderEntityFrameworkTests: CommonEntityFrameworkTests
    {

        private OrderData _order;
        private OrderItemData _orderItem;

        [ClassInitialize]
        public static void InitializeAssembly(TestContext context)
        {
            Initialize();
        }

        private void CleanupOrderAndOrderItemData()
        {
            var ctx = GetMockCtx(1);
            //cleanup if exists
            var oi = ctx.OrderItemData.Where(n => n.BID == _orderItem.BID && n.ID == _orderItem.ID).FirstOrDefault();
            if (oi != null)
            {
                ctx.OrderItemComponent.RemoveRange(ctx.OrderItemComponent.Where(x => x.BID == oi.BID && x.OrderItemID == oi.ID).ToArray());
                ctx.OrderItemData.Remove(oi);
                Assert.AreEqual(1, ctx.SaveChanges());
            }

            var o = ctx.OrderData.Where(n => n.BID == _order.BID && n.ID == _order.ID).FirstOrDefault();
            if (o != null)
            {
                ctx.OrderItemComponent.RemoveRange(ctx.OrderItemComponent.Where(x => x.BID == oi.BID && x.OrderID == o.ID).ToArray());
                ctx.OrderItemData.RemoveRange(ctx.OrderItemData.Where(x => x.BID == oi.BID && x.OrderID == o.ID).ToArray());
                ctx.OrderData.Remove(o);
                Assert.AreEqual(1, ctx.SaveChanges());
            }
        }

        [TestInitialize]
        public void InitTests()
        {
            using (var ctx = GetMockCtx(1))
            {
                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -98,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };
                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderItemData orderItem = new OrderItemData()
                {
                    BID = order.BID,
                    ID = -99,
                    OrderID = order.ID,
                    ItemNumber = 1,
                    Quantity = 1,
                    Name = "Test Order Item",
                    IsOutsourced = false,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    ProductionLocationID = order.LocationID,
                    PriceIsLocked = false,
                    //PriceUnitOV = false,
                    //PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasProof = false,
                    HasDocuments = false,
                    HasCustomImage = false
                };

                var cleanup = ctx.OrderData.FirstOrDefault(t => t.BID == order.BID && t.ID == order.ID);
                if (cleanup != null)
                {
                    ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(n => n.BID == order.BID && n.OrderID == order.ID));
                    ctx.OrderItemData.RemoveRange(ctx.OrderItemData.Where(n => n.BID == order.BID && n.OrderID == order.ID));
                    ctx.OrderData.Attach(cleanup);
                    ctx.OrderData.Remove(cleanup);
                    try
                    {
                        ctx.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Assert.Fail(e.ToString());
                    }
                }

                this._order = order;
                this._orderItem = orderItem;
                CleanupOrderAndOrderItemData();
                ctx.OrderData.Add(order);
                ctx.OrderItemData.Add(orderItem);
                try
                {
                    ctx.SaveChanges();
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
            }
        }

        [TestCleanup()]
        public void CleanupTests()
        {
            using (var ctx = GetMockCtx(1))
            {
                ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(n => n.BID == _order.BID && n.OrderID == _order.ID));
                // ctx.OrderItemData.RemoveRange(ctx.OrderItemData.Where(n => n.BID == _order.BID && n.OrderID == _order.ID));
                ctx.OrderItemData.Attach(_orderItem);
                ctx.OrderItemData.Remove(_orderItem);
                ctx.OrderData.Attach(_order);
                ctx.OrderData.Remove(_order);
                try
                {
                    ctx.SaveChanges();
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
                _order = null;
                _orderItem = null;
            }
        }

        [TestMethod]
        public async Task SerializeOrderData()
        {
            using (var ctx = GetMockCtx(1))
            {
                var companyData = await ctx.CompanyData.FirstOrDefaultAsync();
                var locationData = await ctx.LocationData.FirstOrDefaultAsync();
                var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync();

                //Temporary order data
                var orderData = new OrderData()
                {
                    BID = companyData.BID,
                    CompanyID = companyData.ID,
                    PickupLocationID = locationData.ID,
                    ProductionLocationID = locationData.ID,
                    LocationID = locationData.ID,
                    TaxGroupID = taxGroup.ID,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                if (ctx.OrderData.Where(od => od.ID == 0 && od.BID == companyData.BID).FirstOrDefault() == null)
                {
                    ctx.Add(orderData);
                    Assert.AreEqual(1, ctx.SaveChanges());
                }

                if (ctx.OrderCustomData.Where(ocd => ocd.ID == 0 && ocd.BID == companyData.BID).FirstOrDefault() == null && ctx.OrderItemData.Where(oi => oi.OrderID == 0 && oi.BID == companyData.BID).FirstOrDefault() == null)
                {
                    ctx.Remove(orderData);
                    Assert.AreEqual(1, ctx.SaveChanges());
                }

                var orderString = JsonConvert.SerializeObject(orderData,
                    new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }
                    );
                Assert.IsNotNull(orderString);
            }
        }

        [TestMethod]
        public void EFOrderDataTest()
        {
            using (var ctx = GetMockCtx(1))
            {

                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };
                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                ctx.OrderData.Add(order);
                try
                {
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
                var priceProductTotal = 10m;
                order.PriceProductTotal = priceProductTotal;
                try
                {
                    ctx.SaveChanges();

                    Assert.AreEqual(priceProductTotal, order.PricePreTax);
                    Assert.AreEqual(priceProductTotal, order.PriceNet);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }


                ctx.OrderData.Remove(order);

                try
                {
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
            }
        }

        [TestMethod]
        public void EFCreditMemoDataTest()
        {
            using (var ctx = GetMockCtx(1))
            {

                CreditMemoData creditMemo = new CreditMemoData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.CreditMemo,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Memo,
                    OrderStatusID = OrderOrderStatus.CreditMemoPosted,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };
                creditMemo.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == creditMemo.BID).ID;
                creditMemo.PickupLocationID = creditMemo.LocationID;
                creditMemo.ProductionLocationID = creditMemo.LocationID;
                creditMemo.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == creditMemo.BID).ID;
                creditMemo.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == creditMemo.BID).ID;

                ctx.CreditMemoData.Add(creditMemo);
                try
                {
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
                var priceProductTotal = 10m;
                creditMemo.PriceProductTotal = priceProductTotal;
                try
                {
                    ctx.SaveChanges();

                    Assert.AreEqual(priceProductTotal, creditMemo.PricePreTax);
                    Assert.AreEqual(priceProductTotal, creditMemo.PriceNet);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }


                ctx.CreditMemoData.Remove(creditMemo);

                try
                {
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
            }
        }

        [TestMethod]
        public void EFOrderOrderLinkTest()
        {
            using (var ctx = GetMockCtx(1))
            {

                OrderData order1 = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order1.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order1.BID).ID;
                order1.PickupLocationID = order1.LocationID;
                order1.ProductionLocationID = order1.LocationID;
                order1.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order1.BID).ID;
                order1.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order1.BID).ID;


                OrderData order2 = new OrderData()
                {
                    BID = 1,
                    ID = -96,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order2.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order2.BID).ID;
                order2.PickupLocationID = order2.LocationID;
                order2.ProductionLocationID = order2.LocationID;
                order2.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order2.BID).ID;
                order2.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order2.BID).ID;

                OrderOrderLink orderOrderLink = new OrderOrderLink()
                {
                    BID = order1.BID,
                    Description = "related orders",
                    LinkType = OrderOrderLinkType.ChildOrder,
                    LinkedOrderID = order2.ID,
                    OrderID = order1.ID,
                    LinkedFormattedNumber = order2.FormattedNumber,
                };

                OrderOrderLink orderOrderLink2 = new OrderOrderLink()
                {
                    BID = order1.BID,
                    Description = "related orders",
                    LinkType = OrderOrderLinkType.MasterOrder,
                    LinkedOrderID = order1.ID,
                    OrderID = order2.ID,
                    LinkedFormattedNumber = order1.FormattedNumber,
                };

                ctx.OrderData.Add(order1);
                ctx.OrderData.Add(order2);
                ctx.OrderOrderLink.Add(orderOrderLink);
                ctx.OrderOrderLink.Add(orderOrderLink2);

                try
                {
                    Assert.AreEqual(4, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }


                var adjustedAmount = 10m;
                orderOrderLink.Amount = adjustedAmount;
                try
                {
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }


                ctx.OrderOrderLink.Remove(orderOrderLink);
                ctx.OrderOrderLink.Remove(orderOrderLink2);
                ctx.OrderData.Remove(order1);
                ctx.OrderData.Remove(order2);

                try
                {
                    Assert.AreEqual(4, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
            }
        }

        [TestMethod]
        public void OrderNoteTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                OrderNote orderNote = new OrderNote()
                {
                    BID = 1,
                    ID = -99,
                    Note = "Test Add Note",
                    OrderID = _order.ID,
                    ModifiedDT = DateTime.Now,
                    NoteType = OrderNoteType.Other,
                };

                ctx.OrderNote.Add(orderNote);
                try
                {
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
            }
        }

        [TestMethod]
        public void OrderItemNoteTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                OrderNote orderNote = new OrderNote()
                {
                    BID = 1,
                    ID = -99,
                    Note = "Test Add Note",
                    OrderID = _order.ID,
                    OrderItemID = _orderItem.ID,
                    ModifiedDT = DateTime.Now,
                    NoteType = OrderNoteType.Other,
                };

                ctx.OrderNote.Add(orderNote);
                try
                {
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
            }
        }

        [TestMethod]
        public void OrderContactRoleTest()
        {
            using (var ctx = GetMockCtx(1))
            {

                OrderData order1 = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order1.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order1.BID).ID;
                order1.PickupLocationID = order1.LocationID;
                order1.ProductionLocationID = order1.LocationID;
                order1.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order1.BID).ID;
                order1.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order1.BID).ID;

                int? contactID = ctx.CompanyContactLink.FirstOrDefault(t => t.BID == order1.BID && t.CompanyID == order1.CompanyID)?.ContactID;

                OrderContactRole orderContactRole = new OrderContactRole()
                {
                    BID = order1.BID,
                    ID = -97,
                    ClassTypeID = (int)ClassType.OrderContactRole,
                    OrderID = order1.ID,
                    ContactID = contactID,
                    IsOrderRole = true,
                    IsDestinationRole = false,
                    IsOrderItemRole = false,
                    RoleType = OrderContactRoleType.Primary,
                };

                ctx.OrderData.Add(order1);
                ctx.OrderContactRole.Add(orderContactRole);

                try
                {
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }


                ctx.OrderContactRole.Remove(orderContactRole);
                ctx.OrderData.Remove(order1);

                try
                {
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }
            }
        }

        [TestMethod]
        public void EnumOrderContactRoleType()
        {
            using (var ctx = GetMockCtx(1))
            {
                List<EnumOrderContactRoleType> enums = ctx.EnumOrderContactRoleType.OrderBy(t => t.ID).ToList();

                String[] enumNames = Enum.GetNames(typeof(OrderContactRoleType));
                OrderContactRoleType[] enumValues = (OrderContactRoleType[])Enum.GetValues(typeof(OrderContactRoleType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumOrderContactRoleType enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);
                    Assert.AreEqual(enumItem.Name.Replace(" ", ""), enumNames[i]);

                    i++;
                }
            }
        }

        [TestMethod]
        public void OrderEmployeeRoleTest()
        {
            using (var ctx = GetMockCtx(1))
            {

                OrderData order1 = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-1000",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order1.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order1.BID).ID;
                order1.PickupLocationID = order1.LocationID;
                order1.ProductionLocationID = order1.LocationID;
                order1.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order1.BID).ID;
                order1.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order1.BID).ID;

                EmployeeRole employeeRole = new EmployeeRole()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.EmployeeRole,
                    ModifiedDT = DateTime.UtcNow,
                    Name = "Test Employee Role",
                    AllowMultiple = false,
                    AllowOnTeam = true,
                    IsActive = true,
                    IsSystem = false,
                    OrderRestriction = RoleAccess.Optional,
                    OrderItemRestriction = RoleAccess.Optional,
                    OrderDestinationRestriction = RoleAccess.Optional,
                    EstimateRestriction = RoleAccess.Optional,
                    EstimateItemRestriction = RoleAccess.Optional,
                    EstimateDestinationRestriction = RoleAccess.Optional,
                    OpportunityRestriction = RoleAccess.Optional,
                    PORestriction = RoleAccess.Optional
                };

                OrderEmployeeRole orderEmployeeRole = new OrderEmployeeRole()
                {
                    BID = order1.BID,
                    ID = -97,
                    ClassTypeID = (int)ClassType.OrderEmployeeRole,
                    OrderID = order1.ID,
                    EmployeeID = ctx.EmployeeData.FirstOrDefault(t => t.BID == order1.BID).ID,
                    IsOrderRole = true,
                    IsDestinationRole = false,
                    IsOrderItemRole = false,
                    RoleID = employeeRole.ID,
                };

                ctx.OrderData.Add(order1);
                ctx.EmployeeRole.Add(employeeRole);
                ctx.OrderEmployeeRole.Add(orderEmployeeRole);

                try
                {
                    Assert.AreEqual(3, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }


                ctx.OrderEmployeeRole.Remove(orderEmployeeRole);
                ctx.EmployeeRole.Remove(employeeRole);
                ctx.OrderData.Remove(order1);

                try
                {
                    Assert.AreEqual(3, ctx.SaveChanges());
                }
                catch (Exception e)
                {
                    Assert.Fail(e.ToString());
                }


            }
        }

        [TestMethod]
        public async Task OrderEmployeeRoleNavigationPropertiesTest()
        {
            using (ApiContext ctc = GetMockCtx(1))
            {
                var orderEmployeeRole = await ctc.OrderEmployeeRole.Include("Order").FirstOrDefaultAsync();

                var orderData = orderEmployeeRole?.Order;
            }
        }

        [TestMethod]
        public async Task EFGetFirstOrderItemStatus()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsTrue(await ctx.OrderItemStatus.FirstOrDefaultAsync() != null);
            }
        }

        [TestMethod]
        public async Task EFTestOrderItemStatusCreate()
        {
            var testOrderItemStatus = new OrderItemStatus()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = 10022,
                IsActive = true,
                IsSystem = true,
                IsDefault = false,
                TransactionType = (byte)OrderTransactionType.Destination,
                StatusIndex = 0,
                Name = "test name"
            };
            using (ApiContext ctx = GetMockCtx(1))
            {
                ctx.Entry(testOrderItemStatus).Property(x => x.ModifiedDT).IsModified = false;
                ctx.OrderItemStatus.Add(testOrderItemStatus);
                await ctx.SaveChangesAsync();
                ctx.OrderItemStatus.Remove(testOrderItemStatus);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EFTestOrderItemStatusCRUD()
        {
            var testOrderItemStatus = new OrderItemStatus()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = 10022,
                IsActive = true,
                IsSystem = true,
                IsDefault = false,
                TransactionType = (byte)OrderTransactionType.Destination,
                StatusIndex = 0,
                Name = "test name"
            };
            await this.TestEntityCrud<OrderItemStatus,short, string>(testOrderItemStatus, nameof(testOrderItemStatus.Name), "updated name");
        }

        [TestMethod]
        public async Task EFTestOrderItemSubStatusCRUD()
        {
            var test = new OrderItemSubStatus()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = 10023,
                Name = "banana",
                Description = "des"
            };
            await this.TestEntityCrud<OrderItemSubStatus,short,string>(test, nameof(OrderItemSubStatus.Description), "description banana update");
        }

        [TestMethod]
        public async Task EFTestOrderItemStatusSubStatusLink()
        {
            var testOrderItemSubStatus = new OrderItemSubStatus()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = 10023,
                Name = "banana",
                Description = "des"
            };
            var testOrderItemStatus = new OrderItemStatus()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = 10022,
                IsActive = true,
                IsSystem = true,
                IsDefault = false,
                TransactionType = (byte)OrderTransactionType.Destination,
                StatusIndex = 0,
                Name = "test name"
            };
            var testlink = new OrderItemStatusSubStatusLink()
            {
                BID = 1,
                StatusID = -99,
                SubStatusID = -99
            };
            using (ApiContext ctx = GetMockCtx(1))
            {
                var existingOrderItemStatusTest = await ctx.OrderItemStatus.Where(ois => ois.BID == testOrderItemStatus.BID && ois.ID == testOrderItemStatus.ID).FirstOrDefaultAsync();
                if (existingOrderItemStatusTest != null)
                {
                    ctx.OrderItemStatus.Remove(existingOrderItemStatusTest);
                    await ctx.SaveChangesAsync();//remove existing test records
                }

                var found = ctx.OrderItemSubStatus.Find(testOrderItemSubStatus.BID, testOrderItemSubStatus.ID);
                if (found != null)
                {
                    ctx.OrderItemSubStatus.Remove(found);
                    Assert.AreEqual(1, ctx.SaveChanges());//remove existing test records
                }
                var existingOrderItemSubStatusTest = ctx.OrderItemSubStatus.Where(oiss => oiss.BID == testOrderItemSubStatus.BID && oiss.Name == testOrderItemSubStatus.Name).FirstOrDefault();
                if (existingOrderItemSubStatusTest != null)
                {
                    ctx.OrderItemSubStatus.Remove(existingOrderItemSubStatusTest);
                    Assert.AreEqual(1, ctx.SaveChanges());//remove existing test records
                }

                ctx.OrderItemStatus.Add(testOrderItemStatus);
                ctx.OrderItemSubStatus.Add(testOrderItemSubStatus);
                await ctx.SaveChangesAsync();

                ctx.OrderItemStatusSubStatusLink.Add(testlink);
                Assert.IsTrue((await ctx.SaveChangesAsync()) > 0);//test insert

                var mytestLink = await ctx.OrderItemStatusSubStatusLink
                    .Include(link => link.OrderItemStatus)
                    .Include(link => link.OrderItemSubStatus)
                    .Where(link => link.StatusID == -99 && link.SubStatusID == -99)
                    .FirstOrDefaultAsync();

                Assert.IsNotNull(mytestLink.OrderItemStatus);//test navigation prop
                Assert.IsNotNull(mytestLink.OrderItemSubStatus);//test navigation prop

                // test actual navigation properties
                var actualOrderItemStatus = await ctx.OrderItemStatus.Where(ois => ois.ID == testOrderItemStatus.ID).Include("OrderItemStatusSubStatusLinks.OrderItemSubStatus").FirstOrDefaultAsync();
                Assert.IsNotNull(actualOrderItemStatus.OrderItemStatusSubStatusLinks);
                Assert.IsNotNull(actualOrderItemStatus.OrderItemStatusSubStatusLinks.First().OrderItemSubStatus);

                var actualOrderItemSubStatus = await ctx.OrderItemStatus.Where(ois => ois.ID == testOrderItemSubStatus.ID).Include("OrderItemStatusSubStatusLinks.OrderItemStatus").FirstOrDefaultAsync();
                Assert.IsNotNull(actualOrderItemSubStatus.OrderItemStatusSubStatusLinks);
                Assert.IsNotNull(actualOrderItemSubStatus.OrderItemStatusSubStatusLinks.First().OrderItemStatus);

                ctx.OrderItemStatusSubStatusLink.Remove(testlink);
                ctx.OrderItemSubStatus.Remove(testOrderItemSubStatus);
                ctx.OrderItemStatus.Remove(testOrderItemStatus);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EFTestOrderItemDataCRUD()
        {
            using (var ctx = GetMockCtx(1))
            {
                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderItemData orderItem = new OrderItemData()
                {
                    BID = order.BID,
                    ID = -99,
                    OrderID = order.ID,
                    ItemNumber = 1,
                    Quantity = 1,
                    Name = "Test Order Item",
                    IsOutsourced = false,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    ProductionLocationID = order.LocationID,
                    PriceIsLocked = false,
                    //PriceUnitOV = false,
                    //PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasProof = false,
                    HasDocuments = false,
                    HasCustomImage = false
                };

                this._order = order;
                this._orderItem = orderItem;
                CleanupOrderAndOrderItemData();

                ctx.OrderData.Add(order);
                ctx.OrderItemData.Add(orderItem);
                Assert.AreEqual(2, ctx.SaveChanges());

                var orderItemRetrieve = await ctx.OrderItemData.Where(x => x.BID == order.BID && x.OrderID == order.ID && x.ID == orderItem.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(orderItemRetrieve);
                Assert.AreEqual(orderItemRetrieve.Name, orderItem.Name);

                var newName = "Change Test Order Item Name";
                orderItem.Name = newName;
                Assert.AreEqual(1, ctx.SaveChanges());

                orderItemRetrieve = await ctx.OrderItemData.Where(x => x.BID == order.BID && x.OrderID == order.ID && x.ID == orderItem.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(orderItemRetrieve);
                Assert.AreEqual(orderItem.Name, newName);
            }
        }

        [TestMethod]
        public async Task EFTestOrderItemComponentCRUD()
        {
            using (var ctx = GetMockCtx(1))
            using (var tdt = new TestDataTracker(ctx))
            {
                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = ctx.OrderData.OrderBy(x => x.ID).FirstOrDefault().ID - 1,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderItemData orderItem = new OrderItemData()
                {
                    BID = order.BID,
                    ID = ctx.OrderItemData.OrderBy(x => x.ID).FirstOrDefault().ID - 1,
                    OrderID = order.ID,
                    ItemNumber = 1,
                    Quantity = 1,
                    Name = "Test Order Item",
                    IsOutsourced = false,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    ProductionLocationID = order.LocationID,
                    PriceIsLocked = false,
                    //PriceUnitOV = false,
                    //PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasProof = false,
                    HasDocuments = false,
                    HasCustomImage = false,
                };

                OrderItemComponent orderItemComponent = new OrderItemComponent()
                {
                    BID = order.BID,
                    ID = ctx.OrderItemComponent.OrderBy(x => x.ID).FirstOrDefault().ID - 1,
                    OrderID = order.ID,
                    OrderItemID = orderItem.ID,
                    Name = "New Component",
                };

                tdt.Add(orderItemComponent);
                tdt.Add(orderItem);
                tdt.Add(order);

                tdt.SaveChanges();

                var orderItemComponentRetrieve = await ctx.OrderItemComponent
                                                            .FirstOrDefaultAsync(o => o.BID == order.BID && o.OrderID == order.ID && o.OrderItemID == orderItem.ID);
                Assert.IsNotNull(orderItemComponentRetrieve);

                var orderItemRetrieve = await ctx.OrderItemData
                                                    .Include(o => o.Components)
                                                    .FirstOrDefaultAsync(o => o.BID == order.BID && o.OrderID == order.ID);


                Assert.IsNotNull(orderItemRetrieve?.Components);
                Assert.AreEqual(1, orderItemRetrieve.Components.Count);
               
            }
        }

        [TestMethod]
        public async Task EFTestOrderItemContactRoles()
        {
            using (var ctx = GetMockCtx(1))
            {
                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderItemData orderItem = new OrderItemData()
                {
                    BID = order.BID,
                    ID = -99,
                    OrderID = order.ID,
                    ItemNumber = 1,
                    Quantity = 1,
                    Name = "Test Order Item",
                    IsOutsourced = false,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    ProductionLocationID = order.LocationID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    PriceIsLocked = false,
                    //PriceUnitOV = false,
                    //PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasProof = false,
                    HasDocuments = false,
                    HasCustomImage = false
                };

                OrderContactRole contactRole = new OrderContactRole()
                {
                    ID = -99,
                    BID = order.BID,
                    OrderID = order.ID,
                    OrderItemID = orderItem.ID,
                    RoleType = OrderContactRoleType.Primary,
                    ContactID = ctx.ContactData.FirstOrDefault(t => t.BID == order.BID).ID
                };

                this._order = order;
                this._orderItem = orderItem;
                CleanupOrderAndOrderItemData();
                ctx.OrderData.Add(order);
                ctx.OrderItemData.Add(orderItem);
                ctx.OrderContactRole.Add(contactRole);

                Assert.AreEqual(3, ctx.SaveChanges());

                var includes = new string[] { "ContactRoles" };
                var orderItemRetrieve = await ctx.OrderItemData.IncludeAll(includes).Where(o => o.BID == orderItem.BID && o.ID == orderItem.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(orderItemRetrieve);
                Assert.IsNotNull(orderItemRetrieve.ContactRoles);

                ctx.OrderContactRole.Remove(contactRole);
                Assert.AreEqual(1, ctx.SaveChanges());
            }
        }

        [TestMethod]
        public async Task EFTestOrderItemEmployeeRoles()
        {
            using (var ctx = GetMockCtx(1))
            {
                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderItemData orderItem = new OrderItemData()
                {
                    BID = order.BID,
                    ID = -99,
                    OrderID = order.ID,
                    ItemNumber = 1,
                    Quantity = 1,
                    Name = "Test Order Item",
                    IsOutsourced = false,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    ProductionLocationID = order.LocationID,
                    PriceIsLocked = false,
                    //PriceUnitOV = false,
                    //PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasProof = false,
                    HasDocuments = false,
                    HasCustomImage = false
                };

                EmployeeRole empRole = new EmployeeRole()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.EmployeeRole,
                    ModifiedDT = DateTime.UtcNow,
                    Name = "Test Employee Role",
                    AllowMultiple = false,
                    AllowOnTeam = true,
                    IsActive = true,
                    IsSystem = false,
                    OrderRestriction = RoleAccess.Optional,
                    OrderItemRestriction = RoleAccess.Optional,
                    OrderDestinationRestriction = RoleAccess.Optional,
                    EstimateRestriction = RoleAccess.Optional,
                    EstimateItemRestriction = RoleAccess.Optional,
                    EstimateDestinationRestriction = RoleAccess.Optional,
                    OpportunityRestriction = RoleAccess.Optional,
                    PORestriction = RoleAccess.Optional
                };

                OrderEmployeeRole employeeRole = new OrderEmployeeRole()
                {
                    ID = -99,
                    BID = order.BID,
                    OrderID = order.ID,
                    OrderItemID = orderItem.ID,
                    RoleID = empRole.ID,
                    EmployeeID = ctx.EmployeeData.FirstOrDefault(t => t.BID == order.BID).ID
                };

                this._order = order;
                this._orderItem = orderItem;
                CleanupOrderAndOrderItemData();

                //cleanup if exists
                var er = ctx.EmployeeRole.Where(n => n.BID == order.BID && n.ID == empRole.ID).FirstOrDefault();
                if (er != null)
                {
                    ctx.EmployeeRole.Remove(er);
                    Assert.AreEqual(1, ctx.SaveChanges());
                }

                //cleanup if exists
                var oer = ctx.OrderEmployeeRole.Where(n => n.BID == order.BID && n.ID == employeeRole.ID).FirstOrDefault();
                if (oer != null)
                {
                    ctx.OrderEmployeeRole.Remove(oer);
                    Assert.AreEqual(1, ctx.SaveChanges());
                }


                try
                {
                    ctx.OrderData.Add(order);
                    ctx.OrderItemData.Add(orderItem);
                    ctx.EmployeeRole.Add(empRole);
                    ctx.OrderEmployeeRole.Add(employeeRole);
                    Assert.AreEqual(4, ctx.SaveChanges());

                    var includes = new string[] { "EmployeeRoles" };
                    var orderItemRetrieve = await ctx.OrderItemData.IncludeAll(includes).Where(x => x.BID == orderItem.BID && x.ID == orderItem.ID).FirstOrDefaultAsync();
                    Assert.IsNotNull(orderItemRetrieve);
                    Assert.IsNotNull(orderItemRetrieve.EmployeeRoles);
                }
                finally {

                    //cleanup
                    ctx.OrderEmployeeRole.Remove(employeeRole);
                    ctx.EmployeeRole.Remove(empRole);
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
            }
        }

        [TestMethod]
        public async Task EFTestDestinationEmployeeRoles()
        {
            using (var ctx = GetMockCtx(1))
            {

                var prevDestArtifact = ctx.OrderDestinationData.Find((short)1, -101);
                var prevEmpRoleArtifact = ctx.EmployeeRole.Find((short)1, (short)-101);
                var prevOrderEmpRoleArtifact = ctx.OrderEmployeeRole.Find((short)1, -101);
                var prevOrderArtifact = ctx.OrderData.Find((short)1, -101);

                if (prevDestArtifact != null)
                {
                    ctx.Remove(prevDestArtifact);
                }
                if (prevEmpRoleArtifact != null)
                {
                    ctx.Remove(prevEmpRoleArtifact);
                }
                if (prevOrderEmpRoleArtifact != null)
                {
                    ctx.Remove(prevOrderEmpRoleArtifact);
                }
                if (prevOrderArtifact != null)
                {
                    ctx.Remove(prevOrderArtifact);
                }

                ctx.SaveChanges();

                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -101,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderDestinationData destinationData = new OrderDestinationData()
                {
                    BID = order.BID,
                    ID = -101,
                    OrderID = order.ID,
                    Name = "Test Order Destination",
                    OrderStatusID = OrderOrderStatus.DestinationPending,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    PriceIsLocked = false,
                    PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasDocuments = false,
                    TransactionType = (byte)OrderTransactionType.Destination,
                };

                EmployeeRole empRole = new EmployeeRole()
                {
                    BID = 1,
                    ID = -101,
                    ClassTypeID = (int)ClassType.EmployeeRole,
                    ModifiedDT = DateTime.UtcNow,
                    Name = "Test Employee Role",
                    AllowMultiple = false,
                    AllowOnTeam = true,
                    IsActive = true,
                    IsSystem = false,
                    OrderRestriction = RoleAccess.Optional,
                    OrderItemRestriction = RoleAccess.Optional,
                    OrderDestinationRestriction = RoleAccess.Optional,
                    EstimateRestriction = RoleAccess.Optional,
                    EstimateItemRestriction = RoleAccess.Optional,
                    EstimateDestinationRestriction = RoleAccess.Optional,
                    OpportunityRestriction = RoleAccess.Optional,
                    PORestriction = RoleAccess.Optional
                };

                OrderEmployeeRole employeeRole = new OrderEmployeeRole()
                {
                    ID = -101,
                    BID = order.BID,
                    OrderID = order.ID,
                    DestinationID = destinationData.ID,
                    RoleID = empRole.ID,
                    EmployeeID = ctx.EmployeeData.FirstOrDefault(t => t.BID == order.BID).ID
                };

                ctx.OrderData.Add(order);
                ctx.OrderDestinationData.Add(destinationData);
                ctx.EmployeeRole.Add(empRole);
                ctx.OrderEmployeeRole.Add(employeeRole);

                try
                {
                    Assert.AreEqual(4, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    ctx.OrderDestinationData.Remove(destinationData);
                    Assert.AreEqual(1, ctx.SaveChanges(), ex.ToString());
                    ctx.OrderEmployeeRole.Remove(employeeRole);
                    Assert.AreEqual(1, ctx.SaveChanges(), ex.ToString());
                    ctx.OrderData.Remove(order);
                    Assert.AreEqual(1, ctx.SaveChanges(), ex.ToString());
                    ctx.EmployeeRole.Remove(empRole);
                    Assert.AreEqual(1, ctx.SaveChanges(), ex.ToString());
                    Assert.Fail(ex.ToString());
                }

                var includes = new string[] { "EmployeeRoles" };
                var orderDestination = await ctx.OrderDestinationData.IncludeAll(includes).Where(o => o.BID == destinationData.BID && o.ID == destinationData.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(orderDestination);
                Assert.IsNotNull(orderDestination.EmployeeRoles);

                ctx.OrderEmployeeRole.Remove(employeeRole);
                ctx.EmployeeRole.Remove(empRole);
                ctx.OrderDestinationData.Remove(destinationData);
                ctx.OrderData.Remove(order);

                try
                {
                    Assert.AreEqual(4, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.ToString());
                }
            }
        }

        [TestMethod]
        public async Task EFTestDestinationContactRoles()
        {
            using (var ctx = GetMockCtx(1))
            {
                var prevDestArtifact = ctx.OrderDestinationData.Find((short)1, -102);
                var prevContArtifact = ctx.OrderContactRole.Find((short)1, -102);
                var prevOrderArtifact = ctx.OrderContactRole.Find((short)1, -102);

                if (prevDestArtifact != null)
                {
                    ctx.Remove(prevDestArtifact);
                }
                if (prevContArtifact != null)
                {
                    ctx.Remove(prevContArtifact);
                }
                if (prevOrderArtifact != null)
                {
                    ctx.Remove(prevOrderArtifact);
                }

                ctx.SaveChanges();

                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -102,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderDestinationData destinationData = new OrderDestinationData()
                {
                    BID = order.BID,
                    ID = -102,
                    OrderID = order.ID,
                    Name = "Test Order Destination",
                    OrderStatusID = OrderOrderStatus.DestinationPending,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    PriceIsLocked = false,
                    PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasDocuments = false,
                    TransactionType = (byte)OrderTransactionType.Destination,
                };

                OrderContactRole orderContactRole = new OrderContactRole()
                {
                    BID = order.BID,
                    ID = -102,
                    ClassTypeID = (int)ClassType.OrderContactRole,
                    OrderID = order.ID,
                    ContactID = ctx.ContactData.FirstOrDefault(t => t.BID == order.BID).ID,
                    DestinationID = destinationData.ID,
                    IsOrderRole = true,
                    IsDestinationRole = true,
                    IsOrderItemRole = false,
                    RoleType = OrderContactRoleType.Primary,
                };

                ctx.OrderData.Add(order);
                ctx.OrderDestinationData.Add(destinationData);
                ctx.OrderContactRole.Add(orderContactRole);

                try
                {
                    Assert.AreEqual(3, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    ctx.OrderContactRole.Remove(orderContactRole);
                    Assert.AreEqual(1, ctx.SaveChanges());
                    ctx.OrderDestinationData.Remove(destinationData);
                    Assert.AreEqual(1, ctx.SaveChanges());
                    ctx.OrderData.Remove(order);
                    Assert.AreEqual(1, ctx.SaveChanges());
                    Assert.Fail(ex.ToString());
                }

                var includes = new string[] { "ContactRoles" };
                var orderDestination = await ctx.OrderDestinationData.IncludeAll(includes).Where(o => o.BID == destinationData.BID && o.ID == destinationData.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(orderDestination);
                Assert.IsNotNull(orderDestination.ContactRoles);

                ctx.OrderContactRole.Remove(orderContactRole);
                ctx.OrderDestinationData.Remove(destinationData);
                ctx.OrderData.Remove(order);

                try
                {
                    Assert.AreEqual(3, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.ToString());
                }
            }
        }

        [TestMethod]
        public async Task EFTestDestinationKeyDate()
        {
            short TestBID = 1;
            using (ApiContext ctx = GetMockCtx(TestBID))
            {
                #region Create Order and OrderItem

                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderBuilt,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderDestinationData destinationData = new OrderDestinationData()
                {
                    BID = order.BID,
                    ID = -103,
                    OrderID = order.ID,
                    Name = "Test Order Destination",
                    OrderStatusID = OrderOrderStatus.DestinationPending,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    TransactionType = (byte)OrderTransactionType.Destination,
                    PriceIsLocked = false,
                    PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasDocuments = false,
                };

                ctx.OrderData.Add(order);
                ctx.OrderDestinationData.Add(destinationData);

                try
                {
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    ctx.OrderDestinationData.Remove(destinationData);
                    ctx.OrderData.Remove(order);
                    ctx.SaveChanges();
                    Assert.Fail(ex.ToString());
                }

                #endregion

                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var bidParam = new SqlParameter("@BID", TestBID);
                var orderIdParam = new SqlParameter("@OrderID", order.ID);
                var orderItemIdParam = new SqlParameter("@OrderItemID", DBNull.Value);
                var nullOrderItemIdParam = new SqlParameter("@OrderItemID", DBNull.Value);
                var destinationIdParam = new SqlParameter("@DestinationID", destinationData.ID);
                var keyDateTypeParam = new SqlParameter("@KeyDateType", 1);
                var dateTimeParam = new SqlParameter("@DateTime", new DateTime(2020, 1, 1));
                var nullDateTimeParam = new SqlParameter("@DateTime", DBNull.Value);
                var isFirmDateParam = new SqlParameter("@IsFirmDate", DBNull.Value);

                #region KeyDate for Order

                // Create
                try
                {
                    object[] createOrderKeyDateParams = 
                    {
                        bidParam,
                        orderIdParam,
                        nullOrderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        nullDateTimeParam,
                        isFirmDateParam,
                        resultParam
                    };

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT;", parameters: createOrderKeyDateParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup
                    ctx.OrderDestinationData.Remove(destinationData);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                // Update
                try
                {
                    object[] updateOrderKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        nullOrderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        dateTimeParam,
                        isFirmDateParam,
                        resultParam
                    };
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT;", parameters: updateOrderKeyDateParams);
                    Assert.AreEqual(0, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderDestinationData.Remove(destinationData);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                // Delete
                try
                {
                    object[] deleteOrderKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        nullOrderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        resultParam
                    };

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.RemoveKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @Result OUTPUT;", parameters: deleteOrderKeyDateParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderDestinationData.Remove(destinationData);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                #endregion

                #region KeyDate for Order

                // Create
                try
                {
                    object[] createOrderItemKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        orderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        nullDateTimeParam,
                        isFirmDateParam,
                        resultParam
                    };

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT;", parameters: createOrderItemKeyDateParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderDestinationData.Remove(destinationData);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                // Update
                try
                {
                    object[] updateOrderItemKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        orderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        dateTimeParam,
                        isFirmDateParam,
                        resultParam
                    };
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT;", parameters: updateOrderItemKeyDateParams);
                    Assert.AreEqual(0, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderDestinationData.Remove(destinationData);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                // Delete
                try
                {
                    object[] deleteOrderItemKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        orderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        resultParam
                    };

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.RemoveKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @Result OUTPUT;", parameters: deleteOrderItemKeyDateParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderDestinationData.Remove(destinationData);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                #endregion

                #region Cleanup

                ctx.OrderDestinationData.Remove(destinationData);
                ctx.OrderData.Remove(order);

                try
                {
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
                catch (Exception ex2)
                {
                    Assert.Fail(ex2.ToString());
                }

                #endregion

            }
        }

        [TestMethod]
        public async Task EFTestDestinationNotes()
        {
            await Task.Yield();

            using (var ctx = GetMockCtx(1))
            {
                OrderNote orderNote = new OrderNote()
                {
                    BID = 1,
                    ID = -104,
                    Note = "Test Add Note",
                    OrderID = this._order.ID,
                    NoteType = OrderNoteType.Other,
                };

                try
                {
                    ctx.OrderNote.Add(orderNote);
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
                finally
                {
                    ctx.OrderNote.Remove(orderNote);
                    Assert.AreEqual(1, ctx.SaveChanges());
                }
            }
        }


        [TestMethod]
        public async Task EFTestEnumRoleAccess()
        {
            using (var ctx = GetMockCtx(1))
            {
                var roleAccess = await ctx.EnumRoleAccess.FirstOrDefaultAsync(r => r.ID == RoleAccess.NotAllowed);
                Assert.AreEqual(RoleAccess.NotAllowed, roleAccess.ID);
                Assert.AreEqual("Not Allowed", roleAccess.Name);

                roleAccess = await ctx.EnumRoleAccess.FirstOrDefaultAsync(r => r.ID == RoleAccess.Optional);
                Assert.AreEqual(RoleAccess.Optional, roleAccess.ID);
                Assert.AreEqual("Optional", roleAccess.Name);

                roleAccess = await ctx.EnumRoleAccess.FirstOrDefaultAsync(r => r.ID == RoleAccess.Mandatory);
                Assert.AreEqual(RoleAccess.Mandatory, roleAccess.ID);
                Assert.AreEqual("Mandatory", roleAccess.Name);
            }
        }

        [TestMethod]
        public async Task EFTestEmployeeRoleCRUD()
        {
            var employeeRole = new EmployeeRole()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.EmployeeRole,
                ModifiedDT = DateTime.UtcNow,
                Name = "Test Employee Role",
                AllowMultiple = false,
                AllowOnTeam = true,
                IsActive = true,
                IsSystem = false,
                OrderRestriction = RoleAccess.Optional,
                OrderItemRestriction = RoleAccess.Optional,
                OrderDestinationRestriction = RoleAccess.Optional,
                EstimateRestriction = RoleAccess.Optional,
                EstimateItemRestriction = RoleAccess.Optional,
                EstimateDestinationRestriction = RoleAccess.Optional,
                OpportunityRestriction = RoleAccess.Optional,
                PORestriction = RoleAccess.Optional
            };

            await TestEntityCrud<EmployeeRole,short,bool>(employeeRole, "AllowOnTeam", false);
        }

        [TestMethod]
        public async Task EFTestEmployeeTeamLink()
        {
            await Task.Yield();

            short _bid = 1;

            using (var ctx = GetMockCtx(_bid))
            {
                EmployeeData employee = null;
                EmployeeTeam team = null;

                try
                {
                    employee = new EmployeeData()
                    {
                        ID = -99,
                        BID = _bid,
                        First = "Temp",
                        Last = "Employee",
                        LocationID = this._order.LocationID,
                    };
                    ctx.EmployeeData.Add(employee);

                    try
                    {
                        Assert.AreEqual(1, ctx.SaveChanges());
                    }
                    catch (Exception ex)
                    {
                        Assert.Fail(ex.ToString());
                    }

                    team = new EmployeeTeam()
                    {
                        ID = -99,
                        BID = _bid,
                        Name = "Temp Team"
                    };
                    ctx.EmployeeTeam.Add(team);

                    try
                    {
                        Assert.AreEqual(1, ctx.SaveChanges());
                    }
                    catch (Exception ex)
                    {
                        Assert.Fail(ex.ToString());
                    }

                    var etl = new EmployeeTeamLink()
                    {
                        BID = 1,
                        EmployeeID = employee.ID,
                        RoleID = 1,
                        TeamID = team.ID
                    };

                    ctx.EmployeeTeamLink.Add(etl);
                    try
                    {
                        Assert.AreEqual(1, ctx.SaveChanges());
                    }
                    catch (Exception ex)
                    {
                        Assert.Fail(ex.ToString());
                    }

                    ctx.EmployeeTeamLink.Remove(etl);
                    try
                    {
                        Assert.AreEqual(1, ctx.SaveChanges());
                    }
                    catch (Exception ex)
                    {
                        Assert.Fail(ex.ToString());
                    }
                }
                finally
                {
                    if (employee != null)
                    {
                        ctx.EmployeeData.Remove(employee);
                        ctx.SaveChanges();
                    }

                    if (team != null)
                    {
                        ctx.EmployeeTeam.Remove(team);
                        ctx.SaveChanges();
                    }
                }
            }
        }

        [TestMethod]
        public async Task EFTestLocationDataCRUD()
        {
            var location = new LocationData()
            {
                BID = 1,
                ID = 99,
                ClassTypeID = (int)ClassType.Location,
                CreatedDate = DateTime.UtcNow,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsDefault = false,
                HasImage = false,
                Name = "Test Location CRUD",
                LegalName = "Test Location Legal Name",
                DBA = "Test Location DBA",
                EstimatePrefix = "EST",
                InvoicePrefix = "INV",
                POPrefix = "PO",
                OrderNumberPrefix = "ORD"
            };

            await TestEntityCrud<LocationData,byte,string>(location, "OrderNumberPrefix", "ONP");
        }

        [TestMethod]
        public async Task EFTestNumberingOptions()
        {
            using (var ctx = GetMockCtx(1))
            {
                var orderNumberForInvoiceOption = await ctx.SystemOptionDefinition.Where(o => o.Name.Equals("Order.Invoice.Number.UseOrderNumber")).FirstOrDefaultAsync();
                Assert.IsNotNull(orderNumberForInvoiceOption);
            }
        }

        [TestMethod]
        public async Task EFTestOrderItemStatusChangeSproc()
        {
            short TestBID = 1;
            using (ApiContext ctx = GetMockCtx(TestBID))
            {
                var expectedFail = "The new Line Item Status can not be for an Order Status that is before the current one.";

                #region Create Order and OrderItem

                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderBuilt,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderItemData orderItem = new OrderItemData()
                {
                    BID = order.BID,
                    ID = -99,
                    OrderID = order.ID,
                    ItemNumber = 1,
                    Quantity = 1,
                    Name = "Test Order Item",
                    IsOutsourced = false,
                    OrderStatusID = OrderOrderStatus.OrderBuilt,
                    ItemStatusID = 23, //ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    ProductionLocationID = order.LocationID,
                    PriceIsLocked = false,
                    //PriceUnitOV = false,
                    //PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasProof = false,
                    HasDocuments = false,
                    HasCustomImage = false
                };

                this._order = order;
                this._orderItem = orderItem;
                CleanupOrderAndOrderItemData();
                ctx.OrderData.Add(order);
                ctx.OrderItemData.Add(orderItem);

                try
                {
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    ctx.OrderData.Remove(order);
                    Assert.AreEqual(1, ctx.SaveChanges());
                    Assert.Fail(ex.ToString());
                }

                #endregion

                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var bidParam = new SqlParameter("@BID", TestBID);
                var orderItemIdParam = new SqlParameter("@OrderItemID", orderItem.ID);
                var targetStatusID = new SqlParameter("@TargetStatusID", 18);

                object[] myParams = {
                    bidParam,
                    orderItemIdParam,
                    targetStatusID,
                    resultParam
                };

                try
                {
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Item.Action.ChangeStatus] @BID, @OrderItemID, @TargetStatusID, @Result OUTPUT;", parameters: myParams);
                }
                catch (Exception ex)
                {
                    Assert.AreEqual(null, resultParam.Value);
                    Assert.AreEqual(expectedFail, ex.Message);
                }

                myParams[2] = new SqlParameter("@TargetStatusID", 24);

                try
                {
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Item.Action.ChangeStatus] @BID, @OrderItemID, @TargetStatusID, @Result OUTPUT;", parameters: myParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderItemData.Remove(orderItem);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

            }
        }

        [TestMethod]
        public async Task EFTestOrderKeyDateSprocs()
        {
            short TestBID = 1;
            using (ApiContext ctx = GetMockCtx(TestBID))
            {
                #region Create Order and OrderItem

                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderBuilt,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m,
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderItemData orderItem = new OrderItemData()
                {
                    BID = order.BID,
                    ID = -99,
                    OrderID = order.ID,
                    ItemNumber = 1,
                    Quantity = 1,
                    Name = "Test Order Item",
                    IsOutsourced = false,
                    OrderStatusID = OrderOrderStatus.OrderBuilt,
                    ItemStatusID = 23, //ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    ProductionLocationID = order.LocationID,
                    PriceIsLocked = false,
                    //PriceUnitOV = false,
                    //PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasProof = false,
                    HasDocuments = false,
                    HasCustomImage = false
                };

                this._order = order;
                this._orderItem = orderItem;
                CleanupOrderAndOrderItemData();
                ctx.OrderData.Add(order);
                ctx.OrderItemData.Add(orderItem);

                try
                {
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    ctx.OrderData.Remove(order);
                    Assert.AreEqual(1, ctx.SaveChanges());
                    Assert.Fail(ex.ToString());
                }

                #endregion

                var resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                var bidParam = new SqlParameter("@BID", TestBID);
                var orderIdParam = new SqlParameter("@OrderID", order.ID);
                var orderItemIdParam = new SqlParameter("@OrderItemID", orderItem.ID);
                var nullOrderItemIdParam = new SqlParameter("@OrderItemID", DBNull.Value);
                var destinationIdParam = new SqlParameter("@DestinationID", DBNull.Value);
                var keyDateTypeParam = new SqlParameter("@KeyDateType", 1);
                var dateTimeParam = new SqlParameter("@DateTime", new DateTime(2020, 1, 1));
                var nullDateTimeParam = new SqlParameter("@DateTime", DBNull.Value);
                var isFirmDateParam = new SqlParameter("@IsFirmDate", DBNull.Value);

                #region KeyDate for Order

                // Create
                try
                {
                    object[] createOrderKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        nullOrderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        nullDateTimeParam,
                        isFirmDateParam,
                        resultParam
                    };

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT;", parameters: createOrderKeyDateParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderItemData.Remove(orderItem);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                // Update
                try
                {
                    object[] updateOrderKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        nullOrderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        dateTimeParam,
                        isFirmDateParam,
                        resultParam
                    };
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT;", parameters: updateOrderKeyDateParams);
                    Assert.AreEqual(0, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderItemData.Remove(orderItem);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                // Delete
                try
                {
                    object[] deleteOrderKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        nullOrderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        resultParam
                    };

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.RemoveKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @Result OUTPUT;", parameters: deleteOrderKeyDateParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderItemData.Remove(orderItem);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                #endregion

                #region KeyDate for Order

                // Create
                try
                {
                    object[] createOrderItemKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        orderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        nullDateTimeParam,
                        isFirmDateParam,
                        resultParam
                    };

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT;", parameters: createOrderItemKeyDateParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderItemData.Remove(orderItem);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                // Update
                try
                {
                    object[] updateOrderItemKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        orderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        dateTimeParam,
                        isFirmDateParam,
                        resultParam
                    };
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.ChangeKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @DateTime, @IsFirmDate, @Result OUTPUT;", parameters: updateOrderItemKeyDateParams);
                    Assert.AreEqual(0, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderItemData.Remove(orderItem);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }

                // Delete
                try
                {
                    object[] deleteOrderItemKeyDateParams = {
                        bidParam,
                        orderIdParam,
                        orderItemIdParam,
                        destinationIdParam,
                        keyDateTypeParam,
                        resultParam
                    };

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC [dbo].[Order.Action.RemoveKeyDate] @BID, @OrderID, @OrderItemID, @DestinationID, @KeyDateType, @Result OUTPUT;", parameters: deleteOrderItemKeyDateParams);
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception ex)
                {
                    #region Cleanup

                    ctx.OrderItemData.Remove(orderItem);
                    ctx.OrderData.Remove(order);

                    try
                    {
                        Assert.AreEqual(2, ctx.SaveChanges());
                    }
                    catch (Exception ex2)
                    {
                        Assert.Fail(ex2.ToString());
                    }

                    #endregion

                    Assert.Fail(ex.ToString());
                }
                #endregion

            }
        }

        

        [TestMethod]
        public async Task EFTestOrderDestinationCRUD()
        {
            using (var ctx = GetMockCtx(1))
            {
                OrderData order = new OrderData()
                {
                    BID = 1,
                    ID = -97,
                    ClassTypeID = (int)ClassType.Order,
                    ModifiedDT = DateTime.UtcNow,
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    OrderStatusStartDT = DateTime.UtcNow,
                    Number = 1000,
                    FormattedNumber = "INV-9999",
                    PriceTaxRate = 0.01m,
                    PaymentPaid = 0m
                };

                order.LocationID = ctx.LocationData.FirstOrDefault(t => t.BID == order.BID).ID;
                order.PickupLocationID = order.LocationID;
                order.ProductionLocationID = order.LocationID;
                order.TaxGroupID = ctx.TaxGroup.FirstOrDefault(t => t.BID == order.BID).ID;
                order.CompanyID = ctx.CompanyData.FirstOrDefault(t => t.BID == order.BID).ID;

                OrderDestinationData destination = new OrderDestinationData()
                {
                    BID = order.BID,
                    ID = -99,
                    OrderID = order.ID,
                    DestinationNumber = 1,
                    Name = "Test Order Destination",
                    OrderStatusID = OrderOrderStatus.OrderPreWIP,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(t => t.BID == order.BID).ID,
                    PriceIsLocked = false,
                    PriceNetOV = false,
                    PriceTaxableOV = false,
                    TaxGroupID = order.TaxGroupID,
                    TaxGroupOV = false,
                    HasDocuments = false,
                    TransactionType = (byte)OrderTransactionType.Destination
                };

                ctx.OrderData.Add(order);
                ctx.OrderDestinationData.Add(destination);

                try
                {
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    ctx.OrderData.Remove(order);
                    Assert.AreEqual(1, ctx.SaveChanges());
                    Assert.Fail(ex.ToString());
                }

                var destinationRetrieve = await ctx.OrderDestinationData.Where(o => o.BID == order.BID && o.OrderID == order.ID && o.ID == -99).FirstOrDefaultAsync();
                Assert.IsNotNull(destinationRetrieve);
                Assert.AreEqual(destinationRetrieve.Name, destination.Name);

                var newName = "Change Test Order Destination Name";
                destination.Name = newName;

                try
                {
                    ctx.SaveChanges();
                    Assert.AreEqual(destination.Name, newName);
                }
                catch (Exception ex)
                {
                    ctx.OrderDestinationData.Remove(destination);
                    ctx.OrderData.Remove(order);
                    Assert.AreEqual(2, ctx.SaveChanges());
                    Assert.Fail(ex.ToString());
                }

                await ctx.Entry(order).Collection(t => t.Destinations).LoadAsync();
                Assert.AreEqual(1, order.Destinations.Count);

                // Test with new loading, untracked to verify
                var noTrackingDestination = ctx.OrderDestinationData.AsNoTracking().Include(t => t.Order).Where(t => t.ID == destination.ID).FirstOrDefault(t => t.ID == destination.ID);
                Assert.IsNotNull(noTrackingDestination.Order);
                Assert.AreEqual(order.ID, noTrackingDestination.Order.ID);

                var noTrackingOrder = ctx.OrderDestinationData.AsNoTracking().Include(t => t.Order).Where(t => t.ID == destination.ID).FirstOrDefault(t => t.ID == destination.ID);
                Assert.IsNotNull(noTrackingDestination.Order);
                Assert.AreEqual(order.ID, noTrackingDestination.Order.ID);

                ctx.OrderDestinationData.Remove(destination);
                ctx.OrderData.Remove(order);

                try
                {
                    Assert.AreEqual(2, ctx.SaveChanges());
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.ToString());
                }
            }
        }
    }
}
