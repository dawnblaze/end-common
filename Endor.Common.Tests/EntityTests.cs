﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Common.Tests
{
    [TestClass]
    public class EntityTests
    {
        [TestMethod]
        public void EmployeeLocatorXMLRoundtripTest()
        {
            PhoneMetaData originalMetaData = new PhoneMetaData()
            {
                AreaCode = "123",
                CountryCode = "1",
                Extension = "3",
                PhoneNumber = "123-1234"
            };
            string soloXML = XMLUtilities.SerializeMetaDataToXML(originalMetaData, LocatorType.Phone);

            EmployeeLocator originalLocator = new EmployeeLocator()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int) ClassType.EmployeeLocator,
                LocatorType = (int) LocatorType.Phone,
                ModifiedDT = DateTime.UtcNow,
                ParentID = 1,
                MetaDataObject = originalMetaData
            };

            //the constructor should have initialized the metadata field
            string testXML = originalLocator.MetaDataXML;
            Assert.IsNotNull(testXML);

            //serialization should be the same
            Assert.AreEqual(testXML, soloXML);

            //blanking metadata should blank metaDataObject
            originalLocator.MetaDataXML = null;
            Assert.IsNull(originalLocator.MetaDataObject);

            //setting the metadata should create a metaDataObject on access
            originalLocator.MetaDataXML = testXML;
            Assert.IsNotNull(originalLocator.MetaDataObject);

            EmployeeLocator xmlFirstLocator = new EmployeeLocator()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int) ClassType.EmployeeLocator,
                LocatorType = (int) LocatorType.Phone,
                ModifiedDT = DateTime.UtcNow,
                ParentID = 1,
                MetaDataXML = soloXML
            };
            Assert.IsNotNull(xmlFirstLocator.MetaDataObject);

            string JSON = JsonConvert.SerializeObject(xmlFirstLocator);
            //the JSON should have our metadata as JSON
            Assert.IsTrue(JSON.Contains(JsonConvert.SerializeObject(originalMetaData)));
            //and should not have a MetaDataObject
            Assert.IsFalse(JSON.Contains("MetaDataObject"));

            EmployeeLocator fromJSON = JsonConvert.DeserializeObject<EmployeeLocator>(JSON);
            //the JSON deserializer should be able to materialize our JSON
            Assert.IsNotNull(fromJSON);
            //and should have MetaData filled in
            Assert.IsFalse(String.IsNullOrWhiteSpace(fromJSON.MetaDataXML));

            //the metadata should have the correct data
            object metaDataObjectFromJSON = fromJSON.MetaDataObject;
            PhoneMetaData phoneMetaDataFromJSON = metaDataObjectFromJSON as PhoneMetaData;
            Assert.IsNotNull(phoneMetaDataFromJSON);
            Assert.AreEqual(originalMetaData.AreaCode, phoneMetaDataFromJSON.AreaCode);
            Assert.AreEqual(originalMetaData.PhoneNumber, phoneMetaDataFromJSON.PhoneNumber);
        }

        [TestMethod]
        public void ListFilterXMLRoundtripTest()
        {
            ListFilterItem item = new ListFilterItem()
            {
                IsHidden = true,
                CategoryID = 1,
                DisplayText = "blah",
                Field = "IsActive",
                Label = "blah",
                SearchValue = "True"
            };
            var items = new ListFilterItem[] {item};
            string soloXML = XMLUtilities.SerializeMetaDataToXML(items, typeof(ListFilterItem[]));

            var listFilter = new SystemListFilter()
            {
                CriteriaObject = items
            };

            string testXML = listFilter.CriteriaXML;
            Assert.IsNotNull(testXML);

            //serialization should be the same
            Assert.AreEqual(testXML, soloXML);

            //blanking metadata should blank metaDataObject
            listFilter.CriteriaXML = null;
            Assert.IsNull(listFilter.CriteriaObject);

            //setting the Criteria should create a CriteriaObject on access
            listFilter.CriteriaXML = testXML;
            Assert.IsNotNull(listFilter.CriteriaObject);


            string JSON = JsonConvert.SerializeObject(listFilter);
            //the JSON should have our Criteria as JSON
            Assert.IsTrue(JSON.Contains(JsonConvert.SerializeObject(items)));
            //and should not have a CriteriaObject
            Assert.IsFalse(JSON.Contains("CriteriaObject"));

            SystemListFilter fromJSON = JsonConvert.DeserializeObject<SystemListFilter>(JSON);
            //the JSON deserializer should be able to materialize our JSON
            Assert.IsNotNull(fromJSON);
            //and should have Criteria filled in
            Assert.IsFalse(String.IsNullOrWhiteSpace(fromJSON.CriteriaXML));

            //the Criteria should have the correct data
            object CriteriaObjectFromJSON = fromJSON.CriteriaObject;
            ListFilterItem[] criteriaFromJSON = CriteriaObjectFromJSON as ListFilterItem[];
            Assert.IsNotNull(criteriaFromJSON);
            Assert.AreEqual(items[0].SearchValue, criteriaFromJSON[0].SearchValue);
        }

        [TestMethod]
        public void EmployeeRoleSerializationTest()
        {
            var employeeRole = new EmployeeRole()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int) ClassType.EmployeeRole,
                ModifiedDT = DateTime.UtcNow,
                Name = "Test Employee Role",
                AllowMultiple = false,
                AllowOnTeam = true,
                IsActive = true,
                IsSystem = false,
                OrderRestriction = RoleAccess.Optional,
                OrderItemRestriction = RoleAccess.Optional,
                OrderDestinationRestriction = RoleAccess.Optional,
                EstimateRestriction = RoleAccess.Optional,
                EstimateItemRestriction = RoleAccess.Optional,
                EstimateDestinationRestriction = RoleAccess.Optional,
                OpportunityRestriction = RoleAccess.Optional,
                PORestriction = RoleAccess.Optional
            };

            var employeeRoleStr = JsonConvert.SerializeObject(employeeRole);
            Assert.IsFalse(employeeRoleStr.Contains("BID"));
        }


        [TestMethod]
        public void MessageBodySerializationTest()
        {
            var msgBody = new MessageBody()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int) ClassType.MessageBody,
                ModifiedDT = DateTime.UtcNow,
                Subject = null,
                BodyFirstLine = null,
                HasBody = true,
                AttachedFileCount = 0,
                AttachedFileNames = null,
                HasAttachment = false,
                MetaData = null,
                WasModified = false,
                SizeInKB = null
            };

            //test serialization with null as specified in wiki https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/845578283/MessageBody+Object
            var msgBodyStr = JsonConvert.SerializeObject(msgBody);
            Assert.IsFalse(msgBodyStr.Contains("BID"));

            Assert.IsFalse(msgBodyStr.Contains("Subject"));
            Assert.IsFalse(msgBodyStr.Contains("BodyFirstLine"));
            Assert.IsFalse(msgBodyStr.Contains("AttachedFileNames"));
            Assert.IsFalse(msgBodyStr.Contains("MetaData"));
            Assert.IsFalse(msgBodyStr.Contains("SizeInKB"));

            msgBody.Subject = "test subject";
            msgBody.BodyFirstLine = "testing body first line...";
            msgBody.AttachedFileNames = "test-attachement";
            msgBody.MetaData = "testmeta data";
            msgBody.SizeInKB = 123;

            msgBodyStr = JsonConvert.SerializeObject(msgBody);
            Assert.IsTrue(msgBodyStr.Contains("Subject"));
            Assert.IsTrue(msgBodyStr.Contains("BodyFirstLine"));
            Assert.IsTrue(msgBodyStr.Contains("AttachedFileNames"));
            Assert.IsTrue(msgBodyStr.Contains("MetaData"));
            Assert.IsTrue(msgBodyStr.Contains("SizeInKB"));
        }

        [TestMethod]
        public void MessageDeliveryRecordSerializationTest()
        {
            var messageDeliveryRecord = new MessageDeliveryRecord()
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int) ClassType.MessageDeliveryRecord,
                ParticipantID = 500,
                AttemptNumber = 500,
                IsPending = false,
                ScheduledDT = DateTime.Now,
                AttemptedDT = DateTime.Now,
                WasSuccessful = false,
                FailureMessage = "FailureMessage",
                MetaData = "Metadata"
            };

            var serialized = JsonConvert.SerializeObject(messageDeliveryRecord);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }

        [TestMethod]
        public void MessageParticipantInfoSerializationTest()
        {
            var messageParticipantInfo = new MessageParticipantInfo
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int) ClassType.MessageParticipantInfo,
                BodyID = 1000,
                ParticipantRoleType = MessageParticipantRoleType.To,
                Channel = MessageChannelType.Email,
                UserName = "Test",
                IsMergeField = false,
                EmployeeID = 1000,
                TeamID = 1000,
                DeliveredDT = DateTime.Now,
                ContactID = 1000,
                IsDelivered = false
            };

            var serialized = JsonConvert.SerializeObject(messageParticipantInfo);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }

        [TestMethod]
        public void MessageHeaderSerializationTest()
        {
            var msgHeader = new MessageHeader
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageHeader,
                ReceivedDT = DateTime.Now,
                EmployeeID = 1000,
                ParticipantID = 1000,
                BodyID = 1000,
                IsRead = false,
                ReadDT = DateTime.Now,
                IsDeleted = false,
                DeletedOrExpiredDT = DateTime.Now,
                IsExpired = false,
                Channels = MessageChannelType.Message,
                InSentFolder = false
            };

            var serialized = JsonConvert.SerializeObject(msgHeader);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }

        [TestMethod]
        public void MessageObjectLinkSerializationTest()
        {
            var msgObjectLink = new MessageObjectLink
            {
                BID = 1,
                BodyID = 500,
                LinkedObjectClassTypeID = 500,
                LinkedObjectID = 1000,
                IsSourceOfMessage = false,
                DisplayText = "Display Text"
            };

            var serialized = JsonConvert.SerializeObject(msgObjectLink);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }


    }
}
