﻿using Endor.Common.Tests;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Common.Level2.Tests
{
    [TestClass]
    public class AssemblyEntityFrameworkTests : CommonEntityFrameworkTests
    {

        [ClassInitialize]
        public static void InitializeAssembly(TestContext context)
        {
            Initialize();
        }

        [TestMethod]
        public async Task EFTestAssemblyFilters()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var listFilters = await ctx.ListFilter.Where(x => x.TargetClassTypeID == Convert.ToInt32(ClassType.Assembly)).ToListAsync();
                Assert.IsNotNull(listFilters);
                Assert.IsTrue(listFilters.Count() >= 1);
                Assert.IsNotNull(listFilters.FirstOrDefault(x => x.Name == "All Assemblies"));
                Assert.IsNotNull(listFilters.FirstOrDefault(x => x.Name == "All Machine Templates"));
            }
        }
        [TestMethod]
        public async Task EFAssemblyIncludeVariablesFormulasTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                string[] includes = new string[] {
                    //"Layouts.Elements",
                    //"Variables",
                    //"AssemblyCategoryLinks.AssemblyCategory",
                    "Variables.Formulas"
                };

                var testQuery = await ctx.AssemblyData.IncludeAll(includes).ToListAsync();
                Assert.IsNotNull(testQuery);
            }
        }

        [TestMethod]
        public async Task EFAssemblyCRUDTest()
        {
            var assembly = new AssemblyData()
            {
                BID = 1,
                ID = -99,
                Name = "Test.Assembly"
            };

            var assemblyCategory = new AssemblyCategory()
            {
                BID = 1,
                ID = -99,
                Name = "Test.Assembly.Category"
            };

            var assemblyCategoryLink = new AssemblyCategoryLink()
            {
                BID = 1,
                PartID = -99,
                CategoryID = -99
            };

            using (var ctx = GetMockCtx(1))
            {
                var addAssembly = await ctx.AssemblyData.AddAsync(assembly);
                var addAssemblyCategory = await ctx.AssemblyCategory.AddAsync(assemblyCategory);
                var addAssemblyCategoryLink = await ctx.AssemblyCategoryLink.AddAsync(assemblyCategoryLink);
                await ctx.SaveChangesAsync();

                var getAssembly = await ctx.AssemblyData.Where(sa => sa.ID == -99 && sa.BID == 1).FirstOrDefaultAsync();
                Assert.IsNotNull(getAssembly);

                var getAssemblyCategory = await ctx.AssemblyCategory.Where(sa => sa.ID == -99 && sa.BID == 1).FirstOrDefaultAsync();
                Assert.IsNotNull(getAssemblyCategory);

                var getAssemblyCategoryLink = await ctx.AssemblyCategoryLink.Where(l => l.PartID == -99 && l.CategoryID == -99 && l.BID == 1).FirstOrDefaultAsync();
                Assert.IsNotNull(getAssemblyCategoryLink);

                getAssembly.Name += ".Updated";
                ctx.AssemblyData.Update(getAssembly);
                getAssemblyCategory.Name += ".Updated";
                ctx.AssemblyCategory.Update(getAssemblyCategory);
                await ctx.SaveChangesAsync();

                var updatedAssembly = await ctx.AssemblyData.Where(sa => sa.ID == -99 && sa.BID == 1).FirstOrDefaultAsync();
                Assert.IsNotNull(updatedAssembly);
                Assert.AreEqual("Test.Assembly.Updated", updatedAssembly.Name);

                var updatedAssemblyCategory = await ctx.AssemblyCategory.Where(sa => sa.ID == -99 && sa.BID == 1).FirstOrDefaultAsync();
                Assert.IsNotNull(updatedAssemblyCategory);
                Assert.AreEqual("Test.Assembly.Category.Updated", updatedAssemblyCategory.Name);

                ctx.AssemblyCategoryLink.Remove(assemblyCategoryLink);
                ctx.AssemblyCategory.Remove(updatedAssemblyCategory);
                ctx.AssemblyData.Remove(updatedAssembly);
                await ctx.SaveChangesAsync();

                getAssembly = await ctx.AssemblyData.Where(sa => sa.ID == -99 && sa.BID == 1).FirstOrDefaultAsync();
                Assert.IsNull(getAssembly);

                getAssemblyCategory = await ctx.AssemblyCategory.Where(sa => sa.ID == -99 && sa.BID == 1).FirstOrDefaultAsync();
                Assert.IsNull(getAssemblyCategory);

                getAssemblyCategoryLink = await ctx.AssemblyCategoryLink.Where(l => l.PartID == -99 && l.CategoryID == -99 && l.BID == 1).FirstOrDefaultAsync();
                Assert.IsNull(getAssemblyCategoryLink);
            }
        }


        [TestMethod]
        public async Task EFAssemblyElementTest()
        {
            var assembly = new AssemblyData()
            {
                BID = 1,
                ID = -99,
                Name = "Test.Assembly"
            };
            var assemblyLayout = new AssemblyLayout()
            {
                BID = 1,
                ID = -99,
                Name = "Test.Assembly",
                LayoutType = AssemblyLayoutType.AssemblyEcommerce,
                AssemblyID = -99
            };

            var element = new AssemblyElement()
            {
                BID = 1,
                ID = -99,
                IsDisabled = false,
                IsReadOnly = false,
                Column = 1,
                ColumnsWide = 2,
                Row = 1,
                ElementType = AssemblyElementType.Checkbox,
                DataType = DataType.Number,
                AssemblyID = -99,
                LayoutID = -99,
                VariableName = "Test.Assembly.Element"
            };

            using (var ctx = GetMockCtx(1))
            {
                var existingAssembly = await ctx.AssemblyData.Where(x => x.BID == 1 && x.ID == assembly.ID).FirstOrDefaultAsync();
                var existingLayout = await ctx.AssemblyLayout.Where(x => x.BID == 1 && x.ID == assemblyLayout.ID).FirstOrDefaultAsync();
                var existingElement = await ctx.AssemblyElement.Where(x => x.BID == 1 && x.ID == element.ID).FirstOrDefaultAsync();

                if (existingElement != null)
                    ctx.AssemblyElement.Remove(existingElement);
                if (existingLayout != null)
                    ctx.AssemblyLayout.Remove(existingLayout);
                if (existingAssembly != null)
                    ctx.AssemblyData.Remove(existingAssembly);
                await ctx.SaveChangesAsync();

                var addAssembly = await ctx.AssemblyData.AddAsync(assembly);
                Assert.IsNotNull(addAssembly);
                var addAssemblyLayout = await ctx.AssemblyLayout.AddAsync(assemblyLayout);
                Assert.IsNotNull(addAssemblyLayout);
                var addElement = await ctx.AssemblyElement.AddAsync(element);
                Assert.IsNotNull(addElement);
                await ctx.SaveChangesAsync();

                var getAssembly = await ctx.AssemblyData.Where(x => x.BID == 1 && x.ID == assembly.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(getAssembly);

                element.ColumnsWide = 1;
                ctx.AssemblyElement.Update(element);
                await ctx.SaveChangesAsync();

                var getLayout = await ctx.AssemblyLayout.Where(x => x.BID == 1 && x.ID == assemblyLayout.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(getLayout);

                var getElement = await ctx.AssemblyElement.Where(x => x.BID == 1 && x.ID == element.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(getElement);
                Assert.AreEqual(1, getElement.ColumnsWide);

                ctx.AssemblyElement.Remove(getElement);
                ctx.AssemblyLayout.Remove(getLayout);
                ctx.AssemblyData.Remove(getAssembly);
                await ctx.SaveChangesAsync();

                getAssembly = await ctx.AssemblyData.Where(x => x.BID == 1 && x.ID == assembly.ID).FirstOrDefaultAsync();
                Assert.IsNull(getAssembly);
                getLayout = await ctx.AssemblyLayout.Where(x => x.BID == 1 && x.ID == assemblyLayout.ID).FirstOrDefaultAsync();
                Assert.IsNull(getLayout);
                getElement = await ctx.AssemblyElement.Where(x => x.BID == 1 && x.ID == element.ID).FirstOrDefaultAsync();
                Assert.IsNull(getElement);
            }
        }
        [TestMethod]
        public void AssemblyVariableTest()
        {
            int testAssemblyID = -100;
            using (ApiContext ctx = GetMockCtx(1))
            {
                try
                {
                    var ProfileSetupHint = "hello";
                    var ProfileSetupLabel = "world";

                    var foundoldone = ctx.AssemblyVariable.FirstOrDefault(t => t.ID == testAssemblyID && t.BID == 1);
                    if (foundoldone != null)
                    {
                        ctx.AssemblyVariable.Remove(foundoldone);
                    }

                    var foundoldanotherone = ctx.AssemblyData.FirstOrDefault(t => t.ID == testAssemblyID && t.BID == 1);
                    if (foundoldanotherone != null)
                    {
                        ctx.AssemblyData.Remove(foundoldanotherone);
                    }

                    ctx.SaveChanges();
                    AssemblyData assemblyData = new AssemblyData() { BID = 1, Name = "Test1" + DateTime.Now.Ticks.ToString(), ID = testAssemblyID };
                    assemblyData.Variables = new List<AssemblyVariable>();
                    assemblyData.Variables.Add(new AssemblyVariable()
                    {
                        BID = 1,
                        Name = "Test1" + DateTime.Now.Ticks.ToString(),
                        ID = testAssemblyID,
                        EnableProfileOV = true,
                        ProfileSetupHint = ProfileSetupHint,
                        ProfileSetupLabel = ProfileSetupLabel

                    });


                    ctx.AssemblyData.Add(assemblyData);

                    ctx.SaveChanges();

                    var testModel = ctx.AssemblyVariable
                                        .AsNoTracking()
                                        .FirstOrDefault(t => t.AssemblyID == testAssemblyID &&
                                            t.BID == assemblyData.BID &&
                                            t.ID == testAssemblyID
                                        );

                    Assert.IsTrue(testModel.EnableProfileOV, "EnableProfileOV");
                    Assert.AreEqual(testModel.ProfileSetupHint, ProfileSetupHint, "ProfileSetupHint");
                    Assert.AreEqual(testModel.ProfileSetupLabel, ProfileSetupLabel, "ProfileSetupLabel");

                    Assert.IsNotNull(ctx.AssemblyVariable.FirstOrDefault(t => t.AssemblyID == assemblyData.ID && t.BID == assemblyData.BID));
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
                finally
                {
                    var cleanUpVariable = ctx.AssemblyVariable.FirstOrDefault(t => t.ID == testAssemblyID && t.BID == 1);
                    if (cleanUpVariable != null)
                        ctx.AssemblyVariable.Remove(cleanUpVariable);

                    var cleanUpAssembly = ctx.AssemblyData.FirstOrDefault(t => t.ID == testAssemblyID && t.BID == 1);
                    if (cleanUpAssembly != null)
                        ctx.AssemblyData.Remove(cleanUpAssembly);

                    ctx.SaveChanges();
                }
            }
        }

        [TestMethod]
        public void AssemblyVariableFormulaTest()
        {
            int testAssemblyID = -100;
            using (ApiContext ctx = GetMockCtx(1))
            {
                var foundoldone = ctx.AssemblyVariableFormula.FirstOrDefault(t => t.ID == testAssemblyID && t.BID == 1);
                if (foundoldone != null)
                {
                    ctx.AssemblyVariableFormula.Remove(foundoldone);
                }
                var foundoldoneVar = ctx.AssemblyVariable.FirstOrDefault(t => t.ID == testAssemblyID && t.BID == 1);
                if (foundoldoneVar != null)
                {
                    ctx.AssemblyVariable.Remove(foundoldoneVar);
                }

                var foundoldanotherone = ctx.AssemblyData.FirstOrDefault(t => t.ID == testAssemblyID && t.BID == 1);
                if (foundoldanotherone != null)
                {
                    ctx.AssemblyData.Remove(foundoldanotherone);
                }

                ctx.SaveChanges();
                AssemblyData assemblyData = new AssemblyData() { BID = 1, Name = "Test1" + DateTime.Now.Ticks.ToString(), ID = testAssemblyID };
                assemblyData.Variables = new List<AssemblyVariable>();
                assemblyData.Variables.Add(new AssemblyVariable() { BID = 1, Name = "Test1" + DateTime.Now.Ticks.ToString(), ID = testAssemblyID });
                ctx.AssemblyData.Add(assemblyData);
                ctx.SaveChanges();

                AssemblyVariableFormula assemblyVFData = new AssemblyVariableFormula() { BID = 1, ID = testAssemblyID, VariableID = testAssemblyID };
                ctx.AssemblyVariableFormula.Add(assemblyVFData);

                ctx.SaveChanges();

                Assert.IsNotNull(ctx.AssemblyVariableFormula.FirstOrDefault(t => t.ID == assemblyVFData.ID && t.BID == assemblyVFData.BID));

                ctx.AssemblyVariableFormula.RemoveRange(ctx.AssemblyVariableFormula.Where(t => t.ID == testAssemblyID && t.BID == 1));
                ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(t => t.ID == testAssemblyID && t.BID == 1));
                ctx.AssemblyData.RemoveRange(ctx.AssemblyData.Where(t => t.ID == testAssemblyID && t.BID == 1));
                ctx.SaveChanges();

            }
        }

        [TestMethod]
        public void SystemAssemblyVariableTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.SystemAssemblyVariable.FirstOrDefault(t => t.ID.Equals(1) && t.VariableName.Equals("Quantity")));
                Assert.IsNotNull(ctx.SystemAssemblyVariable.FirstOrDefault(t => t.ID.Equals(2) && t.VariableName.Equals("Tier")));
                Assert.IsNotNull(ctx.SystemAssemblyVariable.FirstOrDefault(t => t.ID.Equals(6) && t.VariableName.Equals("Height")));
                Assert.IsNotNull(ctx.SystemAssemblyVariable.FirstOrDefault(t => t.ID.Equals(7) && t.VariableName.Equals("Width")));
                Assert.IsNotNull(ctx.SystemAssemblyVariable.FirstOrDefault(t => t.ID.Equals(8) && t.VariableName.Equals("Area")));
            }
        }

        [TestMethod]
        public void SystemAssemblyLayoutTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.AssemblyLayout.ToList());
            }
        }

        [TestMethod]
        public void SimpleSystemAssemblyVariableTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.SimpleSystemAssemblyVariable.ToList());
            }
        }

        [TestMethod]
        public async Task EFAssemblyTableTest()
        {
            const int BID = 1;
            AssemblyVariableTest(); //need to add a sample assembly data
            using (var ctx = GetMockCtx(BID))
            {
                var assemblyTable = new AssemblyTable()
                {
                    BID = BID,
                    Label = "Test.AssemblyTable",
                    TableType = AssemblyTableType.Custom,
                    CellDataJSON = "",
                    VariableName = "",
                    AssemblyID = ctx.AssemblyData.FirstOrDefault().ID

                };

                // create
                await ctx.AssemblyTable.AddAsync(assemblyTable);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // retrieve
                var getAssemblyTable = await ctx.AssemblyTable.FirstOrDefaultAsync(q => q.ID == assemblyTable.ID && q.BID == assemblyTable.BID);
                Assert.IsNotNull(assemblyTable);
                // update
                assemblyTable.Label = "Test.AssemblyTable1";
                ctx.AssemblyTable.Update(assemblyTable);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // delete
                ctx.AssemblyTable.Remove(assemblyTable);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
            }

        }

        [TestMethod]
        public async Task AssemblyDataAssemblyTypeBasicTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.AssemblyData.Where(e => e.AssemblyType >= 0).ToListAsync());
            }
        }

        [TestMethod]
        public async Task LayoutType12NamedOther()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var type12Assemblies = await ctx.AssemblyLayout
                                        .Where(a => a.LayoutType == AssemblyLayoutType.MachineTemplateMaterialLayout)
                                        .ToListAsync();
                Assert.IsTrue(type12Assemblies.All(a => a.Name == "Other"));
            }
        }

        [TestMethod]
        public void AssemblyDataSimpleTest()
        {
            short TestBID = 1;
            using (var ctx = GetMockCtx(TestBID))
            {
                var assemblyData = ctx.AssemblyData
                    .Where(t => t.BID == TestBID && t.ID < 0
                        || t.TaxabilityCodeID < 0
                        || t.IncomeAccountID < 0).ToList();
                Assert.IsNotNull(assemblyData);
            }
        }


        [TestMethod]
        public async Task AssembliesDoNotHaveLaborOrMaterialDropdowns()
        {
            short TestBID = 1;
            using (var ctx = GetMockCtx(TestBID))
            {
                var labMatDropdownVariables = await ctx.AssemblyVariable
                    .Where(v => 
                        v.BID == TestBID && v.ElementType == AssemblyElementType.DropDown && 
                        (v.ListDataType == DataType.LaborPart || v.ListDataType == DataType.MaterialPart)
                    ).ToListAsync();

                Assert.AreEqual(0, labMatDropdownVariables.Count, "dropdown variables should no longer use labor or material type anymore after END-10884");
            }
        }
    }
}
