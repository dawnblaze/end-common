﻿using Endor.Common.Tests;
using Endor.EF;
using Endor.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Common.Level2.Tests
{
    [TestClass]
    public class CustomField : AutomaticTreeCleanupTests
    {
        [ClassInitialize]
        public static void InitializeAssembly(TestContext context)
        {
        }

        protected override void DoCleanupEntityTree()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                ctx.CustomFieldDefinition.RemoveWhereNegativeShort();
                ctx.CustomFieldLayoutDefinition.RemoveWhereNegativeShort();
                ctx.CustomFieldLayoutElement.RemoveWhereNegativeInt();
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public async Task CustomFieldDefinitionTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var newCFD = new CustomFieldDefinition()
                {
                    ID = -99,
                    BID = 1,
                    IsActive = true,
                    IsSystem = true,
                    AppliesToClassTypeID = 10000, //Order
                    Name = "my checkbox",
                    DataType = 0, //0=string
                    ElementType = AssemblyElementType.Checkbox,
                    HasValidators = false
                };
                //CREATE
                ctx.Add(newCFD);
                Assert.IsTrue(ctx.SaveChanges() > 0);//test insert

                //READ
                var myCFD = ctx.CustomFieldDefinition.FirstOrDefault(p => p.ID == -99);
                Assert.IsNotNull(myCFD);//test select

                //UPDATE
                myCFD.Name = "my checkbox 2";
                await ctx.SaveChangesAsync();
                var updateMyCFD = ctx.CustomFieldDefinition.FirstOrDefault(p => p.ID == -99);
                Assert.IsTrue(updateMyCFD.Name == "my checkbox 2");//test update

                //DELETE
                ctx.Remove(updateMyCFD);
                await ctx.SaveChangesAsync();
                var deleteMyCFD = await ctx.CustomFieldDefinition.Where(p => p.ID == -99).FirstOrDefaultAsync();
                Assert.IsNull(deleteMyCFD);//test delete
            }
        }

        [TestMethod]
        //https://api.media.atlassian.com/file/515aba5f-2f33-4894-91b7-542d338d37e2/image?token=eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI3MzI1NjQ1MC0xMzlhLTQ2ZWUtOTdhZS00ODUwZmJiNzQxMDYiLCJhY2Nlc3MiOnsidXJuOmZpbGVzdG9yZTpmaWxlOjUxNWFiYTVmLTJmMzMtNDg5NC05MWI3LTU0MmQzMzhkMzdlMiI6WyJyZWFkIl19LCJleHAiOjE1MzUwMzY3NjMsIm5iZiI6MTUzNTAzMzcwM30.P8rrMpFbf92QUYtHbGsmkDgDeblze-HBi2vnZz7TLmQ&client=73256450-139a-46ee-97ae-4850fbb74106&name=image2018-8-22_14-29-15.png&max-age=2940&width=900&height=273
        public async Task NavProp_CustomFieldLayoutDefinitionTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var fieldDef = new CustomFieldDefinition()
                {
                    ID = -99,
                    BID = 1,
                    Name = "asdf",
                    ModifiedDT = DateTime.UtcNow,
                    DisplayType = CustomFieldNumberDisplayType.General
                };
                ctx.Add(fieldDef);
                await ctx.SaveChangesAsync();

                var layoutDef = new CustomFieldLayoutDefinition() { 
                    ID = -99,
                    BID = 1,
                    Name = "waa",
                    ModifiedDT = DateTime.UtcNow
                };
                ctx.Add(layoutDef);
                await ctx.SaveChangesAsync();

                var containerElement = new CustomFieldLayoutElement()
                {
                    BID = 1,
                    ID = -99,
                    Name = "rororo",
                    LayoutID = layoutDef.ID,
                    ModifiedDT = DateTime.UtcNow
                };
                ctx.Add(containerElement);
                await ctx.SaveChangesAsync();

                var checkboxElement = new CustomFieldLayoutElement()
                {
                    ID = -98,
                    BID = 1,
                    Name = "wa",
                    ModifiedDT = DateTime.UtcNow,
                    LayoutID = layoutDef.ID,
                    ParentID = containerElement.ID,
                    DefinitionID = fieldDef.ID
                };
                ctx.Add(checkboxElement);
                await ctx.SaveChangesAsync();

                var containerWithChildren = await ctx.CustomFieldLayoutElement
                                            .Include(x => x.Elements)
                                            .FirstOrDefaultAsync(x => x.ID == containerElement.ID);

                var layoutWithElements = await ctx.CustomFieldLayoutDefinition
                                              .Include(x => x.Elements)
                                              .FirstOrDefaultAsync(x => x.ID == layoutDef.ID);

                var elemWithContainerWithDefinition = await ctx.CustomFieldLayoutElement
                                            .Include(x => x.Definition)
                                            .FirstOrDefaultAsync(x => x.ID == checkboxElement.ID);

                Assert.AreNotEqual(0, containerWithChildren.Elements.Count(), "container elements should have children");
                Assert.AreNotEqual(0, layoutWithElements.Elements.Count(), "layouts should have elements");
                Assert.IsNotNull(elemWithContainerWithDefinition.Definition, "elements should have definitions");
            }
        }

        [TestMethod]
        public async Task SavingTree_CustomFieldLayoutDefinitionTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var fieldDef = new CustomFieldDefinition()
                {
                    BID = 1,
                    ID = -99,
                    IsActive = true,
                    IsSystem = true,
                    AppliesToClassTypeID = 10000, //Order
                    Name = "my checkbox",
                    DataType = 0, //0=string
                    ElementType = AssemblyElementType.Checkbox,
                    HasValidators = false
                };
                ctx.Add(fieldDef);

                var layoutDef = new CustomFieldLayoutDefinition()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.CustomFieldLayoutDefinition,
                    ModifiedDT = DateTime.UtcNow,
                    IsActive = true,
                    IsSystem = true,
                    AppliesToClassTypeID = (int)ClassType.Company,
                    IsAllTab = true,
                    IsSubTab = true,
                    SortIndex = 1,
                    Name = "Test Custom Field Layout Definition-" + DateTime.Now.ToString("yyyyMMddHHmmssffff")
                };
                const int sectionID = -99;
                const int groupID = -98;
                layoutDef.Elements = new List<CustomFieldLayoutElement> {
                    new CustomFieldLayoutElement()
                    {
                                        BID = 1,
                        ID = sectionID,
                        Name = "Test Custom Field Layout Definition Container 2-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                        LayoutID = layoutDef.ID,
                        SortIndex = 2,
                        DefinitionID = null,
                        Elements = new List<CustomFieldLayoutElement>()
                        {
                            new CustomFieldLayoutElement()
                            {
                                        BID = 1,
                                ID = groupID,
                                ParentID = sectionID,
                                Name = "Test Custom Field Layout Definition Container 1-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                Column = 1,
                                LayoutID = layoutDef.ID,
                                SortIndex = 1,
                                DefinitionID = null,
                                Elements = new List<CustomFieldLayoutElement>
                                {
                                    new CustomFieldLayoutElement()
                                    {
                                        BID = 1,
                                        ID = -97,
                                        Name = "Test Custom Field Layout Definition Element 1-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                        SortIndex = 1,
                                        LayoutID = layoutDef.ID,
                                        DataType = (short) DataType.Boolean,
                                        ParentID = groupID,
                                        DefinitionID = fieldDef.ID
                                    },
                                    new CustomFieldLayoutElement()
                                    {
                                        BID = 1,
                                        ID = -96,
                                        LayoutID = layoutDef.ID,
                                        Name = "Test Custom Field Layout Definition Element 2-" + DateTime.Now.ToString("yyyyMMddHHmmssffff"),
                                        SortIndex = 2,
                                        DataType = (short) DataType.Boolean,
                                        ParentID = groupID,
                                        DefinitionID = fieldDef.ID
                                    }
                                }
                            }
                        }
                    }
                };

                ctx.Set<CustomFieldLayoutDefinition>().Add(layoutDef);
                await ctx.SaveChangesAsync();

                var retrieved = await ctx.CustomFieldLayoutElement
                                    .AsNoTracking()
                                    .Include(x => x.Elements)
                                    .ThenInclude(x => x.Elements)
                                    .FirstOrDefaultAsync(x => x.ID == -98);

                Assert.IsTrue(retrieved.Elements.Count > 0, "retrieved.ChildContainers.Count > 0");
            }
        }

        [TestMethod]
        public async Task CustomFieldLayoutElementTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.CustomFieldDefinition.ToListAsync());
                Assert.IsNotNull(await ctx.CustomFieldLayoutDefinition.ToListAsync());
                Assert.IsNotNull(await ctx.CustomFieldLayoutElement.ToListAsync());
            }
        }
    }
}
