﻿using Endor.AzureStorage;
using Endor.DocumentStorage.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Endor.Common.Tests
{
    [TestClass]
    public class EntityStorageClientTests
    {
        private string DevelopmentConnectionString = null;
        private const string TestPNGFilename = "1x1pixel.png";
        private const string TestPNGFilePath = @"Endor.Common.Tests\" + TestPNGFilename;
        private const short TestBID = 254;
        private CloudBlobClient CloudBlobClient = null;
        private CloudStorageAccount StorageAccount = null;
        private static async Task<CloudBlobClient> GetBlobClient()
        {            
            return (await GetStorageAccount()).CreateCloudBlobClient();
        }

        private static async Task<CloudStorageAccount> GetStorageAccount()
        {
            string connectionString = (await new Endor.Common.Tests.MockTenantDataCache().Get(1)).StorageConnectionString;
            return CloudStorageAccount.Parse(connectionString);
        }

        [TestInitialize]
        public async Task Initialize()
        {
            var cache = new Endor.Common.Tests.MockTenantDataCache();
            this.DevelopmentConnectionString = (await cache.Get(1)).StorageConnectionString;
            this.StorageAccount = CloudStorageAccount.Parse(DevelopmentConnectionString);
            this.CloudBlobClient = StorageAccount.CreateCloudBlobClient();
        }

        [TestMethod]
        public async Task AddFileTest()
        {
            short bid = 1;
            int classtypeid = 3500;
            int id = 1;
            int creatorID = 1;
            int creatorCTID = 5000;
            DMID dmID = new DMID() { ctid = classtypeid, id = id };
            string fileName = "Image";
            string fileContent = DateTime.UtcNow.ToFileTime().ToString();

            var blobClient = CloudBlobClient;
            await ManuallyDeleteIfExists(bid, Bucket.Documents, classtypeid, id, fileName, blobClient);

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            DMItem result = await AddFile(client, StorageBin.Permanent, dmID, fileName, creatorID, creatorCTID, content: fileContent);

            ICloudBlob blobRef = await blobClient.GetBlobReferenceFromServerAsync(new Uri(blobClient.BaseUri,
                $"{blobClient.BaseUri.PathAndQuery}bid{bid}/{Bucket.Documents}/{classtypeid}/{id}/{fileName}"));

            MemoryStream assertStream = new MemoryStream();
            await blobRef.DownloadToStreamAsync(assertStream);
            assertStream.Position = 0;
            StreamReader sr = new StreamReader(assertStream);
            Assert.AreEqual(fileContent, sr.ReadToEnd());

            // This is testing a null stream. I am trying to handle edge cases appropriately
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () =>
            {
                await client.AddFile(null, StorageBin.Permanent, Bucket.Documents, new DMID() { ctid = classtypeid, id = id + 1 }, "", null, creatorID, creatorCTID);
            });

            //Test Exception Thrown when created by id not provided for non Data bucket
            await Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                using (var ms = new MemoryStream())
                {
                    await blobRef.DownloadToStreamAsync(ms);
                    ms.Position = 0;
                    await client.AddFile(ms, StorageBin.Permanent, Bucket.Documents, new DMID() { ctid = classtypeid, id = id + 1 }, "", null, createdByID: null, createdByCTID: creatorCTID);
                }
            });

            //Test Exception Thrown when created by ctid not provided for non Data bucket
            await Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                using (var ms = new MemoryStream())
                {
                    await blobRef.DownloadToStreamAsync(ms);
                    ms.Position = 0;
                    await client.AddFile(ms, StorageBin.Permanent, Bucket.Documents, new DMID() { ctid = classtypeid, id = id + 1 }, "", null, createdByID: creatorID, createdByCTID: null);
                }
            });

            //Test Exception Thrown when created by id uses default(int) for non Data bucket
            await Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                using (var ms = new MemoryStream())
                {
                    await blobRef.DownloadToStreamAsync(ms);
                    ms.Position = 0;
                    await client.AddFile(ms, StorageBin.Permanent, Bucket.Documents, new DMID() { ctid = classtypeid, id = id + 1 }, "", null, createdByID: default(int), createdByCTID: creatorCTID);
                }
            });

            //Test Exception Thrown when created by ctid uses default(int) for non Data bucket
            await Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                using (var ms = new MemoryStream())
                {
                    await blobRef.DownloadToStreamAsync(ms);
                    ms.Position = 0;
                    await client.AddFile(ms, StorageBin.Permanent, Bucket.Documents, new DMID() { ctid = classtypeid, id = id + 1 }, "", null, createdByID: creatorID, createdByCTID: default(int));
                }
            });

            Assert.IsNotNull(result);

            result = await AddFile(client, StorageBin.Permanent, dmID, "foo/bar/text.txt", creatorID, creatorCTID, content: fileContent);
            Assert.IsNotNull(result);
            result = await AddFile(client, StorageBin.Permanent, dmID, "foo/bar/buzz/fizz/text.txt", creatorID, creatorCTID, content: fileContent);
            Assert.IsNotNull(result);

            result = await AddFile(client, StorageBin.Permanent, dmID, "foo/bar/buzz/fizz/boom/text.txt", null, null, Bucket.Data);
            Assert.IsNotNull(result);

            await Assert.ThrowsExceptionAsync<ArgumentException>(async () =>
            {
                await AddFile(client, StorageBin.Permanent, dmID, "foo/bar/buzz/fizz/boom/text.txt", null, null);
            });
        }

        [TestMethod]
        public async Task DeleteBusinessContainer()
        {
            short bid = 5; // Don't use BID 1, we don't want to wipe our primary container during testing
            int classtypeid = 3500;
            int id = 1;
            int creatorID = 1;
            int creatorCTID = 5000;
            DMID dmID = new DMID() { ctid = classtypeid, id = id };
            string fileName = "Image";
            string fileContent = DateTime.UtcNow.ToFileTime().ToString();

            var blobClient = CloudBlobClient;
            await ManuallyDeleteIfExists(bid, Bucket.Documents, classtypeid, id, fileName, blobClient);
            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            DMItem result = await AddFile(client, StorageBin.Permanent, dmID, fileName, creatorID, creatorCTID, content: fileContent);
            Assert.IsTrue(await client.DeleteBusinessBlobContainer(bid));
        }

        [TestMethod]
        public async Task PathTest()
        {
            short bid = 1;
            int classtypeid = 3500;
            int id = GetHashIDForTestMethod();
            DMID dmID = new DMID() { ctid = classtypeid, id = id };
            int creatorID = 1;
            int creatorCTID = 5000;

            string fileName = "thing.txt";

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            await client.DeleteAll(StorageBin.Permanent, dmID);

            Assert.IsNotNull(await AddFile(client, StorageBin.Permanent, dmID, fileName, creatorID, creatorCTID));
            Assert.IsNotNull(await AddFile(client, StorageBin.Permanent, dmID, $"foo/bar/{fileName}", creatorID, creatorCTID));
            Assert.IsNotNull(await AddFile(client, StorageBin.Permanent, dmID, $"foo/bar/buzz/fizz/{fileName}", creatorID, creatorCTID));

            var docs = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, docs.Count);

            var thing = docs.FirstOrDefault(t => !t.IsFolder);
            Assert.IsNotNull(thing);
            Assert.AreEqual("", thing.Path);

            var fooFolder = docs.FirstOrDefault(f => f.IsFolder);
            Assert.IsNotNull(fooFolder);
            Assert.AreEqual("", fooFolder.Path);
            Assert.IsNotNull(fooFolder.Contents);

            var barFolder = fooFolder.Contents.FirstOrDefault(f => f.IsFolder);
            Assert.IsNotNull(barFolder);
            Assert.AreEqual("foo/", barFolder.Path);
            Assert.IsNotNull(barFolder.Contents);

            var buzzFolder = barFolder.Contents.FirstOrDefault(f => f.IsFolder);
            Assert.IsNotNull(buzzFolder);
            Assert.AreEqual("foo/bar/", buzzFolder.Path);
            Assert.IsNotNull(buzzFolder.Contents);

            var fizzFolder = buzzFolder.Contents.FirstOrDefault(f => f.IsFolder);
            Assert.IsNotNull(fizzFolder);
            Assert.AreEqual("foo/bar/buzz/", fizzFolder.Path);
            Assert.IsNotNull(fizzFolder.Contents);

            var fizzFile = fizzFolder.Contents.FirstOrDefault(f => !f.IsFolder);
            Assert.IsNotNull(fizzFile);
            Assert.AreEqual("foo/bar/buzz/fizz/", fizzFile.Path);
            
            docs = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID, true);
            Assert.AreEqual(3, docs.Count);
            Assert.AreEqual(0, docs.Where(x => x.IsFolder == true).Count());
        }

        [TestMethod]
        public async Task SASUrlTest()
        {
            short bid = 1;
            int classtypeid = 3500;
            int id = 1;
            string fileName = "sasTest";
            int creatorID = 1;
            int creatorCTID = 5000;

            await ManuallyDeleteIfExists(bid, Bucket.Documents, classtypeid, id, fileName);

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            DMItem result = await AddFile(client, StorageBin.Permanent, new DMID() { ctid = classtypeid, id = id }, fileName, creatorID, creatorCTID);
            Assert.IsNotNull(result);
            Uri canonical = await client.GetCanonicalURLWithReadSASTokenAsync(result);
            Assert.IsNotNull(canonical);
            Assert.IsTrue(canonical.IsAbsoluteUri);
            Assert.IsTrue(canonical.ToString().Contains(fileName));
            Assert.IsTrue(canonical.ToString().Contains("?"));
            Assert.IsTrue(canonical.ToString().Contains("sig="));
        }

        [TestMethod]
        public async Task DuplicateTest()
        {
            short bid = 1;
            int classtypeid = 3500;
            int id = 1;
            string fileName = "dupeTest.txt";
            string folderName = "myFolder.gotcha";
            string fileInFolderName = $"{folderName}/dupeTest.txt";
            string expectedDupeName = "dupeTest (2).txt";
            string expectedDupeNameTwo = "dupeTest (3).txt";
            var dmID = new DMID() { ctid = classtypeid, id = id };
            int creatorID = 1;
            int creatorCTID = 5000;

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            await client.DeleteAll(StorageBin.Permanent, dmID);

            //upload test files
            DMItem result = await AddFile(client, StorageBin.Permanent, dmID, fileName, creatorID, creatorCTID);
            Assert.IsNotNull(result);
            result = await AddFile(client, StorageBin.Permanent, dmID, fileInFolderName, creatorID, creatorCTID);
            Assert.IsNotNull(result);


            //test first duplicate
            int duplicated = await client.Duplicate(StorageBin.Permanent, Bucket.Documents, dmID, new string[] { fileName });
            Assert.IsTrue(duplicated == 1);

            var docs = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == fileName));
            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == expectedDupeName));

            //test second duplicate
            duplicated = await client.Duplicate(StorageBin.Permanent, Bucket.Documents, dmID, new string[] { fileName });
            Assert.IsTrue(duplicated == 1);

            docs = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == fileName));
            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == expectedDupeName));
            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == expectedDupeNameTwo));

            //test file inside folder (and folder has dot in name for good measure)
            duplicated = await client.Duplicate(StorageBin.Permanent, Bucket.Documents, dmID, new string[] { fileInFolderName });
            Assert.IsTrue(duplicated == 1);

            docs = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            var folder = docs.FirstOrDefault(f => f.IsFolder && f.Name == folderName);
            Assert.IsNotNull(folder);
            Assert.IsNotNull(folder.Contents);
            Assert.IsNotNull(folder.Contents.FirstOrDefault(f => !f.IsFolder && f.Name == expectedDupeName));
            Assert.AreEqual(2, folder.Contents.Count);
        }

        private static async Task ManuallyDeleteStaticIfExists(short bid, Bucket bucket, int classtypeid, int id, string fileName, CloudBlobClient blobClient = null)
        {
            CloudBlobContainer cbc = (blobClient ?? await GetBlobClient()).GetContainerReference($"bid{bid}");
            CloudBlockBlob cbb = cbc.GetBlockBlobReference($"{StorageBin.Permanent.BlobNamePrefix(bucket, new DMID() { id = id, ctid = classtypeid, classFolder = "static" })}/{fileName}");
            await cbb.DeleteIfExistsAsync();
        }

        private static async Task ManuallyDeleteAssociationIfExists(short aid, Bucket bucket, int classtypeid, int id, string fileName, CloudBlobClient blobClient = null)
        {
            CloudBlobContainer cbc = (blobClient ?? await GetBlobClient()).GetContainerReference($"aid{aid}");
            CloudBlockBlob cbb = cbc.GetBlockBlobReference($"{StorageBin.Permanent.BlobNamePrefix(bucket, new DMID() { id = id, ctid = classtypeid, classFolder = "static" })}/{fileName}");
            await cbb.DeleteIfExistsAsync();
        }


        private static async Task ManuallyDeleteIfExists(short bid, Bucket bucket, int classtypeid, int id, string fileName, CloudBlobClient blobClient = null)
        {
            await (blobClient ?? await GetBlobClient())
                .GetContainerReference($"bid{bid}")
                .GetBlockBlobReference($"{StorageBin.Permanent.BlobNamePrefix(bucket, new DMID() { id = id, ctid = classtypeid })}/{fileName}")
                .DeleteIfExistsAsync();
        }

        private static async Task ManuallyDeleteIfExists(short bid, Bucket bucket, Guid tempGuid, string fileName, CloudBlobClient blobClient = null)
        {
            await (blobClient ?? await GetBlobClient())
                .GetContainerReference($"bid{bid}")
                .GetBlockBlobReference($"{StorageBin.Temp.BlobNamePrefix(bucket, new DMID() { guid = tempGuid })}/{fileName}")
                .DeleteIfExistsAsync();
        }

        [TestMethod]
        [Ignore]
#warning This test needs to be re-evaluated
        [DeploymentItem(TestPNGFilePath)]
        public async Task StoreDefaultImageTest()
        {
            short bid = 10000;
            int classtypeid = 5000;
            int id = 1;
            string fileName = "Image";
            int creatorID = 1;
            int creatorCTID = 5000;

            byte[] pngBytes = File.ReadAllBytes(TestPNGFilename);
            int streamLength = pngBytes.Length;

            var blobClient = await GetBlobClient();

            await ManuallyDeleteIfExists(bid, Bucket.Data, classtypeid, id, fileName, blobClient);

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            DMID imageID = new DMID() { ctid = classtypeid, id = id };
            await client.StoreDefaultImage(File.OpenRead(TestPNGFilename), imageID, "image/png", creatorID, creatorCTID);

            Assert.IsTrue(await client.HasDefaultImage(imageID));

            ICloudBlob blobRef = await blobClient.GetBlobReferenceFromServerAsync(new Uri(blobClient.BaseUri,
                $"{blobClient.BaseUri.PathAndQuery}/bid{bid}/{Bucket.Data}/{classtypeid}/{id}/{fileName}"));

            MemoryStream assertStream = new MemoryStream();
            await blobRef.DownloadToStreamAsync(assertStream);
            assertStream.Position = 0;
            byte[] blobBytes = new byte[streamLength];
            assertStream.Read(blobBytes, 0, (int)assertStream.Length);

            for (int i = 0; i < streamLength; i++)
            {
                Assert.AreEqual<byte>(pngBytes[i], blobBytes[i]);
            }
        }

        [TestMethod]
        public async Task GetDocumentListTest()
        {
            short bid = 1;
            int ctid = 5000;
            int id = 1;

            DMID myID = new DMID() { ctid = ctid, id = id };

            #region setup blobData
            CloudBlobClient blobClient = await GetBlobClient();
            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            await client.DeleteAll(StorageBin.Permanent, myID);

            CloudBlobContainer cloudBlobContainer = blobClient.GetContainerReference($"bid{bid}");
            CloudBlobDirectory cloudBlobDirectory = cloudBlobContainer.GetDirectoryReference($"Documents/{ctid}/{id}");

            CloudBlockBlob blobFolder0 = cloudBlobDirectory.GetBlockBlobReference($"folder1/{EntityStorageClient.SpecialFolderBlobName}");
            CloudBlockBlob blobFile1 = cloudBlobDirectory.GetBlockBlobReference("file1.txt");
            CloudBlockBlob blobFolder1 = cloudBlobDirectory.GetBlockBlobReference($"folder1/folder2/{EntityStorageClient.SpecialFolderBlobName}");
            CloudBlockBlob blobFile2 = cloudBlobDirectory.GetBlockBlobReference("file2.txt");
            CloudBlockBlob blobFile3 = cloudBlobDirectory.GetBlockBlobReference("folder1/file3.txt");
            CloudBlockBlob blobNotInFolderFile = cloudBlobContainer.GetBlockBlobReference($"Documents/{ctid}/{id + 1}/file4.txt");

            DateTime utcStartTime = DateTime.UtcNow;
            await blobFolder0.UploadTextAsync("Endor Team.  2017-09. AR, BK, NS, RK, NH, MSC, JH");
            await blobFile1.UploadTextAsync("file1.text");
            blobFile1.SetMetadataValue(Constants.CreatedDT, utcStartTime);
            blobFile1.SetMetadataValue(Constants.ModifiedByID, 2);
            blobFile1.SetMetadataValue(Constants.IconClass, "fa1");
            blobFile1.SetMetadataValue(Constants.ModifiedByClasstypeID, 3500);
            blobFile1.SetMetadataValue(Constants.HasThumbnail, false);
            await blobFile1.SetMetadataAsync();

            await blobFolder1.UploadTextAsync("Endor Team.  2017-09. AR, BK, NS, RK, NH, MSC, JH");

            await blobFile2.UploadTextAsync("file2.text");
            blobFile2.SetMetadataValue(Constants.CreatedDT, utcStartTime);
            blobFile2.SetMetadataValue(Constants.ModifiedByID, 4);
            blobFile2.SetMetadataValue(Constants.IconClass, "fa2");
            blobFile2.SetMetadataValue(Constants.ModifiedByClasstypeID, 3502);
            blobFile2.SetMetadataValue(Constants.HasThumbnail, true);
            await blobFile2.SetMetadataAsync();

            await blobFile3.UploadTextAsync("file3.text");
            blobFile3.SetMetadataValue(Constants.CreatedDT, utcStartTime);
            blobFile3.SetMetadataValue(Constants.ModifiedByID, 6);
            blobFile3.SetMetadataValue(Constants.IconClass, "fa3");
            blobFile3.SetMetadataValue(Constants.ModifiedByClasstypeID, 3503);
            blobFile3.SetMetadataValue(Constants.HasThumbnail, false);
            await blobFile3.SetMetadataAsync();

            await blobNotInFolderFile.UploadTextAsync("file4.text");

            /// this should result in a construction of folders like this
            /// folder1/
            ///     folder2/
            ///     file3.txt
            /// file1.txt
            /// file2.txt
            #endregion

            List<DMItem> docs = (await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, myID)).ToList();

            Assert.IsNotNull(docs);
            Assert.AreEqual(3, docs.Count);
            Assert.AreEqual(1, docs.Count(t => t.IsFolder));
            DMItem folder1 = docs.First(t => t.IsFolder);
            Assert.AreEqual("folder1", folder1.Name);

            Assert.IsNotNull(folder1.Contents);
            Assert.AreEqual(2, folder1.Contents.Count);
            Assert.AreEqual(1, folder1.Contents.Count(t => t.IsFolder));
            DMItem folder2 = folder1.Contents.First(t => t.IsFolder);
            Assert.AreEqual("folder2", folder2.Name);
            DMItem file1 = docs.FirstOrDefault(t => t.Name == "file1.txt");
            Assert.IsNotNull(file1);
            Assert.IsTrue((utcStartTime - file1.CreatedDT).Duration() < TimeSpan.FromSeconds(1), $"{(utcStartTime - file1.CreatedDT).Duration()} < {TimeSpan.FromSeconds(1)}");
            Assert.AreEqual(2, file1.ModifiedByID);
            Assert.AreEqual("fa1", file1.IconClass);
            Assert.AreEqual(3500, file1.ModifiedByClasstypeID);
            Assert.AreEqual(false, file1.HasThumbnail);

            DMItem file2 = docs.FirstOrDefault(t => t.Name == "file2.txt");
            Assert.IsNotNull(file2);
            Assert.IsTrue((utcStartTime - file2.CreatedDT).Duration() < TimeSpan.FromSeconds(1));
            Assert.AreEqual(4, file2.ModifiedByID);
            Assert.AreEqual("fa2", file2.IconClass);
            Assert.AreEqual(3502, file2.ModifiedByClasstypeID);
            Assert.AreEqual(true, file2.HasThumbnail);

            Assert.AreEqual(2, folder1.Contents.Count);
            DMItem file3 = folder1.Contents.FirstOrDefault();
            Assert.IsFalse(file3.IsFolder);
            Assert.IsNotNull(file3);
            Assert.IsTrue((utcStartTime - file3.CreatedDT).Duration() < TimeSpan.FromSeconds(1));
            Assert.AreEqual("file3.txt", file3.Name);
            Assert.AreEqual("folder1/", file3.Path);
            Assert.AreEqual(6, file3.ModifiedByID);
            Assert.AreEqual("fa3", file3.IconClass);
            Assert.AreEqual(3503, file3.ModifiedByClasstypeID);
            Assert.AreEqual(false, file3.HasThumbnail);

            Assert.IsNull(docs.FirstOrDefault(t => t.Name == "file4.txt" && !t.IsFolder));
        }

        [TestMethod]
        public async Task GetDocumentFlatListTest()
        {
            short bid = 1;
            int ctid = 5000;
            int id = 1056;

            DMID myID = new DMID() { ctid = ctid, id = id };

            CloudBlobClient blobClient = await GetBlobClient();
            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            async Task<List<DMItem>> GetDmItems(bool includeSpecialFolderBlobName) =>
                await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, myID, useFlatDocumentListing: true, includeSpecialFolderBlobName: includeSpecialFolderBlobName);

            List<DMItem> dmItems = await GetDmItems(includeSpecialFolderBlobName: false);
            int originalBlobCount = dmItems.Count;

            #region setup blobData
            CloudBlobContainer cloudBlobContainer = blobClient.GetContainerReference($"bid{bid}");
            CloudBlobDirectory cloudBlobDirectory = cloudBlobContainer.GetDirectoryReference($"Documents/{ctid}/{id}");

            const string folder1BlobName = "folder1";
            const string folder2BlobName = "folder2";
            string filePrefix = DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss_");
            string file1BlobName = filePrefix + "file1.txt";
            string file2BlobName = filePrefix + "file2.txt";
            string file3BlobName = filePrefix + "file3.txt";
            string file4BlobName = filePrefix + "file4.txt";
            CloudBlockBlob blobFile0 = cloudBlobDirectory.GetBlockBlobReference($"{folder1BlobName}/{EntityStorageClient.SpecialFolderBlobName}");
            CloudBlockBlob blobFile1 = cloudBlobDirectory.GetBlockBlobReference($"{folder1BlobName}/{file1BlobName}");
            CloudBlockBlob blobFolder1 = cloudBlobDirectory.GetBlockBlobReference($"{folder1BlobName}/{folder2BlobName}/{EntityStorageClient.SpecialFolderBlobName}");
            CloudBlockBlob blobFile2 = cloudBlobDirectory.GetBlockBlobReference($"{folder2BlobName}/{file2BlobName}");
            CloudBlockBlob blobFile3 = cloudBlobDirectory.GetBlockBlobReference($"{folder1BlobName}/{file3BlobName}");
            CloudBlockBlob blobFile4 = cloudBlobContainer.GetBlockBlobReference($"Documents/{ctid}/{id}/{folder1BlobName}/{file4BlobName}");

            DateTime utcStartTime = DateTime.UtcNow;
            await blobFile0.UploadTextAsync("Endor Team.  2017-09. AR, BK, NS, RK, NH, MSC, JH");
            await blobFile1.UploadTextAsync(file1BlobName);
            blobFile1.SetMetadataValue(Constants.CreatedDT, utcStartTime);
            blobFile1.SetMetadataValue(Constants.ModifiedByID, 2);
            blobFile1.SetMetadataValue(Constants.IconClass, "fa1");
            blobFile1.SetMetadataValue(Constants.ModifiedByClasstypeID, 3500);
            blobFile1.SetMetadataValue(Constants.HasThumbnail, false);
            await blobFile1.SetMetadataAsync();

            await blobFolder1.UploadTextAsync("Endor Team.  2017-09. AR, BK, NS, RK, NH, MSC, JH");

            await blobFile2.UploadTextAsync(file2BlobName);
            blobFile2.SetMetadataValue(Constants.CreatedDT, utcStartTime);
            blobFile2.SetMetadataValue(Constants.ModifiedByID, 4);
            blobFile2.SetMetadataValue(Constants.IconClass, "fa2");
            blobFile2.SetMetadataValue(Constants.ModifiedByClasstypeID, 3502);
            blobFile2.SetMetadataValue(Constants.HasThumbnail, true);
            await blobFile2.SetMetadataAsync();

            await blobFile3.UploadTextAsync(file3BlobName);
            blobFile3.SetMetadataValue(Constants.CreatedDT, utcStartTime);
            blobFile3.SetMetadataValue(Constants.ModifiedByID, 6);
            blobFile3.SetMetadataValue(Constants.IconClass, "fa3");
            blobFile3.SetMetadataValue(Constants.ModifiedByClasstypeID, 3503);
            blobFile3.SetMetadataValue(Constants.HasThumbnail, false);
            await blobFile3.SetMetadataAsync();

            await blobFile4.UploadTextAsync(file4BlobName);

            /// this should result in a construction of folders like this
            /// folder1/
            ///     folder2/
            ///     file3.txt
            /// file1.txt
            /// file2.txt
            #endregion

            //Refresh list after adding files
            dmItems = await GetDmItems(includeSpecialFolderBlobName: false);

            List<DMItem> docs = dmItems.ToList();
            Assert.IsNotNull(docs);
            Assert.AreEqual(4 + originalBlobCount, docs.Count);
            Assert.AreEqual(0, docs.Count(t => t.IsFolder));

            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == $"{folder1BlobName}/{file1BlobName}"));
            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == $"{folder2BlobName}/{file2BlobName}"));
            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == $"{folder1BlobName}/{file3BlobName}"));
            Assert.IsNotNull(docs.FirstOrDefault(f => f.Name == $"{folder1BlobName}/{file4BlobName}"));



            //Document listing including special '_' file
            docs = await GetDmItems(includeSpecialFolderBlobName: true);
            Assert.AreEqual(6 + originalBlobCount, docs.Count);
            //Cleanup
            await blobFile0.DeleteAsync();
            await blobFile1.DeleteAsync();
            await blobFile2.DeleteAsync();
            await blobFile3.DeleteAsync();
            await blobFolder1.DeleteAsync();
            await blobFile4.DeleteAsync();
        }

        [TestMethod]
        public async Task AddTempFileTest()
        {
            short bid = 1;
            int ctid = 5003;
            string fileContent = DateTime.UtcNow.ToFileTime().ToString();
            string fileName = $"test{fileContent}.txt";
            Guid tempFileID = Guid.NewGuid();
            int creatorID = 1;
            int creatorCTID = 5000;

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            DMItem newItem = await AddFile(client, StorageBin.Temp, new DMID() { ctid = ctid, guid = tempFileID }, fileName, creatorID, creatorCTID, content: fileContent);

            var blobClient = await GetBlobClient();
            // Adjust uri because we are in development storage

            var blobRef = await blobClient.GetBlobReferenceFromServerAsync(new Uri(blobClient.BaseUri,
               $"{blobClient.BaseUri.PathAndQuery}bid{bid}/{newItem.URL}"));

            Assert.IsTrue(await blobRef.ExistsAsync());

            using (MemoryStream contentStream = new MemoryStream())
            {
                await blobRef.DownloadToStreamAsync(contentStream);
                StreamReader streamReader = new StreamReader(contentStream);
                contentStream.Position = 0;

                Assert.AreEqual(fileContent, streamReader.ReadToEnd());
            }
        }

        [TestMethod]
        public async Task AddTempFolderTest()
        {
            short bid = 1;
            int ctid = 5004;
            Guid tempID = Guid.NewGuid();
            int creatorID = 1;
            int creatorCTID = 5000;

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            DMItem tempFolder = await client.AddFolder(StorageBin.Temp, Bucket.Documents, new DMID() { ctid = ctid, guid = tempID }, "mytempfolder", creatorID, creatorCTID);
            bool success = tempFolder != null;

            CloudBlobClient blobClient = await GetBlobClient();
            CloudBlobContainer cloudBlobContainer = blobClient.GetContainerReference($"bid{bid}");
            List<DMItem> docs = await client.GetDocumentList(StorageBin.Temp, Bucket.Documents, new DMID() { ctid = ctid, guid = tempID });

            Assert.IsTrue(docs.Count == 1);
            Assert.IsTrue(docs[0].Contents != null);
            Assert.IsTrue(docs[0].Contents.Count == 0);
            Assert.IsTrue(docs[0].CanDelete);
            Assert.IsTrue(docs[0].CanMove);
            Assert.IsTrue(docs[0].CanRename);
            Assert.IsFalse(docs[0].IsShared);
        }

        [TestMethod]
        public async Task DeleteFileTest()
        {
            short bid = 1;
            int id = GetHashIDForTestMethod();
            int ctid = 5005;
            const string deleteFileName = "deletefiletest.txt";
            Bucket docBucket = Bucket.Documents;
            CloudBlobClient blobClient = await GetBlobClient();
            CloudBlobContainer cbc = blobClient.GetContainerReference($"bid{bid}");

            CloudBlockBlob cbb = cbc.GetBlockBlobReference($"{docBucket}/{ctid}/{id}/{deleteFileName}");
            await cbb.UploadTextAsync(deleteFileName);

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            DMID dMID = new DMID() { ctid = ctid, id = id };

            // Delete 1 item
            int resultCount = await client.DeleteFile(StorageBin.Permanent, docBucket, dMID, deleteFileName);
            Assert.AreEqual(1, resultCount);

            // Try to delete the item from documents again (no items deleted)
            resultCount = await client.DeleteFile(StorageBin.Permanent, docBucket, dMID, deleteFileName);
            Assert.AreEqual(0, resultCount);

            await client.DeleteAll(StorageBin.Permanent, dMID);
        }

        [TestMethod]
        public async Task DeleteFolderTest()
        {
            short bid = 1;
            int id = GetHashIDForTestMethod();
            int ctid = 5005;
            const string deleteFolderName = "deletefoldertest";
            const string deleteFileName = "DeleteFolderTest.txt";
            Bucket docBucket = Bucket.Documents;
            int creatorID = 1;
            int creatorCTID = 5000;

            //CloudBlobClient blobClient = await GetBlobClient();
            //CloudBlobContainer cbc = blobClient.GetContainerReference($"bid{bid}");

            //CloudBlockBlob cbb = cbc.GetBlockBlobReference($"{docBucket}/{ctid}/{id}/{deleteFolderName}/{deleteFileName}");
            //await cbb.UploadTextAsync(deleteFileName);

            //CloudBlockBlob deleteFolderBlob = cbc.GetBlockBlobReference($"{docBucket}/{ctid}/{id}/{deleteFolderName}/{EntityStorageClient.SpecialFolderBlobName}");
            //await deleteFolderBlob.UploadTextAsync("Endor Team.  2017-09. AR, BK, NS, RK, NH, MSC, JH");

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            DMID myDMID = new DMID()
            {
                ctid = ctid,
                id = id,
            };
            await AddFile(client, StorageBin.Permanent, myDMID, $"{deleteFolderName}/{deleteFileName}", creatorID, creatorCTID);

            //// Delete Folder containing 1 item Should result in 2
            int resultCount = await client.DeleteFile(StorageBin.Permanent, docBucket, myDMID, $"{deleteFolderName}/{EntityStorageClient.SpecialFolderBlobName}");
            Assert.AreEqual(2, resultCount);

            // Try to delete the item from documents again (no items deleted)
            resultCount = await client.DeleteFile(StorageBin.Permanent, docBucket, myDMID, $"{deleteFolderName}/{EntityStorageClient.SpecialFolderBlobName}");
            Assert.AreEqual(0, resultCount);

            await client.DeleteAll(StorageBin.Permanent, myDMID);
        }

        [TestMethod]
        public async Task DeleteAllTest()
        {
            short bid = 1;
            int id = 1;
            int ctid = 5006;

            const string deleteFileName = "deletealltest.txt";
            const string deleteFileName2 = "deletealltest2.txt";

            Bucket docBucket = Bucket.Documents;

            CloudBlobContainer cbc = (await GetBlobClient()).GetContainerReference($"bid{bid}");

            CloudBlockBlob cbb = cbc.GetBlockBlobReference($"{docBucket}/{ctid}/{id}/{deleteFileName}");

            CloudBlockBlob cbb2 = cbc.GetBlockBlobReference($"{docBucket}/{ctid}/{id}/{deleteFileName2}");
            await cbb.UploadTextAsync(deleteFileName);
            await cbb2.UploadTextAsync(deleteFileName2);

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            int deletedCount = await client.DeleteAll(StorageBin.Permanent, new DMID { ctid = ctid, id = id });
            Assert.AreEqual(2, deletedCount);

            deletedCount = await client.DeleteAll(StorageBin.Permanent, new DMID { ctid = ctid, id = id });
            Assert.AreEqual(0, deletedCount);
        }

        [TestMethod]
        public async Task MoveFileTest()
        {
            short bid = 1;
            int id = GetHashIDForTestMethod();
            int ctid = 5007;
            int targetctid = 50071;
            int targetid = 2;
            const string srcFileName = "movefiletestsource.txt";
            DMID srcID = new DMID() { ctid = ctid, id = id };
            DMID targetID = new DMID() { ctid = targetctid, id = targetid };

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            await client.DeleteAll(StorageBin.Permanent, srcID);
            await client.DeleteAll(StorageBin.Permanent, targetID);
            
            Assert.IsNotNull(await AddFile(client, StorageBin.Permanent, srcID, srcFileName, 1, 5000));

            int movedCount = await client.Move(StorageBin.Permanent, Bucket.Documents, srcID, new string[] { srcFileName }, StorageBin.Permanent, Bucket.Documents, targetID);
            Assert.AreEqual(1, movedCount);

            List<DMItem> results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, targetID);
            Assert.AreEqual(1, results.Count);

            List<DMItem> srcresults = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, srcID);
            Assert.AreEqual(0, srcresults.Count);
        }

        [TestMethod]
        public void RenameFileFolderNameTest()
        {
            Assert.AreEqual("/gamma", EntityStorageClient.GetRenamedName("gamma",/****/"/alpha"));
            Assert.AreEqual("/gamma", EntityStorageClient.GetRenamedName("gamma",/*****/"alpha"));
            Assert.AreEqual("/gamma", EntityStorageClient.GetRenamedName("/gamma",/***/"/alpha"));
            Assert.AreEqual("/gamma", EntityStorageClient.GetRenamedName("/gamma",/****/"alpha"));

            Assert.AreEqual("/gamma/_", EntityStorageClient.GetRenamedName("gamma",/******/"alpha/_"));
            Assert.AreEqual("/gamma/_", EntityStorageClient.GetRenamedName("gamma/",/*****/"alpha/_"));
            Assert.AreEqual("/gamma/_", EntityStorageClient.GetRenamedName("gamma/_",/****/"alpha/_"));
            Assert.AreEqual("/gamma/_", EntityStorageClient.GetRenamedName("/gamma",/*****/"alpha/_"));
            Assert.AreEqual("/gamma/_", EntityStorageClient.GetRenamedName("/gamma/",/****/"alpha/_"));
            Assert.AreEqual("/gamma/_", EntityStorageClient.GetRenamedName("/gamma/_",/***/"alpha/_"));
            Assert.AreEqual("/gamma/_", EntityStorageClient.GetRenamedName("/gamma/_",/**/"/alpha/_"));
        }

        [TestMethod]
        public void MovedFolderNameWithoutFolderTest()
        {
            //basic folder to folder
            Assert.AreEqual("/sub/folder/_",/************/EntityStorageClient.GetMovedName("/folder/_",/********/"/sub"));
            Assert.AreEqual("/sub/folder/_",/************/EntityStorageClient.GetMovedName("/folder/_",/********/"/sub/_"));
            Assert.AreEqual("/some/merchant/marine/_",/**/EntityStorageClient.GetMovedName("/sub/marine/_",/****/"/some/merchant"));
            Assert.AreEqual("/some/merchant/marine/_",/**/EntityStorageClient.GetMovedName("/sub/marine/_",/****/"/some/merchant/_"));

            //deep folder to shallower folder
            Assert.AreEqual("/merchant/sailors/_", EntityStorageClient.GetMovedName("/sub/marine/sailors/_",/****/"/merchant"));
            Assert.AreEqual("/merchant/sailors/_", EntityStorageClient.GetMovedName("/sub/marine/sailors/_",/****/"/merchant/_"));
            //shallow folder to deep folder
            Assert.AreEqual("/a/b/c/d/e/b/_", EntityStorageClient.GetMovedName("/a/b/_",/**/"/a/b/c/d/e/_"));
        }

        [TestMethod]
        public void MovedFileNameWithoutFolderTest()
        {
            //basic file to folder
            Assert.AreEqual("/folder/file.txt", EntityStorageClient.GetMovedName("file.txt",/***/"folder"));
            Assert.AreEqual("/folder/file.txt", EntityStorageClient.GetMovedName("file.txt",/***/"folder/"));
            Assert.AreEqual("/folder/file.txt", EntityStorageClient.GetMovedName("file.txt",/***/"folder/_"));
            Assert.AreEqual("/folder/file.txt", EntityStorageClient.GetMovedName("/file.txt",/**/"folder/_"));

            //shallow file to deep folder
            Assert.AreEqual("/deeply/nested/subfolder/file.txt", EntityStorageClient.GetMovedName("/file.txt", "/deeply/nested/subfolder"));
            Assert.AreEqual("/deeply/nested/subfolder/file.txt", EntityStorageClient.GetMovedName("/file.txt", "/deeply/nested/subfolder/_"));
            //deep file to shallower folder
            Assert.AreEqual("/a/z/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt", "/a/z"));
            Assert.AreEqual("/a/z/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt", "/a/z/"));
            Assert.AreEqual("/a/z/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt", "/a/z/_"));
            //deep file to root folder
            Assert.AreEqual("/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt", "/_"));
            Assert.AreEqual("/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt", "/"));
            Assert.AreEqual("/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt", ""));
        }

        [TestMethod]
        public void MovedFolderNameWithFolderTest()
        {
            //rename test
            Assert.AreEqual("/rock/final/temp/c/_", EntityStorageClient.GetMovedName("/rock/temp/c/_", "/rock/final/_", "/rock/temp/_"));
            Assert.AreEqual("/rock/final/temp/c/_", EntityStorageClient.GetMovedName("/rock/temp/c/_", "rock/final/_", "/rock/temp/_"));

            //shallow folder to deep folder w/ folder
            // /a/b/_ is moving to /a/0/9/_
            Assert.AreEqual("/a/0/9/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "/a/0/9", "/a/b/_"));
            Assert.AreEqual("/a/0/9/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "/a/0/9/", "/a/b/_"));
            Assert.AreEqual("/a/0/9/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "/a/0/9/_", "/a/b/_"));
            //deep folder to shallower folder w/ folder
            // /a/0/9/b/_ is moving to /a/b/_
            Assert.AreEqual("/a/b/c/_", EntityStorageClient.GetMovedName("/a/0/9/b/c/_", "/a",   "/a/0/9/b/_"));
            Assert.AreEqual("/a/b/c/_", EntityStorageClient.GetMovedName("/a/0/9/b/c/_", "/a/",  "/a/0/9/b/_"));
            Assert.AreEqual("/a/b/c/_", EntityStorageClient.GetMovedName("/a/0/9/b/c/_", "/a/_", "/a/0/9/b/_"));

            //deep folder w/ folder to root
            // /a/b/_ is moving to /b/_
            Assert.AreEqual("/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "", "/a/b/_"));
            Assert.AreEqual("/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "/", "/a/b/_"));
            Assert.AreEqual("/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "/_", "/a/b/_"));
        }

        [TestMethod]
        public void MovedFileNameWithFolderTest()
        {
            //immediate file to deep folder w/ folder
            // /a/_ is moving to /z/0/1/a/_
            Assert.AreEqual("/z/0/1/a/file.txt", EntityStorageClient.GetMovedName("/a/file.txt", "/z/0/1", "/a/_"));
            Assert.AreEqual("/z/0/1/a/file.txt", EntityStorageClient.GetMovedName("/a/file.txt", "/z/0/1/", "/a/_"));
            Assert.AreEqual("/z/0/1/a/file.txt", EntityStorageClient.GetMovedName("/a/file.txt", "/z/0/1/_", "/a/_"));

            //rename test
            Assert.AreEqual("/rock/final/temp/file.txt", EntityStorageClient.GetMovedName("/rock/temp/file.txt", "/rock/final/_", "/rock/temp/_"));
            Assert.AreEqual("/rock/final/temp/file.txt", EntityStorageClient.GetMovedName("/rock/temp/file.txt", "rock/final/_", "/rock/temp/_"));

            //shallow file to deep folder w/ folder
            // /a/_ is moving to /z/0/1/a/_
            Assert.AreEqual("/z/0/1/a/b/file.txt", EntityStorageClient.GetMovedName("/a/b/file.txt", "/z/0/1",   "/a/_"));
            Assert.AreEqual("/z/0/1/a/b/file.txt", EntityStorageClient.GetMovedName("/a/b/file.txt", "/z/0/1/",  "/a/_"));
            Assert.AreEqual("/z/0/1/a/b/file.txt", EntityStorageClient.GetMovedName("/a/b/file.txt", "/z/0/1/_", "/a/_"));
            //deep file to shallower folder w/ folder
            // /a/b/_ is moving to /z/b/_
            Assert.AreEqual("/z/b/c/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt","/z", "/a/b/_"));
            Assert.AreEqual("/z/b/c/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt","/z/", "/a/b/_"));
            Assert.AreEqual("/z/b/c/file.txt", EntityStorageClient.GetMovedName("/a/b/c/file.txt","/z/_", "/a/b/_"));
            //deep file w/ folder to root
            // /a/b/_ is moving to /b/_
            Assert.AreEqual("/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "",   "/a/b/_"));
            Assert.AreEqual("/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "/",  "/a/b/_"));
            Assert.AreEqual("/b/c/_", EntityStorageClient.GetMovedName("/a/b/c/_", "/_", "/a/b/_"));
        }

        [TestMethod]
        public async Task MoveFolderTest()
        {
            short bid = 1;
            int id = GetHashIDForTestMethod();
            int ctid = 5000;
            DMID dmID = new DMID() { ctid = ctid, id = id };

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            await client.DeleteAll(StorageBin.Permanent, dmID);
            
            const string TargetParentFolderName = "newDad";
            await client.AddFolder(StorageBin.Permanent, Bucket.Documents, dmID, TargetParentFolderName, 1, 5000);

            const string UnmovableFolderName = "rock";
            await client.AddFolder(StorageBin.Permanent, Bucket.Documents, dmID, UnmovableFolderName, 1, 5000);

            const string MobileFolderName = "mobile";
            await client.AddFolder(StorageBin.Permanent, Bucket.Documents, dmID, MobileFolderName, 1, 5000);

            /* anticipated structure:
             * 
             * /newDad/_
             * /rock/_
             * /mobile/_
             */

            List<DMItem> results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(3, results.Count);


            // test one, move from / to subfolder
            // mobile/_ move to /rock/mobile/_

            int movedCount = await client.MoveFolders(StorageBin.Permanent, Bucket.Documents, dmID, new string[] { MobileFolderName+"/_" }, StorageBin.Permanent, Bucket.Documents, null, UnmovableFolderName);
            Assert.AreEqual(1, movedCount);

            /* anticipated structure:
             * 
             * /newDad/_
             * /rock/_
             * /rock/mobile/_
             */

            results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, results.Count);
            bool mobileIsUnderSanFolder = results.FirstOrDefault(x => x.Name == UnmovableFolderName)?.Contents.FirstOrDefault(x => x.Name == MobileFolderName) != null;
            Assert.IsTrue(mobileIsUnderSanFolder);


            // test two, move from subfolder to another subfolder
            // move /rock/mobile/_ to /newDad/_

            movedCount = await client.MoveFolders(StorageBin.Permanent, Bucket.Documents, dmID, new string[] { UnmovableFolderName+"/"+MobileFolderName + "/_" }, StorageBin.Permanent, Bucket.Documents, null, TargetParentFolderName);
            Assert.AreEqual(1, movedCount);

            /* anticipated structure:
             * 
             * /newDad/_
             * /newDad/mobile
             * /rock/_
             */

            results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, results.Count);
            bool doesTargetParentContainMobileFolder = 
                results.FirstOrDefault(x => x.Name == TargetParentFolderName)?.Contents
                       .FirstOrDefault(x => x.Name == MobileFolderName) != null;
            Assert.IsTrue(doesTargetParentContainMobileFolder);


            //test three, move folder contents
            //add a deep file that must move with the folder
            const string mobileFileName = "portable.txt";
            Assert.IsNotNull(await AddFile(client, StorageBin.Permanent, dmID, $"{TargetParentFolderName}/{MobileFolderName}/{mobileFileName}", 1, 5000));
            //add an unrelated deep file
            const string unmovedFileName = "thing.txt";
            Assert.IsNotNull(await AddFile(client, StorageBin.Permanent, dmID, $"{UnmovableFolderName}/{unmovedFileName}", 1, 5000));

            /* current structure:
             * 
             * /newDad/_
             * /newDad/mobile/_
             * /newDad/mobile/portable.txt
             * /rock/_
             * /rock/thing.txt
             */

            // move newDad/mobile/_ to /rock/mobile/_

            movedCount = await client.MoveFolders(StorageBin.Permanent, Bucket.Documents, dmID, new string[] { TargetParentFolderName + "/" + MobileFolderName + "/_" }, StorageBin.Permanent, Bucket.Documents, null, UnmovableFolderName);
            Assert.AreEqual(2, movedCount);//it's two because we count the folder and the one file

            /* anticipated structure:
             * 
             * /newDad/_
             * /rock/_
             * /rock/mobile/_
             * /rock/mobile/portable.txt
             * /rock/thing.txt
             */

            results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, results.Count);
            bool doesUnmovableContainMobileContainFile = 
                results.FirstOrDefault(x => x.Name == UnmovableFolderName)?.Contents
                       .FirstOrDefault(x => x.Name == MobileFolderName)?.Contents
                       .FirstOrDefault(f => f.Name == mobileFileName) != null;
            Assert.IsTrue(doesUnmovableContainMobileContainFile);

        }

        [TestMethod]
        public async Task RenameFileTest()
        {
            short bid = 1;
            int id = GetHashIDForTestMethod();
            int ctid = 5007;
            StorageBin bin = StorageBin.Permanent;
            Bucket bucket = Bucket.Documents;
            const string originalFileName = "iAmNotLongForThisWorld.txt";
            const string newFileName = "heyoRename.txt";
            const string folderName = "folder";
            const string originalFilePath = folderName + "/" + originalFileName;
            int creatorID = 1;
            int creatorCTID = 5000;

            //setup
            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            DMID dmID = new DMID() { ctid = ctid, id = id };
            await client.DeleteAll(bin, dmID);

            //add a file in the root folder and a subfolder
            DMItem item1 = await AddFile(client, bin, dmID, originalFilePath, creatorID, creatorCTID, bucket);
            Assert.IsNotNull(item1);
            Assert.AreEqual(creatorID, item1.CreatedByID);
            Assert.AreEqual(creatorID, item1.ModifiedByID);
            Assert.AreEqual(creatorCTID, item1.ModifiedByClasstypeID);

            Assert.IsTrue(DateTime.UtcNow - item1.ModifiedDT < TimeSpan.FromSeconds(5));
            Assert.AreEqual(item1.CreatedDT, item1.ModifiedDT);


            DMItem item2 = await AddFile(client, bin, dmID, originalFileName, creatorID, creatorCTID, bucket);
            Assert.IsNotNull(item2);

            //make sure we have both files 
            List<DMItem> results = await client.GetDocumentList(bin, bucket, dmID);
            Assert.AreEqual(2, results.Count);
            var file = results.FirstOrDefault(f => !f.IsFolder);
            Assert.IsNotNull(file);
            Assert.AreEqual(originalFileName, file.Name);
            var folder = results.FirstOrDefault(f => f.IsFolder);
            Assert.IsNotNull(folder);
            Assert.IsNotNull(folder.Contents);
            Assert.IsNotNull(folder.Contents.FirstOrDefault(f => f.Name == originalFileName));

            //rename the file in the root folder
            int renamedCount = await client.Rename(bin, bucket, dmID, originalFileName, newFileName);
            Assert.AreEqual(1, renamedCount);

            //assert that the file in the root folder renamed
            results = await client.GetDocumentList(bin, bucket, dmID);
            Assert.AreEqual(2, results.Count); //one file, one folder
            file = results.FirstOrDefault(f => !f.IsFolder);
            Assert.IsNotNull(file);
            Assert.AreEqual(newFileName, file.Name);

            //rename the file in the subfolder
            renamedCount = await client.Rename(bin, bucket, dmID, originalFilePath, newFileName);
            Assert.AreEqual(1, renamedCount);

            //assert we still have one file, one folder
            results = await client.GetDocumentList(bin, bucket, dmID);
            Assert.AreEqual(2, results.Count);
            folder = results.FirstOrDefault(f => f.IsFolder);
            Assert.IsNotNull(folder);
            Assert.IsNotNull(folder.Contents);

            //assert the file in the subfolder has the new name
            var rename = folder.Contents.FirstOrDefault(f => f.Name == newFileName);
            Assert.IsNotNull(rename);
            Assert.AreEqual(false, rename.IsFolder);
            Assert.IsNull(rename.Contents);
        }

        [TestMethod]
        public async Task RenameFolderTest()
        {
            short bid = 1;
            int id = GetHashIDForTestMethod();
            int ctid = 5000;
            DMID dmID = new DMID() { ctid = ctid, id = id };

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            await client.DeleteAll(StorageBin.Permanent, dmID);

            const string TemporaryRootFolderName = "temp";
            const string NewRootFolderName = "perm";
            await client.AddFolder(StorageBin.Permanent, Bucket.Documents, dmID, TemporaryRootFolderName, 1, 5000);

            const string UnmovedFolderName = "rock";
            await client.AddFolder(StorageBin.Permanent, Bucket.Documents, dmID, UnmovedFolderName, 1, 5000);

            const string TempSubfolderName = "tempsub";
            const string IntermediateSubfolderName = "intermediate";
            const string FinalSubfolderName = "finalsub";
            await client.AddFolder(StorageBin.Permanent, Bucket.Documents, dmID, UnmovedFolderName+"/"+TempSubfolderName, 1, 5000);

            /* anticipated structure:
             * 
             * /temp/_
             * /rock/_
             * /rock/tempsub/_
             */

            //basic setup test
            List<DMItem> results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, results.Count); //temp & rock
            bool isTempSubfolderUnderUnmovedFolder = 
                results.FirstOrDefault(x => x.Name == UnmovedFolderName)?.Contents
                       .FirstOrDefault(x => x.Name == TempSubfolderName) != null;
            Assert.IsTrue(isTempSubfolderUnderUnmovedFolder);

            //test one, rename immediate folder
            int movedCount = await client.RenameFolder(StorageBin.Permanent, Bucket.Documents, dmID, $"{TemporaryRootFolderName}/_", NewRootFolderName);
            Assert.AreEqual(1, movedCount);

            /* anticipated structure:
             * 
             * /perm/_
             * /rock/_
             * /rock/tempsub/_
             */

            results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, results.Count);
            bool permanentRootFolderExists = results.FirstOrDefault(x => x.Name == NewRootFolderName) != null;
            Assert.IsTrue(permanentRootFolderExists);
            var permanentRootFolderContents = results.FirstOrDefault(x => x.Name == NewRootFolderName).Contents;
            Assert.IsTrue(permanentRootFolderContents == null || permanentRootFolderContents.Count == 0);

            //test two, rename empty deep folder
            movedCount = await client.RenameFolder(StorageBin.Permanent, Bucket.Documents, dmID, $"{UnmovedFolderName}/{TempSubfolderName}/_", IntermediateSubfolderName);
            Assert.AreEqual(1, movedCount);

            /* anticipated structure:
             * 
             * /perm/_
             * /rock/_
             * /rock/intermediate/_
             */

            results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, results.Count);
            bool intermediateIsUnderUnmoved = results.FirstOrDefault(x => x.Name == UnmovedFolderName)?.Contents.FirstOrDefault(x => x.Name == IntermediateSubfolderName) != null;
            Assert.IsTrue(intermediateIsUnderUnmoved);
            var intermediateContents = results.FirstOrDefault(x => x.Name == UnmovedFolderName)?.Contents.FirstOrDefault(x => x.Name == IntermediateSubfolderName).Contents;
            Assert.IsTrue(intermediateContents == null || intermediateContents.Count == 0);

            //test three, rename full folder
            const string fileName = "product.txt";
            Assert.IsNotNull(await AddFile(client, StorageBin.Permanent, dmID,  $"{UnmovedFolderName}/{IntermediateSubfolderName}/{fileName}", 1, 5000));

            /* anticipated structure:
             * 
             * /perm/_
             * /rock/_
             * /rock/intermediate/_
             * /rock/intermediate/product.txt
             */

            results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, results.Count);
            bool productIsUnderIntermediateFolder = 
                results.FirstOrDefault(x => x.Name == UnmovedFolderName)
                ?.Contents.FirstOrDefault(x => x.Name == IntermediateSubfolderName)
                ?.Contents.FirstOrDefault(x => x.Name == fileName) != null;
            Assert.IsTrue(productIsUnderIntermediateFolder);


            movedCount = await client.RenameFolder(StorageBin.Permanent, Bucket.Documents, dmID, $"{UnmovedFolderName}/{IntermediateSubfolderName}/_", FinalSubfolderName);
            Assert.AreEqual(2, movedCount);

            /* anticipated structure:
             * 
             * /perm/_
             * /rock/_
             * /rock/finalsub/_
             * /rock/finalsub/product.txt
             */

            results = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, dmID);
            Assert.AreEqual(2, results.Count);
            bool productIsUnderFinalSubfolder =
                results.FirstOrDefault(x => x.Name == UnmovedFolderName)
                ?.Contents.FirstOrDefault(x => x.Name == FinalSubfolderName)
                ?.Contents.FirstOrDefault(x => x.Name == IntermediateSubfolderName)
                ?.Contents.FirstOrDefault(x => x.Name == fileName) != null;
            Assert.IsTrue(productIsUnderFinalSubfolder);
        }

        private int GetHashIDForTestMethod([System.Runtime.CompilerServices.CallerMemberName]string memberName = "")
        {
            //ABS fouls up the hash code but entityClient regexes assume positive IDs
            return Math.Abs(memberName.GetHashCode());
        }

        [TestMethod]
        [DeploymentItem(TestPNGFilePath)]
        public async Task InitializeDMTest()
        {
            short bid = 1;
            int id = GetHashIDForTestMethod();
            int ctid = 5010;
            Guid guid = Guid.NewGuid();
            Bucket docs = Bucket.Documents;
            int creatorID = 1;
            int creatorCTID = 5000;

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);

            using (var png = File.OpenRead(TestPNGFilename))
            {
                await client.AddFile(png, StorageBin.Permanent, docs, new DMID()
                {
                    ctid = ctid,
                    classFolder = "template"
                }, TestPNGFilename, null, creatorID, creatorCTID);

                await client.AddFile(png, StorageBin.Permanent, docs, new DMID()
                {
                    ctid = ctid,
                    classFolder = "template"
                }, "MyFolder/" + TestPNGFilename, null, creatorID, creatorCTID);
            }

            int tempResultCount = await client.Initialize(StorageBin.Temp, new DMID()
            {
                ctid = ctid,
                guid = guid
            });
            Assert.AreEqual(2, tempResultCount);

            int idResultCount = await client.Initialize(StorageBin.Permanent, new DMID()
            {
                id = id,
                ctid = ctid
            });
            Assert.AreEqual(2, idResultCount);

            DMID otherTempID = new DMID()
            {
                ctid = ctid,
                guid = Guid.NewGuid()
            };
            //upload a temp with the same name as something in template
            using (var png = File.OpenRead(TestPNGFilename))
            {
                await client.AddFile(png, StorageBin.Temp, Bucket.Documents, otherTempID, TestPNGFilename, null, creatorID, creatorCTID);
            }
            int nonOverrideTempResultCount = await client.Initialize(StorageBin.Temp, otherTempID);
            //only one copy, not two
            Assert.AreEqual(1, nonOverrideTempResultCount);
            var tempDocs = await client.GetDocumentList(StorageBin.Temp, docs, otherTempID);
            Assert.IsNotNull(tempDocs);
            //make sure we don't have 3 docs, only 2
            //make sure we're excluding _TemplateFiles.json
            Assert.AreEqual(2, tempDocs.Count);

            //calling initialize twice should result in noop
            int secondTempResultCount = await client.Initialize(StorageBin.Temp, new DMID()
            {
                ctid = ctid,
                guid = guid
            });
            Assert.AreEqual(0, secondTempResultCount);

            int secondResultCount = await client.Initialize(StorageBin.Permanent, new DMID()
            {
                id = id,
                ctid = ctid
            });
            Assert.AreEqual(0, secondResultCount);
        }

        [TestMethod]
        public async Task MakePermanentTest()
        {
            short bid = 1;
            Guid srcGuid = Guid.NewGuid();
            int targetid = GetHashIDForTestMethod();
            int ctid = 5013;
            string tempfileName = "makepermanenttest.txt";
            string tempfileName2 = "makepermanenttest2.txt";
            int creatorID = 1;
            int creatorCTID = 5000;


            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            DMID srcID = new DMID() { guid = srcGuid, ctid = ctid };

            Assert.IsNotNull(await AddFile(client, StorageBin.Temp, srcID, tempfileName, creatorID, creatorCTID));
            Assert.IsNotNull(await AddFile(client, StorageBin.Temp, srcID, tempfileName2, creatorID, creatorCTID, Bucket.Data));

            DMID targetID = new DMID() { id = targetid, ctid = ctid };
            int movedCount = await client.MakePermanent(srcGuid, targetID);
            Assert.AreEqual(2, movedCount);
        }

        [TestMethod]
        [DeploymentItem(TestPNGFilePath)]
        public async Task CopyTest()
        {
            short bid = 1;
            int fromID = GetHashIDForTestMethod();
            int fromCTID = 5014;
            int toID = fromID + 1;
            int toCTID = fromCTID + 1;

            int creatorID = 1;
            int creatorCTID = 5000;

            DMID from = new DMID()
            {
                ctid = fromCTID,
                id = fromID
            };
            DMID toSameCTID = new DMID()
            {
                ctid = fromCTID,
                id = toID
            };
            DMID toDifferentCTID = new DMID()
            {
                ctid = toCTID,
                id = toID
            };
            DMID temp = new DMID()
            {
                guid = Guid.NewGuid()
            };

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            using (var png = File.OpenRead(TestPNGFilename))
            {
                await client.AddFile(png, StorageBin.Permanent, Bucket.Documents, from, TestPNGFilename, null, creatorID, creatorCTID);
            }

            int copyToFolderCount = 0;

            copyToFolderCount += await client.Copy(StorageBin.Permanent, Bucket.Documents, from, new string[] { TestPNGFilename }, StorageBin.Permanent, Bucket.Documents, null, "myfolder");
            Assert.AreEqual(copyToFolderCount, 1);

            copyToFolderCount += await client.Copy(StorageBin.Permanent, Bucket.Documents, from, new string[] { TestPNGFilename }, StorageBin.Permanent, Bucket.Documents, null, "myfolder/");
            Assert.AreEqual(copyToFolderCount, 1);
            //trick question! / at the end is trimmed and then skipped

            copyToFolderCount += await client.Copy(StorageBin.Permanent, Bucket.Documents, from, new string[] { TestPNGFilename }, StorageBin.Permanent, Bucket.Documents, null, "myfolder/deep");
            Assert.AreEqual(copyToFolderCount, 2);

            var docs = await client.GetDocumentList(StorageBin.Permanent, Bucket.Documents, @from);
            Assert.IsNotNull(docs);
            Assert.AreEqual(docs.Count, 2); //myfolder folder and test png

            var myFolder = docs.First(d => d.Contents != null && d.Contents.Count > 0);
            Assert.IsTrue(myFolder.IsFolder);
            Assert.IsNotNull(myFolder.Contents);
            Assert.AreEqual(myFolder.Contents.Count, 2);//deep folder and test png

            var deepFolder = myFolder.Contents.First(d => d.Contents != null && d.Contents.Count > 0);
            Assert.IsTrue(deepFolder.IsFolder);
            Assert.IsNotNull(deepFolder.Contents);
            Assert.AreEqual(deepFolder.Contents.Count, 1); // test png

            int copyToSameClassCount = await client.Copy(StorageBin.Permanent, Bucket.Documents, from, new string[] { TestPNGFilename }, StorageBin.Permanent, Bucket.Documents, toSameCTID);
            Assert.AreEqual(copyToSameClassCount, 1);

            int copyToAnotherClassCount = await client.Copy(StorageBin.Permanent, Bucket.Documents, from, new string[] { TestPNGFilename }, StorageBin.Permanent, Bucket.Documents, toDifferentCTID);
            Assert.AreEqual(copyToAnotherClassCount, 1);

            int copyToTempCount = await client.Copy(StorageBin.Permanent, Bucket.Documents, from, new string[] { TestPNGFilename }, StorageBin.Temp, Bucket.Documents, temp);
            Assert.AreEqual(copyToTempCount, 1);
        }

        [TestMethod]
        public async Task GetStreamTest()
        {
            short bid = 1;
            var bin = StorageBin.Permanent;
            var bucket = Bucket.Documents;
            var id = new DMID() { ctid = 5020, id = 1 };
            var blobClient = await GetBlobClient();
            var entityClient = new EntityStorageClient(DevelopmentConnectionString, bid);
            const string FileName = "GetStreamTest.txt";
            var container = blobClient.GetContainerReference("bid1");
            var blob = container.GetBlockBlobReference($"{bin.BlobNamePrefix(bucket, id)}/{FileName}");
            await blob.UploadTextAsync(FileName);
            string actual = null;
            using (var actualStream = await entityClient.GetStream(bin, bucket, id, FileName))
            {
                StreamReader sr = new StreamReader(actualStream);
                actual = sr.ReadToEnd();
            }

            Assert.AreEqual(FileName, actual);
        }

        [TestMethod]
        [Ignore]
#warning This test needs to be re-evaluated
        [DeploymentItem(TestPNGFilePath)]
        public async Task TemporaryImageTest()
        {
            short bid = 10000;
            string fileName = "Image";
            Guid imageID = Guid.NewGuid();
            int creatorID = 1;
            int creatorCTID = 5000;

            byte[] pngBytes = File.ReadAllBytes(TestPNGFilename);
            int streamLength = pngBytes.Length;

            var blobClient = await GetBlobClient();

            await ManuallyDeleteIfExists(bid, Bucket.Data, imageID, fileName, blobClient);

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            DMItem dmi = await client.StoreTemporaryImage(File.OpenRead(TestPNGFilename), imageID, "image/png", creatorID, creatorCTID);
            // here the dmi.Url in response should be a relative url to the StorageEndpoint
            // with the local emulator, the storage endpoint = http://127.0.0.1:10000/devstoreaccount1/

            Assert.IsNotNull(dmi);
            Assert.IsNotNull(dmi.URL);

            Assert.IsFalse(dmi.URL.Contains((await GetStorageAccount()).BlobEndpoint.GetLeftPart(UriPartial.Authority)));

            HttpClient httpClient = new HttpClient();
            Uri dmiUri = new Uri((await GetStorageAccount()).BlobStorageUri.PrimaryUri.AbsoluteUri + $"/bid{bid}/" + dmi.URL);
            Stream contentStream = await httpClient.GetStreamAsync(dmiUri);
            MemoryStream assertStream = new MemoryStream();
            await contentStream.CopyToAsync(assertStream);
            assertStream.Position = 0;

            byte[] blobBytes = new byte[streamLength];
            assertStream.Read(blobBytes, 0, (int)assertStream.Length);

            for (int i = 0; i < streamLength; i++)
            {
                Assert.AreEqual<byte>(pngBytes[i], blobBytes[i]);
            }

            Uri tempUri = await client.GetTempDefaultImageUri(imageID);
            Assert.AreEqual(dmiUri.GetLeftPart(UriPartial.Path), tempUri.GetLeftPart(UriPartial.Path));

            HttpResponseMessage response = await httpClient.GetAsync(tempUri);
            Assert.IsTrue(response.IsSuccessStatusCode);
        }

        [TestMethod]
        public async Task ComputeUsageTest()
        {
            short bid = 10001;
            var id = new DMID() { ctid = 5020, id = 1 };
            var blobClient = await GetBlobClient();
            var entityClient = new EntityStorageClient(DevelopmentConnectionString, bid);
            var blobContainer = blobClient.GetContainerReference($"bid{bid}");
            var blob = blobContainer.GetBlockBlobReference("computeusagetest.txt");
            await blob.DeleteIfExistsAsync();
            long usage = await entityClient.ComputeUsage();
            Assert.AreEqual(0, usage);
            string testString = "computeusagetest";
            await blob.UploadTextAsync(testString);

            usage = await entityClient.ComputeUsage();

            Assert.AreEqual(testString.Length, usage);
        }

        [TestMethod]
        public async Task EmptyTrashTest()
        {
            short bid = 10001;
            string bin = StorageBin.Trash.ToString();
            string bucket = Bucket.Documents.ToString();
            int ctid = 11234;
            int id = 1000;

            var blobClient = await GetBlobClient();
            var entityClient = new EntityStorageClient(DevelopmentConnectionString, bid);
            var blobContainer = blobClient.GetContainerReference($"bid{bid}");
            var dt = DateTime.UtcNow - TimeSpan.FromDays(45);
            var dtString = dt.ToString(DMExtensionMethods.TrashDirectoryDateFormat);
            for (int i = 0; i < 5; i++)
            {
                var blob = blobContainer.GetBlockBlobReference($"trash/{dtString}/{bucket}/{ctid}/{id}/EmptyTrashTest{i}.txt");
                await blob.DeleteIfExistsAsync();
                string testString = $"EmptyTrashTest{i}";
                await blob.UploadTextAsync(testString);
            }

            await entityClient.EmptyTrash();

            var bdrTrash = blobContainer.GetDirectoryReference("trash");
            var bct = new BlobContinuationToken();
            var segmentResults = await bdrTrash.ListBlobsSegmentedAsync(true, BlobListingDetails.None, null, bct, null, null);
            foreach (var blob in segmentResults.Results)
            {
                DMItem dmItem = DMExtensionMethods.ToFlatListedDMItem((CloudBlockBlob)blob, bdrTrash.Prefix);
                Assert.IsFalse(dmItem.CreatedDT <= dt);
            }            
        }

        [TestMethod]
        public async Task EmptyTempTest()
        {
            short bid = 10001;

            var blobClient = await GetBlobClient();
            var entityClient = new EntityStorageClient(DevelopmentConnectionString, bid);
            var blobContainer = blobClient.GetContainerReference($"bid{bid}");
            await blobContainer.CreateIfNotExistsAsync();
            var employeeID = 1000;
            var employeeCTID = 5000;
            var tempGuid = Guid.NewGuid().ToString().ToLower();
            DateTime currentDT = DateTime.UtcNow;

            for (int i = 0; i < 5; i++)
            {
                var blob = blobContainer.GetBlockBlobReference($"temp/{tempGuid}/EmptyTempTest{i}.txt");
                await blob.DeleteIfExistsAsync();
                string testString = $"EmptyTempTest{i}";
                
                await blob.UploadTextAsync(testString);
                DateTime expiredDateTime = currentDT - TimeSpan.FromDays(45);

                blob.SetMetadataValue(Constants.CreatedDT, expiredDateTime);
                blob.SetMetadataValue(Constants.ModifiedByClasstypeID, employeeCTID);
                blob.SetMetadataValue(Constants.ModifiedByID, employeeID);
                blob.SetMetadataValue(Constants.CreatedByID, employeeID);
                await blob.SetMetadataAsync();
            }

            await entityClient.EmptyTemp();


            var bdrTrash = blobContainer.GetDirectoryReference("trash");
            var bct = new BlobContinuationToken();
            var segmentResults = await bdrTrash.ListBlobsSegmentedAsync(true, BlobListingDetails.Metadata, null, bct, null, null);

            foreach (var blob in segmentResults.Results)
            {
                if (((CloudBlockBlob)blob).Metadata.TryGetValue(Constants.CreatedDT, out string createdDTstring))
                {
                    if (DateTime.TryParse(createdDTstring, out DateTime createdDT))
                    {
                        Assert.IsFalse(createdDT <= currentDT);
                    }
                }
            }
        }

        [TestMethod]
        public async Task AddStaticFileTest()
        {
            short bid = 1;
            int classtypeid = 3500;
            int id = 1;
            int creatorID = 1;
            int creatorCTID = 5000;
            DMID dmID = new DMID() { ctid = classtypeid, id = id, classFolder = "static" };
            string fileName = "Image";
            string fileContent = DateTime.UtcNow.ToFileTime().ToString();

            var blobClient = CloudBlobClient;
            //await ManuallyDeleteStaticIfExists(bid, Bucket.Documents, classtypeid, id, fileName, blobClient);

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid);
            await client.DeleteFile(StorageBin.Permanent, Bucket.Documents, dmID, fileName);
            DMItem result = await AddFile(client, StorageBin.Permanent, dmID, fileName, creatorID, creatorCTID, content: fileContent);

            ICloudBlob blobRef = await blobClient.GetBlobReferenceFromServerAsync(new Uri(blobClient.BaseUri,
                $"{blobClient.BaseUri.PathAndQuery}bid{bid}/{Bucket.Documents}/{classtypeid}/static/{fileName}"));

            MemoryStream assertStream = new MemoryStream();
            await blobRef.DownloadToStreamAsync(assertStream);
            assertStream.Position = 0;
            StreamReader sr = new StreamReader(assertStream);
            Assert.AreEqual(fileContent, sr.ReadToEnd());

            // This is testing a null stream. I am trying to handle edge cases appropriately
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () =>
            {
                await client.AddFile(null, StorageBin.Permanent, Bucket.Documents, new DMID() { ctid = classtypeid, id = id + 1 }, "", null, creatorID, creatorCTID);
            });

            Assert.IsNotNull(result);

            result = await AddFile(client, StorageBin.Permanent, dmID, "foo/bar/text.txt", creatorID, creatorCTID, content: fileContent);
            result = await AddFile(client, StorageBin.Permanent, dmID, "foo/bar/buzz/fizz/text.txt", creatorID, creatorCTID, content: fileContent);
        }

        [TestMethod]
        public async Task AddAssociationFileTest()
        {
            short bid = 1;
            short aid = 1;
            int classtypeid = 3500;
            int id = 1;
            int creatorID = 1;
            int creatorCTID = 5000;
            DMID dmID = new DMID() { ctid = classtypeid, id = id, classFolder = "association" };
            string fileName = "Image";
            string documentReportType = "Estimate";
            string reportFileName = "MyEstimate.config";
            string fileContent = DateTime.UtcNow.ToFileTime().ToString();

            var blobClient = CloudBlobClient;
            //await ManuallyDeleteAssociationIfExists(aid, Bucket.Documents, classtypeid, id, fileName, blobClient);

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid, aid);
            //Add Report Config File
            await client.AddFile(GenerateStreamFromString(fileContent), StorageBin.Permanent, Bucket.Reports, dmID, $"{documentReportType}/{reportFileName}", "text/plain", creatorID, creatorCTID);
            //Remove Report Config File
            await client.DeleteFile(StorageBin.Permanent, Bucket.Reports, dmID, reportFileName);
            await client.DeleteFile(StorageBin.Permanent, Bucket.Documents, dmID, fileName);
            DMItem result = await AddFile(client, StorageBin.Permanent, dmID, fileName, creatorID, creatorCTID, content: fileContent);

            ICloudBlob blobRef = await blobClient.GetBlobReferenceFromServerAsync(new Uri(blobClient.BaseUri,
                $"{blobClient.BaseUri.PathAndQuery}aid{aid}/{Bucket.Documents}/{classtypeid}/static/{fileName}"));

            MemoryStream assertStream = new MemoryStream();
            await blobRef.DownloadToStreamAsync(assertStream);
            assertStream.Position = 0;
            StreamReader sr = new StreamReader(assertStream);
            Assert.AreEqual(fileContent, sr.ReadToEnd());

            // This is testing a null stream. I am trying to handle edge cases appropriately
            await Assert.ThrowsExceptionAsync<ArgumentNullException>(async () =>
            {
                await client.AddFile(null, StorageBin.Permanent, Bucket.Documents, new DMID() { ctid = classtypeid, id = id + 1 }, "", null, creatorID, creatorCTID);
            });

            Assert.IsNotNull(result);

            result = await AddFile(client, StorageBin.Permanent, dmID, "foo/bar/text.txt", creatorID, creatorCTID, content: fileContent);
            result = await AddFile(client, StorageBin.Permanent, dmID, "foo/bar/buzz/fizz/text.txt", creatorID, creatorCTID, content: fileContent);
        }

        [TestMethod]
        public async Task CheckIfBlobExists()
        {
            short bid = 1;
            short aid = 1;
            int classtypeid = 3500;
            int id = 1;
            int creatorID = 1;
            int creatorCTID = 5000;
            DMID dmID = new DMID() { ctid = classtypeid, id = id, classFolder = "association" };
            string documentReportType = "Estimate";
            string reportFileName = "MyEstimate.config";
            //Randomize Casing of file name for case insensitive comparisons
            string relativePath = $"{documentReportType}/{reportFileName}";
            string relativePathWithRandomizedFileName = $"{documentReportType}/{RandomizeCasing(reportFileName)}";
            string fileContent = DateTime.UtcNow.ToFileTime().ToString();

            EntityStorageClient client = new EntityStorageClient(DevelopmentConnectionString, bid, aid);

            //Add Report Config File
            await client.AddFile(GenerateStreamFromString(fileContent), StorageBin.Permanent, Bucket.Reports, dmID, relativePath, "text/plain", creatorID, creatorCTID);
            //Check if Report Config Exists -- Use path with randomized filename casing
            bool exists = await client.Exists(relativePathWithRandomizedFileName, StorageBin.Permanent, Bucket.Reports, dmID);
            Assert.IsTrue(exists);
            //Remove Report Config File
            await client.DeleteFile(StorageBin.Permanent, Bucket.Reports, dmID, reportFileName);

        }
        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private static async Task<DMItem> AddFile(EntityStorageClient client, StorageBin bin, DMID id, string fileName, int? creatorID, int? creatorCTID, Bucket bucket = Bucket.Documents, string content = null)
        {
            if (content == null)
                content = DateTime.UtcNow.ToFileTime().ToString();

            using (Stream stringStream = GenerateStreamFromString(content))
                return await client.AddFile(stringStream, bin, bucket, id, fileName, "text/plain", creatorID, creatorCTID);
        }

        private string RandomizeCasing(string input)
        {
            for (var index = 0; index < input.Length; index++)
            {
                var rand = new Random(new Random().Next(0, 9));
                char c = input[index];
                c = rand.Next(0, 9) % 2 == 0 ? Char.ToLowerInvariant(c) : Char.ToUpperInvariant(c);
                input = input.Remove(index, 1);
                input = input.Insert(index, c.ToString());
            }

            return input;
        }
    }
}
