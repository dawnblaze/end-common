﻿using Endor.EF;
using Endor.Models;
using Endor.Units;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Common.Tests
{
    [TestClass]
    public class EntityFrameworkTests : CommonEntityFrameworkTests
    {
        [ClassInitialize]
        public static void InitializeAssembly(TestContext context)
        {
            Initialize();
        }

        [TestMethod]
        public void AccountingPaymentTermsSimpleListReturnsFallbackBasedOnSystemDefinitionID()
        {
            //Assert required migration
            using (ApiContext ctx = GetMockCtx(1,
                "20190815080233_END-8340_AccountingPaymentTermSimpleList_Modify"))
            {
                //Clean up data that was created for this test
                var existingTestPaymentTerm = ctx.PaymentTerm.FirstOrDefault(pt => pt.ID == -1);
                if (existingTestPaymentTerm != null)
                {
                    ctx.Remove(existingTestPaymentTerm);
                    ctx.SaveChanges();
                }

                //Setup data (includes payment terms and system option definitions)
                var newTestPaymentTerm = new PaymentTerm
                {
                    BID = 1,
                    ID = -1,
                    IsActive = true,
                    Name = "To Test PaymentTerm Default"
                };
                ctx.PaymentTerm.Add(newTestPaymentTerm);
                ctx.SaveChanges();

                //Assert IsDefault on SimplePaymentTerm uses SystemOptionDefinition
                //as the definition of the view implies
                var optionDefinition =
                    ctx.SystemOptionDefinition.FirstOrDefault(option =>
                        option.Name == "Accounting.PaymentTerm.DefaultID");
                var oldDefaultValue = "";
                if (optionDefinition != null)
                {
                    oldDefaultValue = optionDefinition.DefaultValue;
                    optionDefinition.DefaultValue = "-1";
                    ctx.SaveChanges();
                }

                var simplePaymentTerm = ctx.SimplePaymentTerm.FirstOrDefault(spt => spt.ID == -1);
                Assert.IsNotNull(simplePaymentTerm);
                Assert.IsTrue(simplePaymentTerm.IsDefault);

                //Clean up data that was created for this test
                var savedTestPaymentTerm = ctx.PaymentTerm.FirstOrDefault(pt => pt.ID == newTestPaymentTerm.ID);
                if (savedTestPaymentTerm == null || optionDefinition == null)
                    Assert.Fail(
                        "Test Payment Term not saved or Accounting.PaymentTerm.DefaultID option definition not existing");
                optionDefinition.DefaultValue = oldDefaultValue;
                ctx.Remove(savedTestPaymentTerm);
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void OptionCategory104NameDescriptionChangeTest11200()
        {
            using (ApiContext ctx = GetMockCtx(1,
                "20191227143916_END_11200_AddNewSystemSMTPConfigurationType"))
            {
                var optionCategory = ctx.SystemOptionCategory.FirstOrDefault(x => x.ID == 104);

                Assert.AreEqual("Email Domains", optionCategory.Name);
                Assert.AreEqual("Email Domains", optionCategory.Description);
                Assert.AreEqual("email gmail outlook smtp internet google microsoft handler yahoo send domain pop3 imap", optionCategory.SearchTerms);
            }
        }

        [TestMethod]
        public async Task OptionCategoriesByLevelTestAsync()
        {
            try
            {
                byte level = 2;

                List<SqlParameter> sp = new List<SqlParameter>()
                {
                    new SqlParameter() { ParameterName = "@Level", SqlDbType = System.Data.SqlDbType.TinyInt, Value = level }
                };
                ApiContext ctx = GetMockCtx(1);
                var rowResult = ctx.SystemOptionCategory.FromSqlRaw<SystemOptionCategory>(@"SELECT * FROM [dbo].[Option.Categories.ByLevel] ( @Level )",
                                    parameters: sp.ToArray())
                                    .Include("Section")
                                    .Include("Section.Parent")
                                    .Include("SystemOptionDefinitions")
                                    .Include("SystemOptionDefinitions.Options");

                List<SystemOptionCategory> categories = await Task.FromResult(rowResult.ToList());

            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public async Task OptionDataOptionLevelTestAsync()
        {
            try
            {
                ApiContext ctx = GetMockCtx(1);
                Assert.IsNotNull(await ctx.OptionData.Where(x => x.BID == 1 && x.OptionLevel >= 0).ToListAsync(), "Invalid OptionLevel type");
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public async Task EFGetFirstBusiness()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsTrue(await ctx.BusinessData.FirstOrDefaultAsync() != null);
            }
        }

        /// <summary>
        /// Tests the EF mapping is correct for GLAccount
        /// This test doensn't require data on GLAccount table
        /// </summary>
        [TestMethod]
        public async Task EFGetFirstGLAccount()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.GLAccount.ToListAsync());
            }
        }

        [TestMethod]
        public async Task PaymentTermsAndPaymentMethods()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.PaymentTerm.ToListAsync());
                Assert.IsNotNull(await ctx.PaymentMethod.ToListAsync());
                Assert.IsNotNull(await ctx.SimplePaymentMethod.ToListAsync());
                Assert.IsNotNull(await ctx.SimplePaymentTerm.ToListAsync());
            }
        }

        [TestMethod]
        public async Task EFTestTaxEntities()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.TaxGroup.ToListAsync());
                Assert.IsNotNull(await ctx.SimpleTaxGroup.ToListAsync());
                Assert.IsNotNull(await ctx.TaxabilityCodes.ToListAsync());
                Assert.IsNotNull(await ctx.TaxabilityCodeItemExemptionLinks.ToListAsync());
                Assert.IsNotNull(await ctx.TaxGroupItemLink.ToListAsync());
                Assert.IsNotNull(await ctx.TaxGroupLocationLink.ToListAsync());
                Assert.IsNotNull(await ctx.TaxItem.ToListAsync());
                Assert.IsNotNull(await ctx.SimpleTaxItem.ToListAsync());
            }
            using (ApiContext ctx = GetMockCtx(1))
            {
                GLAccount glAccount = await ctx.GLAccount.FirstOrDefaultAsync();
                Assert.IsNotNull(glAccount);
                TaxabilityCode taxCode = await ctx.TaxabilityCodes.FirstOrDefaultAsync();
                Assert.IsNotNull(taxCode);
                TaxItem taxItem = new TaxItem
                {
                    BID = 1,
                    ID = -99,
                    IsActive = true,
                    AccountNumber = "9999",
                    AgencyName = "AgencyName",
                    InvoiceText = "InvoiceText",
                    LookupCode = "LookupCode",
                    GLAccount = glAccount,
                    GLAccountID = glAccount.ID,
                    ModifiedDT = DateTime.UtcNow,
                    Name = "tax item name",
                    TaxRate = 0.9m,
                    TaxableSalesCap = 0m,
                };
                var taxCodeExemptionLink = new TaxabilityCodeItemExemptionLink
                {
                    BID = 1,
                    TaxCodeID = taxCode.ID,
                    TaxItemID = taxItem.ID,
                    TaxabilityCode = taxCode,
                    TaxItem = taxItem,
                };

                try
                {
                    ctx.Set<TaxabilityCodeItemExemptionLink>().Add(taxCodeExemptionLink);
                    await ctx.SaveChangesAsync();
                }
                finally
                {
                    ctx.Set<TaxabilityCodeItemExemptionLink>().Remove(taxCodeExemptionLink);
                    ctx.Set<TaxItem>().Remove(taxItem);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        [TestMethod]
        public async Task EFTestPartEntities()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.LaborData.ToListAsync());
                Assert.IsNotNull(await ctx.SimpleLaborData.ToListAsync());
                Assert.IsNotNull(await ctx.LaborCategory.ToListAsync());
                Assert.IsNotNull(await ctx.SimpleLaborCategory.ToListAsync());
                Assert.IsNotNull(await ctx.LaborCategoryLink.ToListAsync());
                Assert.IsNotNull(await ctx.MachineData.ToListAsync());
            }
        }

        [TestMethod]
        public async Task LaborDataCRUDTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                //remove existing -99
                var testLabor = await ctx.LaborData.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testLabor != null)
                {
                    ctx.Remove(testLabor);
                    await ctx.SaveChangesAsync();
                }

                var newLabor = new LaborData()
                {
                    ID = -99,
                    BID = 1,
                    Name = "wakanda foreva",
                    InvoiceText = "wakanda foreva",
                    ExpenseAccountID = 1000,
                    IncomeAccountID = 1000,
                };
                //CREATE
                ctx.LaborData.Add(newLabor);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                //READ
                var wakanda = ctx.LaborData.FirstOrDefault(p => p.ID == -99);
                Assert.IsNotNull(wakanda);//test select

                //UPDATE
                wakanda.InvoiceText = "killmonger rulz";
                await ctx.SaveChangesAsync();
                var updatedwakanda = ctx.LaborData.FirstOrDefault(p => p.ID == -99);
                Assert.IsTrue(updatedwakanda.InvoiceText == "killmonger rulz");//test update

                //DELETE
                ctx.Remove(updatedwakanda);
                await ctx.SaveChangesAsync();
                var deletedwakanda = await ctx.LaborData.Where(p => p.ID == -99).FirstOrDefaultAsync();
                Assert.IsNull(deletedwakanda);//test delete
            }
        }

        [TestMethod]
        public async Task LaborCategoryCRUDTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                //remove existing -99
                var testLink = await ctx.LaborCategoryLink.Where(p => p.CategoryID == -99).FirstOrDefaultAsync();
                if (testLink != null)
                {
                    ctx.Remove(testLink);
                    await ctx.SaveChangesAsync();
                }
                var testCategory = await ctx.LaborCategory.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testCategory != null)
                {
                    ctx.Remove(testCategory);
                    await ctx.SaveChangesAsync();
                }
                var testLabor = await ctx.LaborData.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testLabor != null)
                {
                    ctx.Remove(testLabor);
                    await ctx.SaveChangesAsync();
                }

                var newLabor = new LaborData()
                {
                    ID = -99,
                    BID = 1,
                    Name = "wakanda foreva",
                    InvoiceText = "wakanda foreva",
                    ExpenseAccountID = 1000,
                    IncomeAccountID = 1000,
                };
                ctx.LaborData.Add(newLabor);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                var newLaborCat = new LaborCategory()
                {
                    ID = -99,
                    BID = 1,
                    Name = "wakanda foreva"
                };
                ctx.LaborCategory.Add(newLaborCat);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                var newLaborCatLink = new LaborCategoryLink()
                {
                    BID = 1,
                    PartID = -99,
                    CategoryID = -99
                };
                ctx.LaborCategoryLink.Add(newLaborCatLink);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                newLaborCat.Description = "killmonger rulz";
                await ctx.SaveChangesAsync();
                var updatedwakanda = ctx.LaborCategory.FirstOrDefault(p => p.ID == -99);
                Assert.IsTrue(updatedwakanda.Description == "killmonger rulz");//test update


                ctx.Remove(newLaborCatLink);
                ctx.Remove(newLabor);
                ctx.Remove(updatedwakanda);
                await ctx.SaveChangesAsync();
                var deletedwakanda = await ctx.LaborCategory.FirstOrDefaultAsync(p => p.ID == -99);
                Assert.IsNull(deletedwakanda);//test delete
            }
        }

        [TestMethod]
        public async Task EFMaterialDataTests()
        {
            int TestMaterialID = -98;
            /*___________________________________________
             |                                           |
             | NOTE: Migration for related enum tables   |
             | must be applied before starting the test  |
             |___________________________________________|
            */
            using (ApiContext ctx = GetMockCtx(1))
            {
                var existingNinetyNine = ctx.MaterialData.Where(test => test.ID == TestMaterialID).FirstOrDefault();
                if (existingNinetyNine != null)
                {
                    ctx.Remove(existingNinetyNine);
                    await ctx.SaveChangesAsync();
                }

                var testGLAccount1 = await ctx.GLAccount.Where(gl => gl.BID == 1).FirstOrDefaultAsync();
                var testGLAccount2 = await ctx.GLAccount.Where(gl => gl.BID == 1 && gl.ID != testGLAccount1.ID).FirstOrDefaultAsync();
                var testGLAccount3 = await ctx.GLAccount.Where(gl => gl.BID == 1 && gl.ID != testGLAccount1.ID && gl.ID != testGLAccount2.ID).FirstOrDefaultAsync();

                var data = new MaterialData();
                data.BID = 1;
                data.ID = TestMaterialID;
                data.Name = "test data";
                data.Description = "test";
                data.InvoiceText = "test";
                data.ExpenseAccountID = testGLAccount1.ID;
                data.IncomeAccountID = testGLAccount2.ID;
                data.InventoryAccountID = testGLAccount3.ID;
                data.ConsumptionUnit = Unit.Centimeter; //enum test
                data.Height = new Measurement(null, Unit.Box);
                data.Length = new Measurement(null, Unit.Carton);
                data.Weight = new Measurement(null, Unit.Gram);
                data.Width = new Measurement(null, Unit.Gram);
                data.PhysicalMaterialType = MaterialPhysicalType.Roll; //enum test
                data.EstimatingConsumptionMethod = MaterialConsumptionMethod.Each; //enum test
                data.EstimatingCostingMethod = MaterialCostingMethod.AverageInventoryCost; //enum test

                //CREATE
                ctx.MaterialData.Add(data);
                await ctx.SaveChangesAsync();

                //READ
                var mytestdata = await ctx.MaterialData.Where(mat =>
                    mat.ID == TestMaterialID
                    && mat.ConsumptionUnit == Unit.Centimeter
                    && mat._HeightUnit == Unit.Box
                    && mat._LengthUnit == Unit.Carton
                    && mat._WeightUnit == Unit.Gram
                    && mat._WidthUnit == Unit.Gram
                    && data.PhysicalMaterialType == MaterialPhysicalType.Roll
                    && data.EstimatingConsumptionMethod == MaterialConsumptionMethod.Each
                    && data.EstimatingCostingMethod == MaterialCostingMethod.AverageInventoryCost
                ).FirstOrDefaultAsync();
                Assert.IsNotNull(mytestdata);

                //UPDATE
                data.Name = "updated";
                await ctx.SaveChangesAsync();
                var updateddata = await ctx.MaterialData.Where(mat => mat.ID == TestMaterialID).FirstOrDefaultAsync();
                Assert.IsTrue(updateddata.Name.Equals("updated"));

                //DELETE
                ctx.MaterialData.Remove(data);
                await ctx.SaveChangesAsync();
                Assert.IsNull(await ctx.MaterialData.Where(mat => mat.ID == TestMaterialID).FirstOrDefaultAsync());
            }
        }

        [TestMethod]
        public async Task EFMaterialCategoryTests()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var existingNinetyNine = ctx.MaterialCategory.Where(test => test.ID == -99).FirstOrDefault();
                if (existingNinetyNine != null)
                {
                    ctx.Remove(existingNinetyNine);
                    await ctx.SaveChangesAsync();
                }

                var data = new MaterialCategory();
                data.BID = 1;
                data.ID = -99;
                data.Name = "test cat";
                ctx.MaterialCategory.Add(data);
                await ctx.SaveChangesAsync();

                var mytest = await ctx.MaterialCategory.SingleOrDefaultAsync(cat => cat.ID == -99);
                Assert.IsTrue(mytest.Name.Equals("test cat"));//create and read

                data.Name = "updated";
                await ctx.SaveChangesAsync();

                var updated = await ctx.MaterialCategory.SingleOrDefaultAsync(cat => cat.ID == -99);
                Assert.IsTrue(updated.Name.Equals("updated"));//update

                ctx.MaterialCategory.Remove(data);
                await ctx.SaveChangesAsync();

                Assert.IsNull(await ctx.MaterialCategory.SingleOrDefaultAsync(cat => cat.ID == -11));//delete
            }
        }

        [TestMethod]
        public async Task EFMaterialCategoryLinkTests()
        {
            short TestMaterialCategoryID = -99;
            int TestMaterialID = -99;
            using (ApiContext ctx = GetMockCtx(1))
            {
                var existingNinetyNine_data = ctx.MaterialData.Where(test => test.ID == TestMaterialCategoryID).FirstOrDefault();
                if (existingNinetyNine_data != null)
                {
                    ctx.Remove(existingNinetyNine_data);
                    await ctx.SaveChangesAsync();
                }

                var existingNinetyNine_cat = ctx.MaterialCategory.Where(test => test.ID == TestMaterialCategoryID).FirstOrDefault();
                if (existingNinetyNine_cat != null)
                {
                    ctx.Remove(existingNinetyNine_cat);
                    await ctx.SaveChangesAsync();
                }

                var testGLAccount1 = await ctx.GLAccount.Where(gl => gl.BID == 1).FirstOrDefaultAsync();
                var testGLAccount2 = await ctx.GLAccount.Where(gl => gl.BID == 1 && gl.ID != testGLAccount1.ID).FirstOrDefaultAsync();
                var testGLAccount3 = await ctx.GLAccount.Where(gl => gl.BID == 1 && gl.ID != testGLAccount1.ID && gl.ID != testGLAccount2.ID).FirstOrDefaultAsync();

                var data = new MaterialData();
                data.BID = 1;
                data.ID = TestMaterialID;
                data.Name = "test data";
                data.Description = "test";
                data.InvoiceText = "test";
                data.ExpenseAccountID = testGLAccount1.ID;
                data.IncomeAccountID = testGLAccount2.ID;
                data.InventoryAccountID = testGLAccount3.ID;

                var cat = new MaterialCategory();
                cat.BID = 1;
                cat.ID = TestMaterialCategoryID;
                cat.Name = "test cat";

                ctx.MaterialCategory.Add(cat);
                ctx.MaterialData.Add(data);
                await ctx.SaveChangesAsync();

                var link = new MaterialCategoryLink();
                link.BID = 1;
                link.PartID = data.ID;
                link.CategoryID = cat.ID;
                ctx.MaterialCategoryLink.Add(link);
                await ctx.SaveChangesAsync();

                var mylink = await ctx.MaterialCategoryLink.Where(l => l.BID == 1 && l.PartID == data.ID && l.CategoryID == cat.ID).FirstOrDefaultAsync();
                Assert.IsNotNull(mylink);//create and read

                ctx.MaterialCategoryLink.Remove(link);
                await ctx.SaveChangesAsync();

                Assert.IsNull(await ctx.MaterialCategoryLink.Where(l => l.BID == 1 && l.PartID == data.ID && l.CategoryID == cat.ID).FirstOrDefaultAsync());//delete

                //remove testdata
                ctx.MaterialData.Remove(data);
                ctx.MaterialCategory.Remove(cat);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EFGetEmployee()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsTrue(await ctx.EmployeeData.FirstOrDefaultAsync() != null);
            }
        }

        [TestMethod]
        public async Task EFGetLocationData()
        {
            using (ApiContext ctx = GetMockCtx(1, "20190821173048_END-8476-AddLocationNumber"))
            {
                byte minbyte = byte.MinValue;

                Func<Task> cleanup = async () =>
                {
                    var testloc = await ctx.LocationData.FirstOrDefaultAsync(x => x.ID == minbyte && x.BID == 1);
                    if (testloc != null)
                    {
                        ctx.Remove(testloc);
                        await ctx.SaveChangesAsync();
                    }

                };
                try
                {
                    await cleanup();

                    var testloc = new LocationData
                    {
                        BID = 1,
                        ID = minbyte,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDT = DateTime.UtcNow,
                        IsActive = false,
                        Name = "testloc00000000",
                        IsDefault = false,
                        HasImage = false,
                        Abbreviation = "0123456789"
                    };

                    ctx.Add(testloc);
                    await ctx.SaveChangesAsync();

                    //retrieve
                    var testlocInDB = await ctx.LocationData.AsNoTracking().FirstOrDefaultAsync(x => x.ID == minbyte && x.BID == 1);
                    Assert.IsNotNull(testlocInDB);
                    Assert.AreEqual(testloc.Abbreviation, testlocInDB.Abbreviation, "Abbreviation");
                }
                finally
                {
                    await cleanup();
                }
            }
        }

        [TestMethod]
        public async Task EFGetEmployeeWithLocator()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsTrue(await ctx.EmployeeData.Include(e => e.EmployeeLocators).FirstOrDefaultAsync() != null);
            }
        }

        [TestMethod]
        public async Task EFGetEmployeeWithUserLinks()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var linkedEmployee = await ctx.EmployeeData.Include("UserLinks").Where(x => x.UserLinks != null && x.UserLinks.Count > 0).FirstOrDefaultAsync();
                Assert.IsTrue(linkedEmployee != null);
                var UserIDs = String.Join(",", linkedEmployee.UserLinks.Select(x => x.AuthUserID.ToString()).ToArray());
                Assert.IsTrue(UserIDs != null && UserIDs.Length > 0);
            }
        }

        [TestMethod]
        public async Task EFGetEmployeeWithManyIncludes()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsTrue(await ctx.Set<EmployeeData>().IncludeAll(new string[]
                {
                    "Business",
                    "EmployeeLocators",
                    "EmployeeTeamLinks",
                    "UserLinks"
                }).FirstOrDefaultAsync() != null);
            }
        }

        [TestMethod]
        public async Task EFGetPageEntities()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                await AssertTopFiveNotNull(ctx.EmployeeData);
                await AssertTopFiveNotNull(ctx.LocationData);
                await AssertTopFiveNotNull(ctx.BusinessData);
                await AssertTopFiveNotNull(ctx.ContactData);
            }
        }

        [TestMethod]
        public async Task EFGetOptionEntities()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                await AssertTopFiveNotNull(ctx.OptionData);
                await AssertTopFiveNotNull(ctx.SystemOptionSection);
                await AssertTopFiveNotNull(ctx.SystemOptionCategory);
                await AssertTopFiveNotNull(ctx.SystemOptionDefinition);
            }
        }

        [TestMethod]
        public async Task EFOptionsSelectDuplicatesReturnsNothing()
        {
            string requiresMigration = "20191118155359_END_10301_CleanUpBadOptionsThatShouldNowExist";
            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                var sql = @"SELECT * FROM [Option.Data] O1
WHERE OptionLevel >= 32
 AND OptionID IN (SELECT OptionID FROM [Option.Data] O2 WHERE O1.BID = O2.BID AND OptionLevel < 32)
;";
                var result = await ctx.OptionData.FromSqlRaw(sql).ToListAsync();
                Assert.AreEqual(result.Count, 0);
            }
        }

        [TestMethod]
        public async Task EFGetSimpleEntities()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.SimpleBusinessData.FirstOrDefaultAsync());
                Assert.IsNotNull(await ctx.SimpleCompanyData.FirstOrDefaultAsync());
                var simpleContact = await ctx.SimpleContactData.Include(x => x.ParentCompany).FirstOrDefaultAsync();
                Assert.IsNotNull(simpleContact);
                Assert.IsNotNull(simpleContact.ParentCompany);
                Assert.IsNotNull(await ctx.SimpleEmployeeData.FirstOrDefaultAsync());
                await ctx.SimpleEmployeeTeam.FirstOrDefaultAsync();
                Assert.IsNotNull(await ctx.SimpleEnumTimeZone.FirstOrDefaultAsync());
                Assert.IsNotNull(await ctx.SimpleLocationData.FirstOrDefaultAsync());
                Assert.IsNotNull(await ctx.SimpleLaborData.ToListAsync());
            }
        }

        [TestMethod]
        public async Task EFGetCompanyContacts()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                string[] includes = { "CompanyContactLinks", "CompanyContactLinks.Company" };
                var company = await ctx.Set<CompanyData>().IncludeAll(includes)
                    .FirstOrDefaultAsync(x => x.CompanyContactLinks.Count() > 0);

                if (company == null)
                {
                    Assert.Inconclusive("No companies exist with CompanyContactLinks");
                }
                else
                {
                    Assert.IsNotNull(company.CompanyContactLinks);
                    var contactFromLink = company.CompanyContactLinks.FirstOrDefault();
                    Assert.IsNotNull(contactFromLink);
                    var contactFromDB = await ctx.Set<ContactData>().FirstOrDefaultAsync(x => x.ID == company.CompanyContactLinks.FirstOrDefault().ContactID);
                    Assert.IsNotNull(contactFromDB);
                }
            }
        }

        [TestMethod]
        public async Task EFGetContactStatusID()
        {
            string requiresMigration = "20191113011045_END_10096_UpdatingContactStatusID";
            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                var order = await ctx.OrderData.OrderByDescending(x => x.ID).FirstOrDefaultAsync();
                var orderContactRoles = await ctx.OrderContactRole.Where(x => x.OrderID == order.ID).ToListAsync();
                foreach (var contactRole in orderContactRoles)
                {
                    var contact = await ctx.ContactData.FirstOrDefaultAsync(x => x.ID == contactRole.ContactID);
                    if (contactRole.RoleType == OrderContactRoleType.Primary &&
                        order.TransactionType == (byte)OrderTransactionType.Order)
                    {
                        Assert.AreEqual(4, contact.StatusID);
                    }

                    if (contactRole.RoleType == OrderContactRoleType.Primary &&
                        order.TransactionType == (byte)OrderTransactionType.Estimate)
                    {
                        Assert.AreEqual(2, contact.StatusID);
                    }
                }
            }
        }

        [TestMethod]
        public void EFInsertEnumAssemblyElementTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                List<EnumAssemblyElementType> enums = ctx.EnumAssemblyElementType.OrderBy(t => t.ID).ToList();
                String[] enumNames = Enum.GetNames(typeof(AssemblyElementType));
                AssemblyElementType[] enumValues = (AssemblyElementType[])Enum.GetValues(typeof(AssemblyElementType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumAssemblyElementType enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);

                    i++;
                }

                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Group"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Single Line Label"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Multi-Line Label"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "URL Label"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Spacer"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Single Line Text"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Multi-Line String"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Drop-Down"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Number"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Checkbox"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Show/Hide Group"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Linked Subassembly"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Linked Machine"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Detail Form Button"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Layout Visualizer"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Radio Group"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Toggle"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "DateTime Picker"));
            }
        }

        [TestMethod]
        public void EFInsertEnumAssemblyIncomeAllocationTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                List<EnumAssemblyIncomeAllocationType> enums = ctx.EnumAssemblyIncomeAllocationType.OrderBy(t => t.ID).ToList();
                String[] enumNames = Enum.GetNames(typeof(AssemblyIncomeAllocationType));
                AssemblyIncomeAllocationType[] enumValues = (AssemblyIncomeAllocationType[])Enum.GetValues(typeof(AssemblyIncomeAllocationType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumAssemblyIncomeAllocationType enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);

                    i++;
                }

                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Single Income Allocation"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Allocation by Material Income Account"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Allocation by Machine Income Account"));
            }
        }

        [TestMethod]
        public void EFInsertEnumAssemblyLabelTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                List<EnumAssemblyLabelType> enums = ctx.EnumAssemblyLabelType.OrderBy(t => t.ID).ToList();
                String[] enumNames = Enum.GetNames(typeof(AssemblyLabelType));
                AssemblyLabelType[] enumValues = (AssemblyLabelType[])Enum.GetValues(typeof(AssemblyLabelType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumAssemblyLabelType enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);

                    i++;
                }

                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "None"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Information"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Success"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Warning"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Error"));
            }
        }

        [TestMethod]
        public void EFInsertEnumAssemblyLayoutTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Dictionary<AssemblyLayoutType, string> enumSQLRecords = ctx.EnumAssemblyLayoutType.OrderBy(t => t.ID).ToDictionary(x => x.ID, y => y.Name);

                AssemblyLayoutType[] enumModels = (AssemblyLayoutType[])Enum.GetValues(typeof(AssemblyLayoutType));

                AssertEnumModelMatchesSQLTable(enumModels, enumSQLRecords);
            }
        }

        [TestMethod]
        public void EFInsertEnumMachineTemplateLayoutTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Dictionary<MachineLayoutType, string> enumSQLRecords = ctx.EnumMachineLayoutType.OrderBy(t => t.ID).ToDictionary(x => x.ID, y => y.Name);

                MachineLayoutType[] enumModels = (MachineLayoutType[])Enum.GetValues(typeof(MachineLayoutType));

                AssertEnumModelMatchesSQLTable(enumModels, enumSQLRecords);
            }
        }

        public void AssertEnumModelMatchesSQLTable<T>(T[] enumModels, Dictionary<T, string> enumSQLRecords)
        {
            Assert.AreEqual(enumSQLRecords.Count, enumModels.Length, $"{nameof(T)} model and SQL records mismatch!");

            foreach (T enumItem in enumModels)
            {
                string enumModelName = Enum.GetName(typeof(T), enumItem);
                Assert.IsTrue(enumSQLRecords.ContainsKey(enumItem), $"{nameof(T)} is missing a SQL record for {enumModelName}");
                string enumSQLName = enumSQLRecords[enumItem].Replace(" ", String.Empty);
                Assert.AreEqual(enumSQLName, enumModelName, $"{nameof(T)}.{enumModelName} has a SQL record with a mismatched Name field of {enumSQLName}");
            }
        }

        [TestMethod]
        public void EFInsertEnumAssemblyListDataTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                List<EnumAssemblyListDataType> enums = ctx.EnumAssemblyListDataType.OrderBy(t => t.ID).ToList();
                String[] enumNames = Enum.GetNames(typeof(AssemblyListDataType));
                AssemblyListDataType[] enumValues = (AssemblyListDataType[])Enum.GetValues(typeof(AssemblyListDataType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumAssemblyListDataType enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);

                    i++;
                }

                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Text"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Text with Number Value"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Materials"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Labor"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Machines"));
            }
        }

        [TestMethod]
        public void EFInsertEnumAssemblyPricingTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                List<EnumAssemblyPricingType> enums = ctx.EnumAssemblyPricingType.OrderBy(t => t.ID).ToList();
                String[] enumNames = Enum.GetNames(typeof(AssemblyPricingType));
                AssemblyPricingType[] enumValues = (AssemblyPricingType[])Enum.GetValues(typeof(AssemblyPricingType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumAssemblyPricingType enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);

                    i++;
                }

                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Market Based Pricing"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Margin Based Pricing"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Markup Based Pricing"));
            }
        }

        [TestMethod]
        public void EFInsertEnumAssemblyTableMatchTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                List<EnumAssemblyTableMatchType> enums = ctx.EnumAssemblyTableMatchType.OrderBy(t => t.ID).ToList();
                String[] enumNames = Enum.GetNames(typeof(AssemblyTableMatchType));
                AssemblyTableMatchType[] enumValues = (AssemblyTableMatchType[])Enum.GetValues(typeof(AssemblyTableMatchType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumAssemblyTableMatchType enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);

                    i++;
                }

                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Exact Match"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Use Lower Value"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Use Higher Value"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Interpolate"));
            }
        }

        [TestMethod]
        public void EFInsertEnumAssemblyTableTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                List<EnumAssemblyTableType> enums = ctx.EnumAssemblyTableType.OrderBy(t => t.ID).ToList();
                String[] enumNames = Enum.GetNames(typeof(AssemblyTableType));
                AssemblyTableType[] enumValues = (AssemblyTableType[])Enum.GetValues(typeof(AssemblyTableType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumAssemblyTableType enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);

                    i++;
                }

                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Custom"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Tier - Percentage Discount (Market Based)"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Tier - Unit Price (Market Based)"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Tier - Margin (Cost Based)"));
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Tier - Multiplier (Cost Based)"));
            }
        }

        private async Task AssertTopFiveNotNull<T>(DbSet<T> set)
            where T : class
        {
            Assert.IsTrue((await set.ToListAsync())?.Take(5) != null);
        }

        [TestMethod]
        public async Task EFGetManyLocations()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var locations = await ctx.LocationData.Where(x => x.BID == 1).ToListAsync();
                Assert.IsTrue(locations.Count > 0);
                foreach (var loc in locations)
                {
                    Assert.IsTrue(loc.ID > 0);
                }
            }
        }

        [TestMethod]
        public async Task EFCheckListFilterMappings()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsTrue(await ctx.ListFilter.ToListAsync() != null);
                Assert.IsTrue(await ctx.SystemListColumn.ToListAsync() != null);
                Assert.IsTrue(await ctx.SystemListFilterCriteria.ToListAsync() != null);
            }
        }

        [TestMethod]
        public async Task EFTestGLAccountListFilters()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var glAccountListFilters = await ctx.ListFilter.Where(x => x.TargetClassTypeID == Convert.ToInt32(ClassType.GLAccount)).ToListAsync();
                Assert.IsNotNull(glAccountListFilters);
                Assert.IsTrue(glAccountListFilters.Count() >= 5);
                Assert.IsNotNull(glAccountListFilters.FirstOrDefault(x => x.Name == "Assets"));
                Assert.IsNotNull(glAccountListFilters.FirstOrDefault(x => x.Name == "Liabilities"));
                Assert.IsNotNull(glAccountListFilters.FirstOrDefault(x => x.Name == "COGS"));
                Assert.IsNotNull(glAccountListFilters.FirstOrDefault(x => x.Name == "Income"));
                Assert.IsNotNull(glAccountListFilters.FirstOrDefault(x => x.Name == "Expenses"));
            }
        }

        [TestMethod]
        public async Task PaymentMethodCRUD()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                //remove existing -99
                var negative99 = await ctx.PaymentMethod.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (negative99 != null)
                {
                    ctx.Remove(negative99);
                    await ctx.SaveChangesAsync();
                }

                var newPaymentMethod = new PaymentMethod();
                newPaymentMethod.ID = -99;
                newPaymentMethod.BID = 1;
                newPaymentMethod.Name = "wakanda foreva";
                newPaymentMethod.PaymentMethodType = PaymentMethodType.Visa;
                newPaymentMethod.DepositGLAccountID = 1000;
                ctx.PaymentMethod.Add(newPaymentMethod);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                var wakanda = ctx.PaymentMethod.Single(p => p.ID == -99);
                Assert.IsNotNull(wakanda);//test select

                wakanda.PaymentMethodType = PaymentMethodType.Discover;
                await ctx.SaveChangesAsync();
                var updatedwakanda = ctx.PaymentMethod.Single(p => p.ID == -99);
                Assert.IsTrue(updatedwakanda.PaymentMethodType == PaymentMethodType.Discover);//test update

                ctx.Remove(updatedwakanda);
                await ctx.SaveChangesAsync();
                var deletedwakanda = await ctx.PaymentMethod.Where(p => p.ID == -99).FirstOrDefaultAsync();
                Assert.IsNull(deletedwakanda);//test delete

                Assert.IsTrue((await ctx.EnumPaymentMethodType.ToListAsync()).Count > 0);//data for this table is part of migration
            }
        }

        [TestMethod]
        //Check if the default payment methods exists
        public async Task CheckDefaultPaymentMethods()
        {
            string requiresMigration = "20200121013142_END-11635_InsertStandardPaymentMethods";
            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                var paymentMethods = await ctx.PaymentMethod.Where(pm => pm.BID == -1).ToListAsync();
                Assert.IsNotNull(paymentMethods);
                // This test will fail if there are new default payment methods added. 
                // This should be updated if there are new data.
                Assert.IsTrue(paymentMethods.Count() == 12);
            }
        }

        [TestMethod]
        public async Task PaymentMethodIncludeGLAccountTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var result = await ctx.PaymentMethod.Include(e => e.DepositGLAccount).FirstOrDefaultAsync();
                Assert.IsTrue(result.DepositGLAccount != null);
            }
        }

        [TestMethod]
        public async Task EFTestPaymentTermListFilters()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var listFilters = await ctx.ListFilter.Where(x => x.TargetClassTypeID == Convert.ToInt32(ClassType.PaymentTerm)).ToListAsync();
                Assert.IsNotNull(listFilters);
                Assert.IsTrue(listFilters.Count() >= 1);
                Assert.IsNotNull(listFilters.FirstOrDefault(x => x.Name == "All"));

                var listCriteria = await ctx.SystemListFilterCriteria.Where(x => x.TargetClassTypeID == Convert.ToInt32(ClassType.PaymentTerm)).ToListAsync();
                Assert.IsNotNull(listCriteria);
                Assert.IsTrue(listCriteria.Count() >= 2);
                Assert.IsNotNull(listCriteria.FirstOrDefault(x => x.Name == "Name"));
                Assert.IsNotNull(listCriteria.FirstOrDefault(x => x.Name == "Include Inactive"));
            }
        }

        [TestMethod]
        public async Task EFTestFlatListFilters()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var listFilters = await ctx.ListFilter.Where(x => x.TargetClassTypeID == Convert.ToInt32(ClassType.FlatListItem)).ToListAsync();
                Assert.IsNotNull(listFilters);
                Assert.IsTrue(listFilters.Count() >= 1);
                Assert.IsNotNull(listFilters.FirstOrDefault(x => x.Name == "All"));

                var listCriteria = await ctx.SystemListFilterCriteria.Where(x => x.TargetClassTypeID == Convert.ToInt32(ClassType.FlatListItem)).ToListAsync();
                Assert.IsNotNull(listCriteria);
                Assert.IsTrue(listCriteria.Count() >= 2);
                Assert.IsNotNull(listCriteria.FirstOrDefault(x => x.Name == "Name"));
                Assert.IsNotNull(listCriteria.FirstOrDefault(x => x.Name == "Include Inactive"));
            }
        }

        [TestMethod]
        public async Task EFContactSetBillingPrimarySprocTest()
        {
            short testBID = 1;
            using (ApiContext ctx = GetMockCtx(testBID))
            {
                #region Remove if Existing Data

                var negativeLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (negativeLinks != null)
                {
                    ctx.RemoveRange(negativeLinks);
                    await ctx.SaveChangesAsync();
                }

                var negativeContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (negativeContacts != null)
                {
                    ctx.RemoveRange(negativeContacts);
                    await ctx.SaveChangesAsync();
                }

                var negativeCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (negativeCompanies != null)
                {
                    ctx.RemoveRange(negativeCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                var location = await ctx.LocationData.FirstOrDefaultAsync(l => l.BID == testBID);
                if (location == null)
                    Assert.Inconclusive("No location data found. Unable to complete test.");

                var company1 = new CompanyData()
                {
                    BID = testBID,
                    ID = -99,
                    Name = "TEST COMPANY 1",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                var company2 = new CompanyData()
                {
                    BID = testBID,
                    ID = -98,
                    Name = "TEST COMPANY 2",
                    LocationID = location.ID,
                    IsAdHoc = false,
                    StatusID = 1
                };
                ctx.CompanyData.AddRange(new[] { company1, company2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var contact1 = new ContactData()
                {
                    BID = testBID,
                    ID = -99,
                    First = "TEST",
                    Last = "SUBJECT1",
                };
                var contact2 = new ContactData()
                {
                    BID = testBID,
                    ID = -98,
                    First = "TEST",
                    Last = "SUBJECT2",
                };
                ctx.ContactData.AddRange(new[] { contact1, contact2 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var companyContactLink1 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company1.ID,
                    ContactID = contact1.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                var companyContactLink2 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact1.ID,
                    Roles = ContactRole.Inactive
                };
                var companyContactLink3 = new CompanyContactLink()
                {
                    BID = testBID,
                    CompanyID = company2.ID,
                    ContactID = contact2.ID,
                    Roles = (ContactRole.Primary | ContactRole.Billing)
                };
                ctx.CompanyContactLink.AddRange(new[] { companyContactLink1, companyContactLink2, companyContactLink3 });
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                // these SHOULD throw exceptions
                try
                {
                    await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                        , parameters: new[]{
                            new SqlParameter("@BID", testBID),
                            new SqlParameter("@ContactID", contact2.ID),
                            new SqlParameter("@CompanyID", company1.ID),
                            new SqlParameter(@"RoleID", ContactRole.Primary),
                            resultParam
                    });
                }
                catch (Exception ex)
                {
                    Assert.AreEqual($"Company or Contact do not exist or are Not Associated with the each other. CompanyID={company1.ID} ContactID={contact2.ID}", ex.Message);
                }

                try
                {
                    await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                        , parameters: new[]{
                            new SqlParameter("@BID", testBID),
                            new SqlParameter("@ContactID", contact1.ID),
                            new SqlParameter("@CompanyID", DBNull.Value),
                            new SqlParameter(@"RoleID", ContactRole.Primary),
                            resultParam
                    });
                }
                catch (Exception ex)
                {
                    Assert.AreEqual($"The Specified Contact does not exist or is associated with multiple Companies.  You must Specify a company when Company when setting the billing contact. ContactID={contact1.ID}", ex.Message);
                }

                try
                {
                    await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                        , parameters: new[]{
                            new SqlParameter("@BID", testBID),
                            new SqlParameter("@ContactID", contact1.ID),
                            new SqlParameter("@CompanyID", company1.ID),
                            new SqlParameter(@"RoleID", ContactRole.Inactive),
                            resultParam
                    });
                }
                catch (Exception ex)
                {
                    Assert.AreEqual($"You can't set the primary or billing contact inactive without first setting another contact to those roles. ContactID={contact1.ID}", ex.Message);
                }

                Assert.AreEqual(ContactRole.Inactive, (await ctx.CompanyContactLink.AsNoTracking()
                        .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);
                Assert.AreEqual((ContactRole.Primary | ContactRole.Billing), (await ctx.CompanyContactLink.AsNoTracking()
                        .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);

                await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                    , parameters: new[]{
                        new SqlParameter("@BID", testBID),
                        new SqlParameter("@ContactID", contact1.ID),
                        new SqlParameter("@CompanyID", company2.ID),
                        new SqlParameter(@"RoleID", ContactRole.Primary),
                        resultParam
                    });
                Assert.AreEqual(2, resultParam.Value);
                Assert.AreEqual(ContactRole.Primary, (await ctx.CompanyContactLink.AsNoTracking()
                        .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);
                Assert.AreEqual(ContactRole.Billing, (await ctx.CompanyContactLink.AsNoTracking()
                        .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);

                await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                    , parameters: new[]{
                        new SqlParameter("@BID", testBID),
                        new SqlParameter("@ContactID", contact1.ID),
                        new SqlParameter("@CompanyID", company2.ID),
                        new SqlParameter(@"RoleID", ContactRole.Billing),
                        resultParam
                    });
                Assert.AreEqual(2, resultParam.Value);
                Assert.AreEqual((ContactRole.Primary | ContactRole.Billing), (await ctx.CompanyContactLink.AsNoTracking()
                        .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);
                Assert.AreEqual(ContactRole.Observer, (await ctx.CompanyContactLink.AsNoTracking()
                        .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);

                await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                    , parameters: new[]{
                        new SqlParameter("@BID", testBID),
                        new SqlParameter("@ContactID", contact2.ID),
                        new SqlParameter("@CompanyID", company2.ID),
                        new SqlParameter(@"RoleID", (ContactRole.Primary | ContactRole.Billing)),
                        resultParam
                    });
                Assert.AreEqual(2, resultParam.Value);
                Assert.AreEqual((ContactRole.Primary | ContactRole.Billing), (await ctx.CompanyContactLink.AsNoTracking()
                    .FirstOrDefaultAsync(l => l.ContactID == contact2.ID && l.CompanyID == company2.ID)).Roles);

                await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                    , parameters: new[]{
                        new SqlParameter("@BID", testBID),
                        new SqlParameter("@ContactID", contact1.ID),
                        new SqlParameter("@CompanyID", company2.ID),
                        new SqlParameter(@"RoleID", ContactRole.Inactive),
                        resultParam
                    });
                Assert.AreEqual(2, resultParam.Value);
                Assert.AreEqual(ContactRole.Inactive, (await ctx.CompanyContactLink.AsNoTracking()
                        .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);

                await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Contact.Action.SetRole] @BID, @ContactID, @CompanyID, @RoleID, @Result;"
                    , parameters: new[]{
                        new SqlParameter("@BID", testBID),
                        new SqlParameter("@ContactID", contact1.ID),
                        new SqlParameter("@CompanyID", company2.ID),
                        new SqlParameter(@"RoleID", ContactRole.Owner),
                        resultParam
                    });
                Assert.AreEqual(2, resultParam.Value);
                Assert.AreEqual(ContactRole.Owner, (await ctx.CompanyContactLink.AsNoTracking()
                        .FirstOrDefaultAsync(l => l.ContactID == contact1.ID && l.CompanyID == company2.ID)).Roles);

                #region Cleanup Data

                var cleanupLinks = await ctx.CompanyContactLink.Where(x => x.ContactID < 0 || x.CompanyID < 0).ToListAsync();
                if (cleanupLinks != null)
                {
                    ctx.RemoveRange(cleanupLinks);
                    await ctx.SaveChangesAsync();
                }

                var cleanupContacts = await ctx.ContactData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupContacts != null)
                {
                    ctx.RemoveRange(cleanupContacts);
                    await ctx.SaveChangesAsync();
                }

                var cleanupCompanies = await ctx.CompanyData.Where(x => x.ID < 0).ToListAsync();
                if (cleanupCompanies != null)
                {
                    ctx.RemoveRange(cleanupCompanies);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public async Task EFMachineCategoryLinkMachineSprocTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                #region Remove if Existing Data

                var neg99MachineCategoryLink = await ctx.MachineCategoryLink.Where(x => x.PartID == -99 && x.CategoryID == -99).FirstOrDefaultAsync();
                if (neg99MachineCategoryLink != null)
                {
                    ctx.Remove(neg99MachineCategoryLink);
                    await ctx.SaveChangesAsync();
                }

                var neg99Machine = await ctx.MachineData.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99Machine != null)
                {
                    ctx.Remove(neg99Machine);
                    await ctx.SaveChangesAsync();
                }

                var neg99MachineCategory = await ctx.MachineCategory.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99MachineCategory != null)
                {
                    ctx.Remove(neg99MachineCategory);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                // Machine Create
                var machine = new MachineData()
                {
                    BID = 1,
                    ID = -99,
                    Description = "Test Subject",
                    ExpenseAccount = null,
                    IncomeAccountID = null,
                    HasImage = false,
                    IsActive = true,
                    Name = "Test Subject",
                    SKU = ""

                };
                ctx.MachineData.Add(machine);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // AdHoc Contact Create
                var machineCategory = new MachineCategory()
                {
                    BID = 1,
                    ID = -99,
                    Description = "Test Subject",
                    IsActive = true,
                    Name = "Test Subject"
                };
                ctx.MachineCategory.Add(machineCategory);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // Test Sproc for Linking machine to a category
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;
                int rowResult = 0;

                SqlParameter[] myParams = new SqlParameter[5]
                {
                    new SqlParameter("@BID", 1),
                    new SqlParameter("@MachineCategoryID", -99), // does not exist
                    new SqlParameter("@MachineDataID", -99),
                    new SqlParameter("@IsLinked", 1),
                    resultParam
                };

                // these SHOULD throw exceptions
                try
                {
                    // when invalid category ID is passed
                    myParams.SetValue(new SqlParameter("@MachineCategoryID", 999), 1);
                    myParams.SetValue(new SqlParameter("@MachineDataID", -99), 2);
                    await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Part.Machine.Category.Action.LinkMachine] @BID, @MachineCategoryID, @MachineDataID, @IsLinked;", parameters: myParams);
                }
                catch (Exception ex)
                {
                    Assert.AreEqual("Invalid MachineCategory Specified. MachineCategoryID=999 not found", ex.Message);
                }

                try
                {
                    // when invalid machine ID is passed
                    myParams.SetValue(new SqlParameter("@MachineCategoryID", -99), 1);
                    myParams.SetValue(new SqlParameter("@MachineDataID", 999), 2);
                    await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Part.Machine.Category.Action.LinkMachine] @BID, @MachineCategoryID, @MachineDataID, @IsLinked;", parameters: myParams);
                }
                catch (Exception ex)
                {
                    Assert.AreEqual("Invalid MachineData Specified. MachineDataID=999 not found", ex.Message);
                }

                // Test Sprocs for Normal
                // override MYSQL params with valid category
                myParams.SetValue(new SqlParameter("@MachineCategoryID", -99), 1);
                myParams.SetValue(new SqlParameter("@MachineDataID", -99), 2);
                rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Part.Machine.Category.Action.LinkMachine] @BID, @MachineCategoryID, @MachineDataID, @IsLinked;", parameters: myParams);
                Assert.AreEqual(1, rowResult);


                #region Cleanup Data

                neg99MachineCategoryLink = await ctx.MachineCategoryLink.Where(x => x.PartID == -99 && x.CategoryID == -99).FirstOrDefaultAsync();
                if (neg99MachineCategoryLink != null)
                {
                    ctx.Remove(neg99MachineCategoryLink);
                    await ctx.SaveChangesAsync();
                }

                neg99Machine = await ctx.MachineData.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99Machine != null)
                {
                    ctx.Remove(neg99Machine);
                    await ctx.SaveChangesAsync();
                }

                neg99MachineCategory = await ctx.MachineCategory.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99MachineCategory != null)
                {
                    ctx.Remove(neg99MachineCategory);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }

        [TestMethod]
        public void EnumMaterialConsumptionMethodTest()
        {
            string requiresMigration = "20180404174512_PartMaterialEnumTables_END802";
            //"PartMaterialEnumTables_END802";

            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                ctx.EnumMaterialConsumptionMethod.FirstOrDefault();
            }
        }

        [TestMethod]
        [Ignore]
#warning END-8910 removed Units in ctx
        public void EFUnitPartTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                //Assert.IsTrue(await ctx.EnumUnit.Include(x => x.UnitSystemEnum).Include(x => x.UnitClassificationEnum).FirstOrDefaultAsync() != null);
            }
        }

        [TestMethod]
        public async Task EFMaterialCategorySimpleListTest()
        {
            string requiresMigration = "20180406170656_AddPartMaterialCategorySimpleListView";

            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                await ctx.SimpleMaterialCategory.FirstOrDefaultAsync();
            }
        }

        [TestMethod]
        public async Task EFMaterialCategoryLinkMaterialSprocTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                #region Remove if Existing Data

                var neg99MaterialCategoryLink = await ctx.MaterialCategoryLink.Where(x => x.PartID == -99 && x.CategoryID == -99).FirstOrDefaultAsync();
                if (neg99MaterialCategoryLink != null)
                {
                    ctx.Remove(neg99MaterialCategoryLink);
                    await ctx.SaveChangesAsync();
                }

                var neg99Material = await ctx.MaterialData.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99Material != null)
                {
                    ctx.Remove(neg99Material);
                    await ctx.SaveChangesAsync();
                }

                ctx.RemoveRange(ctx.MaterialCategoryLink.Where(x => x.PartID <= 0 || x.CategoryID <= 0));
                await ctx.SaveChangesAsync();

                var neg99MaterialCategory = await ctx.MaterialCategory.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99MaterialCategory != null)
                {
                    ctx.Remove(neg99MaterialCategory);
                    await ctx.SaveChangesAsync();
                }

                #endregion

                // Material Create
                var material = new MaterialData()
                {
                    BID = 1,
                    ID = -99,
                    Description = "Test Subject",
                    ExpenseAccountID = 1000,
                    IncomeAccountID = 1000,
                    InventoryAccountID = 1000,
                    InvoiceText = "",
                    HasImage = false,
                    IsActive = true,
                    Name = "Test Subject",
                    SKU = ""

                };
                ctx.MaterialData.Add(material);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // AdHoc Contact Create
                var materialCategory = new MaterialCategory()
                {
                    BID = 1,
                    ID = -99,
                    Description = "Test Subject",
                    IsActive = true,
                    Name = "Test Subject"
                };
                ctx.MaterialCategory.Add(materialCategory);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // Test Sproc for Linking material to a category
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;
                int rowResult = 0;

                object[] myParams = {
                    new SqlParameter("@BID", 1),
                    new SqlParameter("@MaterialCategoryID", -99), // does not exist
                    new SqlParameter("@MaterialDataID", -99),
                    new SqlParameter("@IsLinked", 1),
                    resultParam
                };

                // these SHOULD throw exceptions
                try
                {
                    // when invalid category ID is passed
                    myParams.SetValue(new SqlParameter("@MaterialCategoryID", 999), 1);
                    myParams.SetValue(new SqlParameter("@MaterialDataID", -99), 2);
                    await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Part.Material.Category.Action.LinkMaterial] @BID, @MaterialCategoryID, @MaterialDataID, @IsLinked;", parameters: myParams);
                }
                catch (Exception ex)
                {
                    Assert.AreEqual("Invalid MaterialCategory Specified. MaterialCategoryID=999 not found", ex.Message);
                }

                try
                {
                    // when invalid material ID is passed
                    myParams.SetValue(new SqlParameter("@MaterialCategoryID", -99), 1);
                    myParams.SetValue(new SqlParameter("@MaterialDataID", 999), 2);
                    await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Part.Material.Category.Action.LinkMaterial] @BID, @MaterialCategoryID, @MaterialDataID, @IsLinked;", parameters: myParams);
                }
                catch (Exception ex)
                {
                    Assert.AreEqual("Invalid MaterialData Specified. MaterialDataID=999 not found", ex.Message);
                }

                // Test Sprocs for Normal
                // override MYSQL params with valid category
                myParams.SetValue(new SqlParameter("@MaterialCategoryID", -99), 1);
                myParams.SetValue(new SqlParameter("@MaterialDataID", -99), 2);
                rowResult = await ctx.Database.ExecuteSqlRawAsync("EXEC @Result = dbo.[Part.Material.Category.Action.LinkMaterial] @BID, @MaterialCategoryID, @MaterialDataID, @IsLinked;", parameters: myParams);
                Assert.AreEqual(1, rowResult);


                #region Cleanup Data

                neg99MaterialCategoryLink = await ctx.MaterialCategoryLink.Where(x => x.PartID == -99 && x.CategoryID == -99).FirstOrDefaultAsync();
                if (neg99MaterialCategoryLink != null)
                {
                    ctx.Remove(neg99MaterialCategoryLink);
                    await ctx.SaveChangesAsync();
                }

                neg99Material = await ctx.MaterialData.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99Material != null)
                {
                    ctx.Remove(neg99Material);
                    await ctx.SaveChangesAsync();
                }

                ctx.RemoveRange(ctx.MaterialCategoryLink.Where(x => x.PartID <= 0 || x.CategoryID <= 0));
                await ctx.SaveChangesAsync();

                neg99MaterialCategory = await ctx.MaterialCategory.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99MaterialCategory != null)
                {
                    ctx.Remove(neg99MaterialCategory);
                    await ctx.SaveChangesAsync();
                }

                #endregion
            }
        }


        [TestMethod]
        public async Task MaterialCategoryCanDeleteSprocTest()
        {
            short TestCategoryID = -101;
            short TestMaterialID = -101;

            short TestBID = 1;
            short InvalidTestCategoryID = -999;
            string storedProcedureName = "IAtom.Action.CanDelete";

            using (ApiContext ctx = GetMockCtx(TestBID))
            {
                #region Remove if Existing Data

                var testMaterialCategoryLink = await ctx.MaterialCategoryLink.Where(x => x.BID == TestBID && x.CategoryID == TestCategoryID && x.PartID == TestMaterialID).FirstOrDefaultAsync();
                if (testMaterialCategoryLink != null)
                    ctx.Remove(testMaterialCategoryLink);

                var neg100MaterialCategory = await ctx.MaterialCategory.Where(x => x.BID == TestBID && x.ID == TestCategoryID).FirstOrDefaultAsync();
                if (neg100MaterialCategory != null)
                {
                    ctx.Remove(neg100MaterialCategory);
                }
                var neg100MaterialData = await ctx.MaterialData.Where(x => x.BID == TestBID && x.ID == TestMaterialID).FirstOrDefaultAsync();

                if (neg100MaterialData != null)
                    ctx.Remove(neg100MaterialData);

                await ctx.SaveChangesAsync();

                #endregion

                #region Create Test Data

                var testGLAccount1 = await ctx.GLAccount.Where(gl => gl.BID == 1).FirstOrDefaultAsync();
                var testGLAccount2 = await ctx.GLAccount.Where(gl => gl.BID == 1 && gl.ID != testGLAccount1.ID).FirstOrDefaultAsync();
                var testGLAccount3 = await ctx.GLAccount.Where(gl => gl.BID == 1 && gl.ID != testGLAccount1.ID && gl.ID != testGLAccount2.ID).FirstOrDefaultAsync();


                MaterialCategory testMC = new MaterialCategory()
                {
                    BID = TestBID,
                    ID = TestCategoryID,
                    Name = "MaterialCategorySetActiveSprocTest",
                    Description = "MaterialCategorySetActiveSprocTest",
                    IsActive = true,
                };

                var data = new MaterialData();
                data.BID = 1;
                data.ID = TestMaterialID;
                data.Name = "test data";
                data.Description = "test";
                data.InvoiceText = "test";
                data.ExpenseAccountID = testGLAccount1.ID;
                data.IncomeAccountID = testGLAccount2.ID;
                data.InventoryAccountID = testGLAccount3.ID;
                data.ConsumptionUnit = Unit.Centimeter; //enum test
                data.Height = new Measurement(null, Unit.Box);
                data.Length = new Measurement(null, Unit.Carton);
                data.Weight = new Measurement(null, Unit.Gram);
                data.Width = new Measurement(null, Unit.Gram);
                data.PhysicalMaterialType = MaterialPhysicalType.Roll; //enum test
                data.EstimatingConsumptionMethod = MaterialConsumptionMethod.Each; //enum test
                data.EstimatingCostingMethod = MaterialCostingMethod.AverageInventoryCost; //enum test

                ctx.MaterialCategory.Add(testMC);
                ctx.MaterialData.Add(data);

                Assert.IsTrue(ctx.SaveChanges() == 2);

                #endregion
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                SqlParameter materialCategoryIDParam = new SqlParameter("@ID", TestCategoryID);
                SqlParameter materialCategoryCTIDParam = new SqlParameter("@ClassTypeID", ClassType.MaterialCategory.ID());

                SqlParameter cantDeleteReasonParam = new SqlParameter("@CantDeleteReason", System.Data.SqlDbType.VarChar, 1024);
                cantDeleteReasonParam.Direction = System.Data.ParameterDirection.Output;

                SqlParameter showCantDeleteReasonParam = new SqlParameter("@ShowCantDeleteReason", true);
                object[] myParams = {
                    new SqlParameter("@BID", TestBID),
                    materialCategoryIDParam,
                    materialCategoryCTIDParam,
                    cantDeleteReasonParam,
                    showCantDeleteReasonParam,
                    resultParam
                };

                // these SHOULD throw exceptions
                try
                {
                    // when invalid category ID is passed
                    materialCategoryIDParam.Value = InvalidTestCategoryID;
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC dbo.[{storedProcedureName}] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT, @CantDeleteReason OUTPUT;", parameters: myParams);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }

                materialCategoryIDParam.Value = TestCategoryID;
                try
                {
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC dbo.[{storedProcedureName}] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT, @CantDeleteReason OUTPUT;", parameters: myParams);
                    // resultParam will be 1 because the material category can be deleted

                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }

                // Link up TestCategory to TestData
                var TestCategoryLink = new MaterialCategoryLink()
                {
                    BID = TestBID,
                    CategoryID = TestCategoryID,
                    PartID = TestMaterialID,
                };
                ctx.MaterialCategoryLink.Add(TestCategoryLink);
                Assert.IsTrue(ctx.SaveChanges() == 1);
                try
                {
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC dbo.[{storedProcedureName}] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT,@CantDeleteReason OUTPUT;", parameters: myParams);

                    Assert.AreEqual(0, resultParam.Value);
                    Assert.AreEqual("Unable to Delete Record - Reference by the following Tables: [Part.Material.CategoryLink]", cantDeleteReasonParam.Value);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }


                #region Cleanup Data
                try
                {
                    ctx.MaterialCategoryLink.Remove(TestCategoryLink);
                    ctx.MaterialCategory.Remove(testMC);
                    ctx.MaterialData.Remove(data);
                    ctx.SaveChanges();
                }
                catch
                {

                }
                #endregion
            }
        }

        [TestMethod]
        public async Task MaterialCanDeleteSprocTest()
        {
            short TestCategoryID = -102;
            short TestMaterialID = -102;

            short TestBID = 1;
            short InvalidTestMaterialID = -999;
            string storedProcedureName = "IAtom.Action.CanDelete";

            using (ApiContext ctx = GetMockCtx(TestBID))
            {
                #region Remove if Existing Data

                var testMaterialCategoryLink = await ctx.MaterialCategoryLink.Where(x => x.BID == TestBID && x.CategoryID == TestCategoryID && x.PartID == TestMaterialID).FirstOrDefaultAsync();
                if (testMaterialCategoryLink != null)
                    ctx.Remove(testMaterialCategoryLink);

                var neg100MaterialCategory = await ctx.MaterialCategory.Where(x => x.BID == TestBID && x.ID == TestCategoryID).FirstOrDefaultAsync();
                if (neg100MaterialCategory != null)
                {
                    ctx.Remove(neg100MaterialCategory);
                }
                var neg100MaterialData = await ctx.MaterialData.Where(x => x.BID == TestBID && x.ID == TestMaterialID).FirstOrDefaultAsync();

                if (neg100MaterialData != null)
                    ctx.Remove(neg100MaterialData);

                await ctx.SaveChangesAsync();

                #endregion

                #region Create Test Data

                var testGLAccount1 = await ctx.GLAccount.Where(gl => gl.BID == 1).FirstOrDefaultAsync();
                var testGLAccount2 = await ctx.GLAccount.Where(gl => gl.BID == 1 && gl.ID != testGLAccount1.ID).FirstOrDefaultAsync();
                var testGLAccount3 = await ctx.GLAccount.Where(gl => gl.BID == 1 && gl.ID != testGLAccount1.ID && gl.ID != testGLAccount2.ID).FirstOrDefaultAsync();


                MaterialCategory testMC = new MaterialCategory()
                {
                    BID = TestBID,
                    ID = TestCategoryID,
                    Name = "MaterialCategorySetActiveSprocTest",
                    Description = "MaterialCategorySetActiveSprocTest",
                    IsActive = true,
                };

                var data = new MaterialData();
                data.BID = TestBID;
                data.ID = TestMaterialID;
                data.Name = "test data";
                data.Description = "test";
                data.InvoiceText = "test";
                data.ExpenseAccountID = testGLAccount1.ID;
                data.IncomeAccountID = testGLAccount2.ID;
                data.InventoryAccountID = testGLAccount3.ID;
                data.ConsumptionUnit = Unit.Centimeter; //enum test
                data.Height = new Measurement(null, Unit.Box);
                data.Length = new Measurement(null, Unit.Carton);
                data.Weight = new Measurement(null, Unit.Gram);
                data.Width = new Measurement(null, Unit.Gram);
                data.PhysicalMaterialType = MaterialPhysicalType.Roll; //enum test
                data.EstimatingConsumptionMethod = MaterialConsumptionMethod.Each; //enum test
                data.EstimatingCostingMethod = MaterialCostingMethod.AverageInventoryCost; //enum test

                ctx.MaterialCategory.Add(testMC);
                ctx.MaterialData.Add(data);

                Assert.IsTrue(ctx.SaveChanges() == 2);

                #endregion
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                SqlParameter materialIDParam = new SqlParameter("@ID", TestMaterialID);
                SqlParameter materialCTIDParam = new SqlParameter("@ClassTypeID", ClassType.Material.ID());

                SqlParameter cantDeleteReasonParam = new SqlParameter("@CantDeleteReason", System.Data.SqlDbType.VarChar, 1024);
                cantDeleteReasonParam.Direction = System.Data.ParameterDirection.Output;

                SqlParameter showCantDeleteReasonParam = new SqlParameter("@ShowCantDeleteReason", true);
                object[] myParams = {
                    new SqlParameter("@BID", TestBID),
                    materialIDParam,
                    materialCTIDParam,
                    cantDeleteReasonParam,
                    showCantDeleteReasonParam,
                    resultParam
                };

                // these SHOULD throw exceptions
                try
                {
                    // when invalid category ID is passed
                    materialIDParam.Value = InvalidTestMaterialID;
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC dbo.[{storedProcedureName}] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT, @CantDeleteReason OUTPUT;", parameters: myParams);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }

                materialIDParam.Value = TestMaterialID;
                try
                {
                    await ctx.Database.ExecuteSqlRawAsync($"EXEC dbo.[{storedProcedureName}] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT, @CantDeleteReason OUTPUT;", parameters: myParams);
                    // resultParam will be 1 because the material category can be deleted
                    Assert.AreEqual(1, resultParam.Value);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }

                // Link up TestCategory to TestData
                var TestCategoryLink = new MaterialCategoryLink()
                {
                    BID = TestBID,
                    CategoryID = TestCategoryID,
                    PartID = TestMaterialID,
                };
                ctx.MaterialCategoryLink.Add(TestCategoryLink);
                Assert.IsTrue(ctx.SaveChanges() == 1);
                try
                {

                    await ctx.Database.ExecuteSqlRawAsync($"EXEC dbo.[{storedProcedureName}] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT, @CantDeleteReason OUTPUT;", parameters: myParams);

                    Assert.AreEqual(0, resultParam.Value);
                    Assert.AreEqual("Unable to Delete Record - Reference by the following Tables: [Part.Material.CategoryLink]", cantDeleteReasonParam.Value);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }


                #region Cleanup Data
                try
                {
                    ctx.MaterialCategoryLink.Remove(TestCategoryLink);
                    ctx.MaterialCategory.Remove(testMC);
                    ctx.MaterialData.Remove(data);
                    ctx.SaveChanges();
                }
                catch
                {

                }
                #endregion
            }
        }

        [TestMethod]
        public async Task TestEnumClassTypeTable()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var buisinessClassType = (short)Models.ClassType.Business;
                var classTypeEntry = await ctx.EnumClassType.FirstOrDefaultAsync(c => c.ID == buisinessClassType);
                Assert.IsNotNull(classTypeEntry);
                Assert.AreEqual(buisinessClassType, classTypeEntry.ID);
            }
        }

        [TestMethod]
        public async Task TestTablesAndColumnsView()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                using (var command = ctx.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = @"
                        SELECT
                        TableID, 
                        TableName, 
                        TableType, 
                        ColumnID, 
                        ColumnName, 
                        ColumnType, 
                        IsNullable, 
                        IsComputed, 
                        IsSparse, 
                        IsIdentity, 
                        ComputedFormula, 
                        max_length, 
                        IsPK, 
                        Precision, 
                        Scale, 
                        IsHistoryTable, 
                        IsTemporalTable, 
                        HistoryTableName
                        FROM 
                        TablesAndColumns";

                    try
                    {
                        ctx.Database.OpenConnection();
                        using (var reader = await command.ExecuteReaderAsync())
                        {
                            var results = reader.SelectFromReader(r => new
                            {
                                TableID = r.GetInt32(r.GetOrdinal("TableID")),
                                TableName = r.DbGetStringOrNull(r.GetOrdinal("TableName")),
                                TableType = r.DbGetStringOrNull(r.GetOrdinal("TableType")),
                                ColumnID = r.GetInt32(r.GetOrdinal("ColumnID")),
                                ColumnName = r.DbGetStringOrNull(r.GetOrdinal("ColumnName")),
                                ColumnType = r.DbGetStringOrNull(r.GetOrdinal("ColumnType")),
                                IsNullable = r.GetBoolean(r.GetOrdinal("IsNullable")),
                                IsComputed = r.GetBoolean(r.GetOrdinal("IsComputed")),
                                IsSparse = r.GetBoolean(r.GetOrdinal("IsSparse")),
                                IsIdentity = r.GetBoolean(r.GetOrdinal("IsIdentity")),
                                ComputedFormula = r.DbGetStringOrNull(r.GetOrdinal("ComputedFormula")),
                                max_length = r.GetInt16(r.GetOrdinal("max_length")),
                                IsPK = r.GetBoolean(r.GetOrdinal("IsPK")),
                                Precision = r.GetByte(r.GetOrdinal("Precision")),
                                Scale = r.GetByte(r.GetOrdinal("Scale")),
                                IsHistoryTable = r.GetBoolean(r.GetOrdinal("IsHistoryTable")),
                                IsTemporalTable = r.GetInt32(r.GetOrdinal("IsTemporalTable")),
                                HistoryTableName = r.DbGetStringOrNull(r.GetOrdinal("HistoryTableName")),
                            });

                            var firstResult = results.FirstOrDefault();

                            Assert.IsNotNull(firstResult);
                            Assert.IsTrue(firstResult.TableID > 0);
                            Assert.IsTrue(firstResult.TableType.Length > 0);
                            Assert.IsTrue(firstResult.ColumnID > 0);
                            Assert.IsTrue(firstResult.ColumnName.Length > 0);
                            Assert.IsTrue(firstResult.ColumnType.Length > 0);
                            Assert.IsTrue(firstResult.max_length > 0);
                            //Rest of columns can be null, false or 0 so as long as we don't have any exceptions reading we should be OK
                        }
                    }
                    finally
                    {
                        ctx.Database.CloseConnection();
                    }
                }
            }
        }

        [TestMethod]
        public async Task TestIAtomActionCanDeleteSproc()
        {
            short TestBID = 1;
            int testGlAccountID = -99;
            int glAccountID = 1000;
            int classTypeID = (int)ClassType.GLAccount;
            string expectedFail = "Unable to Delete Record - Reference by the following Tables";

            using (ApiContext ctx = GetMockCtx(TestBID))
            {
                SqlParameter resultParam = new SqlParameter("@Result", System.Data.SqlDbType.Int);
                resultParam.Direction = System.Data.ParameterDirection.Output;

                SqlParameter idParam = new SqlParameter("@ID", glAccountID);
                SqlParameter classTypeIdParam = new SqlParameter("@ClassTypeID", classTypeID);

                SqlParameter cantDeleteReasonParam = new SqlParameter("@CantDeleteReason", System.Data.SqlDbType.VarChar, 1024);
                cantDeleteReasonParam.Direction = System.Data.ParameterDirection.Output;

                SqlParameter showCantDeleteReasonParam = new SqlParameter("@ShowCantDeleteReason", true);
                object[] myParams = {
                    new SqlParameter("@BID", TestBID),
                    idParam,
                    classTypeIdParam,
                    cantDeleteReasonParam,
                    showCantDeleteReasonParam,
                    resultParam
                };

                await ctx.Database.ExecuteSqlRawAsync($"EXEC dbo.[IAtom.Action.CanDelete] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT,@CantDeleteReason OUTPUT;", parameters: myParams);
                Assert.IsTrue(cantDeleteReasonParam.Value.ToString().Contains(expectedFail));

                var glAccount = new GLAccount()
                {
                    Name = $"TEST GLACCOUNT {DateTime.UtcNow}",
                    CanEdit = true,
                    IsActive = true,
                    BID = 1,
                    ID = testGlAccountID,
                    GLAccountType = 10
                };
                ctx.GLAccount.Add(glAccount);
                Assert.IsTrue(ctx.SaveChanges() == 1);

                idParam = new SqlParameter("@ID", testGlAccountID);
                object[] myParams2 = {
                    new SqlParameter("@BID", TestBID),
                    idParam,
                    classTypeIdParam,
                    cantDeleteReasonParam,
                    showCantDeleteReasonParam,
                    resultParam
                };

                await ctx.Database.ExecuteSqlRawAsync($"EXEC dbo.[IAtom.Action.CanDelete] @BID, @ID, @ClassTypeID, @ShowCantDeleteReason, @Result OUTPUT,@CantDeleteReason OUTPUT;", parameters: myParams2);
                Assert.AreEqual(1, resultParam.Value);

                ctx.GLAccount.Remove(glAccount);
                Assert.IsTrue(ctx.SaveChanges() == 1);
            }
        }

        /// <summary>
        /// Unit test for END8163_PaymentTermDefaultSupport migration
        /// </summary>
        /// <returns></returns>
        [TestMethod]
        public async Task EFPaymentTermDefaultTest()
        {
            using (ApiContext ctx = GetMockCtx(1,
                "20190815020913_END-8256_AccountingPaymentTermActionSetDefault_Sproc_Fix"))
            {
                var defaultPaymentTerm = ctx.PaymentTerm.FirstOrDefault(pt => pt.BID == -1);
                Assert.AreEqual("Cash", defaultPaymentTerm?.Name);
                var paymentTermOptionDefinition =
                    await ctx.SystemOptionDefinition.FirstOrDefaultAsync(option =>
                        option.Name == "Accounting.PaymentTerm.DefaultID");
                Assert.AreEqual((Int16)121, paymentTermOptionDefinition?.ID);
            }
        }

        [TestMethod]
        public async Task EFTestBoardDefinitionDataCRUD()
        {
            var boardDefinitionData = new BoardDefinitionData()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.BoardDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                Description = "Test Board Definition",
                DataType = DataType.Boolean,
                IsAlwaysShown = false,
                LimitToRoles = false,
                ConditionFx = "test fx"
            };

            await TestEntityCrud<BoardDefinitionData, short, bool>(boardDefinitionData, "IsAlwaysShown", false);
        }

        [TestMethod]
        public async Task EFTestBoardViewCRUD()
        {
            var boardView = new BoardView()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.BoardView,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                DataType = DataType.Boolean,
            };

            await TestEntityCrud<BoardView, short, bool>(boardView, nameof(boardView.IsActive), false);
        }

        [TestMethod]
        public async Task EFTestBoardViewLink()
        {
            var boardView = new BoardView()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.BoardView,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                DataType = DataType.Boolean,
            };

            var boardDefinitionData = new BoardDefinitionData()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.BoardDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                Description = "Test Board Definition",
                DataType = DataType.Boolean,
                IsAlwaysShown = false,
                LimitToRoles = false,
                ConditionFx = "test fx"
            };

            var boardViewLink = new BoardViewLink()
            {
                BID = 1,
                BoardID = -99,
                ViewID = -99,
                SortIndex = 1
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                ctx.Set<BoardDefinitionData>().Add(boardDefinitionData);
                await ctx.SaveChangesAsync();
                var bddEntity = await ctx.Set<BoardDefinitionData>().FirstOrDefaultAsync(x => x.ID == boardDefinitionData.ID);
                Assert.IsNotNull(bddEntity);

                ctx.Set<BoardView>().Add(boardView);
                await ctx.SaveChangesAsync();
                var bvEntity = await ctx.Set<BoardView>().FirstOrDefaultAsync(x => x.ID == boardView.ID);
                Assert.IsNotNull(bvEntity);

                ctx.Set<BoardViewLink>().Add(boardViewLink);
                await ctx.SaveChangesAsync();
                var bvlEntity = await ctx.Set<BoardViewLink>().FirstOrDefaultAsync(x => x.BoardID == boardDefinitionData.ID && x.ViewID == boardView.ID);
                Assert.IsNotNull(bvlEntity);

                ctx.Set<BoardViewLink>().Remove(bvlEntity);
                ctx.Set<BoardView>().Remove(bvEntity);
                ctx.Set<BoardDefinitionData>().Remove(bddEntity);
            }
        }

        [TestMethod]
        public async Task TestAddApprovedLostEstimateFilterToFailWhenInsertingExistingIDs()
        {
            try
            {
                var systemListFilterCriteria = new SystemListFilterCriteria()
                {
                    AllowMultiple = true,
                    ID = 97,
                    Name = "TEST"
                };
                using (ApiContext ctx = GetMockCtx(1, "20190711041521_Add_Approved_Lost_Estimate_Filter"))
                {
                    Assert.IsTrue(await ctx.SystemListFilterCriteria.AsNoTracking().ToListAsync() != null);
                    ctx.Set<SystemListFilterCriteria>().Add(systemListFilterCriteria);
                    await ctx.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Assert.AreEqual("An error occurred while updating the entries. See the inner exception for details.", ex.Message);
            }
            finally
            {
                using (ApiContext ctx = GetMockCtx(1, "20190711041521_Add_Approved_Lost_Estimate_Filter"))
                {
                    var testtoremove = await ctx.SystemListFilterCriteria.AsNoTracking().FirstOrDefaultAsync(x => x.Name.Contains("TEST"));
                    if (testtoremove != null)
                    {
                        ctx.Set<SystemListFilterCriteria>().Remove(testtoremove);
                        await ctx.SaveChangesAsync();
                    }
                }
            }
        }

        [TestMethod]
        public void ConfirmDataTypeEnumValuesExistInDatabase()
        {
            using (var ctx = GetMockCtx(1))
            {
                List<EnumDataType> enums = ctx.EnumDataType.OrderBy(t => t.ID).ToList();
                DataType[] enumValues = (DataType[])Enum.GetValues(typeof(DataType));

                List<string> errors = new List<string>();

                foreach (var enumValue in enumValues)
                {
                    var dbEnum = enums.FirstOrDefault(e => e.ID == enumValue);
                    if (dbEnum == null)
                    {
                        errors.Add($"DB table [enum.DataType] does not have entry for {enumValue}");
                    }
                    else if (dbEnum.Name.Replace(" ", "").ToLowerInvariant() != enumValue.ToString().ToLowerInvariant())
                    {
                        errors.Add($"DB table [enum.DataType] record '{dbEnum.Name}' does not match enum '{enumValue}'");
                    }
                }
                Assert.IsTrue(errors.Count == 0, String.Join('\n', errors));
            }
        }

        [TestMethod]
        public async Task FlatListItemTest()
        {
            var task = Task.Run(async () =>
            {
                using (var ctx = GetMockCtx(1))
                {
                    DateTime startDateTime = DateTime.UtcNow;
                    // Create
                    var fli = new FlatListItem()
                    {
                        BID = 1,
                        ID = -2,
                        FlatListType = FlatListType.EstimateCancelledReasons,
                        IsActive = true,
                        IsSystem = false,
                        Name = "FlatListItemTest-2",
                        SortIndex = 0,
                    };

                    ctx.FlatListItem.Add(fli);
                    try
                    {
                        int resultCount = await ctx.SaveChangesAsync();
                        Assert.AreEqual(1, resultCount);
                        ctx.Entry(fli).State = EntityState.Detached;
                    }
                    catch (Exception e)
                    {
                        Assert.Fail("Failed To Create FlatListItem:" + e.ToString());
                    }

                    //Read
                    var retrievedFli = await ctx.FlatListItem.AsNoTracking().FirstOrDefaultAsync(t => t.BID == 1 && t.ID == fli.ID);
                    Assert.AreEqual(fli.BID, retrievedFli.BID);
                    Assert.AreEqual(fli.ID, retrievedFli.ID);
                    Assert.AreEqual(fli.IsActive, retrievedFli.IsActive);
                    Assert.AreEqual(fli.FlatListType, retrievedFli.FlatListType);
                    Assert.AreEqual(fli.IsSystem, retrievedFli.IsSystem);
                    Assert.AreEqual(fli.Name, retrievedFli.Name);
                    Assert.AreEqual(fli.SortIndex, retrievedFli.SortIndex);
                    Assert.IsTrue(retrievedFli.ModifiedDT > startDateTime);
                    Assert.AreEqual(ClassType.FlatListItem.ID(), (int)retrievedFli.ClassTypeID);

                    //Update
                    var updateFli = await ctx.FlatListItem.FirstOrDefaultAsync(t => t.BID == 1 && t.ID == fli.ID);
                    updateFli.Name = "UpdatedFli";
                    try
                    {
                        Assert.AreEqual(1, await ctx.SaveChangesAsync());
                    }
                    catch (Exception e)
                    {
                        Assert.Fail("Failed to update FlatListItem:" + e.ToString());
                    }

                    retrievedFli = await ctx.FlatListItem.AsNoTracking().FirstOrDefaultAsync(t => t.BID == 1 && t.ID == fli.ID);
                    Assert.AreEqual(updateFli.BID, retrievedFli.BID);
                    Assert.AreEqual(updateFli.ID, retrievedFli.ID);
                    Assert.AreEqual(updateFli.IsActive, retrievedFli.IsActive);
                    Assert.AreEqual(updateFli.FlatListType, retrievedFli.FlatListType);
                    Assert.AreEqual(updateFli.IsSystem, retrievedFli.IsSystem);
                    Assert.AreEqual(updateFli.Name, retrievedFli.Name);
                    Assert.AreEqual(updateFli.SortIndex, retrievedFli.SortIndex);

                    Assert.AreEqual(ClassType.FlatListItem.ID(), (int)retrievedFli.ClassTypeID);

                    return new { ctx, fli };
                }
            });
            await task.ContinueWith(async (x) =>
            {
                var c = await x;
                try
                {
                    c.ctx.Remove(c.fli);
                    Assert.AreEqual(1, await c.ctx.SaveChangesAsync());
                }
                catch (Exception e)
                {
                    Assert.Fail("Failed to delete FlatListItem:" + e.ToString());
                }
            });

        }

        [TestMethod]
        public void FlatListTypeEnumTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                Assert.IsTrue(ctx.EnumFlatListType.Any());
                List<EnumFlatListType> enums = ctx.EnumFlatListType.OrderBy(t => t.ID).ToList();

                String[] enumNames = Enum.GetNames(typeof(FlatListType));
                FlatListType[] enumValues = (FlatListType[])Enum.GetValues(typeof(FlatListType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                for (int i = 0; i < enums.Count; i++)
                {
                    Assert.AreEqual(enums[i].ID, enumValues[i]);
                    if (enums[i].ID == FlatListType.EstimateCancelledReasons)
                        Assert.IsTrue(enums[i].Name == "Estimate Voided Reasons"); // these don't match
                    else
                        Assert.AreEqual(enums[i].Name.Replace(" ", ""), enumNames[i]);
                }
            }
        }

        [TestMethod]
        public async Task ContactCustomDataTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var notNull = await ctx.ContactData.FirstOrDefaultAsync(c => c.BID == 1 && c.ID == -99);
                if (notNull != null)
                {
                    ctx.ContactData.Remove(notNull);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }
                var contact = new ContactData()
                {
                    BID = 1,
                    ID = -99,
                    First = "Test",
                    Last = "Subject",
                    // REVIEW: no more CompanyID column on contact // END-9521
                    //CompanyID = ctx.CompanyData.FirstOrDefault().ID
                };
                ctx.ContactData.Add(contact);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var ContactID = contact.ID;

                var newCCD = new ContactCustomData()
                {
                    ID = ContactID,
                    BID = 1,
                    DataXML = "Test"
                };
                //CREATE
                ctx.ContactCustomData.Add(newCCD);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                //READ
                var myCCD = ctx.ContactCustomData.FirstOrDefault(p => p.ID == ContactID);
                Assert.IsNotNull(myCCD);//test select

                //UPDATE
                myCCD.DataXML = "TEST1";
                await ctx.SaveChangesAsync();
                var updateMyCFD = ctx.ContactCustomData.FirstOrDefault(p => p.ID == ContactID);
                await ctx.SaveChangesAsync();
                Assert.IsTrue(updateMyCFD.DataXML == "TEST1");//test update

                //DELETE
                ctx.Remove(updateMyCFD);
                ctx.ContactData.Remove(contact);
                await ctx.SaveChangesAsync();
                var deleteMyCFD = await ctx.ContactCustomData.Where(p => p.ID == ContactID).FirstOrDefaultAsync();
                Assert.IsNull(deleteMyCFD);//test delete
            }
        }

        [TestMethod]
        public async Task OrderCustomDataTest()
        {

            var task = Task.Run(async () => {

                using (ApiContext ctx = GetMockCtx(1))
                {

                    var companyData = await ctx.CompanyData.FirstOrDefaultAsync();
                    var locationData = await ctx.LocationData.FirstOrDefaultAsync();
                    var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync();

                    //Temporary order data
                    var orderData = new OrderData()
                    {
                        BID = companyData.BID,
                        CompanyID = companyData.ID,
                        PickupLocationID = locationData.ID,
                        ProductionLocationID = locationData.ID,
                        LocationID = locationData.ID,
                        TaxGroupID = taxGroup.ID,
                        ClassTypeID = (int)ClassType.Order,
                        ModifiedDT = DateTime.UtcNow,
                        TransactionType = (byte)OrderTransactionType.Order,
                        OrderStatusID = OrderOrderStatus.OrderPreWIP,
                        OrderStatusStartDT = DateTime.UtcNow,
                        Number = 1000,
                        FormattedNumber = "INV-1000",
                        PriceTaxRate = 0.01m,
                        PaymentPaid = 0m,
                    };

                    ctx.Add(orderData);
                    Assert.AreEqual(1, ctx.SaveChanges());

                    var OrderID = orderData.ID;

                    var newCCD = new OrderCustomData()
                    {
                        ID = OrderID,
                        BID = 1,
                        DataXML = "Test"
                    };
                    //CREATE
                    ctx.OrderCustomData.Add(newCCD);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                    //READ
                    var myCCD = ctx.OrderCustomData.FirstOrDefault(p => p.ID == OrderID);
                    Assert.IsNotNull(myCCD);//test select

                    //UPDATE
                    myCCD.DataXML = "TEST1";
                    await ctx.SaveChangesAsync();
                    var updateMyCFD = ctx.OrderCustomData.FirstOrDefault(p => p.ID == OrderID);
                    await ctx.SaveChangesAsync();
                    Assert.IsTrue(updateMyCFD.DataXML == "TEST1");//test update

                    return new
                    {
                        ctx,
                        orderData,
                        OrderID,
                        updateMyCFD
                    };
                }

            });
            await task.ContinueWith(async (res) =>
            {
                var x = await res;
                //DELETE
                x.ctx.Remove(x.updateMyCFD);
                x.ctx.OrderData.Remove(x.orderData);
                await x.ctx.SaveChangesAsync();
                var deleteMyCFD = await x.ctx.OrderCustomData.Where(p => p.ID == x.OrderID).FirstOrDefaultAsync();
                Assert.IsNull(deleteMyCFD);//test delete
            });

        }
        [TestMethod]
        public async Task CustomFieldOtherTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                //remove existing -99
                var customFieldDefinition = await ctx.CustomFieldOtherData.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (customFieldDefinition != null)
                {
                    ctx.Remove(customFieldDefinition);
                    await ctx.SaveChangesAsync();
                }

                var newCCD = new CustomFieldOtherData()
                {
                    ID = -99,
                    BID = 1,
                    AppliesToClassTypeID = (int)ClassType.Employee,
                    DataXML = "Test"
                };
                //CREATE
                ctx.CustomFieldOtherData.Add(newCCD);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                //READ
                var myCCD = ctx.CustomFieldOtherData.FirstOrDefault(p => p.ID == -99);
                Assert.IsNotNull(myCCD);//test select

                //UPDATE
                myCCD.DataXML = "TEST1";
                await ctx.SaveChangesAsync();
                var updateMyCFD = ctx.CustomFieldOtherData.FirstOrDefault(p => p.ID == -99);
                Assert.IsTrue(updateMyCFD.DataXML == "TEST1");//test update

                //DELETE
                ctx.Remove(updateMyCFD);
                await ctx.SaveChangesAsync();
                var deleteMyCFD = await ctx.CustomFieldOtherData.Where(p => p.ID == -99).FirstOrDefaultAsync();
                Assert.IsNull(deleteMyCFD);//test delete
            }
        }


        [TestMethod]
        public void EnumCustomFieldNumberDisplayType()
        {
            using (var ctx = GetMockCtx(1))
            {
                List<EnumCustomFieldNumberDisplayType> enums = ctx.EnumCustomFieldNumberDisplayType.OrderBy(t => t.ID).ToList();

                String[] enumNames = Enum.GetNames(typeof(CustomFieldNumberDisplayType));
                CustomFieldNumberDisplayType[] enumValues = (CustomFieldNumberDisplayType[])Enum.GetValues(typeof(CustomFieldNumberDisplayType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumCustomFieldNumberDisplayType enumItem in enums)
                {
                    Assert.AreEqual(enumValues[i], enumItem.ID);

                    i++;
                }
            }
        }

        [TestMethod]
        public async Task CompanyCustomDataTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {

                var neg99Company = await ctx.CompanyData.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99Company != null)
                {
                    ctx.Remove(neg99Company);
                    await ctx.SaveChangesAsync();
                }

                // AdHoc Company Create
                var company = new CompanyData()
                {
                    BID = 1,
                    ID = -99,
                    Name = "Test Subject",
                    LocationID = 1,
                    IsAdHoc = true,
                    StatusID = 1
                };
                ctx.CompanyData.Add(company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var CompanyID = company.ID;

                //remove existing -99
                var companyCustomData = await ctx.CompanyCustomData.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (companyCustomData != null)
                {
                    ctx.Remove(companyCustomData);
                    await ctx.SaveChangesAsync();
                }

                var newCCD = new CompanyCustomData()
                {
                    ID = CompanyID,
                    BID = 1,
                    DataXML = "{}"
                };
                //CREATE
                ctx.CompanyCustomData.Add(newCCD);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);//test insert

                //READ
                var myCCD = ctx.CompanyCustomData.FirstOrDefault(p => p.ID == -99);
                Assert.IsNotNull(myCCD);//test select

                //UPDATE
                myCCD.DataXML = "{}";
                await ctx.SaveChangesAsync();
                var updateMyCCD = ctx.CompanyCustomData.FirstOrDefault(p => p.ID == -99);
                Assert.IsTrue(updateMyCCD.DataXML == "{}");//test update

                //DELETE
                ctx.Remove(updateMyCCD);
                ctx.CompanyData.Remove(company);
                await ctx.SaveChangesAsync();
                var deleteMyCCD = await ctx.CompanyCustomData.Where(p => p.ID == -99).FirstOrDefaultAsync();
                Assert.IsNull(deleteMyCCD);//test delete

            }
        }

        [TestMethod]
        public async Task CheckCompanyStatusEnums()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var statuses = await ctx.EnumCrmCompanyStatus.ToListAsync();
                Assert.IsNotNull(statuses);
                Assert.AreEqual(15, statuses.Count);
            }
        }

        [TestMethod]
        public async Task EFAddCompanyTaxExemptReason()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                // remove if companydata exist
                var neg99Company = await ctx.CompanyData.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99Company != null)
                {
                    if (neg99Company.TaxExemptReasonID.HasValue)
                        ctx.Remove(ctx.FlatListItem.Where(x => x.BID == neg99Company.BID && x.ID == neg99Company.TaxExemptReasonID));

                    ctx.Remove(neg99Company);
                    await ctx.SaveChangesAsync();
                }
                // remove if FlatListItem exist
                var neg99FlatListItem = await ctx.FlatListItem.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99FlatListItem != null)
                {
                    ctx.Remove(neg99FlatListItem);
                    await ctx.SaveChangesAsync();
                }

                FlatListItem fli = GetNewTaxExemptReasonFlatListItem(-99);
                ctx.FlatListItem.Add(fli);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // AdHoc Company Create
                var company = new CompanyData()
                {
                    BID = 1,
                    ID = -99,
                    Name = "Test Subject",
                    LocationID = 1,
                    IsAdHoc = true,
                    StatusID = 1
                };
                ctx.CompanyData.Add(company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // Test setting TaxExemptReason
                company.TaxExemptReasonID = fli.ID;
                company.IsTaxExempt = true;
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                ctx.CompanyData.Remove(company);
                ctx.FlatListItem.Remove(fli);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }
        }

        private static FlatListItem GetNewTaxExemptReasonFlatListItem(short ID, short BID = 1)
        {
            // create new FlatListItem
            return new FlatListItem()
            {
                BID = BID,
                ID = ID,
                FlatListType = FlatListType.TaxExemptReasons,
                IsActive = true,
                IsSystem = false,
                Name = $"FlatListItemTest{ID}",
                SortIndex = 0,
            };
        }

        [TestMethod]
        public async Task EFRightsGroupList()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                // remove if RightsGroupList exist
                var neg99RightsGroupListItem = await ctx.RightsGroupList.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99RightsGroupListItem != null)
                {
                    ctx.Remove(neg99RightsGroupListItem);
                    await ctx.SaveChangesAsync();
                }

                // create new RightsGroupListItem
                var rgl = new RightsGroupList()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.RightsGroupList,
                    Name = "RightsGroupListTest-99",
                    RightsArray = "TestArray",
                };
                ctx.RightsGroupList.Add(rgl);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                ctx.RightsGroupList.Remove(rgl);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EFRightsGroup()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                // remove if RightsGroup exist
                var neg99RightsGroupItem = await ctx.RightsGroup.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99RightsGroupItem != null)
                {
                    ctx.Remove(neg99RightsGroupItem);
                    await ctx.SaveChangesAsync();
                }

                // create new RightsGroup
                var rgl = new RightsGroup()
                {
                    ID = -99,
                    Name = "RightsGroupTest-99"
                };
                ctx.RightsGroup.Add(rgl);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                ctx.RightsGroup.Remove(rgl);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EFRightsGroupListRightsGroupLink()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                // remove if RightsGroupListRightsGroupLink exist
                var neg99RightsGroupListRightsGroupLinkItem = await ctx.RightsGroupListRightsGroupLink.Where(x => x.GroupID == -99 || x.ListID == -99).FirstOrDefaultAsync();
                if (neg99RightsGroupListRightsGroupLinkItem != null)
                {
                    ctx.Remove(neg99RightsGroupListRightsGroupLinkItem);
                    await ctx.SaveChangesAsync();
                }

                // remove if RightsGroupList exist
                var neg99RightsGroupListItem = await ctx.RightsGroupList.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99RightsGroupListItem != null)
                {
                    ctx.Remove(neg99RightsGroupListItem);
                    await ctx.SaveChangesAsync();
                }

                // remove if RightsGroup exist
                var neg99RightsGroupItem = await ctx.RightsGroup.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99RightsGroupItem != null)
                {
                    ctx.Remove(neg99RightsGroupItem);
                    await ctx.SaveChangesAsync();
                }

                // create new RightsGroupListItem
                var rgl = new RightsGroupList()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.RightsGroupList,
                    Name = "RightsGroupListTest-99",
                    RightsArray = "TestArray",
                };
                ctx.RightsGroupList.Add(rgl);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // create new RightsGroup
                var rg = new RightsGroup()
                {
                    ID = -99,
                    Name = "RightsGroupTest-99",
                };
                ctx.RightsGroup.Add(rg);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // create new RightsGroupListRightsGroupLink
                var rglrgl = new RightsGroupListRightsGroupLink()
                {
                    BID = 1,
                    ListID = rgl.ID,
                    GroupID = rg.ID
                };
                ctx.RightsGroupListRightsGroupLink.Add(rglrgl);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);


                ctx.RightsGroupListRightsGroupLink.Remove(rglrgl);
                ctx.RightsGroup.Remove(rg);
                ctx.RightsGroupList.Remove(rgl);
                await ctx.SaveChangesAsync();

            }
        }

        [TestMethod]
        public async Task EFRightsGroupChildGroup()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                // remove if RightsGroup exist
                var RightsGroupChildGroupLink = await ctx.RightsGroupChildGroupLink.Where(x => x.ParentID == -99).FirstOrDefaultAsync();
                if (RightsGroupChildGroupLink != null)
                {
                    ctx.Remove(RightsGroupChildGroupLink);
                    await ctx.SaveChangesAsync();
                }

                var neg99RightsGroupItem = await ctx.RightsGroup.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99RightsGroupItem != null)
                {
                    ctx.Remove(neg99RightsGroupItem);
                    await ctx.SaveChangesAsync();
                }

                var neg98RightsGroupItem = await ctx.RightsGroup.Where(x => x.ID == -98).FirstOrDefaultAsync();
                if (neg98RightsGroupItem != null)
                {
                    ctx.Remove(neg98RightsGroupItem);
                    await ctx.SaveChangesAsync();
                }

                // create new RightsGroup
                var rgl = new RightsGroup()
                {
                    ID = -99,
                    Name = "RightsGroupTest-99"
                };
                ctx.RightsGroup.Add(rgl);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var rgl2 = new RightsGroup()
                {
                    ID = -98,
                    Name = "RightsGroupTest-98"
                };
                ctx.RightsGroup.Add(rgl2);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var rgcgl = new RightsGroupChildGroupLink()
                {
                    ParentID = -99,
                    ChildID = -98
                };
                ctx.RightsGroupChildGroupLink.Add(rgcgl);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                ctx.RightsGroupChildGroupLink.Remove(rgcgl);
                ctx.RightsGroup.Remove(rgl);
                ctx.RightsGroup.Remove(rgl2);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EFUserLink()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                // remove if RightsGroupList exist
                var neg99UserLink = await ctx.UserLink.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99UserLink != null)
                {
                    ctx.Remove(neg99UserLink);
                    await ctx.SaveChangesAsync();
                }

                // create new RightsGroupListItem
                var ul = new UserLink()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.UserLink,
                    EmployeeID = ctx.EmployeeData.FirstOrDefault().ID,
                    ContactID = ctx.ContactData.FirstOrDefault().ID,
                    UserAccessType = UserAccessType.Contact,
                    AuthUserID = 1,
                    TempUserID = new Guid()
                };
                ctx.UserLink.Add(ul);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                ctx.UserLink.Remove(ul);
                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EFBoardEmployeeLink()
        {
            var employee = new EmployeeData()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.Employee,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                First = "TEST",
                Last = "SUBJECT"
            };

            var boardDefinitionData = new BoardDefinitionData()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.BoardDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                Description = "Test Board Definition",
                DataType = DataType.Boolean,
                IsAlwaysShown = false,
                LimitToRoles = false,
                ConditionFx = "test fx"
            };

            var boardLink = new BoardEmployeeLink()
            {
                BID = 1,
                BoardID = -99,
                EmployeeID = -99,
                SortIndex = 1,
                IsFavorite = false,
                ModuleType = Module.Sales
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                var boardExists = await ctx.BoardDefinitionData.FirstOrDefaultAsync(x => x.BID == 1 && x.ID == boardDefinitionData.ID);
                if (boardExists != null)
                {
                    ctx.Remove(boardExists);
                    await ctx.SaveChangesAsync();
                }

                ctx.Set<BoardDefinitionData>().Add(boardDefinitionData);
                await ctx.SaveChangesAsync();
                var bddEntity = await ctx.Set<BoardDefinitionData>().FirstOrDefaultAsync(x => x.BID == 1 && x.ID == boardDefinitionData.ID);
                Assert.IsNotNull(bddEntity);

                var empExists = await ctx.EmployeeData.FirstOrDefaultAsync(x => x.BID == 1 && x.ID == employee.ID);
                if (empExists != null)
                {
                    ctx.Remove(empExists);
                    await ctx.SaveChangesAsync();
                }

                employee.LocationID = (await ctx.LocationData.FirstOrDefaultAsync(x => x.BID == 1)).ID;
                ctx.Set<EmployeeData>().Add(employee);
                await ctx.SaveChangesAsync();
                var eEntity = await ctx.Set<EmployeeData>().FirstOrDefaultAsync(x => x.BID == 1 && x.ID == employee.ID);
                Assert.IsNotNull(eEntity);

                ctx.Set<BoardEmployeeLink>().Add(boardLink);
                await ctx.SaveChangesAsync();
                var belEntity = await ctx.Set<BoardEmployeeLink>().FirstOrDefaultAsync(x => x.BID == 1 && x.BoardID == boardDefinitionData.ID && x.EmployeeID == employee.ID);
                Assert.IsNotNull(belEntity);

                ctx.Set<BoardEmployeeLink>().Remove(belEntity);
                ctx.Set<EmployeeData>().Remove(eEntity);
                ctx.Set<BoardDefinitionData>().Remove(bddEntity);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EFBoardRoleLink()
        {
            var role = new EmployeeRole()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.EmployeeRole,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                Name = "TEST ROLE",
            };

            var boardDefinitionData = new BoardDefinitionData()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.BoardDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                Description = "Test Board Definition",
                DataType = DataType.Boolean,
                IsAlwaysShown = false,
                LimitToRoles = false,
                ConditionFx = "test fx"
            };

            var boardLink = new BoardRoleLink()
            {
                BID = 1,
                BoardID = -99,
                RoleID = -99
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                ctx.Set<BoardDefinitionData>().Add(boardDefinitionData);
                await ctx.SaveChangesAsync();
                var bddEntity = await ctx.Set<BoardDefinitionData>().FirstOrDefaultAsync(x => x.ID == boardDefinitionData.ID);
                Assert.IsNotNull(bddEntity);

                ctx.Set<EmployeeRole>().Add(role);
                await ctx.SaveChangesAsync();
                var rEntity = await ctx.Set<EmployeeRole>().FirstOrDefaultAsync(x => x.ID == role.ID);
                Assert.IsNotNull(rEntity);

                ctx.Set<BoardRoleLink>().Add(boardLink);
                await ctx.SaveChangesAsync();
                var blEntity = await ctx.Set<BoardRoleLink>().FirstOrDefaultAsync(x => x.BoardID == boardDefinitionData.ID && x.RoleID == role.ID);
                Assert.IsNotNull(blEntity);

                ctx.Set<BoardRoleLink>().Remove(blEntity);
                ctx.Set<EmployeeRole>().Remove(rEntity);
                ctx.Set<BoardDefinitionData>().Remove(bddEntity);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public void EnumModuleTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                List<EnumModule> enums = ctx.EnumModule.OrderBy(t => t.ID).ToList();

                String[] enumNames = Enum.GetNames(typeof(Module));
                Module[] enumValues = (Module[])Enum.GetValues(typeof(Module));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumModule enumItem in enums)
                {
                    Assert.AreEqual(enumItem.ID, enumValues[i]);
                    Assert.AreEqual(enumItem.Name.Replace(" ", ""), enumNames[i]);

                    i++;
                }
            }
        }

        [TestMethod]
        public void EnumDomainEmailTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.DomainEmail.ToList());
                Assert.IsNotNull(ctx.DomainEmailLocationLink.ToList());
                Assert.IsNotNull(ctx.EmailSecurityType.ToList());
                Assert.IsNotNull(ctx.EmailProviderType.ToList());
                Assert.IsNotNull(ctx.SystemEmailSMTPConfigurationType.ToList());
            }
        }

        [TestMethod]
        public void EFDomainEmailLocationLink_CheckFields()
        {
            string requiresMigration = "20190717140211_END-7565_FixDomainEmailLocationExcessFields";

            using (var ctx = GetMockCtx(1, requiresMigration))
            {
                try
                {
                    var rowResult =
                        ctx.Database.ExecuteSqlRawAsync(
                            @"SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME = 'Domain.Email.LocationLink' 
AND COLUMN_NAME = 'DomainEmailBID'
OR COLUMN_NAME = 'DomainEmailID'
OR COLUMN_NAME = 'LocationDataBID'
OR COLUMN_NAME = 'LocationDataID';");
                    Assert.IsNotNull(rowResult);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }
            }
        }

        [TestMethod]
        public async Task EFDomainEmailSimpleListTest()
        {
            string requiresMigration = "20180801111939_AddDomainEmailSimpleListView";

            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                await ctx.SimpleDomainEmail.FirstOrDefaultAsync();
            }
        }

        [TestMethod]
        public async Task EFEmployeeTimeCardDetailTest()
        {
            string requiresMigration = "20190904050638_END-8311_ComputedFields_EmployeeTimeCardDetail_Fix";
            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                try
                {
                    var rowResult = await
                        ctx.Database.ExecuteSqlRawAsync(
                            @"SELECT * FROM [Employee.TimeCard.Detail];");

                    Assert.IsNotNull(rowResult);
                }
                catch (Exception e)
                {
                    Assert.Fail(e.Message);
                }
            }
        }

        [TestMethod]
        public void SystemAutomationActionTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.SystemAutomationActionDefinition.ToList());
                Assert.IsNotNull(ctx.SystemAutomationActionDataTypeLink.ToList());
                Assert.IsNotNull(ctx.SystemAutomationTriggerDefinition.ToList());
                Assert.IsNotNull(ctx.EnumAutomationTriggerCategoryType.ToList());

                Assert.IsNotNull(ctx.AlertDefinition.ToList());
                Assert.IsNotNull(ctx.AlertDefinitionAction.ToList());

                Assert.IsNotNull(ctx.AlertDefinition.Include("Actions").Include("Actions.Definition").ToList());
            }
        }

        [TestMethod]
        public async Task EFAlertDefinitionSimpleListTest()
        {
            string requiresMigration = "20180814133941_AddAlertDefinitionSimpleListView";

            using (ApiContext ctx = GetMockCtx(1, requiresMigration))
            {
                await ctx.SimpleAlertDefinition.FirstOrDefaultAsync();
            }
        }


        [TestMethod]
        public async Task EFAlertDefinitionListFilters()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var listFilters = await ctx.ListFilter.Where(x => x.TargetClassTypeID == Convert.ToInt32(ClassType.AlertDefinition)).ToListAsync();
                Assert.IsNotNull(listFilters);
                Assert.IsTrue(listFilters.Count() >= 1);
                Assert.IsNotNull(listFilters.FirstOrDefault(x => x.Name == "All"));

                var criteria = await ctx.SystemListFilterCriteria.Where(x => x.TargetClassTypeID == Convert.ToInt32(ClassType.AlertDefinition)).ToListAsync();
                Assert.IsNotNull(criteria);
                Assert.IsTrue(criteria.Count() >= 1);
                Assert.IsNotNull(criteria.FirstOrDefault(x => x.Field == "EmployeeID"));
                Assert.IsNotNull(criteria.FirstOrDefault(x => x.Field == "-IsActive"));
            }
        }

        [TestMethod]
        public async Task EFBoardModuleLink()
        {
            var boardDefinitionData = new BoardDefinitionData()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.BoardDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = false,
                Name = "Test Board Definition",
                Description = "Test Board Definition",
                DataType = DataType.Boolean,
                IsAlwaysShown = false,
                LimitToRoles = false,
                ConditionFx = "test fx"
            };

            var boardLink = new BoardModuleLink()
            {
                BID = 1,
                BoardID = -99,
                ModuleType = Module.Accounting
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                ctx.Set<BoardDefinitionData>().Add(boardDefinitionData);
                await ctx.SaveChangesAsync();
                var bddEntity = await ctx.Set<BoardDefinitionData>().FirstOrDefaultAsync(x => x.ID == boardDefinitionData.ID);
                Assert.IsNotNull(bddEntity);

                ctx.Set<BoardModuleLink>().Add(boardLink);
                await ctx.SaveChangesAsync();
                var blEntity = await ctx.Set<BoardModuleLink>().FirstOrDefaultAsync(x => x.BoardID == boardDefinitionData.ID && x.ModuleType == Module.Accounting);
                Assert.IsNotNull(blEntity);

                ctx.Set<BoardModuleLink>().Remove(blEntity);
                ctx.Set<BoardDefinitionData>().Remove(bddEntity);

                await ctx.SaveChangesAsync();
            }
        }

        [TestMethod]
        public async Task EnumDomainStatusTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                var enums = await ctx.EnumDomainStatus.OrderBy(t => t.ID).ToListAsync();
                var enumNames = Enum.GetNames(typeof(DomainStatus));
                var enumValues = (DomainStatus[])Enum.GetValues(typeof(DomainStatus));

                Assert.AreEqual(enums.Count, enumNames.Length);
                for (int i = 0; i < enums.Count(); i++)
                {
                    Assert.AreEqual(enums[i].ID, enumValues[i]);
                    Assert.AreEqual(enums[i].Name.Replace(" ", ""), enumNames[i]);
                }
            }
        }

        [TestMethod]
        public async Task EnumDomainAccessTypeTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                var enums = await ctx.EnumDomainAccessType.OrderBy(t => t.ID).ToListAsync();
                var enumNames = Enum.GetNames(typeof(DomainAccessType));
                var enumValues = (DomainAccessType[])Enum.GetValues(typeof(DomainAccessType));

                Assert.AreEqual(enums.Count, enumNames.Length);
                for (int i = 0; i < enums.Count(); i++)
                {
                    Assert.AreEqual(enums[i].ID, enumValues[i]);
                    Assert.AreEqual(enums[i].Name.Replace(" ", ""), enumNames[i]);
                }
            }
        }

        [TestMethod]
        public async Task EFDomainDataTest()
        {
            var domainData = new DomainData()
            {
                BID = 1,
                ID = -99,
                AccessType = DomainAccessType.Business,
                Status = DomainStatus.Pending,
                Domain = "TEST.CUSTOM.DOMAIN",
                LocationID = 1
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                try
                {
                    await ctx.DomainData.AddAsync(domainData);
                    await ctx.SaveChangesAsync();
                    var get = await ctx.DomainData.Where(d => d.BID == 1 && d.ID == domainData.ID).FirstOrDefaultAsync();
                    Assert.IsNotNull(get);

                    get.Status = DomainStatus.Online;
                    ctx.DomainData.Update(get);
                    await ctx.SaveChangesAsync();

                    var put = await ctx.DomainData.Where(d => d.BID == 1 && d.ID == get.ID).FirstOrDefaultAsync();
                    Assert.IsNotNull(put);
                    Assert.AreEqual(DomainStatus.Online, put.Status);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
                finally
                {
                    ctx.DomainData.Remove(domainData);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        [TestMethod]
        public async Task EFSSLCertificateDataTest()
        {
            var date = DateTime.UtcNow;
            var sslCert = new SSLCertificateData()
            {
                BID = 1,
                ID = -99,
                CommonName = "TEST SSL CERT",
                FileName = "File Name",
                Thumbprint = Guid.NewGuid().ToString(),
                CanHaveMultipleDomains = false,
                InstalledDT = date,
                ValidFromDT = date,
                ValidToDT = date.AddMonths(3)
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                try
                {
                    await ctx.SSLCertificateData.AddAsync(sslCert);
                    await ctx.SaveChangesAsync();
                    var get = await ctx.SSLCertificateData.Where(d => d.BID == 1 && d.ID == sslCert.ID).FirstOrDefaultAsync();
                    Assert.IsNotNull(get);

                    get.ValidToDT = date.AddMonths(6);
                    ctx.SSLCertificateData.Update(get);
                    await ctx.SaveChangesAsync();

                    var put = await ctx.SSLCertificateData.Where(d => d.BID == 1 && d.ID == get.ID).FirstOrDefaultAsync();
                    Assert.IsNotNull(put);
                    Assert.AreEqual(date.AddMonths(6), put.ValidToDT);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
                finally
                {
                    ctx.SSLCertificateData.Remove(sslCert);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        /** Can't run this Test till EnumUserRight's are populated
        [TestMethod]
        public async Task EFRightsGroupRightLink()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                // remove if RightsGroupListRightsGroupLink exist
                var neg99RightsGroupRightItem = await ctx.RightsGroupRightLink.Where(x => x.RightsGroupID == -99).FirstOrDefaultAsync();
                if (neg99RightsGroupRightItem != null)
                {
                    ctx.Remove(neg99RightsGroupRightItem);
                    await ctx.SaveChangesAsync();
                }

                // remove if RightsGroup exist
                var neg99RightsGroupItem = await ctx.RightsGroup.Where(x => x.ID == -99).FirstOrDefaultAsync();
                if (neg99RightsGroupItem != null)
                {
                    ctx.Remove(neg99RightsGroupItem);
                    await ctx.SaveChangesAsync();
                }

                // create new RightsGroup
                var rg = new RightsGroup()
                {
                    BID = 1,
                    ID = -99,
                    ClassTypeID = (int)ClassType.RightsGroup,
                    Name = "RightsGroupTest-99",
                    IsSystem = true,
                    UserAccessType = UserAccessType.Contact,
                    Module = 1
                };
                ctx.RightsGroup.Add(rg);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                // create new RightsGroupListRightsGroupLink
                var rgr = new RightsGroupRightLink()
                {
                    BID = 1,
                    RightsGroupID = rg.ID,
                    RightID = ctx.EnumUserRight.FirstOrDefault().ID
                };
                ctx.RightsGroupRightLink.Add(rgr);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);


                ctx.RightsGroupRightLink.Remove(rgr);
                ctx.RightsGroup.Remove(rg);
                await ctx.SaveChangesAsync();
            }
        }*/

        /** Can't run this Test till EnumUserRight's are populated
        [TestMethod]
        public void TestEnumUserRight()
        {
            using (var ctx = GetMockCtx(1))
            {
                List<EnumUserRight> enums = ctx.EnumUserRight.OrderBy(t => t.ID).ToList();

                String[] enumNames = Enum.GetNames(typeof(UserRight));
                UserRight[] enumValues = (UserRight[])Enum.GetValues(typeof(UserRight));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumUserRight enumItem in enums)
                {
                    Assert.AreEqual(enumValues[i], enumItem.ID);

                    switch (enumValues[i])
                    {
                        case UserRight.None:
                            Assert.AreEqual("0", enumItem.Name);
                            break;

                        default:
                            Assert.AreEqual(enumNames[i], enumItem.Name.Replace(" ", "").Replace("&", ""));
                            break;
                    }
                    i++;
                }
            }
        }
        */

        [TestMethod]
        public void TestEnumUserAccessType()
        {
            using (var ctx = GetMockCtx(1))
            {
                List<EnumUserAccessType> enums = ctx.EnumUserAccessType.OrderBy(t => t.ID).ToList();

                String[] enumNames = Enum.GetNames(typeof(UserAccessType));
                UserAccessType[] enumValues = (UserAccessType[])Enum.GetValues(typeof(UserAccessType));

                Assert.AreEqual(enums.Count, enumNames.Length);

                int i = 0;
                foreach (EnumUserAccessType enumItem in enums)
                {
                    Assert.AreEqual(enumValues[i], enumItem.ID);

                    switch (enumValues[i])
                    {
                        case UserAccessType.Contact:
                            Assert.AreEqual("Contact", enumItem.Name);
                            break;

                        default:
                            Assert.AreEqual(enumNames[i], enumItem.Name.Replace(" ", "").Replace("&", ""));
                            break;
                    }
                    i++;
                }
            }
        }

        [TestMethod]
        public void EFDashboardTest()
        {
            using (var ctx = GetMockCtx(1))
            {
                var getDash = ctx.DashboardData.FirstOrDefault();
                Assert.IsNotNull(getDash);
                var getWidget = ctx.DashboardWidgetData.FirstOrDefault();
                Assert.IsNotNull(getWidget);
                var getWidgetDef = ctx.DashboardWidgetDefinition.FirstOrDefault();
                Assert.IsNotNull(getWidgetDef);
            }
        }

        [TestMethod]
        public async Task EFTestLabelOptions()
        {
            using (var ctx = GetMockCtx(1))
            {
                var designDue = await ctx.SystemOptionDefinition.Where(o => o.Label.Equals("Default Time for Design Due")).FirstOrDefaultAsync();
                Assert.IsNotNull(designDue);
                var orderDue = await ctx.SystemOptionDefinition.Where(o => o.Label.Equals("Default Time for Order Due")).FirstOrDefaultAsync();
                Assert.IsNotNull(orderDue);
            }
        }

        [TestMethod]
        public async Task EFTestUpdateOptionsCategory()
        {
            using (var ctx = GetMockCtx(1))
            {
                var designDue = await ctx.SystemOptionCategory.Where(o => o.ID.Equals(401) && o.Name.Equals("Tag Setup")).FirstOrDefaultAsync();
                Assert.IsNotNull(designDue);
            }
        }

        [TestMethod]
        public async Task EFTestUpdateOptionsCategoryCustomField()
        {
            using (var ctx = GetMockCtx(1))
            {
                var customField = await ctx.SystemOptionCategory.Where(o => o.ID.Equals(203) && o.Name.Equals("Custom Fields")).FirstOrDefaultAsync();
                Assert.IsNotNull(customField);
            }
        }

        [TestMethod]
        public async Task EFTestInsertOptionsCategorySalesGoal()
        {
            using (var ctx = GetMockCtx(1))
            {
                var salesGoal = await ctx.SystemOptionCategory.Where(o => o.ID.Equals(69) && o.Name.Equals("Sales Goals")).FirstOrDefaultAsync();
                Assert.IsNotNull(salesGoal);
            }
        }

        [TestMethod]
        public async Task EFTestInsertOptionsCategoryPermissionGroup()
        {
            using (var ctx = GetMockCtx(1))
            {
                var permissionGroup = await ctx.SystemOptionCategory.Where(o => o.ID.Equals(106) && o.Name.Equals("Permission Groups")).FirstOrDefaultAsync();
                Assert.IsNotNull(permissionGroup);
            }
        }

        [TestMethod]
        public async Task EFUpdateSortIndexOfEmployeeFilter()
        {
            using (var ctx = GetMockCtx(1))
            {
                var emailFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(4) && o.TargetClassTypeID.Equals(5000)).FirstOrDefaultAsync();
                Assert.IsNotNull(emailFilter);

                var phoneNumberFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(1) && o.TargetClassTypeID.Equals(5000)).FirstOrDefaultAsync();
                Assert.IsNotNull(phoneNumberFilter);

                var locationFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(5) && o.TargetClassTypeID.Equals(5000)).FirstOrDefaultAsync();
                Assert.IsNotNull(locationFilter);

                var isActiveFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(3) && o.TargetClassTypeID.Equals(5000)).FirstOrDefaultAsync();
                Assert.IsNotNull(isActiveFilter);
            }
        }

        [TestMethod]
        [Ignore]
#warning END-8910 removed Units in ctx
        public void EFUpdateUnitEnumTableAndRecords()
        {
            using (var ctx = GetMockCtx(1))
            {
                //var inch = await ctx.EnumUnit.Where(x => x.ID.Equals(Unit.Inch) && x.UnitSystem.Equals(1)).FirstOrDefaultAsync();
                //Assert.IsNotNull(inch);

                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.SquareInch) && x.Name == "Square Inch"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.SquareCentimeter) && x.Name == "Square Centimeter"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.SquareFoot) && x.Name == "Square Foot"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.SquareMeter) && x.Name == "Square Meter"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.SquareMillimeter) && x.Name == "Square Millimeter"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.SquareYard) && x.Name == "Square Yard"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.CubicInch) && x.Name == "Cubic Inch"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.CubicCentimeter) && x.Name == "Cubic Centimeter"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.CubicFoot) && x.Name == "Cubic Foot"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.CubicMeter) && x.Name == "Cubic Meter"));
                //Assert.IsNotNull(ctx.EnumUnit.FirstOrDefault(x => x.ID.Equals(Unit.CubicMillimeter) && x.Name == "Cubic Millimeter"));
            }
        }

        [TestMethod]
        public async Task EFUpdateSortIndexOfAlertDefinitionFilter()
        {
            using (var ctx = GetMockCtx(1))
            {

                var employeeFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(72) && o.TargetClassTypeID.Equals(14100)).FirstOrDefaultAsync();
                Assert.IsNotNull(employeeFilter);

                var isActiveFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(71) && o.TargetClassTypeID.Equals(14100)).FirstOrDefaultAsync();
                Assert.IsNotNull(isActiveFilter);
            }
        }

        [TestMethod]
        public async Task EFUpdateSortIndexOfCompanyFilter()
        {
            using (var ctx = GetMockCtx(1))
            {

                var salesPersonFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(46) && o.TargetClassTypeID.Equals(2000)).FirstOrDefaultAsync();
                Assert.IsNotNull(salesPersonFilter);

                var locationFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(47) && o.TargetClassTypeID.Equals(2000)).FirstOrDefaultAsync();
                Assert.IsNotNull(locationFilter);

                var contactFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(48) && o.TargetClassTypeID.Equals(2000)).FirstOrDefaultAsync();
                Assert.IsNotNull(contactFilter);

                var isActiveFilter = await ctx.SystemListFilterCriteria.Where(o => o.ID.Equals(45) && o.TargetClassTypeID.Equals(2000)).FirstOrDefaultAsync();
                Assert.IsNotNull(isActiveFilter);
            }
        }

        [TestMethod]
        public async Task EFTestTimeCard()
        {
            using (var ctx = GetMockCtx(1))
            {
                var first = await ctx.TimeCard.Include("TimeCardDetails").FirstOrDefaultAsync();
            }
        }

        [TestMethod]
        public async Task EFTestTimeCardDetail()
        {
            using (var ctx = GetMockCtx(1))
            {
                var first = await ctx.TimeCardDetail.FirstOrDefaultAsync();
            }
        }

        [TestMethod]
        public async Task EFTestListTagColorField()
        {
            using (var ctx = GetMockCtx(1))
            {
                var existingTag = await ctx.ListTag.Where(o => o.ID.Equals(1)).FirstOrDefaultAsync();
                Assert.IsNotNull(existingTag);
                Assert.IsNotNull(existingTag.RGB);
                Assert.IsTrue(existingTag.RGB.Length > 0);
            }
        }

        [TestMethod]
        public async Task EFTestInsertTagsForTheDefaultDataSet()
        {
            using (var ctx = GetMockCtx(1))
            {
                var listTage1 = await ctx.ListTag.Where(o => o.ID.Equals(1) && o.Name.Equals("Rush")).FirstOrDefaultAsync();
                Assert.IsNotNull(listTage1);
#warning DB sometimes has "Installing" instead of "Install"
                var listTage2 = await ctx.ListTag.Where(o => o.ID.Equals(2) && o.Name.Equals("Install")).FirstOrDefaultAsync();
                Assert.IsNotNull(listTage2);

                var listTage3 = await ctx.ListTag.Where(o => o.ID.Equals(3) && o.Name.Equals("Shipped")).FirstOrDefaultAsync();
                Assert.IsNotNull(listTage3);
            }
        }

        [TestMethod]
        public async Task EFAddTaxabilityCodeSysOptCatRecord()
        {
            using (var ctx = GetMockCtx(1))
            {
                var taxCodeOpt = await ctx.SystemOptionCategory.Where(o => o.ID.Equals(24) && o.Name.Equals("Taxability Codes")).FirstOrDefaultAsync();
                Assert.IsNotNull(taxCodeOpt);
            }
        }

        [TestMethod]
        public async Task EnumMessageParticipantRoleTypeTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var enumRecords = await ctx.EnumMessageParticipantRoleType.ToListAsync();
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageParticipantRoleType.BCC));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageParticipantRoleType.CC));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageParticipantRoleType.From));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageParticipantRoleType.To));

                Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(enumRecords));
            }
        }

        [TestMethod]
        public async Task EnumMessageChannelTypeTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var enumRecords = await ctx.EnumMessageChannelType.ToListAsync();
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.None));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.Message));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.Alert));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.Email));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.SMS));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.Slack));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.WebHook));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.Zapier));
                Assert.IsTrue(enumRecords.Select(e => e.ID).Contains(MessageChannelType.SystemNotification));

                Console.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(enumRecords));
            }
        }

        [TestMethod]
        public async Task EFMessageHeaderTest()
        {
            var msgBody = new MessageBody()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.MessageBody,
                ModifiedDT = DateTime.UtcNow,
                Subject = "Subject",
                BodyFirstLine = "The quick brown fox ...",
                HasBody = true,
                AttachedFileCount = (byte)0,
                AttachedFileNames = null,
                HasAttachment = false,
                MetaData = null,
                WasModified = false,
                SizeInKB = 20
            };

            var participantInfo = new MessageParticipantInfo()
            {
                BID = 1,
                ID = -99,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageParticipantInfo,
                ParticipantRoleType = MessageParticipantRoleType.To,
                Channel = MessageChannelType.Email,
                UserName = "Test",
                IsMergeField = false,
                DeliveredDT = DateTime.Now,

                IsDelivered = false
            };

            var msgHeader = new MessageHeader
            {
                BID = 1,
                ID = -99,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageHeader,
                ReceivedDT = DateTime.Now,
                EmployeeID = -1,
                IsRead = false,
                ReadDT = DateTime.Now,
                IsDeleted = false,
                DeletedOrExpiredDT = DateTime.Now,
                IsExpired = false,
                Channels = MessageChannelType.Message,
                InSentFolder = false
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                try
                {
                    var testMsgHeader = await ctx.MessageHeader.Where(p => p.ID == -99).FirstOrDefaultAsync();
                    if (testMsgHeader != null)
                    {
                        ctx.Remove(testMsgHeader);
                        await ctx.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
                finally
                { }

                var testParticipantInfo = await ctx.MessageParticipantInfo.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testParticipantInfo != null)
                {
                    ctx.Remove(testParticipantInfo);
                    await ctx.SaveChangesAsync();
                }

                var testMessageBody = await ctx.MessageBody.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testMessageBody != null)
                {
                    ctx.Remove(testMessageBody);
                    await ctx.SaveChangesAsync();
                }

                try
                {

                    await ctx.MessageBody.AddAsync(msgBody);
                    await ctx.SaveChangesAsync();

                    var getMessageBody = await ctx.MessageBody.Where(d => d.BID == 1 && d.ID == msgBody.ID).FirstOrDefaultAsync();
                    Assert.IsNotNull(getMessageBody);
                    participantInfo.BodyID = getMessageBody.ID;

                    await ctx.MessageParticipantInfo.AddAsync(participantInfo);
                    await ctx.SaveChangesAsync();
                    var getParticipantInfo = await ctx.MessageParticipantInfo.Where(d => d.BID == 1 && d.ID == participantInfo.ID)
                        .FirstOrDefaultAsync();
                    Assert.IsNotNull(getParticipantInfo);

                    msgHeader.BodyID = getMessageBody.ID;
                    msgHeader.ParticipantID = getParticipantInfo.ID;
                    await ctx.MessageHeader.AddAsync(msgHeader);
                    await ctx.SaveChangesAsync();

                    //test msgHeaderWithBody
                    var msgHeaderWithBody = await ctx.MessageHeader
                                                    .Include(x => x.MessageBody)
                                                    .AsNoTracking()
                                                    .FirstOrDefaultAsync(x => x.ID == -99);
                    Console.WriteLine(JsonConvert.SerializeObject(new { msgHeaderWithBody = msgHeaderWithBody }, Newtonsoft.Json.Formatting.Indented));
                    Assert.IsNotNull(msgHeaderWithBody.MessageBody);

                    var get = await ctx.MessageHeader.Where(d => d.BID == 1 && d.ID == msgHeader.ID)
                        .FirstOrDefaultAsync();
                    Assert.IsNotNull(get);

                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
                finally
                {
                    ctx.MessageHeader.Remove(msgHeader);
                    ctx.MessageParticipantInfo.Remove(participantInfo);
                    ctx.MessageBody.Remove(msgBody);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        [TestMethod]
        public async Task EFMessageObjectLinkTest()
        {

            var msgBody = new MessageBody()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.MessageBody,
                ModifiedDT = DateTime.UtcNow,
                Subject = "Subject",
                BodyFirstLine = "The quick brown fox ...",
                HasBody = true,
                AttachedFileCount = (byte)0,
                AttachedFileNames = null,
                HasAttachment = false,
                MetaData = null,
                WasModified = false,
                SizeInKB = 20
            };

            var msgObjectLink = new MessageObjectLink
            {
                BID = 1,
                ID = -99,
                //                BodyID = 500,
                LinkedObjectClassTypeID = 500,
                LinkedObjectID = 1000,
                IsSourceOfMessage = false,
                DisplayText = "Display Text"
            };

            using (ApiContext ctx = GetMockCtx(1))
            {

                var testMessageBody = await ctx.MessageBody.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testMessageBody != null)
                {
                    ctx.Remove(testMessageBody);
                    await ctx.SaveChangesAsync();
                }

                var testMessageObjectLink = await ctx.MessageObjectLink.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testMessageObjectLink != null)
                {
                    ctx.Remove(testMessageObjectLink);
                    await ctx.SaveChangesAsync();
                }

                try
                {
                    await ctx.MessageBody.AddAsync(msgBody);
                    await ctx.SaveChangesAsync();

                    var getMessageBody = await ctx.MessageBody.Where(d => d.BID == 1 && d.ID == msgBody.ID).FirstOrDefaultAsync();
                    msgObjectLink.BodyID = getMessageBody.ID;

                    await ctx.MessageObjectLink.AddAsync(msgObjectLink);
                    await ctx.SaveChangesAsync();

                    //test msgBodyWithObjectLink
                    var msgBodyWithObjectLink = await ctx.MessageBody
                                                    .Include(x => x.MessageObjectLinks)
                                                    .FirstOrDefaultAsync(x => x.ID == -99);

                    Console.WriteLine(JsonConvert.SerializeObject(new { msgBodyWithObjectLink = msgBodyWithObjectLink }, Newtonsoft.Json.Formatting.Indented));
                    Assert.IsTrue(msgBodyWithObjectLink.MessageObjectLinks.Count > 0);


                    var get = await ctx.MessageObjectLink.Where(d => d.BID == 1 && d.BodyID == msgObjectLink.BodyID).FirstOrDefaultAsync();
                    Assert.IsNotNull(get);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
                finally
                {
                    ctx.MessageBody.Remove(msgBody);
                    ctx.MessageObjectLink.Remove(msgObjectLink);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        [TestMethod]
        public async Task EFMessageBodyTest()
        {

            var msgBody = new MessageBody()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.MessageBody,
                ModifiedDT = DateTime.UtcNow,
                Subject = "Subject",
                BodyFirstLine = "The quick brown fox ...",
                HasBody = true,
                AttachedFileCount = (byte)0,
                AttachedFileNames = null,
                HasAttachment = false,
                MetaData = null,
                WasModified = false,
                SizeInKB = 20
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                var part = await ctx.MessageParticipantInfo.FirstOrDefaultAsync(p => p.ID == -99);
                if (part != null)
                {
                    ctx.Remove(part);
                    ctx.SaveChanges();
                }

                //remove existing -99
                var testMessageBody = await ctx.MessageBody.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testMessageBody != null)
                {
                    ctx.Remove(testMessageBody);
                    await ctx.SaveChangesAsync();
                }

                try
                {
                    await ctx.MessageBody.AddAsync(msgBody);
                    await ctx.SaveChangesAsync();

                    var get = await ctx.MessageBody.Where(d => d.BID == 1 && d.ID == msgBody.ID).FirstOrDefaultAsync();
                    Assert.IsNotNull(get);
                }
                catch (Exception ex)
                {
                    Assert.Fail(ex.Message);
                }
                finally
                {
                    ctx.MessageBody.Remove(msgBody);
                    await ctx.SaveChangesAsync();
                }
            }
        }



        [TestMethod]
        public async Task EFMessageDeliveryRecordTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                //remove existing test objects
                var existingdelivery = await ctx.MessageDeliveryRecord.FirstOrDefaultAsync(d => d.ID == -99);
                if (existingdelivery != null)
                {
                    ctx.Remove(existingdelivery);
                }

                var existingparticipantinfo = await ctx.MessageParticipantInfo.FirstOrDefaultAsync(p => p.ID == -99);
                if (existingparticipantinfo != null)
                {
                    ctx.Remove(existingparticipantinfo);
                }

                var existingtestMsgBody = await ctx.MessageBody.FirstOrDefaultAsync(m => m.ID == -99);
                if (existingtestMsgBody != null)
                {
                    ctx.Remove(existingtestMsgBody);
                }

                await ctx.SaveChangesAsync();


                //insert test objects
                var testMsgBody = new MessageBody()
                {
                    BID = 1,
                    ID = -99
                };
                ctx.Add(testMsgBody);
                await ctx.SaveChangesAsync();

                var participant = new MessageParticipantInfo()
                {
                    BodyID = -99,
                    BID = 1,
                    ID = -99,
                    ParticipantRoleType = MessageParticipantRoleType.To
                };
                ctx.Add(participant);
                await ctx.SaveChangesAsync();

                var testDeliveryRecord = new MessageDeliveryRecord()
                {
                    ParticipantID = -99,
                    ID = -99,
                    BID = 1,
                    AttemptNumber = 1
                };

                MessageDeliveryRecord untrackedDeliveryRecord;
                Func<MessageDeliveryRecord> getUntrackedDeliveryRecord = () =>
                {
                    return (ctx.MessageDeliveryRecord
                                .AsNoTracking()//to make sure reference on `testDeliveryRecord` is different
                                .FirstOrDefault(d => d.ID == -99));
                };

                //test create and read
                ctx.Entry(testDeliveryRecord).Property(d => d.IsPending).IsModified = false;
                ctx.Add(testDeliveryRecord);
                await ctx.SaveChangesAsync();
                untrackedDeliveryRecord = getUntrackedDeliveryRecord();
                Assert.IsNotNull(untrackedDeliveryRecord);
                Console.WriteLine(JsonConvert.SerializeObject(new { CREATE = untrackedDeliveryRecord }, Newtonsoft.Json.Formatting.Indented));

                //test update   
                testDeliveryRecord.FailureMessage = "THOU SHALL UPDATE THY NEIGHBOR";
                await ctx.SaveChangesAsync();
                untrackedDeliveryRecord = getUntrackedDeliveryRecord();
                Assert.IsTrue(untrackedDeliveryRecord.FailureMessage.Equals("THOU SHALL UPDATE THY NEIGHBOR"));
                Console.WriteLine(JsonConvert.SerializeObject(new { UPDATE = untrackedDeliveryRecord }, Newtonsoft.Json.Formatting.Indented));

                //test participantWithDeliveryRecord
                var participantWithDeliveryRecord = await ctx.MessageParticipantInfo
                                                        .AsNoTracking()
                                                        .Include(x => x.MessageDeliveryRecords)
                                                        .FirstOrDefaultAsync(x => x.ID == -99);
                Console.WriteLine(JsonConvert.SerializeObject(new { participantWithDeliveryRecord = participantWithDeliveryRecord }, Newtonsoft.Json.Formatting.Indented));
                Assert.IsTrue(participantWithDeliveryRecord.MessageDeliveryRecords.Count > 0);


                //test delete
                ctx.Remove(testDeliveryRecord);
                await ctx.SaveChangesAsync();
                untrackedDeliveryRecord = getUntrackedDeliveryRecord();
                Assert.IsNull(untrackedDeliveryRecord);
                Console.WriteLine(JsonConvert.SerializeObject(new { DELETE = untrackedDeliveryRecord }, Newtonsoft.Json.Formatting.Indented));
            }
        }

        [TestMethod]
        public async Task EFMessageParticipantInfoTest()
        {
            var msgBody = new MessageBody()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.MessageBody,
                ModifiedDT = DateTime.UtcNow,
                Subject = "Subject",
                BodyFirstLine = "The quick brown fox ...",
                HasBody = true,
                AttachedFileCount = (byte)0,
                AttachedFileNames = null,
                HasAttachment = false,
                MetaData = null,
                WasModified = false,
                SizeInKB = 20
            };

            var participantInfo = new MessageParticipantInfo()
            {
                BID = 1,
                ID = -99,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageParticipantInfo,
                ParticipantRoleType = MessageParticipantRoleType.To,
                Channel = MessageChannelType.Email,
                UserName = "Test",
                IsMergeField = false,
                DeliveredDT = DateTime.Now,

                IsDelivered = false
            };

            var msgHeader = new MessageHeader()
            {
                BID = 1,
                ID = -99,
                ParticipantID = -99,
                BodyID = -99
            };

            var msgHeader2 = new MessageHeader()
            {
                BID = 1,
                ID = -98,
                ParticipantID = -99,
                BodyID = -99
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                var testParticipantInfo = await ctx.MessageParticipantInfo.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testParticipantInfo != null)
                {
                    ctx.Remove(testParticipantInfo);
                }

                var testMessageBody = await ctx.MessageBody.Where(p => p.ID == -99).FirstOrDefaultAsync();
                if (testMessageBody != null)
                {
                    ctx.Remove(testMessageBody);
                }

                var testMsgHeader = await ctx.MessageHeader.Where(e => e.ID == -99).FirstOrDefaultAsync();
                if (testMsgHeader != null)
                {
                    ctx.Remove(testMsgHeader);
                }

                var testMsgHeader2 = await ctx.MessageHeader.Where(e => e.ID == -98).FirstOrDefaultAsync();
                if (testMsgHeader2 != null)
                {
                    ctx.Remove(testMsgHeader2);
                }

                await ctx.SaveChangesAsync();
                Console.WriteLine("\n\n" + "existing test objects removed");

                try
                {

                    var testEmployee = await ctx.EmployeeData.FirstOrDefaultAsync(e => e.BID == 1);

                    msgHeader.EmployeeID = testEmployee.ID;
                    msgHeader2.EmployeeID = testEmployee.ID;

                    await ctx.MessageBody.AddAsync(msgBody);
                    await ctx.SaveChangesAsync();

                    var getMessageBody = await ctx.MessageBody.Where(d => d.BID == 1 && d.ID == msgBody.ID).FirstOrDefaultAsync();
                    participantInfo.BodyID = getMessageBody.ID;
                    participantInfo.EmployeeID = testEmployee.ID;



                    await ctx.MessageParticipantInfo.AddAsync(participantInfo);
                    await ctx.SaveChangesAsync();

                    await ctx.AddAsync(msgHeader);
                    await ctx.AddAsync(msgHeader2);
                    await ctx.SaveChangesAsync();

                    //test MessageBody.ParticipantInfos
                    var bodyWithParticipant = await ctx.MessageBody
                                                .Include(body => body.MessageParticipantInfos)
                                                .AsNoTracking()
                                                .FirstOrDefaultAsync(body => body.ID == -99);
                    Console.WriteLine("\n\n" + JsonConvert.SerializeObject(
                        new { bodyWithParticipant = bodyWithParticipant },
                        Newtonsoft.Json.Formatting.Indented,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        })
                    );
                    Assert.IsTrue(bodyWithParticipant.MessageParticipantInfos.Count > 0);


                    //test MessageHeaders nav prop-------------------------------------------------------------------------------
                    var participantInfoWithHeaders = await ctx.MessageParticipantInfo
                                                        .AsNoTracking()
                                                        .Include(mpi => mpi.MessageHeaders)
                                                        .FirstOrDefaultAsync(mpi => mpi.ID == -99);

                    Console.WriteLine("\n\n" + JsonConvert.SerializeObject(
                        new { participantInfoWithHeaders = participantInfoWithHeaders },
                        Newtonsoft.Json.Formatting.Indented,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        })
                    );
                    Assert.IsTrue(participantInfoWithHeaders.MessageHeaders.Count > 0);


                    //test EmployeeData nav prop-------------------------------------------------------------------------------------
                    var participantInfoWithEmployeeData = await ctx.MessageParticipantInfo
                                                        .AsNoTracking()
                                                        .Include(mpi => mpi.EmployeeData)
                                                        .FirstOrDefaultAsync(mpi => mpi.ID == -99);
                    Console.WriteLine("\n\n" + JsonConvert.SerializeObject(
                        new { participantInfoWithEmployeeData = participantInfoWithEmployeeData },
                        Newtonsoft.Json.Formatting.Indented,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        })
                    );
                    Assert.IsNotNull(participantInfoWithEmployeeData.EmployeeData);

                    var get = await ctx.MessageParticipantInfo.Where(d => d.BID == 1 && d.ID == participantInfo.ID)
                        .FirstOrDefaultAsync();
                    Assert.IsNotNull(get);

                }
                catch (Exception ex)
                {
                    Console.WriteLine("ex:" + ex.ToString());
                    Assert.Fail(ex.Message);
                }
                finally
                {
                    ctx.MessageBody.Remove(msgBody);

                    ctx.MessageParticipantInfo.Remove(participantInfo);
                    await ctx.SaveChangesAsync();
                }
            }
        }

        [TestMethod]
        public async Task EFNewSystemRightsGroupDataTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var SystemRightsGroupData = await ctx.RightsGroup.ToListAsync();
                Assert.IsTrue(SystemRightsGroupData.Count > 0);
            }
        }

        [TestMethod]
        public async Task EFNewSystemRightsGroupChildLinkDataTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var SystemRightsGroupChildLinkData = await ctx.RightsGroupChildGroupLink.ToListAsync();
                Assert.IsTrue(SystemRightsGroupChildLinkData.Count > 0);
            }
        }

        [TestMethod]
        public async Task EFNewSystemRightsGroupMenuTreeDataTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var SystemRightsGroupMenuTreeData = await ctx.RightsGroupMenuTree.ToListAsync();
                Assert.IsTrue(SystemRightsGroupMenuTreeData.Count > 0);
            }
        }

        [TestMethod]
        public async Task EFUserSecurityRightsStringTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                try
                {
                    var rowResult = await ctx.Database.ExecuteSqlRawAsync("SELECT dbo.[User.Security.Rights.String]( 1, 101 )");
                    Assert.IsNotNull(rowResult);
                }
                catch (Exception ex)
                {
                    Assert.AreEqual("Cannot change Billing contact for AdHoc company.", ex.Message);
                }
            }
        }

        [TestMethod]
        public async Task EFTransHeaderDataTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var company = await ctx.CompanyData.Where(t => t.BID == 1).FirstOrDefaultAsync();
                var location = await ctx.LocationData.Where(t => t.BID == 1).FirstOrDefaultAsync();
                var taxGroup = await ctx.TaxGroup.Where(t => t.BID == 1).FirstOrDefaultAsync();

                #region ESTIMATE

                var addEst = new EstimateData()
                {
                    BID = 1,
                    ID = -99,
                    FormattedNumber = "666",
                    TransactionType = (byte)OrderTransactionType.Estimate,
                    OrderStatusID = OrderOrderStatus.EstimateApproved,
                    CompanyID = company.ID,
                    LocationID = location.ID,
                    PickupLocationID = location.ID,
                    ProductionLocationID = location.ID,
                    TaxGroupID = taxGroup.ID,
                };

                await ctx.EstimateData.AddAsync(addEst);
                await ctx.SaveChangesAsync();

                var estimate = await ctx.EstimateData.Where(t => t.BID == 1 && t.ID == -99).FirstOrDefaultAsync();
                Assert.IsNotNull(estimate);

                ctx.EstimateData.Remove(estimate);
                await ctx.SaveChangesAsync();

                #endregion

                #region ORDER

                var addOrder = new OrderData()
                {
                    BID = 1,
                    ID = -99,
                    FormattedNumber = "666",
                    TransactionType = (byte)OrderTransactionType.Order,
                    OrderStatusID = OrderOrderStatus.OrderWIP,
                    CompanyID = company.ID,
                    LocationID = location.ID,
                    PickupLocationID = location.ID,
                    ProductionLocationID = location.ID,
                    TaxGroupID = taxGroup.ID,
                };

                await ctx.OrderData.AddAsync(addOrder);
                await ctx.SaveChangesAsync();

                var order = await ctx.OrderData.Where(t => t.BID == 1 && t.ID == -99).FirstOrDefaultAsync();
                Assert.IsNotNull(order);

                ctx.OrderData.Remove(order);
                await ctx.SaveChangesAsync();

                #endregion

                #region PO

                var addPO = new PurchaseOrderData()
                {
                    BID = 1,
                    ID = -99,
                    FormattedNumber = "666",
                    TransactionType = (byte)OrderTransactionType.PurchaseOrder,
                    OrderStatusID = OrderOrderStatus.POApproved,
                    CompanyID = company.ID,
                    LocationID = location.ID,
                    PickupLocationID = location.ID,
                    ProductionLocationID = location.ID,
                    TaxGroupID = taxGroup.ID,
                };

                await ctx.PurchaseOrderData.AddAsync(addPO);
                await ctx.SaveChangesAsync();

                var po = await ctx.PurchaseOrderData.Where(t => t.BID == 1 && t.ID == -99).FirstOrDefaultAsync();
                Assert.IsNotNull(po);

                ctx.PurchaseOrderData.Remove(po);
                await ctx.SaveChangesAsync();

                #endregion
            }
        }

        [TestMethod]
        public async Task EFUserDraftTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                var userData = await ctx.UserLink.FirstOrDefaultAsync();

                var userDraftData = new UserDraft()
                {
                    BID = 1,
                    ID = -99,
                    UserLinkID = (short)userData.AuthUserID,
                    ObjectCTID = (int)ClassType.Order
                };


                var newUserDraft = await ctx.UserDraft.AddAsync(userDraftData);

                Assert.IsNotNull(newUserDraft);
            }
        }

        [TestMethod]
        public void ActivityGLActivity()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.ActivityGlactivity.Include(e => e.Company).ToList());
                Assert.IsNotNull(ctx.ActivityGlactivity.Include(e => e.GL).ToList());
            }
        }

        [TestMethod]
        public void ActivityGLData()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.GLData.Include(e => e.Location).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.GLAccount).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.TaxGroup).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.GLActivity).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.Company).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.Order).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.OrderItem).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.OrderDestination).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.OrderItemComponent).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.Material).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.Labor).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.Machine).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.Assembly).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.TaxItem).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.PaymentMethod).ToList());
                Assert.IsNotNull(ctx.GLData.Include(e => e.Payment).ToList());
            }
        }

        [TestMethod]
        public void PaymentData()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.PaymentMaster.Include(e => e.Company).ToList());
                Assert.IsNotNull(ctx.PaymentApplication.Include(e => e.Company).ToList());
            }
        }


        [TestMethod]
        public async Task EFTaxExemptReasonNavigationProperties()
        {
            const int BID = 1;
            using (var ctx = GetMockCtx(BID))
            {
                bool added = false;
                var reason = await ctx.FlatListItem.Where(x => x.BID == BID).FirstOrDefaultAsync(x => x.FlatListType == FlatListType.TaxExemptReasons);
                if (reason == null)
                {
                    short itemID = (short)((await ctx.FlatListItem.MaxAsync(x => x.ID)) + BID);
                    var newItem = GetNewTaxExemptReasonFlatListItem(itemID);
                    ctx.FlatListItem.Add(newItem);
                    ctx.SaveChanges();
                    reason = newItem;
                    added = true;
                }

                var company = await ctx.CompanyData.Where(x => x.BID == BID && !x.TaxExemptReasonID.HasValue).FirstOrDefaultAsync();
                Assert.IsNotNull(company, "no company in dataset, this test needs to create one");
                var trans = await ctx.TransactionHeaderData.Where(x => x.BID == BID && !x.TaxExemptReasonID.HasValue).FirstOrDefaultAsync();
                Assert.IsNotNull(trans, "no transheader in dataset, this test needs to create one");
                var orderitem = await ctx.OrderItemData.Where(x => x.BID == BID && !x.TaxExemptReasonID.HasValue).FirstOrDefaultAsync();
                Assert.IsNotNull(orderitem, "no orderitem in dataset, this test needs to create one");
                var orderdest = await ctx.OrderDestinationData.Where(x => x.BID == BID && !x.TaxExemptReasonID.HasValue).FirstOrDefaultAsync();
                Assert.IsNotNull(orderdest, "no orderdestination in dataset, this test needs to create one");

                company.TaxExemptReasonID = reason.ID;
                trans.TaxExemptReasonID = reason.ID;
                orderitem.TaxExemptReasonID = reason.ID;
                orderdest.TaxExemptReasonID = reason.ID;

                Assert.IsNotNull(ctx.CompanyData.Where(x => x.BID == BID && x.ID == company.ID).Include(x => x.TaxExemptReason).FirstOrDefaultAsync(),
                    "include on company.taxexemptReason did not work");
                Assert.IsNotNull(ctx.TransactionHeaderData.Where(x => x.BID == BID && x.ID == trans.ID).Include(x => x.TaxExemptReason).FirstOrDefaultAsync(),
                    "include on transactionheader.taxexemptReason did not work");
                Assert.IsNotNull(ctx.OrderItemData.Where(x => x.BID == BID && x.ID == orderitem.ID).Include(x => x.TaxExemptReason).FirstOrDefaultAsync(),
                    "include on orderitem.taxexemptReason did not work");
                Assert.IsNotNull(ctx.OrderDestinationData.Where(x => x.BID == BID && x.ID == orderdest.ID).Include(x => x.TaxExemptReason).FirstOrDefaultAsync(),
                    "include on orderdestination.taxexemptReason did not work");

                company.TaxExemptReasonID = null;
                trans.TaxExemptReasonID = null;
                orderitem.TaxExemptReasonID = null;
                orderdest.TaxExemptReasonID = null;

                if (added)
                    ctx.FlatListItem.Remove(reason);

                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public async Task EFQuickItemTest()
        {
            const int BID = 1;
            var quickItem = new QuickItemData()
            {
                BID = BID,
                ID = -99,
                Name = "Test.QuickItem.Data",
                IsActive = false,
                ModifiedDT = DateTime.UtcNow,
                ClassTypeID = ClassType.QuickItem.ID()
            };

            using (var ctx = GetMockCtx(BID))
            {
                // create
                await ctx.QuickItemData.AddAsync(quickItem);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // retrieve
                var getQuickItem = await ctx.QuickItemData.FirstOrDefaultAsync(q => q.ID == quickItem.ID && q.BID == quickItem.BID);
                Assert.IsNotNull(getQuickItem);
                // update
                getQuickItem.IsActive = true;
                ctx.QuickItemData.Update(getQuickItem);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // delete
                ctx.QuickItemData.Remove(getQuickItem);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
            }

        }


        [TestMethod]
        public async Task EFSurchargeTest()
        {
            const int BID = 1;

            using (var ctx = GetMockCtx(BID))
            {
                var surchargeDef = new SurchargeDef()
                {
                    BID = BID,
                    ID = -99,
                    Name = "Test.SurchargeDef",
                    IsActive = false,
                    ModifiedDT = DateTime.UtcNow,
                    ClassTypeID = ClassType.SurchargeDef.ID(),
                    TaxCode = ctx.TaxabilityCodes.FirstOrDefault(b => b.BID == BID),
                    IncomeAccount = ctx.GLAccount.Where(t => t.IsIncome.HasValue && t.IsIncome.Value == true).FirstOrDefault()
                };
                Assert.IsNotNull(surchargeDef.IncomeAccount);


                // create
                await ctx.SurchargeDef.AddAsync(surchargeDef);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // retrieve
                var getSurcharge = await ctx.SurchargeDef.FirstOrDefaultAsync(q => q.ID == surchargeDef.ID && q.BID == surchargeDef.BID);
                Assert.IsNotNull(getSurcharge);
                // update
                getSurcharge.IsActive = true;
                ctx.SurchargeDef.Update(getSurcharge);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // delete
                ctx.SurchargeDef.Remove(getSurcharge);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
            }

        }

        [TestMethod]
        public async Task EFOrderItemSurchargeTest()
        {
            const int BID = 1;

            using (var ctx = GetMockCtx(BID))
            {
                var existingEntries = ctx.OrderItemSurcharge.Where(ois => ois.ID < 0);
                ctx.RemoveRange(existingEntries);
                await ctx.SaveChangesAsync();

                var surcharge = new SurchargeDef()
                {
                    BID = BID,
                    ID = -99,
                    Name = "Test.SurchargeDef",
                    IsActive = false,
                    ModifiedDT = DateTime.UtcNow,
                    ClassTypeID = ClassType.SurchargeDef.ID(),
                    TaxCode = ctx.TaxabilityCodes.FirstOrDefault(b => b.BID == BID),
                    IncomeAccount = ctx.GLAccount.FirstOrDefault(a => a.IsIncome == true && a.BID == BID)
                };

                if (!ctx.SurchargeDef.Any(s => s.ID == -99 && s.BID == BID))
                {
                    await ctx.SurchargeDef.AddAsync(surcharge);
                    Assert.AreEqual(1, await ctx.SaveChangesAsync());
                }
                else
                {
                    surcharge = ctx.SurchargeDef.Where(s => s.ID == -99).First();
                }

                var oi = new OrderItemData()
                {
                    BID = BID,
                    ID = -99,
                    Name = "Test.SurchargeDef",
                    TransactionType = 2,
                    OrderID = ctx.OrderData.FirstOrDefault(b => b.BID == BID).ID,
                    ItemStatusID = ctx.OrderItemStatus.FirstOrDefault(b => b.BID == BID).ID,
                    ProductionLocationID = ctx.LocationData.FirstOrDefault(b => b.BID == BID).ID,
                    TaxGroupID = ctx.TaxGroup.FirstOrDefault(b => b.BID == BID).ID,
                    OrderStatusID = ctx.EnumOrderOrderStatus.Where(os => os.TransactionType == 2).FirstOrDefault().ID
                };

                if (!ctx.OrderItemData.Any(i => i.ID == -99))
                {
                    await ctx.OrderItemData.AddAsync(oi);
                    Assert.AreEqual(1, await ctx.SaveChangesAsync());
                }
                else
                {
                    oi = ctx.OrderItemData.Where(s => s.ID == -99).First();
                }

                var surchargeDef = new OrderItemSurcharge()
                {
                    BID = BID,
                    ID = -99,
                    Name = "Test.SurchargeDef",
                    TaxCode = ctx.TaxabilityCodes.FirstOrDefault(b => b.BID == BID),
                    IncomeAccountID = ctx.GLAccount.FirstOrDefault(b => b.BID == BID).ID,
                    OrderItemID = ctx.OrderItemData.FirstOrDefault(b => b.BID == BID).ID,
                    SurchargeDefID = ctx.SurchargeDef.FirstOrDefault(b => b.BID == BID).ID
                };

                // create
                await ctx.OrderItemSurcharge.AddAsync(surchargeDef);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // retrieve
                var getSurcharge = await ctx.OrderItemSurcharge.FirstOrDefaultAsync(q => q.ID == surchargeDef.ID && q.BID == surchargeDef.BID);
                Assert.IsNotNull(getSurcharge);
                // delete
                ctx.OrderItemSurcharge.Remove(getSurcharge);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                ctx.SurchargeDef.Remove(surcharge);
                ctx.OrderItemData.Remove(oi);
                Assert.AreEqual(2, await ctx.SaveChangesAsync());
            }

        }

        [TestMethod]
        public async Task EFURLRegistrationTest()
        {
            const int BID = 1;

            using (var ctx = GetMockCtx(BID))
            {
                var existingEntries = ctx.URLRegistrationLookupHistory.Where(url => url.ID < 0);
                ctx.RemoveRange(existingEntries);
                await ctx.SaveChangesAsync();

                var existingEntries2 = ctx.URLRegistrationData.Where(url => url.ID < 0);
                ctx.RemoveRange(existingEntries2);
                await ctx.SaveChangesAsync();

                var urlRegistrationData = new URLRegistrationData()
                {
                    BID = BID,
                    ID = -99,
                    IsActive = false,
                    RegistrationType = URLRegistrationType.Tracking,
                    PublishedURLRoot = ""
                };

                var urlRegistrationLookupHistory = new URLRegistrationLookupHistory()
                {
                    BID = BID,
                    ID = -99
                };



                // create
                await ctx.URLRegistrationData.AddAsync(urlRegistrationData);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                urlRegistrationLookupHistory.RegistrationID = urlRegistrationData.ID;
                await ctx.URLRegistrationLookupHistory.AddAsync(urlRegistrationLookupHistory);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

                // retrieve
                var getURLRegistrationData = await ctx.URLRegistrationData.FirstOrDefaultAsync(q => q.ID == urlRegistrationData.ID && q.BID == BID);
                Assert.IsNotNull(getURLRegistrationData);
                // retrieve
                var getURLRegistrationDataHistory = await ctx.URLRegistrationLookupHistory.FirstOrDefaultAsync(q => q.ID == urlRegistrationLookupHistory.ID && q.BID == BID);
                Assert.IsNotNull(getURLRegistrationDataHistory);
                // delete
                ctx.URLRegistrationLookupHistory.Remove(getURLRegistrationDataHistory);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                ctx.URLRegistrationData.Remove(getURLRegistrationData);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());

            }

        }

        [TestMethod]
        public async Task EFDMAccessTokenTest()
        {
            const int BID = 1;
            using (var ctx = GetMockCtx(BID))
            {
                var dMAccessToken = new DMAccessToken()
                {
                    BID = BID,
                    ID = Guid.NewGuid(),
                    URL = "Test",
                    SASToken = "Test",
                    ExpirationDate = DateTime.UtcNow

                };

                // create
                await ctx.DMAccessToken.AddAsync(dMAccessToken);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // retrieve
                var getAssemblyTable = await ctx.DMAccessToken.FirstOrDefaultAsync(q => q.ID == dMAccessToken.ID && q.BID == dMAccessToken.BID);
                Assert.IsNotNull(dMAccessToken);
                // delete
                ctx.DMAccessToken.Remove(dMAccessToken);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
            }

        }

        [TestMethod]
        public async Task EmailAccountDataTest()
        {
            const int BID = 1;
            using (var ctx = GetMockCtx(BID))
            {
                var domainEmail = await ctx.SimpleDomainEmail.FirstOrDefaultAsync();
                var emailAccount = new EmailAccountData()
                {
                    BID = BID,
                    UserName = "test",
                    DomainEmailID = domainEmail.ID,
                    DomainName = "test.com",
                    Credentials = "test",
                    StatusType = EmailAccountStatus.Pending

                };

                // create
                await ctx.EmailAccountData.AddAsync(emailAccount);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
                // retrieve
                var getEmailAccount = await ctx.EmailAccountData.FirstOrDefaultAsync(q => q.ID == emailAccount.ID && q.BID == emailAccount.BID);
                Assert.IsNotNull(getEmailAccount);
                var getSimpleEmailAccount = await ctx.SimpleEmailAccountData.FirstOrDefaultAsync();
                Assert.IsNotNull(getSimpleEmailAccount);
                // delete
                ctx.EmailAccountData.Remove(emailAccount);
                Assert.AreEqual(1, await ctx.SaveChangesAsync());
            }

        }

        [TestMethod]
        public async Task EmailAccountTeamLinkTest()
        {
            const short testBID = 1;
            const short domainEmailID = -99;
            const short emailAccountID = -99;
            const int teamID = -99;

            using (var ctx = GetMockCtx(testBID))
            {
                #region Test Data Objects

                var domainEmail = new DomainEmail()
                {
                    BID = testBID,
                    ID = domainEmailID,
                    DomainName = "domain.test.stuff",
                };
                var emailAccount = new EmailAccountData()
                {
                    BID = testBID,
                    ID = emailAccountID,
                    DomainName = "domain.test.stuff",
                    DomainEmailID = domainEmailID,
                    DisplayName = "TEST EMAIL ACCOUNT",
                    UserName = "TEST USER NAME",
                    StatusType = EmailAccountStatus.Inactive
                };
                var team = new EmployeeTeam()
                {
                    BID = testBID,
                    ID = teamID,
                    Name = "TEST TEAM"
                };
                var emailAccountTeamLink = new EmailAccountTeamLink()
                {
                    BID = testBID,
                    EmailAccountID = emailAccountID,
                    TeamID = teamID
                };

                #endregion

                #region Clean-up Existing

                var existingEmailTeamLink = await ctx.EmailAccountTeamLink.FirstOrDefaultAsync(x => x.BID == testBID && x.EmailAccountID == emailAccountID && x.TeamID == teamID);
                if (existingEmailTeamLink != null)
                {
                    ctx.Remove(existingEmailTeamLink);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }
                var existingEmailAccount = await ctx.EmailAccountData.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == emailAccountID);
                if (existingEmailAccount != null)
                {
                    ctx.Remove(existingEmailAccount);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }
                var existingTeam = await ctx.EmployeeTeam.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == teamID);
                if (existingTeam != null)
                {
                    ctx.Remove(existingTeam);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }
                var existingDomainEmail = await ctx.DomainEmail.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == domainEmailID);
                if (existingDomainEmail != null)
                {
                    ctx.Remove(existingDomainEmail);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }

                #endregion

                ctx.DomainEmail.Add(domainEmail);
                ctx.EmailAccountData.Add(emailAccount);
                ctx.EmployeeTeam.Add(team);
                ctx.EmailAccountTeamLink.Add(emailAccountTeamLink);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var dbEmailAccount = await ctx.EmailAccountData.Include(x => x.EmailAccountTeamLinks)
                    .FirstOrDefaultAsync(x => x.BID == testBID && x.ID == emailAccountID);
                Assert.IsNotNull(dbEmailAccount);
                Assert.IsNotNull(dbEmailAccount.EmailAccountTeamLinks);
                Assert.IsTrue(dbEmailAccount.EmailAccountTeamLinks.Count > 0);

                var dbTeam = await ctx.EmailAccountData.Include(x => x.EmailAccountTeamLinks)
                    .FirstOrDefaultAsync(x => x.BID == testBID && x.ID == teamID);
                Assert.IsNotNull(dbTeam);
                Assert.IsNotNull(dbTeam.EmailAccountTeamLinks);
                Assert.IsTrue(dbTeam.EmailAccountTeamLinks.Count > 0);

                #region Final Clean-up

                var cleanupEmailTeamLink = await ctx.EmailAccountTeamLink.FirstOrDefaultAsync(x => x.BID == testBID && x.EmailAccountID == emailAccountID && x.TeamID == teamID);
                if (cleanupEmailTeamLink != null)
                {
                    ctx.Remove(cleanupEmailTeamLink);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }
                var cleanupEmailAccount = await ctx.EmailAccountData.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == emailAccountID);
                if (cleanupEmailAccount != null)
                {
                    ctx.Remove(cleanupEmailAccount);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }
                var cleanupTeam = await ctx.EmployeeTeam.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == teamID);
                if (cleanupTeam != null)
                {
                    ctx.Remove(cleanupTeam);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }
                var cleanupDomainEmail = await ctx.DomainEmail.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == domainEmailID);
                if (cleanupDomainEmail != null)
                {
                    ctx.Remove(cleanupDomainEmail);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
                }

                #endregion
            }
        }

        /// <summary>
        /// Tests the option definition and all related user options are deleted
        /// </summary>
        [TestMethod]
        public async Task EFTestOptionDefinitionAreDeleted()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                int[] IDs = new int[] { 20003, 20004, 20005, 20006, 20007 };
                Assert.IsNull(await ctx.OptionData.FirstOrDefaultAsync(r => IDs.Contains(r.ID)));
                Assert.IsNull(await ctx.SystemOptionDefinition.FirstOrDefaultAsync(r => IDs.Contains(r.ID)));
            }
        }

        [TestMethod]
        public async Task TestCompanyPaymentsPropertyInclude()
        {
            try
            {
                using (ApiContext ctx = GetMockCtx(1))
                {
                    await ctx.CompanyData.Include(c => c.Payments).FirstOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public async Task EnumDashboardWidgetCategoryType()
        {
            using (var ctx = GetMockCtx(1))
            {
                var enums = await ctx.EnumDashboardWidgetCategoryType.OrderBy(t => t.ID).ToListAsync();
                var enumNames = Enum.GetNames(typeof(DashboardWidgetCategoryType));
                var enumValues = (DashboardWidgetCategoryType[])Enum.GetValues(typeof(DashboardWidgetCategoryType));

                Assert.AreEqual(enums.Count, enumNames.Length);
                for (int i = 0; i < enums.Count(); i++)
                {
                    Assert.AreEqual(enums[i].ID, enumValues[i]);
                    Assert.AreEqual(enums[i].Name.Replace(" ", ""), enumNames[i]);
                }
            }
        }

        [TestMethod]
        public async Task EFDashboardWidgetCategoryLink()
        {
            var definitionData = new DashboardWidgetDefinition()
            {
                BID = 1,
                ID = -99,
                ClassTypeID = (int)ClassType.DashboardWidgetDefinition,
                ModifiedDT = DateTime.UtcNow,
                DefaultName = "Test",
                DefaultCols = (byte)1,
                DefaultRows = (byte)1,
                DefaultOptions = "",
                MinRows = (byte)1,
                MaxRows = (byte)1,
                MaxCols = (byte)1,
                MinCols = (byte)1,
                HasImage = false,
                Description = "Test Board Definition",
            };

            var widgetLink = new DashboardWidgetCategoryLink()
            {
                WidgetDefID = -99,
                CategoryType = DashboardWidgetCategoryType.Accounting,
            };

            using (ApiContext ctx = GetMockCtx(1))
            {
                ctx.Set<DashboardWidgetDefinition>().Add(definitionData);
                await ctx.SaveChangesAsync();
                var entity = await ctx.Set<DashboardWidgetDefinition>().FirstOrDefaultAsync(x => x.ID == definitionData.ID);
                Assert.IsNotNull(entity);

                ctx.Set<DashboardWidgetCategoryLink>().Add(widgetLink);
                await ctx.SaveChangesAsync();
                var linkEntity = await ctx.Set<DashboardWidgetCategoryLink>().FirstOrDefaultAsync(x => x.WidgetDefID == definitionData.ID && x.CategoryType == widgetLink.CategoryType);
                Assert.IsNotNull(linkEntity);

                ctx.Set<DashboardWidgetCategoryLink>().Remove(linkEntity);
                ctx.Set<DashboardWidgetDefinition>().Remove(entity);

                await ctx.SaveChangesAsync();
            }
        }

        /// <summary>
        /// Tests Pending is inserted in [enum.order.KeyDateType] table,
        /// </summary>
        [TestMethod]
        public async Task EFTestKeyDateTypePendingIsInserted()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.EnumOrderKeyDateType.Where(k => k.ID == OrderKeyDateType.Pending).FirstOrDefaultAsync());
            }
        }


        /// <summary>
        /// Tests Followup is inserted in [enum.KeyDateType] table,
        /// </summary>
        [TestMethod]
        public async Task EFTestKeyDateTypeFollowupIsInserted()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.EnumOrderKeyDateType.Where(k => k.ID == OrderKeyDateType.Pending).FirstOrDefaultAsync());
            }
        }

        [TestMethod]
        public async Task MachineInstanceBasicTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.MachineInstance.ToListAsync());
            }
        }

        [TestMethod]
        public async Task MachineProfileBasicTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.MachineProfile.ToListAsync());
            }
        }

        [TestMethod]
        public async Task MachineProfileTableBasicTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.MachineProfileTable.ToListAsync());
            }
        }

        [TestMethod]
        public async Task MachineProfileVariableBasicTest()
        {
            const short TestBID = 1;
            const short TestID = -100;


            using (ApiContext ctx = GetMockCtx(1))
            {
                var profiles = ctx.MachineProfile.Where(x => x.BID == TestBID && x.MachineID == TestID).ToArray();
                var profileIDs = profiles.Select(x => x.ID).ToArray();
                ctx.RemoveRange(ctx.MachineProfileTable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(ctx.MachineProfileVariable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(profiles);
                ctx.RemoveRange(ctx.MachineData.Where(x => x.BID == TestBID && x.ID == TestID));
                ctx.SaveChanges();
            }
            using (ApiContext ctx = GetMockCtx(1))
            {
                int testAssemblyID = ctx.AssemblyData.FirstOrDefault(e => e.BID == TestBID).ID;
                int testVariableID = ctx.AssemblyVariable.FirstOrDefault(e => e.BID == TestBID && e.AssemblyID == testAssemblyID).ID;

                Assert.IsNotNull(ctx.MachineData.ToList());

                MachineData machineData = new MachineData()
                {
                    BID = TestBID,
                    ID = TestID,
                    Name = "SavingMachineDataSucceeds",
                    Profiles = new List<MachineProfile>
                    {
                        new MachineProfile()
                        {
                            ID = TestID,
                            IsActive = true,
                            IsDefault = true,
                            Name = "Parent Profile",
                            MachineID = TestID,
                            MachineTemplateID = testAssemblyID,
                            AssemblyOVDataJSON = "{}",
                            BID = TestBID
                        }
                    }
                };

                ctx.MachineData.Add(machineData);
                Assert.AreEqual(2, ctx.SaveChanges());

                MachineProfileVariable machineProfileVariable = new MachineProfileVariable()
                {
                    BID = TestBID,
                    ID = TestID,
                    ProfileID = TestID,
                    VariableID = testVariableID,
                    AssemblyID = testAssemblyID,

                };

                ctx.MachineProfileVariable.Add(machineProfileVariable);
                Assert.AreEqual(1, ctx.SaveChanges());

                Assert.IsNotNull(await ctx.MachineProfile.ToListAsync());
            }

            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.MachineProfileVariable.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
                Assert.IsNotNull(ctx.MachineData.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
                Assert.IsNotNull(ctx.MachineProfile.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
            }

            using (ApiContext ctx = GetMockCtx(1))
            {
                var profiles = ctx.MachineProfile.Where(x => x.BID == TestBID && x.MachineID == TestID).ToArray();
                var profileIDs = profiles.Select(x => x.ID).ToArray();
                ctx.RemoveRange(ctx.MachineProfileTable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(ctx.MachineProfileVariable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(profiles);
                ctx.RemoveRange(ctx.MachineData.Where(x => x.BID == TestBID && x.ID == TestID));
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public async Task DestinationProfileBasicTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.DestinationProfile.ToListAsync());
            }
        }

        [TestMethod]
        public async Task DestinationProfileTableBasicTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.DestinationProfileTable.ToListAsync());
            }
        }

        [TestMethod]
        public async Task DestinationProfileVariableBasicTest()
        {
            const short TestBID = 1;
            const short TestID = -100;


            using (ApiContext ctx = GetMockCtx(1))
            {
                var profiles = ctx.DestinationProfile.Where(x => x.BID == TestBID && x.DestinationID == TestID).ToArray();
                var profileIDs = profiles.Select(x => x.ID).ToArray();
                ctx.RemoveRange(ctx.DestinationProfileTable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(ctx.DestinationProfileVariable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(profiles);
                ctx.RemoveRange(ctx.DestinationData.Where(x => x.BID == TestBID && x.ID == TestID));
                ctx.SaveChanges();
            }
            using (ApiContext ctx = GetMockCtx(1))
            {
                int testAssemblyID = ctx.AssemblyData.FirstOrDefault(e => e.BID == TestBID).ID;
                int testVariableID = ctx.AssemblyVariable.FirstOrDefault(e => e.BID == TestBID && e.AssemblyID == testAssemblyID).ID;

                Assert.IsNotNull(ctx.DestinationData.ToList());

                DestinationData destinationData = new DestinationData()
                {
                    BID = TestBID,
                    ID = TestID,
                    Name = "SavingMachineDataSucceeds",
                    Profiles = new List<DestinationProfile>
                    {
                        new DestinationProfile()
                        {
                            ID = TestID,
                            IsActive = true,
                            IsDefault = true,
                            Name = "Parent Profile",
                            DestinationID = TestID,
                            TemplateID = testAssemblyID,
                            AssemblyOVDataJSON = "{}",
                            BID = TestBID
                        }
                    }
                };

                ctx.DestinationData.Add(destinationData);
                Assert.AreEqual(2, ctx.SaveChanges());

                DestinationProfileVariable destinationProfileVariable = new DestinationProfileVariable()
                {
                    BID = TestBID,
                    ID = TestID,
                    ProfileID = TestID,
                    VariableID = testVariableID,
                    AssemblyID = testAssemblyID,

                };

                ctx.DestinationProfileVariable.Add(destinationProfileVariable);
                Assert.AreEqual(1, ctx.SaveChanges());

                Assert.IsNotNull(await ctx.MachineProfile.ToListAsync());
            }

            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.DestinationProfileVariable.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
                Assert.IsNotNull(ctx.DestinationData.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
                Assert.IsNotNull(ctx.DestinationProfile.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
            }

            using (ApiContext ctx = GetMockCtx(1))
            {
                var profiles = ctx.DestinationProfile.Where(x => x.BID == TestBID && x.ID == TestID).ToArray();
                var profileIDs = profiles.Select(x => x.ID).ToArray();
                ctx.RemoveRange(ctx.DestinationProfileTable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(ctx.DestinationProfileVariable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(profiles);
                ctx.RemoveRange(ctx.DestinationData.Where(x => x.BID == TestBID && x.ID == TestID));
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void SavingMachineDataSucceeds()
        {
            const string json = "json";
            const short TestBID = 1;
            const short TestID = -100;
            using (ApiContext ctx = GetMockCtx(1))
            {
                var profiles = ctx.MachineProfile.Where(x => x.BID == TestBID && x.MachineID == TestID).ToArray();
                var profileIDs = profiles.Select(x => x.ID).ToArray();
                ctx.RemoveRange(ctx.MachineProfileTable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(profiles);
                ctx.RemoveRange(ctx.MachineData.Where(x => x.BID == TestBID && x.ID == TestID));
                ctx.SaveChanges();
            }
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsTrue(ctx.MachineData.Any());

                MachineData machineData = new MachineData()
                {
                    BID = TestBID,
                    ID = TestID,
                    Name = "SavingMachineDataSucceeds",
                    AssemblyOVDataJSON = json
                };

                ctx.MachineData.Add(machineData);
                Assert.AreEqual(1, ctx.SaveChanges());

                var untrackedMachine = ctx.MachineData
                                        .AsNoTracking()
                                        .FirstOrDefault(x => x.BID == TestBID && x.ID == TestID);
                Assert.AreEqual(json, untrackedMachine.AssemblyOVDataJSON, "untrackedMachine.AssemblyOVDataJSON");

            }

            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(ctx.MachineData.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
            }

            using (ApiContext ctx = GetMockCtx(1))
            {
                var profiles = ctx.MachineProfile.Where(x => x.BID == TestBID && x.MachineID == TestID).ToArray();
                var profileIDs = profiles.Select(x => x.ID).ToArray();
                ctx.RemoveRange(ctx.MachineProfileTable.Where(x => x.BID == TestBID && profileIDs.Contains(x.ProfileID)));
                ctx.RemoveRange(profiles);
                ctx.RemoveRange(ctx.MachineData.Where(x => x.BID == TestBID && x.ID == TestID));
                ctx.SaveChanges();
            }
        }

        [TestMethod]
        public void SavingMachineProfileDataSucceeds()
        {

            const short TestBID = 1;
            const short TestID = -100;

            var task = Task.Run(() =>
            {
                using (ApiContext ctx = GetMockCtx(1))
                {
                    ctx.Database.ExecuteSqlRawAsync($"Delete from [Part.Machine.Profile.Table] where bid = {TestBID} and ID = {TestID}");
                    ctx.Database.ExecuteSqlRawAsync($"Delete from [Part.Machine.Profile] where bid = {TestBID} and ID = {TestID}");
                    ctx.Database.ExecuteSqlRawAsync($"Delete from [Part.Machine.Data] where bid = {TestBID} and ID = {TestID}");
                }
                using (ApiContext ctx = GetMockCtx(1))
                {
                    Assert.IsNotNull(ctx.MachineData.ToList());
                }
                using (ApiContext ctx = GetMockCtx(1))
                {
                    int testAssemblyID = ctx.AssemblyData.FirstOrDefault(e => e.BID == TestBID).ID;
                    short testAssemblyTableID = ctx.AssemblyTable.FirstOrDefault(e => e.BID == TestBID).ID;

                    MachineData machineData = new MachineData()
                    {
                        BID = TestBID,
                        ID = TestID,
                        Name = "SavingMachineDataSucceeds",
                        Profiles = new List<MachineProfile>
                    {
                        new MachineProfile()
                        {
                            ID = TestID,
                            IsActive = true,
                            IsDefault = true,
                            Name = "Parent Profile",
                            MachineID = TestID,
                            MachineTemplateID = testAssemblyID,
                            AssemblyOVDataJSON = "{}",
                            BID = TestBID,

                            MachineProfileTables = new List<MachineProfileTable>
                            {
                                new MachineProfileTable
                                {
                                    ID = TestID,
                                    BID = TestBID,
                                    ProfileID = TestID,
                                    TableID = testAssemblyTableID,
                                    Label = "Assembly table 1",
                                    OverrideDefault = false
                                }
                            }
                        }
                    }
                    };

                    ctx.MachineData.Add(machineData);
                    Assert.AreEqual(3, ctx.SaveChanges());
                }
            });
            task.ContinueWith((x) =>
            {
                using (ApiContext ctx = GetMockCtx(1))
                {
                    Assert.IsNotNull(ctx.MachineData.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
                    Assert.IsNotNull(ctx.MachineProfile.FirstOrDefault(t => t.BID == TestBID && t.ID == TestID));
                    Assert.IsNotNull(ctx.MachineProfileTable.FirstOrDefault(t => t.BID == TestBID && t.ID == (short)TestID));
                }

                using (ApiContext ctx = GetMockCtx(1))
                {
                    ctx.Database.ExecuteSqlRawAsync($"Delete from [Part.Machine.Profile.Table] where bid = {TestBID} and ID = {TestID}");
                    ctx.Database.ExecuteSqlRawAsync($"Delete from [Part.Machine.Profile] where bid = {TestBID} and ID = {TestID}");
                    ctx.Database.ExecuteSqlRawAsync($"Delete from [Part.Machine.Data] where bid = {TestBID} and ID = {TestID}");
                }
            });
        }

        [TestMethod]
        public void EFInsertEnumEmbeddedAssemblyTypeRecords()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                List<EnumEmbeddedAssemblyType> enums = ctx.EnumEmbeddedAssemblyType.OrderBy(t => t.ID).ToList();

                String[] enumNames = Enum.GetNames(typeof(EmbeddedAssemblyType));
                EmbeddedAssemblyType[] enumValues = (EmbeddedAssemblyType[])Enum.GetValues(typeof(EmbeddedAssemblyType));

                Assert.AreEqual(enums.Count, enumNames.Length);
                Assert.IsNotNull(enums.FirstOrDefault(x => x.Name == "Layout Component")); ;
            }
        }


        [TestMethod]
        public void EFTestCopyDefaultRecordsExist()
        {
            int countResults(ApiContext ctx, string sql)
            {
                int count;
                using (var connection = ctx.Database.GetDbConnection())
                {
                    connection.Open();
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = sql;
                        string result = command.ExecuteScalar().ToString();

                        int.TryParse(result, out count);
                    }
                }
                return count;
            }

            using (ApiContext ctx = GetMockCtx(1))
            {
                var countCopyDefaultRecordsSPROC = countResults(ctx, "SELECT COUNT(*) FROM sys.objects WHERE type = 'P' AND name = 'Util.Table.CopyDefaultRecords'");
                Assert.AreEqual(1, countCopyDefaultRecordsSPROC);
            }

            using (ApiContext ctx = GetMockCtx(1))
            {
                var countDependencyDepthFUNC = countResults(ctx, "SELECT COUNT(*) FROM sys.objects WHERE type IN ( N'FN', N'IF', N'TF', N'FS', N'FT' ) and name = 'Util.Table.DependencyDepth'");
                Assert.AreEqual(1, countDependencyDepthFUNC);
            }

        }

        [TestMethod]
        public async Task SystemMessageTemplateTypeBasicTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.SystemMessageTemplateType.ToListAsync());
            }
        }

        [TestMethod]
        public async Task MessageBodyTemplateBasicTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Assert.IsNotNull(await ctx.MessageBodyTemplate.ToListAsync());
            }
        }


        [TestMethod]
        public void ReconciliationDataTest()
        {
            using (ApiContext ctx = GetMockCtx(1))
            {
                Action cleanup = () =>
                {
                    var testData = ctx.Reconciliation.FirstOrDefault(x => x.BID == 1 && x.ID == -98);
                    if (testData != null)
                    {
                        ctx.Remove(testData);
                    }
                    var parentTestData = ctx.Reconciliation.FirstOrDefault(x => x.BID == 1 && x.ID == -99);
                    if (parentTestData != null)
                    {
                        ctx.Remove(parentTestData);
                    }
                    ctx.SaveChanges();
                };
                cleanup();

                Assert.IsNotNull(ctx.Reconciliation.Include(e => e.Items).ToList());
                Assert.IsNotNull(ctx.ReconciliationItem.Include(e => e.Reconciliation).ToList());
                var empID = ctx.EmployeeData.Where(x => x.BID == 1).Select(x => x.ID).FirstOrDefault();
                var locID = ctx.LocationData.Where(x => x.BID == 1).Select(x => x.ID).FirstOrDefault();
                var parentRecon = new Reconciliation()
                {
                    BID = 1,
                    ID = -99,
                    LocationID = locID,
                    FormattedNumber = "abc",
                    ExportedByID = empID,
                    SyncedDT = new DateTime(2020, 1, 1),
                    SyncedByID = empID,
                    TotalIncome = 101,
                    TotalPayments = 102
                };

                var recon = new Reconciliation()
                {
                    AdjustedReconciliationID = parentRecon.ID,
                    BID = 1,
                    ID = -98,
                    LocationID = locID,
                    FormattedNumber = "abc",
                    ExportedByID = empID,
                    SyncedDT = new DateTime(2020, 1, 1),
                    SyncedByID = empID,
                    TotalIncome = 101,
                    TotalPayments = 102,
                    IsEmpty = true
                };

                //add
                ctx.Add(parentRecon);
                ctx.Add(recon);
                ctx.SaveChanges();

                //test persist
                var reconInDB = ctx.Reconciliation
                                    .AsNoTracking()//do not track because we dont want the same object ref
                                    .FirstOrDefault(x => x.BID == recon.BID && x.ID == recon.ID);
                Assert.IsNotNull(reconInDB);
                Assert.AreEqual(recon.FormattedNumber, reconInDB.FormattedNumber, "FormattedNumber");
                Assert.AreEqual(recon.AdjustedReconciliationID, reconInDB.AdjustedReconciliationID, "AdjustedReconciliationID");
                Assert.AreEqual(recon.ExportedByID, reconInDB.ExportedByID, "ExportedByID");
                Assert.AreEqual(recon.SyncedDT.Value.Ticks, reconInDB.SyncedDT.Value.Ticks, "SyncedDT");
                Assert.AreEqual(recon.SyncedByID, reconInDB.SyncedByID, "SyncedByID");
                Assert.AreEqual(recon.TotalIncome, reconInDB.TotalIncome, "TotalIncome");
                Assert.AreEqual(recon.TotalPayments, reconInDB.TotalPayments, "TotalPayments");
                Assert.AreEqual(recon.IsEmpty, reconInDB.IsEmpty, "IsEmpty");

                cleanup();
            }
        }

        [TestMethod]
        public void InsertOptionForOrderAndEstimate()
        {
            using (ApiContext ctx = GetMockCtx(1,
                "20191125073548_InsertOptionForOrderAndEstimate"))
            {
                // Order.SimpleItem.Enable          - 6061
                var orderSimpleItemEnable = ctx.SystemOptionDefinition.Where(x => x.ID == 6061).FirstOrDefault();
                Assert.IsNotNull(orderSimpleItemEnable);
                Assert.AreEqual("Order.SimpleItem.Enable", orderSimpleItemEnable.Name);

                // Order.SimpleItem.ClassTypeID     - 6062
                var orderSimpleItemClassTypeID = ctx.SystemOptionDefinition.Where(x => x.ID == 6062).FirstOrDefault();
                Assert.IsNotNull(orderSimpleItemClassTypeID);
                Assert.AreEqual("Order.SimpleItem.ClassTypeID", orderSimpleItemClassTypeID.Name);

                // Order.SimpleItem.ComponentID     - 6063
                var orderSimpleItemComponentID = ctx.SystemOptionDefinition.Where(x => x.ID == 6063).FirstOrDefault();
                Assert.IsNotNull(orderSimpleItemComponentID);
                Assert.AreEqual("Order.SimpleItem.ComponentID", orderSimpleItemComponentID.Name);

                // Estimate.SimpleItem.Enable       - 6100
                var estimateSimpleItemEnable = ctx.SystemOptionDefinition.Where(x => x.ID == 6100).FirstOrDefault();
                Assert.IsNotNull(estimateSimpleItemEnable);
                Assert.AreEqual("Estimate.SimpleItem.Enable", estimateSimpleItemEnable.Name);

                // Estimate.SimpleItem.ClassTypeID  - 6101
                var estimateSimpleItemClassTypeID = ctx.SystemOptionDefinition.Where(x => x.ID == 6101).FirstOrDefault();
                Assert.IsNotNull(estimateSimpleItemClassTypeID);
                Assert.AreEqual("Estimate.SimpleItem.ClassTypeID", estimateSimpleItemClassTypeID.Name);

                // Estimate.SimpleItem.ComponentID  - 6102
                var estimateSimpleItemComponentID = ctx.SystemOptionDefinition.Where(x => x.ID == 6102).FirstOrDefault();
                Assert.IsNotNull(estimateSimpleItemComponentID);
                Assert.AreEqual("Estimate.SimpleItem.ComponentID", estimateSimpleItemComponentID.Name);
            }
        }

        [TestMethod]
        public async Task TestDomainEmailIsForAllLocationsDefaults()
        {
            short testBID = 1;
            short testID = -99;

            using (var ctx = GetMockCtx(testBID))
            {
                var de = await ctx.DomainEmail.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == testID);
                if (de != null)
                {
                    ctx.DomainEmail.Remove(de);
                    await ctx.SaveChangesAsync();
                }

                var domainEmail = new DomainEmail()
                {
                    BID = testBID,
                    ID = testID,
                    DomainName = "TEST DOMAIN NAME"
                };
                Assert.IsTrue(domainEmail.IsForAllLocations);
                ctx.DomainEmail.Add(domainEmail);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                var getDomainEmail = await ctx.DomainEmail.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == testID);
                Assert.IsNotNull(getDomainEmail);
                Assert.IsTrue(getDomainEmail.IsForAllLocations);

                getDomainEmail.IsForAllLocations = false;
                ctx.DomainEmail.Update(getDomainEmail);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

                getDomainEmail = await ctx.DomainEmail.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == testID);
                Assert.IsNotNull(getDomainEmail);
                Assert.IsFalse(getDomainEmail.IsForAllLocations);

                ctx.DomainEmail.Remove(await ctx.DomainEmail.FirstOrDefaultAsync(x => x.BID == testBID && x.ID == testID));
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }
        }

        [TestMethod]
        public void InsertOptionForReconciliation()
        {
            using (ApiContext ctx = GetMockCtx(1,
                "20200212080653_END-12115_Add_New_Reconciliation_Options"))
            {
                var optionCategory = ctx.SystemOptionCategory.FirstOrDefault(x => x.ID == 304);
                Assert.AreEqual("Reconciliation Options", optionCategory.Name);
                Assert.AreEqual("Reconciliation Options", optionCategory.Description);

                var reconciliationAutoReconcileEnabled = ctx.SystemOptionDefinition.Where(x => x.ID == 3041).FirstOrDefault();
                Assert.IsNotNull(reconciliationAutoReconcileEnabled);
                Assert.AreEqual("Reconciliation.Auto.Reconcile.Enabled", reconciliationAutoReconcileEnabled.Name);

                var reconciliationAutoReconcileTime = ctx.SystemOptionDefinition.Where(x => x.ID == 3042).FirstOrDefault();
                Assert.IsNotNull(reconciliationAutoReconcileTime);
                Assert.AreEqual("Reconciliation.Auto.Reconcile.Time", reconciliationAutoReconcileTime.Name);

                var reconciliationAutoReconcilePostEmpty = ctx.SystemOptionDefinition.Where(x => x.ID == 3043).FirstOrDefault();
                Assert.IsNotNull(reconciliationAutoReconcilePostEmpty);
                Assert.AreEqual("Reconciliation.Auto.Reconcile.PostEmpty", reconciliationAutoReconcilePostEmpty.Name);
            }
        } 
    }
}
