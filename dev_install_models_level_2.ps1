param (
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [Parameter(Mandatory=$true)][string]$version
)

pushd .\_scripts
$valid = .\_dev_validate
popd

function Install(
   [Parameter(Mandatory)][string]$project,
   [Parameter(Mandatory)][string]$mygetkey,
   [Parameter(Mandatory)][string]$version,
   [Parameter()][AllowNull()][string]$folderName = $null
) {
	if (!$folderName) { $folderName = $project }
	& dotnet add ./$folderName/$project.csproj package Endor.Models --source https://www.myget.org/F/corebridgeshared/auth/$mygetkey/api/v2 --version $version
}

if ($valid) {
	Install 'Endor.AEL' $mygetkey $version

	Install 'Endor.AEL.Tests' $mygetkey $version

	#Install 'Endor.AzureStorage' $mygetkey $version

	#Install 'Endor.Common.Level2.Tests' $mygetkey $version 'Endor.Common.Tests'

	Install 'Endor.EF' $mygetkey $version

	Install 'Endor.EF.StartupEmulator' $mygetkey $version

	#Install 'Endor.Logging.Client' $mygetkey $version

	#Install 'Endor.Lucene' $mygetkey $version

	#Install 'Endor.Lucene.Tests' $mygetkey $version

	#Install 'Endor.Tenant.Cache.Controller' $mygetkey $version

	#Install 'Endor.Tenant.Cache.Controller.Net462' $mygetkey $version
	
	Read-Host -Prompt 'In Visual Studio, please install Models to Endor.Lucene, and Endor.Lucene.Tests, then press the enter key'
}