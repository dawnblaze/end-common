﻿using Endor.Logging.Models;
using Serilog;
using Serilog.Core;
using Serilog.Sinks.Http;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Logging.Client
{
    internal static class RemoteLoggerFactory
    {
        private class BasicAuthenticatedHttpClient : IHttpClient
        {
            private readonly HttpClient client;

            public BasicAuthenticatedHttpClient(string tenantSecret)
            {
                client = CreateHttpClient(tenantSecret);
            }

            public Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content) =>
              client.PostAsync(requestUri, content);

            public void Dispose() =>
              client.Dispose();

            private static HttpClient CreateHttpClient(string tenantSecret)
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Internal", tenantSecret);

                return client;
            }
        }

        public static LoggerConfiguration GetCommonConfiguration(string loggingAppServerUrl, LoggingLevelSwitch @switch, string tenantSecret)
        {   
            return new LoggerConfiguration()
                .MinimumLevel.ControlledBy(@switch)
                .WriteTo.Http(loggingAppServerUrl.Trim('/') + "/api/log/", httpClient: new BasicAuthenticatedHttpClient(tenantSecret))
                .Enrich.With(new NamespaceEnricher(System.Reflection.Assembly.GetEntryAssembly().EntryPoint.DeclaringType.Namespace));
        }
    }
}
