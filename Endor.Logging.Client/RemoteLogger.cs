﻿using System;
using Endor.Logging.Models;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Serilog;
using Serilog.Sinks.Http;
using Endor.Tenant;
using System.Net.Http.Headers;

namespace Endor.Logging.Client
{
    public class RemoteLogger
    {
        public static string FallbackUrl { get; set; }
        public string TenantSecret { get; set; }
        
        private readonly ITenantDataCache _iTenantCache;

        public RemoteLogger(ITenantDataCache iTenantCache)
        {
            this._iTenantCache = iTenantCache;
        }

        private Serilog.ILogger _logger;

        private async Task<string> LoggingUrl(short bid)
        {
            TenantData tenant = await this._iTenantCache.Get(bid);

            if (tenant == null)
                return FallbackUrl;
            else
                return tenant.LoggingURL;
        }

        private async Task<Serilog.ILogger> GetLogger(short bid)
        {
            if (_logger == null)
            {
                string loggingUrl = await LoggingUrl(bid);

                if (string.IsNullOrWhiteSpace(loggingUrl))
                    return null;

                _logger = RemoteLoggerFactory.GetCommonConfiguration(loggingUrl, LoggingFilter.EndorLevelSwitch, this.TenantSecret).CreateLogger();
            }

            return _logger;
        }

        public async Task Billing(BillingEntryType type, short bid, string machineName, int userID, string summary)
        {
            try
            {
                (await this.GetLogger(bid)).Billing(type, bid, machineName, userID, summary);
            }
            catch
            {
                // Logger should quietly give up when an error is thrown.
            }
        }

        public async Task ClientAction(ClientActionEntryType type, short bid, int userID, string clientID, string summary)
        {
            try
            {
                (await this.GetLogger(bid)).ClientAction(type, bid, userID, clientID, summary);
            }
            catch
            {
                // Logger should quietly give up when an error is thrown.
            }
        }

        public async Task Information(short bid, string message)
        {
            try
            {
                (await this.GetLogger(bid)).Information(Constants.TypeAndBIDTemplate + message, LoggerType.Diagnostic, bid);
            }
            catch
            {
                // Logger should quietly give up when an error is thrown.
            }
        }

        public async Task Warning(short bid, string message)
        {
            try
            {
                (await this.GetLogger(bid)).Warning(Constants.TypeAndBIDTemplate + message, LoggerType.Diagnostic, bid);
            }
            catch
            {
                // Logger should quietly give up when an error is thrown.
            }
        }

        public async Task Warning(short bid, string message, Exception e)
        {
            try
            {
                (await this.GetLogger(bid)).Warning(e, Constants.TypeAndBIDTemplate + message, LoggerType.Diagnostic, bid);
            }
            catch
            {
                // Logger should quietly give up when an error is thrown.
            }
        }

        public async Task Error(short bid, string message, Exception e)
        {
            try
            {
                (await this.GetLogger(bid)).Error(e, Constants.TypeAndBIDTemplate + message, LoggerType.Diagnostic, bid);
            }
            catch
            {
                // Logger should quietly give up when an error is thrown.
            }
        }

        /// <summary>
        /// POSTs a ConnectionStateChangeEvent to the logging server
        /// </summary>
        /// <param name="change"></param>
        /// <exception cref="ArgumentNullException">if object supplied is null</exception>
        /// <exception cref="ArgumentException">if BID on object is not non-default</exception>
        /// <exception cref="InvalidOperationException">if tenant data is null or LoggingURL is empty</exception>
        /// <returns></returns>
        public async Task Connection(ConnectionStateChangeEvent change)
        {
            if (change == null)
                throw new ArgumentNullException("change");

            string loggingUrl = await LoggingUrl(change.BID);

            if (String.IsNullOrWhiteSpace(loggingUrl))
                throw new InvalidOperationException("No tenant data or missing logging URL");

            using (HttpClient client = new HttpClient())
            {
                string effectiveURL = loggingUrl;
                string endpoint = "api/connection";

                if (!effectiveURL.EndsWith("/"))
                    effectiveURL += '/';

                effectiveURL += endpoint;

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Internal", String.Format("{0}:{1}", TenantSecret, change.BID));
                await client.PostAsync(effectiveURL, new StringContent(JsonConvert.SerializeObject(change), Encoding.UTF8, "application/json"));
            }
        }

        public async Task TryPostConnection(ConnectionStateChangeEvent change)
        {
            try
            {
                await Connection(change);
            }
            catch(Exception e)
            {
                await Error(change.BID, "Error posting connection change", e);
            }
        }

        public async Task PricingError(short bid, string message, Exception e)
        {
            try
            {
                (await this.GetLogger(bid)).Error(e, Constants.TypeAndBIDTemplate + message, LoggerType.PricingError, bid);
            }
            catch
            {
                await Error(bid, "Error ", e);
            }
        }
    }
}
