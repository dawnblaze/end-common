﻿using Endor.Logging.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.Extensions.Hosting;

namespace Endor.Logging.Client
{
    public static class RemoteLoggingExtensions
    {
        [System.Runtime.CompilerServices.MethodImpl(MethodImplOptions.NoInlining)]
        public static void ConfigureSystemRemoteLogging(IConfigurationRoot config, ILoggerFactory loggerFactory, IHostApplicationLifetime appLifetime)
        {
            string remoteURL = config[Constants.RemoteDiagnosticLoggingURLConfigName];
            string tenantSecret = config[Constants.TenantSecretConfigName];

            if (!string.IsNullOrWhiteSpace(remoteURL))
            {
                RemoteLogger.FallbackUrl = remoteURL;

                Log.Logger = RemoteLoggerFactory.GetCommonConfiguration(remoteURL, LoggingFilter.SystemLevelSwitch, tenantSecret)
                    .CreateLogger();

                loggerFactory.AddSerilog();

                appLifetime.ApplicationStopped.Register(Log.CloseAndFlush);
            }
        }
    }
}
