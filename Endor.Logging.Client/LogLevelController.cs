﻿using Endor.Logging.Models;
using Microsoft.AspNetCore.Mvc;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Logging.Client
{
    [Route("api/[controller]")]
    public class LogLevelController: ControllerBase
    {
        [HttpPost("framework")]
        public IActionResult SetFrameworkLogLevel([FromQuery] LogEventLevel newLevel)
        {
            return SetSwitch(LoggingFilter.SystemLevelSwitch, newLevel);
        }

        [HttpPost("app")]
        public IActionResult SetAppLogLevel([FromQuery] LogEventLevel newLevel)
        {
            return SetSwitch(LoggingFilter.EndorLevelSwitch, newLevel);
        }

        private IActionResult SetSwitch(LoggingLevelSwitch @switch, LogEventLevel newLevel)
        {
            LogEventLevel oldLevel = @switch.MinimumLevel;
            @switch.MinimumLevel = newLevel;

            if (oldLevel != @switch.MinimumLevel)
                return new OkResult();
            else
                return StatusCode(500);
        }
    }
}
