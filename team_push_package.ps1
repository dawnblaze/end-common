param(
	[string] $packageFile,
	[string] $apiKey,
	[string] $teamName
)

if ([string]::IsNullOrEmpty($apiKey)) 
{
	dotnet.exe nuget push $packageFile -s https://www.myget.org/F/corebridgeteam$teamName/api/v2/package -ss https://www.myget.org/F/corebridgeteam$teamName/symbols/api/v2/package
}
else
{
	dotnet.exe nuget push $packageFile -s https://www.myget.org/F/corebridgeteam$teamName/api/v2/package -ss https://www.myget.org/F/corebridgeteam$teamName/symbols/api/v2/package -k $apiKey -sk $apiKey
}
