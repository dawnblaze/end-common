param (
    [Parameter(Mandatory=$true)][string]$csprojPath,
    [Parameter(Mandatory=$true)][string]$newAssemblyVersion
)
Write-Host 'Setting' $csprojPath 'to v'$newAssemblyVersion

#Read csproj (XML)
$xml = [xml](Get-Content $csprojPath)

$propertyGroup = $xml.Project.PropertyGroup
if ($propertyGroup -is [array]) {
  $propertyGroup = $xml.Project.PropertyGroup[0]
} 

if ($propertyGroup.Version -is [array]) {
  Throw 'This project has two version tags! Correct this and re-run the script'
} 
$propertyGroup.Version = $newAssemblyVersion

if ($propertyGroup.AssemblyVersion -ne $null) {
	$propertyGroup.AssemblyVersion = $newAssemblyVersion.split('-')[0]
}

if ($propertyGroup.FileVersion -ne $null) {
	$propertyGroup.FileVersion = $newAssemblyVersion.split('-')[0]
}

#Save csproj (XML)
$xml.Save($csprojPath)