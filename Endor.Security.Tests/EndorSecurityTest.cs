using Endor.Security.Interfaces;
using Endor.Security.Repositories.Classes;
using Endor.Security.Repositories.Interfaces;
using FakeItEasy;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Security.Tests
{
    [TestClass]
    public class EndorSecurityTest
    {
        [TestMethod]
        public void TestRightsCache()
        {
            //Arrange
            short BID = 1;
            short NotReallyAvailableBID = 2;
            short NotAvailableBID = 3;
            short userLinkID = 1;
            short notAvailableUserLinkID = 2;

            var UserRights = new UserRights[]
            {
                new UserRights
                {
                    BID = 1,
                    UserLinkID = 1,
                    RightsGroup = new int[] { 1 },
                    SecurityRights = new SecurityRight[]
                    {
                        SecurityRight.Access3rdPartyIntegrationSetup,
                        SecurityRight.AccessAccountingBoards,
                        SecurityRight.AccessAccountingOptionsSetup,
                        SecurityRight.AccessAdjustTimeCards
                    }
                }
            };

            var useRightsRepositoryFake = A.Fake<IUserRightsWithSourceRepository>();

            A.CallTo(() => useRightsRepositoryFake.GetRightsBy(BID, userLinkID)).Returns(new UserRightsWithSourceResult[] {
                new UserRightsWithSourceResult { GroupID = 1, RightID = (int)SecurityRight.Access3rdPartyIntegrationSetup },
                new UserRightsWithSourceResult { GroupID = 1, RightID = (int)SecurityRight.AccessAccountingBoards },
                new UserRightsWithSourceResult { GroupID = 1, RightID = (int)SecurityRight.AccessAccountingOptionsSetup },
                new UserRightsWithSourceResult { GroupID = 1, RightID = (int)SecurityRight.AccessAdjustTimeCards }
,            });

            var rightsCache = new RightsCache(A.Fake<IServerConfigurations>(), useRightsRepositoryFake);

            // Act and Assertions
            rightsCache.HasRight(BID, userLinkID, SecurityRight.AccessAlerts).Should().Be(false);
            rightsCache.HasRight(BID, userLinkID, SecurityRight.AccessAccountingBoards).Should().Be(true);

            rightsCache.HasAnyRight(BID, userLinkID, new SecurityRight[]
            {
                SecurityRight.AccessAccountingBoards,
                SecurityRight.AccessAssemblySetup,
                SecurityRight.AccessBoardSetup
            }).Should().Be(true);
            rightsCache.HasAnyRight(BID, userLinkID, new SecurityRight[]
            {
                SecurityRight.AccessMachineSetup,
                SecurityRight.AccessAssemblySetup,
                SecurityRight.AccessBoardSetup
            }).Should().Be(false);

            rightsCache.HasAllRights(BID, userLinkID, new SecurityRight[]
            {
                SecurityRight.Access3rdPartyIntegrationSetup,
                SecurityRight.AccessAccountingBoards,
                SecurityRight.AccessAccountingOptionsSetup,
                SecurityRight.AccessAdjustTimeCards
            }).Should().Be(true);
            rightsCache.HasAllRights(BID, userLinkID, new SecurityRight[]
            {
                SecurityRight.AccessModuleAccounting,
                SecurityRight.AccessAccountingBoards,
                SecurityRight.AccessAccountingOptionsSetup,
                SecurityRight.AccessAdjustTimeCards
            }).Should().Be(false);

            A.CallTo(() => useRightsRepositoryFake.GetRightsBy(BID, userLinkID)).MustHaveHappenedOnceExactly();

            rightsCache.HasRight(NotReallyAvailableBID, userLinkID, SecurityRight.AccessAlerts).Should().Be(false);
            rightsCache.HasRight(NotReallyAvailableBID, userLinkID, SecurityRight.AccessAlerts).Should().Be(false);

            A.CallTo(() => useRightsRepositoryFake.GetRightsBy(NotReallyAvailableBID, userLinkID)).MustHaveHappenedTwiceExactly();

            rightsCache.ClearCache(BID);

            rightsCache.HasRight(BID, userLinkID, SecurityRight.Access3rdPartyIntegrationSetup).Should().Be(true);

            A.CallTo(() => useRightsRepositoryFake.GetRightsBy(BID, userLinkID)).MustHaveHappenedTwiceExactly();

            rightsCache.HasRight(BID, notAvailableUserLinkID, SecurityRight.AccessAlerts).Should().Be(false);
            rightsCache.HasRight(BID, notAvailableUserLinkID, SecurityRight.AccessAlerts).Should().Be(false);

            A.CallTo(() => useRightsRepositoryFake.GetRightsBy(BID, notAvailableUserLinkID)).MustHaveHappenedTwiceExactly();

            Assert.ThrowsException<KeyNotFoundException>(() => rightsCache.ClearCache(NotAvailableBID));
        }
    }
}
