﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class DestinationProfile : IAtom<int>
    {
        public int ID { get; set; }
        [JsonIgnore]
        public short BID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public string Name { get; set; }
        public string AssemblyOVDataJSON { get; set; }

        public short DestinationID { get; set; }

        public int TemplateID { get; set; }

        [JsonIgnore]
        public DestinationData Destination { get; set; }
        [JsonIgnore]
        public AssemblyData Template { get; set; }

        public ICollection<DestinationProfileTable> DestinationProfileTables { get; set; }

        public ICollection<DestinationProfileVariable> DestinationProfileVariables { get; set; }
    }
}
