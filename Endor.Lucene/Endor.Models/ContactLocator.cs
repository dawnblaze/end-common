﻿namespace Endor.Models
{
    public partial class ContactLocator : BaseLocator<int, ContactData>
    {
        public override int ParentID { get; set; }
        public override ContactData Parent { get; set; }
        public override EnumLocatorSubType LocatorSubTypeNavigation { get; set; }
        public override EnumLocatorType LocatorTypeNavigation { get; set; }
    }
}
