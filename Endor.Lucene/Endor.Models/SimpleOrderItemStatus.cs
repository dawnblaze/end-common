﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class SimpleOrderItemStatus: SimpleListItem<short>
    {
        public byte TransactionType { get; set; }
        public OrderOrderStatus OrderStatusID { get; set; }
    }
}
