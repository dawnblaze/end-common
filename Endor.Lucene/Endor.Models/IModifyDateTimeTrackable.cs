﻿using System;

namespace Endor.Models
{
    public interface IModifyDateTimeTrackable
    {
        DateTime ModifiedDT { get; set; }
    }
}
