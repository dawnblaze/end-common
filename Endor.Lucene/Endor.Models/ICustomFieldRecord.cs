﻿using System;

namespace Endor.Models
{
    public interface ICustomFieldRecord<I> : IAtom<I>
        where I : struct, IConvertible
    {
        /// <summary>
        /// An ID identifying the type of object this CF applies to. This field is a read-only constant for dedicated CF tables, and read-write for the "other" CF table.
        /// </summary>
        int AppliesToClassTypeID { get; set; }
        /// <summary>
        /// The XML Data for the CustomField Values for this object, 
        /// </summary>
        string DataXML { get; set; }
    }
}
