﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public class MessageDeliveryRecord : IAtom<int>, IDocumentRepository<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public int ParticipantID { get; set; }
        public short AttemptNumber { get; set; }
        public bool IsPending { get; set; }
        public DateTime? ScheduledDT { get; set; }
        public DateTime? AttemptedDT { get; set; }
        public bool WasSuccessful { get; set; }
        public string FailureMessage { get; set; }
        public string MetaData { get; set; }

    }
}
