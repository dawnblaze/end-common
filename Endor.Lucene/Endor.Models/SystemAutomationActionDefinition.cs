﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class SystemAutomationActionDefinition
    {
        [Key]
        public byte ID { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        public bool AppliesToAlerts { get; set; }

        public bool AppliesToAutomations { get; set; }

        public bool AppliesToAllDataTypes { get; set; }

        public ICollection<SystemAutomationActionDataTypeLink> DataTypeLinks { get; set; }
    }
}
