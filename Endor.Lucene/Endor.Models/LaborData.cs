﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class LaborData : IAtom<int>, IImageCandidate
    {
        public short BID { get; set; }

        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string InvoiceText { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        public bool HasImage { get; set; }

        [StringLength(512)]
        public string SKU { get; set; }

        public int IncomeAccountID { get; set; }

        public int ExpenseAccountID { get; set; }

        public decimal? EstimatingCostFixed { get; set; }

        public decimal? EstimatingCostPerHour { get; set; }

        public decimal? EstimatingPriceFixed { get; set; }

        public decimal? EstimatingPricePerHour { get; set; }
        
        public byte? MinimumTimeInMin { get; set; }

        public byte? BillingIncrementInMin { get; set; }

        public short? TaxCodeID { get; set; }

        public TaxabilityCode TaxabilityCode { get; set; }

        public GLAccount IncomeAccount { get; set; }

        public GLAccount ExpenseAccount { get; set; }

        public ICollection<LaborCategoryLink> LaborCategoryLinks { get; set; }

        public ICollection<LaborCategory> LaborCategories { get; set; }

        public ICollection<SimpleLaborCategory> SimpleLaborCategories { get; set; }
    }
}
