﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public sealed class UnitTypeInfo
    {
        public UnitType UnitType { get; internal set; }
        public string Name { get; internal set; }

        public List<UnitInfo> Units
        {
            get => UnitInfo.InfoListByUnitType(UnitType);
        }

        public List<UnitInfo> UnitsBySystem(SystemOfUnits sou)
                => UnitInfo.InfoListByUnitType(UnitType, sou);

        public override string ToString()
                => Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);


        private static object MyLock = new object();

        private static List<UnitTypeInfo> _InfoList;
        public static List<UnitTypeInfo> InfoList
        {
            get
            {
                if (_InfoList == null)
                {
                    // obtain a lock to prevent multiple instances
                    lock (MyLock)
                    {
                        // check that someone else wasn't building ... 
                        if (_InfoList == null)
                        {
                            _InfoList = GetInfoList();
                        }
                    }

                }
                return _InfoList;
            }
        }

        private static List<UnitTypeInfo> GetInfoList()
        {
            return new List<UnitTypeInfo>()
            {
                new UnitTypeInfo() { UnitType = UnitType.General, Name = "General" },
                new UnitTypeInfo() { UnitType = UnitType.Currency, Name = "Currency" },
                new UnitTypeInfo() { UnitType = UnitType.Percent, Name = "Percent" },
                new UnitTypeInfo() { UnitType = UnitType.Custom, Name = "Custom" },
                new UnitTypeInfo() { UnitType = UnitType.Discrete, Name = "Discrete" },
                new UnitTypeInfo() { UnitType = UnitType.Length, Name = "Length" },
                new UnitTypeInfo() { UnitType = UnitType.Area, Name = "Area" },
                new UnitTypeInfo() { UnitType = UnitType.Volume_Solid, Name = "Volume (Solid)" },
                new UnitTypeInfo() { UnitType = UnitType.Volume_Liquid, Name = "Volume (Liquid)" },
                new UnitTypeInfo() { UnitType = UnitType.Weight_Mass, Name = "Weight/Mass" },
                new UnitTypeInfo() { UnitType = UnitType.DigitalStorage, Name = "Digital Storage" },
                new UnitTypeInfo() { UnitType = UnitType.Time, Name = "Time" },
                new UnitTypeInfo() { UnitType = UnitType.Speed_Length, Name = "Speed (Length/Rate)" },
                new UnitTypeInfo() { UnitType = UnitType.Speed_Area, Name = "Speed (Area/Rate)" },
                new UnitTypeInfo() { UnitType = UnitType.Speed_Solid, Name = "Speed (Solid Volume/Rate)" },
                new UnitTypeInfo() { UnitType = UnitType.Speed_Liquid, Name = "Speed (Liquid/Rate)" },
                new UnitTypeInfo() { UnitType = UnitType.Coverage, Name = "Coverage (Liquid/Area)" }
            };
        }
    }
}
