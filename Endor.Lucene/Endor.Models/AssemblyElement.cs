﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class AssemblyElement : IAtom<int>, IAssemblyElement
    {
        /// <summary>
        /// The Business ID for the Assembly Element.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID for the Assembly Element.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 12044.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Layout ID
        /// </summary>
        public short LayoutID { get; set; }

        /// <summary>
        /// The type of element used for input
        /// </summary>
        public AssemblyElementType ElementType { get; set; }

        /// <summary>
        /// The datatype of the element
        /// </summary>
        public DataType DataType { get; set; }

        /// <summary>
        /// Flag indicating whether the element can have a value entered
        /// </summary>
        public bool IsReadOnly { get; set; }

        /// <summary>
        /// Flag indicating that an element is disabled, but can still be seen
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// Indicates which column this elements left side is in
        /// </summary>
        public byte Column { get; set; }

        /// <summary>
        /// Indicates how many columns that this element takes up
        /// </summary>
        public byte ColumnsWide { get; set; }

        /// <summary>
        /// Indicates which row this element is in
        /// </summary>
        public byte Row { get; set; }

        /// <summary>
        /// The help text that is displayed on mouse over
        /// </summary>
        [StringLength(512)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Tooltip { get; set; } 

        /// <summary>
        /// Foreign Key pointing to the Visual Parent Element that this element is placed in.
        /// <para>Required for all Elements except Containers.</para>
        /// </summary>
        public int? ParentID { get; set; }

        /// <summary>
        /// Foreign Key pointing to the variable that this element is displaying or using for input
        /// </summary>
        public int? VariableID { get; set; }

        /// <summary>
        /// The VariableName of the underlying variable for this element.  This is not alterable, but used for display.
        /// </summary>
        public string VariableName { get; set; }
        
        /// <summary>
        /// Foreign Key pointing to AssemblyData
        /// </summary>
        public int AssemblyID { get; set; }

        /// <summary>
        /// This property holds an array of AssemblyElement Object objects for Containers and Hide/Show Groups.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyElement> Elements { get; set; }

        /// <summary>
        /// References the variable this element is bound to
        /// </summary>
        [JsonIgnore]
        public AssemblyVariable Variable { get; set; }
                
        /// <summary>
        /// temporary guid used to store a variable's relationship (not stored)
        /// </summary>
        public Guid? TempVariableID { get; set; }
    }
}
