﻿namespace Endor.Models
{
    public class MachineCategoryLink
    {
        public short BID { get; set; }

        public short PartID { get; set; }

        public short CategoryID { get; set; }

        public MachineData Machine { get; set; }

        public MachineCategory MachineCategory { get; set; }
    }
}
