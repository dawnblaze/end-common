﻿namespace Endor.Models
{
    public interface IMeasurement
    {
        decimal? ValueInUnits { get; }
        Unit Unit { get; set; }
        UnitType UnitType { get; }
        void LogConversionError(string message);

        string UnitName { get; }
        string UnitTypeName { get; }
        string UnitSystemName { get; }
        string UnitClassificationName { get; }
        int UnitDimension { get; }
    }
}