﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Text;
//https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/622231585/OrderItemSubStatus+Object
namespace Endor.Models
{
    /// <summary>
    /// has the ff shadow property/s - ValidToDT
    /// </summary>
    public class OrderItemSubStatus : IAtom<short>
    {
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderItemStatusSubStatusLink> OrderItemStatusSubStatusLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderItemStatus> OrderItemStatuses { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleOrderItemStatus> SimpleOrderItemStatuses { get; set; }
    }
}
