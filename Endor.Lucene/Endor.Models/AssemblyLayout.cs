﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class AssemblyLayout : IAtom<short>
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID of this object (Unique within the Business).
        /// </summary>
        public short ID { get; set; }

        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 12043.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The ID of the parent Assembly.
        /// </summary>
        public int AssemblyID { get; set; }

        /// <summary>
        /// The Type of the LayoutType.
        /// </summary>
        public AssemblyLayoutType LayoutType { get; set; }

        /// <summary>
        /// The type of MachineLayout
        /// </summary>
        public MachineLayoutType MachineLayoutType { get; set; }

        /// <summary>
        /// The Name for this object.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// Temporary ID of a Layout being saved (not stored in db)
        /// </summary>
        public Guid? TempLayoutID { get; set; }

        /// <summary>
        /// This property holds an array of AssemblyElement Object objects for Containers and Hide/Show Groups.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyElement> Elements { get; set; }
    }
}
