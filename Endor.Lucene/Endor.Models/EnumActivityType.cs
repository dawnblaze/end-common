﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum ActivityType : byte
    {
        System = 0,
        Meeting = 1,
        Call = 2,
        Appointment = 3,
        Task = 11,
        Note = 12,
        Email = 13,
        SMS_Message = 14,
        Accounting_Entry = 50,
    }

    public partial class EnumActivityType
    {
        public byte ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public byte RecordType { get; set; }
        [Required]
        [StringLength(15)]
        public string RecordTypeText { get; set; }
    }
}
