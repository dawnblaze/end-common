﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum AssemblyLayoutType : byte
    {
        AssemblyBasic = 1,
        AssemblyAdvanced = 2,
        AssemblyEcommerce = 3,
        MachineTemplatePrimary = 11,
        MachineTemplateMaterialLayout = 12,
        MachineTemplateSetup = 13,
        MachineTemplateProperties = 14
    }

    public class EnumAssemblyLayoutType
    {
        public AssemblyLayoutType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
