﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class CampaignData : IAtom<int>
    {
        public CampaignData()
        {
            Opportunities = new HashSet<OpportunityData>();
        }

        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public byte StatusID { get; set; }
        public int? TeamID { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public byte CampaignType { get; set; }
        public decimal? Budget { get; set; }
        public decimal? ActualCost { get; set; }
        public decimal? ExceptedRevenue { get; set; }
        public decimal? ActualRevenue { get; set; }
        public string Description { get; set; }
        public string Objective { get; set; }
        public string MetaData { get; set; }

        public BusinessData Business { get; set; }
        public EnumCampaignType CampaignTypeNavigation { get; set; }
        public EmployeeTeam EmployeeTeam { get; set; }
        public ICollection<OpportunityData> Opportunities { get; set; }
    }
}
