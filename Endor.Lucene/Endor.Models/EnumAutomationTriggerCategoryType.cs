﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class EnumAutomationTriggerCategoryType
    {
        /// <summary>
        /// The Ordinal Value of the enum.
        /// </summary>
        public AutomationTriggerCategoryType ID { get; set; }

        /// <summary>
        /// The text name for the ordinal value	
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

    }

    public enum AutomationTriggerCategoryType: byte
    {
        None                  = 0,
        Estimate              = 1,
        Order                 = 2,
        PurchaseOrder         = 3,
        OrderEstimateLineItem = 4,
        OrderDestination      = 6,
        Payment               = 8,
        ProofFile             = 9,
        Opportunity           = 10,
        Company               = 11
    }
}
