﻿using System;

namespace Endor.Models
{
    public interface IAtom: IModifyDateTimeTrackable
    {
        short BID { get; set; }
        int ClassTypeID { get; set; }
    }

    public interface IAtom<I> : IAtom where I : struct, IConvertible
    {        
        I ID { get; set; }
    }

    //public abstract class BaseAtom<I> : IAtom<I> where I : struct, IConvertible
    //{
    //    public short BID { get; set; }
    //
    //    public virtual I ID { get; set; }
    //
    //    public int ClassTypeID { get; set; }
    //
    //    public DateTime ModifiedDT { get; set; }
    //}
}
