﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public class ContactTagLink
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID of the Tag Object being linked.
        /// </summary>
        public short TagID { get; set; }

        /// <summary>
        /// The ID of the Associate Record.
        /// </summary>
        public int ContactID { get; set; }

        public ListTag Tag { get; set; }

        public ContactData Contact { get; set; }
    }
}
