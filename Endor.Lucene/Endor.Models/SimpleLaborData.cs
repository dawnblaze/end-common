﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public partial class SimpleLaborData : SimpleListItem<int>
    {
        public bool HasImage { get; set; }
        public string InvoiceText { get; set; }
        public string Description { get; set; }
    }
}
