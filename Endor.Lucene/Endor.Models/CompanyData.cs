﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class CompanyData : IAtom<int>, IImageCandidate, IHasCustomField<int, CompanyCustomData>
    {
        public CompanyData()
        {
            CompanyLocators = new HashSet<CompanyLocator>();
            CompanyContactLinks = new HashSet<CompanyContactLink>();
            ChildCompanies = new HashSet<CompanyData>();
            Opportunities = new HashSet<OpportunityData>();
            Orders = new HashSet<TransactionHeaderData>();
            TagLinks = new HashSet<CompanyTagLink>();
            Payments = new HashSet<PaymentMaster>();
        }

        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public byte LocationID { get; set; }
        public int? ParentID { get; set; }
        public bool IsAdHoc { get; set; }
        public byte StatusID { get; set; }
        public int? TeamID { get; set; }
        public short? OriginID { get; set; }
        public short? IndustryID { get; set; }
        public short? TaxGroupID { get; set; }
        public short? PricingTierID { get; set; }
        public int? PaymentTermID { get; set; }
        public bool IsPORequired { get; set; }
        [StringLength(100)]
        public string DefaultPONumber { get; set; }
        public decimal? RefundableCredit { get; set; }
        public decimal? NonRefundableCredit { get; set; }
        public decimal? CreditLimit { get; set; }
        [StringLength(100)]
        public string CreditNumber { get; set; }
        public DateTime? CreditApprovalDate { get; set; }
        public short? TimeZoneID { get; set; }
        public bool HasImage { get; set; }
        public string Tags { get; set; }
        [Required]
        [StringLength(24)]
        public string StatusText { get; set; }
        public bool? HasCreditAccount { get; set; }
        public bool? IsLead { get; set; }
        public bool? IsProspect { get; set; }
        public bool? IsClient { get; set; }
        public bool? IsVendor { get; set; }
        public bool? IsPersonal { get; set; }

        public bool IsTaxExempt { get; set; }
        public short? TaxExemptReasonID { get; set; }

        public int? VendorPaymentTermID { get; set; }
        [StringLength(100)]
        public string VendorAccountNumber { get; set; }
        [StringLength(100)]
        public string TaxID { get; set; }

        public TaxGroup TaxGroup { get; set; }
        public BusinessData Business { get; set; }
        public CompanyData ParentCompany { get; set; }
        public CrmIndustry CrmIndustry { get; set; }
        public CrmOrigin CrmOrigin { get; set; }
        public EmployeeTeam EmployeeTeam { get; set; }
        public LocationData Location { get; set; }
        public EnumCrmCompanyStatus Status { get; set; }
        public EnumTimeZone TimeZone { get; set; }
        public PaymentTerm DefaultPaymentTerms { get; set; }
        public PaymentTerm VendorPaymentTerms { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FlatListItem TaxExemptReason { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FlatListItem PricingTier { get; set; }

        public ICollection<CompanyLocator> CompanyLocators { get; set; }
        public ICollection<CompanyContactLink> CompanyContactLinks { get; set; }
        public ICollection<SimpleContactData> SimpleContacts { get; set; }
        public ICollection<CompanyData> ChildCompanies { get; set; }
        public ICollection<OpportunityData> Opportunities { get; set; }
        public ICollection<TransactionHeaderData> Orders { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<CompanyTagLink> TagLinks { get; set; }
        public ICollection<PaymentMaster> Payments { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string CFValuesJSON { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<CustomFieldValue> CFValues
        {
            get
            {
                if (String.IsNullOrWhiteSpace(CFValuesJSON))
                    return null;
                else
                    return JsonConvert.DeserializeObject<List<CustomFieldValue>>(CFValuesJSON);
            }
        }
    }
}
