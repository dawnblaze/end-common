﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum ElementType : byte
    {
        NumericInput = 0,
        BooleanInput = 1,
        RadioGroupInput = 2,
        StringInput = 3,
        MemoInput = 4,
        ListInput = 5,
        Label = 6,
        WebUrl = 7,
        Container = 8,
        LinkedAssembly = 9,
    	ShowHideContainer = 10,
    	Document = 11,
    	Spacer = 12,
    	Measurement = 13,
    	Custom = 14
    }

    public class EnumElementType
    {
        public ElementType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }

}
