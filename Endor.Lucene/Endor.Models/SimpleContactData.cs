﻿
namespace Endor.Models
{
    public partial class SimpleContactData : SimpleListItem<int>
    {
        public int ParentID { get; set; }

        public CompanyData ParentCompany { get; set; }
    }
}
