﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public class AssemblyCategoryLink
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID of the linked part (Assembly)
        /// </summary>
        public int PartID { get; set; }

        /// <summary>
        /// The ID of the linked part (Assembly) category.
        /// </summary>
        public short CategoryID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AssemblyData Assembly { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AssemblyCategory AssemblyCategory { get; set; }
    }
}
