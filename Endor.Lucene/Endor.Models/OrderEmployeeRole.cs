﻿using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Endor.Models
{
    public class OrderEmployeeRole : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public int OrderID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? OrderItemID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? DestinationID { get; set; }
        public short RoleID { get; set; }
        public short EmployeeID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string EmployeeName { get; set; }
        [JsonIgnore]
        public bool IsOrderRole { get; set; }
        [JsonIgnore]
        public bool IsOrderItemRole { get; set; }
        [JsonIgnore]
        public bool IsDestinationRole { get; set; }

        public TransactionHeaderData Order { get; set; }

        public EmployeeData Employee { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemData OrderItem { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderDestinationData Destination { get; set; }

        public EmployeeRole Role { get; set; }
    }
}
