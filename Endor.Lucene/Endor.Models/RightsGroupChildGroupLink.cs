﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public class RightsGroupChildGroupLink
    {
        public short ParentID { get; set; }

        public short ChildID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroup ParentRightsGroup { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroup ChildRightsGroup { get; set; }
    }
}
