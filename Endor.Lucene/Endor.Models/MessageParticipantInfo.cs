﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class MessageParticipantInfo : IAtom<int>, IDocumentRepository<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int ID { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int ClassTypeID { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public DateTime ModifiedDT { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public int BodyID { get; set; }
        [Required]
        public MessageParticipantRoleType ParticipantRoleType { get; set; }
        [Required]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public MessageChannelType Channel { get; set; }
        public string UserName { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsMergeField { get; set; }
        public short? EmployeeID { get; set; }
        public int? TeamID { get; set; }
        public int? ContactID { get; set; }
        public DateTime? DeliveredDT { get; set; }
        public bool? IsDelivered { get; set; }

        //navprops
        public MessageBody MesssageBody { get; set; }
        public ICollection<MessageDeliveryRecord> MessageDeliveryRecords { get; set; }
        public ICollection<MessageHeader> MessageHeaders { get; set; }
        public EmployeeData EmployeeData { get; set; }
        public EmployeeTeam EmployeeTeam { get; set; }
    }
}
