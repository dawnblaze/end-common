﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class SimpleListItem<T> where T : struct, IConvertible
    {
        [JsonIgnore]
        public short BID { get; set; }
        public T ID { get; set; }
        public int ClassTypeID { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public string DisplayName { get; set; }
    }
}
