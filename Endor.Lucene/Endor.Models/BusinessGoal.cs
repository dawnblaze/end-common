﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Endor.Models
{
    public partial class BusinessGoal : IAtom<short>
    {
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The Location ID
        /// </summary>
        public byte? LocationID { get; set; }

        /// <summary>
        /// The Year
        /// </summary>
        public short Year { get; set; }

        /// <summary>
        /// The Month
        /// </summary>
        public byte? Month { get; set; }

        /// <summary>
        /// The Budgeted amount
        /// </summary>
        public decimal? Budgeted { get; set; }

        /// <summary>
        /// The Actual amount
        /// </summary>
        public decimal? Actual { get; set; }

        /// <summary>
        /// Percentage of Goal hit
        /// </summary>
        [Column(TypeName="numeric(38,6)")]
        public decimal? PercentOfGoal { get; set; }

        /// <summary>
        /// If it is the Yearly Total
        /// </summary>
        public bool? IsYearlyTotal { get; set; }

        /// <summary>
        /// If it is the Business Total
        /// </summary>
        public bool? IsBusinessTotal { get; set; }

    }
}
