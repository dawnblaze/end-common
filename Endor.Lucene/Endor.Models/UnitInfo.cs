﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Models
{
    public sealed class UnitInfo
    {
        public Unit Unit { get; set; }
        public string NameUS { get; set; }
        public string NamePluralUS { get; set; }

        private string _NameUK;
        // We only store UK names when they are different
        // so we need to check when retrieved
        public string NameUK
        {
            get => (_NameUK == null || _NameUK.Length == 0) ? NameUS : _NameUK;
            set => _NameUK = value;
        }
        private string _NamePluralUK;
        // We only store UK names when they are different
        // so we need to check when retrieved
        public string NamePluralUK
        {
            get => (_NamePluralUK == null ||_NamePluralUK.Length == 0) ? NamePluralUS : _NamePluralUK;
            set => _NamePluralUK = value;
        }

        public string Name(bool UKEnglish = false) => UKEnglish ? NameUK : NameUS;

        public string NamePlural(bool UKEnglish = false) => UKEnglish ? NamePluralUK : NamePluralUS;

        public string Abbreviation { get; set; }
        public string Aliases { get; set; }
        public UnitType UnitType { get; set; }
        public bool IsStandard { get; set; }

        public decimal ToStandardMultiplyBy { get; set; }
        public decimal FromStandardMultiplyBy { get => 1.0m / ToStandardMultiplyBy; }
        public SystemOfUnits UnitSystem { get; set; }

        private string[] _Names;
        /// <summary>
        /// Returns a list of all possible names for this Unit
        /// </summary>
        public string[] Names
        {
            get
            {
                if (_Names == null || _Names.Length == 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine(NameUS);
                    sb.AppendLine(NamePluralUS);
                    if (NameUS != NameUK) sb.AppendLine(NameUK);
                    if (NamePluralUS != NamePluralUK) sb.AppendLine(NamePluralUK);
                    sb.Append(Abbreviation);
                    if (Aliases != null && Aliases.Length > 0)
                        // not working for net426
                        ////foreach (string s in Aliases.Split(" "))
                        // did a workaround that works for netcore2.2 and net426
                        foreach (string s in Aliases.Split(' '))
                            if (s.Trim().Length > 0)
                                {
                                sb.AppendLine();
                                sb.Append(s.Trim());
                            }

                    // not working for net426
                    //_Names = sb.ToString().Split(Environment.NewLine);
                    // did a workaround that works for netcore2.2 and net426
                    string[] separators = { Environment.NewLine };
                    _Names = sb.ToString().Split(separators, StringSplitOptions.None);
                }
                return _Names;
            }
        }

        public bool IsMetric() => ((UnitSystem & SystemOfUnits.Metric) != 0);

        public bool IsImperial() => ((UnitSystem & SystemOfUnits.Imperial) != 0);

        private static decimal? Convert(UnitInfo From, decimal value, UnitInfo To, Action<string> errorProc)
        {
            if (From.UnitType == To.UnitType)
                return Math.Round(value * From.ToStandardMultiplyBy / To.ToStandardMultiplyBy, 10);
            
            if (errorProc != null)
            {
                errorProc($"Incompatible Unit Types: Unable to Convert {From.Name()} to {To.Name()}.");
                return null;
            }
            
            throw new UnitConversionException(From.Unit, To.Unit);
        }

        public decimal? ConvertTo(Unit ToUnit, Action<string> errorProc = null)
        {
            return Convert(this, 1.0m, ToUnit.Info(), errorProc);
        }

        public decimal? ConvertTo(decimal? value, Unit ToUnit, Action<string> errorProc = null)
        {
            if (value == null)
                return null;
            else
                return Convert(this, value.Value, ToUnit.Info(), errorProc);
        }

        public decimal? ConvertFrom(Unit FromUnit, Action<string> errorProc = null)
        {
            return Convert(FromUnit.Info(), 1.0m, this, errorProc);
        }

        public decimal? ConvertFrom(decimal? value, Unit FromUnit, Action<string> errorProc = null)
        {
            if (value == null)
                return null;
            else
                return Convert(FromUnit.Info(), value.Value, this, errorProc);
        }

        public override string ToString()
                => Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);

        private static readonly object MyLock = new object();

        private static List<UnitInfo> _InfoList;
        private static SortedList<Unit, UnitInfo> _InfoListSorted;
        private static SortedList<SystemOfUnits, List<UnitInfo>> _InfoListBySystem;
        private static SortedList<(UnitType, SystemOfUnits), List<UnitInfo>> _InfoListByUnitType;

        public static List<UnitInfo> InfoList
        {
            get
            {
                if (_InfoList == null)
                {
                    // obtain a lock to prevent multiple instances
                    lock (MyLock)
                    {
                        // check that someone else wasn't building ... 
                        if (_InfoList == null)
                        {
                            _InfoList = new List<UnitInfo>(InfoListSorted.Values);
                        }
                    }

                }
                return _InfoList;
            }
        }

        public static SortedList<Unit, UnitInfo> InfoListSorted
        {
            get
            {
                if (_InfoListSorted == null)
                {
                    // obtain a lock to prevent multiple instances
                    lock (MyLock)
                    {
                        // check that someone else wasn't building ... 
                        if (_InfoListSorted == null)
                        {
                            _InfoListSorted = GetInfoList();
                        }
                    }

                }
                return _InfoListSorted;
            }
        }

        public static List<UnitInfo> InfoListByUnitType(UnitType ut, SystemOfUnits sou = SystemOfUnits.MetricAndImperial)
        {
            // not working for net426
            //List<UnitInfo> result = _InfoListByUnitType?.GetValueOrDefault((ut, sou));
            // did a workaround that works for netcore2.2 and net426
            List<UnitInfo> result = null;
            _InfoListByUnitType?.TryGetValue((ut, sou), out result);

            if (result == null)
            {
                // obtain a lock to prevent multiple instances
                lock (MyLock)
                {
                    // not working for net426
                    //result = _InfoListByUnitType?.GetValueOrDefault((ut, sou));
                    // did a workaround that works for netcore2.2 and net426
                    _InfoListByUnitType?.TryGetValue((ut, sou), out result);

                    // check that someone else wasn't building ... 
                    if (result == null)
                    {
                        result = InfoList.Where(li => (li.UnitType == ut) && (li.UnitSystem & sou) != 0).ToList<UnitInfo>();

                        if (_InfoListByUnitType == null)
                        {
                            _InfoListByUnitType = new SortedList<(UnitType, SystemOfUnits), List<UnitInfo>>();
                        }

                        _InfoListByUnitType.Add((ut, sou), result);
                    }
                }
            }
            return result;

        }

        public static List<UnitInfo> InfoListBySystem(SystemOfUnits sou)
        {
            // not working for net426
            //List<UnitInfo> result = _InfoListBySystem?.GetValueOrDefault(sou);
            // did a workaround that works for netcore2.2 and net426
            List<UnitInfo> result = null;
            _InfoListBySystem?.TryGetValue(sou, out result);
            if (result == null)
            {
                // obtain a lock to prevent multiple instances
                lock (MyLock)
                {
                    // not working for net426
                    //result = _InfoListBySystem?.GetValueOrDefault(sou);
                    // did a workaround that works for netcore2.2 and net426
                    _InfoListBySystem?.TryGetValue(sou, out result);


                    // check that someone else wasn't building ... 
                    if (result == null)
                    {
                        result = InfoList.Where(li => (li.UnitSystem & sou) != 0).ToList<UnitInfo>();

                        if (_InfoListBySystem == null)
                        {
                            _InfoListBySystem = new SortedList<SystemOfUnits, List<UnitInfo>>();
                        }

                        _InfoListBySystem.Add(sou, result);
                    }
                }

            }
            return result;
        }

        private static SortedList<Unit, UnitInfo> GetInfoList()
        {
            return new SortedList<Unit, UnitInfo>()
            {
                {  Unit.None, new UnitInfo() { Unit = Unit.None, NameUS = "None", NamePluralUS = "None", Abbreviation = "", Aliases = "", UnitType = UnitType.General, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Each, new UnitInfo() { Unit = Unit.Each, NameUS = "Each", NamePluralUS = "Each", Abbreviation = "ea", Aliases = "", UnitType = UnitType.Discrete, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Box, new UnitInfo() { Unit = Unit.Box, NameUS = "Box", NamePluralUS = "Boxes", Abbreviation = "box", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Carton, new UnitInfo() { Unit = Unit.Carton, NameUS = "Carton", NamePluralUS = "Cartons", Abbreviation = "ctn", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Impression, new UnitInfo() { Unit = Unit.Impression, NameUS = "Impression", NamePluralUS = "Impressions", Abbreviation = "imp", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Package, new UnitInfo() { Unit = Unit.Package, NameUS = "Package", NamePluralUS = "Packages", Abbreviation = "pkg", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Page, new UnitInfo() { Unit = Unit.Page, NameUS = "Page", NamePluralUS = "Pages", Abbreviation = "pg", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Pallet, new UnitInfo() { Unit = Unit.Pallet, NameUS = "Pallet", NamePluralUS = "Pallets", Abbreviation = "sd", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Piece, new UnitInfo() { Unit = Unit.Piece, NameUS = "Piece", NamePluralUS = "Pieces", Abbreviation = "pc", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Ream, new UnitInfo() { Unit = Unit.Ream, NameUS = "Ream", NamePluralUS = "Reams", Abbreviation = "rm", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Roll, new UnitInfo() { Unit = Unit.Roll, NameUS = "Roll", NamePluralUS = "Rolls", Abbreviation = "roll", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Set, new UnitInfo() { Unit = Unit.Set, NameUS = "Set", NamePluralUS = "Sets", Abbreviation = "set", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Sheet, new UnitInfo() { Unit = Unit.Sheet, NameUS = "Sheet", NamePluralUS = "Sheets", Abbreviation = "sheet", Aliases = "", UnitType = UnitType.Discrete, IsStandard = false, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Inch, new UnitInfo() { Unit = Unit.Inch, NameUS = "Inch", NamePluralUS = "Inches", Abbreviation = "in", Aliases = "", UnitType = UnitType.Length, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.Foot, new UnitInfo() { Unit = Unit.Foot, NameUS = "Foot", NamePluralUS = "Feet", Abbreviation = "ft", Aliases = "", UnitType = UnitType.Length, IsStandard = false, ToStandardMultiplyBy  = 12m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.Yard, new UnitInfo() { Unit = Unit.Yard, NameUS = "Yard", NamePluralUS = "Yards", Abbreviation = "yd", Aliases = "", UnitType = UnitType.Length, IsStandard = false, ToStandardMultiplyBy  = 36m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.Millimeter, new UnitInfo() { Unit = Unit.Millimeter, NameUS = "Millimeter", NamePluralUS = "Millimeters", Abbreviation = "mm", Aliases = "", UnitType = UnitType.Length, IsStandard = false, ToStandardMultiplyBy  = 0.0393700787m, NameUK = "Millimetre", NamePluralUK = "Millimetres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.Centimeter, new UnitInfo() { Unit = Unit.Centimeter, NameUS = "Centimeter", NamePluralUS = "Centimeters", Abbreviation = "cm", Aliases = "", UnitType = UnitType.Length, IsStandard = false, ToStandardMultiplyBy  = 0.3937007874m, NameUK = "Centimetre", NamePluralUK = "Centimetres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.Meter, new UnitInfo() { Unit = Unit.Meter, NameUS = "Meter", NamePluralUS = "Meters", Abbreviation = "m", Aliases = "", UnitType = UnitType.Length, IsStandard = false, ToStandardMultiplyBy  = 39.3700787401m, NameUK = "Metre", NamePluralUK = "Metres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareInch, new UnitInfo() { Unit = Unit.SquareInch, NameUS = "Square Inch", NamePluralUS = "Square Inches", Abbreviation = "sq in", Aliases = "sqin in^2 sqinch sqinches squarein si", UnitType = UnitType.Area, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareFoot, new UnitInfo() { Unit = Unit.SquareFoot, NameUS = "Square Foot", NamePluralUS = "Square Feet", Abbreviation = "sq ft", Aliases = "sqft sf ft^2 sqfoot sqfeet squareft ft2", UnitType = UnitType.Area, IsStandard = false, ToStandardMultiplyBy  = 144m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareYard, new UnitInfo() { Unit = Unit.SquareYard, NameUS = "Square Yard", NamePluralUS = "Square Yards", Abbreviation = "sq yd", Aliases = "sqrd sy yd^2 sqyard sqyards sqyd square yd2", UnitType = UnitType.Area, IsStandard = false, ToStandardMultiplyBy  = 1296m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareMillimeter, new UnitInfo() { Unit = Unit.SquareMillimeter, NameUS = "Square Millimeter", NamePluralUS = "Square Millimeters", Abbreviation = "sq mm", Aliases = "sqmm mm^2 sqmillimeter sqmillimeters sqmillimetre sqmillimetres square mm mm2", UnitType = UnitType.Area, IsStandard = false, ToStandardMultiplyBy  = 0.0015500031000016m, NameUK = "Square Millimetre", NamePluralUK = "Square Millimetres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareCentimeter, new UnitInfo() { Unit = Unit.SquareCentimeter, NameUS = "Square Centimeter", NamePluralUS = "Square Centimeters", Abbreviation = "sq cm", Aliases = "sqcm cm^2 sqcentimeter sqcentimeters sqcentimetre sqcentimetres sc squarecm cm2", UnitType = UnitType.Area, IsStandard = false, ToStandardMultiplyBy  = 0.15500031000016m, NameUK = "Square Centimetre", NamePluralUK = "Square Centimetres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareMeter, new UnitInfo() { Unit = Unit.SquareMeter, NameUS = "Square Meter", NamePluralUS = "Square Meters", Abbreviation = "sq m", Aliases = "sqm m^2 sqmeter sqmeters sqmetre sqmetres sm squarem m2", UnitType = UnitType.Area, IsStandard = false, ToStandardMultiplyBy  = 1550.0031000016m, NameUK = "Square Metre", NamePluralUK = "Square Metres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicInch, new UnitInfo() { Unit = Unit.CubicInch, NameUS = "Cubic Inch", NamePluralUS = "Cubic Inches", Abbreviation = "cu in", Aliases = "cuin in^3", UnitType = UnitType.Volume_Solid, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicFoot, new UnitInfo() { Unit = Unit.CubicFoot, NameUS = "Cubic Foot", NamePluralUS = "Cubic Feet", Abbreviation = "cu ft", Aliases = "cuft cf ft^3", UnitType = UnitType.Volume_Solid, IsStandard = false, ToStandardMultiplyBy  = 1728m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicYard, new UnitInfo() { Unit = Unit.CubicYard, NameUS = "Cubic Yard", NamePluralUS = "Cubic Yards", Abbreviation = "cu yd", Aliases = "cuyd yd^3", UnitType = UnitType.Volume_Solid, IsStandard = false, ToStandardMultiplyBy  = 46656m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicMillimeter, new UnitInfo() { Unit = Unit.CubicMillimeter, NameUS = "Cubic Millimeter", NamePluralUS = "Cubic Millimeters", Abbreviation = "cu mm", Aliases = "cumm mm^3", UnitType = UnitType.Volume_Solid, IsStandard = false, ToStandardMultiplyBy  = 0.000061023744094465m, NameUK = "Cubic Millimetre", NamePluralUK = "Cubic Millimetres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicCentimeter, new UnitInfo() { Unit = Unit.CubicCentimeter, NameUS = "Cubic Centimeter", NamePluralUS = "Cubic Centimeters", Abbreviation = "cu cm", Aliases = "cucc cm^3", UnitType = UnitType.Volume_Solid, IsStandard = false, ToStandardMultiplyBy  = 0.061023744094465m, NameUK = "Cubic Centimetre", NamePluralUK = "Cubic Centimetres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicMeter, new UnitInfo() { Unit = Unit.CubicMeter, NameUS = "Cubic Meter", NamePluralUS = "Cubic Meters", Abbreviation = "cu m", Aliases = "cum m^3", UnitType = UnitType.Volume_Solid, IsStandard = false, ToStandardMultiplyBy  = 61023.744094465m, NameUK = "Cubic Metre", NamePluralUK = "Cubic Metres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.OunceSolid, new UnitInfo() { Unit = Unit.OunceSolid, NameUS = "Ounce", NamePluralUS = "Ounces", Abbreviation = "oz", Aliases = "", UnitType = UnitType.Weight_Mass, IsStandard = false, ToStandardMultiplyBy  = 28.349523125m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.Pint, new UnitInfo() { Unit = Unit.Pint, NameUS = "Pint", NamePluralUS = "Pints", Abbreviation = "pint", Aliases = "", UnitType = UnitType.Volume_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.473176473m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.Quart, new UnitInfo() { Unit = Unit.Quart, NameUS = "Quart", NamePluralUS = "Quarts", Abbreviation = "qt", Aliases = "", UnitType = UnitType.Volume_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.946352946m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.Gallon, new UnitInfo() { Unit = Unit.Gallon, NameUS = "Gallon", NamePluralUS = "Gallons", Abbreviation = "gal", Aliases = "", UnitType = UnitType.Volume_Liquid, IsStandard = false, ToStandardMultiplyBy  = 3.785411784m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.Milliliter, new UnitInfo() { Unit = Unit.Milliliter, NameUS = "Milliliter", NamePluralUS = "Milliliters", Abbreviation = "ml", Aliases = "mil", UnitType = UnitType.Volume_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.001m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.Liter, new UnitInfo() { Unit = Unit.Liter, NameUS = "Liter", NamePluralUS = "Liters", Abbreviation = "L", Aliases = "", UnitType = UnitType.Volume_Liquid, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "Litre", NamePluralUK = "Litres", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.OunceFluid, new UnitInfo() { Unit = Unit.OunceFluid, NameUS = "Ounce", NamePluralUS = "Ounces", Abbreviation = "oz", Aliases = "floz", UnitType = UnitType.Volume_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.0295735295625m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial } },
                {  Unit.Cup, new UnitInfo() { Unit = Unit.Cup, NameUS = "Cup", NamePluralUS = "Cups", Abbreviation = "cup", Aliases = "", UnitType = UnitType.Volume_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.236588237m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.Pound, new UnitInfo() { Unit = Unit.Pound, NameUS = "Pound", NamePluralUS = "Pounds", Abbreviation = "lb", Aliases = "", UnitType = UnitType.Weight_Mass, IsStandard = false, ToStandardMultiplyBy  = 453.59237m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial } },
                {  Unit.Milligram, new UnitInfo() { Unit = Unit.Milligram, NameUS = "Milligram", NamePluralUS = "Milligrams", Abbreviation = "mg", Aliases = "", UnitType = UnitType.Weight_Mass, IsStandard = false, ToStandardMultiplyBy  = 0.001m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Metric } },
                {  Unit.Gram, new UnitInfo() { Unit = Unit.Gram, NameUS = "Gram", NamePluralUS = "Grams", Abbreviation = "g", Aliases = "", UnitType = UnitType.Weight_Mass, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Metric } },
                {  Unit.Kilogram, new UnitInfo() { Unit = Unit.Kilogram, NameUS = "Kilogram", NamePluralUS = "Kilograms", Abbreviation = "kg", Aliases = "", UnitType = UnitType.Weight_Mass, IsStandard = false, ToStandardMultiplyBy  = 1000m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Metric } },
                {  Unit.Byte, new UnitInfo() { Unit = Unit.Byte, NameUS = "Byte", NamePluralUS = "Bytes", Abbreviation = "B", Aliases = "", UnitType = UnitType.DigitalStorage, IsStandard = false, ToStandardMultiplyBy  = 0.000001m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.KiloByte, new UnitInfo() { Unit = Unit.KiloByte, NameUS = "KiloByte", NamePluralUS = "KiloBytes", Abbreviation = "KB", Aliases = "", UnitType = UnitType.DigitalStorage, IsStandard = false, ToStandardMultiplyBy  = 0.001m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Megabyte, new UnitInfo() { Unit = Unit.Megabyte, NameUS = "Megabyte", NamePluralUS = "Megabytes", Abbreviation = "MB", Aliases = "", UnitType = UnitType.DigitalStorage, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Gigabyte, new UnitInfo() { Unit = Unit.Gigabyte, NameUS = "Gigabyte", NamePluralUS = "Gigabytes", Abbreviation = "GB", Aliases = "", UnitType = UnitType.DigitalStorage, IsStandard = false, ToStandardMultiplyBy  = 1000m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Terabyte, new UnitInfo() { Unit = Unit.Terabyte, NameUS = "Terabyte", NamePluralUS = "Terabytes", Abbreviation = "TB", Aliases = "", UnitType = UnitType.DigitalStorage, IsStandard = false, ToStandardMultiplyBy  = 1000000m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Second, new UnitInfo() { Unit = Unit.Second, NameUS = "Second", NamePluralUS = "Seconds", Abbreviation = "sec", Aliases = "", UnitType = UnitType.Time, IsStandard = false, ToStandardMultiplyBy  = 0.016666666666666666666666667m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Minute, new UnitInfo() { Unit = Unit.Minute, NameUS = "Minute", NamePluralUS = "Minutes", Abbreviation = "min", Aliases = "", UnitType = UnitType.Time, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Hour, new UnitInfo() { Unit = Unit.Hour, NameUS = "Hour", NamePluralUS = "Hours", Abbreviation = "hr", Aliases = "", UnitType = UnitType.Time, IsStandard = false, ToStandardMultiplyBy  = 60m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.Day, new UnitInfo() { Unit = Unit.Day, NameUS = "Day", NamePluralUS = "Days", Abbreviation = "d", Aliases = "", UnitType = UnitType.Time, IsStandard = false, ToStandardMultiplyBy  = 1440m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.MetricAndImperial } },
                {  Unit.InchPerSecond, new UnitInfo() { Unit = Unit.InchPerSecond, NameUS = "Inch/Second", NamePluralUS = "Inches/Second", Abbreviation = "in/sec", Aliases = "ips", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 0.016666667m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.FootPerSecond, new UnitInfo() { Unit = Unit.FootPerSecond, NameUS = "Foot/Second", NamePluralUS = "Feet/Second", Abbreviation = "ft/sec", Aliases = "fps", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 0.2m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MillimeterPerSecond, new UnitInfo() { Unit = Unit.MillimeterPerSecond, NameUS = "Millimeter/Second", NamePluralUS = "Millimeters/Second", Abbreviation = "mm/sec", Aliases = "", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 0.004233333m, NameUK = "Millimetre/Second", NamePluralUK = "Millimetres/Second", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.MeterPerSecond, new UnitInfo() { Unit = Unit.MeterPerSecond, NameUS = "Meter/Second", NamePluralUS = "Meters/Second", Abbreviation = "m/sec", Aliases = "mps", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 4.233333333m, NameUK = "Metre/Second", NamePluralUK = "Metres/Second", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.InchPerMinute, new UnitInfo() { Unit = Unit.InchPerMinute, NameUS = "Inch/Minute", NamePluralUS = "Inches/Minute", Abbreviation = "in/min", Aliases = "", UnitType = UnitType.Speed_Length, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.FootPerMinute, new UnitInfo() { Unit = Unit.FootPerMinute, NameUS = "Foot/Minute", NamePluralUS = "Feet/Minute", Abbreviation = "ft/min", Aliases = "fpm", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 12m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MillimeterPerMinute, new UnitInfo() { Unit = Unit.MillimeterPerMinute, NameUS = "Millimeter/Minute", NamePluralUS = "Millimeters/Minute", Abbreviation = "mm/min", Aliases = "", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 0.254m, NameUK = "Millimetre/Minute", NamePluralUK = "Millimetres/Minute", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.MeterPerMinute, new UnitInfo() { Unit = Unit.MeterPerMinute, NameUS = "Meter/Minute", NamePluralUS = "Meters/Minute", Abbreviation = "m/min", Aliases = "", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 254m, NameUK = "Metre/Minute", NamePluralUK = "Metres/Minute", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.InchPerHour, new UnitInfo() { Unit = Unit.InchPerHour, NameUS = "Inch/Hour", NamePluralUS = "Inches/Hour", Abbreviation = "in/hr", Aliases = "", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 60m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.FeetPerHour, new UnitInfo() { Unit = Unit.FeetPerHour, NameUS = "Feet/Hour", NamePluralUS = "Feet/Hour", Abbreviation = "ft/hr", Aliases = "fph", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 720m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MillimetersPerHour, new UnitInfo() { Unit = Unit.MillimetersPerHour, NameUS = "Millimeters/Hour", NamePluralUS = "Millimeters/Hour", Abbreviation = "mm/hr", Aliases = "", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 15.24m, NameUK = "Millimetre/Hour", NamePluralUK = "Millimetres/Hour", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.MetersPerHour, new UnitInfo() { Unit = Unit.MetersPerHour, NameUS = "Meters/Hour", NamePluralUS = "Meters/Hour", Abbreviation = "m/hr", Aliases = "", UnitType = UnitType.Speed_Length, IsStandard = false, ToStandardMultiplyBy  = 15240m, NameUK = "Metre/Hour", NamePluralUK = "Metres/Hour", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareInchPerSecond, new UnitInfo() { Unit = Unit.SquareInchPerSecond, NameUS = "Square Inch/Second", NamePluralUS = "Square Inches/Second", Abbreviation = "sq in/sec", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 0.016666667m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareFootPerSecond, new UnitInfo() { Unit = Unit.SquareFootPerSecond, NameUS = "Square Foot/Second", NamePluralUS = "Square Feet/Second", Abbreviation = "sq ft/sec", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 2.4m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareMillimeterPerSecond, new UnitInfo() { Unit = Unit.SquareMillimeterPerSecond, NameUS = "Square Millimeter/Second", NamePluralUS = "Square Millimeters/Second", Abbreviation = "sq mm/sec", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 0.001075267m, NameUK = "Square Millimetre/Second", NamePluralUK = "Square Millimetres/Second", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareMeterPerSecond, new UnitInfo() { Unit = Unit.SquareMeterPerSecond, NameUS = "Square Meter/Second", NamePluralUS = "Square Meters/Second", Abbreviation = "sq m/sec", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 1075.266667m, NameUK = "Square Metre/Second", NamePluralUK = "Square Metres/Second", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareInchPerMinute, new UnitInfo() { Unit = Unit.SquareInchPerMinute, NameUS = "Square Inch/Minute", NamePluralUS = "Square Inches/Minute", Abbreviation = "sq in/min", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareFootPerMinute, new UnitInfo() { Unit = Unit.SquareFootPerMinute, NameUS = "Square Foot/Minute", NamePluralUS = "Square Feet/Minute", Abbreviation = "sq ft/min", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 144m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareMillimeterPerMinute, new UnitInfo() { Unit = Unit.SquareMillimeterPerMinute, NameUS = "Square Millimeter/Minute", NamePluralUS = "Square Millimeters/Minute", Abbreviation = "sq mm/min", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 0.064516m, NameUK = "Square Millimetre/Minute", NamePluralUK = "Square Millimetres/Minute", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareMeterPerMinute, new UnitInfo() { Unit = Unit.SquareMeterPerMinute, NameUS = "Square Meter/Minute", NamePluralUS = "Square Meters/Minute", Abbreviation = "sq m/min", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 64516m, NameUK = "Square Metre/Minute", NamePluralUK = "Square Metres/Minute", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareInchesPerHour, new UnitInfo() { Unit = Unit.SquareInchesPerHour, NameUS = "Square Inches/Hour", NamePluralUS = "Square Inches/Hour", Abbreviation = "sq in/hr", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 60m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareFeetPerHour, new UnitInfo() { Unit = Unit.SquareFeetPerHour, NameUS = "Square Feet/Hour", NamePluralUS = "Square Feet/Hour", Abbreviation = "sq ft/hr", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 8640m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.SquareMillimetersPerHour, new UnitInfo() { Unit = Unit.SquareMillimetersPerHour, NameUS = "Square Millimeters/Hour", NamePluralUS = "Square Millimeters/Hour", Abbreviation = "sq mm/hr", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 3.87096m, NameUK = "Square Millimetre/Hour", NamePluralUK = "Square Millimetres/Hour", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.SquareMetersPerHour, new UnitInfo() { Unit = Unit.SquareMetersPerHour, NameUS = "Square Meters/Hour", NamePluralUS = "Square Meters/Hour", Abbreviation = "sq m/hr", Aliases = "", UnitType = UnitType.Speed_Area, IsStandard = false, ToStandardMultiplyBy  = 3870960m, NameUK = "Square Metre/Hour", NamePluralUK = "Square Metres/Hour", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicInchPerSecond, new UnitInfo() { Unit = Unit.CubicInchPerSecond, NameUS = "Cubic Inch/Second", NamePluralUS = "Cubic Inches/Second", Abbreviation = "cu in/sec", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 0.016666667m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicFootPerSecond, new UnitInfo() { Unit = Unit.CubicFootPerSecond, NameUS = "Cubic Foot/Second", NamePluralUS = "Cubic Feet/Second", Abbreviation = "cu ft/sec", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 2.4m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicMillimeterPerSecond, new UnitInfo() { Unit = Unit.CubicMillimeterPerSecond, NameUS = "Cubic Millimeter/Second", NamePluralUS = "Cubic Millimeters/Second", Abbreviation = "cu mm/sec", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 0.001075267m, NameUK = "Cubic Millimetre/Second", NamePluralUK = "Cubic Millimetres/Second", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicMeterPerSecond, new UnitInfo() { Unit = Unit.CubicMeterPerSecond, NameUS = "Cubic Meter/Second", NamePluralUS = "Cubic Meters/Second", Abbreviation = "cu m/sec", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 1075.266667m, NameUK = "Cubic Metre/Second", NamePluralUK = "Cubic Metres/Second", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicInchPerMinute, new UnitInfo() { Unit = Unit.CubicInchPerMinute, NameUS = "Cubic Inch/Minute", NamePluralUS = "Cubic Inches/Minute", Abbreviation = "cu in/min", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicFootPerMinute, new UnitInfo() { Unit = Unit.CubicFootPerMinute, NameUS = "Cubic Foot/Minute", NamePluralUS = "Cubic Feet/Minute", Abbreviation = "cu ft/min", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 144m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicMillimeterPerMinute, new UnitInfo() { Unit = Unit.CubicMillimeterPerMinute, NameUS = "Cubic Millimeter/Minute", NamePluralUS = "Cubic Millimeters/Minute", Abbreviation = "cu mm/min", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 0.064516m, NameUK = "Cubic Millimetre/Minute", NamePluralUK = "Cubic Millimetres/Minute", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicMeterPerMinute, new UnitInfo() { Unit = Unit.CubicMeterPerMinute, NameUS = "Cubic Meter/Minute", NamePluralUS = "Cubic Meters/Minute", Abbreviation = "cu m/min", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 64516m, NameUK = "Cubic Metre/Minute", NamePluralUK = "Cubic Metres/Minute", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicInchesPerHour, new UnitInfo() { Unit = Unit.CubicInchesPerHour, NameUS = "Cubic Inches/Hour", NamePluralUS = "Cubic Inches/Hour", Abbreviation = "cu in/hr", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 60m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicFeetPerHour, new UnitInfo() { Unit = Unit.CubicFeetPerHour, NameUS = "Cubic Feet/Hour", NamePluralUS = "Cubic Feet/Hour", Abbreviation = "cu ft/hr", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 8640m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.CubicMillimetersPerHour, new UnitInfo() { Unit = Unit.CubicMillimetersPerHour, NameUS = "Cubic Millimeters/Hour", NamePluralUS = "Cubic Millimeters/Hour", Abbreviation = "cu mm/hr", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 3.87096m, NameUK = "Cubic Millimetre/Hour", NamePluralUK = "Cubic Millimetres/Hour", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.CubicMetersPerHour, new UnitInfo() { Unit = Unit.CubicMetersPerHour, NameUS = "Cubic Meters/Hour", NamePluralUS = "Cubic Meters/Hour", Abbreviation = "cu m/hr", Aliases = "", UnitType = UnitType.Speed_Solid, IsStandard = false, ToStandardMultiplyBy  = 3870960m, NameUK = "Cubic Metre/Hour", NamePluralUK = "Cubic Metres/Hour", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.OuncePerSecond, new UnitInfo() { Unit = Unit.OuncePerSecond, NameUS = "Ounce/Second", NamePluralUS = "Ounces/Second", Abbreviation = "oz/sec", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.000492892m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.GallonPerSecond, new UnitInfo() { Unit = Unit.GallonPerSecond, NameUS = "Gallon/Second", NamePluralUS = "Gallons/Second", Abbreviation = "gal/sec", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.063090196m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MilliliterPerSecond, new UnitInfo() { Unit = Unit.MilliliterPerSecond, NameUS = "Milliliter/Second", NamePluralUS = "Milliliters/Second", Abbreviation = "ml/sec", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.00001666666666667m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.LiterPerSecond, new UnitInfo() { Unit = Unit.LiterPerSecond, NameUS = "Liter/Second", NamePluralUS = "Liters/Second", Abbreviation = "L/sec", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.016666667m, NameUK = "Litre/Second", NamePluralUK = "Litres/Second", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.OuncePerMinute, new UnitInfo() { Unit = Unit.OuncePerMinute, NameUS = "Ounce/Minute", NamePluralUS = "Ounces/Minute", Abbreviation = "oz/min", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.02957353m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.GallonPerMinute, new UnitInfo() { Unit = Unit.GallonPerMinute, NameUS = "Gallon/Minute", NamePluralUS = "Gallons/Minute", Abbreviation = "gal/min", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 3.785411784m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MilliliterPerMinute, new UnitInfo() { Unit = Unit.MilliliterPerMinute, NameUS = "Milliliter/Minute", NamePluralUS = "Milliliters/Minute", Abbreviation = "ml/min", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.001m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.LiterPerMinute, new UnitInfo() { Unit = Unit.LiterPerMinute, NameUS = "Liter/Minute", NamePluralUS = "Liters/Minute", Abbreviation = "L/min", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "Litre/Minute", NamePluralUK = "Litres/Minute", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.OuncesPerHour, new UnitInfo() { Unit = Unit.OuncesPerHour, NameUS = "Ounces/Hour", NamePluralUS = "Ounces/Hour", Abbreviation = "oz/hr", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 1.7744118m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.GallonsPerHour, new UnitInfo() { Unit = Unit.GallonsPerHour, NameUS = "Gallons/Hour", NamePluralUS = "Gallons/Hour", Abbreviation = "gal/hr", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 227.124707m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MillilitersPerHour, new UnitInfo() { Unit = Unit.MillilitersPerHour, NameUS = "Milliliters/Hour", NamePluralUS = "Milliliters/Hour", Abbreviation = "ml/hr", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 0.06m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.LitersPerHour, new UnitInfo() { Unit = Unit.LitersPerHour, NameUS = "Liters/Hour", NamePluralUS = "Liters/Hour", Abbreviation = "L/hr", Aliases = "", UnitType = UnitType.Speed_Liquid, IsStandard = false, ToStandardMultiplyBy  = 60m, NameUK = "Litre/Hour", NamePluralUK = "Litres/Hour", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.OuncePerSquareInch, new UnitInfo() { Unit = Unit.OuncePerSquareInch, NameUS = "Ounce/Square Inch", NamePluralUS = "Ounces/Square Inch", Abbreviation = "oz/sq in", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 0.02957353m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.OuncePerSquareFoot, new UnitInfo() { Unit = Unit.OuncePerSquareFoot, NameUS = "Ounce/Square Foot", NamePluralUS = "Ounces/Square Foot", Abbreviation = "oz/sq ft", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 4.25858832m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.OuncePerSquareMillimeter, new UnitInfo() { Unit = Unit.OuncePerSquareMillimeter, NameUS = "Ounce/Square Millimeter", NamePluralUS = "Ounces/Square Millimeter", Abbreviation = "oz/sq mm", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 19.07965861m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.OuncePerSquareMeter, new UnitInfo() { Unit = Unit.OuncePerSquareMeter, NameUS = "Ounce/Square Meter", NamePluralUS = "Ounces/Square Meter", Abbreviation = "oz/sq m", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 45.83906318m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.GallonPerSquareInch, new UnitInfo() { Unit = Unit.GallonPerSquareInch, NameUS = "Gallon/Square Inch", NamePluralUS = "Gallons/Square Inch", Abbreviation = "gal/sq in", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 3.785411784m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.GallonPerSquareFoot, new UnitInfo() { Unit = Unit.GallonPerSquareFoot, NameUS = "Gallon/Square Foot", NamePluralUS = "Gallons/Square Foot", Abbreviation = "gal/sq ft", Aliases = "gpf", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 545.0992969m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.GallonPerSquareMillimeter, new UnitInfo() { Unit = Unit.GallonPerSquareMillimeter, NameUS = "Gallon/Square Millimeter", NamePluralUS = "Gallons/Square Millimeter", Abbreviation = "gal/sq mm", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 2442.196267m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.GallonPerSquareMeter, new UnitInfo() { Unit = Unit.GallonPerSquareMeter, NameUS = "Gallon/Square Meter", NamePluralUS = "Gallons/Square Meter", Abbreviation = "gal/sq m", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 5867.4m, NameUK = "", NamePluralUK = "", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MilliliterPerSquareInch, new UnitInfo() { Unit = Unit.MilliliterPerSquareInch, NameUS = "Milliliter/Square Inch", NamePluralUS = "Milliliters/Square Inch", Abbreviation = "ml/sq in", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 0.001m, NameUK = "Milliliter/Square Inch", NamePluralUK = "Milliliters/Square Inch", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MilliliterPerSquareFoot, new UnitInfo() { Unit = Unit.MilliliterPerSquareFoot, NameUS = "Milliliter/Square Foot", NamePluralUS = "Milliliters/Square Foot", Abbreviation = "ml/sq ft", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 0.144m, NameUK = "Milliliter/Square Foot", NamePluralUK = "Milliliters/Square Foot", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.MilliliterPerSquareMillimeter, new UnitInfo() { Unit = Unit.MilliliterPerSquareMillimeter, NameUS = "Milliliter/Square Millimeter", NamePluralUS = "Milliliters/Square Millimeter", Abbreviation = "ml/sq mm", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 0.64516m, NameUK = "Milliliter/Square Millimeter", NamePluralUK = "Milliliters/Square Millimeter", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.MilliliterPerSquareMeter, new UnitInfo() { Unit = Unit.MilliliterPerSquareMeter, NameUS = "Milliliter/Square Meter", NamePluralUS = "Milliliters/Square Meter", Abbreviation = "ml/sq m", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 1.5500031m, NameUK = "Milliliter/Square Meter", NamePluralUK = "Milliliters/Square Meter", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.LiterPerSquareInch, new UnitInfo() { Unit = Unit.LiterPerSquareInch, NameUS = "Liter/Square Inch", NamePluralUS = "Liters/Square Inch", Abbreviation = "L/sq in", Aliases = "", UnitType = UnitType.Coverage, IsStandard = true, ToStandardMultiplyBy  = 1m, NameUK = "Liter/Square Inch", NamePluralUK = "Liters/Square Inch", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.LiterPerSquareFoot, new UnitInfo() { Unit = Unit.LiterPerSquareFoot, NameUS = "Liter/Square Foot", NamePluralUS = "Liters/Square Foot", Abbreviation = "L/sq ft", Aliases = "lpf", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 144m, NameUK = "Liter/Square Foot", NamePluralUK = "Liters/Square Foot", UnitSystem = SystemOfUnits.Imperial  } },
                {  Unit.LiterPerSquareMillimeter, new UnitInfo() { Unit = Unit.LiterPerSquareMillimeter, NameUS = "Liter/Square Millimeter", NamePluralUS = "Liters/Square Millimeter", Abbreviation = "L/sq mm", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 645.16m, NameUK = "Liter/Square Millimeter", NamePluralUK = "Liters/Square Millimeter", UnitSystem = SystemOfUnits.Metric  } },
                {  Unit.LiterPerSquareMeter, new UnitInfo() { Unit = Unit.LiterPerSquareMeter, NameUS = "Liter/Square Meter", NamePluralUS = "Liters/Square Meter", Abbreviation = "L/sq m", Aliases = "", UnitType = UnitType.Coverage, IsStandard = false, ToStandardMultiplyBy  = 1550.0031m, NameUK = "Liter/Square Meter", NamePluralUK = "Liters/Square Meter", UnitSystem = SystemOfUnits.Metric  } }

            };
        }

        public static Unit? UnitByName(string UnitName)
        {
            return UnitInfoByName(UnitName)?.Unit;
        }

        public static UnitInfo UnitInfoByName(string UnitName)
        {
            return InfoList.FirstOrDefault(u => u.Names.Contains(UnitName, new CaseInsensitiveComparer()));
        }
        public static UnitInfo UnitInfoByUnit(Unit unit)
        {
            return InfoList.FirstOrDefault(u => u.Unit == unit);
        }
    }

    public class CaseInsensitiveComparer : IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
        {
            return x.Equals(y, StringComparison.CurrentCultureIgnoreCase);
        }

        public int GetHashCode(string obj)
        {
            return obj.GetHashCode();
        }
    }



}
