﻿
namespace Endor.Models
{
    public class SystemAutomationActionDataTypeLink
    {
        public byte ActionID { get; set; }

        public DataType DataType { get; set; }

        public SystemAutomationActionDefinition ActionDefinition { get; set; }
    }
}
