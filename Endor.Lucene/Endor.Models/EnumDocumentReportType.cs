﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    /// <summary>
    /// Enum indicating the type of DocumentReportType
    /// </summary>
    public enum DocumentReportType : byte
    {
        None = 0,
        Estimate = 1,
        WorkOrder = 2,
        Invoice = 3,
        PackingSlip = 4,
        Proposal = 11,
        Statement = 21,
        PurchaseOrder = 91
    }
}
