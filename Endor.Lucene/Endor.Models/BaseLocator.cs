﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public abstract class BaseLocator<I, P> : IImageCandidate, IAtom<int>, IChild<I, P> where I : struct, IConvertible where P : IAtom<I>
    {
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }

        [StringLength(1024)]
        public string Locator { get; set; }
        [Required]
        [StringLength(1024)]
        public string RawInput { get; set; }

        /// <summary>
        /// XML MetaData. There is no performance cost for accessing this property
        /// </summary>
        [JsonIgnore]
        public string MetaDataXML { get; set; }

        //it's possible this could work, but things get tricky with state management
        //right now, MetaDataXML is the source of truth, if you add a third layer, is it memory objects or the XML that is true?
        //you'd have to dirty check the in memory object, and that's not something I want to deal with right now
        //public LocatorMetaData MetaData { get; set; }

        /// <summary>
        /// Deserializes or serialzes MetaData of type LocatorMetaData or some child type on get or set. CPU INTENSIVE, CACHE AGGRESSIVELY
        /// </summary>
        [JsonProperty("MetaData")]
        public object MetaDataObject
        {
            get
            {
                return XMLUtilities.DeserializeMetaDataFromXML(this.MetaDataXML, this.eLocatorType);
            }
            set
            {
                this.MetaDataXML = XMLUtilities.SerializeMetaDataToXML(value, this.eLocatorType);
            }
        }

        /// <summary>
        /// The locator type (phone, address, etc)
        /// </summary>
        [JsonProperty(Order = -2)] //default order is -1, we want to serialize this first so we can use it in MetaDataObject
        public byte LocatorType { get; set; }
        public LocatorType eLocatorType { get { return (LocatorType)this.LocatorType; } }
        public virtual EnumLocatorType LocatorTypeNavigation { get; set; }

        public byte LocatorSubType { get; set; }
        public virtual EnumLocatorSubType LocatorSubTypeNavigation { get; set; }
        
        public short SortIndex { get; set; }
        public bool IsVerified { get; set; }
        public bool IsValid { get; set; }
        public bool HasImage { get; set; }

        public abstract I ParentID { get; set; }
        public abstract P Parent { get; set; }
    }
}
