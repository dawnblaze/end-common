﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class SystemListFilterCriteria
    {
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public int? TargetClassTypeID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Label { get; set; }
        [StringLength(255)]
        public string Field { get; set; }
        public bool IsHidden { get; set; }
        public short DataType { get; set; }
        public byte InputType { get; set; }
        public bool AllowMultiple { get; set; }
        public string ListValues { get; set; }
        [StringLength(255)]
        public string ListValuesEndpoint { get; set; }
        public bool? IsLimitToList { get; set; }
        public byte? SortIndex { get; set; }
        public bool IsVisible { get; set; }
    }
}
