﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class EnumLocatorSubType
    {
        public EnumLocatorSubType()
        {
        }

        public byte LocatorType { get; set; }
        public byte ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(255)]
        public string LinkFormatString { get; set; }        
    }
}
