﻿namespace Endor.Models
{
    public partial class BusinessLocator : BaseLocator<short, BusinessData>
    {
        public override short ParentID { get; set; }
        
        public override EnumLocatorSubType LocatorSubTypeNavigation { get; set; }
        public override EnumLocatorType LocatorTypeNavigation { get; set; }
        public override BusinessData Parent { get; set; }
    }
}
