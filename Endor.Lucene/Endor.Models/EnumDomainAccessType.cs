﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum DomainAccessType : byte
    {
        Business = 1,
        CustomerPortal = 2,
        ECommerce = 3,
        VendorPortal = 4
    }

    public partial class EnumDomainAccessType
    {
        public DomainAccessType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
