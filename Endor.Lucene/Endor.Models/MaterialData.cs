﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Endor.Models
{
    public class MaterialData : IAtom<int>, IImageCandidate
    {
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public string InvoiceText { get; set; }
        public string Description { get; set; }
        public bool HasImage { get; set; }
        public string SKU { get; set; }
        public int IncomeAccountID { get; set; }
        public int ExpenseAccountID { get; set; }
        public bool TrackInventory { get; set; }
        public int InventoryAccountID { get; set; }
        public MaterialConsumptionMethod EstimatingConsumptionMethod { get; set; }
        public Unit ConsumptionUnit { get; set; }
        public MaterialCostingMethod EstimatingCostingMethod { get; set; }
        public decimal? EstimatingCost { get; set; }
        public decimal? EstimatingPrice { get; set; }
        public string Color { get; set; }
        public MaterialPhysicalType PhysicalMaterialType { get; set; }

        private decimal? GetMeasurementValue(Measurement measurement)
        {
            if (measurement == null)
                return null;

            return measurement.Value;
        }
        private Measurement SetMeasurementValue(Measurement measurement, decimal? value)
        {
            if (!value.HasValue && (measurement == null || measurement.Unit == Unit.None))
                return null;

            if (measurement == null)
                return new Measurement(value, Unit.None);

            measurement.Value = value;
            return measurement;
        }

        private Unit? GetMeasurementUnit(Measurement measurement)
        {
            if (measurement == null)
                return null;

            return measurement.Unit;
        }
        private Measurement SetMeasurementUnit(Measurement measurement, Unit? unit)
        {
            if (unit.GetValueOrDefault(Unit.None) == Unit.None && (measurement == null || !measurement.Value.HasValue))
                return null;

            if (measurement == null)
                return new Measurement(null, unit.GetValueOrDefault(Unit.None));

            measurement.Unit = unit.GetValueOrDefault(Unit.None);
            return measurement;
        }

        public Measurement Height { get; set; }
        [JsonIgnore]
        public decimal? _HeightValue
        {
            get => GetMeasurementValue(Height);
            set => Height = SetMeasurementValue(Height, value);
        }
        [JsonIgnore]
        public Unit? _HeightUnit
        {
            get => GetMeasurementUnit(Height);
            set => Height = SetMeasurementUnit(Height, value);
        }

        public Measurement Width { get; set; }
        [JsonIgnore]
        public decimal? _WidthValue
        {
            get => GetMeasurementValue(Width);
            set => Width = SetMeasurementValue(Width, value);
        }
        [JsonIgnore]
        public Unit? _WidthUnit
        {
            get => GetMeasurementUnit(Width);
            set => Width = SetMeasurementUnit(Width, value);
        }

        public Measurement Length { get; set; }
        [JsonIgnore]
        public decimal? _LengthValue
        {
            get => GetMeasurementValue(Length);
            set => Length = SetMeasurementValue(Length, value);
        }
        [JsonIgnore]
        public Unit? _LengthUnit
        {
            get => GetMeasurementUnit(Length);
            set => Length = SetMeasurementUnit(Length, value);
        }

        public Measurement Weight { get; set; }
        [JsonIgnore]
        public decimal? _WeightValue
        {
            get => GetMeasurementValue(Weight);
            set => Weight = SetMeasurementValue(Weight, value);
        }
        [JsonIgnore]
        public Unit? _WeightUnit
        {
            get => GetMeasurementUnit(Weight);
            set => Weight = SetMeasurementUnit(Weight, value);
        }

        public Measurement Volume { get; set; }
        [JsonIgnore]
        public decimal? _VolumeValue
        {
            get => GetMeasurementValue(Volume);
            set => Volume = SetMeasurementValue(Volume, value);
        }
        [JsonIgnore]
        public Unit? _VolumeUnit
        {
            get => GetMeasurementUnit(Volume);
            set => Volume = SetMeasurementUnit(Volume, value);
        }

        public byte? QuantityInSet { get; set; }
        public UnitType UnitType { get; set; }
        public decimal RoundingFactor { get; set; }
        public decimal MinimumQuantity { get; set; }

        public short? TaxCodeID { get; set; }
        public TaxabilityCode TaxabilityCode { get; set; }

        //navigation
        public GLAccount IncomeAccount { get; set; }
        public GLAccount ExpenseAccount { get; set; }
        public GLAccount InventoryAccount { get; set; }

        public ICollection<MaterialCategoryLink> MaterialCategoryLinks { get; set; }

        public ICollection<MaterialCategory> MaterialCategories { get; set; }
        public ICollection<SimpleMaterialCategory> SimpleMaterialCategories { get; set; }
    }
}
