﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public class MachineProfileVariable : IAtom<short>
    {
        /// <summary>
        /// The Business ID for this Machine Profile Variable.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }
        /// <summary>
        /// The ID of this Machine Profile Variable(Unique within the Business).
        /// </summary>
        public short ID { get; set; }
        /// <summary>
        /// (Read Only) An ID identifying the object as being a Machine Profile Variable.  Always 12036.
        /// </summary>
        public int ClassTypeID { get; set; }
        /// <summary>
        /// (Read Only) The date time the Machine Profile Variablewas last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }
        /// <summary>
        /// The ID of the Profile record linked to this Machine Profile Variable.
        /// </summary>
        public int ProfileID { get; set; }
        /// <summary>
        /// The ID of the Variable record linked to this Machine Profile Variable.
        /// </summary>
        public int VariableID { get; set; }
        /// <summary>
        /// The OverrideDefault value for this Machine Profile Variable.
        /// </summary>
        public bool OverrideDefault { get; set; }
        /// <summary>
        /// The Name value for this Machine Profile Variable.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The Label value for this Machine Profile Variable.
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// The Tooltip value for this Machine Profile Variable.
        /// </summary>
        public string Tooltip { get; set; }
        /// <summary>
        /// The DefaultValue value for this Machine Profile Variable.
        /// </summary>
        public string DefaultValue { get; set; }
        /// <summary>
        /// The IsRequired value for this Machine Profile Variable.
        /// </summary>
        public bool? IsRequired { get; set; }
        /// <summary>
        /// The IsDisabled value for this Machine Profile Variable.
        /// </summary>
        public bool? IsDisabled { get; set; }
        /// <summary>
        /// The ID of the Assembly record linked to this Machine Profile Variable.
        /// </summary>
        public int AssemblyID { get; set; }
        /// <summary>
        /// The ListValuesJSON value for this Machine Profile Variable.
        /// </summary>
        public string ListValuesJSON { get; set; }
        /// <summary>
        /// The ID of the Unit record linked to this Machine Profile Variable.
        /// </summary>
        public byte? UnitID { get; set; }
        /// <summary>
        /// The GroupOptionsByCategory value for this Machine Profile Variable.
        /// </summary>
        public bool? GroupOptionByCategory { get; set; }
        /// <summary>
        /// The DataType of the variable
        /// </summary>
        public DataType? DataType { get; set; }
        /// <summary>
        /// For List Elements, this field indicates the DataType of the list.  This is a subset of the full DataType and is used for referential integrity.
        /// </summary>
        public DataType? ListDataType { get; set; }
        /// <summary>
        /// ID of the linked Material 
        /// </summary>
        public int? LinkedMaterialID { get; set; }
        /// <summary>
        /// ID of the linked Labor
        /// </summary>
        public int? LinkedLaborID { get; set; }
        /// <summary>
        /// Link to the AssemblyVariable Object whose values this table Overrides. 
        /// </summary>
        [JsonIgnore]
        public AssemblyVariable Variable { get; set; }
    }
}
