﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    [Flags]
    public enum OrderTransactionType : byte
    {
        Estimate = 1,
        Order = 2,
        PurchaseOrder = 4,
        Memo = 8,
        Opportunity = 32,
        Destination = 64,
    }

    public class EnumOrderTransactionType
    {
        public byte ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
