﻿namespace Endor.Models
{
    public partial class EmployeeTeamLocationLink
    {
        public short BID { get; set; }
        public int TeamID { get; set; }
        public byte LocationID { get; set; }

        public EmployeeTeam EmployeeTeam { get; set; }
        public LocationData Location { get; set; }
    }
}
