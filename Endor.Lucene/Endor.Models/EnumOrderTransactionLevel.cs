﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    [Flags]
    public enum OrderTransactionLevel : byte
    {
        Header = 1,
        Destination = 2,
        LineItem = 4,
        Part = 8,
        MasterHeader = 32,
        LinkedHeader = 64
    }

    public class EnumOrderTransactionLevel
    {
        public OrderTransactionLevel ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
