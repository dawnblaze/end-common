﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    [Flags]
    public enum CompanyStatus: byte
    {
        Lead = 1,
        Prospect = 2,
        Customer = 4,
        Vendor = 8,
        Personal = 16,

        LeadAndVendor = CompanyStatus.Lead | CompanyStatus.Vendor,
        ProspectAndVendor = CompanyStatus.Prospect | CompanyStatus.Vendor,
        CustomerAndVendor = CompanyStatus.Customer | CompanyStatus.Vendor,

        LeadAndPersonal = CompanyStatus.Lead | CompanyStatus.Personal,
        ProspectAndPersonal = CompanyStatus.Prospect | CompanyStatus.Personal,
        CustomerAndPersonal = CompanyStatus.Customer | CompanyStatus.Personal,
        VendorAndPersonal = CompanyStatus.Vendor | CompanyStatus.Personal,

        LeadAndVendorAndPersonal = CompanyStatus.Lead | CompanyStatus.Vendor | CompanyStatus.Personal,
        ProspectAndVendorAndPersonal = CompanyStatus.Prospect | CompanyStatus.Vendor | CompanyStatus.Personal,
        CustomerAndVendorAndPersonal = CompanyStatus.Customer | CompanyStatus.Vendor | CompanyStatus.Personal
    }

    public partial class EnumCrmCompanyStatus
    {
        public EnumCrmCompanyStatus()
        {
        }

        public byte ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

    }
}
