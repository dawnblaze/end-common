﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class EnumGLAccountType
    {
        public byte ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public byte? ParentType { get; set; }
    }
}
