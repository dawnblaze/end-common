﻿namespace Endor.Models
{
    public enum EmailSecurityType : byte
    {
        None = 0,
        SSL = 1,
        TLS = 2
    }
}
