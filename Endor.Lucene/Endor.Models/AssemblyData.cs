﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class AssemblyData : IAtom<int>, IImageCandidate
    {
        /// <summary>
        /// The Business ID for the Assembly.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID for the Assembly.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 12040.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if the Assembly is Active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// The name of the assembly
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// The InvoiceText of the Assembly
        /// </summary>
        [StringLength(255)]
        public string InvoiceText { get; set; }
        /// <summary>
        /// The description of the Assembly
        /// </summary>
        [StringLength(255)]
        public string Description { get; set; }

        /// <summary>
        /// Flag indicating if a unique image is stored in DM for this object.
        /// <para>If False, the default image for the classtype is used instead.</para>
        /// </summary>
        public bool HasImage { get; set; }

        /// <summary>
        /// The internal part SKU.  Used for searching.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SKU { get; set; }

        /// <summary>
        /// The AssemblyPricingType Enum for this object.
        /// </summary>
        public AssemblyPricingType PricingType { get; set; }

        /// <summary>
        /// The TaxabilityCodeID for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? TaxabilityCodeID { get; set; }

        /// <summary>
        /// The IncomeAllocationType for this object.
        /// </summary>
        public AssemblyIncomeAllocationType IncomeAllocationType { get; set; }

        /// <summary>
        /// The IncomeAccountID for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? IncomeAccountID { get; set; }

        /// <summary>
        /// The default price when using Market Based Pricing.
        /// Null when using other pricing methods.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? FixedPrice { get; set; }

        /// <summary>
        /// The default margin when using Margin Base Pricing.
        /// Null when using other pricing methods.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? FixedMargin { get; set; }

        /// <summary>
        /// The default markup when using Markup Based Pricing.
        /// Null when using other pricing methods.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? FixedMarkup { get; set; }

        /// <summary>
        /// Flag indicating if a tier table is automatically created.
        /// The type of tier table will depend on the PricingType and PriceFormulaType.
        /// </summary>
        public bool HasTierTable { get; set; }

        public MachineLayoutType MachineLayoutTypes { get; set; }

        /// <summary>
        /// The PriceFormulaType Enum for this Assembly.  It determines the basis of the default formula.
        /// </summary>
        public PriceFormulaType PriceFormulaType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyCategoryLink> AssemblyCategoryLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyCategory> AssemblyCategories { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleAssemblyCategory> SimpleAssemblyCategories { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyVariable> Variables { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyLayout> Layouts { get; set; }

        /// <summary>
        /// A nested collection of AssemblyTable Objects on the Assembly.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyTable> Tables { get; set; }

        /// <summary>
        /// The AssemblyType for this object.
        /// </summary>
        public AssemblyType AssemblyType { get; set; }

        /// <summary>
        /// Indicates if the Ship By Date is enabled for the Destination 
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool EnableShipByDate { get; set; }

        /// <summary>
        /// Indicates if Arrive By Date is enabled for the Destination
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool EnableArriveByDate { get; set; }

        /// <summary>
        /// Indicates if FromAddress is enabled for the Destination
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool EnableFromAddress { get; set; }

        /// <summary>
        /// Indicates if ToAddress is enabled for the Destination
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool EnableToAddress { get; set; }

        /// <summary>
        /// Indicates if Blind Shipping is enabled for the Destination
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool EnableBlindShipping { get; set; }

        /// <summary>
        /// Indicates if the Package Tab is enabled for the Destination
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool EnablePackagesTab { get; set; }

        /// <summary>
        /// Specifies alternate Label Text for the ShipByDate
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ShipByDateLabel { get; set; }

        /// <summary>
        /// Specifies the alternate Label Text for the ArriveByDate
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ArriveByDateLabel { get; set; }

        /// <summary>
        /// Specifies the Default Label Text for the From Address
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string FromAddressLabel { get; set; }

        /// <summary>
        /// Specifies the Default Label Text for the To Address
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ToAddressLabel { get; set; }

        /// <summary>
        /// Specifies the Default Label Text for the Blind Shipping Address
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string BlindShippingLabel { get; set; }

    }
}
