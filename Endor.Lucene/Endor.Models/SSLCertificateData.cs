﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class SSLCertificateData : IAtom<short>
    {
        public SSLCertificateData()
        {

        }

        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        [Required]
        [StringLength(255)]
        public string CommonName { get; set; }
        public bool CanHaveMultipleDomains { get; set; }
        public DateTime? InstalledDT { get; set; }
        [Required]
        [StringLength(255)]
        public string FileName { get; set; }
        public DateTime ValidFromDT { get; set; }
        public DateTime ValidToDT { get; set; }
        [Required]
        [StringLength(255)]
        public string Thumbprint { get; set; }
    }
}
