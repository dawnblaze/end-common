﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class OrderOrderLink
    {
        ///<summary>
        /// The Business ID for this record.
        ///</summary>
        [JsonIgnore]
        public short BID { get; set; }

        ///<summary>
        ///The Order Object for this object.
        ///</summary>
        public int OrderID { get; set; }

        ///<summary>
        ///The OrderOrderLinkType Enum for this object.
        ///</summary>        
        public OrderOrderLinkType LinkType { get; set; }

        ///<summary>
        ///The linked Order Object.
        ///</summary>
        public int LinkedOrderID { get; set; }

        ///<summary>
        ///"(Read Only) The date time the record was last modified.
        /// Updated by the system on every save or action."
        /// </summary>
        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }

        ///<summary>
        ///(Read Only) The date time this record will no longer be valid. This field is not used in this table but is used in the History table associated with this table.
        ///The Formatted Number (e.g. ORD-1234) for the linked Object.
        ///</summary>
        [StringLength(12)]
        public string LinkedFormattedNumber { get; set; }

        ///<summary>
        ///The Description of the link (e.g. "Order Cloned from EST-1234")
        ///</summary>
        [StringLength(255)]
        public string Description { get; set; }

        ///<summary>
        ///The Amount associated with this link. This is used for certain credit transfers and credit memos.
        ///</summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Amount { get; set; }

        ///<summary>
        ///The ID of the Employee establishing the link.
        ///</summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? EmployeeID { get; set; }

        ///<summary>
        /// Primary Order two way navigation
        ///</summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TransactionHeaderData Order { get; set; }

        /// <summary>
        /// Related order one way navigation
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TransactionHeaderData LinkedOrder { get; set; }

        /// <summary>
        /// Optional Employee navigation for who connected the orders
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData Employee { get; set; }
    }
}
