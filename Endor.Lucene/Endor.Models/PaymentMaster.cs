﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class PaymentMaster : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }
        //This column is always "Infinite" in the live table and get the current DT when moved to the archive table. 
        //That's how it keeps track when it is no longer the current record
        [JsonIgnore]
        public DateTime ValidToDT { get; set; }
        public byte LocationID { get; set; }
        public byte ReceivedLocationID { get; set; }
        public DateTime AccountingDT { get; set; }
        public decimal Amount { get; set; }
        public byte? CurrencyType { get; set; }
        public byte PaymentTransactionType { get; set; }
        [StringLength(50)]
        public string DisplayNumber { get; set; }
        public int CompanyID { get; set; }
        public decimal RefBalance { get; set; }
        public decimal? NonRefBalance { get; set; }
        public decimal? ChangeInRefCredit { get; set; }
        public decimal? ChangeInNonRefCredit { get; set; }
        [StringLength(100)]
        public string Notes { get; set; }

        public EnumAccountingCurrencyType CurrencyTypeNavigation { get; set; }
        public EnumAccountingPaymentTransactionType PaymentTransactionTypeNavigation { get; set; }
        public LocationData Location { get; set; }
        public CompanyData Company { get; set; }

        public ICollection<PaymentApplication> PaymentApplications { get; set; }

    }
}
