﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class ReportMenu : IAtom<short>
    {
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public short ReportID { get; set; }
        [Required]
        [StringLength(255)]
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string ReportOptions { get; set; }
        public short? ParentID { get; set; }
        public short SortIndex { get; set; }
        public short? DefaultMenuID { get; set; }
        public bool IsOv { get; set; }
    }
}
