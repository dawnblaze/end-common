﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public class BoardViewLink
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short BoardID { get; set; }

        public short ViewID { get; set; }

        public byte SortIndex { get; set; }


        public bool? IsPrimary { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BoardView BoardView { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BoardDefinitionData BoardDefinitionData { get; set; }
    }
}
