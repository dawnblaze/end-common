﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class SimpleCustomFieldLayoutDefinition: SimpleListItem<short>
    {
        public short AppliesToClassTypeID { get; set; }
    }
}
