﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace Endor.Models
{

    public partial class GLData : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }
        public DateTime AccountingDT { get; set; }
        public DateTime RecordedDT { get; set; }

        public byte LocationID { get; set; }
        public int GLAccountID { get; set; }

        public byte CurrencyType { get; set; }
        public EnumAccountingCurrencyType CurrencyTypeNavigation { get; set; }

        public decimal Amount { get; set; }
        [JsonProperty("Debit", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Debit { get; set; }
        [JsonProperty("Credit", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Credit { get; set; }
        [JsonProperty("IsOffBS", DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(false)]
        public bool IsOffBS { get; set; }
        [JsonProperty("IsTaxable", NullValueHandling = NullValueHandling.Ignore, DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(false)]
        public bool? IsTaxable { get; set; }

        [JsonProperty("TaxGroupID", NullValueHandling = NullValueHandling.Ignore)]
        public short? TaxGroupID { get; set; }
        public int ActivityID { get; set; }

        [JsonProperty("ReconciliationID", NullValueHandling = NullValueHandling.Ignore)]
        public int? ReconciliationID { get; set; }
        [JsonProperty("CompanyID", NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }
        [JsonProperty("OrderID", NullValueHandling = NullValueHandling.Ignore)]
        public int? OrderID { get; set; }
        [JsonProperty("OrderItemID", NullValueHandling = NullValueHandling.Ignore)]
        public int? OrderItemID { get; set; }
        [JsonProperty("DestinationID", NullValueHandling = NullValueHandling.Ignore)]
        public int? DestinationID { get; set; }
        [JsonProperty("ItemComponentID", NullValueHandling = NullValueHandling.Ignore)]
        public int? ItemComponentID { get; set; }
        [JsonProperty("MaterialID", NullValueHandling = NullValueHandling.Ignore)]
        public int? MaterialID { get; set; }
        [JsonProperty("LaborID", NullValueHandling = NullValueHandling.Ignore)]
        public int? LaborID { get; set; }
        [JsonProperty("MachineID", NullValueHandling = NullValueHandling.Ignore)]
        public short? MachineID { get; set; }
        [JsonProperty("AssemblyID", NullValueHandling = NullValueHandling.Ignore)]
        public int? AssemblyID { get; set; }
        [JsonProperty("PlaceID", NullValueHandling = NullValueHandling.Ignore)]
        public int? PlaceID { get; set; }
        [JsonProperty("AssetID", NullValueHandling = NullValueHandling.Ignore)]
        public int? AssetID { get; set; }
        [JsonProperty("TaxItemID", NullValueHandling = NullValueHandling.Ignore)]
        public short? TaxItemID { get; set; }
        [JsonProperty("PaymentMethodID", NullValueHandling = NullValueHandling.Ignore)]
        public int? PaymentMethodID { get; set; }
        [JsonProperty("PaymentID", NullValueHandling = NullValueHandling.Ignore)]
        public int? PaymentID { get; set; }
        [JsonProperty("ItemSurchargeID", NullValueHandling = NullValueHandling.Ignore)]
        public int? ItemSurchargeID { get; set; }
        [JsonProperty("TaxCodeID", NullValueHandling = NullValueHandling.Ignore)]
        public short? TaxCodeID { get; set; }

        public LocationData Location { get; set; }
        public GLAccount GLAccount { get; set; }
        public TaxGroup TaxGroup { get; set; }
        public ActivityGlactivity GLActivity { get; set; }
        public CompanyData Company { get; set; }
        public OrderData Order { get; set; }
        public OrderItemData OrderItem { get; set; }
        public OrderItemComponent OrderItemComponent { get; set; }
        public OrderDestinationData OrderDestination { get; set; }
        public MaterialData Material { get; set; }
        public AssemblyData Assembly { get; set; }
        public LaborData Labor { get; set; }
        public MachineData Machine { get; set; }
        public TaxItem TaxItem { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public PaymentApplication Payment { get; set; }
        public OrderItemSurcharge ItemSurcharge { get; set; }
        public TaxabilityCode TaxCode { get; set; }

    }
}
