﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    /// <summary>
    /// Defines an individual Tax Assessment Item.
    /// </summary>
    public class TaxAssessmentItemResult
    {
        // for Online tax lookup, the ID is NULL
        public short? TaxItemID { get; set; }

        // The text displayed for this tax assessment on the invoice
        public string InvoiceText { get; set; }

        // The Tax Rate, stored as a percent  (e.g 7.25% would be 7.25)
        public decimal TaxRate { get; set; }

        // The Amount of taxes computed = TaxRate * PriceTaxable
        public decimal TaxAmount { get; set; }

        // The GLAccount ID
        public int GLAccountID { get; set; }
    }
}
