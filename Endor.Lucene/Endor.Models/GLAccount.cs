﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class GLAccount : IAtom<int>
    {
        public GLAccount()
        {
            PaymentMethods = new HashSet<PaymentMethod>();
        }

        [JsonIgnore]
        public short BID { get; set; }

        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }
        [Required]
        public bool IsActive { get; set; }

        [Required]
        public bool CanEdit { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public int? Number { get; set; }

        public byte GLAccountType { get; set; }

        public int? ParentID { get; set; }

        [StringLength(255)]
        public string ExportAccountName { get; set; }

        [StringLength(255)]
        public string ExportAccountNumber { get; set; }

        public byte? ExportGLAccountType { get; set; }

        [StringLength(255)]
        public string NumberedName { get; set; }

        public bool? IsAsset { get; set; }

        public bool? IsLiability { get; set; }

        public bool? IsEquity { get; set; }

        public bool? IsIncome { get; set; }

        public bool? IsCOGS { get; set; }

        public bool? IsExpense { get; set; }

        public GLAccount ParentGLAccount { get; set; }

        public ICollection<GLAccount> Subaccounts { get; set; }

        public EnumGLAccountType GLAccountTypeNavigation { get; set; }

        public EnumGLAccountType ExportGLAccountTypeNavigation { get; set; }

        [JsonIgnore]
        public TaxItem TaxItem { get; set; }

        [JsonIgnore]
        public ICollection<PaymentMethod> PaymentMethods { get; set; }

        [JsonIgnore]
        public ICollection<MachineData> IncomeMachines { get; set; }

        [JsonIgnore]
        public ICollection<MachineData> ExpenseMachines { get; set; }

        [JsonIgnore]
        public ICollection<LaborData> IncomeLabors { get; set; }

        [JsonIgnore]
        public ICollection<LaborData> ExpenseLabors { get; set; }
    }
}
