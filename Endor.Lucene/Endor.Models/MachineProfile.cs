﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Endor.Models
{
    public class MachineProfile : IAtom<int>
    {
        public int ID { get; set; }
        [JsonIgnore]
        public short BID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public string Name { get; set; }
        public string AssemblyOVDataJSON { get; set; }

        public short MachineID { get; set; }

        [JsonIgnore]
        public MachineData Machine { get; set; }

        public int MachineTemplateID { get; set; }  

        [JsonIgnore]
        public AssemblyData MachineTemplate { get; set; }

        public MachineLayoutType MachineLayoutType { get; set; }

        public ICollection<MachineProfileTable> MachineProfileTables { get; set; }

        public ICollection<MachineProfileVariable> MachineProfileVariables { get; set; }

    }
}
