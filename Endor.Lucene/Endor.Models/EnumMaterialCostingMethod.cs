﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum MaterialCostingMethod : byte
    {
        Manual = 0,
        PreferredVendorCost = 1,
        AverageInventoryCost = 2,
        FIFO = 3
    }

    public class EnumMaterialCostingMethod
    {
        public MaterialCostingMethod ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
