﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class AssemblyTable : IAtom<short>
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID of this object (Unique within the Business).
        /// </summary>
        public short ID { get; set; }

        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 12060.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// (Read Only) The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The ID of the Assembly Object this Table applies to.
        /// </summary>
        public int AssemblyID { get; set; }

        /// <summary>
        /// The Label for this Table.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Label { get; set; }

        /// <summary>
        /// The Variable Name (aka Reference Key) of this Table used in Pricing formulas.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string VariableName { get; set; }

        /// <summary>
        /// The Description of this Table.
        /// </summary>
        [StringLength(4000)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        /// <summary>
        /// The AssemblyTableType Enum for this table.
        /// </summary>
        public AssemblyTableType TableType { get; set; }

        /// <summary>
        /// Flag indicating if this is a Tier Table.  An assembly has one (and only one) Tier Table.  For Tier Tables, the RowVariableID is always XXX, the Tier Variable ID.
        /// </summary>
        public bool IsTierTable { get; set; }

        /// <summary>
        /// The Row Heading to display, normally the Row Variable Label.
        /// </summary>
        [StringLength(255)]
        public string RowLabel { get; set; }

        /// <summary>
        /// The ID of the AssemblyVariable Object used for the Row.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? RowVariableID { get; set; }

        /// <summary>
        /// the temp ID of the row variable
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string RowVariableTempID { get; set; }

        /// <summary>
        /// The DataType Enum of the row, which is the DataType of the the AssemblyVariable Object used for the Row.
        /// </summary>
        public DataType RowDataType { get; set; }

        /// <summary>
        /// The Unit Enum of the row, which is the unit of the the AssemblyVariable Object used for the Row.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Unit? RowUnitID { get; set; }

        /// <summary>
        /// The number of rows in the table.  Must be >= 1.
        /// </summary>
        public short RowCount { get; set; }

        /// <summary>
        /// The Value of all of the Row Headers for this Table, stored as a JSON array of objects.  (e.g.   ["Default", "Tier 1", "MVP"] )
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string RowValuesJSON { get; set; }

        /// <summary>
        /// For Tier Tables, this field is used to store the value of any Default Markup values for the Row. 
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string RowDefaultMarkupJSON { get; set; }

        /// <summary>
        /// Flag indicating if the Row Is Sorted AlphNumerically.
        /// </summary>
        public bool RowIsSorted { get; set; }

        /// <summary>
        /// The AssemblyTableMatchType Enum which indicates how non-exact matches are handled when looking up the Row to use.
        /// </summary>
        public AssemblyTableMatchType RowMatchType { get; set; }

        /// <summary>
        /// The Column Heading to display, normally the Column Variable Label.
        /// </summary>
        [StringLength(255)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ColumnLabel { get; set; }

        /// <summary>
        /// The ID of the AssemblyVariable Object used for the Column.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ColumnVariableID { get; set; }

        /// <summary>
        /// the temp ID of the column variable
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ColumnVariableTempID { get; set; }

        /// <summary>
        /// The DataType Enum of the row, which is the DataType of the the AssemblyVariable Object used for the Column.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DataType? ColumnDataType { get; set; }

        /// <summary>
        /// The Unit Enum of the row, which is the unit of the the AssemblyVariable Object used for the Column.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Unit? ColumnUnitID { get; set; }

        /// <summary>
        /// The number of columns in the table.  Must be >= 1.
        /// </summary>
        public short ColumnCount { get; set; }

        /// <summary>
        /// The Value of all of the Column Headers for this Table, stored as a JSON array. (e.g.   [1, 2, 4, 99] )
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ColumnValuesJSON { get; set; }

        /// <summary>
        /// Flag indicating if the Column Is Sorted AlphNumerically.
        /// </summary>
        public bool ColumnIsSorted { get; set; }

        /// <summary>
        /// The AssemblyTableMatchType Enum which indicates how non-exact matches are handled when looking up the Column to use.  
        /// </summary>
        public AssemblyTableMatchType ColumnMatchType { get; set; }

        /// <summary>
        /// The DataType Enum that the Table returns.  This is also used as the DataType of the individual cells.
        /// </summary>
        public DataType CellDataType { get; set; }

        /// <summary>
        /// The Value of all of the Cells for this Table, stored as a JSON  array or arrays. (e.g.   [ [1, 2, 4, 99], [3, 5, 6, 101], [5, 7, 9, 103] ] )
        /// </summary>
        public string CellDataJSON { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<MachineProfileTable> MachineProfileTables { get; set; }

    }
}
