﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum AssemblyFormulaUseType : byte
    {
        DefaultValue = 0,
        Consumption = 1,
        IsEnabled = 2,
        IsRequired = 3,
        IsVisible = 4,
        IsLinked = 10,
        LinkedVariableOV = 11
    }

    public class EnumAssemblyFormulaUseType
    {
        public AssemblyFormulaUseType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }

}