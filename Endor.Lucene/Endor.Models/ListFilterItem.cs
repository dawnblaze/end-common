﻿namespace Endor.Models
{
    public class ListFilterItem
    {
        /// <summary>
        /// The name of the filter.  This is used as the Label.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// The data field associated with the filter.
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// The value the user entered to be searched for.
        /// </summary>
        public string SearchValue { get; set; }

        /// <summary>
        /// The display text to show for this filter in the RecordListFilter component.
        /// </summary>
        public string DisplayText { get; set; }

        /// <summary>
        /// A unique ID used to combine identical FilterItems in a list (e.g., When 3 Salespeople are added as Filters).  By default, FilterItems with the same CategoryID are OR’d together and AND’ed with other FilterItems. 
        /// </summary>
        public int? CategoryID { get; set; }

        /// <summary>
        /// If set to True, this option is never displayed (for input or in the filter list).  This option is used for Filters that are part of the Tab and don’t need to be shown (e.g., “IsActive=1”).  
        /// This only applies to System Tabs.  All FilterItems are visible on custom tabs.
        /// </summary>
        public bool IsHidden { get; set; }

    }
}
