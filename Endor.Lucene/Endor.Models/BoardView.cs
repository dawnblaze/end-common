﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Xml;

namespace Endor.Models
{
    public class BoardView : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(false)]
        public bool IsSystem { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Required]
        public DataType DataType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string GroupBy { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Columns { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string SortOptions { get; set; }

        [JsonIgnore]
        public string CustomLayout { get; set; }

        [JsonProperty("CustomLayout", NullValueHandling = NullValueHandling.Ignore)]
        public string CustomLayoutJson {
            get
            {
                if (string.IsNullOrWhiteSpace(this.CustomLayout)) return null;

                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(this.CustomLayout);
                    return JsonConvert.SerializeXmlNode(xmlDoc);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [Required]
        [DefaultValue(false)]
        public bool IsTileView { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        [DefaultValue(false)]
        public bool IsListView { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<BoardViewLink> BoardViewLinks { get; set; }

        /// <summary>
        // To be mapped in API Service via BoardViewLink
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<BoardDefinitionData> Boards { get; set; }
    }
}
