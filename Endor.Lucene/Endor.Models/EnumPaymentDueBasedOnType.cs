﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class EnumPaymentDueBasedOnType
    {
        public PaymentDueBasedOnType ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
