﻿using Newtonsoft.Json;
using System;


namespace Endor.Models
{
    public partial class TimeCardDetail : IAtom<short>
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        public short BID { get; set; }

        /// <summary>
        /// The ID of this object (Unique within the Business).
        /// </summary>
        public short ID { get; set; }

        /// <summary>
        /// An ID identifying the type of object. Always 5031.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The Employee ID for this TimeCard.
        /// </summary>
        public short EmployeeID { get; set; }

        /// <summary>
        /// The Date Time this TimeCard was Clocked In/On.
        /// </summary>
        public DateTime StartDT { get; set; }

        /// <summary>
        /// The Date Time this TimeCard was Clocked Out/Off.
        /// </summary>
        public DateTime? EndDT { get; set; }

        /// <summary>
        /// The time in minutes between the Starting and Ending DateTimes.  
        /// If the Ending DateTime is not filled in, the current time is used.
        /// </summary>
        public decimal? TimeInMin { get; set; }

        /// <summary>
        /// The time in minutes paid for this Time Card.  This is equal to the TimeInMin minus any unpaid breaks.
        /// This value is AUTOMATICALLY filled in when TimeCardDetails are added, adjusted, or removed.
        /// It only needs to be manually filled in when Detailed TimeCards are not used.
        /// </summary>
        public decimal? PaidTimeInMin { get; set; }

        /// <summary>
        /// A Flag indicating if the TimeCard is closed.  An EndingDT indicates a close
        /// </summary>
        public bool? IsClosed { get; set; }

        /// <summary>
        /// The Latitude of the Employee when she clocked in/on.
        /// </summary>
        public decimal? LatStart { get; set; }

        /// <summary>
        /// The Longitude of the Employee when she clocked in/on.
        /// </summary>
        public decimal? LongStart { get; set; }

        /// <summary>
        /// The Latitude of the Employee when she clocked out/off.
        /// </summary>
        public decimal? LatEnd { get; set; }

        /// <summary>
        /// The Longitude of the Employee when she clocked out/off.
        /// </summary>
        public decimal? LongEnd { get; set; }

        /// <summary>
        /// A flag indicating if the TimeCard was manually adjusted.
        /// </summary>
        public bool IsAdjusted { get; set; }

        /// <summary>
        /// The ID of the Employee that most recently adjusted the TimeCard, if any.
        /// </summary>
        public short? AdjustedByEmployeeID { get; set; }

        /// <summary>
        /// The DateTime the TimeCard was most recently adjusted, if any.
        /// </summary>
        public DateTime? AdjustedDT { get; set; }

        /// <summary>
        /// A field for additional data that can be stored on the TimeCard.
        /// </summary>
        public string MetaData { get; set; }

        /// <summary>
        /// The TimeCardID for this object.
        /// </summary>
        public short TimeCardID { get; set; }

        /// <summary>
        /// The SimultaneousDetailCards for this object.
        /// </summary>
        public byte SimultaneousDetailCards { get; set; }

        /// <summary>
        /// Flag indicating if this activity is a paid or unpaid status.  Only break statuses may be unpaid.
        /// </summary>
        public bool IsPaid { get; set; }

        /// <summary>
        /// The ID of the OrderItemID for this TimeCard Entry.
        /// </summary>
        public int? OrderItemID { get; set; }

        /// <summary>
        /// The Order Item StatusID for this TimeCard, if applicable. 
        /// No more than one of the OrderItemStatus, TimeClockActivityID, and TimeClockBreakID can be non-null.
        /// </summary>
        public short? OrderItemStatusID { get; set; }

        /// <summary>
        /// The ID of the TimeClockActivity for this TimeCard Detail.
        /// No more than one of the OrderItemStatus, TimeClockActivityID, and TimeClockBreakID can be non-null.
        /// </summary>
        public short? TimeClockActivityID { get; set; }

        /// <summary>
        /// The ID of the TimeClockBreak for this TimeCard Detail.
        /// No more than one of the OrderItemStatus, TimeClockActivityID, and TimeClockBreakID can be non-null.
        /// </summary>
        public short? TimeClockBreakID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData Employee { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData AdjustedByEmployee { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TimeCard TimeCard { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FlatListItem TimeClockActivity { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FlatListItem TimeClockBreak { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemStatus OrderItemStatus { get; set; }        
    }
}
