﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// The Order object is the top level container (the Transaction Header) for the following record types:
    /// Order(TransactionType = 1)
    /// Estimate(TransactionType = 2)
    /// Purchase Order(TransactionType = 8)
    /// </summary>
    public abstract class TransactionHeaderData : IAtom<int>, IPrioritizable
    {
        public TransactionHeaderData()
        {

        }

        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public byte LocationID { get; set; }

        /// <summary>
        /// The type of transaction this is.  Valid TransactionType values are for this object are:
        /// 1 = Estimate
        /// 2 = Order
        /// 4 = PO
        /// </summary>
        public byte TransactionType { get; set; }
        
        /// <summary>
        /// The OrderNumber, EstimateNumber, or PO Number for this Transaction.The type of number is determined by the TransactionType.
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// The Formatted Reference Number for this transaction, which includes the Invoice, Estimate, or PO prefix from the sales LocationID.
        /// </summary>
        [Required]
        [StringLength(32)]
        public string FormattedNumber { get; set; }

        /// <summary>
        /// The invoice number for an order (TransactionType=1) if separate invoice numbering is enabled. 
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? InvoiceNumber { get; set; }

        /// <summary>
        /// The description for this order/estimate/purchase order.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The ID of the Company this order is for.
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// The current status of the Order.
        /// </summary>
        public OrderOrderStatus OrderStatusID { get; set; }

        /// <summary>
        /// The DateTime that the order's status was set to its current value.
        /// </summary>
        public DateTime OrderStatusStartDT { get; set; }

        /// <summary>
        /// The SUM of the Price.PreTax for all Line Item Items for this TransHeader.If any line item prices are NULL, the total is NULL.
        /// NULL is used to indicate the price is not defined("Call for Quote") or an error in configuration or pricing has occurred.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceProductTotal { get; set; }

        /// <summary>
        /// The SUM of the Price.PreTax for all Destinations for this TransHeader.If any line item prices are NULL, the total is NULL.
        /// NULL is used to indicate the price is not defined("Call for Quote") or an error in configuration or pricing has occurred.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceDestinationTotal { get; set; }

        /// <summary>
        /// Any finance charge or prepayment discount for this TransHeader.
        /// A positive value represents a FinanceCharge
        /// A negative value represents a pre-payment discount that has been applied.
        /// A value of 0.0 indicates a Fiancce Charge or Pre-Payment Discount was applied but subsequently removed.
        /// NULL indicates no finance charge or pre-payment discount has ever been applied.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceFinanceCharge { get; set; }

        /// <summary>
        /// The TransHeader level DiscountPercentage in percent(e.g. for a 7.5% discount this value should be 7.5000).
        /// This is separate from any discounts on line items or destinations.
        /// Use NULL if there is no discount or if the discount is a fixed amount and not computed by percentage.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceDiscountPercent { get; set; }

        /// <summary>
        /// Flag indicating if the taxable amount was manually overridden.
        /// </summary>
        public bool PriceTaxableOV { get; set; }

        /// <summary>
        /// The total of payments applied to this order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal PaymentPaid { get; set; }

        /// <summary>
        /// The total of credit card transactions authorizations but not captured for this order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PaymentAuthorized { get; set; }

        /// <summary>
        /// The total of write-offs for this order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PaymentWriteOff { get; set; }

        /// <summary>
        /// The total of credit memos applied to this order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CreditMemoApplied { get; set; }

        /// <summary>
        /// The total of all payments, write-offs, and credit memos applied to this order.
        /// </summary>
        public decimal PaymentTotal { get; set; }

        /// <summary>
        /// The balance due on the order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PaymentBalanceDue { get; set; }

        /// <summary>
        /// The amount of a credit memo created by this order.Set to NULL if no credit memo was created for this order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CreditMemoCredit { get; set; }

        /// <summary>
        /// The amount of a credit memo for this order that was used. Set to NULL if no credit memo was created for this order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CreditMemoUsed { get; set; }

        /// <summary>
        /// The amount of a credit memo balance remaining on this order.Set to NULL if no credit memo was created for this order.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CreditMemoBalance { get; set; }

        /// <summary>
        /// Flag indicating if the order has a remaining CreditMemo balance (e.g. CreditMemo.Balance <> 0.0).
        /// This is primarily used for SQL searching so the database field is persisted.
        /// </summary>
        public bool CreditMemoHasBalance { get; set; }

        /// <summary>
        /// The ID of the TaxGroup for this transaction.
        /// </summary>
        public short TaxGroupID { get; set; }

        /// <summary>
        /// A flag indicating if the TaxGroup was manually overridden or is the default value.
        /// If it is the default value, it will change if the the default tax group of the company changes.
        /// </summary>
        public bool TaxGroupOV { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? OpportunityID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string OrderPONumber { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? OriginID { get; set; }

        /// <summary>
        /// Flag indicating if the Order has a single DestinationStatus.  If false, multiple destinations will apply to the order.
        /// </summary>
        public bool HasSingleDestination { get; set; }

        /// <summary>
        /// The DestinationType Enum for the order.
        /// If the order has 1 destination, Order.DestinationType = Destination[0].DestinationType
        /// If the order has multiple destinations and they are all the same, this value is set to that DestinationType.
        /// If the order has Destinations with different DestinationType, this is set to NULL.
        /// </summary>
        public OrderDestinationType DestinationType { get; set; }

        /// <summary>
        /// The LocationID the order is to be produced at.  Defaults to the (sales) LocationID if not specified.
        /// </summary>
        public byte ProductionLocationID { get; set; }

        /// <summary>
        /// 	The LocationID the order is to be picked up at. Defaults to the (sales) LocationID if not specified.
        /// </summary>
        public byte PickupLocationID { get; set; }

        /// <summary>
        /// Flag indicating if the Order has any links in the OrderOrderLink Object collection. If this is false, no attempt to load the links collection should be made.
        /// </summary>
        public bool HasOrderLinks { get; set; }

        /// <summary>
        /// Flag indicating if the Order has any documents in its Document Management folder.
        /// </summary>
        public bool HasDocuments { get; set; }

        /// <summary>
        ///  Flag indicating if the pricing is locked(and won't be automatically recomputed on edit)	
        /// </summary>
        [Required]
        public bool PriceIsLocked { get; set; }

        /// <summary>
        /// The Starting Price.  This may be the aggregated child price, the base price, net price, etc. NULL is used to indicate the price is not defined ("Call for Quote") or an error in configuration or pricing has occurred.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceNet { get; set; }

        /// <summary>
        /// The Discount Amount.
        /// Use NULL for no discount.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceDiscount { get; set; }

        /// <summary>
        /// The PreTax total for this record.
        /// = Price.Net - IsNull(Price.Discount, 0)
        /// This will be NULL if the Price.Net is NULL, indicating the price is not defined.
        ///=> PriceNet - PriceDiscount.GetValueOrDefault(0);
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax { get; set; }

        /// <summary>
        /// The Taxable total for this record.For non-taxable items, this should be 0.0.  Otherwise this may be any value between 0 and Price.PreTax.
        /// This should only be NULL when Price.PreTax is NULL.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTaxable { get; set; }

        /// <summary>
        /// The combined tax rate for the applicable TaxGroup.Price.TaxRate DECIMAL(9,4)
        /// </summary>
        public decimal PriceTaxRate { get; set; }

        /// <summary>
        /// A flag indicating if the transaction is tax exempt.  For tax exempt orders, the TaxExemptReasonID must be set.
        /// </summary>
        public bool IsTaxExempt { get; set; }

        /// <summary>
        /// If the flag is tax exempt, this field stores the ID of the TaxExemptReason Object associated with this line item.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? TaxExemptReasonID { get; set; }

        /// <summary>
        /// The Tax for this transaction item.This should always equal (Price.Taxable* Price.TaxRate), but may have additional rounding rules applied.
        /// => PriceTaxable * PriceTaxRate;
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTax { get; set; }

        /// <summary>
        /// The computed total price for this transaction item.
        ///=> PricePreTax + PriceTax;
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTotal { get; set; }

        /// <summary>
        /// The total Cost of the Material Parts for this Item. 
        /// If there are no Material Parts, this field should be NULL(not 0.0).
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMaterial { get; set; }

        /// <summary>
        /// The total Cost of the Labor Parts for this Item. 
        /// If there are no Labor Parts, this field should be NULL(not 0.0).
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostLabor { get; set; }

        /// <summary>
        /// The total Cost of the Machine Parts for this Item. If there are no Machine Parts, this field should be NULL(not 0.0).
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMachine { get; set; }

        /// <summary>
        /// The total non-part Costs for this Item.This is used for manually entered shipping and other miscellaneous costs.
        /// If there are no other costs, this field should be NULL(not 0.0).
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostOther { get; set; }

        /// <summary>
        /// The total Cost of all parts for this Item. This is computed as the sum of the other costs.
        /// => CostMaterial.GetValueOrDefault(0m) + CostLabor.GetValueOrDefault(0m) + CostMachine.GetValueOrDefault(0m) + CostOther.GetValueOrDefault(0m);
        /// </summary>
        public decimal CostTotal { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FlatListItem TaxExemptReason { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationData Location { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CrmOrigin Origin { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TaxGroup TaxGroup { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CompanyData Company { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumOrderOrderStatus EnumOrderOrderStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumOrderDestinationType EnumOrderDestinationType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OpportunityData Opportunity { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationData ProductionLocation { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationData PickupLocation { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderKeyDate> Dates { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderNote> Notes { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderOrderLink> Links { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderContactRole> ContactRoles { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderEmployeeRole> EmployeeRoles { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderItemData> Items { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderDestinationData> Destinations { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleOrderDestinationData> SimpleDestinations { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleOrderItemData> SimpleItems { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderTagLink> TagLinks { get; set; }

        public short? Priority { get; set; }

        public bool? IsUrgent { get; set; }

        public short? ItemCount { get; set; }

        /// <summary>
        /// used to copy blobs from quickItem storage to new order storage
        /// only used on order create
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? FromQuickItemID { get; set; }

        [SwaggerInclude]
        public ICollection<PaymentApplication> Payments { get; set; }

    }
}
