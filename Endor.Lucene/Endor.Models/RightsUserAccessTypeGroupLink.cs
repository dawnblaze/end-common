﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public class RightsUserAccessTypeGroupLink
    {
        public short ID { get; set; }

        public UserAccessType UserAccessType { get; set; }

        public short IncludedRightsGroupID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroup IncludedRightsGroup { get; set; }
    }
}
