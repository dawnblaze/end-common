﻿using System;

namespace Endor.Models
{
    public class SystemTranslationDefinition
    {
        public int ID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsVerified { get; set; }

        public string SourceText { get; set; }

        public string AltMeaning { get; set; }

        public string TranslatedText { get; set; }

        public virtual byte LanguageTypeId { get; set; }

        public virtual SystemLanguageType LanguageType { get; set; }
    }
}
