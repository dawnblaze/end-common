﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum OrderOrderLinkType : byte
    {
        ClonedFrom = 1,
        ClonedTo = 2,
        ConvertedFrom = 3,
        ConvertedTo = 4,
        MasterOrder = 5,
        ChildOrder = 6,
        CreditMemoAppliedFrom = 7,
        CreditMemoAppliedTo = 8,
        PrimaryVariationFor = 9,
        AlternateVariationOf = 10,
    }

    public class EnumOrderOrderLinkType
    {
        //The ID for this object.
        public OrderOrderLinkType ID { get; set; }
        //The Name for this object.
        [StringLength(100)]
        public string Name { get; set; }
        public OrderOrderLinkType SymmetricLinkType { get; set; }
        //The TransactionTypeSet for this object.
        public OrderTransactionType TransactionTypeSet { get; set; }
        //The TransactionLevelSet for this object.
        public OrderTransactionLevel TransactionLevelSet { get; set; }
        //Flag indicating if this enum type applies to Estimates.
        public bool AppliesToEstimate { get; set; }
        //Flag indicating if this enum type applies to Orders.
        public bool AppliesToOrder { get; set; }
        //Flag indicating if this enum type applies to POs.
        public bool AppliesToPO { get; set; }
        //Flag indicating if this enum type is applied at the TransHeader level.
        public bool IsHeaderLevel { get; set; }
        //Flag indicating if this enum is applied at the Destination level.
        public bool IsDestinationLevel { get; set; }
        //Flag indicating if this enum is applied at the Line Items level.
        public bool IsItemLevel { get; set; }

    }
}
