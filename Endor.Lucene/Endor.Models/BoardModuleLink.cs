﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Endor.Models
{
    public class BoardModuleLink
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short BoardID { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Module ModuleType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BoardDefinitionData BoardDefinition { get; set; }

        [JsonIgnore]
        public EnumModule EnumModule { get; set; }
       
    }
}
