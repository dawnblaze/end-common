﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum URLRegistrationType : byte
    {
        Tracking = 0, //A tracking call always returns an image (a.k.a. Web Beacon) that is a single-pixel white (or ideally transparent) GIF
        Redirect = 1, //This call is used for a generic redirect with no additional processing.  This method simply results in a HTTP 307 result to the target URL.
        RedirectWithToken = 2, //This call is used for a generic redirect.  
        RedirectAsEndorEmployee = 3, //This call is used to redirect a request but to do so as the specified Endor User/Employee.
        RedirectAsEndorContact = 4, //This call is used to redirect a request but to do so as the specified Endor User/Contact.
        RedirectAsEcommContact = 5, //This call is used to redirect a request but to do so as the specified Ecommerce User/Contact. 
        BlobRead = 10, //This call returns a URL to the target Blob file or folder with Read-Only credentials.
        BlobReadWrite = 11, //This call returns a URL to the target Blob folder with Read-Write credentials.
        ArtworkApproval = 20, //This call redirects the caller to the artwork approval page for the target OrderID. 
        OnlinePayment = 30, //This call redirects the caller to the online payment page for the target OrderID
        OnlineInvoice = 31  //This call redirects the caller to the online invoice page for the target OrderID.
    }

    public class EnumURLRegistrationType
    {
        public URLRegistrationType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
