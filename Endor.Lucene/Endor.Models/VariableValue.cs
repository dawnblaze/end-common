﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Endor.Models
{
    [JsonObject]
    public struct VariableValue : System.IConvertible
    {
        [JsonIgnore]
        public DataType DataType { get; set; }

        [JsonProperty("Value", NullValueHandling = NullValueHandling.Ignore)]
        public dynamic SerializedValue
        {
            get
            {
                if (Value == null)
                    return null;

                if (DataType == DataType.Boolean)
                    return ValueAsBool;

                if (DataType == DataType.Number)
                    return ValueAsDecimal;

                return Value;
            }
            set
            {
                Value = value.ToString();
            }
        }
        // Store the value as a string (since it was streamed that way)
        private string _Value;
        [JsonIgnore]
        public string Value
        {
            get
            {
                return _Value;
            }
            set
            {
                intvalue = null;
                boolvalue = null;
                decimalvalue = null;

                _Value = value;
            }
        }

        // Tracks if the value was manually entered (Overridden) or default/computed
        [DefaultValue(false)]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public bool ValueOV { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Alt { get; set; }

        private int? intvalue;
        private bool? boolvalue;
        private decimal? decimalvalue;

        // Define a property list (for future use)
        // public List<VariableValue> Metadata { get; set; }

        // Stored Integer value if it can be converted (so that the conversion is only done once)
        [JsonIgnore]
        public int ValueAsInt
        {
            get
            {
                if (intvalue.HasValue)
                    return intvalue.Value;

                else if (int.TryParse(Value, out int newValue))
                {
                    intvalue = newValue;
                    return newValue;
                }

                else
                {
                    intvalue = 0;
                    return 0;
                }
            }
            set
            {
                Value = value.ToString();
                intvalue = value;
            }
        }

        // Stored decimal value if it can be converted (so that the conversion is only done once)
        [JsonIgnore]
        public decimal ValueAsDecimal
        {
            get
            {
                if (decimalvalue.HasValue)
                    return decimalvalue.Value;

                else if (decimal.TryParse(Value, out decimal newValue))
                {
                    decimalvalue = newValue;
                    return newValue;
                }

                else
                {
                    decimalvalue = 0m;
                    return 0m;
                }
            }
            set
            {
                Value = value.ToString();
                decimalvalue = value;
            }
        }

        // Stored boolean  value if it can be converted (so that the conversion is only done once)
        [JsonIgnore]
        public bool ValueAsBool
        {
            get
            {
                if (boolvalue.HasValue)
                    return boolvalue.Value;

                else if (!string.IsNullOrWhiteSpace(Value) && ("1ty".IndexOf(Value.ToLowerInvariant()[0]) > 0))
                {
                    boolvalue = true;
                    return true;
                }

                else
                {
                    boolvalue = false;
                    return false;
                }
            }
            set
            {
                Value = value.ToString();
                boolvalue = value;
            }
        }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsIncluded { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsIncludedOV { get; set; }

        // These functions implement IConvertible
        public TypeCode GetTypeCode()
        {
            switch (DataType)
            {
                case DataType.Boolean:
                    return TypeCode.Boolean;
                case DataType.Number:
                    return TypeCode.Decimal;
                case DataType.DateTime:
                    return TypeCode.DateTime;
                default:
                    return TypeCode.String;
            }
        }

        public bool ToBoolean(IFormatProvider provider)
        {
            return ValueAsBool;
        }

        public byte ToByte(IFormatProvider provider)
        {
            return (byte)ValueAsInt;
        }

        public char ToChar(IFormatProvider provider)
        {
            return Value[0];
        }

        public DateTime ToDateTime(IFormatProvider provider)
        {
            return Convert.ToDateTime(Value);
        }

        public decimal ToDecimal(IFormatProvider provider)
        {
            return ValueAsDecimal;
        }

        public double ToDouble(IFormatProvider provider)
        {
            return (double)ValueAsDecimal;
        }

        public short ToInt16(IFormatProvider provider)
        {
            return (Int16)ValueAsInt;
        }

        public int ToInt32(IFormatProvider provider)
        {
            return ValueAsInt;
        }

        public long ToInt64(IFormatProvider provider)
        {
            return ValueAsInt;
        }

        public sbyte ToSByte(IFormatProvider provider)
        {
            return (SByte)ValueAsInt;
        }

        public float ToSingle(IFormatProvider provider)
        {
            return (Single)ValueAsDecimal;
        }

        public string ToString(IFormatProvider provider)
        {
            return Value;
        }

        public object ToType(Type conversionType, IFormatProvider provider)
        {
            /*if (conversionType == typeof(Decimal))
            {
                // Do your conversion here and return the string.
                return this.ToDecimal(provider);
            }
            if (conversionType == typeof(Object))
            {
                return this;
            }
            if (conversionType == typeof(String))
            {
                //return this.ToString(provider);
                return @"{'Value':'"+Value+"'}";
            }*/
            throw new InvalidCastException($"Converting type \"{typeof(VariableValue)}\" to type \"{conversionType.Name}\" is not supported.");
        }

        public ushort ToUInt16(IFormatProvider provider)
        {
            return (UInt16)ValueAsInt;
        }

        public uint ToUInt32(IFormatProvider provider)
        {
            return (UInt32)ValueAsInt;
        }

        public ulong ToUInt64(IFormatProvider provider)
        {
            return (UInt64)Convert.ToInt64(Value);
        }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
        public Unit Unit { get; set; }
    }
}
