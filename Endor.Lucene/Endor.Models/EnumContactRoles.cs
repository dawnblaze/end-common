﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    [Flags]
    public enum ContactRole : byte
    {
        Inactive = 0,
        Primary = 1,
        Billing = 2,
        Owner = 4,
        Observer = 128
    }

    public class EnumContactRoles
    {
        public ContactRole ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
