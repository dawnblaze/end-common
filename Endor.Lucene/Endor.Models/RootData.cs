﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class RootData
    {
        public int ID { get; set; }
        public short BID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }
}
