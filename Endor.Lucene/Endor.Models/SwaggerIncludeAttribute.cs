﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    [AttributeUsage(AttributeTargets.Property)]
    public class SwaggerIncludeAttribute : Attribute
    {
    }
}
