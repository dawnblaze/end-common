﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace Endor.Models
{
    public partial class EmployeeRole : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(200)]
        public string Name { get; set; }
        public bool IsSystem { get; set; }
        public bool AllowMultiple { get; set; }
        public bool AllowOnTeam { get; set; }
        public RoleAccess OrderRestriction { get; set; }
        public RoleAccess OrderItemRestriction { get; set; }
        public RoleAccess OrderDestinationRestriction { get; set; }
        public RoleAccess EstimateRestriction { get; set; }
        public RoleAccess EstimateItemRestriction { get; set; }
        public RoleAccess EstimateDestinationRestriction { get; set; }
        public RoleAccess OpportunityRestriction { get; set; }
        public RoleAccess PORestriction { get; set; }

        public ICollection<BoardRoleLink> BoardLinks { get; set; }
    }
}
