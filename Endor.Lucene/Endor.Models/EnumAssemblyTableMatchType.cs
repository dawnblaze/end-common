﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum AssemblyTableMatchType : byte
    {
        ExactMatch = 0,
        UseLowerValue = 1,
        UseHigherValue = 2,
        Interpolate = 3
    }

    public class EnumAssemblyTableMatchType
    {
        public AssemblyTableMatchType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
