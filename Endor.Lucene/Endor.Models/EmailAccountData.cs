﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Endor.Models
{
    public class EmailAccountData : IAtom<short>
    {

        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public EmailAccountStatus StatusType { get; set; }
        public short DomainEmailID { get; set; }
        public string UserName { get; set; }
        public string DomainName { get; set; }
        public bool IsPrivate { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? EmployeeID { get; set; }
        public string Credentials { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? CredentialsExpDT { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastEmailSuccessDT { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastEmailFailureDT { get; set; }
        public string EmailAddress { get; set; }
        public string AliasUserNames { get; set; }
        public string DisplayName { get; set; }

        public BusinessData Business { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DomainEmail DomainEmail { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData Employee { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumEmailAccountStatus EnumEmailAccountStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<EmailAccountTeamLink> EmailAccountTeamLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<EmployeeTeam> Teams { get; set; }
    }
}
