﻿
namespace Endor.Models.Autocomplete
{
    public class AssemblyVariableAutocompleteBrief
    {
        public string Name { get; set; }
        public AssemblyElementType ElementType { get; set; }
        public UnitType? UnitType { get; set; }  // only included for number variables
        public int? ListDataType { get; set; }  // only inclulded for drop-down variables
        public int? LinkedAssemblyID { get; set; }  // only included for Linked Assemblies
        public int? LinkedMachineID { get; set; }  // only included for Linked Machines
    }
}
