﻿using System;

namespace Endor.Models.Autocomplete
{
    [AttributeUsage(AttributeTargets.Interface | AttributeTargets.Class)]
    public sealed class AutocompleteAttribute : Attribute
    {
        public AutocompleteAttribute(string alias)
        {
            this.Alias = alias;
        }

        public string Alias { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public sealed class AutocompleteIgnoreAttribute : Attribute
    {
        public AutocompleteIgnoreAttribute()
        {
        }
    }
}
