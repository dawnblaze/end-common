﻿using Newtonsoft.Json;

namespace Endor.Models
{
    /// <summary>
    /// Email Account Team Link
    /// </summary>
    public class EmailAccountTeamLink
    {
        /// <summary>
        /// Business ID
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }
        /// <summary>
        /// Email Account ID
        /// </summary>
        public short EmailAccountID { get; set; }
        /// <summary>
        /// Team ID
        /// </summary>
        public int TeamID { get; set; }

        /// <summary>
        /// Navigation property to the EmailAccount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmailAccountData EmailAccount { get; set; }
        /// <summary>
        /// Navigation property to the EmployeeTeam
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeTeam Team { get; set; }
    }
}
