﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum AssociationType : byte
    {
        Independent = 0,
        Fastsigns = 1,
        AllianceFranchiseBrands = 2,
        SignWorld = 3,
        SignARama = 4,
        SpeedproUS = 5,
        SpeedproCA = 6,
        AlphaGraphics = 11,
    }

    public class EnumAssociationType
    {
        public AssociationType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
