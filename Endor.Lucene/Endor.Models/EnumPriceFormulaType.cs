﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// Price Formula Type enum
    /// </summary>
    public enum PriceFormulaType: byte
    {
        Fixed = 0,
        PriceTable = 1,
        DiscountTable = 2,
        MarginTable = 3,
        MarkupTable = 4,
        Custom = 255
    }


    public class EnumPriceFormulaType
    {
        public PriceFormulaType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
