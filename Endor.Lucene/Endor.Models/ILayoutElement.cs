﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public interface ILayoutElement<E> : IAtom<int>
    {
        short LayoutID { get; set; }

        AssemblyElementType ElementType { get; set; }

        int? ParentID { get; set; }

        byte Column { get; set; }
        byte Row { get; set; }
        byte ColumnsWide { get; set; }

        
        ICollection<E> Elements { get; set; }
    }

    public interface ICustomFieldElement: ILayoutElement<CustomFieldLayoutElement>
    {
        string Name { get; set; }
        short? DefinitionID { get; set; }
        CustomFieldDefinition Definition { get; set; }
        Guid? TempDefinitionID { get; set; }
    }

    public interface IAssemblyElement : ILayoutElement<AssemblyElement>
    {
        int? VariableID { get; set; }
        AssemblyVariable Variable { get; set; }
        Guid? TempVariableID { get; set; }
        string VariableName { get; set; }
    }
}
