﻿using System;
using Newtonsoft.Json;

namespace Endor.Models
{
    public class RightsGroupListRightsGroupLink
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short ListID { get; set; }

        public short GroupID { get; set; }

        [JsonIgnore]
        public DateTime CreatedDT { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroup RightsGroup { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroupList RightsGroupList { get; set; }
    }
}
