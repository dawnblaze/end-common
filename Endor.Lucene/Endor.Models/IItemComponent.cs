﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public interface IItemComponent<T>
        where T: class, IItemComponent<T>
    {
        int ID { get; set; }
        Guid? TempID { get; set; }

        string Name { get; set; }               // New -- stored Component Name
        string VariableName { get; set; }

        ICollection<T> ChildComponents { get; set; }  // was Components, Type Changed

        Dictionary<string, VariableValue> Variables { get; set; }
        bool CostOV { get; set; }              // was CostUnitOV
        decimal? CostNet { get; set; }          // was CostTotal
        decimal? CostUnit { get; set; }

        decimal? PricePreTax { get; set; }
        decimal? PriceTax { get; set; }  // was TaxAmount
        decimal? PriceTotal { get; set; }
        decimal? PriceUnit { get; set; }
        bool PriceUnitOV { get; set; } // was PricePreTaxOV

        decimal? TotalQuantity { get; set; }        
        bool TotalQuantityOV { get; set; }    
        decimal? AssemblyQuantity { get; set; }
        bool AssemblyQuantityOV { get; set; }
        Unit? QuantityUnit { get; set; }        // New

        bool? RollupLinkedPriceAndCost { get; set; }

        int? IncomeAccountID { get; set; }

        int? ExpenseAccountID { get; set; }

        /// <summary>
        /// The IncomeAllocationType for this object.
        /// </summary>
        AssemblyIncomeAllocationType? IncomeAllocationType { get; set; }

        ICollection<TaxAssessmentResult> TaxInfoList { get; set; }
    }
}
