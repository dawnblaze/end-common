﻿
namespace Endor.Models
{
    public partial class TaxGroupLocationLink
    {
        public short BID { get; set; }

        public short GroupID { get; set; }

        public byte LocationID { get; set; }

        public TaxGroup TaxGroup { get; set; }

        public LocationData Location { get; set; }
    }
}
