﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Endor.Models
{
    public class OrderItemData : IAtom<int>, IPrioritizable
    {
        public OrderItemData()
        {

        }

        [JsonIgnore]
        public short BID { get; set; }

        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public int OrderID { get; set; }

        public short ItemNumber { get; set; }

        [Column(TypeName="decimal(18,6)")]
        public decimal Quantity { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        public bool IsOutsourced { get; set; }

        public OrderOrderStatus OrderStatusID { get; set; }

        public short ItemStatusID { get; set; }

        public short? SubStatusID { get; set; }

        public byte ProductionLocationID { get; set; }

        public bool PriceIsLocked { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceComponent { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceSurcharge { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceList { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceItemDiscountPercent { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceAppliedOrderDiscountPercent { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceDiscountPercent { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceItemDiscount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceAppliedOrderDiscount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceDiscount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceUnitPreTax { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTaxable { get; set; }

        public bool PriceTaxableOV { get; set; }

        public bool PriceUnitOV { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTax { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTotal { get; set; }

        public short TaxGroupID { get; set; }

        public bool TaxGroupOV { get; set; }

        public bool IsTaxExempt { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? TaxExemptReasonID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMaterial { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostLabor { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMachine { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostOther { get; set; }

        public decimal CostTotal { get; set; }

        public bool HasProof { get; set; }

        public bool HasDocuments { get; set; }

        public bool HasCustomImage { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FlatListItem TaxExemptReason { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TransactionHeaderData Order { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumOrderOrderStatus EnumOrderOrderStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemStatus OrderItemStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TaxGroup TaxGroup { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemSubStatus SubStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationData ProductionLocation { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FlatListItem Category { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderNote> Notes { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderKeyDate> Dates { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderContactRole> ContactRoles { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderEmployeeRole> EmployeeRoles { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderItemTagLink> TagLinks { get; set; }

        public short? Priority { get; set; }

        public bool? IsUrgent { get; set; }

        public short? ItemCount { get; set; }

        /// <summary>
        /// The type of transaction this is.
        /// </summary>
        public byte TransactionType { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderItemComponent> Components { get; set; }

        [SwaggerInclude]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderItemSurcharge> Surcharges { get; set; }

        public short? CategoryID { get; set; }

        public string TempID { get; set; }

        /// <summary>
        /// used to copy blobs from quickItem storage to new line item storage
        /// only used on line item create
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? FromQuickItemID { get; set; }

        /// <summary>
        /// used to copy blobs from quickItem storage to new line item storage
        /// only used on line item create
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? FromQuickItemLineItemNumber { get; set; }
    }
}
