﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Endor.Models
{
    public class RightsGroup
    {
        public short ID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }
}
