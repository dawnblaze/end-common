﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class SimpleOrderData : SimpleListItem<int>
    {
        public byte TransactionType { get; set; }
    }
}
