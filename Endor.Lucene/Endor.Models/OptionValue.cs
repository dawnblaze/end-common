﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class OptionValue
    {
        public short ID { get; set; }
        public string Value { get; set; }
        public byte OptionLevel { get; set; }
    }
}
