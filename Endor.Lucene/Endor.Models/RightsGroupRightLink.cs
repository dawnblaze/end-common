﻿using Endor.Security;
using Newtonsoft.Json;

namespace Endor.Models
{
    public class RightsGroupRightLink
    {
        public short GroupID { get; set; }

        public short RightID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroup RightsGroup { get; set; }
    }
}
