﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Endor.Models
{
    public class OrderKeyDate : IAtom<int>
    {
        [JsonIgnore]
        public int ID { get; set; }
        [JsonIgnore]
        public short BID { get; set; }
        [JsonIgnore]
        public int ClassTypeID { get; set; }
        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }

        [JsonIgnore]
        public int OrderID { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public OrderKeyDateType KeyDateType { get; set; }

        public DateTime KeyDT { get; set; }

        public bool IsFirmDate { get; set; }

        [JsonIgnore]
        public int? DestinationID { get; set; }

        [JsonIgnore]
        public int? OrderItemID { get; set; }

        public string OrderItemTempID { get; set; }        

        [JsonIgnore]
        public bool IsDestinationDate { get; set; }

        [JsonIgnore]
        public bool IsOrderItemDate { get; set; }

        [JsonIgnore]
        public DateTime? KeyDate { get; set; }

        [JsonIgnore]
        public TimeSpan? KeyTime { get; set; }

        public TransactionHeaderData Order { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemData OrderItem { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderDestinationData Destination { get; set; }

    }
}
