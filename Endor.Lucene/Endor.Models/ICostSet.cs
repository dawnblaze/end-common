﻿namespace Endor.Models
{
    /// <summary>
    /// The CostSet class is a container for costs on the following transaction items.
    /// </summary>
    public interface ICostSet
    {
        /// <summary>
        /// The total Cost of the Material Parts for this Item. 
        /// If there are no Material Parts, this field should be NULL(not 0.0).
        /// </summary>
        decimal? CostMaterial { get; set; }
        /// <summary>
        /// The total Cost of the Labor Parts for this Item. 
        /// If there are no Labor Parts, this field should be NULL(not 0.0).
        /// </summary>
        decimal? CostLabor { get; set; }
        /// <summary>
        /// The total Cost of the Machine Parts for this Item. If there are no Machine Parts, this field should be NULL(not 0.0).
        /// </summary>
        decimal? CostMachine { get; set; }
        /// <summary>
        /// The total non-part Costs for this Item.This is used for manually entered shipping and other miscellaneous costs.
        /// If there are no other costs, this field should be NULL(not 0.0).
        /// </summary>
        decimal? CostOther { get; set; }
        /// <summary>
        /// The total Cost of all parts for this Item. This is computed as the sum of the other costs.
        /// </summary>
        decimal CostTotal { get; }

    }
}
