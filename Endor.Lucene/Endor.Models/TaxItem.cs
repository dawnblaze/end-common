﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class TaxItem:IAtom<short>
    {
        public TaxItem()
        {
            TaxGroupItemLinks = new HashSet<TaxGroupItemLink>();
        }

        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public decimal TaxRate { get; set; }

        public decimal? TaxableSalesCap { get; set; }

        [Required]
        [StringLength(50)]
        public string InvoiceText { get; set; }

        [StringLength(255)]
        public string LookupCode { get; set; }

        [StringLength(255)]
        public string AgencyName { get; set; }

        [StringLength(255)]
        public string AccountNumber { get; set; }

        public int? VendorID { get; set; }

        public int GLAccountID { get; set; }

        public ICollection<TaxGroupItemLink> TaxGroupItemLinks { get; set; }

        public ICollection<SimpleTaxGroup> SimpleTaxGroups { get; set; }

        public GLAccount GLAccount { get; set; }

        public TaxEngineType TaxEngineType { get; set; }
    }
}
