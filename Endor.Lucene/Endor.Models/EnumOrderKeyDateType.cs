﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum OrderKeyDateType : byte
    {
        Created = 1,
        Voided = 2,
        Lost = 3,
        Converted = 4,
        ReleasedToWIP = 5,
        Built = 6,
        InInvoicing = 7,
        Invoiced = 8,
        Closed = 9,
        DestinationPending = 10,
        DestinationReady = 11,
        DestinationComplete = 12,
        Requested = 13,
        Approved = 14,
        Ordered = 15,
        Received = 16,
        DesignDue = 20,
        DesignCompleted = 21,
        ProductionDue = 22,
        ShippingDue = 23,
        ArrivalDue = 24,
        Arrived = 25,
        ProposalDue = 26,
        ProposalDelivered = 27,
        FinanceChargeDue = 28,
        FinanceChargeApplied = 29,
        Pending = 30,
        Followup = 31,
        BalanceDue = 32,
        ScheduledPayment = 33,
        EarlyPaymentDate = 34
    }

    public class EnumOrderKeyDateType
    {
        /// <summary>
        /// Order Key Date Type ID
        /// </summary>
        public OrderKeyDateType ID { get; set; }

        /// <summary>
        /// Order Key Date Name
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// Indicates if the KeyDate uses the time as well as the date.
        /// </summary>
        public bool HasTime { get; set; }

        /// <summary>
        /// Indicates if this date is user set or set by the system automatically.
        /// </summary>
        public bool IsUserInput { get; set; }

        /// <summary>
        /// The TransactionTypeSet for this object.
        /// </summary>
        public OrderTransactionType TransactionTypeSet { get; set; }

        /// <summary>
        /// The TransactionLevelSet for this object.
        /// </summary>
        public OrderTransactionLevel TransactionLevelSet { get; set; }

        /// <summary>
        /// Flag indicating if this enum type applies to Estimates.
        /// </summary>
        public bool AppliesToEstimate { get; set; }

        /// <summary>
        /// Flag indicating if this enum type applies to Orders.
        /// </summary>
        public bool AppliesToOrder { get; set; }

        /// <summary>
        /// Flag indicating if this enum type applies to POs.
        /// </summary>
        public bool AppliesToPO { get; set; }

        /// <summary>
        /// Flag indicating if this enum type is applied at the TransHeader level.
        /// </summary>
        public bool IsHeaderLevel { get; set; }

        /// <summary>
        /// Flag indicating if this enum is applied at the Destination level.
        /// </summary>
        public bool IsDestinationLevel { get; set; }

        /// <summary>
        /// Flag indicating if this enum is applied at the Line Items level.
        /// </summary>
        public bool IsItemLevel { get; set; }
    }
}
