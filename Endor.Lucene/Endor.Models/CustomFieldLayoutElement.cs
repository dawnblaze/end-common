﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class CustomFieldLayoutElement : IAtom<int>, ICustomFieldElement
    {
        //IAtom
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }

        //props
        [Required]
        public string Name { get; set; }
        public short SortIndex { get; set; }
        public short? DefinitionID { get; set; }
        public Guid? TempDefinitionID { get; set; }
        public short? DataType { get; set; }

        /// <summary>
        /// Foreign Key pointing to the Visual Parent Element that this element is placed in.
        /// </summary>
        public int? ParentID { get; set; }

        /// <summary>
        /// This property holds an array of CustomFieldLayoutElement Object objects for Split Column or Other Group Elements.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<CustomFieldLayoutElement> Elements { get; set; }

        //nav props
        public CustomFieldDefinition Definition { get; set; }
        public  byte Column { get; set; }
        public byte ColumnsWide { get; set; }
        public AssemblyElementType ElementType { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsReadOnly { get; set; }
        public byte Row { get; set; }
        public string Tooltip { get; set; }
        public short LayoutID { get; set; }
    }
}
