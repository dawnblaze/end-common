﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class BusinessData:IAtom<short>, IImageCandidate
    {
        public BusinessData()
        {
            Campaigns = new HashSet<CampaignData>();
            Companies = new HashSet<CompanyData>();
            Contacts = new HashSet<ContactData>();
            Employees = new HashSet<EmployeeData>();
            Locations = new HashSet<LocationData>();
            Opportunities = new HashSet<OpportunityData>();
        }

        [Key]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(255)]
        public string LegalName { get; set; }
        public bool HasImage { get; set; }
        public short? OwnerEmployeeID { get; set; }
        public short? BillingEmployeeID { get; set; }
        public byte? AssociationType { get; set; }

        public EmployeeData BillingEmployee { get; set; }
        public EmployeeData OwnerEmployee { get; set; }
        public ICollection<BusinessLocator> BusinessLocators { get; set; }
        public ICollection<CampaignData> Campaigns { get; set; }
        public ICollection<CompanyData> Companies { get; set; }
        public ICollection<ContactData> Contacts { get; set; }
        public ICollection<EmployeeData> Employees { get; set; }
        public ICollection<LocationData> Locations { get; set; }
        public ICollection<OpportunityData> Opportunities { get; set; }
    }
}
