﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class BoardDefinitionData : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Required]
        public DataType DataType { get; set; }

        public bool LimitToRoles { get; set; }
        public bool IsAlwaysShown { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ConditionFx { get; set; }

        [JsonIgnore]
        public string ConditionSQL { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastRunDT { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? LastRecordCount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double? LastRunDurationSec { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? CummCounterDT { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CummRunCount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double? CummRunDurationSec { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double? AverageRunDuration { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<BoardViewLink> BoardViewLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<BoardEmployeeLink> EmployeeLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<BoardRoleLink> RoleLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<BoardModuleLink> ModuleLinks { get; set; }
    }
}
