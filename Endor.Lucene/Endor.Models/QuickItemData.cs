﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// A Quick Item can be thought of as a Line Item Template.  A QuickItem Object is a Line Item or collection of Line Items saved as an XML record that can then be easily imported into an existing order.
    /// </summary>
    public class QuickItemData : IAtom<int>
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }
        /// <summary>
        /// The ID of this object (Unique within the Business).
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 12060.
        /// </summary>
        public int ClassTypeID { get; set; }
        /// <summary>
        /// (Read Only) The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }
        /// <summary>
        /// Flag indicating if the record is Active.
        /// </summary>
        public bool IsActive { get; set; }
        /// <summary>
        /// The Name for Quick Item.  This is the name used in the list, as well as the name of the first line item.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        /// <summary>
        /// The specific CompanyID associated with the Quick Item.  If assigned, this Quick Item can only be used for this Company or one of it's subsidiariesr. 
        /// <para>This is NULL for global Quick Items.</para>
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }
        /// <summary>
        /// The EmployeeID associated with this Quick Item.  Each employee may create their own Quick Item.  If the user has the appropriate rights, she can share her Quick Item so that it is globally accessible (though only she can edit it).
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? EmployeeID { get; set; }
        /// <summary>
        /// A flag used for Employee Quick Items to indicate that they are shared Globally.
        /// </summary>
        public bool IsShared { get; set; }
        /// <summary>
        /// A Count of the number of Line Items in this Quick Item. 
        /// </summary>
        public byte LineItemCount { get; set; }
        /// <summary>
        /// A computed flag indicating if this Quick Item is available in the Global Quick Items list.  Items are available in the Global List if they:
        ///<para>1) Have no associated Company or Employee.</para>
        ///<para>2) Have an associated Employee but the IsShared is set to true. </para>
        /// </summary>
        public bool IsGlobalItem { get; set; }
        /// <summary>
        /// A computed flag indicating the Quick Item is for a specific Company.
        /// </summary>
        public bool IsCompanyItem { get; set; }
        /// <summary>
        /// A JSON record of the Quick Item.  This is the serialized version of a Line Item collection.
        /// </summary>
        public string DataJSON { get; set; }

        /// <summary>
        /// This will deserialize from the DataJSON field
        /// </summary>
        public QuickItemDef DataContent { get; set; }

        [JsonIgnore]
        public ICollection<QuickItemCategoryLink> CategoryLinks { get; set; }
        /// <summary>
        /// A link to a list of Quick Item Categories for this Quick Item.  This link is established through the Part.QuickItem.CategoryLink table.
        /// </summary>

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<FlatListItem> Categories { get; set; }

        /// <summary>
        /// The Company this Quick Item is for. Only available if CompanyID is not null.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CompanyData Company { get; set; }

        /// <summary>
        /// The Employee this Quick Item is for. Only available if EmployeeID is not null.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData Employee { get; set; }
    }

    public class QuickItemDef
    {
        public QuickOrdersHeaderSchema Header { get; set; }

        public List<DestinationInformationSchema> Destinations { get; set; }

        public List<LineItemInformationSchema> Items { get; set; }
    }

    public class QuickOrdersHeaderSchema
    {
        public int CompanyID { get; set; }
        public int ContactID { get; set; }
        public string Description { get; set; }
        public bool HasDocuments { get; set; }
        public int OriginID { get; set; }
        public string EmployeeRoles { get; set; }
        public string CustomFields { get; set; }
    }

    public class DestinationInformationSchema
    {
        //TBD
    }

    public class LineItemInformationSchema
    {
        public int ID { get; set; }
        public string TempID { get; set; }
        public int ItemNumber { get; set; }
        public decimal Quantity { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsOutsourced { get; set; }
        public bool HasProof { get; set; }
        public bool HasDocuments { get; set; }
        public bool HasCustomImage { get; set; }
        public List<ComponentInformationSchema> Components  { get; set; }
    }

    public class ComponentInformationSchema: IOrderItemComponentInformation
    {
        public short Number { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? TotalQuantity { get; set; }
        public bool TotalQuantityOV { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Unit? QuantityUnit { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? MaterialID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? LaborID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? MachineID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? AssemblyID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? DestinationID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? AssemblyVariableID { get; set; }
        public string AssemblyDataJSON { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ComponentID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemComponentType ComponentType { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceUnit { get; set; }
        public bool PriceUnitOV { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostUnit { get; set; }
        public bool CostOV { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ExpenseAccountID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? IncomeAccountID { get; set; }
    }
    public interface IOrderItemComponentInformation
    {
        short Number { get; set; }
        string Name { get; set; }
        decimal? TotalQuantity { get; set; }
        bool TotalQuantityOV { get; set; }
        Unit? QuantityUnit { get; set; }
        string Description { get; set; }
        int? MaterialID { get; set; }
        int? LaborID { get; set; }
        short? MachineID { get; set; }
        int? AssemblyID { get; set; }
        short? DestinationID { get; set; }
        int? AssemblyVariableID { get; set; }
        int ComponentID { get; set; }
        OrderItemComponentType ComponentType { get; set; }
        decimal? PriceUnit { get; set; }
        bool PriceUnitOV { get; set; }
        decimal? CostUnit { get; set; }
        bool CostOV { get; set; }
        int? ExpenseAccountID { get; set; }
        int? IncomeAccountID { get; set; }
    }
}
