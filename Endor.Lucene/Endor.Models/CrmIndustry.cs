﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class CrmIndustry : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public bool IsLocked { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? ParentID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsTopLevel { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CrmIndustry ParentIndustry { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<CompanyData> Companies { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<CrmIndustry> ChildIndustries { get; set; }
    }
}
