﻿using System;
using System.Runtime.Serialization;

namespace Endor.Models
{
    [Serializable]
    public class UnitConversionException : Exception
    {
        public UnitConversionException()
        {
        }

        public UnitConversionException(string message) : base(message)
        {
        }

        public UnitConversionException(Unit u, Unit toUnit) 
            : base($"Invalid unit conversion between {u.Info().NameUS} and {toUnit.Info().NameUS}"  )
        {
        }

        public UnitConversionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnitConversionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}