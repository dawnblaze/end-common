﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum MaterialConsumptionMethod: byte
    {
        Each = 0,
        Linear = 1,
        Area = 2,
        Volume = 3
    }

    public class EnumMaterialConsumptionMethod
    {
        public MaterialConsumptionMethod ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
