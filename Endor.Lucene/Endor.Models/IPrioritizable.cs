﻿namespace Endor.Models
{
    public interface IPrioritizable
    {
        short? Priority { get; set; }
        bool? IsUrgent { get; set; }
    }
}
