﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class ContactData : IAtom<int>, IImageCandidate
    {
        public ContactData()
        {
            CompanyContactLinks = new HashSet<CompanyContactLink>();
            ContactLocators = new HashSet<ContactLocator>();
            Opportunities = new HashSet<OpportunityData>();
            UserLinks = new HashSet<UserLink>();
        }

        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        [StringLength(16)]
        public string Prefix { get; set; }
        [StringLength(100)]
        public string First { get; set; }
        [StringLength(100)]
        public string Middle { get; set; }
        [StringLength(100)]
        public string Last { get; set; }
        [StringLength(16)]
        public string Suffix { get; set; }
        [StringLength(100)]
        public string NickName { get; set; }
        [StringLength(201)]
        public string ShortName { get; set; }
        [StringLength(238)]
        public string LongName { get; set; }
        public bool? GenderType { get; set; }
        [StringLength(100)]
        public string Department { get; set; }
        public DateTime? BirthDate { get; set; }
        public byte? BirthDayOfMonth { get; set; }
        public byte? BirthMonth { get; set; }
        public short? BirthYear { get; set; }
        public short? TimeZoneID { get; set; }
        public byte? LocationID { get; set; }
        public bool HasImage { get; set; }
        public string Tags { get; set; }
        public byte StatusID { get; set; }
        public EnumCrmCompanyStatus Status { get; set; }

        public BusinessData Business { get; set; }
        public ICollection<CompanyContactLink> CompanyContactLinks { get; set; }
        public LocationData Location { get; set; }
        public EnumTimeZone TimeZone { get; set; }
        public ICollection<ContactLocator> ContactLocators { get; set; }
        public ICollection<OpportunityData> Opportunities { get; set; }
        public ICollection<UserLink> UserLinks { get; set; }
        public ICollection<OrderContactRole> OrderRoles { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<ContactTagLink> TagLinks { get; set; }
    }
}
