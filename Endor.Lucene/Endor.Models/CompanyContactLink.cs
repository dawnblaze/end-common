﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Endor.Models
{
    public class CompanyContactLink
    {
        /// <summary>
        /// Business ID
        /// </summary>
        public short BID { get; set; }

        /// <summary>
        /// CompanyID
        /// </summary>
        public int CompanyID { get; set; }

        /// <summary>
        /// ContactID
        /// </summary>
        public int ContactID { get; set; }

        /// <summary>
        /// ContactRoles enum
        /// </summary>
        public ContactRole Roles { get; set; }

        /// <summary>
        /// If the contact is active for the Company
        /// </summary>
        public bool? IsActive { get; set; }

        /// <summary>
        /// If the Contact is the Primary Contact for the Company
        /// </summary>
        public bool? IsPrimary { get; set; }

        /// <summary>
        /// If the Contact is the Billing Contact for the Company
        /// </summary>
        public bool? IsBilling { get; set; }

        /// <summary>
        /// Contact's Position with the Company
        /// </summary>
        [StringLength(100)]
        public string Position { get; set; }

        public CompanyData Company { get; set; }
        public ContactData Contact { get; set; }
        public EnumContactRoles ContactRole { get; set; }
    }
}
