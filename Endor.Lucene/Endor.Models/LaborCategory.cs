﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class LaborCategory: IAtom<short>
    {
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// A general description of the Category
        /// </summary>
        [StringLength(255)]
        public string Description { get; set; }

        /// <summary>
        /// ID of the Parent Category this Category is under.	
        /// </summary>
        public short? ParentID { get; set; }

        public LaborCategory ParentCategory { get; set; }

        public ICollection<LaborCategory> ChildCategories { get; set; }

        public ICollection<LaborCategoryLink> LaborCategoryLinks { get; set; }

        public ICollection<LaborData> Labors { get; set; }

        public ICollection<SimpleLaborData> SimpleLabors { get; set; }
    }
}
