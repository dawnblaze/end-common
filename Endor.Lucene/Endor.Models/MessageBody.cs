﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/845578283/MessageBody+Object
    /// </summary>
    public partial class MessageBody : IAtom<int>, IDocumentRepository
    {

        [JsonIgnore]
        public short BID { get; set; }

        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(250)]
        public string Subject { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(100)]
        public string BodyFirstLine { get; set; }

        public bool HasBody { get; set; }
        public byte AttachedFileCount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string AttachedFileNames { get; set; }

        public bool HasAttachment { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string MetaData { get; set; }

        public bool WasModified { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? SizeInKB { get; set; }

        //navprops
        public ICollection<MessageObjectLink> MessageObjectLinks { get; set; }
        public ICollection<MessageParticipantInfo> MessageParticipantInfos { get; set; }

    }
}
