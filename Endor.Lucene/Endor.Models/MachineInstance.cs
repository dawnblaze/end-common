﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public class MachineInstance : IAtom<int>
    {
        public int ID { get; set; }
        [JsonIgnore]
        public short BID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string IPAddress { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public bool HasServiceContract { get; set; }
        public string ContractNumber { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public string ContractInfo { get; set; }
        public byte? LocationID { get; set; }
        public short MachineID { get; set; }
        public MachineData Machine { get; set; }
    }
}
