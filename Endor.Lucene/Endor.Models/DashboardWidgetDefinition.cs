﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// System Dashboard Widget Definition
    /// </summary>
    public class DashboardWidgetDefinition : IAtom<short>
    {
        /// <summary>
        /// Business ID - Not used as is system level
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }
        /// <summary>
        /// Widget Definition ID
        /// </summary>
        public short ID { get; set; }
        /// <summary>
        /// Class Type ID
        /// </summary>
        public int ClassTypeID { get; set; }
        /// <summary>
        /// DateTime Last Modified
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The Default Name for the widget.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string DefaultName { get; set; }

        /// <summary>
        /// The Default number of Rows hide for this widget.
        /// </summary>
        public byte DefaultCols { get; set; }

        /// <summary>
        /// The Default number of Cols wide for this widget.
        /// </summary>
        public byte DefaultRows { get; set; }

        /// <summary>
        /// The Default Options for this widget.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string DefaultOptions { get; set; }

        /// <summary>
        /// The DefaultRefreshIntervalInMin for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? DefaultRefreshIntervalInMin { get; set; }

        /// <summary>
        /// The Description for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        /// <summary>
        /// Flag set of valid modules.  If NULL or 0, then all modules are allowed.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? Modules { get; set; }

        /// <summary>
        /// The SecurityRightID for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? SecurityRightID { get; set; }

        ///// <summary>
        ///// The Categories for this object as a string list
        ///// </summary>
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //[NotMapped]
        //public List<string> CategoriesList
        //{
        //    get { return (!string.IsNullOrWhiteSpace(Categories)) ? Categories.Split(',').ToList() : null; }
        //}

        ///// <summary>
        ///// The Categories for this object.
        ///// </summary>
        //[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        //[Column("Categories", TypeName = "nvarchar(MAX)")]
        //public string Categories { get; set; }

        /// <summary>
        /// The Minimum number of Rows this Widget knows how to render.
        /// </summary>
        public byte MinRows { get; set; }

        /// <summary>
        /// The Maximum number of Rows this Widget knows how to render.
        /// </summary>
        public byte MaxRows { get; set; }

        /// <summary>
        /// The Minimum number of Columns this Widget knows how to render.
        /// </summary>
        public byte MinCols { get; set; }

        /// <summary>
        /// The Maximum number of Columns this Widget knows how to render.
        /// </summary>
        public byte MaxCols { get; set; }

        /// <summary>
        /// Flag indicating if a unique image is stored in DM for this object.  If False, the default image for the classtype is used instead.
        /// </summary>
        public bool HasImage { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public DashboardWidgetDefinition()
        {
            MinCols = 1;
            MaxCols = 1;
            MaxRows = 1;
            MinRows = 1;

            ModifiedDT = DateTime.UtcNow;
        }

        /// <summary>
        /// If this widget can be resized
        /// </summary>
        /// <returns></returns>
        public bool CanResize() { return (MaxRows != MinRows || MaxCols != MinCols ); }

        /// <summary>
        /// The widgets associated with this widget definition.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<DashboardWidgetData> Widgets { get; set; }

        /// <summary>
        /// The Category Link associated with this widget definition.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<DashboardWidgetCategoryLink> WidgetCategoryLinks { get; set; }
    }
}
