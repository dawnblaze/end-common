﻿using System;
using System.Collections.Generic;

namespace Endor.Models
{
    public class Message : IAtom<int>, IDocumentRepository<int>
    {


        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }
        public DateTime ReceivedDT { get; set; }
        public short EmployeeID { get; set; }
        public int ParticipantID { get; set; }
        public int BodyID { get; set; }
        public bool IsRead { get; set; }
        public DateTime? ReadDT { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsExpired { get; set; }
        public DateTime? DeletedOrExpiredDT { get; set; }
        public MessageChannelType Channels { get; set; }
        public bool IsSentFolder { get; set; }

        public string Subject { get; set; }
        public string BodyFirstLine { get; set; }
        public bool HasBody { get; set; }
        public byte AttachedFileCount { get; set; }
        public string AttachedFileNames { get; set; }
        public bool HasAttachment { get; set; }
        public string MetaData { get; set; }
        public bool WasModified { get; set; }
        public int? SizeInKB { get; set; }

        public string Body { get; set; }
        public ICollection<MessageParticipantInfo> Participants { get; set; }
        public ICollection<MessageObjectLink> ObjectLinks { get; set; }
        public ICollection<MessageDeliveryRecord> DeliveryRecords { get; set; }
    }
}
