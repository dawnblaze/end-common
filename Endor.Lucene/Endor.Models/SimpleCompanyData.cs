﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public partial class SimpleCompanyData : SimpleListItem<int>
    {
        public bool IsAdHoc { get; set; }
    }
}
