﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum CustomFieldNumberDisplayType: byte
    {
        General = 0,
        Fixed = 1,
        Currency = 2,
        Accounting = 3
    }

    public partial class EnumCustomFieldNumberDisplayType
    {
        public CustomFieldNumberDisplayType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
