﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum AssemblyTableType : byte
    {
        Custom = 0,
        PercentageDiscount = 1,
        UnitPrice = 2,
        Margin = 3,
        Multiplier = 4
    }

    public class EnumAssemblyTableType
    {
        public AssemblyTableType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
