﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class AlertDefinition: IAtom<short>, IImageCandidate
    {
        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsSystem { get; set; }
        public bool IsActive { get; set; }

        public bool HasImage { get; set; }

        [Required]
        [MaxLength(255)]
        public string Name { get; set; }

        public short EmployeeID { get; set; }

        public string Description { get; set; }

        public DataType DataType { get; set; }

        public short TriggerID { get; set; }

        public string TriggerConditionFx { get; set; }

        public string TriggerReadable { get; set; }

        public string ConditionFx { get; set; }

        public string ConditionReadable { get; set; }

        public string ConditionSQL { get; set; }


        public DateTime? LastRunDT { get; set; }

        public int? CummRunCount { get; set; }

        public BusinessData Business { get; set; }

        public EmployeeData Employee { get; set; }

        public SystemAutomationTriggerDefinition Trigger { get; set; }

        public ICollection<AlertDefinitionAction> Actions { get; set; }
    }
}
