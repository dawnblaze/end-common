﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Endor.Models
{
    public class OrderContactRole : IAtom<int>
    {
        public OrderContactRole()
        {
            OrderContactRoleLocators = new HashSet<OrderContactRoleLocator>();
        }

        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public int OrderID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? OrderItemID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? DestinationID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public OrderContactRoleType RoleType { get; set; }
        public bool IsAdHoc { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ContactID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ContactName { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string ContactCompany { get; set; }
        [JsonIgnore]
        public bool IsOrderRole { get; set; }
        [JsonIgnore]
        public bool IsOrderItemRole { get; set; }
        [JsonIgnore]
        public bool IsDestinationRole { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TransactionHeaderData Order { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ContactData Contact { get; set; }

        public ICollection<OrderContactRoleLocator> OrderContactRoleLocators { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemData OrderItem { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderDestinationData Destination { get; set; }
    }
}
