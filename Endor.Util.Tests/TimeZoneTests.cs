using Endor.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Endor.Util.Test
{
    [TestClass]
    public class TimeZoneTests
    {
        [TestMethod]
        public void Test_DateTimeTZ_Quarter()
        {
            DateTime dt = new DateTime(2019, 06, 11, 18, 55, 30);
            DateTime quarterstart = new DateTime(2019, 04, 01);
            DateTime quarterend = new DateTime(2019, 06, 30, 23, 59, 59);

            DateRange PST = new DateRangeTZ(StandardDateRangeType.ThisQuarter, dt, "Pacific Standard Time");
            Assert.AreEqual(PST.StartDT, quarterstart.AddHours(7), "PST Quarter Start Date Failed");
            Assert.AreEqual(PST.EndDT, quarterend.AddHours(7), "PST Quarter End Date Failed");


            // now compare timeZone differentials

            DateRange EST = new DateRangeTZ(StandardDateRangeType.ThisQuarter, dt, "Eastern Standard Time");
            double diff = (EST.StartDT - PST.StartDT).TotalHours;

            // compute the timezone offset of the machine the test is running on

            Assert.AreNotEqual(EST, PST, "TimeZones Should have different times");
            Assert.AreEqual(diff, -3.0, "EST and PST timeZones Should only 3 hours apart");
        }
    }
}
