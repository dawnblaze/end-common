using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Endor.Util.Test
{
    [TestClass]
    public class DateAndTimeTests
    {
        [TestMethod]
        public void Test_DateTime_Quarter()
        {
            DateTime dt = new DateTime(2019, 06, 11, 18, 55, 30);
            DateTime quarterstart = new DateTime(2019, 04, 01);
            DateTime quarterend = new DateTime(2019, 06, 30, 23, 59, 59);

            DateRange range = new DateRange(StandardDateRangeType.ThisQuarter, dt);

            Assert.AreEqual(quarterstart, range.StartDT, "Quarter Start Date Failed");
            Assert.AreEqual(quarterend, range.EndDT, "Quarter End Date Failed");
        }

        [TestMethod]
        public void Test_FriendlyText_Annotation()
        {
            StandardDateRangeType range = StandardDateRangeType.ThisQuarter;

            Assert.AreEqual(range.DisplayText(), "This Quarter", "Annotation on StandardDateRangeType failed.");
        }
    }
}
