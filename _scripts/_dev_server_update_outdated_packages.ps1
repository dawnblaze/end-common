param (
   [Parameter(Mandatory)][string]$csprojPath,
   [Parameter(Mandatory)][string]$mygetkey
)

Write-Host "Updating outdated Packages on $csprojPath" -BackgroundColor Yellow -ForegroundColor Black

$raw = & dotnet list "..\..\$csprojPath" package --outdated
#raw `dotnet list` contains a lot of info, let's whittle it down
$outdatedPackages = $raw | where { 
	# only look for endor packages
	$_ -match "Endor.+" 
} | % {
	# get rid of some stuff the CLI adds
	$_.Trim().TrimStart('>',' ')
} | where { 
	# only match `endor*` packages
	$_ -match "^Endor.+" 
} | % {
	#and get rid of the version stuff at the end
	$_.Substring(0, $_.IndexOf(' '))
}
Write-Host "Outdated Packages on $csprojPath :"
Write-Host $outdatedPackages

$outdatedPackages | ForEach-Object {
	#https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-add-package
	& dotnet add "..\..\$csprojPath" package $_
}
