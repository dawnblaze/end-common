param (
   [Parameter(Mandatory)][string]$csprojPath,
   [Parameter(Mandatory)][string]$package,
   [Parameter(Mandatory)][string]$version
)

& dotnet build $csprojPath

Write-Host "Committing '$package $version'" -BackgroundColor Yellow -ForegroundColor Black
pushd (Split-Path -Path $csprojPath)
git commit -a -m "$package $version"
popd