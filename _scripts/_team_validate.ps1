$isTeamBranch = & .\_getIsTeamBranch

if ($isTeamBranch -eq $false) {
	Write-Host "Preventing install of team package, git is not currently on a team branch" -BackgroundColor Red -ForegroundColor White	
	return $false
}

$yn = & .\_readChoice 'Unit Tests' '&yes','&no' '&no' 'Did all Unit Tests pass?'

switch($yn) 
{
	'&yes' 
	{
		return $true
	}
	'&no'
	{
		Write-Host "Run and fix all unit tests!" -BackgroundColor Red -ForegroundColor White
		return $false
	}
}

return $false

