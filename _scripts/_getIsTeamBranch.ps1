$branch = git rev-parse --abbrev-ref HEAD

if ($branch -eq 'dev') {
	return $false
}
	
if ($branch -eq 'master') {
	return $false
}

if ($branch.StartsWith("rel/")) {
	return $false
}

if ($branch.StartsWith("relfix/")) {
	return $false
}
	
return $true
