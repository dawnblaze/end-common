$isTeamBranch = & .\_getIsTeamBranch

if ($isTeamBranch -eq $true) {
	Write-Host "Preventing update of live package, git is not currently on a live branch" -BackgroundColor Red -ForegroundColor White	
	return $false
}

$yn = & .\_readChoice 'Unit Tests' '&yes','&no' '&no' 'Did all Unit Tests pass?'

switch($yn) 
{
	'&yes' 
	{
		return $true
	}
	'&no'
	{
		Write-Host "Run and fix all unit tests!" -BackgroundColor Red -ForegroundColor White
		return $false
	}
}

return $false

