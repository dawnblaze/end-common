param (
   [Parameter(Mandatory)][string]$csprojPath,
   [Parameter(Mandatory)][string]$package,
   [Parameter(Mandatory)][string]$mygetkey,
   [string]$version
)

$raw = & dotnet list $csprojPath package
#raw `dotnet list` contains a lot of info, let's whittle it down
$packages = $raw | where { 
	# only look for endor packages
	$_ -match "Endor.+" 
} | % {
	# get rid of some stuff the CLI adds
	$_.Trim().TrimStart('>',' ')
} | where { 
	# only match `endor*` packages
	$_ -match "^$package+" 
} | % {
	#and get rid of the version stuff at the end
	$_.Substring(0, $_.IndexOf(' '))
}
if ($packages.Length -lt 1) {
	Write-Host "$package is not installed in $csprojPath !" -BackgroundColor Red -ForegroundColor White
	return $false
}
pushd (Split-Path -Path $csprojPath)
$branch = git rev-parse --abbrev-ref HEAD
popd

Write-Host "$csprojPath is currently on $branch branch" -BackgroundColor Yellow -ForegroundColor Black

if ($branch -ne 'dev' -and $branch.startsWith('rel/') -eq $false) {
	Write-Host "$csprojPath is not currently on dev or release branch!" -BackgroundColor Red -ForegroundColor White
	return $false
}

if ($version) {
	Write-Host "Installing $package v$version in $csprojPath" -BackgroundColor Yellow -ForegroundColor Black
	#https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-add-package
	& dotnet add $csprojPath package $package -v $version
} else {
	Write-Host "Installing latest $package in $csprojPath" -BackgroundColor Yellow -ForegroundColor Black
	#https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-add-package
	& dotnet add $csprojPath package $package
}
