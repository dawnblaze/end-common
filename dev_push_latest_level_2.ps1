param (
    [Parameter(Mandatory=$true)][string]$mygetkey
)

function Publish(
   [Parameter(Mandatory)][string]$project,
   [Parameter(Mandatory)][string]$mygetkey
) {
	& ./dev_push_latest_package.ps1 $project $mygetkey
}

pushd .\_scripts
$valid = .\_dev_validate
popd

if ($valid) {
	Publish 'Endor.AEL' $mygetkey

	Publish 'Endor.AzureStorage' $mygetkey

	Publish 'Endor.EF' $mygetkey

	Publish 'Endor.Logging.Client' $mygetkey

	Publish 'Endor.Lucene' $mygetkey

	Publish 'Endor.Tenant.Cache.Controller' $mygetkey

	Publish 'Endor.Tenant.Cache.Controller.Net462' $mygetkey
}

