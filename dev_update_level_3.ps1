param (
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [string]$build = "Debug"
)
pushd .\_scripts
$valid = .\_dev_validate
popd

if ($valid) {
	& .\_dev_publish_package 'Endor.Api.Common' $mygetkey

	& .\_dev_publish_package 'Endor.GLEngine' $mygetkey

	& .\_dev_publish_package 'Endor.Pricing' $mygetkey	
}

