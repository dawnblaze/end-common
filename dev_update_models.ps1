param (
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [string]$build = "Debug"
)
pushd .\_scripts
$isTeamBranch = & .\_getIsTeamBranch
popd

if ($isTeamBranch -eq $true) {
	Write-Host "Preventing update of live package, git is not currently on a live branch" -BackgroundColor Red -ForegroundColor White	
	exit;
}

$yn = & .\_scripts\_readChoice 'Unit Tests' '&yes','&no' '&no' 'Did all Unit Tests pass?'

switch($yn) 
{
	'&yes' 
	{
	}
	'&no'
	{
		Write-Host "Run and fix all unit tests!" -BackgroundColor Red -ForegroundColor White
		exit;
	}
}

& .\_dev_publish_package 'Endor.Models' $mygetkey

