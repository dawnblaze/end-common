﻿using Endor.CBEL.Common;
using Endor.CBEL.Compute;
using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Units;
using Endor.Logging.Client;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class LinkedAssemblyTests
    {
        private ApiContext ctx = null;

        private const short BID = 1;
        private PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestMethod]
        public async Task LinkedAssemblyCompileAndComputeTest()
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);
            int companyID = -99;
            decimal parentQuantity = 500m;
            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "12",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "24",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyPrice
                }
            };

            AssemblyVariable parentAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyHeightFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Height",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Height.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyWidthFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Width",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Width.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
                parentAssemblyHeightFormula,
                parentAssemblyWidthFormula
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyHeight,
                    parentAssemblyWidth,
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice
                }
            };

            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
            ICBELAssembly cbelParentAssembly = await parentGen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelParentAssembly);

            CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);

            var saveResp = await childGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1m,
                QuantityOV = true,
                CompanyID = companyID,
                ParentQuantity = parentQuantity,
                IsVended = true,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "Height",
                        IsOV = true,
                        Value = "2",
                    },
                    new VariableData()
                    {
                        VariableName = "Width",
                        IsOV = true,
                        Value = "16",
                    }
                },
                OVChildValues = new List<ICBELOverriddenValues>()
                {
                    new CBELOverriddenValues()
                    {
                        ComponentID = childID,
                        VariableName = childName
                    }
                }
            };
            ICBELComputeResult result = cbelParentAssembly.Compute(ovValues);

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);
            Assert.AreEqual(myAssembly.IsVended, true);
            Assert.AreEqual(myAssembly.CompanyID, companyID);
            Assert.AreEqual(myAssembly.ParentQuantity, parentQuantity);

            IVariableData priceData = result.VariableData.FirstOrDefault(x => x.VariableName == "Price");
            Assert.IsNotNull(priceData);
            Assert.AreEqual(32, decimal.Parse(priceData.Value));

            IVariableData heightData = result.VariableData.FirstOrDefault(x => x.VariableName == "Height");
            Assert.IsNotNull(heightData);
            Assert.AreEqual(2, decimal.Parse(heightData.Value));

            IVariableData widthData = result.VariableData.FirstOrDefault(x => x.VariableName == "Width");
            Assert.IsNotNull(widthData);
            Assert.AreEqual(16, decimal.Parse(widthData.Value));

            Assert.AreEqual(1, result.Results.Count);

            priceData = result.Results[0].VariableData.FirstOrDefault(x => x.VariableName == "Price");
            Assert.IsNotNull(priceData);
            Assert.AreEqual(8, decimal.Parse(priceData.Value));

            heightData = result.Results[0].VariableData.FirstOrDefault(x => x.VariableName == "Height");
            Assert.IsNotNull(heightData);
            Assert.AreEqual(1, decimal.Parse(heightData.Value));

            widthData = result.Results[0].VariableData.FirstOrDefault(x => x.VariableName == "Width");
            Assert.IsNotNull(widthData);
            Assert.AreEqual(8, decimal.Parse(widthData.Value));
        }

        [TestMethod]
        public async Task LinkedAssemblyWithUnitConversionCompileAndComputeTest()
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);
            int companyID = -99;
            decimal parentQuantity = 500m;
            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "12",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "24",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyPrice
                }
            };

            AssemblyVariable parentAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Foot,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Foot,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID,
            };

            AssemblyVariableFormula parentAssemblyHeightFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Height",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "Height",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyWidthFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Width",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "Width",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
                parentAssemblyHeightFormula,
                parentAssemblyWidthFormula
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyHeight,
                    parentAssemblyWidth,
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice
                }
            };

            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
            ICBELAssembly cbelParentAssembly = await parentGen.GetAssemblyInstance(ctx, cache, logger);

            Assert.AreEqual(0, parentGen.Errors.Count, string.Join("\r\n", parentGen.Errors));
            Assert.IsNotNull(cbelParentAssembly);

            CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);

            var saveResp = await childGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1m,
                QuantityOV = true,
                CompanyID = companyID,
                ParentQuantity = parentQuantity,
                IsVended = true,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "Height",
                        IsOV = true,
                        Value = "2",
                    },
                    new VariableData()
                    {
                        VariableName = "Width",
                        IsOV = true,
                        Value = "1",
                    }
                },
                OVChildValues = new List<ICBELOverriddenValues>()
                {
                    new CBELOverriddenValues()
                    {
                        ComponentID = childID,
                        VariableName = childName
                    }
                }
            };
            ICBELComputeResult result = cbelParentAssembly.Compute(ovValues);

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);
            Assert.AreEqual(myAssembly.IsVended, true);
            Assert.AreEqual(myAssembly.CompanyID, companyID);
            Assert.AreEqual(myAssembly.ParentQuantity, parentQuantity);

            IVariableData heightData = result.VariableData.FirstOrDefault(x => x.VariableName == "Height");
            Assert.IsNotNull(heightData);
            Assert.AreEqual(2, decimal.Parse(heightData.Value));

            IVariableData widthData = result.VariableData.FirstOrDefault(x => x.VariableName == "Width");
            Assert.IsNotNull(widthData);
            Assert.AreEqual(1, decimal.Parse(widthData.Value));

            IVariableData priceData = result.VariableData.FirstOrDefault(x => x.VariableName == "Price");
            Assert.IsNotNull(priceData);
            Assert.AreEqual(2, decimal.Parse(priceData.Value));

            Assert.AreEqual(1, result.Results.Count);

            heightData = result.Results[0].VariableData.FirstOrDefault(x => x.VariableName == "Height");
            Assert.IsNotNull(heightData);
            Assert.AreEqual(24, decimal.Parse(heightData.Value));

            widthData = result.Results[0].VariableData.FirstOrDefault(x => x.VariableName == "Width");
            Assert.IsNotNull(widthData);
            Assert.AreEqual(12, decimal.Parse(widthData.Value));

            priceData = result.Results[0].VariableData.FirstOrDefault(x => x.VariableName == "Price");
            Assert.IsNotNull(priceData);
            Assert.AreEqual(288, decimal.Parse(priceData.Value));
        }

        [TestMethod]
        public async Task LinkedAssemblyCompileAndComputeParentQuantityTest()
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);

            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "12",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "24",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable childAssemblyParentQuantity = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "ChildParentQuantityTest",
                Label = "Parent Quantity",
                DataType = DataType.Number,
                DefaultValue = "=ParentQuantity",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyPrice,
                    childAssemblyParentQuantity
                }
            };

            AssemblyVariable parentAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyHeightFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Height",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Height.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyWidthFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Width",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Width.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
                parentAssemblyHeightFormula,
                parentAssemblyWidthFormula
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable parentAssemblyParentQuantity = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "ParentQuantityTest",
                Label = "Parent Quantity Test",
                DataType = DataType.Number,
                DefaultValue = "=ParentQuantity * 2",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyHeight,
                    parentAssemblyWidth,
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice,
                    parentAssemblyParentQuantity
                }
            };

            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
            ICBELAssembly cbelParentAssembly = await parentGen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelParentAssembly);

            CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);
            ICBELAssembly cbelChildAssembly = await childGen.GetAssemblyInstance(ctx, cache, logger);

            var saveResp = await childGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1,
                ParentQuantity = 1,
                QuantityOV = true,
                IsVended = true,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "Height",
                        IsOV = true,
                        Value = "2",
                    },
                    new VariableData()
                    {
                        VariableName = "Width",
                        IsOV = true,
                        Value = "16",
                    }
                },
                OVChildValues = new List<ICBELOverriddenValues>()
                {
                    new CBELOverriddenValues()
                    {
                        ComponentID = childID,
                        VariableName = childName,
                        QuantityOV = true,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "Height",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "Width",
                                IsOV = true,
                                Value = "16",
                            }
                        }
                    }
                }
            };
            ICBELComputeResult result = cbelParentAssembly.Compute(ovValues);

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            //var myComponents = myAssembly.Components.FirstOrDefault(c => c.ComponentClassType == ComponentType.Assembly);
            //Assert.IsNotNull(myComponents);

            //parent assembly
            IVariableData parentQuantityVar = result.VariableData.FirstOrDefault(x => x.VariableName == "ParentQuantityTest");
            Assert.IsNotNull(parentQuantityVar);
            Assert.AreEqual("2", parentQuantityVar.Value);

            //child assembly
            ICBELComputeResult childResult = cbelChildAssembly.Compute(ovValues);
            IVariableData childQuantityVar = childResult.VariableData.FirstOrDefault(x => x.VariableName == "ChildParentQuantityTest");
            Assert.IsNotNull(childQuantityVar);
            Assert.AreEqual("1", childQuantityVar.Value);
        }

        [TestMethod]
        public async Task LinkedAssemblyCompileAndComputeCompanyNameTest()
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);

            var companyData = ctx.CompanyData.FirstOrDefault(t => t.BID == BID);
            Assert.IsNotNull(companyData);

            Assert.IsNotNull(companyData);
            string companyName = companyData.Name;
            int companyID = companyData.ID;

            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            ///
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "12",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "24",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable childAssemblyCompanyName = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "ChildCompanyNameTest",
                Label = "Company Name",
                DataType = DataType.String,
                DefaultValue = "=Company.Name",
                ElementType = AssemblyElementType.SingleLineText,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyPrice,
                    childAssemblyCompanyName
                }
            };

            AssemblyVariable parentAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyHeightFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Height",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Height.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyWidthFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Width",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Width.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
                parentAssemblyHeightFormula,
                parentAssemblyWidthFormula
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable parentAssemblyCompanyName = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "CompanyNameTest",
                Label = "Company Name",
                DataType = DataType.String,
                DefaultValue = "=Company.Name",
                ElementType = AssemblyElementType.SingleLineText,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyHeight,
                    parentAssemblyWidth,
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice,
                    parentAssemblyCompanyName
                }
            };

            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
            ICBELAssembly cbelParentAssembly = await parentGen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelParentAssembly);

            CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);
            ICBELAssembly cbelChildAssembly = await childGen.GetAssemblyInstance(ctx, cache, logger);

            var saveResp = await childGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1,
                QuantityOV = true,
                CompanyID = companyData.ID,
                IsVended = true,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "Height",
                        IsOV = true,
                        Value = "2",
                    },
                    new VariableData()
                    {
                        VariableName = "Width",
                        IsOV = true,
                        Value = "16",
                    }
                },
                OVChildValues = new List<ICBELOverriddenValues>()
                {
                    new CBELOverriddenValues()
                    {
                        ComponentID = childID,
                        VariableName = childName,
                        QuantityOV = true,
                    }
                }
            };
            ICBELComputeResult result = cbelParentAssembly.Compute(ovValues);

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            //parent assembly
            IVariableData companyNameVar = result.VariableData.FirstOrDefault(x => x.VariableName == "CompanyNameTest");
            Assert.IsNotNull(companyNameVar);
            Assert.AreEqual(companyData.Name, companyNameVar.Value);

            //child assembly
            ICBELComputeResult childResult = cbelChildAssembly.Compute(ovValues);
            IVariableData childCompanyNameVar = childResult.VariableData.FirstOrDefault(x => x.VariableName == "ChildCompanyNameTest");
            Assert.IsNotNull(companyNameVar);
            Assert.AreEqual(companyData.Name, childCompanyNameVar.Value);
        }

        [TestMethod]
        public async Task LinkedAssemblyCompileAndComputeIsVendedTest()
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);

            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "12",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "24",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable childAssemblyIsVended = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "ChildIsVendedTest",
                Label = "Is Vended",
                DataType = DataType.String,
                DefaultValue = "=IF(IsVended = true, \"Vended\", \"Manufactured\") ",
                ElementType = AssemblyElementType.SingleLineText,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyPrice,
                    childAssemblyIsVended
                }
            };

            AssemblyVariable parentAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyHeightFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Height",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Height.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyWidthFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Width",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Width.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
                parentAssemblyHeightFormula,
                parentAssemblyWidthFormula
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable parentAssemblyIsVended = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "IsVendedTest",
                Label = "Is Vended",
                DataType = DataType.String,
                DefaultValue = "=IF(IsVended = true, \"Vended\", \"Manufactured\") ",
                ElementType = AssemblyElementType.SingleLineText,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyHeight,
                    parentAssemblyWidth,
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice,
                    parentAssemblyIsVended
                }
            };

            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
            ICBELAssembly cbelParentAssembly = await parentGen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelParentAssembly);

            CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);
            ICBELAssembly cbelChildAssembly = await childGen.GetAssemblyInstance(ctx, cache, logger);

            var saveResp = await childGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1,
                QuantityOV = true,
                IsVended = true,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "Height",
                        IsOV = true,
                        Value = "2",
                    },
                    new VariableData()
                    {
                        VariableName = "Width",
                        IsOV = true,
                        Value = "16",
                    }
                },
                OVChildValues = new List<ICBELOverriddenValues>()
                {
                    new CBELOverriddenValues()
                    {
                        ComponentID = childID,
                        VariableName = childName,
                        QuantityOV = true,
                    }
                }
            };
            ICBELComputeResult result = cbelParentAssembly.Compute(ovValues);

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            //parent
            IVariableData isVendedVar = result.VariableData.FirstOrDefault(x => x.VariableName == "IsVendedTest");
            Assert.IsNotNull(isVendedVar);
            Assert.AreEqual("Vended", isVendedVar.Value);

            //child
            //parent
            ICBELComputeResult childResult = cbelChildAssembly.Compute(ovValues);
            IVariableData childIsVendedVar = childResult.VariableData.FirstOrDefault(x => x.VariableName == "ChildIsVendedTest");
            Assert.IsNotNull(childIsVendedVar);
            Assert.AreEqual("Vended", childIsVendedVar.Value);
        }

        [TestMethod]
        public async Task NullLinkedFormulaCompileAndComputeTest()
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);

            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "12",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "24",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable childAssemblyIsVended = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "ChildIsVendedTest",
                Label = "Is Vended",
                DataType = DataType.String,
                DefaultValue = "=IF(IsVended = true, \"Vended\", \"Manufactured\") ",
                ElementType = AssemblyElementType.SingleLineText,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyPrice,
                    childAssemblyIsVended
                }
            };

            AssemblyVariable parentAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyHeightFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Height",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Height.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyWidthFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "Width",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "=ROUND(Width.Value/2, 1)",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = true,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyNullFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = null,
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = null,
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
                parentAssemblyHeightFormula,
                parentAssemblyWidthFormula,
                parentAssemblyNullFormula
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable parentAssemblyIsVended = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "IsVendedTest",
                Label = "Is Vended",
                DataType = DataType.String,
                DefaultValue = "=IF(IsVended = true, \"Vended\", \"Manufactured\") ",
                ElementType = AssemblyElementType.SingleLineText,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable parentAssemblyNullFormulaVariable = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "NullFormulaVariable",
                Label = null,
                DataType = DataType.String,
                DefaultValue = null,
                ElementType = AssemblyElementType.SingleLineText,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable parentAssemblyNullValueVariable = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "NullValueVariable",
                Label = null,
                DataType = DataType.String,
                DefaultValue = null,
                ElementType = AssemblyElementType.SingleLineText,
                AllowDecimals = true,
                IsFormula = false,
            };

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyHeight,
                    parentAssemblyWidth,
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice,
                    parentAssemblyIsVended,
                    parentAssemblyNullFormulaVariable,
                    parentAssemblyNullValueVariable
                }
            };

            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
            ICBELAssembly cbelParentAssembly = await parentGen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelParentAssembly);

            CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);
            ICBELAssembly cbelChildAssembly = await childGen.GetAssemblyInstance(ctx, cache, logger);

            var saveResp = await childGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1,
                QuantityOV = true,
                IsVended = true,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "Height",
                        IsOV = true,
                        Value = "2",
                    },
                    new VariableData()
                    {
                        VariableName = "Width",
                        IsOV = true,
                        Value = "16",
                    }
                },
                OVChildValues = new List<ICBELOverriddenValues>()
                {
                    new CBELOverriddenValues()
                    {
                        ComponentID = childID,
                        VariableName = childName,
                        QuantityOV = true,
                    }
                }
            };
            ICBELComputeResult result = cbelParentAssembly.Compute(ovValues);

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            //parent
            IVariableData isVendedVar = result.VariableData.FirstOrDefault(x => x.VariableName == "IsVendedTest");
            Assert.IsNotNull(isVendedVar);
            Assert.AreEqual("Vended", isVendedVar.Value);

            IVariableData nullFormulaVar = result.VariableData.FirstOrDefault(x => x.VariableName == "NullFormulaVariable");
            Assert.IsNotNull(nullFormulaVar);
            Assert.IsNull(nullFormulaVar.Value);

            IVariableData nullValueVar = result.VariableData.FirstOrDefault(x => x.VariableName == "NullValueVariable");
            Assert.IsNotNull(nullValueVar);
            Assert.IsNull(nullValueVar.Value);

            //child
            ICBELComputeResult childResult = cbelChildAssembly.Compute(ovValues);
            IVariableData childIsVendedVar = childResult.VariableData.FirstOrDefault(x => x.VariableName == "ChildIsVendedTest");
            Assert.IsNotNull(childIsVendedVar);
            Assert.AreEqual("Vended", childIsVendedVar.Value);
        }

        [TestMethod]
        public async Task LinkedAssemblyCompileAndComputeVariableReferenceTest()
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);

            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyArea = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Area",
                Label = "Area",
                DataType = DataType.Number,
                DefaultValue = "=Height.Value *Width.Value",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.SquareInch,
                IsFormula = true,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };



            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyArea,
                    childAssemblyPrice,
                }
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariable parentAssemblyChildArea = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "ChildArea",
                Label = "ChildArea",
                DataType = DataType.String,
                DefaultValue = $"={childName}.Area.Value",
                IsFormula = true,
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice,
                    parentAssemblyChildArea
                }
            };

            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
            ICBELAssembly cbelParentAssembly = await parentGen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelParentAssembly);

            CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);
            ICBELAssembly cbelChildAssembly = await childGen.GetAssemblyInstance(ctx, cache, logger);

            var saveResp = await childGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            ICBELComputeResult result = cbelParentAssembly.Compute();

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            IVariableData varChildArea = result.VariableData.FirstOrDefault(x => x.VariableName == "ChildArea");
            Assert.IsNotNull(varChildArea);
            Assert.IsNull(varChildArea.Value);


            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1,
                QuantityOV = true,

                OVChildValues = new List<ICBELOverriddenValues>
                {
                    new CBELOverriddenValues()
                {
                    VariableName = childName,
                    VariableData = new List<IVariableData>()
                    {
                        new VariableData()
                        {
                            VariableName = "Height",
                            IsOV = true,
                            Value = "6",
                        },
                        new VariableData()
                        {
                            VariableName = "Width",
                            IsOV = true,
                            Value = "4",
                        },
                    }
                }
                }
            };
            result = cbelParentAssembly.Compute(ovValues);

            varChildArea = result.VariableData.FirstOrDefault(x => x.VariableName == "ChildArea");
            Assert.IsNotNull(varChildArea);
            Assert.AreEqual("24", varChildArea.Value);
        }

        [TestMethod]
        public async Task LinkedAssemblyCompileAndComputeIsIncludedOVSetTest()
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);

            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                UnitType = UnitType.Length,
                UnitID = Unit.Inch,
                IsFormula = false,
                DefaultValue = "4"
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                UnitType = UnitType.Length,
                UnitID = Unit.Inch,
                IsFormula = false,
                DefaultValue = "6"
            };

            AssemblyVariable childAssemblyArea = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Area",
                Label = "Area",
                DataType = DataType.Number,
                DefaultValue = "=Height.Value *Width.Value",
                ElementType = AssemblyElementType.Number,
                UnitType = UnitType.Length,
                UnitID = Unit.SquareInch,
                IsFormula = true,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyArea,
                    childAssemblyPrice,
                }
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
                InclusionFormula = "IncludeChild.Value"
            };

            AssemblyVariable parentAssemblyChildArea = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "ChildArea",
                Label = "ChildArea",
                DataType = DataType.String,
                DefaultValue = $"={childName}.Area.Value",
                IsFormula = true,
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
            };

            AssemblyVariable parentAssemblyIncludeChild = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "IncludeChild",
                Label = "IncludeChild",
                DataType = DataType.Boolean,
                DefaultValue = "True",
                ElementType = AssemblyElementType.Checkbox,
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyChildAssembly,
                    parentAssemblyIncludeChild,
                    parentAssemblyPrice,
                    parentAssemblyChildArea
                }
            };


            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator parentGen = CBELAssemblyGenerator.Create(ctx, parentAssembly);
            CBELAssemblyGenerator childGen = CBELAssemblyGenerator.Create(ctx, childAssembly);

            AssemblyCompilationResponse saveResp = await parentGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            saveResp = await childGen.Save(cache);
            Assert.IsTrue(saveResp.Success);

            var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, parentID, ClassType.Assembly.ID(), 1, cache, logger, BID);

            Assert.AreEqual(0, compilationResponse.ErrorCount);
            Assert.IsNotNull(compilationResponse.Assembly);

            ICBELAssembly cbelParentAssembly = compilationResponse.Assembly;

            ICBELComputeResult result = cbelParentAssembly.Compute();

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            IVariableData varChild = result.VariableData.FirstOrDefault(x => x.VariableName == childName);
            Assert.IsNotNull(varChild);
            Assert.IsTrue(varChild.IsIncluded.Value);
            Assert.IsFalse(varChild.IsIncludedOV.GetValueOrDefault(false));

            IVariableData varChildArea = result.VariableData.FirstOrDefault(x => x.VariableName == "ChildArea");
            Assert.IsNotNull(varChildArea);
            Assert.AreEqual("24", varChildArea.Value);


            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1,
                QuantityOV = true,

                VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "IncludeChild",
                                Value = "false",
                                IsOV = true,
                            }
                        },
            };

            result = cbelParentAssembly.Compute(ovValues);

            varChild = result.VariableData.FirstOrDefault(x => x.VariableName == childName);
            Assert.IsNotNull(varChild);
            Assert.IsFalse(varChild.IsIncluded.Value);
            Assert.IsFalse(varChild.IsIncludedOV.GetValueOrDefault(false));

            varChildArea = result.VariableData.FirstOrDefault(x => x.VariableName == "ChildArea");
            Assert.IsNotNull(varChildArea);
            Assert.IsNull(varChildArea.Value);


            ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1,
                QuantityOV = true,

                VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "IncludeChild",
                                Value = "true",
                                IsOV = true,
                            }
                        },
            };

            result = cbelParentAssembly.Compute(ovValues);

            varChild = result.VariableData.FirstOrDefault(x => x.VariableName == childName);
            Assert.IsNotNull(varChild);
            Assert.IsTrue(varChild.IsIncluded.Value);
            Assert.IsFalse(varChild.IsIncludedOV.GetValueOrDefault(false));

            varChildArea = result.VariableData.FirstOrDefault(x => x.VariableName == "ChildArea");
            Assert.IsNotNull(varChildArea);
            Assert.AreEqual("24", varChildArea.Value);

            ovValues = new CBELOverriddenValues()
            {
                ComponentID = parentID,
                Quantity = 1,
                QuantityOV = true,

                OVChildValues = new List<ICBELOverriddenValues>
                {
                    new CBELOverriddenValues()
                    {
                        VariableName = childName,
                        IsIncluded = false,
                        IsIncludedOV = true,
                    }
                }
            };
            result = cbelParentAssembly.Compute(ovValues);

            varChild = result.VariableData.FirstOrDefault(x => x.VariableName == childName);
            Assert.IsNotNull(varChild);
            Assert.IsFalse(varChild.IsIncluded.Value);
            Assert.IsTrue(varChild.IsIncludedOV.GetValueOrDefault(false));

            varChildArea = result.VariableData.FirstOrDefault(x => x.VariableName == "ChildArea");
            Assert.IsNotNull(varChildArea);
            Assert.IsNull(varChildArea.Value);
        }

        [TestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public async Task LinkedAssemblyCompileAndComputeRolledUpTest(bool rollUp)
        {
            int parentID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int childID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, 1m, 2m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, 1m, 3m);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalCost * 1.5",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable childAssemblyMaterial = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "LinkedMaterial",
                Label = "LinkedMaterial",
                DataType = DataType.String,
                DefaultValue = material1.Name,
                LinkedMaterialID = material1.ID,
                ElementType = AssemblyElementType.LinkedMaterial,
                ConsumptionDefaultValue = "1",
            };

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = BID,
                ID = childID,
                Name = childName,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyPrice,
                    childAssemblyMaterial,
                }
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
                RollupLinkedPriceAndCost = rollUp,
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = PricingTestHelper.NextAssemblyVariableFormulaID(ctx, BID),
                BID = BID,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
            };

            AssemblyVariable parentAssemblyMaterial = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "LinkedMaterial",
                Label = "LinkedMaterial",
                DataType = DataType.String,
                DefaultValue = material2.Name,
                LinkedMaterialID = material2.ID,
                ElementType = AssemblyElementType.LinkedMaterial,
                ConsumptionDefaultValue = "1"
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalCost * 1.2",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = parentID,
                Name = parentName,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyChildAssembly,
                    parentAssemblyMaterial,
                    parentAssemblyPrice,
                }
            };


            ctx.AssemblyData.Add(childAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            ctx.SaveChanges();

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = 1,
                Components = new List<ComponentPriceRequest>()
                {
                    new ComponentPriceRequest()
                    {
                        ComponentID = parentID,
                        ComponentType = OrderItemComponentType.Assembly,
                    }
                }
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest);

            decimal? itemCostTotal;
            decimal? parentAssemblyCostTotal;
            decimal? childAssemblyCostTotal;

            decimal? itemPriceTotal;
            decimal? parentAssemblyPriceTotal;
            decimal? childAssemblyPriceTotal;

            if (rollUp)
            {
                itemCostTotal = 5m;
                parentAssemblyCostTotal = 5m;
                childAssemblyCostTotal = 2m;

                itemPriceTotal = 6m;
                parentAssemblyPriceTotal = 6m;
                childAssemblyPriceTotal = null;
            }
            else
            {
                itemCostTotal = 5m;
                parentAssemblyCostTotal = 3m;
                childAssemblyCostTotal = 2m;

                itemPriceTotal = 6.6m;
                parentAssemblyPriceTotal = 3.6m;
                childAssemblyPriceTotal = 3m;
            }

            Assert.IsNotNull(result);

            string resultJSON = JsonConvert.SerializeObject(result);

            Assert.AreEqual(itemPriceTotal, result.PricePreTax);
            Assert.AreEqual(itemCostTotal, result.CostTotal);

            ComponentPriceResult parentAssemblyResult = result.Components.FirstOrDefault(x => x.ComponentType == OrderItemComponentType.Assembly);

            Assert.IsNotNull(parentAssemblyResult);

            Assert.AreEqual(parentAssemblyPriceTotal, parentAssemblyResult.PricePreTax);
            Assert.AreEqual(parentAssemblyCostTotal, parentAssemblyResult.CostNet);

            ComponentPriceResult childAssemblyResult = parentAssemblyResult.ChildComponents.FirstOrDefault(x => x.ComponentType == OrderItemComponentType.Assembly);

            Assert.IsNotNull(childAssemblyResult);

            Assert.AreEqual(childAssemblyPriceTotal, childAssemblyResult.PricePreTax);
            Assert.AreEqual(childAssemblyCostTotal, childAssemblyResult.CostNet);
        }
    }
}
