﻿using Endor.CBEL.Assembly.Components;
using Endor.CBEL.Common;
using Endor.CBEL.Compute;
using Endor.CBEL.Elements;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Grammar;
using Endor.CBEL.Interfaces;
using Endor.CBEL.Metadata;
using Endor.CBEL.ObjectGeneration;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Irony.Parsing;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Sdk;
using static Endor.Level3.Tests.Common.TestHelper;
using static Endor.Pricing.Tests.Helper.PricingTestHelper;
using System.Globalization;
using System.Reflection;
using Endor.Units;
using Endor.Logging.Client;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class CbelTests
    {
        private Parser TestParser => new Parser(new CBELGrammar());

        #region CBEL Tests

        [TestMethod]
        public async Task TestCreateCBELAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);
        }


        [TestMethod]
        public async Task InternalGetAssemblyScriptMemoryReductionTest()
        {
            var generator = GetCBELTestAssemblyGeneratorWithDefaultElements();
            generator.BID = 1;
            string AssemblyClassName = generator.AssemblyClassName;
            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < 10; i++)
            {
                if (i == 1)
                    sw.Start();
                await CompileStuff(generator, AssemblyClassName);
            }
            sw.Stop();
            Console.WriteLine(sw.Elapsed.TotalSeconds / 10.0);
            GC.Collect();

            sw.Reset();
            for (int i = 0; i < 10; i++)
            {
                if (i == 1)
                    sw.Start();
                //string resultAssemblyCode = generator.GetAssemblyCode();
                //string assemblyCode = $"{resultAssemblyCode}{Endor.CBEL.Common.Constants.CR}return typeof({AssemblyClassName});";

                //Script<Type> script = CSharpScript.Create<Type>(assemblyCode, CBELBaseAssemblyGenerator.scriptOptions);

                //ImmutableArray<Diagnostic> compileResults = script.Compile();
                //var resultScript = script;
                //Type resultAssemblyType;

                //resultAssemblyType = (await script.RunAsync()).ReturnValue;
                await CompileStuff(generator, AssemblyClassName);
                GC.Collect();

            }
            sw.Stop();
            Console.WriteLine(sw.Elapsed.TotalSeconds / 10.0);
        }

        private static async Task CompileStuff(CBELAssemblyGenerator generator, string AssemblyClassName)
        {
            string resultAssemblyCode = generator.GetAssemblyCode();
            string assemblyCode = $"{resultAssemblyCode}{Endor.CBEL.Common.Constants.CR}return typeof({AssemblyClassName});";

            Script<Type> script = CSharpScript.Create<Type>(assemblyCode, CBELBaseAssemblyGenerator.scriptOptions);

            ImmutableArray<Diagnostic> compileResults = script.Compile();
            var resultScript = script;
            _ = (await script.RunAsync()).ReturnValue;
        }

        [TestMethod]
        public void TestCBELMetadata()
        {
            AssemblyData assemblyData = new AssemblyData
            {
                ID = 1065,
                ClassTypeID = 12040,
                ModifiedDT = DateTime.Now,
                IsActive = true,
                Name = "Blue",
                Description = null,
                HasImage = false,
                PricingType = 0,
                TaxabilityCodeID = 0,
                IncomeAllocationType = 0,
                IncomeAccountID = 4000,
                HasTierTable = false,
                PriceFormulaType = PriceFormulaType.Fixed,
                SimpleAssemblyCategories = null,
                Variables = new List<AssemblyVariable>
                {
                    #region Variables
                    new AssemblyVariable
                    {
                        ID = 1143,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "ABCDEF",
                        DefaultValue = "ABCDEF",
                        DataType = DataType.LaborPart,
                        IsFormula = false,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = false,
                        AllowMultiSelect = false,
                        GroupOptionsByCategory = null,
                        Label = "ABCDEF",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 1,
                        Tooltip = null,
                        AllowDecimals = null,
                        DisplayType = null,
                        DecimalPlaces = null,
                        SystemVariableID = null,
                        IsRequired = false,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.LinkedLabor,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = 1020,
                        LinkedMachineID = null,
                        LinkedAssemblyID = null,
                        ConsumptionDefaultValue = "=AssemblyQuantity.Value",
                        IsConsumptionFormula = true,
                        Formulas = null
                    },
                    new AssemblyVariable
                    {
                        ID = 1144,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "TotalQuantity",
                        DefaultValue = "1",
                        DataType = DataType.Number,
                        IsFormula = false,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = null,
                        AllowMultiSelect = null,
                        GroupOptionsByCategory = null,
                        Label = "Quantity1",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 0,
                        Tooltip = null,
                        AllowDecimals = false,
                        DisplayType = null,
                        DecimalPlaces = 0,
                        SystemVariableID = null,
                        IsRequired = true,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.Number,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = null,
                        ConsumptionDefaultValue = null,
                        IsConsumptionFormula = false,
                        Formulas = null
                    },
                    new AssemblyVariable
                    {
                        ID = 1145,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "Tier",
                        DefaultValue = "=CompanyTier();",
                        DataType = DataType.String,
                        IsFormula = true,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = null,
                        AllowMultiSelect = null,
                        GroupOptionsByCategory = null,
                        Label = "Tier",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 0,
                        Tooltip = null,
                        AllowDecimals = null,
                        DisplayType = null,
                        DecimalPlaces = null,
                        SystemVariableID = 2,
                        IsRequired = true,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.SingleLineLabel,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = null,
                        ConsumptionDefaultValue = null,
                        IsConsumptionFormula = false,
                        Formulas = null
                    },
                    new AssemblyVariable
                    {
                        ID = 1146,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "Price",
                        DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                        DataType = DataType.Number,
                        IsFormula = true,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = null,
                        AllowMultiSelect = null,
                        GroupOptionsByCategory = null,
                        Label = "Total Retail Price",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 0,
                        Tooltip = null,
                        AllowDecimals = true,
                        DisplayType = CustomFieldNumberDisplayType.Accounting,
                        DecimalPlaces = 2,
                        SystemVariableID = null,
                        IsRequired = false,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.Number,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = null,
                        ConsumptionDefaultValue = null,
                        IsConsumptionFormula = false,
                        Formulas = null
                    },
                    new AssemblyVariable
                    {
                        ID = 1147,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "Group1",
                        DefaultValue = null,
                        DataType = DataType.None,
                        IsFormula = false,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = false,
                        AllowMultiSelect = false,
                        GroupOptionsByCategory = null,
                        Label = "Group 1",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 1,
                        Tooltip = null,
                        AllowDecimals = null,
                        DisplayType = null,
                        DecimalPlaces = null,
                        SystemVariableID = null,
                        IsRequired = false,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.Group,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = null,
                        ConsumptionDefaultValue = null,
                        IsConsumptionFormula = false,
                        Formulas = null
                    },
                    new AssemblyVariable
                    {
                        ID = 1148,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "Number01",
                        DefaultValue = "2",
                        DataType = DataType.None,
                        IsFormula = false,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = false,
                        AllowMultiSelect = false,
                        GroupOptionsByCategory = null,
                        Label = "Number 01",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 2,
                        Tooltip = null,
                        AllowDecimals = null,
                        DisplayType = null,
                        DecimalPlaces = null,
                        SystemVariableID = null,
                        IsRequired = false,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.Number,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = null,
                        ConsumptionDefaultValue = null,
                        IsConsumptionFormula = false,
                        Formulas = null
                    },
                    new AssemblyVariable
                    {
                        ID = 1149,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "Measurement01",
                        DefaultValue = "23",
                        DataType = DataType.Number,
                        IsFormula = false,
                        UnitID = Unit.Inch,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = false,
                        AllowMultiSelect = false,
                        GroupOptionsByCategory = null,
                        Label = "Measurement 01",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 1,
                        Tooltip = null,
                        AllowDecimals = null,
                        DisplayType = null,
                        DecimalPlaces = null,
                        SystemVariableID = null,
                        IsRequired = false,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.Number,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = null,
                        ConsumptionDefaultValue = null,
                        IsConsumptionFormula = false,
                        Formulas = null
                    },
                    new AssemblyVariable
                    {
                        ID = 1150,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "Group02",
                        DefaultValue = null,
                        DataType = DataType.None,
                        IsFormula = false,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = false,
                        AllowMultiSelect = false,
                        GroupOptionsByCategory = null,
                        Label = "Group 02",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 1,
                        Tooltip = null,
                        AllowDecimals = null,
                        DisplayType = null,
                        DecimalPlaces = null,
                        SystemVariableID = null,
                        IsRequired = false,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.Group,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = null,
                        ConsumptionDefaultValue = null,
                        IsConsumptionFormula = false,
                        Formulas = null
                    },
                    new AssemblyVariable
                    {
                        ID = 1155,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "AssemblyLink",
                        DefaultValue = "Assembly Link",
                        DataType = DataType.Assembly,
                        IsFormula = false,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = false,
                        AllowMultiSelect = false,
                        GroupOptionsByCategory = null,
                        Label = "Assembly Link",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 1,
                        Tooltip = null,
                        AllowDecimals = null,
                        DisplayType = null,
                        DecimalPlaces = null,
                        SystemVariableID = null,
                        IsRequired = false,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.LinkedAssembly,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = 1066,
                        ConsumptionDefaultValue = "=AssemblyQuantity.Value",
                        IsConsumptionFormula = true,
                        Formulas = new List<AssemblyVariableFormula>
                        {
                            new AssemblyVariableFormula
                            {
                                ID = 1002,
                                ClassTypeID = 12052,
                                ModifiedDT = DateTime.Now,
                                VariableID = 1155,
                                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                                FormulaText = "TotalQuantity",
                                DataType = DataType.Number,
                                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                                IsFormula = false,
                                ChildVariableName = "TotalQuantity"
                            }
                        }
                    },
                    new AssemblyVariable
                    {
                        ID = 1157,
                        ClassTypeID = 12046,
                        ModifiedDT = DateTime.Now,
                        Name = "Number02",
                        DefaultValue = "5",
                        DataType = DataType.Number,
                        IsFormula = false,
                        UnitID = null,
                        ListDataType = null,
                        ListValuesJSON = null,
                        AllowCustomValue = false,
                        AllowMultiSelect = false,
                        GroupOptionsByCategory = null,
                        Label = "Number 02",
                        LabelType = null,
                        AltText = null,
                        ElementUseCount = 1,
                        Elements = null,
                        Tooltip = null,
                        AllowDecimals = null,
                        DisplayType = null,
                        DecimalPlaces = null,
                        SystemVariableID = null,
                        IsRequired = false,
                        IsDisabled = false,
                        ElementType = AssemblyElementType.Number,
                        AssemblyID = 1065,
                        TempID = null,
                        LinkedMaterialID = null,
                        LinkedLaborID = null,
                        LinkedMachineID = null,
                        LinkedAssemblyID = 1066,
                        ConsumptionDefaultValue = null,
                        IsConsumptionFormula = false,
                        Formulas = null
                    }
#endregion
                },
                AssemblyType = AssemblyType.Product
            };

            SortedDictionary<string, ICBELMember> memberList = MetadataHelper.KnownMemberList(assemblyData);
            Assert.IsNotNull(memberList);
            Assert.IsTrue(memberList.Any());
        }

        [TestMethod]
        public async Task TestCreateCBELAssemblyDLL()
        {
            //This test save the assembly to the file system and reloads it
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();
            short testBID = 1;
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            await gen.Save(cache);
            (ICBELAssembly assembly, string assemblyCode) = await CBELAssemblyHelper.LoadAssemblyFromStorage(gen.ID, gen.ClassTypeID, gen.Version, cache, logger, GetMockCtx(testBID), testBID);
            Assert.IsNotNull(assembly);
            Assert.IsTrue(assembly.Variables.Count > 0);

        }

        [TestMethod]
        public async Task TestLoadCBELAssemblyDLL_NotFound()
        {
            short testBID = 1;
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            (ICBELAssembly assembly, string assemblyCode) = await CBELAssemblyHelper.LoadAssemblyFromStorage(-99, ClassType.Assembly.ID(), 999, cache, logger, GetMockCtx(testBID), testBID);
            Assert.IsNull(assembly);

        }

        [TestMethod]
        public async Task TestCBELAssemblyCompile()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements(BID: bid, ID: 100, Version: 2);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);

            Assert.AreEqual("Assembly_BID1_CT12040_ID100_V2", assembly.GetType().Name);
        }

        [TestMethod]
        public async Task TestCBELMachineProfile()
        {
            short bid = 1;
            CBELMachineGenerator gen = GetCBELTestMachineGeneratorWithDefaultElements(BID: bid, ID: 100, Version: 2);

            gen.AddProfile("Profile1", MachineLayoutType.Other);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);

            Assert.AreEqual("Machine_BID1_CT12030_ID100_V2", assembly.GetType().Name);

            Assert.IsInstanceOfType(assembly, typeof(CBELMachineBase));
            Assert.AreEqual(1, ((CBELMachineBase)assembly).ProfileList.Count);
            Assert.AreEqual("Profile1", ((CBELMachineBase)assembly).ProfileList[0]);
        }

        [TestMethod]
        public async Task TestCBELMultiLineAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator Enscription = new CBELElementGenerator(gen)
            {
                Name = "Enscription",
                VariableClassName = "MultiLineTextElement",
                FormulaText = "The quick brown fox jumps over the lazy dog \\n Near the river.",
                Label = "Description",
                VariableDataType = DataType.String,
                DefaultValueConstant = "The quick brown fox jumps over the lazy dog \\n Near the river.",
            };
            gen.AddVariable(Enscription);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["Enscription"];
            Assert.IsInstanceOfType(assemblyElement, typeof(MultiLineTextElement));
            Assert.AreEqual("The quick brown fox jumps over the lazy dog \n Near the river.", ((ICBELVariableComputed<string>)assemblyElement).Value);
            //Overridable
            ((MultiLineTextElement)assemblyElement).Value = "test assign";
            Assert.AreEqual("test assign", ((MultiLineTextElement)assemblyElement).Value);
        }

        [TestMethod]
        public async Task TestCBELURLLabelAssembly()
        {
            const string BasicURLLabelVarName = "BasicURLLabel";
            const string ComputedURLLabelVarName = "ComputedURLLabel";

            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator basicURLLabel = new CBELElementGenerator(gen)
            {
                Name = BasicURLLabelVarName,
                VariableClassName = "URLLabelElement",
                Label = "My URL label",
                VariableDataType = DataType.String,
                DefaultValueConstant = BasicURLLabelVarName,
                AltConstant = BasicURLLabelVarName
            };
            gen.AddVariable(basicURLLabel);
            CBELElementGenerator computedURLLabel = new CBELElementGenerator(gen)
            {
                Name = ComputedURLLabelVarName,
                VariableClassName = "URLLabelElement",
                Label = "Computed URL label",
                VariableDataType = DataType.String,
                DefaultValueFunction = "\"test\"",
                AltFunction = "\"test\""
            };
            gen.AddVariable(computedURLLabel);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable basicCBELVar = assembly.Variables[BasicURLLabelVarName];
            Assert.IsInstanceOfType(basicCBELVar, typeof(URLLabelElement));
            Assert.AreEqual(basicURLLabel.Label, basicCBELVar.Label);

            //Overridable
            ((URLLabelElement)basicCBELVar).Value = "test assign";
            Assert.AreEqual("test assign", ((URLLabelElement)basicCBELVar).Value);

            ICBELVariable computedCBELVar = assembly.Variables[ComputedURLLabelVarName];
            Assert.IsInstanceOfType(computedCBELVar, typeof(URLLabelElement));
            Assert.AreEqual(computedURLLabel.Label, computedCBELVar.Label);
            Assert.AreEqual("test", (computedCBELVar as URLLabelElement).Value);
            Assert.AreEqual("test", (computedCBELVar as URLLabelElement).AltAsString);
        }

        [TestMethod]
        public async Task TestCBELSingleLineLabelAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator mySingleLineLabel = new CBELElementGenerator(gen)
            {
                Name = "SingleLineLabel1",
                VariableClassName = "SingleLineLabelElement",
                Label = "My single line label",
                VariableDataType = DataType.String,
            };
            gen.AddVariable(mySingleLineLabel);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["SingleLineLabel1"];
            Assert.IsInstanceOfType(assemblyElement, typeof(SingleLineLabelElement));
            Assert.AreEqual("My single line label", assemblyElement.Label);
            //Not Overridable
            ((SingleLineLabelElement)assemblyElement).Value = "test assign";
            Assert.AreEqual(null, ((SingleLineLabelElement)assemblyElement).Value);
        }

        [TestMethod]
        public async Task TestCBELMultiLineLabelAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator myMultiLineLabel = new CBELElementGenerator(gen)
            {
                Name = "MultiLineLabel1",
                VariableClassName = "MultiLineLabelElement",
                Label = "My multi line \n label",
                VariableDataType = DataType.String,
            };
            gen.AddVariable(myMultiLineLabel);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["MultiLineLabel1"];
            Assert.IsInstanceOfType(assemblyElement, typeof(MultiLineLabelElement));
            Assert.AreEqual("My multi line \n label", assemblyElement.Label);
            //Not Overridable
            ((MultiLineLabelElement)assemblyElement).Value = "test assign";
            Assert.AreEqual(null, ((MultiLineLabelElement)assemblyElement).Value);
        }

        [TestMethod]
        public async Task TestCBELMeasurementAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator myMeasurement = new CBELElementGenerator(gen)
            {
                Name = "Measurement",
                VariableClassName = nameof(NumberEditElement),
                Label = "12345",
                VariableDataType = DataType.Number,
            };
            gen.AddVariable(myMeasurement);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);
            Assert.IsNotNull(assembly.Variables);
            Assert.IsTrue(assembly.Variables.ContainsKey(myMeasurement.Name));
            ICBELVariable assemblyElement = assembly.Variables[myMeasurement.Name];
            Assert.IsInstanceOfType(assemblyElement, typeof(NumberEditElement));
            Assert.AreEqual(myMeasurement.Label, assemblyElement.Label);
        }

        [TestMethod]
        public async Task TestCBELShowHideGroupAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator myShowHideGroup = new CBELElementGenerator(gen)
            {
                Name = "ShowHideGroup",
                VariableClassName = "ShowHideGroupElement",
                Label = "Show Group",
                VariableDataType = DataType.Boolean
            };
            gen.AddVariable(myShowHideGroup);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            var assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["ShowHideGroup"];
            Assert.IsInstanceOfType(assemblyElement, typeof(ShowHideGroupElement));
            Assert.AreEqual("Show Group", assemblyElement.Label);
        }

        [TestMethod]
        public async Task TestCBELDropdownStringListAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator myDropdown = new CBELElementGenerator(gen)
            {
                Name = "DropDown",
                VariableClassName = "DropDownStringListElement",
                Label = "Dropdown Text",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myDropdown);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["DropDown"];
            Assert.IsInstanceOfType(assemblyElement, typeof(DropDownStringListElement));
            Assert.AreEqual("Dropdown Text", assemblyElement.Label);
        }

        [TestMethod]
        public async Task TestCBELDropdownLaborListAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator myDropdown = new CBELElementGenerator(gen)
            {
                Name = "DropDown",
                VariableClassName = "DropDownLaborListElement",
                Label = "Dropdown Text",
                VariableDataType = DataType.String,
                ConsumptionFormulaText = "Width.Value + Height.Value",
                DefaultConsumptionValueFunction = "Width.Value + Height.Value"
            };
            gen.AddVariable(myDropdown);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["DropDown"];
            Assert.IsInstanceOfType(assemblyElement, typeof(DropDownLaborListElement));
            DropDownLaborListElement dropdownElement = (DropDownLaborListElement)assemblyElement;
            Assert.AreEqual("Dropdown Text", dropdownElement.Label);
            Assert.AreEqual(42m, dropdownElement.ConsumptionQuantity.Value);

            dropdownElement.ConsumptionQuantity.Value = 10m;
            Assert.AreEqual(10m, dropdownElement.ConsumptionQuantity.Value);
            Assert.IsTrue(dropdownElement.ConsumptionQuantity.IsOV);
        }

        [TestMethod]
        public async Task TestCBELDropdownMaterialListAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator myDropdown = new CBELElementGenerator(gen)
            {
                Name = "DropDown",
                VariableClassName = "DropDownMaterialListElement",
                Label = "Dropdown Text",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myDropdown);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["DropDown"];
            Assert.IsInstanceOfType(assemblyElement, typeof(DropDownMaterialListElement));
            Assert.AreEqual("Dropdown Text", assemblyElement.Label);
        }

        [TestMethod]
        public async Task TestCBELDropdownNumberListAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator myDropdown = new CBELElementGenerator(gen)
            {
                Name = "DropDown",
                VariableClassName = "DropDownNumberListElement",
                Label = "Dropdown Text",
                VariableDataType = DataType.Number
            };
            gen.AddVariable(myDropdown);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["DropDown"];
            Assert.IsInstanceOfType(assemblyElement, typeof(DropDownNumberListElement));
            Assert.AreEqual("Dropdown Text", assemblyElement.Label);
        }

        [TestMethod]
        public async Task TestCBELLinkedAssemblyAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LinkedLabor",
                VariableClassName = "LinkedLaborVariable",
                Label = "LaborID",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedAssembly);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["LinkedLabor"];
            Assert.IsInstanceOfType(assemblyElement, typeof(LinkedLaborVariable));
            Assert.AreEqual("LaborID", assemblyElement.Label);
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationItemsVertical()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables[myLinkedAssembly.Name];
            Assert.IsInstanceOfType(assemblyElement, typeof(LayoutComputationAssemblyVariable));

            LayoutComputationAssemblyVariable variable = assemblyElement as LayoutComputationAssemblyVariable;
            variable.LoadLinkedComponent();
            Assert.IsInstanceOfType(variable.Component, typeof(LayoutComputingAssembly));
            LayoutComputingAssembly layout = variable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = assembly.ID,
                Quantity = 1m,
                QuantityOV = true,
                CompanyID = assembly.CompanyID,
                ParentQuantity = assembly.ParentQuantity,
                IsVended = true,
                VariableName = myLinkedAssembly.Name,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "LayoutType",
                        IsOV = true,
                        Value = "Sheet",
                    },
                    new VariableData()
                    {
                        VariableName = "MaterialHeight",
                        IsOV = true,
                        Value = "100",
                    },
                    new VariableData()
                    {
                        VariableName = "ItemHeight",
                        IsOV = true,
                        Value = "10",
                    },
                    new VariableData()
                    {
                        VariableName = "IncludeColorBar",
                        IsOV = true,
                        Value = "true",
                    },
                },
            };

            layout.Compute(ovValues);
            Assert.AreEqual(10m, layout.ItemsVertical.Value);

            ovValues.VariableData.Add(
                new VariableData()
                {
                    VariableName = "BleedTop",
                    IsOV = true,
                    Value = "10",
                }
            );

            // Bt=10 I=10                  x x
            // Os1Ih1Bt1Ih2Bt2Ih3Bt3Ih4Bt4Ih5Bt5
            // 10+10+10+10+10+10+10+10+10+10+10 = 110            
            layout.Compute(ovValues);
            Assert.AreEqual(4m, layout.ItemsVertical.Value);

            ovValues.VariableData.Add(
                new VariableData()
                {
                    VariableName = "BleedBottom",
                    IsOV = true,
                    Value = "10",
                }
            );

            // Each Item is now 30 tall
            layout.Compute(ovValues);
            Assert.AreEqual(3m, layout.ItemsVertical.Value);

            ovValues.VariableData.Add(
                new VariableData()
                {
                    VariableName = "GutterVertical",
                    IsOV = true,
                    Value = "10",
                }
            );

            // Each Item is now 20 tall and the gutter isn't any bigger than the margin so nothing changes
            layout.Compute(ovValues);
            Assert.AreEqual(3m, layout.ItemsVertical.Value);

            ovValues.VariableData.FirstOrDefault(t => t.VariableName == "GutterVertical").Value = "30";
            // Each Item is 10 tall and the bleeds will only add 20 which is less than the gutter which is 30 
            // starting at top edge
            // 30 offset (gutter)= 30
            // 10 item1 height   = 40 
            // 30 gutter1        = 70
            // 10 item2 height   = 80            
            // another 30 gutter = 110,
            // which doesn't leave enough space for item 2 and the item2 gutter

            layout.Compute(ovValues);
            Assert.AreEqual(1m, layout.ItemsVertical.Value);

            ovValues.VariableData.FirstOrDefault(t => t.VariableName == "GutterVertical").Value = "20";
            ovValues.VariableData.Add(
               new VariableData()
               {
                   VariableName = "LeaderTop",
                   IsOV = true,
                   Value = "30",
               }
            );

            //                       x x  x
            //(LT+B1)+I1+B1+B2+I2+B2+B3+I3+B3
            //50+    10+10+10+10+10+10+10+10 = 130
            // Each Item is 20 tall and the the leader isn't big enough to bump the 2nd image off the page
            layout.Compute(ovValues);
            Assert.AreEqual(1m, layout.ItemsVertical.Value);

            //50+10+30+10 = 100;
            ovValues.VariableData.FirstOrDefault(t => t.VariableName == "LeaderTop").Value = "50";

            layout.Compute(ovValues);
            Assert.AreEqual(0m, layout.ItemsVertical.Value);

            ovValues.VariableData.FirstOrDefault(t => t.VariableName == "LeaderTop").Value = "20";

            ovValues.VariableData.Add(
               new VariableData()
               {
                   VariableName = "MaterialMarginTop",
                   IsOV = true,
                   Value = "10",
               }
            );

            layout.Compute(ovValues);
            Assert.AreEqual(1m, layout.ItemsVertical.Value);

            ovValues.VariableData.RemoveAt(ovValues.VariableData.Count - 1);

            ovValues.VariableData.Add(
               new VariableData()
               {
                   VariableName = "MaterialMarginTop",
                   IsOV = true,
                   Value = "10",
               }
            );

            layout.Compute(ovValues);
            Assert.AreEqual(1m, layout.ItemsVertical.Value);


            ovValues.VariableData.Add(
               new VariableData()
               {
                   VariableName = "MaterialMarginTop",
                   IsOV = true,
                   Value = "10",
               }
            );

            layout.Compute(ovValues);
            Assert.AreEqual(1m, layout.ItemsVertical.Value);

            /*  ItemHeight           x
            *  MaterialHeight       x
            *  ColorBarPosition
            *  ColorBarThickness;   
            *  GutterVertical       x
            *  LeaderTop            x
            *  MaterialMarginTop    x
            *  BleedTop             x
            *  BleedBottom          x
            *  */
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationItemsHorizontal()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables[myLinkedAssembly.Name];
            Assert.IsInstanceOfType(assemblyElement, typeof(LayoutComputationAssemblyVariable));

            LayoutComputationAssemblyVariable variable = assemblyElement as LayoutComputationAssemblyVariable;
            variable.LoadLinkedComponent();
            Assert.IsInstanceOfType(variable.Component, typeof(LayoutComputingAssembly));
            LayoutComputingAssembly layout = variable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = assembly.ID,
                Quantity = 1m,
                QuantityOV = true,
                CompanyID = assembly.CompanyID,
                ParentQuantity = assembly.ParentQuantity,
                IsVended = true,
                VariableName = myLinkedAssembly.Name,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "LayoutType",
                        IsOV = true,
                        Value = "Sheet",
                    },
                    new VariableData()
                    {
                        VariableName = "MaterialWidth",
                        IsOV = true,
                        Value = "100",
                    },
                    new VariableData()
                    {
                        VariableName = "ItemWidth",
                        IsOV = true,
                        Value = "10",
                    },
                    new VariableData()
                    {
                        VariableName = "IncludeColorBar",
                        IsOV = true,
                        Value = "true",
                    },
                    new VariableData()
                    {
                        VariableName = "ColorBarPosition",
                        IsOV = true,
                        Value = "Top",
                    }
                },
            };
            layout.Compute(ovValues);
            Assert.AreEqual(10m, layout.ItemsHorizontal.Value);
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationVisualizationDataPages()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String,
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            var layoutVariable = assembly.Variables.First(t => t.Key.Contains(myLinkedAssembly.Name)).Value as LayoutComputationAssemblyVariable;
            //layoutVariable.LoadLinkedComponent();
            //LayoutComputingAssembly layout = layoutVariable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                OVChildValues = new List<ICBELOverriddenValues>(){
                    new CBELOverriddenValues()
                    {
                        VariableName = myLinkedAssembly.Name,
                        ComponentID = 1,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "LayoutType",
                                IsOV = true,
                                Value = "Sheet",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialWidth",
                                IsOV = true,
                                Value = "5",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialHeight",
                                IsOV = true,
                                Value = "5",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemWidth",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemHeight",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "OrderedQuantity",
                                IsOV = true,
                                Value = "10",
                            },
                        }
                    }
                }
            };
            var result = assembly.Compute(ovValues);

            string visualizationJson = result.Results.First().VariableData.First(t => t.VariableName == nameof(LayoutComputingAssembly.VisualizationData)).Value;
            var visualizationModel = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationJson);

            Assert.AreEqual(3, visualizationModel.Pages.Count);
            Assert.AreEqual(3, visualizationModel.PageCount);
            Assert.AreEqual(1, visualizationModel.Sides);
            Assert.AreEqual("Sheet", visualizationModel.LayoutType);

            for (var i = 0; i < visualizationModel.Pages.Count; i++)
            {
                var page = visualizationModel.Pages[i];
                Assert.AreEqual(i + 1, page.PageNumber);
                Assert.AreEqual(1, page.Side);
                Assert.AreEqual((i * 4) + 1, page.FirstItemNumber);
                if (i != (visualizationModel.Pages.Count - 1))
                {
                    Assert.AreEqual(0, page.LayoutID);
                }
                else
                {
                    Assert.AreEqual(1, page.LayoutID);
                }
            }
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationVisualizationDataPages2Sided()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String,
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            var layoutVariable = assembly.Variables.First(t => t.Key.Contains(myLinkedAssembly.Name)).Value as LayoutComputationAssemblyVariable;
            //layoutVariable.LoadLinkedComponent();
            //LayoutComputingAssembly layout = layoutVariable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                OVChildValues = new List<ICBELOverriddenValues>(){
                    new CBELOverriddenValues()
                    {
                        VariableName = myLinkedAssembly.Name,
                        ComponentID = 1,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "Is2Sided",
                                IsOV = true,
                                Value = "true",
                            },
                            new VariableData()
                            {
                                VariableName = "LayoutType",
                                IsOV = true,
                                Value = "Sheet",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialWidth",
                                IsOV = true,
                                Value = "10",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialHeight",
                                IsOV = true,
                                Value = "10",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemWidth",
                                IsOV = true,
                                Value = "2.5",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemHeight",
                                IsOV = true,
                                Value = "2.2",
                            },
                            new VariableData()
                            {
                                VariableName = "Bleed",
                                IsOV = true,
                                Value = ".2",
                            },
                            new VariableData()
                            {
                                VariableName = "OrderedQuantity",
                                IsOV = true,
                                Value = "22",
                            },
                            new VariableData()
                            {
                                VariableName = "IncludeColorBar",
                                IsOV = true,
                                Value = "true",
                            },
                            new VariableData()
                            {
                                VariableName = "ColorBarPosition",
                                IsOV = true,
                                Value = "Left",
                            },
                        }
                    }
                }
            };
            var result = assembly.Compute(ovValues);

            string visualizationJson = result.Results.First().VariableData.First(t => t.VariableName == nameof(LayoutComputingAssembly.VisualizationData)).Value;
            var visualizationModel = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationJson);

            Assert.AreEqual(6, visualizationModel.Pages.Count);
            Assert.AreEqual(6, visualizationModel.PageCount);
            Assert.AreEqual(2, visualizationModel.Sides);
            Assert.AreEqual("Sheet", visualizationModel.LayoutType);
            for (var i = 0; i < visualizationModel.Pages.Count; i++)
            {
                var page = visualizationModel.Pages[i];
                Assert.AreEqual(i + 1, page.PageNumber);
                Assert.AreEqual((i % 2) + 1, page.Side);

                if (page.Side == 1)
                    Assert.IsTrue(visualizationModel.Layouts.FirstOrDefault(t => t.ID == page.LayoutID).Items.Where(t => !t.Placeholder).Select(t => t.Label).All(t => t.EndsWith("F")));
                else if (page.Side == 2)
                    Assert.IsTrue(visualizationModel.Layouts.FirstOrDefault(t => t.ID == page.LayoutID).Items.Where(t => !t.Placeholder).Select(t => t.Label).All(t => t.EndsWith("B")));

                Assert.AreEqual((((int)(i / 2)) * 9) + 1, page.FirstItemNumber);
                if (i < (visualizationModel.Pages.Count - 2))
                {
                    Assert.AreEqual((i % 2), page.LayoutID);
                }
                else
                {
                    Assert.AreEqual((i % 2) + 2, page.LayoutID);
                }
            }

            // Layout assertions
            var possibleItemLeftValues = new decimal[]{
                0.3m,
                3.2m,
                6.1m,
                7.2m,
                4.3m,
                1.4m,
            };
            var possibleTopValues = new decimal[]{
                0.2m,
                2.8m,
                5.4m,
            };

            Assert.AreEqual(4, visualizationModel.Layouts.Count);

            foreach (var layout in visualizationModel.Layouts.Select((value, i) => new { value, i }))
            {
                Assert.AreEqual(layout.i, layout.value.ID);
                Assert.AreNotEqual(layout.value.Bleed.Top, 0m);
                foreach (var item in layout.value.Items.Select((value, i) => new { value, i }))
                {
                    Assert.AreEqual(item.i, item.value.ItemIndex);
                    var side = visualizationModel.Pages.First(t => t.LayoutID == layout.value.ID).Side;
                    if (!item.value.Placeholder)
                        Assert.AreEqual("{ItemNumber}" + (side == 1 ? 'F' : 'B'), item.value.Label);
                    Assert.IsTrue(possibleItemLeftValues.Contains(item.value.Left), "Item left is: " + item.value.Left);
                    Assert.IsTrue(possibleTopValues.Contains(item.value.Top), "Item top is: " + item.value.Top);
                }
            }
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationVisualizationDataPagesRoll()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String,
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            var layoutVariable = assembly.Variables.First(t => t.Key.Contains(myLinkedAssembly.Name)).Value as LayoutComputationAssemblyVariable;
            //layoutVariable.LoadLinkedComponent();
            //LayoutComputingAssembly layout = layoutVariable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                OVChildValues = new List<ICBELOverriddenValues>(){
                    new CBELOverriddenValues()
                    {
                        VariableName = myLinkedAssembly.Name,
                        ComponentID = 1,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "LayoutType",
                                IsOV = true,
                                Value = "Roll",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialWidth",
                                IsOV = true,
                                Value = "5",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemWidth",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemHeight",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "OrderedQuantity",
                                IsOV = true,
                                Value = "10",
                            },
                        }
                    }
                }
            };
            var result = assembly.Compute(ovValues);

            string visualizationJson = result.Results.First().VariableData.First(t => t.VariableName == nameof(LayoutComputingAssembly.VisualizationData)).Value;
            var visualizationModel = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationJson);

            Assert.AreEqual(5, visualizationModel.Pages.Count);
            Assert.AreEqual(5, visualizationModel.PageCount);
            Assert.AreEqual(1, visualizationModel.Sides);
            Assert.AreEqual("Roll", visualizationModel.LayoutType);
            for (var i = 0; i < visualizationModel.Pages.Count; i++)
            {
                var page = visualizationModel.Pages[i];
                Assert.AreEqual(i + 1, page.PageNumber);
                Assert.AreEqual(1, page.Side);
                Assert.AreEqual((i * 2) + 1, page.FirstItemNumber);
                Assert.AreEqual(0, page.LayoutID);
            }
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationVisualizationDataPagesRoll2Sided()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String,
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            
            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            var layoutVariable = assembly.Variables.First(t => t.Key.Contains(myLinkedAssembly.Name)).Value as LayoutComputationAssemblyVariable;
            //layoutVariable.LoadLinkedComponent();
            //LayoutComputingAssembly layout = layoutVariable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                OVChildValues = new List<ICBELOverriddenValues>(){
                    new CBELOverriddenValues()
                    {
                        VariableName = myLinkedAssembly.Name,
                        ComponentID = 1,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "Is2Sided",
                                IsOV = true,
                                Value = "true",
                            },
                            new VariableData()
                            {
                                VariableName = "LayoutType",
                                IsOV = true,
                                Value = "Roll",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialWidth",
                                IsOV = true,
                                Value = "5",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialHeight",
                                IsOV = true,
                                Value = "5",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemWidth",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemHeight",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "OrderedQuantity",
                                IsOV = true,
                                Value = "10",
                            },
                        }
                    }
                }
            };
            var result = assembly.Compute(ovValues);

            string visualizationJson = result.Results.First().VariableData.First(t => t.VariableName == nameof(LayoutComputingAssembly.VisualizationData)).Value;
            var visualizationModel = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationJson);

            Assert.AreEqual(10, visualizationModel.Pages.Count);
            Assert.AreEqual(10, visualizationModel.PageCount);
            Assert.AreEqual(2, visualizationModel.Sides);
            Assert.AreEqual("Roll", visualizationModel.LayoutType);
            for (var i = 0; i < visualizationModel.Pages.Count; i++)
            {
                var page = visualizationModel.Pages[i];
                Assert.AreEqual(i + 1, page.PageNumber);
                Assert.AreEqual((i % 2) + 1, page.Side);

                if (page.Side == 1)
                    Assert.IsTrue(visualizationModel.Layouts.FirstOrDefault(t => t.ID == page.LayoutID).Items.Where(t => !t.Placeholder).Select(t => t.Label).All(t => t.EndsWith("F")));
                else if (page.Side == 2)
                    Assert.IsTrue(visualizationModel.Layouts.FirstOrDefault(t => t.ID == page.LayoutID).Items.Where(t => !t.Placeholder).Select(t => t.Label).All(t => t.EndsWith("B")));

                Assert.AreEqual((((int)(i / 2)) * 2) + 1, page.FirstItemNumber);
                Assert.AreEqual((i % 2), page.LayoutID);
            }
            //Assert.AreEqual(visualizationModel.Layouts[1].Items[1].Left, visualizationModel.Material.Size.Horizontal - visualizationModel.Layouts[1].ItemSize.Horizontal - visualizationModel.Layouts[0].Items[0].Left);
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationVisualizationLayout()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String,
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            var layoutVariable = assembly.Variables.First(t => t.Key.Contains(myLinkedAssembly.Name)).Value as LayoutComputationAssemblyVariable;
            //layoutVariable.LoadLinkedComponent();
            //LayoutComputingAssembly layout = layoutVariable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                OVChildValues = new List<ICBELOverriddenValues>(){
                    new CBELOverriddenValues()
                    {
                        VariableName = myLinkedAssembly.Name,
                        ComponentID = 1,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "LayoutType",
                                IsOV = true,
                                Value = "Sheet",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialWidth",
                                IsOV = true,
                                Value = "5",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialHeight",
                                IsOV = true,
                                Value = "5",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemWidth",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemHeight",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "OrderedQuantity",
                                IsOV = true,
                                Value = "10",
                            },
                            new VariableData()
                            {
                                VariableName = "GutterHorizontal",
                                IsOV = true,
                                Value = "0.1",
                            },
                            new VariableData()
                            {
                                VariableName = "GutterVertical",
                                IsOV = true,
                                Value = "0.1",
                            },

                        }
                    }
                }
            };
            var result = assembly.Compute(ovValues);

            string visualizationJson = result.Results.First().VariableData.First(t => t.VariableName == nameof(LayoutComputingAssembly.VisualizationData)).Value;
            var visualizationModel = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationJson);

            Assert.AreEqual(2, visualizationModel.Layouts.Count);
            Assert.AreEqual(2, visualizationModel.LayoutCount);
            Assert.AreEqual(4, visualizationModel.Layouts[0].ItemCount);
            Assert.AreEqual(2, visualizationModel.Layouts[1].ItemCount);
            Assert.AreEqual(2, visualizationModel.Layouts[1].ItemSize.Horizontal);
            Assert.AreEqual(2, visualizationModel.Layouts[1].ItemSize.Vertical);
            Assert.AreEqual(0, visualizationModel.Layouts[1].PrinterMarks.Count);
            Assert.AreEqual(0.1m, visualizationModel.Layouts[0].Items.First().Left);
            Assert.AreEqual(0.1m, visualizationModel.Layouts[0].Items.First().Top);
            Assert.AreEqual(2.2m, visualizationModel.Layouts[0].Items.Last().Top);
            Assert.AreEqual(2.2m, visualizationModel.Layouts[0].Items.Last().Top);

        }

        [TestMethod]
        public async Task TestCBELLayoutComputationVisualizationLayoutRoll()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String,
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            var layoutVariable = assembly.Variables.First(t => t.Key.Contains(myLinkedAssembly.Name)).Value as LayoutComputationAssemblyVariable;
            //layoutVariable.LoadLinkedComponent();
            //LayoutComputingAssembly layout = layoutVariable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                OVChildValues = new List<ICBELOverriddenValues>(){
                    new CBELOverriddenValues()
                    {
                        VariableName = myLinkedAssembly.Name,
                        ComponentID = 1,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "LayoutType",
                                IsOV = true,
                                Value = "Roll",
                            },
                            new VariableData()
                            {
                                VariableName = "MaterialWidth",
                                IsOV = true,
                                Value = "5",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemWidth",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemHeight",
                                IsOV = true,
                                Value = "2",
                            },
                            new VariableData()
                            {
                                VariableName = "OrderedQuantity",
                                IsOV = true,
                                Value = "10",
                            },
                            new VariableData()
                            {
                                VariableName = "GutterHorizontal",
                                IsOV = true,
                                Value = "0.1",
                            },
                            new VariableData()
                            {
                                VariableName = "GutterVertical",
                                IsOV = true,
                                Value = "0.1",
                            },

                        }
                    }
                }
            };
            var result = assembly.Compute(ovValues);

            string visualizationJson = result.Results.First().VariableData.First(t => t.VariableName == nameof(LayoutComputingAssembly.VisualizationData)).Value;
            var visualizationModel = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationJson);

            Assert.AreEqual(1, visualizationModel.Layouts.Count);
            Assert.AreEqual(1, visualizationModel.LayoutCount);
            Assert.AreEqual(2, visualizationModel.Layouts[0].ItemCount);
            Assert.AreEqual(2, visualizationModel.Layouts[0].ItemSize.Horizontal);
            Assert.AreEqual(2, visualizationModel.Layouts[0].ItemSize.Vertical);
            Assert.AreEqual(0, visualizationModel.Layouts[0].PrinterMarks.Count);
            Assert.AreEqual(0.1m, visualizationModel.Layouts[0].Items.First().Left);
            Assert.AreEqual(0.1m, visualizationModel.Layouts[0].Items.First().Top);
            Assert.AreEqual(2.2m, visualizationModel.Layouts[0].Items.Last().Left);
            Assert.AreEqual(0.1m, visualizationModel.Layouts[0].Items.Last().Top);

        }

        [TestMethod]
        public async Task TestCBELLayoutComputationVisualizationDataDefaultOptions()
        {
            short bid = 1;
            var ctx = GetMockCtx(bid);
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(ctx, cache, logger);
            // NOTE: Manually load layout for now, not sure why this is misbehaving right now
            var layoutVariable = assembly.Variables.First(t => t.Key.Contains(myLinkedAssembly.Name)).Value as LayoutComputationAssemblyVariable;

            var result = assembly.Compute(new CBELOverriddenValues()
            {
                OVChildValues = new List<ICBELOverriddenValues>(){
                    new CBELOverriddenValues()
                    {
                        VariableName = layoutVariable.Name,
                        ComponentID = 1,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "ItemHeight",
                                IsOV = true,
                                Value = "10",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemWidth",
                                IsOV = true,
                                Value = "5",
                            }
                        }
                    }
                }
            });
            var visualizationJson = result.Results.First().VariableData.First(t => t.VariableName == nameof(LayoutComputingAssembly.VisualizationData)).Value;
            var visualizationModel = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationJson);
            var optionCategory = ctx.SystemOptionCategory.Include(t => t.SystemOptionDefinitions).FirstOrDefault(t => t.ID == 910);
            bool getOptionValue(string name, ICollection<SystemOptionDefinition> optionDefinitions)
            {
                var option = optionDefinitions.First(t => t.Name.Contains(name, StringComparison.OrdinalIgnoreCase));
                return Convert.ToBoolean(option.DefaultValue);
            };

            Assert.IsNotNull(visualizationModel.DefaultOptions);
            Assert.AreEqual(
                getOptionValue("HideMaterialDimensions", optionCategory.SystemOptionDefinitions),
                visualizationModel.DefaultOptions.HideMaterialDimensions
            );
            Assert.AreEqual(
                // NOTE: rules will set this to TRUE if no margin specified in the layout
                //getOptionValue("HideMargin", optionCategory.SystemOptionDefinitions), 
                true,
                visualizationModel.DefaultOptions.HideMargin
            );
            Assert.AreEqual(
                // NOTE: rules will set this to TRUE if no bleed specified in the layout
                //getOptionValue("HideBleed", optionCategory.SystemOptionDefinitions), 
                true,
                visualizationModel.DefaultOptions.HideBleed
            );
            Assert.AreEqual(
                // NOTE: rules will set this to TRUE if no leader specified in the layout
                //getOptionValue("HideLeader", optionCategory.SystemOptionDefinitions), 
                true,
                visualizationModel.DefaultOptions.HideLeader
            );
            Assert.AreEqual(
                getOptionValue("HideGutter", optionCategory.SystemOptionDefinitions),
                visualizationModel.DefaultOptions.HideGutter
            );
            Assert.AreEqual(
                // NOTE: rules will set this to TRUE if no colorbar specified in the layout
                //getOptionValue("HideColorbar", optionCategory.SystemOptionDefinitions), 
                true,
                visualizationModel.DefaultOptions.HideColorbar
            );
            Assert.AreEqual(
                getOptionValue("HideCutLines", optionCategory.SystemOptionDefinitions),
                visualizationModel.DefaultOptions.HideCutlines
            );
            Assert.AreEqual(
                getOptionValue("HideFoldLines", optionCategory.SystemOptionDefinitions),
                visualizationModel.DefaultOptions.HideFoldlines
            );
            Assert.AreEqual(
                getOptionValue("HideWatermark", optionCategory.SystemOptionDefinitions),
                visualizationModel.DefaultOptions.HideWatermark
            );
            Assert.AreEqual(
                getOptionValue("HideRegistration", optionCategory.SystemOptionDefinitions),
                visualizationModel.DefaultOptions.HideRegistration
            );
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationVisualizationDataMaterialAndMachine()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables[myLinkedAssembly.Name];
            Assert.IsInstanceOfType(assemblyElement, typeof(LayoutComputationAssemblyVariable));

            LayoutComputationAssemblyVariable variable = assemblyElement as LayoutComputationAssemblyVariable;
            Assert.IsInstanceOfType(variable.Component, typeof(LayoutComputingAssembly));
            var result = assembly.Compute(new CBELOverriddenValues()
            {
                OVChildValues = new List<ICBELOverriddenValues>(){
                    new CBELOverriddenValues()
                    {
                        VariableName = variable.Name,
                        ComponentID = 1,
                        VariableData = new List<IVariableData>()
                        {
                            new VariableData()
                            {
                                VariableName = "ItemHeight",
                                IsOV = true,
                                Value = "10",
                            },
                            new VariableData()
                            {
                                VariableName = "ItemWidth",
                                IsOV = true,
                                Value = "5",
                            }
                        }
                    }
                }
            });
            var visualizationJson = result.Results.First().VariableData.First(t => t.VariableName == nameof(LayoutComputingAssembly.VisualizationData)).Value;
            var settings = new JsonSerializerSettings
            {
                ObjectCreationHandling = ObjectCreationHandling.Replace
            };
            var visualizationModel = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationJson);
            var materialModel = visualizationModel.Material;
            Assert.IsNotNull(materialModel.Name);
            Assert.IsNotNull(materialModel.Size);
            Assert.IsNotNull(materialModel.Units);
            var machineModel = visualizationModel.Machine;
            Assert.IsNotNull(machineModel.Name);
            Assert.IsNotNull(machineModel.MaterialMargin);
            Assert.IsNotNull(machineModel.Leader);
            Assert.IsNotNull(machineModel.MaxSize);

        }

        [TestMethod]
        public async Task TestCBELLayoutComputationIsPaneledBase()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables[myLinkedAssembly.Name];
            Assert.IsInstanceOfType(assemblyElement, typeof(LayoutComputationAssemblyVariable));

            LayoutComputationAssemblyVariable variable = assemblyElement as LayoutComputationAssemblyVariable;
            variable.LoadLinkedComponent();
            Assert.IsInstanceOfType(variable.Component, typeof(LayoutComputingAssembly));
            LayoutComputingAssembly layout = variable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = assembly.ID,
                Quantity = 1m,
                QuantityOV = true,
                CompanyID = assembly.CompanyID,
                ParentQuantity = assembly.ParentQuantity,
                IsVended = true,
                VariableName = myLinkedAssembly.Name,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "GutterHorizontal",
                        IsOV = true,
                        Value = "5",
                    },
                    new VariableData()
                    {
                        VariableName = "MaterialWidth",
                        IsOV = true,
                        Value = "100",
                    },
                    new VariableData()
                    {
                        VariableName = "ItemWidth",
                        IsOV = true,
                        Value = "10",
                    },
                    new VariableData()
                    {
                        VariableName = "MaterialHeight",
                        IsOV = true,
                        Value = "100",
                    },
                    new VariableData()
                    {
                        VariableName = "ItemHeight",
                        IsOV = true,
                        Value = "10",
                    },
                    new VariableData()
                    {
                        VariableName = "GutterVertical",
                        IsOV = true,
                        Value = "5",
                    },

                },
            };
            layout.Compute(ovValues);
            Assert.AreEqual(false, layout.IsPaneled.Value);
            Assert.AreEqual(false, layout.IsPaneledHorizontally);
            Assert.AreEqual(false, layout.IsPaneledVertically);
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationIsPaneledHorizontally()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables[myLinkedAssembly.Name];
            Assert.IsInstanceOfType(assemblyElement, typeof(LayoutComputationAssemblyVariable));

            LayoutComputationAssemblyVariable variable = assemblyElement as LayoutComputationAssemblyVariable;
            variable.LoadLinkedComponent();
            Assert.IsInstanceOfType(variable.Component, typeof(LayoutComputingAssembly));
            LayoutComputingAssembly layout = variable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = assembly.ID,
                Quantity = 1m,
                QuantityOV = true,
                CompanyID = assembly.CompanyID,
                ParentQuantity = assembly.ParentQuantity,
                IsVended = true,
                VariableName = myLinkedAssembly.Name,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "GutterHorizontal",
                        IsOV = true,
                        Value = "5",
                    },
                    new VariableData()
                    {
                        VariableName = "BleedLeft",
                        IsOV = true,
                        Value = "5",
                    },
                    new VariableData()
                    {
                        VariableName = "MaterialWidth",
                        IsOV = true,
                        Value = "100",
                    },
                    new VariableData()
                    {
                        VariableName = "ItemWidth",
                        IsOV = true,
                        Value = "100",
                    },
                },
            };
            layout.Compute(ovValues);
            Assert.AreEqual(true, layout.IsPaneled.Value);
            Assert.AreEqual(true, layout.IsPaneledHorizontally);
            Assert.AreEqual(false, layout.IsPaneledVertically);
        }

        [TestMethod]
        public async Task TestCBELLayoutComputationIsPaneledVertically()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedAssembly = new CBELVariableGenerator(gen)
            {
                Name = "LayoutComputation",
                VariableClassName = "LayoutComputationAssemblyVariable",
                Label = "LayoutComputation",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedAssembly);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables[myLinkedAssembly.Name];
            Assert.IsInstanceOfType(assemblyElement, typeof(LayoutComputationAssemblyVariable));

            LayoutComputationAssemblyVariable variable = assemblyElement as LayoutComputationAssemblyVariable;
            variable.LoadLinkedComponent();
            Assert.IsInstanceOfType(variable.Component, typeof(LayoutComputingAssembly));
            LayoutComputingAssembly layout = variable.Component as LayoutComputingAssembly;
            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = assembly.ID,
                Quantity = 1m,
                QuantityOV = true,
                CompanyID = assembly.CompanyID,
                ParentQuantity = assembly.ParentQuantity,
                IsVended = true,
                VariableName = myLinkedAssembly.Name,
                VariableData = new List<IVariableData>()
                {
                    new VariableData()
                    {
                        VariableName = "GutterHorizontal",
                        IsOV = true,
                        Value = "5",
                    },
                    new VariableData()
                    {
                        VariableName = "MaterialWidth",
                        IsOV = true,
                        Value = "100",
                    },
                    new VariableData()
                    {
                        VariableName = "ItemWidth",
                        IsOV = true,
                        Value = "10",
                    },
                    new VariableData()
                    {
                        VariableName = "MaterialHeight",
                        IsOV = true,
                        Value = "100",
                    },
                    new VariableData()
                    {
                        VariableName = "ItemHeight",
                        IsOV = true,
                        Value = "100",
                    },
                    new VariableData()
                    {
                        VariableName = "GutterVertical",
                        IsOV = true,
                        Value = "5",
                    },
                    new VariableData()
                    {
                        VariableName = "BleedTop",
                        IsOV = true,
                        Value = "5",
                    },

                },
            };
            layout.Compute(ovValues);
            Assert.AreEqual(true, layout.IsPaneled.Value);
            Assert.AreEqual(false, layout.IsPaneledHorizontally);
            Assert.AreEqual(true, layout.IsPaneledVertically);
        }

        [TestMethod]
        public async Task TestCBELLinkedMachineAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedMachine = new CBELVariableGenerator(gen)
            {
                Name = "LinkedMaterial",
                VariableClassName = "LinkedMaterialVariable",
                Label = "MaterialID",
                VariableDataType = DataType.String
            };
            gen.AddVariable(myLinkedMachine);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["LinkedMaterial"];
            Assert.IsInstanceOfType(assemblyElement, typeof(LinkedMaterialVariable));
            Assert.AreEqual("MaterialID", assemblyElement.Label);
        }

        [TestMethod]
        public async Task TestCBELAssemblyReferencedByElements()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements(BID: bid, ID: 100, Version: 2);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);

            ICBELVariable assemblyElement = assembly.Variables["Area"];
            Assert.IsInstanceOfType(assemblyElement, typeof(NumberEditElement));
            var areaElem = (ICBELVariableComputed)assemblyElement;
            Assert.IsTrue(areaElem.ReliesOn.ContainsKey("Height"));
            Assert.IsTrue(areaElem.ReferencedBy.ContainsKey("TotalArea"));
            Assert.IsFalse(areaElem.ReliesOn.ContainsKey("TotalArea"));
            Assert.IsFalse(areaElem.ReferencedBy.ContainsKey("Height"));
            Assert.IsFalse(areaElem.ReliesOn.ContainsKey("Area"));
            Assert.IsFalse(areaElem.ReferencedBy.ContainsKey("Area"));

            ICBELVariable totalAreaElement = assembly.Variables["TotalArea"];
            Assert.IsInstanceOfType(totalAreaElement, typeof(NumberEditElement));
            var totalAreaElem = (ICBELVariableComputed)totalAreaElement;

            Assert.IsTrue(totalAreaElem.ReliesOn.ContainsKey("AssemblyQuantity"));
        }

        [TestMethod]
        public async Task TestCBELAssemblyBadFunctionException_1()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator AreaFail = new CBELElementGenerator(gen)
            {
                Name = "AreaFail",
                VariableClassName = "NumberEditElement",
                FormulaText = "1",
                IsRequired = true,
                Label = "Area Fail",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "Height * Width * (1 + IsTwoSidedElement.AsNumber)",
            };
            AreaFail.ReliesOn.Add("Width");
            AreaFail.ReliesOn.Add("Height");
            AreaFail.ReliesOn.Add("IsTwoSided");
            gen.AddVariable(AreaFail);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            try
            {
                ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            }
            catch (InvalidOperationException ex)
            {
                Assert.IsTrue(ex.Message.Contains("Operator '*' cannot be applied to operands of type 'NumberEditElement' and 'NumberEditElement'"));
            }
        }

        [TestMethod]
        public async Task TestCBELAssemblyBadFunctionException_2()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator AreaFail = new CBELElementGenerator(gen)
            {
                Name = "AreaFail",
                VariableClassName = "NumberEditElement",
                FormulaText = "1",
                IsRequired = true,
                Label = "Area Fail",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "Height.Value * Green * (1 + IsTwoSided.AsNumber)",
            };
            AreaFail.ReliesOn.Add("Width");
            AreaFail.ReliesOn.Add("Height");
            AreaFail.ReliesOn.Add("IsTwoSided");
            gen.AddVariable(AreaFail);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            try
            {
                ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            }
            catch (InvalidOperationException ex)
            {
                Assert.IsTrue(ex.Message.Contains("The name 'Green' does not exist in the current context"));
            }
        }

        [TestMethod]
        public async Task TestCBELAssemblyBadFunctionException_3()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator AreaFail = new CBELElementGenerator(gen)
            {
                Name = "AreaFail",
                VariableClassName = "NumberEditElement",
                FormulaText = "1",
                IsRequired = true,
                Label = "Area Fail",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "Height.Value * SIN(3, \"red\") * (1 + IsTwoSided.AsNumber)",
            };
            AreaFail.ReliesOn.Add("Width");
            AreaFail.ReliesOn.Add("Height");
            AreaFail.ReliesOn.Add("IsTwoSided");
            gen.AddVariable(AreaFail);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            try
            {
                ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            }
            catch (InvalidOperationException ex)
            {
                Assert.IsTrue(ex.Message.Contains("No overload for method 'SIN' takes 2 arguments"));
            }
        }

        [TestMethod]
        public async Task TestCBELAssemblyBadFunctionException_4()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELElementGenerator DescriptionFail = new CBELElementGenerator(gen)
            {
                Name = "DescriptionFail",
                VariableClassName = "SingleLineTextElement",
                FormulaText = "= TEXT( NOW(), \"MM/dd/yyyy\" ) + Jeremy",
                IsRequired = true,
                Label = "Description Fail",
                VariableDataType = DataType.String,
                DefaultValueFunction = "ExcelFunctions.TEXT(ExcelFunctions.NOW(), \"MM/dd/yyyy\") + Jeremy",
            };
            gen.AddVariable(DescriptionFail);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            try
            {
                ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            }
            catch (InvalidOperationException ex)
            {
                Assert.IsTrue(ex.Message.Contains("The name 'Jeremy' does not exist in the current context"));
            }
        }

        [TestMethod]
        public async Task TestCBELAssemblyCompute_SuccessNoErrors()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);

            CBELOverriddenValues request = new CBELOverriddenValues();
            request.VariableData.Add(new VariableData
            {
                VariableName = "Height",
                IsOV = true,
                Value = "18",
            });
            request.VariableData.Add(new VariableData
            {
                VariableName = "Width",
                IsOV = true,
                Value = "24",
            });


            ICBELComputeResult result = assembly.Compute(request);

            Assert.IsNotNull(result?.VariableData);

            AssertVariableValue("Height", "18", true, result.VariableData);
            AssertVariableValue("Width", "24", true, result.VariableData);
            AssertVariableValue("Area", (18 * 24).ToString(), false, result.VariableData);

            Assert.IsFalse(result.HasErrors);
            Assert.IsFalse(result.HasValidationFailures);
        }

        [TestMethod]
        public async Task TestCBELAssemblyCompute_Error()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);

            CBELOverriddenValues request = new CBELOverriddenValues();
            request.VariableData.Add(new VariableData
            {
                VariableName = "Height",
                IsOV = true,
                Value = "3",
            });

            request.VariableData.Add(new VariableData
            {
                VariableName = "Width",
                IsOV = true,
                Value = "0",
            });

            request.VariableData.Add(new VariableData
            {
                VariableName = "TotalQuantity",
                IsOV = false,
                Value = null,
            });

            ICBELComputeResult result = assembly.Compute(request);

            Assert.IsNotNull(result?.VariableData);

            AssertVariableValue("Height", "3", true, result.VariableData);
            AssertVariableValue("Width", "0", true, result.VariableData);
            AssertVariableValue("Area", (3 * 0).ToString(), false, result.VariableData);

            Assert.IsTrue(result.HasErrors);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.IsInstanceOfType(result.Errors[0].InnerException, typeof(DivideByZeroException));

            Assert.IsFalse(result.HasValidationFailures);
        }

        [TestMethod]
        public async Task TestCBELAssemblyCompute_SuccessValidationErrors()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements(DescriptionRequired: true);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);

            CBELOverriddenValues request = new CBELOverriddenValues();
            request.VariableData.Add(new VariableData
            {
                VariableName = "Height",
                IsOV = true,
                Value = "2",
            });
            request.VariableData.Add(new VariableData
            {
                VariableName = "Width",
                IsOV = true,
                Value = "2",
            });

            ICBELComputeResult result = assembly.Compute(request);

            Assert.IsNotNull(result?.VariableData);

            AssertVariableValue("Height", "2", true, result.VariableData);
            AssertVariableValue("Width", "2", true, result.VariableData);
            AssertVariableValue("Area", (2 * 2).ToString(), false, result.VariableData);

            Assert.IsFalse(result.HasErrors);

            Assert.IsTrue(result.HasValidationFailures);
            Assert.AreEqual(1, result.ValidationFailures.Count);
            Assert.IsInstanceOfType(result.ValidationFailures[0], typeof(ICBELValidationFailure));
            Assert.AreEqual("Description", result.ValidationFailures[0].ElementComponent.Name);
        }

        [TestMethod]
        public async Task TestCBELAssemblyCompute_ErrorAndValidationError()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements(DescriptionRequired: true);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);

            CBELOverriddenValues request = new CBELOverriddenValues();
            request.VariableData.Add(new VariableData
            {
                VariableName = "Height",
                IsOV = true,
                Value = "3",
            });
            request.VariableData.Add(new VariableData
            {
                VariableName = "Width",
                IsOV = true,
                Value = "0",
            });

            ICBELComputeResult result = assembly.Compute(request);

            Assert.IsNotNull(result?.VariableData);

            AssertVariableValue("Height", "3", true, result.VariableData);
            AssertVariableValue("Width", "0", true, result.VariableData);
            AssertVariableValue("Area", (3 * 0).ToString(), false, result.VariableData);

            Assert.IsTrue(result.HasErrors);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.IsInstanceOfType(result.Errors[0].InnerException, typeof(DivideByZeroException));

            Assert.IsTrue(result.HasValidationFailures);
            Assert.AreEqual(1, result.ValidationFailures.Count);
            Assert.IsInstanceOfType(result.ValidationFailures[0], typeof(ICBELValidationFailure));
            Assert.AreEqual("Description", result.ValidationFailures[0].ElementComponent.Name);
        }

        /// <summary>
        /// Gets a CBELAssemblyGenerator with some default elements
        /// </summary>
        /// <param name="AssemblyName">Assembly Name</param>
        /// <param name="BID">Business ID</param>
        /// <param name="ID">ID</param>
        /// <param name="Version">Version</param>
        /// <returns></returns>
        public static CBELAssemblyGenerator GetCBELTestAssemblyGeneratorWithDefaultElements(string AssemblyName = "TestAssembly", short BID = 1, int ID = 1023, int Version = 1, bool DescriptionRequired = false)
        {
            CBELAssemblyGenerator gen = new CBELAssemblyGenerator(BID, ID, ClassType.Assembly.ID(), $"Assembly: {AssemblyName}", Version);

            // add any additional usings
            // 
            // gen.AddUsingReference("Endor.CBEL.Common", false);

            // now set any default fields
            gen.AddSetter("Name", $"\"{AssemblyName}\"");  // be sure to add the quotes around strings
            gen.AddSetter("CompanyID", null);

            #region Default Elements

            // Main : groupbox
            // Update the TotalQuantity Variable
            CBELElementGenerator AssemblyQuantity = new CBELElementGenerator(gen)
            {
                Name = "AssemblyQuantity",
                VariableClassName = "NumberEditElement",
                FormulaText = "5",
                IsRequired = true,
                Label = "Assembly Quantity",
                VariableDataType = DataType.Number,
                DefaultValueConstant = "5",
            };
            AssemblyQuantity.ReferencedBy.Add("TotalArea");
            gen.AddVariable(AssemblyQuantity);

            // Width : number edit default 18
            CBELElementGenerator Width = new CBELElementGenerator(gen)
            {
                Name = "Width",
                VariableClassName = "NumberEditElement",
                FormulaText = "18",
                IsRequired = true,
                Label = "Width",
                VariableDataType = DataType.Number,
                DefaultValueConstant = "18",
            };
            Width.ReferencedBy.Add("Area");
            gen.AddVariable(Width);

            // Height : number edit default 24
            CBELElementGenerator Height = new CBELElementGenerator(gen)
            {
                Name = "Height",
                VariableClassName = "NumberEditElement",
                FormulaText = "24",
                IsRequired = true,
                Label = "Height",
                VariableDataType = DataType.Number,
                DefaultValueConstant = "24",
            };
            Height.ReferencedBy.Add("Area");
            gen.AddVariable(Height);

            // TwoSided : checkbox default false
            CBELElementGenerator IsTwoSided = new CBELElementGenerator(gen)
            {
                Name = "IsTwoSided",
                VariableClassName = "CheckboxElement",
                FormulaText = "= false",
                Label = "Is Two Sided",
                VariableDataType = DataType.Boolean,
                DefaultValueConstant = "false"
            };
            IsTwoSided.ReferencedBy.Add("Area");
            gen.AddVariable(IsTwoSided);

            // Area : number edit = Height.Value * Width.Value * (1 + IsTwoSided.AsNumber)
            CBELElementGenerator Area = new CBELElementGenerator(gen)
            {
                Name = "Area",
                VariableClassName = "NumberEditElement",
                FormulaText = "= Height.Value * Width.Value * (1 + IsTwoSided.AsNumber)",
                Label = "Area",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "(Height.Value * Width.Value * (1 + IsTwoSided.AsNumber))",
            };
            Area.ReliesOn.Add("Width");
            Area.ReliesOn.Add("Height");
            Area.ReliesOn.Add("IsTwoSided");
            Area.ReferencedBy.Add("TotalArea");
            gen.AddVariable(Area);

            CBELElementGenerator TotalArea = new CBELElementGenerator(gen)
            {
                Name = "TotalArea",
                VariableClassName = "NumberEditElement",
                FormulaText = "= AssemblyQuantity * Area.Value",
                Label = "TotalArea",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "(AssemblyQuantity.Value * Area.Value)",
            };
            TotalArea.ReliesOn.Add("AssemblyQuantity");
            TotalArea.ReliesOn.Add("Area");
            gen.AddVariable(TotalArea);

            // Description : string edit
            CBELElementGenerator Description = new CBELElementGenerator(gen)
            {
                Name = "Description",
                VariableClassName = "SingleLineTextElement",
                FormulaText = "= TEXT(NOW(), \"MM/dd/yyyy\")",
                Label = "Description",
                VariableDataType = DataType.String,
            };

            if (DescriptionRequired)
                Description.IsRequired = DescriptionRequired;
            else
                Description.DefaultValueFunction = "ExcelFunctions.TEXT(ExcelFunctions.NOW(), \"MM/dd/yyyy\")";

            gen.AddVariable(Description);

            // AspectRatio : number edit = Height.Value / Width.Value
            CBELElementGenerator AspectRatio = new CBELElementGenerator(gen)
            {
                Name = "AspectRatio",
                VariableClassName = "NumberEditElement",
                FormulaText = "= Height.Value / Width.Value",
                Label = "Aspect Ratio",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "(Height.Value / Width.Value)",
            };
            AspectRatio.ReliesOn.Add("Width");
            AspectRatio.ReliesOn.Add("Height");
            gen.AddVariable(AspectRatio);

            #endregion

            return gen;
        }

        public static CBELMachineGenerator GetCBELTestMachineGeneratorWithDefaultElements(string AssemblyName = "TestAssembly", short BID = 1, int ID = 1023, int Version = 1, bool DescriptionRequired = false)
        {
            CBELMachineGenerator gen = new CBELMachineGenerator(BID, ID, ClassType.Machine.ID(), $"Machine:  {AssemblyName}", Version);

            // add any additional usings
            // 
            // gen.AddUsingReference("Endor.CBEL.Common", false);

            // now set any default fields
            gen.AddSetter("Name", $"\"{AssemblyName}\"");  // be sure to add the quotes around strings
            gen.AddSetter("CompanyID", null);

            #region Default Elements

            // Main : groupbox
            // Update the AssemblyQuantity Variable
            CBELElementGenerator AssemblyQuantity = new CBELElementGenerator(gen)
            {
                Name = "AssemblyQuantity",
                VariableClassName = "NumberEditElement",
                FormulaText = "5",
                IsRequired = true,
                Label = "Assembly Quantity",
                VariableDataType = DataType.Number,
                DefaultValueConstant = "5",
            };
            AssemblyQuantity.ReferencedBy.Add("TotalArea");
            gen.AddVariable(AssemblyQuantity);

            // Width : number edit default 18
            CBELElementGenerator Width = new CBELElementGenerator(gen)
            {
                Name = "Width",
                VariableClassName = "NumberEditElement",
                FormulaText = "18",
                IsRequired = true,
                Label = "Width",
                VariableDataType = DataType.Number,
                DefaultValueConstant = "18",
            };
            Width.ReferencedBy.Add("Area");
            gen.AddVariable(Width);

            // Height : number edit default 24
            CBELElementGenerator Height = new CBELElementGenerator(gen)
            {
                Name = "Height",
                VariableClassName = "NumberEditElement",
                FormulaText = "24",
                IsRequired = true,
                Label = "Height",
                VariableDataType = DataType.Number,
                DefaultValueConstant = "24",
            };
            Height.ReferencedBy.Add("Area");
            gen.AddVariable(Height);

            // TwoSided : checkbox default false
            CBELElementGenerator IsTwoSided = new CBELElementGenerator(gen)
            {
                Name = "IsTwoSided",
                VariableClassName = "CheckboxElement",
                FormulaText = "= false",
                Label = "Is Two Sided",
                VariableDataType = DataType.Boolean,
                DefaultValueConstant = "false"
            };
            IsTwoSided.ReferencedBy.Add("Area");
            gen.AddVariable(IsTwoSided);

            // Area : number edit = Height.Value * Width.Value * (1 + IsTwoSided.AsNumber)
            CBELElementGenerator Area = new CBELElementGenerator(gen)
            {
                Name = "Area",
                VariableClassName = "NumberEditElement",
                FormulaText = "= Height.Value * Width.Value * (1 + IsTwoSided.AsNumber)",
                Label = "Area",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "(Height.Value * Width.Value * (1 + IsTwoSided.AsNumber))",
            };
            Area.ReliesOn.Add("Width");
            Area.ReliesOn.Add("Height");
            Area.ReliesOn.Add("IsTwoSided");
            Area.ReferencedBy.Add("TotalArea");
            gen.AddVariable(Area);

            CBELElementGenerator TotalArea = new CBELElementGenerator(gen)
            {
                Name = "TotalArea",
                VariableClassName = "NumberEditElement",
                FormulaText = "= AssemblyQuantity * Area.Value",
                Label = "TotalArea",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "(AssemblyQuantity.Value * Area.Value)",
            };
            TotalArea.ReliesOn.Add("AssemblyQuantity");
            TotalArea.ReliesOn.Add("Area");
            gen.AddVariable(TotalArea);

            // Description : string edit
            CBELElementGenerator Description = new CBELElementGenerator(gen)
            {
                Name = "DescriptionText",
                VariableClassName = "SingleLineTextElement",
                FormulaText = "= TEXT(NOW(), \"MM/dd/yyyy\")",
                Label = "Description",
                VariableDataType = DataType.String,
            };

            if (DescriptionRequired)
                Description.IsRequired = DescriptionRequired;
            else
                Description.DefaultValueFunction = "ExcelFunctions.TEXT(ExcelFunctions.NOW(), \"MM/dd/yyyy\")";

            gen.AddVariable(Description);

            // AspectRatio : number edit = Height.Value / Width.Value
            CBELElementGenerator AspectRatio = new CBELElementGenerator(gen)
            {
                Name = "AspectRatio",
                VariableClassName = "NumberEditElement",
                FormulaText = "= Height.Value / Width.Value",
                Label = "Aspect Ratio",
                VariableDataType = DataType.Number,
                DefaultValueFunction = "(Height.Value / Width.Value)",
            };
            AspectRatio.ReliesOn.Add("Width");
            AspectRatio.ReliesOn.Add("Height");
            gen.AddVariable(AspectRatio);

            #endregion

            return gen;
        }

        #endregion

        private void AssertVariableValue(string VariableName, string expectedValue, bool expectedOV, List<IVariableData> variableDataList)
        {
            IVariableData variableData = variableDataList.FirstOrDefault(v => v.VariableName == VariableName);
            Assert.IsNotNull(variableData);
            Assert.AreEqual(expectedOV, variableData.IsOV);
            Assert.AreEqual(expectedValue, variableData.Value);
        }

        [TestMethod]
        public void CreateCBELAssemblyGeneratorThrowsExceptionWhenContextIsNotSupplied()
        {
            short bid = 1;
            int id = -199;

            Assert.ThrowsException<ArgumentNullException>(() => CBELAssemblyGenerator.Create(null, bid, id, ClassType.Assembly.ID()));
        }

        [TestMethod]
        public void CreateCBELAssemblyGeneratorThrowsExceptionWhenAnInvalidAssemblyIDIsSupplied()
        {
            short bid = 1;
            int id = -201;
            var apiContext = PricingTestHelper.GetMockCtx(bid);

            Assert.ThrowsException<ArgumentException>(() => CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID()));
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithOnlyBaseElements()
        {
            short bid = 1;
            int id = -202;
            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            await CleanupPreviousUsage(bid, id, apiContext, cache);

            apiContext = PricingTestHelper.GetMockCtx(bid);
            apiContext.AssemblyData.Add(new AssemblyData() { BID = bid, ID = id, Name = "AssemblyWithOnlyBaseElements" });
            apiContext.SaveChanges();

            var gen = CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID());

            ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            var properties = instance.GetType().GetProperties().ToList();
            Assert.AreEqual(4, properties.Where(t => t.PropertyType.GetInterface(nameof(ICBELVariable)) != null).Count());

            await CleanupPreviousUsage(bid, id, apiContext, cache);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithASingleLineTextElement()
        {
            short bid = 1;
            int id = -205;
            string value = "Lorem Ipsum";

            await TestVariable<SingleLineTextElement>(bid, id, value, AssemblyElementType.SingleLineText, DataType.String);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithACheckboxElement()
        {
            short bid = 1;
            int id = -206;
            string value = "true";

            await TestVariable<CheckboxElement>(bid, id, value, AssemblyElementType.Checkbox, DataType.Boolean);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithACheckboxElementFormula()
        {
            short bid = 1;
            int id = -206;
            string value = "=true";

            await TestVariable<CheckboxElement>(bid, id, value, AssemblyElementType.Checkbox, DataType.Boolean);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithACheckboxElementNullValue()
        {
            short bid = 1;
            int id = -206;

            await TestVariable<CheckboxElement>(bid, id, null, "false", AssemblyElementType.Checkbox, DataType.Boolean);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithANumberEditElementFormula()
        {
            short bid = 1;
            int id = -218;
            string value = "=10";

            await TestVariable<NumberEditElement>(bid, id, value, AssemblyElementType.Number, DataType.Number);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithRelatedVariables()
        {
            short bid = 1;
            int id = -214;

            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            string descriptionVarName = AssemblyElementType.Number.ToString();
            await CleanupPreviousUsage(bid, id, apiContext, cache);

            apiContext = PricingTestHelper.GetMockCtx(bid);
            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"AssemblyWithRelatedVariables" };
            var test1Var = new AssemblyVariable()
            {
                Name = "test1Var",
                ElementType = AssemblyElementType.Number,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "= test2Var.Value + test3Var.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };
            var test2Var = new AssemblyVariable()
            {
                Name = "test2Var",
                ElementType = AssemblyElementType.Number,
                ID = id - 1,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "10",
                DataType = DataType.Number,
                IsFormula = false,
            };
            var test3Var = new AssemblyVariable()
            {
                Name = "test3Var",
                ElementType = AssemblyElementType.Number,
                ID = id - 2,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "8",
                DataType = DataType.Number,
                IsFormula = false,
            };
            apiContext.AssemblyVariable.Add(test1Var);
            apiContext.AssemblyVariable.Add(test2Var);
            apiContext.AssemblyVariable.Add(test3Var);
            apiContext.AssemblyData.Add(assemblyData);

            apiContext.SaveChanges();

            var gen = CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID());
            //string myCode = gen.GetAssemblyCode();

            ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            var properties = instance.GetType().GetProperties().ToList();
            Assert.AreEqual(7, properties.Where(t => t.PropertyType.GetInterface(nameof(ICBELVariable)) != null).Count());
            Assert.IsNotNull(instance.Variables.FirstOrDefault(t => t.Key == descriptionVarName));
            Assert.AreEqual(int.Parse(test2Var.DefaultValue) + int.Parse(test3Var.DefaultValue), ((NumberEditElement)instance.Variables[nameof(test1Var)]).Value);

            var test1VarReliesOn = ((NumberEditElement)instance.Variables[nameof(test1Var)]).ReliesOn;
            var test2VarReliesOn = ((NumberEditElement)instance.Variables[nameof(test2Var)]).ReliesOn;
            var test3VarReliesOn = ((NumberEditElement)instance.Variables[nameof(test3Var)]).ReliesOn;

            Assert.AreEqual(2, test1VarReliesOn.Count);
            Assert.IsTrue(test1VarReliesOn.Any(t => t.Key == test2Var.Name));
            Assert.IsTrue(test1VarReliesOn.Any(t => t.Key == test3Var.Name));
            Assert.IsFalse(test1VarReliesOn.Any(t => t.Key == test1Var.Name));

            Assert.AreEqual(0, test2VarReliesOn.Count);
            Assert.AreEqual(0, test3VarReliesOn.Count);

            //Clean everything up so it doesn't interfere with other tests using the same IDs
            await CleanupPreviousUsage(bid, id, apiContext, cache);
            await CleanupPreviousUsage(bid, id - 1, apiContext, cache);
            await CleanupPreviousUsage(bid, id - 2, apiContext, cache);

        }


        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithAMeasurementElement()
        {
            short bid = 1;
            int id = -211;
            string value = "10";

            await TestVariable<NumberEditElement>(bid, id, value, AssemblyElementType.Number, DataType.Number);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithAMultiLineTextElement()
        {
            short bid = 1;
            int id = -212;
            string value = "true\r\nfalse";

            await TestVariable<MultiLineTextElement>(bid, id, value, AssemblyElementType.MultiLineString, DataType.String);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithAShowHideGroupElement()
        {
            short bid = 1;
            int id = -213;
            string value = "true";

            await TestVariable<ShowHideGroupElement>(bid, id, value, AssemblyElementType.ShowHideGroup, DataType.Boolean);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithURLLabelElement()
        {
            short bid = 1;
            int id = -214;
            string value = "true";

            await TestVariable<URLLabelElement>(bid, id, value, AssemblyElementType.UrlLabel, DataType.String);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithAMultiLineLabelElement()
        {
            short bid = 1;
            int id = -215;
            string value = @"true
false";

            await TestVariable<MultiLineLabelElement>(bid, id, value, AssemblyElementType.MultiLineLabel, DataType.String);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithADropDownLaborListElement()
        {
            short bid = 1;
            int id = -207;
            string value = AssemblyElementType.DropDown.ToString();

            await TestVariable<DropDownLaborListElement>(bid, id, value, AssemblyElementType.DropDown, DataType.String, DataType.LaborPart);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithADropDownMaterialListElement()
        {
            short bid = 1;
            int id = -208;
            string value = AssemblyElementType.DropDown.ToString();

            await TestVariable<DropDownMaterialListElement>(bid, id, value, AssemblyElementType.DropDown, DataType.String, DataType.MaterialPart);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithALinkedLaborVariable()
        {
            short bid = 1;
            int id = -209;
            string value = AssemblyElementType.LinkedLabor.ToString();

            await TestVariable<LinkedLaborVariable>(bid, id, value, AssemblyElementType.LinkedLabor, DataType.LaborPart);
        }

        [TestMethod]
        public async Task CreateCBELAssemblyGeneratorReturnsAGeneratorWithALinkedMaterial()
        {
            short bid = 1;
            int id = -210;
            string value = AssemblyElementType.LinkedMaterial.ToString();

            await TestVariable<LinkedMaterialVariable>(bid, id, value, AssemblyElementType.LinkedMaterial, DataType.MaterialPart);
        }

        private async Task TestVariable<T>(short bid, int id, string valueAsString, AssemblyElementType elementType, DataType dataType, DataType? listDataType = null) where T : ICBELVariableComputed
        {
            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            string descriptionVarName = elementType.ToString();

            await CleanupPreviousUsage(bid, id, apiContext, cache);

            apiContext = PricingTestHelper.GetMockCtx(bid);
            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"AssemblyWithA{typeof(T).Name}" };
            var testVar = new AssemblyVariable()
            {
                Name = descriptionVarName,
                ElementType = elementType,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = valueAsString.ToStringLiteral(false) ?? null,
                DataType = dataType,
                ListDataType = listDataType,
                IsFormula = !String.IsNullOrWhiteSpace(valueAsString) && valueAsString.StartsWith('='),
            };
            if (testVar.ElementType == AssemblyElementType.Number)
                testVar.UnitID = Unit.None;
            apiContext.AssemblyVariable.Add(testVar);
            apiContext.AssemblyData.Add(assemblyData);

            apiContext.SaveChanges();

            var gen = CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID());

            ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(instance);
            var properties = instance.GetType().GetProperties().ToList();
            var t = properties.Where(t => t.PropertyType.GetInterface(nameof(ICBELVariable)) != null);
            Assert.AreEqual(5, properties.Where(t => t.PropertyType.GetInterface(nameof(ICBELVariable)) != null).Count());
            Assert.IsNotNull(instance.Variables.FirstOrDefault(t => t.Key == descriptionVarName));
            if (!testVar.IsFormula)
                Assert.AreEqual(valueAsString, ((T)instance.Variables[descriptionVarName]).AsString);
            await CleanupPreviousUsage(bid, id, apiContext, cache);
        }

        private async Task TestVariable<T>(short bid, int id, string valueAsString, string expectedValueAsString, AssemblyElementType elementType, DataType dataType, DataType? listDataType = null) where T : ICBELVariableComputed
        {
            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            string descriptionVarName = elementType.ToString();

            await CleanupPreviousUsage(bid, id, apiContext, cache);

            apiContext = PricingTestHelper.GetMockCtx(bid);
            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"AssemblyWithA{typeof(T).Name}" };
            var testVar = new AssemblyVariable()
            {
                Name = descriptionVarName,
                ElementType = elementType,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = valueAsString.ToStringLiteral(false) ?? null,
                DataType = dataType,
                ListDataType = listDataType,
                IsFormula = !String.IsNullOrWhiteSpace(valueAsString) && valueAsString.StartsWith('='),
            };
            if (testVar.ElementType == AssemblyElementType.Number)
                testVar.UnitID = Unit.None;
            apiContext.AssemblyVariable.Add(testVar);
            apiContext.AssemblyData.Add(assemblyData);

            apiContext.SaveChanges();

            var gen = CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID());

            ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(instance);
            var properties = instance.GetType().GetProperties().ToList();
            var t = properties.Where(t => t.PropertyType.GetInterface(nameof(ICBELVariable)) != null);
            Assert.AreEqual(5, properties.Where(t => t.PropertyType.GetInterface(nameof(ICBELVariable)) != null).Count());
            Assert.IsNotNull(instance.Variables.FirstOrDefault(t => t.Key == descriptionVarName));
            if (!testVar.IsFormula)
                Assert.AreEqual(expectedValueAsString, ((T)instance.Variables[descriptionVarName]).AsString);
            await CleanupPreviousUsage(bid, id, apiContext, cache);
        }

        #region Excel Tests

        [TestMethod]
        public void ExcelStringFunctionsTests()
        {
            string test = "This is my test string";

            // CHAR
            Assert.AreEqual(null, ExcelFunctions.CHAR(null));
            Assert.AreEqual("A", ExcelFunctions.CHAR(65m));
            Assert.AreEqual("$", ExcelFunctions.CHAR(36m));

            // CLEAN
            string stringDirty = "12345\r\n\t654789";
            string stringClean = "12345654789";
            Assert.AreEqual(null, ExcelFunctions.CLEAN(null));
            Assert.AreEqual(stringClean, ExcelFunctions.CLEAN(stringDirty));

            // CODE
            Assert.AreEqual(null, ExcelFunctions.CODE(null));
            Assert.AreEqual(65m, ExcelFunctions.CODE("A"));
            Assert.AreEqual(36m, ExcelFunctions.CODE("$"));

            // CONCAT
            Assert.AreEqual(null, ExcelFunctions.CONCAT(null));
            Assert.AreEqual(test, ExcelFunctions.CONCAT(null, test));
            Assert.AreEqual(test + test, ExcelFunctions.CONCAT(test, test));

            // CONCATENATE
            Assert.AreEqual(null, ExcelFunctions.CONCATENATE(null));
            Assert.AreEqual(test, ExcelFunctions.CONCATENATE(null, test));
            Assert.AreEqual(test + test, ExcelFunctions.CONCATENATE(test, test));

            // DOLLAR
            Assert.AreEqual(null, ExcelFunctions.DOLLAR(null));
            Assert.AreEqual(string.Format(CultureInfo.CreateSpecificCulture("en-US"), "{0:c}", 12.34m), ExcelFunctions.DOLLAR(12.34m)); // I added to specify the culture since it won't run on PH systems because of the culture
            Assert.AreEqual(ExcelFunctions.DOLLAR(12.34m, System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits), ExcelFunctions.DOLLAR(12.34m));
            Assert.AreEqual("$12.3400000", ExcelFunctions.DOLLAR(12.34m, 7));

            // EXACT
            Assert.AreEqual(false, ExcelFunctions.EXACT(null, null));
            Assert.AreEqual(false, ExcelFunctions.EXACT("test", null));
            Assert.AreEqual(false, ExcelFunctions.EXACT(null, "test"));
            Assert.AreEqual(true, ExcelFunctions.EXACT("test", "test"));
            Assert.AreEqual(false, ExcelFunctions.EXACT("Test", "test"));
            Assert.AreEqual(false, ExcelFunctions.EXACT("TEST", "test"));
            Assert.AreEqual(false, ExcelFunctions.EXACT("test ", "test"));

            // FIND
            Assert.AreEqual(null, ExcelFunctions.FIND(null, null));
            Assert.AreEqual(1, ExcelFunctions.FIND("M", "Miriam McGovern"));
            Assert.AreEqual(6, ExcelFunctions.FIND("m", "Miriam McGovern"));
            Assert.AreEqual(3, ExcelFunctions.FIND("A", "AAAAAAA", 3));
            Assert.AreEqual(4, ExcelFunctions.FIND("%", "1 3%$%456"));
            Assert.AreEqual(7, ExcelFunctions.FIND("4", "1 2 3 4 5 6"));
            Assert.AreEqual(7, ExcelFunctions.FIND("B", "A     B", 1));
            Assert.AreEqual(7, ExcelFunctions.FIND("middle", "start middle end", 1));
            Assert.AreEqual(null, ExcelFunctions.FIND("B", "AAAAAA"));
            Assert.AreEqual(null, ExcelFunctions.FIND("b", "a b c d e", 5));
            Assert.AreEqual(null, ExcelFunctions.FIND("A", "AAAA", 20));
            Assert.AreEqual(null, ExcelFunctions.FIND("A", "AAAA", 0));

            // FIXED
            Assert.AreEqual(null, ExcelFunctions.FIXED(null));
            Assert.AreEqual("1,234.6", ExcelFunctions.FIXED(1234.567m, 1m));
            Assert.AreEqual("1,230", ExcelFunctions.FIXED(1234.567m, -1m));
            Assert.AreEqual("-1,230", ExcelFunctions.FIXED(-1234.567m, -1m));
            Assert.AreEqual("12000.8", ExcelFunctions.FIXED(12000.75m, 1m, true));
            Assert.AreEqual("12,000.8", ExcelFunctions.FIXED(12000.75m, 1m, false));
            Assert.AreEqual("44.33", ExcelFunctions.FIXED(44.332m, 2m));
            Assert.AreEqual("44.33", ExcelFunctions.FIXED(44.332m));

            // ISBLANK
            Assert.AreEqual(true, ExcelFunctions.ISBLANK(null));
            Assert.AreEqual(false, ExcelFunctions.ISBLANK(""));
            Assert.AreEqual(false, ExcelFunctions.ISBLANK("Test"));
            Assert.AreEqual(false, ExcelFunctions.ISBLANK("10"));

            // ISNONTEXT
            Assert.AreEqual(false, ExcelFunctions.ISNONTEXT(null));
            Assert.AreEqual(true, ExcelFunctions.ISNONTEXT(""));
            Assert.AreEqual(false, ExcelFunctions.ISNONTEXT("Test"));
            Assert.AreEqual(true, ExcelFunctions.ISNONTEXT("10"));

            // ISNUMBER
            Assert.AreEqual(false, ExcelFunctions.ISNUMBER(null));
            Assert.AreEqual(false, ExcelFunctions.ISNUMBER(""));
            Assert.AreEqual(false, ExcelFunctions.ISNUMBER("Test"));
            Assert.AreEqual(true, ExcelFunctions.ISNUMBER("10"));

            // ISTEXT
            Assert.AreEqual(false, ExcelFunctions.ISTEXT(null));
            Assert.AreEqual(true, ExcelFunctions.ISTEXT(""));
            Assert.AreEqual(true, ExcelFunctions.ISTEXT("Test"));
            Assert.AreEqual(false, ExcelFunctions.ISTEXT("10"));

            // LEFT
            Assert.AreEqual("T", ExcelFunctions.LEFT(test));
            Assert.AreEqual("This", ExcelFunctions.LEFT(test, 4));
            Assert.IsNull(ExcelFunctions.LEFT(null));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => ExcelFunctions.LEFT(test, -1));
            // LEN
            Assert.AreEqual(test.Length, ExcelFunctions.LEN(test));
            Assert.IsNull(ExcelFunctions.LEN(null));
            // LOWER
            Assert.AreEqual(test.ToLower(), ExcelFunctions.LOWER(test));
            // MID
            Assert.AreEqual("T", ExcelFunctions.MID(test));
            Assert.AreEqual("h", ExcelFunctions.MID(test, 2));
            Assert.AreEqual("is", ExcelFunctions.MID(test, 3, 2));
            Assert.IsNull(ExcelFunctions.MID(null));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => ExcelFunctions.MID(test, 0));
            // NUMBERVALUE
            Assert.AreEqual(5m, ExcelFunctions.NUMBERVALUE("5"));
            Assert.AreEqual(0.05m, ExcelFunctions.NUMBERVALUE("5%"));
            Assert.AreEqual(0.001m, ExcelFunctions.NUMBERVALUE("10%%"));

            //REPLACE
            Assert.AreEqual("HABCDEFG World.", ExcelFunctions.REPLACE("Hello World.", 2, 4, "ABCDEFG"));

            //REPT
            Assert.AreEqual("ABCABCABC", ExcelFunctions.REPT("ABC", 3));

            //RIGHT
            Assert.AreEqual("orld.", ExcelFunctions.RIGHT("Hello World.", 4));

            //SEARCH
            Assert.AreEqual(4, ExcelFunctions.SEARCH("o", "Hello World."));
            Assert.AreEqual(7, ExcelFunctions.SEARCH("o", "Hello World.", 6));

            //SUBSTITUTE
            Assert.AreEqual("HABC World.", ExcelFunctions.SUBSTITUTE("Hello World.", "ello", "ABC"));
            Assert.AreEqual("Hello World ABC.", ExcelFunctions.SUBSTITUTE("Hello World Hello.", "Hello", "ABC", 2));
            Assert.AreEqual("HelloHelloABCHello.", ExcelFunctions.SUBSTITUTE("HelloHelloHelloHello.", "Hello", "ABC", 3));

            //T
            Assert.AreEqual("12", ExcelFunctions.T(12));

            //TEXT
            DateTime dtTextTest = DateTime.Parse("10/14/2012 05:30:10", CultureInfo.CreateSpecificCulture("en-US")); // I added to specify the culture since it won't run on PH systems because of the culture
            Assert.AreEqual("Sun, 14 Oct 2012 05:30:10", ExcelFunctions.TEXT(dtTextTest, "ddd, dd MMM yyyy HH':'mm':'ss"));
            Assert.AreEqual("123.457", ExcelFunctions.TEXT(123.456789m, "0.000"));
            Assert.AreEqual("123.457", ExcelFunctions.TEXT("123.456789", "0.000"));

            //TRIM
            Assert.AreEqual("Hello     World.", ExcelFunctions.TRIM("   Hello     World.    "));

            //UPPER
            Assert.AreEqual("HELLO WORLD.", ExcelFunctions.UPPER("Hello World."));

            //VALUE
            Assert.AreEqual(15, ExcelFunctions.VALUE("15"));
        }

        [TestMethod]
        public void ExcelMathFunctionsTests()
        {
            // ABS
            Assert.AreEqual(null, ExcelFunctions.ABS(null));
            Assert.AreEqual(1, ExcelFunctions.ABS(1));
            Assert.AreEqual(1, ExcelFunctions.ABS(-1));
            Assert.AreEqual(1.234m, ExcelFunctions.ABS(1.234m));
            Assert.AreEqual(1.234m, ExcelFunctions.ABS(-1.234m));

            // ACOS
            Assert.AreEqual(null, ExcelFunctions.ACOS(null));
            Assert.AreEqual(1.5708m, Math.Round(ExcelFunctions.ACOS(0).GetValueOrDefault(0m), 4));

            // ACOT
            Assert.AreEqual(null, ExcelFunctions.ACOT(null));
            Assert.AreEqual(0.7854m, Math.Round(ExcelFunctions.ACOT(1).GetValueOrDefault(0m), 4));

            // ASIN
            Assert.AreEqual(null, ExcelFunctions.ASIN(null));
            Assert.AreEqual(1.5708m, Math.Round(ExcelFunctions.ASIN(1).GetValueOrDefault(0m), 4));

            // ATAN
            Assert.AreEqual(null, ExcelFunctions.ATAN(null));
            Assert.AreEqual(0.7854m, Math.Round(ExcelFunctions.ATAN(1).GetValueOrDefault(0m), 4));

            // ATAN2
            Assert.AreEqual(null, ExcelFunctions.ATAN2(null, null));
            Assert.AreEqual(0.1974m, Math.Round(ExcelFunctions.ATAN2(1, 5).GetValueOrDefault(0m), 4));

            // AVERAGE
            Assert.AreEqual(null, ExcelFunctions.AVERAGE(null, null));
            Assert.AreEqual(1m, ExcelFunctions.AVERAGE(1m, null));
            Assert.AreEqual(1.5m, ExcelFunctions.AVERAGE(1m, 2m));

            // CEILING
            Assert.AreEqual(null, ExcelFunctions.CEILING(null));
            Assert.AreEqual(1m, ExcelFunctions.CEILING(1m));
            Assert.AreEqual(2m, ExcelFunctions.CEILING(1.1m));

            // COS
            Assert.AreEqual(null, ExcelFunctions.COS(null));
            Assert.AreEqual(0.5403m, Math.Round(ExcelFunctions.COS(1).GetValueOrDefault(0m), 4));

            // COT
            Assert.AreEqual(null, ExcelFunctions.COT(null));
            Assert.AreEqual(0.6421m, Math.Round(ExcelFunctions.COT(1).GetValueOrDefault(0m), 4));

            // CSC
            Assert.AreEqual(null, ExcelFunctions.CSC(null));
            Assert.AreEqual(1.1884m, Math.Round(ExcelFunctions.CSC(1).GetValueOrDefault(0m), 4));

            // EVEN
            Assert.AreEqual(null, ExcelFunctions.EVEN(null));
            Assert.AreEqual(24m, ExcelFunctions.EVEN(23.4m));
            Assert.AreEqual(2m, ExcelFunctions.EVEN(1m));
            Assert.AreEqual(14m, ExcelFunctions.EVEN(12.1m));
            Assert.AreEqual(0m, ExcelFunctions.EVEN(0m));
            Assert.AreEqual(-2m, ExcelFunctions.EVEN(-1));
            Assert.AreEqual(-24m, ExcelFunctions.EVEN(-23.2m));
            Assert.AreEqual(-24m, ExcelFunctions.EVEN(-23.9m));

            // EXP
            Assert.AreEqual(null, ExcelFunctions.EXP(null));
            Assert.AreEqual(2.7183m, Math.Round(ExcelFunctions.EXP(1m).GetValueOrDefault(0m), 4));
            Assert.AreEqual(7.3891m, Math.Round(ExcelFunctions.EXP(2m).GetValueOrDefault(0m), 4));
            Assert.AreEqual(0.3679m, Math.Round(ExcelFunctions.EXP(-1m).GetValueOrDefault(0m), 4));

            // FACT
            Assert.AreEqual(null, ExcelFunctions.FACT(null));
            Assert.AreEqual(1m, ExcelFunctions.FACT(1m));
            Assert.AreEqual(2m, ExcelFunctions.FACT(2m));
            Assert.AreEqual(6m, ExcelFunctions.FACT(3m));
            Assert.AreEqual(120m, ExcelFunctions.FACT(5m));
            Assert.AreEqual(720m, ExcelFunctions.FACT(6m));
            Assert.AreEqual(1m, ExcelFunctions.FACT(0m));
            Assert.AreEqual(1m, ExcelFunctions.FACT(1.1m));
            Assert.AreEqual(1m, ExcelFunctions.FACT(1.9m));
            Assert.AreEqual(2m, ExcelFunctions.FACT(2.1m));
            Assert.AreEqual(2m, ExcelFunctions.FACT(2.9999m));
            Assert.AreEqual(6m, ExcelFunctions.FACT(3.1m));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => ExcelFunctions.FACT(-1m));

            // FLOOR
            Assert.AreEqual(null, ExcelFunctions.FLOOR(null, null));
            Assert.AreEqual(2m, ExcelFunctions.FLOOR(3.7m, 2m));
            Assert.AreEqual(-2m, ExcelFunctions.FLOOR(-2.5m, -2m));
            Assert.AreEqual(1.5m, ExcelFunctions.FLOOR(1.58m, 0.1m));
            Assert.AreEqual(0.23m, ExcelFunctions.FLOOR(0.234m, 0.01m));
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => ExcelFunctions.FLOOR(2.5m, -2m));

            // INT
            Assert.AreEqual(null, ExcelFunctions.INT(null));
            Assert.AreEqual(1m, ExcelFunctions.INT(1.5m));
            Assert.AreEqual(-5m, ExcelFunctions.INT(-4.3m));
            Assert.AreEqual(-9m, ExcelFunctions.INT(-8.9m));
            Assert.AreEqual(50m, ExcelFunctions.INT(50.01m));
            Assert.AreEqual(50m, ExcelFunctions.INT(50.99m));
            Assert.AreEqual(-51m, ExcelFunctions.INT(-50.01m));
            Assert.AreEqual(-51m, ExcelFunctions.INT(-50.99m));

            // ISEVEN
            Assert.AreEqual(false, ExcelFunctions.ISEVEN(null));
            Assert.AreEqual(false, ExcelFunctions.ISEVEN(1m));
            Assert.AreEqual(true, ExcelFunctions.ISEVEN(2m));
            Assert.AreEqual(false, ExcelFunctions.ISEVEN(-1m));
            Assert.AreEqual(true, ExcelFunctions.ISEVEN(-2m));
            Assert.AreEqual(false, ExcelFunctions.ISEVEN(1.5m));
            Assert.AreEqual(true, ExcelFunctions.ISEVEN(2.5m));
            Assert.AreEqual(false, ExcelFunctions.ISEVEN(-1.5m));
            Assert.AreEqual(true, ExcelFunctions.ISEVEN(-2.5m));


            // LN
            Assert.AreEqual(0.69m, Math.Round(ExcelFunctions.LN(2).Value, 2));
            Assert.AreEqual(1.10m, Math.Round(ExcelFunctions.LN(3).Value, 2));
            // LOG
            Assert.AreEqual(4m, Math.Round(ExcelFunctions.LOG(16, 2).Value, 2));
            Assert.AreEqual(4.45m, Math.Round(ExcelFunctions.LOG(86, 2.7182818m).Value, 2));
            // LOG10
            Assert.AreEqual(1m, Math.Round(ExcelFunctions.LOG10(10).Value, 2));
            Assert.AreEqual(1.93m, Math.Round(ExcelFunctions.LOG10(86).Value, 2));
            // MAX
            decimal?[] numArray = new decimal?[11] { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };
            Assert.AreEqual(5, ExcelFunctions.MAX(numArray));
            // MIN
            Assert.AreEqual(-5, ExcelFunctions.MIN(numArray));
            // MOD
            Assert.AreEqual(1, ExcelFunctions.MOD(3, 2));
            Assert.AreEqual(1.86m, Math.Round(ExcelFunctions.MOD(15.86m, 2).Value, 2));
            Assert.IsNull(ExcelFunctions.MOD(4, 0));
            // ODD
            Assert.AreEqual(1, ExcelFunctions.ODD(0));
            Assert.AreEqual(7, ExcelFunctions.ODD(7));
            Assert.AreEqual(7, ExcelFunctions.ODD(6));
            Assert.AreEqual(-1, ExcelFunctions.ODD(-1));
            Assert.AreEqual(-3, ExcelFunctions.ODD(-2));

            //PI
            Assert.AreEqual((decimal)Math.PI, ExcelFunctions.PI());

            //RAND
            Assert.IsTrue(ExcelFunctions.RAND() < 1);

            //ROUND
            Assert.AreEqual(0m, ExcelFunctions.ROUND(123.456m, -4m));
            Assert.AreEqual(0m, ExcelFunctions.ROUND(123.456m, -3m));
            Assert.AreEqual(100m, ExcelFunctions.ROUND(123.456m, -2m));
            Assert.AreEqual(120m, ExcelFunctions.ROUND(123.456m, -1m));
            Assert.AreEqual(123m, ExcelFunctions.ROUND(123.456m, 0m));
            Assert.AreEqual(123.5m, ExcelFunctions.ROUND(123.456m, 1m));
            Assert.AreEqual(123.46m, ExcelFunctions.ROUND(123.456m, 2m));
            Assert.AreEqual(123.456m, ExcelFunctions.ROUND(123.456m, 3m));
            Assert.AreEqual(123.456m, ExcelFunctions.ROUND(123.456m, 4m));
            Assert.AreEqual(0m, ExcelFunctions.ROUND(56.78m, -4m));
            Assert.AreEqual(0m, ExcelFunctions.ROUND(56.78m, -3m));
            Assert.AreEqual(100m, ExcelFunctions.ROUND(56.78m, -2m));
            Assert.AreEqual(60m, ExcelFunctions.ROUND(56.78m, -1m));
            Assert.AreEqual(57m, ExcelFunctions.ROUND(56.78m, 0m));
            Assert.AreEqual(56.8m, ExcelFunctions.ROUND(56.78m, 1m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUND(56.78m, 2m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUND(56.78m, 3m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUND(56.78m, 4m));

            Assert.AreEqual(10000m, ExcelFunctions.ROUNDUP(123.456m, -4m));
            Assert.AreEqual(1000m, ExcelFunctions.ROUNDUP(123.456m, -3m));
            Assert.AreEqual(200m, ExcelFunctions.ROUNDUP(123.456m, -2m));
            Assert.AreEqual(130m, ExcelFunctions.ROUNDUP(123.456m, -1m));
            Assert.AreEqual(124m, ExcelFunctions.ROUNDUP(123.456m, 0m));
            Assert.AreEqual(123.5m, ExcelFunctions.ROUNDUP(123.456m, 1m));
            Assert.AreEqual(123.46m, ExcelFunctions.ROUNDUP(123.456m, 2m));
            Assert.AreEqual(123.456m, ExcelFunctions.ROUNDUP(123.456m, 3m));
            Assert.AreEqual(123.456m, ExcelFunctions.ROUNDUP(123.456m, 4m));
            Assert.AreEqual(10000m, ExcelFunctions.ROUNDUP(56.78m, -4m));
            Assert.AreEqual(1000m, ExcelFunctions.ROUNDUP(56.78m, -3m));
            Assert.AreEqual(100m, ExcelFunctions.ROUNDUP(56.78m, -2m));
            Assert.AreEqual(60m, ExcelFunctions.ROUNDUP(56.78m, -1m));
            Assert.AreEqual(57m, ExcelFunctions.ROUNDUP(56.78m, 0m));
            Assert.AreEqual(56.8m, ExcelFunctions.ROUNDUP(56.78m, 1m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUNDUP(56.78m, 2m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUNDUP(56.78m, 3m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUNDUP(56.78m, 4m));

            Assert.AreEqual(0m, ExcelFunctions.ROUNDDOWN(123.456m, -4m));
            Assert.AreEqual(0m, ExcelFunctions.ROUNDDOWN(123.456m, -3m));
            Assert.AreEqual(100m, ExcelFunctions.ROUNDDOWN(123.456m, -2m));
            Assert.AreEqual(120m, ExcelFunctions.ROUNDDOWN(123.456m, -1m));
            Assert.AreEqual(123m, ExcelFunctions.ROUNDDOWN(123.456m, 0m));
            Assert.AreEqual(123.4m, ExcelFunctions.ROUNDDOWN(123.456m, 1m));
            Assert.AreEqual(123.45m, ExcelFunctions.ROUNDDOWN(123.456m, 2m));
            Assert.AreEqual(123.456m, ExcelFunctions.ROUNDDOWN(123.456m, 3m));
            Assert.AreEqual(123.456m, ExcelFunctions.ROUNDDOWN(123.456m, 4m));
            Assert.AreEqual(0m, ExcelFunctions.ROUNDDOWN(56.78m, -4m));
            Assert.AreEqual(0m, ExcelFunctions.ROUNDDOWN(56.78m, -3m));
            Assert.AreEqual(0m, ExcelFunctions.ROUNDDOWN(56.78m, -2m));
            Assert.AreEqual(50m, ExcelFunctions.ROUNDDOWN(56.78m, -1m));
            Assert.AreEqual(56m, ExcelFunctions.ROUNDDOWN(56.78m, 0m));
            Assert.AreEqual(56.7m, ExcelFunctions.ROUNDDOWN(56.78m, 1m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUNDDOWN(56.78m, 2m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUNDDOWN(56.78m, 3m));
            Assert.AreEqual(56.78m, ExcelFunctions.ROUNDDOWN(56.78m, 4m));

            //SIGN
            Assert.AreEqual(1, ExcelFunctions.SIGN(10.5m));
            Assert.AreEqual(-1, ExcelFunctions.SIGN(-10.5m));

            //SIN
            Assert.AreEqual(-0.54402111088937m, ExcelFunctions.SIN(10));

            //SQRT
            Assert.AreEqual(4, ExcelFunctions.SQRT(16));

            //SUM
            decimal?[] sumArray = new decimal?[5] { 1, 2, 3, 4, 5 };
            Assert.AreEqual(15, ExcelFunctions.SUM(sumArray));

            //TAN
            Assert.AreEqual(0.648360827459087m, ExcelFunctions.TAN(10));

            //TRUNC
            Assert.AreEqual(15.12m, ExcelFunctions.TRUNC(15.123456m, 2));
        }

        [TestMethod]
        public void ExcelBitFunctionsTests()
        {
            //BITAND
            Assert.AreEqual(null, ExcelFunctions.BITAND(null, null));
            Assert.AreEqual(0m, ExcelFunctions.BITAND(7m, 0m));
            Assert.AreEqual(5m, ExcelFunctions.BITAND(7m, 13m));

            //BITOR
            Assert.AreEqual(null, ExcelFunctions.BITOR(null, null));
            Assert.AreEqual(7m, ExcelFunctions.BITOR(7m, 0m));
            Assert.AreEqual(15m, ExcelFunctions.BITOR(7m, 13m));

            //BITXOR
            Assert.AreEqual(null, ExcelFunctions.BITXOR(null, null));
            Assert.AreEqual(7m, ExcelFunctions.BITXOR(7m, 0m));
            Assert.AreEqual(10m, ExcelFunctions.BITXOR(7m, 13m));
        }

        [TestMethod]
        public void ExcelBooleanLogicFunctionsTests()
        {
            // AND
            Assert.AreEqual(false, ExcelFunctions.AND(null));
            Assert.AreEqual(true, ExcelFunctions.AND(true, true));
            Assert.AreEqual(false, ExcelFunctions.AND(true, null));
            Assert.AreEqual(false, ExcelFunctions.AND(false));
            Assert.AreEqual(false, ExcelFunctions.AND(false, true));

            // NOT
            Assert.AreEqual(true, ExcelFunctions.NOT(false));
            Assert.AreEqual(false, ExcelFunctions.NOT(true));
            Assert.AreEqual(null, ExcelFunctions.NOT(null));
            Assert.AreEqual(true, ExcelFunctions.NOT(5 > 10));
            Assert.AreEqual(false, ExcelFunctions.NOT(5 < 10));

            // OR
            Assert.AreEqual(true, ExcelFunctions.OR(true, false));
            Assert.AreEqual(false, ExcelFunctions.OR(false, false));
            Assert.AreEqual(true, ExcelFunctions.OR(false, true));
            Assert.AreEqual(true, ExcelFunctions.OR(true, false, false));

            // FALSE
            Assert.AreEqual(false, ExcelFunctions.FALSE());

            // IF
            Assert.AreEqual(0, ExcelFunctions.IF(null, 1, 0));
            Assert.AreEqual(1, ExcelFunctions.IF(true, 1, 0));
            Assert.AreEqual(0, ExcelFunctions.IF(false, 1, 0));
            Assert.AreEqual(1, ExcelFunctions.IF(true, 1, null));
            Assert.AreEqual(0, ExcelFunctions.IF(false, null, 0));
            Assert.AreEqual(null, ExcelFunctions.IF(true, null, 0));
            Assert.AreEqual(null, ExcelFunctions.IF(false, 1, null));

            Assert.AreEqual("false", ExcelFunctions.IF(null, "true", "false"));
            Assert.AreEqual("true", ExcelFunctions.IF(true, "true", "false"));
            Assert.AreEqual("false", ExcelFunctions.IF(false, "true", "false"));

            Assert.AreEqual(false, ExcelFunctions.IF(null, true, false));
            Assert.AreEqual(true, ExcelFunctions.IF(true, true, false));
            Assert.AreEqual(false, ExcelFunctions.IF(false, true, false));

            //XOR
            bool?[] boolArray = new bool?[4] { true, true, true, false };
            Assert.AreEqual(true, ExcelFunctions.XOR(boolArray));
        }

        [TestMethod]
        public void ExcelDateFunctionsTests()
        {
            // DATE
            Assert.AreEqual(null, ExcelFunctions.DATE(null, null, null));
            Assert.AreEqual(new DateTime(2020, 12, 1), ExcelFunctions.DATE(2020m, 12m, 1m));
            Assert.AreEqual(new DateTime(1899, 1, 1), ExcelFunctions.DATE(1899m, 1m, 1m));
            Assert.ThrowsException<DateConversionException>(() => ExcelFunctions.DATE(2000m, 14m, 1m));
            Assert.ThrowsException<DateConversionException>(() => ExcelFunctions.DATE(2000m, 1m, 35m));

            // DATEDIF
            Assert.AreEqual(null, ExcelFunctions.DATEDIF((DateTime?)null, (DateTime?)null, null));
            Assert.AreEqual(null, ExcelFunctions.DATEDIF(new DateTime(2000, 1, 1), (DateTime?)null, null));
            Assert.AreEqual(null, ExcelFunctions.DATEDIF((DateTime?)null, new DateTime(2000, 1, 1), null));
            Assert.AreEqual(366m, ExcelFunctions.DATEDIF(new DateTime(2000, 1, 1), new DateTime(2001, 1, 1), "D"));
            Assert.AreEqual(12m, ExcelFunctions.DATEDIF(new DateTime(2000, 1, 1), new DateTime(2001, 1, 1), "M"));
            Assert.AreEqual(1m, ExcelFunctions.DATEDIF(new DateTime(2000, 1, 1), new DateTime(2001, 1, 1), "Y"));
            Assert.AreEqual(9m, ExcelFunctions.DATEDIF(new DateTime(2000, 1, 1), new DateTime(2001, 2, 10), "MD"));
            Assert.AreEqual(40m, ExcelFunctions.DATEDIF(new DateTime(2000, 1, 1), new DateTime(2001, 2, 10), "YD"));
            Assert.AreEqual(1m, ExcelFunctions.DATEDIF(new DateTime(2000, 1, 1), new DateTime(2001, 2, 10), "YM"));
            Assert.ThrowsException<DateComparisonException>(() => ExcelFunctions.DATEDIF(new DateTime(2005, 1, 1), new DateTime(2001, 2, 10), "D"));
            Assert.ThrowsException<DateComparisonException>(() => ExcelFunctions.DATEDIF(new DateTime(2000, 1, 1), new DateTime(2001, 2, 10), "MY"));

            Assert.AreEqual(366m, ExcelFunctions.DATEDIF("1-1-2000", "1-1-2001", "D"));
            Assert.AreEqual(12m, ExcelFunctions.DATEDIF("1-1-2000", "1-1-2001", "M"));
            Assert.AreEqual(1m, ExcelFunctions.DATEDIF("1-1-2000", "1-1-2001", "Y"));
            Assert.AreEqual(9m, ExcelFunctions.DATEDIF("1-1-2000", "2-10-2001", "MD"));
            Assert.AreEqual(40m, ExcelFunctions.DATEDIF("1-1-2000", "2-10-2001", "YD"));
            Assert.AreEqual(1m, ExcelFunctions.DATEDIF("1-1-2000", "2-10-2001", "YM"));


            // DATEVALUE
            Assert.AreEqual(null, ExcelFunctions.DATEVALUE((DateTime?)null));
            Assert.AreEqual(0m, ExcelFunctions.DATEVALUE(new DateTime(1899, 12, 30)));
            Assert.AreEqual(2m, ExcelFunctions.DATEVALUE(new DateTime(1900, 1, 1)));
            Assert.AreEqual(37803m, ExcelFunctions.DATEVALUE(new DateTime(2003, 7, 1)));

            // DAY
            Assert.AreEqual(null, ExcelFunctions.DAY((DateTime?)null));
            Assert.AreEqual(1m, ExcelFunctions.DAY(new DateTime(2000, 1, 1)));
            Assert.AreEqual(31m, ExcelFunctions.DAY(new DateTime(2000, 1, 31)));

            // DAYS
            Assert.AreEqual(null, ExcelFunctions.DAYS(null, null));
            Assert.AreEqual(366m, ExcelFunctions.DAYS(new DateTime(2001, 1, 1), new DateTime(2000, 1, 1)));

            // MONTH
            Assert.AreEqual(1m, ExcelFunctions.MONTH(new DateTime(2000, 1, 1)));
            Assert.AreEqual(1m, ExcelFunctions.MONTH("01-01-2000"));
            Assert.AreEqual(1m, ExcelFunctions.MONTH(0));
            Assert.AreEqual(4m, ExcelFunctions.MONTH(38094));

            // NOW
            Assert.AreEqual(DateTime.UtcNow.ToShortDateString(), ExcelFunctions.NOW().Value.ToShortDateString());

            // EOMONTH 
            Assert.AreEqual(null, ExcelFunctions.EOMONTH(null));
            Assert.AreEqual(new DateTime(2000, 1, 31), ExcelFunctions.EOMONTH(new DateTime(2000, 1, 12)));
            Assert.AreEqual(new DateTime(2000, 2, 29), ExcelFunctions.EOMONTH(new DateTime(2000, 1, 12), 1));
            Assert.AreEqual(new DateTime(1999, 12, 31), ExcelFunctions.EOMONTH(new DateTime(2000, 1, 12), -1));

            //SECOND
            Assert.AreEqual(15, ExcelFunctions.SECOND("11:30:15"));

            //TIME
            DateTime dtTest = DateTime.Parse("11:30:15");
            Assert.AreEqual(dtTest.TimeOfDay, ExcelFunctions.TIME(11, 30, 15).Value.TimeOfDay);

            //TIMEVALUE
            Assert.AreEqual((decimal)dtTest.TimeOfDay.TotalDays, ExcelFunctions.TIMEVALUE("11:30:15"));

            //TODAY
            var today = DateTime.UtcNow;
            Assert.AreEqual(today.Date, ExcelFunctions.TODAY());
            Assert.AreEqual(today.Date.Day, ExcelFunctions.DAY(ExcelFunctions.TODAY()));
            Assert.AreEqual(today.Date.Month, ExcelFunctions.MONTH(ExcelFunctions.TODAY()));
            Assert.AreEqual(today.Date.Year, ExcelFunctions.YEAR(ExcelFunctions.TODAY()));

            //WEEKDAY
            var sunDate = new DateTime(2018, 10, 21);
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(sunDate));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(sunDate, 1));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(sunDate, 2));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(sunDate, 3));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(sunDate, 11));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(sunDate, 12));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(sunDate, 13));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(sunDate, 14));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(sunDate, 15));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(sunDate, 16));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(sunDate, 17));

            var monDate = new DateTime(2018, 10, 22);
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(monDate));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(monDate, 1));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(monDate, 2));
            Assert.AreEqual(0, ExcelFunctions.WEEKDAY(monDate, 3));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(monDate, 11));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(monDate, 12));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(monDate, 13));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(monDate, 14));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(monDate, 15));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(monDate, 16));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(monDate, 17));

            var tueDate = new DateTime(2018, 10, 23);
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(tueDate));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(tueDate, 1));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(tueDate, 2));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(tueDate, 3));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(tueDate, 11));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(tueDate, 12));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(tueDate, 13));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(tueDate, 14));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(tueDate, 15));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(tueDate, 16));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(tueDate, 17));

            var wedDate = new DateTime(2018, 10, 24);
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(wedDate));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(wedDate, 1));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(wedDate, 2));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(wedDate, 3));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(wedDate, 11));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(wedDate, 12));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(wedDate, 13));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(wedDate, 14));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(wedDate, 15));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(wedDate, 16));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(wedDate, 17));

            var thuDate = new DateTime(2018, 10, 25);
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(thuDate));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(thuDate, 1));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(thuDate, 2));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(thuDate, 3));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(thuDate, 11));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(thuDate, 12));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(thuDate, 13));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(thuDate, 14));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(thuDate, 15));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(thuDate, 16));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(thuDate, 17));

            var friDate = new DateTime(2018, 10, 26);
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(friDate));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(friDate, 1));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(friDate, 2));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(friDate, 3));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(friDate, 11));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(friDate, 12));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(friDate, 13));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(friDate, 14));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(friDate, 15));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(friDate, 16));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(friDate, 17));

            var satDate = new DateTime(2018, 10, 27);
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(satDate));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(satDate, 1));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(satDate, 2));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(satDate, 3));
            Assert.AreEqual(6, ExcelFunctions.WEEKDAY(satDate, 11));
            Assert.AreEqual(5, ExcelFunctions.WEEKDAY(satDate, 12));
            Assert.AreEqual(4, ExcelFunctions.WEEKDAY(satDate, 13));
            Assert.AreEqual(3, ExcelFunctions.WEEKDAY(satDate, 14));
            Assert.AreEqual(2, ExcelFunctions.WEEKDAY(satDate, 15));
            Assert.AreEqual(1, ExcelFunctions.WEEKDAY(satDate, 16));
            Assert.AreEqual(7, ExcelFunctions.WEEKDAY(satDate, 17));

            //WEEKNUM
            dtTest = DateTime.Parse("10/24/2009", CultureInfo.CreateSpecificCulture("en-US")); // I added to specify the culture since it won't run on PH systems because of the culture
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(dtTest));
            Assert.AreEqual(16, ExcelFunctions.WEEKNUM(38093m));

            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(sunDate));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(sunDate, 1));
            Assert.AreEqual(42, ExcelFunctions.WEEKNUM(sunDate, 2));
            Assert.AreEqual(42, ExcelFunctions.WEEKNUM(sunDate, 11));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(sunDate, 12));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(sunDate, 13));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(sunDate, 14));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(sunDate, 15));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(sunDate, 16));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(sunDate, 17));
            Assert.AreEqual(42, ExcelFunctions.WEEKNUM(sunDate, 21));

            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 1));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 2));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 11));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 12));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 13));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 14));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 15));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 16));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 17));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(monDate, 21));

            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 1));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 2));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 11));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(tueDate, 12));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 13));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 14));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 15));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 16));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 17));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(tueDate, 21));

            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate, 1));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate, 2));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate, 11));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(wedDate, 12));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(wedDate, 13));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate, 14));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate, 15));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate, 16));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate, 17));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(wedDate, 21));

            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(thuDate));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(thuDate, 1));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(thuDate, 2));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(thuDate, 11));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(thuDate, 12));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(thuDate, 13));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(thuDate, 14));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(thuDate, 15));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(thuDate, 16));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(thuDate, 17));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(thuDate, 21));

            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(friDate));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(friDate, 1));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(friDate, 2));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(friDate, 11));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(friDate, 12));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(friDate, 13));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(friDate, 14));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(friDate, 15));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(friDate, 16));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(friDate, 17));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(friDate, 21));

            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(satDate));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(satDate, 1));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(satDate, 2));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(satDate, 11));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(satDate, 12));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(satDate, 13));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(satDate, 14));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(satDate, 15));
            Assert.AreEqual(44, ExcelFunctions.WEEKNUM(satDate, 16));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(satDate, 17));
            Assert.AreEqual(43, ExcelFunctions.WEEKNUM(satDate, 21));

            //YEAR
            Assert.AreEqual(DateTime.UtcNow.Year, ExcelFunctions.YEAR(DateTime.UtcNow));
            Assert.AreEqual(1900, ExcelFunctions.YEAR(0));
        }

        #endregion

        [TestMethod]
        public void TestComponentPriceRequestToComputeRequest()
        {
            var variables = new Dictionary<string, VariableValue>()
            {
                { "string",  new VariableValue() { Value = "test" } },
                { "anotherString",  new VariableValue() { Value = "anotherString", ValueOV = true } },
                { "decimal",  new VariableValue() { Value = "1.5", ValueOV = true } },
            };
            ComponentPriceRequest request = new ComponentPriceRequest()
            {
                CompanyID = 1,
                ComponentType = OrderItemComponentType.Material,
                ComponentID = 1,
                TotalQuantity = 2,
                PriceUnit = 3.5m,
                Variables = variables,
            };

            CBELOverriddenValues computeRequest = CBELAssemblyHelper.ComponentPriceRequestToComputeRequest(request);
            // +1 added to the variables count due to the addition of the Quantity Variables
            Assert.AreEqual(variables.Count + 2, computeRequest.VariableData.Count);
            computeRequest.VariableData.ForEach(i => Assert.IsNotNull(i));
        }

        [TestMethod]
        public async Task TestComputePriceResultToComponentPriceResult()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements(DescriptionRequired: true);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);

            CBELComputeResult computeResult = new CBELComputeResult(assembly);

            Assert.IsNotNull(computeResult?.VariableData);
            ComponentPriceResult result = new ComponentPriceResult();
            ComponentPriceResult componentPriceResult = CBELAssemblyHelper.ComputePriceResultToComponentPriceResult(result, computeResult);
            Assert.AreEqual(OrderItemComponentType.Assembly, componentPriceResult.ComponentType);
            Assert.AreEqual(computeResult.VariableData.Count, componentPriceResult.Variables.Count);
            Assert.AreEqual(componentPriceResult.TotalQuantity, computeResult.Assembly.QuantityValue);
            Assert.AreEqual(componentPriceResult.PricePreTax, computeResult.Assembly.TotalPrice);
            Assert.AreEqual(componentPriceResult.CostLabor, computeResult.Assembly.LaborCost);
            Assert.AreEqual(componentPriceResult.CostMachine, computeResult.Assembly.MachineCost);
            Assert.AreEqual(componentPriceResult.CostMaterial, computeResult.Assembly.MaterialCost);
            Assert.AreEqual(componentPriceResult.CostNet, computeResult.Assembly.TotalCost);

        }

        [TestMethod]
        public async Task TestCBELLinkedMaterialsAddedToAssembly()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELVariableGenerator myLinkedMachine = new CBELVariableGenerator(gen)
            {
                Name = "LinkedMaterial",
                VariableClassName = "LinkedMaterialVariable",
                Label = "MaterialID",
                VariableDataType = DataType.String,
                FormulaText = "Material1",
                ConsumptionFormulaText = "= Area",
                DefaultConsumptionValueFunction = "(Area.Value)",
            };
            myLinkedMachine.ReliesOn.Add("Area");
            gen.AddVariable(myLinkedMachine);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            ICBELVariable assemblyElement = assembly.Variables["LinkedMaterial"];
            Assert.IsInstanceOfType(assemblyElement, typeof(LinkedMaterialVariable));
            Assert.AreEqual(myLinkedMachine.Name, assemblyElement.Name);
        }

        [TestMethod]
        public async Task TestAssemblyGeneratorShouldHandleIsFormulaAndNullDefaultValue()
        {
            short bid = 1;
            int id = -214;

            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            string descriptionVarName = AssemblyElementType.Number.ToString();
            await CleanupPreviousUsage(bid, id, apiContext, cache);

            apiContext = PricingTestHelper.GetMockCtx(bid);
            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"AssemblyWithRelatedVariables" };
            var test1Var = new AssemblyVariable()
            {
                Name = "test1Var",
                ElementType = AssemblyElementType.Number,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = null,
                DataType = DataType.Number,
                IsFormula = true,
            };

            apiContext.AssemblyVariable.Add(test1Var);

            var test2Var = new AssemblyVariable()
            {
                Name = "test2Var",
                ElementType = AssemblyElementType.Number,
                ID = id - 1,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "",
                DataType = DataType.Number,
                IsFormula = true,
            };

            apiContext.AssemblyVariable.Add(test2Var);

            var test3Var = new AssemblyVariable()
            {
                Name = "test3Var",
                ElementType = AssemblyElementType.Number,
                ID = id - 2,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "5",
                DataType = DataType.Number,
                IsFormula = true,
            };

            apiContext.AssemblyVariable.Add(test3Var);

            var test4Var = new AssemblyVariable()
            {
                Name = "test4Var",
                ElementType = AssemblyElementType.SingleLineText,
                ID = id - 3,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "\"Test\"",
                DataType = DataType.String,
                IsFormula = true,
            };

            apiContext.AssemblyVariable.Add(test4Var);

            apiContext.AssemblyData.Add(assemblyData);

            apiContext.SaveChanges();

            try
            {

                var gen = CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID());
                ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
                Assert.IsNotNull(instance.Variables.FirstOrDefault(t => t.Key == descriptionVarName));
                var test1VarReliesOn = ((NumberEditElement)instance.Variables[nameof(test1Var)]).ReliesOn;
                Assert.AreEqual(0, test1VarReliesOn.Count);

                Assert.IsNotNull(instance.Compute());

            }
            finally
            {
                //Clean everything up so it doesn't interfere with other tests using the same IDs
                await CleanupPreviousUsage(bid, id, apiContext, cache);
                await CleanupPreviousUsage(bid, id - 1, apiContext, cache);
                await CleanupPreviousUsage(bid, id - 2, apiContext, cache);
            }
        }

        [TestMethod]
        public async Task AssemblyWithNumberDataTableCompiles()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELTableGenerator myCustomTable = new CBELTableGenerator(gen)
            {
                Name = "ShirtPricing",
                VariableClassName = "NumberDataTable<decimal?,string>",
                Label = "ShirtPricing",
                VariableDataType = DataType.Number,
                RowDataType = "decimal?",
                ColumnDataType = "string",
                RowVariableName = "AssemblyQuantity",
                ColumnVariableName = "Height",
                RowHeadersJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":10},{\"index\":0,\"Value\":50.5}]",
                ColumnHeadersJSON = "[{\"index\":0,\"Value\":\"small\"},{\"index\":0,\"Value\":\"medium\"},{\"index\":0,\"Value\":\"large\"}]",
                CellValuesJSON = "[[45,4,3],[90,2,],[,9,]]",
            };
            myCustomTable.ReliesOn.Add("ShirtSize");
            myCustomTable.ReliesOn.Add("AssemblyQuantity");
            myCustomTable.ReferencedBy.Add("AssemblyPrice");
            gen.AddVariable(myCustomTable);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);
            ICBELVariable assemblyElement = assembly.Variables["ShirtPricing"];
            Assert.IsInstanceOfType(assemblyElement, typeof(NumberDataTable<decimal?, string>));
            Assert.AreEqual(myCustomTable.Name, assemblyElement.Name);
            Assert.AreEqual(myCustomTable.ReferencedBy.Count(), 1);
            Assert.AreEqual(myCustomTable.ReliesOn.Count(), 2);
        }

        [TestMethod]
        public async Task AssemblyWithStringDataTableCompiles()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELTableGenerator myCustomTable = new CBELTableGenerator(gen)
            {
                Name = "ShirtPricing",
                VariableClassName = "StringDataTable<decimal?,string>",
                Label = "ShirtPricing",
                VariableDataType = DataType.String,
                RowDataType = "decimal?",
                ColumnDataType = "string",
                RowVariableName = "AssemblyQuantity",
                ColumnVariableName = "Height",
                RowHeadersJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":10},{\"index\":0,\"Value\":50.5}]",
                ColumnHeadersJSON = "[{\"index\":0,\"Value\":\"small\"},{\"index\":0,\"Value\":\"medium\"},{\"index\":0,\"Value\":\"large\"}]",
                CellValuesJSON = "[[\"free\",\"overcharge\",\"retail\"],[\"retail\",\"free\",],[,\"retail\",\"free\"]]",
            };
            myCustomTable.ReliesOn.Add("ShirtSize");
            myCustomTable.ReliesOn.Add("AssemblyQuantity");
            myCustomTable.ReferencedBy.Add("AssemblyPrice");
            gen.AddVariable(myCustomTable);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);
            ICBELVariable assemblyElement = assembly.Variables["ShirtPricing"];
            Assert.IsInstanceOfType(assemblyElement, typeof(StringDataTable<decimal?, string>));
            Assert.AreEqual(myCustomTable.Name, assemblyElement.Name);
            Assert.AreEqual(myCustomTable.ReferencedBy.Count(), 1);
            Assert.AreEqual(myCustomTable.ReliesOn.Count(), 2);
        }

        [TestMethod]
        public async Task AssemblyWithBooleanDataTableCompiles()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            CBELTableGenerator myCustomTable = new CBELTableGenerator(gen)
            {
                Name = "ShirtPricing",
                VariableClassName = "BooleanDataTable<decimal?,string>",
                Label = "ShirtPricing",
                VariableDataType = DataType.Boolean,
                RowDataType = "decimal?",
                ColumnDataType = "string",
                RowVariableName = "AssemblyQuantity",
                ColumnVariableName = "Height",
                RowHeadersJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":10},{\"index\":0,\"Value\":50.5}]",
                ColumnHeadersJSON = "[{\"index\":0,\"Value\":\"small\"},{\"index\":0,\"Value\":\"medium\"},{\"index\":0,\"Value\":\"large\"}]",
                CellValuesJSON = "[[true,false,],[,true,false],[false,,true]]",
            };
            myCustomTable.ReliesOn.Add("ShirtSize");
            myCustomTable.ReliesOn.Add("AssemblyQuantity");
            myCustomTable.ReferencedBy.Add("AssemblyPrice");
            gen.AddVariable(myCustomTable);

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);
            ICBELVariable assemblyElement = assembly.Variables["ShirtPricing"];
            Assert.IsInstanceOfType(assemblyElement, typeof(BooleanDataTable<decimal?, string>));
            Assert.AreEqual(myCustomTable.Name, assemblyElement.Name);
            Assert.AreEqual(myCustomTable.ReferencedBy.Count(), 1);
            Assert.AreEqual(myCustomTable.ReliesOn.Count(), 2);
        }

        [TestMethod]
        public async Task AddHelperTableGeneratorTest()
        {
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();
            short bid = 1;
            int id = -100;
            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            await CleanupPreviousUsage(bid, id, apiContext, cache);
            await CleanupPreviousUsage(bid, id - 1, apiContext, cache);
            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"AssemblyWithRelatedVariables" };
            apiContext.AssemblyData.Add(assemblyData);
            apiContext.SaveChanges();

            var test1Var = new AssemblyVariable()
            {
                Name = "test1Var",
                ElementType = AssemblyElementType.Number,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "= test2Var.Value + test3Var.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };
            var test2Var = new AssemblyVariable()
            {
                Name = "test2Var",
                ElementType = AssemblyElementType.Number,
                ID = id - 1,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "10",
                DataType = DataType.Number,
                IsFormula = false,
            };
            apiContext.AssemblyVariable.Add(test1Var);
            apiContext.AssemblyVariable.Add(test2Var);
            apiContext.SaveChanges();

            gen.AddElementGenerator<NumberEditElement>(test1Var, TestParser);
            gen.AddElementGenerator<NumberEditElement>(test2Var, TestParser);

            string variableName = "ShirtPricing";
            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                VariableName = variableName,
                CellDataType = DataType.Number,
                Label = variableName,
                RowDataType = DataType.Number,
                ColumnDataType = DataType.String,
                RowVariableID = test1Var.ID,
                ColumnVariableID = test2Var.ID,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":10},{\"index\":0,\"Value\":50}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":\"small\"},{\"index\":0,\"Value\":\"medium\"},{\"index\":0,\"Value\":\"large\"}]",
                CellDataJSON = "[[45,4,3],[90,,2],[,9,]]"
            };
            try
            {
                gen.AddTableGenerator(new List<AssemblyVariable>() { test1Var, test2Var }, table);
                Assert.AreEqual(gen.Variables.Where(var => var.Name == variableName).Count(), 1);
                Assert.AreEqual(gen.Variables.Where(var => var.Name == variableName).First().ReliesOn.Count(), 2);
            }
            finally
            {
                //do cleanup
                await CleanupPreviousUsage(bid, id, apiContext, cache);
                await CleanupPreviousUsage(bid, id - 1, apiContext, cache);
            }

        }

        [TestMethod]
        public async Task AddTableToAssemblyGenerationTest()
        {
            short bid = 1;
            int id = -100;
            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            await CleanupPreviousUsage(bid, id, apiContext, cache);
            await CleanupPreviousUsage(bid, id - 1, apiContext, cache);

            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"AssemblyWithRelatedVariables" };
            apiContext.AssemblyData.Add(assemblyData);
            apiContext.SaveChanges();

            var test1Var = new AssemblyVariable()
            {
                Name = "test1Var",
                ElementType = AssemblyElementType.Number,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "= test2Var.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };
            var test2Var = new AssemblyVariable()
            {
                Name = "test2Var",
                ElementType = AssemblyElementType.Number,
                ID = id - 1,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "10",
                DataType = DataType.Number,
                IsFormula = false,
            };
            apiContext.AssemblyVariable.Add(test1Var);
            apiContext.AssemblyVariable.Add(test2Var);
            apiContext.SaveChanges();

            string variableName = "ShirtPricing";
            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                ID = (short)id,
                VariableName = variableName,
                CellDataType = DataType.Number,
                Label = variableName,
                RowDataType = DataType.Number,
                ColumnDataType = DataType.String,
                RowVariableID = test1Var.ID,
                ColumnVariableID = test2Var.ID,
                AssemblyID = id,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":10},{\"index\":0,\"Value\":50}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":\"small\"},{\"index\":0,\"Value\":\"medium\"},{\"index\":0,\"Value\":\"large\"}]",
                CellDataJSON = "[[45,4,3],[90,,2],[,9,]]"
            };
            apiContext.AssemblyTable.Add(table);
            apiContext.SaveChanges();

            try
            {
                var gen = CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID());
                ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
                Assert.IsNotNull(instance);
                Assert.IsNotNull(instance.Variables.FirstOrDefault(t => t.Key == variableName));
                var test1VarReliesOn = ((NumberDataTable<decimal?, string>)instance.Variables[variableName]).ReliesOn;
                Assert.AreEqual(2, test1VarReliesOn.Count); ;
            }
            finally
            {
                //Clean everything up so it doesn't interfere with other tests using the same IDs
                await CleanupPreviousUsage(bid, id, apiContext, cache);
                await CleanupPreviousUsage(bid, id - 1, apiContext, cache);
            }

        }

        [TestMethod]
        public async Task ElementNameHasADefaultValueTypeGetterInCompiledAssembly()
        {
            short bid = 1;
            int assemblyID = -218;
            int heightVarID = -218;
            int areaVarID = -219;

            DataType dataType = DataType.Number;
            AssemblyElementType elementType = AssemblyElementType.Number;

            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            await CleanupPreviousUsage(bid, assemblyID, apiContext, cache);

            apiContext = PricingTestHelper.GetMockCtx(bid);
            var assemblyData = new AssemblyData() { BID = bid, ID = assemblyID, Name = $"AssemblyWithA{typeof(NumberEditElement).Name}" };
            var heightVar = new AssemblyVariable()
            {
                Name = "Height",
                ElementType = elementType,
                ID = heightVarID,
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = null,
                DataType = dataType,
                IsFormula = false,
            };
            apiContext.AssemblyVariable.Add(heightVar);

            var areaVar = new AssemblyVariable()
            {
                Name = "Area",
                ElementType = elementType,
                ID = areaVarID,
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "=Height.Value * Height.Value",
                DataType = dataType,
                IsFormula = true,
            };
            apiContext.AssemblyVariable.Add(areaVar);

            apiContext.AssemblyData.Add(assemblyData);

            apiContext.SaveChanges();

            var gen = CBELAssemblyGenerator.Create(apiContext, bid, heightVarID, ClassType.Assembly.ID());

            string assemblyCode = gen.GetAssemblyCode();

            ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(instance, assemblyCode);
            var properties = instance.GetType().GetProperties().ToList();
            Assert.AreEqual(6, properties.Where(t => t.PropertyType.GetInterface(nameof(ICBELVariable)) != null).Count());
            Assert.IsNotNull(instance.Variables.Keys.FirstOrDefault(t => t == "Height"));

            Assert.AreEqual("Height", ((NumberEditElement)instance.Variables.FirstOrDefault(t => t.Key == "Area").Value).ReliesOn.FirstOrDefault().Key);
            await CleanupPreviousUsage(bid, assemblyID, apiContext, cache);

        }

        [TestMethod]
        public async Task TestFromSQLServer()
        {
            short bid = 1;
            int assemblyID = -255;
            string assemblyDataJson = @"
    {
        'BID': 1,

        'ID': 1153,
        'ClassTypeID': 12040,
        'HasImage': false,
        'IsActive': true,
        'ModifiedDT': '2019-04-13T01:40:51.85',
        'Name': 'AssertNewAssemblyFormat',
        'IncomeAccountID': 4000,
        'IncomeAllocationType': 0,
        'PricingType': 0,
        'TaxabilityCodeID': 0
    }";
            AssemblyData assemblyData = JsonConvert.DeserializeObject<AssemblyData>(assemblyDataJson);
            string assemblyVariablesJson = @"
[
    {
        'BID': 1,
        'ID': 2482,
        'ClassTypeID': 12046,
        'DataType': 2,
        'DefaultValue': '1',
        'IsRequired': true,
        'IsFormula': false,
        'ModifiedDT': '2019-04-13T01:40:51.9589494',
        'Name': 'TotalQuantity',
        'SubassemblyID': 1153,
        'AllowDecimals': false,
        'DecimalPlaces': 0,
        'DisplayType': 0,
        'ElementType': 21,
        'ElementUseCount': 0,
        'Label': 'Total Quantity',
        'IsDisabled': false,
        'IsConsumptionFormula': false

    },
    {
        'BID': 1,
        'ID': 2483,
        'ClassTypeID': 12046,
        'DataType': 2,
        'DefaultValue': '=Area.Value*10',
        'IsRequired': false,
        'IsFormula': true,
        'ModifiedDT': '2019-04-13T01:40:51.9562079',
        'Name': 'Price',
        'SubassemblyID': 1153,
        'AllowDecimals': true,
        'DecimalPlaces': 2,
        'DisplayType': 2,
        'ElementType': 21,
        'ElementUseCount': 0,
        'Label': 'Price',
        'IsDisabled': false,
        'IsConsumptionFormula': false
    },
    {
        'BID': 1,
        'ID': 2484,
        'ClassTypeID': 12046,
        'DataType': 1,
        'DefaultValue': '=CompanyTier();',
        'IsRequired': true,
        'IsFormula': true,
        'ModifiedDT': '2019-04-13T01:40:51.9614347',
        'Name': 'Tier',
        'SubassemblyID': 1153,
        'AllowDecimals': true,
        'ElementType': 1,
        'ElementUseCount': 0,
        'Label': 'Tier',
        'LabelType': 0,
        'SystemVariableID': 2,
        'IsDisabled': false,
        'IsConsumptionFormula': false
    },
    {
        'BID': 1,
        'ID': 2485,
        'ClassTypeID': 12046,
        'DataType': 0,
        'IsRequired': false,
        'IsFormula': false,
        'ModifiedDT': '2019-04-13T01:40:51.9485279',
        'Name': 'Group01',
        'SubassemblyID': 1153,
        'AllowCustomValue': false,
        'AllowMultiSelect': false,
        'ElementType': 0,
        'ElementUseCount': 1,
        'Label': 'Group 01',
        'IsDisabled': false,
        'IsConsumptionFormula': false
    },
    {
        'BID': 1,
        'ID': 2486,
        'ClassTypeID': 12046,
        'DataType': 2,
        'IsRequired': false,
        'IsFormula': false,
        'ModifiedDT': '2019-04-13T01:40:51.9541571',
        'Name': 'Height',
        'SubassemblyID': 1153,
        'AllowCustomValue': false,
        'AllowMultiSelect': false,
        'ElementType': 21,
        'ElementUseCount': 1,
        'Label': 'Height',
        'IsDisabled': false,
        'IsConsumptionFormula': false
    },
    {
        'BID': 1,
        'ID': 2487,
        'ClassTypeID': 12046,
        'DataType': 0,
        'IsRequired': false,
        'IsFormula': false,
        'ModifiedDT': '2019-04-13T01:40:51.9521765',
        'Name': 'Group02',
        'SubassemblyID': 1153,
        'AllowCustomValue': false,
        'AllowMultiSelect': false,
        'ElementType': 0,
        'ElementUseCount': 1,
        'Label': 'Group 02',
        'IsDisabled': false,
        'IsConsumptionFormula': false
    },
    {
        'BID': 1,
        'ID': 2488,
        'ClassTypeID': 12046,
        'DataType': 2,
        'DefaultValue': '=Height.Value * Height.Value +10',
        'IsRequired': false,
        'IsFormula': true,
        'ModifiedDT': '2019-04-13T01:40:51.9466247',
        'Name': 'Area',
        'SubassemblyID': 1153,
        'AllowCustomValue': false,
        'AllowMultiSelect': false,
        'ElementType': 21,
        'ElementUseCount': 1,
        'Label': 'Area',
        'IsDisabled': false,
        'IsConsumptionFormula': false
    }
]";
            List<AssemblyVariable> assemblyVariables = JsonConvert.DeserializeObject<List<AssemblyVariable>>(assemblyVariablesJson);

            Assert.IsNotNull(assemblyData);
            Assert.IsNotNull(assemblyVariables);

            assemblyData.ID = assemblyID;
            assemblyData.BID = bid;
            assemblyVariables.ForEach(t => { t.BID = bid; t.AssemblyID = assemblyID; });

            var ctx = GetMockCtx(bid);

            var foundAssemblyData = ctx.AssemblyData.FirstOrDefault(t => t.BID == bid && t.ID == assemblyID);
            if (foundAssemblyData != null)
                ctx.Remove(foundAssemblyData);

            var incomeaccount = ctx.GLAccount.FirstOrDefault(t => t.BID == bid && t.ID == assemblyData.IncomeAccountID);

            assemblyVariables.ForEach(
                x =>
                {
                    var foundAssemblyVar = ctx.AssemblyVariable.FirstOrDefault(t => t.BID == bid && t.ID == x.ID);

                    if (foundAssemblyVar != null)
                        ctx.Remove(foundAssemblyVar);
                });


            ctx.SaveChanges();
            ctx.AssemblyData.Add(assemblyData);
            ctx.AssemblyVariable.AddRange(assemblyVariables);

            ctx.SaveChanges();
            var gen = CBELAssemblyGenerator.Create(ctx, bid, assemblyData.ID, ClassType.Assembly.ID());

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            var instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(instance);
        }

        [TestMethod]
        public async Task StandaloneAssemblyValidation()
        {
            short bid = 1;
            int id = -100;

            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"AssemblyWithRelatedVariables" };
            assemblyData.Variables = new List<AssemblyVariable>();
            assemblyData.Tables = new List<AssemblyTable>();
            var test1Var = new AssemblyVariable()
            {
                Name = "test1Var",
                ElementType = AssemblyElementType.Number,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "= test2Var.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };
            var test2Var = new AssemblyVariable()
            {
                Name = "test2Var",
                ElementType = AssemblyElementType.Number,
                ID = id - 1,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "10",
                DataType = DataType.Number,
                IsFormula = false,
            };
            assemblyData.Variables.Add(test1Var);
            assemblyData.Variables.Add(test2Var);

            string variableName = "ShirtPricing";
            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                ID = (short)id,
                VariableName = variableName,
                CellDataType = DataType.Number,
                Label = variableName,
                RowDataType = DataType.Number,
                ColumnDataType = DataType.String,
                RowVariableID = test1Var.ID,
                ColumnVariableID = test2Var.ID,
                AssemblyID = id,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":10},{\"index\":0,\"Value\":50}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":\"small\"},{\"index\":0,\"Value\":\"medium\"},{\"index\":0,\"Value\":\"large\"}]",
                CellDataJSON = "[[45,4,3],[90,,2],[,9,]]"
            };
            assemblyData.Tables.Add(table);


            int ctid = ClassType.Assembly.ID();

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(instance);
            Assert.IsNotNull(instance.Variables.FirstOrDefault(t => t.Key == variableName));
            var test1VarReliesOn = ((NumberDataTable<decimal?, string>)instance.Variables[variableName]).ReliesOn;
            Assert.AreEqual(2, test1VarReliesOn.Count);
        }

        [TestMethod]
        public async Task AssemblyGeneratorTestCompilationReturnsNoError()
        {
            AssemblyData assemblyData = GetAssemblyDataWithTableAndVariables();

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Success);
            Assert.IsNull(result.Errors);
        }

        [TestMethod]
        public void AssemblyGeneratorTestCompilationReturnsErrorWithInvalidRowVariableID()
        {
            AssemblyData assemblyData = GetAssemblyDataWithTableAndVariables();
            assemblyData.Tables.First().RowVariableID = -1;
            var gen = CBELAssemblyGenerator.Create(null, assemblyData);

            Assert.AreEqual(1, gen.Errors.Count);
        }

        [TestMethod]
        public async Task AssemblyGeneratorTestCompilationWithTypoReturnsFailure()
        {
            var assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Height",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "0",
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Width",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "0",
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Area",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "=Width.Value * Hieght.Value",
                        IsFormula = true,
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Description",
                        ElementType = AssemblyElementType.SingleLineText,
                        DefaultValue = "Place Description Here",
                        DataType = DataType.String,
                    },
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsFalse(result.Success);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual(1, result.ExternalDependencies.Count);

            Assert.AreEqual("Area", result.Errors[0].VariableName);
            Assert.AreEqual("=Width.Value * Hieght.Value", result.Errors[0].FormulaText);

        }

        [TestMethod]
        public async Task AssemblyGeneratorTestCompilationWithMissingValueReturnsFailure()
        {
            var assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Height",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "0",
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Width",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "0",
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Area",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "=Width * Height",
                        IsFormula = true,
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Description",
                        ElementType = AssemblyElementType.SingleLineText,
                        DefaultValue = "Place Description Here",
                        DataType = DataType.String,
                    },
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsFalse(result.Success);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(1, result.Errors.Count);

            Assert.AreEqual("Area", result.Errors[0].VariableName);
            Assert.AreEqual("=Width * Height", result.Errors[0].FormulaText);
            Assert.IsTrue(result.Errors[0].Message.EndsWith("Width variable reference does not include a property."), result.Errors[0].Message);
        }

        [TestMethod]
        public async Task AssemblyGeneratorTestCompilationReturnsFailure()
        {
            AssemblyData assemblyData = GetAssemblyDataWithTableAndVariables();
            var var1 = assemblyData.Variables.ToList()[0];
            var1.DefaultValue = "=loremIpsum.Value";
            var1.IsFormula = true;

            var var2 = assemblyData.Variables.ToList()[1];
            var2.DefaultValue = "=loremIpsumTwo.Value";
            var2.IsFormula = true;

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsFalse(result.Success);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(2, result.Errors.Count);
            Assert.AreEqual(2, result.ExternalDependencies.Count);

            Assert.AreEqual(var1.Name, result.Errors[0].VariableName);
            Assert.AreEqual(var1.DefaultValue, result.Errors[0].FormulaText);

            Assert.AreEqual(var2.Name, result.Errors[1].VariableName);
            Assert.AreEqual(var2.DefaultValue, result.Errors[1].FormulaText);

        }

        [TestMethod]
        public void AssemblyGeneratorTestCompilationReturnsErrorWithInvalidColumnVariableID()
        {
            AssemblyData assemblyData = GetAssemblyDataWithTableAndVariables();
            assemblyData.Tables.First().ColumnVariableID = -1;
            var gen = CBELAssemblyGenerator.Create(null, assemblyData);

            Assert.AreEqual(1, gen.Errors.Count);
        }

        [TestMethod]
        public async Task AssemblyValidation()
        {
            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Test",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "=Blah.Value",
                        IsFormula = true,
                    }
    }
            };

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);

            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(result.Success);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(1, result.Errors.Count);
            Assert.AreEqual(1, result.ExternalDependencies.Count);
        }

        [TestMethod]
        [DataRow("20", Unit.Megabyte, "InKiloBytes", Unit.KiloByte, "20000")]
        [DataRow("1", Unit.Meter, "InBoxes", Unit.Box, null)]
        [DataRow("4", Unit.Hour, "InSeconds", Unit.Second, "14400")]
        [DataRow("4", Unit.Gallon, "InOunces", Unit.OunceFluid, "512")]
        [DataRow("4", Unit.Pound, "InOunces", Unit.OunceSolid, "64")]
        public async Task NumberVariableConversionValidation_Test(string originalValue, Unit originalUnit, string conversionProperty, Unit convertToUnit, string expectedValue)
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            var ctx = PricingTestHelper.GetMockCtx(1);
            decimal? expected = Convert.ToDecimal(expectedValue);

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "originalValue",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = originalValue,
                        UnitID = originalUnit,
                        IsFormula = false,
                    },
                    new AssemblyVariable()
                    {
                        Name = "convertedValue",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = $"=originalValue.{conversionProperty}",
                        UnitID = convertToUnit,
                        IsFormula = true,
                    }
                }
            };

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);

            AssemblyCompilationResponse compileResult = await gen.TestAssemblyCompilation();
            Assert.IsNotNull(compileResult);
            Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));

            ICBELAssembly cbelAssembly = await gen.GetAssemblyInstance(ctx, cache, logger);
            Assert.IsNotNull(cbelAssembly);

            ICBELComputeResult result = cbelAssembly.Compute();

            Assert.IsNotNull(result);

            IVariableData convertedValue = result.VariableData.FirstOrDefault(x => x.VariableName == "convertedValue");
            Assert.IsNotNull(convertedValue);

            if(expected != null)
                Assert.AreEqual(expected, Convert.ToDecimal(convertedValue.Value));
            else
                Assert.AreEqual(expected, convertedValue.Value);
        }

        [TestMethod]
        [DataRow(Unit.Second)]
        [DataRow(Unit.Hour)]
        [DataRow(Unit.Meter)]
        [DataRow(Unit.Carton)]
        [DataRow(Unit.CubicCentimeter)]
        public async Task NumberVariablePropertyValidation_Test(Unit originalUnit)
        {
            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "originalObject",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "1",
                        UnitID = originalUnit,
                        IsFormula = false,
                    },
                    new AssemblyVariable()
                    {
                        Name = "getUnitType",
                        DataType = DataType.AnyObjectScalar,
                        DefaultValue = "=originalObject.UnitType",
                        UnitID = Unit.None,
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "getUnitTypeName",
                        DataType = DataType.String,
                        DefaultValue = "=originalObject.UnitTypeName",
                        UnitID = Unit.None,
                        IsFormula = true,
                    }
                }
            };

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);

            AssemblyCompilationResponse compileResult = await gen.TestAssemblyCompilation();
            Assert.IsNotNull(compileResult);
            Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));
        }

        [TestMethod]
        public async Task AssemblyMeasurementUnitIDValidation()
        {
            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Test0",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        UnitType = UnitType.General,
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Test1",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        UnitType = UnitType.Length,
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Test2",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        UnitID = Unit.None,
                        UnitType = UnitType.Length,
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Test3",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        UnitType = UnitType.Length,
                        UnitID = (Unit)21,
                        IsFormula = true,
                    }
                }
            };

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);

            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(result.Success);
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(2, result.Errors.Count);
            Assert.IsTrue(result.Errors.Any(x => x.Message == $"{CBELBaseAssemblyGenerator.NoDefaultUnitErrorMessage}Test1"));
            Assert.IsTrue(result.Errors.Any(x => x.Message == "Invalid Unit (21)"));
        }

        private static AssemblyData GetAssemblyDataWithTableAndVariables()
        {
            short bid = 1;
            int id = -100;

            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"AssemblyWithRelatedVariables" };
            assemblyData.Variables = new List<AssemblyVariable>();
            assemblyData.Tables = new List<AssemblyTable>();
            var test1Var = new AssemblyVariable()
            {
                Name = "test1Var",
                ElementType = AssemblyElementType.Number,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "= test2Var.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };
            var test2Var = new AssemblyVariable()
            {
                Name = "test2Var",
                ElementType = AssemblyElementType.Number,
                ID = id - 1,
                BID = bid,
                AssemblyID = id,
                DefaultValue = "10",
                DataType = DataType.Number,
                IsFormula = false,
            };
            assemblyData.Variables.Add(test1Var);
            assemblyData.Variables.Add(test2Var);

            string variableName = "ShirtPricing";
            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                ID = (short)id,
                VariableName = variableName,
                CellDataType = DataType.Number,
                Label = variableName,
                RowDataType = DataType.Number,
                ColumnDataType = DataType.String,
                RowVariableID = test1Var.ID,
                ColumnVariableID = test2Var.ID,
                AssemblyID = id,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":10},{\"index\":0,\"Value\":50}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":\"small\"},{\"index\":0,\"Value\":\"medium\"},{\"index\":0,\"Value\":\"large\"}]",
                CellDataJSON = "[[45,4,3],[90,,2],[,9,]]"
            };
            assemblyData.Tables.Add(table);


            return assemblyData;
        }


        [TestMethod]
        public async Task TestAssemblyCompileWithParseError()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "MyCheck",
                        ElementType = AssemblyElementType.Checkbox,
                        DataType = DataType.Boolean,
                        IsFormula = true,
                        DefaultValue = "=true"
                    },
                    new AssemblyVariable()
                    {
                        Name = "MyNumber",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "MyError",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "=MyNumber.Value * IF(MyCheck.Value, 2, 1)",
                        IsFormula = true,
                    }
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();
            Assert.IsFalse(Result.Success);

            var saveResp = await gen.Save(cache);
            Assert.IsFalse(saveResp.Success);
        }

        [TestMethod]
        public async Task ConstantAreCastToDecimalsFromTheParser()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "PrintArea",
                        ElementType = AssemblyElementType.Number,
                        UnitID = Unit.None,
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Resolution",
                        ElementType = AssemblyElementType.DropDown,
                        ListDataType = DataType.String,
                        DataType = DataType.String,
                    },
                    new AssemblyVariable()
                    {
                        Name = "PrintEquipmentTime",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "=0.15+IF(Resolution.AsNumber>0, (PrintArea.Value/Resolution.AsNumber),0)",
                        UnitID = Unit.None,
                        IsFormula = true,
                    }
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsTrue(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
        }

        private class TestFormulaConditionsResult
        {
            public string Fx { get; set; }
            public bool IsValid { get; set; }
            public bool ParsedValid { get; set; }
            public string Message { get; set; }
            public bool Success => IsValid == ParsedValid;

            public TestFormulaConditionsResult(string fx, bool isValid)
            {
                this.Fx = fx;
                this.IsValid = isValid;
            }
        }

        [TestMethod]
        public void TestFormulaConditions()
        {
            List<TestFormulaConditionsResult> testCases = new List<TestFormulaConditionsResult>()
            {
                  new TestFormulaConditionsResult("ab.c"                  , true  ) // 00
                , new TestFormulaConditionsResult("\"abc\""               , true  ) // 01
                , new TestFormulaConditionsResult("\"abc and def\""       , true  ) // 02
                , new TestFormulaConditionsResult("null"                  , true  ) // 03
                , new TestFormulaConditionsResult("\"6\""                 , true  ) // 04
                , new TestFormulaConditionsResult("6"                     , true  ) // 05
                , new TestFormulaConditionsResult("\"2 = 2\""             , true  ) // 06
                , new TestFormulaConditionsResult("1"                     , true  ) // 07
                , new TestFormulaConditionsResult("null"                  , true  ) // 08
                , new TestFormulaConditionsResult("Labor"                 , true  ) // 09
                , new TestFormulaConditionsResult("\"Labor\""             , true  ) // 10
                , new TestFormulaConditionsResult("6\" Pipe"              , false ) // 11
                , new TestFormulaConditionsResult("\"6\\\" Pipe\""        , true  ) // 12
                , new TestFormulaConditionsResult("null"                  , true  ) // 13
                , new TestFormulaConditionsResult("\"Acrylic 1 / 2\\\"\"" , true  ) // 14
                , new TestFormulaConditionsResult("\"Acrylic 1\\2\\\"\""  , true  ) // 15
                , new TestFormulaConditionsResult("Acrylic 1\\2\""        , false ) // 16
                , new TestFormulaConditionsResult("TRUE"                  , true  ) // 17
                , new TestFormulaConditionsResult("true"                  , true  ) // 18
                , new TestFormulaConditionsResult("TRUE"                  , true  ) // 19
                , new TestFormulaConditionsResult("FALSE"                 , true  ) // 20
                , new TestFormulaConditionsResult("null"                  , true  ) // 21
                , new TestFormulaConditionsResult("abc"                   , false ) // 22
            };

            Parser parser = new Parser(new CBELGrammar());
            foreach (TestFormulaConditionsResult testCase in testCases)
            {
                var parseTree = parser.Parse(testCase.Fx);

                testCase.ParsedValid = parseTree.Status != ParseTreeStatus.Error;
                testCase.Message = String.Join("\r\n", parseTree.ParserMessages.Select(t => $"{t.Location}:  {t.Message}").ToList());
            }

            Assert.IsFalse(testCases.Any(t => !t.Success), String.Join("\r\n", testCases.Where(t => !t.Success).Select(t => $"{testCases.IndexOf(t)})  {t.Fx}:  {(t.IsValid ? t.Message : "Should NOT be valid")}")));
        }

        [TestMethod]
        public void CheckFormulaThrowsArgumentNullExceptionIfFormulaIsNullOrWhitespace()
        {
            Assert.ThrowsException<ArgumentNullException>(() => PricingTestHelper.CheckFormula(null));
            Assert.ThrowsException<ArgumentNullException>(() => PricingTestHelper.CheckFormula(""));
        }

        [TestMethod]
        public void CheckFormulaThrowsArgumentExceptionIfFormulaOnlyContainsAStartingEqualSignAndRemainderIsNullOrWhitespace()
        {
            Assert.ThrowsException<ArgumentException>(() => PricingTestHelper.CheckFormula("="));
            Assert.ThrowsException<ArgumentException>(() => PricingTestHelper.CheckFormula("=  "));
        }

        [TestMethod]
        public void CheckFormulaReturnsParseResult()
        {
            string formula = "=Ten.Value * Eleven.Value * Twelve.Value";
            var checkFormulaResponse = PricingTestHelper.CheckFormula(formula);

            Assert.IsNotNull(checkFormulaResponse);
            Assert.AreEqual(formula.Substring(1), checkFormulaResponse.Formula);
            Assert.AreEqual(0, checkFormulaResponse.Failures.Count);

            formula = "Ten Eleven Twelve";
            checkFormulaResponse = PricingTestHelper.CheckFormula(formula);

            Assert.IsNotNull(checkFormulaResponse);
            Assert.AreEqual(formula, checkFormulaResponse.Formula);
            Assert.AreEqual(1, checkFormulaResponse.Failures.Count);

        }

        [TestMethod]
        public async Task TestCBELMultiLineAssemblyWithParagraphTextAndNoEquals()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Enscription",
                        ElementType = AssemblyElementType.MultiLineString,
                        DataType = DataType.String,
                        DefaultValue = "<p><span style=\"color: rgb(230, 0, 0);\">These</span></p><p><strong>Blanks</strong></p><p><strong style=\"color: rgb(178, 178, 0);\"><em>Are </em></strong></p><p><em>Very </em></p><p><u>Important</u></p>",
                        IsFormula = false,
                        Label = "Description",
                    },
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsTrue(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
        }

        [TestMethod]
        public async Task TestValidCompanyNamesInFormula()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "TierNameTest",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "CompanyNameTest",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Name",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "CompanyIDTest",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "=Company.ID",
                        IsFormula = true,
                    }

                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsTrue(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
        }

        [TestMethod]
        public async Task TestInValidCompanyNamesInFormula()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "TierNameTest",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Blah",
                        IsFormula = true,
                    },
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
        }

        [TestMethod]
        public async Task TestNoDuplicateVariableNames()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Name1",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Name2",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Name",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "CompanyIDTest",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "=Company.ID",
                        IsFormula = true,
                    }

                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsTrue(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));

        }

        [TestMethod]
        public async Task TestDuplicateVariableNames()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Name1",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Name1",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Name",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "CompanyIDTest",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "=Company.ID",
                        IsFormula = true,
                    }

                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
            Assert.AreEqual(Result.Errors.Select(t => t.Message).First(), "Non unique variable name:Name1");
        }

        [TestMethod]
        public async Task TestDuplicateTableNames()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Name1",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Name2",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Name",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "CompanyIDTest",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "=Company.ID",
                        IsFormula = true,
                    }

                },
                Tables = new List<AssemblyTable>()
                {
                    new AssemblyTable()
                    {
                        VariableName = "Table1",
                    },
                    new AssemblyTable()
                    {
                        VariableName = "Table1",
                    },

                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
            Assert.AreEqual(Result.Errors.Select(t => t.Message).First(), "Non unique table name:Table1");
        }

        [TestMethod]
        public async Task TestDuplicateTableVariableNames()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Name1",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    }

                },
                Tables = new List<AssemblyTable>()
                {
                    new AssemblyTable()
                    {
                        VariableName = "Name1",
                    },

                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
            Assert.AreEqual("Non unique table/variable name:Name1", Result.Errors.Select(t => t.Message).First());
        }

        [TestMethod]
        public async Task TestVariableConflictsWithAssemblyBaseNames()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Company",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Price",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "CompanyElement",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Tier",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    },

                    new AssemblyVariable()
                    {
                        Name = "QuantityElement",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.Tier",
                        IsFormula = true,
                    }

                },
                Tables = new List<AssemblyTable>()
                {
                    new AssemblyTable()
                    {
                        VariableName = "TotalCost",
                    },
                    new AssemblyTable()
                    {
                        VariableName = "TierTable",
                    },

                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
            Assert.AreEqual(2, Result.Errors.Count);
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Variable name of:Company is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Table name of:TotalCost is a reserved variable in Assemblies");

        }

        [TestMethod]
        public async Task TestVariableConflictsWithReservedWords()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Time",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Case",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Materials",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Compute",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "IsCalculated",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                    }
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
            Assert.AreEqual(5, Result.Errors.Count);
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Variable name of:Time is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Variable name of:Case is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Variable name of:Materials is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Variable name of:Compute is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Variable name of:IsCalculated is a reserved variable in Assemblies");
        }

        [TestMethod]
        public async Task TestTablesConflictsWithReservedWords()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                Tables = new List<AssemblyTable>()
                {
                    new AssemblyTable()
                    {
                        VariableName = "Time",
                    },
                    new AssemblyTable()
                    {
                        VariableName = "Case",
                    },
                    new AssemblyTable()
                    {
                        VariableName = "Materials",
                    },
                    new AssemblyTable()
                    {
                        VariableName = "Compute",
                    },
                    new AssemblyTable()
                    {
                        VariableName = "IsCalculated",
                    }
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
            Assert.AreEqual(5, Result.Errors.Count);
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Table name of:Time is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Table name of:Case is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Table name of:Materials is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Table name of:Compute is a reserved variable in Assemblies");
            CollectionAssert.Contains(Result.Errors.Select(t => t.Message).ToList(), "Table name of:IsCalculated is a reserved variable in Assemblies");
        }

        [TestMethod]
        public async Task InlineCodeAfterDefaultValueConstantShouldGenerateAnErrorDuringCompilation()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assemblyData = new AssemblyData()
            {
                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Name1",
                        ElementType = AssemblyElementType.Checkbox,
                        DataType = DataType.Boolean,
                        DefaultValue = @"true; System.IO.File.WriteAllText(""C:\\dev\\test.txt"", ""ben's test constant"")",
                        IsFormula = false,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Name2",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = @"10; System.IO.File.WriteAllText(""C:\\dev\\test.txt"", ""ben's test constant"")",
                        IsFormula = false,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Name3",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = @"10""; System.IO.File.WriteAllText(""C:\\dev\\test.txt"", ""ben's test constant"")",
                        IsFormula = false,
                    }
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();

            Assert.IsFalse(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
            Assert.AreEqual("Invalid data in default value constant", Result.Errors.Select(t => t.Message).First());

        }

        [TestMethod]
        public async Task TestFixedAndTierTableFieldsPopulateWithValues()
        {
            short bid = 1;
            int id = -99;

            EF.ApiContext apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            await CleanupPreviousUsage(bid, id, apiContext, cache);

            apiContext = PricingTestHelper.GetMockCtx(bid);
            AssemblyData assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = id,
                Name = $"TestAssemblyFixedAndTierTableFields",
                FixedPrice = 17m,
                FixedMargin = 18m,
                FixedMarkup = 19m
            };

            assemblyData.Variables = new List<AssemblyVariable>();
            assemblyData.Tables = new List<AssemblyTable>();

            AssemblyVariable columnVariable = new AssemblyVariable()
            {
                Name = "TotalQuantity",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(apiContext, bid),
                BID = bid,
                AssemblyID = id,
                DefaultValue = "1",
                DataType = DataType.Number,
                IsFormula = false,
            };

            AssemblyVariable rowVariable = new AssemblyVariable
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyVariableID(apiContext, bid),
                ClassTypeID = 12046,
                ModifiedDT = DateTime.Now,
                Name = "Tier",
                DefaultValue = "=CompanyTier();",
                DataType = DataType.String,
                IsFormula = true,
                UnitID = null,
                ListDataType = null,
                ListValuesJSON = null,
                AllowCustomValue = null,
                AllowMultiSelect = null,
                GroupOptionsByCategory = null,
                Label = "Tier",
                LabelType = null,
                AltText = null,
                ElementUseCount = 0,
                Tooltip = null,
                AllowDecimals = null,
                DisplayType = null,
                DecimalPlaces = null,
                SystemVariableID = 2,
                IsRequired = true,
                IsDisabled = false,
                ElementType = AssemblyElementType.SingleLineLabel,
                AssemblyID = id,
                TempID = null,
                LinkedMaterialID = null,
                LinkedLaborID = null,
                LinkedMachineID = null,
                LinkedAssemblyID = null,
                ConsumptionDefaultValue = null,
                IsConsumptionFormula = false,
                Formulas = null
            };

            assemblyData.Variables.Add(rowVariable);
            assemblyData.Variables.Add(columnVariable);

            AssemblyTable tierMargintable = new AssemblyTable()
            {
                BID = bid,
                ID = (short)PricingTestHelper.NextAssemblyID(apiContext, bid),
                VariableName = "TierMarginTable",
                CellDataType = DataType.Number,
                Label = "TierMarginTable",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = columnVariable.ID,
                ColumnMatchType = AssemblyTableMatchType.ExactMatch,
                RowMatchType = AssemblyTableMatchType.ExactMatch,
                AssemblyID = id,
                TableType = AssemblyTableType.Margin,
                IsTierTable = true,
                RowValuesJSON = "[{\"index\":0,\"Value\":\"1\",\"Name\":\"Default Tier\"}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[0,2,4,6,8]]"
            };

            AssemblyTable tierPriceTable = new AssemblyTable()
            {
                BID = bid,
                ID = (short)PricingTestHelper.NextAssemblyID(apiContext, bid),
                VariableName = "TierPriceTable",
                CellDataType = DataType.Number,
                Label = "TierPriceTable",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = columnVariable.ID,
                RowMatchType = AssemblyTableMatchType.ExactMatch,
                ColumnMatchType = AssemblyTableMatchType.ExactMatch,
                AssemblyID = id,
                IsTierTable = true,
                TableType = AssemblyTableType.PercentageDiscount,
                RowValuesJSON = "[{\"index\":0,\"Value\":\"1\",\"Name\":\"Default Tier\"}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[0,2,4,6,8]]"
            };

            AssemblyTable tierPercentagetable = new AssemblyTable()
            {
                BID = bid,
                ID = (short)PricingTestHelper.NextAssemblyID(apiContext, bid),
                VariableName = "TierDiscountTable",
                CellDataType = DataType.Number,
                Label = "TierPercentageTable",
                RowDataType = DataType.String,
                ColumnDataType = DataType.Number,
                ColumnVariableID = columnVariable.ID,
                ColumnMatchType = AssemblyTableMatchType.ExactMatch,
                RowMatchType = AssemblyTableMatchType.ExactMatch,
                AssemblyID = id,
                IsTierTable = true,
                TableType = AssemblyTableType.PercentageDiscount,
                RowValuesJSON = "[{\"index\":0,\"Value\":\"1\",\"Name\":\"Default Tier\"}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[0,2,4,6,8]]"
            };

            AssemblyTable tierMultipliertable = new AssemblyTable()
            {
                BID = bid,
                ID = (short)PricingTestHelper.NextAssemblyID(apiContext, bid),
                VariableName = "TierMarkupTable",
                CellDataType = DataType.Number,
                Label = "TierMarkupTable",
                RowDataType = DataType.String,
                ColumnDataType = DataType.Number,
                ColumnVariableID = columnVariable.ID,
                ColumnMatchType = AssemblyTableMatchType.ExactMatch,
                RowMatchType = AssemblyTableMatchType.ExactMatch,
                AssemblyID = id,
                IsTierTable = true,
                TableType = AssemblyTableType.UnitPrice,
                RowValuesJSON = "[{\"index\":0,\"Value\":\"1\",\"Name\":\"Default Tier\"}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[0,2,4,6,8]]"
            };

            assemblyData.Tables.Add(tierMargintable);
            assemblyData.Tables.Add(tierPriceTable);
            assemblyData.Tables.Add(tierPercentagetable);
            assemblyData.Tables.Add(tierMultipliertable);

            apiContext.AssemblyData.Add(assemblyData);

            apiContext.SaveChanges();

            var gen = CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID());

            ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(instance);

            Assert.IsNotNull(instance.FixedPrice);
            Assert.AreEqual(assemblyData.FixedPrice, instance.FixedPrice);
            Assert.IsNotNull(instance.FixedMargin);
            Assert.AreEqual(assemblyData.FixedMargin, instance.FixedMargin);
            Assert.IsNotNull(instance.FixedMarkup);
            Assert.AreEqual(assemblyData.FixedMarkup, instance.FixedMarkup);

            Assert.IsNotNull(instance.TierDiscountTable);
            Assert.IsNotNull(instance.TierMarginTable);
            Assert.IsNotNull(instance.TierMarkupTable);
            Assert.IsNotNull(instance.TierPriceTable);

            await CleanupPreviousUsage(bid, id, apiContext, cache);
        }

        [TestMethod]
        public async Task TestFixedFieldsPopulateWithNullValues()
        {
            short bid = 1;
            int id = -99;

            var apiContext = PricingTestHelper.GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            await CleanupPreviousUsage(bid, id, apiContext, cache);

            apiContext = PricingTestHelper.GetMockCtx(bid);
            var assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = id,
                Name = $"TestAssemblyFixedAndTierTableFields"
            };
            apiContext.AssemblyData.Add(assemblyData);

            apiContext.SaveChanges();

            var gen = CBELAssemblyGenerator.Create(apiContext, bid, id, ClassType.Assembly.ID());

            ICBELAssembly instance = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(instance);

            Assert.IsNull(instance.FixedPrice);
            Assert.IsNull(instance.FixedMargin);
            Assert.IsNull(instance.FixedMarkup);

            await CleanupPreviousUsage(bid, id, apiContext, cache);
        }

        [TestMethod]
        public void TestCBELMaterialModelAndInterface()
        {
            var myType = typeof(CBELMaterialComponent);
            var propertiesToCheck = new string[]{
                "Name",
                "NameOnInvoice",
                "SKU",
                "Description",
                "MaterialType",
                "Length",
                "Height",
                "Width",
                "Depth",
                "Thickness",
                "Weight",
                "QuantityInSet"
            };

            foreach (var propertyName in propertiesToCheck)
            {
                var myPropInfo = myType.GetProperty(propertyName);
                Assert.IsNotNull(myPropInfo);
            }

            CBELMaterialComponent material = new CBELMaterialComponent();
            Assert.IsTrue(material is ICBELMaterialComponent);
        }

        [TestMethod]
        public void TestCBELLaborModelAndInterface()
        {
            var myType = typeof(CBELLaborComponent);
            var propertiesToCheck = new string[]{
                "Name",
                "NameOnInvoice",
                "SKU",
                "Description",
                "SetupPrice",
                "HourlyPrice",
                "BillingIncrement",
                "MinimumBillingTime"
            };

            foreach (var propertyName in propertiesToCheck)
            {
                var myPropInfo = myType.GetProperty(propertyName);
                Assert.IsNotNull(myPropInfo);
            }

            CBELLaborComponent labor = new CBELLaborComponent();
            Assert.IsTrue(labor is ICBELLaborComponent);
        }

        [TestMethod]
        public async Task TestErrorExtensionMethod()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            string result = gen.GetAssemblyCode();
            Assert.IsNotNull(result);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);
            assembly.ERROR("test1 error");
            assembly.ERROR("test2 error");
            Assert.IsTrue(assembly.HasError());
            Assert.AreEqual(assembly.Errors.Count(), 2);
            Assert.AreEqual(assembly.Errors[0].Message, "test1 error");
            Assert.AreEqual(assembly.Errors[1].Message, "test2 error");
        }

        [TestMethod]
        public async Task TestCBELComputeNonOVAndOverriddenValues()
        {
            short bid = 1;
            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(bid), cache, logger);
            Assert.IsNotNull(assembly);
            CBELOverriddenValues request = new CBELOverriddenValues();


            //Test Non-OverriddenValues
            ICBELComputeResult result = assembly.Compute(request);

            Assert.IsNotNull(result?.VariableData);
            AssertVariableValue("Height", "24", false, result.VariableData);
            AssertVariableValue("Width", "18", false, result.VariableData);


            // Test OverriddenValues
            request.VariableData.Add(new VariableData
            {
                VariableName = "Height",
                IsOV = true,
                Value = "24",
            });
            request.VariableData.Add(new VariableData
            {
                VariableName = "Width",
                IsOV = true,
                Value = "18",
            });


            result = assembly.Compute(request);

            Assert.IsNotNull(result?.VariableData);
            AssertVariableValue("Height", "24", true, result.VariableData);
            AssertVariableValue("Width", "18", true, result.VariableData);
            AssertVariableValue("Area", (24 * 18).ToString(), false, result.VariableData);

            Assert.IsFalse(result.HasErrors);
            Assert.IsFalse(result.HasValidationFailures);

        }

        [TestMethod]
        public async Task LinkedAssemblyCompileAndComputeCostTest()
        {
            short bid = 1;
            int assemblyID = -99;
            int materialID = -99;
            int currentVariableID = -99;

            string assemblyName = "TestAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            MaterialData materialData = new MaterialData()
            {
                BID = bid,
                ID = materialID,
                ConsumptionUnit = Unit.Each,
                EstimatingConsumptionMethod = MaterialConsumptionMethod.Each,
                EstimatingCost = 1m,
                EstimatingCostingMethod = MaterialCostingMethod.Manual,
                ExpenseAccountID = 5000,
                HasImage = false,
                IncomeAccountID = 4000,
                InventoryAccountID = 1510,
                InvoiceText = "Test Material",
                IsActive = true,
                Name = "Test Material",
                PhysicalMaterialType = MaterialPhysicalType.Discrete,
                QuantityInSet = 1,
                SKU = "",
                TrackInventory = false,
                TaxCodeID = 9,
            };

            AssemblyVariable assemblyQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                IsFormula = false,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable assemblyCalculatedCost = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "CalculatedCost",
                Label = "Calculated Cost",
                DataType = DataType.String,
                DefaultValue = "=text(totalcost, \"F2\")",
                ElementType = AssemblyElementType.SingleLineText,
                IsFormula = true,
            };

            AssemblyVariable assemblyMyMaterial = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "MyMaterial",
                Label = "My Material",
                DataType = DataType.MaterialPart,
                DefaultValue = "My Material",
                ElementType = AssemblyElementType.LinkedMaterial,
                IsFormula = false,
                LinkedMaterialID = materialID,
                ConsumptionDefaultValue = "=AssemblyQuantity.Value",
                IsConsumptionFormula = true,
                UnitType = UnitType.Length,
                UnitID = Unit.Inch,
            };


            AssemblyData testAssembly = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyQuantity,
                    assemblyPrice,
                    assemblyCalculatedCost,
                    assemblyMyMaterial,
                }
            };

            ctx.MaterialData.Add(materialData);
            ctx.AssemblyData.Add(testAssembly);
            ctx.SaveChanges();

            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, testAssembly);
                ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);

                Assert.IsNotNull(cbelAssembly);

                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    VariableData = new List<IVariableData>()
                    {
                        new VariableData()
                        {
                            VariableName = "Height",
                            IsOV = true,
                            Value = "2",
                        },
                        new VariableData()
                        {
                            VariableName = "Width",
                            IsOV = true,
                            Value = "16",
                        }
                    },
                };
                ICBELComputeResult result = cbelAssembly.Compute(ovValues);

                Assert.IsNotNull(result);

                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);

                IVariableData varComputedCost = result.VariableData.FirstOrDefault(x => x.VariableName == "CalculatedCost");
                Assert.IsNotNull(varComputedCost);
                Assert.AreEqual("1.00", varComputedCost.Value);

            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyCalculatedCost, assemblyMyMaterial);
                await PricingTestHelper.CleanupEntities(bid, testAssembly);
                await PricingTestHelper.CleanupEntities(bid, materialData);
            }
        }


        [TestMethod]
        public async Task LinkedAssemblyWithMachineProfilesWithConstantDefaultTest()
        {
            short bid = 1;
            int assemblyID = -99;
            int machineTemplateID = -98;
            short machineID = -99;
            int currentVariableID = -99;
            int currentProfileID = -99;

            string assemblyName = "TestAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineTemplateName = "TestMachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineName = "TestMachine" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable templateCostPerHour = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "MyCostPerHour",
                Label = "MyCostPerHour",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "=MachineCostPerHour",
                IsFormula = true,
                IsRequired = false,
            };

            AssemblyVariable templateQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "1",
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable templatePrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable templateProfile = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Profile",
                Label = "Profile",
                DataType = DataType.String,
                DefaultValue = null,
                ElementType = AssemblyElementType.DropDown,
                DisplayType = CustomFieldNumberDisplayType.General,
                ListDataType = DataType.String,
                IsFormula = false,
            };

            AssemblyData testTemplate = new AssemblyData()
            {
                BID = bid,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    templateQuantity,
                    templatePrice,
                    templateProfile,
                    templateCostPerHour,
                }
            };

            MachineProfile machineProfile01 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = true,
                Name = "Profile 01",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"AssemblyQuantity\":{\"Value\":\"6\",\"ValueOV\":true}}",
            };

            MachineProfile machineProfile02 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 02",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"AssemblyQuantity\":{\"Value\":\"24\",\"ValueOV\":true}}",
            };

            MachineProfile machineProfile03 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 03",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{}",
            };

            MachineData testMachine = new MachineData()
            {
                BID = bid,
                ID = machineID,
                ExpenseAccountID = 5000,
                Name = machineName,
                MachineTemplateID = machineTemplateID,
                EstimatingCostPerHourFormula = "1",
                Profiles = new List<MachineProfile>()
                {
                    machineProfile01,
                    machineProfile02,
                    machineProfile03,
                },
            };

            AssemblyVariable assemblyQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable assemblyMachine = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "TestMachine",
                Label = "Test Machine",
                DataType = DataType.MachinePart,
                DefaultValue = machineName,
                ElementType = AssemblyElementType.LinkedMachine,
                IsFormula = false,
                LinkedMachineID = machineID,
                ConsumptionDefaultValue = "=AssemblyQuantity.Value",
                IsConsumptionFormula = true,

            };

            AssemblyData testAssembly = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = assemblyName,
                AssemblyType = AssemblyType.Product,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyQuantity,
                    assemblyPrice,
                    assemblyMachine,
                }
            };

            try
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyMachine, testAssembly);
                await PricingTestHelper.CleanupEntities(bid, templateQuantity, templatePrice, templateProfile, machineProfile01, machineProfile02, machineProfile03, testMachine, testTemplate, templateCostPerHour);

            }
            catch
            {
                ;
            }

            ctx.MachineData.Add(testMachine);
            ctx.AssemblyData.Add(testTemplate);
            ctx.MachineProfile.Add(machineProfile01);
            ctx.MachineProfile.Add(machineProfile02);
            ctx.MachineProfile.Add(machineProfile03);
            ctx.AssemblyData.Add(testAssembly);
            ctx.SaveChanges();

            try
            {
                var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, assemblyID, ClassType.Assembly.ID(), 1, cache, logger, bid);

                Assert.AreEqual(0, compilationResponse.ErrorCount);
                Assert.IsNotNull(compilationResponse.Assembly);


                ICBELComputeResult result = compilationResponse.Assembly.Compute();

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);



                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.AreEqual(testMachine.EstimatingCostPerHourFormula, myAssembly.Components.OfType<LinkedMachineVariable>().FirstOrDefault().MachineCostPerHour.ToString());
                Assert.AreEqual(testMachine.EstimatingCostPerHourFormula, myAssembly.Components.OfType<LinkedMachineVariable>().FirstOrDefault().MachineCostPerHour.ToString());
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                ICBELMachine cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                ICBELVariable profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 01", ((ICBELVariableComputed)profileVariable).AsString);

                ICBELVariable quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(6, ((ICBELVariableComputed)quantityVariable).AsNumber);


                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Profile",
                                    IsOV = true,
                                    Value = "Profile 02"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 02", ((ICBELVariableComputed)profileVariable).AsString);

                quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(24, ((ICBELVariableComputed)quantityVariable).AsNumber);


                ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Profile",
                                    IsOV = true,
                                    Value = "Profile 03"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);


                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 03", ((ICBELVariableComputed)profileVariable).AsString);

                quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(1, ((ICBELVariableComputed)quantityVariable).AsNumber);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyMachine, testAssembly);
                await PricingTestHelper.CleanupEntities(bid, templateQuantity, templatePrice, templateProfile, machineProfile01, machineProfile02, machineProfile03, testMachine, testTemplate, templateCostPerHour);
            }
        }

        [TestMethod]
        public async Task LinkedAssemblyWithMachineProfilesWithFormulaDefaultTest()
        {
            short bid = 1;
            int assemblyID = -99;
            int machineTemplateID = -98;
            short machineID = -99;
            int currentVariableID = -99;
            int currentProfileID = -99;

            string assemblyName = "TestAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineTemplateName = "TestMachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineName = "TestMachine" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable templateQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "=Average(1,2,3)",
                IsFormula = true,
                IsRequired = true,
            };

            AssemblyVariable templatePrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable templateProfile = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Profile",
                Label = "Profile",
                DataType = DataType.String,
                DefaultValue = null,
                ElementType = AssemblyElementType.DropDown,
                DisplayType = CustomFieldNumberDisplayType.General,
                ListDataType = DataType.String,
                IsFormula = false,
            };

            AssemblyData testTemplate = new AssemblyData()
            {
                BID = bid,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    templateQuantity,
                    templatePrice,
                    templateProfile,
                }
            };

            MachineProfile machineProfile01 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = true,
                Name = "Profile 01",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"AssemblyQuantity\":{\"Value\":\"6\",\"ValueOV\":true}}",
            };

            MachineProfile machineProfile02 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 02",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"AssemblyQuantity\":{\"Value\":\"24\",\"ValueOV\":true}}",
            };

            MachineProfile machineProfile03 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 03",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{}",
            };

            MachineData testMachine = new MachineData()
            {
                BID = bid,
                ID = machineID,
                ExpenseAccountID = 5000,
                Name = machineName,
                MachineTemplateID = machineTemplateID,
                EstimatingCostPerHourFormula = "1",
                Profiles = new List<MachineProfile>()
                {
                    machineProfile01,
                    machineProfile02,
                    machineProfile03,
                }
            };

            AssemblyVariable assemblyQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable assemblyMachine = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "TestMachine",
                Label = "Test Machine",
                DataType = DataType.MachinePart,
                DefaultValue = machineName,
                ElementType = AssemblyElementType.LinkedMachine,
                IsFormula = false,
                LinkedMachineID = machineID,
                ConsumptionDefaultValue = "=AssemblyQuantity.Value",
                IsConsumptionFormula = true,

            };

            AssemblyData testAssembly = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = assemblyName,
                AssemblyType = AssemblyType.Product,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyQuantity,
                    assemblyPrice,
                    assemblyMachine,
                }
            };

            ctx.MachineData.Add(testMachine);
            ctx.AssemblyData.Add(testTemplate);
            ctx.MachineProfile.Add(machineProfile01);
            ctx.MachineProfile.Add(machineProfile02);
            ctx.MachineProfile.Add(machineProfile03);
            ctx.AssemblyData.Add(testAssembly);
            ctx.SaveChanges();

            try
            {
                var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, assemblyID, ClassType.Assembly.ID(), 1, cache, logger, bid);

                Assert.AreEqual(0, compilationResponse.ErrorCount);
                Assert.IsNotNull(compilationResponse.Assembly);


                ICBELComputeResult result = compilationResponse.Assembly.Compute();

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                ICBELMachine cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                ICBELVariable profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 01", ((ICBELVariableComputed)profileVariable).AsString);

                ICBELVariable quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(6, ((ICBELVariableComputed)quantityVariable).AsNumber);


                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Profile",
                                    IsOV = true,
                                    Value = "Profile 02"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 02", ((ICBELVariableComputed)profileVariable).AsString);

                quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(24, ((ICBELVariableComputed)quantityVariable).AsNumber);


                ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Profile",
                                    IsOV = true,
                                    Value = "Profile 03"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);


                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 03", ((ICBELVariableComputed)profileVariable).AsString);

                quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(2, ((ICBELVariableComputed)quantityVariable).AsNumber);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyMachine, testAssembly);
                await PricingTestHelper.CleanupEntities(bid, templateQuantity, templatePrice, templateProfile, machineProfile01, machineProfile02, machineProfile03, testMachine, testTemplate);
            }
        }

        [TestMethod]
        public async Task LinkedAssemblyWithMachineProfilesWithNoDefaultTest()
        {
            short bid = 1;
            int assemblyID = -99;
            int machineTemplateID = -98;
            short machineID = -99;
            int currentVariableID = -99;
            int currentProfileID = -99;

            string assemblyName = "TestAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineTemplateName = "TestMachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineName = "TestMachine" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable templateQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable templatePrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable templateProfile = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Profile",
                Label = "Profile",
                DataType = DataType.String,
                DefaultValue = null,
                ElementType = AssemblyElementType.DropDown,
                DisplayType = CustomFieldNumberDisplayType.General,
                ListDataType = DataType.String,
                IsFormula = false,
            };

            AssemblyData testTemplate = new AssemblyData()
            {
                BID = bid,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    templateQuantity,
                    templatePrice,
                    templateProfile,
                }
            };

            MachineProfile machineProfile01 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = true,
                Name = "Profile 01",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"AssemblyQuantity\":{\"Value\":\"6\",\"ValueOV\":true}}",
            };

            MachineProfile machineProfile02 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 02",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"AssemblyQuantity\":{\"Value\":\"24\",\"ValueOV\":true}}",
            };

            MachineProfile machineProfile03 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 03",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{}",
            };

            MachineData testMachine = new MachineData()
            {
                BID = bid,
                ID = machineID,
                ExpenseAccountID = 5000,
                Name = machineName,
                MachineTemplateID = machineTemplateID,
                EstimatingCostPerHourFormula = "1",
                Profiles = new List<MachineProfile>()
                {
                    machineProfile01,
                    machineProfile02,
                    machineProfile03,
                }
            };

            AssemblyVariable assemblyQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable assemblyMachine = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "TestMachine",
                Label = "Test Machine",
                DataType = DataType.MachinePart,
                DefaultValue = machineName,
                ElementType = AssemblyElementType.LinkedMachine,
                IsFormula = false,
                LinkedMachineID = machineID,
                ConsumptionDefaultValue = "=AssemblyQuantity.Value",
                IsConsumptionFormula = true,

            };

            AssemblyData testAssembly = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = assemblyName,
                AssemblyType = AssemblyType.Product,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyQuantity,
                    assemblyPrice,
                    assemblyMachine,
                }
            };

            ctx.MachineData.Add(testMachine);
            ctx.AssemblyData.Add(testTemplate);
            ctx.MachineProfile.Add(machineProfile01);
            ctx.MachineProfile.Add(machineProfile02);
            ctx.MachineProfile.Add(machineProfile03);
            ctx.AssemblyData.Add(testAssembly);
            ctx.SaveChanges();

            try
            {
                var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, assemblyID, ClassType.Assembly.ID(), 1, cache, logger, bid);

                Assert.AreEqual(0, compilationResponse.ErrorCount);
                Assert.IsNotNull(compilationResponse.Assembly);


                ICBELComputeResult result = compilationResponse.Assembly.Compute();

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                ICBELMachine cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                ICBELVariable profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 01", ((ICBELVariableComputed)profileVariable).AsString);

                ICBELVariable quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(6, ((ICBELVariableComputed)quantityVariable).AsNumber);


                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Profile",
                                    IsOV = true,
                                    Value = "Profile 02"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 02", ((ICBELVariableComputed)profileVariable).AsString);

                quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(24, ((ICBELVariableComputed)quantityVariable).AsNumber);


                ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Profile",
                                    IsOV = true,
                                    Value = "Profile 03"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsTrue(result.HasValidationFailures);
                Assert.AreEqual(1, result.ValidationFailures.Count);
                Assert.AreEqual(machineName, result.ValidationFailures[0].AssemblyComponent?.Name);
                Assert.AreEqual("AssemblyQuantity", result.ValidationFailures[0].ElementComponent?.Name);


                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 03", ((ICBELVariableComputed)profileVariable).AsString);

                quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(null, ((ICBELVariableComputed)quantityVariable).AsNumber);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyMachine, testAssembly);
                await PricingTestHelper.CleanupEntities(bid, templateQuantity, templatePrice, templateProfile, machineProfile01, machineProfile02, machineProfile03, testMachine, testTemplate);
            }
        }

        [TestMethod]
        public async Task MachineWithMachineProfilesTest()
        {
            short BID = 1;
            short machineID = -76;
            int machineTemplateID = -70;

            // Machine Template
            string assemblyDataJson = $@"
            {{
                'BID': 1,
                'ID': {machineTemplateID},
                'ClassTypeID': 12040,
                'IsActive': true,
                'Name': 'Machine Template',
                'HasImage': false,
                'PricingType': 0,
                'TaxabilityCodeID': 0,
                'IncomeAllocationType': 0,
                'HasTierTable': false,
                'MachineLayoutTypes': 0,
                'PriceFormulaType': 0,
                'SimpleAssemblyCategories': [],
                'AssemblyType': 3
            }}";
            AssemblyData assemblyData = JsonConvert.DeserializeObject<AssemblyData>(assemblyDataJson);
            assemblyData.BID = BID;

            // Variables
            string assemblyVariablesJson = $@"
            [
                {{
                    'BID': 1,
                    'ID': -71,
                    'ClassTypeID': 12046,
                    'Name': 'Area',
                    'DataType': 2,
                    'IsFormula': false,
                    'AllowCustomValue': false,
                    'AllowMultiSelect': false,
                    'Label': 'Area',
                    'ElementUseCount': 1,
                    'RollupLinkedPriceAndCost': false,
                    'IsRequired': false,
                    'IsDisabled': false,
                    'ElementType': 21,
                    'AssemblyID': {machineTemplateID},
                    'IsConsumptionFormula': false,
                    'Formulas': []
                }},
                {{
                    'BID': 1,
                    'ID': -72,
                    'ClassTypeID': 12046,
                    'Name': 'Material',
                    'DefaultValue': 'Paper',
                    'DataType': 1,
                    'IsFormula': false,
                    'ListDataType': 1,
                    'ListValuesJSON': '[{{\'Label\': \'Paper\'}},{{\'Label\': \'Vinyl\'}},{{\'Label\': \'Canvas\'}}]',
                    'AllowCustomValue': false,
                    'AllowMultiSelect': false,
                    'Label': 'Material',
                    'ElementUseCount': 1,
                    'RollupLinkedPriceAndCost': false,
                    'DisplayType': 1,
                    'IsRequired': false,
                    'IsDisabled': false,
                    'ElementType': 13,
                    'AssemblyID': {machineTemplateID},
                    'IsConsumptionFormula': false,
                    'Formulas': []
                }},
                {{
                    'BID': 1,
                    'ID': -73,
                    'ClassTypeID': 12046,
                    'Name': 'Profile',
                    'DataType': 1,
                    'IsFormula': false,
                    'ListDataType': 1,
                    'AllowCustomValue': false,
                    'AllowMultiSelect': false,
                    'Label': 'Profile',
                    'ElementUseCount': 0,
                    'RollupLinkedPriceAndCost': false,
                    'DisplayType': 1,
                    'IsRequired': true,
                    'IsDisabled': false,
                    'ElementType': 13,
                    'AssemblyID': {machineTemplateID},
                    'IsConsumptionFormula': false,
                    'Formulas': []
                }},
                {{
                    'BID': 1,
                    'ID': -74,
                    'ClassTypeID': 12046,
                    'Name': 'TotalQuantity',
                    'DefaultValue': '1',
                    'DataType': 2,
                    'IsFormula': false,
                    'Label': 'Total Quantity',
                    'ElementUseCount': 0,
                    'RollupLinkedPriceAndCost': false,
                    'AllowDecimals': false,
                    'DisplayType': 0,
                    'DecimalPlaces': 0,
                    'IsRequired': true,
                    'IsDisabled': false,
                    'ElementType': 21,
                    'AssemblyID': {machineTemplateID},
                    'IsConsumptionFormula': false,
                    'Formulas': []
                }}
            ]";
            List<AssemblyVariable> assemblyVariables = JsonConvert.DeserializeObject<List<AssemblyVariable>>(assemblyVariablesJson);
            assemblyVariables.ForEach(t => { t.BID = BID; });
            // Add the variables
            assemblyData.Variables = assemblyVariables;

            // Tables
            string assemblyTablesJson = $@"
            [
                {{
                    'BID': 1,
                    'ID': -75,
                    'ClassTypeID': 12047,
                    'AssemblyID': {machineTemplateID},
                    'Label': 'RunSpeed',
                    'VariableName': 'RunSpeed',
                    'TableType': 0,
                    'IsTierTable': false,
                    'RowLabel': 'Material',
                    'RowVariableID': -72,
                    'RowDataType': 1,
                    'RowUnitID': 0,
                    'RowCount': 3,
                    'RowValuesJSON': '[{{\'index\': 0, \'value\': \'Canvas\'}},{{\'index\': 1, \'value\': \'Paper\'}},{{\'index\': 2, \'value\': \'Vinyl\'}}]',
                    'RowDefaultMarkupJSON': '',
                    'RowIsSorted': false,
                    'RowMatchType': 0,
                    'ColumnLabel': 'Area',
                    'ColumnVariableID': -71,
                    'ColumnDataType': 2,
                    'ColumnUnitID': 0,
                    'ColumnCount': 1,
                    'ColumnValuesJSON': '[{{\'index\': 0,\'value\': 1}}]',
                    'ColumnIsSorted': false,
                    'ColumnMatchType': 0,
                    'CellDataType': 2,
                    'CellDataJSON': '[[20],[40],[30]]'
                }}
            ]";
            List<AssemblyTable> assemblyTables = JsonConvert.DeserializeObject<List<AssemblyTable>>(assemblyTablesJson);
            assemblyTables.ForEach(t => { t.BID = BID; });
            assemblyData.Tables = assemblyTables;

            // Machine
            string machineDataJson = $@"
            {{
                'BID': 1,
                'ID': {machineID},
                'ClassTypeID': 12030,
                'IsActive': true,
                'Name': 'Machine',
                'HasImage': false,
                'ExpenseAccountID': 5110,
                'EstimatingCostPerHourFormula': '10',
                'MachineCategoryLinks': [],
                'SimpleMachineCategories': [],
                'ActiveInstanceCount': 1,
                'ActiveProfileCount': 0,
                'MachineTemplateID': {machineTemplateID},
                'EstimatingCost': 10
            }}";
            MachineData machineData = JsonConvert.DeserializeObject<MachineData>(machineDataJson);
            machineData.BID = BID;

            string machineProfileTablesJson = @"
            [
                {
                    'BID': 1,
                    'ID': -77,
                    'ClassTypeID': 12035,
                    'ProfileID': -79,
                    'TableID': -75,
                    'Label': 'RunSpeed',
                    'OverrideDefault': false,
                    'RowCount': 3,
                    'RowValuesJSON': '[{\'index\': 0, \'value\': \'Canvas\'},{\'index\': 1, \'value\': \'Paper\'},{\'index\': 2, \'value\': \'Vinyl\'}]',
                    'ColumnCount': 1,
                    'ColumnValuesJSON': '[{\'index\': 0,\'value\': 1}]',
                    'CellDataJSON': '[[20],[40],[30]]'
                },
                {
                    'BID': 1,
                    'ID': -78,
                    'ClassTypeID': 12035,
                    'ProfileID': -80,
                    'TableID': -75,
                    'Label': 'RunSpeed',
                    'OverrideDefault': true,
                    'RowCount': 3,
                    'RowValuesJSON': '[{\'index\': 0, \'value\': \'Canvas\'},{\'index\': 1, \'value\': \'Paper\'},{\'index\': 2, \'value\': \'Vinyl\'}]',
                    'ColumnCount': 2,
                    'ColumnValuesJSON': '[{\'index\': 0,\'value\': 1},{\'index\': 1,\'value\': 2}]',
                    'CellDataJSON': '[[30,40],[50,60],[40,50]]'
                }
            ]";
            List<MachineProfileTable> machineProfileTables = JsonConvert.DeserializeObject<List<MachineProfileTable>>(machineProfileTablesJson);
            machineProfileTables.ForEach(t => { t.BID = BID; });

            string machineProfilesJson = $@"
            [
                {{
                    'BID': 1,
                    'ID': -79,
                    'ClassTypeID': 12034,
                    'IsActive': true,
                    'IsDefault': false,
                    'Name': 'Standard',
                    'AssemblyOVDataJSON': '',
                    'MachineID': {machineID},
                    'MachineTemplateID': {machineTemplateID}
                }},
                {{
                    'BID': 1,
                    'ID': -80,
                    'ClassTypeID': 12034,
                    'IsActive': true,
                    'IsDefault': true,
                    'Name': 'Low Resolution',
                    'AssemblyOVDataJSON': '',
                    'MachineID': {machineID},
                    'MachineTemplateID': {machineTemplateID}
                }}
            ]";
            List<MachineProfile> machineProfiles = JsonConvert.DeserializeObject<List<MachineProfile>>(machineProfilesJson);
            machineProfiles.ForEach(t => { t.BID = BID; });

            // Add the Machine Profile Table to Machine Profile
            MachineProfile machineProfileStandard = machineProfiles.FirstOrDefault();
            machineProfileStandard.MachineProfileTables = new List<MachineProfileTable>
            {
                machineProfileTables.FirstOrDefault()
            };

            MachineProfile machineProfileLowRes = machineProfiles.LastOrDefault();
            machineProfileLowRes.MachineProfileTables = new List<MachineProfileTable>
            {
                machineProfileTables.LastOrDefault()
            };

            // Add the Machine Profile to Machine
            machineData.Profiles = new List<MachineProfile>
            {
                machineProfileStandard,
                machineProfileLowRes
            };

            try
            {
                var ctx = GetMockCtx(BID);
                ITenantDataCache cache = new TestHelper.MockTenantDataCache();
                RemoteLogger logger = new RemoteLogger(cache);

                ctx.MachineData.Add(machineData);
                ctx.AssemblyData.Add(assemblyData);
                ctx.SaveChanges();

                CBELMachineGenerator gen = CBELMachineGenerator.Create(ctx, BID, machineID, ClassType.Machine.ID());
                string result = gen.GetAssemblyCode();
                Assert.IsNotNull(result);

                ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(BID), cache, logger);
                Assert.IsNotNull(assembly);

                Assert.AreEqual("Machine_BID1_CT12030_ID_76_V1", assembly.GetType().Name);

                Assert.IsInstanceOfType(assembly, typeof(CBELMachineBase));
                Assert.AreEqual(2, ((CBELMachineBase)assembly).ProfileList.Count);
                Assert.AreEqual("Standard", ((CBELMachineBase)assembly).ProfileList[0]);
                Assert.AreEqual("Low Resolution", ((CBELMachineBase)assembly).ProfileList[1]);
                // Default Profile
                Assert.AreEqual("Low Resolution", ((CBELMachineBase)assembly).Profile.Value);

                Assert.AreEqual("Machine", ((CBELMachineBase)assembly).Name);

                // Check if compilation is successful
                AssemblyCompilationResponse compileTestResult = await gen.TestAssemblyCompilation();
                Assert.IsTrue(compileTestResult.Success);
            }
            finally
            {
                // Delete Machine Profile Table
                foreach (var machineProfileTable in machineProfileTables)
                {
                    await PricingTestHelper.CleanupEntities(BID, machineProfileTable);
                };
                // Delete Profile
                foreach (var machineProfile in machineProfiles)
                {
                    await PricingTestHelper.CleanupEntities(BID, machineProfile);
                };
                // Delete Machine
                await PricingTestHelper.CleanupEntities(BID, machineData);
                // Delete Machine Template Table
                foreach (var assemblyTable in assemblyTables)
                {
                    await PricingTestHelper.CleanupEntities(BID, assemblyTable);
                };
                // Delete Machine Template Variable
                foreach (var assemblyVariable in assemblyVariables)
                {
                    await PricingTestHelper.CleanupEntities(BID, assemblyVariable);
                };
                // Delete Machine Template
                await PricingTestHelper.CleanupEntities(BID, assemblyData);
            }

        }

        [TestMethod]
        public async Task MachineWithMachineInstanceTest()
        {
            short BID = 1;
            short machineID = -76;
            int machineTemplateID = -70;

            // Machine Template
            string assemblyDataJson = $@"
            {{
                'BID': 1,
                'ID': {machineTemplateID},
                'ClassTypeID': 12040,
                'IsActive': true,
                'Name': 'Machine Template',
                'HasImage': false,
                'PricingType': 0,
                'TaxabilityCodeID': 0,
                'IncomeAllocationType': 0,
                'HasTierTable': false,
                'MachineLayoutTypes': 0,
                'PriceFormulaType': 0,
                'SimpleAssemblyCategories': [],
                'AssemblyType': 3
            }}";
            AssemblyData assemblyData = JsonConvert.DeserializeObject<AssemblyData>(assemblyDataJson);
            assemblyData.BID = BID;

            // Machine
            string machineDataJson = $@"
            {{
                'BID': 1,
                'ID': {machineID},
                'ClassTypeID': 12030,
                'IsActive': true,
                'Name': 'Machine',
                'HasImage': false,
                'ExpenseAccountID': 5110,
                'EstimatingCostPerHourFormula': '10',
                'MachineCategoryLinks': [],
                'SimpleMachineCategories': [],
                'ActiveInstanceCount': 1,
                'ActiveProfileCount': 0,
                'MachineTemplateID': {machineTemplateID},
                'EstimatingCost': 10
            }}";
            MachineData machineData = JsonConvert.DeserializeObject<MachineData>(machineDataJson);
            machineData.BID = BID;

            string machineInstancesJson = $@"
            [
                {{
                    'BID': 1,
                    'ID': -77,
                    'ClassTypeID': 12033,
                    'IsActive': true,
                    'Name': 'Instance 01',
                    'HasServiceContract': false,
                    'LocationID': 1,
                    'MachineID': {machineID}
                }}
            ]";
            List<MachineInstance> machineInstances = JsonConvert.DeserializeObject<List<MachineInstance>>(machineInstancesJson);
            machineInstances.ForEach(t => { t.BID = BID; });

            // Add the Machine Instances
            machineData.Instances = machineInstances;

            try
            {
                var ctx = GetMockCtx(BID);
                ITenantDataCache cache = new TestHelper.MockTenantDataCache();
                RemoteLogger logger = new RemoteLogger(cache);

                ctx.MachineData.Add(machineData);
                ctx.AssemblyData.Add(assemblyData);
                ctx.SaveChanges();

                CBELMachineGenerator gen = CBELMachineGenerator.Create(ctx, BID, machineID, ClassType.Machine.ID());
                // Check if compilation is successful
                AssemblyCompilationResponse compileTestResult = await gen.TestAssemblyCompilation();
                Assert.IsTrue(compileTestResult.Success);

                ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(BID), cache, logger);
                Assert.IsNotNull(assembly);

                Assert.AreEqual("Machine_BID1_CT12030_ID_76_V1", assembly.GetType().Name);

                Assert.IsInstanceOfType(assembly, typeof(CBELMachineBase));

                Assert.AreEqual("Machine", ((CBELMachineBase)assembly).Name);
                Assert.AreEqual(1, ((CBELMachineBase)assembly).InstanceList.Count);
                Assert.AreEqual("Instance 01", ((CBELMachineBase)assembly).Instance.Value);

                Assert.IsTrue(compileTestResult.Success);
            }
            finally
            {
                // Delete Machine Instance
                foreach (var machineInstance in machineInstances)
                {
                    await PricingTestHelper.CleanupEntities(BID, machineInstance);
                };
                // Delete Machine
                await PricingTestHelper.CleanupEntities(BID, machineData);
                // Delete Machine Template
                await PricingTestHelper.CleanupEntities(BID, assemblyData);
            }

        }

        [TestMethod]
        public async Task MachineTemplateWithoutMachineTest()
        {
            short BID = 1;
            int machineTemplateID = -70;

            // Machine Template
            string assemblyDataJson = $@"
            {{
                'BID': 1,
                'ID': {machineTemplateID},
                'ClassTypeID': 12040,
                'IsActive': true,
                'Name': 'Machine Template',
                'HasImage': false,
                'PricingType': 0,
                'TaxabilityCodeID': 0,
                'IncomeAllocationType': 0,
                'HasTierTable': false,
                'MachineLayoutTypes': 0,
                'PriceFormulaType': 0,
                'SimpleAssemblyCategories': [],
                'AssemblyType': 3
            }}";
            AssemblyData assemblyData = JsonConvert.DeserializeObject<AssemblyData>(assemblyDataJson);
            assemblyData.BID = BID;

            try
            {
                var ctx = GetMockCtx(BID);
                ITenantDataCache cache = new TestHelper.MockTenantDataCache();
                RemoteLogger logger = new RemoteLogger(cache);

                ctx.AssemblyData.Add(assemblyData);
                ctx.SaveChanges();

                CBELMachineGenerator gen = CBELMachineGenerator.Create(ctx, BID, machineTemplateID, ClassType.Assembly.ID());
                // Check if compilation is successful
                AssemblyCompilationResponse compileTestResult = await gen.TestAssemblyCompilation();
                Assert.IsTrue(compileTestResult.Success);

                ICBELAssembly assembly = await gen.GetAssemblyInstance(GetMockCtx(BID), cache, logger);
                Assert.IsNotNull(assembly);

                Assert.AreEqual("Assembly_BID1_CT12040_ID_70_V1", assembly.GetType().Name);

                Assert.IsInstanceOfType(assembly, typeof(CBELMachineBase));

                Assert.AreEqual("Machine Template", ((CBELMachineBase)assembly).Name);
                Assert.AreEqual(0, ((CBELMachineBase)assembly).InstanceList.Count);

                Assert.IsTrue(compileTestResult.Success);
            }
            finally
            {
                // Delete Machine Template
                await PricingTestHelper.CleanupEntities(BID, assemblyData);
            }

        }

        [TestMethod]
        public async Task LinkedAssemblyWithMachineInstancesWithConstantDefaultTest()
        {
            short bid = 1;
            int assemblyID = -99;
            int machineTemplateID = -98;
            short machineID = -99;
            int currentVariableID = -99;
            int currentProfileID = -99;
            int currentInstanceID = -99;

            string assemblyName = "TestAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineTemplateName = "TestMachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineName = "TestMachine" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable templateQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "1",
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable templateHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = @"=IF(Instance.Value = ""Instance 01"", 1,
IF(Instance.Value = ""Instance 02"", 2,
IF(Instance.Value = ""Instance 03"", 3, 0)))",
                IsFormula = true,
                IsRequired = false,
            };

            AssemblyVariable templatePrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable templateProfile = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Profile",
                Label = "Profile",
                DataType = DataType.String,
                DefaultValue = null,
                ElementType = AssemblyElementType.DropDown,
                DisplayType = CustomFieldNumberDisplayType.General,
                ListDataType = DataType.String,
                IsFormula = false,
            };

            AssemblyData testTemplate = new AssemblyData()
            {
                BID = bid,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    templateQuantity,
                    templateHeight,
                    templatePrice,
                    templateProfile,
                }
            };

            MachineProfile machineProfile01 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = true,
                Name = "Profile 01",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"AssemblyQuantity\":{\"Value\":\"6\",\"ValueOV\":true}}",
            };

            MachineProfile machineProfile02 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 02",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"AssemblyQuantity\":{\"Value\":\"24\",\"ValueOV\":true}}",
            };

            MachineInstance machineInstance01 = new MachineInstance()
            {
                BID = bid,
                ID = currentInstanceID--,
                IsActive = true,
                Name = "Instance 01",
                HasServiceContract = false,
                LocationID = 1,
                MachineID = machineID,
            };

            MachineInstance machineInstance02 = new MachineInstance()
            {
                BID = bid,
                ID = currentInstanceID--,
                IsActive = true,
                Name = "Instance 02",
                HasServiceContract = false,
                LocationID = 1,
                MachineID = machineID,
            };

            MachineInstance machineInstance03 = new MachineInstance()
            {
                BID = bid,
                ID = currentInstanceID--,
                IsActive = true,
                Name = "Instance 03",
                HasServiceContract = false,
                LocationID = 1,
                MachineID = machineID,
            };

            MachineData testMachine = new MachineData()
            {
                BID = bid,
                ID = machineID,
                ExpenseAccountID = 5000,
                Name = machineName,
                MachineTemplateID = machineTemplateID,
                EstimatingCostPerHourFormula = "1",
                Profiles = new List<MachineProfile>()
                {
                    machineProfile01,
                    machineProfile02,
                },
                Instances = new List<MachineInstance>()
                {
                    machineInstance01,
                    machineInstance02,
                    machineInstance03,
                }
            };

            AssemblyVariable assemblyQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable assemblyMachine = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "TestMachine",
                Label = "Test Machine",
                DataType = DataType.MachinePart,
                DefaultValue = machineName,
                ElementType = AssemblyElementType.LinkedMachine,
                IsFormula = false,
                LinkedMachineID = machineID,
                ConsumptionDefaultValue = "=AssemblyQuantity.Value",
                IsConsumptionFormula = true,

            };

            AssemblyData testAssembly = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = assemblyName,
                AssemblyType = AssemblyType.Product,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyQuantity,
                    assemblyPrice,
                    assemblyMachine,
                }
            };

            await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyMachine, testAssembly);
            await PricingTestHelper.CleanupEntities(bid, templateQuantity, templateHeight, templatePrice, templateProfile, machineInstance01,
                    machineInstance02, machineInstance03, machineProfile01, machineProfile02, testMachine, testTemplate);
            ctx.MachineData.Add(testMachine);
            ctx.AssemblyData.Add(testTemplate);
            ctx.MachineProfile.Add(machineProfile01);
            ctx.MachineProfile.Add(machineProfile02);
            ctx.MachineInstance.Add(machineInstance01);
            ctx.MachineInstance.Add(machineInstance02);
            ctx.MachineInstance.Add(machineInstance03);
            ctx.AssemblyData.Add(testAssembly);
            ctx.SaveChanges();

            try
            {
                var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, assemblyID, ClassType.Assembly.ID(), 1, cache, logger, bid);

                Assert.AreEqual(0, compilationResponse.ErrorCount);
                Assert.IsNotNull(compilationResponse.Assembly);


                ICBELComputeResult result = compilationResponse.Assembly.Compute();

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                ICBELMachine cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                ICBELVariable profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 01", ((ICBELVariableComputed)profileVariable).AsString);

                ICBELVariable quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(6, ((ICBELVariableComputed)quantityVariable).AsNumber);

                ICBELVariable instanceVariable = cbelMachine.Variables.Where(x => x.Key == "Instance").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(instanceVariable, typeof(ICBELVariableComputed));
                // Result should be an empty string since there are more than 1 Instance
                Assert.IsNull(((ICBELVariableComputed)instanceVariable).AsString);

                ICBELVariable heightVariable = cbelMachine.Variables.Where(x => x.Key == "Height").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(heightVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(0, ((ICBELVariableComputed)heightVariable).AsNumber);

                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Profile",
                                    IsOV = true,
                                    Value = "Profile 02"
                                },
                                new VariableData()
                                {
                                    VariableName = "Instance",
                                    IsOV = true,
                                    Value = "Instance 02"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                profileVariable = cbelMachine.Variables.Where(x => x.Key == "Profile").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(profileVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Profile 02", ((ICBELVariableComputed)profileVariable).AsString);

                instanceVariable = cbelMachine.Variables.Where(x => x.Key == "Instance").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(instanceVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Instance 02", ((ICBELVariableComputed)instanceVariable).AsString);

                quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(24, ((ICBELVariableComputed)quantityVariable).AsNumber);

                heightVariable = cbelMachine.Variables.Where(x => x.Key == "Height").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(heightVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(2, ((ICBELVariableComputed)heightVariable).AsNumber);

                ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Instance",
                                    IsOV = true,
                                    Value = "Instance 03"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);


                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);


                Assert.AreEqual(1, myAssembly.Components.Count);
                Assert.IsInstanceOfType(myAssembly.Components[0], typeof(LinkedMachineVariable));

                cbelMachine = ((LinkedMachineVariable)myAssembly.Components[0]).TypedComponent;

                Assert.IsNotNull(cbelMachine);

                instanceVariable = cbelMachine.Variables.Where(x => x.Key == "Instance").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(instanceVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual("Instance 03", ((ICBELVariableComputed)instanceVariable).AsString);

                quantityVariable = cbelMachine.Variables.Where(x => x.Key == "AssemblyQuantity").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(quantityVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(6, ((ICBELVariableComputed)quantityVariable).AsNumber);

                heightVariable = cbelMachine.Variables.Where(x => x.Key == "Height").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(heightVariable, typeof(ICBELVariableComputed));
                Assert.AreEqual(3, ((ICBELVariableComputed)heightVariable).AsNumber);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyMachine, testAssembly);
                await PricingTestHelper.CleanupEntities(bid, templateQuantity, templateHeight, templatePrice, templateProfile, machineInstance01,
                        machineInstance02, machineInstance03, machineProfile01, machineProfile02, testMachine, testTemplate);
            }
        }

        [TestMethod]
        public async Task AssemblyCompileAndComputeMeasurementTest()
        {
            short bid = 1;
            int assemblyID = -99;

            var apiContext = PricingTestHelper.GetMockCtx(bid);

            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int currentVariableID = -99;

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable assemblyHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
            };

            AssemblyVariable assemblyWidth = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
            };

            AssemblyVariable assemblyAreaInFeet = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AreaInFeet",
                Label = "AreaInFeet",
                DataType = DataType.Number,
                DefaultValue = "=Height.Infeet * width.infeet",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = 1,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyHeight,
                    assemblyWidth,
                    assemblyAreaInFeet,
                    assemblyPrice,
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

                AssemblyCompilationResponse compileResult = await assemblyGen.TestAssemblyCompilation();
                Assert.IsNotNull(compileResult);
                Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));

                ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);
                Assert.IsNotNull(cbelAssembly);

                ICBELComputeResult result = cbelAssembly.Compute();

                Assert.IsNotNull(result);

                IVariableData areaInFeetVar = result.VariableData.FirstOrDefault(x => x.VariableName == "AreaInFeet");
                Assert.IsNotNull(areaInFeetVar);
                Assert.IsNull(areaInFeetVar.Value);


                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    QuantityOV = true,
                    IsVended = true,
                    VariableData = new List<IVariableData>()
                    {
                        new VariableData()
                        {
                            VariableName = "Height",
                            IsOV = true,
                            Value = "120",
                        },
                        new VariableData()
                        {
                            VariableName = "Width",
                            IsOV = true,
                            Value = "12",
                        }
                    }
                };
                result = cbelAssembly.Compute(ovValues);

                Assert.IsNotNull(result);

                areaInFeetVar = result.VariableData.FirstOrDefault(x => x.VariableName == "AreaInFeet");
                Assert.IsNotNull(areaInFeetVar);
                Assert.AreEqual(10m, decimal.Parse(areaInFeetVar.Value));

            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyHeight, assemblyWidth, assemblyAreaInFeet, assemblyPrice);
                await PricingTestHelper.CleanupEntities(bid, assembly);
            }
        }


        [TestMethod]
        [DataRow(null, null, null, null, null)]
        [DataRow(2.00, null, 3.00, null, 6.00)]
        [DataRow(1.00, Unit.Foot, 3.00, null, 36.00)]
        [DataRow(1.00, Unit.Foot, 1.00, Unit.Yard, 432.00)]
        public async Task AssemblyCompileAndComputeMeasurementUnitTest(
              double? _heightOverride, Unit? _heightUnit
            , double? _widthOverride, Unit? _widthUnit
            , double? _areaResult
            )
        {
            decimal? heightOverride = (decimal?)_heightOverride;
            Unit heightUnit = _heightUnit ?? Unit.None;
            decimal? widthOverride = (decimal?)_widthOverride;
            Unit widthUnit = _widthUnit ?? Unit.None;
            decimal? areaResult = (decimal?)_areaResult;

            short bid = 1;
            int assemblyID = -99;

            var apiContext = PricingTestHelper.GetMockCtx(bid);

            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int currentVariableID = -99;

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable assemblyHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
            };

            AssemblyVariable assemblyWidth = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
            };

            AssemblyVariable assemblyAreaInFeet = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Area",
                Label = "Area",
                DataType = DataType.Number,
                DefaultValue = "=Height.Value * width.Value",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
                UnitID = Unit.SquareInch,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = 1,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyHeight,
                    assemblyWidth,
                    assemblyAreaInFeet,
                    assemblyPrice,
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

                AssemblyCompilationResponse compileResult = await assemblyGen.TestAssemblyCompilation();
                Assert.IsNotNull(compileResult);
                Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));

                ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);
                Assert.IsNotNull(cbelAssembly);

                CBELOverriddenValues ovValues = null;

                if (heightOverride.HasValue || widthOverride.HasValue)
                {
                    ovValues = new CBELOverriddenValues()
                    {
                        ComponentID = assemblyID,
                        VariableData = new List<IVariableData>()
                    };

                    if (heightOverride.HasValue)
                        ovValues.VariableData.Add(new VariableData
                        {
                            VariableName = "Height",
                            IsOV = true,
                            Value = heightOverride.Value.ToString(),
                            Unit = heightUnit
                        });

                    if (widthOverride.HasValue)
                        ovValues.VariableData.Add(new VariableData
                        {
                            VariableName = "Width",
                            IsOV = true,
                            Value = widthOverride.Value.ToString(),
                            Unit = widthUnit
                        });
                }

                ICBELComputeResult result = cbelAssembly.Compute(ovValues);

                Assert.IsNotNull(result);

                IVariableData heightVar = result.VariableData.FirstOrDefault(x => x.VariableName == "Height");
                Assert.IsNotNull(heightVar);

                if (heightOverride.HasValue)
                {
                    Assert.AreEqual(heightOverride, decimal.Parse(heightVar.Value));

                    if (heightUnit == Unit.None)
                        Assert.AreEqual(Unit.Inch, heightVar.Unit);
                    else
                        Assert.AreEqual(heightUnit, heightVar.Unit);
                }
                else
                    Assert.IsNull(heightVar.Value);

                IVariableData widthVar = result.VariableData.FirstOrDefault(x => x.VariableName == "Width");
                Assert.IsNotNull(widthVar);

                if (widthOverride.HasValue)
                {
                    Assert.AreEqual(widthOverride, decimal.Parse(widthVar.Value));

                    if (widthUnit == Unit.None)
                        Assert.AreEqual(Unit.Inch, widthVar.Unit);
                    else
                        Assert.AreEqual(widthUnit, widthVar.Unit);
                }
                else
                    Assert.IsNull(widthVar.Value);

                IVariableData areaVar = result.VariableData.FirstOrDefault(x => x.VariableName == "Area");
                Assert.IsNotNull(areaVar);

                if (areaResult.HasValue)
                    Assert.AreEqual(areaResult, decimal.Parse(areaVar.Value));
                else
                    Assert.IsNull(areaVar.Value);

                Assert.AreEqual(Unit.SquareInch, areaVar.Unit);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyHeight, assemblyWidth, assemblyAreaInFeet, assemblyPrice);
                await PricingTestHelper.CleanupEntities(bid, assembly);
            }
        }


        [TestMethod]
        public async Task AssemblyCompileAndComputeMeasurementConversionsTest()
        {
            short bid = 1;
            int assemblyID = -99;

            var apiContext = PricingTestHelper.GetMockCtx(bid);
            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int currentVariableID = -99;

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable assemblyInputInHours = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "InputInHours",
                Label = "InputInHours",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Hour,
                IsFormula = false,
            };

            AssemblyVariable assemblyHoursInHours = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "HoursInHours",
                Label = "HoursInHours",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInHours.InHours)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyHoursInMinutes = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "HoursInMinutes",
                Label = "HoursInMinutes",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInHours.InMinutes)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyHoursInSeconds = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "HoursInSeconds",
                Label = "HoursInSeconds",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInHours.InSeconds)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyInputInMinutes = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "InputInMinutes",
                Label = "InputInMinutes",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Minute,
                IsFormula = false,
            };


            AssemblyVariable assemblyMinutesInHours = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "MinutesInHours",
                Label = "MinutesInHours",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInMinutes.InHours)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyMinutesInMinutes = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "MinutesInMinutes",
                Label = "MinutesInMinutes",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInMinutes.InMinutes)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyMinutesInSeconds = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "MinutesInSeconds",
                Label = "MinutesInSeconds",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInMinutes.InSeconds)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyInputInSeconds = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "InputInSeconds",
                Label = "InputInSeconds",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Second,
                IsFormula = false,
            };


            AssemblyVariable assemblySecondsInHours = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "SecondsInHours",
                Label = "SecondsInHours",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInSeconds.InHours)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblySecondsInMinutes = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "SecondsInMinutes",
                Label = "SecondsInMinutes",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInSeconds.InMinutes)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblySecondsInSeconds = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "SecondsInSeconds",
                Label = "SecondsInSeconds",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=T(InputInSeconds.InSeconds)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };


            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = false,
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = 1,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyInputInHours,
                    assemblyHoursInHours,
                    assemblyHoursInMinutes,
                    assemblyHoursInSeconds,
                    assemblyInputInMinutes,
                    assemblyMinutesInHours,
                    assemblyMinutesInMinutes,
                    assemblyMinutesInSeconds,
                    assemblyInputInSeconds,
                    assemblySecondsInHours,
                    assemblySecondsInMinutes,
                    assemblySecondsInSeconds,
                    assemblyPrice,
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

                AssemblyCompilationResponse compileResult = await assemblyGen.TestAssemblyCompilation();
                Assert.IsNotNull(compileResult);
                Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));

                ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);
                Assert.IsNotNull(cbelAssembly);

                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    QuantityOV = true,
                    IsVended = true,
                    VariableData = new List<IVariableData>()
                    {
                        new VariableData()
                        {
                            VariableName = "InputInHours",
                            IsOV = true,
                            Value = "2",
                        },
                        new VariableData()
                        {
                            VariableName = "InputInMinutes",
                            IsOV = true,
                            Value = "180",
                        },
                        new VariableData()
                        {
                            VariableName = "InputInSeconds",
                            IsOV = true,
                            Value = "360",
                        }
                    }
                };
                ICBELComputeResult result = cbelAssembly.Compute(ovValues);

                Assert.IsNotNull(result);

                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);

                IVariableData varHoursInHours = result.VariableData.FirstOrDefault(x => x.VariableName == "HoursInHours");
                Assert.IsNotNull(varHoursInHours);
                Assert.AreEqual(2m, decimal.Parse(varHoursInHours.Value));

                IVariableData varHoursInMinutes = result.VariableData.FirstOrDefault(x => x.VariableName == "HoursInMinutes");
                Assert.IsNotNull(varHoursInMinutes);
                Assert.AreEqual(120m, decimal.Parse(varHoursInMinutes.Value));

                IVariableData varHoursInSeconds = result.VariableData.FirstOrDefault(x => x.VariableName == "HoursInSeconds");
                Assert.IsNotNull(varHoursInSeconds);
                Assert.AreEqual(7200m, decimal.Parse(varHoursInSeconds.Value));


                IVariableData varMinutesInHours = result.VariableData.FirstOrDefault(x => x.VariableName == "MinutesInHours");
                Assert.IsNotNull(varMinutesInHours);
                Assert.AreEqual(3m, decimal.Parse(varMinutesInHours.Value));

                IVariableData varMinutesInMinutes = result.VariableData.FirstOrDefault(x => x.VariableName == "MinutesInMinutes");
                Assert.IsNotNull(varMinutesInMinutes);
                Assert.AreEqual(180m, decimal.Parse(varMinutesInMinutes.Value));

                IVariableData varMinutesInSeconds = result.VariableData.FirstOrDefault(x => x.VariableName == "MinutesInSeconds");
                Assert.IsNotNull(varMinutesInSeconds);
                Assert.AreEqual(10800m, decimal.Parse(varMinutesInSeconds.Value));


                IVariableData varSecondsInHours = result.VariableData.FirstOrDefault(x => x.VariableName == "SecondsInHours");
                Assert.IsNotNull(varSecondsInHours);
                Assert.AreEqual(0.1m, decimal.Parse(varSecondsInHours.Value));

                IVariableData varSecondsInMinutes = result.VariableData.FirstOrDefault(x => x.VariableName == "SecondsInMinutes");
                Assert.IsNotNull(varSecondsInMinutes);
                Assert.AreEqual(6m, decimal.Parse(varSecondsInMinutes.Value));

                IVariableData varSecondsInSeconds = result.VariableData.FirstOrDefault(x => x.VariableName == "SecondsInSeconds");
                Assert.IsNotNull(varSecondsInSeconds);
                Assert.AreEqual(360m, decimal.Parse(varSecondsInSeconds.Value));
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyInputInHours,
                                                             assemblyHoursInHours,
                                                             assemblyHoursInMinutes,
                                                             assemblyHoursInSeconds,
                                                             assemblyInputInMinutes,
                                                             assemblyMinutesInHours,
                                                             assemblyMinutesInMinutes,
                                                             assemblyMinutesInSeconds,
                                                             assemblyInputInSeconds,
                                                             assemblySecondsInHours,
                                                             assemblySecondsInMinutes,
                                                             assemblySecondsInSeconds,
                                                             assemblyPrice,
                                                             assembly);
            }
        }

        [TestMethod]
        public async Task AssemblyCompileWithSpecialCharactersInNameTest()
        {
            short bid = 1;
            int assemblyID = -99;

            var apiContext = PricingTestHelper.GetMockCtx(bid);

            string assemblyName = @"Assembly - ' ({}; \ ""
) - " + DateTime.Now.ToString("yyyyMMdd_HHmm");

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyData assembly = new AssemblyData()
            {
                BID = 1,
                ID = assemblyID,
                Name = assemblyName,
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

                AssemblyCompilationResponse compileResult = await assemblyGen.TestAssemblyCompilation();
                Assert.IsNotNull(compileResult);
                Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assembly);
            }
        }

        [TestMethod]
        [DataRow(5, "3", true, "15", null)]
        [DataRow(2, "3", true, "6", null)]
        public async Task CBELAssemblyComputeQuantityPerItemTimesLineItemQuantity_Test(
            double doubleLineItemQuantityValue = 0,
            string quantityPerItemValue = "= IF( AssemblyQuantityOV, 0, AssemblyQuantity.Value / IF(LineItemQuantity = 0, 1, LineItemQuantity) )",
            bool useQuantityFormula = false,
            string expectedResult = "",
            Exception expectedException = null)
        {
            short bid = 1;
            int currentVariableID = -99;
            int assemblyID = -99;
            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            decimal lineItemQuantityValue = Convert.ToDecimal(doubleLineItemQuantityValue);

            CBELAssemblyGenerator gen = GetCBELTestAssemblyGeneratorWithDefaultElements();
            EF.ApiContext ctx = GetMockCtx(bid);

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(ctx, cache, logger);

            AssemblyVariable assemblyQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                DefaultValue = quantityPerItemValue,
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true
            };

            AssemblyVariable totalQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "TotalQuantity",
                Label = "Total Quantity",
                DataType = DataType.Number,
                DefaultValue = "=LineItemQuantity*AssemblyQuantity.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = useQuantityFormula
            };

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyQuantity,
                    totalQuantity
                }
            };

            ctx.AssemblyData.Add(assemblyData);
            ctx.SaveChanges();


            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assemblyData);
                ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);
                Assert.IsNotNull(cbelAssembly);

                CBELOverriddenValues ovValues = CBELAssemblyHelper.ComponentPriceRequestToComputeRequest(
                    new ComponentPriceRequest()
                    {
                        ComponentID = currentVariableID--,
                        TotalQuantityOV = false,
                        LineItemQuantity = lineItemQuantityValue,
                    });

                ICBELComputeResult result = cbelAssembly.Compute(ovValues);
                Assert.IsNotNull(result);

                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);

                IVariableData quantity = result.VariableData.FirstOrDefault(x => x.VariableName == "TotalQuantity");

                // check if quantity formula worked
                Assert.IsNotNull(quantity);
                Assert.AreEqual(expectedResult, quantity.Value);
            }
            catch (Exception afe)
            {
                if (expectedException != afe)
                {
                    Assert.Fail();
                }
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, totalQuantity);
                await PricingTestHelper.CleanupEntities(bid, assemblyData);
            }

        }

        [TestMethod]
        public async Task AssemblyCompileAndComputeWithLabelsWithMismatchedValueTypesTest()
        {
            short bid = 1;
            int assemblyID = -99;

            var apiContext = PricingTestHelper.GetMockCtx(bid);

            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int currentVariableID = -99;

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable assemblyLabelWithNumber = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "LabelWithNumber",
                Label = "LabelWithNumber",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=5*10",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyLabelWithString = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "LabelWithString",
                Label = "LabelWithString",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=\"Test\"",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyLabelWithBoolean = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "LabelWithBoolean",
                Label = "LabelWithBoolean",
                DataType = DataType.String,
                IsFormula = true,
                DefaultValue = "=IsEven(2)",
                ElementType = AssemblyElementType.SingleLineLabel,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = false,
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = 1,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyLabelWithNumber,
                    assemblyLabelWithString,
                    assemblyLabelWithBoolean,
                    assemblyPrice,
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

                AssemblyCompilationResponse compileResult = await assemblyGen.TestAssemblyCompilation();
                Assert.IsNotNull(compileResult);
                Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));

                ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);
                Assert.IsNotNull(cbelAssembly);

                ICBELComputeResult result = cbelAssembly.Compute();

                Assert.IsNotNull(result);

                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);

                IVariableData varLabelWithNumber = result.VariableData.FirstOrDefault(x => x.VariableName == "LabelWithNumber");
                Assert.IsNotNull(varLabelWithNumber);
                Assert.AreEqual("50", varLabelWithNumber.Value);

                IVariableData varLabelWithString = result.VariableData.FirstOrDefault(x => x.VariableName == "LabelWithString");
                Assert.IsNotNull(varLabelWithString);
                Assert.AreEqual("Test", varLabelWithString.Value);

                IVariableData varLabelWithBoolean = result.VariableData.FirstOrDefault(x => x.VariableName == "LabelWithBoolean");
                Assert.IsNotNull(varLabelWithBoolean);
                Assert.AreEqual("True", varLabelWithBoolean.Value);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyLabelWithNumber,
                                                             assemblyLabelWithString,
                                                             assemblyLabelWithBoolean,
                                                             assemblyPrice,
                                                             assembly);
            }
        }

        [TestMethod]
        public async Task AssemblyCompileAndComputeWithNumbersWithMismatchedValueTypesTest()
        {
            short bid = 1;
            int assemblyID = -99;

            var apiContext = PricingTestHelper.GetMockCtx(bid);

            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int currentVariableID = -99;

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable assemblyNumberWithNumber = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "NumberWithNumber",
                Label = "NumberWithNumber",
                DataType = DataType.Number,
                IsFormula = true,
                DefaultValue = "=5*10",
                ElementType = AssemblyElementType.Number,
            };

            AssemblyVariable assemblyNumberWithStringInvalid = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "NumberWithStringInvalid",
                Label = "NumberWithStringInvalid",
                DataType = DataType.Number,
                IsFormula = true,
                DefaultValue = "=\"Test\"",
                ElementType = AssemblyElementType.Number,
            };

            AssemblyVariable assemblyNumberWithStringValid = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "NumberWithStringValid",
                Label = "NumberWithStringValid",
                DataType = DataType.Number,
                IsFormula = true,
                DefaultValue = "=\"123\"",
                ElementType = AssemblyElementType.Number,
            };

            AssemblyVariable assemblyNumberWithBoolean = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "NumberWithBoolean",
                Label = "NumberWithBoolean",
                DataType = DataType.Number,
                IsFormula = true,
                DefaultValue = "=IsEven(2)",
                ElementType = AssemblyElementType.Number,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = false,
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = 1,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyNumberWithNumber,
                    assemblyNumberWithStringInvalid,
                    assemblyNumberWithStringValid,
                    assemblyNumberWithBoolean,
                    assemblyPrice,
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

                AssemblyCompilationResponse compileResult = await assemblyGen.TestAssemblyCompilation();
                Assert.IsNotNull(compileResult);
                Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));

                ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);
                Assert.IsNotNull(cbelAssembly);

                ICBELComputeResult result = cbelAssembly.Compute();

                Assert.IsNotNull(result);

                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);

                IVariableData varNumberWithNumber = result.VariableData.FirstOrDefault(x => x.VariableName == "NumberWithNumber");
                Assert.IsNotNull(varNumberWithNumber);
                Assert.AreEqual("50", varNumberWithNumber.Value);

                IVariableData varNumberWithStringInvalid = result.VariableData.FirstOrDefault(x => x.VariableName == "NumberWithStringInvalid");
                Assert.IsNotNull(varNumberWithStringInvalid);
                Assert.AreEqual(null, varNumberWithStringInvalid.Value);

                IVariableData varNumberWithStringValid = result.VariableData.FirstOrDefault(x => x.VariableName == "NumberWithStringValid");
                Assert.IsNotNull(varNumberWithStringValid);
                Assert.AreEqual("123", varNumberWithStringValid.Value);

                IVariableData varNumberWithBoolean = result.VariableData.FirstOrDefault(x => x.VariableName == "NumberWithBoolean");
                Assert.IsNotNull(varNumberWithBoolean);
                Assert.AreEqual("1", varNumberWithBoolean.Value);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyNumberWithNumber,
                                                             assemblyNumberWithStringInvalid,
                                                             assemblyNumberWithStringValid,
                                                             assemblyNumberWithBoolean,
                                                             assemblyPrice,
                                                             assembly);
            }
        }

        [TestMethod]
        public async Task AssemblyCompileAndComputeWithCheckboxesWithMismatchedValueTypesTest()
        {
            short bid = 1;
            int assemblyID = -99;

            var apiContext = PricingTestHelper.GetMockCtx(bid);

            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int currentVariableID = -99;

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable assemblyCheckboxWithNumber = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "CheckboxWithNumber",
                Label = "CheckboxWithNumber",
                DataType = DataType.Boolean,
                IsFormula = true,
                DefaultValue = "=5*10",
                ElementType = AssemblyElementType.Checkbox,
            };

            AssemblyVariable assemblyCheckboxWithStringInvalid = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "CheckboxWithStringInvalid",
                Label = "CheckboxWithStringInvalid",
                DataType = DataType.Boolean,
                IsFormula = true,
                DefaultValue = "=\"Blah\"",
                ElementType = AssemblyElementType.Checkbox,
            };

            AssemblyVariable assemblyCheckboxWithStringValid = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "CheckboxWithStringValid",
                Label = "CheckboxWithStringValid",
                DataType = DataType.Boolean,
                IsFormula = true,
                DefaultValue = "=\"Yes\"",
                ElementType = AssemblyElementType.Checkbox,
            };

            AssemblyVariable assemblyCheckboxWithBoolean = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "CheckboxWithBoolean",
                Label = "CheckboxWithBoolean",
                DataType = DataType.Boolean,
                IsFormula = true,
                DefaultValue = "=IsEven(2)",
                ElementType = AssemblyElementType.Checkbox,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = false,
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = 1,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyCheckboxWithNumber,
                    assemblyCheckboxWithStringInvalid,
                    assemblyCheckboxWithStringValid,
                    assemblyCheckboxWithBoolean,
                    assemblyPrice,
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            try
            {
                CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

                AssemblyCompilationResponse compileResult = await assemblyGen.TestAssemblyCompilation();
                Assert.IsNotNull(compileResult);
                Assert.AreEqual(true, compileResult.Success, compileResult.Errors == null ? null : string.Join("\n", compileResult.Errors.Select(x => x.Message)));

                ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);
                Assert.IsNotNull(cbelAssembly);

                ICBELComputeResult result = cbelAssembly.Compute();

                Assert.IsNotNull(result);

                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);

                IVariableData varCheckboxWithNumber = result.VariableData.FirstOrDefault(x => x.VariableName == "CheckboxWithNumber");
                Assert.IsNotNull(varCheckboxWithNumber);
                Assert.AreEqual("true", varCheckboxWithNumber.Value);

                IVariableData varCheckboxWithStringInvalid = result.VariableData.FirstOrDefault(x => x.VariableName == "CheckboxWithStringInvalid");
                Assert.IsNotNull(varCheckboxWithStringInvalid);
                Assert.AreEqual("false", varCheckboxWithStringInvalid.Value);

                IVariableData varCheckboxWithStringValid = result.VariableData.FirstOrDefault(x => x.VariableName == "CheckboxWithStringValid");
                Assert.IsNotNull(varCheckboxWithStringValid);
                Assert.AreEqual("true", varCheckboxWithStringValid.Value);

                IVariableData varCheckboxWithBoolean = result.VariableData.FirstOrDefault(x => x.VariableName == "CheckboxWithBoolean");
                Assert.IsNotNull(varCheckboxWithBoolean);
                Assert.AreEqual("true", varCheckboxWithBoolean.Value);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyCheckboxWithNumber,
                                                             assemblyCheckboxWithStringInvalid,
                                                             assemblyCheckboxWithStringValid,
                                                             assemblyCheckboxWithBoolean,
                                                             assemblyPrice,
                                                             assembly);
            }
        }

        [TestMethod]
        public async Task LinkedMachineCompileAndComputeVariableReferenceTest()
        {
            short bid = 1;
            int assemblyID = -99;
            int machineTemplateID = -98;
            short machineID = -99;
            int currentVariableID = -99;
            int currentProfileID = -99;

            string assemblyName = "TestAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineTemplateName = "TestMachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineName = "TestMachine" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            AssemblyVariable templateCostPerHour = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "MyCostPerHour",
                Label = "MyCostPerHour",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "=MachineCostPerHour",
                IsFormula = true,
                IsRequired = false,
            };

            AssemblyVariable templateQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "1",
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable templateHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
            };

            AssemblyVariable templateWidth = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
            };

            AssemblyVariable templateArea = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Area",
                Label = "Area",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "=Height.Value * Width.Value",
                IsFormula = true
            };

            AssemblyVariable templatePrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable templateProfile = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Profile",
                Label = "Profile",
                DataType = DataType.String,
                DefaultValue = null,
                ElementType = AssemblyElementType.DropDown,
                DisplayType = CustomFieldNumberDisplayType.General,
                ListDataType = DataType.String,
                IsFormula = false,
            };

            AssemblyData testTemplate = new AssemblyData()
            {
                BID = bid,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    templateQuantity,
                    templateHeight,
                    templateWidth,
                    templateArea,
                    templatePrice,
                    templateProfile,
                    templateCostPerHour,
                }
            };

            MachineProfile machineProfile01 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = true,
                Name = "Profile 01",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{}",
            };

            MachineData testMachine = new MachineData()
            {
                BID = bid,
                ID = machineID,
                ExpenseAccountID = 5000,
                Name = machineName,
                MachineTemplateID = machineTemplateID,
                EstimatingCostPerHourFormula = "1",
                Profiles = new List<MachineProfile>()
                {
                    machineProfile01,
                },
            };

            AssemblyVariable assemblyQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "AssemblyQuantity",
                Label = "Assembly Quantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=AssemblyQuantity.Value * FixedPrice",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyVariable assemblyMachine = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "TestMachine",
                Label = "Test Machine",
                DataType = DataType.MachinePart,
                DefaultValue = machineName,
                ElementType = AssemblyElementType.LinkedMachine,
                IsFormula = false,
                LinkedMachineID = machineID,
                ConsumptionDefaultValue = "=AssemblyQuantity.Value",
                IsConsumptionFormula = true,

            };

            AssemblyVariable assemblyMachineArea = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "TestMachineArea",
                Label = "TestMachineArea",
                DataType = DataType.Number,
                DefaultValue = $"=TestMachine.Area.Value",
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
            };

            AssemblyData testAssembly = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = assemblyName,
                AssemblyType = AssemblyType.Product,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyQuantity,
                    assemblyPrice,
                    assemblyMachine,
                    assemblyMachineArea,
                }
            };

            try
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyMachine, assemblyMachineArea, testAssembly);
                await PricingTestHelper.CleanupEntities(bid, templateQuantity, templateHeight, templateWidth, templateArea, templatePrice, templateProfile, machineProfile01, testMachine, testTemplate, templateCostPerHour);

            }
            catch
            {
                ;
            }

            ctx.MachineData.Add(testMachine);
            ctx.AssemblyData.Add(testTemplate);
            ctx.AssemblyData.Add(testAssembly);
            ctx.MachineProfile.Add(machineProfile01);
            ctx.SaveChanges();

            try
            {
                var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, assemblyID, ClassType.Assembly.ID(), 1, cache, logger, bid);

                Assert.AreEqual(0, compilationResponse.ErrorCount);
                Assert.IsNotNull(compilationResponse.Assembly);


                ICBELComputeResult result = compilationResponse.Assembly.Compute();

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                ICBELAssembly myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);

                ICBELVariable varMachineArea = myAssembly.Variables.Where(x => x.Key == "TestMachineArea").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(varMachineArea, typeof(ICBELVariableComputed));
                Assert.IsNull(((ICBELVariableComputed)varMachineArea).AsNumber);


                CBELOverriddenValues ovValues = new CBELOverriddenValues()
                {
                    ComponentID = assemblyID,
                    Quantity = 1,
                    ParentQuantity = 1,
                    QuantityOV = true,
                    OVChildValues = new List<ICBELOverriddenValues>
                    {
                        new CBELOverriddenValues()
                        {
                            VariableName = "TestMachine",
                            VariableData = new List<IVariableData>()
                            {
                                new VariableData()
                                {
                                    VariableName = "Height",
                                    IsOV = true,
                                    Value = "4"
                                },
                                new VariableData()
                                {
                                    VariableName = "Width",
                                    IsOV = true,
                                    Value = "6"
                                }
                            }
                        }
                    }
                };

                result = compilationResponse.Assembly.Compute(ovValues);

                Assert.IsNotNull(result);

                Assert.IsFalse(result.HasErrors, result.ErrorText);
                Assert.IsFalse(result.HasValidationFailures);
                myAssembly = result.Assembly;
                Assert.IsNotNull(myAssembly);

                varMachineArea = myAssembly.Variables.Where(x => x.Key == "TestMachineArea").Select(x => x.Value).FirstOrDefault();
                Assert.IsInstanceOfType(varMachineArea, typeof(ICBELVariableComputed));
                Assert.AreEqual(24, ((ICBELVariableComputed)varMachineArea).AsNumber);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, assemblyQuantity, assemblyPrice, assemblyMachine, assemblyMachineArea, testAssembly);
                await PricingTestHelper.CleanupEntities(bid, templateQuantity, templateHeight, templateWidth, templateArea, templatePrice, templateProfile, machineProfile01, testMachine, testTemplate, templateCostPerHour);
            }
        }

        [TestMethod]
        [DataRow("5", "10", "10")]
        [DataRow("5", null, "5")]
        public async Task MachineWithMachineProfileAndMachineProperty_Test(string defaultValue, string overrideValue, string expectedValue)
        {
            short bid = 1;
            int machineTemplateID = -98;
            short machineID = -99;
            int currentVariableID = -99;
            int currentProfileID = -99;

            string machineTemplateName = "TestMachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            string machineName = "TestMachine" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            var ctx = GetMockCtx(bid);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            MachineProfile machineProfile01 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = true,
                Name = "Profile 01",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"TestProperty1\":{\"Value\":null,\"ValueOV\":false}}",
            };

            MachineProfile machineProfile02 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 02",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"TestProperty1\":{\"Value\":null,\"ValueOV\":false}}",
            };

            MachineProfile machineProfile03 = new MachineProfile()
            {
                BID = bid,
                ID = currentProfileID--,
                IsActive = true,
                IsDefault = false,
                Name = "Profile 03",
                MachineID = machineID,
                MachineTemplateID = machineTemplateID,
                AssemblyOVDataJSON = "{\"TestProperty1\":{\"Value\":null,\"ValueOV\":false}}",
            };

            string ovData = "{\"TestProperty1\":{\"Value\":null,\"ValueOV\":false}}";

            if (overrideValue != null)
                ovData = "{\"TestProperty1\":{\"Value\":\"" + overrideValue + "\",\"ValueOV\": true }}";
                

            MachineData testMachine = new MachineData()
            {
                BID = bid,
                ID = machineID,
                ExpenseAccountID = 5000,
                Name = machineName,
                MachineTemplateID = machineTemplateID,
                EstimatingCostPerHourFormula = "1",
                Profiles = new List<MachineProfile>()
                {
                    machineProfile01,
                    machineProfile02,
                    machineProfile03,
                },
                AssemblyOVDataJSON = ovData
            };

            AssemblyVariable templateTestProperty = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "TestProperty1",
                Label = "Test Property 1",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = defaultValue,
                IsFormula = true,
                IsRequired = true,
                UnitID = Unit.None
            };

            AssemblyData testTemplate = new AssemblyData()
            {
                BID = bid,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    templateTestProperty,
                },

            };

            ctx.MachineData.Add(testMachine);
            ctx.AssemblyData.Add(testTemplate);
            ctx.SaveChanges();

            try
            {
                var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, testMachine.ID, ClassType.Machine.ID(), 1, cache, logger, bid);
                
                Assert.AreEqual(0, compilationResponse.ErrorCount);
                Assert.IsNotNull(compilationResponse.Assembly);

                var property = compilationResponse.Assembly.GetType().GetProperty("TestProperty1");
                dynamic result = property.GetValue(compilationResponse.Assembly);

                Assert.AreEqual(expectedValue, result.AsString);
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(bid, templateTestProperty, machineProfile01, machineProfile02, machineProfile03, testMachine, testTemplate);
            }
        }
    }
}