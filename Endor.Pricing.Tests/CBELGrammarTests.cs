﻿using Endor.CBEL.Common;
using Endor.CBEL.Grammar;
using Endor.CBEL.ObjectGeneration;
using Endor.Models;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class CBELGrammarTests
    {
        #region CBELParser
        private Parser TestParser => new Parser(new CBELGrammar());

        private Token GetFirstToken(Parser parser, string input, bool useTerminator = true)
        {
            var tree = parser.Parse(input);
            var first = tree.Tokens.First();

            if (first.IsError())
                return first;

            return first;
        }


        [TestMethod]
        public void TestLineComment()
        {
            Parser parser = TestParser;

            var tree = parser.Parse("// Line Comment");
            var first = tree.Tokens.First();

            Assert.AreEqual(TokenCategory.Comment, tree.Tokens.First().Category);
            Assert.AreEqual("line-comment", tree.Tokens.First().Terminal.Name);
        }

        [TestMethod]
        public void TestBlockComment()
        {
            Parser parser = TestParser;

            var token = GetFirstToken(parser, "/* Block Comment \n 01 */");
            Assert.AreEqual(TokenCategory.Comment, token.Category);
            Assert.AreEqual("block-comment", token.Terminal.Name);
        }

        [TestMethod]
        public void TestBinaryExprWithThreeOperands()
        {
            var sourceText = "2 * 3 + 1";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("((2m * 3m) + 1m)", cs);
        }

        [TestMethod]
        public void TestDivision()
        {
            var sourceText = "2 / 3";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("(2m / (decimal) 3m)", cs);
        }

        [TestMethod]
        public void TestBinaryExprWithThreeOperandsNoSpaceInBetween()
        {
            var sourceText = "2*3+1";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("((2m * 3m) + 1m)", cs);
        }

        [TestMethod]
        public async Task TestVariableCaseSensitivity()
        {
            var assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "Height",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "0",
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Width",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "0",
                        DataType = DataType.Number,
                    },
                    new AssemblyVariable()
                    {
                        Name = "Area",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "=wiDtH.Asnumber * hEiGhT.vAlue",
                        IsFormula = true,
                        DataType = DataType.Number,
                    },
                }
            };

            var sourceText = "heiGht.asNumBer * 2";
            Parser parser = new Parser(new CBELGrammar(assemblyData));
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();
            
            Assert.AreEqual("(Height?.AsNumber * 2m)", cs);

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsTrue(result.Success);
            Assert.IsNull(result.Errors);
        }

        [TestMethod]
        public async Task TestAssemblyPropertyCaseSensitivity()
        {
            var assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "CalculatedCost",
                        ElementType = AssemblyElementType.Number,
                        DefaultValue = "=totalcost",
                        IsFormula = true,
                        DataType = DataType.Number,
                    },
                }
            };

            var sourceText = "totalcost";
            Parser parser = new Parser(new CBELGrammar(assemblyData));
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("TotalCost", cs);

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsTrue(result.Success);
            Assert.IsNull(result.Errors);
        }

        [TestMethod]
        public void TestVariableParsingWithTwoTokens()
        {
            var sourceText = "Height.Value * 2";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("(Height.Value * 2m)", cs);
        }

        [TestMethod]
        public void TestComponentParsing()
        {
            var sourceText = "NameOfMaterialVariable.Height * 2";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("(NameOfMaterialVariable.Height * 2m)", cs);
        }

        [TestMethod]
        public void TestVariableParsingWithTwoTokensConvertingToVariableElementName()
        {
            var sourceText = "Height.Value * 2";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("(Height.Value * 2m)", cs);
        }

        [TestMethod]
        public void TestVariableParsingWithThreeTokens()
        {
            var sourceText = "MyVariable.SubProperty.Name * 2";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("(MyVariable.SubProperty.Name * 2m)", cs);
        }

        [TestMethod]
        public void TestBooleanAndNumericLiteralIfExpr()
        {
            List<string> sourceTextList = new List<string>();
            sourceTextList.Add("IF(FALSE, 0, 1)");
            sourceTextList.Add("IF(false, 0, 1)");
            sourceTextList.Add("iF(FALSE, 0, 1)");
            sourceTextList.Add("iF(false, 0, 1)");
            sourceTextList.Add("if(FALSE, 0, 1)");
            sourceTextList.Add("if(fALSE, 0, 1)");
            sourceTextList.Add("if(false, 0, 1)");
            sourceTextList.Add("iF(fALSE, 0, 1)");
            Parser parser = TestParser;
            foreach(string sourceText in sourceTextList)
            {
                ParseTree tree = parser.Parse(sourceText);
                Assert.IsFalse(tree.HasErrors());
                TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
                Assert.IsNotNull(program);

                string cs = program.ToCS();


                Assert.AreEqual("((false) ? (0m) : (1m))", cs);
            }
        }

        [TestMethod]
        public void TestBooleanExpressionAndNumericLiteralIfExpr()
        {
            var sourceText = "IF(2 * 3 > 5, 0, 1)";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("((((2m * 3m) > 5m)) ? (0m) : (1m))", cs);
        }

        [TestMethod]
        public void TestMathFunctionExpression()
        {
            var sourceText = "SIN(3.1415)";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();
            Assert.AreEqual("ExcelFunctions.SIN(3.1415m)", cs);
        }

        [TestMethod]
        public void TestTextFunctionExpression()
        {
            var sourceText = "Text(3*2, \"G\")";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("ExcelFunctions.TEXT((3m * 2m),\"G\")", cs);
        }

        [TestMethod]
        public void TestConditionalOperator()
        {
            var sourceText = "IF(False, 3/0, 62)";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("((false) ? ((3m / (decimal) 0m)) : (62m))", cs);
        }

        [TestMethod]
        public void TestParenthesesFunctionExpression()
        {
            var sourceText = "(TimeTravel.Value+10)/60";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("((TimeTravel.Value + 10m) / (decimal) 60m)", cs);
        }

        [TestMethod]
        public void TestMathExponentExpression()
        {
            var sourceText = "Height.Value^2";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("Math.Pow(Height.Value, 2m)", cs);
        }

        [TestMethod]
        public void TestErrorExpr()
        {
            var sourceText = $"ERROR(\"This is a test\")";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("this.ERROR(\"This is a test\")", cs);
        }

        [TestMethod]
        public void TestNullExpr()
        {
            var sourceText = $"null";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("null", cs);
        }
        [TestMethod]
        public void TestParseErrorMessageFormatting()
        {
            var syntaxError1 = "Height.Value + ";
            var syntaxError2 = "3**5 - 34Height";
            var invalidToken = "34Height";
            var unterminatedString = "SIN(\"red, 4)";
            var parenthesisExpected = "((3+4)";
            Parser parser = TestParser;

            ParseTree tree1 = parser.Parse(syntaxError1);
            ParseResponse response1 = Endor.CBEL.Grammar.Util.CheckParseErrors(tree1);
            Assert.IsTrue(response1.Failures.Count > 0);
            Assert.AreEqual("Syntax Error.", response1.Failures[0].Message);

            ParseTree tree2 = parser.Parse(syntaxError2);
            ParseResponse response2 = Endor.CBEL.Grammar.Util.CheckParseErrors(tree2);
            Assert.IsTrue(response2.Failures.Count > 0);
            Assert.AreEqual("Syntax Error.", response2.Failures[0].Message);

            ParseTree tree3 = parser.Parse(invalidToken);
            ParseResponse response3 = Endor.CBEL.Grammar.Util.CheckParseErrors(tree3);
            Assert.IsTrue(response3.Failures.Count > 0);
            Assert.AreEqual("Invalid Token Name.", response3.Failures[0].Message);

            ParseTree tree4 = parser.Parse(unterminatedString);
            ParseResponse response4 = Endor.CBEL.Grammar.Util.CheckParseErrors(tree4);
            Assert.IsTrue(response4.Failures.Count > 0);
            Assert.AreEqual("Unterminated String.", response4.Failures[0].Message);

            ParseTree tree5 = parser.Parse(parenthesisExpected);
            ParseResponse response5 = Endor.CBEL.Grammar.Util.CheckParseErrors(tree5);
            Assert.IsTrue(response5.Failures.Count > 0);
            Assert.AreEqual("Parenthesis expected.", response5.Failures[0].Message);
        }

        [TestMethod]
        public void TestExprParseErrorsTrailingOperator()
        {
            var sourceText = "2 * 3 + 1 +";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsTrue(tree.ParserMessages[0].Message.Length > 0);
            Assert.IsNull(tree.Root);
        }

        [TestMethod]
        public void TestExprParseErrorsDoubleOperator()
        {
            var sourceText = "2 **";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsTrue(tree.ParserMessages[0].Message.Length > 0);
            Assert.IsNull(tree.Root);
        }

        [TestMethod]
        public void ParserDoesNotReturnEXCELMethods()
        {
            var sourceText = "Height.Value * Width.Value + SIN(0)";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            List<string> unknownTokens = tree.ExternalDependencies();
            Assert.IsFalse(unknownTokens.Any(t => t.Equals("Sin", StringComparison.OrdinalIgnoreCase)));
        }

        [TestMethod]
        public void ParserReturnsVariableNameDotValueAsOneIdentifier()
        {
            var sourceText = "Height.Value * Width.Value + SIN(0)";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            List<string> unknownTokens = tree.ExternalDependencies();
            Assert.AreEqual(2, unknownTokens.Count);
            Assert.AreEqual("Height", unknownTokens[0]);
            Assert.AreEqual("Width", unknownTokens[1]);
        }

        [TestMethod]
        public void ParserReturnsVariableNameDotValueAsOneIdentifier2()
        {
            var sourceText = "Height.Value + Height.Value";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            List<string> unknownTokens = tree.ExternalDependencies();
            Assert.AreEqual(1, unknownTokens.Count);
            Assert.AreEqual("Height", unknownTokens[0]);
        }

        [TestMethod]
        public void ParserReturnsVariableNameFromInsideFunctionCall()
        {
            var sourceText = "\"Middle Text: \" + MID(Quantityx.Name, 3, 15)";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            List<string> unknownTokens = tree.ExternalDependencies();
            Assert.AreEqual(1, unknownTokens.Count);
            Assert.AreEqual("Quantityx", unknownTokens[0]);
        }

        [TestMethod]
        public void ParserReturnsVariableNameDotVariableNameDotValueAsOneIdentifier()
        {
            var sourceText = "Height.OtherProperty.Value * Width.Value + SIN(0)";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            List<string> unknownTokens = tree.ExternalDependencies();
            Assert.AreEqual(2, unknownTokens.Count);
            Assert.AreEqual("Height", unknownTokens[0]);
            Assert.AreEqual("Width", unknownTokens[1]);
        }


        [TestMethod]
        public void ParserReturnsSingleInstanceOfVariable()
        {
            var sourceText = "Left(Order.Salesperson.FirstName, LEN(Order.Salesperson.FirstName) - 2)";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            List<string> unknownTokens = tree.ExternalDependencies();
            Assert.AreEqual(1, unknownTokens.Count);
            Assert.AreEqual("Order", unknownTokens[0]);

        }

        [TestMethod]
        public void ParserReturnsExcludeReservedTerms()
        {
            var sourceText = "UnitPrice + TotalCost + UnitCost + MaterialCost + LaborCost + MachineCost";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            List<string> unknownTokens = tree.ExternalDependencies();
            Assert.AreEqual(0, unknownTokens.Count);
        }

        [TestMethod]
        public void ParserReturnsWithTranslatedTerms()
        {
            var sourceText = "Quantityx.Value * Pricex.Value";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            List<string> unknownTokens = tree.ExternalDependencies();
            Assert.AreEqual(2, unknownTokens.Count);
            Assert.AreEqual("Quantityx", unknownTokens[0]);
            Assert.AreEqual("Pricex", unknownTokens[1]);
        }

        [TestMethod]
        public void TestCompanyNameToken()
        {
            var sourceText = "Company.Name";
            Parser parser = TestParser;
            ParseTree tree = parser.Parse(sourceText);
            Assert.IsFalse(tree.HasErrors());
            TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
            Assert.IsNotNull(program);

            string cs = program.ToCS();

            Assert.AreEqual("Company.Name", cs);
            Assert.AreEqual(1, program.ChildNodes.Count);
            Assert.AreEqual(2, program.ChildNodes[0].ChildNodes.Count);
            Assert.AreEqual("Company", program.ChildNodes[0].ChildNodes[0].ToString());
            Assert.AreEqual("Name", program.ChildNodes[0].ChildNodes[1].ToString());
        }

        [TestMethod]
        public void Testbooleanliteral()
        {
            List<string> sourceTextList1 = new List<string>();
            sourceTextList1.Add("TRUE");
            sourceTextList1.Add("True");
            sourceTextList1.Add("tRue");
            sourceTextList1.Add("true");
            sourceTextList1.Add("tRUE");

            List<string> sourceTextList2 = new List<string>();
            sourceTextList2.Add("FALSE");
            sourceTextList2.Add("False");
            sourceTextList2.Add("fAlse");
            sourceTextList2.Add("false");
            sourceTextList2.Add("fALSE");

            Parser parser = TestParser;
            foreach (string sourceText in sourceTextList1)
            {
                ParseTree tree = parser.Parse(sourceText);
                Assert.IsFalse(tree.HasErrors());
                TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
                Assert.IsNotNull(program);

                string cs = program.ToCS();
                Assert.AreEqual("true", cs);
            }

            foreach (string sourceText in sourceTextList2)
            {
                ParseTree tree = parser.Parse(sourceText);
                Assert.IsFalse(tree.HasErrors());
                TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
                Assert.IsNotNull(program);

                string cs = program.ToCS();
                Assert.AreEqual("false", cs);
            }
        }

        [TestMethod]
        public void TestUnaryOperation()
        {
            List<string> sourceTextList = new List<string>();

            //not true
            sourceTextList.Add("Not(True)");
            sourceTextList.Add("nOT(TRUE)");
            sourceTextList.Add("NOT(TrUE)");
            sourceTextList.Add("not(true)");

            Parser parser = TestParser;
            foreach (string sourceText in sourceTextList)
            {
                ParseTree tree = parser.Parse(sourceText);
                Assert.IsFalse(tree.HasErrors());
                TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
                Assert.IsNotNull(program);

                string cs = program.ToCS();


                Assert.AreEqual("ExcelFunctions.NOT(true)", cs);
            }


            //not false
            sourceTextList.Clear();
            sourceTextList.Add("Not(False)");
            sourceTextList.Add("nOT(FALSE)");
            sourceTextList.Add("NOT(FaLSE)");
            sourceTextList.Add("not(false)");

            foreach (string sourceText in sourceTextList)
            {
                ParseTree tree = parser.Parse(sourceText);
                Assert.IsFalse(tree.HasErrors());
                TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
                Assert.IsNotNull(program);

                string cs = program.ToCS();


                Assert.AreEqual("ExcelFunctions.NOT(false)", cs);
            }
        }

        [TestMethod]
        public async Task TestCompanyTKnownObjectReference()
        {
           
            var assemblyData = new AssemblyData()
            {
                BID = 1,
                ID = -10,

                Variables = new List<AssemblyVariable>()
                {
                    new AssemblyVariable()
                    {
                        Name = "CompanyTierTest",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.tier",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                        Name = "CompanyNameTest",
                        ElementType = AssemblyElementType.SingleLineText,
                        DataType = DataType.String,
                        DefaultValue = "=Company.naME",
                        IsFormula = true,
                    },
                    new AssemblyVariable()
                    {
                         Name = "CompanyIDTest",
                        ElementType = AssemblyElementType.Number,
                        DataType = DataType.Number,
                        DefaultValue = "=Company.ID",
                        IsFormula = true,
                    },
                }
            };

            var gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse Result = await gen.TestAssemblyCompilation();
            Assert.IsTrue(Result.Success, String.Join("\r\n", Result?.Errors?.Select(t => t.Message) ?? new List<string>()));
            
            List<string> sourceTextList = new List<string>();
            sourceTextList.Add("Company.tier");

            sourceTextList.Add("Company.Name");
            sourceTextList.Add("Company.naME");
            sourceTextList.Add("company.NAME");

            sourceTextList.Add("Company.Id");
            sourceTextList.Add("Company.id");
            sourceTextList.Add("COMPANY.ID");


            Parser parser = new Parser(new CBELGrammar(assemblyData));

            foreach (string sourceText in sourceTextList)
            {
                ParseTree tree = parser.Parse(sourceText);
                Assert.IsFalse(tree.HasErrors());
                TStatementListNode program = (TStatementListNode)tree.Root.AstNode;
                Assert.IsNotNull(program);

                string cs = program.ToCS();
                switch(cs)
                {
                    case "Company.Name":
                        Assert.AreEqual("Company.Name", cs);
                        break;
                    case "Company.ID":
                        Assert.AreEqual("Company.ID", cs);
                        break;
                    case "Company.Tier":
                        Assert.AreEqual("Company.Tier", cs);
                        break;
                    default:
                        Assert.Fail(cs);
                        break;
                }                
            }
        }

        #endregion



    }
}
