﻿using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.CBEL.Compute;
using Endor.CBEL.Elements;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Grammar;
using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Level3.Common.Tests;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Irony.Parsing;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class LinkedMachineTests
    {
        #region Initialization and Cleanup

        #region Private Variables

        private ApiContext ctx = null;
        private PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        private int glAccountID = 0;
        private const short BID = 1;
        private const int _companyID = -99;
        private const int _taxGroupID = -99;
        private const int _assemblyID = -99;
        private const int _orderID = -99;
        private const int _orderItemID = -99;
        private const int _orderItemComponentID = -99;

        private OrderData order = null;
        private OrderItemData orderItem = null;
        private OrderItemComponent orderItemComponent = null;

        #endregion

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            await CleanupDefaultPricingTests();
            await InitializeCompanyAndGLAccountIDs();
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await CleanupHelper.DeleteNegativeIDAssemblyAndMachineRelatedData(BID, ctx);

        
            var _orderItem = await ctx.OrderItemData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemID);
            if (_orderItem != null)
            {
                ctx.OrderItemData.Remove(_orderItem);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
            }

            ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
            ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
            ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
            ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == _orderID).ToArrayAsync());
            ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());

            var _order = await ctx.OrderData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderID);
            if (_order != null)
            {
                ctx.OrderData.Remove(_order);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
            }

            var _taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
            if (_taxGroup != null)
            {
                ctx.TaxGroup.Remove(_taxGroup);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }

            var _company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
            if (_company != null)
            {
                var _orders = await ctx.OrderData.Where(t => t.BID == BID && t.CompanyID == _companyID).ToListAsync();
                foreach (var o in _orders)
                {

                    ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == o.ID && t.IsOrderNote));
                    var items = ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == o.ID);
                    var itemIDs = items.Select(x => x.ID);
                    ctx.OrderItemData.RemoveRange(items);
                    ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(t => t.BID == BID && itemIDs.Contains(t.OrderItemID)));
                    ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                    ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                    ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                    ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == o.ID).ToArrayAsync());
                    ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());

                    ctx.OrderData.Remove(o);
                    await RemoveTransheaderItems(o.ID);
                }

                var _estimates = await ctx.EstimateData.Where(t => t.BID == BID && t.CompanyID == _companyID).ToListAsync();
                foreach (var e in _estimates)
                {
                    ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == e.ID));
                    var items = ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == e.ID);
                    var itemIDs = items.Select(x => x.ID);
                    ctx.OrderItemData.RemoveRange(items);
                    ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(t => t.BID == BID && itemIDs.Contains(t.OrderItemID)));
                    ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                    ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                    ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                    ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == e.ID).ToArrayAsync());
                    ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                    ctx.EstimateData.Remove(e);
                    await RemoveTransheaderItems(e.ID);
                }

                ctx.CompanyData.Remove(_company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }
        }
        
        private async Task RemoveTransheaderItems(int orderID)
        {
            var items = await ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync();
            foreach (var orderItem in items)
            {
                ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderItemID == orderItem.ID));
                ctx.OrderItemData.Remove(orderItem);
                foreach (var component in await ctx.OrderItemComponent.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
                {
                    ctx.OrderItemComponent.Remove(component);
                }
            }

            foreach (var role in await ctx.OrderEmployeeRole.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderEmployeeRole.Remove(role);
            }

            foreach (var keydate in await ctx.OrderKeyDate.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderKeyDate.Remove(keydate);
            }
        }

        /// <summary>
        /// Initializes the Company and GLAccount IDs
        /// </summary>
        /// <returns></returns>
        protected async Task InitializeCompanyAndGLAccountIDs()
        {
            // check for existing company
            var company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
            if (company == null)
            {
                company = new CompanyData()
                {
                    BID = BID,
                    ID = _companyID,
                    Name = "Test Pricing Company",
                    LocationID = 1,
                    IsAdHoc = true,
                    StatusID = 1
                };

                ctx.CompanyData.Add(company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _taxGroupID);
            if (taxGroup == null)
            {
                taxGroup = new TaxGroup()
                {
                    BID = BID,
                    ID = _taxGroupID,
                    Name = "Test Pricing TaxGroup"
                };

                ctx.TaxGroup.Add(taxGroup);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            // there should always be at least one GLAccount and TaxCode
            glAccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == BID).ID;
        }

        #region Creates

        protected delegate int AssemblyChildAdder(AssemblyData machineTemplate, int newRecords, short machineID);
        protected delegate int MachineTemplateChildAdder(AssemblyData machineTemplate, int newRecords, bool useRoll = false);

        protected int AddVisualizationElements(AssemblyData machineTemplate, int newRecords, bool useRoll = false)
        {
            int newElementID = ctx.AssemblyElement.GetTestID(BID);
            var layout = machineTemplate.Layouts.First();

            var group02Element = new AssemblyElement()
            {
                BID = BID,
                ID = newElementID--,
                AssemblyID = machineTemplate.ID,
                LayoutID = layout.ID,
                ElementType = AssemblyElementType.Group,
                DataType = DataType.None,
                VariableID = machineTemplate.Variables.FirstOrDefault(v => v.Name == "Group02").ID,
                VariableName = "Group02",
            };
            newRecords++;

            var layoutVisualizer01Element = new AssemblyElement()
            {
                BID = BID,
                ID = newElementID--,
                AssemblyID = machineTemplate.ID,
                LayoutID = layout.ID,
                DataType = DataType.None,
                ElementType = AssemblyElementType.LayoutVisualizer,
                ParentID = group02Element.ID,
                VariableID = machineTemplate.Variables.FirstOrDefault(v => v.Name == "LayoutVisualizer01").ID,
                VariableName = "LayoutVisualizer01",
            };
            newRecords++;
            layout.Elements = new List<AssemblyElement>() { group02Element, layoutVisualizer01Element };
            group02Element.Elements = new List<AssemblyElement>() { layoutVisualizer01Element };

            return newRecords;
        }
        protected MachineTemplateChildAdder AddVisualizationVariablesRoll()
        {
            return (template, newRecs, useroll) => this.AddVisualizationVariables(template, newRecs, true);
        }

        protected int AddVisualizationVariables(AssemblyData machineTemplate, int newRecords, bool useRoll = false)
        {
            int newVariableID = ctx.AssemblyVariable.GetTestID(BID);

            int newFormulaID = ctx.AssemblyVariableFormula.GetTestID(BID);

            var defaultVariables = new List<AssemblyVariable>();
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = machineTemplate.ID,
                DefaultValue = "Sheet",
                Name = "Profile",
                DataType = DataType.String,
                IsFormula = false,
                ListDataType = DataType.String,
                ListValuesJSON = "[{\"Label\":\"Sheet\"},{\"Label\":\"Roll\"}]",
                Label = "Profile",
                IsRequired = false,
                ElementType = AssemblyElementType.DropDown,
                IsConsumptionFormula = false,
            });
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = machineTemplate.ID,
                Name = "Quantity",
                DefaultValue = "1",
                DataType = DataType.Number,
                IsFormula = false,
                Label = "Quantity",
                IsRequired = true,
                ElementType = AssemblyElementType.Number,
                IsConsumptionFormula = false,
            });
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = machineTemplate.ID,
                Name = "Group01",
                DataType = DataType.None,
                IsFormula = false,
                Label = "Group01",
                IsRequired = false,
                ElementType = AssemblyElementType.Number,
                IsConsumptionFormula = false,
            });
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = machineTemplate.ID,
                Name = "Group02",
                DataType = DataType.None,
                IsFormula = false,
                Label = "Group02",
                IsRequired = false,
                ElementType = AssemblyElementType.Number,
                IsConsumptionFormula = false,
            });
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = machineTemplate.ID,
                Name = "LayoutManager01",
                DataType = DataType.None,
                IsFormula = false,
                Label = "LayoutManager01",
                IsRequired = false,
                ElementType = AssemblyElementType.DetailFormButton,
                IsConsumptionFormula = false,
            });
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = machineTemplate.ID,
                Name = "LayoutVisualizer01",
                DataType = DataType.None,
                IsFormula = false,
                Label = "LayoutVisualizer01",
                IsRequired = false,
                ElementType = AssemblyElementType.LayoutVisualizer,
                IsConsumptionFormula = false,
                Formulas = new List<AssemblyVariableFormula>()
                    {
                        new AssemblyVariableFormula()
                        {
                            ID = newFormulaID--,
                            FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            ChildVariableName = "UseVisualization",
                            IsFormula = true,
                            FormulaText = "=true"
                        },
                        new AssemblyVariableFormula()
                        {
                            ID = newFormulaID--,
                            FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            ChildVariableName = "LayoutType",
                            IsFormula = true,
                            FormulaText = !useRoll? "=\"Sheet\"" :"=\"Roll\""
                        },
                        new AssemblyVariableFormula()
                        {
                            ID = newFormulaID--,
                            FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            ChildVariableName = "ItemHeight",
                            IsFormula = true,
                            FormulaText = "=2"
                        },
                        new AssemblyVariableFormula()
                        {
                            ID = newFormulaID--,
                            FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            ChildVariableName = "ItemWidth",
                            IsFormula = true,
                            FormulaText = "=2"
                        },
                        new AssemblyVariableFormula()
                        {
                            ID = newFormulaID--,
                            FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            ChildVariableName = "MachineMaxHorizontal",
                            IsFormula = true,
                            FormulaText = "=10"
                        },
                        new AssemblyVariableFormula()
                        {
                            ID = newFormulaID--,
                            FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            ChildVariableName = "MachineMaxVertical",
                            IsFormula = true,
                            FormulaText = "=10"
                        },
                        new AssemblyVariableFormula()
                        {
                            ID = newFormulaID--,
                            FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            ChildVariableName = "MaterialWidth",
                            IsFormula = true,
                            FormulaText = "=10"
                        },
                        new AssemblyVariableFormula()
                        {
                            ID = newFormulaID--,
                            FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                            FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                            ChildVariableName = "MaterialHeight",
                            IsFormula = true,
                            FormulaText = !useRoll? "=10" : ""
                        }
                    }
            });

            defaultVariables.ForEach(x => machineTemplate.Variables.Add(x));
            newRecords += defaultVariables.Count;

            foreach (var variable in defaultVariables)
            {
                if (variable.Formulas != null)
                {
                    newRecords += variable.Formulas.Count;
                }
            }

            return newRecords;
        }

        protected int AddBasicElements(AssemblyData machineTemplate, int newRecords, bool useRoll = false)
        {
            int newElementID = ctx.AssemblyElement.GetTestID(BID);
            var layout = machineTemplate.Layouts.First();

            var group02Element = new AssemblyElement()
            {
                BID = BID,
                ID = newElementID--,
                AssemblyID = machineTemplate.ID,
                LayoutID = layout.ID,
                ElementType = AssemblyElementType.Group,
                DataType = DataType.None,
                VariableID = machineTemplate.Variables.FirstOrDefault(v => v.Name == "Group01").ID,
                VariableName = "Group01",
            };
            newRecords++;

            var heightElement = new AssemblyElement()
            {
                BID = BID,
                ID = newElementID--,
                AssemblyID = machineTemplate.ID,
                LayoutID = layout.ID,
                DataType = DataType.None,
                ElementType = AssemblyElementType.Number,
                ParentID = group02Element.ID,
                VariableID = machineTemplate.Variables.FirstOrDefault(v => v.Name == "Height").ID,
                VariableName = "Height",
            };
            newRecords++;
            var widthElement = new AssemblyElement()
            {
                BID = BID,
                ID = newElementID--,
                AssemblyID = machineTemplate.ID,
                LayoutID = layout.ID,
                DataType = DataType.None,
                ElementType = AssemblyElementType.Number,
                ParentID = group02Element.ID,
                VariableID = machineTemplate.Variables.FirstOrDefault(v => v.Name == "Width").ID,
                VariableName = "Width",
            };
            newRecords++;

            layout.Elements = new List<AssemblyElement>() { group02Element, heightElement, widthElement };
            group02Element.Elements = new List<AssemblyElement>() { heightElement, widthElement };

            return newRecords;
        }

        protected int AddLinkedMachineVariable(AssemblyData assembly, int newRecords, short machineID)
        {
            int newVariableID = assembly.Variables.Count == 0 ? ctx.AssemblyVariable.GetTestID(BID) : assembly.Variables.Min(x => x.ID) - 1;

            assembly.Variables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID,
                ElementType = AssemblyElementType.LinkedMachine,
                DataType = DataType.MachinePart,
                AssemblyID = assembly.ID,
                Name = "testMachineVar",
                DefaultValue = "testMachineVar",
                LinkedMachineID = machineID
            });

            return newRecords + 1;
        }

        protected int AddBasicVariablesWithNoFormulas(AssemblyData assembly, int newRecords, bool useRoll = false)
        {
            int newVariableID = assembly.Variables.Count == 0 ? ctx.AssemblyVariable.GetTestID(BID) : assembly.Variables.Min(x => x.ID) - 1;

            var defaultVariables = new List<AssemblyVariable>();
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = assembly.ID,
                Name = "Quantity",
                DefaultValue = "1",
                DataType = DataType.Number,
                IsFormula = false,
                Label = "Quantity",
                IsRequired = true,
                ElementType = AssemblyElementType.Number,
                IsConsumptionFormula = false,
            });
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = assembly.ID,
                Name = "Group01",
                DataType = DataType.None,
                IsFormula = false,
                Label = "Group01",
                IsRequired = false,
                ElementType = AssemblyElementType.Group,
                IsConsumptionFormula = false,
            });
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = assembly.ID,
                Name = "Height",
                DataType = DataType.Number,
                IsFormula = false,
                Label = "Height",
                IsRequired = false,
                ElementType = AssemblyElementType.Number,
                IsConsumptionFormula = false,
            });
            defaultVariables.Add(new AssemblyVariable()
            {
                BID = BID,
                ID = newVariableID--,
                AssemblyID = assembly.ID,
                Name = "Width",
                DataType = DataType.Number,
                IsFormula = false,
                Label = "Width",
                IsRequired = false,
                ElementType = AssemblyElementType.Number,
                IsConsumptionFormula = false,
            });

            defaultVariables.ForEach(x => assembly.Variables.Add(x));
            newRecords += defaultVariables.Count;

            foreach (var variable in defaultVariables)
            {
                if (variable.Formulas != null)
                {
                    newRecords += variable.Formulas.Count;
                }
            }

            return newRecords;
        }

        protected int AddBasicVariablesWithLinkedMachineWithHeightWidthFormulas(AssemblyData assembly, int newRecords, short machineID)
        {
            newRecords = AddBasicVariablesWithNoFormulas(assembly, newRecords);
            newRecords = AddLinkedMachineVariable(assembly, newRecords, machineID);

            int newFormulaID = assembly.Variables.Min(x => x.ID) - 1;
            AssemblyVariable linkedMachine = assembly.Variables.First(x => x.ElementType == AssemblyElementType.LinkedMachine);

            if (linkedMachine.Formulas == null)
                linkedMachine.Formulas = new List<AssemblyVariableFormula>();

            linkedMachine.Formulas.Add(
                new AssemblyVariableFormula()
                {
                    ID = newFormulaID--,
                    FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                    FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                    ChildVariableName = "Height",
                    IsFormula = false,
                    DataType = DataType.Number,
                    FormulaText = "Height"
                });
            newRecords++;
            linkedMachine.Formulas.Add(
                new AssemblyVariableFormula()
                {
                    ID = newFormulaID--,
                    FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                    FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                    ChildVariableName = "Width",
                    IsFormula = false,
                    DataType = DataType.Number,
                    FormulaText = "Width"
                });
            newRecords++;

            return newRecords;
        }
        /// <summary>
        /// Creates a new Machine Template for testing
        /// </summary>
        /// <returns></returns>
        protected async Task<AssemblyData> CreateMachineTemplate(
            MachineTemplateChildAdder variableAdder,
            MachineTemplateChildAdder elementAdder,
            AssemblyLayoutType layoutType = AssemblyLayoutType.MachineTemplateMaterialLayout
            )
        {
            int newRecords = 1;
            AssemblyData machineTemplate = new AssemblyData()
            {
                ID = ctx.AssemblyData.GetTestID(BID),
                BID = BID,
                Name = "Test MachineTemplate For Visualization",
                IncomeAccountID = glAccountID,
                AssemblyType = AssemblyType.MachineTemplate,
                PricingType = AssemblyPricingType.MarketBasedPricing,
                PriceFormulaType = PriceFormulaType.Fixed,
                MachineLayoutTypes = (MachineLayoutType)3,
            };

            ctx.AssemblyData.Add(machineTemplate);
            machineTemplate.Variables = new List<AssemblyVariable>();
            newRecords = variableAdder(machineTemplate, newRecords);

            var layout = new AssemblyLayout()
            {
                BID = BID,
                ID = ctx.AssemblyLayout.GetTestIDShort(BID),
                AssemblyID = machineTemplate.ID,
                Name = "Sheet",
                LayoutType = layoutType
            };

            newRecords++;
            machineTemplate.Layouts = new List<AssemblyLayout>() { layout };

            newRecords = elementAdder(machineTemplate, newRecords);

            Assert.AreEqual(newRecords, await ctx.SaveChangesAsync()); // verify add

            return machineTemplate;
        }

        /// <summary>
        /// Creates a new Destination for testing
        /// </summary>
        /// <param name="templateID">Destination's DestinationTemplate ID</param>
        /// <param name="destinationName">Destination's Name</param>
        /// <returns></returns>
        protected async Task<DestinationData> CreateDestination(int templateID, string destinationName = "TestDestinationForVisualization")
        {
            var destination = new DestinationData()
            {
                ID = ctx.MachineData.GetTestIDShort(BID),
                BID = BID,
                Name = destinationName,
                ActiveProfileCount = 1,
                TemplateID = templateID,
            };
            ctx.DestinationData.Add(destination);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            return destination;
        }

        /// <summary>
        /// Creates a new Machine for testing
        /// </summary>
        /// <param name="templateID">Machine's MachineTemplate ID</param>
        /// <param name="machineName">Machine's Name</param>
        /// <returns></returns>
        protected async Task<MachineData> CreateMachine(int templateID, string machineName = "TestMachineForVisualization")
        {
            var machine = new MachineData()
            {
                ID = ctx.MachineData.GetTestIDShort(BID),
                BID = BID,
                Name = machineName,
                ActiveProfileCount = 1,
                ActiveInstanceCount = 1,
                MachineTemplateID = templateID,
            };
            ctx.MachineData.Add(machine);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            return machine;
        }

        /// <summary>
        /// Creates a new Assembly for testing
        /// </summary>
        /// <param name="estimatingPrice">EstimatingPrice to use (default 5m)</param>
        /// <param name="estimatingCost">EstimatingCost to use (default 2.5m)</param>
        /// <param name="assemblyID">MaterialID (default -99)</param>
        /// <returns></returns>
        protected async Task<AssemblyData> CreateAssembly(
            AssemblyChildAdder variableAdder,
            short machineID, 
            Dictionary<string, string> variableDefaultDictionary = null)
        {
            int newRecords = 0;
            AssemblyData assembly = new AssemblyData()
            {
                ID = ctx.AssemblyData.GetTestID(BID),
                BID = BID,
                Name = "Test Assembly For Visualization",
                IncomeAccountID = glAccountID,
                AssemblyType = AssemblyType.Product
            };

            newRecords++;
            ctx.AssemblyData.Add(assembly);
            assembly.Variables = new List<AssemblyVariable>();

            int newVariableID = ctx.AssemblyVariable.GetTestID(BID);

            if (variableDefaultDictionary != null && variableDefaultDictionary.Count > 0)
            {

                foreach (var variableDefault in variableDefaultDictionary)
                {
                    var newVariable = new AssemblyVariable()
                    {
                        ID = newVariableID--,
                        BID = BID,
                        AssemblyID = assembly.ID,
                        ElementType = AssemblyElementType.SingleLineText,
                        IsRequired = false,
                        Name = variableDefault.Key,
                        DefaultValue = variableDefault.Value,
                        IsFormula = (!string.IsNullOrWhiteSpace(variableDefault.Value) && variableDefault.Value.TrimStart().First() == '=')
                    };

                    newRecords++;
                    ctx.AssemblyVariable.Add(newVariable);
                }

            }
            newRecords = variableAdder(assembly, newRecords, machineID);

            Assert.AreEqual(newRecords, await ctx.SaveChangesAsync()); // verify add

            return assembly;
        }

        /// <summary>
        /// Creates an Order, OrderItem, (SurchargeDef if not already made) and OrderItemSurcharge
        /// </summary>
        /// <param name="orderID">Order ID (default -99)</param>
        /// <param name="orderItemID">OrderItem ID (default -99)</param>
        /// <param name="orderItemSurchargeID">OrderItemSurcharge ID (default -99)</param>
        /// <param name="surchargeDefID">SurchargeDef ID (default -99)</param>
        /// <param name="surchargeFixedFee">SurchargeDef DefaultFixedFee to use (default 10m)</param>
        /// <param name="surchargeUnitFee">SurchargeDef DefaultPerUnitFee to use (default 0m)</param>
        /// <returns></returns>
        protected async Task CreateTestOrderData(int orderID = _orderID, int orderItemID = _orderItemID)
        {
            var locationID = (await ctx.LocationData.FirstOrDefaultAsync(t => t.BID == BID)).ID;

            order = await ctx.OrderData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == orderID);
            if (order == null)
            {
                order = new OrderData()
                {
                    BID = BID,
                    ID = orderID,
                    CompanyID = _companyID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    FormattedNumber = "",
                    ProductionLocationID = locationID,
                    LocationID = locationID,
                    PickupLocationID = locationID,
                    OrderStatusID = OrderOrderStatus.OrderWIP,
                    TaxGroupID = _taxGroupID,
                    IsTaxExempt = false,
                    TaxExemptReasonID = null
                };

                ctx.OrderData.Add(order);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }

            orderItem = await ctx.OrderItemData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemID);
            if (orderItem == null)
            {
                orderItem = new OrderItemData()
                {
                    BID = BID,
                    OrderID = orderID,
                    ID = orderItemID,
                    Name = "TEST OrderItem For Visualization",
                    OrderStatusID = OrderOrderStatus.OrderWIP,
                    TransactionType = (byte)OrderTransactionType.Order,
                    ItemStatusID = (await ctx.OrderItemStatus.FirstOrDefaultAsync(t => t.BID == BID)).ID,
                    TaxGroupID = _taxGroupID,
                    ProductionLocationID = locationID
                };

                ctx.OrderItemData.Add(orderItem);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }

            orderItemComponent = await ctx.OrderItemComponent.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemComponentID);
            if (orderItemComponent == null)
            {
                orderItemComponent = new OrderItemComponent()
                {
                    BID = BID,
                    ID = _orderItemComponentID,
                    OrderID = orderID,
                    OrderItemID = orderItemID,
                    Name = "TEST OrderItemComponent For Visualization",
                    TotalQuantity = 1,
                    ComponentID = _assemblyID,
                    ComponentType = OrderItemComponentType.Assembly
                };

                ctx.OrderItemComponent.Add(orderItemComponent);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            orderItem.Components = new List<OrderItemComponent>() { orderItemComponent };
            order.Items = new List<OrderItemData>() { orderItem };
        }

        protected async Task<ItemPriceRequest> GetItemPriceRequest(int assemblyID, int machineID, string linkedMachineVariableName, Dictionary<string, VariableValue> assemblyVariables, Dictionary<string, VariableValue> machineVariables)
        {
            var company = await ctx.CompanyData.AsNoTracking().FirstOrDefaultAsync();
            return new ItemPriceRequest()
            {
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Surcharges = new List<SurchargePriceRequest>(),
                Components = new List<ComponentPriceRequest>()
                {
                    new ComponentPriceRequest()
                    {
                        ComponentType = OrderItemComponentType.Assembly,
                        ComponentID = assemblyID,
                        CompanyID = company.ID,
                        TotalQuantity = 1,
                        TotalQuantityOV = false,
                        PriceUnit = 10,
                        PriceUnitOV = false,
                        Variables = assemblyVariables,
                        ChildComponents = new List<ComponentPriceRequest>()
                        {
                            new ComponentPriceRequest()
                            {
                                ComponentType = OrderItemComponentType.Machine,
                                ComponentID = machineID,
                                CompanyID = company.ID,
                                TotalQuantity = 1,
                                TotalQuantityOV = false,
                                PriceUnit = null,
                                PriceUnitOV = false,
                                VariableName = linkedMachineVariableName,
                                Variables = machineVariables
                            }
                        }
                    }
                }
            };
        }


        #endregion

        #endregion

        [TestMethod]
        public async Task TestLinkedMachineReturnsVisualizationData()
        {
            var machineTemplate = await this.CreateMachineTemplate(AddVisualizationVariables, AddVisualizationElements);
            Assert.IsNotNull(machineTemplate);

            var machine = await this.CreateMachine(machineTemplate.ID);
            Assert.IsNotNull(machine);

            var assembly = await this.CreateAssembly(AddLinkedMachineVariable, machine.ID);
           

            Assert.IsNotNull(assembly);

            string linkedMachineVariableName = assembly.Variables.First(x => x.ElementType == AssemblyElementType.LinkedMachine).Name;

            /*compute request | start*/

            var itemRequest = await this.GetItemPriceRequest(
                assembly.ID, 
                machine.ID, 
                linkedMachineVariableName,
                assemblyVariables: new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = false} },
                    { "LayoutType", new VariableValue() { Value = "Sheet", ValueOV = true} }
                },
                machineVariables: new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = false} },
                    { "Profile", new VariableValue() { Value = "Default", ValueOV = true} },
                    { "LayoutType", new VariableValue() { Value = "Sheet", ValueOV = false} }
                });
            var itemPriceResult = await this.pricingEngine.Compute(itemRequest, false);
            Assert.IsNotNull(itemPriceResult);

            var assemblyComponentResult = itemPriceResult.Components.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(assemblyComponentResult);

            var machineComponentResult = assemblyComponentResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Machine);
            Assert.IsNotNull(machineComponentResult);

            var specialLayoutAssemblyComponentResult = machineComponentResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(specialLayoutAssemblyComponentResult);

            var visualizationDataVariable = specialLayoutAssemblyComponentResult
                                                .Variables
                                                .Where(kvp => kvp.Key.ToLower().Trim() == "visualizationdata")
                                                .Select(kvp => kvp.Value)
                                                .FirstOrDefault();

            Assert.IsNotNull(visualizationDataVariable);
            Assert.IsNotNull(visualizationDataVariable.Value);
            Assert.IsTrue(visualizationDataVariable.Value.Length > 0);
            Assert.IsNotNull(JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationDataVariable.Value));
            /*compute request | end*/
        }

        [TestMethod]
        [Ignore("END-7889")]
        public async Task TestLinkedMachineReturnsVisualizationDataForRoll()
        {
            var machineTemplate = await this.CreateMachineTemplate(this.AddVisualizationVariablesRoll(), this.AddVisualizationElements);
            Assert.IsNotNull(machineTemplate);

            var machine = await this.CreateMachine(machineTemplate.ID);
            Assert.IsNotNull(machine);

            var assembly = await this.CreateAssembly(AddLinkedMachineVariable, machine.ID);


            Assert.IsNotNull(assembly);

            string linkedMachineVariableName = assembly.Variables.First(x => x.ElementType == AssemblyElementType.LinkedMachine).Name;

            /*compute request | start*/

            var itemRequest = await this.GetItemPriceRequest(
                assembly.ID,
                machine.ID,
                linkedMachineVariableName,
                assemblyVariables: new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = false} },
                    { "LayoutType", new VariableValue() { Value = "Roll", ValueOV = true} }
                },
                machineVariables: new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = false} },
                    { "Profile", new VariableValue() { Value = "Default", ValueOV = true} },
                    { "LayoutType", new VariableValue() { Value = "Roll", ValueOV = false} }
                });
            var itemPriceResult = await this.pricingEngine.Compute(itemRequest, false);
            Assert.IsNotNull(itemPriceResult);

            var assemblyComponentResult = itemPriceResult.Components.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(assemblyComponentResult);

            var machineComponentResult = assemblyComponentResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Machine);
            Assert.IsNotNull(machineComponentResult);

            var specialLayoutAssemblyComponentResult = machineComponentResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(specialLayoutAssemblyComponentResult);

            var visualizationDataVariable = specialLayoutAssemblyComponentResult
                                                .Variables
                                                .Where(kvp => kvp.Key.ToLower().Trim() == "visualizationdata")
                                                .Select(kvp => kvp.Value)
                                                .FirstOrDefault();

            Assert.IsNotNull(visualizationDataVariable);
            Assert.IsNotNull(visualizationDataVariable.Value);
            Assert.IsTrue(visualizationDataVariable.Value.Length > 0);
            Assert.IsNotNull(JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationDataVariable.Value));

            var visualization = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(visualizationDataVariable.Value);
            Assert.IsTrue(visualization.Layouts.All(x => x.LayoutSize.Vertical != null), "visualization.Layouts.All(x => x.LayoutSize.Vertical != null)");
            /*compute request | end*/
        }

        [TestMethod]
        public async Task TestAssemblyCanAccessLayoutVisualizerVariables()
        {
            var machineTemplate = await this.CreateMachineTemplate(AddVisualizationVariables, AddVisualizationElements);
            Assert.IsNotNull(machineTemplate);

            var machine = await this.CreateMachine(machineTemplate.ID);
            Assert.IsNotNull(machine);

            var assembly = await this.CreateAssembly(AddLinkedMachineVariable, machine.ID);
            Assert.IsNotNull(assembly);
            assembly.Variables.Add(new AssemblyVariable()
            {
                BID = assembly.BID,
                ID = assembly.Variables.Count == 0 ? ctx.AssemblyVariable.GetTestID(BID) : assembly.Variables.Min(x => x.ID) - 1,
                DefaultValue = "=testMachineVar.LayoutVisualizer01.ItemsHorizontal",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                AssemblyID = assembly.ID,
                Name = "AssemblyLayoutItemsHorizontal",
                Label = "Assembly Layout Items Horizontal",
                IsFormula = true,
            });

            ctx.SaveChanges();
            string linkedMachineVariableName = assembly.Variables.First(x => x.ElementType == AssemblyElementType.LinkedMachine).Name;

            /*compute request | start*/

            var itemRequest = await this.GetItemPriceRequest(
                assembly.ID,
                machine.ID,
                linkedMachineVariableName,
                assemblyVariables: new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = false} },
                    { "LayoutType", new VariableValue() { Value = "Sheet", ValueOV = true} }
                },
                machineVariables: new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = false} },
                    { "Profile", new VariableValue() { Value = "Default", ValueOV = true} },
                    { "LayoutType", new VariableValue() { Value = "Sheet", ValueOV = false} }
                });
            var itemPriceResult = await this.pricingEngine.Compute(itemRequest, false);
            Assert.IsNotNull(itemPriceResult);

            var assemblyComponentResult = itemPriceResult.Components.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(assemblyComponentResult);

            var machineComponentResult = assemblyComponentResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Machine);
            Assert.IsNotNull(machineComponentResult);

            var specialLayoutAssemblyComponentResult = machineComponentResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(specialLayoutAssemblyComponentResult);

            var visualizationDataVariable = specialLayoutAssemblyComponentResult
                                                .Variables
                                                .Where(kvp => kvp.Key.ToLower().Trim() == "visualizationdata")
                                                .Select(kvp => kvp.Value)
                                                .FirstOrDefault();

            Assert.IsNotNull(visualizationDataVariable);
            Assert.IsNotNull(visualizationDataVariable.Value);
            Assert.IsTrue(visualizationDataVariable.Value.Length > 0);
            Assert.AreEqual(specialLayoutAssemblyComponentResult.Variables["ItemsHorizontal"].ValueAsDecimal, assemblyComponentResult.Variables["AssemblyLayoutItemsHorizontal"].ValueAsDecimal);
            /*compute request | end*/
        }




        [TestMethod]
        public async Task TestLinkedMachineVariableMapping()
        {
            // setup
            var machineTemplate = await this.CreateMachineTemplate(AddBasicVariablesWithNoFormulas, AddBasicElements);
            Assert.IsNotNull(machineTemplate);

            var machine = await this.CreateMachine(machineTemplate.ID);
            Assert.IsNotNull(machine);

            var assembly = await this.CreateAssembly(AddBasicVariablesWithLinkedMachineWithHeightWidthFormulas, machine.ID);
            Assert.IsNotNull(assembly);

            string linkedMachineVariableName = assembly.Variables.First(x => x.ElementType == AssemblyElementType.LinkedMachine).Name;

            // now test basic compute

            await TestEmptyCompute(assembly, machine, linkedMachineVariableName);


            // now test second compute with OVs from first compute

            var secondResult = await TestParentVarOV(
                assembly, machine, linkedMachineVariableName,
                new Dictionary<string, VariableValue>()
                {
                    {"Height",new VariableValue() { Value = "12", ValueOV = true } },
                    {"Width", new VariableValue() { Value = "12", ValueOV = true } },
                    {assembly.Variables.First(x => x.ElementType == AssemblyElementType.LinkedMachine).Name, new VariableValue() { Value = machine.Name, ValueOV = false } },
                },
                new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = true } },
                    { "Profile", new VariableValue() { Value = "Default", ValueOV = true } },
                    { "LayoutType", new VariableValue() { Value = "Sheet", ValueOV = true } },
                }
                );

            var thirdResult = await TestParentVarOV(
                assembly, machine, linkedMachineVariableName,
                new Dictionary<string, VariableValue>()
                {
                    {"Height",new VariableValue() { Value = "8", ValueOV = true } },
                    {"Width", new VariableValue() { Value = "8", ValueOV = true } },
                    {assembly.Variables.First(x => x.ElementType == AssemblyElementType.LinkedMachine).Name, new VariableValue() { Value = machine.Name, ValueOV = false } },
                },
                new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = true } },
                    { "Profile", new VariableValue() { Value = "Default", ValueOV = true } },
                    { "LayoutType", new VariableValue() { Value = "Sheet", ValueOV = true } },
                    { "Height", new VariableValue() { Value = "12", ValueOV = false } },
                    { "Width", new VariableValue() { Value = "12", ValueOV = false } },
                }                
                );

            // what happens if we send a height and width for our parent
            // but send overrides for our child vars?
            // we would not expect child values to be equal to parent values (1)
            // but we would expect the child value OV (99) to be returned
            var fourthResult = await TestChildVarOV(
                assembly, machine, linkedMachineVariableName,
                new Dictionary<string, VariableValue>()
                {
                    {"Height",new VariableValue() { Value = "1", ValueOV = true } },
                    {"Width",new VariableValue() { Value = "1", ValueOV = true } },
                    {assembly.Variables.First(x => x.ElementType == AssemblyElementType.LinkedMachine).Name, new VariableValue() { Value = machine.Name, ValueOV = false } },
                },
                new Dictionary<string, VariableValue>()
                {
                    { "Quantity", new VariableValue() { Value = "1", ValueOV = true } },
                    { "Profile", new VariableValue() { Value = "Default", ValueOV = true } },
                    { "LayoutType", new VariableValue() { Value = "Sheet", ValueOV = true } },
                    { "Height", new VariableValue() { Value = "99", ValueOV = true } },
                    { "Width", new VariableValue() { Value = "99", ValueOV = true } },
                }
                );
        }

        public async Task TestEmptyCompute(AssemblyData assembly, MachineData machine, string linkedMachineVariableName)
        {
            var itemRequest = await GetItemPriceRequest(assembly.ID, machine.ID, linkedMachineVariableName, new Dictionary<string, VariableValue>(), new Dictionary<string, VariableValue>());
            // fire a basic compute with no variable OVs
            var itemPriceResult = await pricingEngine.Compute(itemRequest, false);
            Assert.IsNotNull(itemPriceResult);

            var assemblyComponentResult = itemPriceResult.Components.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(assemblyComponentResult);

            var machineComponentResult = assemblyComponentResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Machine);
            Assert.IsNotNull(machineComponentResult);

            Assert.IsNotNull(assemblyComponentResult.Variables["Height"]);
            Assert.IsNotNull(assemblyComponentResult.Variables["Width"]);
            Assert.IsNotNull(machineComponentResult.Variables["Height"]);
            Assert.AreEqual(false, machineComponentResult.Variables["Height"].ValueOV);
            Assert.IsNotNull(machineComponentResult.Variables["Width"]);
            Assert.AreEqual(false, machineComponentResult.Variables["Width"].ValueOV);

        }

        public async Task<ItemPriceResult> TestParentVarOV(AssemblyData assembly, MachineData machine, string linkedMachineVariableName, Dictionary<string, VariableValue> parentVariables, Dictionary<string, VariableValue> childVariables)
        {
            var request = await GetItemPriceRequest(
                assembly.ID,
                machine.ID,
                linkedMachineVariableName,
                parentVariables,
                childVariables);

            var priceResult = await pricingEngine.Compute(request, false);
            Assert.IsNotNull(priceResult);

            var assemblyResult = priceResult.Components.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(assemblyResult);

            var machineResult = assemblyResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Machine);
            Assert.IsNotNull(machineResult);
            Assert.AreEqual(true, machineResult.Variables.ContainsKey("Height"));
            Assert.AreEqual(parentVariables["Height"].Value, assemblyResult.Variables["Height"].Value);
            Assert.AreEqual(parentVariables["Height"].Value, machineResult.Variables["Height"].Value);
            Assert.AreEqual(false, machineResult.Variables["Height"].ValueOV);

            Assert.AreEqual(true, machineResult.Variables.ContainsKey("Width"));
            Assert.AreEqual(parentVariables["Width"].Value, assemblyResult.Variables["Width"].Value);
            Assert.AreEqual(parentVariables["Width"].Value, machineResult.Variables["Width"].Value);
            Assert.AreEqual(false, machineResult.Variables["Width"].ValueOV);

            return priceResult;
        }
        public async Task<ItemPriceResult> TestChildVarOV(AssemblyData assembly, MachineData machine, string linkedMachineVariableName, Dictionary<string, VariableValue> parentVariables, Dictionary<string, VariableValue> childVariables)
        {
            Assert.IsNotNull(parentVariables["Height"]);
            Assert.AreEqual(true, parentVariables["Height"].ValueOV);
            Assert.IsNotNull(childVariables["Height"]);
            Assert.AreEqual(true, childVariables["Height"].ValueOV);

            Assert.IsNotNull(parentVariables["Width"]);
            Assert.AreEqual(true, parentVariables["Width"].ValueOV);
            Assert.IsNotNull(childVariables["Width"]);
            Assert.AreEqual(true, parentVariables["Height"].ValueOV);

            var request = await GetItemPriceRequest(
                assembly.ID,
                machine.ID,
                linkedMachineVariableName,
                parentVariables,
                childVariables);

            var priceResult = await pricingEngine.Compute(request);
            Assert.IsNotNull(priceResult);

            var assemblyResult = priceResult.Components.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(assemblyResult);

            var machineResult = assemblyResult.ChildComponents.FirstOrDefault(c => c.ComponentType == OrderItemComponentType.Machine);
            Assert.IsNotNull(machineResult);
            Assert.AreEqual(true, machineResult.Variables.ContainsKey("Height"));
            Assert.AreEqual(parentVariables["Height"].Value, assemblyResult.Variables["Height"].Value);
            Assert.AreEqual(true, machineResult.Variables.ContainsKey("Width"));
            Assert.AreEqual(parentVariables["Width"].Value, assemblyResult.Variables["Width"].Value);

            Assert.AreEqual(childVariables["Height"].Value, machineResult.Variables["Height"].Value);
            Assert.AreEqual(true, machineResult.Variables["Height"].ValueOV);

            Assert.AreEqual(childVariables["Width"].Value, machineResult.Variables["Width"].Value);
            Assert.AreEqual(true, machineResult.Variables["Width"].ValueOV);

            return priceResult;
        }
    }
}


/*

MachineTemplate Layouts =
    OrderEntry Layout
        .Elements = {
            Group01 Element (linked to variable's ID)
                Elements = {
                    LayoutManager (linked to variable's ID)
                    }
                }
    Sheet Layout
        .Elements = {
            Group02 Element (linked to variable's ID)
                Elements = {
                    LayoutVisualizer (linked to variable's ID)
                    }
                }
MachineTemplate Variables = {
    Group01,
    Group02,
    LayoutVisualizer,
    LayoutManager,
    Profile,
    Quantity
    }

*/
