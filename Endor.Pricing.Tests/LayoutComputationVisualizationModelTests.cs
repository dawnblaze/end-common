﻿using Endor.CBEL.Common;
using Endor.CBEL.Elements;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using static Endor.Level3.Tests.Common.TestHelper;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class LayoutComputationVisualizationModelTests
    {
        private const int BidScope = 1;

        [TestMethod]
        public void TestSerialization()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            LayoutComputingAssembly layoutComputingAssembly = new LayoutComputingAssembly("Test", new ApiContext(cache, BidScope), BidScope, cache, logger);
            layoutComputingAssembly.ItemHeight.Value = 2m;
            layoutComputingAssembly.ItemWidth.Value = 3m;
            layoutComputingAssembly.ItemQuantity.Value = 10m;

            LayoutComputationVisualizationModel model = new LayoutComputationVisualizationModel(layoutComputingAssembly);
            string serialized = JsonConvert.SerializeObject(model);
            Assert.IsNotNull(serialized);
        }
    }
}
