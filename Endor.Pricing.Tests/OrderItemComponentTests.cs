﻿using Endor.CBEL.ObjectGeneration;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Pricing.CBEL.Common;
using Endor.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class OrderItemComponentTest : PricingEngineTest
    {
        [TestMethod]
        [DataRow(true, true, true)]
        [DataRow(false, true, true)]
        [DataRow(true, false, true)]
        [DataRow(true, true, false)]
        [DataRow(false, false, false)]
        public async Task OrderItemComponentMaterialRoundtrip(bool qtyOV = true, bool priceOV = true, bool costOV = true)
        {
            var material = await CreateMaterial(doBeforeSave: (mat) =>
            {
                mat.QuantityInSet = 1;
                mat.MinimumQuantity = 1;
            });

            OrderItemComponent oicMaterial = new OrderItemComponent()
            {
                ID = -99,
                TempID = Guid.NewGuid(),
                VariableName = "oicMaterial",
                ComponentID = _materialID,
                ComponentType = OrderItemComponentType.Material,
                TotalQuantity = 2,
                TotalQuantityOV = qtyOV,
                AssemblyQuantityOV = qtyOV,
                AssemblyQuantity = 2,
                PriceUnit = 3,
                PriceUnitOV = priceOV,
                CostUnit = 1,
                CostOV = costOV,
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                IncomeAllocationType = AssemblyIncomeAllocationType.AllocationByMaterialIncomeAccount,
            };

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>() { oicMaterial.ToPriceRequest(_companyID) };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };
            OrderPriceRequest orderRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                    itemPriceRequest
                }
            };

            OrderPriceResult result = await pricingEngine.Compute(orderRequest, false);
            Assert.IsNotNull(result);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);

            var materialResult = result.Items[0].Components.FirstOrDefault(x => x.ID == oicMaterial.ID);
            Assert.IsNotNull(materialResult);

            decimal? effectiveQuantity = qtyOV ? oicMaterial.TotalQuantity : 1;

            Assert.AreEqual(oicMaterial.ID, /****************/ materialResult.ID);
            Assert.AreEqual(oicMaterial.TempID, /************/ materialResult.TempID);
            Assert.AreEqual(oicMaterial.MaterialID, /********/ materialResult.ComponentID);
            if(qtyOV)
            {
                Assert.AreEqual(oicMaterial.TotalQuantity, /**********/ materialResult.TotalQuantity);
                Assert.AreEqual(oicMaterial.AssemblyQuantity, /***/ materialResult.AssemblyQuantity);
            }
            else
            {
                Assert.AreEqual(effectiveQuantity, /**********/ materialResult.TotalQuantity);
                Assert.AreEqual(effectiveQuantity / itemPriceRequest.Quantity, /***/ materialResult.AssemblyQuantity);
            }
            Assert.AreEqual(oicMaterial.AssemblyQuantityOV, /********/ materialResult.AssemblyQuantityOV);
            Assert.AreEqual(oicMaterial.TotalQuantityOV, /**/ materialResult.TotalQuantityOV);
            if(priceOV)
            {
                Assert.AreEqual(oicMaterial.PriceUnit, /*********/ materialResult.PriceUnit);
            }
            else
            {
                Assert.AreEqual(this.GetMaterialExpectedPriceUnit(material), /*********/ materialResult.PriceUnit);
            }
            Assert.AreEqual(oicMaterial.PriceUnitOV, /*******/ materialResult.PriceUnitOV);
            if(costOV)
            {
                Assert.AreEqual(oicMaterial.CostUnit, /**********/ materialResult.CostUnit);
            }
            else
            {
                Assert.AreEqual(this.GetMaterialExpectedCostUnit(material), /**********/ materialResult.CostUnit);
            }
            Assert.AreEqual(oicMaterial.CostOV, /************/ materialResult.CostOV);
            Assert.AreEqual(oicMaterial.IncomeAccountID, /***/ materialResult.IncomeAccountID);
            Assert.AreEqual(oicMaterial.ExpenseAccountID, /**/ materialResult.ExpenseAccountID);
            Assert.AreEqual(oicMaterial.IncomeAllocationType, materialResult.IncomeAllocationType);

        }

        [TestMethod]
        [DataRow(true, true, true)]
        [DataRow(false, true, true)]
        [DataRow(true, false, true)]
        [DataRow(true, true, false)]
        [DataRow(false, false, false)]
        public async Task OrderItemComponentLaborRoundtrip(bool qtyOV = true, bool priceOV = true, bool costOV = true)
        {
            var labor = await CreateLabor();
            OrderItemComponent oicLabor = new OrderItemComponent()
            {
                ID = -98,
                TempID = Guid.NewGuid(),
                VariableName = "oicLabor",
                ComponentID = _laborID,
                ComponentType = OrderItemComponentType.Labor,
                TotalQuantity = 2,
                TotalQuantityOV = qtyOV,
                AssemblyQuantity = 2,
                AssemblyQuantityOV = qtyOV,
                QuantityUnit = Unit.Minute,
                PriceUnit = 3,
                PriceUnitOV = priceOV,
                CostUnit = 1,
                CostOV = costOV,
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                IncomeAllocationType = AssemblyIncomeAllocationType.AllocationByMaterialIncomeAccount,
            };

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>() { oicLabor.ToPriceRequest(_companyID) };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };
            OrderPriceRequest orderRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                    itemPriceRequest
                }
            };

            OrderPriceResult result = await pricingEngine.Compute(orderRequest, false);
            Assert.IsNotNull(result);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);

            var laborResult = result.Items[0].Components.FirstOrDefault(x => x.ID == oicLabor.ID);
            Assert.IsNotNull(laborResult);


            decimal? effectiveQty = qtyOV ? 
                                    oicLabor.TotalQuantity :
                                    LaborQuantityHelper
                                        .ComputeQuantity(new Measurement(labor.BillingIncrementInMin, Unit.Minute),
                                                         new Measurement(labor.MinimumTimeInMin, Unit.Minute),
                                                         1);

            Assert.AreEqual(oicLabor.ID, /****************/ laborResult.ID);
            Assert.AreEqual(oicLabor.TempID, /************/ laborResult.TempID);
            Assert.AreEqual(oicLabor.LaborID, /***********/ laborResult.ComponentID);
            if(qtyOV)
            {
                Assert.AreEqual(oicLabor.TotalQuantity, /**********/ laborResult.TotalQuantity);
                Assert.AreEqual(oicLabor.AssemblyQuantity, /***/ laborResult.AssemblyQuantity);
            }
            else
            {
                Assert.AreEqual(effectiveQty, /**********/ laborResult.TotalQuantity);
                Assert.AreEqual(effectiveQty/itemPriceRequest.Quantity, /***/ laborResult.AssemblyQuantity);
            }
            Assert.AreEqual(oicLabor.AssemblyQuantityOV, /********/ laborResult.AssemblyQuantityOV);
            Assert.AreEqual(oicLabor.TotalQuantityOV, /**/ laborResult.TotalQuantityOV);
            if(priceOV)
            {
                Assert.AreEqual(oicLabor.PriceUnit, /*********/ laborResult.PriceUnit);
            }
            else
            {
                decimal? expectedPriceUnit = this.GetLaborExpectedPriceUnit(labor, effectiveQty);
                Assert.AreEqual(Math.Round(expectedPriceUnit.GetValueOrDefault(), 2), Math.Round(laborResult.PriceUnit.GetValueOrDefault(), 2));
            }
            Assert.AreEqual(oicLabor.PriceUnitOV, /*******/ laborResult.PriceUnitOV);
            if(costOV)
            {
                Assert.AreEqual(oicLabor.CostUnit, /**********/ laborResult.CostUnit);
            }
            else
            {
                decimal? expectedCostUnit = this.GetLaborExpectedCostUnit(labor, effectiveQty);
                Assert.AreEqual(Math.Round(expectedCostUnit.GetValueOrDefault(), 2), Math.Round(laborResult.CostUnit.GetValueOrDefault(), 2));
            }
            Assert.AreEqual(oicLabor.CostOV, /************/ laborResult.CostOV);
            Assert.AreEqual(oicLabor.IncomeAccountID, /***/ laborResult.IncomeAccountID);
            Assert.AreEqual(oicLabor.ExpenseAccountID, /**/ laborResult.ExpenseAccountID);
            Assert.AreEqual(oicLabor.IncomeAllocationType, laborResult.IncomeAllocationType);
        }

        /// <summary>
        /// this will include both assembly and machine roundtrip tests since machines are not root components
        /// </summary>
        /// <param name="qtyOV"></param>
        /// <param name="priceOV"></param>
        /// <param name="costOV"></param>
        /// <param name="includeChildren"></param>
        /// <returns></returns>
        [TestMethod]
        [DataRow(true, true, true, true)]
        [DataRow(false, true, true, true)]
        [DataRow(true, false, true, true)]
        [DataRow(true, true, false, true)]
        [DataRow(true, true, true, false)]
        [DataRow(false, false, false, false)]
        public async Task OrderItemComponentAssemblyRoundtrip(bool qtyOV = true, bool priceOV = true, bool costOV = true, bool includeChildren = true)
        {   
            //setup material------------------------------------------------------------
            var material = await this.CreateMaterial();
            //setup labor-------------------------------------------------------------
            var labor = await this.CreateLabor();
            var assembly = await this.CreateAssembly(doBeforeSave: a => {
                var asm = a;//not sure why visual studio is treating 'a' as decimal giving wrong autocomplete suggestions;
                asm.PricingType = AssemblyPricingType.MarketBasedPricing;
                asm.FixedPrice = 13;
            });
            //linked material variable
            await this.CreateAssemblyVariable(true, dobeforeSave: variable =>
            {
                var v = variable;
                v.IsConsumptionFormula = true;
                v.ConsumptionDefaultValue = "=1";
                v.InclusionFormula = $"={includeChildren.ToString()}";
            });
            //linked labor variable
            await this.CreateAssemblyVariable(false, dobeforeSave: variable =>
            {
                var v = variable;
                v.IsConsumptionFormula = true;
                v.ConsumptionDefaultValue = "=1";
                v.InclusionFormula = $"={includeChildren.ToString()}";
            });
            await this.CreateAssemblyVariable(false, dobeforeSave: variable =>
            {
                var v = variable;
                v.Name = "TotalQuantity";
                v.DataType = DataType.Number;
                v.ElementType = AssemblyElementType.Number;
                v.Label = "Total Quantity";
                v.DefaultValue = "=AssemblyQuantity.Value * LineItemQuantity";
                v.IsFormula = true;
                v.LinkedLaborID = null;
                v.LinkedMaterialID = null;
            });
            await this.CreateAssemblyVariable(false, dobeforeSave: variable =>
            {
                var v = variable;
                v.Name = "AssemblyQuantity";
                v.DataType = DataType.Number;
                v.ElementType = AssemblyElementType.Number;
                v.Label = "Assembly Quantity";
                v.DefaultValue = "=3";
                v.IsFormula = true;
                v.LinkedLaborID = null;
                v.LinkedMaterialID = null;
            });
            await this.CreateAssemblyVariable(false, dobeforeSave: variable =>
            {
                var v = variable;
                v.Name = "Tier";
                v.DataType = DataType.String;
                v.ElementType = AssemblyElementType.SingleLineLabel;
                v.Label = "Tier";
                v.DefaultValue = "=CompanyTier()";
                v.IsFormula = true;
                v.LinkedLaborID = null;
                v.LinkedMaterialID = null;
            });
            await this.CreateAssemblyVariable(false, dobeforeSave: variable =>
            {
                var v = variable;
                v.Name = "Price";
                v.DataType = DataType.Number;
                v.ElementType = AssemblyElementType.Number;
                v.Label = "Total Retail Price";
                v.DefaultValue = "=TotalQuantity.Value * FixedPrice";
                v.IsFormula = true;
                v.LinkedLaborID = null;
                v.LinkedMaterialID = null;
            });


            //setup machine--------------------------------------------------------
            var template = await this.CreateAssembly(-98, doBeforeSave: machineTemplate =>{
                var t = machineTemplate;
                t.Name = "MachineTemplateTest";
                t.AssemblyType = AssemblyType.MachineTemplate;
            });
            AssemblyVariable linkedMachineVar = null;
            const int LinkedMachineQtyPerLineItem = 3;
            var machine = await this.CreateMachine(assemblyID: assembly.ID,
                doBeforeSave: machine =>
                {
                    var m = machine;
                    m.MachineTemplateID = template.ID;
                    m.ExpenseAccountID = glAccountID;
                    m.IncomeAccountID = glAccountID;
                },
                doBeforeSaveVariable: variable =>
                {
                    linkedMachineVar = variable;
                    linkedMachineVar.ID = -93;
                    linkedMachineVar.Name = "myLinkedMachine";
                    linkedMachineVar.InclusionFormula = $"={includeChildren.ToString()}";
                }
            );
            await this.CreateAssemblyVariable(false, assemblyID: template.ID, dobeforeSave: variable =>
            {
                var v = variable;
                //v.AssemblyID = template.ID;
                v.Name = "TotalQuantity";
                v.DataType = DataType.Number;
                v.ElementType = AssemblyElementType.Number;
                v.Label = "Total Quantity";
                v.DefaultValue = "=AssemblyQuantity.Value * LineItemQuantity";
                v.IsFormula = true;
                v.LinkedLaborID = null;
                v.LinkedMaterialID = null;
            });
            await this.CreateAssemblyVariable(false, assemblyID: template.ID, dobeforeSave: variable =>
            {
                var v = variable;
                //v.AssemblyID = template.ID;
                v.Name = "AssemblyQuantity";
                v.DataType = DataType.Number;
                v.ElementType = AssemblyElementType.Number;
                v.Label = "Assembly Quantity";
                v.DefaultValue = $"={LinkedMachineQtyPerLineItem}";
                v.IsFormula = true;
                v.LinkedLaborID = null;
                v.LinkedMaterialID = null;
            });



            //then compile assemblies
            await this.CompileAssembly(template);//machine templates are also assemblies
            await this.CompileMachine(machine);
            await this.CompileAssembly(assembly);

            const int LineItemQuantity = 1;

            OrderItemComponent oicMachine = new OrderItemComponent()
            {
                ID = -97,
                TempID = Guid.NewGuid(),
                VariableName = linkedMachineVar.Name,
                ComponentID = machine.ID,
                ComponentType = OrderItemComponentType.Machine,
                TotalQuantity = LineItemQuantity * LinkedMachineQtyPerLineItem, // 3
                TotalQuantityOV = false,
                AssemblyQuantity = LinkedMachineQtyPerLineItem, // 3
                AssemblyQuantityOV = false,
                PriceUnit = 3,
                PricePreTax = 6,//PriceUnit*Quantity
                PriceUnitOV = false,
                CostUnit = 1,
                CostOV = false,
                IncomeAccountID = machine.IncomeAccountID,
                ExpenseAccountID = machine.ExpenseAccountID,
                IncomeAllocationType = null,
            };

            OrderItemComponent oicAssembly = new OrderItemComponent()
            {
                ID = -98,
                TempID = Guid.NewGuid(),
                VariableName = null,
                ComponentID = assembly.ID,
                ComponentType = OrderItemComponentType.Assembly,
                TotalQuantity = 2,
                TotalQuantityOV = qtyOV,
                AssemblyQuantity = 2,
                AssemblyQuantityOV = qtyOV,
                PriceUnit = 3,
                PricePreTax = 6,//PriceUnit*Quantity
                PriceUnitOV = priceOV,
                CostUnit = 1,
                CostOV = costOV,
                IncomeAccountID = glAccountID,
                ExpenseAccountID = glAccountID,
                IncomeAllocationType = AssemblyIncomeAllocationType.AllocationByMaterialIncomeAccount,
                ChildComponents = new List<OrderItemComponent>
                {
                    oicMachine
                }
            };
            //assemblydatajson for assembly
            var dcAssemblyDataJSON = new Dictionary<string, VariableValue>()
            {
                { "AssemblyQuantity", new VariableValue{ Value = oicAssembly.TotalQuantity.ToString(), ValueOV = oicAssembly.AssemblyQuantityOV} },
                { "Price", new VariableValue{ Value = oicAssembly.PricePreTax.ToString(), ValueOV = oicAssembly.PriceUnitOV } },
            };
            oicAssembly.AssemblyDataJSON = JsonConvert.SerializeObject(dcAssemblyDataJSON);
            //assemblydatajson for machine
            var dcMachineAssemblyDataJSON = new Dictionary<string, VariableValue>()
            {
                { "Quantity", new VariableValue{ Value = oicMachine.TotalQuantity.ToString(), ValueOV = oicMachine.AssemblyQuantityOV} },
                { "Price", new VariableValue{ Value = oicMachine.PricePreTax.ToString(), ValueOV = oicMachine.PriceUnitOV } },
            };
            oicMachine.AssemblyDataJSON = JsonConvert.SerializeObject(dcMachineAssemblyDataJSON);

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>() { oicAssembly.ToPriceRequest(_companyID) };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = LineItemQuantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };
            OrderPriceRequest orderRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                    itemPriceRequest
                }
            };

            OrderPriceResult result = await pricingEngine.Compute(orderRequest, false);
            Assert.IsNotNull(result);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);

            var assemblyResult = result.Items[0].Components.FirstOrDefault(x => x.ID == oicAssembly.ID);
            Assert.IsNotNull(assemblyResult);
            Console.WriteLine(JsonConvert.SerializeObject(new { 
                computeErrors = assemblyResult?.Errors
            }, Formatting.Indented));
            decimal? effectiveQty = qtyOV? oicAssembly.TotalQuantity : itemPriceRequest.Quantity * 3;//where 3 is AssemblyQuantity Variable

            Assert.AreEqual(oicAssembly.ID, /****************/ assemblyResult.ID);
            Assert.AreEqual(oicAssembly.TempID, /************/ assemblyResult.TempID);
            Assert.AreEqual(oicAssembly.AssemblyID, /***********/ assemblyResult.ComponentID);
            if (qtyOV)
            {
                Assert.AreEqual(oicAssembly.TotalQuantity, /**********/ assemblyResult.TotalQuantity);
                Assert.AreEqual(oicAssembly.AssemblyQuantity, /***/ assemblyResult.AssemblyQuantity);
            }
            else
            {
                Assert.AreEqual(effectiveQty, /**********/ assemblyResult.TotalQuantity, "expect assembly quantity to calculate");
                Assert.AreEqual(effectiveQty/itemPriceRequest.Quantity, /***/ assemblyResult.AssemblyQuantity);
            }
            Assert.AreEqual(oicAssembly.AssemblyQuantityOV, /********/ assemblyResult.AssemblyQuantityOV);
            Assert.AreEqual(oicAssembly.TotalQuantityOV, /**/ assemblyResult.TotalQuantityOV);
            if (priceOV)
            {
                Assert.AreEqual(oicAssembly.PriceUnit, /*********/ assemblyResult.PriceUnit);
            }
            else
            {
                var materialPrice = this.GetMaterialExpectedPriceUnit(material);
                var laborPrice = this.GetLaborExpectedPriceUnit(labor, 1);
                Assert.AreEqual(materialPrice + laborPrice + assembly.FixedPrice, assemblyResult.PriceUnit, "expect assembly Price to be material+labor+fixed");
            }

            Assert.AreEqual(oicAssembly.PriceUnitOV, /*******/ assemblyResult.PriceUnitOV, "expect assembly PriceUnitOV to match");
            if (costOV)
            {
                Assert.AreEqual(oicAssembly.CostUnit, /**********/ assemblyResult.CostUnit);
            }
            else if (includeChildren)
            {
                decimal? laborQty = LaborQuantityHelper
                                            .ComputeQuantity(new Measurement(labor.BillingIncrementInMin, Unit.Minute),
                                                             new Measurement(labor.MinimumTimeInMin, Unit.Minute),
                                                             1);

                decimal? materialCost = this.GetMaterialExpectedCostUnit(material);
                decimal? laborCost = this.GetLaborExpectedCost(labor, laborQty);

                decimal? expectedUnitCost = Math.Round((materialCost.Value + laborCost.Value) / assemblyResult.TotalQuantity.Value, 2); 
                Assert.AreEqual(expectedUnitCost, assemblyResult.CostUnit, "expect assemblyCost to be material+labor");
            }

            Assert.AreEqual(oicAssembly.CostOV, /************/ assemblyResult.CostOV);
            Assert.AreEqual(oicAssembly.IncomeAccountID, /***/ assemblyResult.IncomeAccountID);
            Assert.AreEqual(oicAssembly.ExpenseAccountID, /**/ assemblyResult.ExpenseAccountID);
            Assert.AreEqual(oicAssembly.IncomeAllocationType, assemblyResult.IncomeAllocationType);
            if (includeChildren)
            {
                Assert.AreEqual(3, assemblyResult?.ChildComponents?.Count, message: "assemblyResult.ChildComponents.Count == 3");
            }
            else
            {
                Assert.AreEqual(0, assemblyResult?.ChildComponents?.Count ?? 0, message: "assemblyResult.ChildComponents.Count == 0");
            }


            if(includeChildren)
            {   //roundtrip test for machine
                var machineResult = assemblyResult?.ChildComponents?.FirstOrDefault(x => x.VariableName == linkedMachineVar.Name);
                Assert.IsNotNull(machineResult);

                Assert.AreEqual(0, /****************************/ machineResult.ID);
                Assert.AreEqual(null, /*************************/ machineResult.TempID);
                Assert.AreEqual(oicMachine.MachineID, /*********/ machineResult.ComponentID);
                Assert.AreEqual(oicMachine.TotalQuantity, /**********/ machineResult.TotalQuantity, "expect machine quantity to calculate");
                Assert.AreEqual(oicMachine.AssemblyQuantity, /***/ machineResult.AssemblyQuantity, "expect machine qtyPerItem to calculate");
                Assert.AreEqual(oicMachine.AssemblyQuantityOV, /********/ machineResult.AssemblyQuantityOV, "expect machine QuantityOV");
                Assert.AreEqual(oicMachine.TotalQuantityOV, /**/ machineResult.TotalQuantityOV);
                Assert.AreEqual(null, /****************************/ machineResult.PriceUnit);
                Assert.AreEqual(oicMachine.PriceUnitOV, /*******/ machineResult.PriceUnitOV);
                Assert.AreEqual(0, /****************************/ machineResult.CostUnit);
                Assert.AreEqual(oicMachine.CostOV, /************/ machineResult.CostOV);

                Assert.AreEqual(oicMachine.IncomeAccountID, /***/ machineResult.IncomeAccountID    , "expect income/expense to default");
                Assert.AreEqual(oicMachine.ExpenseAccountID, /**/ machineResult.ExpenseAccountID   , "expect income/expense to default");
                Assert.AreEqual(oicMachine.IncomeAllocationType, machineResult.IncomeAllocationType, "expect income/expense to default");
            }

        }


        //            if(includeChildren)
        //            {   //roundtrip test for machine
        //                var machineResult = assemblyResult?.ChildComponents?.FirstOrDefault(x => x.VariableName == linkedMachineVar.Name);
        //        Assert.IsNotNull(machineResult);
        //                //Assert.AreEqual(oicMachine.ID, /****************/ machineResult.ID);
        //                //Assert.AreEqual(oicMachine.TempID, /************/ machineResult.TempID);
        //                Assert.AreEqual(oicMachine.MachineID, /***********/ machineResult.ComponentID);
        //                if (qtyOV)
        //                {
        //                    Assert.AreEqual(oicMachine.Quantity, machineResult.Quantity, "expect machine quantity OV to match");
        //                    Assert.AreEqual(oicMachine.AssemblyQuantity, machineResult.AssemblyQuantity, "expect machine qtyPerItem OV to match");
        //                }
        //                else
        //                {
        //                    Assert.AreEqual(oicMachine.Quantity, machineResult.Quantity, "expect machine quantity to calculate");
        //                    Assert.AreEqual(oicMachine.AssemblyQuantity, machineResult.AssemblyQuantity, "expect machine qtyPerItem to calculate");
        //                }
        //Assert.AreEqual(oicMachine.QuantityOV, /********/ machineResult.QuantityOV, "expect machine QuantityOV");
        //                Assert.AreEqual(oicMachine.TotalQuantityOV, /**/ machineResult.TotalQuantityOV);
        //                if (priceOV)
        //                {
        //                    Assert.AreEqual(oicMachine.PriceUnit, /*********/ machineResult.PriceUnit);
        //                }
        //                else
        //                {
        //                    Assert.AreEqual(0, machineResult.PriceUnit);
        //                }
        //                Assert.AreEqual(oicMachine.PriceUnitOV, /*******/ machineResult.PriceUnitOV);
        //                if (costOV)
        //                {
        //                    Assert.AreEqual(oicMachine.CostUnit, /**********/ machineResult.CostUnit);
        //                }
        //                else
        //                {
        //                    Assert.AreEqual(0, machineResult.CostUnit);
        //                }
        //                Assert.AreEqual(oicMachine.CostOV, /************/ machineResult.CostOV);
        //                Assert.AreEqual(oicMachine.IncomeAccountID, /***/ machineResult.IncomeAccountID);
        //                Assert.AreEqual(oicMachine.ExpenseAccountID, /**/ machineResult.ExpenseAccountID);
        //                Assert.AreEqual(oicMachine.IncomeAllocationType, machineResult.IncomeAllocationType);
        //            }

        /// <summary>
        /// refer to ComponentPriceEngine_Labor.Compute
        /// </summary>
        protected decimal? GetLaborExpectedPriceUnit(LaborData labor, decimal? effectiveQty)
        {
            var pricePreTax = labor.EstimatingPriceFixed.GetValueOrDefault(0m) + (labor.EstimatingPricePerHour.GetValueOrDefault(0m) / 60m * effectiveQty);
            return pricePreTax / effectiveQty;
        }

        protected decimal? GetMaterialExpectedPriceUnit(MaterialData material)
        {
            return material.EstimatingPrice;
        }

        protected decimal? GetLaborExpectedCost(LaborData labor, decimal? effectiveQty)
        {
            return labor.EstimatingCostFixed.GetValueOrDefault(0m) + (labor.EstimatingCostPerHour.GetValueOrDefault(0m) / 60m * effectiveQty);
        }

        /// <summary>
        /// refer to ComponentPriceEngine_Labor.Compute
        /// </summary>
        protected decimal? GetLaborExpectedCostUnit(LaborData labor, decimal? effectiveQty)
        {
            var costLabor = labor.EstimatingCostFixed.GetValueOrDefault(0m) + (labor.EstimatingCostPerHour.GetValueOrDefault(0m) / 60m * effectiveQty);
            return costLabor / effectiveQty;
        }

        protected decimal? GetMaterialExpectedCostUnit(MaterialData material)
        {
            return material.EstimatingCost;
        }

    }
}
