﻿using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class PriceDiscountTests
    {
        private ApiContext ctx = null;
        private PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        private const short BID = 1;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestMethod]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 5.00, 0.00, 0.00, 0.00, 4.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 250.00, 0.00, 0.00, 0.00, 1.00)]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 5.00, 15.00, 0.00, 1.00, 4.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 250.00, 20.00, 0.00, 1.00, 1.00)]
        public async Task ComponentPriceWithFixedDiscount(
            double _labor1PricePerMinute, double _labor1Quantity,
            double _material1Price, double _material1Quantity,
            double _material2Price, double _material2Quantity,
            double _fixedDiscountAmount,
            double _surchargePriceFixedAmount, double _surchargePricePerUnitAmount, double _surchargeQuantity,
            double _quantity
            )
        {
            decimal labor1PricePerMinute = (decimal)_labor1PricePerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal material1Price = (decimal)_material1Price;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material2Price = (decimal)_material2Price;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal fixedDiscountAmount = (decimal)_fixedDiscountAmount;
            decimal surchargePriceFixedAmount = (decimal)_surchargePriceFixedAmount;
            decimal surchargePricePerUnitAmount = (decimal)_surchargePricePerUnitAmount;
            decimal surchargeQuantity = (decimal)_surchargeQuantity;
            decimal quantity = (decimal)_quantity;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1", glAccountID, glAccountID, 0.00m, labor1PricePerMinute, 0.00m, 0.00m);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, material1Price, 0.00m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, material2Price, 0.00m);

            SurchargeDef surcharge = null;
            if (surchargePriceFixedAmount != 0m || surchargePricePerUnitAmount != 0m)
            {
                short taxCode = ctx.TaxabilityCodes.First(t => t.BID == BID).ID;
                surcharge = PricingTestHelper.InitSurcharge(ctx, BID, "Surcharge", glAccountID, taxCode, surchargePriceFixedAmount, surchargePricePerUnitAmount);
            }

            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true
                }

            };

            List<SurchargePriceRequest> surcharges = null;

            if (surcharge != null)
            {
                surcharges = new List<SurchargePriceRequest>
                {
                    new SurchargePriceRequest
                    {
                      SurchargeDefID = surcharge.ID,
                      Quantity = surchargeQuantity,
                      PriceFixedAmount = surchargePriceFixedAmount,
                      PricePerUnitAmount = surchargePricePerUnitAmount,
                    }
                };
            }

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                Surcharges = surcharges,
                ItemDiscountAmount = fixedDiscountAmount
            };

            decimal labor1CalcPrice = (labor1PricePerMinute * labor1Quantity);
            decimal material1CalcPrice = (material1Price * material1Quantity);
            decimal material2CalcPrice = (material2Price * material2Quantity);
            decimal surchargeCalcPrice = surchargePriceFixedAmount + (surchargeQuantity * surchargePricePerUnitAmount);

            decimal subtotalCalcPrice = labor1CalcPrice + material1CalcPrice + material2CalcPrice + surchargeCalcPrice;

            decimal discountTotal = fixedDiscountAmount;

            decimal totalCalcPrice = (subtotalCalcPrice - discountTotal);

            decimal labor1Percentage = subtotalCalcPrice == 0m ? 0m : labor1CalcPrice / subtotalCalcPrice;
            decimal material1Percentage = subtotalCalcPrice == 0m ? 0m : material1CalcPrice / subtotalCalcPrice;
            decimal material2Percentage = subtotalCalcPrice == 0m ? 0m : material2CalcPrice / subtotalCalcPrice;
            decimal surchargePercentage = subtotalCalcPrice == 0m ? 0m : surchargeCalcPrice / subtotalCalcPrice;

            decimal labor1TaxablePrice = labor1CalcPrice - decimal.Round(discountTotal * labor1Percentage, 2);
            decimal material1TaxablePrice = material1CalcPrice - decimal.Round(discountTotal * material1Percentage, 2);
            decimal material2TaxablePrice = material2CalcPrice - decimal.Round(discountTotal * material2Percentage, 2);
            decimal surchargeTaxablePrice = surchargeCalcPrice - decimal.Round(discountTotal * surchargePercentage, 2);

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalCalcPrice, result.PricePreTax);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1CalcPrice, labor1Result.PricePreTax);
            Assert.AreEqual(labor1TaxablePrice, labor1Result.PriceTaxable);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1CalcPrice, material1Result.PricePreTax);
            Assert.AreEqual(material1TaxablePrice, material1Result.PriceTaxable);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material2CalcPrice, material2Result.PricePreTax);
            Assert.AreEqual(material2TaxablePrice, material2Result.PriceTaxable);

            Assert.AreEqual(discountTotal, result.PriceDiscount);

            Assert.AreEqual(surchargeCalcPrice, result.PriceSurcharge);

            if (surcharge != null)
            {
                SurchargePriceResult surchargeResult = result.Surcharges?.FirstOrDefault();
                Assert.IsNotNull(surchargeResult);
                Assert.AreEqual(surchargeCalcPrice, surchargeResult.PricePreTax);
                Assert.AreEqual(surchargeTaxablePrice, surchargeResult.PriceTaxable);
            }

            else
                Assert.IsTrue(result.Surcharges == null || result.Surcharges.Count == 0);
        }

        [TestMethod]
        [DataRow(5.00, 4.00)]
        [DataRow(250.00, 1.00)]
        public async Task ComponentPriceWithFixedDiscountAndNoComponents(
            double _fixedDiscountAmount,
            double _quantity
            )
        {
            decimal fixedDiscountAmount = (decimal)_fixedDiscountAmount;
            decimal quantity = (decimal)_quantity;

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                ItemDiscountAmount = fixedDiscountAmount
            };

            decimal discountTotal = fixedDiscountAmount;

            decimal totalCalcPrice = -discountTotal;

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalCalcPrice, result.PricePreTax);

            Assert.AreEqual(discountTotal, result.PriceDiscount);

            Assert.AreEqual(0m, result.PriceSurcharge);

            SurchargePriceResult surchargeResult = result.Surcharges?.FirstOrDefault();
            Assert.IsNotNull(surchargeResult);
            Assert.AreEqual(0m, surchargeResult.PricePreTax);
            Assert.AreEqual(-discountTotal, surchargeResult.PriceTaxable);
        }

        [TestMethod]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 0.10, 0.00, 0.00, 0.00, 4.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 0.50, 0.00, 0.00, 0.00, 1.00)]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 0.10, 15.00, 0.00, 1.00, 4.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 0.10, 20.00, 0.00, 1.00, 1.00)]
        public async Task ComponentPriceWithPercentageDiscount(
            double _labor1PricePerMinute, double _labor1Quantity,
            double _material1Price, double _material1Quantity,
            double _material2Price, double _material2Quantity,
            double _percentageDiscountAmount,
            double _surchargePriceFixedAmount, double _surchargePricePerUnitAmount, double _surchargeQuantity,
            double _quantity
            )
        {
            decimal labor1PricePerMinute = (decimal)_labor1PricePerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal material1Price = (decimal)_material1Price;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material2Price = (decimal)_material2Price;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal percentageDiscountAmount = (decimal)_percentageDiscountAmount;
            decimal surchargePriceFixedAmount = (decimal)_surchargePriceFixedAmount;
            decimal surchargePricePerUnitAmount = (decimal)_surchargePricePerUnitAmount;
            decimal surchargeQuantity = (decimal)_surchargeQuantity;
            decimal quantity = (decimal)_quantity;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1", glAccountID, glAccountID, 0.00m, labor1PricePerMinute, 0.00m, 0.00m);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, material1Price, 0.00m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, material2Price, 0.00m);

            SurchargeDef surcharge = null;
            if (surchargePriceFixedAmount != 0m || surchargePricePerUnitAmount != 0m)
            {
                short taxCode = ctx.TaxabilityCodes.First(t => t.BID == BID).ID;
                surcharge = PricingTestHelper.InitSurcharge(ctx, BID, "Surcharge", glAccountID, taxCode, surchargePriceFixedAmount, surchargePricePerUnitAmount);
            }

            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true
                }

            };

            List<SurchargePriceRequest> surcharges = null;

            if (surcharge != null)
            {
                surcharges = new List<SurchargePriceRequest>
                {
                    new SurchargePriceRequest
                    {
                      SurchargeDefID = surcharge.ID,
                      Quantity = surchargeQuantity,
                      PriceFixedAmount = surchargePriceFixedAmount,
                      PricePerUnitAmount = surchargePricePerUnitAmount,
                    }
                };
            }

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                Surcharges = surcharges,
                ItemDiscountPercent = percentageDiscountAmount
            };

            decimal labor1CalcPrice = (labor1PricePerMinute * labor1Quantity);
            decimal material1CalcPrice = (material1Price * material1Quantity);
            decimal material2CalcPrice = (material2Price * material2Quantity);
            decimal surchargeCalcPrice = surchargePriceFixedAmount + (surchargeQuantity * surchargePricePerUnitAmount);

            decimal subtotalCalcPrice = labor1CalcPrice + material1CalcPrice + material2CalcPrice + surchargeCalcPrice;

            decimal discountTotal = decimal.Round(subtotalCalcPrice * percentageDiscountAmount, 2);

            decimal totalCalcPrice = (subtotalCalcPrice - discountTotal);

            decimal labor1Percentage = subtotalCalcPrice == 0m ? 0m : labor1CalcPrice / subtotalCalcPrice;
            decimal material1Percentage = subtotalCalcPrice == 0m ? 0m : material1CalcPrice / subtotalCalcPrice;
            decimal material2Percentage = subtotalCalcPrice == 0m ? 0m : material2CalcPrice / subtotalCalcPrice;
            decimal surchargePercentage = subtotalCalcPrice == 0m ? 0m : surchargeCalcPrice / subtotalCalcPrice;

            decimal labor1TaxablePrice = labor1CalcPrice - decimal.Round(discountTotal * labor1Percentage, 2);
            decimal material1TaxablePrice = material1CalcPrice - decimal.Round(discountTotal * material1Percentage, 2);
            decimal material2TaxablePrice = material2CalcPrice - decimal.Round(discountTotal * material2Percentage, 2);
            decimal surchargeTaxablePrice = surchargeCalcPrice - decimal.Round(discountTotal * surchargePercentage, 2);

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalCalcPrice, result.PricePreTax);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1CalcPrice, labor1Result.PricePreTax);
            Assert.AreEqual(labor1TaxablePrice, labor1Result.PriceTaxable);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1CalcPrice, material1Result.PricePreTax);
            Assert.AreEqual(material1TaxablePrice, material1Result.PriceTaxable);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material2CalcPrice, material2Result.PricePreTax);
            Assert.AreEqual(material2TaxablePrice, material2Result.PriceTaxable);

            Assert.AreEqual(discountTotal, result.PriceDiscount);

            Assert.AreEqual(surchargeCalcPrice, result.PriceSurcharge);

            if (surcharge != null)
            {
                SurchargePriceResult surchargeResult = result.Surcharges?.FirstOrDefault();
                Assert.IsNotNull(surchargeResult);
                Assert.AreEqual(surchargeCalcPrice, surchargeResult.PricePreTax);
                Assert.AreEqual(surchargeTaxablePrice, surchargeResult.PriceTaxable);
            }

            else
                Assert.IsTrue(result.Surcharges == null || result.Surcharges.Count == 0);
        }

        [TestMethod]
        async public Task TestComputeOrderWithDiscount()
        {
            short BID = 1;


            TaxGroup taxGroup = new TaxGroup()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "Baton Rouge",
                TaxRate = 11m,
            };

            TaxItem taxItem1 = new TaxItem()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "EBR",
                AgencyName = "EBR",
                InvoiceText = "EBR",
                GLAccount = new GLAccount()
                {
                    BID = 1,
                    ID = -99,
                    Name = "EBR Tax",
                    IsActive = true,
                    CanEdit = false,
                    GLAccountType = 23
                },
                TaxRate = 8m,
            };

            TaxItem taxItem2 = new TaxItem()
            {

                BID = 1,
                ID = -98,
                IsActive = true,
                Name = "BR City",
                AgencyName = "BR City",
                InvoiceText = "BR City",
                GLAccount = new GLAccount()
                {
                    BID = 1,
                    ID = -98,
                    Name = "BR City Tax",
                    IsActive = true,
                    CanEdit = false,
                    GLAccountType = 23
                },
                TaxRate = 3m,
            };

            TaxGroupItemLink taxGroupItemLink1 = new TaxGroupItemLink()
            {
                BID = 1,
                GroupID = taxGroup.ID,
                ItemID = taxItem1.ID,
            };

            TaxGroupItemLink taxGroupItemLink2 = new TaxGroupItemLink()
            {
                BID = 1,
                GroupID = taxGroup.ID,
                ItemID = taxItem2.ID,
            };

            MaterialData material = new MaterialData
            {
                BID = 1,
                ID = -99,
                EstimatingCost = 5,
                EstimatingPrice = 10,
                Name = "Material 1",
                InvoiceText = "Material 1",
                ExpenseAccountID = 6000,
                IncomeAccountID = 4000,
                InventoryAccountID = 1510,
            };


            OrderPriceResult result = null;

            try
            {
                using (var ctx = PricingTestHelper.GetMockCtx(BID))
                {
                    await TaxEngineTests.TestCleanup(ctx, BID);
                    await ctx.SaveChangesAsync();
                    await ctx.TaxGroup.AddAsync(taxGroup);
                    await ctx.TaxItem.AddRangeAsync(taxItem1, taxItem2);
                    await ctx.TaxGroupItemLink.AddRangeAsync(taxGroupItemLink1, taxGroupItemLink2);
                    await ctx.MaterialData.AddAsync(material);
                    Assert.IsTrue((await ctx.SaveChangesAsync()) > 0); //test insert
                    OrderPriceRequest req = new OrderPriceRequest()
                    {
                        DiscountList = new List<OrderDiscount>()
                        {
                            new OrderDiscount()
                            {
                                Name = "$100 off",
                                Discount = 100m,
                                IsPercentBased = false,
                            }
                        },
                        Items = new List<ItemPriceRequest>()
                        {
                            new ItemPriceRequest()
                            {
                                EngineType = PricingEngineType.SimplePart,
                                Quantity = 15,
                                Components = new List<ComponentPriceRequest>
                                {
                                    new ComponentPriceRequest
                                    {
                                        ComponentID = material.ID,
                                        ComponentType = OrderItemComponentType.Material,
                                        TotalQuantity = 15,
                                        PriceUnit = 5m,
                                    }
                                },
                                TaxInfoList = new List<TaxAssessmentRequest>()
                                {
                                }
                            },
                            new ItemPriceRequest()
                            {
                                EngineType = PricingEngineType.SimplePart,
                                Quantity = 6,
                                Components = new List<ComponentPriceRequest>
                                {
                                    new ComponentPriceRequest
                                    {
                                        ComponentID = material.ID,
                                        ComponentType = OrderItemComponentType.Material,
                                        TotalQuantity = 6,
                                        PriceUnit = 2.5m,
                                    }
                                },
                                TaxInfoList = new List<TaxAssessmentRequest>()
                                {
                                }
                            },
                            new ItemPriceRequest()
                            {
                                EngineType = PricingEngineType.SimplePart,
                                Quantity = 2,
                                Components = new List<ComponentPriceRequest>
                                {
                                    new ComponentPriceRequest
                                    {
                                        ComponentID = material.ID,
                                        ComponentType = OrderItemComponentType.Material,
                                        TotalQuantity = 2,
                                        PriceUnit = 3.5m,
                                    }
                                },
                                TaxInfoList = new List<TaxAssessmentRequest>()
                                {

                                }
                            }
                        },
                        Destinations = new List<DestinationPriceRequest>()
                        {
                            new DestinationPriceRequest()
                            {
                                EngineType = DestinationEngineType.Pickedup,
                            }
                        },
                        TaxNexusList = new Dictionary<string, TaxAssessmentNexus>()
                        {
                        }
                    };

                    cache = new TestHelper.MockTenantDataCache();
                    logger = new RemoteLogger(cache);

                    result = await (new PricingEngine(BID, ctx, cache, logger)).Compute(req, false);
                }
            }
            finally
            {
                using (var ctx = PricingTestHelper.GetMockCtx(BID))
                {
                    await TaxEngineTests.TestCleanup(ctx, BID);
                }
            }

            Assert.IsNotNull(result);

            Assert.IsNotNull(result.Items);
            Assert.IsNotNull(result.Destinations);

            Assert.AreEqual(3, result.Items.Count);
            Assert.AreEqual(1, result.Destinations.Count);

            Assert.AreEqual(130.0m, result.ItemPreTaxTotal);
            Assert.AreEqual(0m, result.DestPreTaxTotal);
            Assert.AreEqual(100m, result.PriceOrderDiscountTotal);
            Assert.AreEqual(130.0m, result.PricePreTax);

            //Assert.AreEqual(1000m, result.Items[0].PriceNet);
            Assert.AreEqual(65.22m, result.Items[0].PriceDiscount);
            Assert.AreEqual(84.78m, result.Items[0].PricePreTax);
            Assert.AreEqual(65.22m, result.Items[0].Components.First().PriceAppliedDiscount);
            Assert.AreEqual(150.00m, result.Items[0].Components.First().PricePreTax);
            Assert.AreEqual(84.78m, result.Items[0].Components.First().PriceTaxable);

            //Assert.AreEqual(300m, result.Items[1].PriceNet);
            Assert.AreEqual(26.09m, result.Items[1].PriceDiscount);
            Assert.AreEqual(33.91m, result.Items[1].PricePreTax);
            
            //Assert.AreEqual(500m, result.Items[2].PriceNet);
            Assert.AreEqual(8.69m, result.Items[2].PriceDiscount);
            Assert.AreEqual(11.31m, result.Items[2].PricePreTax);
            
            Assert.AreEqual(0m, result.Destinations[0].PriceNet);
            Assert.AreEqual(0m, result.Destinations[0].PricePreTax);
            
        }
    }
}