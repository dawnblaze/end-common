﻿using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.CBEL.Elements;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class PricingExampleTests
    {
        [TestMethod]
        public async Task LineItemDeliveryItem1Test()
        {
            //Line Item Component Element/Sub     Name              Value   IsOV     UnitCost  Cost   UnitPrice Price  DefaultValueFormula Unit Cost Units        Fixed Price     Per Unit Price
            //1                                                     1.00                       $7.02  $40.60    $40.60 
            //1         Assembly: Delivery
            //1                   Variable        AssemblyPrice     35.10                                              = TotalCost * 5
            //1                   Variable        AssemblyQuantity  1.00                                               = ItemQuantity.Value
            //1                   NumberElement   Distance          15.00                                              15
            //1                   NumberElement   TravelTime        1.00                                               = Distance.Value / 15
            //1                   LinkedMaterial  Gasoline          0.75             $3.25     $2.44                   = Distance.Value / 20   $ 3.25
            //1                   LinkedLabor     Driver            11.00           $25.00     $4.58                   = TravelTime.Value + 10   $ 25.00    Hour
            //
            //1   Setup Fee : Rush                                 $ 5.50                             $ 5.50    $ 5.00      $ 0.50

            short bid = 1;
            decimal? expectedLineItemPriceTotal = 40.60m;
            decimal? expectedLineItemCostTotal = 7.02m;
            decimal? expectedLineItemLaborCostTotal = 4.58m;
            decimal ItemQuantity = 1m;
            string costName = "TotalCost";

            int assemblyID = -250;
            string assemblyName = "Delivery";

            int assemblyPriceID = -250;
            string assemblyPriceName = "Price";
            string assemblyPriceDefaultFormula = $"={costName}*5";

            int assemblyQuantityID = -251;
            string assemblyQuantityName = "Quantity";
            string assemblyQuantityDefaultFormula = $"1";

            int distanceID = -252;
            string distanceName = "Distance";
            string distanceDefaultFormula = "15";

            int travelTimeID = -253;
            string travelTimeName = "TravelTime";
            string travelTimeDefaultFormula = $"=({distanceName}.Value/15)";

            int gasolineID = -254;
            string gasolineName = "GasolineMaterial";
            string gasolineDefaultFormula = gasolineName;
            string gasolineConsumptionFormula = $"={distanceName}.Value/20";
           

            int driverID = -255;
            string driverName = "DriverLabor";
            string driverDefaultFormula = driverName;
            string driverConsumptionFormula = $"={travelTimeName}.Value+10";

            int gasolineDescriptionID = -256;
            string gasolineDescriptionName = "GasolineDescripion";
            string gasolineDescriptionDefaultFormula = $"={gasolineName}.Description";

            int gasolineMaterialID = -250;
            string gasolineMaterialName = gasolineName;
            decimal? gasolineMaterialCost = 3.25m;

            int driverLaborID = -251;
            string driverLaborName = driverName;
            decimal? driverLaborCostPerHour = 25m;
            decimal? driverLaborPriceFixed = 0m;
            decimal? driverLaborCostFixed = 0m;
            decimal? driverLaborPricePerHour = 0m;

            int glExpenseAccountID = -260;
            string glExpenseAccountName = "glexpenseaccount";
            int glIncomeAccountID = -261;
            string glIncomeAccountName = "glincomeaccount";
            int glInventoryAccountID = -262;
            string glInventoryAccountName = "glinventoryaccount";

            short surchargeID = -250;
            decimal surchargePriceFixed = 5.00m;
            decimal surchargePriceEach = 0.50m;
            string surchargeDefName = "Setup Fee : Rush";

            short taxabilityCodeID = -250;
            string taxabilityCodeTaxCode = "TEST";
            string taxabilityCodeName = "TEST TAXABILITY CODE";

            GLAccount expenseAccount = GetExpenseAccount(bid, glExpenseAccountID, glExpenseAccountName);
            GLAccount incomeAccount = GetIncomeAccount(bid, glIncomeAccountID, glIncomeAccountName);
            GLAccount inventoryAccount = GetInventoryAccount(bid, glInventoryAccountID, glInventoryAccountName);

            MaterialData gasolineMaterial = GetMaterial(bid, gasolineMaterialID, gasolineMaterialName, gasolineMaterialCost, expenseAccount, incomeAccount, inventoryAccount);
            gasolineMaterial.Description = "Stinky";
            LaborData driverLabor = GetLabor(bid, driverLaborID, driverLaborName, driverLaborCostPerHour, driverLaborCostFixed, driverLaborPricePerHour, driverLaborPriceFixed, expenseAccount, incomeAccount);
            AssemblyVariable assemblyPrice = GetVariable(bid, assemblyPriceID, assemblyPriceName, assemblyPriceDefaultFormula, AssemblyElementType.Number, DataType.Number);
            AssemblyVariable assemblyQuantity = GetVariable(bid, assemblyQuantityID, assemblyQuantityName, assemblyQuantityDefaultFormula, AssemblyElementType.Number, DataType.Number);
            AssemblyVariable distance = GetVariable(bid, distanceID, distanceName, distanceDefaultFormula, AssemblyElementType.Number, DataType.Number);
            AssemblyVariable travelTime = GetVariable(bid, travelTimeID, travelTimeName, travelTimeDefaultFormula, AssemblyElementType.Number, DataType.Number);
            AssemblyVariable gasoline = GetVariable(bid, gasolineID, gasolineName, gasolineDefaultFormula, AssemblyElementType.LinkedMaterial, DataType.MaterialPart, gasolineConsumptionFormula);
            AssemblyVariable gasolineDescription = GetVariable(bid, gasolineDescriptionID, gasolineDescriptionName, gasolineDescriptionDefaultFormula, AssemblyElementType.SingleLineText, DataType.String);
            AssemblyVariable driver = GetVariable(bid, driverID, driverName, driverDefaultFormula, AssemblyElementType.LinkedLabor, DataType.LaborPart, driverConsumptionFormula);
            AssemblyData assembly = GetAssemblyData(bid, assemblyID, assemblyName, assemblyPrice, assemblyQuantity, distance, travelTime, gasoline, driver, gasolineDescription);
            

            TaxabilityCode taxabilityCode = GetTaxabilityCode(bid, taxabilityCodeID, taxabilityCodeName, taxabilityCodeTaxCode);

            SurchargeDef surchargeDef = GetSurchargeDef(bid, surchargeID, surchargePriceFixed, surchargePriceEach, surchargeDefName, incomeAccount, taxabilityCode);

            await PricingTestHelper.CleanupEntities(bid, assembly, assemblyPrice, assemblyQuantity, distance, travelTime, gasoline, driver, gasolineMaterial, driverLabor, surchargeDef, taxabilityCode, incomeAccount, expenseAccount, inventoryAccount, gasolineDescription);
            CreateEntities(bid, assembly, assemblyPrice, incomeAccount, expenseAccount, assemblyQuantity, distance, travelTime, gasoline, driver, gasolineMaterial, driverLabor, inventoryAccount, surchargeDef, taxabilityCode, gasolineDescription);

            ItemPriceRequest priceRequest = GetItemPriceRequest(bid, ItemQuantity,
                new List<ComponentPriceRequest>() { GetComponentPriceRequest(assemblyID, ItemQuantity) },
                new List<SurchargePriceRequest>() { GetSurchargePriceRequest(ItemQuantity, surchargeDef) });

            ItemPriceResult priceResult = await GetItemPriceResult(bid, priceRequest);

            Assert.AreEqual(expectedLineItemLaborCostTotal, priceResult.CostLabor);
            Assert.AreEqual(expectedLineItemCostTotal, priceResult.CostTotal);

            Assert.AreEqual(gasolineMaterial.Description, priceResult.Components.FirstOrDefault().Variables[gasolineDescriptionName].Value);
            Assert.AreEqual(expectedLineItemPriceTotal, priceResult.PriceTotal);
            priceRequest = GetItemPriceRequest(bid, ItemQuantity,
                new List<ComponentPriceRequest>() { GetComponentPriceRequest(assemblyID, ItemQuantity) },
                new List<SurchargePriceRequest>() { GetSurchargePriceRequest(ItemQuantity, surchargeDef) });
            
            priceRequest.Components[0].TotalQuantity = 2;
            priceRequest.Components[0].TotalQuantityOV = true;

            priceResult = await GetItemPriceResult(bid, priceRequest);
            Assert.AreEqual(2, priceResult.Components[0].TotalQuantity);

            Assert.AreEqual<bool?>(true, priceResult.Components[0].TotalQuantityOV);

            // Even though Quantity has increased due to being overridden, the formulas for the price and costs are NOT dependent on quantity
            // Therefor we should expect the same output as before
            Assert.AreEqual(expectedLineItemLaborCostTotal, priceResult.CostLabor);
            Assert.AreEqual(expectedLineItemCostTotal, priceResult.CostTotal);
            Assert.AreEqual(expectedLineItemPriceTotal, priceResult.PriceTotal);


            await PricingTestHelper.CleanupEntities(bid, assembly, assemblyPrice, assemblyQuantity, distance, travelTime, gasoline, driver, gasolineMaterial, driverLabor, surchargeDef, taxabilityCode, incomeAccount, expenseAccount, inventoryAccount, gasolineDescription);
        }

        private TaxabilityCode GetTaxabilityCode(short bid, short taxabilityCodeID, string taxabilityCodeName, string taxabilityCodeTaxCode)
        {
            return new TaxabilityCode()
            {
                BID = bid,
                ID = taxabilityCodeID,
                Name = taxabilityCodeName,
                TaxCode = taxabilityCodeTaxCode,
            };
        }

        private SurchargeDef GetSurchargeDef(short bid, short surchargeID, decimal surchargePriceFixed, decimal surchargePriceEach, string name, GLAccount incomeAccount, TaxabilityCode taxabilityCode)
        {
            return new SurchargeDef()
            {
                BID = bid,
                ID = surchargeID,
                DefaultFixedFee = surchargePriceFixed,
                DefaultPerUnitFee = surchargePriceEach,
                Name = name,
                IncomeAccount = incomeAccount,
                IsActive = true,
                TaxCode = taxabilityCode,
            };
        }

        private SurchargePriceRequest GetSurchargePriceRequest(decimal? quantity, SurchargeDef surchargeDef)
        {
            return new SurchargePriceRequest()
            {
                SurchargeDefID = surchargeDef.ID,
                Quantity = quantity,
                PricePerUnitAmount = surchargeDef.DefaultPerUnitFee,
                PriceFixedAmount = surchargeDef.DefaultFixedFee,
            };
        }

        private ComponentPriceRequest GetComponentPriceRequest(int componentID, decimal? quantity, Dictionary<string, VariableValue> variables = null, List<ComponentPriceRequest> components = null)
        {
            return new ComponentPriceRequest()
            {
                ComponentID = componentID,
                ComponentType = OrderItemComponentType.Assembly,
                TotalQuantity = quantity,
                Variables = variables,
                ChildComponents = components,
            };
        }

        private GLAccount GetInventoryAccount(short bid, int glInventoryAccountID, string glInventoryAccountName)
        {
            return new GLAccount()
            {
                BID = bid,
                ID = glInventoryAccountID,
                Name = glInventoryAccountName,
                GLAccountType = 13,
            };
        }

        private GLAccount GetIncomeAccount(short bid, int glIncomeAccountID, string glIncomeAccountName)
        {
            return new GLAccount()
            {
                BID = bid,
                ID = glIncomeAccountID,
                Name = glIncomeAccountName,
                GLAccountType = 40,
            };
        }

        private GLAccount GetExpenseAccount(short bid, int glExpenseAccountID, string glExpenseAccountName)
        {
            return new GLAccount()
            {
                BID = bid,
                ID = glExpenseAccountID,
                Name = glExpenseAccountName,
                GLAccountType = 60,
            };
        }

        private void CreateEntities(short bid, params object[] entities)
        {
            ApiContext ctx = new ApiContext(new TestHelper.MockTenantDataCache(), bid);
            foreach (var item in entities)
            {
                ctx.Add(item);
            }

            Assert.AreEqual(entities.Length, ctx.SaveChanges());
        }

        private AssemblyData GetAssemblyData(short bid, int id, string assemblyName, params AssemblyVariable[] assemblyVariables)
        {
            var assembly = new AssemblyData()
            {
                BID = bid,
                ID = id,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
            };

            foreach (var item in assemblyVariables)
            {
                assembly.Variables.Add(item);
            }

            return assembly;
        }

        private AssemblyVariable GetVariable(short bid, int id, string variableName, string defaultFormula, AssemblyElementType assemblyElementType, DataType dataType, string consumptionFormula = null)
        {
            return new AssemblyVariable()
            {
                BID = bid,
                ID = id,
                Name = variableName,
                ElementType = assemblyElementType,
                DataType = dataType,
                ConsumptionDefaultValue = consumptionFormula,
                DefaultValue = defaultFormula,
                IsFormula = !(String.IsNullOrWhiteSpace(defaultFormula) || defaultFormula.Length < 1 || defaultFormula[0] != '='),
                IsConsumptionFormula = !(String.IsNullOrWhiteSpace(consumptionFormula) || consumptionFormula.Length < 1 || consumptionFormula[0] != '='),
            };
        }

        private LaborData GetLabor(short bid, int id, string laborName, decimal? laborCostPerHour, decimal? laborCostFixed, decimal? laborPricePerHour, decimal? laborPriceFixed, GLAccount glExpenseAccount, GLAccount glIncomeAccount)
        {
            return new LaborData()
            {
                BID = bid,
                ID = id,
                Name = laborName,
                InvoiceText = laborName,
                EstimatingCostPerHour = laborCostPerHour.GetValueOrDefault(0m),
                EstimatingPricePerHour = laborPricePerHour.GetValueOrDefault(0m),
                EstimatingCostFixed = laborCostFixed.GetValueOrDefault(0m),
                EstimatingPriceFixed = laborPriceFixed.GetValueOrDefault(0m),
                BillingIncrementInMin = null,
                MinimumTimeInMin = null,
                IncomeAccount = glIncomeAccount,
                ExpenseAccount = glExpenseAccount,
            };
        }

        private MaterialData GetMaterial(short bid, int id, string materialName, decimal? materialCost, GLAccount glExpenseAccount, GLAccount glIncomeAccount, GLAccount glInventoryAccount)
        {
            return new MaterialData()
            {
                BID = bid,
                ID = id,
                Name = materialName,
                InvoiceText = materialName,
                EstimatingCost = materialCost.GetValueOrDefault(0m),
                IncomeAccount = glIncomeAccount,
                ExpenseAccount = glExpenseAccount,
                InventoryAccount = glInventoryAccount,
            };
        }

        private ItemPriceRequest GetItemPriceRequest(short bid, decimal itemQuantity, List<ComponentPriceRequest> components, List<SurchargePriceRequest> surcharges)
        {
            var request = new ItemPriceRequest()
            {
                Quantity = itemQuantity,
                Components = components,
                EngineType = PricingEngineType.Computed,
                Surcharges = surcharges,
            };

            return request;
        }

        private async Task<ItemPriceResult> GetItemPriceResult(short bid, ItemPriceRequest priceRequest)
        {
            var cache = new TestHelper.MockTenantDataCache();
            ApiContext ctx = new ApiContext(cache, bid);
            RemoteLogger logger = new RemoteLogger(cache);
            var pricingEngine = new PricingEngine(bid, ctx, cache, logger);
            var priceResult = await pricingEngine.Compute(priceRequest);
            return priceResult;
        }
    }
}
