﻿using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class PriceOverrideTests
    {
        private ApiContext ctx = null;
        private PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        private const short BID = 1;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestMethod]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 4.00, 100.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 1.00, 100.00)]
        public async Task LineItemWithOverriddenPrice(
            double _labor1PricePerMinute, double _labor1Quantity,
            double _material1Price, double _material1Quantity,
            double _material2Price, double _material2Quantity,
            double _quantity, double _ovUnitPrice
            )
        {
            decimal labor1PricePerMinute = (decimal)_labor1PricePerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal material1Price = (decimal)_material1Price;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material2Price = (decimal)_material2Price;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal quantity = (decimal)_quantity;
            decimal ovUnitPrice = (decimal)_ovUnitPrice;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1",  glAccountID, glAccountID, 0.00m, labor1PricePerMinute, 0.00m, 0.00m);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, material1Price, 0.00m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, material2Price, 0.00m);
            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true
                }

            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                UnitPrice = ovUnitPrice,
                UnitPriceOV = true,
            };

            decimal labor1CalcPrice = (labor1PricePerMinute * labor1Quantity);
            decimal material1CalcPrice = (material1Price * material1Quantity);
            decimal material2CalcPrice = (material2Price * material2Quantity);

            decimal totalComponentCalcPrice = labor1CalcPrice + material1CalcPrice + material2CalcPrice;
            decimal totalItemPrice = (ovUnitPrice * quantity);

            decimal labor1Percentage = totalComponentCalcPrice == 0m ? 0m : labor1CalcPrice / totalComponentCalcPrice;
            decimal material1Percentage = totalComponentCalcPrice == 0m ? 0m : material1CalcPrice / totalComponentCalcPrice;
            decimal material2Percentage = totalComponentCalcPrice == 0m ? 0m : material2CalcPrice / totalComponentCalcPrice;
            decimal totalComponentPrice = totalItemPrice;

            decimal labor1NewPrice = decimal.Round(totalComponentPrice * labor1Percentage, 2);
            decimal material1NewPrice = decimal.Round(totalComponentPrice * material1Percentage, 2);
            decimal material2NewPrice = decimal.Round(totalComponentPrice * material2Percentage, 2);

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalItemPrice, result.PricePreTax);
            Assert.AreEqual(ovUnitPrice, result.UnitPrice);
            Assert.IsTrue(result.UnitPriceOV);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1NewPrice, labor1Result.PricePreTax);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1NewPrice, material1Result.PricePreTax);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material2NewPrice, material2Result.PricePreTax);
        }

        [TestMethod]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 2.00, 5.00, 2.00, 4.00, 100.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 1.00, 1.00, 1.00, 1.00, 100.00)]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 1.00, 1.00, 1.00, 1.00, 100.00)]
        public async Task LineItemWithOverriddenPriceWithSurcharge(
            double _labor1PricePerMinute, double _labor1Quantity,
            double _material1Price, double _material1Quantity,
            double _material2Price, double _material2Quantity,
            double _surchargePriceFixedAmount, double _surchargePricePerUnitAmount, double _surchargeQuantity,
            double _quantity, double _ovUnitPrice
            )
        {
            decimal labor1PricePerMinute = (decimal)_labor1PricePerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal material1Price = (decimal)_material1Price;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material2Price = (decimal)_material2Price;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal surchargePriceFixedAmount = (decimal)_surchargePriceFixedAmount;
            decimal surchargePricePerUnitAmount = (decimal)_surchargePricePerUnitAmount;
            decimal surchargeQuantity = (decimal)_surchargeQuantity;
            decimal quantity = (decimal)_quantity;
            decimal ovUnitPrice = (decimal)_ovUnitPrice;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;
            short taxCode = ctx.TaxabilityCodes.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1", glAccountID, glAccountID, 0.00m, labor1PricePerMinute, 0.00m, 0.00m);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, material1Price, 0.00m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, material2Price, 0.00m);
            SurchargeDef surcharge = PricingTestHelper.InitSurcharge(ctx, BID, "Surcharge", glAccountID, taxCode, surchargePriceFixedAmount, surchargePricePerUnitAmount);
            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true
                }

            };

            List<SurchargePriceRequest> surcharges = new List<SurchargePriceRequest>
            {
                new SurchargePriceRequest
                {
                  SurchargeDefID = surcharge.ID,
                  Quantity = surchargeQuantity,
                  PriceFixedAmount = surchargePriceFixedAmount,
                  PricePerUnitAmount = surchargePricePerUnitAmount,
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                Surcharges = surcharges,
                UnitPrice = ovUnitPrice,
                UnitPriceOV = true,
            };

            decimal labor1CalcPrice = (labor1PricePerMinute * labor1Quantity);
            decimal material1CalcPrice = (material1Price * material1Quantity);
            decimal material2CalcPrice = (material2Price * material2Quantity);

            decimal totalComponentCalcPrice = labor1CalcPrice + material1CalcPrice + material2CalcPrice;
            decimal totalItemPrice = (ovUnitPrice * quantity);

            decimal surchargeTotal = surchargePriceFixedAmount + (surchargeQuantity * surchargePricePerUnitAmount);

            decimal labor1Percentage = totalComponentCalcPrice == 0m ? 0m : labor1CalcPrice / totalComponentCalcPrice;
            decimal material1Percentage = totalComponentCalcPrice == 0m ? 0m : material1CalcPrice / totalComponentCalcPrice;
            decimal material2Percentage = totalComponentCalcPrice == 0m ? 0m : material2CalcPrice / totalComponentCalcPrice;
            decimal totalComponentPrice = (totalItemPrice - surchargeTotal);

            decimal labor1NewPrice = decimal.Round(totalComponentPrice * labor1Percentage, 2);
            decimal material1NewPrice = decimal.Round(totalComponentPrice * material1Percentage, 2);
            decimal material2NewPrice = decimal.Round(totalComponentPrice * material2Percentage, 2);


            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalItemPrice, result.PricePreTax);
            Assert.AreEqual(ovUnitPrice, result.UnitPrice);
            Assert.IsTrue(result.UnitPriceOV);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1NewPrice, labor1Result.PricePreTax);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1NewPrice, material1Result.PricePreTax);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material2NewPrice, material2Result.PricePreTax);

            Assert.AreEqual(surchargeTotal, result.PriceSurcharge);

            if (surchargeTotal != 0m)
            {
                SurchargePriceResult surchargeResult = result.Surcharges.FirstOrDefault(x => x.SurchargeDefID == surcharge.ID);
                Assert.IsNotNull(surchargeResult);
                Assert.AreEqual(surchargeTotal, surchargeResult.PricePreTax);
            }

            else
                Assert.IsTrue(result.Surcharges == null || result.Surcharges.Count == 0);
        }

        [TestMethod]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 1.00, 1.00, 200.00, 4.00, 100.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 1.00, 1.00, 5.00, 1.00, 100.00)]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 1.00, 1.00, 200.00, 1.00, 100.00)]
        public async Task LineItemWithOverriddenPriceWithOverriddenComponent(
            double _labor1PricePerMinute, double _labor1Quantity,
            double _material1Price, double _material1Quantity,
            double _material2Price, double _material2Quantity,
            double _material3Price, double _material3Quantity, double _material3OVPrice,
            double _quantity, double _ovUnitPrice
            )
        {
            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            decimal labor1PricePerMinute = (decimal)_labor1PricePerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal material1Price = (decimal)_material1Price;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material2Price = (decimal)_material2Price;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal material3Price = (decimal)_material3Price;
            decimal material3Quantity = (decimal)_material3Quantity;
            decimal material3OVPrice = (decimal)_material3OVPrice;
            decimal quantity = (decimal)_quantity;
            decimal ovUnitPrice = (decimal)_ovUnitPrice;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1", glAccountID, glAccountID, 0.00m, labor1PricePerMinute, 0.00m, 0.00m);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, material1Price, 0.00m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, material2Price, 0.00m);
            MaterialData material3 = PricingTestHelper.InitMaterial(ctx, BID, "material3", glAccountID, glAccountID, glAccountID, material3Price, 0.00m);
            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material3.ID,
                    TotalQuantity = material3Quantity,
                    TotalQuantityOV = true,
                    PriceUnit = material3OVPrice,
                    PriceUnitOV = true
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                UnitPrice = ovUnitPrice,
                UnitPriceOV = true,
            };

            decimal labor1CalcPrice = (labor1PricePerMinute * labor1Quantity);
            decimal material1CalcPrice = (material1Price * material1Quantity);
            decimal material2CalcPrice = (material2Price * material2Quantity);

            decimal totalComponentCalcPrice = labor1CalcPrice + material1CalcPrice + material2CalcPrice;
            decimal totalItemPrice = (ovUnitPrice * quantity);

            decimal labor1Percentage = totalComponentCalcPrice == 0m ? 0m : labor1CalcPrice / totalComponentCalcPrice;
            decimal material1Percentage = totalComponentCalcPrice == 0m ? 0m : material1CalcPrice / totalComponentCalcPrice;
            decimal material2Percentage = totalComponentCalcPrice == 0m ? 0m : material2CalcPrice / totalComponentCalcPrice;
            decimal totalComponentPrice = (totalItemPrice - material3OVPrice);

            decimal labor1NewPrice = decimal.Round(totalComponentPrice * labor1Percentage, 2);
            decimal material1NewPrice = decimal.Round(totalComponentPrice * material1Percentage, 2);
            decimal material2NewPrice = decimal.Round(totalComponentPrice * material2Percentage, 2);

            decimal totalComponentNewPrice = labor1NewPrice + material1NewPrice + material2NewPrice;
            decimal surchargeTotal = totalItemPrice - (totalComponentNewPrice + material3OVPrice);

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalItemPrice, result.PricePreTax);
            Assert.AreEqual(ovUnitPrice, result.UnitPrice);
            Assert.IsTrue(result.UnitPriceOV);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1NewPrice, labor1Result.PricePreTax);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1NewPrice, material1Result.PricePreTax);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material2NewPrice, material2Result.PricePreTax);

            ComponentPriceResult material3Result = result.Components.FirstOrDefault(x => x.ComponentID == material3.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material3Result);
            Assert.AreEqual(material3OVPrice, material3Result.PricePreTax);

            Assert.AreEqual(surchargeTotal, result.PriceSurcharge);

            if (surchargeTotal != 0m)
            {
                SurchargePriceResult surchargeResult = result.Surcharges.FirstOrDefault(x => x.SurchargeDefID == 1);
                Assert.IsNotNull(surchargeResult);
                Assert.AreEqual(surchargeTotal, surchargeResult.PricePreTax);
            }

            else
                Assert.IsTrue(result.Surcharges == null || result.Surcharges.Count == 0);
        }

        [TestMethod]
        [DataRow(4.00, 100.00)]
        [DataRow(1.00, 100.00)]
        public async Task LineItemWithOverriddenPriceWithNoComponent(
            double _quantity, double _ovUnitPrice
            )
        {
            decimal quantity = (decimal)_quantity;
            decimal ovUnitPrice = (decimal)_ovUnitPrice;

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                UnitPrice = ovUnitPrice,
                UnitPriceOV = true,
            };

            decimal totalItemPrice = (ovUnitPrice * quantity);
            decimal surchargeTotal = totalItemPrice;

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalItemPrice, result.PricePreTax);
            Assert.AreEqual(ovUnitPrice, result.UnitPrice);
            Assert.IsTrue(result.UnitPriceOV);

            Assert.AreEqual(surchargeTotal, result.PriceSurcharge);

            if (surchargeTotal != 0m)
            {
                SurchargePriceResult surchargeResult = result.Surcharges.FirstOrDefault(x => x.SurchargeDefID == 1);
                Assert.IsNotNull(surchargeResult);
                Assert.AreEqual(surchargeTotal, surchargeResult.PricePreTax);
            }

            else
                Assert.IsTrue(result.Surcharges == null || result.Surcharges.Count == 0);
        }

        [TestMethod]
        [DataRow(1.00, 1.00, 200.00, 4.00, 100.00)]
        [DataRow(1.00, 1.00, 5.00, 1.00, 100.00)]
        [DataRow(1.00, 1.00, 200.00, 1.00, 100.00)]
        public async Task LineItemWithOverriddenPriceWithAllOverriddenComponent(
            double _material1Price, double _material1Quantity, double _material1OVPrice,
            double _quantity, double _ovUnitPrice
            )
        {
            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            decimal material1Price = (decimal)_material1Price;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material1OVPrice = (decimal)_material1OVPrice;
            decimal quantity = (decimal)_quantity;
            decimal ovUnitPrice = (decimal)_ovUnitPrice;

            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, material1Price, 0.00m);
            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true,
                    PriceUnit = material1OVPrice,
                    PriceUnitOV = true
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                UnitPrice = ovUnitPrice,
                UnitPriceOV = true,
            };

            decimal totalItemPrice = (ovUnitPrice * quantity);
            decimal surchargeTotal = totalItemPrice - material1OVPrice;

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalItemPrice, result.PricePreTax);
            Assert.AreEqual(ovUnitPrice, result.UnitPrice);
            Assert.IsTrue(result.UnitPriceOV);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1OVPrice, material1Result.PricePreTax);

            Assert.AreEqual(surchargeTotal, result.PriceSurcharge);

            if (surchargeTotal != 0m)
            {
                SurchargePriceResult surchargeResult = result.Surcharges.FirstOrDefault(x => x.SurchargeDefID == 1);
                Assert.IsNotNull(surchargeResult);
                Assert.AreEqual(surchargeTotal, surchargeResult.PricePreTax);
            }

            else
                Assert.IsTrue(result.Surcharges == null || result.Surcharges.Count == 0);
        }

        [TestMethod]
        [DataRow(1.00, 1.00, 4.00, 4.00, 100.00)]
        [DataRow(1.00, 1.00, 1.00, 1.00, 100.00)]
        public async Task LineItemWithOverriddenPriceWithAllZeros(
            double _labor1Quantity,
            double _material1Quantity,
            double _material2Quantity,
            double _quantity, double _ovUnitPrice
            )
        {
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal quantity = (decimal)_quantity;
            decimal ovUnitPrice = (decimal)_ovUnitPrice;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1", glAccountID, glAccountID, 0.00m, 0.00m, 0.00m, 0.00m);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, 0.00m, 0.00m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, 0.00m, 0.00m);
            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true
                }

            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                UnitPrice = ovUnitPrice,
                UnitPriceOV = true,
            };

            decimal totalItemPrice = (ovUnitPrice * quantity);

            decimal labor1Percentage = 1m/3m;
            decimal material1Percentage = 1m/3m;
            decimal material2Percentage = 1m/3m;
            decimal totalComponentPrice = totalItemPrice;

            decimal labor1NewPrice = decimal.Round(totalComponentPrice * labor1Percentage, 2);
            decimal material1NewPrice = decimal.Round(totalComponentPrice * material1Percentage, 2);
            decimal material2NewPrice = decimal.Round(totalComponentPrice * material2Percentage, 2);

            labor1NewPrice += totalComponentPrice - (labor1NewPrice + material1NewPrice + material2NewPrice);

            decimal surchargeTotal = 0.00m;

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalItemPrice, result.PricePreTax);
            Assert.AreEqual(ovUnitPrice, result.UnitPrice);
            Assert.IsTrue(result.UnitPriceOV);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1NewPrice, labor1Result.PricePreTax);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1NewPrice, material1Result.PricePreTax);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material2NewPrice, material2Result.PricePreTax);

            Assert.AreEqual(surchargeTotal, result.PriceSurcharge);

            if (surchargeTotal != 0m)
            {
                SurchargePriceResult surchargeResult = result.Surcharges.FirstOrDefault(x => x.SurchargeDefID == 1);
                Assert.IsNotNull(surchargeResult);
                Assert.AreEqual(surchargeTotal, surchargeResult.PricePreTax);
            }

            else
                Assert.IsTrue(result.Surcharges == null || result.Surcharges.Count == 0);
        }

        [TestMethod]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 5.00, 4.00, 100.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 250.00, 1.00, 100.00)]
        public async Task LineItemWithOverriddenPriceAndFixedDiscount(
            double _labor1PricePerMinute, double _labor1Quantity,
            double _material1Price, double _material1Quantity,
            double _material2Price, double _material2Quantity,
            double _fixedDiscountAmount,
            double _quantity, double _ovUnitPrice
            )
        {
            decimal labor1PricePerMinute = (decimal)_labor1PricePerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal material1Price = (decimal)_material1Price;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material2Price = (decimal)_material2Price;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal fixedDiscountAmount = (decimal)_fixedDiscountAmount;
            decimal quantity = (decimal)_quantity;
            decimal ovUnitPrice = (decimal)_ovUnitPrice;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1", glAccountID, glAccountID, 0.00m, labor1PricePerMinute, 0.00m, 0.00m);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, material1Price, 0.00m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, material2Price, 0.00m);
            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true
                }

            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                UnitPrice = ovUnitPrice,
                UnitPriceOV = true,
                ItemDiscountAmount = fixedDiscountAmount
            };

            decimal labor1CalcPrice = (labor1PricePerMinute * labor1Quantity);
            decimal material1CalcPrice = (material1Price * material1Quantity);
            decimal material2CalcPrice = (material2Price * material2Quantity);

            decimal totalComponentCalcPrice = labor1CalcPrice + material1CalcPrice + material2CalcPrice;
            decimal totalItemPrice = (ovUnitPrice * quantity);

            decimal labor1Percentage = totalComponentCalcPrice == 0m ? 0m : labor1CalcPrice / totalComponentCalcPrice;
            decimal material1Percentage = totalComponentCalcPrice == 0m ? 0m : material1CalcPrice / totalComponentCalcPrice;
            decimal material2Percentage = totalComponentCalcPrice == 0m ? 0m : material2CalcPrice / totalComponentCalcPrice;
            decimal totalComponentPrice = (totalItemPrice + fixedDiscountAmount);

            decimal labor1NewPrice = decimal.Round(totalComponentPrice * labor1Percentage, 2);
            decimal material1NewPrice = decimal.Round(totalComponentPrice * material1Percentage, 2);
            decimal material2NewPrice = decimal.Round(totalComponentPrice * material2Percentage, 2);

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalItemPrice, result.PricePreTax);
            Assert.AreEqual(ovUnitPrice, result.UnitPrice);
            Assert.IsTrue(result.UnitPriceOV);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1NewPrice, labor1Result.PricePreTax);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1NewPrice, material1Result.PricePreTax);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material2NewPrice, material2Result.PricePreTax);

            Assert.AreEqual(fixedDiscountAmount, result.PriceDiscount);
        }

        [TestMethod]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 0.10, 0.00, 0.00, 0.00, 4.00, 100.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 0.50, 0.00, 0.00, 0.00, 1.00, 100.00)]
        [DataRow(55.00, 1.00, 25.00, 1.00, 5.00, 4.00, 0.10, 15.00, 0.00, 1.00, 4.00, 100.00)]
        [DataRow(1.00, 1.00, 0.00, 1.00, 0.00, 1.00, 0.10, 20.00, 0.00, 1.00, 1.00, 55.00)]
        public async Task LineItemWithOverriddenPriceAndPercentageDiscount(
            double _labor1PricePerMinute, double _labor1Quantity,
            double _material1Price, double _material1Quantity,
            double _material2Price, double _material2Quantity,
            double _percentageDiscountAmount,
            double _surchargePriceFixedAmount, double _surchargePricePerUnitAmount, double _surchargeQuantity,
            double _quantity, double _ovUnitPrice
            )
        {
            decimal labor1PricePerMinute = (decimal)_labor1PricePerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal material1Price = (decimal)_material1Price;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material2Price = (decimal)_material2Price;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal percentageDiscountAmount = (decimal)_percentageDiscountAmount;
            decimal surchargePriceFixedAmount = (decimal)_surchargePriceFixedAmount;
            decimal surchargePricePerUnitAmount = (decimal)_surchargePricePerUnitAmount;
            decimal surchargeQuantity = (decimal)_surchargeQuantity;
            decimal quantity = (decimal)_quantity;
            decimal ovUnitPrice = (decimal)_ovUnitPrice;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1", glAccountID, glAccountID, 0.00m, labor1PricePerMinute, 0.00m, 0.00m);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, material1Price, 0.00m);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, material2Price, 0.00m);

            SurchargeDef surcharge = null;
            if (surchargePriceFixedAmount != 0m || surchargePricePerUnitAmount != 0m)
            {
                short taxCode = ctx.TaxabilityCodes.First(t => t.BID == BID).ID;
                surcharge = PricingTestHelper.InitSurcharge(ctx, BID, "Surcharge", glAccountID, taxCode, surchargePriceFixedAmount, surchargePricePerUnitAmount);
            }

            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true
                }

            };

            List<SurchargePriceRequest> surcharges = null;

            if (surcharge != null)
            {
                surcharges = new List<SurchargePriceRequest>
                {
                    new SurchargePriceRequest
                    {
                      SurchargeDefID = surcharge.ID,
                      Quantity = surchargeQuantity,
                      PriceFixedAmount = surchargePriceFixedAmount,
                      PricePerUnitAmount = surchargePricePerUnitAmount,
                    }
                };
            }

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = quantity,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
                Surcharges = surcharges,
                UnitPrice = ovUnitPrice,
                UnitPriceOV = true,
                ItemDiscountPercent = percentageDiscountAmount
            };

            decimal labor1CalcPrice = (labor1PricePerMinute * labor1Quantity);
            decimal material1CalcPrice = (material1Price * material1Quantity);
            decimal material2CalcPrice = (material2Price * material2Quantity);

            decimal totalComponentCalcPrice = labor1CalcPrice + material1CalcPrice + material2CalcPrice;
            decimal totalItemPrice = (ovUnitPrice * quantity);

            decimal surchargeTotal = surchargePriceFixedAmount + (surchargeQuantity * surchargePricePerUnitAmount);
            
            decimal labor1Percentage = totalComponentCalcPrice == 0m ? 0m : labor1CalcPrice / totalComponentCalcPrice;
            decimal material1Percentage = totalComponentCalcPrice == 0m ? 0m : material1CalcPrice / totalComponentCalcPrice;
            decimal material2Percentage = totalComponentCalcPrice == 0m ? 0m : material2CalcPrice / totalComponentCalcPrice;
            decimal totalComponentPrice = decimal.Round(totalItemPrice / (1 - percentageDiscountAmount), 2) - surchargeTotal;

            decimal labor1NewPrice = decimal.Round(totalComponentPrice * labor1Percentage, 2);
            decimal material1NewPrice = decimal.Round(totalComponentPrice * material1Percentage, 2);
            decimal material2NewPrice = decimal.Round(totalComponentPrice * material2Percentage, 2);

            decimal discountTotal = decimal.Round((totalComponentPrice + surchargeTotal) * percentageDiscountAmount, 2);

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(totalItemPrice, result.PricePreTax);
            Assert.AreEqual(ovUnitPrice, result.UnitPrice);
            Assert.IsTrue(result.UnitPriceOV);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1NewPrice, labor1Result.PricePreTax);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1NewPrice, material1Result.PricePreTax);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material2NewPrice, material2Result.PricePreTax);

            Assert.AreEqual(discountTotal, result.PriceDiscount);

            Assert.AreEqual(surchargeTotal, result.PriceSurcharge);

            if (surcharge != null)
            {
                SurchargePriceResult surchargeResult = result.Surcharges.FirstOrDefault(x => x.SurchargeDefID == surcharge.ID);
                Assert.IsNotNull(surchargeResult);
                Assert.AreEqual(surchargeTotal, surchargeResult.PricePreTax);
            }

            else
                Assert.IsTrue(result.Surcharges == null || result.Surcharges.Count == 0);
        }
    }
}