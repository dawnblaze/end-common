﻿using ATE.Models;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Pricing.Tax_Business_Logic.ATE.Helpers;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Endor.AzureStorage;
using Endor.DocumentStorage.Models;
using Endor.CBEL.Common;
using Endor.Units;
using Endor.Logging.Client;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class TaxEngineTests
    {
        public TestContext TestContext { get; set; }
        public ApiContext apiContext;
        //public static string ATEConnectionURL = "https://ateqa.corebridge.net";
        public static string ATEConnectionURL = "http://localhost:5800";
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        [TestMethod]
        public void TestCompressTaxAssessmentResults()
        {
            var assessments = GetTestTaxAssessmentResults();
            Assert.AreEqual(5, assessments.Count);

            decimal taxTotal = 0.0m;
            foreach (var assessment in assessments)
                foreach (var item in assessment.Items)
                    taxTotal += item.TaxAmount;

            assessments.Compress();
            Assert.AreEqual(3, assessments.Count);

            decimal taxTotalPostCompress = 0.0m;
            foreach (var assessment in assessments)
                foreach (var item in assessment.Items)
                    taxTotalPostCompress += item.TaxAmount;

            Assert.AreEqual(taxTotal, taxTotalPostCompress);
        }

        [TestMethod]
        public async Task TestTaxEngineInternalComputation()
        {
            short BID = 1;
            ApiContext context = PricingTestHelper.GetMockCtx(BID);
            apiContext = context;

            // setup TaxGroup
            TaxGroup taxGroup = await GetCompleteTaxGroup(BID, context);

            TaxAssessmentRequest request = new TaxAssessmentRequest()
            {
                Quantity = 3,
                Price = 2.0M,
                IsTaxExempt = false,
                NexusID = "Test",
            };
            TaxAssessmentNexus nexus = new TaxAssessmentNexus()
            {
                EngineType = TaxEngineType.Internal,
                InvoiceText = "Test",
                TaxGroupID = taxGroup.ID,
            };

            TaxAssessmentResult result = TaxEngine_Internal.Compute(BID, context, request, nexus);
            Assert.AreEqual(request.Quantity, result.Quantity);
            Assert.AreEqual(request.Price, result.Price);
            Assert.AreEqual(nexus.InvoiceText, result.InvoiceText);
            Assert.AreEqual(request.IsTaxExempt, result.IsTaxExempt);
            Assert.AreEqual(request.Price, result.PriceTaxable);
            Assert.AreEqual(request.PriceTaxable.HasValue, result.PriceTaxableOV);
            Assert.AreEqual(nexus.EngineType, result.TaxEngine);
            Assert.AreEqual(nexus.TaxGroupID, result.TaxGroupID);

            List<TaxAssessmentItemResult> resultItems = result.Items;


            foreach (TaxAssessmentItemResult item in resultItems)
            {
                TaxGroupItemLink link = taxGroup.TaxGroupItemLinks.Where(i => i.ItemID == item.TaxItemID).FirstOrDefault();
                Assert.IsNotNull(link);
                Assert.AreEqual(link.TaxItem.ID, item.TaxItemID);
                Assert.AreEqual(link.TaxItem.InvoiceText, item.InvoiceText);
                Assert.AreEqual(link.TaxItem.TaxRate, item.TaxRate);
                Assert.AreEqual(link.TaxItem.GLAccountID, item.GLAccountID);

                decimal expectedTaxAmount = Math.Round(((link.TaxItem.TaxRate/100) * (request.PriceTaxable ?? request.Price ?? 0m)), 2);
                Assert.AreEqual(expectedTaxAmount, item.TaxAmount);
            }
        }

        [TestMethod]
        async public Task TestComputeOrderWithTaxes()
        {
            short BID = 1;
            

            TaxGroup taxGroup = new TaxGroup()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "Baton Rouge",
                TaxRate = 11m,
            };

            TaxItem taxItem1 = new TaxItem()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "EBR",
                AgencyName = "EBR",
                InvoiceText = "EBR",
                GLAccount = new GLAccount()
                {
                    BID = 1,
                    ID = -99,
                    Name = "EBR Tax",
                    IsActive = true,
                    CanEdit = false,
                    GLAccountType = 23
                },
                TaxRate = 8m,
            };

            TaxItem taxItem2 = new TaxItem()
            {

                BID = 1,
                ID = -98,
                IsActive = true,
                Name = "BR City",
                AgencyName = "BR City",
                InvoiceText = "BR City",
                GLAccount = new GLAccount()
                {
                    BID = 1,
                    ID = -98,
                    Name = "BR City Tax",
                    IsActive = true,
                    CanEdit = false,
                    GLAccountType = 23
                },
                TaxRate = 3m,
            };

            TaxGroupItemLink taxGroupItemLink1 = new TaxGroupItemLink()
            {
                BID = 1,
                GroupID = taxGroup.ID,
                ItemID = taxItem1.ID,
            };

            TaxGroupItemLink taxGroupItemLink2 = new TaxGroupItemLink()
            {
                BID = 1,
                GroupID = taxGroup.ID,
                ItemID = taxItem2.ID,
            };

            MaterialData material = new MaterialData
            {
                BID = 1,
                ID = -99,
                EstimatingCost = 5,
                EstimatingPrice = 10,
                Name = "Material 1",
                InvoiceText = "Material 1",
                ExpenseAccountID = 6000,
                IncomeAccountID = 4000,
                InventoryAccountID = 1510,
            };


            OrderPriceResult result = null;

            try
            {
                using (var ctx = PricingTestHelper.GetMockCtx(BID))
                {
                    await TestCleanup(ctx, BID); 
                    await ctx.SaveChangesAsync();
                    await ctx.TaxGroup.AddAsync(taxGroup);
                    await ctx.TaxItem.AddRangeAsync(taxItem1, taxItem2);
                    await ctx.TaxGroupItemLink.AddRangeAsync(taxGroupItemLink1, taxGroupItemLink2);
                    await ctx.MaterialData.AddAsync(material);
                    Assert.IsTrue((await ctx.SaveChangesAsync()) > 0); //test insert
                    OrderPriceRequest req = new OrderPriceRequest()
                    {
                        DiscountList = new List<OrderDiscount>()
                        {
                            new OrderDiscount()
                            {
                                Name = "$100 off",
                                Discount = 100m,
                                IsPercentBased = false,
                            }
                        },
                        Items = new List<ItemPriceRequest>()
                        {
                            new ItemPriceRequest()
                            {
                                EngineType = PricingEngineType.SimplePart,
                                Quantity = 15,
                                Components = new List<ComponentPriceRequest>
                                {
                                    new ComponentPriceRequest
                                    {
                                        ComponentID = material.ID,
                                        ComponentType = OrderItemComponentType.Material,
                                        TotalQuantity = 15,
                                        PriceUnit = 5m,
                                    }
                                },
                                TaxInfoList = new List<TaxAssessmentRequest>()
                                {
                                    new TaxAssessmentRequest()
                                    {
                                        Quantity = 15,
                                        NexusID = "Baton Rouge",
                                        IsTaxExempt = false,
                                    }
                                }
                            },
                            new ItemPriceRequest()
                            {
                                EngineType = PricingEngineType.SimplePart,
                                Quantity = 6,
                                Components = new List<ComponentPriceRequest>
                                {
                                    new ComponentPriceRequest
                                    {
                                        ComponentID = material.ID,
                                        ComponentType = OrderItemComponentType.Material,
                                        TotalQuantity = 6,
                                        PriceUnit = 2.5m,
                                    }
                                },
                                TaxInfoList = new List<TaxAssessmentRequest>()
                                {
                                    new TaxAssessmentRequest()
                                    {
                                        Quantity = 6,
                                        NexusID = "Baton Rouge",
                                        IsTaxExempt = false,
                                    }
                                }
                            },
                            new ItemPriceRequest()
                            {
                                EngineType = PricingEngineType.SimplePart,
                                Quantity = 2,
                                Components = new List<ComponentPriceRequest>
                                {
                                    new ComponentPriceRequest
                                    {
                                        ComponentID = material.ID,
                                        ComponentType = OrderItemComponentType.Material,
                                        TotalQuantity = 2,
                                        PriceUnit = 3.5m,
                                    }
                                },
                                TaxInfoList = new List<TaxAssessmentRequest>()
                                {
                                    new TaxAssessmentRequest()
                                    {
                                        Quantity = 2,
                                        NexusID = "Baton Rouge",
                                        IsTaxExempt = false,
                                    }
                                }
                            }
                        },
                        Destinations = new List<DestinationPriceRequest>()
                        {
                            new DestinationPriceRequest()
                            {
                                EngineType = DestinationEngineType.Pickedup,
                                TaxInfo = new TaxAssessmentRequest()
                                {
                                    NexusID = "Baton Rouge",
                                    IsTaxExempt = false,
                                }
                            }
                        },
                        TaxNexusList = new Dictionary<string, TaxAssessmentNexus>()
                        {
                            {
                                "Baton Rouge",
                                new TaxAssessmentNexus()
                                {
                                    EngineType = TaxEngineType.Internal,
                                    TaxGroupID = -99,
                                }
                            }
                        }
                    };
                    cache = new TestHelper.MockTenantDataCache();
                    logger = new RemoteLogger(cache);
                    result = await (new PricingEngine(BID, ctx, cache, logger)).Compute(req, true);
                }
            }
            finally
            {
                using (var ctx = PricingTestHelper.GetMockCtx(BID))
                {
                    await TestCleanup(ctx, BID);
                }
            }

            Assert.IsNotNull(result);

            Assert.IsNotNull(result.Items);
            Assert.IsNotNull(result.Destinations);

            Assert.AreEqual(3, result.Items.Count);
            Assert.AreEqual(1, result.Destinations.Count);

            Assert.AreEqual(130.0m, result.ItemPreTaxTotal);
            Assert.AreEqual(0m, result.DestPreTaxTotal);
            Assert.AreEqual(100m, result.PriceOrderDiscountTotal);
            Assert.AreEqual(130.0m, result.PricePreTax);
            Assert.AreEqual(14.29m, result.TaxAmount);
            Assert.AreEqual(144.29m, result.PriceTotal);


            //Assert.AreEqual(1000m, result.Items[0].PriceNet);
            Assert.AreEqual(65.22m, result.Items[0].PriceDiscount);
            Assert.AreEqual(84.78m, result.Items[0].PricePreTax);
            Assert.AreEqual(9.32m, result.Items[0].TaxAmount);
            Assert.AreEqual(94.10m, result.Items[0].PriceTotal);

            //Assert.AreEqual(300m, result.Items[1].PriceNet);
            Assert.AreEqual(26.09m, result.Items[1].PriceDiscount);
            Assert.AreEqual(33.91m, result.Items[1].PricePreTax);
            Assert.AreEqual(3.73m, result.Items[1].TaxAmount);
            Assert.AreEqual(37.64m, result.Items[1].PriceTotal);

            //Assert.AreEqual(500m, result.Items[2].PriceNet);
            Assert.AreEqual(8.69m, result.Items[2].PriceDiscount);
            Assert.AreEqual(11.31m, result.Items[2].PricePreTax);
            Assert.AreEqual(1.24m, result.Items[2].TaxAmount);
            Assert.AreEqual(12.55m, result.Items[2].PriceTotal);

            Assert.AreEqual(0m, result.Destinations[0].PriceNet);
            Assert.AreEqual(0m, result.Destinations[0].PricePreTax);
            Assert.AreEqual(0m, result.Destinations[0].TaxAmount);
            Assert.AreEqual(0m, result.Destinations[0].PriceTotal);
        }

        [TestMethod]
        async public Task TestComputeItemWithTaxes()
        {
            short BID = 1;


            TaxGroup taxGroup = new TaxGroup()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "Baton Rouge",
                TaxRate = 11m,
            };

            TaxItem taxItem1 = new TaxItem()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "EBR",
                AgencyName = "EBR",
                InvoiceText = "EBR",
                GLAccount = new GLAccount()
                {
                    BID = 1,
                    ID = -99,
                    Name = "EBR Tax",
                    IsActive = true,
                    CanEdit = false,
                    GLAccountType = 23
                },
                TaxRate = 8m,
            };

            TaxItem taxItem2 = new TaxItem()
            {

                BID = 1,
                ID = -98,
                IsActive = true,
                Name = "BR City",
                AgencyName = "BR City",
                InvoiceText = "BR City",
                GLAccount = new GLAccount()
                {
                    BID = 1,
                    ID = -98,
                    Name = "BR City Tax",
                    IsActive = true,
                    CanEdit = false,
                    GLAccountType = 23
                },
                TaxRate = 3m,
            };

            TaxGroupItemLink taxGroupItemLink1 = new TaxGroupItemLink()
            {
                BID = 1,
                GroupID = taxGroup.ID,
                ItemID = taxItem1.ID,
            };

            TaxGroupItemLink taxGroupItemLink2 = new TaxGroupItemLink()
            {
                BID = 1,
                GroupID = taxGroup.ID,
                ItemID = taxItem2.ID,
            };

            MaterialData material = new MaterialData
            {
                BID = 1,
                ID = -99,
                EstimatingCost = 5,
                EstimatingPrice = 10,
                Name = "Material 1",
                InvoiceText = "Material 1",
                ExpenseAccountID = 6000,
                IncomeAccountID = 4000,
                InventoryAccountID = 1510,
            };

            SurchargeDef surchargeDef = new SurchargeDef
            {
                BID = 1,
                ID = -99,
                TaxCodeID = 0,
                IncomeAccountID = 4000,
                Name = "Test surcharge def"
            };


            ItemPriceResult result = null;

            try
            {
                using (var ctx = PricingTestHelper.GetMockCtx(BID))
                {
                    await ctx.TaxGroup.AddAsync(taxGroup);
                    await ctx.TaxItem.AddRangeAsync(taxItem1, taxItem2);
                    await ctx.TaxGroupItemLink.AddRangeAsync(taxGroupItemLink1, taxGroupItemLink2);
                    await ctx.MaterialData.AddAsync(material);
                    await ctx.SurchargeDef.AddAsync(surchargeDef);
                    Assert.IsTrue((await ctx.SaveChangesAsync()) > 0); //test insert
                    ItemPriceRequest req = new ItemPriceRequest()
                    {
                        EngineType = PricingEngineType.SimplePart,
                        Quantity = 15,
                        Components = new List<ComponentPriceRequest>
                        {
                            new ComponentPriceRequest
                            {
                                ComponentID = material.ID,
                                ComponentType = OrderItemComponentType.Material,
                                TotalQuantity = 15,
                                PriceUnit = 5m,
                            }
                        },
                        Surcharges = new List<SurchargePriceRequest>
                        {
                            new SurchargePriceRequest
                            {
                                SurchargeDefID = ctx.SurchargeDef.Where(x => x.ID != 1).First().ID,
                                Quantity = 1,
                                PricePerUnitAmount = 1
                            }
                        },
                        TaxInfoList = new List<TaxAssessmentRequest>()
                        {
                            new TaxAssessmentRequest()
                            {
                                Quantity = 15,
                                NexusID = "Baton Rouge",
                                IsTaxExempt = false,
                            }
                        },
                        TaxNexusList = new Dictionary<string, TaxAssessmentNexus>()
                        {
                            {
                                "Baton Rouge",
                                new TaxAssessmentNexus()
                                {
                                    EngineType = TaxEngineType.Internal,
                                    TaxGroupID = -99,
                                }
                            }
                        }
                    };
                    cache = new TestHelper.MockTenantDataCache();
                    logger = new RemoteLogger(cache);
                    result = await (new PricingEngine(BID, ctx, cache, logger)).Compute(req, true);
                }
            }
            finally
            {
                using (var ctx = PricingTestHelper.GetMockCtx(BID))
                {
                    await TestCleanup(ctx, BID);
                }
            }

            Assert.IsNotNull(result);

            Assert.AreEqual(151.0m, result.PricePreTax);
            Assert.AreEqual(16.61m, result.TaxAmount);
            Assert.AreEqual(167.61m, result.PriceTotal);

            Assert.IsTrue(result.Components.First().TaxInfoList.Count > 0);
            Assert.IsTrue(result.Surcharges.First().TaxInfoList.Count > 0);

        }

        [TestMethod]
        async public Task TestComputeLinkedAssemblyItemWithTaxes()
        {
            short BID = 1;


            TaxGroup taxGroup = new TaxGroup()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "Baton Rouge",
                TaxRate = 11m,
            };

            TaxItem taxItem1 = new TaxItem()
            {
                BID = 1,
                ID = -99,
                IsActive = true,
                Name = "EBR",
                AgencyName = "EBR",
                InvoiceText = "EBR",
                GLAccount = new GLAccount()
                {
                    BID = 1,
                    ID = -99,
                    Name = "EBR Tax",
                    IsActive = true,
                    CanEdit = false,
                    GLAccountType = 23
                },
                TaxRate = 8m,
            };

            TaxItem taxItem2 = new TaxItem()
            {

                BID = 1,
                ID = -98,
                IsActive = true,
                Name = "BR City",
                AgencyName = "BR City",
                InvoiceText = "BR City",
                GLAccount = new GLAccount()
                {
                    BID = 1,
                    ID = -98,
                    Name = "BR City Tax",
                    IsActive = true,
                    CanEdit = false,
                    GLAccountType = 23
                },
                TaxRate = 3m,
            };

            TaxGroupItemLink taxGroupItemLink1 = new TaxGroupItemLink()
            {
                BID = 1,
                GroupID = taxGroup.ID,
                ItemID = taxItem1.ID,
            };

            TaxGroupItemLink taxGroupItemLink2 = new TaxGroupItemLink()
            {
                BID = 1,
                GroupID = taxGroup.ID,
                ItemID = taxItem2.ID,
            };

            short bid = 1;
            int currentVariableID = -99;
            int currentFormulaID = -99;


            AssemblyVariable childAssemblyHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "12",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyWidth = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "24",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable childAssemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=FixedPrice",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable childAssemblyParentQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "ChildParentQuantityTest",
                Label = "Parent Quantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = false,
            };
            string parentName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");
            int childID = -98;
            string childName = "ChildAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            AssemblyData childAssembly = new AssemblyData()
            {
                BID = bid,
                ID = childID,
                Name = childName,
                FixedPrice = 5m,
                Variables = new List<AssemblyVariable>()
                {
                    childAssemblyHeight,
                    childAssemblyWidth,
                    childAssemblyPrice,
                    childAssemblyParentQuantity
                }
            };

            AssemblyVariable parentAssemblyHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyWidth = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable parentAssemblyChildAssembly = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = childName,
                Label = "Child Assembly",
                DataType = DataType.Assembly,
                DefaultValue = childName,
                ElementType = AssemblyElementType.LinkedAssembly,
                LinkedAssemblyID = childID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
                RollupLinkedPriceAndCost = true
            };

            AssemblyVariableFormula parentAssemblyQuantityFormula = new AssemblyVariableFormula()
            {
                ID = currentFormulaID--,
                BID = bid,
                ChildVariableName = "TotalQuantity",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.MappedVariable,
                FormulaText = "TotalQuantity",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyHeightFormula = new AssemblyVariableFormula()
            {
                ID = currentFormulaID--,
                BID = bid,
                ChildVariableName = "Height",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "1",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            AssemblyVariableFormula parentAssemblyWidthFormula = new AssemblyVariableFormula()
            {
                ID = currentFormulaID--,
                BID = bid,
                ChildVariableName = "Width",
                DataType = DataType.Number,
                FormulaEvalType = AssemblyFormulaEvalType.CBEL,
                FormulaText = "1",
                FormulaUseType = AssemblyFormulaUseType.LinkedVariableOV,
                IsFormula = false,
                VariableID = parentAssemblyChildAssembly.ID
            };

            parentAssemblyChildAssembly.Formulas = new List<AssemblyVariableFormula>()
            {
                parentAssemblyQuantityFormula,
                parentAssemblyHeightFormula,
                parentAssemblyWidthFormula
            };

            AssemblyVariable parentAssemblyPrice = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=FixedPrice",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable parentAssemblyParentQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = bid,
                Name = "ParentQuantityTest",
                Label = "Parent Quantity Test",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = false,
            };
            int parentID = -99;

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = 1,
                ID = parentID,
                Name = parentName,
                FixedPrice = 10m,
                Variables = new List<AssemblyVariable>()
                {
                    parentAssemblyHeight,
                    parentAssemblyWidth,
                    parentAssemblyChildAssembly,
                    parentAssemblyPrice,
                    parentAssemblyParentQuantity
                }
            };


            ItemPriceResult result = null;

            try
            {
                using (var ctx = PricingTestHelper.GetMockCtx(BID))
                {
                    await TestCleanup(ctx, BID);
                    await ctx.TaxGroup.AddAsync(taxGroup);
                    await ctx.TaxItem.AddRangeAsync(taxItem1, taxItem2);
                    await ctx.TaxGroupItemLink.AddRangeAsync(taxGroupItemLink1, taxGroupItemLink2);
                    await ctx.AssemblyData.AddAsync(childAssembly);
                    await ctx.AssemblyData.AddAsync(parentAssembly);
                    Assert.IsTrue((await ctx.SaveChangesAsync()) > 0); //test insert
                    ItemPriceRequest req = new ItemPriceRequest()
                    {
                        EngineType = PricingEngineType.SimplePart,
                        Quantity = 1,
                        Components = new List<ComponentPriceRequest>
                        {
                            new ComponentPriceRequest
                            {
                                ComponentID = parentAssembly.ID,
                                ComponentType = OrderItemComponentType.Assembly,
                                TotalQuantity = 1,
                                ChildComponents = new List<ComponentPriceRequest>
                                {
                                    new ComponentPriceRequest
                                    {
                                        ComponentID = childAssembly.ID,
                                        ComponentType = OrderItemComponentType.Assembly,
                                        TotalQuantity = 1,
                                    }
                                },
                            }
                        },
                        Surcharges = new List<SurchargePriceRequest>
                        {
                        },
                        TaxInfoList = new List<TaxAssessmentRequest>()
                        {
                            new TaxAssessmentRequest()
                            {
                                Quantity = 1,
                                NexusID = "Baton Rouge",
                                IsTaxExempt = false,
                            }
                        },
                        TaxNexusList = new Dictionary<string, TaxAssessmentNexus>()
                        {
                            {
                                "Baton Rouge",
                                new TaxAssessmentNexus()
                                {
                                    EngineType = TaxEngineType.Internal,
                                    TaxGroupID = -99,
                                }
                            }
                        }
                    };
                    cache = new TestHelper.MockTenantDataCache();
                    logger = new RemoteLogger(cache);
                    result = await (new PricingEngine(BID, ctx, cache, logger)).Compute(req, true);
                }
            }
            finally
            {
                using (var ctx = PricingTestHelper.GetMockCtx(BID))
                {
                    await TestCleanup(ctx, BID);
                }
            }

            Assert.IsNotNull(result);

            Assert.AreEqual(10.0m, result.PricePreTax);
            Assert.AreEqual(1.10m, result.TaxAmount);
            Assert.AreEqual(11.1m, result.PriceTotal);

            Assert.IsTrue(result.Components.First().TaxInfoList.Count > 0);

        }

        
        public static async Task TestCleanup(ApiContext ctx, short bid)
        {
            ctx.OrderItemComponent.RemoveRange(ctx.OrderItemComponent.Where(x => x.OrderItemID < 0));
            await ctx.SaveChangesAsync();
            ctx.OrderItemData.RemoveRange(ctx.OrderItemData.Where(x => x.TaxGroupID == -99));
            await ctx.SaveChangesAsync();

            ctx.OrderData.RemoveRange(ctx.OrderData.Where(x => x.TaxGroupID == -99));
            await ctx.SaveChangesAsync();

            ctx.TaxGroup.RemoveRange(ctx.TaxGroup.Where(x => x.ID == -99));
            await ctx.SaveChangesAsync();
            ctx.TaxItem.RemoveRange(ctx.TaxItem.Where(x => x.ID < 0));
            await ctx.SaveChangesAsync();
            ctx.LaborData.RemoveRange(ctx.LaborData.Where(x => x.ExpenseAccountID < 0 || x.ID < 0));
            ctx.MaterialData.RemoveRange(ctx.MaterialData.Where(x => x.ExpenseAccountID < 0 || x.ID < 0));
            ctx.AssemblyVariableFormula.RemoveRange(ctx.AssemblyVariableFormula.Where(x => x.ID < 0));
            ctx.AssemblyVariable.RemoveRange(ctx.AssemblyVariable.Where(x => x.ID < 0));
            await ctx.SaveChangesAsync();

            IEnumerable<AssemblyData> _assemblies = ctx.AssemblyData.Where(t => t.BID == bid && t.ID < 0);
            foreach (AssemblyData _assembly in _assemblies)
            {
                ITenantDataCache cache = new TestHelper.MockTenantDataCache();
                EntityStorageClient client = new EntityStorageClient((await cache.Get(bid)).StorageConnectionString, bid);
                var assemblyDoc = new DMID() { id = _assembly.ID, ctid = ClassType.Assembly.ID() };
                string assemblyName = CBELAssemblyHelper.AssemblyName(bid, _assembly.ID, _assembly.ClassTypeID, 1);

                await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
            }
            ctx.AssemblyData.RemoveRange(_assemblies);
            await ctx.SaveChangesAsync();

            var machinesToDelete = ctx.MachineData.Where(x => x.ExpenseAccountID < 0 || x.ID < 0);
            ctx.MachineData.RemoveRange(machinesToDelete);
            short[] machineIDsToDelete = machinesToDelete.Select(x => x.ID).ToArray();
            ctx.MachineCategoryLink.RemoveRange(ctx.MachineCategoryLink.Where(x => x.PartID < 0 || machineIDsToDelete.Contains(x.PartID)));
            ctx.MachineInstance.RemoveRange(ctx.MachineInstance.Where(x => x.MachineID < 0 || x.ID < 0 || machineIDsToDelete.Contains(x.MachineID)));
            var machineProfilesToDelete = ctx.MachineProfile.Where(x => x.MachineID < 0 || x.ID < 0 || machineIDsToDelete.Contains(x.MachineID));
            ctx.MachineProfile.RemoveRange(machineProfilesToDelete);
            int[] machineProfileIDsToDelete = machineProfilesToDelete.Select(x => x.ID).ToArray();
            ctx.MachineProfileTable.RemoveRange(ctx.MachineProfileTable.Where(x => x.ProfileID < 0 || x.ID < 0 || machineProfileIDsToDelete.Contains(x.ProfileID)));
            ctx.GLAccount.RemoveRange(ctx.GLAccount.Where(x => x.ID < 0));
            await ctx.SaveChangesAsync();
            ctx.TaxGroupItemLink.RemoveRange(ctx.TaxGroupItemLink.Where(x => x.GroupID < 0));
            await ctx.SaveChangesAsync();
            ctx.SurchargeDef.RemoveRange(ctx.SurchargeDef.Where(x => x.ID < 0));
            await ctx.SaveChangesAsync();
        }

        private async Task<TaxGroup> GetCompleteTaxGroup(short BID, ApiContext context)
        {
            TaxGroup taxGroup = new TaxGroup()
            {
                ID = -999,
                BID = BID,
                Name = "Test",
                TaxRate = 3.0M,
            };

            taxGroup = await this.InsertOrUpdate(BID, context, taxGroup);
            taxGroup = await this.LinkTaxItem(context, taxGroup, -999);
            taxGroup = await this.LinkTaxItem(context, taxGroup, -998);

            return taxGroup;
        }

        private async Task<TaxGroup> LinkTaxItem(ApiContext context, TaxGroup taxGroup, short taxItemId)
        {
            TaxItem taxItem = new TaxItem()
            {
                ID = taxItemId,
                BID = taxGroup.BID,
                Name = "Test",
                InvoiceText = "Testing",
                TaxRate = 2.0M,
                GLAccountID = 1000, // Assets
            };
            await this.InsertOrUpdate(taxGroup.BID, context, taxItem);

            TaxGroupItemLink link = new TaxGroupItemLink()
            {
                BID = taxGroup.BID,
                GroupID = taxGroup.ID,
                ItemID = taxItem.ID,
            };

            bool p(TaxGroupItemLink item) => item.BID == link.BID && item.GroupID == link.GroupID && item.ItemID == link.ItemID;
            bool linkExist = context.Set<TaxGroupItemLink>().Any(p);
            if (linkExist)
            {
                context.Entry(link).CurrentValues.SetValues(link);
            }
            else
            {
                context.Add(link);
            }

            await context.SaveChangesAsync();

            link.TaxItem = taxItem;
            taxGroup.TaxGroupItemLinks.Add(link);

            return taxGroup;
        }

        private async Task<T> InsertOrUpdate<T>(short BID, ApiContext context, T entity) where T : class, IAtom<short>, IAtom
        {
            bool itemExist = context.Set<T>().Any((T item) => item.BID == BID && item.ID == entity.ID);
            if (itemExist)
            {
                context.Entry(entity).CurrentValues.SetValues(entity);
            }
            else
            {
                context.Add(entity);
            }

            await context.SaveChangesAsync();

            return entity;
        }

        /// <summary>
        /// Gets a List of TaxAssessmentResults test data
        /// </summary>
        /// <returns></returns>
        private List<TaxAssessmentResult> GetTestTaxAssessmentResults()
        {
            List<TaxAssessmentItemResult> items1 = new List<TaxAssessmentItemResult>
            {
                new TaxAssessmentItemResult()
                {
                    InvoiceText = "EBR County",
                    TaxRate = 4.0m,
                    TaxAmount = 3.75m
                },
                new TaxAssessmentItemResult()
                {
                    InvoiceText = "EBR City",
                    TaxRate = 2.0m,
                    TaxAmount = 1.94m
                }
            };

            List<TaxAssessmentItemResult> items2 = new List<TaxAssessmentItemResult>
            {
                new TaxAssessmentItemResult()
                {
                    InvoiceText = "Chappaqa",
                    TaxRate = 5.0m,
                    TaxAmount = 4.75m
                }
            };

            List<TaxAssessmentItemResult> items3 = new List<TaxAssessmentItemResult>
            {
                new TaxAssessmentItemResult()
                {
                    InvoiceText = "Chappaqa",
                    TaxRate = 5.0m,
                    TaxAmount = 4.75m
                },
                new TaxAssessmentItemResult()
                {
                    InvoiceText = "Chappaqa",
                    TaxRate = 5.0m,
                    TaxAmount = 4.75m
                }
            };

            List<TaxAssessmentResult> assessments = new List<TaxAssessmentResult>
            {
                new TaxAssessmentResult()
                {
                    NexusID = "3",
                    Items = items1
                },
                new TaxAssessmentResult()
                {
                    NexusID = "1",
                    Items = items2
                },
                new TaxAssessmentResult()
                {
                    NexusID = "3",
                    Items = items1
                },
                new TaxAssessmentResult()
                {
                    NexusID = "99",
                    Items = items3
                },
                new TaxAssessmentResult()
                {
                    NexusID = "99",
                    Items = items3
                }
            };

            return assessments;
        }


        [TestCleanup]
        public void Teardown()
        {
            // filter Test methods to cleanup
            switch (this.TestContext.TestName)
            {
                case "TestTaxEngineInternalComputation":
                    // unlink TaxGroups and TaxItems
                    TaxGroupItemLink testTaxGroupItemLink = this.apiContext.TaxGroupItemLink.Local.Where(i => i.BID == 1 && i.GroupID == -999).FirstOrDefault();
                    if (testTaxGroupItemLink != null)
                    {
                        this.apiContext.TaxGroupItemLink.Remove(testTaxGroupItemLink);
                    }
                    this.apiContext.SaveChanges();

                    TaxItem testTaxItem1 = this.apiContext.TaxItem.Local.Where(i => i.BID == 1 && i.ID == -999).FirstOrDefault();
                    if (testTaxItem1 != null)
                    {
                        this.apiContext.TaxItem.Remove(testTaxItem1);
                    }
                    this.apiContext.SaveChanges();

                    TaxItem testTaxItem2 = this.apiContext.TaxItem.Local.Where(i => i.BID == 1 && i.ID == -998).FirstOrDefault();
                    if (testTaxItem2 != null)
                    {
                        this.apiContext.TaxItem.Remove(testTaxItem2);
                    }
                    this.apiContext.SaveChanges();

                    TaxGroup testTaxGroup = this.apiContext.TaxGroup.Local.Where(i => i.BID == 1 && i.ID == -999).FirstOrDefault();
                    if (testTaxGroup != null)
                    {
                        this.apiContext.TaxGroup.Remove(testTaxGroup);
                    }
                    this.apiContext.SaveChanges();

                    break;
                default:
                    break;
            }
        }

        [TestMethod]
        public async Task TestTaxExempt()
        {
            short BID = 1;
            ApiContext context = PricingTestHelper.GetMockCtx(BID);
            TaxGroup taxGroup = await GetCompleteTaxGroup(BID, context);

            var request = new TaxAssessmentRequest
            {
                Quantity = 3,
                Price = 2.0M,
                IsTaxExempt = true,
                NexusID = "Tax Exempt",
                PriceTaxable = 0
            };

            var nexus = new TaxAssessmentNexus()
            {
                EngineType = TaxEngineType.Exempt,
                InvoiceText = "Tax Exempt",
                TaxGroupID = taxGroup.ID,
                CompanyID = context.CompanyData.First().ID,
                CompanyName = context.CompanyData.First().Name,
                ToAddress = new Address()
                {
                    Country = "US",
                    State = "LA",
                    PostalCode = "70809"
                },
                ATEApiUrl = "http://localhost:5800",
                ATERegistrationID = new Guid("93C2BCDF-868E-4A87-B810-0182509DE5E4")
            };

            var taxEngine = new TaxEngine(BID, context);
            var result = taxEngine.Compute((byte)OrderTransactionType.Order, request, nexus);

            Assert.IsTrue(result.PriceTaxable == 0);
            Assert.IsTrue(result.TaxAmount == 0);
            Assert.IsTrue(result.TaxEngine == TaxEngineType.Exempt);
            Assert.IsTrue(result.IsTaxExempt);
        }

        [Ignore]
        [TestMethod]
        public async Task TestTaxEngineTaxJarComputation()
        {
            short BID = 1;
            ApiContext context = PricingTestHelper.GetMockCtx(BID);
            TaxGroup taxGroup = await GetCompleteTaxGroup(BID, context);
            TaxAssessmentRequest request = new TaxAssessmentRequest()
            {
                Quantity = 3,
                Price = 2.0M,
                IsTaxExempt = false,
                NexusID = "Test",
            };
            TaxAssessmentNexus nexus = new TaxAssessmentNexus()
            {
                EngineType = TaxEngineType.TaxJar,
                InvoiceText = "Test",
                TaxGroupID = taxGroup.ID,
                CompanyID = context.CompanyData.First().ID,
                CompanyName = context.CompanyData.First().Name,
                ToAddress = new Address()
                {
                    Country = "US",
                    State = "LA",
                    PostalCode = "70809"
                },
                ATEApiUrl = "http://localhost:5800",
                ATERegistrationID = new Guid("93C2BCDF-868E-4A87-B810-0182509DE5E4")
            };
            var taxEngine = new TaxEngine(BID, context);
            var result = taxEngine.Compute((byte)OrderTransactionType.Order, request, nexus);
            Assert.AreEqual(result.Items.Count(), 5);
        }

        [Ignore]
        [TestMethod]
        public async Task TestTaxEngineTaxJarComputationCreditMemo()
        {
            short BID = 1;
            ApiContext context = PricingTestHelper.GetMockCtx(BID);
            TaxGroup taxGroup = await GetCompleteTaxGroup(BID, context);
            TaxAssessmentRequest request = new TaxAssessmentRequest()
            {
                Quantity = 3,
                Price = 2.0M,
                IsTaxExempt = false,
                NexusID = "Test",
            };
            TaxAssessmentNexus nexus = new TaxAssessmentNexus()
            {
                EngineType = TaxEngineType.TaxJar,
                InvoiceText = "Test",
                TaxGroupID = taxGroup.ID,
                CompanyID = context.CompanyData.First().ID,
                CompanyName = context.CompanyData.First().Name,
                ToAddress = new Address()
                {
                    Country = "US",
                    State = "LA",
                    PostalCode = "70809"
                },
                ATEApiUrl = "http://localhost:5800",
                ATERegistrationID = new Guid("93C2BCDF-868E-4A87-B810-0182509DE5E4")
            };
            var taxEngine = new TaxEngine(BID, context);
            var result = taxEngine.Compute((byte)OrderTransactionType.Memo, request, nexus);
            Assert.AreEqual(result.Items.Count(), 5);
            Assert.AreEqual(0,result.Items.Where(x => x.TaxAmount < 0).Count());
        }
    }
}
