﻿using Endor.CBEL.Elements;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class TierDataTableTests
    {

        [TestMethod]
        public void TestTierNameExists()
        {
            short BID = 1;
            var ctx = PricingTestHelper.GetMockCtx(BID);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx parent = new Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx(ctx, BID, cache, logger);
            var pricingTier = ctx.FlatListItem.Where(c => c.BID == BID && c.FlatListType == FlatListType.PricingTiers).OrderByDescending(x => x.ID).First();
            var dbCompany = ctx.CompanyData.Include(c => c.PricingTier).Where(c => c.BID == BID).First();
            var originalPricingTier = dbCompany.PricingTierID;
            dbCompany.PricingTierID = pricingTier.ID;
            ctx.SaveChanges();
            parent.CompanyID = dbCompany.ID;
            TierDataTable<string> table = new TierDataTable<string>(parent);
            table.RowHeadings.AddRange(new string[] { "A", "B", pricingTier.Name, "D", "E" });
            table.ColumnHeadings.AddRange(new string[] { "Amber", "Bill", "Charlie", "Derek", "Greg", "Mike", "Zebin" });
            decimal?[,] cellarray = {
                {1.75m, 5   ,12  ,18  ,24  ,36  ,null},
                {3   ,15  ,36  ,54  ,72  ,108 ,null},
                {10  ,50  ,120 ,180 ,240 ,360 ,420 },
                {null,90  ,216 ,null,432 ,648 ,null},
                {36  ,180 ,432 ,648 ,864 ,null,null} };
            table.AddCells(cellarray);
            var result = table.TableLookup("Charlie");
            Assert.AreEqual(120, result.Value);
            dbCompany.PricingTierID = originalPricingTier;
            ctx.SaveChanges();

        }

        [TestMethod]
        public void TestTierNameDoesntExist()
        {
            short BID = 1;
            var ctx = PricingTestHelper.GetMockCtx(BID);
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx parent = new Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx(ctx, BID, cache, logger);
            var pricingTier = ctx.FlatListItem.Where(c => c.BID == BID && c.FlatListType == FlatListType.PricingTiers).OrderByDescending(c => c.ID).First();
            var dbCompany = ctx.CompanyData.Include(c => c.PricingTier).Where(c => c.BID == BID).First();
            var originalPricingTier = dbCompany.PricingTierID;
            dbCompany.PricingTierID = pricingTier.ID;
            ctx.SaveChanges();
            parent.CompanyID = dbCompany.ID;
            TierDataTable<string> table = new TierDataTable<string>(parent);
            table.RowHeadings.AddRange(new string[] { "A", "B", "C", "D", "E" });
            table.ColumnHeadings.AddRange(new string[] { "Amber", "Bill", "Charlie", "Derek", "Greg", "Mike", "Zebin" });
            decimal?[,] cellarray = {
                {1.75m, 5   ,12  ,18  ,24  ,36  ,null},
                {3   ,15  ,36  ,54  ,72  ,108 ,null},
                {10  ,50  ,120 ,180 ,240 ,360 ,420 },
                {null,90  ,216 ,null,432 ,648 ,null},
                {36  ,180 ,432 ,648 ,864 ,null,null} };
            table.AddCells(cellarray);
            var result = table.TableLookup("Charlie");
            Assert.AreEqual(12, result.Value);
            dbCompany.PricingTierID = originalPricingTier;
            ctx.SaveChanges();

        }

        public class Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx : CBELAssemblyBase
        {
            public Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx(ApiContext ctx, short bid, ITenantDataCache cache, RemoteLogger logger) : base(ctx, bid, cache, logger)
            {
            }

            public override NumberEditElement Price => throw new NotImplementedException();
            public override NumberEditElement TotalQuantity => throw new NotImplementedException();
            public override void InitializeVariables()
            {

            }

            public override void InitializeData()
            {

            }
        }
        
    }

}
