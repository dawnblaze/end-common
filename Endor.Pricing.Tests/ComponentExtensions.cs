﻿using Endor.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Pricing.Tests
{
    public static class ComponentExtensions
    {

        public static ComponentPriceRequest ToPriceRequest(this OrderItemComponent self, int CompanyID)
        {
            var request = new ComponentPriceRequest()
            {
                ID = self.ID,
                TempID = self.TempID,
                CompanyID = CompanyID,
                TotalQuantity = self.TotalQuantity.Value,
                TotalQuantityOV = self.TotalQuantityOV,
                AssemblyQuantity = self.AssemblyQuantity.Value,
                AssemblyQuantityOV = self.AssemblyQuantityOV,
                QuantityUnit = self.QuantityUnit,
                PriceUnit = self.PriceUnit,
                PriceUnitOV = self.PriceUnitOV,
                CostUnit = self.CostUnit,
                CostUnitOV = self.CostOV,
                IncomeAccountID = self.IncomeAccountID,
                ExpenseAccountID = self.ExpenseAccountID,
                IncomeAllocationType = self.IncomeAllocationType
            };

            request.ComponentType = self.ComponentType;
            request.ComponentID = self.ComponentID;

            return request;
        }
    }
}
