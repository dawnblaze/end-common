﻿using Endor.CBEL.Compute;
using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Endor.Logging.Client;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class MachineTimeTests
    {
        private ApiContext ctx = null;
        private PricingEngine pricingEngine = null;
        private RemoteLogger logger = null;
        private const short BID = 1;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            pricingEngine = new PricingEngine(BID, ctx, new TestHelper.MockTenantDataCache(), logger);
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestMethod]
        public async Task LinkedMachineCompileAndComputeTest()
        {
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int machineTemplateID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string machineTemplateName = "MachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            short machineID = PricingTestHelper.NextMachineID(ctx, BID);
            string machineName = "Machine" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable machineTemplateQuantity = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "AssemblyQuantity",
                Label = "AssemblyQuantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
            };

            AssemblyData machineTemplate = new AssemblyData()
            {
                BID = BID,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    machineTemplateQuantity,
                }
            };

            MachineData machine = new MachineData()
            {
                BID = BID,
                ID = machineID,
                Name = machineName,
                EstimatingCostPerHourFormula = "2",
                MachineTemplateID = machineTemplateID,
            };

            AssemblyVariable assemblyMachine = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = machineName,
                Label = "Machine Link",
                DataType = DataType.MachinePart,
                DefaultValue = machineName,
                ElementType = AssemblyElementType.LinkedMachine,
                LinkedMachineID = machineID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyMachine,
                    assemblyPrice
                }
            };

            ctx.AssemblyData.Add(machineTemplate);
            ctx.MachineData.Add(machine);
            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = new List<ComponentPriceRequest>()
                {
                    new ComponentPriceRequest()
                    {
                        ComponentType = OrderItemComponentType.Assembly,
                        ComponentID = assemblyID,
                        TotalQuantity = 2,
                        TotalQuantityOV = true,

                        ChildComponents = new List<ComponentPriceRequest>()
                        {
                            new ComponentPriceRequest()
                            {
                                ComponentType = OrderItemComponentType.Machine,
                                ComponentID = machineID,
                                VariableName = machineName,
                                Variables = new Dictionary<string, VariableValue>()
                                {
                                    { "Profile", new VariableValue() {Value = "Default", ValueOV = true} },
                                    { "LayoutType", new VariableValue() {Value = "NoLayoutManager", ValueOV = true} },
                                }
                            }
                        }
                    }
                }
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(4m, result.CostMachine);
            Assert.AreEqual(4m, result.CostTotal);

            Assert.AreEqual(1, result.Components.Count);
            ComponentPriceResult assemblyResult = result.Components[0];

            Assert.AreEqual(4m, assemblyResult.CostMachine);
            Assert.AreEqual(4m, assemblyResult.CostNet);

            Assert.AreEqual(1, assemblyResult.ChildComponents.Count);
            ComponentPriceResult machineResult = (assemblyResult.ChildComponents as List<ComponentPriceResult>)[0];

            Assert.AreEqual(4m, machineResult.CostMachine);
            Assert.AreEqual(4m, machineResult.CostNet);
        }


        [TestMethod]
        public async Task LinkedMachineCostOVTest()
        {
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            int machineTemplateID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string machineTemplateName = "MachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            short machineID = PricingTestHelper.NextMachineID(ctx, BID);
            string machineName = "Machine" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable machineTemplateQuantity = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "AssemblyQuantity",
                Label = "AssemblyQuantity",
                DataType = DataType.Number,
                DefaultValue = "1",
                ElementType = AssemblyElementType.Number,
            };

            AssemblyData machineTemplate = new AssemblyData()
            {
                BID = BID,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    machineTemplateQuantity,
                }
            };

            MachineData machine = new MachineData()
            {
                BID = BID,
                ID = machineID,
                Name = machineName,
                EstimatingCostPerHourFormula = "2",
                MachineTemplateID = machineTemplateID,
            };

            AssemblyVariable assemblyMachine = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = machineName,
                Label = "Machine Link",
                DataType = DataType.MachinePart,
                DefaultValue = machineName,
                ElementType = AssemblyElementType.LinkedMachine,
                LinkedMachineID = machineID,
                ConsumptionDefaultValue = "=TotalQuantity.Value",
                IsConsumptionFormula = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyMachine,
                    assemblyPrice
                }
            };

            ctx.AssemblyData.Add(machineTemplate);
            ctx.MachineData.Add(machine);
            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = new List<ComponentPriceRequest>()
                {
                    new ComponentPriceRequest()
                    {
                        ComponentType = OrderItemComponentType.Assembly,
                        ComponentID = assemblyID,
                        TotalQuantity = 2,
                        TotalQuantityOV = true,

                        ChildComponents = new List<ComponentPriceRequest>()
                        {
                            new ComponentPriceRequest()
                            {
                                ComponentType = OrderItemComponentType.Machine,
                                ComponentID = machineID,
                                VariableName = machineName,
                                Variables = new Dictionary<string, VariableValue>()
                                {
                                    { "Profile", new VariableValue() {Value = "Default", ValueOV = true} },
                                    { "LayoutType", new VariableValue() {Value = "NoLayoutManager", ValueOV = true} },
                                },
                                ChildComponents = new List<ComponentPriceRequest>()
                                {
                                    new ComponentPriceRequest()
                                    {
                                        ComponentType = OrderItemComponentType.MachineTime,
                                        CostUnit = 5,
                                        CostUnitOV = true,
                                        VariableName = "MachineTime"
                                    }
                                }
                            }
                        }
                    }
                }
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            Assert.AreEqual(10m, result.CostMachine);
            Assert.AreEqual(10m, result.CostTotal);

            Assert.AreEqual(1, result.Components.Count);
            ComponentPriceResult assemblyResult = result.Components[0];

            Assert.AreEqual(10m, assemblyResult.CostMachine);
            Assert.AreEqual(10m, assemblyResult.CostNet);

            Assert.AreEqual(1, assemblyResult.ChildComponents.Count);
            ComponentPriceResult machineResult = (assemblyResult.ChildComponents as List<ComponentPriceResult>)[0];

            Assert.AreEqual(10m, machineResult.CostMachine);
            Assert.AreEqual(10m, machineResult.CostNet);
            Assert.AreEqual(true, machineResult.CostOV);

            Assert.AreEqual(1, machineResult.ChildComponents.Count);
            ComponentPriceResult machineTimeResult = (machineResult.ChildComponents as List<ComponentPriceResult>)[0];

            Assert.AreEqual(10m, machineTimeResult.CostMachine);
            Assert.AreEqual(10m, machineTimeResult.CostNet);
            Assert.AreEqual(true, machineTimeResult.CostOV);
        }
    }
}
