﻿using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class LayoutComputationTests
    {
        private ApiContext ctx = null;
        private PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;
        private int glAccountID = 0;
        private short taxCodeID = 0;

        private const short BID = 1;
        private const int _companyID = -99;
        private const int _taxGroupID = -99;

        private const int _materialID = -99;
        private const decimal _materialPrice = 5m;
        private const decimal _materialCost = 2.5m;

        private MachineData machine = null;
        private const int _machineID = -99;

        private const int _destinationID = -99;

        private const int _laborID = -99;
        private const decimal _laborPriceFixed = 15m;
        private const decimal _laborPriceHourly = 25m;
        private const decimal _laborCostFixed = 7.5m;
        private const decimal _laborCostHourly = 12.5m;

        private const int _assemblyID = -99;

        private const int _assemblyVariableID = -99;

        private const int _orderID = -99;
        private const int _orderItemID = -99;
        private const int _orderItemSurchargeID = -99;
        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            await CleanupDefaultPricingTests();
            await InitializeCompanyAndGLAccountIDs();
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            var _assemblyTables = ctx.AssemblyTable.Where(t => t.BID == BID && (t.ID < 0 || t.AssemblyID < 0));
            if (_assemblyTables.Any())
            {
                ctx.AssemblyTable.RemoveRange(_assemblyTables);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }
            var _assemblyVariableFormulas = ctx.AssemblyVariableFormula.Where(t => t.BID == BID && (t.ID < 0 || t.VariableID < 0));
            if (_assemblyVariableFormulas.Any())
            {
                ctx.AssemblyVariableFormula.RemoveRange(_assemblyVariableFormulas);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }
            var _assemblyVariables = ctx.AssemblyVariable.Where(t => t.BID == BID && (t.ID < 0 || t.AssemblyID < 0 || t.LinkedMaterialID < 0));
            if (_assemblyVariables.Any())
            {
                ctx.AssemblyVariable.RemoveRange(_assemblyVariables);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }

            var _orderItemComponents = ctx.OrderItemComponent.Where(t => t.BID == BID && (t.AssemblyID < 0 || t.ID < 0));
            if (_orderItemComponents.Any())
            {
                ctx.OrderItemComponent.RemoveRange(_orderItemComponents);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
            }

            var _assembly = await ctx.AssemblyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
            if (_assembly != null)
            {
                ITenantDataCache cache = new TestHelper.MockTenantDataCache();
                EntityStorageClient client = new EntityStorageClient((await cache.Get(BID)).StorageConnectionString, BID);
                var assemblyDoc = new DMID() { id = _assembly.ID, ctid = ClassType.Assembly.ID() };
                string assemblyName = CBELAssemblyHelper.AssemblyName(BID, _assembly.ID, _assembly.ClassTypeID, 1);

                await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
                ctx.MachineData.RemoveRange(ctx.MachineData.Where(t => t.BID == BID && t.MachineTemplateID == _assembly.ID));
                ctx.AssemblyData.Remove(_assembly);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }

            var _partMaterialCategoryLinks = ctx.MaterialCategoryLink.Where(t => t.BID == BID && t.PartID < 0);
            if (_partMaterialCategoryLinks.Any())
            {
                ctx.MaterialCategoryLink.RemoveRange(_partMaterialCategoryLinks);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
            }

            var _material1 = await ctx.MaterialData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
            if (_material1 != null)
            {
                ctx.MaterialData.Remove(_material1);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }

            var _orderItem = await ctx.OrderItemData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemID);
            if (_orderItem != null)
            {
                ctx.OrderItemData.Remove(_orderItem);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
            }

            var _taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
            if (_taxGroup != null)
            {
                ctx.TaxGroup.Remove(_taxGroup);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }

            ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
            ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
            ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
            ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == _orderID).ToArrayAsync());
            ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());

            var _order = await ctx.OrderData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderID);
            if (_order != null)
            {
                ctx.OrderData.Remove(_order);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
            }

            var _surcharge = await ctx.SurchargeDef.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
            if (_surcharge != null)
            {
                ctx.SurchargeDef.RemoveRange(_surcharge);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }

            var _company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
            if (_company != null)
            {
                var _orders = await ctx.OrderData.Where(t => t.BID == BID && t.CompanyID == _companyID).ToListAsync();
                foreach (var o in _orders)
                {

                    ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == o.ID && t.IsOrderNote));
                    var items = ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == o.ID);
                    var itemIDs = items.Select(x => x.ID);
                    ctx.OrderItemData.RemoveRange(items);
                    ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(t => t.BID == BID && itemIDs.Contains(t.OrderItemID)));
                    ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                    ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                    ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                    ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == o.ID).ToArrayAsync());
                    ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());

                    ctx.OrderData.Remove(o);
                    await RemoveTransheaderItems(o.ID);
                }

                var _estimates = await ctx.EstimateData.Where(t => t.BID == BID && t.CompanyID == _companyID).ToListAsync();
                foreach (var e in _estimates)
                {
                    ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == e.ID));
                    var items = ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == e.ID);
                    var itemIDs = items.Select(x => x.ID);
                    ctx.OrderItemData.RemoveRange(items);
                    ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(t => t.BID == BID && itemIDs.Contains(t.OrderItemID)));
                    ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                    ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                    ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                    ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == e.ID).ToArrayAsync());
                    ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                    ctx.EstimateData.Remove(e);
                    await RemoveTransheaderItems(e.ID);
                }

                ctx.CompanyData.Remove(_company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }

            var _labor = await ctx.LaborData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
            if (_labor != null)
            {
                ctx.LaborData.Remove(_labor);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
            }
        }

        private async Task RemoveTransheaderItems(int orderID)
        {
            var items = await ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync();
            foreach (var orderItem in items)
            {
                ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderItemID == orderItem.ID));
                ctx.OrderItemData.Remove(orderItem);
                foreach (var component in await ctx.OrderItemComponent.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
                {
                    ctx.OrderItemComponent.Remove(component);
                }
            }
            foreach (var role in await ctx.OrderEmployeeRole.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderEmployeeRole.Remove(role);
            }
            foreach (var keydate in await ctx.OrderKeyDate.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderKeyDate.Remove(keydate);
            }
        }
        // <summary>
        /// Initializes the Company and GLAccount IDs
        /// </summary>
        /// <returns></returns>
        protected async Task InitializeCompanyAndGLAccountIDs()
        {
            // check for existing company
            var company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
            if (company == null)
            {
                company = new CompanyData()
                {
                    BID = BID,
                    ID = _companyID,
                    Name = "Test Pricing Company",
                    LocationID = 1,
                    IsAdHoc = true,
                    StatusID = 1
                };

                ctx.CompanyData.Add(company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _taxGroupID);
            if (taxGroup == null)
            {
                taxGroup = new TaxGroup()
                {
                    BID = BID,
                    ID = _taxGroupID,
                    Name = "Test Pricing TaxGroup"
                };

                ctx.TaxGroup.Add(taxGroup);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            // there should always be at least one GLAccount and TaxCode
            glAccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == BID).ID;
            taxCodeID = ctx.TaxabilityCodes.FirstOrDefault(t => t.BID == BID).ID;
        }

        /// <summary>
        /// Creates a new Assembly for testing
        /// </summary>
        /// <param name="estimatingPrice">EstimatingPrice to use (default 5m)</param>
        /// <param name="estimatingCost">EstimatingCost to use (default 2.5m)</param>
        /// <param name="ID">MaterialID (default -99)</param>
        /// <returns></returns>
        protected async Task<AssemblyData> CreateAssembly(int ID = _assemblyID, Dictionary<string, string> variableDefaultDictionary = null)
        {
            int newRecords = 0;
            AssemblyData assembly = await ctx.AssemblyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (assembly == null)
            {
                newRecords++;
                assembly = new AssemblyData()
                {
                    ID = ID,
                    BID = BID,
                    Name = "Test Assembly For Pricing",
                    IncomeAccountID = glAccountID,

                };

                ctx.AssemblyData.Add(assembly);

                if (variableDefaultDictionary != null && variableDefaultDictionary.Count > 0)
                {
                    int lowVariableID = -99;

                    var variables = ctx.AssemblyVariable.Where(t => t.BID == BID);
                    if (variables.Any())
                    {
                        lowVariableID = variables.Min(t => t.ID);

                        if (lowVariableID > -99)
                            lowVariableID = -99;
                    }

                    assembly.Variables = new List<AssemblyVariable>();

                    foreach (var variableDefault in variableDefaultDictionary)
                    {
                        newRecords++;
                        lowVariableID--;
                        var newVariable = new AssemblyVariable()
                        {
                            ID = lowVariableID,
                            BID = BID,
                            AssemblyID = ID,
                            ElementType = AssemblyElementType.SingleLineText,
                            IsRequired = false,
                            Name = variableDefault.Key,
                            DefaultValue = variableDefault.Value,
                            IsFormula = (!string.IsNullOrWhiteSpace(variableDefault.Value) && variableDefault.Value.TrimStart().First() == '=')
                        };

                        ctx.AssemblyVariable.Add(newVariable);
                    }
                }

                Assert.AreEqual(newRecords, await ctx.SaveChangesAsync()); // verify add
            }

            return assembly;
        }

        private ComponentPriceRequest CreateComponentRequest(int assemblyID, Dictionary<string, VariableValue> Variables)
        {
            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assemblyID,
                TotalQuantity = 5,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { $"{machine.Name}", new VariableValue() { DataType = DataType.String, Value = machine.Name, ValueOV = true } },
                },
                ChildComponents = new List<ComponentPriceRequest>()
                {
                    new ComponentPriceRequest()
                    {
                        ComponentID = _assemblyVariableID,
                        ComponentType = OrderItemComponentType.Assembly,
                        TotalQuantity = 2,
                        TotalQuantityOV = false,
                        VariableName = "TestLayoutComputation",
                        Variables = Variables
                    }
                }
            };
            return request;
        }

        private void CreateAssemblyVariable(int assemblyID)
        {
            var assemblyVariable = new AssemblyVariable()
            {
                Name = "TestLayoutComputation",
                BID = BID,
                AssemblyID = assemblyID,
                ID = _assemblyVariableID + 1,
                ElementType = AssemblyElementType.LayoutComputationAssembly,
                IsConsumptionFormula = false
            };
            ctx.AssemblyVariable.Add(assemblyVariable);
        }

        protected async Task<DestinationData> CreateDestination(short ID = _destinationID, int assemblyID = _assemblyID, string destinationName = "Test Destination For Pricing")
        {
            var destination = await ctx.DestinationData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (destination == null)
            {
                destination = new DestinationData()
                {
                    ID = ID,
                    BID = BID,
                    Name = destinationName,
                    ActiveProfileCount = 1
                };
                ctx.DestinationData.Add(destination);
                var assemblyVariable = new AssemblyVariable()
                {
                    Name = destinationName,
                    DefaultValue = destinationName,
                    BID = BID,
                    AssemblyID = assemblyID,
                    ID = ID,
                    ConsumptionDefaultValue = "=4*2",
                    IsConsumptionFormula = true,
                    LinkedMachineID = machine.ID,
                    ElementType = AssemblyElementType.LinkedMachine,
                    DataType = DataType.String,
                    ListDataType = DataType.MachinePart,
                };
                ctx.AssemblyVariable.Add(assemblyVariable);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }
            return destination;
        }

        protected async Task<MachineData> CreateMachine(short ID = _machineID, int assemblyID = _assemblyID, string machineName = "Test Machine For Pricing")
        {
            var machine = await ctx.MachineData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (machine == null)
            {
                machine = new MachineData()
                {
                    ID = ID,
                    BID = BID,
                    Name = machineName,
                    ActiveProfileCount = 1,
                    ActiveInstanceCount = 2,
                };
                ctx.MachineData.Add(machine);
                var assemblyVariable = new AssemblyVariable()
                {
                    Name = machineName,
                    DefaultValue = machineName,
                    BID = BID,
                    AssemblyID = assemblyID,
                    ID = ID,
                    ConsumptionDefaultValue = "=4*2",
                    IsConsumptionFormula = true,
                    LinkedMachineID = machine.ID,
                    ElementType = AssemblyElementType.LinkedMachine,
                    DataType = DataType.String,
                    ListDataType = DataType.MachinePart,
                };
                ctx.AssemblyVariable.Add(assemblyVariable);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }
            return machine;
        }

        [TestMethod]
        public async Task TestAssemblyLayoutComputationAreLoaded()
        {
            var assembly = await CreateAssembly();

            var assemblyVariable = new AssemblyVariable()
            {
                Name = "TestLayoutComputation",
                BID = BID,
                AssemblyID = assembly.ID,
                ID = _assemblyVariableID + 1,
                ElementType = AssemblyElementType.LayoutComputationAssembly,
                IsConsumptionFormula = false
            };
            ctx.AssemblyVariable.Add(assemblyVariable);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 5,
                ChildComponents = new List<ComponentPriceRequest>()
                {
                    new ComponentPriceRequest()
                    {
                        ComponentID = 1,
                        ComponentType = OrderItemComponentType.Assembly,
                        TotalQuantity = 2,
                        TotalQuantityOV = false,
                        VariableName = "TestLayoutComputation",
                        Variables = new Dictionary<string, VariableValue>()
                        {
                            { $"ColorsBack", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"ColorsFront", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"UseVisualization", new VariableValue() { DataType = DataType.Boolean, Value="true", ValueOV = true } },
                            { $"MaterialSetupWasteCount", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"MaterialRunningWastePercentage", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"SignatureCount", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"LayoutType", new VariableValue() { DataType = DataType.String, Value="Sheet", ValueOV = true } },
                            { $"ItemHeight", new VariableValue() { DataType = DataType.Number, Value="5", ValueOV = true } },
                            { $"ItemWidth", new VariableValue() { DataType = DataType.Number, Value="2", ValueOV = true } },
                            { $"MaterialWidth", new VariableValue() { DataType = DataType.Number, Value="100", ValueOV = true } },
                            { $"MaterialHeight", new VariableValue() { DataType = DataType.Number, Value="100", ValueOV = true } },
                        }
                    }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            if (result.ErrorCount > 0)
                Assert.Fail(String.Join("\r\n", result.Errors.Select(t => t.Message)));
            
            var myAssemblyLayoutVariable = result.Variables.Where(x => x.Key.Contains("TestLayoutComputation")).First();
            var value = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(result.ChildComponents.First().Variables.First(t=>t.Key == "VisualizationData").Value.Value);
            Assert.IsNotNull(value);
            //Assert.AreEqual(labor.MinimumTimeInMin, result.Variables[assemblyVariable.Name].ValueAsDecimal);
        }

        [TestMethod]
        public async Task TestLinkedAssemblyLayoutComputationPropertiesAreAccessibleInParentAssembly()
        {
            var assembly = await CreateAssembly();

            var assemblyVariable = new AssemblyVariable()
            {
                Name = "TestLayoutComputation",
                BID = BID,
                AssemblyID = assembly.ID,
                ID = _assemblyVariableID + 1,
                ElementType = AssemblyElementType.LayoutComputationAssembly,
                IsConsumptionFormula = false
            };
            ctx.AssemblyVariable.Add(assemblyVariable);

            var totalItemAreaVariable = new AssemblyVariable()
            {
                Name = "TotalItemArea",
                BID = BID,
                AssemblyID = assembly.ID,
                ID = _assemblyVariableID + 2,
                ElementType = AssemblyElementType.Number,
                IsFormula = true,
                DefaultValue = "=TestLayoutComputation.TotalItemArea",
            };
            ctx.AssemblyVariable.Add(totalItemAreaVariable);

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);

            const decimal itemHeight = 5m;
            const decimal itemWidth = 2m;
            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 5,
                ChildComponents = new List<ComponentPriceRequest>()
                {
                    new ComponentPriceRequest()
                    {
                        ComponentID = 1,
                        ComponentType = OrderItemComponentType.Assembly,
                        TotalQuantity = 2,
                        TotalQuantityOV = false,
                        VariableName = "TestLayoutComputation",
                        Variables = new Dictionary<string, VariableValue>()
                        {
                            { $"ColorsBack", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"ColorsFront", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"UseVisualization", new VariableValue() { DataType = DataType.Boolean, Value="true", ValueOV = true } },
                            { $"MaterialSetupWasteCount", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"MaterialRunningWastePercentage", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"SignatureCount", new VariableValue() { DataType = DataType.Number, Value="1", ValueOV = true } },
                            { $"LayoutType", new VariableValue() { DataType = DataType.String, Value="Sheet", ValueOV = true } },
                            { $"ItemHeight", new VariableValue() { DataType = DataType.Number, Value= itemHeight.ToString(), ValueOV = true } },
                            { $"ItemWidth", new VariableValue() { DataType = DataType.Number, Value=itemWidth.ToString(), ValueOV = true } },
                            { $"MaterialWidth", new VariableValue() { DataType = DataType.Number, Value="100", ValueOV = true } },
                            { $"MaterialHeight", new VariableValue() { DataType = DataType.Number, Value="100", ValueOV = true } },
                        }
                    }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            if (result.ErrorCount > 0)
                Assert.Fail(String.Join("\r\n", result.Errors.Select(t => t.Message)));

            var computedVariable = result.Variables.FirstOrDefault(t=>t.Key == totalItemAreaVariable.Name).Value;
            Assert.IsNotNull(computedVariable);
            Assert.AreEqual<decimal>(itemHeight * itemWidth, computedVariable.ValueAsDecimal);            
        }
        /*[TestMethod]
        public async Task TestIsPaneledHorizontally()
        {
            var assembly = await CreateAssembly();
            var machine = await CreateMachine(templateID: assembly.ID, machineName: "TestLinkedMachine");
            ctx.SaveChanges();
            CreateAssemblyVariable(assembly.ID);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0);


            var request = CreateComponentRequest(assembly.ID,
                new Dictionary<string, VariableValue>()
                        {
                            { $"LayoutType", new VariableValue() { DataType = DataType.String, Value="Sheet", ValueOV = true } }
                        }
                );
            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.ErrorCount);
            var value = JsonConvert.DeserializeObject<LayoutComputationVisualizationModel>(result.Variables.Where(x => x.Key.Contains("TestLayoutComputation")).First().Value.Value);
            Assert.IsNotNull(value);
            //Assert.AreEqual(labor.MinimumTimeInMin, result.Variables[assemblyVariable.Name].ValueAsDecimal);
        }*/

    }
}
