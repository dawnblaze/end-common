﻿using Endor.CBEL.Common;
using Endor.CBEL.Elements;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class CBELDataTableTests
    {
        [TestMethod]
        public void TestStringTableNumberRowStringColumn()
        {
            short bid = 1;

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);

            Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx parent = new Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx(PricingTestHelper.GetMockCtx(bid), bid, cache, logger);
            StringDataTable<decimal?, string> table = new StringDataTable<decimal?, string>(parent);
            table.RowHeadings.AddRange(new decimal?[] { 1, 3, 10, 18, 36 });
            table.ColumnHeadings.AddRange(new string[] { "Amber","Bill","Charlie","Derek","Greg","Mike","Zebin"});
            string[,] cellarray = { { "Red","Ginger", "Green","Yellow","Violet","Orange","" },
                { "Blue","Black", "","Brown","Red","White","Red" },
                { "Red","Ginger", "Green","Yellow","Violet","Orange","" },
                { "Blue","Black", "Blue","Brown","Red","White","Red"},
                { "Yellow","Violet", "Orange","","Ginger","Green","Yellow" }};
            table.AddCells(cellarray);

            RunTest<string,decimal?,string>(table,"Amber", 0, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, "Error", true);
            RunTest<string, decimal?, string>(table, "Amber", 0, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseLowerValue, "Error", true);
            RunTest<string, decimal?, string>(table, "Amber", 0, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseHigherValue, "Red");
            RunTest<string, decimal?, string>(table, "Derek", 2, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseHigherValue, "Brown");
            RunTest<string, decimal?, string>(table, "Greg", 7, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseHigherValue, "Violet");
            RunTest<string, decimal?, string>(table, "Derek", 18, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, "Brown");
            RunTest<string, decimal?, string>(table, "Derek", 20, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, "Error", true);
            RunTest<string, decimal?, string>(table, "Derek", 20, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseLowerValue, "Brown");
            RunTest<string, decimal?, string>(table, "Derek", 28, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseHigherValue, "");
            RunTest<string, decimal?, string>(table, "Derek", 40, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseLowerValue, "");
            RunTest<string, decimal?, string>(table, "Derek", 40, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseHigherValue, "Error", true);
            RunTest<string, decimal?, string>(table, "Sarah", 3, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, "Error", true);
            RunTest<string, decimal?, string>(table, "Zebin", 36, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.UseHigherValue, "Yellow");
            RunTest<string, decimal?, string>(table, "Zebin", 36, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, "Yellow");
            
        }

        [TestMethod]
        public void TestNumberTableNumberRowNumberColumn()
        {
            short bid = 1;
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx parent = new Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx(PricingTestHelper.GetMockCtx(bid), bid, cache, logger);
            NumberDataTable<decimal?, decimal?> table = new NumberDataTable<decimal?, decimal?>(parent);

            /*
                       1       5      12      18      24      36      99
                 ------- ------- ------- ------- ------- ------- -------
             1      1.75    5.00   12.00   18.00   24.00   36.00    null
             3      3.00   15.00   36.00   54.00   72.00  108.00    null
            10     10.00   50.00  120.00  180.00  240.00  360.00  420.00
            18      null   90.00  216.00    null  432.00  648.00    null
            36     36.00  180.00  432.00  648.00  864.00    null    null
              
            */

            table.RowHeadings.AddRange(new decimal?[] { 1, 3, 10, 18, 36 });
            table.ColumnHeadings.AddRange(new decimal?[] { 1, 5, 12, 18, 24, 36, 99 });
            decimal?[,] cellarray = { 
                {1.75m, 5   ,12  ,18  ,24  ,36  ,null},
                {3   ,15  ,36  ,54  ,72  ,108 ,null},
                {10  ,50  ,120 ,180 ,240 ,360 ,420 },
                {null,90  ,216 ,null,432 ,648 ,null},
                {36  ,180 ,432 ,648 ,864 ,null,null}
            };
            table.AddCells(cellarray);

            RunTest<decimal?, decimal?, decimal?>(table, 1, 1, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, 1.75m);
            RunTest<decimal?, decimal?, decimal?>(table, 3, 3, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, null, true);
            RunTest<decimal?, decimal?, decimal?>(table, 3, 3, AssemblyTableMatchType.UseHigherValue, AssemblyTableMatchType.UseLowerValue, 15);
            RunTest<decimal?, decimal?, decimal?>(table, 3, 3, AssemblyTableMatchType.UseLowerValue, AssemblyTableMatchType.UseHigherValue, 3);
            RunTest<decimal?, decimal?, decimal?>(table, 3, 3, AssemblyTableMatchType.Interpolate, AssemblyTableMatchType.Interpolate, 9);
            RunTest<decimal?, decimal?, decimal?>(table, 20, 20, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, null, true);
            RunTest<decimal?, decimal?, decimal?>(table, 20, 20, AssemblyTableMatchType.UseHigherValue, AssemblyTableMatchType.Interpolate, 480);
            RunTest<decimal?, decimal?, decimal?>(table, 20, 20, AssemblyTableMatchType.Interpolate, AssemblyTableMatchType.UseLowerValue, null);
            RunTest<decimal?, decimal?, decimal?>(table, 20, 20, AssemblyTableMatchType.Interpolate, AssemblyTableMatchType.Interpolate, null);
            RunTest<decimal?, decimal?, decimal?>(table, 99, 1, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, null);
            RunTest<decimal?, decimal?, decimal?>(table, 50, 50, AssemblyTableMatchType.UseHigherValue, AssemblyTableMatchType.UseHigherValue, null, true);
            RunTest<decimal?, decimal?, decimal?>(table, 1, 18, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, null);
            RunTest<decimal?, decimal?, decimal?>(table, 0, 0, AssemblyTableMatchType.UseHigherValue, AssemblyTableMatchType.UseHigherValue, 1.75m);
            RunTest<decimal?, decimal?, decimal?>(table, 0, 0, AssemblyTableMatchType.UseLowerValue, AssemblyTableMatchType.UseLowerValue, null, true);
            RunTest<decimal?, decimal?, decimal?>(table, 0, 0, AssemblyTableMatchType.Interpolate, AssemblyTableMatchType.Interpolate, null, true);
            RunTest<decimal?, decimal?, decimal?>(table, 100, 10, AssemblyTableMatchType.ExactMatch, AssemblyTableMatchType.ExactMatch, null, true);
            RunTest<decimal?, decimal?, decimal?>(table, 100, 10, AssemblyTableMatchType.UseLowerValue, AssemblyTableMatchType.UseLowerValue, 420);
            RunTest<decimal?, decimal?, decimal?>(table, 100, 100, AssemblyTableMatchType.Interpolate, AssemblyTableMatchType.Interpolate, null, true);
            
        }

        private void RunTest<T,RowDataType,ColumnDataType>(CBELDataTable<T, RowDataType, ColumnDataType> table, ColumnDataType colValue, RowDataType rowValue, AssemblyTableMatchType colMatch, AssemblyTableMatchType rowMatch,  T Expected, bool IsError = false)
        {
            //clear up errors for new test
            table.Parent.Errors.Clear();

            T result = table.TableLookup(rowValue, colValue, rowMatch, colMatch);
            if (!IsError)
            {
                Assert.AreEqual(Expected, result);
                Assert.IsTrue(table.Parent.Errors.Count == 0, "Errors found when they weren't expected");
            } else
            {
                Assert.AreEqual(null, result);
                Assert.IsTrue(table.Parent.Errors.Count > 0, "Errors were expected but none were found");
            }
        }
    }

    public class Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx : CBELAssemblyBase
    {
        public Assembly_BIDxxx_CTxxx_IDxxx_Vxxxx(ApiContext ctx, short bid, ITenantDataCache cache, RemoteLogger logger) : base(ctx, bid, cache, logger)
        {
        }

        public override NumberEditElement Price => throw new NotImplementedException();
        public override NumberEditElement TotalQuantity => throw new NotImplementedException();
        public override void InitializeVariables()
        {

        }

        public override void InitializeData()
        {

        }
    }
}
