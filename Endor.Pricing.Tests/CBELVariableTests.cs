﻿using Endor.CBEL.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Endor.Models;
using System.Collections.Generic;
using System.Reflection;
using System;
using Endor.Units;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class CBELVariableTests
    {
        [TestMethod]
        [DataRow(36, Unit.Foot, "InInches", 36.0 * 12.0)]
        [DataRow(36, Unit.Foot, "InYards", 36.0 / 3.0)]
        [DataRow(12, Unit.Foot, "InBytes", null)]
        [DataRow(2, Unit.Foot, "InMillimeters", 2.0 * 304.8)]
        [DataRow(450, Unit.Gram, "InKilograms", 450.0 / 1000.0)]
        [DataRow(1.7, Unit.Yard, "InMillilitersPerHour", null)]
        [DataRow(1.7, Unit.Gigabyte, "InKiloBytes", 1.7 * 1e+6)]
        [DataRow(10, Unit.InchPerSecond, "InFeetPerMinute", 0.0138888892)] // formula seems to be too long for this convertion to be done inline, but doable
        [DataRow(15, Unit.Carton, "InCubicInches", null)]
        public void MeasurementConvertPrecision_Test(double defaultValueConstant, Unit unit, string propertyName, double? expectedValue)
        {
            static decimal? InvokeConvertMethod(MethodInfo method, NumberVariable variable)
            {
                decimal? result = method.Invoke(null, new object[] { variable }) as decimal?;

                if (result.HasValue)
                    return decimal.Round(result.Value, 4);

                return null;
            }

            decimal? expected = null;
            if (expectedValue != null)
                expected = decimal.Round(Convert.ToDecimal(expectedValue), 4);

            MethodInfo method = typeof(MeasurementExtension).GetMethod(propertyName);

            NumberVariable measurementVariable = new NumberVariable(null)
            {
                DefaultValueConstant = Convert.ToDecimal(defaultValueConstant),
                Unit = unit,
            };

            Assert.AreEqual(expected, InvokeConvertMethod(method, measurementVariable));
        }

        [TestMethod]
        [DataRow(Unit.Centimeter, Unit.KiloByte, null)]
        [DataRow(Unit.Foot, Unit.Inch, typeof(decimal))]
        [DataRow(Unit.Second, Unit.Hour, typeof(decimal))]
        [DataRow(Unit.Box, Unit.Liter, null)]
        [DataRow(Unit.Milligram, Unit.Kilogram, typeof(decimal))]
        [DataRow(Unit.CubicInchPerMinute, Unit.Minute, null)]
        [DataRow(Unit.SquareInchPerSecond, Unit.SquareMillimeterPerMinute, typeof(decimal))]
        public void CompatibilityConversion_Test(Unit unit, Unit toUnit, System.Type expected)
        {
            NumberVariable measurementVariable = new NumberVariable(null)
            {
                DefaultValueConstant = 1m,
                Unit = unit,
            };

            var result = measurementVariable.ConvertTo(toUnit);
            System.Type resultType = null;

            // try to get type of result
            try
            {
                resultType = result.GetType();
            }
            catch(System.Exception)
            {
                // ignore since it should still be null
            }

            Assert.AreEqual(expected, resultType);
        }

        [TestMethod]
        [DataRow(Unit.Box, UnitType.Discrete)]
        [DataRow(Unit.Carton, UnitType.Discrete)]
        [DataRow(Unit.SquareCentimeter, UnitType.Area)]
        [DataRow(Unit.CubicFoot, UnitType.Volume_Solid)]
        [DataRow(Unit.KiloByte, UnitType.DigitalStorage)]
        [DataRow(Unit.CubicFeetPerHour, UnitType.Speed_Solid)]
        [DataRow(Unit.SquareFootPerSecond, UnitType.Speed_Area)]
        [DataRow(Unit.InchPerMinute, UnitType.Speed_Length)]
        [DataRow(Unit.FeetPerHour, UnitType.Speed_Length)]
        [DataRow(Unit.Minute, UnitType.Time)]
        [DataRow(Unit.Hour, UnitType.Time)]
        [DataRow(Unit.Meter, UnitType.Length)]
        [DataRow(Unit.Inch, UnitType.Length)]
        public void TypeAndNameValidation_Test(Unit unit, UnitType expected)
        {
            NumberVariable measurementVariable = new NumberVariable(null)
            {
                DefaultValueConstant = 1m,
                Unit = unit,
            };

            Assert.AreEqual(expected, measurementVariable.UnitType);
            Assert.AreEqual(expected.Name(), measurementVariable.UnitTypeName);

        }
    }
}
