﻿using Endor.Level3.Tests.Common;
using Endor.Pricing.Tests.Helper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Endor.Models;
using Endor.Models.Autocomplete;
using System.Collections.Generic;
using Endor.EF;
using Endor.CBEL.Metadata;
using Endor.CBEL.Operators;
using Endor.CBEL.Autocomplete;
using Endor.Units;
using Endor.Tenant;
using Endor.Logging.Client;
using Endor.CBEL.Common;
using System.Reflection;
using System;
using System.Linq;
using Newtonsoft.Json;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class AutocompleteTests
    {
        private ApiContext ctx = null;
        private IAutocompleteEngine autocompleteEngine = null;
        ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        private const short BID = 1;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);

            autocompleteEngine = new AutocompleteEngine(BID, ctx, cache, logger);
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }


        [TestMethod]
        public async Task GetCommon_Test()
        {
            string common = await autocompleteEngine.CommonDefinition();

            Assert.IsNotNull(common);
        }

        [TestMethod]
        public async Task GetCommonAssembly_Test()
        {
            string common = await autocompleteEngine.CommonDefinition(ClassType.Assembly.ID());

            Assert.IsNotNull(common);
        }

        [TestMethod]
        public async Task GetCommonMachine_Test()
        {
            string common = await autocompleteEngine.CommonDefinition(ClassType.Machine.ID());

            Assert.IsNotNull(common);
        }

        [TestMethod]
        public async Task GetHint_Test()
        {
            string hint = await autocompleteEngine.Hint("DATE");

            Assert.IsNotNull(hint);
        }

        [TestMethod]
        public async Task GetLocalVariableDefinition_Test()
        {
            string expectedOutput = @"declare var Group1: VariableBase;
declare var TotalQuantity: NumberVariable;
declare var Height: NumericLengthVariable;
declare var Width: NumericLengthVariable;
declare var Area: NumericAreaVariable;
declare var Name: StringVariable;
declare var LinkedAssembly_1: LinkedAssemblyVariableBase;
declare var LinkedLabor_1: LinkedLaborVariableBase;
declare var LinkedMachine_1: LinkedMachineVariableBase;
declare var LinkedMaterial_1: LinkedMaterialVariableBase;
";

            List<AssemblyVariableAutocompleteBrief> assemblyVariables = new List<AssemblyVariableAutocompleteBrief>()
            { 
                                new AssemblyVariableAutocompleteBrief()
                                {
                    Name = "Group1",
                    ElementType = AssemblyElementType.Group
                },
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "TotalQuantity",
                    ElementType = AssemblyElementType.Number,            
                    UnitType = UnitType.General
                },
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "Height",
                    ElementType = AssemblyElementType.Number,
                    UnitType = UnitType.Length
                },
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "Width",
                    ElementType = AssemblyElementType.Number,
                    UnitType = UnitType.Length
                },                
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "Area",
                    ElementType = AssemblyElementType.Number,
                    UnitType = UnitType.Area
                },
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "Name",
                    ElementType = AssemblyElementType.SingleLineText
                },
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "LinkedAssembly_1",
                    ElementType = AssemblyElementType.LinkedAssembly
                },
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "LinkedLabor_1",
                    ElementType = AssemblyElementType.LinkedLabor
                },
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "LinkedMachine_1",
                    ElementType = AssemblyElementType.LinkedMachine
                },
                new AssemblyVariableAutocompleteBrief()
                {
                    Name = "LinkedMaterial_1",
                    ElementType = AssemblyElementType.LinkedMaterial
                },
            };

            var localVariables = await autocompleteEngine.LocalVariableDefinitions(assemblyVariables);
            Assert.IsNotNull(localVariables);
            Assert.AreEqual(expectedOutput, localVariables);
        }

        [TestMethod]
        [DataRow("ExpenseAccountID", false, true)]
        [DataRow("TheCost", false, false)]
        [DataRow("TotalQuantity", false, true)]
        [DataRow("ChildA", true, true)]
        [DataRow("ChildA.ExpenseAccountID", false, true)]
        [DataRow("ChildA.TheCost", false, false)]
        [DataRow("ChildA.TotalQuantity", false, true)]
        [DataRow("ChildA.ChildB", true, true)]
        [DataRow("ChildA.ChildB.ExpenseAccountID", false, true)]
        [DataRow("ChildA.ChildB.TheCost", false, false)]
        [DataRow("ChildA.ChildB.TotalQuantity", false, true)]
        public async Task PathNaming_Test(string path, bool isAssembly, bool isValidPath)
        {
            string pathvalue = null;
            string classDefinition = null;
            AssemblyData parentProduct = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "MyParentProduct",
                Variables = new List<AssemblyVariable>()
            };

            AssemblyData childProduct = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "MyChildProduct",
                Variables = new List<AssemblyVariable>()
            };            

            AssemblyData machineTemplate = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "Test Template",
                AssemblyType = AssemblyType.MachineTemplate
            };

            MachineData childFactory = new MachineData()
            {
                BID = BID,
                ID = -100,
                ExpenseAccountID = 5000,
                Name = "MyChildFactory",
                EstimatingCostPerHourFormula = "1",
                MachineTemplateID = machineTemplate.ID
            };

            AssemblyVariable childA = new AssemblyVariable()
            {
                Name = "ChildA",
                ElementType = AssemblyElementType.LinkedAssembly,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                LinkedAssemblyID = childProduct.ID
            };

            AssemblyVariable childB = new AssemblyVariable()
            {
                Name = "ChildB",
                ElementType = AssemblyElementType.LinkedMachine,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                LinkedMachineID = childFactory.ID
            };

            parentProduct.Variables.Add(childA);
            childProduct.Variables.Add(childB);

            ctx.AssemblyData.Add(machineTemplate);
            ctx.MachineData.Add(childFactory);
            ctx.AssemblyData.Add(childProduct);
            ctx.AssemblyData.Add(parentProduct);

            await ctx.SaveChangesAsync();


            try
            {
                pathvalue = await autocompleteEngine.GetAssemblyByID(parentProduct.ID, path);
                // for reference
                classDefinition = await autocompleteEngine.ClassDefinition(parentProduct.ID, ClassType.Assembly.ID());
            }
            catch (KeyNotFoundException e)
            {
                Assert.AreEqual($"Invalid Path {path.Split('.').Last()}", e.Message);
            }

            Assert.AreEqual(isAssembly, !string.IsNullOrWhiteSpace(pathvalue));
            Assert.AreEqual(isValidPath, pathvalue != null);
        }

        [TestMethod]
        [DataRow("Lamination", true)]
        [DataRow("Lamination.Machine", true)]
        [DataRow("Prices", false)]
        [DataRow("DefinedFixedMargin", false)]
        [DataRow("Name", false)]
        public async Task PathValidity_Test(string path, bool isAssembly)
        {
            string pathvalue = null;
            AssemblyData laminationAssembly = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "Lamination",
                Variables = new List<AssemblyVariable>()
            };

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "ParentAssembly",
                Variables = new List<AssemblyVariable>()
            };

            AssemblyData machineTemplate = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "Test Template",
                AssemblyType = AssemblyType.MachineTemplate
            };

            MachineData machineData = new MachineData()
            {
                BID = BID,
                ID = -100,
                ExpenseAccountID = 5000,
                Name = "Linked_Machine",
                EstimatingCostPerHourFormula = "1",
                MachineTemplateID = machineTemplate.ID
            };

            AssemblyVariable varPrice = new AssemblyVariable()
            {
                Name = "Price",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = parentAssembly.ID,
                DefaultValue = "=100",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varArea = new AssemblyVariable()
            {
                Name = "Area",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = parentAssembly.ID,
                DefaultValue = "2",
                DataType = DataType.Number,
                IsFormula = false,
            };

            AssemblyVariable varLamination = new AssemblyVariable()
            {
                Name = "Lamination",
                ElementType = AssemblyElementType.LinkedAssembly,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                LinkedAssemblyID = laminationAssembly.ID
            };

            AssemblyVariable varMachine = new AssemblyVariable()
            {
                Name = "Machine",
                ElementType = AssemblyElementType.LinkedMachine,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                LinkedMachineID = machineData.ID
            };

            parentAssembly.Variables.Add(varPrice);
            parentAssembly.Variables.Add(varArea);
            parentAssembly.Variables.Add(varLamination);

            laminationAssembly.Variables.Add(varMachine);

            ctx.AssemblyData.Add(machineTemplate);
            ctx.MachineData.Add(machineData);
            ctx.AssemblyData.Add(laminationAssembly);
            ctx.AssemblyData.Add(parentAssembly);
            
            await ctx.SaveChangesAsync();


            try
            {
                pathvalue = await autocompleteEngine.GetAssemblyByID(parentAssembly.ID, path);
            }
            catch (KeyNotFoundException e)
            {
                Assert.AreEqual("Provided path is invalid.", e.Message);
            }

            Assert.AreEqual(isAssembly, !string.IsNullOrWhiteSpace(pathvalue));
        }

        [TestMethod]
        [DataRow("Lamination", true)]
        [DataRow("Lamination.Machine", true)]
        [DataRow("Prices", false)]
        [DataRow("DefinedFixedMargin", false)]
        [DataRow("Name", false)]
        public async Task PathValidityThruAdhocAssemblyTest(string path, bool isAssembly)
        {
            string pathvalue = null;
            AssemblyData laminationAssembly = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "Lamination",
                Variables = new List<AssemblyVariable>()
            };

            string json = "[{\"Name\":\"TotalQuantity\",\"ElementType\":21,\"UnitType\":9},{\"Name\":\"AssemblyQuantity\",\"ElementType\":21,\"UnitType\":9,\"ListDataType\":2},{\"Name\":\"Price\",\"ElementType\":21,\"UnitType\":1},{\"Name\":\"Tier\",\"ElementType\":1},{\"Name\":\"Lamination\",\"ElementType\":106,\"LinkedAssemblyID\":"+ laminationAssembly.ID.ToString() + "}]";
            var assemblyVariables = JsonConvert.DeserializeObject<List<AssemblyVariableAutocompleteBrief>>(json);

            AssemblyData parentAssembly = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "ParentAssembly",
                Variables = new List<AssemblyVariable>()
            };

            AssemblyData machineTemplate = new AssemblyData()
            {
                BID = BID,
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                Name = "Test Template",
                AssemblyType = AssemblyType.MachineTemplate
            };

            MachineData machineData = new MachineData()
            {
                BID = BID,
                ID = -100,
                ExpenseAccountID = 5000,
                Name = "Linked_Machine",
                EstimatingCostPerHourFormula = "1",
                MachineTemplateID = machineTemplate.ID
            };

            AssemblyVariable varPrice = new AssemblyVariable()
            {
                Name = "Price",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = parentAssembly.ID,
                DefaultValue = "=100",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varArea = new AssemblyVariable()
            {
                Name = "Area",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = parentAssembly.ID,
                DefaultValue = "2",
                DataType = DataType.Number,
                IsFormula = false,
            };

            AssemblyVariable varLamination = new AssemblyVariable()
            {
                Name = "Lamination",
                ElementType = AssemblyElementType.LinkedAssembly,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                LinkedAssemblyID = laminationAssembly.ID
            };

            AssemblyVariable varMachine = new AssemblyVariable()
            {
                Name = "Machine",
                ElementType = AssemblyElementType.LinkedMachine,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                LinkedMachineID = machineData.ID
            };

            parentAssembly.Variables.Add(varPrice);
            parentAssembly.Variables.Add(varArea);
            parentAssembly.Variables.Add(varLamination);

            laminationAssembly.Variables.Add(varMachine);

            ctx.AssemblyData.Add(machineTemplate);
            ctx.MachineData.Add(machineData);
            ctx.AssemblyData.Add(laminationAssembly);
            ctx.AssemblyData.Add(parentAssembly);

            await ctx.SaveChangesAsync();


            try
            {   
                pathvalue = await autocompleteEngine.AssemblyVariables(assemblyVariables, path);
            }
            catch (KeyNotFoundException)
            {

            }

            Assert.AreEqual(isAssembly, !string.IsNullOrWhiteSpace(pathvalue));
        }

        [TestMethod]
        public async Task GetHintNotFound_Test()
        {
            string hint = await autocompleteEngine.Hint("abc123");

            Assert.IsNull(hint);
        }

        [TestMethod]
        public async Task GetFunction_Test()
        {
            CBELFunctionInfo[] result = await autocompleteEngine.Functions();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetOperator_Test()
        {
            CBELOperatorInfo[] result = await autocompleteEngine.Operators();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetAssembly_Test()
        {
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            int glaccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == BID).ID;

            AssemblyVariable varPrice = new AssemblyVariable()
            {
                Name = "Price",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = assemblyID,
                DefaultValue = "=100",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varArea = new AssemblyVariable()
            {
                Name = "Area",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = assemblyID,
                DefaultValue = "2",
                DataType = DataType.Number,
                IsFormula = false,
            };
            
            AssemblyData linkedAssembly = new AssemblyData()
            {
                Name = "LinkedAssembly_1",
                ID = PricingTestHelper.NextAssemblyID(ctx, BID),
                BID = BID
            };

            MachineData linkedMachine = new MachineData()
            {
                Name = "LinkedMachine_1",
                ID = -99,
                BID = BID,
                ExpenseAccountID = 5000,
                EstimatingCostPerHourFormula = "1",
            };            

            MaterialData linkedMaterial = new MaterialData()
            {
                Name = "LinkedMaterial_1",
                ID = PricingTestHelper.NextMaterialID(ctx, BID),
                BID = BID,
                InvoiceText = "Test Linked Material",
                ConsumptionUnit = Unit.Each,
                EstimatingConsumptionMethod = MaterialConsumptionMethod.Each,
                EstimatingCost = 1m,
                EstimatingCostingMethod = MaterialCostingMethod.Manual,
                ExpenseAccountID = 5000,
                HasImage = false,
                IncomeAccountID = 4000,
                InventoryAccountID = 1510,
                IsActive = true,
                PhysicalMaterialType = MaterialPhysicalType.Discrete,
                QuantityInSet = 1,
                SKU = "",
                TrackInventory = false,
                TaxCodeID = 9,
            };

            LaborData linkedLabor = new LaborData()
            {
                Name = "LinkedLabor_1",
                ID = PricingTestHelper.NextLaborID(ctx, BID),
                BID = BID,
                EstimatingCostPerHour = 1 * 60,
                EstimatingPricePerHour = 1 * 60,
                InvoiceText = "Test Linked Labor",
                ExpenseAccountID = glaccountID,
                IncomeAccountID = glaccountID
            };

            AssemblyVariable parentLinkedAssembly = new AssemblyVariable()
            {
                Name = "ParentLinkedAssembly_1",
                ElementType = AssemblyElementType.LinkedAssembly,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                DefaultValue = "2",
                DataType = DataType.Number,
                LinkedAssemblyID = linkedAssembly.ID,
                IsFormula = false,
            };

            AssemblyVariable parentLinkedMachine = new AssemblyVariable()
            {
                Name = "ParentLinkedMachine_1",
                ElementType = AssemblyElementType.LinkedMachine,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                DefaultValue = "2",
                DataType = DataType.Number,
                LinkedMachineID = (short)linkedMachine.ID,
                IsFormula = false,
            };

            AssemblyVariable parentLinkedMaterial = new AssemblyVariable()
            {
                Name = "ParentLinkedMaterial_1",
                ElementType = AssemblyElementType.LinkedMaterial,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                DefaultValue = "2",
                DataType = DataType.Number,
                LinkedMaterialID = linkedMaterial.ID,
                IsFormula = false,
            };

            AssemblyVariable parentLinkedLabor = new AssemblyVariable()
            {
                Name = "ParentLinkedLabor_1",
                ElementType = AssemblyElementType.LinkedLabor,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                DefaultValue = "2",
                DataType = DataType.Number,
                LinkedLaborID = linkedLabor.ID,
                IsFormula = false,
            };

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = $"TestAssembly",
                Variables = new List<AssemblyVariable>()
                {
                    varPrice,
                    varArea,

                    parentLinkedAssembly,
                    parentLinkedMachine,
                    parentLinkedMaterial,
                    parentLinkedLabor
                }
            };

            ctx.AssemblyData.Add(linkedAssembly);
            ctx.MachineData.Add(linkedMachine);
            ctx.MaterialData.Add(linkedMaterial);
            ctx.LaborData.Add(linkedLabor);
            ctx.AssemblyData.Add(assemblyData);
            await ctx.SaveChangesAsync();

            string localDefinitionAssembly = await autocompleteEngine.LocalDefinition(assemblyID, ClassType.Assembly.ID());
            string classDefinitionAssembly = await autocompleteEngine.ClassDefinition(assemblyID, ClassType.Assembly.ID());

            Assert.IsNotNull(localDefinitionAssembly);
            Assert.IsNotNull(classDefinitionAssembly);
        }

        [TestMethod]
        public async Task GetAssemblyFile_Test()
        {
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);

            AssemblyVariable varPrice = new AssemblyVariable()
            {
                Name = "Price",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = assemblyID,
                DefaultValue = "=100",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varArea = new AssemblyVariable()
            {
                Name = "Area",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = assemblyID,
                DefaultValue = "2",
                DataType = DataType.Number,
                IsFormula = false,
            };

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = $"TestAssembly",
                Variables = new List<AssemblyVariable>()
                {
                    varPrice,
                    varArea
                }
            };

            ctx.AssemblyData.Add(assemblyData);
            await ctx.SaveChangesAsync();

            string assembly = await autocompleteEngine.ServerDefinition(assemblyID, ClassType.Assembly.ID());

            Assert.IsNotNull(assembly);
        }
    }
}
