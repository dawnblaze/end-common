﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Schema.Generation;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class JsonSchemaTests
    {
        #region RawJson
        private string _rawOrderPriceRequest =
@"{
	""Items"": [{
		""TaxNexusList"": null,
		""TaxInfoList"": null,
		""IsOutsourced"": false,
		""Quantity"": 1,
		""EngineType"": 1,
		""CompanyID"": 1378,
		""Components"": [{
			""ComponentType"": 12040,
			""ComponentID"": 1067,
			""Quantity"": 1,
			""QuantityOV"": false,
			""PriceUnit"": null,
			""PriceUnitOV"": false,
			""CostUnit"": null,
			""CostUnitOV"": false,
			""ChildComponents"": [],
			""TaxNexusList"": {},
			""TaxInfoList"": [],
			""CompanyID"": 1213,
            ""RollupLinkedPriceAndCost"": false,
			""Variables"": {
				""1"": {
					""Value"": ""Paper""
				}
			},
            ""IsVended"": false
		},
		{
			""ComponentType"": 12020,
			""ComponentID"": 1018,
			""Quantity"": 1,
			""QuantityOV"": false,
			""PriceUnit"": null,
			""PriceUnitOV"": false,
			""CostUnit"": null,
			""CostUnitOV"": false,
			""ChildComponents"": [],
			""TaxNexusList"": {},
			""TaxInfoList"": [],
			""CompanyID"": 1213,
			""Variables"": {},
            ""RollupLinkedPriceAndCost"": false,
            ""IsVended"": false
		}],
		""Surcharges"": []
	}],
	""Destinations"": null,
	""TaxNexusList"": null,
	""DiscountList"": null
}
";
        private string orderRequestWithComponentsAndVariables = @"
{
	""Items"": [{
	    ""TaxNexusList"": null,
	    ""TaxInfoList"": null,
	    ""IsOutsourced"": false,
	    ""Quantity"": 1,
	    ""EngineType"": 1,
	    ""CompanyID"": 0,
	    ""Components"": [{
		    ""ComponentType"": 12040,
		    ""ComponentID"": 1087,
		    ""Quantity"": 1,
		    ""QuantityOV"": false,
		    ""PriceUnit"": 0,
		    ""PriceUnitOV"": false,
			""CostUnit"": null,
			""CostUnitOV"": false,
		    ""Variables"": {
			    ""1072"": {
				    ""Value"": ""1""

                },
			    ""1073"": {
				    ""Value"": ""b""
			    },
			    ""1074"": {
				    ""Value"": ""true""
			    }
		    },
		    ""ChildComponents"": [],
		    ""TaxNexusList"": {
			
		    },
		    ""TaxInfoList"": [],
		    ""CompanyID"": 0,
            ""RollupLinkedPriceAndCost"": false,
            ""IsVended"": false
	    }],
	    ""Surcharges"": [{
		    ""SurchargeDefID"": 1000,
		    ""TaxabilityCode"": 1,
		    ""Quantity"": 1,
		    ""PriceFixedAmount"": 100,
		    ""PricePerUnitAmount"": 100,
		    ""PricePreTax"": 200,
		    ""PricePreTaxOV"": false,
		    ""TaxNexusList"": null,
		    ""TaxInfoList"": null,
		    ""CompanyID"": 0
	    }]
    }],
	""Destinations"": null,
	""TaxNexusList"": null,
	""DiscountList"": null
}";
#endregion RawJson

        [TestMethod]
        public void ValidateOrderPriceRequestJsonSchema()
        {
            JSchema jsonSchema = new JSchemaGenerator().Generate(typeof(OrderPriceRequest));
            JsonTextReader reader = new JsonTextReader(new StringReader(_rawOrderPriceRequest));

            JSchemaValidatingReader validatingReader = new JSchemaValidatingReader(reader) { Schema = jsonSchema };

            JsonSerializer serializer = new JsonSerializer();
            OrderPriceRequest orderPriceRequest = serializer.Deserialize<OrderPriceRequest>(validatingReader);
            Assert.IsNotNull(orderPriceRequest);
            Assert.IsNotNull(orderPriceRequest.Items);
            Assert.IsNotNull(orderPriceRequest.Items);

            Dictionary<string, VariableValue> values = new Dictionary<string, VariableValue>()
            {
                {"1", new VariableValue(){ Value="Paper" } }
            };
            ComponentPriceRequest req = new ComponentPriceRequest()
            {
                ComponentType = (OrderItemComponentType)12040,
                ComponentID = 1067,
                TotalQuantity = 1,
                TotalQuantityOV = false,
                PriceUnit = null,
                PriceUnitOV = false,
                ChildComponents = new List<ComponentPriceRequest>(),
                TaxNexusList = new Dictionary<string, TaxAssessmentNexus>(),
                TaxInfoList = new List<TaxAssessmentRequest>(),
                CompanyID = 1213,
                Variables = values
            };
            string reqJSON = JsonConvert.SerializeObject(req);
            Assert.IsNotNull(reqJSON);
        }

        [TestMethod]
        public void ValidateOrderPriceRequestAndResultWithComponentsAndVariablesJsonSchema()
        {
            JSchema requestJSONSchema = new JSchemaGenerator().Generate(typeof(OrderPriceRequest));
            JsonTextReader reader = new JsonTextReader(new StringReader(orderRequestWithComponentsAndVariables));
            JSchemaValidatingReader validatingReader = new JSchemaValidatingReader(reader) { Schema = requestJSONSchema };

            JsonSerializer serializer = new JsonSerializer();
            OrderPriceRequest orderPriceRequest = serializer.Deserialize<OrderPriceRequest>(validatingReader);
            Assert.IsNotNull(orderPriceRequest);

            OrderPriceResult result = new OrderPriceResult()
            {
                Items = orderPriceRequest.Items.Select(x =>
                {
                    return new ItemPriceResult()
                    {
                        Components = x.Components.Select(y =>
                        {
                            return new ComponentPriceResult()
                            {
                                ComponentType = y.ComponentType,
                                Variables = y.Variables
                            };
                        }).ToList()
                    };
                }).ToList()
            };

            string jsonResult = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(jsonResult);

            JsonTextWriter writer = new JsonTextWriter(new StringWriter());
            JSchemaValidatingWriter validatingWriter = new JSchemaValidatingWriter(writer) { Schema = new JSchemaGenerator().Generate(typeof(OrderPriceResult)) };

            serializer.Serialize(validatingWriter, result);
            Assert.IsNotNull(validatingWriter.ToString());
        }
    }
}
