﻿using System;
using Endor.CBEL.Common;
using Endor.CBEL.Compute;
using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.CBEL.Elements;
using Endor.Logging.Client;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class TablePricingTests
    {
        private ApiContext ctx = null;
        private PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        private const short BID = 1;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestMethod]
        public async Task AssemblyWithCustomPriceFromTable()
        {
            short bid = 1;
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            decimal lineItemQuantityValue = Convert.ToDecimal(5);
            AssemblyVariable varPrice = new AssemblyVariable()
            {
                Name = "Price",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "=Quantity.Value * CustomPrice.Value",
                //DefaultValue = "=MarginTable.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varQuantity = new AssemblyVariable()
            {
                Name = "Quantity",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "=LineItemQuantity * QuantityPerItem.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varQuantityPerItem = new AssemblyVariable()
            {
                Name = "QuantityPerItem",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varTableColumn = new AssemblyVariable()
            {
                Name = "TableColumn",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Number,
            };

            AssemblyVariable varTableRow = new AssemblyVariable()
            {
                Name = "TableRow",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Number,
            };

            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                VariableName = "CustomPrice",
                CellDataType = DataType.Number,
                Label = "CustomPrice",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                RowVariableID = varTableRow.ID,
                ColumnVariableID = varTableColumn.ID,
                AssemblyID = assemblyID,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.5},{\"index\":2,\"Value\":50.5}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.4},{\"index\":2,\"Value\":50.6}]",
                CellDataJSON = "[[45.5,4,3],[90,,2],[,9,]]"
            };

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = $"AssemblyWithTable",
                HasTierTable = true,
                Variables = new List<AssemblyVariable>()
                {
                    varQuantityPerItem,
                    varQuantity,
                    varPrice,
                    varTableColumn,
                    varTableRow
                },
                Tables = new List<AssemblyTable>()
                {
                    table
                },
            };

            ctx.AssemblyData.Add(assemblyData);
            await ctx.SaveChangesAsync();

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(ctx, assemblyData);
            ICBELAssembly cbelAssembly = await gen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelAssembly);


            CBELOverriddenValues ovValues = CBELAssemblyHelper.ComponentPriceRequestToComputeRequest(
                new ComponentPriceRequest()
                {
                    LineItemQuantity = lineItemQuantityValue,
                });

            ICBELComputeResult result = cbelAssembly.Compute(ovValues);


            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            IVariableData priceData = result.VariableData.FirstOrDefault(x => x.VariableName == "Price");
            Assert.IsNotNull(priceData);
            Assert.AreEqual(227.5m, decimal.Parse(priceData.Value));
        }


        [TestMethod]
        public async Task AssemblyWithTierTables()
        {
            short bid = 1;
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            decimal lineItemQuantityValue = Convert.ToDecimal(5);
            AssemblyVariable varPrice = new AssemblyVariable()
            {
                Name = "Price",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "=Quantity.Value * TierPriceTable",
                //DefaultValue = "=MarginTable.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varQuantity = new AssemblyVariable()
            {
                Name = "Quantity",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "=LineItemQuantity * QuantityPerItem.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varQuantityPerItem = new AssemblyVariable()
            {
                Name = "QuantityPerItem",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varTier = new AssemblyVariable
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                ClassTypeID = 12046,
                ModifiedDT = DateTime.Now,
                Name = "Tier",
                DefaultValue = "=CompanyTier();",
                DataType = DataType.String,
                IsFormula = true,
                UnitID = null,
                ListDataType = null,
                ListValuesJSON = null,
                AllowCustomValue = null,
                AllowMultiSelect = null,
                GroupOptionsByCategory = null,
                Label = "Tier",
                LabelType = null,
                AltText = null,
                ElementUseCount = 0,
                Tooltip = null,
                AllowDecimals = null,
                DisplayType = null,
                DecimalPlaces = null,
                SystemVariableID = 2,
                IsRequired = true,
                IsDisabled = false,
                ElementType = AssemblyElementType.SingleLineLabel,
                AssemblyID = assemblyID,
                TempID = null,
                LinkedMaterialID = null,
                LinkedLaborID = null,
                LinkedMachineID = null,
                LinkedAssemblyID = null,
                ConsumptionDefaultValue = null,
                IsConsumptionFormula = false,
                Formulas = null
            };

            AssemblyVariable varTableColumn = new AssemblyVariable()
            {
                Name = "TableColumn",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Number,
            };

            AssemblyVariable varTableRow = new AssemblyVariable()
            {
                Name = "TableRow",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Number,
            };

            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                VariableName = "TierTable",
                CellDataType = DataType.Number,
                Label = "CustomPrice",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = varTableColumn.ID,
                AssemblyID = assemblyID,
                IsTierTable = true,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.5},{\"index\":2,\"Value\":50.5}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.4},{\"index\":2,\"Value\":50.6}]",
                CellDataJSON = "[[45.5,4,3],[90,,2],[,9,]]"
            };


            AssemblyTable tierDiscountTable = new AssemblyTable()
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                VariableName = "TierDiscountTable",
                CellDataType = DataType.Number,
                Label = "CustomPrice",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = varTableColumn.ID,
                AssemblyID = assemblyID,
                IsTierTable = true,
                TableType = AssemblyTableType.PercentageDiscount,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.5},{\"index\":2,\"Value\":50.5}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.4},{\"index\":2,\"Value\":50.6}]",
                CellDataJSON = "[[45.5,4,3],[90,,2],[,9,]]"
            };


            AssemblyTable tierMarginTable = new AssemblyTable()
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                VariableName = "TierMarginTable",
                CellDataType = DataType.Number,
                Label = "CustomPrice",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = varTableColumn.ID,
                AssemblyID = assemblyID,
                IsTierTable = true,
                TableType = AssemblyTableType.Margin,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.5},{\"index\":2,\"Value\":50.5}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.4},{\"index\":2,\"Value\":50.6}]",
                CellDataJSON = "[[45.5,4,3],[90,,2],[,9,]]"
            };

            AssemblyTable tierMarkupTable = new AssemblyTable()
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                VariableName = "TierMarkupTable",
                CellDataType = DataType.Number,
                Label = "CustomPrice",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = varTableColumn.ID,
                AssemblyID = assemblyID,
                IsTierTable = true,
                TableType = AssemblyTableType.Multiplier,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.5},{\"index\":2,\"Value\":50.5}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.4},{\"index\":2,\"Value\":50.6}]",
                CellDataJSON = "[[45.5,4,3],[90,,2],[,9,]]"
            };

            AssemblyTable tierPriceTable = new AssemblyTable()
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                VariableName = "TierPriceTable",
                CellDataType = DataType.Number,
                Label = "CustomPrice",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = varTableColumn.ID,
                AssemblyID = assemblyID,
                IsTierTable = true,
                TableType = AssemblyTableType.UnitPrice,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.5},{\"index\":2,\"Value\":50.5}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.4},{\"index\":2,\"Value\":50.6}]",
                CellDataJSON = "[[45.5,4,3],[90,,2],[,9,]]"
            };

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = $"AssemblyWithTable",
                HasTierTable = true,
                PriceFormulaType = PriceFormulaType.PriceTable,
                FixedPrice = 3.5m,
                Variables = new List<AssemblyVariable>()
                {
                    varQuantityPerItem,
                    varQuantity,
                    varPrice,
                    varTableColumn,
                    varTableRow,
                    varTier
                },
                Tables = new List<AssemblyTable>()
                {
                    table,
                    tierDiscountTable,
                    tierMarginTable,
                    tierMarkupTable,
                    tierPriceTable
                },
            };

            ctx.AssemblyData.Add(assemblyData);
            await ctx.SaveChangesAsync();

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(ctx, assemblyData);
            ICBELAssembly cbelAssembly = await gen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelAssembly);


            CBELOverriddenValues ovValues = CBELAssemblyHelper.ComponentPriceRequestToComputeRequest(
                new ComponentPriceRequest()
                {
                    LineItemQuantity = lineItemQuantityValue,
                });

            ICBELComputeResult result = cbelAssembly.Compute(ovValues);


            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            IVariableData priceData = result.VariableData.FirstOrDefault(x => x.VariableName == "Price");
            Assert.IsNotNull(priceData);
            Assert.AreEqual(227.5m, decimal.Parse(priceData.Value));
        }

        [TestMethod]
        public async Task AssemblyWithPriceFromTable()
        {
            short bid = 1;
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);

            AssemblyVariable varPrice = new AssemblyVariable()
            {
                Name = "Price",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "=PriceTable.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varTableColumn = new AssemblyVariable()
            {
                Name = "TableColumn",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Number,
            };

            AssemblyVariable varTableRow = new AssemblyVariable()
            {
                Name = "TableRow",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Number,
            };

            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                VariableName = "PriceTable",
                CellDataType = DataType.Number,
                Label = "PriceTable",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                RowVariableID = varTableRow.ID,
                ColumnVariableID = varTableColumn.ID,
                AssemblyID = assemblyID,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.5},{\"index\":2,\"Value\":50.5}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":1,\"Value\":10.4},{\"index\":2,\"Value\":50.6}]",
                CellDataJSON = "[[45.5,4,3],[90,,2],[,9,]]"
            };

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = $"AssemblyWithTable",
                Variables = new List<AssemblyVariable>()
                {
                    varPrice,
                    varTableColumn,
                    varTableRow
                },
                Tables = new List<AssemblyTable>()
                {
                    table
                },
            };

            ctx.AssemblyData.Add(assemblyData);
            await ctx.SaveChangesAsync();

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(ctx, assemblyData);
            ICBELAssembly cbelAssembly = await gen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelAssembly);

            ICBELComputeResult result = cbelAssembly.Compute();

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            IVariableData priceData = result.VariableData.FirstOrDefault(x => x.VariableName == "Price");
            Assert.IsNotNull(priceData);
            Assert.AreEqual(45.5m, decimal.Parse(priceData.Value));
        }

        [TestMethod]
        [DataRow(false, false, 0.00)]
        [DataRow(true, false, 1.00)]
        [DataRow(false, true, 2.00)]
        [DataRow(true, true, 3.00)]
        public async Task AssemblyWithTableWithBooleanVariables(bool rowValue, bool colValue, double _expectedPrice)
        {
            decimal expectedPrice = (decimal)_expectedPrice;

            short bid = 1;
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);

            AssemblyVariable varPrice = new AssemblyVariable()
            {
                Name = "Price",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "=PriceTable.Value",
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable varTableColumn = new AssemblyVariable()
            {
                Name = "TableColumn",
                ElementType = AssemblyElementType.Checkbox,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Boolean,
            };

            AssemblyVariable varTableRow = new AssemblyVariable()
            {
                Name = "TableRow",
                ElementType = AssemblyElementType.Checkbox,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = "1",
                DataType = DataType.Boolean,
            };

            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                VariableName = "PriceTable",
                CellDataType = DataType.Number,
                Label = "PriceTable",
                RowDataType = DataType.Boolean,
                ColumnDataType = DataType.Boolean,
                RowVariableID = varTableRow.ID,
                ColumnVariableID = varTableColumn.ID,
                AssemblyID = assemblyID,
                RowValuesJSON = "[{\"index\":0,\"value\":0},{\"index\":1,\"value\":1}]",
                ColumnValuesJSON = "[{\"index\":0,\"value\":0},{\"index\":1,\"value\":1}]",
                CellDataJSON = "[[0,2],[1,3]]"
            };

            AssemblyData assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = $"AssemblyWithTable",
                Variables = new List<AssemblyVariable>()
                {
                    varPrice,
                    varTableColumn,
                    varTableRow
                },
                Tables = new List<AssemblyTable>()
                {
                    table
                },
            };

            ctx.AssemblyData.Add(assemblyData);
            await ctx.SaveChangesAsync();

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(ctx, assemblyData);
            ICBELAssembly cbelAssembly = await gen.GetAssemblyInstance(ctx, cache, logger);

            Assert.AreEqual(0, gen.Errors?.Count ?? 0);

            Assert.IsNotNull(cbelAssembly);

            CBELOverriddenValues ovValues = new CBELOverriddenValues()
            {
                ComponentID = assemblyID,
                Quantity = 1,
                ParentQuantity = 1,
                QuantityOV = true,
                VariableData = new List<IVariableData>()
                    {
                        new VariableData()
                        {
                            VariableName = "TableRow",
                            IsOV = true,
                            Value = rowValue.ToString(),
                        },
                        new VariableData()
                        {
                            VariableName = "TableColumn",
                            IsOV = true,
                            Value = colValue.ToString(),
                        }
                    },
            };

            ICBELComputeResult result = cbelAssembly.Compute(ovValues);

            Assert.IsNotNull(result);

            ICBELAssembly myAssembly = result.Assembly;
            Assert.IsNotNull(myAssembly);

            IVariableData priceData = result.VariableData.FirstOrDefault(x => x.VariableName == "Price");
            Assert.IsNotNull(priceData);
            Assert.AreEqual(expectedPrice, decimal.Parse(priceData.Value));
        }

        /// <summary>
        /// to understand better
        /// variable names, and values are based on 
        /// https://web.microsoftstream.com/video/4504f1ae-c9dd-44a3-87d8-437404840144?referrer=https:%2F%2Fcorebridge.atlassian.net%2Fsecure%2FRapidBoard.jspa%3FrapidView%3D101&projectKey=END&modal=detail&selectedIssue=END-11536
        /// </summary>
        [TestMethod]
        [DataRow("1", "1", AssemblyTableMatchType.ExactMatch, "1:1")]
        [DataRow("1", "2", AssemblyTableMatchType.ExactMatch, "1:2")]
        [DataRow("1", "3", AssemblyTableMatchType.ExactMatch, "1:3")]
        [DataRow("1", "4", AssemblyTableMatchType.ExactMatch, "1:4")]
        [DataRow("1", "5", AssemblyTableMatchType.ExactMatch, "1:5")]
        [DataRow("2", "1", AssemblyTableMatchType.ExactMatch, null)]
        [DataRow("2", "2", AssemblyTableMatchType.ExactMatch, null)]
        [DataRow("2", "3", AssemblyTableMatchType.ExactMatch, null)]
        [DataRow("2", "4", AssemblyTableMatchType.ExactMatch, null)]
        [DataRow("2", "5", AssemblyTableMatchType.ExactMatch, null)]
        [DataRow("3", "1", AssemblyTableMatchType.ExactMatch, "3:1")]
        [DataRow("3", "2", AssemblyTableMatchType.ExactMatch, "3:2")]
        [DataRow("3", "3", AssemblyTableMatchType.ExactMatch, "3:3")]
        [DataRow("3", "4", AssemblyTableMatchType.ExactMatch, "3:4")]
        [DataRow("3", "5", AssemblyTableMatchType.ExactMatch, "3:5")]
        // interpolation does not work on string so it's always a null
        [DataRow("1", "1", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("1", "2", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("1", "3", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("1", "4", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("1", "5", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("2", "1", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("2", "2", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("2", "3", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("2", "4", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("2", "5", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("3", "1", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("3", "2", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("3", "3", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("3", "4", AssemblyTableMatchType.Interpolate, null)]
        [DataRow("3", "5", AssemblyTableMatchType.Interpolate, null)]
        //
        [DataRow("1", "1", AssemblyTableMatchType.UseLowerValue, "1:1")]
        [DataRow("1", "2", AssemblyTableMatchType.UseLowerValue, "1:2")]
        [DataRow("1", "3", AssemblyTableMatchType.UseLowerValue, "1:3")]
        [DataRow("1", "4", AssemblyTableMatchType.UseLowerValue, "1:4")]
        [DataRow("1", "5", AssemblyTableMatchType.UseLowerValue, "1:5")]
        [DataRow("2", "1", AssemblyTableMatchType.UseLowerValue, "1:1")]
        [DataRow("2", "2", AssemblyTableMatchType.UseLowerValue, "1:2")]
        [DataRow("2", "3", AssemblyTableMatchType.UseLowerValue, "1:3")]
        [DataRow("2", "4", AssemblyTableMatchType.UseLowerValue, "1:4")]
        [DataRow("2", "5", AssemblyTableMatchType.UseLowerValue, "1:5")]
        [DataRow("3", "1", AssemblyTableMatchType.UseLowerValue, "3:1")]
        [DataRow("3", "2", AssemblyTableMatchType.UseLowerValue, "3:2")]
        [DataRow("3", "3", AssemblyTableMatchType.UseLowerValue, "3:3")]
        [DataRow("3", "4", AssemblyTableMatchType.UseLowerValue, "3:4")]
        [DataRow("3", "5", AssemblyTableMatchType.UseLowerValue, "3:5")]
        //
        [DataRow("1", "1", AssemblyTableMatchType.UseHigherValue, "1:1")]
        [DataRow("1", "2", AssemblyTableMatchType.UseHigherValue, "1:2")]
        [DataRow("1", "3", AssemblyTableMatchType.UseHigherValue, "1:3")]
        [DataRow("1", "4", AssemblyTableMatchType.UseHigherValue, "1:4")]
        [DataRow("1", "5", AssemblyTableMatchType.UseHigherValue, "1:5")]
        [DataRow("2", "1", AssemblyTableMatchType.UseHigherValue, "3:1")]
        [DataRow("2", "2", AssemblyTableMatchType.UseHigherValue, "3:2")]
        [DataRow("2", "3", AssemblyTableMatchType.UseHigherValue, "3:3")]
        [DataRow("2", "4", AssemblyTableMatchType.UseHigherValue, "3:4")]
        [DataRow("2", "5", AssemblyTableMatchType.UseHigherValue, "3:5")]
        [DataRow("3", "1", AssemblyTableMatchType.UseHigherValue, "3:1")]
        [DataRow("3", "2", AssemblyTableMatchType.UseHigherValue, "3:2")]
        [DataRow("3", "3", AssemblyTableMatchType.UseHigherValue, "3:3")]
        [DataRow("3", "4", AssemblyTableMatchType.UseHigherValue, "3:4")]
        [DataRow("3", "5", AssemblyTableMatchType.UseHigherValue, "3:5")]
        public async Task Table_MatchType_Test(string row, string column, AssemblyTableMatchType matchType, string expectedResult)
        {
            short bid = 1;
            int id = -100;
            string variableName = "MyNumberTableTest";

            var assemblyData = new AssemblyData() { BID = bid, ID = id, Name = $"MyLittleTable" };
            assemblyData.Variables = new List<AssemblyVariable>();
            assemblyData.Tables = new List<AssemblyTable>();

            AssemblyVariable rowVariable = new AssemblyVariable()
            {
                Name = "TotalQuantity",
                ElementType = AssemblyElementType.Number,
                ID = id,
                BID = bid,
                AssemblyID = id,
                DefaultValue = row,
                DataType = DataType.Number,
                IsFormula = true,
            };

            AssemblyVariable columnVariable = new AssemblyVariable()
            {
                Name = "TableHValue",
                ElementType = AssemblyElementType.Number,
                ID = id - 1,
                BID = bid,
                AssemblyID = id,
                DefaultValue = column,
                DataType = DataType.Number,
                IsFormula = false,
            };

            assemblyData.Variables.Add(rowVariable);
            assemblyData.Variables.Add(columnVariable);

            AssemblyTable table = new AssemblyTable()
            {
                BID = bid,
                ID = (short)id,
                VariableName = variableName,
                CellDataType = DataType.String,
                Label = variableName,
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                RowVariableID = rowVariable.ID,
                ColumnVariableID = columnVariable.ID,
                ColumnMatchType = matchType,
                RowMatchType = matchType,
                AssemblyID = id,
                RowValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":5}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[\"1:1\",\"1:2\",\"1:3\",\"1:4\",\"1:5\"]," +
                    "[\"3:1\",\"3:2\",\"3:3\",\"3:4\",\"3:5\"]," +
                    "[\"5:1\",\"5:2\",\"5:3\",\"5:4\",\"5:5\"]]"
            };
            assemblyData.Tables.Add(table);

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Success);
            Assert.IsNull(result.Errors);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(null, cache, logger);
            Assert.IsNotNull(assembly);

            ICBELVariable assemblyElement = assembly.Variables[variableName];
            Assert.IsNotNull(assemblyElement);
            Assert.IsInstanceOfType(assemblyElement, typeof(StringDataTable<decimal?, decimal?>));

            StringDataTable<decimal?, decimal?> numberDataTable = assemblyElement as StringDataTable<decimal?, decimal?>;

            var currentResult = numberDataTable.TableLookup();
            Assert.AreEqual(expectedResult, currentResult);
        }

        /// <summary>
        /// to understand better
        /// variable names, and values are based on 
        /// https://web.microsoftstream.com/video/4504f1ae-c9dd-44a3-87d8-437404840144?referrer=https:%2F%2Fcorebridge.atlassian.net%2Fsecure%2FRapidBoard.jspa%3FrapidView%3D101&projectKey=END&modal=detail&selectedIssue=END-11536
        /// </summary>
        [TestMethod]
        [DataRow("1", AssemblyTableMatchType.ExactMatch, "0")]
        [DataRow("2", AssemblyTableMatchType.ExactMatch, "2")]
        [DataRow("3", AssemblyTableMatchType.ExactMatch, "4")]
        [DataRow("4", AssemblyTableMatchType.ExactMatch, "6")]
        [DataRow("5", AssemblyTableMatchType.ExactMatch, "8")]
        public async Task Table_MarginBasedPricing_Test(string column, AssemblyTableMatchType matchType, string expectedResult)
        {
            short bid = 1;
            int assemblyID = -100;

            var assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = $"MyLittleTable",
                PricingType = AssemblyPricingType.MarginBasedPricing
            };
            assemblyData.Variables = new List<AssemblyVariable>();
            assemblyData.Tables = new List<AssemblyTable>();

            AssemblyVariable columnVariable = new AssemblyVariable()
            {
                Name = "TotalQuantity",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = column,
                DataType = DataType.Number,
                IsFormula = false,
            };

            AssemblyVariable rowVariable = new AssemblyVariable
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                ClassTypeID = 12046,
                ModifiedDT = DateTime.Now,
                Name = "Tier",
                DefaultValue = "=CompanyTier();",
                DataType = DataType.String,
                IsFormula = true,
                UnitID = null,
                ListDataType = null,
                ListValuesJSON = null,
                AllowCustomValue = null,
                AllowMultiSelect = null,
                GroupOptionsByCategory = null,
                Label = "Tier",
                LabelType = null,
                AltText = null,
                ElementUseCount = 0,
                Tooltip = null,
                AllowDecimals = null,
                DisplayType = null,
                DecimalPlaces = null,
                SystemVariableID = 2,
                IsRequired = true,
                IsDisabled = false,
                ElementType = AssemblyElementType.SingleLineLabel,
                AssemblyID = assemblyID,
                TempID = null,
                LinkedMaterialID = null,
                LinkedLaborID = null,
                LinkedMachineID = null,
                LinkedAssemblyID = null,
                ConsumptionDefaultValue = null,
                IsConsumptionFormula = false,
                Formulas = null
            };

            assemblyData.Variables.Add(rowVariable);
            assemblyData.Variables.Add(columnVariable);

            AssemblyTable tierMargintable = new AssemblyTable()
            {
                BID = bid,
                ID = (short)assemblyID,
                VariableName = "TierMarginTable",
                CellDataType = DataType.Number,
                Label = "TierMarginTable",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = columnVariable.ID,
                ColumnMatchType = matchType,
                RowMatchType = matchType,
                AssemblyID = assemblyID,
                TableType = AssemblyTableType.Margin,
                IsTierTable = true,
                RowValuesJSON = "[{\"index\":0,\"Value\":\"1\",\"Name\":\"Default Tier\"}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[0,2,4,6,8]]"
            };

            assemblyData.Tables.Add(tierMargintable);

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Success);
            Assert.IsNull(result.Errors);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(ctx, cache, logger);
            Assert.IsNotNull(assembly);

            Assert.AreEqual(expectedResult, assembly.TierMarginTable.ToString());
        }

        /// <summary>
        /// to understand better
        /// variable names, and values are based on 
        /// https://web.microsoftstream.com/video/4504f1ae-c9dd-44a3-87d8-437404840144?referrer=https:%2F%2Fcorebridge.atlassian.net%2Fsecure%2FRapidBoard.jspa%3FrapidView%3D101&projectKey=END&modal=detail&selectedIssue=END-11536
        /// </summary>
        [TestMethod]
        [DataRow("1", AssemblyTableMatchType.ExactMatch, "0")]
        [DataRow("2", AssemblyTableMatchType.ExactMatch, "2")]
        [DataRow("3", AssemblyTableMatchType.ExactMatch, "4")]
        [DataRow("4", AssemblyTableMatchType.ExactMatch, "6")]
        [DataRow("5", AssemblyTableMatchType.ExactMatch, "8")]
        public async Task Table_TierPricing_Test(string column, AssemblyTableMatchType matchType, string expectedResult)
        {
            short bid = 1;
            int assemblyID = -100;

            var assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = $"MyLittleTable",
                PricingType = AssemblyPricingType.MarketBasedPricing
            };
            assemblyData.Variables = new List<AssemblyVariable>();
            assemblyData.Tables = new List<AssemblyTable>();

            AssemblyVariable columnVariable = new AssemblyVariable()
            {
                Name = "TotalQuantity",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = column,
                DataType = DataType.Number,
                IsFormula = false,
            };

            AssemblyVariable rowVariable = new AssemblyVariable
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                ClassTypeID = 12046,
                ModifiedDT = DateTime.Now,
                Name = "Tier",
                DefaultValue = "=CompanyTier();",
                DataType = DataType.String,
                IsFormula = true,
                UnitID = null,
                ListDataType = null,
                ListValuesJSON = null,
                AllowCustomValue = null,
                AllowMultiSelect = null,
                GroupOptionsByCategory = null,
                Label = "Tier",
                LabelType = null,
                AltText = null,
                ElementUseCount = 0,
                Tooltip = null,
                AllowDecimals = null,
                DisplayType = null,
                DecimalPlaces = null,
                SystemVariableID = 2,
                IsRequired = true,
                IsDisabled = false,
                ElementType = AssemblyElementType.SingleLineLabel,
                AssemblyID = assemblyID,
                TempID = null,
                LinkedMaterialID = null,
                LinkedLaborID = null,
                LinkedMachineID = null,
                LinkedAssemblyID = null,
                ConsumptionDefaultValue = null,
                IsConsumptionFormula = false,
                Formulas = null
            };

            assemblyData.Variables.Add(rowVariable);
            assemblyData.Variables.Add(columnVariable);

            AssemblyTable tierPriceTable = new AssemblyTable()
            {
                BID = bid,
                ID = (short)assemblyID,
                VariableName = "TierPriceTable",
                CellDataType = DataType.Number,
                Label = "TierPriceTable",
                RowDataType = DataType.Number,
                ColumnDataType = DataType.Number,
                ColumnVariableID = columnVariable.ID,
                RowMatchType = matchType,
                ColumnMatchType = matchType,
                AssemblyID = assemblyID,
                IsTierTable = true,
                TableType = AssemblyTableType.PercentageDiscount,
                RowValuesJSON = "[{\"index\":0,\"Value\":\"1\",\"Name\":\"Default Tier\"}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[0,2,4,6,8]]"
            };

            assemblyData.Tables.Add(tierPriceTable);

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Success);
            Assert.IsNull(result.Errors);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(ctx, cache, logger);
            Assert.IsNotNull(assembly);

            Assert.AreEqual(expectedResult, assembly.TierPriceTable.ToString());
        }

        /// <summary>
        /// to understand better
        /// variable names, and values are based on 
        /// https://web.microsoftstream.com/video/4504f1ae-c9dd-44a3-87d8-437404840144?referrer=https:%2F%2Fcorebridge.atlassian.net%2Fsecure%2FRapidBoard.jspa%3FrapidView%3D101&projectKey=END&modal=detail&selectedIssue=END-11536
        /// </summary>
        [TestMethod]
        [DataRow("1", AssemblyTableMatchType.ExactMatch, "0")]
        [DataRow("2", AssemblyTableMatchType.ExactMatch, "2")]
        [DataRow("3", AssemblyTableMatchType.ExactMatch, "4")]
        [DataRow("4", AssemblyTableMatchType.ExactMatch, "6")]
        [DataRow("5", AssemblyTableMatchType.ExactMatch, "8")]
        public async Task Table_MarketBasedPricing_Test(string column, AssemblyTableMatchType matchType, string expectedResult)
        {
            short bid = 1;
            int assemblyID = -100;

            var assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = $"MyLittleTable",
                PricingType = AssemblyPricingType.MarketBasedPricing
            };
            assemblyData.Variables = new List<AssemblyVariable>();
            assemblyData.Tables = new List<AssemblyTable>();

            AssemblyVariable columnVariable = new AssemblyVariable()
            {
                Name = "TotalQuantity",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = column,
                DataType = DataType.Number,
                IsFormula = false,
            };

            AssemblyVariable rowVariable = new AssemblyVariable
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                ClassTypeID = 12046,
                ModifiedDT = DateTime.Now,
                Name = "Tier",
                DefaultValue = "=CompanyTier();",
                DataType = DataType.String,
                IsFormula = true,
                UnitID = null,
                ListDataType = null,
                ListValuesJSON = null,
                AllowCustomValue = null,
                AllowMultiSelect = null,
                GroupOptionsByCategory = null,
                Label = "Tier",
                LabelType = null,
                AltText = null,
                ElementUseCount = 0,
                Tooltip = null,
                AllowDecimals = null,
                DisplayType = null,
                DecimalPlaces = null,
                SystemVariableID = 2,
                IsRequired = true,
                IsDisabled = false,
                ElementType = AssemblyElementType.SingleLineLabel,
                AssemblyID = assemblyID,
                TempID = null,
                LinkedMaterialID = null,
                LinkedLaborID = null,
                LinkedMachineID = null,
                LinkedAssemblyID = null,
                ConsumptionDefaultValue = null,
                IsConsumptionFormula = false,
                Formulas = null
            };

            assemblyData.Variables.Add(rowVariable);
            assemblyData.Variables.Add(columnVariable);

            AssemblyTable tierPercentagetable = new AssemblyTable()
            {
                BID = bid,
                ID = (short)assemblyID,
                VariableName = "TierDiscountTable",
                CellDataType = DataType.Number,
                Label = "TierPercentageTable",
                RowDataType = DataType.String,
                ColumnDataType = DataType.Number,
                ColumnVariableID = columnVariable.ID,
                ColumnMatchType = matchType,
                RowMatchType = matchType,
                AssemblyID = assemblyID,
                IsTierTable = true,
                TableType = AssemblyTableType.PercentageDiscount,
                RowValuesJSON = "[{\"index\":0,\"Value\":\"1\",\"Name\":\"Default Tier\"}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[0,2,4,6,8]]"
            };

            assemblyData.Tables.Add(tierPercentagetable);

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Success);
            Assert.IsNull(result.Errors);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(ctx, cache, logger);
            Assert.IsNotNull(assembly);

            Assert.AreEqual(expectedResult, assembly.TierDiscountTable.ToString());
        }

        /// <summary>
        /// to understand better
        /// variable names, and values are based on 
        /// https://web.microsoftstream.com/video/4504f1ae-c9dd-44a3-87d8-437404840144?referrer=https:%2F%2Fcorebridge.atlassian.net%2Fsecure%2FRapidBoard.jspa%3FrapidView%3D101&projectKey=END&modal=detail&selectedIssue=END-11536
        /// </summary>
        [TestMethod]
        [DataRow("1", AssemblyTableMatchType.ExactMatch, "0")]
        [DataRow("2", AssemblyTableMatchType.ExactMatch, "2")]
        [DataRow("3", AssemblyTableMatchType.ExactMatch, "4")]
        [DataRow("4", AssemblyTableMatchType.ExactMatch, "6")]
        [DataRow("5", AssemblyTableMatchType.ExactMatch, "8")]
        public async Task Table_MarkupBasedPricing_Test(string column, AssemblyTableMatchType matchType, string expectedResult)
        {
            short bid = 1;
            int assemblyID = -100;

            var assemblyData = new AssemblyData()
            {
                BID = bid,
                ID = assemblyID,
                Name = $"MyLittleTable",
                PricingType = AssemblyPricingType.MarkupBasedPricing
            };
            assemblyData.Variables = new List<AssemblyVariable>();
            assemblyData.Tables = new List<AssemblyTable>();

            AssemblyVariable columnVariable = new AssemblyVariable()
            {
                Name = "TotalQuantity",
                ElementType = AssemblyElementType.Number,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = bid,
                AssemblyID = assemblyID,
                DefaultValue = column,
                DataType = DataType.Number,
                IsFormula = false,
            };

            AssemblyVariable rowVariable = new AssemblyVariable
            {
                BID = bid,
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                ClassTypeID = 12046,
                ModifiedDT = DateTime.Now,
                Name = "Tier",
                DefaultValue = "=CompanyTier();",
                DataType = DataType.String,
                IsFormula = true,
                UnitID = null,
                ListDataType = null,
                ListValuesJSON = null,
                AllowCustomValue = null,
                AllowMultiSelect = null,
                GroupOptionsByCategory = null,
                Label = "Tier",
                LabelType = null,
                AltText = null,
                ElementUseCount = 0,
                Tooltip = null,
                AllowDecimals = null,
                DisplayType = null,
                DecimalPlaces = null,
                SystemVariableID = 2,
                IsRequired = true,
                IsDisabled = false,
                ElementType = AssemblyElementType.SingleLineLabel,
                AssemblyID = assemblyID,
                TempID = null,
                LinkedMaterialID = null,
                LinkedLaborID = null,
                LinkedMachineID = null,
                LinkedAssemblyID = null,
                ConsumptionDefaultValue = null,
                IsConsumptionFormula = false,
                Formulas = null
            };

            assemblyData.Variables.Add(rowVariable);
            assemblyData.Variables.Add(columnVariable);

            AssemblyTable tierMultipliertable = new AssemblyTable()
            {
                BID = bid,
                ID = (short)assemblyID,
                VariableName = "TierMarkupTable",
                CellDataType = DataType.Number,
                Label = "TierMarkupTable",
                RowDataType = DataType.String,
                ColumnDataType = DataType.Number,
                ColumnVariableID = columnVariable.ID,
                ColumnMatchType = matchType,
                RowMatchType = matchType,
                AssemblyID = assemblyID,
                IsTierTable = true,
                TableType = AssemblyTableType.UnitPrice,
                RowValuesJSON = "[{\"index\":0,\"Value\":\"1\",\"Name\":\"Default Tier\"}]",
                ColumnValuesJSON = "[{\"index\":0,\"Value\":1},{\"index\":0,\"Value\":2},{\"index\":0,\"Value\":3},{\"index\":0,\"Value\":4},{\"index\":0,\"Value\":5}]",
                CellDataJSON = "[[0,2,4,6,8]]"
            };

            assemblyData.Tables.Add(tierMultipliertable);

            CBELAssemblyGenerator gen = CBELAssemblyGenerator.Create(null, assemblyData);
            AssemblyCompilationResponse result = await gen.TestAssemblyCompilation();
            Assert.IsNotNull(result);
            Assert.AreEqual(true, result.Success);
            Assert.IsNull(result.Errors);

            ICBELAssembly assembly = await gen.GetAssemblyInstance(ctx, cache, logger);
            Assert.IsNotNull(assembly);

            Assert.AreEqual(expectedResult, assembly.TierMarkupTable.ToString());
        }
    }
}
