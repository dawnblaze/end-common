﻿using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.CBEL.Compute;
using Endor.CBEL.Elements;
using Endor.CBEL.Exceptions;
using Endor.CBEL.Grammar;
using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Endor.Units;
using Irony.Parsing;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class PricingEngineTest
    {
        #region Initialization and Cleanup

        protected ApiContext ctx = null;
        protected PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        protected int glAccountID = 0;
        private short taxCodeID = 0;
        private decimal PriceList = 0m;

        protected const short BID = 1;
        protected const int _companyID = -99;
        protected const int _taxGroupID = -99;

        protected MaterialData material = null;
        protected const int _materialID = -99;
        protected const decimal _materialPrice = 5m;
        protected const decimal _materialCost = 2.5m;

        protected const int _machineID = -99;
        protected const int _destinationID = -99;
        
        protected LaborData labor = null;
        protected const int _laborID = -99;
        protected const decimal _laborPriceFixed = 15m;
        protected const decimal _laborPriceHourly = 25m;
        protected const decimal _laborCostFixed = 7.5m;
        protected const decimal _laborCostHourly = 12.5m;

        private SurchargeDef surcharge = null;
        private const short _surchargeID = -99;
        private const decimal _surchargeFixedFee = 10m;
        private const decimal _surchargeUnitFee = 0m;

        protected const int _assemblyID = -99;

        protected AssemblyVariable assemblyVariable = null;
        protected const int _assemblyVariableID = -99;

        protected OrderData order = null;
        protected const int _orderID = -99;
        protected OrderItemData orderItem = null;
        protected const int _orderItemID = -99;
        protected OrderItemSurcharge orderItemSurcharge = null;
        protected const int _orderItemSurchargeID = -99;
        protected const int _laborMinimumBillableMinutes = 5;
        protected const int _laborIntervalBillableMinutes = 5;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            await CleanupDefaultPricingTests();
            await InitializeCompanyAndGLAccountIDs();
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            try
            {
                var _assemblyTables = ctx.AssemblyTable.Where(t => t.BID == BID && (t.ID < 0 || t.AssemblyID < 0));
                if (_assemblyTables.Any())
                {
                    ctx.AssemblyTable.RemoveRange(_assemblyTables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }
                var _assemblyVariableFormulas = ctx.AssemblyVariableFormula.Where(t => t.BID == BID && (t.ID < 0 || t.VariableID < 0));
                if (_assemblyVariableFormulas.Any())
                {
                    ctx.AssemblyVariableFormula.RemoveRange(_assemblyVariableFormulas);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }
                var _assemblyVariables = ctx.AssemblyVariable.Where(t => t.BID == BID && (t.ID < 0 || t.AssemblyID < 0 || t.LinkedMaterialID < 0));
                if (_assemblyVariables.Any())
                {
                    ctx.AssemblyVariable.RemoveRange(_assemblyVariables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _orderItemComponents = ctx.OrderItemComponent.Where(t => t.BID == BID && (t.AssemblyID < 0 || t.ID < 0));
                if (_orderItemComponents.Any())
                {
                    ctx.OrderItemComponent.RemoveRange(_orderItemComponents);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _machineProfileTables = ctx.MachineProfileTable.Where(t => t.BID == BID && (t.ID < 0 || t.ProfileID < 0 || t.TableID < 0));
                if (_machineProfileTables.Count() > 0)
                {
                    ctx.MachineProfileTable.RemoveRange(_machineProfileTables);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _machineProfiles = ctx.MachineProfile.Where(t => t.BID == BID && (t.ID < 0 || t.MachineID < 0 || t.MachineTemplateID < 0));
                if (_machineProfiles.Count() > 0)
                {
                    ctx.MachineProfile.RemoveRange(_machineProfiles);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _assembly = await ctx.AssemblyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_assembly != null)
                {
                    ITenantDataCache cache = new TestHelper.MockTenantDataCache();
                    EntityStorageClient client = new EntityStorageClient((await cache.Get(BID)).StorageConnectionString, BID);
                    var assemblyDoc = new DMID() { id = _assembly.ID, ctid = ClassType.Assembly.ID() };
                    string assemblyName = CBELAssemblyHelper.AssemblyName(BID, _assembly.ID, _assembly.ClassTypeID, 1);

                    await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                    await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
                    ctx.MachineData.RemoveRange(ctx.MachineData.Where(t => t.BID == BID && t.MachineTemplateID == _assembly.ID));
                    ctx.MachineProfile.RemoveRange(ctx.MachineProfile.Where(t => t.BID == BID && t.MachineTemplateID == _assembly.ID));
                    ctx.AssemblyData.Remove(_assembly);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _partMaterialCategoryLinks = ctx.MaterialCategoryLink.Where(t => t.BID == BID && t.PartID < 0);
                if (_partMaterialCategoryLinks.Any())
                {
                    ctx.MaterialCategoryLink.RemoveRange(_partMaterialCategoryLinks);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _material1 = await ctx.MaterialData.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_material1 != null)
                {
                    ctx.MaterialData.Remove(_material1);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _orderItemSurcharges = ctx.OrderItemSurcharge.Where(t => t.BID == BID && (t.ID == _orderItemSurchargeID || t.SurchargeDefID == _surchargeID));
                if (_orderItemSurcharges.Any())
                {
                    ctx.OrderItemSurcharge.RemoveRange(_orderItemSurcharges);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _orderItem = await ctx.OrderItemData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemID);
                if (_orderItem != null)
                {
                    ctx.OrderItemData.Remove(_orderItem);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == _orderID).ToArrayAsync());
                ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == _orderID).ToArrayAsync());

                var _order = await ctx.OrderData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderID);
                if (_order != null)
                {
                    ctx.OrderData.Remove(_order);
                await RemoveTransheaderItems(_order.ID);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify removal
                }

                var _surcharge = await ctx.SurchargeDef.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_surcharge != null)
                {
                    ctx.SurchargeDef.RemoveRange(_surcharge);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID < 0);
                if (_taxGroup != null)
                {
                    ctx.TaxGroup.Remove(_taxGroup);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
                if (_company != null)
                {
                    var _orders = await ctx.OrderData.Where(t => t.BID == BID && t.CompanyID == _companyID).ToListAsync();
                    foreach (var o in _orders)
                    {

                        ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == o.ID && t.IsOrderNote));
                        var items = ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == o.ID);
                        var itemIDs = items.Select(x => x.ID);
                        ctx.OrderItemData.RemoveRange(items);
                        ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(t => t.BID == BID && itemIDs.Contains(t.OrderItemID)));
                        ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                        ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                        ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());
                        ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == o.ID).ToArrayAsync());
                        ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == o.ID).ToArrayAsync());

                        ctx.OrderData.Remove(o);
                        await RemoveTransheaderItems(o.ID);
                    }

                    var _estimates = await ctx.EstimateData.Where(t => t.BID == BID && t.CompanyID == _companyID).ToListAsync();
                    foreach (var e in _estimates)
                    {
                        ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderID == e.ID));
                        var items = ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == e.ID);
                        var itemIDs = items.Select(x => x.ID);
                        ctx.OrderItemData.RemoveRange(items);
                        ctx.OrderItemSurcharge.RemoveRange(ctx.OrderItemSurcharge.Where(t => t.BID == BID && itemIDs.Contains(t.OrderItemID)));
                        ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                        ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                        ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                        ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == BID && x.Parent.Order.ID == e.ID).ToArrayAsync());
                        ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == BID && x.Order.ID == e.ID).ToArrayAsync());
                        ctx.EstimateData.Remove(e);
                        await RemoveTransheaderItems(e.ID);
                    }

                    ctx.CompanyData.Remove(_company);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }

                var _labor = ctx.LaborData.Where(t => t.BID == BID && t.ID < 0);
                if (_labor.Count() > 0)
                {
                    ctx.LaborData.RemoveRange(_labor);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }


                var _machine = ctx.MachineData.Where(t => t.BID == BID && t.ID < 0);
                if (_machine.Count() > 0)
                {
                    ctx.MachineData.RemoveRange(_machine);
                    Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify delete
                }
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        private async Task RemoveTransheaderItems(int orderID)
        {
            var items = await ctx.OrderItemData.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync();
            foreach (var orderItem in items)
            {
                ctx.OrderNote.RemoveRange(ctx.OrderNote.Where(t => t.BID == BID && t.OrderItemID == orderItem.ID));
                ctx.OrderItemData.Remove(orderItem);
                foreach (var component in await ctx.OrderItemComponent.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
                {
                    ctx.OrderItemComponent.Remove(component);
                }
            }
            foreach (var role in await ctx.OrderEmployeeRole.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderEmployeeRole.Remove(role);
            }
            foreach (var keydate in await ctx.OrderKeyDate.Where(t => t.BID == BID && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderKeyDate.Remove(keydate);
            }

        }

        /// <summary>
        /// Initializes the Company and GLAccount IDs
        /// </summary>
        /// <returns></returns>
        protected async Task InitializeCompanyAndGLAccountIDs()
        {
            // check for existing company
            var company = await ctx.CompanyData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _companyID);
            if (company == null)
            {
                company = new CompanyData()
                {
                    BID = BID,
                    ID = _companyID,
                    Name = "Test Pricing Company",
                    LocationID = 1,
                    IsAdHoc = true,
                    StatusID = 1
                };

                ctx.CompanyData.Add(company);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            var taxGroup = await ctx.TaxGroup.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _taxGroupID);
            if (taxGroup == null)
            {
                taxGroup = new TaxGroup()
                {
                    BID = BID,
                    ID = _taxGroupID,
                    Name = "Test Pricing TaxGroup"
                };

                ctx.TaxGroup.Add(taxGroup);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0);
            }

            // there should always be at least one GLAccount and TaxCode
            glAccountID = ctx.GLAccount.FirstOrDefault(t => t.BID == BID).ID;
            taxCodeID = ctx.TaxabilityCodes.FirstOrDefault(t => t.BID == BID).ID;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="variableDefaultDictionary"></param>
        /// <param name="elementType"></param>
        /// <param name="altText"></param>
        /// <param name="doBeforeSave"></param>
        /// <returns>AssemblyData</returns>
        protected async Task<AssemblyData> CreateAssembly(
            int ID = _assemblyID, 
            Dictionary<string, string> variableDefaultDictionary = null, 
            AssemblyElementType elementType = AssemblyElementType.SingleLineText,
            string altText = null,
            Action<AssemblyData> doBeforeSave = null)
        {
            int newRecords = 0;
            AssemblyData assembly = await ctx.AssemblyData.Include(x => x.Variables).FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (assembly != null)
            {
                if ((assembly.Variables?.Count ?? 0) > 0)
                {
                    ctx.RemoveRange(assembly.Variables);
                }
                ctx.Remove(assembly);
            }

            newRecords++;
            assembly = new AssemblyData()
            {
                ID = ID,
                BID = BID,
                Name = "Test Assembly For Pricing",
                IncomeAccountID = glAccountID,

            };

            ctx.AssemblyData.Add(assembly);

            if (variableDefaultDictionary != null && variableDefaultDictionary.Count > 0)
            {
                int lowVariableID = -99;

                var variables = ctx.AssemblyVariable.Where(t => t.BID == BID);
                if (variables.Any())
                {
                    lowVariableID = variables.Min(t => t.ID);

                    if (lowVariableID > -99)
                        lowVariableID = -99;
                }

                assembly.Variables = new List<AssemblyVariable>();

                foreach (var variableDefault in variableDefaultDictionary)
                {
                    newRecords++;
                    lowVariableID--;
                    var newVariable = new AssemblyVariable()
                    {
                        ID = lowVariableID,
                        BID = BID,
                        AssemblyID = ID,
                        ElementType = elementType,
                        IsRequired = false,
                        Name = variableDefault.Key,
                        DefaultValue = variableDefault.Value,
                        IsFormula = (!string.IsNullOrWhiteSpace(variableDefault.Value) && variableDefault.Value.TrimStart().First() == '='),
                        AltText = altText,
                        IsAltTextFormula = (!string.IsNullOrWhiteSpace(altText) && altText.TrimStart().First() == '=')
                    };

                    ctx.AssemblyVariable.Add(newVariable);
                }
            }

            Assert.AreEqual(newRecords, await ctx.SaveChangesAsync()); // verify add

            return assembly;
        }

        /// <summary>
        /// Creates a new Assembly Variable for testing
        /// </summary>
        /// <param name="isMaterial">When true then Material, when false then Labor</param>
        /// <param name="ID">Assembly Variable ID (default -99)</param>
        /// <param name="dobeforeSave">dobeforeSave</param>
        /// <returns></returns>
        protected async Task<AssemblyVariable> CreateAssemblyVariable(bool isMaterial,
            int assemblyID = _assemblyID,
            string consumptionDefaultValue = "=4*2",
            Action<AssemblyVariable> dobeforeSave = null)
        {
            int ID = this.ctx.AssemblyVariable.GetTestID(BID);

            assemblyVariable = await ctx.AssemblyVariable.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (assemblyVariable != null)
                ctx.Remove(assemblyVariable);

            assemblyVariable = new AssemblyVariable()
            {
                Name = "TestLinked",
                DefaultValue = "TestLinked",
                BID = BID,
                AssemblyID = assemblyID,
                ID = ID,
                ConsumptionDefaultValue = consumptionDefaultValue,
                IsConsumptionFormula = true,
            };

            if (isMaterial)
            {
                assemblyVariable.Name += "Material";
                assemblyVariable.DefaultValue += "Material";
                assemblyVariable.LinkedMaterialID = material.ID;
                assemblyVariable.ElementType = AssemblyElementType.LinkedMaterial;
                assemblyVariable.DataType = DataType.String;
                assemblyVariable.ListDataType = DataType.MaterialPart;
            }
            else
            {
                assemblyVariable.Name += "Labor";
                assemblyVariable.DefaultValue += "Labor";
                assemblyVariable.LinkedLaborID = labor.ID;
                assemblyVariable.ElementType = AssemblyElementType.LinkedLabor;
                assemblyVariable.DataType = DataType.String;
                assemblyVariable.ListDataType = DataType.LaborPart;
            }

            if(dobeforeSave != null)
            {
                dobeforeSave(assemblyVariable);
            }

            ctx.AssemblyVariable.Add(assemblyVariable);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add

            return assemblyVariable;
        }

        /// <summary>
        /// Creates a new Material for testing
        /// </summary>
        /// <param name="estimatingPrice">EstimatingPrice to use (default 5m)</param>
        /// <param name="estimatingCost">EstimatingCost to use (default 2.5m)</param>
        /// <param name="ID">MaterialID (default -99)</param>
        /// <param name="doBeforeSave">doBeforeSave</param>
        /// <returns></returns>
        protected async Task<MaterialData> CreateMaterial(decimal estimatingPrice = _materialPrice, 
            decimal estimatingCost = _materialCost, 
            int ID = _materialID, 
            string materialName = "Test Material For Pricing",
            Action<MaterialData> doBeforeSave = null)
        {
            material = await ctx.MaterialData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (material != null)
            {
                ctx.Remove(material);
            }
            material = new MaterialData()
            {
                ID = ID,
                BID = BID,
                Name = materialName,
                InvoiceText = materialName,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
                EstimatingPrice = estimatingPrice,
                EstimatingCost = estimatingCost,
                Length = new Measurement(1, Unit.Inch),
                Width = new Measurement(1, Unit.Inch),
                Height = new Measurement(1, Unit.Inch),
                Weight = new Measurement(1, Unit.Inch),
            };

            if(doBeforeSave != null)
            {
                doBeforeSave(material);
            }

            ctx.MaterialData.Add(material);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add

            return material;
        }


        protected async Task<DestinationData> CreateDestination(short ID = _destinationID,
            int assemblyID = _assemblyID,
            string destinationName = "Test Destination For Pricing",
            Action<DestinationData> doBeforeSave = null,
            Action<AssemblyVariable> doBeforeSaveVariable = null)
        {
            var destination = await ctx.DestinationData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (destination != null)
            {
                ctx.Remove(destination);
            }
            destination = new DestinationData()
            {
                ID = ID,
                BID = BID,
                Name = destinationName,
                ActiveProfileCount = 1
            };
            if (doBeforeSave != null)
            {
                doBeforeSave(destination);
            }
            ctx.DestinationData.Add(destination);

            var assemblyVariable = new AssemblyVariable()
            {
                Name = destinationName,
                DefaultValue = destinationName,
                BID = BID,
                AssemblyID = assemblyID,
                ID = ID,
                ConsumptionDefaultValue = "=4*2",
                IsConsumptionFormula = true,
                LinkedMachineID = destination.ID,
                ElementType = AssemblyElementType.LinkedMachine,
                DataType = DataType.String,
                ListDataType = DataType.MachinePart,
            };
            if (doBeforeSaveVariable != null)
            {
                doBeforeSaveVariable(assemblyVariable);
            }
            ctx.AssemblyVariable.Add(assemblyVariable);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            return destination;
        }

        protected async Task<MachineData> CreateMachine(short ID = _machineID, 
            int assemblyID = _assemblyID, 
            string machineName = "Test Machine For Pricing",
            Action<MachineData> doBeforeSave = null,
            Action<AssemblyVariable> doBeforeSaveVariable = null)
        {
            var machine = await ctx.MachineData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (machine != null)
            {
                ctx.Remove(machine);
            }
            machine = new MachineData()
            {
                ID = ID,
                BID = BID,
                Name = machineName,
                ActiveProfileCount = 1,
                ActiveInstanceCount = 2,
            };
            if(doBeforeSave != null)
            {
                doBeforeSave(machine);
            }
            ctx.MachineData.Add(machine);

            var assemblyVariable = new AssemblyVariable()
            {
                Name = machineName,
                DefaultValue = machineName,
                BID = BID,
                AssemblyID = assemblyID,
                ID = ID,
                ConsumptionDefaultValue = "=4*2",
                IsConsumptionFormula = true,
                LinkedMachineID = machine.ID,
                ElementType = AssemblyElementType.LinkedMachine,
                DataType = DataType.String,
                ListDataType = DataType.MachinePart,
            };
            if(doBeforeSaveVariable != null)
            {
                doBeforeSaveVariable(assemblyVariable);
            }
            ctx.AssemblyVariable.Add(assemblyVariable);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            return machine;
        }

        /// <summary>
        /// Updates an existing Material for testing
        /// </summary>
        /// <param name="estimatingPrice">EstimatingPrice to use (default 5m)</param>
        /// <param name="estimatingCost">EstimatingCost to use (default 2.5m)</param>
        /// <param name="ID">MaterialID (default -99)</param>
        /// <returns></returns>
        protected async Task UpdateMaterial(decimal estimatingPrice = _materialPrice, decimal estimatingCost = _materialCost, int ID = _materialID, string materialName = "Test Material For Pricing")
        {
            material = await ctx.MaterialData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);

            Assert.IsNotNull(material);

            material.Name = materialName;
            material.InvoiceText = materialName;
            material.ExpenseAccountID = glAccountID;
            material.IncomeAccountID = glAccountID;
            material.InventoryAccountID = glAccountID;
            material.EstimatingPrice = estimatingPrice;
            material.EstimatingCost = estimatingCost;

            Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
        }

        /// <summary>
        /// Creates a new Labor for testing
        /// </summary>
        /// <param name="priceFixed">EstimatingPriceFixed to use (default 15m)</param>
        /// <param name="priceHourly">EstimatingPriceHourly to use (default 25m)</param>
        /// <param name="costFixed">EstimatingCostFixed to use (default 7.5m)</param>
        /// <param name="costHourly">EstimatingCostHourly to use (default 12.5m)</param>
        /// <param name="ID">LaborID (default -99)</param>
        /// <returns></returns>
        protected async Task<LaborData> CreateLabor(decimal priceFixed = _laborPriceFixed, decimal priceHourly = _laborPriceHourly, decimal costFixed = _laborCostFixed, decimal costHourly = _laborCostHourly, int ID = _laborID, string laborName = "Test Labor For Pricing")
        {
            labor = await ctx.LaborData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (labor != null)
                ctx.Remove(labor);

            labor = new LaborData()
            {
                ID = ID,
                BID = BID,
                Name = laborName,
                InvoiceText = laborName,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                EstimatingPriceFixed = priceFixed,
                EstimatingPricePerHour = priceHourly,
                EstimatingCostFixed = costFixed,
                EstimatingCostPerHour = costHourly,
                MinimumTimeInMin = _laborMinimumBillableMinutes,
                BillingIncrementInMin = _laborIntervalBillableMinutes
            };

            ctx.LaborData.Add(labor);
            Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add

            return labor;
        }

        /// <summary>
        /// Updates a new Labor for testing
        /// </summary>
        /// <param name="priceFixed">EstimatingPriceFixed to use (default 15m)</param>
        /// <param name="priceHourly">EstimatingPriceHourly to use (default 25m)</param>
        /// <param name="costFixed">EstimatingCostFixed to use (default 7.5m)</param>
        /// <param name="costHourly">EstimatingCostHourly to use (default 12.5m)</param>
        /// <param name="ID">LaborID (default -99)</param>
        /// <returns></returns>
        protected async Task UpdateLabor(decimal priceFixed = _laborPriceFixed, decimal priceHourly = _laborPriceHourly, decimal costFixed = _laborCostFixed, decimal costHourly = _laborCostHourly, short ID = _surchargeID, string laborName = "Test Labor For Pricing")
        {
            labor = await ctx.LaborData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);

            Assert.IsNotNull(labor);

            if (labor != null)
            {
                labor.Name = laborName;
                labor.InvoiceText = laborName;
                labor.ExpenseAccountID = glAccountID;
                labor.IncomeAccountID = glAccountID;
                labor.EstimatingPriceFixed = priceFixed;
                labor.EstimatingPricePerHour = priceHourly;
                labor.EstimatingCostFixed = costFixed;
                labor.EstimatingCostPerHour = costHourly;
                labor.MinimumTimeInMin = 5;
                labor.BillingIncrementInMin = 5;

                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }
        }

        /// <summary>
        /// Creates a new SurchargeDef for testing
        /// </summary>
        /// <param name="fixedFee">DefaultFixedFee to use (default 10m)</param>
        /// <param name="unitFee">DefaultPerUnitFee to use (default 0m)</param>
        /// <param name="ID">SurchargeDefID (default -99)</param>
        /// <returns></returns>
        protected async Task CreateSurchargeDef(decimal fixedFee = _surchargeFixedFee, decimal unitFee = _surchargeUnitFee, short ID = _surchargeID)
        {
            surcharge = await ctx.SurchargeDef.FirstOrDefaultAsync(t => t.BID == BID && t.ID == ID);
            if (surcharge == null)
            {
                surcharge = new SurchargeDef()
                {
                    BID = BID,
                    ID = ID,
                    IncomeAccountID = glAccountID,
                    CompanyID = _companyID,
                    Name = "Test Surcharge For Pricing",
                    DefaultFixedFee = fixedFee,
                    DefaultPerUnitFee = unitFee,
                    TaxCodeID = taxCodeID,
                };

                ctx.SurchargeDef.Add(surcharge);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }
        }

        /// <summary>
        /// Creates an Order, OrderItem, (SurchargeDef if not already made) and OrderItemSurcharge
        /// </summary>
        /// <param name="orderID">Order ID (default -99)</param>
        /// <param name="orderItemID">OrderItem ID (default -99)</param>
        /// <param name="orderItemSurchargeID">OrderItemSurcharge ID (default -99)</param>
        /// <param name="surchargeDefID">SurchargeDef ID (default -99)</param>
        /// <param name="surchargeFixedFee">SurchargeDef DefaultFixedFee to use (default 10m)</param>
        /// <param name="surchargeUnitFee">SurchargeDef DefaultPerUnitFee to use (default 0m)</param>
        /// <returns></returns>
        protected async Task CreateTestOrderData(int orderID = _orderID, int orderItemID = _orderItemID, int orderItemSurchargeID = _orderItemSurchargeID, short surchargeDefID = _surchargeID, decimal surchargeFixedFee = _surchargeFixedFee, decimal surchargeUnitFee = _surchargeUnitFee)
        {
            if (surcharge == null)
                await CreateSurchargeDef(surchargeFixedFee, surchargeUnitFee, surchargeDefID);

            var locationID = (await ctx.LocationData.FirstOrDefaultAsync(t => t.BID == BID)).ID;

            order = await ctx.OrderData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderID);
            if (order == null)
            {
                order = new OrderData()
                {
                    BID = BID,
                    ID = _orderID,
                    CompanyID = _companyID,
                    TransactionType = (byte)OrderTransactionType.Order,
                    FormattedNumber = "",
                    ProductionLocationID = locationID,
                    LocationID = locationID,
                    PickupLocationID = locationID,
                    OrderStatusID = OrderOrderStatus.OrderWIP,
                    TaxGroupID = _taxGroupID,
                    IsTaxExempt = false,
                    TaxExemptReasonID = null
                };

                ctx.OrderData.Add(order);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }

            orderItem = await ctx.OrderItemData.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemID);
            if (orderItem == null)
            {
                orderItem = new OrderItemData()
                {
                    BID = BID,
                    OrderID = _orderID,
                    ID = _orderItemID,
                    Name = "Test OrderItem For Pricing",
                    OrderStatusID = OrderOrderStatus.OrderWIP,
                    TransactionType = (byte)OrderTransactionType.Order,
                    ItemStatusID = (await ctx.OrderItemStatus.FirstOrDefaultAsync(t => t.BID == BID)).ID,
                    TaxGroupID = _taxGroupID,
                    ProductionLocationID = locationID
                };

                ctx.OrderItemData.Add(orderItem);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }

            orderItemSurcharge = await ctx.OrderItemSurcharge.FirstOrDefaultAsync(t => t.BID == BID && t.ID == _orderItemSurchargeID);
            if (orderItemSurcharge == null)
            {
                orderItemSurcharge = new OrderItemSurcharge()
                {
                    BID = BID,
                    OrderItemID = _orderItemID,
                    ID = _orderItemSurchargeID,
                    Name = "Test OrderItemSurcharge For Pricing",
                    SurchargeDefID = surchargeDefID,
                    DefaultFixedFee = surchargeFixedFee,
                    DefaultPerUnitFee = surchargeUnitFee,
                    IncomeAccountID = glAccountID,
                    TaxCodeID = taxCodeID,
                };

                ctx.OrderItemSurcharge.Add(orderItemSurcharge);
                Assert.IsTrue(await ctx.SaveChangesAsync() > 0); // verify add
            }
        }

        #endregion

        [TestMethod]
        public void SerializeVariableInfo()
        {

            var variable = new VariableValue();
            variable.ValueOV = true;
            variable.ValueAsDecimal = 24m;
            variable.Value = "24";
            /*var variable2 = new VariableValue();
            variable2.ValueOV = true;
            variable2.ValueAsDecimal = 36m;
            variable2.Type = TypeCode.Decimal;
            variable2.Value = "36";
            var Variables = new Dictionary<string, VariableValue>() { { "Height", variable }, { "Width", variable2 } };*/
            //this.TestSerialization(variable, "ValueAsInt", "ValueAsDecimal", "ValueAsBool");

            OrderPriceRequest testRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                   new ItemPriceRequest()
                   {
                       EngineType = PricingEngineType.Supplied,
                       //PriceNet = 100m,
                       //Variables = new Dictionary<string, VariableValue>()
                       //{
                       //    { "Height", new VariableValue() { ValueAsDecimal = 24m } },
                       //    { "Width", new VariableValue() { ValueAsDecimal = 36m } },
                       //}
                   },
                   new ItemPriceRequest()
                   {
                       EngineType = PricingEngineType.Supplied,
                       //PriceNet = 50m,
                   }
               }
            };
            string jsontest = JsonConvert.SerializeObject(variable);
            string json = JsonConvert.SerializeObject(testRequest);
            int q = 0;
            var test = JsonConvert.DeserializeObject<OrderPriceRequest>(json);
            q++;
        }

        #region Supplied PricingEngine

        [TestMethod]
        public async Task TestComputeOrderItemWithSupplied()
        {
            var orderItemRequest = new ItemPriceRequest
            {
                EngineType = PricingEngineType.Supplied,
                Quantity = 5,
            };

            ItemPriceResult result = await pricingEngine.Compute(orderItemRequest, false);

        }

        #endregion

        #region SimplePart PricingEngine

        [TestMethod]
        public async Task TestComputeOrderWithItemComponentSimplePartPrice()
        {
            await CreateMaterial();
            await CreateLabor();

            decimal qty = 3;
            PriceList += decimal.Round(_materialPrice * qty, 2);
            PriceList += decimal.Round(_laborPriceFixed + (_laborPriceHourly * qty / 60), 2);

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _laborPriceHourly,
                },
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _materialPrice
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };
            OrderPriceRequest orderRequest = new OrderPriceRequest()
            {
                Items = new List<ItemPriceRequest>()
                {
                    itemPriceRequest
                }
            };

            OrderPriceResult result = await pricingEngine.Compute(orderRequest, false);
            Assert.IsNotNull(result);

            Assert.AreEqual(PriceList, result.PricePreTax);
            Assert.AreEqual(PriceList, result.PriceNet);

            Assert.AreEqual(orderRequest.Items.Count, result.Items.Count);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);
        }

        [TestMethod]
        public async Task TestComputeItemWithComponentSimplePartPrice()
        {
            await CreateMaterial();
            await CreateLabor();

            decimal qty = 3;
            PriceList += decimal.Round(_materialPrice * qty, 2);
            PriceList += decimal.Round(_laborPriceFixed + (_laborPriceHourly * qty / 60), 2);

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _laborPriceHourly,
                },
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _materialPrice
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);

            Assert.AreEqual(_companyID, result.CompanyID);
            Assert.AreEqual(PriceList, result.PriceList);

            Assert.IsFalse(result.PriceComponentOV);

            Assert.IsTrue(result.CostMaterial.HasValue);
            Assert.AreEqual(decimal.Round((_materialCost) * qty, 2), result.CostMaterial.Value);
            Assert.IsTrue(result.CostLabor.HasValue);
            Assert.AreEqual(decimal.Round(_laborCostFixed + (_laborCostHourly * qty / 60), 3), result.CostLabor.Value);
            Assert.IsFalse(result.PriceTotal.HasValue);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);
        }

        [TestMethod]
        public async Task TestComputeItemWithComponentSimplePartQuantityRevert()
        {
            int materialID;
            if (ctx.MaterialData.Any(x => x.BID == BID))
                materialID = ctx.MaterialData.Where(x => x.BID == BID).Select(x => x.ID).Min() - 1;
            else
                materialID = 1000;

            int laborID;

            if (ctx.LaborData.Any(x => x.BID == BID))
                laborID = ctx.LaborData.Where(x => x.BID == BID).Select(x => x.ID).Min() - 1;
            else
                laborID = 1000;

            await CreateMaterial(ID: materialID);
            LaborData labor = await CreateLabor(ID: laborID);

            decimal lineItemQty = 4;
            decimal componentOverriddenQty = 2;

            ComponentPriceRequest laborRequest = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Labor,
                ComponentID = laborID,
                TotalQuantity = componentOverriddenQty,
                TotalQuantityOV = true,
                QuantityUnit = Unit.Minute,
                PriceUnit = _laborPriceHourly,
            };
            ComponentPriceRequest materialRequest = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Material,
                ComponentID = materialID,
                TotalQuantity = componentOverriddenQty,
                TotalQuantityOV = true,
                PriceUnit = _materialPrice
            };
            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){ materialRequest, laborRequest };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = lineItemQty,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);

            var materialResult = result.Components.FirstOrDefault(x => x.ComponentID == materialID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(materialResult);
            Assert.AreEqual(componentOverriddenQty, materialResult.TotalQuantity);
            Assert.AreEqual(true, materialResult.TotalQuantityOV);
            var laborResult = result.Components.FirstOrDefault(x => x.ComponentID == laborID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(laborResult);
            Assert.AreEqual(componentOverriddenQty, laborResult.TotalQuantity);
            Assert.AreEqual(true, laborResult.TotalQuantityOV);

            materialRequest.TotalQuantityOV = false;
            laborRequest.TotalQuantityOV = false;
            ItemPriceResult result2 = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result2);

            materialResult = result2.Components.FirstOrDefault(x => x.ComponentID == materialID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(materialResult);
            Assert.AreEqual(lineItemQty, materialResult.TotalQuantity);
            Assert.AreEqual(false, materialResult.TotalQuantityOV);
            laborResult = result2.Components.FirstOrDefault(x => x.ComponentID == laborID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(laborResult);
            Assert.AreEqual(labor.MinimumTimeInMin, laborResult.TotalQuantity);
            Assert.AreEqual(false, laborResult.TotalQuantityOV);


            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);
        }

        [TestMethod]
        public async Task TestComputeItemWithComponentSimplePartPriceWithOV()
        {
            await CreateMaterial();
            await CreateLabor();

            decimal qty = 3;
            PriceList += decimal.Round(_materialPrice * qty, 2);
            PriceList += decimal.Round(_laborPriceFixed + (_laborPriceHourly * qty / 60), 2);

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _laborPriceHourly,

                },
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material.ID,
                    TotalQuantity = qty,
                    TotalQuantityOV = true,
                    PriceUnit = _materialPrice,
                    PriceUnitOV = true
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);

            Assert.AreEqual(_companyID, result.CompanyID);
            Assert.AreEqual(PriceList, result.PriceList);

            Assert.IsTrue(result.PriceComponentOV);

            Assert.IsTrue(result.CostMaterial.HasValue);
            Assert.AreEqual(decimal.Round((_materialCost) * qty, 2), result.CostMaterial.Value);
            Assert.IsTrue(result.CostLabor.HasValue);
            Assert.AreEqual(decimal.Round(_laborCostFixed + (_laborCostHourly * qty / 60), 3), result.CostLabor.Value);
            Assert.IsFalse(result.PriceTotal.HasValue);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);
        }

        [TestMethod]
        public async Task TestComputeItemWithSurchargeFixedOnlySimplePartPrice()
        {
            await CreateTestOrderData();

            decimal qty = 3;
            PriceList = _surchargeFixedFee;

            List<SurchargePriceRequest> surcharges = new List<SurchargePriceRequest>()
            {
                new SurchargePriceRequest()
                {
                    CompanyID = _companyID,
                    SurchargeDefID = _surchargeID,
                    Quantity = qty,
                    PriceFixedAmount = _surchargeFixedFee,
                    PricePerUnitAmount = _surchargeUnitFee
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Surcharges = surcharges
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);

            Assert.AreEqual(_companyID, result.CompanyID);
            Assert.AreEqual(PriceList, result.PriceList);

            Assert.IsFalse(result.PriceSurchargeOV);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);
        }

        [TestMethod]
        public async Task TestComputeItemWithSurchargeFixedAndPerUnitPriceAndQuantityPrice()
        {
            List<SurchargePriceRequest> surcharges = new List<SurchargePriceRequest>()
            {
                new SurchargePriceRequest()
                {
                    SurchargeDefID = -1,
                    Quantity = 1,
                    PriceFixedAmount = 7m,
                    PricePerUnitAmount = 7m
                },
                new SurchargePriceRequest()
                {
                    SurchargeDefID = -1,
                    Quantity = 5,
                    PriceFixedAmount = 7m,
                    PricePerUnitAmount = 7m
                },
                new SurchargePriceRequest()
                {
                    SurchargeDefID = -1,
                    Quantity = 1,
                    PriceFixedAmount = 4m,
                    PricePerUnitAmount = 4m
                },
                new SurchargePriceRequest()
                {
                    SurchargeDefID = -1,
                    Quantity = 5,
                    PriceFixedAmount = 4m,
                    PricePerUnitAmount = 4m
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Surcharges = surcharges
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result?.Surcharges);
            Assert.AreEqual(4, result.Surcharges.Count);

            Assert.AreEqual(14m, result.Surcharges[0].PricePreTax);
            Assert.AreEqual(42m, result.Surcharges[1].PricePreTax);
            Assert.AreEqual(8m, result.Surcharges[2].PricePreTax);
            Assert.AreEqual(24m, result.Surcharges[3].PricePreTax);
        }

        [TestMethod]
        public async Task TestComputeItemWithSurchargeFixedAndPerUnitPriceSimplePartPrice()
        {
            decimal fixedFeeOV = 5m;
            decimal unitFeeOV = 5m;

            await CreateTestOrderData(surchargeFixedFee: fixedFeeOV, surchargeUnitFee: unitFeeOV);

            decimal qty = 3;
            PriceList = fixedFeeOV + (unitFeeOV * qty);

            List<SurchargePriceRequest> surcharges = new List<SurchargePriceRequest>()
            {
                new SurchargePriceRequest()
                {
                    CompanyID = _companyID,
                    SurchargeDefID = _surchargeID,
                    Quantity = qty,
                    PriceFixedAmount = fixedFeeOV,
                    PricePerUnitAmount = unitFeeOV
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Surcharges = surcharges
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);

            Assert.AreEqual(_companyID, result.CompanyID);
            Assert.AreEqual(PriceList, result.PriceList);

            Assert.IsFalse(result.PriceSurchargeOV);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);
        }

        [TestMethod]
        public async Task TestComputeItemWithSurchargeOVSimplePartPrice()
        {
            decimal setFee = 100m;
            decimal qty = 3;
            PriceList = setFee;

            List<SurchargePriceRequest> surcharges = new List<SurchargePriceRequest>()
            {
                new SurchargePriceRequest()
                {
                    CompanyID = _companyID,
                    SurchargeDefID = _surchargeID,
                    Quantity = qty,
                    PriceFixedAmount = _surchargeFixedFee,
                    PricePerUnitAmount = _surchargeUnitFee,
                    PricePreTax = setFee,
                    PricePreTaxOV = true
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = qty,
                EngineType = PricingEngineType.SimplePart,
                Surcharges = surcharges
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);

            Assert.AreEqual(_companyID, result.CompanyID);
            Assert.AreEqual(PriceList, result.PriceList);

            Assert.IsTrue(result.PriceSurchargeOV);

            string serialized = JsonConvert.SerializeObject(result);
            Assert.IsNotNull(serialized);
        }

        [TestMethod]
        public async Task TestComputeItemWithErrors()
        {
            AssemblyData assembly = await CreateAssembly(
                variableDefaultDictionary: new Dictionary<string, string>()
                    {
                        { "FailVar", "=Blah" }
                    });

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Assembly,
                    ComponentID = assembly.ID,
                    TotalQuantity = 1,
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);
            Assert.IsFalse(result.Success);
            Assert.AreEqual(1, result.ErrorCount);
        }

        #endregion

        #region CustomFabrication PricingEngine

        // to do

        #endregion

        #region OnlineLookup PricingEngine

        // to do

        #endregion

        [TestMethod]
        public void ComponentPriceRequestHandlesAllValidPropertiesDuringSerializeAndDeserialize()
        {
            JsonSerializerSettings defaultSettings = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

            string componentPriceRequestSerialized =
@"{
    'ComponentType':'Material',
    'ComponentID':10,
    'Variables':{},
    'TaxNexusList':{},
    'TaxInfoList':[]    
}";
            ComponentPriceRequest deserializedRequest = JsonConvert.DeserializeObject<ComponentPriceRequest>(componentPriceRequestSerialized, defaultSettings);

            Assert.IsNull(deserializedRequest.TotalQuantity);
            Assert.IsNull(deserializedRequest.TotalQuantityOV);
            Assert.IsNull(deserializedRequest.PriceUnit);
            Assert.IsNull(deserializedRequest.PriceUnitOV);

            Assert.IsNotNull(deserializedRequest.Variables);
            Assert.IsNotNull(deserializedRequest.TaxNexusList);
            Assert.IsNotNull(deserializedRequest.TaxInfoList);

            Assert.IsNull(deserializedRequest.ChildComponents);
            Assert.IsNull(deserializedRequest.CompanyID);
        }

        [TestMethod]
        public void ComponentPriceResultHandlesAllValidPropertiesDuringSerializeAndDeserialize()
        {
            JsonSerializerSettings defaultSettings = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };

            string componentPriceResultSerialized =
@"{
    'ComponentType':'Material',
    'ComponentID':10,
    'Variables':{},
    'TaxNexusList':{},
    'TaxInfoList':[]    
}";
            ComponentPriceResult deserializedRequest = JsonConvert.DeserializeObject<ComponentPriceResult>(componentPriceResultSerialized, defaultSettings);

            Assert.IsNull(deserializedRequest.TotalQuantity);
            Assert.IsFalse(deserializedRequest.TotalQuantityOV);
            Assert.IsNull(deserializedRequest.PricePreTax);
            Assert.IsFalse(deserializedRequest.PriceUnitOV);
            Assert.IsNull(deserializedRequest.ChildComponents);
            Assert.IsNull(deserializedRequest.PriceTax);
            Assert.IsNull(deserializedRequest.PriceTotal);
            Assert.IsNull(deserializedRequest.CostMaterial);
            Assert.IsNull(deserializedRequest.CostLabor);
            Assert.IsNull(deserializedRequest.CostMachine);
            Assert.IsNull(deserializedRequest.CostNet);

            Assert.IsNotNull(deserializedRequest.TaxNexusList);
            Assert.IsNotNull(deserializedRequest.TaxInfoList);

            Assert.IsNull(deserializedRequest.CompanyID);
        }

        public async Task CompileAssembly(AssemblyData assembly)
        {
            var cache = new TestHelper.MockTenantDataCache();
            CBELAssemblyGenerator assemblyGenerator = CBELAssemblyGenerator.Create(this.ctx, assembly);
            await assemblyGenerator.Save(cache);
        }

        public async Task CompileMachine(MachineData mac)
        {
            var cache = new TestHelper.MockTenantDataCache();
            var macGenerator = CBELMachineGenerator.Create(this.ctx, mac);
            await macGenerator.Save(cache);
        }

        [TestMethod]
        public async Task TestCheckDLLOrCreateCBELAssemblyDLL()
        {
            short testBID = 1;
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            await CreateAssembly();

            //This test save the assembly to the file system and reloads it
            CBELAssemblyGenerator gen = CbelTests.GetCBELTestAssemblyGeneratorWithDefaultElements();
            await gen.Save(cache);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = _assemblyID,
                TotalQuantity = 1,
                //PriceUnit = _laborPriceHourly,
            };

            await pricingEngine.Compute(request, PricingEngineType.Computed);

            //Delete DLL - make sure it still works
            EntityStorageClient client = new EntityStorageClient((await cache.Get(testBID)).StorageConnectionString, testBID);

            var assemblyDoc = new DMID() { id = _assemblyID, ctid = ClassType.Assembly.ID() };
            await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{gen.AssemblyClassName}.dll");
            await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{gen.AssemblyClassName}.cs");

            ComponentPriceResult cpr = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(cpr);
            Assert.IsTrue(cpr.Variables.Count() > 0);
        }

        [TestMethod]
        public async Task TestCheckDLLFailsToCompileOnCompute()
        {
            Dictionary<string, string> varDefault = new Dictionary<string, string>()
            {
                {"SomeVariable", "=Blah"}
            };

            await CreateAssembly(variableDefaultDictionary: varDefault);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = _assemblyID,
                TotalQuantity = 1,
            };

            ComponentPriceResult cpr = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(cpr);
            Assert.IsNull(cpr.CostLabor);
            Assert.IsNull(cpr.CostMachine);
            Assert.IsNull(cpr.CostMaterial);
            Assert.IsNull(cpr.CostNet);
            Assert.IsNull(cpr.PricePreTax);
            Assert.IsNull(cpr.PriceTotal);
            Assert.IsNotNull(cpr.ErrorCount);
            Assert.IsFalse(cpr.Success);
            Assert.AreEqual(1, cpr.ErrorCount);
            Assert.IsNotNull(cpr.Errors);
            Assert.IsNotNull(cpr.ErrorCount);
        }

        [TestMethod]
        public async Task TestAssemblyLinkedMaterialSumsCorrectly()
        {
            decimal matCost = 10m;
            decimal matPrice = 20m;

            await CreateMaterial(matPrice, matCost, materialName: "TestLinkedMaterial");
            var assembly = await CreateAssembly();
            await CreateAssemblyVariable(true);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);

            Assert.AreEqual(8m * matCost, result.CostMaterial.Value);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.IsNull(result.PriceTotal);

            // Rename the material
            await UpdateMaterial(matPrice, matCost, materialName: "NotTestLinkedMaterial");

            result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);

            Assert.AreEqual(8m * matCost, result.CostMaterial.Value);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.IsNull(result.PriceTotal);
        }

        [TestMethod]
        public async Task TestAssemblyLinkedMaterialPropertiesAreLoaded()
        {
            decimal matCost = 10m;
            decimal matPrice = 20m;

            await CreateMaterial(matPrice, matCost, materialName: "TestLinkedMaterial");

            var assembly = await CreateAssembly();
            var assemblyVariable = new AssemblyVariable()
            {
                Name = "SomeVariable",
                DefaultValue = $"={material.Name}.Height.InInches",
                BID = BID,
                AssemblyID = assembly.ID,
                ID = -98,
                ElementType = AssemblyElementType.Number,
                DataType = DataType.Number,
                IsFormula = true,
            };
            ctx.AssemblyVariable.Add(assemblyVariable);
            ctx.SaveChanges();

            await CreateAssemblyVariable(true);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { "TestLinkedMaterial", new VariableValue() { DataType = DataType.String, Value = material.Name, ValueOV = true } }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.ErrorCount);
            Assert.AreEqual(material.Height.Value, result.Variables[assemblyVariable.Name].ValueAsDecimal);
        }

        [TestMethod]
        public async Task TestAssemblyWithNoLinkedSumsCorrectly()
        {
            var assembly = await CreateAssembly();

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {

                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);

            Assert.AreEqual(0m, result.CostMaterial.Value);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.IsNull(result.PriceTotal);
        }

        [TestMethod]
        public async Task TestAssemblyLinkedLaborSumsCorrectly()
        {
            var labor = await CreateLabor(laborName: "TestLinkedLabor");
            var assembly = await CreateAssembly();
            decimal laborConsumptionQtyMinutes = Math.Max(labor.MinimumTimeInMin.GetValueOrDefault(1), labor.BillingIncrementInMin.GetValueOrDefault(1));
            await CreateAssemblyVariable(false, consumptionDefaultValue: $"={laborConsumptionQtyMinutes}");

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);

            Assert.AreEqual(Math.Round(_laborCostHourly / 60m * laborConsumptionQtyMinutes + _laborCostFixed, 2), result.CostLabor.Value);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.IsNull(result.PriceTotal);

            // Rename Labor
            await UpdateLabor(laborName: "NotTestLinkedLabor");

            result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);

            Assert.AreEqual(Math.Round(_laborCostHourly / 60m * laborConsumptionQtyMinutes + _laborCostFixed, 2), result.CostLabor.Value);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.IsNull(result.PriceTotal);
        }

        [TestMethod]
        public async Task TestAssemblyLinkedLaborSumsCorrectlyMinimumBillable()
        {
            var labor = await CreateLabor(laborName: "TestLinkedLabor");
            Assert.IsNotNull(labor.MinimumTimeInMin);
            Assert.IsTrue(labor.MinimumTimeInMin > 1);
            // if we have a billing interval larger than 1, adding 1 to it should cause it to be rounded up to the next interval
            int laborQtyMinutes = labor.MinimumTimeInMin.Value - 1;
            // this is our expected "round up to"
            int expectedMinimumLaborQtyMinutes = labor.MinimumTimeInMin.Value;
            var assembly = await CreateAssembly();
            await CreateAssemblyVariable(false, consumptionDefaultValue: $"={laborQtyMinutes}");

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { assemblyVariable.Name, new VariableValue() { DataType = DataType.String, Value = assemblyVariable.Name, ValueOV = true } }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.ErrorCount);

            Assert.AreEqual((double)(_laborCostFixed + (expectedMinimumLaborQtyMinutes * _laborCostHourly / 60m)), (double)result.CostLabor.Value, 0.005);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.IsNull(result.PriceTotal);
        }

        [TestMethod]
        public async Task TestAssemblyLinkedLaborSumsCorrectlyRoundedBillable()
        {
            var labor = await CreateLabor(laborName: "TestLinkedLabor");
            Assert.IsNotNull(labor.BillingIncrementInMin);
            Assert.IsTrue(labor.BillingIncrementInMin > 1);
            // if we have a billing interval larger than 1, adding 1 to it should cause it to be rounded up to the next interval
            int laborQtyMinutes = labor.BillingIncrementInMin.Value + 1;
            // this is our expected "round up to"
            int expectedRoundedLaborQtyMinutes = labor.BillingIncrementInMin.Value * 2;

            var assembly = await CreateAssembly();
            await CreateAssemblyVariable(false, consumptionDefaultValue: $"={laborQtyMinutes}");

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { assemblyVariable.Name, new VariableValue() { DataType = DataType.String, Value = assemblyVariable.Name, ValueOV = true } }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);

            Assert.AreEqual((double)(_laborCostHourly / 60m * expectedRoundedLaborQtyMinutes + _laborCostFixed), (double)result.CostLabor.Value, 0.005);

            Assert.IsNull(result.PriceTotal);
        }

        [TestMethod]
        public async Task TestAssemblyLinkedLaborPropertiesAreLoaded()
        {
            await CreateLabor(laborName: "TestLinkedLabor");
            var assembly = await CreateAssembly();
            var assemblyVariable = new AssemblyVariable()
            {
                Name = "SomeVariable",
                DefaultValue = $"={labor.Name}.MinimumBillingTime.InMinutes",
                BID = BID,
                AssemblyID = assembly.ID,
                ID = -98,
                ElementType = AssemblyElementType.Number,
                DataType = DataType.Number,
                IsFormula = true,
            };
            ctx.AssemblyVariable.Add(assemblyVariable);
            ctx.SaveChanges();

            await CreateAssemblyVariable(false);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { $"{labor.Name}", new VariableValue() { DataType = DataType.String, Value = labor.Name, ValueOV = true } }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.ErrorCount);
            //Assert.AreEqual(labor.MinimumTimeInMin, result.Variables[assemblyVariable.Name].ValueAsDecimal);
        }

        [TestMethod]
        public async Task TestAssemblyWithLinkedMaterialAndLinkedLaborSumsCorrectly()
        {
            decimal matCost = 10m;
            decimal matPrice = 20m;
            await CreateMaterial(matPrice, matCost, materialName: "TestLinkedMaterial");
            var labor = await CreateLabor(laborName: "TestLinkedLabor");
            var assembly = await CreateAssembly();
            const decimal matConsumptionFormula1 = 2;
            const decimal matConsumptionFormula2 = 4;
            const decimal expectedMaterialQty = matConsumptionFormula1 * matConsumptionFormula2;
            await CreateAssemblyVariable(true, consumptionDefaultValue: $"={matConsumptionFormula1}*{matConsumptionFormula2}");
            decimal laborConsumptionQtyMinutes = Math.Max(labor.MinimumTimeInMin.GetValueOrDefault(1), labor.BillingIncrementInMin.GetValueOrDefault(1));
            await CreateAssemblyVariable(false, consumptionDefaultValue: $"={laborConsumptionQtyMinutes}");

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { "TestLinkedMaterial", new VariableValue() { DataType = DataType.String, Value = "TestLinkedMaterial", ValueOV = true } },
                    { "TestLinkedLabor", new VariableValue() { DataType = DataType.String, Value = "TestLinkedLabor", ValueOV = true } }
                }
            };


            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);

            Assert.AreEqual((double)(_laborCostHourly / 60m * laborConsumptionQtyMinutes + _laborCostFixed + expectedMaterialQty * matCost), (double)result.CostNet.Value, 0.005);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.IsNull(result.PriceTotal);
        }

        


        [TestMethod]
        public async Task ComputeWillUseLookedUpComponentsNoDropdownElements()
        {
            int assemblyID = -100;
            int assemblyVariableMaterialID = -100;
            int assemblyVariableLaborID = -99;
            int material1ID = -100;
            int material2ID = -101;
            int material3ID = -102;

            decimal material1Cost = 1m;
            decimal material2Cost = 2m;
            decimal material3Cost = 3m;
            decimal material1Price = 2m;
            decimal material2Price = 4m;
            decimal material3Price = 6m;

            int labor1ID = -100;
            int labor2ID = -101;
            int labor3ID = -102;

            decimal labor1Cost = 1m;
            decimal labor2Cost = 2m;
            decimal labor3Cost = 3m;
            decimal labor1Price = 2m;
            decimal labor2Price = 4m;
            decimal labor3Price = 6m;

            string labor1Name = "labor1";
            string labor2Name = "labor2";
            string labor3Name = "labor3";

            string material1Name = "material1";
            string material2Name = "material2";
            string material3Name = "material3";

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID - 1, assemblyVariableMaterialID, material1ID, material2ID, material3ID);
            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableLaborID, labor1ID, labor2ID, labor3ID, false);
            MaterialData material1 = new MaterialData()
            {
                BID = BID,
                ID = material1ID,
                EstimatingCost = material1Cost,
                EstimatingPrice = material1Price,
                Name = material1Name,
                InvoiceText = material1Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,

            };
            MaterialData material2 = new MaterialData()
            {
                BID = BID,
                ID = material2ID,
                EstimatingCost = material2Cost,
                EstimatingPrice = material2Price,
                Name = material2Name,
                InvoiceText = material2Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
            };
            MaterialData material3 = new MaterialData()
            {
                BID = BID,
                ID = material3ID,
                EstimatingCost = material3Cost,
                EstimatingPrice = material3Price,
                Name = material3Name,
                InvoiceText = material3Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
            };

            LaborData labor1 = new LaborData()
            {
                BID = BID,
                ID = labor1ID,
                EstimatingCostPerHour = labor1Cost,
                EstimatingPricePerHour = labor1Price,
                Name = labor1Name,
                InvoiceText = labor1Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };
            LaborData labor2 = new LaborData()
            {
                BID = BID,
                ID = labor2ID,
                EstimatingCostPerHour = labor2Cost,
                EstimatingPricePerHour = labor2Price,
                Name = labor2Name,
                InvoiceText = labor2Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };
            LaborData labor3 = new LaborData()
            {
                BID = BID,
                ID = labor3ID,
                EstimatingCostPerHour = labor3Cost,
                EstimatingPricePerHour = labor3Price,
                Name = labor3Name,
                InvoiceText = labor3Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = "ComputeWillUseLookedUpComponetsFromDropdownElements"
            };

            AssemblyVariable dropDownMaterialElement = new AssemblyVariable()
            {
                Name = "DropDownMaterial",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.DropDown,
                DataType = DataType.MaterialPart,
                ID = assemblyVariableMaterialID,
                DefaultValue = material1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
            };

            AssemblyVariable dropDownlaborElement = new AssemblyVariable()
            {
                Name = "DropDownLabor",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.DropDown,
                DataType = DataType.LaborPart,
                ID = assemblyVariableLaborID,
                DefaultValue = labor1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
            };

            ctx.AssemblyData.Add(assembly);
            //ctx.AssemblyVariable.Add(dropDownMaterialElement);
            //ctx.AssemblyVariable.Add(dropDownlaborElement);
            ctx.MaterialData.Add(material1);
            ctx.MaterialData.Add(material2);
            ctx.MaterialData.Add(material3);
            ctx.LaborData.Add(labor1);
            ctx.LaborData.Add(labor2);
            ctx.LaborData.Add(labor3);

            Assert.AreEqual(7, ctx.SaveChanges());

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                Variables = new Dictionary<string, VariableValue>()
                {

                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.CostMaterial.HasValue);
            Assert.AreEqual(0m, result.CostNet);

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID - 1, assemblyVariableMaterialID, material1ID, material2ID, material3ID);
            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableLaborID, labor1ID, labor2ID, labor3ID, false);
        }

        [TestMethod]
        public async Task ComputeWillUseLookedUpComponentsFromDropdownElementsMaterial()
        {
            int assemblyID = -100;
            int assemblyVariableID = -100;
            int material1ID = -100;
            int material2ID = -101;
            int material3ID = -102;

            decimal material1Cost = 1m;
            decimal material2Cost = 2m;
            decimal material3Cost = 3m;
            decimal material1Price = 2m;
            decimal material2Price = 4m;
            decimal material3Price = 6m;

            string material1Name = "material1";
            string material2Name = "material2";
            string material3Name = "material3";

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, material1ID, material2ID, material3ID);

            MaterialData material1 = new MaterialData()
            {
                BID = BID,
                ID = material1ID,
                EstimatingCost = material1Cost,
                EstimatingPrice = material1Price,
                Name = material1Name,
                InvoiceText = material1Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,

            };
            MaterialData material2 = new MaterialData()
            {
                BID = BID,
                ID = material2ID,
                EstimatingCost = material2Cost,
                EstimatingPrice = material2Price,
                Name = material2Name,
                InvoiceText = material2Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
            };
            MaterialData material3 = new MaterialData()
            {
                BID = BID,
                ID = material3ID,
                EstimatingCost = material3Cost,
                EstimatingPrice = material3Price,
                Name = material3Name,
                InvoiceText = material3Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = "ComputeWillUseLookedUpComponetsFromDropdownElements"
            };

            AssemblyVariable dropDownMaterialElement = new AssemblyVariable()
            {
                Name = "DropDownMaterial",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.DropDown,
                DataType = DataType.String,
                ListDataType = DataType.MaterialPart,
                ID = assemblyVariableID,
                DefaultValue = material1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
            };

            ctx.AssemblyData.Add(assembly);
            ctx.AssemblyVariable.Add(dropDownMaterialElement);
            ctx.MaterialData.Add(material1);
            ctx.MaterialData.Add(material2);
            ctx.MaterialData.Add(material3);

            Assert.AreEqual(5, ctx.SaveChanges());

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { "DropDownMaterial", new VariableValue() { DataType = DataType.String, Value = material1.Name, ValueOV = true } }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.CostMaterial.HasValue);
            Assert.AreEqual(material1Cost, result.CostNet);

            request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                Variables = new Dictionary<string, VariableValue>()
                { { "DropDownMaterial", new VariableValue() { DataType = DataType.String, Value = material2.Name, ValueOV = true } } }
            };

            string localCSCode = CBELAssemblyGenerator.Create(ctx, BID, assemblyID, ClassType.Assembly.ID()).GetAssemblyCode();

            result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.AreEqual(material2Cost, result.CostNet);

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, material1ID, material2ID, material3ID);
        }

        [TestMethod]
        public async Task ComputeWillUseLookedUpComponentsFromDropdownElementsLabor()
        {
            int assemblyID = -100;
            int assemblyVariableID = -100;
            int labor1ID = -100;
            int labor2ID = -101;
            int labor3ID = -102;

            decimal labor1Cost = 1m;
            decimal labor2Cost = 2m;
            decimal labor3Cost = 3m;
            decimal labor1Price = 2m;
            decimal labor2Price = 4m;
            decimal labor3Price = 6m;

            string labor1Name = "labor1";
            string labor2Name = "labor2";
            string labor3Name = "labor3";

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, labor1ID, labor2ID, labor3ID, false);

            LaborData labor1 = new LaborData()
            {
                BID = BID,
                ID = labor1ID,
                EstimatingCostPerHour = labor1Cost * 60,
                EstimatingPricePerHour = labor1Price * 60,
                Name = labor1Name,
                InvoiceText = labor1Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };
            LaborData labor2 = new LaborData()
            {
                BID = BID,
                ID = labor2ID,
                EstimatingCostPerHour = labor2Cost * 60,
                EstimatingPricePerHour = labor2Price * 60,
                Name = labor2Name,
                InvoiceText = labor2Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };
            LaborData labor3 = new LaborData()
            {
                BID = BID,
                ID = labor3ID,
                EstimatingCostPerHour = labor3Cost * 60,
                EstimatingPricePerHour = labor3Price * 60,
                Name = labor3Name,
                InvoiceText = labor3Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = "ComputeWillUseLookedUpComponetsFromDropdownElements"
            };

            AssemblyVariable dropDownlaborElement = new AssemblyVariable()
            {
                Name = "DropDownLabor",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.DropDown,
                DataType = DataType.String,
                ListDataType = DataType.LaborPart,
                ID = assemblyVariableID,
                DefaultValue = labor1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
            };

            ctx.AssemblyData.Add(assembly);
            ctx.AssemblyVariable.Add(dropDownlaborElement);
            ctx.LaborData.Add(labor1);
            ctx.LaborData.Add(labor2);
            ctx.LaborData.Add(labor3);

            Assert.AreEqual(5, ctx.SaveChanges());

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { "DropDownLabor", new VariableValue() { DataType = DataType.String, Value = labor1.Name, ValueOV = true } }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.CostLabor.HasValue);
            Assert.AreEqual(labor1Cost, result.CostNet);

            request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                Variables = new Dictionary<string, VariableValue>()
                { { "DropDownLabor", new VariableValue() { DataType = DataType.String, Value = labor2.Name, ValueOV = true } } }
            };

            string localCSCode = CBELAssemblyGenerator.Create(ctx, BID, assemblyID, ClassType.Assembly.ID()).GetAssemblyCode();

            result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.AreEqual(labor2Cost, result.CostNet);

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, labor1ID, labor2ID, labor3ID, false);
        }

        private static async Task CleanupComputeWillUseLookedUpComponentsFromDropdownElements(int assemblyID, int assemblyVariableID, int material1ID, int material2ID, int material3ID, bool IsMaterial = true)
        {
            var deletingCtx = PricingTestHelper.GetMockCtx(BID);
            var foundVar = deletingCtx.AssemblyVariable.FirstOrDefault(t => t.BID == BID && t.ID == assemblyVariableID);
            if (foundVar != null)
            {
                deletingCtx.Remove(foundVar);
                deletingCtx.SaveChanges();
            }
            var foundAssembly = deletingCtx.AssemblyData.FirstOrDefault(t => t.BID == BID && t.ID == assemblyID);
            if (foundAssembly != null)
            {
                deletingCtx.Remove(foundAssembly);
                deletingCtx.SaveChanges();
            }
            if (IsMaterial)
            {
                var foundMaterial1 = deletingCtx.MaterialData.FirstOrDefault(t => t.BID == BID && t.ID == material1ID);
                if (foundMaterial1 != null)
                {
                    deletingCtx.Remove(foundMaterial1);
                    deletingCtx.SaveChanges();
                }
                var foundMaterial2 = deletingCtx.MaterialData.FirstOrDefault(t => t.BID == BID && t.ID == material2ID);
                if (foundMaterial2 != null)
                {
                    deletingCtx.Remove(foundMaterial2);
                    deletingCtx.SaveChanges();
                }
                var foundMaterial3 = deletingCtx.MaterialData.FirstOrDefault(t => t.BID == BID && t.ID == material3ID);
                if (foundMaterial3 != null)
                {
                    deletingCtx.Remove(foundMaterial3);
                    deletingCtx.SaveChanges();
                }
            }
            else
            {
                var foundMaterial1 = deletingCtx.LaborData.FirstOrDefault(t => t.BID == BID && t.ID == material1ID);
                if (foundMaterial1 != null)
                {
                    deletingCtx.Remove(foundMaterial1);
                    deletingCtx.SaveChanges();
                }
                var foundMaterial2 = deletingCtx.LaborData.FirstOrDefault(t => t.BID == BID && t.ID == material2ID);
                if (foundMaterial2 != null)
                {
                    deletingCtx.Remove(foundMaterial2);
                    deletingCtx.SaveChanges();
                }
                var foundMaterial3 = deletingCtx.LaborData.FirstOrDefault(t => t.BID == BID && t.ID == material3ID);
                if (foundMaterial3 != null)
                {
                    deletingCtx.Remove(foundMaterial3);
                    deletingCtx.SaveChanges();
                }
            }
            EntityStorageClient client = await TestHelper.GetEntityStorageClient(BID);

            var assemblyDoc = new DMID() { id = assemblyID, ctid = ClassType.Assembly.ID() };
            string assemblyName = CBELAssemblyHelper.AssemblyName(BID, assemblyID, ClassType.Assembly.ID(), 1);
            await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
            await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
        }

        [TestMethod]
        public async Task TestAssemblyLinkedMaterialComponentPrice()
        {
            decimal matCost = 10m;
            decimal matPrice = 20m;

            await CreateMaterial(matPrice, matCost, materialName: "TestLinkedMaterial");
            var assembly = await CreateAssembly();
            await CreateAssemblyVariable(true);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { assemblyVariable.Name, new VariableValue() { DataType = DataType.String, Value = assemblyVariable.Name, ValueOV = true } }
                }
            };
            var cache = new TestHelper.MockTenantDataCache();
            RemoteLogger logger = new RemoteLogger(cache);
            var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, request.ComponentID, ClassType.Assembly.ID(), 1, cache, logger, BID);

            CBELOverriddenValues computeRequest = CBELAssemblyHelper.ComponentPriceRequestToComputeRequest(request);
            ICBELComputeResult computeResult = compilationResponse.Assembly.Compute(computeRequest);

            Assert.IsNotNull(computeResult);
            LinkedMaterialVariable assemblyElement = (LinkedMaterialVariable)computeResult.Assembly.Variables["TestLinkedMaterial"];

            Assert.AreEqual(8m * matPrice, assemblyElement.Price.Value);

        }



        [TestMethod]
        public async Task ComputeWillUseLookedUpComponentsFromDropdownElementsMaterialLinkMaterialsAndDirectlyAddedMaterials()
        {
            int assemblyID = -100;
            int assemblyVariableID = -100;
            int material1ID = -100;

            decimal material1Cost = 1m;
            decimal material1Price = 2m;

            string material1Name = "material1";

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, material1ID, material1ID - 1, material1ID - 2);

            MaterialData material1 = new MaterialData()
            {
                BID = BID,
                ID = material1ID,
                EstimatingCost = material1Cost,
                EstimatingPrice = material1Price,
                Name = material1Name,
                InvoiceText = material1Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,

            };

            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = "ComputeWillUseLookedUpComponetsFromDropdownElements"
            };

            AssemblyVariable dropDownMaterialElement = new AssemblyVariable()
            {
                Name = "DropDownMaterial",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.DropDown,
                DataType = DataType.String,
                ListDataType = DataType.MaterialPart,
                ID = assemblyVariableID,
                DefaultValue = material1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
            };

            AssemblyVariable linkedMaterialElement = new AssemblyVariable()
            {
                Name = "TestLinkedMaterial",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.LinkedMaterial,
                DataType = DataType.String,
                ListDataType = DataType.MaterialPart,
                ID = assemblyVariableID + 1,
                DefaultValue = material1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
                LinkedMaterialID = material1ID
            };

            ctx.AssemblyData.Add(assembly);
            ctx.AssemblyVariable.Add(dropDownMaterialElement);
            ctx.AssemblyVariable.Add(linkedMaterialElement);
            ctx.MaterialData.Add(material1);

            Assert.AreEqual(4, ctx.SaveChanges());
            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>();

            components.Add(
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1ID,
                    TotalQuantity = 1,
                    PriceUnit = _materialPrice

                });
            components.Add(
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Assembly,
                    ComponentID = assembly.ID,
                    Variables = new Dictionary<string, VariableValue>()
                    {
                        { "DropDownMaterial", new VariableValue() { DataType = DataType.String, Value = material1.Name, ValueOV = true } },
                        { "TestLinkedMaterial", new VariableValue() { DataType = DataType.String, Value = material1.Name, ValueOV = true } }
                    }
                });

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };
            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.AreEqual(_companyID, result.CompanyID);
            Assert.IsTrue(result.CostMaterial.HasValue);
            Assert.AreEqual(material1Cost * 3, result.CostMaterial.Value);
            Assert.IsTrue(result.CostLabor.HasValue);
            Assert.AreEqual(result.CostLabor, 0.0m);
            //Assert.AreEqual(decimal.Round(_laborCostFixed + (_laborCostHourly * qty), 2), result.CostLabor.Value);
            Assert.IsFalse(result.PriceTotal.HasValue);
            var foundVar = ctx.AssemblyVariable.FirstOrDefault(t => t.BID == BID && t.ID == (assemblyVariableID + 1));
            if (foundVar != null)
            {
                ctx.Remove(foundVar);
                ctx.SaveChanges();
            }

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, material1ID, material1ID - 1, material1ID - 2);
        }

        [TestMethod]
        public async Task ComputeWillUseLookedUpComponentsFromDropdownElementsLaborLinkLaborAndDirectlyAddedLabor()
        {
            int assemblyID = -100;
            int assemblyVariableID = -100;
            int labor1ID = -100;

            decimal labor1CostMinutes = 1m;
            decimal labor1PriceMinutes = 2m;

            string labor1Name = "material1";

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, labor1ID, labor1ID - 1, labor1ID - 2, false);

            LaborData labor1 = new LaborData()
            {
                BID = BID,
                ID = labor1ID,
                EstimatingCostPerHour = labor1CostMinutes * 60m,
                EstimatingPricePerHour = labor1PriceMinutes * 60m,
                Name = labor1Name,
                InvoiceText = labor1Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };


            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = "ComputeWillUseLookedUpComponetsFromDropdownElements"
            };

            AssemblyVariable dropDownLaborElement = new AssemblyVariable()
            {
                Name = "DropDownLabor",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.DropDown,
                DataType = DataType.String,
                ListDataType = DataType.LaborPart,
                ID = assemblyVariableID,
                DefaultValue = labor1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
            };

            AssemblyVariable linkedLaborElement = new AssemblyVariable()
            {
                Name = "TestLinkedLabor",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.LinkedLabor,
                DataType = DataType.String,
                ListDataType = DataType.LaborPart,
                ID = assemblyVariableID + 1,
                DefaultValue = labor1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
                LinkedLaborID = labor1ID
            };

            ctx.AssemblyData.Add(assembly);
            ctx.AssemblyVariable.Add(dropDownLaborElement);
            ctx.AssemblyVariable.Add(linkedLaborElement);
            ctx.LaborData.Add(labor1);

            Assert.AreEqual(4, ctx.SaveChanges());
            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>();

            components.Add(
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1ID,
                    TotalQuantity = 1,
                    TotalQuantityOV = true
                });
            components.Add(
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Assembly,
                    ComponentID = assembly.ID,
                    Variables = new Dictionary<string, VariableValue>()
                    {
                        { "DropDownLabor", new VariableValue() { DataType = DataType.String, Value = labor1.Name, ValueOV = true } },
                        { "TestLinkedLabor", new VariableValue() { DataType = DataType.String, Value = labor1.Name, ValueOV = true } }
                    }
                });

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };
            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.AreEqual(_companyID, result.CompanyID);
            Assert.IsTrue(result.CostMaterial.HasValue);
            Assert.AreEqual(result.CostMaterial, 0.0m);
            Assert.IsTrue(result.CostLabor.HasValue);
            Assert.AreEqual(labor1CostMinutes * 3, result.CostLabor.Value);

            Assert.IsFalse(result.PriceTotal.HasValue);

            var foundVar = ctx.AssemblyVariable.FirstOrDefault(t => t.BID == BID && t.ID == (assemblyVariableID + 1));
            if (foundVar != null)
            {
                ctx.Remove(foundVar);
                ctx.SaveChanges();
            }

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, labor1ID, labor1ID - 1, labor1ID - 2, false);

        }

        [TestMethod]
        public async Task LineItemCostsCorrectlySumAccrossManuallyAddedComponentsAndAssemblies()
        {
            List<int> variableIDs = new List<int>();
            int assemblyID = -100;
            int assemblyVariableID = -100;
            int labor1ID = -100;
            int material1ID = -100;

            decimal material1Cost = 1m;
            decimal material1Price = 2m;
            decimal labor1CostMinutes = 1m;
            decimal labor1PriceMinutes = 2m;
            int expectedLaborCount = 0;
            int expectedAssemblyLaborCount = 0;

            string labor1Name = "labor1";
            string material1Name = "material1";
            var foundVar = ctx.AssemblyVariable.Where(t => t.BID == BID && (t.ID >= (assemblyVariableID + 1) && (t.ID <= (assemblyVariableID + 3)))).ToList();

            if (foundVar != null)
            {
                ctx.RemoveRange(foundVar);
                ctx.SaveChanges();
            }
            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, labor1ID, labor1ID - 1, labor1ID - 2, false);
            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, material1ID, material1ID - 1, material1ID - 2);

            MaterialData material1 = new MaterialData()
            {
                BID = BID,
                ID = material1ID,
                EstimatingCost = material1Cost,
                EstimatingPrice = material1Price,
                Name = material1Name,
                InvoiceText = material1Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID,
                InventoryAccountID = glAccountID,
            };

            LaborData labor1 = new LaborData()
            {
                BID = BID,
                ID = labor1ID,
                EstimatingCostPerHour = labor1CostMinutes * 60m,
                EstimatingPricePerHour = labor1PriceMinutes * 60m,
                Name = labor1Name,
                InvoiceText = labor1Name,
                ExpenseAccountID = glAccountID,
                IncomeAccountID = glAccountID
            };
            expectedLaborCount++;


            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = "ComputeWillUseLookedUpComponetsFromDropdownElements"
            };

            AssemblyVariable dropDownMaterialElement = new AssemblyVariable()
            {
                Name = "DropDownMaterial",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.DropDown,
                DataType = DataType.String,
                ListDataType = DataType.MaterialPart,
                ID = assemblyVariableID,
                DefaultValue = material1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
            };
            variableIDs.Add(dropDownMaterialElement.ID);

            AssemblyVariable linkedMaterialElement = new AssemblyVariable()
            {
                Name = "TestLinkedMaterial",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.LinkedMaterial,
                DataType = DataType.String,
                ListDataType = DataType.MaterialPart,
                ID = assemblyVariableID + 1,
                DefaultValue = material1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
                LinkedMaterialID = material1ID
            };
            variableIDs.Add(linkedMaterialElement.ID);

            AssemblyVariable dropDownLaborElement = new AssemblyVariable()
            {
                Name = "DropDownLabor",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.DropDown,
                DataType = DataType.String,
                ListDataType = DataType.LaborPart,
                ID = assemblyVariableID + 2,
                DefaultValue = labor1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
            };
            variableIDs.Add(dropDownLaborElement.ID);
            expectedLaborCount++;
            expectedAssemblyLaborCount++;

            AssemblyVariable linkedLaborElement = new AssemblyVariable()
            {
                Name = "TestLinkedLabor",
                BID = BID,
                AssemblyID = assemblyID,
                ElementType = AssemblyElementType.LinkedLabor,
                DataType = DataType.String,
                ListDataType = DataType.LaborPart,
                ID = assemblyVariableID + 3,
                DefaultValue = labor1.Name,
                ConsumptionDefaultValue = "1",
                IsConsumptionFormula = false,
                LinkedLaborID = labor1ID,
                UnitType = UnitType.Time,
                UnitID = Unit.Minute,
            };
            variableIDs.Add(linkedLaborElement.ID);
            expectedLaborCount++;
            expectedAssemblyLaborCount++;

            ctx.AssemblyData.Add(assembly);
            ctx.AssemblyVariable.Add(dropDownLaborElement);
            ctx.AssemblyVariable.Add(linkedLaborElement);
            ctx.LaborData.Add(labor1);
            ctx.AssemblyVariable.Add(dropDownMaterialElement);
            ctx.AssemblyVariable.Add(linkedMaterialElement);
            ctx.MaterialData.Add(material1);

            Assert.AreEqual(7, ctx.SaveChanges());
            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>();

            components.Add(
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1ID,
                    TotalQuantity = 1,
                    TotalQuantityOV = true
                });

            components.Add(
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1ID,
                    TotalQuantity = 1,
                    TotalQuantityOV = true,
                });
            components.Add(
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Assembly,
                    ComponentID = assembly.ID,
                    Variables = new Dictionary<string, VariableValue>()
                    {
                        { "DropDownLabor", new VariableValue() { DataType = DataType.String, Value = labor1.Name, ValueOV = true } },
                        { "TestLinkedLabor", new VariableValue() { DataType = DataType.String, Value = labor1.Name, ValueOV = true } },
                        { "DropDownMaterial", new VariableValue() { DataType = DataType.String, Value = material1.Name, ValueOV = true } },
                        { "TestLinkedMaterial", new VariableValue() { DataType = DataType.String, Value = material1.Name, ValueOV = true } }
                    },
                    ChildComponents = new List<ComponentPriceRequest>()
                    {
                        new ComponentPriceRequest()
                        {
                            CompanyID = _companyID,
                            ComponentType = OrderItemComponentType.Labor,
                            VariableName = "DropDownLabor",
                            CostUnit = 10m,
                            CostUnitOV = true,
                        }
                    }
                });
            components.Add(
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1ID,
                    TotalQuantity = 3,
                    TotalQuantityOV = true,
                    CostUnit = 15m,
                    CostUnitOV = true,
                });

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };
            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.AreEqual(_companyID, result.CompanyID);

            var assemblyPriceResult = result.Components.FirstOrDefault(t => t.ComponentType == OrderItemComponentType.Assembly);

            Assert.IsNotNull(assemblyPriceResult);
            Assert.AreEqual(11m, assemblyPriceResult.CostLabor);
            Assert.AreEqual(0, assemblyPriceResult.CostMachine);
            Assert.AreEqual(material1Cost * 2, assemblyPriceResult.CostMaterial);
            Assert.AreEqual((material1Cost * 2) + 11m, assemblyPriceResult.CostNet);
            Assert.AreEqual(null, assemblyPriceResult.PricePreTax);

            Assert.AreEqual(4, assemblyPriceResult.ChildComponents.Count);
            ComponentPriceResult dropdownLabel = assemblyPriceResult.ChildComponents.FirstOrDefault(x => x.VariableName == "DropDownLabor");
            Assert.IsNotNull(dropdownLabel);
            Assert.AreEqual(10m, dropdownLabel.CostUnit);
            Assert.AreEqual(true, dropdownLabel.CostOV);


            Assert.IsTrue(result.CostMaterial.HasValue);
            // This is 3 materials costs being added up inside the ItemPriceResult
            Assert.AreEqual(material1Cost * 3, result.CostMaterial.Value);
            Assert.IsTrue(result.CostLabor.HasValue);
            Assert.AreEqual((labor1CostMinutes * expectedAssemblyLaborCount) + 55m, result.CostLabor.Value);
            Assert.IsFalse(result.PriceTotal.HasValue);

            Assert.AreEqual(4, result.Components.Count);
            dropdownLabel = result.Components.FirstOrDefault(x => x.ComponentID == labor1ID && x.ComponentType == OrderItemComponentType.Labor && x.TotalQuantity == 3);
            Assert.IsNotNull(dropdownLabel);
            Assert.AreEqual(45m, dropdownLabel.CostNet);
            Assert.AreEqual(15m, dropdownLabel.CostUnit);
            Assert.AreEqual(true, dropdownLabel.CostOV);

            foundVar = ctx.AssemblyVariable
                .Where(t => t.BID == BID && variableIDs.Contains(t.ID))
                .ToList();
            if (foundVar != null)
            {
                ctx.RemoveRange(foundVar);
                ctx.SaveChanges();
            }

            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, material1ID, material1ID - 1, material1ID - 2);
            await CleanupComputeWillUseLookedUpComponentsFromDropdownElements(assemblyID, assemblyVariableID, labor1ID, labor1ID - 1, labor1ID - 2, false);

        }

        [TestMethod]
        public void TestAssemblyName()
        {
            string assemblyName = CBELAssemblyHelper.AssemblyName(BID, 1, 12030, 1);
            Assert.AreEqual("Machine_BID1_CT12030_ID1_V1", assemblyName);
        }

        [TestMethod]
        public async Task TestAssemblyComputeWithInvalidTableLookup()
        {
            var assembly = await CreateAssembly();
            AssemblyVariable shirtSize = new AssemblyVariable()
            {
                ID = -1000,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.SingleLineText,
                IsRequired = false,
                Name = "ShirtSize",
                DefaultValue = "",
                IsFormula = false,
            };
            AssemblyVariable batchSize = new AssemblyVariable()
            {
                ID = -1001,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = false,
                Name = "BatchSize",
                DefaultValue = "",
                IsFormula = false,
            };
            AssemblyTable table = new AssemblyTable()
            {
                ID = -100,
                BID = BID,
                AssemblyID = assembly.ID,
                CellDataJSON = "[[15,20,25],[12,17,22],[10,15,20],[8,13,18]]",
                CellDataType = DataType.Number,
                ColumnCount = 3,
                ColumnDataType = DataType.String,
                ColumnIsSorted = false,
                ColumnLabel = "Shirt Size",
                ColumnMatchType = 0,
                ColumnUnitID = 0,
                ColumnValuesJSON = "[{\"index\":0,\"value\":\"1 Small\"},{\"index\":1,\"value\":\"2 Medium\"},{\"index\":2,\"value\":\"3 Large\"}]",
                ColumnVariableID = shirtSize.ID,
                IsTierTable = false,
                Label = "Test Table",
                ModifiedDT = new DateTime(),
                RowCount = 3,
                RowDataType = DataType.Number,
                RowDefaultMarkupJSON = "",
                RowIsSorted = false,
                RowLabel = "Batch size",
                RowMatchType = 0,
                RowUnitID = 0,
                RowValuesJSON = "[{\"index\":0,\"value\":1},{\"index\":1,\"value\":25},{\"index\":2,\"value\":50},{\"index\":3,\"value\":100}]",
                RowVariableID = batchSize.ID,
                TableType = 0,
                VariableName = "TestTable@\"",
            };

            ctx.AssemblyVariable.Add(shirtSize);
            ctx.AssemblyVariable.Add(batchSize);
            ctx.AssemblyTable.Add(table);
            Assert.IsTrue(ctx.SaveChanges() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { batchSize.Name, new VariableValue() { DataType = DataType.String, Value = "50", ValueOV = true } },
                    { shirtSize.Name, new VariableValue() { DataType = DataType.String, Value = "2 Medium", ValueOV = true } },
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsFalse(result.Success);
            Assert.AreEqual(1, result.ErrorCount);
            Assert.AreEqual("Invalid table variable name", result.Errors[0].Message);
        }

        [TestMethod]
        public async Task TestAssemblyComputeWithTableLookup()
        {
            var assembly = await CreateAssembly();
            AssemblyVariable shirtSize = new AssemblyVariable()
            {
                ID = -1000,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.SingleLineText,
                IsRequired = false,
                Name = "ShirtSize",
                DefaultValue = "",
                IsFormula = false,
            };
            AssemblyVariable batchSize = new AssemblyVariable()
            {
                ID = -1001,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = false,
                Name = "BatchSize",
                DefaultValue = "",
                IsFormula = false,
            };
            AssemblyTable table = new AssemblyTable()
            {
                ID = -100,
                BID = BID,
                AssemblyID = assembly.ID,
                CellDataJSON = "[[15,20,25],[12,17,22],[10,15,20],[8,13,18]]",
                CellDataType = DataType.Number,
                ColumnCount = 3,
                ColumnDataType = DataType.String,
                ColumnIsSorted = false,
                ColumnLabel = "Shirt Size",
                ColumnMatchType = 0,
                ColumnUnitID = 0,
                ColumnValuesJSON = "[{\"index\":0,\"value\":\"1 Small\"},{\"index\":1,\"value\":\"2 Medium\"},{\"index\":2,\"value\":\"3 Large\"}]",
                ColumnVariableID = shirtSize.ID,
                IsTierTable = false,
                Label = "Test Table",
                ModifiedDT = new DateTime(),
                RowCount = 3,
                RowDataType = DataType.Number,
                RowDefaultMarkupJSON = "",
                RowIsSorted = false,
                RowLabel = "Batch size",
                RowMatchType = 0,
                RowUnitID = 0,
                RowValuesJSON = "[{\"index\":0,\"value\":1},{\"index\":1,\"value\":25},{\"index\":2,\"value\":50},{\"index\":3,\"value\":100}]",
                RowVariableID = batchSize.ID,
                TableType = 0,
                VariableName = "TestTable",
            };

            ctx.AssemblyVariable.Add(shirtSize);
            ctx.AssemblyVariable.Add(batchSize);
            ctx.AssemblyTable.Add(table);
            Assert.IsTrue(ctx.SaveChanges() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { batchSize.Name, new VariableValue() { DataType = DataType.String, Value = "50", ValueOV = true } },
                    { shirtSize.Name, new VariableValue() { DataType = DataType.String, Value = "2 Medium", ValueOV = true } },
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Variables.ContainsKey("TestTable"));
            Assert.AreEqual(15, result.Variables["TestTable"].ValueAsDecimal);
        }



        [TestMethod]
        public async Task TestAllTierTypeCombinations()
        {
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarketBasedPricing, PriceFormulaType.Fixed, "");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarketBasedPricing, PriceFormulaType.PriceTable, "PriceTable");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarketBasedPricing, PriceFormulaType.DiscountTable, "DiscountTable");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarketBasedPricing, PriceFormulaType.Custom, "DiscountTable");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarginBasedPricing, PriceFormulaType.Fixed, "");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarginBasedPricing, PriceFormulaType.MarginTable, "MarginTable");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarginBasedPricing, PriceFormulaType.Custom, "MarginTable");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarkupBasedPricing, PriceFormulaType.Fixed, "");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarkupBasedPricing, PriceFormulaType.MarkupTable, "MarkupTable");
            await TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType.MarkupBasedPricing, PriceFormulaType.Custom, "MarkupTable");

        }

        public async Task TestAssemblyComputeWithTierTableLookupHelper(AssemblyPricingType pricingType, PriceFormulaType formulaType, string ExpectedTableType)
        {
            await InitializePricingTests();
            var assembly = await CreateAssembly();

            assembly.PricingType = pricingType;
            assembly.PriceFormulaType = formulaType;
            assembly.HasTierTable = true;

            AssemblyVariable shirtSize = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.SingleLineText,
                IsRequired = false,
                Name = "ShirtSize",
                DefaultValue = "",
                IsFormula = false,
            };
            AssemblyVariable price = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = false,
                Name = "PriceTest",
                DefaultValue = $"={ExpectedTableType}.Value",
                IsFormula = true,
            };

            AssemblyVariable varTier = new AssemblyVariable
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                ClassTypeID = 12046,
                ModifiedDT = DateTime.Now,
                Name = "Tier",
                DefaultValue = "=CompanyTier();",
                DataType = DataType.String,
                IsFormula = true,
                UnitID = null,
                ListDataType = null,
                ListValuesJSON = null,
                AllowCustomValue = null,
                AllowMultiSelect = null,
                GroupOptionsByCategory = null,
                Label = "Tier",
                LabelType = null,
                AltText = null,
                ElementUseCount = 0,
                Tooltip = null,
                AllowDecimals = null,
                DisplayType = null,
                DecimalPlaces = null,
                SystemVariableID = 2,
                IsRequired = true,
                IsDisabled = false,
                ElementType = AssemblyElementType.SingleLineLabel,
                AssemblyID = assembly.ID,
                TempID = null,
                LinkedMaterialID = null,
                LinkedLaborID = null,
                LinkedMachineID = null,
                LinkedAssemblyID = null,
                ConsumptionDefaultValue = null,
                IsConsumptionFormula = false,
                Formulas = null
            };

            AssemblyTable table = new AssemblyTable()
            {
                ID = PricingTestHelper.NextAssemblyTableID(ctx, BID),
                BID = BID,
                AssemblyID = assembly.ID,
                CellDataJSON = "[[15,20,25],[12,17,22],[10,15,20],[8,13,18]]",
                CellDataType = DataType.Number,
                ColumnCount = 3,
                ColumnDataType = DataType.String,
                ColumnIsSorted = false,
                ColumnLabel = "Shirt Size",
                ColumnMatchType = 0,
                ColumnUnitID = 0,
                ColumnValuesJSON = "[{\"index\":0,\"value\":\"1 Small\"},{\"index\":1,\"value\":\"2 Medium\"},{\"index\":2,\"value\":\"3 Large\"}]",
                ColumnVariableID = shirtSize.ID,
                IsTierTable = true,
                Label = "Tier" + ExpectedTableType,
                ModifiedDT = new DateTime(),
                RowVariableID = varTier.ID,
                RowCount = 3,
                RowDataType = DataType.String,
                RowDefaultMarkupJSON = "",
                RowIsSorted = false,
                RowLabel = "",
                RowMatchType = 0,
                RowUnitID = 0,
                RowValuesJSON = "[{\"index\":0,\"value\":\"Default Tier\"},{\"index\":1,\"value\":25},{\"index\":2,\"value\":50},{\"index\":3,\"value\":100}]",
                TableType = 0,
                VariableName = "Tier" + ExpectedTableType,
            };

            if(assembly.Variables == null)
                assembly.Variables = new List<AssemblyVariable>();
            if (assembly.Tables == null)
                assembly.Tables = new List<AssemblyTable>();

            assembly.Variables.Add(shirtSize);
            assembly.Variables.Add(price);
            assembly.Variables.Add(varTier);
            assembly.Tables.Add(table);

            //ctx.AssemblyVariable.Add(shirtSize);
            //ctx.AssemblyVariable.Add(price);
            //ctx.AssemblyVariable.Add(varTier);
            //ctx.AssemblyTable.Add(table);
            ctx.AssemblyData.Update(assembly);

            Assert.IsTrue(ctx.SaveChanges() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { shirtSize.Name, new VariableValue() { DataType = DataType.String, Value = "2 Medium", ValueOV = true } },
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            if (ExpectedTableType.Length > 0)
            {
                Assert.IsTrue(result.Variables.ContainsKey(ExpectedTableType));
                Assert.AreEqual(20, result.Variables["PriceTest"].ValueAsDecimal);
            }
            else Assert.IsFalse(result.Variables.Keys.Where(k => k.Contains("Table")).Any());
            await CleanupDefaultPricingTests();
        }

        [TestMethod]
        public async Task TestFailedAssemblyComputeWithTableLookup()
        {
            var assembly = await CreateAssembly();
            AssemblyVariable shirtSize = new AssemblyVariable()
            {
                ID = -1000,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.SingleLineText,
                IsRequired = false,
                Name = "ShirtSize",
                DefaultValue = "",
                IsFormula = false,
            };
            AssemblyVariable batchSize = new AssemblyVariable()
            {
                ID = -1001,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = false,
                Name = "BatchSize",
                DefaultValue = "",
                IsFormula = false,
            };
            AssemblyTable table = new AssemblyTable()
            {
                ID = -100,
                BID = BID,
                AssemblyID = assembly.ID,
                CellDataJSON = "[[15,20,25],[12,17,22],[10,15,20],[8,13,18]]",
                CellDataType = DataType.Number,
                ColumnCount = 3,
                ColumnDataType = DataType.String,
                ColumnIsSorted = false,
                ColumnLabel = "Shirt Size",
                ColumnMatchType = Models.AssemblyTableMatchType.ExactMatch,
                ColumnUnitID = 0,
                ColumnValuesJSON = "[{\"index\":0,\"value\":\"1 Small\"},{\"index\":1,\"value\":\"2 Medium\"},{\"index\":2,\"value\":\"3 Large\"}]",
                ColumnVariableID = shirtSize.ID,
                IsTierTable = false,
                Label = "Test Table",
                ModifiedDT = new DateTime(),
                RowCount = 3,
                RowDataType = DataType.Number,
                RowDefaultMarkupJSON = "",
                RowIsSorted = false,
                RowLabel = "Batch size",
                RowMatchType = Models.AssemblyTableMatchType.ExactMatch,
                RowUnitID = 0,
                RowValuesJSON = "[{\"index\":0,\"value\":1},{\"index\":1,\"value\":25},{\"index\":2,\"value\":50},{\"index\":3,\"value\":100}]",
                RowVariableID = batchSize.ID,
                TableType = 0,
                VariableName = "TestTable",
            };

            ctx.AssemblyVariable.Add(shirtSize);
            ctx.AssemblyVariable.Add(batchSize);
            ctx.AssemblyTable.Add(table);
            Assert.IsTrue(ctx.SaveChanges() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { batchSize.Name, new VariableValue() { DataType = DataType.String, Value = "50", ValueOV = true } },
                    { shirtSize.Name, new VariableValue() { DataType = DataType.String, Value = "6 XXX-Large", ValueOV = true } },
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsFalse(result.Success);
            Assert.AreEqual(1, result.ErrorCount);

            Assert.AreEqual("TableLookupException", result.Errors[0].ErrorType);
            Assert.IsFalse(result.Errors[0].IsValidation);
            Assert.AreEqual("Exact match must be found row variable: ShirtSize. ColValue is: 6 XXX-Large", result.Errors[0].Message);
            Assert.IsNull(result.Errors[0].VariableName);
            Assert.IsNull(result.Errors[0].FormulaText);
            Assert.IsNull(result.Errors[0].ReferenceID);
        }

        [TestMethod]
        public async Task ComponentPriceRequestWithOverriddenComponentsReturnsCostsWithQuantityOVApplied()
        {
            decimal matCost = 10m;
            decimal matPrice = 20m;

            await CreateMaterial(matPrice, matCost, materialName: "TestLinkedMaterial");
            var assembly = await CreateAssembly();
            await CreateAssemblyVariable(true);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                TotalQuantityOV = true,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { assemblyVariable.Name, new VariableValue() { DataType = DataType.String, Value = assemblyVariable.Name, ValueOV = true } }
                }
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success);

            Assert.AreEqual(8m * matCost, result.CostMaterial.Value);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.IsNull(result.PriceTotal);
            Assert.IsNotNull(result.ChildComponents);
            Assert.AreEqual(1, result.ChildComponents.Count);
            var materialComponent = result.ChildComponents.FirstOrDefault();

            Assert.AreEqual(8, materialComponent.TotalQuantity);
            Assert.AreEqual(false, materialComponent.TotalQuantityOV);

            request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { assemblyVariable.Name, new VariableValue() { DataType = DataType.String, Value = assemblyVariable.Name, ValueOV = true } }
                },
                ChildComponents = new List<ComponentPriceRequest>()
                {
                    new ComponentPriceRequest()
                    {
                        ComponentID = materialComponent.ComponentID.Value,
                        ComponentType = materialComponent.ComponentType,
                        TotalQuantity = 16,
                        TotalQuantityOV = true,
                        VariableName = "TestLinkedMaterial",
                    }
                }
            };

            result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success);
            Assert.IsNull(result.PriceTotal);
            Assert.IsNotNull(result.ChildComponents);
            Assert.AreEqual(1, result.ChildComponents.Count);

            Assert.AreEqual(16m * matCost, result.CostMaterial.Value);
            // PriceTotal is based on the AssemblyPrice DefaultValueFunction which is null unless overridden
            Assert.AreEqual(16, result.ChildComponents.FirstOrDefault().TotalQuantity);
            Assert.AreEqual(true, result.ChildComponents.FirstOrDefault().TotalQuantityOV);


        }

        [TestMethod]
        public async Task DivideByZeroErrorsAreIncludedIfTheyOccurDuringComputation()
        {
            var assembly = await CreateAssembly();
            AssemblyVariable divByZero = new AssemblyVariable()
            {
                ID = -1000,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = false,
                Name = "divByZero",
                DefaultValue = "=TotalQuantity.Value / 0",
                IsFormula = true,
            };

            ctx.AssemblyVariable.Add(divByZero);

            Assert.IsTrue(ctx.SaveChanges() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsFalse(result.Success);

            // This should return a division by zero error in the result.Errors
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(1, result.ErrorCount);
            Assert.AreEqual(nameof(DivideByZeroException), result.Errors[0].ErrorType);
            Assert.AreEqual("divByZero", result.Errors[0].VariableName);
            Assert.AreEqual("=TotalQuantity.Value / 0", result.Errors[0].FormulaText);
            Assert.AreEqual("CBEL Exception: Attempted to divide by zero. in Element divByZero in Assembly Test Assembly For Pricing", result.Errors[0].Message);
            Assert.AreEqual(null, result.Errors[0].ReferenceID);

            Assert.IsFalse(result.Errors[0].IsValidation);
        }

        [TestMethod]
        public async Task CircularReferenceErrorsAreIncludedIfTheyOccurDuringComputation()
        {
            var assembly = await CreateAssembly();
            AssemblyVariable varA = new AssemblyVariable()
            {
                ID = -1000,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = false,
                Name = "A",
                DefaultValue = "=B.Value",
                IsFormula = true,
            };

            AssemblyVariable varB = new AssemblyVariable()
            {
                ID = -1001,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = false,
                Name = "B",
                DefaultValue = "=A.Value+1",
                IsFormula = true,
            };

            ctx.AssemblyVariable.Add(varA);
            ctx.AssemblyVariable.Add(varB);

            Assert.IsTrue(ctx.SaveChanges() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsFalse(result.Success);

            // This should return a circular reference error in the result.Errors
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(1, result.ErrorCount);
            Assert.AreEqual(nameof(CircularReferenceFormulaException), result.Errors[0].ErrorType);
            Assert.AreEqual("A", result.Errors[0].VariableName);
            Assert.AreEqual("=B.Value", result.Errors[0].FormulaText);
            Assert.AreEqual("Recursive Formula Error in variable A. in Element A in Assembly Test Assembly For Pricing", result.Errors[0].Message);
            Assert.AreEqual(null, result.Errors[0].ReferenceID);

            Assert.IsFalse(result.Errors[0].IsValidation);
        }

        [TestMethod]
        public async Task StringConcatenationComputation()
        {
            var assembly = await CreateAssembly();
            AssemblyVariable varA = new AssemblyVariable()
            {
                ID = -1000,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.SingleLineText,
                DataType = DataType.String,
                IsRequired = false,
                Name = "A",
                DefaultValue = "=\"one\" + \"two\"",
                IsFormula = true,
            };

            AssemblyVariable varB = new AssemblyVariable()
            {
                ID = -1001,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.SingleLineText,
                DataType = DataType.String,
                IsRequired = false,
                Name = "B",
                DefaultValue = "=\"one\" & \"two\"",
                IsFormula = true,
            };

            ctx.AssemblyVariable.Add(varA);
            ctx.AssemblyVariable.Add(varB);

            Assert.IsTrue(ctx.SaveChanges() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Success, String.Join("\r\n", result?.Errors?.Select(t => t.Message) ?? new List<string>()));

            Assert.IsNotNull(result.Variables);
            Assert.AreEqual("onetwo", result.Variables["A"].Value);
            Assert.AreEqual("onetwo", result.Variables["B"].Value);
        }

        [TestMethod]
        public async Task RequiredValdiationFailDuringComputation()
        {
            var assembly = await CreateAssembly();
            AssemblyVariable varRequired = new AssemblyVariable()
            {
                ID = -1000,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = true,
                Name = "Required",
            };

            AssemblyVariable varNotRequired = new AssemblyVariable()
            {
                ID = -1001,
                BID = BID,
                AssemblyID = assembly.ID,
                ElementType = AssemblyElementType.Number,
                IsRequired = false,
                Name = "NotRequired",
            };

            ctx.AssemblyVariable.Add(varRequired);
            ctx.AssemblyVariable.Add(varNotRequired);

            Assert.IsTrue(ctx.SaveChanges() > 0);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
            };

            ComponentPriceResult result = await pricingEngine.Compute(request, PricingEngineType.Computed);
            Assert.IsNotNull(result);
            Assert.IsFalse(result.Success);

            // This should return a circular reference error in the result.Errors
            Assert.IsNotNull(result.Errors);
            Assert.AreEqual(1, result.ErrorCount);
            Assert.IsTrue(result.Errors[0].IsValidation);
            Assert.AreEqual("Validation", result.Errors[0].ErrorType);
            Assert.AreEqual("Required", result.Errors[0].VariableName);
            Assert.AreEqual("Value Required for Element Required.", result.Errors[0].Message);
            Assert.IsNull(result.Errors[0].FormulaText);
            Assert.IsNull(result.Errors[0].ReferenceID);
        }

        [TestMethod]
        public async Task TestLoadCompany()
        {
            decimal matCost = 10m;
            decimal matPrice = 20m;

            await CreateMaterial(matPrice, matCost, materialName: "TestLinkedMaterial");

            var assembly = await CreateAssembly();
            await CreateAssemblyVariable(true);
            
            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { assemblyVariable.Name, new VariableValue() { DataType = DataType.String, Value = assemblyVariable.Name, ValueOV = true } }
                }
            };
            var cache = new TestHelper.MockTenantDataCache();
            var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, request.ComponentID, ClassType.Assembly.ID(), 1, cache, logger, BID);
            var pricingTier = ctx.FlatListItem.Where(c => c.BID == BID && c.FlatListType == FlatListType.PricingTiers).OrderByDescending(c => c.ID).First();
            var dbCompany = ctx.CompanyData.Include(c => c.PricingTier).Where(c => c.BID == BID).First();
            var originalPricingTier = dbCompany.PricingTierID;
            dbCompany.PricingTierID = pricingTier.ID;
            ctx.SaveChanges();
            ((CBELAssemblyBase)compilationResponse.Assembly).CompanyID = dbCompany.ID;
            var company = ((CBELAssemblyBase)compilationResponse.Assembly).Company;
            Assert.AreEqual(company.Name, dbCompany.Name);
            Assert.AreEqual(company.Tier, pricingTier.Name);
            dbCompany.PricingTierID = originalPricingTier;
            ctx.SaveChanges();
        }

        [TestMethod]
        public async Task TestNullCompany()
        {
            decimal matCost = 10m;
            decimal matPrice = 20m;

            await CreateMaterial(matPrice, matCost, materialName: "TestLinkedMaterial");

            var assembly = await CreateAssembly();
            await CreateAssemblyVariable(true);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = assembly.ID,
                TotalQuantity = 1,
                Variables = new Dictionary<string, VariableValue>()
                {
                    { assemblyVariable.Name, new VariableValue() { DataType = DataType.String, Value = assemblyVariable.Name, ValueOV = true } }
                }
            };
            var cache = new TestHelper.MockTenantDataCache();
            var compilationResponse = await CBELAssemblyHelper.LoadAssembly(ctx, request.ComponentID, ClassType.Assembly.ID(), 1, cache, logger, BID);
            var company = ((CBELAssemblyBase)compilationResponse.Assembly).Company;
            Assert.AreEqual(company.Name, null);
            Assert.AreEqual(company.Tier, ctx.FlatListItem.Where(c => c.BID == BID && c.ID == 100 && c.FlatListType == FlatListType.PricingTiers).First().Name);


        }

        [TestMethod]
        public async Task TestCheckComputeWithCompany()
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            Dictionary<string, string> varDefault = new Dictionary<string, string>()
            {
                {"SomeVariable", "=Company.Tier"}
            };

            await CreateAssembly(variableDefaultDictionary: varDefault);

            var request = new ComponentPriceRequest()
            {
                CompanyID = _companyID,
                ComponentType = OrderItemComponentType.Assembly,
                ComponentID = _assemblyID,
                TotalQuantity = 1,
                //PriceUnit = _laborPriceHourly,
            };

            var cpr = await pricingEngine.Compute(request, PricingEngineType.Computed);

            Assert.IsTrue(cpr.Variables.Count() > 0);
            Assert.AreEqual(cpr.Variables["SomeVariable"].Value, ctx.FlatListItem.Where(fl => fl.BID == BID && fl.ID == 100 && fl.FlatListType == FlatListType.PricingTiers).First().Name);
        }

        [TestMethod]
        public async Task TestErrorFunctionInCompute()
        {
            AssemblyData assembly = await CreateAssembly(
                variableDefaultDictionary: new Dictionary<string, string>()
                    {
                        { "FailVar", "=Error(\"This is a test\")" }
                    });

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Assembly,
                    ComponentID = assembly.ID,
                    TotalQuantity = 1,
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);
            Assert.IsNull(result.PriceTotal);
            Assert.IsFalse(result.Success);
            Assert.AreEqual(1, result.ErrorCount);
            Assert.AreEqual("This is a test", result.Components.First().Errors.First().Message);
        }

        [TestMethod]
        public async Task TestNullFunctionInCompute()
        {
            AssemblyData assembly = await CreateAssembly(
                variableDefaultDictionary: new Dictionary<string, string>()
                    {
                        { "FailVar", "=NULL()" }
                    });

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Assembly,
                    ComponentID = assembly.ID,
                    TotalQuantity = 1,
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);
            Assert.IsNull(result.PriceTotal);
            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public async Task TestAltFunctionInCompute()
        {
            AssemblyData assembly = await CreateAssembly(
                variableDefaultDictionary: new Dictionary<string, string>()
                    {
                        { "AltVar", "=\"test1\"" }
                    }, 
                elementType: AssemblyElementType.UrlLabel,
                altText: "=\"test2\"");

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>(){
                new ComponentPriceRequest()
                {
                    CompanyID = _companyID,
                    ComponentType = OrderItemComponentType.Assembly,
                    ComponentID = assembly.ID,
                    TotalQuantity = 1,
                }
            };
            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                CompanyID = _companyID,
                Quantity = 1,
                EngineType = PricingEngineType.SimplePart,
                Components = components
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Assert.IsNotNull(result);
            var comp = result.Components.FirstOrDefault();
            Assert.IsNotNull(comp);
            Assert.IsTrue(comp.Errors == null || comp.Errors.Count < 1);
            Assert.IsNotNull(comp.Variables);

            Assert.IsNotNull(comp.Variables["AltVar"].Value);
            Assert.AreEqual("test1", comp.Variables["AltVar"].Value);

            Assert.IsNotNull(comp.Variables["AltVar"].Alt);
            Assert.AreEqual("test2", comp.Variables["AltVar"].Alt);

            Assert.IsTrue(result.Success);
        }

        [TestMethod]
        public async Task MachineTemplateCompileAndComputeTest()
        {
            int machineTemplateID = -98;
            int currentVariableID = -99;

            string machineTemplateName = "TestMachineTemplate" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable templateQuantity = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = BID,
                Name = "Quantity",
                Label = "Quantity",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "1",
                IsFormula = false,
                IsRequired = true,
            };

            AssemblyVariable templateHeight = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "12",
            };

            AssemblyVariable templateWidth = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "24",
            };

            AssemblyVariable templateArea = new AssemblyVariable()
            {
                ID = currentVariableID--,
                BID = BID,
                Name = "Area",
                Label = "Area",
                DataType = DataType.Number,
                ElementType = AssemblyElementType.Number,
                DefaultValue = "=Height.Value * Width.Value",
                IsFormula = true
            };

            AssemblyData testTemplate = new AssemblyData()
            {
                BID = BID,
                ID = machineTemplateID,
                Name = machineTemplateName,
                AssemblyType = AssemblyType.MachineTemplate,
                Variables = new List<AssemblyVariable>()
                {
                    templateQuantity,
                    templateHeight,
                    templateWidth,
                    templateArea,
                }
            };

            await PricingTestHelper.CleanupEntities(BID, templateQuantity, templateHeight, templateWidth, templateArea, testTemplate);

            ctx.AssemblyData.Add(testTemplate);
            ctx.SaveChanges();

            try
            {
                ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
                {
                    Components = new List<ComponentPriceRequest>()
                    {
                        new ComponentPriceRequest()
                        {
                            ComponentType = OrderItemComponentType.Assembly,
                            ComponentID = machineTemplateID,
                            AssemblyQuantity = 1,
                        }
                    }
                };

                ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

                Assert.IsNotNull(result);
                Assert.AreEqual(1, result.Components.Count);
                Assert.AreNotEqual(0, result.Components.First().Variables);

                foreach (VariableValue variableValue in result.Components.First().Variables.Values)
                    Assert.IsTrue(variableValue.IsIncluded.GetValueOrDefault(true));
            }
            finally
            {
                await PricingTestHelper.CleanupEntities(BID, templateQuantity, templateHeight, templateWidth, templateArea, testTemplate);
            }
        }
    }
}
