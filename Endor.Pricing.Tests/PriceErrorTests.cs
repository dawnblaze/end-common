﻿using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Logging.Client;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Endor.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class PriceErrorTests
    {
        private ApiContext ctx = null;
        private PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        private const short BID = 1;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestMethod]
        public async Task DivideByZeroTest()
        {
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string assemblyName = "ParentAssembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=TotalQuantity.Value / 0",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyPrice
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);
            ICBELAssembly cbelAssembly = await assemblyGen.GetAssemblyInstance(ctx, cache, logger);

            Assert.IsNotNull(cbelAssembly);

            ICBELComputeResult result = cbelAssembly.Compute();

            Assert.IsNotNull(result);
            Assert.IsTrue(result.HasErrors);
        }
    }
}