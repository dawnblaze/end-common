﻿using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.Logging.Client;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class ComponentCostTests
    {
        private ApiContext ctx = null;
        private PricingEngine pricingEngine = null;
        private ITenantDataCache cache = null;
        private RemoteLogger logger = null;

        private const short BID = 1;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            cache = new TestHelper.MockTenantDataCache();
            logger = new RemoteLogger(cache);
            pricingEngine = new PricingEngine(BID, ctx, cache, logger);
            cache = new TestHelper.MockTenantDataCache();
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestMethod]
        [DataRow(5.00, 2.00, 3.00, null, 3.00, 5.00, null, 5.00, 4.00, null)]
        [DataRow(5.00, 2.00, 3.00, 10.00, 3.00, 5.00, 0.00, 5.00, 4.00, 1.00)]
        public async Task ComponentsWithCosts(
            double _labor1CostFixed, double _labor1CostPerMinute, double _labor1Quantity, double? _labor1OVUnitCost,
            double _material1Cost, double _material1Quantity, double? _material1OVUnitCost,
            double _material2Cost, double _material2Quantity, double? _material2OVUnitCost
            )
        {
            decimal labor1CostFixed = (decimal)_labor1CostFixed;
            decimal labor1CostPerMinute = (decimal)_labor1CostPerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal labor1UnitCost = (decimal)_labor1OVUnitCost.GetValueOrDefault(0);
            bool labor1UnitCostOV = _labor1OVUnitCost.HasValue;

            decimal material1Cost = (decimal)_material1Cost;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material1UnitCost = (decimal)_material1OVUnitCost.GetValueOrDefault(0);
            bool material1UnitCostOV = _material1OVUnitCost.HasValue;

            decimal material2Cost = (decimal)_material2Cost;
            decimal material2Quantity = (decimal)_material2Quantity;
            decimal material2UnitCost = (decimal)_material2OVUnitCost.GetValueOrDefault(0);
            bool material2UnitCostOV = _material2OVUnitCost.HasValue;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, "labor1", glAccountID, glAccountID, 0m, 1m, labor1CostFixed, labor1CostPerMinute);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, "material1", glAccountID, glAccountID, glAccountID, 1m, material1Cost);
            MaterialData material2 = PricingTestHelper.InitMaterial(ctx, BID, "material2", glAccountID, glAccountID, glAccountID, 1m, material2Cost);
            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            List<ComponentPriceRequest> components = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true,
                    CostUnit = labor1UnitCost,
                    CostUnitOV = labor1UnitCostOV,
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true,
                    CostUnit = material1UnitCost,
                    CostUnitOV = material1UnitCostOV,
                },

                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material2.ID,
                    TotalQuantity = material2Quantity,
                    TotalQuantityOV = true,
                    CostUnit = material2UnitCost,
                    CostUnitOV = material2UnitCostOV,
                }

            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = 1m,
                EngineType = PricingEngineType.SimplePart,
                Components = components,
            };

            decimal labor1NewUnitCost;
            decimal labor1NewCost;

            if (labor1UnitCostOV)
            {
                labor1NewUnitCost = labor1UnitCost;
                labor1NewCost = labor1NewUnitCost * labor1Quantity;
            }

            else
            {
                labor1NewCost = labor1CostFixed + (labor1CostPerMinute * labor1Quantity);
                labor1NewUnitCost = decimal.Round(labor1NewCost / labor1Quantity, 6);
            }

            decimal material1NewUnitCost = material1UnitCostOV ? material1UnitCost : material1Cost;
            decimal material1NewCost = material1NewUnitCost * material1Quantity;

            decimal material2NewUnitCost = material2UnitCostOV ? material2UnitCost : material2Cost;
            decimal material2NewCost = material2NewUnitCost * material2Quantity;


            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);

            ComponentPriceResult labor1Result = result.Components.FirstOrDefault(x => x.ComponentID == labor1.ID && x.ComponentType == OrderItemComponentType.Labor);
            Assert.IsNotNull(labor1Result);
            Assert.AreEqual(labor1NewCost, labor1Result.CostLabor);
            Assert.AreEqual(labor1NewCost, labor1Result.CostNet);
            Assert.AreEqual(labor1UnitCostOV, labor1Result.CostOV);
            Assert.AreEqual(labor1NewUnitCost, labor1Result.CostUnit);

            ComponentPriceResult material1Result = result.Components.FirstOrDefault(x => x.ComponentID == material1.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material1Result);
            Assert.AreEqual(material1NewCost, material1Result.CostMaterial);
            Assert.AreEqual(material1NewCost, material1Result.CostNet);
            Assert.AreEqual(material1UnitCostOV, material1Result.CostOV);
            Assert.AreEqual(material1NewUnitCost, material1Result.CostUnit);

            ComponentPriceResult material2Result = result.Components.FirstOrDefault(x => x.ComponentID == material2.ID && x.ComponentType == OrderItemComponentType.Material);
            Assert.IsNotNull(material2Result);
            Assert.AreEqual(material1NewCost, material1Result.CostMaterial);
            Assert.AreEqual(material2NewCost, material2Result.CostNet);
            Assert.AreEqual(material2UnitCostOV, material2Result.CostOV);
            Assert.AreEqual(material2NewUnitCost, material2Result.CostUnit);
        }



        [TestMethod]
        [DataRow(1.00, null, 5.00, 2.00, 3.00, null, 3.00, 5.00, null)]
        [DataRow(1.00, 10.00, 5.00, 2.00, 3.00, 10.00, 3.00, 5.00, 0.00)]
        public async Task AssemblyWithOVCostWithChildren(
            double _assembly2Quantity, double? _assembly2OVUnitCost,
            double _labor1CostFixed, double _labor1CostPerMinute, double _labor1Quantity, double? _labor1OVUnitCost,
            double _material1Cost, double _material1Quantity, double? _material1OVUnitCost
            )
        {
            string _parentAsmName = "parentAssembly", _childAsmName = "childAssembly", _materialName = "material1", _laborName = "labor1";
            decimal labor1CostFixed = (decimal)_labor1CostFixed;
            decimal labor1CostPerMinute = (decimal)_labor1CostPerMinute;
            decimal labor1Quantity = (decimal)_labor1Quantity;
            decimal labor1UnitCost = (decimal)_labor1OVUnitCost.GetValueOrDefault(0);
            bool labor1UnitCostOV = _labor1OVUnitCost.HasValue;

            decimal material1Cost = (decimal)_material1Cost;
            decimal material1Quantity = (decimal)_material1Quantity;
            decimal material1UnitCost = (decimal)_material1OVUnitCost.GetValueOrDefault(0);
            bool material1UnitCostOV = _material1OVUnitCost.HasValue;

            decimal assembly2Quantity = (decimal)_assembly2Quantity;
            decimal assembly2UnitCost = (decimal)_assembly2OVUnitCost.GetValueOrDefault(0);
            bool assembly2UnitCostOV = _assembly2OVUnitCost.HasValue;

            int glAccountID = ctx.GLAccount.First(t => t.BID == BID).ID;

            LaborData labor1 = PricingTestHelper.InitLabor(ctx, BID, _laborName, glAccountID, glAccountID, 0m, 1m, labor1CostFixed, labor1CostPerMinute);
            MaterialData material1 = PricingTestHelper.InitMaterial(ctx, BID, _materialName, glAccountID, glAccountID, glAccountID, 1m, material1Cost);
            AssemblyData assembly = PricingTestHelper.InitAssembly(ctx, BID, _parentAsmName, glAccountID, 1m);
            AssemblyData assembly2 = PricingTestHelper.InitAssembly(ctx, BID, _childAsmName, glAccountID, 1m);


            AssemblyVariable linkedChildAssembly = PricingTestHelper
                             .InitAssemblyVariable(ctx, BID, _childAsmName, assembly.ID, AssemblyElementType.LinkedAssembly, linkedAssemblyID: assembly2.ID, defaultValue: _childAsmName);
            AssemblyVariable linkedLabor = PricingTestHelper
                            .InitAssemblyVariable(ctx, BID, _laborName, assembly2.ID, AssemblyElementType.LinkedLabor, linkedLaborID: labor1.ID, defaultValue: _laborName);
            AssemblyVariable linkedMaterial = PricingTestHelper
                            .InitAssemblyVariable(ctx, BID, _materialName, assembly2.ID, AssemblyElementType.LinkedMaterial, linkedMaterialID: material1.ID, defaultValue: _materialName);
            Assert.AreNotEqual(0, await ctx.SaveChangesAsync());

            CBELAssemblyGenerator assemblyGenerator = CBELAssemblyGenerator.Create(null, assembly2);
            var compilationResult = await assemblyGenerator.Save(cache);
            assemblyGenerator = CBELAssemblyGenerator.Create(null, assembly);
            compilationResult = await assemblyGenerator.Save(cache);

            List<ComponentPriceRequest> grandchildComponents = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Labor,
                    ComponentID = labor1.ID,
                    TotalQuantity = labor1Quantity,
                    TotalQuantityOV = true,
                    CostUnit = labor1UnitCost,
                    CostUnitOV = labor1UnitCostOV,
                    VariableName =_laborName
                },
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Material,
                    ComponentID = material1.ID,
                    TotalQuantity = material1Quantity,
                    TotalQuantityOV = true,
                    CostUnit = material1UnitCost,
                    CostUnitOV = material1UnitCostOV,
                    VariableName = _materialName
                }
            };

            List<ComponentPriceRequest> parentComponent = new List<ComponentPriceRequest>
            {
                new ComponentPriceRequest()
                {
                    ComponentType = OrderItemComponentType.Assembly,
                    ComponentID = assembly.ID,
                    TotalQuantity = 1,
                    TotalQuantityOV = true,
                    CostUnit = null,
                    CostUnitOV = false,
                    ChildComponents = new List<ComponentPriceRequest>()
                    {
                        new ComponentPriceRequest()
                        {
                            ComponentType = OrderItemComponentType.Assembly,
                            ComponentID = assembly2.ID,
                            TotalQuantity = assembly2Quantity,
                            TotalQuantityOV = true,
                            CostUnit = assembly2UnitCost,
                            CostUnitOV = assembly2UnitCostOV,
                            ChildComponents = grandchildComponents,
                            VariableName = _childAsmName
                        }
                    }
                }
            };

            ItemPriceRequest itemPriceRequest = new ItemPriceRequest()
            {
                Quantity = 1m,
                EngineType = PricingEngineType.SimplePart,
                Components = parentComponent,
            };

            ItemPriceResult result = await pricingEngine.Compute(itemPriceRequest, false);
            Console.WriteLine(JsonConvert.SerializeObject(new { itemPriceRequest }, Formatting.Indented));
            ComponentPriceResult assemblyResult = result.Components.FirstOrDefault(x => x.ComponentID == assembly.ID && x.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(assemblyResult);
            Assert.IsNotNull(assemblyResult.ChildComponents);
            Assert.AreEqual(1, assemblyResult.ChildComponents.Count);

            ComponentPriceResult childAssembly = assemblyResult.ChildComponents.FirstOrDefault(x => x.ComponentID == assembly2.ID && x.ComponentType == OrderItemComponentType.Assembly);
            Assert.IsNotNull(childAssembly);
            Assert.AreEqual(childAssembly.CostOV, assembly2UnitCostOV);
            if (assembly2UnitCostOV)
            {
                if (!childAssembly.RollupLinkedPriceAndCost.GetValueOrDefault(true))
                    Assert.AreEqual(childAssembly.CostUnit, assembly2UnitCost);
                else
                    Assert.IsNull(childAssembly.CostUnit);
            }
        }
    }
}
