﻿using Endor.CBEL.Interfaces;
using Endor.CBEL.ObjectGeneration;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Pricing.Tests.Helper;
using Endor.Tenant;
using Endor.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests
{
    [TestClass]
    public class CBELSyntaxTests
    {
        private ApiContext ctx = null;

        private const short BID = 1;

        [TestInitialize]
        public async Task InitializePricingTests()
        {
            ctx = PricingTestHelper.GetMockCtx(BID);
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestCleanup]
        public async Task CleanupDefaultPricingTests()
        {
            await PricingTestHelper.Cleanup(ctx, BID);
        }

        [TestMethod]
        public void InvalidNameInFormulaTest()
        {
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable assemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable assemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable assemblyTotalPriceTimes2 = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "TotalPriceTimes2",
                Label = "Total Price Times 2",
                DataType = DataType.Number,
                DefaultValue = "=xTotalPrice * 2",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=Quantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyHeight,
                    assemblyWidth,
                    assemblyTotalPriceTimes2,
                    assemblyPrice
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

            Assert.AreEqual(1, assemblyGen.Errors.Count);
            Assert.AreEqual("Location:(1:23)-Unknown property xTotalPrice or xTotalPrice is an object reference without a property.", assemblyGen.Errors[0].Message);
        }

        [TestMethod]
        public void PriceInFormulaTest()
        {
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable assemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable assemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable assemblyPriceTimes2 = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "PriceTimes2",
                Label = "Price Times 2",
                DataType = DataType.Number,
                DefaultValue = "=Price * 2",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=Quantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyHeight,
                    assemblyWidth,
                    assemblyPriceTimes2,
                    assemblyPrice
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

            Assert.AreEqual(1, assemblyGen.Errors.Count);
            Assert.AreEqual("Location:(1:17)-Price variable reference does not include a property.", assemblyGen.Errors[0].Message);
        }

        [TestMethod]
        public void TotalPriceInFormulaTest()
        {
            int assemblyID = PricingTestHelper.NextAssemblyID(ctx, BID);
            string assemblyName = "Assembly" + DateTime.Now.ToString("yyyyMMdd_HHmm");

            ITenantDataCache cache = new TestHelper.MockTenantDataCache();

            AssemblyVariable assemblyHeight = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Height",
                Label = "Height",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable assemblyWidth = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Width",
                Label = "Width",
                DataType = DataType.Number,
                DefaultValue = "0",
                ElementType = AssemblyElementType.Number,
                UnitID = Unit.Inch,
                IsFormula = false,
            };

            AssemblyVariable assemblyTotalPriceTimes2 = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "TotalPriceTimes2",
                Label = "TotalPrice Times 2",
                DataType = DataType.Number,
                DefaultValue = "=TotalPrice * 2",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };

            AssemblyVariable assemblyPrice = new AssemblyVariable()
            {
                ID = PricingTestHelper.NextAssemblyVariableID(ctx, BID),
                BID = BID,
                Name = "Price",
                Label = "Total Retail Price",
                DataType = DataType.Number,
                DefaultValue = "=Quantity.Value * Height.Value * Width.Value",
                ElementType = AssemblyElementType.Number,
                AllowDecimals = true,
                IsFormula = true,
            };


            AssemblyData assembly = new AssemblyData()
            {
                BID = BID,
                ID = assemblyID,
                Name = assemblyName,
                Variables = new List<AssemblyVariable>()
                {
                    assemblyHeight,
                    assemblyWidth,
                    assemblyTotalPriceTimes2,
                    assemblyPrice
                }
            };

            ctx.AssemblyData.Add(assembly);
            ctx.SaveChanges();

            CBELAssemblyGenerator assemblyGen = CBELAssemblyGenerator.Create(ctx, assembly);

            Assert.AreEqual(0, assemblyGen.Errors.Count);
        }
    }
}
