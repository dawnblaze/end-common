﻿using Endor.AzureStorage;
using Endor.CBEL.Common;
using Endor.CBEL.Grammar;
using Endor.DocumentStorage.Models;
using Endor.EF;
using Endor.Level3.Tests.Common;
using Endor.Models;
using Endor.Tenant;
using Irony.Parsing;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.Pricing.Tests.Helper
{
    public static class PricingTestHelper
    {
        public static ApiContext GetMockCtx(short bid)
        {
            var opts = new DbContextOptions<ApiContext>()
            {

            };
            var cache = new TestHelper.MockTenantDataCache();
            var queuer = new TestHelper.MockTaskQueuer();
            bool.TryParse(TestHelper.MockTenantDataCache.LocalSetting("Assemblies:SaveCopyToFile"), out bool saveCopyToFile);
            CBELAssemblyHelper.SaveCopyToFile = saveCopyToFile;

            bool.TryParse(TestHelper.MockTenantDataCache.LocalSetting("Assemblies:SaveCopyToUniqueFileName"), out bool saveCopyToUniqueFileName);
            CBELAssemblyHelper.SaveCopyToUniqueFileName = saveCopyToUniqueFileName;

            CBELAssemblyHelper.SaveToDirectory = TestHelper.MockTenantDataCache.LocalSetting("Assemblies:SaveToDirectory");

            ApiContext ctx = new ApiContext(opts, cache, bid);
            new MigrationHelper(queuer).MigrateDb(ctx);
            return ctx;
        }

        public async static Task CleanupPreviousUsage(short bid, int id, ApiContext apiContext, ITenantDataCache cache)
        {
            AssemblyData assemblyData = apiContext.AssemblyData
                            .Include(t => t.Variables)
                            .Include(t => t.AssemblyCategoryLinks)
                            .Include(t => t.Layouts)
                            .Include(t => t.Tables)
                            .Include("AssemblyCategoryLinks.AssemblyCategory")
                            .FirstOrDefault(t => t.BID == bid && t.ID == id);

            if (assemblyData != null)
            {
                apiContext.AssemblyTable.RemoveRange(assemblyData.Tables);
                apiContext.AssemblyVariable.RemoveRange(assemblyData.Variables);
                apiContext.AssemblyCategoryLink.RemoveRange(assemblyData.AssemblyCategoryLinks);
                apiContext.AssemblyCategory.RemoveRange(assemblyData.AssemblyCategoryLinks.Select(t => t.AssemblyCategory));
                apiContext.AssemblyLayout.RemoveRange(assemblyData.Layouts);
                apiContext.AssemblyData.Remove(assemblyData);

                apiContext.SaveChanges();

                EntityStorageClient client = new EntityStorageClient((await cache.Get(bid)).StorageConnectionString, bid);
                var assemblyDoc = new DMID() { id = assemblyData.ID, ctid = ClassType.Assembly.ID() };
                string assemblyName = CBELAssemblyHelper.AssemblyName(bid, assemblyData.ID, assemblyData.ClassTypeID, 1);

                await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.cs");
            }
        }

        public static async Task CleanupEntities(short bid, params object[] entities)
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            ApiContext ctx = new ApiContext(cache, bid);

            foreach (var item in entities.OfType<AssemblyData>())
            {
                if (item.AssemblyType == AssemblyType.Product)
                {
                    string assemblyName = CBELAssemblyHelper.AssemblyName(bid, item.ID, ClassType.Assembly.ID(), 1);
                    EntityStorageClient entityStorageClient = new EntityStorageClient((await cache.Get(bid)).StorageConnectionString, bid);
                    var dmid = new DMID() { id = item.ID, ctid = ClassType.Assembly.ID() };
                    await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, dmid, $"bin/{assemblyName}.dll");
                    await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, dmid, $"source/{assemblyName}.cs");
                }
            }

            foreach (var item in entities.OfType<MachineData>())
            {

                string assemblyName = CBELAssemblyHelper.AssemblyName(bid, item.ID, ClassType.Machine.ID(), 1);
                EntityStorageClient entityStorageClient = new EntityStorageClient((await cache.Get(bid)).StorageConnectionString, bid);
                var dmid = new DMID() { id = item.ID, ctid = ClassType.Machine.ID() };
                await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, dmid, $"bin/{assemblyName}.dll");
                await entityStorageClient.DeleteFile(StorageBin.Permanent, Bucket.Data, dmid, $"source/{assemblyName}.cs");
            }

            foreach (var item in entities)
            {
                System.Reflection.PropertyInfo BIDprop = item.GetType().GetProperty("BID");
                System.Reflection.PropertyInfo IDprop = item.GetType().GetProperty("ID");

                var foundItem = ctx.Find(item.GetType(), (short)BIDprop.GetValue(item), IDprop.GetValue(item));
                if (foundItem != null)
                    ctx.Remove(foundItem);
            }

            int deletedItems = ctx.SaveChanges();
        }

        public static Parser DefaultParser = new Parser(new CBELGrammar());
        public static ParseResponse CheckFormula(string formula, Parser parser = null)
        {
            Parser myParser = parser ?? DefaultParser;

            if (String.IsNullOrWhiteSpace(formula))
            {
                throw new ArgumentNullException(nameof(formula));
            }

            if (formula[0] == '=')
                formula = formula.Substring(1);

            if (String.IsNullOrWhiteSpace(formula))
            {
                throw new ArgumentException("Formula must not be empty", nameof(formula));
            }

            return Endor.CBEL.Grammar.Util.CheckParseErrors(myParser.Parse(formula));
        }


        public static int GetTestID<T>(this DbSet<T> dbset, short BID) where T: class, IAtom<int>
        {
            int testID = -99;
            var entities = dbset.Where(t => t.BID == BID);
            if (entities.Any())
            {
                testID = entities.Min(t => t.ID) - 1;

                if (testID > -99)
                    testID = -99;
            }
            return testID;
        }

        public static short GetTestIDShort<T>(this DbSet<T> dbset, short BID) where T : class, IAtom<short>
        {
            short testID = -99;
            var entities = dbset.Where(t => t.BID == BID);
            if (entities.Any())
            {
                testID = entities.Min(t => t.ID);

                if (testID > -99)
                    testID = -99;
            }
            return testID;
        }


        private static async Task RemoveTransheaderItems(ApiContext ctx, short bid, int orderID)
        {
            List<OrderItemData> items = await ctx.OrderItemData.Where(t => t.BID == bid && t.OrderID == orderID).ToListAsync();

            foreach (OrderItemData orderItem in items)
            {
                ctx.OrderItemSurcharge.RemoveRange(await ctx.OrderItemSurcharge.Where(t => t.BID == bid && t.OrderItemID == orderItem.ID).ToListAsync());
                await ctx.SaveChangesAsync();

                ctx.OrderNote.RemoveRange(await ctx.OrderNote.Where(t => t.BID == bid && t.OrderItemID == orderItem.ID).ToListAsync());
                await ctx.SaveChangesAsync();

                foreach (OrderItemComponent component in await ctx.OrderItemComponent.Where(t => t.BID == bid && t.OrderID == orderID).ToListAsync())
                {
                    ctx.OrderItemTaxAssessment.RemoveRange(await ctx.OrderItemTaxAssessment.Where(x => x.BID == bid && x.ItemComponentID == component.ID).ToListAsync());
                    ctx.OrderItemComponent.Remove(component);
                }
                await ctx.SaveChangesAsync();

                ctx.OrderItemData.Remove(orderItem);
                await ctx.SaveChangesAsync();
            }

            foreach (OrderEmployeeRole role in await ctx.OrderEmployeeRole.Where(t => t.BID == bid && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderEmployeeRole.Remove(role);
            }
            await ctx.SaveChangesAsync();

            foreach (OrderKeyDate keydate in await ctx.OrderKeyDate.Where(t => t.BID == bid && t.OrderID == orderID).ToListAsync())
            {
                ctx.OrderKeyDate.Remove(keydate);
            }
            await ctx.SaveChangesAsync();
        }

        public static async Task Cleanup(ApiContext ctx, short bid)
        {
            ITenantDataCache cache = new TestHelper.MockTenantDataCache();
            EntityStorageClient client = new EntityStorageClient((await cache.Get(bid)).StorageConnectionString, bid);

            List<AssemblyTable> _assemblyTables = await ctx.AssemblyTable.Where(t => t.BID == bid && (t.ID < 0 || t.AssemblyID < 0)).ToListAsync();
            if (_assemblyTables.Count > 0)
            {
                ctx.AssemblyTable.RemoveRange(_assemblyTables);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<AssemblyVariableFormula> _assemblyVariableFormulas = await ctx.AssemblyVariableFormula.Where(t => t.BID == bid && (t.ID < 0 || t.VariableID < 0)).ToListAsync();
            if (_assemblyVariableFormulas.Count > 0)
            {
                ctx.AssemblyVariableFormula.RemoveRange(_assemblyVariableFormulas);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<AssemblyVariable> _assemblyVariables = await ctx.AssemblyVariable.Where(t => t.BID == bid && (t.ID < 0 || t.AssemblyID < 0 || t.LinkedMaterialID < 0)).ToListAsync();
            if (_assemblyVariables.Count > 0)
            {
                ctx.AssemblyVariable.RemoveRange(_assemblyVariables);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<OrderItemComponent> _orderItemComponents = await ctx.OrderItemComponent.Where(t => t.BID == bid && (t.AssemblyID < 0 || t.ID < 0)).ToListAsync();
            if (_orderItemComponents.Count > 0)
            {
                ctx.OrderItemComponent.RemoveRange(_orderItemComponents);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<MachineProfileTable> _machineProfileTables = await ctx.MachineProfileTable.Where(t => t.BID == bid && (t.ID < 0 || t.ProfileID < 0 || t.TableID < 0)).ToListAsync();
            if (_machineProfileTables.Count > 0)
            {
                ctx.MachineProfileTable.RemoveRange(_machineProfileTables);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<MachineProfile> _machineProfiles = await ctx.MachineProfile.Where(t => t.BID == bid && (t.ID < 0 || t.MachineID < 0 || t.MachineTemplateID < 0)).ToListAsync();
            if (_machineProfiles.Count > 0)
            {
                ctx.MachineProfile.RemoveRange(_machineProfiles);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            IEnumerable<AssemblyData> _assemblies = await ctx.AssemblyData.Where(t => t.BID == bid && t.ID < 0).ToListAsync();
            foreach (AssemblyData _assembly in _assemblies)
            {
                if (_assembly.AssemblyType == AssemblyType.MachineTemplate)
                {
                    IEnumerable<MachineData> _machineAssemblies = await ctx.MachineData.Where(t => t.BID == bid && t.MachineTemplateID == _assembly.ID).ToListAsync();

                    foreach (MachineData _machineAssembly in _machineAssemblies)
                    {
                        DMID assemblyDoc = new DMID() { id = _machineAssembly.ID, ctid = ClassType.Machine.ID() };
                        string assemblyName = CBELAssemblyHelper.AssemblyName(bid, _machineAssembly.ID, ClassType.Machine.ID(), 1);

                        await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                        await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
                    }

                    ctx.MachineData.RemoveRange(_machineAssemblies);
                    ctx.MachineProfile.RemoveRange(await ctx.MachineProfile.Where(t => t.BID == bid && t.MachineTemplateID == _assembly.ID).ToListAsync());
                }

                else
                {
                    DMID assemblyDoc = new DMID() { id = _assembly.ID, ctid = ClassType.Assembly.ID() };
                    string assemblyName = CBELAssemblyHelper.AssemblyName(bid, _assembly.ID, _assembly.ClassTypeID, 1);

                    await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"bin/{assemblyName}.dll");
                    await client.DeleteFile(StorageBin.Permanent, Bucket.Data, assemblyDoc, $"source/{assemblyName}.cs");
                }

                ctx.AssemblyData.Remove(_assembly);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }
            await ctx.SaveChangesAsync();

            List<MaterialCategoryLink> _partMaterialCategoryLinks = await ctx.MaterialCategoryLink.Where(t => t.BID == bid && t.PartID < 0).ToListAsync();
            if (_partMaterialCategoryLinks.Count > 0)
            {
                ctx.MaterialCategoryLink.RemoveRange(_partMaterialCategoryLinks);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<SurchargeDef> _surcharges = await ctx.SurchargeDef.Where(t => t.BID == bid && t.ID < 0).ToListAsync();
            if (_surcharges.Count > 0)
            {
                ctx.SurchargeDef.RemoveRange(_surcharges);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<TaxGroup> _taxGroups = await ctx.TaxGroup.Where(t => t.BID == bid && t.ID < 0).ToListAsync();
            if (_taxGroups.Count > 0)
            {
                ctx.TaxGroup.RemoveRange(_taxGroups);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }
            await ctx.SaveChangesAsync();

            List<CompanyData> _companies = await ctx.CompanyData.Where(t => t.BID == bid && t.ID < 0).ToListAsync();
            foreach (CompanyData _company in _companies)
            {
                List<OrderData> _orders = await ctx.OrderData.Where(t => t.BID == bid && t.CompanyID == _company.ID).ToListAsync();
                foreach (OrderData o in _orders)
                {
                    ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == bid && (x.OrderID == o.ID || x.LinkedOrderID == o.ID)).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderNote.RemoveRange(await ctx.OrderNote.Where(t => t.BID == bid && t.OrderID == o.ID && t.IsOrderNote).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == bid && x.OrderID == o.ID).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == bid && x.Parent.OrderID == o.ID).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == bid && x.OrderID == o.ID).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == bid && x.Order.ID == o.ID).ToListAsync());
                    await ctx.SaveChangesAsync();

                    await RemoveTransheaderItems(ctx, bid, o.ID);
                    await ctx.SaveChangesAsync();

                    ctx.OrderData.Remove(o);
                    await ctx.SaveChangesAsync();
                }


                List<EstimateData> _estimates = await ctx.EstimateData.Where(t => t.BID == bid && t.CompanyID == _company.ID).ToListAsync();
                foreach (EstimateData e in _estimates)
                {
                    ctx.OrderOrderLink.RemoveRange(await ctx.OrderOrderLink.Where(x => x.BID == bid && (x.OrderID == e.ID || x.LinkedOrderID == e.ID)).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderNote.RemoveRange(await ctx.OrderNote.Where(t => t.BID == bid && t.OrderID == e.ID && t.IsOrderNote).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderTagLink.RemoveRange(await ctx.OrderTagLink.Where(x => x.BID == bid && x.OrderID == e.ID).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderContactRole.RemoveRange(await ctx.OrderContactRole.Where(x => x.BID == bid && x.OrderID == e.ID).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderContactRoleLocator.RemoveRange(await ctx.OrderContactRoleLocator.Where(x => x.BID == bid && x.Parent.OrderID == e.ID).ToListAsync());
                    await ctx.SaveChangesAsync();

                    ctx.OrderCustomData.RemoveRange(await ctx.OrderCustomData.Where(x => x.BID == bid && x.Order.ID == e.ID).ToListAsync());
                    await ctx.SaveChangesAsync();

                    await RemoveTransheaderItems(ctx, bid, e.ID);
                    await ctx.SaveChangesAsync();

                    ctx.EstimateData.Remove(e);
                    await ctx.SaveChangesAsync();
                }
                await ctx.SaveChangesAsync();
            }
            ctx.CompanyData.RemoveRange(_companies);
            await ctx.SaveChangesAsync();

            List<MaterialData> _materials = await ctx.MaterialData.Where(t => t.BID == bid && t.ID < 0).ToListAsync();
            if (_materials.Count > 0)
            {
                ctx.MaterialData.RemoveRange(_materials);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<LaborData> _labors = await ctx.LaborData.Where(t => t.BID == bid && t.ID < 0).ToListAsync();
            if (_labors.Count > 0)
            {
                ctx.LaborData.RemoveRange(_labors);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }

            List<MachineData> _machines = await ctx.MachineData.Where(t => t.BID == bid && t.ID < 0).ToListAsync();
            if (_machines.Count > 0)
            {
                ctx.MachineData.RemoveRange(_machines);
                Assert.AreNotEqual(0, await ctx.SaveChangesAsync());
            }
        }

        private static int? minAssemblyID = null;
        public static int NextAssemblyID(ApiContext ctx, short bid)
        {
            if (!minAssemblyID.HasValue)
            {
                IEnumerable<AssemblyData> negAssemblies = ctx.AssemblyData.Where(x => x.BID == bid && x.ID < 0);

                if (negAssemblies.Any())
                    minAssemblyID = negAssemblies.Min(x => x.ID);
                else
                    minAssemblyID = -99;
            }

            minAssemblyID--;

            return minAssemblyID.Value;
        }

        private static int? minAssemblyVariableID = null;
        public static int NextAssemblyVariableID(ApiContext ctx, short bid)
        {
            if (!minAssemblyVariableID.HasValue)
            {
                IEnumerable<AssemblyVariable> negAssemblies = ctx.AssemblyVariable.Where(x => x.BID == bid && x.ID < 0);

                if (negAssemblies.Any())
                    minAssemblyVariableID = negAssemblies.Min(x => x.ID);
                else
                    minAssemblyVariableID = -99;
            }

            minAssemblyVariableID--;

            return minAssemblyVariableID.Value;
        }

        private static int? minAssemblyVariableFormulaID = null;
        public static int NextAssemblyVariableFormulaID(ApiContext ctx, short bid)
        {
            if (!minAssemblyVariableFormulaID.HasValue)
            {
                IEnumerable<AssemblyVariableFormula> negAssemblies = ctx.AssemblyVariableFormula.Where(x => x.BID == bid && x.ID < 0);

                if (negAssemblies.Any())
                    minAssemblyVariableFormulaID = negAssemblies.Min(x => x.ID);
                else
                    minAssemblyVariableFormulaID = -99;
            }

            minAssemblyVariableFormulaID--;

            return minAssemblyVariableFormulaID.Value;
        }

        private static short? minAssemblyTableID = null;
        public static short NextAssemblyTableID(ApiContext ctx, short bid)
        {
            if (!minAssemblyTableID.HasValue)
            {
                IEnumerable<AssemblyTable> negAssemblies = ctx.AssemblyTable.Where(x => x.BID == bid && x.ID < 0);

                if (negAssemblies.Any())
                    minAssemblyTableID = negAssemblies.Min(x => x.ID);
                else
                    minAssemblyTableID = -99;
            }

            minAssemblyTableID--;

            return minAssemblyTableID.Value;
        }

        private static int? minLaborID = null;
        public static int NextLaborID(ApiContext ctx, short bid)
        {
            if (!minLaborID.HasValue)
            {
                IEnumerable<LaborData> negLabors = ctx.LaborData.Where(x => x.BID == bid && x.ID < 0);

                if (negLabors.Any())
                    minLaborID = negLabors.Min(x => x.ID);
                else
                    minLaborID = -99;
            }

            minLaborID--;

            return minLaborID.Value;
        }

        private static int? minMaterialID = null;
        public static int NextMaterialID(ApiContext ctx, short bid)
        {
            if (!minMaterialID.HasValue)
            {
                IEnumerable<MaterialData> negMaterials = ctx.MaterialData.Where(x => x.BID == bid && x.ID < 0);

                if (negMaterials.Any())
                    minMaterialID = negMaterials.Min(x => x.ID);
                else
                    minMaterialID = -99;
            }

            minMaterialID--;

            return minMaterialID.Value;
        }

        private static short? minMachineID = null;
        public static short NextMachineID(ApiContext ctx, short bid)
        {
            if (!minMachineID.HasValue)
            {
                IEnumerable<MachineData> negMachines = ctx.MachineData.Where(x => x.BID == bid && x.ID < 0);

                if (negMachines.Any())
                    minMachineID = negMachines.Min(x => x.ID);
                else
                    minMachineID = -99;
            }

            minMachineID--;

            return minMachineID.Value;
        }

        private static short? minSurchargeID = null;
        public static short NextSurchargeID(ApiContext ctx, short bid)
        {
            if (!minSurchargeID.HasValue)
            {
                IEnumerable<SurchargeDef> negSurcharges = ctx.SurchargeDef.Where(x => x.BID == bid && x.ID < 0);

                if (negSurcharges.Any())
                    minSurchargeID = negSurcharges.Min(x => x.ID);
                else
                    minSurchargeID = -99;
            }

            minSurchargeID--;

            return minSurchargeID.Value;
        }

        public static LaborData InitLabor(ApiContext ctx, short bid, 
                                          string name, int expenseAccountID, int incomeAccountID,
                                          decimal priceFixed, decimal pricePerMinute,
                                          decimal costFixed, decimal costPerMinute, 
                                          bool addToContext = true)
        {
            int newID = NextLaborID(ctx, bid);

            LaborData result = new LaborData()
            {
                BID = bid,
                ID = newID,
                EstimatingPriceFixed = priceFixed,
                EstimatingPricePerHour = pricePerMinute * 60m,
                EstimatingCostFixed = costFixed,
                EstimatingCostPerHour = costPerMinute * 60m,
                Name = name,
                InvoiceText = name,
                ExpenseAccountID = expenseAccountID,
                IncomeAccountID = incomeAccountID
            };

            if (addToContext)
                ctx.LaborData.Add(result);

            return result;
        }

        public static MaterialData InitMaterial(ApiContext ctx, short bid, 
                                                string name, int expenseAccountID, int incomeAccountID, int inventoryAccountID, 
                                                decimal price, 
                                                decimal cost, 
                                                bool addToContext = true)
        {
            int newID = NextMaterialID(ctx, bid);

            MaterialData result = new MaterialData()
            {
                BID = bid,
                ID = newID,
                EstimatingPrice = price,
                EstimatingCost = cost,
                Name = name,
                InvoiceText = name,
                ExpenseAccountID = expenseAccountID,
                IncomeAccountID = incomeAccountID,
                InventoryAccountID = inventoryAccountID,
            };

            if (addToContext)
                ctx.MaterialData.Add(result);

            return result;
        }

        public static AssemblyData InitAssembly(ApiContext ctx, short bid,
                                                string name, int incomeAccountID,
                                                decimal price,
                                                bool addToContext = true)
        {
            int newID = NextAssemblyID(ctx, bid);

            AssemblyData result = new AssemblyData()
            {
                BID = bid,
                ID = newID,
                Name = name,
                InvoiceText = name,
                IncomeAccountID = incomeAccountID,
                IncomeAllocationType = AssemblyIncomeAllocationType.SingleIncomeAllocation,
                PricingType = AssemblyPricingType.MarginBasedPricing,
                PriceFormulaType = PriceFormulaType.Fixed,
                AssemblyType = AssemblyType.Product,
                FixedPrice = price,
            };

            if (addToContext)
                ctx.AssemblyData.Add(result);

            return result;
        }
        public static AssemblyVariable InitAssemblyVariable(ApiContext ctx, short bid,
                                                string name, int assemblyID,
                                                AssemblyElementType type,
                                                int? linkedAssemblyID = null,
                                                int? linkedMaterialID = null,
                                                int? linkedLaborID = null,
                                                bool addToContext = true,
                                                string defaultValue = null)
        {
            int newID = NextAssemblyID(ctx, bid);

            AssemblyVariable result = new AssemblyVariable()
            {
                BID = bid,
                ID = newID,
                Name = name,
                AssemblyID = assemblyID,
                ElementType = type,
                LinkedAssemblyID = linkedAssemblyID,
                LinkedMaterialID = linkedMaterialID,
                LinkedLaborID = linkedLaborID,
                DefaultValue = defaultValue
            };

            if (addToContext)
                ctx.AssemblyVariable.Add(result);

            return result;
        }

        public static SurchargeDef InitSurcharge(ApiContext ctx, short bid, 
                                                 string name, 
                                                 int incomeAccountID, short taxabilityCodeID,
                                                 decimal surchargePriceFixed, decimal surchargePriceEach,
                                                 bool addToContext = true)
        {
            short newID = NextSurchargeID(ctx, bid);

            SurchargeDef result = new SurchargeDef()
            {
                BID = bid,
                ID = newID,
                DefaultFixedFee = surchargePriceFixed,
                DefaultPerUnitFee = surchargePriceEach,
                Name = name,
                IncomeAccountID = incomeAccountID,
                IsActive = true,
                TaxCodeID = taxabilityCodeID,
            };

            if (addToContext)
                ctx.SurchargeDef.Add(result);

            return result;
        }
    }
}