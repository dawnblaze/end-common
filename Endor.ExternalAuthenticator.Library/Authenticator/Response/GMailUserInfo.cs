﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.ExternalAuthenticator.Authenticator.Response
{
    public class GMailUserInfo
    {
        public string Email { get; set; }
        public string ID { get; set; }
        public string Picture { get; set; }
        
    }
}
