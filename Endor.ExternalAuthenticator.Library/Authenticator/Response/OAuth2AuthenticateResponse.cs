﻿using System;

namespace Endor.ExternalAuthenticator.Authenticator.Response
{
    /// <summary>
    /// Authentication response object.
    /// </summary>
    public class OAuth2AuthenticateResponse
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public DateTime Expires { get; set; }
        public string State { get; set; }
    }
}
