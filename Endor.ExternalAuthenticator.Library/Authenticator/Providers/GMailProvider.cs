﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Endor.ExternalAuthenticator.Authenticator.Interfaces;
using Endor.ExternalAuthenticator.Authenticator.Response;
using Endor.ExternalAuthenticator.Helpers;
using Endor.ExternalAuthenticator.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.ExternalAuthenticator.Authenticator.Providers
{
    public class GmailProvider : EmailProviderBase
    {
        /// <summary>
        /// Initialize provider based on project's appsettings.json (e.g. Providers:Gmail:xxx)
        /// </summary>
        public GmailProvider() : base(new OAuth2Provider
        {
            ClientId = AppConfiguration.AppSetting["Providers:Gmail:clientID"],
            ClientSecret = AppConfiguration.AppSetting["Providers:Gmail:clientSecret"],
            Scope = AppConfiguration.AppSetting["Providers:Gmail:scope"],
            AuthUri = AppConfiguration.AppSetting["Providers:Gmail:authUri"],
            AccessTokenUri = AppConfiguration.AppSetting["Providers:Gmail:tokenUri"],
            UserInfoUri = AppConfiguration.AppSetting["Providers:Gmail:infoUri"],
            RevokeUri = AppConfiguration.AppSetting["Providers:Gmail:revokeUri"],
            CallbackUrl = AppConfiguration.AppSetting["Providers:Gmail:callbackUri"],
            EmailUri = AppConfiguration.AppSetting["Providers:Gmail:emailUri"],
            State = "gmail"
        })
        {

        }

        /// <summary>
        /// Initialize provider based on provided configuration (e.g. Providers:Gmail:xxx)
        /// </summary>
        /// <param name="appSetting"></param>
        public GmailProvider(IConfiguration appSetting) : base(new OAuth2Provider
        {
            ClientId = appSetting["Providers:Gmail:clientID"],
            ClientSecret = appSetting["Providers:Gmail:clientSecret"],
            Scope = appSetting["Providers:Gmail:scope"],
            AuthUri = appSetting["Providers:Gmail:authUri"],
            AccessTokenUri = appSetting["Providers:Gmail:tokenUri"],
            UserInfoUri = appSetting["Providers:Gmail:infoUri"],
            RevokeUri = appSetting["Providers:Gmail:revokeUri"],
            CallbackUrl = appSetting["Providers:Gmail:callbackUri"],
            EmailUri = appSetting["Providers:Gmail:emailUri"],
            State = "gmail"
        })
        {

        }

        /// <summary>
        /// Initialize provider based on details provided
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="scope"></param>
        /// <param name="authUri"></param>
        /// <param name="accessTokenUri"></param>
        /// <param name="userInfoUri"></param>
        /// <param name="revokeUri"></param>
        /// <param name="callbackUrl"></param>
        /// <param name="emailUri"></param>
        public GmailProvider(
            string clientId,
            string clientSecret,
            string scope,
            string authUri,
            string accessTokenUri,
            string userInfoUri,
            string revokeUri,
            string callbackUrl,
            string emailUri
        ) : base(new OAuth2Provider
        {
            ClientId = clientId,
            ClientSecret = clientSecret,
            Scope = scope,
            AuthUri = authUri,
            AccessTokenUri = accessTokenUri,
            UserInfoUri = userInfoUri,
            RevokeUri = revokeUri,
            CallbackUrl = callbackUrl,
            EmailUri = emailUri,
            State = "gmail"
        })
        {

        }

        public override string GetAuthenticationUrl()
        {
            return _provider.AuthUri
                   + "?redirect_uri=" + _provider.CallbackUrl
                   + "&response_type=code"
                   + "&client_id=" + _provider.ClientId
                   + "&scope=" + _provider.Scope
                   + "&approval_prompt=force&access_type=offline"
                   + "&state=gmail";
        }

        public async Task<UserInfo> GetUserInfoAsync(string accessToken)
        {
            var http = new HttpClient();

            http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var data = http.GetAsync(_provider.UserInfoUri).Result.Content.ReadAsStringAsync().Result;

            var info = JsonConvert.DeserializeObject<GMailUserInfo>(data);

            var response = new UserInfo
            {
                ID = info.ID,
                Email = info.Email,
                Name = "",
                Picture = info.Picture

            };
            return await Task.FromResult(response);
        }

        public override async Task<string> SendEmail(string refreshToken, Email email)
        {
            var token = RefreshToken(refreshToken);

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            var msg = new MailMessage
            {
                Body = email.Body,
                Subject = email.Subject,
                From = new MailAddress(email.Sender)
            };


            foreach (var recipient in email.Recipients)
            {
                if (!recipient.Equals(""))
                {
                    msg.To.Add(new MailAddress(recipient));
                }
            }

            var mimeMessage = MimeKit.MimeMessage.CreateFromMailMessage(msg);

            var data = Base64UrlEncode(mimeMessage.ToString());
            var postData = "{\"raw\":\"" + data + "\"}";
            var emailRequest = _provider.EmailUri + "?access_token="+ token.AccessToken;

            var request = WebRequest.Create(emailRequest);

            var data2 = Encoding.UTF8.GetBytes(postData);
            request.Method = "POST";
            request.ContentType = "application/json; charset=UTF-8";
            request.ContentLength = data2.Length;


            using (var stream = request.GetRequestStream())
            {
                stream.Write(data2, 0, data2.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var dataStream = response.GetResponseStream();

            var reader = new StreamReader(dataStream);
            var jsonString = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();

            return await Task.FromResult(jsonString);
        }

        private string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            // Special "url-safe" base64 encode.
            return Convert.ToBase64String(inputBytes)
                .Replace('+', '-')
                .Replace('/', '_')
                .Replace("=", "");
        }

        public override async Task<string> Revoke(string accessToken)
        {
            var http = new HttpClient();

            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("token", accessToken),

            });
            var response = await http.PostAsync(_provider.RevokeUri, formContent).ConfigureAwait(false);
            return await response.Content.ReadAsStringAsync();
        }

    }
}
