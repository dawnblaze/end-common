﻿using Endor.ExternalAuthenticator.Authenticator.Interfaces;
using Endor.ExternalAuthenticator.Authenticator.Response;
using Endor.ExternalAuthenticator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.ExternalAuthenticator.Authenticator.Providers
{
    public class MockProvider : EmailProviderBase, IEmailSender, IExternalAuthenticator
    {
        public MockProvider() : base(new OAuth2Provider
        {
            ClientId = "client-id",
            ClientSecret = "client-secret",
            Scope = "scope",
            AuthUri = "auth-uri",
            AccessTokenUri = "access-token-uri",
            UserInfoUri = "user-info-uri",
            RevokeUri = "revoke-uri",
            CallbackUrl = "callback-url",
            State = "mock"
        })
        {
        }

        public override string GetAuthenticationUrl()
        {
            return _provider.AuthUri;
        }

        public override Token GetToken(string code)
        {
            if(code == "auth-code")
            {
                return new Token
                {
                    AccessToken = "access-token",
                    RefreshToken = "refresh-token",
                    ExpiresIn = DateTime.UtcNow.AddDays(1).ToString()
                };
            }
            else
            {
                throw new InvalidOperationException("wrong authentication code");
            }
        }

        public override Token RefreshToken(string refreshToken)
        {
            if (refreshToken == "refresh-token")
            {
                return new Token
                {
                    AccessToken = "new-access-token",
                    RefreshToken = refreshToken,
                    ExpiresIn = DateTime.UtcNow.AddDays(1).ToString()
                };
            }
            else
            {
                throw new InvalidOperationException("wrong refresh token");
            }
        }

        public override Task<string> Revoke(string accessToken)
        {
            if (accessToken == "access-token")
            {
                return Task.Run(() => "success");
            }
            else
            {
                throw new InvalidOperationException("wrong access token");
            }
        }


        public async Task<UserInfo> GetUserInfoAsync(string accessToken)
        {
            if (accessToken == "access-token")
            {
                var response = new UserInfo
                {
                    ID = "1",
                    Email = "mock@dev.com",
                    Name = "mockery mocking",
                    Picture = ""
                };
                return await Task.FromResult(response);
            }
            else
            {
                throw new InvalidOperationException("wrong access token");
            }
        }

        public override Task<string> SendEmail(string refreshToken, Email email)
        {
            if (refreshToken == "refresh-token")
            {
                return Task.Run(() => "mail success");
            }
            else
            {
                return Task.Run(() => "mail failed");
            }
        }

        public override ValidToken Validate(string accessToken)
        {
            if (accessToken == "access-token")
            {
                return new ValidToken
                {
                    IsValid = true,
                    RequestDT = DateTime.UtcNow
                };
            } else
            {
                return new ValidToken
                {
                    IsValid = false,
                    RequestDT = DateTime.UtcNow
                };
            }
        }
    }
}
