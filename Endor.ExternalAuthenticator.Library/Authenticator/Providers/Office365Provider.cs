﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Endor.ExternalAuthenticator.Authenticator.Interfaces;
using Endor.ExternalAuthenticator.Authenticator.Response;
using Endor.ExternalAuthenticator.Helpers;
using Endor.ExternalAuthenticator.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Graph;
using Newtonsoft.Json;

namespace Endor.ExternalAuthenticator.Authenticator.Providers
{
    public class Office365Provider : EmailProviderBase, IEmailSender, IExternalAuthenticator
    {
        /// <summary>
        /// Initialize provider based on project's appsettings.json (e.g. Providers:Office365:xxx)
        /// </summary>
        /// <param name="appSetting"></param>
        public Office365Provider() : base(new OAuth2Provider
        {
            ClientId = AppConfiguration.AppSetting["Providers:Office365:clientID"],
            ClientSecret = AppConfiguration.AppSetting["Providers:Office365:clientSecret"],
            Scope = AppConfiguration.AppSetting["Providers:Office365:scope"],
            AuthUri = AppConfiguration.AppSetting["Providers:Office365:authUri"],
            AccessTokenUri = AppConfiguration.AppSetting["Providers:Office365:tokenUri"],
            UserInfoUri = AppConfiguration.AppSetting["Providers:Office365:infoUri"],
            RevokeUri = AppConfiguration.AppSetting["Providers:Office365:revokeUri"],
            CallbackUrl = AppConfiguration.AppSetting["Providers:Office365:callbackUri"],
            State = "office365"
        })
        {

        }

        /// <summary>
        /// Initialize provider based on provided configuration (e.g. Providers:Office365:xxx)
        /// </summary>
        /// <param name="appSetting"></param>
        public Office365Provider(IConfiguration appSetting) : base(new OAuth2Provider
        {
            ClientId = appSetting["Providers:Office365:clientID"],
            ClientSecret = appSetting["Providers:Office365:clientSecret"],
            Scope = appSetting["Providers:Office365:scope"],
            AuthUri = appSetting["Providers:Office365:authUri"],
            AccessTokenUri = appSetting["Providers:Office365:tokenUri"],
            UserInfoUri = appSetting["Providers:Office365:infoUri"],
            RevokeUri = appSetting["Providers:Office365:revokeUri"],
            CallbackUrl = appSetting["Providers:Office365:callbackUri"],
            State = "office365"
        })
        {

        }

        /// <summary>
        /// Initialize provider based on details provided
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <param name="scope"></param>
        /// <param name="authUri"></param>
        /// <param name="accessTokenUri"></param>
        /// <param name="userInfoUri"></param>
        /// <param name="revokeUri"></param>
        /// <param name="callbackUrl"></param>
        /// <param name="emailUri"></param>
        public Office365Provider(
            string clientId,
            string clientSecret,
            string scope,
            string authUri,
            string accessTokenUri,
            string userInfoUri,
            string revokeUri,
            string callbackUrl
        ) : base(new OAuth2Provider
        {
            ClientId = clientId,
            ClientSecret = clientSecret,
            Scope = scope,
            AuthUri = authUri,
            AccessTokenUri = accessTokenUri,
            UserInfoUri = userInfoUri,
            RevokeUri = revokeUri,
            CallbackUrl = callbackUrl,
            State = "office365"
        })
        {

        }

        public override string GetAuthenticationUrl()
        {
            var parameters = new Dictionary<string, string> {
                {"client_id", _provider.ClientId},
                {"display", "page"},
                {"locale", "en"},
                {"redirect_uri", _provider.CallbackUrl},
                {"response_type", "code"},
                {"scope", _provider.Scope },
                {"approval_prompt", "force" },
                {"access_type", "offline" },
            };

            var qs = parameters.Aggregate("", (c, p) => c + ("&" + p.Key + "=" + p.Value)).Substring(1);
            return _provider.AuthUri + "?" + qs;
        }

        public async Task<UserInfo> GetUserInfoAsync(string accessToken)
        {
            GraphServiceClient client = new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    (requestMessage) =>
                    {
                        requestMessage.Headers.Authorization =
                            new AuthenticationHeaderValue("Bearer", accessToken);

                        return Task.FromResult(0);
                    }
                )
            );

            var office356User = await client.Me.Request().GetAsync();
            var response = new UserInfo
            {
                ID = office356User.Id,
                Email = office356User.Mail,
                Name = office356User.DisplayName
            };
            return response;
        }

        public override async Task<string> SendEmail(string refreshToken, Email email)
        {
            var token = RefreshToken(refreshToken);


            GraphServiceClient client = new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    (requestMessage) =>
                    {
                        requestMessage.Headers.Authorization =
                            new AuthenticationHeaderValue("Bearer", token.AccessToken);

                        return Task.FromResult(0);
                    }
                )
            );

            try
            {
                var recipients = new List<Recipient>();
                foreach (var recipient in email.Recipients)
                {
                    if (!recipient.Equals(""))
                    {
                        recipients.Add(new Recipient { EmailAddress = new EmailAddress { Address = recipient } });
                    }
                }

                Message message = new Message
                {
                    Subject = email.Subject,
                    Body = new ItemBody { Content = email.Body, ContentType = BodyType.Text },
                    ToRecipients = recipients.ToArray()
                };
                var request = client.Me.SendMail(message, true);
                await request.Request().PostAsync();

                return "{\"statusCode\":200}";
            }
            catch (ServiceException ex)
            {
                return ex.Message;
            }
        }


        public override async Task<string> Revoke(string accessToken)
        {
            return await Task.FromResult("Not implementation");
        }

        public override ValidToken Validate(string accessToken)
        {
            GraphServiceClient client = new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    (requestMessage) =>
                    {
                        requestMessage.Headers.Authorization =
                            new AuthenticationHeaderValue("Bearer", accessToken);

                        return Task.FromResult(0);
                    }
                )
            );

            try
            {
                Task<User> task = Task.Run(async () => await client.Me.Request().GetAsync());
                var office356User = task.Result;
                
                return new ValidToken
                {
                    IsValid = office356User != null,
                    RequestDT = DateTime.UtcNow
                };
            }
            catch
            {
                return new ValidToken
                {
                    IsValid = false,
                    RequestDT = DateTime.UtcNow
                };
            }
        }
    }
}
