﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Endor.ExternalAuthenticator.Helpers
{
    public class AppConfiguration
    {
        public static IConfigurationRoot AppSetting { get; }
        static AppConfiguration()
        {
            IConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
   
            configurationBuilder.SetBasePath(Directory.GetCurrentDirectory());
            configurationBuilder.AddJsonFile("appsettings.json");

            string env = Environment.GetEnvironmentVariable("NETCORE_ENVIRONMENT");
            bool isDevelopment = string.IsNullOrEmpty(env) || env.ToLowerInvariant() == "development";

            if (isDevelopment)
            {
                configurationBuilder.AddUserSecrets<AppConfiguration>();
            }

            AppSetting = configurationBuilder.Build();
        }
    }
}
