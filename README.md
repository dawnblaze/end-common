# END.Common #

### Contents ###
END.Common contains shared code for the many servers and programs for Endor.

### Requirements ###

* VS 2017
* .NET 462
* .NET Core 1.1

### Publishing ###

*In order to publish, you need a MyGet API key. Please see a developer about obtaining this key.*

* Make your changes
* Open your csproj's Properties, open the Package tab, and update the version
  * Please use [SEMVER (Semantic Versioning)](http://semver.org/) when selecting a new version
* The "Generate NuGet package on build" option should be on - build the project to create a package at the new version
* Open a terminal to the solution folder
* Invoke `publish.cmd Endor.<package>/bin/Debug/Endor.<package>.<version_major>.<version_minor>.<version_patch>.nupkg <apikey>` to push to MyGet

An example of a publish command (with fake apikey)
```
publish.cmd Endor.Models/bin/Debug/Endor.Models.0.11.2.nupkg 6ffce60b-65c7-46f8-8f99-e8de2943e2c1
```