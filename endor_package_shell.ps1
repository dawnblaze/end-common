Write-Host "=======================" -BackgroundColor Yellow -ForegroundColor Black
Write-Host "= ENDOR PACKAGE SHELL =" -BackgroundColor Yellow -ForegroundColor Black
Write-Host "=======================" -BackgroundColor Yellow -ForegroundColor Black

$continue = $false
$team = & .\_scripts\_readChoice 'Team or Live' '&Team','&Live' '&Team' 'Do you want to work with team or live packages?'
Write-Host "Working with $team packages".replace('&', '') -BackgroundColor Yellow -ForegroundColor Black
$apikey = $env:NUGETAPIKEY;
if ($apikey -eq $null) {
	$apikey = Read-Host -Prompt "Please enter your Myget API Key"
} else {
	$useApikey = & .\_scripts\_readChoice 'Confirm Key' '&yes','&no' '&yes' "Do you want to use MyGet API Key $apikey ?"
	if ($useApikey -eq '&no') {
		$apikey = Read-Host -Prompt "Please enter your Myget API Key"
	}
}

do
{
Write-Host "Working with $team packages".replace('&', '') -BackgroundColor Yellow -ForegroundColor Black

$packages = & .\_scripts\_readChoice 'Choose Packages' '&Models','Level &2','Level &3','&Other','&EF' '&Other' 'Which package(s) do you want to work with?'

if ($packages -eq '&Other') {
	$packages = Read-Host -Prompt 'Please enter a valid package name (ex: Endor.Models)'
}

Write-Host "Working with $team $packages".replace('&', '') -BackgroundColor Yellow -ForegroundColor Black

$action = ''

if ($packages -eq '&Models' -Or $packages -eq 'Level &2') {
	$action = & .\_scripts\_readChoice 'Choose Action' 'Update&VersionAndPush','Install&ModelsOnLevel2','Push&ExistingPackage' 'Update&VersionAndPush' ('What do you want to do with '+$packages+'?')
} elseif ($packages -eq '&EF') {
	$action = & .\_scripts\_readChoice 'Choose Action' 'Update&VersionAndPush','Install&ModelsOnLevel2','&AddMigration','Push&ExistingPackage' 'Update&VersionAndPush' ('What do you want to do with '+$packages+'?')
} else {
	$action = & .\_scripts\_readChoice 'Choose Action' 'Update&VersionAndPush','Push&ExistingPackage','&InstallOnServers' 'Update&VersionAndPush' ('What do you want to do with '+$packages+'?')
}

Write-Host "Starting $action to $team $packages".replace('&', '') -BackgroundColor Yellow -ForegroundColor Black

if ($team -eq '&Live') {
	if ($action -eq 'Update&VersionAndPush') {
		#Update Version, Build, and Push
		if ($packages -eq '&Models') {
			& .\dev_update_models $apikey
		} elseif ($packages -eq 'Level &2') {
			& .\dev_update_level_2 $apikey
		} elseif ($packages -eq 'Level &3') {
			& .\dev_update_level_3 $apikey
		} elseif ($packages -eq '&EF') {
			& .\dev_update_project 'Endor.EF' $apikey
		} else {
			& .\dev_update_project $packages $apikey
		}
	} elseif ($action -eq 'Install&ModelsOnLevel2') {
		& .\dev_install_models_level_2 $apikey
	} elseif ($action -eq '&AddMigration') {
		$migrationName = Read-Host -Prompt 'Enter Migration Name'
		& pushd .\Endor.EF
		& dotnet ef migrations add $migrationName -s ..\Endor.EF.StartupEmulator\Endor.EF.StartupEmulator.csproj
		& popd
	} elseif ($action -eq '&InstallOnServers') {
		if ($packages -eq 'Endor.Pricing') {
			$version = Read-Host -Prompt 'Enter Version'
			& .\_scripts\_dev_server_update_package.ps1 (resolve-path '..\end-api\Endor.Api.Web\Endor.Api.Web.csproj') $packages $apikey $version
			& .\_scripts\_dev_server_update_package.ps1 (resolve-path '..\end-bgengine\Endor.BGEngine.Core.Web\Endor.BGEngine.Core.Web.csproj') $packages $apikey $version
			& .\_scripts\_dev_server_build_and_commit.ps1 (resolve-path '..\end-api\Endor.Api.Web\Endor.Api.Web.csproj') $packages $version
			& .\_scripts\_dev_server_build_and_commit.ps1 (resolve-path '..\end-bgengine\Endor.BGEngine.Core.Web\Endor.BGEngine.Core.Web.csproj') $packages $version
		} else {
			Write-Host "This action is currently unsupported :(" -BackgroundColor Red -ForegroundColor White
		}
	} else {
		#Push Existing Package
		if ($packages -eq '&Models') {
			& .\dev_push_latest_package 'Endor.Models' $apikey
		} elseif ($packages -eq 'Level &2') {
			& .\dev_push_latest_level_2 $apikey
		} elseif ($packages -eq 'Level &3') {
			& .\dev_push_latest_level_3 $apikey
		} elseif ($packages -eq '&EF') {
			& .\dev_push_latest_package 'Endor.EF' $apikey
		} else {
			& .\dev_push_latest_package $packages $apikey
		}
	}
} else {
	#Team packages
	if ($action -eq 'Update&VersionAndPush') {
		if ($packages -eq '&Models') {
			& .\team_update_models $apikey
		} elseif ($packages -eq 'Level &2') {
			& .\team_update_level_2	$apikey
		} elseif ($packages -eq 'Level &3') {
			& .\team_update_level_3	$apikey	
		} elseif ($packages -eq '&EF') {
			& .\team_update_project 'Endor.EF' $apikey
		} else {
			& .\team_update_project $packages $apikey
		}
	} elseif ($action -eq 'Install&ModelsOnLevel2') {
		& .\team_install_models_level_2 $apikey	
	} elseif ($action -eq '&AddMigration') {
		$migrationName = Read-Host -Prompt 'Enter Migration Name'
		& pushd .\Endor.EF
		& dotnet ef migrations add $migrationName -s ..\Endor.EF.StartupEmulator\Endor.EF.StartupEmulator.csproj
		& popd
	} elseif ($action -eq '&InstallOnServers') {
		Write-Host "This action is currently unsupported :(" -BackgroundColor Red -ForegroundColor White
	} else {
		#Push Existing Package
		if ($packages -eq '&Models') {
			& .\team_push_latest_package 'Endor.Models' $apikey
		} elseif ($packages -eq 'Level &2') {
			Write-Host "This action is currently unsupported :(" -BackgroundColor Red -ForegroundColor White
		} elseif ($packages -eq 'Level &3') {
			Write-Host "This action is currently unsupported :(" -BackgroundColor Red -ForegroundColor White
		} elseif ($packages -eq '&EF') {
			& .\team_push_latest_package 'Endor.EF' $apikey
		} else {
			& .\team_push_latest_package $packages $apikey
		}
	}
}

Write-Host "Task Complete" -BackgroundColor Yellow -ForegroundColor Black

$continue = & .\_scripts\_readChoice 'Next Action' '&yes','&no' '&no' "Do you want to execute more $team package actions?"

if ($continue -eq '&yes') {
	$continue = $true;
}
}
while ($continue -eq $true)
Write-Host "Goodbye!" -BackgroundColor Yellow -ForegroundColor Black