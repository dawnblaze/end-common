param (
   [Parameter(Mandatory)][string]$project,
   [Parameter(Mandatory)][string]$mygetkey,
   [Parameter(Mandatory)][string]$teamname,
   [Parameter()][bool]$dotnet = $true
)
if (!$folderName) { $folderName = $project }

Write-Host "| $project |"
Write-Host "|-- LATEST LIVE VERSION --|"
& nuget list $project -source https://www.myget.org/F/corebridgeshared/auth/$mygetkey/api/v2 -prerelease
Write-Host "|-- LATEST TEAM VERSIONS --|"
& nuget list $project -source https://www.myget.org/F/corebridgeteam$teamname/auth/$mygetkey/api/v2 -prerelease -allVersions
Write-Host "|-------------------------|"

if ($dotnet) {
	$newVersion = Read-Host -Prompt 'Input new version, including pre-release tag'
	$yn = & .\_scripts\_readChoice 'Confirm Package Version' '&yes','&no' '&no' ('Confirm '+$project+' v'+$newVersion+'?')
	
	switch($yn) 
	{
		'&yes' 
		{
			$fullPath = (Get-Item -Path ./$folderName/$project.csproj -Verbose).FullName
			& ./_setAssemblyVersion.ps1 $fullPath $newVersion
		}
		'&no'
		{
			return $null;
		}
	}
	& dotnet build ./$folderName/$project.csproj
	& ./team_push_latest_package.ps1 $project $mygetkey $teamname $build ($project + '.' + $newVersion + '.nupkg')
	
	$tag = $project+' v'+$newVersion
	$tag | Clip
	Write-Host -Prompt "Commit message fragment $tag copied to clipboard, please paste into commit message:" -BackgroundColor DarkGreen -ForegroundColor White
	Read-Host -Prompt 'Press return to continue'
} else {
	Read-Host -Prompt 'Update version in VS, then press enter'
	Read-Host -Prompt 'Build project in VS, then press enter to publish'
	& ./team_push_latest_package.ps1 $project $mygetkey $teamname $build
}

Write-Host ""
