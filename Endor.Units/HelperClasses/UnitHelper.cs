﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Endor.Units
{
    public static class UnitHelper
    {
        public static UnitInfo Info(this Unit u)
        {
            return UnitInfo.InfoListSorted[u];
        }

        public static string Name(this Unit u, bool UKEnglish = false)
        {
            return Info(u).Name(UKEnglish);
        }

        public static decimal? ConvertTo(this Unit u, decimal? value, Unit ToUnit, Action<string> errorProc = null) => Info(u).ConvertTo(value, ToUnit, errorProc);

        public static decimal? ConvertFrom(this Unit u, decimal? value, Unit FromUnit, Action<string> errorProc = null) => Info(u).ConvertFrom(value, FromUnit, errorProc);
    }

}
