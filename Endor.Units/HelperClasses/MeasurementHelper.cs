﻿namespace Endor.Units
{
    public static class MeasurementHelper
    {
        public static int CompareMeasurements(IMeasurement measurement1, IMeasurement measurement2)
        {
            if (measurement2 == null)
                return 1;

            if (measurement2.UnitType != measurement1.UnitType)
                throw new UnitConversionException($"Can not compare a {measurement2.UnitType.Name()} to a {measurement1.UnitType.Name()}");

            if (!measurement2.ValueInUnits.HasValue && !measurement1.ValueInUnits.HasValue)
                return 0;

            if (!measurement2.ValueInUnits.HasValue)
                return 1;

            if (!measurement1.ValueInUnits.HasValue)
                return -1;

            if (measurement2.Unit == measurement1.Unit)
                return measurement1.ValueInUnits.Value.CompareTo(measurement2.ValueInUnits.Value);

            decimal otherValue = measurement2.ConvertTo(measurement1.Unit).Value;
            return measurement1.ValueInUnits.Value.CompareTo(otherValue);
        }

        public static byte UnitDimension(IMeasurement measurement)
        {
            switch (measurement.Unit)
            {
                case Unit.SquareInch:
                case Unit.SquareFoot:
                case Unit.SquareYard:
                case Unit.SquareCentimeter:
                case Unit.SquareMeter:
                case Unit.SquareMillimeter:
                    return 2;
                case Unit.CubicInch:
                case Unit.CubicFoot:
                //case Unit.CubicYard:
                case Unit.CubicCentimeter:
                case Unit.CubicMeter:
                case Unit.CubicMillimeter:
                    return 3;
                default:
                    return 1;
            }
        }

    }
}
