﻿namespace Endor.Units
{
    public static class MeasurementExtension
    {
        public static decimal? ConvertTo(this IMeasurement measurement, Unit toUnit)
        {
            return measurement.Unit.ConvertTo(measurement.ValueInUnits, toUnit, measurement.LogConversionError);
        }

        public static UnitInfo UnitInfo(this IMeasurement measurement)
        {
            return measurement.Unit.Info();
        }

        public static UnitTypeInfo UnitTypeInfo(this IMeasurement measurement)
        {
            return measurement.UnitType.Info();
        }

        public static decimal? InInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Inch); }
        public static decimal? InFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Foot); }
        public static decimal? InYards(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Yard); }
        public static decimal? InMM(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Millimeter); }
        public static decimal? InCM(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Centimeter); }
        public static decimal? InMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Meter); }
        public static decimal? InMetres(this IMeasurement measurement) { return InMeters(measurement); }

        public static decimal? InSqInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareInch); }
        public static decimal? InSqFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareFoot); }
        public static decimal? InSqYards(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareYard); }
        public static decimal? InSqMM(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimeter); }
        public static decimal? InSqCM(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareCentimeter); }
        public static decimal? InSqMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMeter); }
        public static decimal? InSqMetres(this IMeasurement measurement) { return InSqMeters(measurement); }

        public static decimal? InCuInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicInch); }
        public static decimal? InCuFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicFoot); }
        public static decimal? InCuYards(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicYard); }
        public static decimal? InCuMM(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimeter); }
        public static decimal? InCuCM(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicCentimeter); }
        public static decimal? InCuMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMeter); }
        public static decimal? InCuMetres(this IMeasurement measurement) { return InCuMeters(measurement); }
        public static decimal? InHours(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Hour); }
        public static decimal? InMinutes(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Minute); }
        public static decimal? InSeconds(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Second); }

        public static decimal? InEach(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Each); }
        public static decimal? InBoxes(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Box); }
        public static decimal? InCartons(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Carton); }
        public static decimal? InImpressions(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Impression); }
        public static decimal? InPackages(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Package); }
        public static decimal? InPages(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Page); }
        public static decimal? InPallets(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Pallet); }
        public static decimal? InPieces(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Piece); }
        public static decimal? InReams(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Ream); }
        public static decimal? InRolls(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Roll); }
        public static decimal? InSets(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Set); }
        public static decimal? InSheets(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Sheet); }
        public static decimal? InMillimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Millimeter); }
        public static decimal? InCentimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Centimeter); }
        public static decimal? InSquareInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareInch); }
        public static decimal? InSquareFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareFoot); }
        public static decimal? InSquareYards(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareYard); }
        public static decimal? InSquareMillimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimeter); }
        public static decimal? InSquareCentimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareCentimeter); }
        public static decimal? InSquareMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMeter); }
        public static decimal? InCubicInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicInch); }
        public static decimal? InCubicFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicFoot); }
        public static decimal? InCubicYards(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicYard); }
        public static decimal? InCubicMillimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimeter); }
        public static decimal? InCubicCentimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicCentimeter); }
        public static decimal? InCubicMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMeter); }
        public static decimal? InOuncesSolid(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OunceSolid); }
        public static decimal? InCups(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Cup); }
        public static decimal? InPints(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Pint); }
        public static decimal? InQuarts(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Quart); }
        public static decimal? InGallons(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Gallon); }
        public static decimal? InMilliliters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Milliliter); }
        public static decimal? InLiters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Liter); }
        public static decimal? InOuncesFluid(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OunceFluid); }
        public static decimal? InPounds(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Pound); }
        public static decimal? InMilligrams(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Milligram); }
        public static decimal? InGrams(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Gram); }
        public static decimal? InKilograms(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Kilogram); }
        public static decimal? InBytes(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Byte); }
        public static decimal? InKiloBytes(this IMeasurement measurement) { return measurement.ConvertTo(Unit.KiloByte); }
        public static decimal? InMegabytes(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Megabyte); }
        public static decimal? InGigabytes(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Gigabyte); }
        public static decimal? InTerabytes(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Terabyte); }
        public static decimal? InDays(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Day); }
        public static decimal? InInchesPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.InchPerSecond); }
        public static decimal? InFeetPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.FootPerSecond); }
        public static decimal? InMillimetersPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MillimeterPerSecond); }
        public static decimal? InMetersPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MeterPerSecond); }
        public static decimal? InInchesPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.InchPerMinute); }
        public static decimal? InFeetPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.FootPerMinute); }
        public static decimal? InMillimetersPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MillimeterPerMinute); }
        public static decimal? InMetersPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MeterPerMinute); }
        public static decimal? InInchesPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.InchPerHour); }
        public static decimal? InFeetPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.FeetPerHour); }
        public static decimal? InMillimetersPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MillimetersPerHour); }
        public static decimal? InMetersPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MetersPerHour); }
        public static decimal? InSquareInchesPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareInchPerSecond); }
        public static decimal? InSquareFeetPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareFootPerSecond); }
        public static decimal? InSquareMillimetersPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimeterPerSecond); }
        public static decimal? InSquareMetersPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMeterPerSecond); }
        public static decimal? InSquareInchesPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareInchPerMinute); }
        public static decimal? InSquareFeetPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareFootPerMinute); }
        public static decimal? InSquareMillimetersPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimeterPerMinute); }
        public static decimal? InSquareMetersPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMeterPerMinute); }
        public static decimal? InSquareInchesPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareInchesPerHour); }
        public static decimal? InSquareFeetPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareFeetPerHour); }
        public static decimal? InSquareMillimetersPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimetersPerHour); }
        public static decimal? InSquareMetersPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMetersPerHour); }
        public static decimal? InCubicInchesPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicInchPerSecond); }
        public static decimal? InCubicFeetPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicFootPerSecond); }
        public static decimal? InCubicMillimetersPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimeterPerSecond); }
        public static decimal? InCubicMeterPerSeconds(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMeterPerSecond); }
        public static decimal? InCubicInchesPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicInchPerMinute); }
        public static decimal? InCubicFeetPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicFootPerMinute); }
        public static decimal? InCubicMillimetersPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimeterPerMinute); }
        public static decimal? InCubicMetersPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMeterPerMinute); }
        public static decimal? InCubicInchesPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicInchesPerHour); }
        public static decimal? InCubicFeetPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicFeetPerHour); }
        public static decimal? InCubicMillimetersPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimetersPerHour); }
        public static decimal? InCubicMetersPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMetersPerHour); }
        public static decimal? InOuncesPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSecond); }
        public static decimal? InGallonsPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSecond); }
        public static decimal? InMillilitersPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSecond); }
        public static decimal? InLitersPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSecond); }
        public static decimal? InOuncesPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerMinute); }
        public static decimal? InGallonsPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerMinute); }
        public static decimal? InMillilitersPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerMinute); }
        public static decimal? InLitersPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerMinute); }
        public static decimal? InOuncesPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncesPerHour); }
        public static decimal? InGallonsPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonsPerHour); }
        public static decimal? InMillilitersPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MillilitersPerHour); }
        public static decimal? InLitersPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LitersPerHour); }
        public static decimal? InOuncesPerSquareInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSquareInch); }
        public static decimal? InOuncesPerSquareFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSquareFoot); }
        public static decimal? InOuncesPerSquareMillimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSquareMillimeter); }
        public static decimal? InOuncesPerSquareMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSquareMeter); }
        public static decimal? InGallonsPerSquareInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSquareInch); }
        public static decimal? InGallonsPerSquareFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSquareFoot); }
        public static decimal? InGallonsPerSquareMillimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSquareMillimeter); }
        public static decimal? InGallonsPerSquareMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSquareMeter); }
        public static decimal? InMillilitersPerSquareInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSquareInch); }
        public static decimal? InMillilitersPerSquareFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSquareFoot); }
        public static decimal? InMillilitersPerSquareMillimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSquareMillimeter); }
        public static decimal? InMillilitersPerSquareMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSquareMeter); }
        public static decimal? InLitersPerSquareInches(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSquareInch); }
        public static decimal? InLitersPerSquareFeet(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSquareFoot); }
        public static decimal? InLitersPerSquareMillimeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSquareMillimeter); }
        public static decimal? InLitersPerSquareMeters(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSquareMeter); }

        public static decimal? InMillimetres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Milliliter); }
        public static decimal? InCentimetres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Centimeter); }
        public static decimal? InSquareMillimetres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimeter); }
        public static decimal? InSquareCentimetres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareCentimeter); }
        public static decimal? InSquareMetres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMeter); }
        public static decimal? InCubicMillimetres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimeter); }
        public static decimal? InCubicCentimetres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicCentimeter); }
        public static decimal? InCubicMetres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMeter); }

        public static decimal? InOunces(this IMeasurement measurement)
        {
            {
                if (measurement.Unit.Info().UnitType == UnitType.Volume_Liquid)
                    return InOuncesFluid(measurement);

                return InOuncesSolid(measurement);
            }
        }

        public static decimal? InLitres(this IMeasurement measurement) { return measurement.ConvertTo(Unit.Liter); }
        public static decimal? InMillimetresPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MillimeterPerSecond); }
        public static decimal? InMetresPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MeterPerSecond); }
        public static decimal? InMillimetresPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MillimeterPerMinute); }
        public static decimal? InMetresPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MeterPerMinute); }
        public static decimal? InMillimetresPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MillimetersPerHour); }
        public static decimal? InMetresPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MetersPerHour); }
        public static decimal? InSquareMillimetresPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimeterPerSecond); }
        public static decimal? InSquareMetresPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMeterPerSecond); }
        public static decimal? InSquareMillimetresPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimeterPerMinute); }
        public static decimal? InSquareMetresPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMeterPerMinute); }
        public static decimal? InSquareMillimetresPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMillimetersPerHour); }
        public static decimal? InSquareMetresPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.SquareMetersPerHour); }
        public static decimal? InCubicMillimetresPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimeterPerSecond); }
        public static decimal? InCubicMetersPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMeterPerSecond); }
        public static decimal? InCubicMetresPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMeterPerSecond); }
        public static decimal? InCubicMillimetresPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimeterPerMinute); }
        public static decimal? InCubicMetresPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMeterPerMinute); }
        public static decimal? InCubicMillimetresPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMillimetersPerHour); }
        public static decimal? InCubicMetresPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.CubicMetersPerHour); }
        public static decimal? InLitresPerSecond(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSecond); }
        public static decimal? InLitresPerMinute(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerMinute); }
        public static decimal? InLitresPerHour(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LitersPerHour); }
        public static decimal? InOuncesPerSquareInch(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSquareInch); }
        public static decimal? InOuncesPerSquareFoot(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSquareFoot); }
        public static decimal? InOuncesPerSquareMillimeter(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSquareMillimeter); }
        public static decimal? InOuncesPerSquareMeter(this IMeasurement measurement) { return measurement.ConvertTo(Unit.OuncePerSquareMeter); }
        public static decimal? InGallonsPerSquareInch(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSquareInch); }
        public static decimal? InGallonsPerSquareFoot(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSquareFoot); }
        public static decimal? InGallonsPerSquareMillimeter(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSquareMillimeter); }
        public static decimal? InGallonsPerSquareMeter(this IMeasurement measurement) { return measurement.ConvertTo(Unit.GallonPerSquareMeter); }
        public static decimal? InMillilitersPerSquareInch(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSquareInch); }
        public static decimal? InMillilitersPerSquareFoot(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSquareFoot); }
        public static decimal? InMillilitersPerSquareMillimeter(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSquareMillimeter); }
        public static decimal? InMillilitersPerSquareMeter(this IMeasurement measurement) { return measurement.ConvertTo(Unit.MilliliterPerSquareMeter); }
        public static decimal? InLitersPerSquareInch(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSquareInch); }
        public static decimal? InLitersPerSquareFoot(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSquareFoot); }
        public static decimal? InLitersPerSquareMillimeter(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSquareMillimeter); }
        public static decimal? InLitersPerSquareMeter(this IMeasurement measurement) { return measurement.ConvertTo(Unit.LiterPerSquareMeter); }
    }
}