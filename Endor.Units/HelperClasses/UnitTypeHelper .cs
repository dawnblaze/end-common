﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Endor.Units
{
    public static class UnitTypeHelper
    {
        public static UnitTypeInfo Info(this UnitType ut)
        {
            return UnitTypeInfo.InfoList.Where(li => li.UnitType == ut).FirstOrDefault();
        }

        public static List<UnitInfo> Units(this UnitType ut) => Info(ut).Units;

        public static string Name(this UnitType ut)
        {
            return Info(ut).Name;
        }
    }

}
