﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Units
{
    [Flags]
    public enum SystemOfUnits:byte
    {
        None = 0,
        Metric = 1,
        ISU = 1,  // the real name for metric is International System of Units
        Imperial = 2,
        MetricAndImperial = 3,
    }
}
