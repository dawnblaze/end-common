﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Units
{
    public enum UnitType:byte
    {
        General = 0,
        Currency = 1,
        Percent = 2,
        Per_Thousand = 3,
        Custom = 5,
        Discrete = 9,
        Length = 11,
        Area = 12,
        Volume_Solid = 13,
        Volume_Liquid = 14,
        Weight_Mass = 15,
        DigitalStorage = 18,
        Time = 19,
        Speed_Length = 21,
        Speed_Area = 22,
        Speed_Solid = 23,
        Speed_Liquid = 24,
        Coverage = 25
    }
}
