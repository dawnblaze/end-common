using Endor.ExternalAuthenticator.Authenticator.Response;
using Endor.ExternalAuthenticator.Controllers;
using Endor.ExternalAuthenticator.Helpers;
using Endor.ExternalAuthenticator.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Endor.ExternalAuthenticator.Tests
{
    [TestClass]
    public class AuthenticationControllerTests
    {
        const string PROVIDER_MOCK = "mock";
        const string PROVIDER_GMAIL = "gmail";
        const string PROVIDER_OFFICE365 = "office365";

        const string MOCK_AUTH_URI = "auth-uri";
        const string MOCK_AUTH_CODE = "auth-code";
        const string MOCK_ACCESS_TOKEN = "access-token";
        const string MOCK_REFRESHED_ACCESS_TOKEN = "new-access-token";
        const string MOCK_REFRESH_TOKEN = "refresh-token";

        AuthenticationController controller = new AuthenticationController();

        [TestMethod]
        public void TestGetAuthenticationUrl()
        {
            // test gmail
            //var result = controller.GetAuthenticationUrl(PROVIDER_GMAIL);
            //Assert.IsTrue(result.Contains(AppConfiguration.AppSetting["Providers:Gmail:authURL"]));

            //// test microsoft
            //result = controller.GetAuthenticationUrl(PROVIDER_OFFICE365);
            //Assert.IsTrue(result.Contains(AppConfiguration.AppSetting["Providers:Office365:authURL"]));

            var result = controller.GetAuthenticationUrl(PROVIDER_MOCK);
            Assert.IsTrue(result.Contains(MOCK_AUTH_URI));
        }

        [TestMethod]
        public void TestGetToken()
        {
            var now = DateTime.Now;
            var result = controller.GetToken(PROVIDER_MOCK, MOCK_AUTH_CODE);
            Assert.IsTrue(result.AccessToken == MOCK_ACCESS_TOKEN, "does not match expected mock access token");
            Assert.IsTrue(result.RefreshToken == MOCK_REFRESH_TOKEN, "does not match expected mock refresh token");
            Assert.IsTrue(now.CompareTo(Convert.ToDateTime(result.ExpiresIn)) < 0, "expiry is less than or equal to current date time");
        }

        [TestMethod]
        public void TestRefreshToken()
        {
            var now = DateTime.Now;
            var result = controller.RefreshToken(PROVIDER_MOCK, MOCK_REFRESH_TOKEN);
            Assert.IsTrue(result.AccessToken == MOCK_REFRESHED_ACCESS_TOKEN, "does not match expected mock new access token");
            Assert.IsTrue(result.RefreshToken == MOCK_REFRESH_TOKEN, "does not match expected mock refresh token");
            Assert.IsTrue(now.CompareTo(Convert.ToDateTime(result.ExpiresIn)) < 0, "expiry is less than or equal to current date time");
            
            //result = controller.RefreshToken(PROVIDER_MOCK, "wrong-refresh-token");
            
        }

        [TestMethod]
        public void TestValidateToken()
        {
            var result = controller.ValidateToken(PROVIDER_MOCK, MOCK_ACCESS_TOKEN);
            Assert.IsTrue(result.IsValid, "token is not valid");
            result = controller.ValidateToken(PROVIDER_MOCK, "wrong-access-token");
            Assert.IsFalse(result.IsValid, "token is valid");
        }

        [TestMethod]
        public async Task TestRevokeTokenAsync()
        {
            var task = await controller.RevokeToken(PROVIDER_MOCK, MOCK_ACCESS_TOKEN);
            var result = ModelFromActionResult<string>(task);
            Assert.IsTrue(result == "success", "does not match expected mock access token");
        }

        [TestMethod]
        public async Task TestSendMail()
        {
            var email = new Email
            {
                Subject = "email subject",
                Sender = "sender@mock.com",
                Body = "The quick brown fox jumps over the lazy dog",
                Recipients = new string[]
                {
                    "receiver@mock.com"
                }
            };

            var result = await controller.SendMail(email, PROVIDER_MOCK, MOCK_REFRESH_TOKEN);
            Assert.IsTrue(result == "mail success", "mail failed");

            result = await controller.SendMail(email, PROVIDER_MOCK, "wrong refresh token");
            Assert.IsTrue(result == "mail failed", "mail succeeded");
        }

        private T ModelFromActionResult<T>(IActionResult actionResult)
        {
            object model;
            if (actionResult.GetType() == typeof(OkObjectResult))
            {
                OkObjectResult okResult = (OkObjectResult)actionResult;
                model = okResult.Value;
            }
            else
            {
                throw new InvalidOperationException(string.Format("Actionresult of type {0} is not supported by ModelFromResult extractor.", actionResult.GetType()));
            }
            T typedModel = (T)model;
            return typedModel;
        }
    }
}
