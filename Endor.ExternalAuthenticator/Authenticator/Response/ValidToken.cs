﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.ExternalAuthenticator.Authenticator.Response
{
    public class ValidToken
    {
        public bool IsValid { get; set; }

        public DateTime RequestDT { get; set; }
    }
}
