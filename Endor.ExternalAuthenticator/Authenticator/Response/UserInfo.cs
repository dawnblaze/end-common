﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.ExternalAuthenticator.Authenticator.Response
{
    public class UserInfo
    {
        public string ID { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string Picture { get; set; }
    }
}
