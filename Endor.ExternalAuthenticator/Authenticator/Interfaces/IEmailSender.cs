﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Endor.ExternalAuthenticator.Models;

namespace Endor.ExternalAuthenticator.Authenticator.Interfaces
{
    public interface IEmailSender
    {
        Task<string> SendEmail(string accessToken, Email email);
    }
}
