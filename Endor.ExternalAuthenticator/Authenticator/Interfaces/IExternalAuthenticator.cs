﻿using Endor.ExternalAuthenticator.Authenticator.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Endor.ExternalAuthenticator.Authenticator.Interfaces
{
    public interface IExternalAuthenticator
    {
        string GetAuthenticationUrl();
        Token GetToken(string code);
        Token RefreshToken(string code);
        ValidToken Validate(string accessToken);
        Task<string> Revoke(string accessToken);
    }
}
