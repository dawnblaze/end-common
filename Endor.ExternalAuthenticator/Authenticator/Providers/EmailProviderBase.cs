﻿using Endor.ExternalAuthenticator.Authenticator.Response;
using Endor.ExternalAuthenticator.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Endor.ExternalAuthenticator.Authenticator.Interfaces;
using Endor.ExternalAuthenticator.Models;
using Newtonsoft.Json;

namespace Endor.ExternalAuthenticator.Authenticator.Providers
{
    public abstract class EmailProviderBase :IEmailSender, IExternalAuthenticator
    {
        protected OAuth2Provider _provider;

        public EmailProviderBase(OAuth2Provider provider)
        {
            _provider = provider;

        }

        public abstract string GetAuthenticationUrl();



        public virtual Token GetToken(string code)
        {
            var parameters = new Dictionary<string, string> {
                {"client_id", _provider.ClientId},
                {"client_secret", _provider.ClientSecret},
                {"redirect_uri", _provider.CallbackUrl},
                {"state", _provider.State},
                {"scope", _provider.Scope},
                {"code", code},
                {"grant_type", "authorization_code"}
            };

            var reply = Request( _provider.AccessTokenUri, payload: parameters.Aggregate("", (c, p) => c + ("&" + p.Key + "=" + p.Value)).Substring(1));
            var response = InterpretReply(reply);

           return new Token
            {
                AccessToken = response.AccessToken,
                ExpiresIn = response.ToString(),
                RefreshToken = response.RefreshToken
            };
        }

        public virtual Token RefreshToken(string refreshToken)
        {

            var parameters = new Dictionary<string, string> {
                {"client_id", _provider.ClientId},
                {"client_secret", _provider.ClientSecret},
                {"refresh_token", refreshToken},
                {"grant_type", "refresh_token"},
                {"scope", _provider.Scope},
                {"state", _provider.State},
            };

            var reply = Request(_provider.AccessTokenUri, payload: parameters.Aggregate("", (c, p) => c + ("&" + p.Key + "=" + p.Value)).Substring(1));
            var response = InterpretReply(reply);

            return new Token
            {
                AccessToken = response.AccessToken,
                ExpiresIn = response.Expires.ToString(),
                RefreshToken = refreshToken
            };

        }

        public virtual ValidToken Validate(string accessToken)
        {
            try
            {

                var parameters = new Dictionary<string, string> {
                    {"access_token", accessToken}
                };

                var result = Request(_provider.UserInfoUri, "GET", payload: parameters.Aggregate("", (c, p) => c + (" &" + p.Key + "=" + p.Value)).Substring(1));

                return new ValidToken
                {
                    IsValid = !string.IsNullOrEmpty(result),
                    RequestDT = DateTime.UtcNow
                };

            }
            catch (Exception e)
            {
                return new ValidToken
                {
                    IsValid = false,
                    RequestDT = DateTime.UtcNow
                };
            }
        }

        public abstract Task<string> Revoke(string accessToken);



        public abstract Task<string> SendEmail(string accessToken, Email email);



        private string Request(string uri, string method = "POST", string payload = null, string token = null)
        {
            if (method == "GET" &&
                !string.IsNullOrWhiteSpace(payload))
                uri += "?" + payload;

            var request = WebRequest.Create(uri) as HttpWebRequest;

            if (request == null)
                throw new WebException("Could not create WebRequest.");

            request.Method = method;
            request.ContentType = "application/x-www-form-urlencoded;";
            request.Expect = null;

            if (!string.IsNullOrEmpty(token))
            {
                request.ContentType = "application/json;";
                request.Accept = "application/json;";
                request.Headers.Add("Authorization", $"Bearer {token}");
            }

            Stream stream;

            if (method == "POST" &&
                !string.IsNullOrWhiteSpace(payload))
            {
                var buffer = Encoding.UTF8.GetBytes(payload);

                request.ContentLength = buffer.Length;
                stream = request.GetRequestStream();

                stream.Write(buffer, 0, buffer.Length);
                stream.Close();
            }
            else
            {
                request.ContentLength = 0;
            }

            var response = request.GetResponse() as HttpWebResponse;

            if (response == null)
                throw new WebException("Could not get response from request.");

            stream = response.GetResponseStream();

            if (stream == null)
                throw new WebException("Could not get stream from response.");

            var reader = new StreamReader(stream);
            var body = reader.ReadToEnd();

            reader.Close();
            stream.Close();

            return body;
        }

        private OAuth2AuthenticateResponse InterpretReply(string reply)
        {
            var response = new OAuth2AuthenticateResponse();

            if (reply.StartsWith("{"))
            { // JSON
                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(reply);

                if (dict.ContainsKey("access_token")) response.AccessToken = dict["access_token"];
                if (dict.ContainsKey("refresh_token")) response.RefreshToken = dict["refresh_token"];
                if (dict.ContainsKey("state")) response.State = dict["state"];

                var seconds = 0;

                if (dict.ContainsKey("expires")) int.TryParse(dict["expires"], out seconds);
                if (dict.ContainsKey("expires_in")) int.TryParse(dict["expires_in"], out seconds);

                if (seconds > 0)
                    response.Expires = DateTime.Now.AddSeconds(seconds);
            }
            else if (reply.Contains('&'))
            { // QueryString
                var dict = reply.Split('&');

                foreach (var entry in dict)
                {
                    var index = entry.IndexOf('=');

                    if (index == -1)
                        continue;

                    var key = entry.Substring(0, index);
                    var value = entry.Substring(index + 1);

                    switch (key)
                    {
                        case "access_token":
                            response.AccessToken = value;
                            break;

                        case "refresh_token":
                            response.RefreshToken = value;
                            break;

                        case "state":
                            response.State = value;
                            break;

                        case "expires":
                        case "expires_in":
                            int seconds;

                            if (int.TryParse(value, out seconds))
                                response.Expires = DateTime.Now.AddSeconds(seconds);

                            break;
                    }
                }
            }

            return response;
        }

    }
}
