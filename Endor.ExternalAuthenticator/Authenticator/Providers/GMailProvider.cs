﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Endor.ExternalAuthenticator.Authenticator.Interfaces;
using Endor.ExternalAuthenticator.Authenticator.Response;
using Endor.ExternalAuthenticator.Helpers;
using Endor.ExternalAuthenticator.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.ExternalAuthenticator.Authenticator.Providers
{
    public class GmailProvider : EmailProviderBase
    {
        public GmailProvider ():base(new OAuth2Provider
            {
                ClientId = AppConfiguration.AppSetting["Providers:Gmail:clientID"],
                ClientSecret = AppConfiguration.AppSetting["Providers:Gmail:clientSecret"],
                Scope = AppConfiguration.AppSetting["Providers:Gmail:scope"],
                AuthUri = AppConfiguration.AppSetting["Providers:Gmail:authUri"],
                AccessTokenUri = AppConfiguration.AppSetting["Providers:Gmail:tokenUri"],
                UserInfoUri = AppConfiguration.AppSetting["Providers:Gmail:infoUri"],
                RevokeUri = AppConfiguration.AppSetting["Providers:Gmail:revokeUri"],
                CallbackUrl = AppConfiguration.AppSetting["Providers:Gmail:callbackUri"],
                EmailUri = AppConfiguration.AppSetting["Providers:Gmail:emailUri"],
                State = "gmail"
        })
        {
            
        }

        public override string GetAuthenticationUrl()
        {
            return _provider.AuthUri
                   + "?redirect_uri=" + _provider.CallbackUrl
                   + "&response_type=code"
                   + "&client_id=" + _provider.ClientId
                   + "&scope=" + _provider.Scope
                   + "&approval_prompt=force&access_type=offline"
                   + "&state=gmail";
        }

        public async Task<ActionResult<UserInfo>> GetUserInfoAsync(string accessToken)
        {
            var http = new HttpClient();

            http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);

            var data = http.GetAsync(_provider.UserInfoUri).Result.Content.ReadAsStringAsync().Result;

            var info = JsonConvert.DeserializeObject<GMailUserInfo>(data);

            var response = new UserInfo
            {
                ID = info.ID,
                Email = info.Email,
                Name = "",
                Picture = info.Picture

            };
            return response;
        }

        public override async Task<string> SendEmail(string refreshToken, Email email)
        {
            var token = RefreshToken(refreshToken);

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            
            var msg = new AE.Net.Mail.MailMessage
            {
                Subject = email.Subject,
                Body = email.Body,
                From = new MailAddress(email.Sender)
            };

            foreach (var recipient in email.Recipients)
            {
                if (!recipient.Equals(""))
                {
                    msg.To.Add(new MailAddress(recipient));
                }
            }
            msg.ReplyTo.Add(msg.From); 
            var msgStr = new StringWriter();
            msg.Save(msgStr);

            var data = Base64UrlEncode(msgStr.ToString());
            var postData = "{\"raw\":\"" + data + "\"}";
            var emailRequest = _provider.EmailUri + "?access_token="+ token.AccessToken;

            var request = WebRequest.Create(emailRequest);

            var data2 = Encoding.UTF8.GetBytes(postData);
            request.Method = "POST";
            request.ContentType = "application/json; charset=UTF-8";
            request.ContentLength = data2.Length;


            using (var stream = request.GetRequestStream())
            {
                stream.Write(data2, 0, data2.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var dataStream = response.GetResponseStream();

            var reader = new StreamReader(dataStream);
            var jsonString = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();

            return jsonString;

        }

        private string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            // Special "url-safe" base64 encode.
            return Convert.ToBase64String(inputBytes)
                .Replace('+', '-')
                .Replace('/', '_')
                .Replace("=", "");
        }

        public override async Task<string> Revoke(string accessToken)
        {
            var http = new HttpClient();

            var formContent = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("token", accessToken),

            });
            var response = await http.PostAsync(_provider.RevokeUri, formContent).ConfigureAwait(false);
            return await response.Content.ReadAsStringAsync();
        }

    }
}
