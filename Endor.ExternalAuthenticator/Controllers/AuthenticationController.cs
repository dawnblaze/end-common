﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Endor.ExternalAuthenticator.Authenticator.Providers;
using Endor.ExternalAuthenticator.Authenticator.Response;
using Endor.ExternalAuthenticator.Helpers;
using Endor.ExternalAuthenticator.Models;
using Microsoft.AspNetCore.Mvc;

namespace Endor.ExternalAuthenticator.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly MockProvider _mockProvider = new MockProvider();
        private readonly GmailProvider _gmailProvider = new GmailProvider();
        private readonly Office365Provider _office365Provider = new Office365Provider();

        [HttpGet("authurl")]
        public string GetAuthenticationUrl([FromQuery] string provider)
        {
            switch (provider.ToLower())
            {
                case "mock":
                    return _mockProvider.GetAuthenticationUrl();
                case "gmail":
                    return _gmailProvider.GetAuthenticationUrl();
                    //break;
                case "office365":
                default:
                    return _office365Provider.GetAuthenticationUrl();
                    //break;
            }
        }

        [HttpGet("info")]
        public async Task<ActionResult<UserInfo>> GetUserInfo([FromQuery] string provider, [FromQuery] string access_token)
        {
            switch (provider.ToLower())
            {
                case "mock":
                    return await _mockProvider.GetUserInfoAsync(access_token);
                case "gmail":
                    return await _gmailProvider.GetUserInfoAsync(access_token);
                //break;
                case "office365":
                default:
                    return await _office365Provider.GetUserInfoAsync(access_token);
                    //break;
            }
        }

        [HttpGet("token")]
        public Token GetToken([FromQuery] string provider, [FromQuery] string code)
        {
            switch (provider.ToLower())
            {
                case "mock":
                    return _mockProvider.GetToken(code);
                case "gmail":
                    return _gmailProvider.GetToken(code);
                //break;
                case "office365":
                default:
                    return _office365Provider.GetToken(code);
                    //break;
            }
        }

        [HttpGet("refreshtoken")]
        public Token RefreshToken([FromQuery] string provider, [FromQuery] string refresh_token)
        {
            switch (provider.ToLower())
            {
                case "mock":
                    return _mockProvider.RefreshToken(refresh_token);
                case "gmail":
                    return _gmailProvider.RefreshToken(refresh_token);
                //break;
                case "office365":
                default:
                    return _office365Provider.RefreshToken(refresh_token);
                    //break;
            }
        }

        [HttpGet("validatetoken")]
        public ValidToken ValidateToken([FromQuery] string provider, [FromQuery] string access_token)
        {
            switch (provider.ToLower())
            {
                case "mock":
                    return _mockProvider.Validate(access_token);
                case "gmail":
                    return _gmailProvider.Validate(access_token);
                //break;
                case "office365":
                default:
                    return _office365Provider.Validate(access_token);
                    //break;
            }
        }

        [HttpGet("revoketoken")]
        public async Task<IActionResult> RevokeToken([FromQuery] string provider, [FromQuery] string access_token)
        {
            switch (provider.ToLower())
            {
                case "mock":
                    return Ok(await _mockProvider.Revoke(access_token));
                case "gmail":
                    return Ok(await _gmailProvider.Revoke(access_token));
                //break;
                case "office365":
                default:
                    // as of 11/7/2019, there is no way to revoke an access token for office365
                    return NotFound();
                    //break;
            }
        }

        [HttpPost("sendmail")]
        public async Task<string> SendMail([FromBody]Email email, [FromQuery] string provider, [FromQuery] string refresh_token)
        {
            switch (provider.ToLower())
            {
                case "mock":
                    return await _mockProvider.SendEmail(refresh_token, email);
                case "gmail":
                    return await _gmailProvider.SendEmail(refresh_token, email);
                //break;
                case "office365":
                default:
                    return await _office365Provider.SendEmail(refresh_token, email);
                    //break;
            }
        }
    }
}
