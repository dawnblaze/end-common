﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Filters;

namespace Endor.ExternalAuthenticator.SwaggerExamples
{
    public class ListResponseExample : IExamplesProvider
    {
        /// <inheritdoc />
        public object GetExamples()
        {
            return new List<Object>
            {
                new {ID = new Random().Next()},
                new {ID = new Random().Next()}
            };
        }
    }
}