﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Swashbuckle.AspNetCore.Filters;

namespace Endor.ExternalAuthenticator.SwaggerExamples
{
    public class ResponseExample : IExamplesProvider
    {
        /// <inheritdoc />
        public object GetExamples()
        {
            return new
            {
                ID = new Random().Next(),
            };
        }
    }
}
