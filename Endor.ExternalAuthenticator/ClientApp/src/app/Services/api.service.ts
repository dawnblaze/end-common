

import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpClient } from "@angular/common/http";
import { AuthService } from 'src/app/services/auth.service'
import { EmailModel } from 'src/app/models';


@Injectable()
export class ApiService {

    constructor(
        private httpClient: HttpClient, private authService: AuthService) {
    }

    getEmailInfo():Observable<any> {
        return this.httpClient.get("/api/authentication/info?provider=" + this.authService.provider + "&access_token=" + this.authService.token.accessToken);
    }

    revoketoken():Observable<any> {
        return this.httpClient.get("/api/authentication/revoketoken?provider=" + this.authService.provider + "&access_token=" + this.authService.token.refreshToken);
    }


    getToken(code: string):Observable<any> {
        return this.httpClient.get("/api/authentication/token?provider=" + this.authService.provider + "&code=" + code);
    }

    send(email: EmailModel):Observable<any> {
        return this.httpClient.post("/api/authentication/sendmail?provider=" + this.authService.provider + "&refresh_token=" + this.authService.token.refreshToken, email);
    }

    validateToken():Observable<any> {
        return this.httpClient.get("/api/authentication/validatetoken?provider=" + this.authService.provider+ "&access_token=" + this.authService.token.accessToken);
    }

    refreshToken():Observable<any> {
        return this.httpClient.get("/api/authentication/refreshtoken?provider=" + this.authService.provider + "&refresh_token=" + this.authService.token.refreshToken);
    }

    authenticate(provider: string):Observable<any> {
        this.authService.provider = provider;
        return this.httpClient.get("/api/authentication/authurl?provider=" + provider, { responseType: 'text' });
    }
}
