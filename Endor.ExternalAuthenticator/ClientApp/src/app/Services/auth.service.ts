
import { Injectable } from '@angular/core';
import { TokenModel } from 'src/app/models';



@Injectable()
export class AuthService {
    public isLoggedIn: boolean;
    public loginRedirectUrl: string;
    public token: TokenModel;
    public provider: string;
    constructor() { }


}