import { NgModule } from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot } from '@angular/router';
import { InjectionToken } from '@angular/core';

import { OAuthCallbackComponent, NotFoundComponent, MainComponent, LoginComponent } from './Routes'
import { AuthGuardService } from './services';

const externalUrlProvider = new InjectionToken('externalUrlRedirectResolver');

const routes: Routes = [
    //{ path: '', component: MainComponent, canActivate: [AuthGuardService] },
    { path: '', component: MainComponent },
    // { path: 'externalRedirect', resolve: { url: externalUrlProvider }, component: NotFoundComponent },
    // { path: 'oauthcallback', component: OAuthCallbackComponent },
    // { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: externalUrlProvider,
      useValue: (route: ActivatedRouteSnapshot) => {

        const externalUrl = route.paramMap.get('externalUrl');
        window.open(externalUrl, '_self');
      },
    }
  ],
})
export class AppRoutingModule { }
