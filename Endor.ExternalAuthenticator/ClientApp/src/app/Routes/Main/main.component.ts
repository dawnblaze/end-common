import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService, AuthService } from 'src/app/services';
import { EmailInfoModel, EmailModel, TokenModel } from 'src/app/models';


@Component({
    selector: "main-page",
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit {
    private sub: any;
    public emailAddress: string;
    
    public emailInfo:EmailInfoModel;

    public email: EmailModel = new EmailModel();
    public recipients: string;
    public response: string = "";
    public tokens: string = "";
    constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService, public authService: AuthService, private router: Router) {
        
    }

    ngOnInit(): void {
        // this.emailInfo = null;
        // this.sub = this.activatedRoute.queryParams.subscribe(params => {
        //     if (params != null && Object.keys(params).length !== 0 ) {
        //         this.apiService.getEmailInfo().subscribe(data => {
        //             this.response = "Response from server:\n" + JSON.stringify(data, undefined, 2);
        //             this.emailInfo = new EmailInfoModel(data);
        //         });
        //     }
        // });
        // this.tokens = "Tokens:\n" + JSON.stringify(this.authService.token, undefined, 2);
    }

    getMailInfo() {
        this.emailInfo = null;
    
        this.apiService.getEmailInfo().subscribe(data => {
            this.response = "Response from server:\n" + JSON.stringify(data, undefined, 2);
            this.emailInfo = new EmailInfoModel(data);
        });

    }

    signout() {
        this.apiService.revoketoken().subscribe(data => {
            this.authService.isLoggedIn = false;
            this.authService.token = null;
//            this.router.navigate(["/login"], { queryParams: null })
        });
    }

    send() {
        this.response = "";
        var recipients = this.recipients.replace(/\s/g, '');
        this.email.recipients = recipients.split(",");
        if (this.emailInfo != null) {
            this.email.sender = this.emailInfo.email;
        }
        this.apiService.send(this.email).subscribe(data => {
            this.response = "Response from server:\n" + JSON.stringify(data, undefined, 2);
            this.recipients = "";
            this.email = new EmailModel();
        });
    }

    refreshToken() {
        this.apiService.refreshToken().subscribe(data => {
            this.response = "Response from server:\n" + JSON.stringify(data, undefined, 2);
            this.authService.token = new TokenModel(data);
            this.tokens = "Tokens:\n" + JSON.stringify(this.authService.token, undefined, 2);
        })
    }

    validateToken() {
        this.apiService.validateToken().subscribe(data => {
            this.response = "Response from server:\n" + JSON.stringify(data, undefined, 2);
        })
    }

    revokeToken() {
        this.apiService.revoketoken().subscribe(data => {
            this.authService.token = null;
            this.response = "Response from server:\n" + JSON.stringify(data, undefined, 2);
            this.tokens = "Tokens:\n" + JSON.stringify(this.authService.token, undefined, 2);
        })
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }


    public authenticate(provider: string) {
       
        this.apiService.authenticate(provider).subscribe(data => {

            var redirectUri = <any>data.match(new RegExp("redirect_uri=([^&#]*)"));
            if (redirectUri != null) {
                redirectUri = redirectUri[1];
            }

            var win = window.open(data, '_blank', 'toolbar=0,width=600,height=500');

            var that = this;
            var pollTimer = window.setInterval(function() { 
                try {

                    if (win.document.URL.indexOf(redirectUri) != -1) {
                        window.clearInterval(pollTimer);
                        var url =   win.document.URL;

                        var code = <any>url.match(new RegExp("code=([^&#]*)"));
                        if (code != null) {
                            code = code[1];
                        }

                        var state = <any>url.match(new RegExp("state=([^&#]*)"));
                        if (state != null) {
                            state = state[1];
                        }
                        that.authService.provider = state == null ? 'gmail' : state;
                        // when the user cancels, code will be null
                        if (code != null) {
                            that.apiService.getToken(code).subscribe(data => {
                                that.authService.token = new TokenModel(data);
                                that.authService.isLoggedIn = true;
                                that.getMailInfo();
                                that.tokens = "Tokens:\n" + JSON.stringify(that.authService.token, undefined, 2);
                            });
                        }
                        win.close();
                    }
                } catch(e) {

                }
            }, 500);

        });
        event.preventDefault();
    }



}
