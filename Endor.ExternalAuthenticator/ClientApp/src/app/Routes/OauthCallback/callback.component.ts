import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService, ApiService } from 'src/app/services';
import { TokenModel } from 'src/app/models';

@Component({
    selector: "activation-page",
    template: `<div>Getting access tokens!</div>`
})

export class OAuthCallbackComponent implements OnInit {
    private sub:any;
    constructor(private activatedRoute: ActivatedRoute, private apiService: ApiService, private router: Router, private authService: AuthService) {

    }

    ngOnInit(): void {
        this.sub = this.activatedRoute.queryParams.subscribe(params => {
            var code = "" + params['code'];
            var state = "" + params['state'];
            this.authService.provider = state;
            this.apiService.getToken(code).subscribe(data => {
                this.authService.token = new TokenModel(data);
                this.authService.isLoggedIn = true;
                this.router.navigate([""], { queryParams: data })
            });
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    
}
