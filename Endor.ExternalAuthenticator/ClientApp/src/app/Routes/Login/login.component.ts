import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ApiService } from 'src/app/services';

@Component({
  selector: "login-page",
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {
  


  constructor(private router: Router, private apiService: ApiService) {

  }



  public authenticate(provider: string) {
      this.apiService.authenticate(provider).subscribe(data => {
        this.router.navigate(['/externalRedirect', { externalUrl: data }], {
            skipLocationChange: true,
        });
      });
      event.preventDefault();
  }
}
