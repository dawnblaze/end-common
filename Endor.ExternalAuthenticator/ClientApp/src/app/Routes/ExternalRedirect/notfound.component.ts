import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";



@Component({
  selector: "external-page",
  template: `<div>Redirecting!</div>`
})

export class NotFoundComponent implements OnInit {
  private sub: any;


  constructor(private activatedRoute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.queryParams.subscribe(params => {


    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
