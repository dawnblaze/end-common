export class TokenModel{
  accessToken: string;
  expiresIn: string;
  refreshToken: string;

  public constructor(init?: Partial<TokenModel>) {
    Object.assign(this, init);
  }
}
