import { TrustedStyleString } from '@angular/core/src/sanitization/bypass';

export class EmailModel{
    subject:string;
    recipients: string[];
    sender:string;
    body:string;
    public constructor(init?:Partial<EmailModel>) {
        Object.assign(this, init);
        if (this.recipients == null) {
            this.recipients = [];
        }
    }
}
