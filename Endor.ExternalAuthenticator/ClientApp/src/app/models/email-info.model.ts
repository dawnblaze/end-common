export class EmailInfoModel{
    email: string;
    id:string;
    picture:string;
    verified_email: boolean;
    
    public constructor(init?:Partial<EmailInfoModel>) {
        Object.assign(this, init);
    }
}
