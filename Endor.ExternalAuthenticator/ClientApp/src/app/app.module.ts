import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiService, AuthGuardService, AuthService } from './services';
import { ActivatedRouteSnapshot } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { OAuthCallbackComponent, NotFoundComponent, MainComponent, LoginComponent } from './Routes'


const externalUrlProvider = new InjectionToken('externalUrlRedirectResolver');

@NgModule({
  declarations: [
    AppComponent,
    OAuthCallbackComponent,
    NotFoundComponent,
    MainComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [ApiService, AuthGuardService, AuthService,

    {
      provide: externalUrlProvider,
      useValue: (route: ActivatedRouteSnapshot) => {
        const externalUrl = route.paramMap.get('externalUrl');
        window.open(externalUrl, '_self');
      },
    }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
