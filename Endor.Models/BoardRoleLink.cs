﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public class BoardRoleLink
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short BoardID { get; set; }

        public short RoleID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeRole Role { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BoardDefinitionData BoardDefinition { get; set; }
    }
}
