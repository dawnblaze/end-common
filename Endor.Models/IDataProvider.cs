﻿using System;
using System.Data.Common;
using System.Linq;

namespace Endor.Models
{
    public interface IDataProvider
    {
        short BID { get; }

        DbConnection Connection { get; }
        IQueryable<T> GetData<T>() where T : class, IAtom;
        IQueryable<IAtom> GetData(Type dataType);
    }
}
