﻿namespace Endor.Models
{
    public enum EmailProviderType: byte
    {
        None = 0,
        GoogleGSuite = 1,
        Microsoft365 = 2,
        CustomSMTP =3
    }
}
