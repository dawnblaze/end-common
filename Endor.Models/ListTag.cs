﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class ListTag : IAtom<short>
    {
        public short ID { get; set; }

        [JsonIgnore]
        public short BID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if the record is a System Record. System Records can't be deleted or altered.
        /// </summary>
        public bool IsSystem { get; set; }

        /// <summary>
        /// The AssociatedClassTypeID this object referenced
        /// <para>This field is not used in MVP 1.</para>
        /// </summary>
        public int? AssociatedClassTypeID { get; set; }

        /// <summary>
        /// The Name of this Tag.
        /// </summary>
        [Required] 
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// The hex value of the color of this object
        /// </summary>
        [StringLength(8)]
        [Required]
        public string RGB { get; set; }

        /// <summary>
        /// A Flag indicating if this record is Deleted.
        /// <para>Deleted Records do not show in any listing, but may still be referenced by Tags applied before the record was deleted.</para>
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
