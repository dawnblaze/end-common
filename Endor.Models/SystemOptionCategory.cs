﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class SystemOptionCategory
    {
        public SystemOptionCategory()
        {
            SystemOptionDefinitions = new HashSet<SystemOptionDefinition>();
        }

        public short ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public short? SectionID { get; set; }
        public string Description { get; set; }
        public byte OptionLevels { get; set; }
        public bool IsHidden { get; set; }
        public string SearchTerms { get; set; }
        public int IsSystemOption { get; set; }
        public bool IsAssociationOption { get; set; }
        public bool IsBusinessOption { get; set; }
        public bool IsLocationOption { get; set; }
        public bool IsStorefrontOption { get; set; }
        public bool IsCompanyOption { get; set; }
        public bool IsContactOption { get; set; }
        public bool IsUserOption { get; set; }

        public SystemOptionSection Section { get; set; }

        public ICollection<SystemOptionDefinition> SystemOptionDefinitions { get; set; }
    }
}
