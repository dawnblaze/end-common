﻿
namespace Endor.Models
{
    public class TaxabilityCodeItemExemptionLink
    {
        public short BID { get; set; }

        public short TaxCodeID { get; set; }

        public short TaxItemID { get; set; }

        public TaxabilityCode TaxabilityCode { get; set; }

        public TaxItem TaxItem { get; set; }
    }
}
