﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Endor.Models
{
    /// <summary>
    /// The URLRegistration Object stores information associated with registered anonymous URLs.  
    /// </summary>
    public class URLRegistrationData : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDT { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsRevoked { get; set; }
        [JsonIgnore]
        public string PublishedURLRoot { get; set; }
        [JsonIgnore]
        public string PublishedURLBase { get; set; }
        [JsonIgnore]
        public Guid PublishedURLPath { get; set; }
        public string PublishedURLFull { get; set; }
        public string PublishedShortenedURL { get; set; }
        public URLRegistrationType RegistrationType { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? TargetClassTypeID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TargetID { get; set; }
        public string TargetURL { get; set; }
        public string TargetURLParameters { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TargetCompanyID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? TargetContactID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? TargetEmployeeID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime  ExpirationDT { get; set; }
        public int UseCount { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? FirstUseDT { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastUseDT { get; set; }
        [DefaultValue(1000000)]
        public int MaxUseCount { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? ExpireAfterUseDays { get; set; }
        public string MetaDataJSON { get; set; }

        public List<URLRegistrationLookupHistory> LookupHistory;
    }
}
