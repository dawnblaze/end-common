﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class EnumLocatorType
    {
        public EnumLocatorType()
        {
            
        }

        public byte ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public string ValidityRegEx { get; set; }
        public string PossibilityRegEx { get; set; }
        public byte? PossibilitySortOrder { get; set; }        
    }
}
