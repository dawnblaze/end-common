﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class PaymentApplication : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }
        //This column is always "Infinite" in the live table and get the current DT when moved to the archive table. 
        //That's how it keeps track when it is no longer the current record
        [JsonIgnore]
        public DateTime ValidToDT { get; set; }
        public byte LocationID { get; set; }
        public byte ReceivedLocationID { get; set; }
        public DateTime AccountingDT { get; set; }
        public int MasterID { get; set; }
        public decimal Amount { get; set; }
        public byte? CurrencyType { get; set; }
        public int PaymentMethodID { get; set; }
        public byte PaymentTransactionType { get; set; }
        public PaymentMethodType PaymentType { get; set; }
        [StringLength(50)]
        public string DisplayNumber { get; set; }
        public int? CompanyID { get; set; }
        public int? OrderID { get; set; }
        public decimal RefBalance { get; set; }
        public decimal? NonRefBalance { get; set; }
        [StringLength(100)]
        public string Notes { get; set; }
        public int? GLActivityID { get; set; }
        //refers to first ID of the Group
        public int ApplicationGroupID { get; set; }
        public int? AdjustedApplicationID { get; set; }
        public bool HasAdjustments { get; set; }
        [JsonProperty("MerchangtAccountID", NullValueHandling = NullValueHandling.Ignore)]
        public byte? MerchangtAccountID { get; set; }
        [StringLength(64)]
        [JsonProperty("TokenID", NullValueHandling = NullValueHandling.Ignore)]
        public string TokenID { get; set; }
        public short? EnteredByEmployeeID { get; set; }
        public int? EnteredByContactID { get; set; }
        public int? ReconciliationID { get; set; }

        public LocationData Location { get; set; }

        public PaymentMaster Master { get; set; }

        public EnumAccountingCurrencyType CurrencyTypeNavigation { get; set; }

        public PaymentMethod PaymentMethod { get; set; }

        public EnumAccountingPaymentTransactionType PaymentTransactionTypeNavigation { get; set; }

        public EnumPaymentMethodType PaymentTypeNavigation { get; set; }

        public CompanyData Company { get; set; }

        public TransactionHeaderData Order { get; set; }

        public ActivityGlactivity GLActivity { get; set; }

        public PaymentApplication AdjustedApplicationNavigation { get; set; }

        public EmployeeData EnteredByEmployee { get; set; }

        public ContactData EnteredByContact { get; set; }

    }
}
