﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Endor.Models
{
    [Table("Business.TimeZone.Link")]
    public class BusinessTimeZoneLink
    {
        [JsonIgnore]
        [Column("BID", TypeName = "smallint")]
        public short BID { get; set; }

        [Column("TimeZoneID", TypeName = "smallint")]
        public short TimeZoneID { get; set; }
    }
}
