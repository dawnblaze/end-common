﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class SimpleSystemListFilter : SimpleListItem<int>
    {
        public bool HasImage { get; set; }
        public int TargetClassTypeID { get; set; }
        public bool IsSubscribed { get; set; }
        public bool IsShownAsTab { get; set; }
        public bool IsFavorite { get; set; }
        public bool IsSystem { get; set; }
    }
}
