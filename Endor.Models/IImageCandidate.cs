﻿namespace Endor.Models
{
    public interface IImageCandidate
    {
        bool HasImage { get; set; }
    }
}
