﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum AssemblyType : byte
    {
        Product = 0,
        Embedded = 1,
        Destination = 2,
        MachineTemplate = 3
    }
    
    public class EnumAssemblyType
    {
        public AssemblyType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
