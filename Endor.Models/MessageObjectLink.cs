﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class MessageObjectLink
    {
        public short BID { get; set; }
        public int ID { get; set; }

        public int BodyID { get; set; }
        public int LinkedObjectClassTypeID { get; set; }
        public int LinkedObjectID { get; set; }
        public bool IsSourceOfMessage { get; set; }

        [StringLength(100)]
        public string DisplayText { get; set; }

    }
}
