﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum OrderDestinationType : byte
    {
        Other = 0,
        PickedUp = 1,
        ElectronicDelivery = 2,
        Delivered = 3,
        Installed = 4,
        OnSiteService = 5,
        Shipped = 11
    }

    public class EnumOrderDestinationType
    {
        public OrderDestinationType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public OrderTransactionType TransactionTypeSet { get; set; }

        [Required]
        public OrderTransactionLevel TransactionLevelSet { get; set; }

        public bool AppliesToEstimate { get; set; }

        public bool AppliesToOrder { get; set; }

        public bool AppliesToPO { get; set; }

        public bool IsHeaderLevel { get; set; }

        public bool IsDestinationLevel { get; set; }

        public bool IsItemLevel { get; set; }

    }
}
