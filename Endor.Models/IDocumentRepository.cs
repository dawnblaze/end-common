﻿using System;

namespace Endor.Models
{
    public interface IDocumentRepository
    {
        short BID { get; set; }
 
        int ClassTypeID { get; set; }
    }

    public interface IDocumentRepository<I> : IDocumentRepository where I : struct, IConvertible
    {
        I ID { get; set; }
    }
}
