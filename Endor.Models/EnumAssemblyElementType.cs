﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/881131620/AssemblyElementType+Enum
    /// </summary>
    public enum AssemblyElementType : byte
    {
        Group = 0,
        SingleLineLabel = 1,
        MultiLineLabel = 2,
        UrlLabel = 3,
        Spacer = 7,
        SingleLineText = 11,
        MultiLineString = 12,
        DropDown = 13,
        RadioGroup = 14,
        Number = 21,
        Checkbox = 31,
        Toggle = 32,
        DateTimePicker = 91,
        ShowHideGroup = 102,
        SplitColumn = 103,
        LinkedMaterial = 104,
        LinkedLabor = 105,
        LinkedAssembly = 106,
        LinkedMachine = 107,
        Section = 121,
        LayoutComputationAssembly = 108,
        DetailFormButton = 111,
        LayoutVisualizer = 112
    }

    public class EnumAssemblyElementType
    {
        public AssemblyElementType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
