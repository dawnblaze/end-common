﻿namespace Endor.Models
{
    public partial class EmployeeLocator : BaseLocator<short, EmployeeData>
    {
        public override short ParentID { get; set; }
        
        public override EmployeeData Parent { get; set; }
        public override EnumLocatorSubType LocatorSubTypeNavigation { get; set; }
        public override EnumLocatorType LocatorTypeNavigation { get; set; }
    }
}
