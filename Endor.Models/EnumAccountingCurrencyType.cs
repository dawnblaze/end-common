﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{

    public enum AccountingCurrencyType : byte
    {
        USDollar = 0,
        CanadianDollar = 1,
        AustrailianDollar = 2,
        Euro = 3,
        PoundsSterling = 4,
        Rand = 5,
        NewZealandDolalr = 6,
    }

    public class EnumAccountingCurrencyType
    {
        public byte ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(3)]
        public string Code { get; set; }

        [Required]
        [StringLength(1)]
        public string Symbol { get; set; }
    }
}
