﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class OptionData
    {
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public short OptionID { get; set; }
        [Required]
        public string Value { get; set; }
        public byte? AssociationID { get; set; }
        public short? BID { get; set; }
        public byte? LocationID { get; set; }
        public short? StoreFrontID { get; set; }
        public int? CompanyID { get; set; }
        public int? ContactID { get; set; }
        public byte OptionLevel { get; set; }
        public short? UserLinkID { get; set; }

        public SystemOptionDefinition OptionDefinition { get; set; }
    }
}
