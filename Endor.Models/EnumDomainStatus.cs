﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum DomainStatus : byte
    {
        Pending = 1,
        Online = 2,
        Offline = 3,
        Expired = 4,
        Inactive = 5
    }

    public partial class EnumDomainStatus
    {
        public DomainStatus ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
