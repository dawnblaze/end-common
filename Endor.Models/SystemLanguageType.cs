﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class SystemLanguageType
    {
        public byte ID { get; set; }
        [StringLength(10)]
        public string Code { get; set; }
        [StringLength(10)]
        public string GoogleCode { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string DisplayName { get; set; }
    }
}
