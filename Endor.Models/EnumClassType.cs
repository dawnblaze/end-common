﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class EnumClassType
    {
        public short ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        [StringLength(255)]
        public string TableName { get; set; }
        public string RecordNameFromObjectIDSQL { get; set; }
    }
}
