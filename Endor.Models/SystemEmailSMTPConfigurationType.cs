﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class SystemEmailSMTPConfigurationType
    {
        public byte ID { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string SMTPAddress { get; set; }

        public short? SMTPPort { get; set; }

        public byte? SMTPSecurityType { get; set; }

        public bool? SMTPAuthenticateFirst { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<DomainEmail> DomainEmails { get; set; }
    }
}
