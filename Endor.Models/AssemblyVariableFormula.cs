﻿using Newtonsoft.Json;
using System;


namespace Endor.Models
{
    public partial class AssemblyVariableFormula : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public int VariableID { get; set; }
        public AssemblyFormulaUseType FormulaUseType { get; set; }
        public string FormulaText { get; set; }
        public DataType DataType { get; set; }
        public AssemblyFormulaEvalType FormulaEvalType { get; set; }
        public bool IsFormula { get; set; }
        public string ChildVariableName { get; set; }
        [JsonIgnore]
        public AssemblyVariable Variable { get; set; }
    }
}
