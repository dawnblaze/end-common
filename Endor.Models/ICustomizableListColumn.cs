﻿namespace Endor.Models
{
    public interface ICustomizableListColumn
    {
        int TargetClassTypeID { get; set; }
        
        /// <summary>
        /// Property of a row data.
        /// </summary>
        string Field { get; set; }

        /// <summary>
        /// Whether the column is fixed in horizontal scrolling or not.
        /// </summary>
        bool IsFrozen { get; set; }
        bool IsExpandable { get; set; }
        
        /// <summary>
        /// Specifies the default order of columns
        /// </summary>
        byte SortIndex { get; set; }

        bool IsVisible { get; set; }
    }
}