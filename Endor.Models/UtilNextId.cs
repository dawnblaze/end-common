﻿
namespace Endor.Models
{
    public partial class UtilNextID
    {
        public short BID { get; set; }
        public int ClassTypeID { get; set; }
        public int NextID { get; set; }
    }
}
