﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Endor.Models
{
    public enum TaxEngineType : byte
    {
        Exempt = 0,
        Supplied = 1,
        Internal = 2,
        TaxJar = 3
    }

    public class EnumTaxEngineType
    {
        public TaxEngineType ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
    }
}
