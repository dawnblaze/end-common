﻿namespace Endor.Models
{
    public class MaterialCategoryLink
    {
        public short BID { get; set; }

        public int PartID { get; set; }

        public short CategoryID { get; set; }

        public MaterialData Material { get; set; }

        public MaterialCategory MaterialCategory { get; set; }
    }
}
