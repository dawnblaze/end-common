﻿namespace Endor.Models
{
    public partial class EnumCampaignStatus
    {
        public byte ID { get; set; }
        public string Name { get; set; }
    }
}
