﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class TaxGroup : IAtom<short>
    {
        public TaxGroup()
        {
            TaxGroupItemLinks = new HashSet<TaxGroupItemLink>();
            TaxGroupLocationLinks = new HashSet<TaxGroupLocationLink>();
            Companies = new HashSet<CompanyData>();
            Locations = new HashSet<LocationData>();
            TaxItems = new HashSet<TaxItem>();
        }

        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public decimal? TaxRate { get; set; }

        public bool? IsTaxExempt { get; set; }

        [JsonIgnore]
        public ICollection<TaxGroupItemLink> TaxGroupItemLinks { get; set; }

        [JsonIgnore]
        public ICollection<TaxGroupLocationLink> TaxGroupLocationLinks { get; set; }

        public ICollection<CompanyData> Companies { get; set; }

        public ICollection<LocationData> DefaultLocations { get; set; }

        public ICollection<LocationData> Locations { get; set; }

        public ICollection<SimpleLocationData> SimpleLocations { get; set; }

        public ICollection<TaxItem> TaxItems { get; set; }
        
        public ICollection<SimpleTaxItem> SimpleTaxItems { get; set; }
    }
}
