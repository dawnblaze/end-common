﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class EnumTimeZone
    {
        public short ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        [StringLength(100)]
        public string StandardName { get; set; }
        public decimal UTCOffset { get; set; }
        public bool? IsCommon { get; set; }
    }
}
