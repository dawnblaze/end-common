﻿using System;

namespace Endor.Models
{
    public static class LocatorTypeExtensions
    {
        public static Type MetaDataClass(this LocatorType locatorType)
        {
            switch (locatorType)
            {
                case LocatorType.Address:
                    return typeof(AddressMetaData);
                case LocatorType.Phone:
                    return typeof(PhoneMetaData);
                case LocatorType.Email:
                    return typeof(EmailMetaData);
                //case LocatorType.WebAddress:
                //    return typeof(WebAddressMetaData);
                case LocatorType.Twitter:
                    return typeof(TwitterMetaData);
                case LocatorType.Facebook:
                    return typeof(FaceBookMetaData);
                case LocatorType.LinkedIn:
                    return typeof(LinkedInMetaData);
                default:
                    return typeof(LocatorMetaData);
            }
        }
    }
}
