﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Endor.Models
{
    public class OrderDestinationData : IAtom<int>
    {
        public OrderDestinationData()
        {

        }

        public short BID { get; set; }

        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public int OrderID { get; set; }

        public short DestinationNumber { get; set; }

        public OrderDestinationType DestinationType { get; set; }

        public bool IsForAllItems { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        public OrderOrderStatus OrderStatusID { get; set; }

        public short ItemStatusID { get; set; }

        public short? SubStatusID { get; set; }

        public string Variables { get; set; }

        public bool PriceIsLocked { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceNet { get; set; }

        public bool PriceNetOV { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceDiscount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePreTax { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTaxable { get; set; }

        public bool PriceTaxableOV { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTax { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTotal { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ComputedNet { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? ComputedPreTax { get; set; }

        public short TaxGroupID { get; set; }

        public bool TaxGroupOV { get; set; }

        public bool IsTaxExempt { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? TaxExemptReasonID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMaterial { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostLabor { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostMachine { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostOther { get; set; }

        public decimal CostTotal { get; set; }

        public bool HasDocuments { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public FlatListItem TaxExemptReason { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TransactionHeaderData Order { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumOrderOrderStatus EnumOrderOrderStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumOrderDestinationType EnumOrderDestinationType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemStatus OrderItemStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TaxGroup TaxGroup { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemSubStatus SubStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderNote> Notes { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderKeyDate> Dates { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderContactRole> ContactRoles { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderEmployeeRole> EmployeeRoles { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderDestinationTagLink> TagLinks { get; set; }

        /// <summary>
        /// The type of transaction this is.
        /// </summary>
        public byte TransactionType { get; set; }

        public short? Priority { get; set; }

        public bool? IsUrgent { get; set; }
    }
}
