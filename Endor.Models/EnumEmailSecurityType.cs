﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class EnumEmailSecurityType
    {
        public EmailSecurityType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<DomainEmail> DomainEmails { get; set; }
    }
}
