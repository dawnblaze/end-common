﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class SystemColor
    {
        public short ID { get; set; }

        /// <summary>
        /// The Internal reference Name for this criteria.
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// The RGB Color object for this color
        /// </summary>
        [Required]
        [StringLength(8)]
        public string RGB { get; set; }

        /// <summary>
        /// Flag indicating if this color is used in Tags
        /// </summary>
        public bool UseInTags { get; set; }

        /// <summary>
        /// String The name of the LESS variable that incorporates this color.
        /// <para>This is for internal reference only</para>
        /// </summary>
        [StringLength(50)]
        public string LESSVariableName { get; set; }
    }
}
