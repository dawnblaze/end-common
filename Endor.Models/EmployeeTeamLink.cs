﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public partial class EmployeeTeamLink
    {
        [JsonIgnore]
        public short BID { get; set; }
        [JsonIgnore]
        public int TeamID { get; set; }
        public short EmployeeID { get; set; }
        public short RoleID { get; set; }

        public EmployeeData Employee { get; set; }

        [JsonIgnore]
        public EmployeeTeam EmployeeTeam { get; set; }

        public EmployeeRole Role { get; set; }
    }
}
