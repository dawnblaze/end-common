﻿namespace Endor.Models
{
    /// <summary>
    /// The PriceSet class is an abstraction for pricing information for transaction objects.
    /// Individual Transaction Types will have additional price fields, but this common set is implemented in all.
    /// </summary>
    public interface IPriceSet
    {
        /// <summary>
        ///  Flag indicating if the pricing is locked(and won't be automatically recomputed on edit)	
        /// </summary>
        bool PriceIsLocked { get; set; }
        
        /// <summary>
        /// The Starting Price.  This may be the aggregated child price, the base price, net price, etc. NULL is used to indicate the price is not defined ("Call for Quote") or an error in configuration or pricing has occurred.
        /// </summary>
        decimal? PriceNet { get; set; }
        
        /// <summary>
        /// The Discount Amount.
        /// Use NULL for no discount.
        /// </summary>
        decimal? PriceDiscount { get; set; }

        /// <summary>
        /// The PreTax total for this record.
        /// = Price.Net - IsNull(Price.Discount, 0)
        /// This will be NULL if the Price.Net is NULL, indicating the price is not defined.
        /// </summary>        
        decimal? PricePreTax { get; } 

        /// <summary>
        /// The Taxable total for this record.For non-taxable items, this should be 0.0.  Otherwise this may be any value between 0 and Price.PreTax.
        /// This should only be NULL when Price.PreTax is NULL.
        /// </summary>
        
        decimal? PriceTaxable { get; set; }
        
        /// <summary>
        /// The combined tax rate for the applicable TaxGroup.Price.TaxRate DECIMAL(9,4)
        /// </summary>
        decimal PriceTaxRate { get; set; }

        /// <summary>
        /// Flag indicating if the TaxRate is 0.0.	Price.IsTaxExempt BIT NOT NULL Computed	0
        /// </summary>
        bool PriceIsTaxExempt { get; }

        /// <summary>
        /// The Tax for this transaction item.This should always equal (Price.Taxable* Price.TaxRate), but may have additional rounding rules applied.
        /// </summary>
        decimal? PriceTax { get; }

        /// <summary>
        /// The computed total price for this transaction item.
        /// </summary>
        decimal? PriceTotal { get; }
    }
}
