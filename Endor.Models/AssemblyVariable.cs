﻿using Endor.Units;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/860913707/AssemblyVariable+Object
    /// </summary>
    public class AssemblyVariable : IAtom<int>, IAssemblyVariable
    {
        /// <summary>
        /// The Business ID for the Assembly Element.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID for the Assembly Variable.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 12046.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The name of the Assembly Element
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// The Default Value for this variable as Text.
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// The DataType of the variable
        /// </summary>
        public DataType DataType { get; set; }

        /// <summary>
        /// True if the DefaultValue is a formula
        /// </summary>
        public bool IsFormula { get; set; }

        /// <summary>
        /// Alt text for formula
        /// </summary>
        public bool IsAltTextFormula { get; set; }

        /// <summary>
        /// When the variable is a Measurement, this contains the ID of the Unit Emum for this variable. 
        /// NULL when the variable is not for a measurement.
        /// </summary>
        public Unit? UnitID { get; set; }

        /// <summary>
        /// Contains the ID of the Unit Type Enum for this variable
        /// NULL when the variable is not for a measurement
        /// </summary>
        public UnitType? UnitType { get; set; }

        /// <summary>
        /// Custom unit text 
        /// </summary>
        public string CustomUnitText { get; set; }

        /// <summary>
        /// For List Elements, this field indicates the DataType of the list.  This is a subset of the full DataType and is used for referential integrity.
        /// </summary>
        public DataType? ListDataType { get; set; }

        /// <summary>
        /// For List Elements, this field stores a JSON encoded list of the formula and/or values that make up the list.  
        /// NULL for non List Elements.
        /// </summary>
        public string ListValuesJSON { get; set; }

        /// <summary>
        /// For List Elements of DataType String, this field indicates if the user can add their own value during order entry.
        /// NULL for non List Elements and when the DataType is not String.
        /// </summary>
        public bool? AllowCustomValue { get; set; }

        /// <summary>
        /// For List Elements of DataType String, this field indicates if the user can select multiple values during order entry.
        /// NULL for non List Elements and when the DataType is not String.
        /// </summary>
        public bool? AllowMultiSelect { get; set; }

        /// <summary>
        /// For List Elements with Part DataTypes that are retrieved by Category, this field indicates if the values are presented to the user in a hierarchical drop-down grouped by Part Category.
        /// NULL for non List Elements and when the DataType is not a Part.
        /// </summary>
        public bool? GroupOptionsByCategory { get; set; }

        /// <summary>
        /// The Label for this element.	
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// For Label Elements, this field indicates the type of warning or label icon to display near the label. 
        /// NULL for non-Label Elements. 
        /// </summary>
        public AssemblyLabelType? LabelType { get; set; }

        /// <summary>
        /// For URL Labels, the text to display for the URL. If empty, the URL is displayed.  
        /// NULL for non-URL Labels.
        /// </summary>
        public string AltText { get; set; }

        /// <summary>
        /// The number of Elements referencing this variable.	
        /// </summary>
        public byte ElementUseCount { get; set; }

        /// <summary>
        /// For a linked Assembly or Machine, indicates whether the price and cost accounting should be rolled up into this assembly (true) or treated independently.
        /// </summary>
        public bool RollupLinkedPriceAndCost { get; set; }

        /// <summary>
        /// The Tooltip for this element.  
        /// When present(and supported), the element will display a tooltip icon next to the label and this text will show on mouse-over or click.
        /// </summary>
        public string Tooltip { get; set; }

        /// <summary>
        /// Flag for numeric data types indicating that digits past the decimal point are displayed.  If false, the numeric value is rounded to the nearest integer for display.  On focus, the full number is still displayed.
        /// </summary>
        public bool? AllowDecimals { get; set; }

        /// <summary>
        /// The Enum indicating the display format for numeric data types.
        /// </summary>
        public CustomFieldNumberDisplayType? DisplayType { get; set; }

        /// <summary>
        /// The number of digits to display after the decimal point (when Allow Decimals is checked).
        /// </summary>
        public byte? DecimalPlaces { get; set; }

        /// <summary>
        /// The ID of the SystemVariable this variable was built with.
        /// </summary>
        public short? SystemVariableID{ get; set; }

        /// <summary>
        /// True if this variable is required
        /// </summary>
        public bool IsRequired { get; set; }

        /// <summary>
        /// Flag indicating if this is disabled
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// The type of element used for input
        /// </summary>
        public AssemblyElementType ElementType { get; set; }

        /// <summary>
        /// Foreign Key pointing to AssemblyData
        /// </summary>
        public int AssemblyID { get; set; }

        /// <summary>
        /// Property used to aid binding of elements to variables
        /// </summary>
        public Guid? TempID { get; set; }

        /// <summary>
        /// ID of the linked Material 
        /// </summary>
        public int? LinkedMaterialID { get; set; }
        /// <summary>
        /// ID of the linked Labor
        /// </summary>
        public int? LinkedLaborID { get; set; }
        /// <summary>
        /// ID of the linked Machine
        /// </summary>
        public short? LinkedMachineID { get; set; }
        /// <summary>
        /// ID of the linked Assembly
        /// </summary>
        public int? LinkedAssemblyID { get; set; }
        /// <summary>
        /// Consumption formula (sparse)
        /// </summary>
        public string ConsumptionDefaultValue { get; set; }
        /// <summary>
        /// Indicates if the ConsumptionDefaultValue is a formula (true) or constant (false)
        /// </summary>
        public bool IsConsumptionFormula { get; set; }

        /// <summary>
        /// Contains a collection of elements bound to this variable
        /// </summary>
        [JsonIgnore]
        public ICollection<AssemblyElement> Elements { get; set; }

        /// <summary>
        /// References a assembly that this variable is bound to
        /// </summary>
        [JsonIgnore]
        public AssemblyData Assembly { get; set; }
        public ICollection<AssemblyVariableFormula> Formulas { get; set; }

        /// <summary>
        /// EnableProfileOV
        /// </summary>
        public bool EnableProfileOV { get; set; }

        /// <summary>
        /// The Label for the Profile setup for this variable, if it can be overridden in the Profile
        /// </summary>
        public string ProfileSetupLabel { get; set; }

        /// <summary>
        /// The Hint for the Profile setup for this variable, if it can be overridden in the Profile
        /// </summary>
        public string ProfileSetupHint { get; set; }

        /// <summary>
        /// Inclusion formula to determine is a variable is included in a assembly
        /// </summary>
        public string InclusionFormula { get; set; }

    }
}
