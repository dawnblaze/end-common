﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public partial class SimpleAssemblyData : SimpleListItem<int>
    {
        public AssemblyType AssemblyType { get; set; }
    }
}
