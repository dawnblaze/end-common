﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Endor.Models
{
    public class RightsGroupMenuTree
    {
        public short ID { get; set; }

        public short? ParentID { get; set; }

        public Module? Module { get; set; }

        public string Name { get; set; }

        public byte Depth { get; set; }

        public byte? SortIndex { get; set; }

        public short? EnabledGroupID { get; set; }

        public bool IsSharedGroup { get; set; }

        public short? FullAccessGroupID { get; set; }

        public string Description { get; set; }

        public string SearchTerms { get; set; }

        public string HintText { get; set; }

        public bool IsInternal { get; set; }
            
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<RightsGroupMenuTree> Children { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroup EnabledGroup { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroupMenuTree Parent { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public RightsGroup FullAccessGroup { get; set; }
    }
}
