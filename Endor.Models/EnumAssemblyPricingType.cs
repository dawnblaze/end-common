﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum AssemblyPricingType : byte
    {
        /// <summary>
        /// Market Based Pricing
        /// </summary>
        MarketBasedPricing = 0,
        /// <summary>
        /// Margin Based Pricing
        /// </summary>
    	MarginBasedPricing = 1,
        /// <summary>
        /// Markup Based Pricing
        /// </summary>
    	MarkupBasedPricing = 2,
        /// <summary>
        /// Custom
        /// </summary>
    	Custom = 3,
    }

    public class EnumAssemblyPricingType
    {
        public AssemblyPricingType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
