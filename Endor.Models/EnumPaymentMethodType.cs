﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class EnumPaymentMethodType
    {
        public PaymentMethodType ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }
}
