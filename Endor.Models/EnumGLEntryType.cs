﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{

    public enum GLEntryType : byte
    {
        Manual_GL_Entry = 0,
        Order_New = 1,
        Order_Edited = 2,
        Order_Status_Change = 3,
        Order_Part_Usage_Applied = 4,
        Order_Tax_Computation = 5,
        Order_Maintenance_Recompute = 10,
        Payment_Applied = 11,
        Payment_Edited = 12,
        Payment_Refunded = 13,
        Payment_Voided = 14,
        Payment_Credit = 15,
        Credit_Applied = 16,
        Credit_Adjusted = 17,
        Credit_Refunded = 18,
        Credit_Voided = 19,
        Starting_Balance_Summary = 30,
        Daily_Summary = 31,
        Monthly_Summary = 32,
        Reconciliation = 33,
        CashDrawerAdjustment = 34,
        CashDrawerDeposit = 35,
    }

    public partial class EnumGLEntryType
    {
        public byte ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

    }
}
