﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class SystemOptionSection
    {
        public SystemOptionSection()
        {
            ChildSections = new HashSet<SystemOptionSection>();
            SystemOptionCategories = new HashSet<SystemOptionCategory>();
        }

        public short ID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public short? ParentID { get; set; }

        public bool IsHidden { get; set; }

        public byte Depth { get; set; }

        public string SearchTerms { get; set; }

        public SystemOptionSection Parent { get; set; }

        public ICollection<SystemOptionSection> ChildSections { get; set; }

        public ICollection<SystemOptionCategory> SystemOptionCategories { get; set; }
    }
}
