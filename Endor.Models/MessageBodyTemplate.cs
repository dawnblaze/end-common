﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1028292825/MessageBodyTemplate+Object
    /// </summary>
    public class MessageBodyTemplate : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        public int AppliesToClassTypeID { get; set; }

        public byte MessageTemplateType { get; set; }

        public MessageChannelType ChannelType { get; set; }

        public byte? LocationID { get; set; }

        public int? CompanyID { get; set; }

        public short? EmployeeID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Description { get; set; }

        [StringLength(2048)]
        public string Subject { get; set; }

        public string Body { get; set; }

        public byte AttachedFileCount { get; set; }

        public string AttachedFileNames { get; set; }

        public int? SizeInKB { get; set; }

        public short SortIndex { get; set; }

        [JsonIgnore]
        public string MergeFieldList { get; set; }

        public bool HasAttachment { get; set; }

        public bool HasMergeFields { get; set; }

        public bool HasBody { get; set; }

        // navigation
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData Employee { get; set; }
        [JsonIgnore]
        public BusinessData Business { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public CompanyData Company { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationData Location { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SystemMessageTemplateType TemplateType { get; set; }

        [JsonIgnore]
        public string ParticipantInfoJSON { get; set; }

        [SwaggerInclude]
        public List<MessageParticipantInfo> Participants { get; set; }
    }
}
