﻿using System;
using System.Collections.Generic;

using System.Text;

namespace Endor.Models
{
    public partial class OptionDefinition
    {
        public OptionDefinition()
        {
            OptionData = new HashSet<OptionData>();
        }

        public short ID { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public string Description { get; set; }
        //[Column("DataType", TypeName = "smallint")]
        public DataType DataType { get; set; }
        public short CategoryID { get; set; }
        public string ListValues { get; set; }
        public string DefaultValue { get; set; }
        public bool IsHidden { get; set; }

        
        public virtual OptionCategory OptionCategory { get; set; }
        public virtual EnumDataType EnumCustomFieldDataType { get; set; }
        public virtual ICollection<OptionData> OptionData { get; set; }
        
        public T CastValue<T>()
        {
            return (T)Convert.ChangeType(DefaultValue, typeof(T));
        }
    }
}
