﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class LocationData : IAtom<byte>, IImageCandidate
    {
        public LocationData()
        {
            TaxGroupLocationLinks = new HashSet<TaxGroupLocationLink>();
            Companies = new HashSet<CompanyData>();
            Contacts = new HashSet<ContactData>();
            EmployeeTeamLocationLinks = new HashSet<EmployeeTeamLocationLink>();
            LocationLocators = new HashSet<LocationLocator>();
        }

        public short BID { get; set; }

        public byte ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public bool IsDefault { get; set; }

        public short? TimeZoneID { get; set; }

        public bool? UseDST { get; set; }

        [StringLength(255)]
        public string LegalName { get; set; }

        [StringLength(255)]
        public string DBA { get; set; }

        [StringLength(10)]
        public string InvoicePrefix { get; set; }

        [StringLength(10)]
        public string EstimatePrefix { get; set; }

        [StringLength(10)]
        public string OrderNumberPrefix { get; set; }

        [StringLength(10)]
        public string POPrefix { get; set; }

        [StringLength(100)]
        public string TaxID { get; set; }

        public short? DefaultTaxGroupID { get; set; }

        [StringLength(10)]
        public string DefaultAreaCode { get; set; }

        [StringLength(255)]
        public string Slogan { get; set; }

        public bool HasImage { get; set; }

        public short? DefaultEmailAccountID { get; set; }

        public TaxGroup TaxGroup { get; set; }

        public BusinessData Business { get; set; }

        public EnumTimeZone TimeZone { get; set; }

        public ICollection<TaxGroupLocationLink> TaxGroupLocationLinks { get; set; }

        public ICollection<CompanyData> Companies { get; set; }

        public ICollection<ContactData> Contacts { get; set; }

        public ICollection<EmployeeTeamLocationLink> EmployeeTeamLocationLinks { get; set; }

        public ICollection<LocationLocator> LocationLocators { get; set; }

        public ICollection<EmployeeData> Employees { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<DomainEmailLocationLink> DomainEmailLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmailAccountData DefaultEmailAccount { get; set; }

        public string LocationNumber { get; set; }

        public string CreditMemoPrefix { get; set; }

        public string Abbreviation { get; set; }

        [JsonIgnore]
        public ICollection<LocationCustomDataValue> CustomDataValues { get; set; }
    }
}
