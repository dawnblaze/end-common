﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public class SystemMessageTemplateType
    {   
        public int AppliesToClassTypeID { get; set; }
        public byte ID { get; set; }
        public string Name { get; set; }
        public bool IsSystem { get; set; }
        public MessageChannelType ChannelType { get; set; }
    }
}
