﻿using System;
using System.Collections.Generic;

namespace Endor.Models
{
    public interface IHasCustomField<I,T> : IAtom<I>
        where I : struct, IConvertible
        where T : class, ICustomFieldRecord<I>
    {
        /// <summary>
        /// The JSON Data for the CustomField Values for this object, 
        /// </summary>
        string CFValuesJSON { get; set; }

        /// <summary>
        /// The object that is filled from the CFValuesJSON property 
        /// </summary>
        List<CustomFieldValue> CFValues { get;}
    }
}
