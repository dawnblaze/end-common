﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class SystemAutomationTriggerDefinition
    {
        /// <summary>
        /// The ID of this Trigger Definition
        /// </summary>
        [Key]
        public short ID { get; set; }

        /// <summary>
        /// The <see cref="AutomationTriggerCategoryType"/> for this Trigger
        /// </summary>
        public AutomationTriggerCategoryType TriggerCategoryType { get; set; }

        /// <summary>
        /// The name for this Trigger
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// The <see cref="Endor.Models.DataType"/> for this Trigger
        /// </summary>
        [Required]
        public DataType DataType { get; set; }

        /// <summary>
        /// A flag indicating if this trigger has an addtional condition.  If true, then the Trigger is assumed to be "IS IN" and a test is required.
        /// </summary>
        public bool HasCondition { get; set; }

        /// <summary>
        /// The DataType for the condition value.  This is used to determine the type of input used.
        /// </summary>
        public DataType? ConditionDataType { get; set; }

        /// <summary>
        /// A flag indicating if this trigger Applies To Automations.
        /// </summary>
        public bool AppliesToAutomations { get; set; }

        /// <summary>
        /// A flag indicating if this trigger Applies To Alerts.
        /// </summary>
        public bool AppliesToAlerts { get; set; }

        public EnumAutomationTriggerCategoryType AutomationTriggerCategoryType { get; set; }
    }
}
