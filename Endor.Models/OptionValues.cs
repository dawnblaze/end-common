﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class OptionValues
    {
        public int OptionID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public byte Level { get; set; }
        public short CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string SectionName { get; set; }
    }
}
