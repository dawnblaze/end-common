﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public partial class AlertDefinitionAction : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }

        public short AlertDefinitionID { get; set; }

        public byte ActionDefinitionID { get; set; }

        public byte SortIndex { get; set; }

        public DataType DataType { get; set; }

        public string Subject { get; set; }
        [JsonIgnore]
        public bool SubjectHasMergeFields { get; set; }

        public string Body { get; set; }

        [JsonIgnore]
        public bool BodyHasMergeFields { get; set; }

        public string MetaData { get; set; }

        public AlertDefinition AlertDefinition { get; set; }

        public SystemAutomationActionDefinition Definition { get; set; }
    }
}
