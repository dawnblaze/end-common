﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum OrderContactRoleType : byte
    {
        Primary = 1,
        Billing = 2,
        ShipTo = 3,
        ShipFrom = 4,
        DesignApproval = 11,
        DesignReviewer = 12
    }

    public class EnumOrderContactRoleType
    {
        /// <summary>
        /// Order Contact Role Type ID
        /// </summary>
        [JsonIgnore]
        public OrderContactRoleType ID { get; set; }

        /// <summary>
        /// Order Contact Role Name
        /// </summary>
        [StringLength(100)]
        public string Name { get; set; }

        /// <summary>
        /// The TransactionTypeSet for this object.
        /// </summary>
        public OrderTransactionType TransactionTypeSet { get; set; }

        /// <summary>
        /// The TransactionLevelSet for this object.
        /// </summary>
        public OrderTransactionLevel TransactionLevelSet { get; set; }

        /// <summary>
        /// Flag indicating if this enum type applies to Estimates.
        /// </summary>
        public bool AppliesToEstimate { get; set; }

        /// <summary>
        /// Flag indicating if this enum type applies to Orders.
        /// </summary>
        public bool AppliesToOrder { get; set; }

        /// <summary>
        /// Flag indicating if this enum type applies to POs.
        /// </summary>
        public bool AppliesToPO { get; set; }

        /// <summary>
        /// Flag indicating if this enum type is applied at the TransHeader level.
        /// </summary>
        public bool IsHeaderLevel { get; set; }

        /// <summary>
        /// Flag indicating if this enum is applied at the Destination level.
        /// </summary>
        public bool IsDestinationLevel { get; set; }

        /// <summary>
        /// Flag indicating if this enum is applied at the Line Items level.
        /// </summary>
        public bool IsItemLevel { get; set; }
    }
}
