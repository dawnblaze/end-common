﻿using Endor.Units;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Endor.Models
{
    public class OrderItemComponent : IAtom<int>, IOrderItemComponentInformation, IItemComponent<OrderItemComponent>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public Guid? TempID { get; set; }

        public int ID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int ClassTypeID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime ModifiedDT { get; set; }

        public int OrderID { get; set; }

        public int OrderItemID { get; set; }

        public int? ParentComponentID { get; set; }

        public short Number { get; set; }

        /// <summary>
        /// Name of the Component, same as the Material/Labor/Assembly/Machine
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// For Components with a <see cref="ParentComponentID"/>, the variable name of the linked component variable
        /// <para>
        /// May not be the same as <see cref="Name"/>
        /// </para>
        /// </summary>
        public string VariableName { get; set; }

        public decimal? TotalQuantity { get; set; }

        public bool TotalQuantityOV { get; set; }

        public decimal? AssemblyQuantity { get; set; }

        public bool AssemblyQuantityOV { get; set; }
        public Unit? QuantityUnit { get; set; }

        public string Description { get; set; }
        public int? AssemblyVariableID { get; set; }

        public string AssemblyDataJSON { get; set; }

        public Dictionary<string, VariableValue> Variables { get; set; }

        public decimal? PriceUnit { get; set; }

        public bool PriceUnitOV { get; set; }

        public decimal? PricePreTax { get; set; }

        public int? IncomeAccountID { get; set; }

        public int? ExpenseAccountID { get; set; }

        /// <summary>
        /// The IncomeAllocationType for this object.
        /// </summary>
        public AssemblyIncomeAllocationType? IncomeAllocationType { get; set; }

        public decimal? CostUnit { get; set; }

        public bool CostOV { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostNet { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PricePerItem { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? CostPerItem { get; set; }

        public decimal? PriceComputed { get; set; }

        public decimal? PriceTaxable { get; set; }

        public decimal? PriceTax { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? PriceTotal { get; set; }

        public TransactionHeaderData Order { get; set; }

        public OrderItemData OrderItem { get; set; }

        public OrderItemComponent ParentComponent { get; set; }

        public ICollection<OrderItemComponent> ChildComponents { get; set; }

        public MaterialData Material { get; set; }

        public LaborData Labor { get; set; }

        public MachineData Machine { get; set; }

        public AssemblyData Assembly { get; set; }

        public DestinationData Destination { get; set; }

        public AssemblyVariable AssemblyVariable { get; set; }

        public GLAccount IncomeAccount { get; set; }

        public GLAccount ExpenseAccount { get; set; }

        /// <summary>
        /// This is the Fixed part of the Components price, pulled from the Component definition when 
        /// it was initially created.  It is used during computation to recalculate the component 
        /// cost.
        /// </summary>
        public decimal? CostComponentFixed { get; set; }

        /// <summary>
        /// This is the Per Unit part of the Components price, pulled from the Component definition when 
        /// it was initially created.  It is used during computation to recalculate the component cost.
        /// </summary>
        public decimal? CostComponentUnit { get; set; }

        public ICollection<TaxAssessmentResult> TaxInfoList { get; set; }

        /// <summary>
        /// Determines Whether a subcomponent's price/cost is rolled up to the parent component
        /// </summary>
        public bool? RollupLinkedPriceAndCost { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemComponentType ComponentType { get; set; }

        public int ComponentID { get; set; }

        [JsonIgnore]
        public short? MachineID { get => (ComponentType == OrderItemComponentType.Machine ? (short)ComponentID: (short?)null); set { } }

        [JsonIgnore]
        public int? MaterialID { get => (ComponentType == OrderItemComponentType.Material ? ComponentID : (int?)null); set { } }

        [JsonIgnore]
        public int? AssemblyID { get => (ComponentType == OrderItemComponentType.Assembly ? ComponentID : (int?)null); set { } }

        [JsonIgnore]
        public int? LaborID { get => (ComponentType == OrderItemComponentType.Labor ? ComponentID : (int?)null); set { } }
        
        [JsonIgnore]
        public short? DestinationID { get => (ComponentType == OrderItemComponentType.Destination ? (short)ComponentID : (short?)null); set { } }
    }
}
