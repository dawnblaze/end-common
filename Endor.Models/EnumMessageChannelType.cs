﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    [Flags]
    public enum MessageChannelType : byte
    {
        None = 0,
        Message = 1,
        Alert = 2,
        Email = 4,
        SMS = 8,
        Slack = 16,
        WebHook = 32,
        Zapier = 64,
        SystemNotification = 128
    }

    //[Table("enum.Message.ChannelType")] //VERIFY
    public partial class EnumMessageChannelType
    {
        //[Column("ID", TypeName = "tinyint")]
        public MessageChannelType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
