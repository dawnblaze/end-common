﻿using System;

namespace Endor.Models
{
    public class CustomDataValue<I> where I : struct, IConvertible
    {
        public short BID { get; set; }
        public I ID { get; set; }
        public int ClassTypeID { get; set; }
        public short CustomFieldDefID { get; set; }
        public string Value { get; set; }
        public decimal? ValueAsNumber { get; set; }
        public bool? ValueAsBoolean { get; set; }
    }

    public class CompanyCustomDataValue : CustomDataValue<int> { }
    public class ContactCustomDataValue : CustomDataValue<int> { }
    public class OpportunityCustomDataValue : CustomDataValue<int> { }
    public class TransactionHeaderCustomDataValue : CustomDataValue<int> { }
    public class AssemblyCustomDataValue : CustomDataValue<int> { }
    public class EmployeeCustomDataValue : CustomDataValue<short> { }
    public class LaborCustomDataValue : CustomDataValue<int> { }
    public class LocationCustomDataValue : CustomDataValue<byte> { }
    public class MachineCustomDataValue : CustomDataValue<short> { }
    public class MaterialCustomDataValue : CustomDataValue<int> { }
}
