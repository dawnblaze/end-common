﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum RoleAccess : byte
    {
        NotAllowed = 0,
        Optional = 1,
        Mandatory = 2
    }

    public class EnumRoleAccess
    {
        public RoleAccess ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }

}
