﻿using System;


namespace Endor.Models
{
    [Flags]
    public enum MachineLayoutType : byte
    {
        NoLayoutManager = 0,
        Sheet = 1,
        Roll = 2,
        Other = 4,
    }

    public class EnumMachineLayoutType
    {
        public MachineLayoutType ID { get; set; }

        public string Name { get; set; }
    }
}
