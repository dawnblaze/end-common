﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public partial class OptionSection
    {
        public OptionSection()
        {
            Children = new HashSet<OptionSection>();
            OptionCategories = new HashSet<OptionCategory>();
        }
        public short ID { get; set; }
        public string Name { get; set; }
        public short? ParentID { get; set; }
        public bool IsHidden { get; set; }
        public byte Depth { get; set; }
        public string SearchTerms { get; set; }

        public virtual OptionSection Parent { get; set; }
        public virtual ICollection<OptionSection> Children { get; set; }
        public virtual ICollection<OptionCategory> OptionCategories { get; set; }
    }
}
