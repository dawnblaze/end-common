﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum EmailAccountStatus : byte
    {
        Pending = 1,
        Authorized = 2,
        Failed = 3,
        Expired = 4,
        Inactive = 5
    }

    public partial class EnumEmailAccountStatus
    {
        public EmailAccountStatus ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
