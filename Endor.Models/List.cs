﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models.Backup
{
    public partial class ListFilter : BaseAtom<int>
    {
        /// <summary>
        /// The associated of the record search.This is used as the Tab label.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///  The ClassTypeID that the List targets.
        /// </summary>
        public int TargetClassTypeID { get; set; }

        public DateTime CreatedDate { get; set; }

        /// <summary>
        ///  The ID of the employee that created and owns this list. This is NULL for System Filters.
        /// </summary>
        public int? OwnerID { get; set; }

        #region xml
        [JsonIgnore]
        public string CriteriaXML { get; set; }

        /// <summary>
        /// An array of the individual filter items applied to this ListFilter.This is only used for Dynamic Lists.
        /// </summary>
        [JsonProperty("Criteria")]
        public ListFilterItem[] CriteriaObject
        {
            get
            {
                return XMLUtilities.DeserializeMetaDataFromXML(this.CriteriaXML, typeof(ListFilterItem[])) as ListFilterItem[];
            }
            set
            {
                this.CriteriaXML = XMLUtilities.SerializeMetaDataToXML(value, typeof(ListFilterItem[]));
            }
        }

        [JsonIgnore]
        public string IDsXML { get; set; }

        /// <summary>
        /// An array of IDs to use for this list.This property is only used for Static Lists.
        /// </summary>
        [JsonProperty("IDs")]
        public int[] IDsObject
        {
            get
            {
                return XMLUtilities.DeserializeMetaDataFromXML(this.IDsXML, typeof(int[])) as int[];
            }
            set
            {
                this.IDsXML = XMLUtilities.SerializeMetaDataToXML(value, typeof(int[]));
            }
        }
        #endregion

        #region booleans
        /// <summary>
        ///  Indicates if this is a system Filter.System Filters are automatically displayed on all systems.
        /// </summary>
        public bool IsSystem { get; set; }

        public bool IsActive { get; set; }

        public bool IsDynamic { get; set; }

        /// <summary>
        /// This only applies if IsSystem=False
        /// </summary>
        public bool IsPublic { get; set; }

        /// <summary>
        /// Indicates if this list is the Default tab when the page is opened.
        /// This field is only used for System Lists.
        /// </summary>
        public bool IsDefault { get; set; }
        #endregion

        public string Hint { get; set; }

        /// <summary>
        /// The sort order of tabs.Lower numbers are displayed on the left, higher numbers on the right.
        /// This field is only used for System Lists.  For Custom Lists, the SortIndex in the EmployeeSubscription is used.
        /// </summary>
        public byte SortIndex { get; set; }
    }
}
