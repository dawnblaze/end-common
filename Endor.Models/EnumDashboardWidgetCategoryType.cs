﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum DashboardWidgetCategoryType : byte
    {
        Management = 1,
        Accounting = 2,
        Sales = 4,
        Production = 8,
        Purchasing = 16,
        Ecommerce = 32,
        User = 128,
    }

    public partial class EnumDashboardWidgetCategoryType
    {
        public DashboardWidgetCategoryType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
