﻿using System;

namespace Endor.Models
{
    public enum PaymentDueBasedOnType : Byte //tinyint
    {
        DaysAfterInvoice = 0,
        DaysAfterBuilt = 1,
        DaysAfterPlaced = 2,
        DaysAfterEndofInvoiceMonth = 3,
        DayoftheMonth = 4,
        DayoftheFollowingMonth = 5
    }

    public partial class PaymentTerm:IAtom<int>
    {
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public short DaysDue { get; set; }
        public PaymentDueBasedOnType PaymentDueBasedOnType { get; set; }
        public string PaymentDueBasedOnText { get; set; }
        public short? EarlyPaymentDays { get; set; }
        public bool? DownPaymentRequired { get; set; }

        /// <summary>
        ///  The order discount applied for early payment (payment made based on the EarlyPaymentDays).  This is stored as 2.00 for a 2% discount, not 0.02.
        ///  Set to NULL if early payment discount does not apply.
        /// </summary>
        public decimal? EarlyPaymentPercent { get; set; }

        /// <summary>
        /// The discount percentage applied when a down payment threshold is not specified, or when the order amount is greater than or equal to the threshold amount.
        /// </summary>
        public decimal? DownPaymentPercent { get; set; }

        /// <summary>
        /// The threshold at which the down payment percentage changes.
        ///
        /// Use DownPaymentPercent when OrderTotal >= Threshold
        /// Use DownPaymentPercentBelow when OrderTotal<Threshold
        /// </summary>
        public decimal? DownPaymentThreshold { get; set; }

        /// <summary>
        /// The discount percentage applied when a down payment threshold is specifiedand the order amount is less than the threshold amount.
        /// </summary>
        public decimal? DownPaymentPercentBelow { get; set; }
    }
}
