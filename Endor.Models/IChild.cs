﻿using System;

namespace Endor.Models
{
    public interface IChild<I, P> where I : struct, IConvertible where P : IAtom<I>
    {
        I ParentID { get; set; }

        P Parent { get; set; }
    }
}
