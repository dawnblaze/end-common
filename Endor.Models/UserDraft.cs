﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public class UserDraft : IAtom<int>
    {
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public DateTime ExpirationDT { get; set; }
        public short UserLinkID { get; set; }
        public int ObjectCTID { get; set; }
        public int? ObjectID { get; set; }
        public string Description { get; set; }
        public string VersionBasedOn { get; set; }
        public string MetaData { get; set; }
        public bool? IsExpired { get; set; }

        public string ObjectJSON { get; set; }

        public IAtom<int> Object { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public UserLink UserLink { get; set; }
    }    
}