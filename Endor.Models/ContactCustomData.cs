﻿using System;


namespace Endor.Models
{
    public class ContactCustomData : ICustomFieldRecord<int>
    {
        public short BID { get; set; }

        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public int AppliesToClassTypeID { get; set; }

        public string DataXML { get; set; }

        public ContactData Contact { get; set; }
    }
}
