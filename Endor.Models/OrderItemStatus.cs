﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.Text;

namespace Endor.Models
{
    public class OrderItemStatus: IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        public bool IsDefault { get; set; }
        public byte TransactionType { get; set; }
        public OrderOrderStatus? OrderStatusID { get; set; }
        public byte StatusIndex { get; set; }
        [Required]
        public string Name { get; set; }
        public string CustomerPortalText { get; set; }
        public string Description { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderItemStatusSubStatusLink> OrderItemStatusSubStatusLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OrderItemSubStatus> OrderItemSubStatuses { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleOrderItemSubStatus> SimpleOrderItemSubStatuses { get; set; }
    }
}
