﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Endor.Models
{
    public partial class LocationGoal : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// The Location ID
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte? LocationID { get; set; }

        /// <summary>
        /// The Year
        /// </summary>
        public short Year { get; set; }

        /// <summary>
        /// The Month
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte? Month { get; set; }

        /// <summary>
        /// The Budgeted amount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Budgeted { get; set; }

        /// <summary>
        /// The Actual amount
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Actual { get; set; }

        /// <summary>
        /// Percentage of Goal hit
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [Column(TypeName = "numeric(38,16)")]
        public decimal? PercentOfGoal { get; set; }

        /// <summary>
        /// If it is the Yearly Total
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsYearlyTotal { get; set; }

        /// <summary>
        /// If it is the Business Total
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsBusinessTotal { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BusinessData Business { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationData Location { get; set; }
    }
}
