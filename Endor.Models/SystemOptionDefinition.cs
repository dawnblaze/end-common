﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class SystemOptionDefinition
    {
        public SystemOptionDefinition()
        {
            Options = new HashSet<OptionData>();
        }

        public short ID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(255)]
        public string Label { get; set; }

        public string Description { get; set; }

        public DataType DataType { get; set; }

        public short CategoryID { get; set; }

        public string ListValues { get; set; }

        public string DefaultValue { get; set; }

        public bool IsHidden { get; set; }

        public SystemOptionCategory Category { get; set; }

        public EnumDataType DataTypeNavigation { get; set; }

        public ICollection<OptionData> Options { get; set; }
    }
}
