﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class MaterialCategory : IAtom<short>
    {
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// A general description of the Category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID of the Parent Category this Category is under.
        /// </summary>
        public short? ParentID { get; set; }

public MaterialCategory ParentCategory { get; set; }

        public ICollection<MaterialCategory> ChildCategories { get; set; }

        public ICollection<MaterialCategoryLink> MaterialCategoryLinks { get; set; }

        public ICollection<MaterialData> Materials { get; set; }

        public ICollection<SimpleMaterialData> SimpleMaterials { get; set; }
    }
}
