﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Endor.Models
{
    public class OrderNote : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }

        [JsonIgnore]
        public int ClassTypeID { get; set; }
        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }
        public int OrderID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? OrderItemID { get; set; }
        [JsonProperty(NullValueHandling=NullValueHandling.Ignore)]
        public int? DestinationID { get; set; }
        public OrderNoteType NoteType { get; set; }
        [Required]
        public string Note { get; set; }

        [JsonIgnore]
        public bool IsOrderNote { get; set; }
        [JsonIgnore]
        public bool IsOrderItemNote { get; set; }
        [JsonIgnore]
        public bool IsDestinationNote { get; set; }

        /// <summary>
        /// Flag indicating if the Ordernote is Active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// (Read Only) The date time the record was last modified.  Updated by the system on every save or action.
        /// </summary>
        public DateTime CreatedDT { get; set; }

        /// <summary>
        /// The ID of the Employee Object who added the note.  NULL for a note added by a Contact.
        /// </summary>
        public short? EmployeeID { get; set; }

        /// <summary>
        /// The ID of the Contact Object who added the note.  NULL for a note added by an Employee.
        /// </summary>
        public int? ContactID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public TransactionHeaderData Order { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemData OrderItem { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderDestinationData Destination { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData Employee { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ContactData Contact { get; set; }
    }
}
