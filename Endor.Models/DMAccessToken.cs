﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public class DMAccessToken
    {
        [JsonIgnore]
        public short BID { get; set; }

        public Guid ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string URL { get; set; }
        public string SASToken { get; set; }
        public short ViewedCount { get; set; }
        public short? ViewsRemaining { get; set; }
        public DateTime? LastViewedDT { get; set; }
    }
}
