﻿using System;

namespace Endor.Models
{
    public enum PaymentMethodType : Byte //tinyint
    {
        Custom = 0,
        Cash = 1,
        Check = 2,
        //ACH = 3, END-790 - Delete Manual eCheck / ACH (External) Payment Method
        WireTransfer = 4,
        CreditCard = 5,
        AmEx = 6,
        MasterCard = 7,
        Visa = 8,
        Discover = 9,
        RefundableCredit = 250,
        NonRefundableCredit = 251,
        Writeoff = 252
    }

    public class PaymentMethod : IAtom<int>
    {
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }

        public PaymentMethodType PaymentMethodType { get; set; }

        public int DepositGLAccountID { get; set; }

        public bool IsIntegrated { get; set; }
        public bool IsACH { get; set; }
        public bool IsCreditCard { get; set; }
        public bool IsCustom { get; set; }

        public GLAccount DepositGLAccount { get; set; }
    }
}
