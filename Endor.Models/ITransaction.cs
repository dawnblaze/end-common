﻿namespace Endor.Models
{
    public interface ITransaction : IAtom<int>, IPriceSet, ICostSet
    {
        /// <summary>
        /// The Description of the item
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// The ID of the applicable TaxGroup for this transaction item.TaxGroupID  SMALLINT
        /// </summary>
        short TaxGroupID { get; set; }
        /// <summary>
        /// Flag indicating if this TaxGroup was manually overridden.	TaxGroupOV BIT NOT NULL
        /// </summary>
        bool TaxGroupOV { get; set; }
    }
}
