﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Endor.Models
{
    public class OrderItemSurcharge : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }

        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public int OrderItemID { get; set; }

        public short SurchargeDefID { get; set; }

        /// <summary>
        /// The Sequence Number for this Surcharge.  This is used as the sort order.
        /// </summary>
        public short Number { get; set; }

        /// <summary>
        /// The Name of the Surcharge.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Price Applied based on this Surcharge.  By default, this is:
        /// SurchargeDef.DefaultFixedFee + OrderItem.Quantity * SurchargeDef.DefaultPerUnitFee
        /// unless it is overridden by the user.
        /// </summary>
        public decimal? PricePreTax { get; set; }

        public bool PriceIsOV { get; set; }

        public decimal? PriceTaxable { get; set; }

        public decimal? PriceTax { get; set; }

        public decimal? PriceTotal { get; set; }

        public int IncomeAccountID { get; set; }

        public short TaxCodeID { get; set; }

        /// <summary>
        /// The DefaultPerUnitFee for this object.
        /// </summary>
        public decimal? DefaultFixedFee { get; set; }

        /// <summary>
        /// The Name of the Surcharge.
        /// </summary>
        public decimal? DefaultPerUnitFee { get; set; }

        [JsonIgnore]
        public OrderItemData OrderItem { get; set; }

        [JsonIgnore]
        public SurchargeDef SurchargeDef { get; set; }

        [JsonIgnore]
        public GLAccount IncomeAccount { get; set; }

        [JsonIgnore]
        public TaxabilityCode TaxCode { get; set; }

        public virtual ICollection<TaxAssessmentResult> TaxInfoList { get; set; }
    }
}
