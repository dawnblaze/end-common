﻿using Newtonsoft.Json;
//https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/622854172/OrderItemStatusSubStatusLink+Object
namespace Endor.Models
{
    public class OrderItemStatusSubStatusLink
    {
        public short BID { get; set; }
        public short StatusID { get; set; }
        public short SubStatusID { get; set; }

        ///<summary>
        /// Primary Order two way navigation
        ///</summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemStatus OrderItemStatus { get; set; }

        ///<summary>
        /// Primary Order two way navigation
        ///</summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public OrderItemSubStatus OrderItemSubStatus { get; set; }
    }
}
