﻿namespace Endor.Models
{
    public class UserListColumn: ICustomizableListColumn
    {
        public int TargetClassTypeID { get; set; }

        /// <summary>
        /// Property of a row data.
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// Whether the column is fixed in horizontal scrolling or not.
        /// </summary>
        public bool IsFrozen { get; set; }
        public bool IsExpandable { get; set; }

        /// <summary>
        /// Specifies the default order of columns
        /// </summary>
        public byte SortIndex { get; set; }

        public bool IsVisible { get; set; }
    }
}