﻿using Newtonsoft.Json;
using System;
using System.Xml.Serialization;

namespace Endor.Models
{
    [XmlRoot("MetaData")]
    public class LocatorMetaData
    {
        public string CustomLabel { get; set; }
    }

    [XmlRoot("MetaData")]
    public class AddressMetaData : LocatorMetaData
    {
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string Suburb { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
        public string PlaceName { get; set; }
        public string BusinessPhone { get; set; }
        public string Website { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? VerifiedDate { get; set; }
        public string PlaceID { get; set; }
        public int TaxClassID { get; set; }

        public string FormattedAddress { get; set; }
        public string IconURL { get; set; }
        public string UtcOffset { get; set; }
    }

    [XmlRoot("MetaData")]
    public class PhoneMetaData : LocatorMetaData
    {
        public string AreaCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Extension { get; set; }
        public string CountryCode { get; set; }
        public bool IsMobile { get; set; }
        public string Carrier { get; set; }
        //public bool SupportsSMS {get;set;}
        public bool DoNotSMS { get; set; }
    }

    [XmlRoot("MetaData")]
    public class EmailMetaData : LocatorMetaData
    {
        public string Domain { get; set; }
        public DateTime LastSuccess { get; set; }
        public DateTime LastFailure { get; set; }
        public bool DoNotMarket { get; set; }
    }
    
    public abstract class SocialNetworkMetaData : LocatorMetaData
    {
        public string AccountName { get; set; }
        public string ImageURL { get; set; }
        public DateTime LastUpdated { get; set; }
        public bool IsActive { get; set; }
    }

    [XmlRoot("MetaData")]
    public class LinkedInMetaData : SocialNetworkMetaData { }

    [XmlRoot("MetaData")]
    public class TwitterMetaData: SocialNetworkMetaData { }

    [XmlRoot("MetaData")]
    public class FaceBookMetaData : SocialNetworkMetaData { }
}