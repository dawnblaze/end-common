﻿namespace Endor.Models
{
    public enum ConsentType : byte
    {
        NotAllowed = 0,
        Optional = 1,
        Mandatory = 2
    }
}
