﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// The FlatListType Enum is used to classify the type of FlatList Object a record belongs to.
    /// </summary>
    public enum FlatListType : byte
    {
        /// <summary>
        /// Tax Exempt Reasons ListType
        /// </summary>
        TaxExemptReasons = 1,
        /// <summary>
        /// Estimate Cancelled Reasons ListType
        /// </summary>
        EstimateCancelledReasons = 2,
        /// <summary>
        /// Order Voided Reasons ListType
        /// </summary>
        OrderVoidedReasons = 3,
        /// <summary>
        /// Estimate Edited Reasons ListType
        /// </summary>
        EstimateEditedReasons = 4,
        /// <summary>
        /// Order Edited Reasons ListType
        /// </summary>
        OrderEditedReasons = 5,
        /// <summary>
        /// Time Card Activities ListType
        /// </summary>
        TimeCardActivities = 11,
        /// <summary>
        /// Time Card Breaks ListType
        /// </summary>
        TimeCardBreaks = 12,
        /// <summary>
        /// Payment Voided Reasons ListType
        /// </summary>
        PaymentVoidedReasons = 21,
        /// <summary>
        /// Credit Given Reasons ListType
        /// </summary>
        CreditGivenReasons = 22,
        /// <summary>
        /// Line Item Categories
        /// </summary>
        LineItemCategories = 31,
        /// <summary>
        /// Quick Item Categories
        /// </summary>
        QuickItemCategories = 32,
        /// <summary>
        /// Pricing Tiers ListType
        /// </summary>
        PricingTiers = 100,
    }

    /// <summary>
    /// Entity type for FlatListType enum
    /// </summary>
    public class EnumFlatListType
    {
        /// <summary>
        /// The ID for the Enum.
        /// </summary>
        public FlatListType ID { get; set; }

        /// <summary>
        /// The name for this Enum value.
        /// </summary>
        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Indicates if the list items should be sorted alphabetically by name.  If false, the SortIndex is used as the primary sort field.
        /// </summary>
        public bool IsAlphaSorted { get; set; }
    }
}
