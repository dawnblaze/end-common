﻿
namespace Endor.Models
{
    public class SimpleTaxGroup : SimpleListItem<short>
    {
        public decimal? TaxRate { get; set; }
    }
}
