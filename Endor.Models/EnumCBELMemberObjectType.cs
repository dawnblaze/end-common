﻿namespace Endor.Models
{
    /// <summary>
    /// The CBELMemberType Enum lists the types of members that can be in a CBEL expression.  These are generated for code-complete as well as case-correction of the CBEL input.
    /// It is a FLAG enum type, so combinations of values are possible.
    /// Note: The CBELMemberType is not used by any saved fields and is not in the database.
    /// </summary>
    public enum EnumCBELMemberObjectType : byte
    {
        Scalar = 1,
        Object = 2,
        Array = 4,
        Variable = 8
    }
}
