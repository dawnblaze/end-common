﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum MaterialPhysicalType: byte
    {
        None = 0,
        Discrete = 1,
        Roll = 2,
        Sheet = 3,
        Volume = 4,
        Weight = 5
    }

    public class EnumMaterialPhysicalType
    {
        public MaterialPhysicalType ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public byte Dimensions { get; set; }
        public bool WidthRequired { get; set; }
        public bool LengthRequired { get; set; }
        public bool HeightRequired { get; set; }
        public bool WeightRequired { get; set; }
    }
}
