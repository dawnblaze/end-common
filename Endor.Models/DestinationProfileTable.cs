﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public class DestinationProfileTable : IAtom<short>
    {
        public short ID { get; set; }
        [JsonIgnore]
        public short BID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public int ProfileID { get; set; }
        public short TableID { get; set; }
        public string Label { get; set; }
        public bool OverrideDefault { get; set; }
        public short? RowCount { get; set; }
        public string RowValuesJSON { get; set; }
        public short? ColumnCount { get; set; }
        public string ColumnValuesJSON { get; set; }
        public string CellDataJSON { get; set; }
        [JsonIgnore]
        public AssemblyTable Table { get; set; }
    }
}
