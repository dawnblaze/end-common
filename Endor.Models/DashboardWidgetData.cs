﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    /// <summary>
    /// Dashboard Widget
    /// </summary>
    public class DashboardWidgetData : IAtom<int>
    {
        /// <summary>
        /// Business ID
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }
        /// <summary>
        /// Widget ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Class Type ID
        /// </summary>
        public int ClassTypeID { get; set; }
        /// <summary>
        /// DateTime Last Modified
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if the Widget is active.
        /// </summary>
        [Required]
        public bool IsActive { get; set; }

        /// <summary>
        /// Widget name.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// Dashboard's ID this widget belongs to.
        /// </summary>
        [Required]
        public short DashboardID { get; set; }

        /// <summary>
        /// Widget Definition's ID for this widget.
        /// </summary>
        [Required]
        public short WidgetDefinitionID { get; set; }

        /// <summary>
        /// The current number of Columns high this widget is set for.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte? Cols { get; set; }

        /// <summary>
        /// The current number of Row wide this widget is set for.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte? Rows { get; set; }

        /// <summary>
        /// The X coordinate for this object (measured from the left column = 0)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte? X { get; set; }

        /// <summary>
        /// The Y for this object (measured from the top column = 0)
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte? Y { get; set; }

        /// <summary>
        /// The Interval In Minutes between refreshes for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? RefreshIntervalinMin { get; set; }

        /// <summary>
        /// Dashboard Widget Options in JSON
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Options { get; set; }

        /// <summary>
        /// The DateTime the widget was last updated and the data stored in LastResults.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastUpdatedDT { get; set; }

        /// <summary>
        /// The JSON data from the last time the model was updated.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string LastResults { get; set; }

        /// <summary>
        /// The Dashboard the widget belongs to
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DashboardData Dashboard { get; set; }

        /// <summary>
        /// The Definition
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DashboardWidgetDefinition WidgetDefinition {get; set;}

        /// <summary>
        /// Loads the default values from the associated Widget Definition
        /// </summary>
        public void LoadDefaults()
        {
            if (WidgetDefinition != null)
            {
                this.Cols = WidgetDefinition.DefaultCols;
                this.Rows = WidgetDefinition.DefaultRows;
                this.Name = WidgetDefinition.DefaultName;
                this.Options = WidgetDefinition.DefaultOptions;
                this.RefreshIntervalinMin = WidgetDefinition.DefaultRefreshIntervalInMin;
            }
        }
    }
}