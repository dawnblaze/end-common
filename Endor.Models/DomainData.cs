﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public class DomainData : IAtom<short>
    {
        public DomainData()
        {

        }

        [JsonIgnore]
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        [Required]
        [StringLength(255)]
        public string Domain { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public bool IsDefault { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        [Required]
        public DomainStatus Status { get; set; }
        public byte LocationID { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        [Required]
        public DomainAccessType AccessType { get; set; }
        public DateTime? DNSLastAttemptDT { get; set; }
        [StringLength(100)]
        public string DNSLastAttemptResult { get; set; }
        public DateTime? DNSVerifiedDT { get; set; }
        public bool? Use3rdPartyCertificate { get; set; }
        public short? SSLCertificateID { get; set; }
        public DateTime? SSLLastAttemptDT { get; set; }
        [StringLength(100)]
        public string SSLLastAttemptResult { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumDomainStatus EnumDomainStatus { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumDomainAccessType EnumDomainAccessType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationData Location { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public SSLCertificateData SSLCertificate { get; set; }
    }
}
