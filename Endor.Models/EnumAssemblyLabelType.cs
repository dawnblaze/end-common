﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum AssemblyLabelType : byte
    {
        None = 0,
        Information = 1,
        Success = 2,
        Warning = 3,
        Error = 4
    }

    public class EnumAssemblyLabelType
    {
        public AssemblyLabelType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
