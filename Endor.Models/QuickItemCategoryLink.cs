﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public class QuickItemCategoryLink
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }
        /// <summary>
        /// The ID of the Quick Item
        /// </summary>
        public int QuickItemID { get; set; }
        /// <summary>
        /// The ID of the Quick Item Category that this Quick Item is linked to.
        /// </summary>
        public short CategoryID { get; set; }

        [JsonIgnore]
        public QuickItemData QuickItem { get; set; }
        [JsonIgnore]
        public FlatListItem Category { get; set; }
    }
}
