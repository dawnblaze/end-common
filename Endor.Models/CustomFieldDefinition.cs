﻿using Endor.Units;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class CustomFieldDefinition : IAtom<short>, ICustomFieldDefinition
    {
        [JsonIgnore]

        public short BID { get; set; }
        [JsonIgnore]
        public BusinessData Business { get; set; }

        public short ID { get; set; }

        [JsonIgnore]
        public int ClassTypeID { get; set; }

        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        public bool IsSystem { get; set; }

        public int AppliesToClassTypeID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? AppliesToID { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(200)]
        public string Label { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        [Required]
        public DataType DataType { get; set; }

        [JsonIgnore]
        public EnumDataType EnumCustomFieldDataType { get; set; }

        /// <summary>
        /// The type of element used for input
        /// </summary>
        public AssemblyElementType ElementType { get; set; }

        /// <summary>
        /// The type of number display type
        /// </summary>
        public CustomFieldNumberDisplayType? DisplayType { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        [StringLength(32)]
        public string DisplayFormatString { get; set; }

        public bool AllowMultipleValues { get; set; }

        public bool HasValidators { get; set; }

        public bool AppliesToClass { get; set; }

        public bool IsRequired { get; set; }

        public Unit? UnitID { get; set; }

        public UnitType? UnitType { get; set; }

        public string CustomUnitText { get; set; }
        public bool? AllowCustomValue { get; set; }
        public bool? AllowDecimals { get; set; }
        public bool? AllowMultiSelect { get; set; }
        public bool? AllowNotSetOption { get; set; }
        public bool? AllowTextFormatting { get; set; }
        public bool? AllowUserTypedOptions { get; set; }
        public string AltText { get; set; }
        public byte? DecimalPlaces { get; set; }
        public string DefaultOption { get; set; }
        public string DefaultValue { get; set; }
        public string DisplayFormat { get; set; }
        public bool? DisplaySpinner { get; set; }
        public byte ElementUseCount { get; set; }
        public bool? GroupOptionsByCategory { get; set; }
        public byte? GroupType { get; set; }
        public short? HasMaxLength { get; set; }
        public bool IsDisabled { get; set; }
        public AssemblyLabelType? LabelType { get; set; }
        public DataType? ListDataType { get; set; }
        public string ListOptions { get; set; }
        public string ListValuesJSON { get; set; }
        public byte? NumberOfDecimals { get; set; }
        public string OptionOneLabel { get; set; }
        public string OptionTwoLabel { get; set; }
        public byte? PickerType { get; set; }
        public bool? PresetTime { get; set; }
        public decimal? SpinnerIncrement { get; set; }
        public DateTime? Time { get; set; }
        public bool? ToggleOptions { get; set; }
        public string Tooltip { get; set; }

        [JsonIgnore]
        public ICollection<CustomFieldLayoutElement> Elements { get; set; }

        public Guid? TempID { get; set; }
    }
}
