﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum AssemblyListDataType : short
    {
        Text = 1,
        TextWithNumberValue = 2,
        Materials = ClassType.Material,
        Labor = ClassType.Labor,
        Machines = ClassType.Machine
    }

    public class EnumAssemblyListDataType
    {
        public AssemblyListDataType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
