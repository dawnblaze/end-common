﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class SimpleEmailAccountData : SimpleListItem<short>
    {
        public short? EmployeeID { get; set; }
    }
}
