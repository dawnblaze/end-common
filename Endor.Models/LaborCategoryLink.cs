﻿
namespace Endor.Models
{
    public class LaborCategoryLink
    {
        public short BID { get; set; }

        public int PartID { get; set; }

        public short CategoryID { get; set; }

        public LaborData Labor { get; set; }

        public LaborCategory LaborCategory { get; set; }
    }
}
