﻿using System;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class ActivityEvent : IAtom<int>
    {
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        public byte ActivityType { get; set; }
        [Required]
        [StringLength(255)]
        public string Subject { get; set; }
        public DateTime StartDT { get; set; }
        public DateTime EndDT { get; set; }
        public string Notes { get; set; }
        public int? TeamID { get; set; }
        public bool? IsComplete { get; set; }
        public int? CompletedByID { get; set; }
        public DateTime? CompletedDT { get; set; }
        public string MetaData { get; set; }
        public bool AutoComplete { get; set; }
        public bool AutoRollover { get; set; }
    }
}
