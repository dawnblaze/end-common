﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class TaxabilityCode : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        public bool IsSystem { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(200)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [StringLength(100)]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string TaxCode { get; set; }

        public bool IsTaxExempt { get; set; }

        public bool HasExcludedTaxItems { get; set; }

        [JsonIgnore]
        public ICollection<TaxabilityCodeItemExemptionLink> TaxabilityCodeItemExemptionLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<TaxItem> TaxItems { get; set; }
    }
}
