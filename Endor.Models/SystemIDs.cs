﻿namespace Endor.Models
{
    public static class SystemIDs
    {
        public static class EmployeeRole
        {
            public const short Salesperson = 1;
            public const short CSR = 2;
            public const short Designer = 3;
            public const short ProjectManager = 4;
            public const short AccountManager = 5;
            public const short Production = 6;
            public const short QA = 9;
            public const short Requestor = 41;
            public const short Approver = 42;
            public const short Orderer = 43;
            public const short Receiver = 44;
            public const short Shipper = 61;
            public const short Installer = 62;
            public const short DeliveryPerson = 63;
            public const short ServicePerson = 64;
            public const short AssignedTo = 253;
            public const short EnteredBy = 254;
            public const short Observer = 255;
        }
    }
}
