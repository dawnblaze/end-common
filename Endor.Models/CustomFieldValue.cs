﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace Endor.Models
{
    [JsonObject]
    public class CustomFieldValue : IComparable<CustomFieldValue>, System.IConvertible
    {
        public short ID { get; set; }  // the ID of the [CustomField.Definition]
        //public string V { get; set; }  // the CustomField value, stored as a string.  This may be null.

        public int CompareTo(CustomFieldValue that)
        {
            return this.ID.CompareTo(that.ID);
        }

        [JsonIgnore]
        public DataType DataType { get; set; }

        [JsonProperty("V", NullValueHandling = NullValueHandling.Ignore)]
        public dynamic SerializedValue
        {
            get
            {
                if (V == null)
                    return null;

                if (DataType == DataType.Boolean)
                    return ValueAsBool;

                if (DataType == DataType.Number)
                    return ValueAsDecimal;

                return V;
            }
            set
            {
                V = value.ToString();
            }
        }
        // Store the value as a string (since it was streamed that way)
        private string _Value;
        [JsonIgnore]
        public string V
        {
            get
            {
                return _Value;
            }
            set
            {
                intvalue = null;
                boolvalue = null;
                decimalvalue = null;

                _Value = value;
            }
        }

        private int? intvalue;
        private bool? boolvalue;
        private decimal? decimalvalue;

        // Stored Integer value if it can be converted (so that the conversion is only done once)
        [JsonIgnore]
        public int ValueAsInt
        {
            get
            {
                if (intvalue.HasValue)
                    return intvalue.Value;

                else if (int.TryParse(V, out int newValue))
                {
                    intvalue = newValue;
                    return newValue;
                }

                else
                {
                    intvalue = 0;
                    return 0;
                }
            }
            set
            {
                V = value.ToString();
                intvalue = value;
            }
        }

        // Stored decimal value if it can be converted (so that the conversion is only done once)
        [JsonIgnore]
        public decimal ValueAsDecimal
        {
            get
            {
                if (decimalvalue.HasValue)
                    return decimalvalue.Value;

                else if (decimal.TryParse(V, out decimal newValue))
                {
                    decimalvalue = newValue;
                    return newValue;
                }

                else
                {
                    decimalvalue = 0m;
                    return 0m;
                }
            }
            set
            {
                V = value.ToString();
                decimalvalue = value;
            }
        }

        // Stored boolean  value if it can be converted (so that the conversion is only done once)
        [JsonIgnore]
        public bool ValueAsBool
        {
            get
            {
                if (boolvalue.HasValue)
                    return boolvalue.Value;

                else if (!string.IsNullOrWhiteSpace(V) && ("1ty".IndexOf(V.ToLowerInvariant()[0]) > 0))
                {
                    boolvalue = true;
                    return true;
                }

                else
                {
                    boolvalue = false;
                    return false;
                }
            }
            set
            {
                V = value.ToString();
                boolvalue = value;
            }
        }

        // These functions implement IConvertible
        public TypeCode GetTypeCode()
        {
            switch (DataType)
            {
                case DataType.Boolean:
                    return TypeCode.Boolean;
                case DataType.Number:
                    return TypeCode.Decimal;
                case DataType.DateTime:
                    return TypeCode.DateTime;
                default:
                    return TypeCode.String;
            }
        }

        public bool ToBoolean(IFormatProvider provider)
        {
            return ValueAsBool;
        }

        public byte ToByte(IFormatProvider provider)
        {
            return (byte)ValueAsInt;
        }

        public char ToChar(IFormatProvider provider)
        {
            return V[0];
        }

        public DateTime ToDateTime(IFormatProvider provider)
        {
            return Convert.ToDateTime(V);
        }

        public decimal ToDecimal(IFormatProvider provider)
        {
            return ValueAsDecimal;
        }

        public double ToDouble(IFormatProvider provider)
        {
            return (double)ValueAsDecimal;
        }

        public short ToInt16(IFormatProvider provider)
        {
            return (Int16)ValueAsInt;
        }

        public int ToInt32(IFormatProvider provider)
        {
            return ValueAsInt;
        }

        public long ToInt64(IFormatProvider provider)
        {
            return ValueAsInt;
        }

        public sbyte ToSByte(IFormatProvider provider)
        {
            return (SByte)ValueAsInt;
        }

        public float ToSingle(IFormatProvider provider)
        {
            return (Single)ValueAsDecimal;
        }

        public string ToString(IFormatProvider provider)
        {
            return V;
        }

        public object ToType(Type conversionType, IFormatProvider provider)
        {
            throw new InvalidCastException($"Converting type \"{typeof(CustomFieldValue)}\" to type \"{conversionType.Name}\" is not supported.");
        }

        public ushort ToUInt16(IFormatProvider provider)
        {
            return (UInt16)ValueAsInt;
        }

        public uint ToUInt32(IFormatProvider provider)
        {
            return (UInt32)ValueAsInt;
        }

        public ulong ToUInt64(IFormatProvider provider)
        {
            return (UInt64)Convert.ToInt64(V);
        }
    }
}
