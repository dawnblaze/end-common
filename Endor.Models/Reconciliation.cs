﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace Endor.Models
{
    public class Reconciliation : IAtom<int>
    {
        public Reconciliation()
        {
            Items = new HashSet<ReconciliationItem>();
        }

        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public DateTime CreatedDT { get; set; }
        public DateTime LastAccountingDT { get; set; }
        public DateTime AccountingDT { get; set; }
        public bool IsAdjustmentEntry { get; set; }
        public byte LocationID { get; set; }
        public int GLActivityID { get; set; }
        public int? StartingGLID { get; set; }
        public int? EndingGLID { get; set; }
        public short? EnteredByID { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
        public DateTime? ExportedDT { get; set; }
        public bool WasExported { get; set; }

        //new props https://corebridge.atlassian.net/browse/END-11800
        public string FormattedNumber { get; set; }
        public int? AdjustedReconciliationID { get; set; }
        public DateTime? SyncedDT { get; set; }
        public short? ExportedByID { get; set; }
        public short? SyncedByID { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalPayments { get; set; }
        public bool IsEmpty { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<ReconciliationItem> Items { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData ExportedByEmployee { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData SyncedByEmployee { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Reconciliation AdjustedReconciliation { get; set; }
    }
}
