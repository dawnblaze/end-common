﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace Endor.Models
{
    public partial class EmployeeData:IAtom<short>,IImageCandidate
    {
        public EmployeeData()
        {
            BillingBusinesses = new HashSet<BusinessData>();
            OwnedBusinesses = new HashSet<BusinessData>();
            EmployeeLocators = new HashSet<EmployeeLocator>();
            EmployeeTeamLinks = new HashSet<EmployeeTeamLink>();
            EmployeesReportingToMe = new HashSet<EmployeeData>();
            UserLinks = new HashSet<UserLink>();
        }

        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [StringLength(16)]
        public string Prefix { get; set; }
        [Required]
        [StringLength(100)]
        public string First { get; set; }
        [StringLength(100)]
        public string Middle { get; set; }
        [Required]
        [StringLength(100)]
        public string Last { get; set; }
        [StringLength(16)]
        public string Suffix { get; set; }
        [StringLength(100)]
        public string NickName { get; set; }
        [StringLength(201)]
        public string ShortName { get; set; }
        [StringLength(238)]
        public string LongName { get; set; }
        [StringLength(128)]
        public string Position { get; set; }
        public bool? GenderType { get; set; }
        public DateTime? HireDate { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public byte? BirthDayOfMonth { get; set; }
        public byte? BirthMonth { get; set; }
        public short? BirthYear { get; set; }
        public short? TimeZoneID { get; set; }
        public byte LocationID { get; set; }
        public bool HasImage { get; set; }
        public short? ReportsToID { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? DefaultEmailAccountID { get; set; }

        public Module? DefaultModule { get; set; }

        [JsonIgnore]
        public EnumModule DefaultEnumModule { get; set; }

        public BusinessData Business { get; set; }
        public EmployeeData ReportsTo { get; set; }
        public ICollection<BusinessData> BillingBusinesses { get; set; }
        public ICollection<BusinessData> OwnedBusinesses { get; set; }
        public ICollection<EmployeeLocator> EmployeeLocators { get; set; }
        public ICollection<EmployeeTeamLink> EmployeeTeamLinks { get; set; }
        public ICollection<EmployeeData> EmployeesReportingToMe { get; set; }
        public ICollection<UserLink> UserLinks { get; set; }


        public LocationData Location { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmailAccountData DefaultEmailAccount { get; set; }

        public ICollection<OrderEmployeeRole> OrderRoles { get; set; }

        public ICollection<BoardEmployeeLink> BoardLinks { get; set; }

        [JsonIgnore]
        public ICollection<EmployeeCustomDataValue> CustomDataValues { get; set; }

    }
}
