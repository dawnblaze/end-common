﻿
namespace Endor.Models
{
    public partial class LocationLocator : BaseLocator<byte, LocationData>
    {
        public override byte ParentID { get; set; }

        public override LocationData Parent { get; set; }

        public override EnumLocatorSubType LocatorSubTypeNavigation { get; set; }

        public override EnumLocatorType LocatorTypeNavigation { get; set; }
    }
}
