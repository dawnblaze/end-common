﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public partial class SimpleAlertDefinition : SimpleListItem<short>
    {
        public DataType DataType { get; set; }

        public short EmployeeID { get; set; }
    }
}
