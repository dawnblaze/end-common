﻿namespace Endor.Models
{
    public enum LocatorType
    {
        Unknown = 0,
        Address,
        Phone,
        Email,
        WebAddress,
        Twitter,
        Facebook,
        LinkedIn,
        FTP,
        IP
    }
}
