﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum OrderOrderStatus : byte
    {
        EstimatePending = 11,
        EstimateApproved = 16,
        EstimateLost = 18,
        EstimateVoided = 19,
        OrderPreWIP = 21,
        OrderWIP = 22,
        OrderBuilt = 23,
        OrderInvoicing = 24,
        OrderInvoiced = 25,
        OrderClosed = 26,
        OrderVoided = 29,
        Holding = 31,
        Active = 32,
        Approved = 36,
        Lost = 38,
        Voided = 39,
        PORequested = 41,
        POApproved = 43,
        POOrdered = 45,
        POReceived = 46,
        POVoided = 49,
        DestinationPending = 61,
        DestinationReady = 63,
        DestinationCompleted = 66,
        CreditMemoUnposted = 81,
        CreditMemoPosted = 86,
        CreditMemoVoided = 89,
    }

    public class EnumOrderOrderStatus
    {
        public OrderOrderStatus ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public byte TransactionType { get; set; }

        [Required]
        [StringLength(14)]
        public string TransactionTypeText { get; set; }

        public bool CanRename { get; set; }

        public bool CanHide { get; set; }

        public OrderOrderStatus? FixedItemStatusID { get; set; }

        public bool EnableCustomItemStatus { get; set; }

        public bool EnableCustomItemSubStatus { get; set; }
    }
}
