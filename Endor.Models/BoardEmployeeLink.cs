﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Endor.Models
{
    public class BoardEmployeeLink
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short BoardID { get; set; }

        public short EmployeeID { get; set; }

        public short SortIndex { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsFavorite { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Module ModuleType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EmployeeData Employee { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public BoardDefinitionData BoardDefinition { get; set; }

        [JsonIgnore]
        public EnumModule EnumModule { get; set; }
    }
}
