﻿using Endor.Units;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public interface ILayoutVariable<T>
        where T: struct, IConvertible
    {
        T ID { get; set; }
        int ClassTypeID { get; set; }
        DateTime ModifiedDT { get; set; }

        DataType DataType { get; set; }
        Unit? UnitID { get; set; }
        UnitType? UnitType { get; set; }
        AssemblyElementType ElementType { get; set; }
        DataType? ListDataType { get; set; }
        AssemblyLabelType? LabelType { get; set; }
        CustomFieldNumberDisplayType? DisplayType { get; set; }

        Guid? TempID { get; set; }
        string Name { get; set; }

        bool? AllowCustomValue { get; set; }
        bool? AllowDecimals { get; set; }
        bool? AllowMultiSelect { get; set; }
        string CustomUnitText { get; set; }
        byte? DecimalPlaces { get; set; }
        string DefaultValue { get; set; }
        byte ElementUseCount { get; set; }
        bool? GroupOptionsByCategory { get; set; }
        bool IsDisabled { get; set; }
        bool IsRequired { get; set; }
        string Label { get; set; }
        string ListValuesJSON { get; set; }
        string Tooltip { get; set; }
    }

    public interface IAssemblyVariable : ILayoutVariable<int>
    {
        bool IsFormula { get; set; }

        int? LinkedAssemblyID { get; set; }
        int? LinkedLaborID { get; set; }
        short? LinkedMachineID { get; set; }
        int? LinkedMaterialID { get; set; }
        bool RollupLinkedPriceAndCost { get; set; }

        bool EnableProfileOV { get; set; }
        string ProfileSetupHint { get; set; }
        string ProfileSetupLabel { get; set; }

        string AltText { get; set; }
        bool IsAltTextFormula { get; set; }

        string ConsumptionDefaultValue { get; set; }
        bool IsConsumptionFormula { get; set; }

        ICollection<AssemblyVariableFormula> Formulas { get; set; }
    }

    public interface ICustomFieldDefinition : ILayoutVariable<short>
    {
        bool? AllowTextFormatting { get; set; }
        bool? AllowUserTypedOptions { get; set; }

        bool? ToggleOptions { get; set; }
        bool? AllowNotSetOption { get; set; }
        string OptionOneLabel { get; set; }
        string OptionTwoLabel { get; set; }

        byte? GroupType { get; set; }
        string ListOptions { get; set; }

        byte? PickerType { get; set; }
        bool? PresetTime { get; set; }
        DateTime? Time { get; set; }
    }

}
