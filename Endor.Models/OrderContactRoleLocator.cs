﻿
namespace Endor.Models
{
    public partial class OrderContactRoleLocator : BaseLocator<int, OrderContactRole>
    {
        public override int ParentID { get; set; }

        public override OrderContactRole Parent { get; set; }

        public override EnumLocatorSubType LocatorSubTypeNavigation { get; set; }

        public override EnumLocatorType LocatorTypeNavigation { get; set; }
    }
}
