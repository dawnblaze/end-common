﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{

    public enum PaymentTransactionType : byte
    {
        Payment = 1,
        Refund = 2,
        Payment_from_Refundable_Credit = 3,
        Refund_to_Refundable_Credit = 4,
        Payment_from_Nonrefundable_Credit = 5,
        Refund_to_Nonrefundable_Credit = 6,
        Credit_Gift = 7,
        Credit_Adjustment = 8
    }

    //[Table("enum.Accounting.PaymentTransactionType")] //VERIFY
    public class EnumAccountingPaymentTransactionType
    {
        //[Column("ID", TypeName = "tinyint")]
        public byte ID { get; set; }

        //[Column("Name", TypeName = "VARCHAR(100)")]
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
