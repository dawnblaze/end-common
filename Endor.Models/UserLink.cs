﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class UserLink : IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public short? EmployeeID { get; set; }

        public int? ContactID { get; set; }

        public UserAccessType UserAccessType { get; set; }

        /// <summary>
        /// Copied from Master database's [User.Data].[UserName] column
        /// </summary>
        [StringLength(255)]
        public string UserName { get; set; }

        /// <summary>
        /// Copied from Master database's [User.Data].[UserName] column
        /// </summary>
        [StringLength(100)]
        public string DisplayName { get; set; }

        public EnumUserAccessType UserAccessTypeEnum { get; set; }

        public short? RightsGroupListID { get; set; }

        public int? AuthUserID { get; set; }

        public Guid? TempUserID { get; set; }

        public EmployeeData Employee { get; set; }

        public ContactData Contact { get; set; }

        public RightsGroupList RightsGroupList { get; set; }

        public DateTime? LastConnectionDT { get; set; }

        public DateTime? LastDisconnectionDT { get; set; }

        public DateTime? LastActivityDT { get; set; }
    }
}
