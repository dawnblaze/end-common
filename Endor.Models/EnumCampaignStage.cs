﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class EnumCampaignStage
    {
        public byte ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }
}
