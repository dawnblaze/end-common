﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class EmployeeTeam : IAtom<int>, IImageCandidate
    {
        public EmployeeTeam()
        {
            Campaigns = new HashSet<CampaignData>();
            Companies = new HashSet<CompanyData>();
            EmployeeTeamLinks = new HashSet<EmployeeTeamLink>();
            EmployeeTeamLocationLinks = new HashSet<EmployeeTeamLocationLink>();
            Opportunities = new HashSet<OpportunityData>();
        }

        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public bool HasImage { get; set; }
        public bool IsAdHocTeam { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<CampaignData> Campaigns { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<CompanyData> Companies { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<EmployeeTeamLink> EmployeeTeamLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<EmployeeTeamLocationLink> EmployeeTeamLocationLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<OpportunityData> Opportunities { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleLocationData> SimpleLocations { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<EmailAccountTeamLink> EmailAccountTeamLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<EmailAccountData> EmailAccounts { get; set; }
    }
}