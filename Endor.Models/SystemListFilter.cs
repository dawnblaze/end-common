﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class SystemListFilter : IAtom<int>
    {
        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public int TargetClassTypeID { get; set; }

        public string Hint { get; set; }

        public byte SortIndex { get; set; }

        #region xml

        [JsonIgnore]
        public string CriteriaXML { get; set; }

        /// <summary>
        /// An array of the individual filter items applied to this ListFilter.This is only used for Dynamic Lists.
        /// </summary>
        [JsonProperty("Criteria")]
        public ListFilterItem[] CriteriaObject
        {
            get
            {
                return XMLUtilities.DeserializeMetaDataFromXML(this.CriteriaXML, typeof(ListFilterItem[])) as ListFilterItem[];
            }
            set
            {
                this.CriteriaXML = XMLUtilities.SerializeMetaDataToXML(value, typeof(ListFilterItem[]));
            }
        }

        [JsonIgnore]
        public string IDsXML { get; set; }

        /// <summary>
        /// An array of IDs to use for this list.This property is only used for Static Lists.
        /// </summary>
        [JsonProperty("IDs")]
        public int[] IDsObject
        {
            get
            {
                return XMLUtilities.DeserializeMetaDataFromXML(this.IDsXML, typeof(int[])) as int[];
            }
            set
            {
                this.IDsXML = XMLUtilities.SerializeMetaDataToXML(value, typeof(int[]));
            }
        }

        #endregion

        #region booleans

        public bool IsActive { get; set; }

        #endregion

        int IAtom<int>.ID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        short IAtom.BID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        int IAtom.ClassTypeID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        DateTime IModifyDateTimeTrackable.ModifiedDT { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
