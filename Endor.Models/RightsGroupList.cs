﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Endor.Models
{
    public class RightsGroupList: IAtom<short>
    {
        [JsonIgnore]
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        [Required]
        public DateTime ModifiedDT { get; set; }

        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public bool IsUserSpecific { get; set; }

        [StringLength(500)]
        public string RightsArray { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<RightsGroup> RightsGroups { get; set; }

        public ICollection<UserLink> UserLinks { get; set; }

        public ICollection<RightsGroupListRightsGroupLink> RightsGroupRightLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<EmployeeData> Employees { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<ContactData> Contacts { get; set; }

        //SimpleMappings
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleEmployeeData> SimpleEmployees { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleContactData> SimpleContacts { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleRightsGroup> SimpleRightsGroup { get; set; }
    }
}
