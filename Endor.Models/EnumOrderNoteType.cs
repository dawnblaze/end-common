﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum OrderNoteType : byte
    {
        Other = 1,
        Sales = 2,
        Vendor = 4,
        Design = 5,
        Production = 6,
        Customer = 7
    }

    public class EnumOrderNoteType
    {
        public OrderNoteType ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        public OrderTransactionType TransactionTypeSet { get; set; }
        public OrderTransactionLevel TransactionLevelSet { get; set; }
        public bool AppliesToEstimate { get; set; }
        public bool AppliesToOrder { get; set; }
        public bool AppliesToPO { get; set; }
        public bool IsHeaderLevel { get; set; }
        public bool IsDestinationLevel { get; set; }
        public bool IsItemLevel { get; set; }
    }
}
