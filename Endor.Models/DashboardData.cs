﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    /// <summary>
    /// Dashboard
    /// </summary>
    public class DashboardData : IAtom<short>
    {
        /// <summary>
        /// Business ID
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }
        /// <summary>
        /// Dashboard ID
        /// </summary>
        public short ID { get; set; }
        /// <summary>
        /// Class Type ID
        /// </summary>
        public int ClassTypeID { get; set; }
        /// <summary>
        /// DateTime Last Modified
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if the board is active.
        /// </summary>
        [Required]
        public bool IsActive { get; set; }

        /// <summary>
        /// Dashboard name.
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// Dashboard description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The current number of Columns in the Dashboard.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte? Cols { get; set; }

        /// <summary>
        /// The current number of Rows in the Dashboard.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public byte? Rows { get; set; }

        /// <summary>
        /// User this board is for.  Null for system boards.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? UserLinkID { get; set; }

        /// <summary>
        /// Module for this dashboard.
        /// </summary>
        [JsonConverter(typeof(StringEnumConverter))]
        public Module Module { get; set; }

        /// <summary>
        /// The SecurityRightID required to view this dashboard.  This is usually used to hide System Dashboards from users with certain rights.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? SecurityRightID { get; set; }

        /// <summary>
        /// The datetime the board (and ALL of the widgets) was last updated.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? LastUpdatedDT { get; set; }

        /// <summary>
        /// The widgets associated with this dashboard.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<DashboardWidgetData> Widgets { get; set; }

        /// <summary>
        /// The UserLink associated with this dashboard.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public UserLink UserLink { get; set; }
    }
}
