﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class CustomFieldLayoutDefinition : IAtom<short>
    {
        public CustomFieldLayoutDefinition()
        {
            Elements = new HashSet<CustomFieldLayoutElement>();
        }

        //IAtom
        public short BID { get; set; }
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }

        //props
        public bool IsActive { get; set; }
        public bool IsSystem { get; set; }
        [Required]
        public string Name { get; set; }
        public short AppliesToClassTypeID { get; set; }
        public bool IsAllTab { get; set; }
        public bool IsSubTab { get; set; }
        public short SortIndex { get; set; }

        //nav props
        public ICollection<CustomFieldLayoutElement> Elements { get; set; }

    }
}
