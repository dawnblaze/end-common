﻿
namespace Endor.Models
{
    public partial class TaxGroupItemLink
    {
        public short BID { get; set; }

        public short GroupID { get; set; }

        public short ItemID { get; set; }

        public TaxItem TaxItem { get; set; }

        public TaxGroup TaxGroup { get; set; }
    }
}
