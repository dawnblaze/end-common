﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public class SurchargeDef : IAtom<short>
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID of this object (Unique within the Business).
        /// </summary>
        public short ID { get; set; }

        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 12060.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// (Read Only) The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if the Surcharge is Active.  Only Active Surcharges may be applied, though Inactive Surcharges on a Line Item will remain.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// The Name of the Surcharge.  The name must be unique for the business.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The ID of the TaxabilityCode Object for this income.
        /// </summary>
        public short TaxCodeID { get; set; }

        /// <summary>
        /// The Default Fixed Fee for the Surcharge.  The total default fee includes both Fixed and Per-Unit fees. 
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? DefaultFixedFee { get; set; }

        /// <summary>
        /// The Default Per-Unit Fee for the Surcharge.  The total default fee includes both Fixed and Per-Unit fees. 
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? DefaultPerUnitFee { get; set; }

        /// <summary>
        /// The ID of the GLAccount Object to record the Income in.
        /// </summary>
        public int IncomeAccountID { get; set; }

        /// <summary>
        /// A flag indicating if this Surcharge should be Applied by Default to new Line Items.
        /// </summary>
        public bool AppliedByDefault { get; set; }

        /// <summary>
        /// The CompanyID for this object.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? CompanyID { get; set; }

        /// <summary>
        /// The SortIndex for this object.
        /// </summary>
        public short SortIndex { get; set; }

        /// <summary>
        /// The TaxabilityCode Object for this income.
        /// </summary>
        [JsonIgnore]
        public TaxabilityCode TaxCode { get; set; }

        /// <summary>
        /// The Company for this object.
        /// </summary>
        [JsonIgnore]
        public CompanyData Company { get; set; }

        /// <summary>
        /// The GLAccount Object to record the Income in.
        /// </summary>
        [JsonIgnore]
        public GLAccount IncomeAccount { get; set; }
    }
}
