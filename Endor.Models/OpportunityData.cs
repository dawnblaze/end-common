﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Endor.Models
{
    public partial class OpportunityData
    {
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Probability { get; set; }
        public decimal? WeightedAmount { get; set; }
        public byte StatusID { get; set; }
        public DateTime? ExpectedDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        public string Notes { get; set; }
        public int? CompanyID { get; set; }
        public int? ContactID { get; set; }
        public int? TeamID { get; set; }
        public int? CampaignID { get; set; }
        public int? OriginID { get; set; }
        public byte LocationID { get; set; }
        public BusinessData Business { get; set; }
        public CampaignData Campaign { get; set; }
        public CompanyData Company { get; set; }
        public ContactData Contact { get; set; }
        public EmployeeTeam EmployeeTeam { get; set; }

        [JsonIgnore]
        public ICollection<OpportunityCustomDataValue> CustomDataValues { get; set; }
    }
}
