﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class MessageHeader : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }

        public int ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public DateTime ReceivedDT { get; set; }

        public short EmployeeID { get; set; }

        public int ParticipantID { get; set; }

        public int BodyID { get; set; }

        public bool IsRead { get; set; }

        public DateTime? ReadDT { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOrExpiredDT { get; set; }

        public bool IsExpired { get; set; }

        [Required]
        public MessageChannelType Channels { get; set; }

        public bool InSentFolder { get; set; }

        //navprops
        public MessageBody MessageBody { get; set; }

        //unmapped properties
        /// <summary>
        /// Not part of EF or any DB column. Only used for lucene indexing
        /// </summary>
        public string _messageBodyString { get; set; }

    }
}
