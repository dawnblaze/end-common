﻿namespace Endor.Models
{
    public partial class CompanyLocator : BaseLocator<int, CompanyData>
    {
        public override int ParentID { get; set; }

        public override CompanyData Parent { get; set; }
        public override EnumLocatorSubType LocatorSubTypeNavigation { get; set; }
        public override EnumLocatorType LocatorTypeNavigation { get; set; }
    }
}
