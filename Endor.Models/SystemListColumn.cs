﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class SystemListColumn:ICustomizableListColumn
    {
        public short ID { get; set; }
        public int ClassTypeID { get; set; }
        public int TargetClassTypeID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [StringLength(255)]
        public string HeaderText { get; set; }
        [StringLength(255)]
        public string Field { get; set; }
        public bool IsSortable { get; set; }
        [StringLength(255)]
        public string SortField { get; set; }
        public bool IsFrozen { get; set; }
        public bool IsExpandable { get; set; }
        [StringLength(255)]
        public string StyleClass { get; set; }
        public byte SortIndex { get; set; }
        public bool IsVisible { get; set; }
    }
}
