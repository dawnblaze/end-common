﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Endor.Models
{
    public static class XMLUtilities
    {
        public static object DeserializeMetaDataFromXML(string metaDataXML, LocatorType type)
        {
            return DeserializeMetaDataFromXML(metaDataXML, type.MetaDataClass());
        }

        public static string SerializeMetaDataToXML(object metaData, LocatorType type)
        {
            return SerializeMetaDataToXML(metaData, type.MetaDataClass());
        }

        public static object DeserializeMetaDataFromXML(string metaDataXML, Type type)
        {
            if (String.IsNullOrWhiteSpace(metaDataXML))
                return null;

            using (var stringReader = new StringReader(metaDataXML))
            {
                return new XmlSerializer(type).Deserialize(stringReader);
            }
        }

        public static string SerializeMetaDataToXML(object metaData, Type metaDataType)
        {
            if (metaData == null)
                return null;

            using (var stringWriter = new StringWriter())
            {
                if (metaData is Newtonsoft.Json.Linq.JObject)
                {
                    metaData = (metaData as Newtonsoft.Json.Linq.JObject).ToObject(metaDataType);
                }

                new XmlSerializer(metaDataType).Serialize(stringWriter, metaData);
                return stringWriter.ToString();
            }
        }
    }
}
