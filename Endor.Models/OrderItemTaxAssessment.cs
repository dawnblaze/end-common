﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    public partial class OrderItemTaxAssessment : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        [JsonIgnore]
        public DateTime ModifiedDT { get; set; }
        //This column is always "Infinite" in the live table and get the current DT when moved to the archive table. 
        //That's how it keeps track when it is no longer the current record
        [JsonIgnore]
        public DateTime ValidToDT { get; set; }
        public int OrderID { get; set; }
        public int? OrderItemID { get; set; }
        public int? DestinationID { get; set; }
        public short TaxCodeID { get; set; }
        public short? TaxItemID { get; set; }
        public decimal TaxableAmount { get; set; }
        public decimal TaxRate { get; set; }
        public decimal TaxAmount { get; set; }

        public int? ItemComponentID { get; set; }
        public int? ItemSurchargeID { get; set; }
        public short? TaxGroupID { get; set; }


        public OrderData Order { get; set; }

        public OrderItemData OrderItem { get; set; }

        public OrderDestinationData Destination  { get; set; }

        public TaxabilityCode TaxCode { get; set; }

        public TaxItem TaxItem { get; set; }

        public OrderItemComponent ItemComponent { get; set; }
        public OrderItemSurcharge ItemSurcharge { get; set; }
        public TaxGroup TaxGroup { get; set; }
    }
}
