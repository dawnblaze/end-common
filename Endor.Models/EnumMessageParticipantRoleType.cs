﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{


    public enum MessageParticipantRoleType : byte
    {
        From = 1,
        To = 2,
        CC = 3,
        BCC = 4,

    }

    //[Table("enum.Message.Participant.RoleType")] //VERIFY
    public partial class EnumMessageParticipantRoleType
    {
        //[Column("ID", TypeName = "tinyint")]
        public MessageParticipantRoleType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
