﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class DashboardWidgetCategoryLink
    {
        /// <summary>
        /// The ID of the Widget Def Object being linked.
        /// </summary>
        public short WidgetDefID { get; set; }

        /// <summary>
        /// The ID of the Associate Record.
        /// </summary>
        [Required]
        public DashboardWidgetCategoryType CategoryType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumDashboardWidgetCategoryType EnumDashboardWidgetCategoryType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DashboardWidgetDefinition WidgetDefinition { get; set; }
    }
}
