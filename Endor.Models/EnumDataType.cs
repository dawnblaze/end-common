﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum DataType: short
    {
        None = 0,

        String = 1,
        Number = 2,
        Boolean = 3,
        DateTime = 9,

        OrderStatus = 100,
        TimeZone = 141,
        FlatList = 1902,

        Location = ClassType.Location,
        Company = ClassType.Company,
        CompanyCustomField = ClassType.CompanyCustomField,
        Industry = ClassType.CrmIndustry,
        Origin = ClassType.CrmOrigin,
        Contact = ClassType.Contact,
        ContactCustomField = ClassType.ContactCustom,
        Employee = ClassType.Employee,
        EmployeeCustomField = ClassType.EmployeeCustomField,
        EmployeeRole = ClassType.EmployeeRole,
        Team = ClassType.EmployeeTeam,
        Opportunity = ClassType.Opportunity,
        OpportunityCustomField = ClassType.OpportunityCustomField,
        OrderCustomField = ClassType.OrderCustom,
        Order = ClassType.Order,
        EstimateCustomField = ClassType.EstimateCustom,
        Estimate = ClassType.Estimate,
        PurchaseOrder = ClassType.PurchaseOrder,
        OrderContactRole = ClassType.OrderContactRole,
        OrderEmployeeRole = ClassType.OrderEmployeeRole,
        OrderNotes = ClassType.OrderNote,
        LineItem = ClassType.OrderItem,
        ItemStatus = ClassType.OrderItemStatus,
        Substatus = ClassType.OrderItemSubStatus,
        Destination = ClassType.OrderDestination,
        DestinationStatus = ClassType.OrderDestinationStatus,
        TaxGroup = ClassType.TaxGroup,
        PaymentTerm = ClassType.PaymentTerm,
        MaterialPart = ClassType.Material,
        LaborPart = ClassType.Labor,
        MachinePart = ClassType.Machine, 
        SurchargeDef = ClassType.SurchargeDef,
        OrderItemComponent = ClassType.OrderItemComponent,
        Assembly = ClassType.Assembly,
        AssemblyElement = ClassType.AssemblyElement,
        UserLink = ClassType.UserLink,

        // Collections are the same, but with a negative ID
        BooleanList = -Boolean,
        NumberList = -Number,
        StringList = -String,
        DateTimeList = -DateTime,

        OrderStatusList = -OrderStatus,

        LocationList = -Location,
        FlatListList = -FlatList,
        CompanyList = -Company,
        CompanyCustomFieldList = -CompanyCustomField,
        IndustryList = -Industry,
        OriginList = -Origin,
        ContactList = -Contact,
        ContactCustomFieldList = -ContactCustomField,
        EmployeeList = -Employee,
        EmployeeCustomFieldList = -EmployeeCustomField,
        EmployeeRoleList = -EmployeeRole,
        TeamList = -Team,
        OpportunityList = -Opportunity,
        OpportunityCustomFieldList = -OpportunityCustomField,
        OrderCustomFieldList = -OrderCustomField,
        OrderList = -Order,
        EstimateCustomFieldList = -EstimateCustomField,
        EstimateList = -Estimate,
        PurchaseOrderList = -PurchaseOrder,
        OrderContactRoleList = -OrderContactRole,
        OrderEmployeeRoleList = -OrderEmployeeRole,
        OrderNotesList = -OrderNotes,
        LineItemList = -LineItem,
        ItemStatusList = -ItemStatus,
        SubstatusList = -Substatus,
        DestinationList = -Destination,
        DestinationStatusList = -DestinationStatus,
        TaxGroupList = -TaxGroup,
        MaterialPartList = -MaterialPart,
        LaborPartList = -LaborPart,
        MachinePartList = -MachinePart,
        SurchargeDefList = -SurchargeDef,
        OrderItemComponentList = -OrderItemComponent,
        AssemblyList = -Assembly,

        //Categories start at 20000
        OrderCategory = 20010,
        LocationCategory = 20020,
        DestinationCategory = 20030,
        LineItemCategory = 20040,
        TaxesCategory = 20050,
        PricesCategory = 20060,
        CostCategory = 20070,
        PaymentCategory = 20090,
        TransactionHeaderNotesCategory = 20100,
        LineItemNotesCategory = 20101,
        ContactCategory = 20120,
        LineItemStatusCategory = 20130,
        CompanyTypeCategory = 20140,
        CompanyTaxCategory = 20150,
        SubsidiariesCategory = 20160,
        CustomerCreditCategory = 20170,
        PONumberCategory = 20180,
        TransactionHeaderContactRoleCategory = 20190,
        TransactionHeaderEmployeeRoleListCategory = 20200,
        TransactionHeaderEmployeeRoleList = 20210,
        TransactionHeaderTagsCategory = 20211,
        EmployeeCategory = 20220,
        OrderDateCategory = 20230,
        TaxExemptReason = 20240,
        EstimateCategory = 20250,
        EstimateDateCategory = 20260,
        OrderItemComponentCategory = 20280,
        IComponentDef = 20290,
        ComponentType = 20300,
        CompanyCategory = 20310,
        CompanyContactListCategory = 20320,
        CompanyEmployeeListCategory = 20330,
        CompanyEmployeeRoleListCategory = 20340,
        CompanyEmployeeRoleCategory = 20350,
        CompanyDatesCategory = 20360,
        CompanyTagsCategory = 20361,
        OrderItemPricesCategory = 20370,
        OrderItemLocationCategory = 20380,
        OrderItemTaxesCategory = 20390,
        OrderItemCostsCategory = 20400,
        OrderItemTagsCategory = 20410,
        OrderItemContactRoleCategory = 20430,
        OrderItemEmployeeRoleListCategory = 20440,
        OrderItemEmployeeRoleList = 20450,


        // These enum values are used
        // in order to signify a type that can reflect
        // any type, any value type, or any object type.
        //
        // They are not real value types and should not be used in expressions
        AnyObjectScalar = 32005,
        AnyObjectList = -32005
    }

    public partial class EnumDataType
    {
        public EnumDataType()
        {
        }

        public DataType ID { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
    }

    public class BoardDataTypeMap
    {
        public string DetailLevel;
        public string Type;
    }

    public static class DataTypeExtensions
    {
        private static readonly Dictionary<DataType, BoardDataTypeMap> TypeMap = new Dictionary<DataType, BoardDataTypeMap>()
        {
            {
                DataType.Company, new BoardDataTypeMap { Type = "Company", DetailLevel = "Company Level" }
            },
            {
                DataType.Contact, new BoardDataTypeMap { Type = "Company", DetailLevel = "Contact Level" }
            },
            {
                DataType.Estimate, new BoardDataTypeMap { Type = "Estimate", DetailLevel = "Estimate Level" }
            },
            {
                DataType.Order, new BoardDataTypeMap { Type = "Order", DetailLevel = "Order Level" }
            },
            {
                DataType.Destination, new BoardDataTypeMap { Type = "Order/Estimate", DetailLevel = "Order Destination Level" }
            },
            {
                DataType.LineItem, new BoardDataTypeMap { Type = "Order/Estimate", DetailLevel = "Order/Estimate Line Item Level" }
            },
            {
                DataType.Opportunity, new BoardDataTypeMap { Type = "Opportunity", DetailLevel ="Opportunity Level" }
            }
        };

        public static BoardDataTypeMap GetBoardDataTypeMappings(this DataType input)
        {
            return TypeMap.ContainsKey(input) ? TypeMap[input] : null;
        }
    }
}
