﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{

    public class MachineCategory : IAtom<short>
    {
        
        public short BID { get; set; }

        public short ID { get; set; }

        public int ClassTypeID { get; set; }

        public DateTime ModifiedDT { get; set; }

        public bool IsActive { get; set; }

        [Required]
        public string Name { get; set; }

        /// <summary>
        /// A general description of the Category
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ID of the Parent Category this Category is under.
        /// </summary>
        public short? ParentID { get; set; }


        public MachineCategory ParentCategory { get; set; }

        public ICollection<MachineCategory> ChildCategories { get; set; }

        public ICollection<MachineCategoryLink> MachineCategoryLinks { get; set; }

        public ICollection<MachineData> Machines { get; set; }

        public ICollection<SimpleMachineData> SimpleMachines { get; set; }
    }
}
