﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    /// <summary>
    /// The FlatListItem Object stores a single record in a FlatList Object. 
    /// </summary>
    public class FlatListItem : IAtom<short>
    {
        public short ID { get; set; }
        [JsonIgnore]
        public short BID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }

        public FlatListType FlatListType { get; set; }

        public bool IsActive { get; set; }

        public bool IsSystem { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [JsonIgnore]
        public bool IsAdHoc { get; set; }

        public short? SortIndex { get; set; }
    }
}
