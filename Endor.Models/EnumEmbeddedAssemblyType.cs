﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    /// <summary>
    /// https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/1001160837/EmbeddedAssemblyType+Enum
    /// </summary>

    public enum EmbeddedAssemblyType : byte
    {
        LayoutComponent = 1
    }
    
    public class EnumEmbeddedAssemblyType
    {
        public EmbeddedAssemblyType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
