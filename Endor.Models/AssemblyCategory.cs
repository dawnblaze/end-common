﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public class AssemblyCategory : IAtom<short>
    {
        /// <summary>
        /// The Business ID for the Assembly.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID for the Assembly.
        /// </summary>
        public short ID { get; set; }

        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 12042.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The date time the object was last modified.  Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if the Assembly Category is Active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// The name of the assembly category
        /// </summary>
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        /// <summary>
        /// The description of the Assembly Category
        /// </summary>
        [StringLength(255)]
        public string Description { get; set; }

        /// <summary>
        /// ID of the Parent Category this Category is under.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? ParentID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public AssemblyCategory ParentCategory { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyCategory> ChildCategories { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyCategoryLink> AssemblyCategoryLinks { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<AssemblyData> Assemblies { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<SimpleAssemblyData> SimpleAssemblies { get; set; }
    }
}
