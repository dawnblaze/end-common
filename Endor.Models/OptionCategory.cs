﻿using System.Collections.Generic;

namespace Endor.Models
{
    public partial class OptionCategory
    {
        public OptionCategory()
        {
            OptionDefinitions = new HashSet<OptionDefinition>();
        }

        public short ID { get; set; }
        public string Name { get; set; }
        public short SectionID { get; set; }
        public string Description { get; set; }
        public byte OptionLevels { get; set; }
        public bool IsHidden { get; set; }
        public string SearchTerms { get; set; }
        public bool IsSystemOption { get; set; }
        public bool IsAssociationOption { get; set; }
        public bool IsBusinessOption { get; set; }
        public bool IsLocationOption { get; set; }
        public bool IsStorefrontOption { get; set; }
        public bool IsCompanyOption { get; set; }
        public bool IsContactOption { get; set; }
        public bool IsUserOption { get; set; }

        public virtual ICollection<OptionDefinition> OptionDefinitions { get; set; }
        public virtual OptionSection OptionSection { get; set; }
    }
}
