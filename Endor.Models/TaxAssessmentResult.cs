﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    /// <summary>
    /// A Tax Assessment Result
    /// </summary>
    public class TaxAssessmentResult
    {
        public TaxAssessmentResult()
        {
            Items = new List<TaxAssessmentItemResult>();
        }

        /// <summary>
        /// The quantity of items taxed.
        /// <para /> For line items taxed by destination, this is the destination quantity.
        /// </summary>
        public decimal Quantity { get; set; }

        /// <summary>
        /// The price that tax was being computed on
        /// </summary>
        public decimal Price { get; set; }

        public string InvoiceText { get; set; }

        /// <summary>
        /// List of the TaxItems computed for this product.
        /// </summary>
        public List<TaxAssessmentItemResult> Items { get; set; }

        /// <summary>
        /// Combined Tax Rate used in the calculation = Sum(Items.TaxRate)
        /// </summary>
        public decimal TaxRate { get; set; }

        /// <summary>
        /// Flag indicating if the TaxRate was zero = (TaxRate == 0.0)
        /// </summary>
        public bool IsTaxExempt { get; set; }

        /// <summary>
        /// Taxable Price used for the computation, and whether this was overridden.
        /// <para /> Note - if the PriceTaxable is overridden at more than the SaleAmount,
        /// the entire sale is assumed to be Tax Exempt.
        /// </summary>
        public decimal PriceTaxable { get; set; }

        /// <summary>
        /// If the TaxablePrice has been overridden
        /// </summary>
        public bool PriceTaxableOV { get; set; }

        /// <summary>
        /// Amount of Tax Computed = Sum(Items.TaxAmount)
        /// </summary>
        public decimal TaxAmount { get; set; }

        /// <summary>
        /// The tax engine used for calculating the taxes.
        /// <para/> Supplied = 0 --> All tax item rates must be supplied.
        /// <para/> TaxGroup = 1 --> Compute tax rates based on the TaxGroupID
        /// <para/> TaxJar  = 2 --> Use TaxJar service
        /// </summary>
        public TaxEngineType TaxEngine { get; set; }

        /// <summary>
        /// Field for TaxEngine = Internal (0)
        /// </summary>
        public short? TaxGroupID { get; set; }

        public short? TaxCodeID { get; set; }

        public TaxAssessmentResult AddItem(TaxAssessmentItemResult item)
        {
            this.TaxRate += item.TaxRate;
            this.TaxAmount += item.TaxAmount;
            this.Items.Add(item);
            return this;
        }

        public string NexusID { get; set; }

        /// <summary>
        /// Combine two TaxAssessmentResults into a single one.
        /// <para/> This method does not compress the tax items.
        /// </summary>
        /// <param name="P1">First TaxAssessmentResult</param>
        /// <param name="P2">Second TaxAssessmentResult</param>
        /// <returns></returns>
        public static TaxAssessmentResult operator +(TaxAssessmentResult P1, TaxAssessmentResult P2)
        {
            TaxAssessmentResult P = new TaxAssessmentResult()
            {
                OrderItemID = P1.OrderItemID,
                ItemComponentID = P1.ItemComponentID,
                ItemSurchargeID = P1.ItemSurchargeID,
                InvoiceText = P1.InvoiceText,
                NexusID = P1.NexusID,
                Price = P1.Price,
                Quantity = P1.Quantity,
                TaxGroupID = P1.TaxGroupID,
                TaxAmount = P1.TaxAmount + P2.TaxAmount,
                IsTaxExempt = P1.IsTaxExempt && P2.IsTaxExempt,
                TaxRate = System.Math.Max(P1.TaxRate, P2.TaxRate),
                PriceTaxable = P1.PriceTaxable + P2.PriceTaxable,
                PriceTaxableOV = (P1.PriceTaxableOV || P2.PriceTaxableOV)
            };

            P.Items = new List<TaxAssessmentItemResult>();
            P.Items.AddRange(P1.Items);
            P.Items.AddRange(P2.Items);

            return P;
        }

        //OrderItemID
        public int OrderItemID { get; set; }

        //ItemComponentID
        public int? ItemComponentID { get; set; }

        //ItemSurchargeID
        public int? ItemSurchargeID { get; set; }


    }
}
