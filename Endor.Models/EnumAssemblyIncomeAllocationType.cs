﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum AssemblyIncomeAllocationType : byte
    {
        SingleIncomeAllocation = 0,
        AllocationByMaterialIncomeAccount = 1,
        AllocationByMachineIncomeAccount = 2
    }

    public class EnumAssemblyIncomeAllocationType
    {
        public AssemblyIncomeAllocationType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
