﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum UserAccessType : byte
    {
        None = 0,
        Employee = 1,
        EmployeeAdministrator = 9,
        Contact = 11,
        ContactAdministrator = 19,
        FranchiseStaff = 21,
        FranchiseAdmin = 29,
        SupportStaff = 41,
        SupportManager = 49,
        DeveloperStaff = 51,
        DeveloperGod = 59
    }

    public class EnumUserAccessType
    {

        public UserAccessType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
