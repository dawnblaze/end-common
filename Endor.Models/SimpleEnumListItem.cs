﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Models
{
    public class SimpleEnumListItem<T>
    {
        public T ID { get; set; }
        public bool IsActive { get; set; }
        public string DisplayName { get; set; }
        public bool HasImage { get; set; }
    }
}
