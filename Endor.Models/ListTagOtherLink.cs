﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public class ListTagOtherLink
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID of the Tag Object being linked.
        /// </summary>
        public short TagID { get; set; }

        /// <summary>
        /// The ClassType ID of the object being linked.
        /// </summary>
        public int AppliesToClassTypeID { get; set; }

        /// <summary>
        /// The ID of the Associate Record.
        /// </summary>
        public int AppliesToID { get; set; }

        public ListTag Tag { get; set; }
    }
}
