﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Endor.Models
{
    public class ReconciliationItem : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }

        public int ReconciliationID { get; set; }
        public int? GLAccountID { get; set; }
        public int? PaymentMethodID { get; set; }
        public short? PaymentMasterCount { get; set; }
        public short? PaymentApplicationCount { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Balance { get; set; }
        public byte? CurrencyType { get; set; }
        public bool IsPaymentSummary { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Reconciliation Reconciliation { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public GLAccount GLAccount { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public PaymentMethod PaymentMethodNavigation { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public EnumAccountingCurrencyType CurrencyTypeNavigation { get; set; }
    }
}
