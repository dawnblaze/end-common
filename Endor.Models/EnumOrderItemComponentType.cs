﻿using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public enum OrderItemComponentType : short
    {
        Material = ClassType.Material,
        Machine = ClassType.Machine,
        MachineTime = ClassType.MachineTime,
        Labor = ClassType.Labor,
        Assembly = ClassType.Assembly,
        Destination = ClassType.Destination
    }

    public class EnumOrderItemComponentType
    {
        public short ID { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
