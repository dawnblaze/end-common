﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public partial class ActivityGlactivity : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime CreatedDT{ get; set; }
        public DateTime ModifiedDT { get; set; }
        public bool IsActive { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        public byte ActivityType { get; set; }

        public int? CompanyID { get; set; }
        public int? OrderID { get; set; }

        [Required]
        [StringLength(255)]
        public string Subject { get; set; }

        public DateTime StartDT { get; set; }
        public DateTime EndDT { get; set; }

        public string Notes { get; set; }

        public int? TeamID { get; set; }
        public bool IsComplete { get; set; }
        public short? CompletedByID { get; set; }

        public int? CompletedByContactID { get; set; }

        public DateTime? CompletedDT { get; set; }
        public string MetaData { get; set; }

        public byte GLEntryType { get; set; }

        public EnumGLEntryType GLEntryTypeNavigation { get; set; }

        public ICollection<int> GLIDs { get; set; }
        public ICollection<GLData> GL { get; set; }

        public CompanyData Company { get; set; }
        public OrderData Order { get; set; }
        public EmployeeData CompletedBy { get; set; }
        public ContactData CompletedByContact { get; set; }

    }
}
