﻿using System.ComponentModel.DataAnnotations;


namespace Endor.Models
{
    public enum AssemblyFormulaEvalType : byte
    {
        Constant = 0,
        MappedVariable = 1,
        CBEL = 2,
        CSharp = 3,
        CFL = 4
    }

    public class EnumAssemblyFormulaEvalType
    {
        public AssemblyFormulaEvalType ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
