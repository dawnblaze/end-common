using System.Collections.Generic;
using System.Linq;

namespace Endor.Models
{
    /// <summary>.
    /// unmapped to any entity. helper class only
    /// </summary>
    public class MessageBreakdown
    {
        /// <summary>
        /// create an empty MessageBreakdown
        /// </summary>
        public MessageBreakdown()
        {

        }

        /// <summary>
        /// parse an Endor.Models.Message object into it's subparts
        /// </summary>
        /// <param name="msg">Endor.Models.Message</param>
        public MessageBreakdown(Message msg) : this()
        {
            this.MessageHeader.BID = msg.BID;
            this.MessageHeader.ID = msg.ID;
            this.MessageHeader.ClassTypeID = 14111;
            this.MessageHeader.ModifiedDT = msg.ModifiedDT;
            this.MessageHeader.ReceivedDT = msg.ReceivedDT;
            this.MessageHeader.EmployeeID = msg.EmployeeID;
            this.MessageHeader.ParticipantID = msg.ParticipantID;
            this.MessageHeader.BodyID = msg.BodyID;
            this.MessageHeader.IsRead = msg.IsRead;
            this.MessageHeader.ReadDT = msg.ReadDT;
            this.MessageHeader.IsDeleted = msg.IsDeleted;
            this.MessageHeader.IsExpired = msg.IsExpired;
            this.MessageHeader.DeletedOrExpiredDT = msg.DeletedOrExpiredDT;
            this.MessageHeader.Channels = msg.Channels;
            this.MessageHeader.InSentFolder = msg.IsSentFolder;

            this.MessageBody.Subject = msg.Subject;
            this.MessageBody.BodyFirstLine = msg.BodyFirstLine;
            this.MessageBody.HasBody = msg.HasBody;
            this.MessageBody.AttachedFileCount = msg.AttachedFileCount;
            this.MessageBody.AttachedFileNames = msg.AttachedFileNames;
            this.MessageBody.HasAttachment = msg.HasAttachment;
            this.MessageBody.MetaData = msg.MetaData;
            this.MessageBody.WasModified = msg.WasModified;
            this.MessageBody.SizeInKB = msg.SizeInKB;

            this.BodyString = msg.Body;

            this.MessageParticipantInfos = msg.Participants;
            this.MessageObjectLinks = msg.ObjectLinks;
            this.MessageDeliveryRecords = msg.DeliveryRecords;
        }

        /// <summary>
        /// converts a message breakdown to Endor.Models.Message Object
        /// </summary>
        /// <param name="bodyString"> the actual html string we get from the blob storage </param>
        /// <param name="filterParticipants">  if the employee is the sender (From), then all participants are returned.  If the employee is not the sender, then BCC records are filtered out. </param>
        /// <param name="filterDeliveryRecords"> If the participant is not the Sender, the Message Delivery Records are filtered to those that apply to this employee only. </param>
        public Message ToMessage(string bodyString = null, bool filterParticipants = true, bool filterDeliveryRecords = true)
        {
            var msg = new Message();

            //assign properties here
            msg.BID = this.MessageHeader.BID;
            msg.ID = this.MessageHeader.ID;
            msg.ClassTypeID = 14109;//(Read Only) An ID identifying the type of object.  Always 14109.
            msg.ModifiedDT = this.MessageHeader.ModifiedDT;
            msg.ReceivedDT = this.MessageHeader.ReceivedDT;
            msg.EmployeeID = this.MessageHeader.EmployeeID;
            msg.ParticipantID = this.MessageHeader.ParticipantID;
            msg.BodyID = this.MessageHeader.BodyID;
            msg.IsRead = this.MessageHeader.IsRead;
            msg.ReadDT = this.MessageHeader.ReadDT;
            msg.IsDeleted = this.MessageHeader.IsDeleted;
            msg.IsExpired = this.MessageHeader.IsExpired;
            msg.DeletedOrExpiredDT = this.MessageHeader.DeletedOrExpiredDT;
            msg.Channels = this.MessageHeader.Channels;
            msg.IsSentFolder = this.MessageHeader.InSentFolder;

            msg.Body = bodyString ?? this.BodyString;

            msg.Subject = this.MessageBody.Subject;
            msg.BodyFirstLine = this.MessageBody.BodyFirstLine;
            msg.HasBody = this.MessageBody.HasBody;
            msg.AttachedFileCount = this.MessageBody.AttachedFileCount;
            msg.AttachedFileNames = this.MessageBody.AttachedFileNames;
            msg.HasAttachment = this.MessageBody.HasAttachment;
            msg.MetaData = this.MessageBody.MetaData;
            msg.WasModified = this.MessageBody.WasModified;
            msg.SizeInKB = this.MessageBody.SizeInKB;

            msg.Participants = this.MessageBody.MessageParticipantInfos ?? new List<MessageParticipantInfo>();
            msg.ObjectLinks = this.MessageBody.MessageObjectLinks ?? new List<MessageObjectLink>();
            msg.DeliveryRecords = (this.MessageParticipantInfos ?? new List<MessageParticipantInfo>())
                                    .Where(mpi => mpi.MessageDeliveryRecords!=null)
                                    .SelectMany(mpi => mpi.MessageDeliveryRecords)
                                    .ToList();


            var sender = msg.Participants.FirstOrDefault(p => p.ParticipantRoleType == MessageParticipantRoleType.From);

            if (filterParticipants)
            {
                //When retrieving the record, if the employee is the sender (From), then all participants are returned.  
                //If the employee is not the sender, then BCC records are filtered out.
                if (sender?.EmployeeID != msg.EmployeeID)//if not the sender
                {
                    msg.Participants = (this.MessageParticipantInfos ?? new List<MessageParticipantInfo>())
                                        .Where(mpi => mpi.ParticipantRoleType != MessageParticipantRoleType.BCC)
                                        .ToList();
                }
            }

            if (filterDeliveryRecords)
            {

                // https://corebridge.atlassian.net/wiki/spaces/ENDOR/pages/846004318/Message+Object
                // If the employee is the Sender (FROM), then all Delivery Records for this message participant array are returned.
                // If the participant is not the Sender, the Message Delivery Records are filtered to those that apply to this employee only.   

                if (sender?.EmployeeID != msg.EmployeeID)//if not the sender
                {
                    msg.DeliveryRecords = (this.MessageParticipantInfos ?? new List<MessageParticipantInfo>())
                                        .Where(mpi => mpi.EmployeeID == msg.EmployeeID && mpi.MessageDeliveryRecords!=null)
                                        .SelectMany(mpi => mpi.MessageDeliveryRecords)
                                        .ToList();
                }
            }

            return msg;
        }

        public MessageHeader MessageHeader { get; set; } = new MessageHeader();
        public MessageBody MessageBody { get; set; } = new MessageBody();

        public string BodyString { get; set; } = "";

        public ICollection<MessageParticipantInfo> MessageParticipantInfos { get; set; }
        public ICollection<MessageObjectLink> MessageObjectLinks { get; set; }
        public ICollection<MessageDeliveryRecord> MessageDeliveryRecords { get; set; }
    }
}