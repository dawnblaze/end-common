﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class DomainEmail: IAtom<short>
    {
        public DomainEmail()
        {
            IsForAllLocations = true;
        }

        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        [JsonIgnore]
        public short BID { get; set; }

        /// <summary>
        /// The ID of this object (Unique within the Business).
        /// </summary>
        public short ID { get; set; }

        /// <summary>
        /// (Read Only) An ID identifying the type of object.  Always 1022.
        /// </summary>
        public int ClassTypeID { get; set; }

        /// <summary>
        /// The date the domain was created.
        /// </summary>
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// The date time the record was last modified.Updated on every save or action.
        /// </summary>
        public DateTime ModifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if the record is Active.
        /// </summary>
        [Required]
        public bool IsActive { get; set; }

        /// <summary>
        /// The email domain name, without the '@' at the front.
        /// </summary>
        [MaxLength(255)]
        [Required]
        public string DomainName { get; set; }

        /// <summary>
        /// The Email ProviderType enum for this domain
        /// </summary>
        public EmailProviderType ProviderType { get; set; }

        /// <summary>
        /// The SMTPConfigurationType enum for this email
        /// This is only used when the ProviderType is Custom SMTP.
        /// </summary>
        public byte? SMTPConfigurationType { get; set; }

        /// <summary>
        /// The SMTP Address (IP or URL) for the SMTP Provider.
        /// This is only used when the ProviderType is Custom SMTP but it is required in that case.
        /// </summary>
        [MaxLength(255)]
        public string SMTPAddress { get; set; }

        /// <summary>
        /// The SMTP Port.
        ///
        /// This is only used when the ProviderType is Custom SMTP but it is required in that case.
        /// </summary>
        public short? SMTPPort { get; set; }

        /// <summary>
        /// The SMTPSecurityType enum for the email provider
        /// 
        /// This is only used when the ProviderType is Custom SMTP but it is required in that case.
        /// </summary>
        public EmailSecurityType? SMTPSecurityType { get; set; }

        /// <summary>
        /// Flag that indicates if the SMTP connection should authenticate first before trying to retrieve email.
        /// This is only used when the ProviderType is Custom SMTP but it is required in that case.
        /// </summary>
        public bool? SMTPAuthenticateFirst { get; set; }

        /// <summary>
        /// The last time verification was attempted on this domain.
        /// </summary>
        public DateTime? LastVerificationAttemptDT { get; set; }

        /// <summary>
        /// Flag indicating if the last verification attempt was successful.	
        /// </summary>
        public bool? LastVerificationSuccess { get; set; }

        /// <summary>
        /// The Result Text for the last verification attempt.
        /// </summary>
        [MaxLength(255)]
        public string LastVerificationResult { get; set; }

        /// <summary>
        /// The DateTime of the last successful verification for this domain.
        /// </summary>
        public DateTime? LastVerifiedDT { get; set; }

        /// <summary>
        /// Flag indicating if this Email Domain is valid for all locations or must be individually specified.
        /// </summary>
        public bool IsForAllLocations { get; set; }


        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ICollection<DomainEmailLocationLink> LocationLinks { get; set; }

        public EnumEmailSecurityType SecurityType { get; set; }

        public EnumEmailProviderType Provider { get; set; }

        public SystemEmailSMTPConfigurationType ConfigurationType { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<SimpleLocationData> SimpleLocations { get; set; }
    }
}
