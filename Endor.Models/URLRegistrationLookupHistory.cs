﻿using Newtonsoft.Json;
using System;

namespace Endor.Models
{
    /// <summary>
    /// The role of the URL registration Lookup History object is to track each access to a URLRegistration. 
    /// </summary>
    public class URLRegistrationLookupHistory : IAtom<int>
    {
        [JsonIgnore]
        public short BID { get; set; }
        public int ID { get; set; }
        public int ClassTypeID { get; set; }
        public DateTime ModifiedDT { get; set; }
        public int RegistrationID { get; set; }
        public bool WasSuccessful { get; set; }
        public string IP { get; set; }
        public string BrowserTypeName { get; set; }
        public string BrowserVersion { get; set; }
        public string Platform { get; set; }
        public string DeviceName { get; set; }
        public string DeviceInfo { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? MaxWidth { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public short? MaxHeight { get; set; }

        public virtual URLRegistrationData Registration { get; set; }

    }
}
