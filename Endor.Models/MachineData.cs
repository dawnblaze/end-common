﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Endor.Models
{
    public partial class MachineData : IAtom<short>
    {
        public short BID { get; set; }
        
        public short ID { get; set; }
        
        public int ClassTypeID { get; set; }
        
        public DateTime ModifiedDT { get; set; }
        
        public bool IsActive { get; set; }
        
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        
        [StringLength(255)]
        public string InvoiceText { get; set; }
        [StringLength(255)]
        public string Description { get; set; }
        
        public bool HasImage { get; set; }
        
        [StringLength(512)]
        public string SKU { get; set; }
        
        public int? IncomeAccountID { get; set; }
        
        public int? ExpenseAccountID { get; set; }
        
        public string EstimatingCostPerHourFormula { get; set; }
        
        public string EstimatingPricePerHourFormula { get; set; }

        public short? TaxCodeID { get; set; }
        
        public TaxabilityCode TaxabilityCode { get; set; }

        public GLAccount IncomeAccount { get; set; }

        public GLAccount ExpenseAccount { get; set; }

        public ICollection<MachineCategoryLink> MachineCategoryLinks { get; set; }

        public ICollection<MachineCategory> MachineCategories { get; set; }
        
        public ICollection<SimpleMachineCategory> SimpleMachineCategories { get; set; }

        public short ActiveInstanceCount { get; set; }

        public short ActiveProfileCount { get; set; }
        
        public string WorksheetData { get; set; }

        public ICollection<MachineInstance> Instances { get; set; }

        public ICollection<MachineProfile> Profiles { get; set; }

        public int? MachineTemplateID { get; set; }

        public string AssemblyOVDataJSON { get; set; }

        public AssemblyData MachineTemplate { get; set; }

        [JsonIgnore]
        public ICollection<MachineCustomDataValue> CustomDataValues { get; set; }
    }
}
