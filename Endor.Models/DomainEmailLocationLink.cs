﻿using Newtonsoft.Json;

namespace Endor.Models
{
    public partial class DomainEmailLocationLink
    {
        /// <summary>
        /// The Business ID for this record.
        /// </summary>
        public short BID { get; set; }

        /// <summary>
        /// The ID of the DomainEmail Object being linked.
        /// </summary>
        public short DomainID { get; set; }

        /// <summary>
        /// The ID of the Location Object being linked.
        /// </summary>
        public byte LocationID { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DomainEmail DomainEmail { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public LocationData Location { get; set; }
    }
}
