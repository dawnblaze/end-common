﻿using System;
using Endor.Models.Autocomplete;
using Endor.Units;
using Newtonsoft.Json;

namespace Endor.Models
{
    [Autocomplete("MeasurementBase")]
    public class Measurement : IMeasurement, IComparable<IMeasurement>
    {
        public Measurement()
        {
            this.Value = null;
            this.Unit = Unit.None;
        }
        public Measurement(decimal? value, Unit unit)
        {
            this.Value = value;
            this.Unit = unit;
        }
        public decimal? Value { get; set; }

        [AutocompleteIgnore]
        public Unit Unit { get; set; }

        [AutocompleteIgnore]
        [JsonIgnore]
        public UnitType UnitType => this.UnitInfo().UnitType;

        [AutocompleteIgnore]
        [JsonIgnore]
        public decimal? ValueInUnits => Value;

        [AutocompleteIgnore]
        public void LogConversionError(string message)
        {
            // Do Nothing
        }

        [AutocompleteIgnore]
        public int CompareTo(IMeasurement other)
        {
            return MeasurementHelper.CompareMeasurements(this, other);
        }

        [JsonIgnore]
        public string UnitName => Unit.Name();
        [JsonIgnore]
        public string UnitTypeName => UnitType.Name();
        [JsonIgnore]
        public string UnitSystemName => this.UnitInfo().UnitSystem.ToString();
        [JsonIgnore]
        public string UnitClassificationName => this.UnitTypeInfo().Name;
        [JsonIgnore]
        public int UnitDimension => MeasurementHelper.UnitDimension(this);
    }
}
