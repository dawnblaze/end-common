﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Linq;
using Endor.DNSManagement.Utils;
using Newtonsoft.Json;
using System.Net;
using System.Net.Security;
using System.Security;
using System.Security.Cryptography;

namespace Endor.DNSManagement
{
    /// <summary>
    /// Endor DNS Manager
    /// </summary>
    public class DNSManager : IDNSManager
    {
        #region Private Members

        /// <summary>
        /// Tenant ID
        /// </summary>
        private readonly string _tenantID;

        /// <summary>
        /// Client ID
        /// </summary>
        private readonly string _clientID;

        /// <summary>
        /// Client Secret
        /// </summary>
        private readonly string _clientSecret;

        /// <summary>
        /// Subscription ID
        /// </summary>
        private readonly string _subscriptionID;

        /// <summary>
        /// AuthorizationToken
        /// </summary>
        private string _accessToken = null;

        /// <summary>
        /// Resource Group
        /// </summary>
        private readonly string _resourceGroup;

        /// <summary>
        /// Host Name DNS Target
        /// </summary>
        private readonly string _hostNameDNSTarget;

        /// <summary>
        /// The resource group location. i.e., "Central US"
        /// </summary>
        private readonly string _location;

        /// <summary>
        /// Web App Name
        /// </summary>
        private readonly string _appName;

        private const string _loginUrl = "https://login.microsoftonline.com/{tenantID}/oauth2/token";
        private const string _baseUrl = "https://management.azure.com/subscriptions/{subscriptionID}/resourceGroups/{resourceGroup}/providers/Microsoft.Web/sites/{appName}/";
        private const string _sslBaseUrl = "https://management.azure.com/subscriptions/{subscriptionID}/resourceGroups/{resourceGroup}/providers/Microsoft.Web/certificates/";
        private const string _hostNameBindings = "hostNameBindings";

        /// <summary>
        ///  SSL Certificate used in callback function
        /// </summary>
        private X509Certificate2 cert = null;

        #endregion

        private string ParseString(string s)
        {
            string result = s;
            result = result.Replace("{tenantID}", _tenantID, StringComparison.CurrentCultureIgnoreCase);
            result = result.Replace("{clientID}", _clientID);
            result = result.Replace("{clientSecret}", _clientSecret);
            result = result.Replace("{subscriptionID}", _subscriptionID);
            result = result.Replace("{resourceGroup}", _resourceGroup);
            result = result.Replace("{appName}", _appName);

            return result;
        }

        async private Task<string> SendRequest(string method, string URL, Object body = null)
        {
            async Task<string> DoSend()
            {
                var headers = new WebHeaderCollection()
                {
                    { "cache-control", "no-cache" },
                    { "Authorization", "Bearer " + this._accessToken }
                };

                return await HttpHelpers.SendRequest(method, URL, headers, "application/json", "application/json", body);
            }

            if (string.IsNullOrWhiteSpace(this._accessToken))
                this._accessToken = GetOauthToken();

            try
            {
                return await DoSend();
            }
            catch (Exception ex)
            {
                WebException webEx = (ex as WebException);

                if (webEx == null)
                    webEx = (ex.InnerException as WebException);

                if (webEx != null && ((HttpWebResponse)webEx.Response).StatusCode == HttpStatusCode.Unauthorized)
                {
                    this._accessToken = GetOauthToken();
                    return await DoSend();
                }

                throw;
            }
        }

        async private Task<string> GET(string URL)
        {
            return await SendRequest("GET", URL);
        }

        async private Task<string> PUT(string URL, Object body = null)
        {
            return await SendRequest("PUT", URL, body);
        }

        async private Task<string> DELETE(string URL)
        {
            return await SendRequest("DELETE", URL);
        }

        /// <summary>
        /// Endor DNS Manager
        /// </summary>
        /// <param name="tenantID">Tenant ID</param>
        /// <param name="clientID">Client ID</param>
        /// <param name="clientSecret">Client Secret</param>
        /// <param name="subscriptionID">Subscription ID</param>
        /// <param name="resourceGroup">Resource Group Name</param>
        /// <param name="appName">App Name</param>
        /// <param name="hostNameDNSTarget">Host Name DNS Target</param>
        /// <param name="location">Resource Location</param>
        public DNSManager(string tenantID, string clientID, string clientSecret, string subscriptionID, string resourceGroup, string appName, string hostNameDNSTarget, string location)
        {
            this._tenantID = tenantID;
            this._clientID = clientID;
            this._clientSecret = clientSecret;
            this._subscriptionID = subscriptionID;
            this._resourceGroup = resourceGroup;
            this._appName = appName;
            this._hostNameDNSTarget = hostNameDNSTarget;
            this._location = location;
        }

        /// <summary>
        /// Adds a Host Name to the service
        /// </summary>
        /// <param name="hostName"></param>
        /// <returns></returns>
        public async Task<Result> AddHostNameAsync(string hostName)
        {
            if (string.IsNullOrWhiteSpace(hostName))
                throw new ArgumentNullException("hostName");

            if (!await VerifyDNSAsync(hostName))
                return new Result() { Success = false, Message = $"{hostName} does not resolve to {_hostNameDNSTarget}." };

            string requestURL = ParseString(_baseUrl) + _hostNameBindings + "/" + hostName + "?api-version=2016-08-01";

            string result = await PUT(requestURL);

            HostNameBinding newBinding = JsonConvert.DeserializeObject<HostNameBinding>(result);

            return new Result() { Success = true, Message = $"Successfully added {newBinding.HostName}." };
        }

        /// <summary>
        /// Adds multiple Host Names to the service
        /// </summary>
        /// <param name="hostNames">List of Host Names to add</param>
        /// <returns></returns>
        public async Task<Dictionary<string, Result>> AddHostNamesAsync(List<string> hostNames)
        {
            var responses = new Dictionary<string, Result>();
            foreach(var hostName in hostNames)
            {
                try
                {
                    var resp = await AddHostNameAsync(hostName);
                    responses.Add(hostName, resp);
                }
                catch (Exception ex)
                {
                    responses.Add(hostName, new Result() { Success = false, Message = ex.Message });
                }
            }
            return responses;
        }

        /// <summary>
        /// Deletes Host Names from the service
        /// </summary>
        /// <param name="hostNames">List of Host Names to delete</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(List<string> hostNames)
        {
            var allHostNames = await this.GetHostNamesAsync();

            var validHostNames = allHostNames.Intersect(hostNames);

            foreach (var hostName in validHostNames)
            {
                string requestURL = ParseString(_baseUrl) + _hostNameBindings + "/" + hostName + "?api-version=2016-08-01";
                await DELETE(requestURL);
            }
            return true;
        }

        /// <summary>
        /// Deletes Host Name from the service
        /// </summary>
        /// <param name="hostName">Host Name to delete</param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(string hostName)
        {
            return await DeleteAsync(new List<string>() { hostName });
        }

        /// <summary>
        /// Deletes all Host Names from the Service
        /// </summary>
        /// <returns></returns>
        public async Task<bool> DeleteAllAsync()
        {
            var allHostNames = await this.GetHostNamesAsync();
            allHostNames.Remove(_hostNameDNSTarget);
            return await DeleteAsync(allHostNames);
        }

        /// <summary>
        /// Retreives all of the Host Names from the service
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetHostNamesAsync()
        {
            string requestURL = ParseString(_baseUrl) + _hostNameBindings + "?api-version=2016-08-01";
            string result = await GET(requestURL);

            HostNameBindingCollection bindings = JsonConvert.DeserializeObject<HostNameBindingCollection>(result);

            List<string> results = new List<string>();
            foreach (var binding in bindings.Bindings)
                results.Add(binding.HostName);
            return results;
        }

        /// <summary>
        /// Registers an SSL Certificate to the Host Name
        /// </summary>
        /// <param name="hostName">Host Name</param>
        /// <param name="sslCert">SSL Certificate as a byte array</param>
        /// <param name="sslPassword">SSL Certificate's password</param>
        /// <returns></returns>
        public async Task<Result> RegisterSSLAsync(string hostName, byte[] sslCert, string sslPassword, string certName = null)
        {
            if (String.IsNullOrWhiteSpace(hostName))
                return new Result()
                {
                    Success = false,
                    Message = "HostName is required."
                };

            if (sslCert == null || sslCert.Length == 0)
                return new Result()
                {
                    Success = false,
                    Message = "SSL Certificate is required."
                };

            if (String.IsNullOrWhiteSpace(sslPassword))
                return new Result()
                {
                    Success = false,
                    Message = "SSL Certificate Password is required."
                };

            const string incorrectPasswordError = "The specified network password is not correct";
            SecureString securePassword = new NetworkCredential("", sslPassword).SecurePassword;

            X509Certificate2 cert;
            try // Attempt to open certificate with password
            {
                cert = new X509Certificate2(sslCert, securePassword);
            }
            catch (CryptographicException e)
            {
                if (e.Message.Equals(incorrectPasswordError)) // Incorrect Password
                {
                    return new Result()
                    {
                        Success = false,
                        Message = "Incorrect SSL Password Supplied"
                    };
                }

                return new Result()
                {
                    Success = false,
                    Message = "Error validating certificate using the password supplied"
                };
            }

            var requestBody = new SSLCertificateRequest()
            {
                location = this._location,
                properties = new SSLCertificateRequestProperties()
                {
                    hostNames = new[] { hostName },
                    pfxBlob = sslCert,
                    password = sslPassword
                }
            };

            try
            {
                //var getTest = ParseString("https://management.azure.com/subscriptions/{subscriptionID}/resourceGroups/{resourceGroup}/providers/Microsoft.Web/certificates?api-version=2016-03-01");
                //var resp = await GET(getTest);

                string requestURL = $"{ParseString(_sslBaseUrl)}";
                if (string.IsNullOrWhiteSpace(certName))
                    requestURL += $"{cert.FriendlyName}?api-version=2018-02-01";
                else
                    requestURL += $"{certName}?api-version=2018-02-01";

                string resp = await PUT(requestURL, requestBody);

                var respJson = JsonConvert.DeserializeObject<SSLCertificateResponse>(resp);
                if (respJson == null)
                    return new Result()
                    {
                        Success = false,
                        Message = "Error deserializing Azure response."
                    };

                return new Result()
                {
                    Success = true,
                    Message = "Successfully added SSL Certificate to Azure."
                };
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
            {
                return new Result()
                {
                    Success = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Checks if the Host Names are valid
        /// </summary>
        /// <param name="hostNames">List of Host Names to validate</param>
        /// <returns></returns>
        public async Task<Dictionary<string, bool>> VerifyDNSAsync(List<string> hostNames)
        {
            var resp = new Dictionary<string, bool>();
            foreach (var hostName in hostNames)
                resp.Add(hostName, await VerifyDNSAsync(hostName));

            return resp;
        }

        /// <summary>
        /// Checks if the Host Name are valid
        /// </summary>
        /// <param name="hostName">Host Name to validate</param>
        /// <returns></returns>
        public async Task<bool> VerifyDNSAsync(string hostName)
        {
            List<string> aliases = DomainAliasProvider.GetAliases(hostName);
            bool result = aliases.Exists(s => s.ToLower() == _hostNameDNSTarget.ToLower());
            return await Task.FromResult(result);
        }

        /// <summary>
        /// Checks if the Host Names are not currently in use
        /// </summary>
        /// <param name="hostNames">List of Host Names to validate</param>
        /// <returns>True if available, false if already in use</returns>
        public async Task<Dictionary<string, bool>> CheckAvailabilityAsync(List<string> hostNames)
        {
            var resp = new Dictionary<string, bool>();
            var allHostNames = await this.GetHostNamesAsync();
            foreach (var hostName in hostNames)
            {
                var found = allHostNames.Where(hn => hn.ToLower() == hostName.ToLower()).FirstOrDefault();
                resp.Add(hostName, (found == null));
            }
            return resp;
        }

        /// <summary>
        /// Checks if the SSL Certificate associated with the Host Names are valid
        /// </summary>
        /// <param name="hostNames">ist of Host Names to validate</param>
        /// <returns></returns>
        public async Task<List<SSLValidationResponse>> VerifySSLAsync(List<string> hostNames)
        {
            var resp = new List<SSLValidationResponse>();
            foreach (var hostName in hostNames)
            {
                var url = hostName;
                if (url.IndexOf("http://") != -1)
                    url = url.Replace("http://", "https://");
                if (url.IndexOf("https://") == -1)
                    url = $"https://{url}";

                try
                {
                    HttpWebRequest request = WebRequest.CreateHttp(url);
                    request.ServerCertificateValidationCallback += ServerCertificateValidationCallback;
                    using (var webResp = await request.GetResponseAsync()) { }

                    resp.Add(new SSLValidationResponse() {
                        HostName = hostName,
                        Certificate = cert,
                        IsValid = (cert != null) ? cert.Verify() : false
                    });
                }
#pragma warning disable CS0168 // Variable is declared but never used
                catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
                {
                    resp.Add(new SSLValidationResponse()
                    {
                        HostName = hostName,
                        Certificate = null,
                        IsValid = false
                    });
                }
            }
            return resp;
        }

        /// <summary>
        /// Checks if the SSL Certificate is valid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        private bool ServerCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                cert = new X509Certificate2(certificate);
            else
                cert = null;

            return true;
        }

        private string GetOauthToken()
        {
            WebRequest request = HttpWebRequest.Create(ParseString(_loginUrl));
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers.Add("cache-control", "no-cache");

            NameValueCollection outgoingQueryString = HttpUtility.ParseQueryString(String.Empty);
            outgoingQueryString.Add("client_id", this._clientID);
            outgoingQueryString.Add("client_secret", this._clientSecret);
            outgoingQueryString.Add("resource", "https://management.core.windows.net/");
            outgoingQueryString.Add("grant_type", "client_credentials");

            byte[] postBytes = new ASCIIEncoding().GetBytes(outgoingQueryString.ToString());

            Stream postStream = request.GetRequestStream();
            postStream.Write(postBytes, 0, postBytes.Length);
            postStream.Flush();
            postStream.Close();

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader streamReader = new StreamReader(response.GetResponseStream()))
                {
                    dynamic jsonResponseText = streamReader.ReadToEnd();
                    dynamic stuff = JObject.Parse(jsonResponseText);
                    string token = stuff.access_token;
                    return token;
                }
            }
        }

        /// <summary>
        /// Get DNS Host Name target
        /// </summary>
        /// <returns></returns>
        public Task<string> DNSHostNameTargetAsync()
        {
            return Task.Run(() => this._hostNameDNSTarget);
        }
    }
}
