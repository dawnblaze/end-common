﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Endor.DNSManagement.Utils
{
    public static class HttpHelpers
    {
        /// <summary>
        /// POSTs data to the API
        /// </summary>
        /// <param name="URL">API Url</param>
        /// <param name="Headers">Collection of request headers</param>
        /// <param name="ContentType">Content Type. Defaults to "application/json"</param>
        /// <param name="AcceptType">Accept Type. Defaults to "application/json"</param>
        /// <param name="body">Content Object</param>
        /// <returns></returns>
        public static async Task<string> POSTData(string URL, Object body, WebHeaderCollection Headers, string ContentType = "application/json", string AcceptType = "application/json")
        {
            return await SendRequest("POST", URL, Headers, ContentType, AcceptType, body);
        }

        /// <summary>
        /// PUTs data to the API
        /// </summary>
        /// <param name="URL">API Url</param>
        /// <param name="Headers">Collection of request headers</param>
        /// <param name="ContentType">Content Type. Defaults to "application/json"</param>
        /// <param name="AcceptType">Accept Type. Defaults to "application/json"</param>
        /// <param name="body">Content Object</param>
        /// <returns></returns>
        public static async Task<string> PUTData(string URL, Object body, WebHeaderCollection Headers, string ContentType = "application/json", string AcceptType = "application/json")
        {
            return await SendRequest("PUT", URL, Headers, ContentType, AcceptType, body);
        }

        /// <summary>
        /// PATCHes data to the API
        /// </summary>
        /// <param name="URL">API Url</param>
        /// <param name="Headers">Collection of request headers</param>
        /// <param name="ContentType">Content Type. Defaults to "application/json"</param>
        /// <param name="AcceptType">Accept Type. Defaults to "application/json"</param>
        /// <param name="body">Content Object</param>
        /// <returns></returns>
        public static async Task<string> PATCHData(string URL, Object body, WebHeaderCollection Headers, string ContentType = "application/json", string AcceptType = "application/json")
        {
            return await SendRequest("PATCH", URL, Headers, ContentType, AcceptType, body);
        }

        /// <summary>
        /// GETs data from the API
        /// </summary>
        /// <param name="URL">API Url</param>
        /// <param name="Headers">Collection of request headers</param>
        /// <param name="ContentType">Content Type. Defaults to "application/json"</param>
        /// <param name="AcceptType">Accept Type. Defaults to "application/json"</param>
        /// <param name="body">Content Object</param>
        /// <returns></returns>
        public static async Task<string> GETData(string URL, WebHeaderCollection Headers, string ContentType = "application/json", string AcceptType = "application/json")
        {
            return await SendRequest("GET", URL, Headers, ContentType, AcceptType);
        }

        /// <summary>
        /// DELETEs data via the API
        /// </summary>
        /// <param name="URL">API Url</param>
        /// <param name="Headers">Collection of request headers</param>
        /// <param name="ContentType">Content Type. Defaults to "application/json"</param>
        /// <param name="AcceptType">Accept Type. Defaults to "application/json"</param>
        /// <param name="body">Content Object</param>
        /// <returns></returns>
        public static async Task<string> DELETEData(string URL, WebHeaderCollection Headers, string ContentType = "application/json", string AcceptType = "application/json")
        {
            return await SendRequest("DELETE", URL, Headers, ContentType, AcceptType, null);
        }

        /// <summary>
        /// Send's the API Web Request
        /// </summary>
        /// <param name="Method">POST, GET, PUT, or DELETE</param>
        /// <param name="URL">API Url</param>
        /// <param name="Headers">Collection of request headers</param>
        /// <param name="ContentType">Content Type. Defaults to "application/json"</param>
        /// <param name="AcceptType">Accept Type. Defaults to "application/json"</param>
        /// <param name="body">Content object</param>
        /// <returns></returns>
        public static async Task<string> SendRequest(string Method, string URL, WebHeaderCollection Headers, string ContentType = "application/json", string AcceptType = "application/json", Object body = null)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            request.Method = Method;
            request.ContentType = ContentType;
            request.Accept = AcceptType;
            request.Headers.Add(Headers);

            if (body != null)
            {
                StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), Encoding.UTF8);
                requestWriter.Write(JsonConvert.SerializeObject(body));
                requestWriter.Close();
            }

            try
            {
                HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = await responseReader.ReadToEndAsync();
                responseReader.Close();
                return response.ToString();
            }
            catch (WebException e)
            {
                Stream data = e.Response.GetResponseStream();
                using (var reader = new StreamReader(data))
                {
                    var errorMessage = await reader.ReadToEndAsync();
                    throw new Exception(errorMessage, e);
                }
            }
        }
    }
}
