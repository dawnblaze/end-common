﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace Endor.DNSManagement
{
    /// <summary>
    /// Pulls Dns Aliases
    /// </summary>
    public static class DomainAliasProvider
    {
        const int DNSPort = 53;
        const int UdpRetryAttempts = 2;
        static int _uniqueId;

        private class Pointer
        {
            // a pointer is a reference to the message and an index
            private readonly byte[] _message;
            //public int Length { get { return this._message.Length; } }
            public int Position { get; private set; }

            // pointers can only be created by passing on an existing message
            public Pointer(byte[] message, int position)
            {
                _message = message;
                Position = position;
            }

            /// <summary>
            /// Shallow copy function
            /// </summary>
            /// <returns></returns>
            public Pointer Copy()
            {
                return new Pointer(_message, Position);
            }

            /// <summary>
            /// Adjust the pointers position within the message
            /// </summary>
            /// <param name="position">new position in the message</param>
            public void SetPosition(int position)
            {
                Position = position;
            }

            /// <summary>
            /// Overloads the + operator to allow advancing the pointer by so many bytes
            /// </summary>
            /// <param name="pointer">the initial pointer</param>
            /// <param name="offset">the offset to add to the pointer in bytes</param>
            /// <returns>a reference to a new pointer moved forward by offset bytes</returns>
            public static Pointer operator +(Pointer pointer, int offset)
            {
                return new Pointer(pointer._message, pointer.Position + offset);
            }

            /// <summary>
            /// Reads a single byte at the current pointer, does not advance pointer
            /// </summary>
            /// <returns>the byte at the pointer</returns>
            public byte Peek()
            {
                return _message[Position];
            }

            /// <summary>
            /// Read X bytes from return message
            /// </summary>
            /// <param name="length"></param>
            /// <returns></returns>
            public byte[] ReadBytes(int length)
            {
                if (Position + length > _message.Length) throw new System.IO.EndOfStreamException();
                byte[] data = new byte[length];
                Array.Copy(_message, Position, data, 0, length);
                Position += length;
                return data;
            }

            /// <summary>
            /// Reads a single byte at the current pointer, advancing pointer
            /// </summary>
            /// <returns>the byte at the pointer</returns>
            public byte ReadByte()
            {
                return _message[Position++];
            }

            /// <summary>
            /// Reads two bytes to form a short at the current pointer, advancing pointer
            /// </summary>
            /// <returns>the byte at the pointer</returns>
            public short ReadShort()
            {
                return (short)(ReadByte() << 8 | ReadByte());
            }

            /// <summary>
            /// Reads four bytes to form a int at the current pointer, advancing pointer
            /// </summary>
            /// <returns>the byte at the pointer</returns>
            public int ReadInt()
            {
                return (ushort)ReadShort() << 16 | (ushort)ReadShort();
            }

            /// <summary>
            /// Reads a single byte as a char at the current pointer, advancing pointer
            /// </summary>
            /// <returns>the byte at the pointer</returns>
            public char ReadChar()
            {
                return (char)ReadByte();
            }

            /// <summary>
            /// Reads the full string from the response pointer
            /// </summary>
            /// <returns></returns>
            public string ReadString()
            {
                int length = ReadShort();
                return ReadString(length);
            }

            /// <summary>
            /// Reads a string from the pointer.
            /// </summary>
            /// <param name="length">Amount of chars to read</param>
            /// <returns></returns>
            public string ReadString(int length)
            {
                if (Position + length > _message.Length) throw new System.IO.EndOfStreamException();
                string ret = Encoding.ASCII.GetString(_message, (int)Position, length);
                Position += length;
                return ret;
            }

            /// <summary>
            /// Reads a domain name from the byte array. The method by which this works is described
            /// in RFC1035 - 4.1.4. Essentially to minimise the size of the message, if part of a domain
            /// name already been seen in the message, rather than repeating it, a pointer to the existing
            /// definition is used. Each word in a domain name is a label, and is preceded by its length
            /// 
            /// eg. bigdevelopments.co.uk
            /// 
            /// is [15] (size of bigdevelopments) + "bigdevelopments"
            ///    [2]  "co"
            ///    [2]  "uk"
            ///    [1]  0 (NULL)
            /// </summary>
            /// <returns>the byte at the pointer</returns>
            public string ReadDomain()
            {
                StringBuilder domain = new StringBuilder();
                int length = 0;

                // get  the length of the first label
                while ((length = ReadByte()) != 0)
                {
                    // top 2 bits set denotes domain name compression and to reference elsewhere
                    if ((length & 0xc0) == 0xc0)
                    {
                        // work out the existing domain name, copy this pointer
                        Pointer newPointer = Copy();

                        // and move it to where specified here
                        newPointer.SetPosition((length & 0x3f) << 8 | ReadByte());

                        // repeat call recursively
                        domain.Append(newPointer.ReadDomain());
                        return domain.ToString();
                    }

                    // if not using compression, copy a char at a time to the domain name
                    while (length > 0)
                    {
                        domain.Append(ReadChar());
                        length--;
                    }

                    // if size of next label isn't null (end of domain name) add a period ready for next label
                    if (Peek() != 0) domain.Append('.');
                }

                // and return
                return domain.ToString();
            }
        }

        private enum DnsType
        {
            A = 1,            // Host address
            NS = 2,            // Authoritative name server
            MD = 3,            // Mail destination (Obsolete - use MX)
            MF = 4,            // Mail forwarder (Obsolete - use MX)
            CNAME = 5,            // Canonical name for an alias
            SOA = 6,            // Start of a zone of authority
            MB = 7,            // Mailbox domain name (EXPERIMENTAL)
            MG = 8,            // Mail group member (EXPERIMENTAL)
            MR = 9,            // Mail rename domain (EXPERIMENTAL)
            NULL = 10,           // Null RR (EXPERIMENTAL)
            WKS = 11,           // Well known service
            PTR = 12,           // Domain name pointer
            HINFO = 13,           // Host information
            MINFO = 14,           // Mail box/list information
            MX = 15,           // Mail exchange
            TXT = 16,           // Text strings

            AAAA = 28,           // IPv6 host address                    [RFC3596]
            LOC = 29,           // Location info                        [RFC1876]
            SRV = 33,
            DNAME = 39,           // Non-Terminal DNS Name Redirection    [RFC2672]
            SSHFP = 44,         // SSH Fingerprint 						[RFC4255]
            IPSECKEY = 45           // IPSECKEY								[RFC4025]
        }

        private static byte[] BuildRequestMessage(string domainName)
        {
            // construct a message for this request. This will be a byte array but we're using
            // an arraylist as we don't know how big it will be
            ArrayList data = new ArrayList()
                {
                    (byte) 0,
                    (byte) 0,
                    (byte) (((byte) 0 << 3) | 0x01),
                    (byte) 0
                };

            void BuildDomain()
            {
                int position = 0;

                // start from the beginning and go to the end
                while (position < domainName.Length)
                {
                    // look for a period, after where we are
                    int length = domainName.IndexOf('.', position) - position;

                    // if there isn't one then this labels length is to the end of the string
                    if (length < 0) length = domainName.Length - position;

                    // add the length
                    data.Add((byte)length);

                    // copy a char at a time to the array
                    while (length-- > 0)
                    {
                        data.Add((byte)domainName[position++]);
                    }

                    // step over '.'
                    position++;
                }

                // end of domain names
                data.Add((byte)0);
            }

            // tell it how many questions
            unchecked
            {
                data.Add((byte)(0));
                data.Add((byte)1);
            }

            // the are no requests, name servers or additional records in a request
            data.Add((byte)0); data.Add((byte)0);
            data.Add((byte)0); data.Add((byte)0);
            data.Add((byte)0); data.Add((byte)0);

            // that's the header done - now add the questions
            BuildDomain();
            unchecked
            {
                data.Add((byte)0);
                data.Add((byte)15); // MX
                data.Add((byte)0);
                data.Add((byte)1); // Internet
            }

            // and convert that to an array
            byte[] message = new byte[data.Count];
            data.CopyTo(message);

            return message;
        }

        private static byte[] UdpTransfer(IPEndPoint server, byte[] requestMessage)
        {
            // UDP can fail - if it does try again keeping track of how many attempts we've made
            int attempts = 0;

            // try repeatedly in case of failure
            while (attempts <= UdpRetryAttempts)
            {
                // firstly, uniquely mark this request with an id
                unchecked
                {
                    // substitute in an id unique to this lookup, the request has no idea about this
                    requestMessage[0] = (byte)(_uniqueId >> 8);
                    requestMessage[1] = (byte)_uniqueId;
                }

                // we'll be send and receiving a UDP packet
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                // we will wait at most 1 second for a dns reply
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 1000);

                // send it off to the server
                socket.SendTo(requestMessage, requestMessage.Length, SocketFlags.None, server);

                // RFC1035 states that the maximum size of a UDP datagram is 512 octets (bytes)
                byte[] responseMessage = new byte[512];

                try
                {
                    // wait for a response upto 1 second
                    socket.Receive(responseMessage);

                    // make sure the message returned is ours
                    if (responseMessage[0] == requestMessage[0] && responseMessage[1] == requestMessage[1])
                    {
                        // trim nulls from the responseMessage

                        var i = responseMessage.Length - 1;
                        while (responseMessage[i] == 0)
                            --i;

                        var newResponseMessage = new byte[i + 1];
                        Array.Copy(responseMessage, newResponseMessage, i + 1);

                        // its a valid response - return it, this is our successful exit point
                        return newResponseMessage;
                    }
                }
                catch (SocketException)
                {
                    // failure - we better try again, but remember how many attempts
                    attempts++;
                }
                finally
                {
                    // increase the unique id
                    _uniqueId++;

                    // close the socket
                    socket.Close();
                }
            }

            // the operation has failed, this is our unsuccessful exit point
            throw new Exception("No Response");
        }

        private static List<string> ReadResponse(byte[] responseMessage)
        {
            short GetShort(byte[] message, int position)
            {
                return (short)(message[position] << 8 | message[position + 1]);
            }

            // the bit flags are in bytes 2 and 3
            byte flags1 = responseMessage[2];
            byte flags2 = responseMessage[3];

            // create the arrays of response objects
            short questionCount = GetShort(responseMessage, 4);
            short answerCount = GetShort(responseMessage, 6);

            // need a pointer to do this, position just after the header
            Pointer pointer = new Pointer(responseMessage, 12);

            // Advance the pointer passed the questions
            for (int index = 0; index < questionCount; index++)
            {
                try
                {
                    pointer.ReadDomain();
                    pointer.ReadShort();
                    pointer.ReadShort();
                }
                catch (Exception ex)
                {
                    // something grim has happened, we can't continue
                    throw new Exception("Invalid Response", ex);
                }
            }

            List<string> result = new List<string>();
            DnsType dnsType;
            int recordLength;
            for (int index = 0; index < answerCount; index++)
            {
                pointer.ReadDomain();
                dnsType = (DnsType)pointer.ReadShort();
                pointer.ReadShort();
                pointer.ReadInt();

                recordLength = pointer.ReadShort();

                // and create the appropriate RDATA record based on the dnsType
                switch (dnsType)
                {
                    case DnsType.A:
                        {
                            pointer.ReadBytes(4);
                            break;
                        }
                    case DnsType.DNAME:
                    case DnsType.MF:
                    case DnsType.MD:
                    case DnsType.NS:
                    case DnsType.MB:
                    case DnsType.MG:
                    case DnsType.MR:
                    case DnsType.PTR:
                    case DnsType.CNAME:
                        {
                            result.Add(pointer.ReadDomain());
                            break;
                        }
                    case DnsType.SOA:
                        {
                            // read all fields RFC1035 3.3.13
                            result.Add(pointer.ReadDomain());
                            pointer.ReadDomain();
                            pointer.ReadInt();
                            pointer.ReadInt();
                            pointer.ReadInt();
                            pointer.ReadInt();
                            pointer.ReadInt();
                            break;
                        }
                    case DnsType.NULL:
                        {
                            pointer.ReadBytes(recordLength);
                            break;
                        }
                    case DnsType.WKS:
                        {
                            pointer.ReadBytes(4);
                            pointer.ReadByte();
                            pointer.ReadBytes(recordLength - 5);
                            break;
                        }
                    case DnsType.HINFO:
                    case DnsType.MINFO:
                        {
                            pointer.ReadString(recordLength);
                            break;
                        }
                    case DnsType.MX:
                        {
                            pointer.ReadShort();
                            result.Add(pointer.ReadDomain());
                            break;
                        }
                    case DnsType.TXT:
                        {
                            pointer.ReadByte();
                            break;
                        }
                    case DnsType.AAAA:
                        {
                            pointer.ReadBytes(16);
                            break;
                        }
                    case DnsType.LOC:
                        {
                            pointer.ReadByte();
                            pointer.ReadByte();
                            pointer.ReadByte();
                            pointer.ReadByte();

                            pointer.ReadInt();
                            pointer.ReadInt();
                            pointer.ReadInt();

                            break;
                        }
                    case DnsType.SRV:
                        {
                            pointer.ReadShort();
                            pointer.ReadShort();
                            pointer.ReadShort();
                            pointer.ReadDomain();

                            break;
                        }
                    case DnsType.SSHFP:
                        {
                            pointer.ReadByte();
                            pointer.ReadByte();
                            pointer.ReadString(recordLength - 2);
                            break;
                        }
                    case DnsType.IPSECKEY:
                        {
                            pointer.ReadByte();

                            int gatewayType = pointer.ReadByte();

                            pointer.ReadByte();

                            switch (gatewayType)
                            {
                                case 0:
                                    break;
                                case 1:
                                    pointer.ReadBytes(4);
                                    break;
                                case 2:
                                    pointer.ReadBytes(16);
                                    break;
                                case 3:
                                    pointer.ReadDomain();
                                    break;
                            }

                            pointer.ReadString();
                            break;
                        }

                    default:
                        {
                            // move the pointer over this unrecognised record
                            pointer += recordLength;
                            break;
                        }
                }
            }

            return result;
        }

        private static List<IPAddress> SystemDnsServers()
        {
            // ditching the old method of reading in the registry for using the .net way
            //    old: http://technet.microsoft.com/en-us/library/cc962470.aspx
            //    new: http://msdn.microsoft.com/en-us/library/system.net.networkinformation.ipinterfaceproperties.dnsaddresses.aspx

            List<IPAddress> servers = new List<IPAddress>();
            NetworkInterface[] intfs = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface intf in intfs)
            {
                if (intf.OperationalStatus == OperationalStatus.Up)
                {
                    IPInterfaceProperties intProps = intf.GetIPProperties();
                    foreach (IPAddress ip in intProps.DnsAddresses)
                    {
                        if (!servers.Contains(ip))
                            servers.Add(ip);
                    }
                }
            }

            return servers;
        }

        /// <summary>
        /// Method to generate a list of Dns Aliases for a given Domain
        /// </summary>
        /// <param name="domainName"></param>
        /// <returns></returns>
        public static List<string> GetAliases(string domainName)
        {
            List<IPAddress> dnsServers = SystemDnsServers();

            List<string> result = new List<string>();

            foreach (var dnsServer in dnsServers)
            {
                if (!dnsServer.IsIPv6LinkLocal && !dnsServer.IsIPv6Multicast && !dnsServer.IsIPv6SiteLocal && !dnsServer.IsIPv6Teredo)
                {
                    try
                    {
                        IPEndPoint server = new IPEndPoint(dnsServer, DNSPort);

                        byte[] requestMessage = BuildRequestMessage(domainName);
                        byte[] responseMessage = UdpTransfer(server, requestMessage);

                        List<string> resp = ReadResponse(responseMessage);

                        foreach (string s in resp)
                        {
                            if (!result.Contains(s))
                                result.Add(s);
                        }
                    }
                    catch
                    {

                    }
                }
            }

            return result;
        }
    }
}
