﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Endor.DNSManagement
{
    /// <summary>
    /// DNS Manager Interface
    /// </summary>
    public interface IDNSManager
    {
        /// <summary>
        /// Get DNS Host Name target
        /// </summary>
        /// <returns></returns>
        Task<string> DNSHostNameTargetAsync();

        /// <summary>
        /// Adds a Host Name to the service
        /// </summary>
        /// <param name="hostName"></param>
        /// <returns></returns>
        Task<Result> AddHostNameAsync(string hostName);

        /// <summary>
        /// Adds multiple Host Names to the service
        /// </summary>
        /// <param name="hostNames">List of Host Names to add</param>
        /// <returns></returns>
        Task<Dictionary<string, Result>> AddHostNamesAsync(List<string> hostNames);

        /// <summary>
        /// Retreives all of the Host Names from the service
        /// </summary>
        /// <returns></returns>
        Task<List<string>> GetHostNamesAsync();

        /// <summary>
        /// Checks if the Host Names are valid
        /// </summary>
        /// <param name="hostNames">List of Host Names to validate</param>
        /// <returns></returns>
        Task<Dictionary<string, bool>> VerifyDNSAsync(List<string> hostNames);

        /// <summary>
        /// Checks if the Host Name is valid
        /// </summary>
        /// <param name="hostName">Host Name to validate</param>
        /// <returns></returns>
        Task<bool> VerifyDNSAsync(string hostName);

        /// <summary>
        /// Deletes all Host Names from the Service
        /// </summary>
        /// <returns></returns>
        Task<bool> DeleteAllAsync();

        /// <summary>
        /// Deletes Host Names from the service
        /// </summary>
        /// <param name="hostNames">List of Host Names to delete</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(List<string> hostNames);

        /// <summary>
        /// Deletes Host Name from the service
        /// </summary>
        /// <param name="hostName">Host Name to delete</param>
        /// <returns></returns>
        Task<bool> DeleteAsync(string hostName);

        /// <summary>
        /// Checks if the Host Names are not currently in use
        /// </summary>
        /// <param name="hostNames">List of Host Names to validate</param>
        /// <returns>True if available, false if already in use</returns>
        Task<Dictionary<string, bool>> CheckAvailabilityAsync(List<string> hostNames);

        /// <summary>
        /// Checks if the SSL Certificate associated with the Host Names are valid
        /// </summary>
        /// <param name="hostNames">ist of Host Names to validate</param>
        /// <returns></returns>
        Task<List<SSLValidationResponse>> VerifySSLAsync(List<string> hostNames);

        /// <summary>
        /// Registers an SSL Certificate to the Host Name
        /// </summary>
        /// <param name="hostName">Host Name</param>
        /// <param name="sslCert">SSL Certificate as a byte array</param>
        /// <param name="sslPassword">SSL Certificate's password</param>
        /// <returns></returns>
        Task<Result> RegisterSSLAsync(string hostName, byte[] sslCert, string sslPassword, string certName = null);
    }
}
