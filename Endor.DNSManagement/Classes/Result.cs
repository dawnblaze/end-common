﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.DNSManagement
{
    public class Result
    {
        /// <summary>
        /// If the Result was successful or not
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Result's message
        /// </summary>
        public string Message { get; set; }
    }
}
