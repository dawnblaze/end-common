﻿using System.Security.Cryptography.X509Certificates;

namespace Endor.DNSManagement
{
    public class SSLValidationResponse
    {
        public string HostName { get; set; }
        public bool IsValid { get; set; }
        public X509Certificate2 Certificate { get; set; }
    }
}
