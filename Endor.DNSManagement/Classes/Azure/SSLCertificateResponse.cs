﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.DNSManagement
{
    public class SSLCertificateResponse
    {
        public string id { get; set; }
        /// <summary>
        /// Kind of resource
        /// </summary>
        public string kind { get; set; }
        /// <summary>
        /// Location of Resource
        /// </summary>
        public string location { get; set; }
        /// <summary>
        /// Resource Name.
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Response Properties Object
        /// </summary>
        public SSLCertificateResponseProperties properties { get; set; }
        /// <summary>
        /// Resource tags
        /// </summary>
        public object tags { get; set; }
        /// <summary>
        /// Resource type.
        /// </summary>
        public string type { get; set; }
    }

    public class SSLCertificateResponseProperties
    {
        /// <summary>
        /// Raw bytes of .cer file
        /// </summary>
        public string cerBlob { get; set; }
        /// <summary>
        /// Certificate expriration date.
        /// </summary>
        public string expirationDate { get; set; }
        /// <summary>
        /// Friendly name of the certificate.
        /// </summary>
        public string friendlyName { get; set; }
        /// <summary>
        /// Region of the certificate.
        /// </summary>
        public string geoRegion { get; set; }
        /// <summary>
        /// Host names the certificate applies to.
        /// </summary>
        public string[] hostNames { get; set; }
        /// <summary>
        /// Specification for the App Service Environment to use for the certificate.
        /// </summary>
        public object hostingEnvironmentProfile { get; set; }
        /// <summary>
        /// Certificate issue Date.
        /// </summary>
        public string issueDate { get; set; }
        /// <summary>
        /// Certificate issuer.
        /// </summary>
        public string issuer { get; set; }
        /// <summary>
        /// Key Vault Csm resource Id.
        /// </summary>
        public string keyVaultId { get; set; }
        /// <summary>
        /// Key Vault secret name.
        /// </summary>
        public string keyVaultSecretName { get; set; }
        /// <summary>
        /// Status of the Key Vault secret.
        /// </summary>
        public object keyVaultSecretStatus { get; set; }
        /// <summary>
        /// Certificate password.
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// Pfx blob.
        /// </summary>
        public string pfxBlob { get; set; }
        /// <summary>
        /// Public key hash.
        /// </summary>
        public string publicKeyHash { get; set; }
        /// <summary>
        /// Self link.
        /// </summary>
        public string selfLink { get; set; }
        /// <summary>
        /// Resource ID of the associated App Service plan, formatted as: "/subscriptions/{subscriptionID}/resourceGroups/{groupName}/providers/Microsoft.Web/serverfarms/{appServicePlanName}".
        /// </summary>
        public string serverFarmId { get; set; }
        /// <summary>
        /// App name.
        /// </summary>
        public string siteName { get; set; }
        /// <summary>
        /// Subject name of the certificate.
        /// </summary>
        public string subjectName { get; set; }
        /// <summary>
        /// Certificate thumbprint.
        /// </summary>
        public string thumbprint { get; set; }
        /// <summary>
        /// Is the certificate valid?.
        /// </summary>
        public bool? valid { get; set; }
    }
}
