﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.DNSManagement
{
    public class SSLCertificateRequest
    {
        /// <summary>
        /// Kind of resource
        /// </summary>
        public string kind { get; set; }
        /// <summary>
        /// Location of Resource
        /// </summary>
        public string location { get; set; }
        /// <summary>
        /// Request Properties Object
        /// </summary>
        public SSLCertificateRequestProperties properties { get; set; }
        /// <summary>
        /// Resource tags
        /// </summary>
        public object tags { get; set; }
    }

    public class SSLCertificateRequestProperties
    {
        /// <summary>
        /// Host names the certificate applies to.
        /// </summary>
        public string[] hostNames { get; set; }
        /// <summary>
        /// Key Vault Csm resource Id.
        /// </summary>
        public string keyVaultId { get; set; }
        /// <summary>
        /// Key Vault secret name.
        /// </summary>
        public string keyVaultSecretName { get; set; }
        /// <summary>
        /// Certificate password.
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// Pfx blob.
        /// </summary>
        public byte[] pfxBlob { get; set; }
        /// <summary>
        /// Resource ID of the associated App Service plan, formatted as: "/subscriptions/{subscriptionID}/resourceGroups/{groupName}/providers/Microsoft.Web/serverfarms/{appServicePlanName}".
        /// </summary>
        public string serverFarmId { get; set; }
    }
}
