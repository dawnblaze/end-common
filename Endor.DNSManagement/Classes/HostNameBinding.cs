﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace Endor.DNSManagement
{
    public enum AzureResourceType
    {
        TrafficManager,
        Website
    }

    public enum CustomHostNameDnsRecordType
    {
        A,
        CNAME
    }

    public enum HostNameType
    {
        Managed,
        Verified
    }

    public enum SslState
    {
        Disabled,
        IpBasedEnabled,
        SniEnabled
    }

    public class HostNameBinding
    {
        [JsonProperty("id", NullValueHandling =NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("kind", NullValueHandling = NullValueHandling.Ignore)]
        public string Kind { get; set; }

        [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("hostname", NullValueHandling = NullValueHandling.Ignore)]
        public string HostName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Properties?.SiteName))
                    return Name;

                if (Name.ToLower().StartsWith(Properties.SiteName.ToLower()))
                    return Name.Replace(Properties.SiteName+"/", "");

                return Name;
            }
        }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("properties", NullValueHandling = NullValueHandling.Ignore)]
        public HostNameBindingProperties Properties { get; set; }
    }

    public class HostNameBindingProperties
    {
        [JsonProperty("azureResourceName", NullValueHandling = NullValueHandling.Ignore)]
        public string AzureResourceName { get; set; }

        [JsonProperty("azureResourceType", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public AzureResourceType AzureResourceType { get; set; }

        [JsonProperty("customHostNameDnsRecordType", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public CustomHostNameDnsRecordType CustomHostNameDnsRecordType { get; set; }

        [JsonProperty("domainId", NullValueHandling = NullValueHandling.Ignore)]
        public string DomainId { get; set; }

        [JsonProperty("hostNameType", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public HostNameType HostNameType { get; set; }

        [JsonProperty("siteName", NullValueHandling = NullValueHandling.Ignore)]
        public string SiteName { get; set; }

        [JsonProperty("sslState", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(StringEnumConverter))]
        public SslState SslState { get; set; }

        [JsonProperty("thumbprint", NullValueHandling = NullValueHandling.Ignore)]
        public string Thumbprint { get; set; }

        [JsonProperty("virtualIP", NullValueHandling = NullValueHandling.Ignore)]
        public string VirtualIP { get; set; }
    }

    public class HostNameBindingCollection
    {
        [JsonProperty("nextLink", NullValueHandling = NullValueHandling.Ignore)]
        public string NextLink { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public List<HostNameBinding> Bindings { get; set; }

    }
}
