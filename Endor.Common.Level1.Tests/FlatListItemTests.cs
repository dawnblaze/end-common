﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class FlatListItemTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        [TestMethod]
        public void FlatListItemSerializationTest()
        {
            FlatListItem fli = new FlatListItem()
            {
                BID = 1,
                ClassTypeID = 1000,
                FlatListType = FlatListType.EstimateCancelledReasons,
                ID = 1,
                IsActive = true,
                IsAdHoc = false,
                IsSystem = true,
                ModifiedDT = DateTime.UtcNow,
                Name = "FlatListItemSerializationTest" + DateTime.UtcNow.ToString("U"),
                SortIndex = 1,
            };

            string serializedFli = JsonConvert.SerializeObject(fli);
            JToken deserializedTokens = JToken.Parse(serializedFli);

            Assert.AreEqual(8, deserializedTokens.Values().Count(), deserializedTokens.ToString());
            Assert.IsNull(deserializedTokens["BID"]);
            Assert.IsNull(deserializedTokens["IsAdHoc"]);

            Assert.IsNotNull(deserializedTokens["ID"]);
            Assert.IsNotNull(deserializedTokens["ClassTypeID"]);
            Assert.IsNotNull(deserializedTokens["ModifiedDT"]);
            Assert.IsNotNull(deserializedTokens["FlatListType"]);
            Assert.IsNotNull(deserializedTokens["IsActive"]);
            Assert.IsNotNull(deserializedTokens["IsSystem"]);
            Assert.IsNotNull(deserializedTokens["Name"]);
            Assert.IsNotNull(deserializedTokens["SortIndex"]);
        }
    }
}
