﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class MessageDeliveryRecordTest
    {

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore };
        }

        private static MessageDeliveryRecord TestMessageHeaderRecord()
        {
            MessageDeliveryRecord msgDeliveryRecord = new MessageDeliveryRecord
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageDeliveryRecord,
                ParticipantID = 500,
                AttemptNumber = 500,
                IsPending = false,
                ScheduledDT = DateTime.Now,
                AttemptedDT = DateTime.Now,
                WasSuccessful = false,
                FailureMessage = "FailureMessage",
                MetaData = "Metadata"
            };


            return msgDeliveryRecord;
        }

        [TestMethod]
        public void MessageDeliveryRecordSerializationTest()
        {
            MessageDeliveryRecord msgDeliveryRecord = TestMessageHeaderRecord();

            string msgDeliveryRecordSerialized = JsonConvert.SerializeObject(msgDeliveryRecord);
            JToken msgDeliverRecordToken = JToken.Parse(msgDeliveryRecordSerialized);

            //Token Count
            Assert.AreEqual(11, msgDeliverRecordToken.Values().Count(), msgDeliverRecordToken.ToString());

            //Serialized
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.ID)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.ClassTypeID)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.ModifiedDT)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.AttemptNumber)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.IsPending)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.ParticipantID)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.ScheduledDT)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.AttemptedDT)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.WasSuccessful)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.FailureMessage)]);
            Assert.IsNotNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.MetaData)]);


            //Not serialized
            Assert.IsNull(msgDeliverRecordToken[nameof(msgDeliveryRecord.BID)]);
        }

    }
}
