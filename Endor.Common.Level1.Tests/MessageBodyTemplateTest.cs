﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class MessageBodyTemplateTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void BasicMessageBodyTemplateSerializationTest()
        {
            MessageBodyTemplate messageBodyTemplate = new MessageBodyTemplate();
            messageBodyTemplate.BID = 1;
            messageBodyTemplate.ParticipantInfoJSON = "xxx";
            messageBodyTemplate.Participants = new List<MessageParticipantInfo>()
            {
                new MessageParticipantInfo() {  
                     ID = 8,
                     ContactID = 2,
                     EmployeeID = 2,
                     TeamID = 2,
                     UserName = "test1"
                },
                new MessageParticipantInfo() {
                     ContactID = 2,
                     EmployeeID = 2,
                     TeamID = 2,
                     UserName = "test2",
                     IsMergeField = true
                },
                new MessageParticipantInfo() {
                     ID = 0,
                     ContactID = 2,
                     EmployeeID = 2,
                     TeamID = 2,
                     UserName = "test3"
                },
                new MessageParticipantInfo() {
                     ContactID = 2,
                     EmployeeID = 2,
                     TeamID = 2,
                     UserName = "test4",
                     IsMergeField = false

                }
            };

            var serializedMessageBodyTemplate = JsonConvert.SerializeObject(messageBodyTemplate);
            JToken jToken = JToken.Parse(serializedMessageBodyTemplate);
            Assert.IsNull(jToken["ParticipantInfoJSON"]);
            Assert.IsNotNull(jToken["Participants"]);
            Assert.AreEqual(jToken["Participants"][0]["ID"],8);
            Assert.AreEqual(jToken["Participants"][1]["IsMergeField"], true);
            Assert.IsNull(jToken["Participants"][2]["ID"]);
            Assert.IsNull(jToken["Participants"][3]["IsMergeField"]);
        }
    }
}
