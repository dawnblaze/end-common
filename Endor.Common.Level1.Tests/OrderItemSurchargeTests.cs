﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class OrderItemSurchargeTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        private static OrderItemSurcharge TestOrderItemSurcharge()
        {
            OrderItemSurcharge orderItemSurcharge = new OrderItemSurcharge()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.OrderItemSurcharge, //10015
                ModifiedDT = DateTime.Now,
                OrderItemID = 1,
                Number = 123,
                Name = "Name",
                PricePreTax = 1.0m
            };

            return orderItemSurcharge;
        }

        [TestMethod]
        public void OrderItemSurchargeSerializationTest()
        {
            OrderItemSurcharge OrderItemSurcharge = TestOrderItemSurcharge();

            string OrderItemSurchargeSerialized = JsonConvert.SerializeObject(OrderItemSurcharge);
            JToken OrderItemSurchargeJToken = JToken.Parse(OrderItemSurchargeSerialized);

            //Token Count
            Assert.AreEqual(11, OrderItemSurchargeJToken.Values().Count(), OrderItemSurchargeJToken.ToString());

            //Serialized
            Assert.IsNotNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.ID)]);
            Assert.IsNotNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.ClassTypeID)]);
            Assert.IsNotNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.ModifiedDT)]);
            Assert.IsNotNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.OrderItemID)]);
            Assert.IsNotNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.Number)]);
            Assert.IsNotNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.Name)]);
            Assert.IsNotNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.PricePreTax)]);
            Assert.IsNotNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.PriceIsOV)]);


            //Not serialized
            Assert.IsNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.BID)]);
            Assert.IsNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.OrderItem)]);
            Assert.IsNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.IncomeAccount)]);
            Assert.IsNull(OrderItemSurchargeJToken[nameof(OrderItemSurcharge.TaxCode)]);
        }
    }
}