﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;


namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class DestinationProfileTableTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void BasicDestinationProfileTableSerializationTest()
        {
            DestinationProfileTable destinationProfileTable = new DestinationProfileTable();


            var serializedDestinationProfileTable = JsonConvert.SerializeObject(destinationProfileTable);
            JToken jToken = JToken.Parse(serializedDestinationProfileTable);

            Assert.IsNull(jToken["BID"]);
            Assert.AreEqual(default(short), jToken["ID"]);
            Assert.AreEqual(default(int), jToken["ClassTypeID"]);
            Assert.AreEqual(default(DateTime), jToken["ModifiedDT"]);
            Assert.AreEqual(default(int), jToken["ProfileID"]);
            Assert.AreEqual(default(short), jToken["TableID"]);
            Assert.IsNull(jToken["Label"]);
            Assert.AreEqual(default(bool), jToken["OverrideDefault"]);
            Assert.IsNull(jToken["RowCount"]);
            Assert.IsNull(jToken["RowValuesJSON"]);
            Assert.IsNull(jToken["ColumnCount"]);
            Assert.IsNull(jToken["ColumnValuesJSON"]);
            Assert.IsNull(jToken["CellDataJSON"]);
        }
    }
}
