﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class OrderContactRoleTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        [TestMethod]
        public void OrderContactRoleSerializationTest()
        {
            OrderContactRole orderContactRole = new OrderContactRole()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.Order.ID(),
                ModifiedDT = DateTime.UtcNow,
                OrderID = 1,
                ContactID = 1,
                IsAdHoc = false,
                IsDestinationRole = false,
                IsOrderItemRole = false,
                IsOrderRole = true,
                RoleType = OrderContactRoleType.Primary,
            };

            string serializedOrderContactRole = JsonConvert.SerializeObject(orderContactRole);
            JToken orderContactRoleToken = JToken.Parse(serializedOrderContactRole);

            Assert.AreEqual(8, orderContactRoleToken.Values().Count(), orderContactRoleToken.ToString());
            Assert.IsNull(orderContactRoleToken["BID"]);
            Assert.IsNotNull(orderContactRoleToken["ID"]);
            Assert.IsNotNull(orderContactRoleToken["ClassTypeID"]);
            Assert.IsNotNull(orderContactRoleToken["ModifiedDT"]);
            Assert.IsNotNull(orderContactRoleToken["OrderID"]);
            Assert.IsNull(orderContactRoleToken["OrderItemID"]);
            Assert.IsNull(orderContactRoleToken["DestinationID"]);
            Assert.IsNotNull(orderContactRoleToken["RoleType"]);
            Assert.IsNotNull(orderContactRoleToken["IsAdHoc"]);
            Assert.IsNull(orderContactRoleToken["ContactName"]);
            Assert.IsNull(orderContactRoleToken["ContactCompany"]);
            Assert.IsNull(orderContactRoleToken["IsOrderRole"]);
            Assert.IsNull(orderContactRoleToken["IsOrderItemRole"]);
            Assert.IsNull(orderContactRoleToken["IsDestinationRole"]);
            Assert.IsNull(orderContactRoleToken["Contact"]);
            Assert.IsNotNull(orderContactRoleToken["OrderContactRoleLocators"]);

            orderContactRole.ContactCompany = "Jones Inc.";
            orderContactRole.ContactName = "Bob Jones";
            orderContactRole.Contact = new ContactData() { LongName = "Bob Jones" };

            serializedOrderContactRole = JsonConvert.SerializeObject(orderContactRole);
            orderContactRoleToken = JToken.Parse(serializedOrderContactRole);

            Assert.AreEqual(11, orderContactRoleToken.Values().Count(), orderContactRoleToken.ToString());
            Assert.IsNull(orderContactRoleToken["BID"]);
            Assert.IsNotNull(orderContactRoleToken["ID"]);
            Assert.IsNotNull(orderContactRoleToken["ClassTypeID"]);
            Assert.IsNotNull(orderContactRoleToken["ModifiedDT"]);
            Assert.IsNotNull(orderContactRoleToken["OrderID"]);
            Assert.IsNull(orderContactRoleToken["OrderItemID"]);
            Assert.IsNull(orderContactRoleToken["DestinationID"]);
            Assert.IsNotNull(orderContactRoleToken["RoleType"]);
            Assert.IsNotNull(orderContactRoleToken["IsAdHoc"]);
            Assert.IsNotNull(orderContactRoleToken["ContactName"]);
            Assert.IsNotNull(orderContactRoleToken["ContactCompany"]);
            Assert.IsNull(orderContactRoleToken["IsOrderRole"]);
            Assert.IsNull(orderContactRoleToken["IsOrderItemRole"]);
            Assert.IsNull(orderContactRoleToken["IsDestinationRole"]);
            Assert.IsNotNull(orderContactRoleToken["Contact"]);
            Assert.IsNotNull(orderContactRoleToken["OrderContactRoleLocators"]);
        }
    }
}
