﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class MachineTests
    {

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void BasicMachineDataSerializationTest()
        {
            MachineData machineData = new MachineData();
            machineData.MachineTemplate = new AssemblyData();

            var serializedMachineData = JsonConvert.SerializeObject(machineData);
            JToken jToken = JToken.Parse(serializedMachineData);
            
            Assert.AreEqual(jToken["ActiveInstanceCount"], default(short));
            Assert.AreEqual(jToken["ActiveProfileCount"], default(short));
            Assert.IsNull(jToken["MachineTemplateID"]);
            Assert.IsNull(jToken["WorksheetData"]);
            Assert.IsNull(jToken["Instances"]);
            Assert.IsNull(jToken["Profiles"]);
            Assert.IsNotNull(jToken["MachineTemplate"]);
        }

        [TestMethod]
        public void BasicMachineProfileSerializationTest()
        {
            MachineProfile machineProfile = new MachineProfile();

            machineProfile.BID = 1;
            machineProfile.Machine = new MachineData();
            machineProfile.MachineTemplate = new AssemblyData();

            var serializedMachineProfile = JsonConvert.SerializeObject(machineProfile);
            JToken jToken = JToken.Parse(serializedMachineProfile);

            Assert.IsNull(jToken["BID"]);
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken["IsActive"], default(bool));
            Assert.AreEqual(jToken["IsDefault"], default(bool));
            Assert.IsNull(jToken["Name"]);
            Assert.AreEqual(jToken["MachineID"], default(int));
            Assert.AreEqual(jToken["MachineTemplateID"], default(int));
            Assert.IsNull(jToken["AssemblyDataJSON"]);
            Assert.IsNull(jToken["EstimatingCostPerHourFormula"]);
            Assert.IsNull(jToken["EstimatingPricePerHourFormula"]);
            Assert.IsNull(jToken["Machine"]);
            Assert.IsNull(jToken["MachineTemplate"]);
        }


        [TestMethod]
        public void BasicMachineInstanceSerializationTest()
        {
            MachineInstance machineInstance = new MachineInstance();

            var serializedMachineInstance = JsonConvert.SerializeObject(machineInstance);
            JToken jToken = JToken.Parse(serializedMachineInstance);

            Assert.IsNull(jToken["BID"]);
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken["IsActive"], default(bool));
            Assert.IsNull(jToken["Name"]);
            Assert.AreEqual(jToken["MachineID"], default(int));
            Assert.IsNull(jToken["Manufacturer"]);
            Assert.IsNull(jToken["Model"]);
            Assert.IsNull(jToken["SerialNumber"]);
            Assert.IsNull(jToken["IPAddress"]);
            Assert.IsNull(jToken["PurchaseDate"]);
            Assert.AreEqual(jToken["HasServiceContract"], default(bool));
            Assert.IsNull(jToken["ContractNumber"]);
            Assert.IsNull(jToken["ContractStartDate"]);
            Assert.IsNull(jToken["ContractEndDate"]);
            Assert.IsNull(jToken["ContractInfo"]);
            Assert.IsNull(jToken["LocationID"]);
            Assert.IsNull(jToken["Machine"]);

            MachineInstance machineInstance2 = new MachineInstance();

            machineInstance2.BID = 1;
            machineInstance2.Name = "Hello World";

            var serializedMachineInstance2 = JsonConvert.SerializeObject(machineInstance2);
            JToken jToken2 = JToken.Parse(serializedMachineInstance2);

            Assert.IsNull(jToken2["BID"]);
            Assert.AreEqual(jToken2["ID"], default(int));
            Assert.AreEqual(jToken2["ClassTypeID"], default(int));
            Assert.AreEqual(jToken2["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken2["IsActive"], default(bool));
            Assert.AreEqual(jToken2["Name"], "Hello World");
            Assert.AreEqual(jToken2["MachineID"], default(int));
            Assert.IsNull(jToken2["Manufacturer"]);
            Assert.IsNull(jToken2["Model"]);
            Assert.IsNull(jToken2["SerialNumber"]);
            Assert.IsNull(jToken2["IPAddress"]);
            Assert.IsNull(jToken2["PurchaseDate"]);
            Assert.AreEqual(jToken2["HasServiceContract"], default(bool));
            Assert.IsNull(jToken2["ContractNumber"]);
            Assert.IsNull(jToken2["ContractStartDate"]);
            Assert.IsNull(jToken2["ContractEndDate"]);
            Assert.IsNull(jToken2["ContractInfo"]);
            Assert.IsNull(jToken2["LocationID"]);
            Assert.IsNull(jToken2["Machine"]);
        }
    }


}
