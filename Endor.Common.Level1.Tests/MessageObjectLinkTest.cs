﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class MessageObjectLinkTest
    {

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        private static MessageObjectLink TestMessageObjectLink()
        {
            MessageObjectLink messageObjectLink = new MessageObjectLink
            {
                BID = 1,
                BodyID = 500,
                LinkedObjectClassTypeID = 500,
                LinkedObjectID = 1000,
                IsSourceOfMessage = false,
                DisplayText = "Display Text"
            };


            return messageObjectLink;
        }

        [TestMethod]
        public void MessageObjectLinkSerializationTest()
        {
            MessageObjectLink messageObjectLink = TestMessageObjectLink();

            string msgObjectLinkSerialized = JsonConvert.SerializeObject(messageObjectLink);
            JToken msgObjectLinkToken = JToken.Parse(msgObjectLinkSerialized);

            //Token Count
            Assert.AreEqual(7, msgObjectLinkToken.Values().Count(), msgObjectLinkToken.ToString());

            //Serialized
            Assert.IsNotNull(msgObjectLinkToken[nameof(messageObjectLink.BID)]);
            Assert.IsNotNull(msgObjectLinkToken[nameof(messageObjectLink.ID)]);
            Assert.IsNotNull(msgObjectLinkToken[nameof(messageObjectLink.BodyID)]);
            Assert.IsNotNull(msgObjectLinkToken[nameof(messageObjectLink.LinkedObjectClassTypeID)]);
            Assert.IsNotNull(msgObjectLinkToken[nameof(messageObjectLink.LinkedObjectID)]);
            Assert.IsNotNull(msgObjectLinkToken[nameof(messageObjectLink.IsSourceOfMessage)]);
            Assert.IsNotNull(msgObjectLinkToken[nameof(messageObjectLink.DisplayText)]);

        }

    }
}
