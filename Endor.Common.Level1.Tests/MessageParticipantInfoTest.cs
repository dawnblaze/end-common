﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class MessageParticipantInfoTest
    {

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        private static MessageParticipantInfo TestMessageParticipantInfo()
        {
            MessageParticipantInfo messageParticipantInfo = new MessageParticipantInfo
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageParticipantInfo,
                BodyID = 1000,
                ParticipantRoleType = MessageParticipantRoleType.To,
                Channel = MessageChannelType.Email,
                UserName = "Test",
                IsMergeField = false,
                EmployeeID = 1000,
                TeamID = 1000,
                DeliveredDT = DateTime.Now,
                ContactID = 1000,
                IsDelivered = false
            };


            return messageParticipantInfo;
        }

        [TestMethod]
        public void MessageParticipationInfoSerializationTest()
        {
            MessageParticipantInfo messageParticipantInfo = TestMessageParticipantInfo();

            string msgParticipationInfoSerialized = JsonConvert.SerializeObject(messageParticipantInfo);
            JToken msgParticipationInfoJToken = JToken.Parse(msgParticipationInfoSerialized);

            //Token Count
            Assert.AreEqual(12, msgParticipationInfoJToken.Values().Count(), msgParticipationInfoJToken.ToString());

            //Serialized
            Assert.IsNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.BID)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.ID)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.ClassTypeID)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.ModifiedDT)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.BodyID)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.EmployeeID)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.ParticipantRoleType)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.Channel)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.UserName)]);
            Assert.IsNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.IsMergeField)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.TeamID)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.DeliveredDT)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.ContactID)]);
            Assert.IsNotNull(msgParticipationInfoJToken[nameof(messageParticipantInfo.IsDelivered)]);
        }

    }
}
