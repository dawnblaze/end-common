﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class DestinationTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void BasicDestinationDataSerializationTest()
        {
            DestinationData destinationData = new DestinationData();


            destinationData.Template = new AssemblyData();

            var serializedDestinationData = JsonConvert.SerializeObject(destinationData);
            JToken jToken = JToken.Parse(serializedDestinationData);
            
            Assert.IsNull(jToken["TemplateID"]);
            Assert.IsNull(jToken["WorksheetData"]);
            Assert.IsNull(jToken["Instances"]);
            Assert.IsNull(jToken["Profiles"]);
            Assert.IsNull(jToken["Template"]);

        }

        [TestMethod]
        public void BasicDestinationProfileSerializationTest()
        {
            DestinationProfile DestinationProfile = new DestinationProfile();

            DestinationProfile.BID = 1;
            DestinationProfile.Destination = new DestinationData();
            DestinationProfile.Template = new AssemblyData();

            var serializedDestinationProfile = JsonConvert.SerializeObject(DestinationProfile);
            JToken jToken = JToken.Parse(serializedDestinationProfile);

            Assert.IsNull(jToken["BID"]);
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken["IsActive"], default(bool));
            Assert.AreEqual(jToken["IsDefault"], default(bool));
            Assert.IsNull(jToken["Name"]);
            Assert.AreEqual(jToken["DestinationID"], default(int));
            Assert.AreEqual(jToken["TemplateID"], default(int));
            Assert.IsNull(jToken["AssemblyDataJSON"]);
            Assert.IsNull(jToken["EstimatingCostPerHourFormula"]);
            Assert.IsNull(jToken["EstimatingPricePerHourFormula"]);
            Assert.IsNull(jToken["Destination"]);
            Assert.IsNull(jToken["Template"]);
        }
    }
}
