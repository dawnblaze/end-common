﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class OrderOrderLinkTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        [TestMethod]
        public void OrderOrderLinkSerializationTest()
        {
            OrderOrderLink orderOrderLink = new OrderOrderLink()
            {
                BID = 1,
                OrderID = 1,
                ModifiedDT = DateTime.UtcNow,
                LinkType = OrderOrderLinkType.ChildOrder,
                LinkedOrderID = 1,
                LinkedFormattedNumber = "LinkedFormattedNumber",
                Description = "Description",
                Amount = null,
                EmployeeID = null
            };

            string serializedOrderOrderlink = JsonConvert.SerializeObject(orderOrderLink);
            JToken orderOrderLinkToken = JToken.Parse(serializedOrderOrderlink);

            Assert.IsNotNull(orderOrderLinkToken["OrderID"]);
            Assert.IsNotNull(orderOrderLinkToken["LinkType"]);
            Assert.IsNotNull(orderOrderLinkToken["LinkedOrderID"]);
            Assert.IsNotNull(orderOrderLinkToken["LinkedFormattedNumber"]);
            Assert.IsNotNull(orderOrderLinkToken["Description"]);

            Assert.AreEqual(5, orderOrderLinkToken.Values().Count(), orderOrderLinkToken.ToString());

            orderOrderLink.Amount = 10.0m;
            orderOrderLink.EmployeeID = 1;

            serializedOrderOrderlink = JsonConvert.SerializeObject(orderOrderLink);
            orderOrderLinkToken = JToken.Parse(serializedOrderOrderlink);

            Assert.IsNotNull(orderOrderLinkToken["Amount"]);
            Assert.IsNotNull(orderOrderLinkToken["EmployeeID"]);
            Assert.AreEqual(7, orderOrderLinkToken.Values().Count());

            orderOrderLink.Employee = new EmployeeData();
            orderOrderLink.Order = new OrderData();
            orderOrderLink.LinkedOrder = new OrderData();

            serializedOrderOrderlink = JsonConvert.SerializeObject(orderOrderLink);
            orderOrderLinkToken = JToken.Parse(serializedOrderOrderlink);

            Assert.IsNotNull(orderOrderLinkToken["Employee"]);
            Assert.IsNotNull(orderOrderLinkToken["Order"]);
            Assert.IsNotNull(orderOrderLinkToken["LinkedOrder"]);

            Assert.AreEqual(10, orderOrderLinkToken.Values().Count());
        }        
    }
}
