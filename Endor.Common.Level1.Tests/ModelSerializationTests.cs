﻿using Endor.Models;
using Endor.RTM.Models;
using Endor.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json.Schema.Generation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class ModelSerializationTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void SerializeLocationDataTest()
        {
            var entity = new LocationData()
            {
                BID = 1,
                ID = 99,
                ClassTypeID = (int)ClassType.Location,
                CreatedDate = DateTime.UtcNow,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsDefault = false,
                HasImage = false,
                Name = "Test Location",
                LegalName = "Test Location Legal Name",
                DBA = "Test Location DBA",
                EstimatePrefix = "EST",
                InvoicePrefix = "INV",
                POPrefix = "PO",
                OrderNumberPrefix = "ORD",
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(20, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken["BID"]);
            Assert.IsNotNull(jToken["ID"]);
            Assert.IsNotNull(jToken["Name"]);
            Assert.IsNotNull(jToken["LegalName"]);
            Assert.IsNotNull(jToken["DBA"]);
            Assert.IsNotNull(jToken["CreatedDate"]);
            Assert.IsNotNull(jToken["ModifiedDT"]);
            Assert.IsNull(jToken["DefaultAreaCode"]);
            Assert.IsNull(jToken["DefaultTaxGroupID"]);
            Assert.IsNotNull(jToken["HasImage"]);
            Assert.IsNotNull(jToken["EstimatePrefix"]);
            Assert.IsNotNull(jToken["InvoicePrefix"]);
            Assert.IsNotNull(jToken["OrderNumberPrefix"]);
            Assert.IsNotNull(jToken["POPrefix"]);
            Assert.IsNotNull(jToken["IsActive"]);
            Assert.IsNotNull(jToken["IsDefault"]);
            Assert.IsNull(jToken["TimeZoneID"]);
        }

        [TestMethod]
        public void SerializeOrderEmployeeRoleTest()
        {
            var orderEmployeeRole = new OrderEmployeeRole()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.Order.ID(),
                ModifiedDT = DateTime.UtcNow,
                OrderID = 1,
                EmployeeID = 1,
                IsDestinationRole = false,
                IsOrderItemRole = false,
                IsOrderRole = true,
                RoleID = 1
            };

            string serializedOrderEmployeeRole = JsonConvert.SerializeObject(orderEmployeeRole);
            JToken orderEmployeeRoleToken = JToken.Parse(serializedOrderEmployeeRole);

            Assert.AreEqual(6, orderEmployeeRoleToken.Values().Count(), orderEmployeeRoleToken.ToString());
            Assert.IsNull(orderEmployeeRoleToken["BID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ClassTypeID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ModifiedDT"]);
            Assert.IsNotNull(orderEmployeeRoleToken["OrderID"]);
            Assert.IsNull(orderEmployeeRoleToken["OrderItemID"]);
            Assert.IsNull(orderEmployeeRoleToken["DestinationID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["RoleID"]);
            Assert.IsNull(orderEmployeeRoleToken["EmployeeName"]);
            Assert.IsNull(orderEmployeeRoleToken["IsOrderRole"]);
            Assert.IsNull(orderEmployeeRoleToken["IsOrderItemRole"]);
            Assert.IsNull(orderEmployeeRoleToken["IsDestinationRole"]);
            Assert.IsNull(orderEmployeeRoleToken["Employee"]);

            orderEmployeeRole.EmployeeName = "Bob Jones";
            orderEmployeeRole.Employee = new EmployeeData() { LongName = "Bob Jones" };

            serializedOrderEmployeeRole = JsonConvert.SerializeObject(orderEmployeeRole);
            orderEmployeeRoleToken = JToken.Parse(serializedOrderEmployeeRole);

            Assert.AreEqual(8, orderEmployeeRoleToken.Values().Count(), orderEmployeeRoleToken.ToString());
            Assert.IsNull(orderEmployeeRoleToken["BID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ClassTypeID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ModifiedDT"]);
            Assert.IsNotNull(orderEmployeeRoleToken["OrderID"]);
            Assert.IsNull(orderEmployeeRoleToken["OrderItemID"]);
            Assert.IsNull(orderEmployeeRoleToken["DestinationID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["RoleID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["EmployeeName"]);
            Assert.IsNull(orderEmployeeRoleToken["IsOrderRole"]);
            Assert.IsNull(orderEmployeeRoleToken["IsOrderItemRole"]);
            Assert.IsNull(orderEmployeeRoleToken["IsDestinationRole"]);
            Assert.IsNotNull(orderEmployeeRoleToken["Employee"]);
        }

        [TestMethod]
        public void SerializeEmployeeTest()
        {
            var employee = new EmployeeData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.Order.ID(),
                ModifiedDT = DateTime.UtcNow,
                LongName = "Bob Jones",
        };

            string serializedEmployee = JsonConvert.SerializeObject(employee);
            JToken employeeToken = JToken.Parse(serializedEmployee);

            Assert.AreEqual(15, employeeToken.Values().Count(), employeeToken.ToString());
            Assert.IsNotNull(employeeToken["BID"]);
            Assert.IsNotNull(employeeToken["ID"]);
            Assert.IsNotNull(employeeToken["ClassTypeID"]);
            Assert.IsNotNull(employeeToken["ModifiedDT"]);
            Assert.IsNull(employeeToken["DefaultModule"]);
        }
        [TestMethod]
        public void SerializeEmployeeTeamLinkTest()
        {
            var entity = new EmployeeTeamLink()
            {
                BID = 1,
                EmployeeID = 100,
                RoleID = 254,
                TeamID = 12,
                Employee = new EmployeeData()
                {
                    LongName = "John Doe"
                },
                EmployeeTeam = new EmployeeTeam()
                {
                    Name = "Test Team"
                },
                Role = new EmployeeRole()
                {
                    Name = "Entered by"
                }
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(4, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken["BID"]);
            Assert.IsNotNull(jToken["EmployeeID"]);
            Assert.IsNotNull(jToken["RoleID"]);
            Assert.IsNull(jToken["TeamID"]);
            Assert.IsNotNull(jToken["Employee"]);
            Assert.IsNull(jToken["EmployeeTeam"]);
            Assert.IsNotNull(jToken["Role"]);
        }

        [TestMethod]
        public void SerializeOrderDestinationDataTest()
        {
            var entity = new OrderDestinationData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.OrderDestination,
                ModifiedDT = DateTime.UtcNow,
                OrderID = 1,
                DestinationNumber = 1,
                DestinationType = OrderDestinationType.PickedUp,
                IsForAllItems = true,
                Name = "Test Destination",
                OrderStatusID = OrderOrderStatus.DestinationPending,
                ItemStatusID = 1,
                PriceIsLocked = false,
                PriceNetOV = false,
                PriceTaxableOV = false,
                TaxGroupID = 1,
                TaxGroupOV = false,
                IsTaxExempt = false,
                CostTotal = 0.0m,
                HasDocuments = false
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(20, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken["BID"]);
            Assert.IsNotNull(jToken["ID"]);
            Assert.IsNotNull(jToken["ClassTypeID"]);
            Assert.IsNotNull(jToken["ModifiedDT"]);
            Assert.IsNotNull(jToken["OrderID"]);
            Assert.IsNotNull(jToken["DestinationNumber"]);
            Assert.IsNotNull(jToken["DestinationType"]);
            Assert.IsNotNull(jToken["IsForAllItems"]);
            Assert.IsNotNull(jToken["Name"]);
            Assert.IsNotNull(jToken["OrderStatusID"]);
            Assert.IsNotNull(jToken["ItemStatusID"]);
            Assert.IsNotNull(jToken["PriceIsLocked"]);
            Assert.IsNotNull(jToken["PriceNetOV"]);
            Assert.IsNotNull(jToken["PriceTaxableOV"]);
            Assert.IsNotNull(jToken["TaxGroupID"]);
            Assert.IsNotNull(jToken["TaxGroupOV"]);
            Assert.IsNotNull(jToken["IsTaxExempt"]);
            Assert.IsNotNull(jToken["CostTotal"]);
            Assert.IsNotNull(jToken["HasDocuments"]);

            Assert.IsNull(jToken["SubStatusID"]);
            Assert.IsNull(jToken["Variables"]);
            Assert.IsNull(jToken["PriceNet"]);
            Assert.IsNull(jToken["PriceDiscount"]);
            Assert.IsNull(jToken["PricePreTax"]);
            Assert.IsNull(jToken["PriceTaxable"]);
            Assert.IsNull(jToken["PriceTax"]);
            Assert.IsNull(jToken["PriceTotal"]);
            Assert.IsNull(jToken["ComputedNet"]);
            Assert.IsNull(jToken["ComputedPreTax"]);
            Assert.IsNull(jToken["TaxExemptReasonID"]);
            Assert.IsNull(jToken["CostMaterial"]);
            Assert.IsNull(jToken["CostLabor"]);
            Assert.IsNull(jToken["CostMachine"]);
            Assert.IsNull(jToken["CostOther"]);
            Assert.IsNull(jToken["Order"]);
            Assert.IsNull(jToken["EnumOrderOrderStatus"]);
            Assert.IsNull(jToken["EnumOrderDestinationType"]);
            Assert.IsNull(jToken["OrderItemStatus"]);
            Assert.IsNull(jToken["TaxGroup"]);
            Assert.IsNull(jToken["SubStatus"]);
            Assert.IsNull(jToken["Notes"]);
            Assert.IsNull(jToken["Dates"]);
            Assert.IsNull(jToken["ContactRoles"]);
            Assert.IsNull(jToken["EmployeeRoles"]);
        }

        [TestMethod]
        public void SerializeCustomFieldDefinitionTest()
        {
            var entity = new CustomFieldDefinition()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.CustomFieldDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsActive = true,
                IsSystem = true,
                AppliesToClassTypeID = 1,
                AppliesToID = 1,
                Name = "TestName",
                Label = "TestLabel",
                Description = "TestDescription",
                DataType = new DataType(),
                ElementType = AssemblyElementType.Checkbox,
                DisplayType = CustomFieldNumberDisplayType.General,
                DisplayFormatString = "TestDisplayFormatString",
                AllowMultipleValues = true,
                HasValidators = true,
                AppliesToClass = true,
                IsRequired = true,
                AllowCustomValue = true,
                AllowDecimals = true,
                AllowMultiSelect = true,
                AllowNotSetOption = true,
                AllowTextFormatting = true,
                AllowUserTypedOptions = true,
                AltText = "",
                DecimalPlaces = 2,
                DefaultOption = "",
                DefaultValue = "",
                DisplayFormat = "",
                DisplaySpinner = true,
                ElementUseCount = 0,
                GroupOptionsByCategory = true,
                GroupType = 0,
                HasMaxLength = 0,
                IsDisabled = false,
                LabelType = 0,
                ListDataType = DataType.None,
                ListOptions = "",
                ListValuesJSON = "",
                NumberOfDecimals = 0,
                OptionOneLabel = "",
                OptionTwoLabel = "",
                PickerType = 0,
                PresetTime = false,
                SpinnerIncrement = 2,
                Time = DateTime.UtcNow,
                ToggleOptions = false,
                Tooltip = "",
                UnitID = Unit.Box,
                UnitType = UnitType.Area
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);


            Assert.AreEqual(48, jToken.Values().Count(), jToken.ToString());
            //BID is jsonignored Assert.IsNotNull(jToken["BID"]);
            Assert.IsNotNull(jToken["ID"]);
            //ClassTypeID is jsonignored Assert.IsNotNull(jToken["ClassTypeID"]);
            //ModifiedDT is jsonignored Assert.IsNotNull(jToken["ModifiedDT"]);
            Assert.IsNotNull(jToken["IsActive"]);
            Assert.IsNotNull(jToken["IsSystem"]);
            Assert.IsNotNull(jToken["AppliesToClassTypeID"]);
            Assert.IsNotNull(jToken["AppliesToID"]);
            Assert.IsNotNull(jToken["Name"]);
            Assert.IsNotNull(jToken["Label"]);
            Assert.IsNotNull(jToken["Description"]);
            Assert.IsNotNull(jToken["DataType"]);
            Assert.IsNotNull(jToken["ElementType"]);
            Assert.IsNotNull(jToken["DisplayType"]);
            Assert.IsNotNull(jToken["DisplayFormatString"]);
            Assert.IsNotNull(jToken["AllowMultipleValues"]);
            Assert.IsNotNull(jToken["HasValidators"]);
            Assert.IsNotNull(jToken["AppliesToClass"]);
            Assert.IsNotNull(jToken["IsRequired"]);
        }

        [TestMethod]
        public void SerializeCompanyCustomDataTest()
        {
            var entity = new CompanyCustomData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.CompanyCustomField,
                ModifiedDT = DateTime.UtcNow,
                AppliesToClassTypeID = 2000,
                DataXML = null
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(5, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken["BID"]);
            Assert.IsNotNull(jToken["ID"]);
            Assert.IsNotNull(jToken["ClassTypeID"]);
            Assert.IsNotNull(jToken["ModifiedDT"]);
            Assert.IsNotNull(jToken["AppliesToClassTypeID"]);
            Assert.IsNull(jToken["DataXML"]);
        }

        [TestMethod]
        public void SerializeContactCustomDataTest()
        {
            var entity = new ContactCustomData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.ContactCustom,
                ModifiedDT = DateTime.UtcNow,
                AppliesToClassTypeID = 2000,
                DataXML = null
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(5, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken["BID"]);
            Assert.IsNotNull(jToken["ID"]);
            Assert.IsNotNull(jToken["ClassTypeID"]);
            Assert.IsNotNull(jToken["ModifiedDT"]);
            Assert.IsNotNull(jToken["AppliesToClassTypeID"]);
            Assert.IsNull(jToken["DataXML"]);
        }

        [TestMethod]
        public void SerializeOrderCustomDataTest()
        {
            var entity = new OrderCustomData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.OrderCustom,
                ModifiedDT = DateTime.UtcNow,
                AppliesToClassTypeID = 2000,
                DataXML = null
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(5, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken["BID"]);
            Assert.IsNotNull(jToken["ID"]);
            Assert.IsNotNull(jToken["ClassTypeID"]);
            Assert.IsNotNull(jToken["ModifiedDT"]);
            Assert.IsNotNull(jToken["AppliesToClassTypeID"]);
            Assert.IsNull(jToken["DataXML"]);
        }

        [TestMethod]
        public void SerializeOtherCustomDataTest()
        {
            var entity = new CustomFieldOtherData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.CustomFieldOther,
                ModifiedDT = DateTime.UtcNow,
                AppliesToClassTypeID = 2000,
                DataXML = null
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(5, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken["BID"]);
            Assert.IsNotNull(jToken["ID"]);
            Assert.IsNotNull(jToken["ClassTypeID"]);
            Assert.IsNotNull(jToken["ModifiedDT"]);
            Assert.IsNotNull(jToken["AppliesToClassTypeID"]);
            Assert.IsNull(jToken["DataXML"]);
        }

        [TestMethod]
        public void CompanyCFValuesTest()
        {
            CompanyData company = new CompanyData()
            {
                BID = 1,
                ID = -99,
                Name = "Test Subject",
                LocationID = 1,
                IsAdHoc = true,
                StatusID = 1,
                CFValuesJSON = "[ {\"ID\":123, \"V\":\"Yellow\"}, {\"ID\":152, \"V\":\"322.5\"} ]"
            };

            string companySerialized = JsonConvert.SerializeObject(company);
            JToken companyJToken = JToken.Parse(companySerialized);

            Assert.IsNotNull(companyJToken["CFValues"]);

            int expectedCount = 2;
            Assert.AreEqual(expectedCount, companyJToken["CFValues"].Count(), "Expected value is " + expectedCount);

            string expectedColor = "Yellow";
            Assert.AreEqual(expectedColor, companyJToken["CFValues"][0]["V"], "Expected value is " + expectedColor);

            string expectedValue = "322.5";
            Assert.AreEqual(expectedValue, companyJToken["CFValues"][1]["V"], "Expected value is " + expectedValue);

        }

        [TestMethod]
        public void SerializeBoardDefinitionTest()
        {
            var entity = new BoardDefinitionData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.BoardDefinition,
                ModifiedDT = DateTime.UtcNow,
                IsSystem = true,
                IsActive = true,
                Name = "test",
                Description = "test",
                ConditionFx = "test",
                ConditionSQL = "test",
                DataType = DataType.Boolean,
                IsAlwaysShown = true,
                LimitToRoles = true,
                AverageRunDuration = null,
                LastRecordCount = null,
                LastRunDT = null,
                LastRunDurationSec = null,
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(11, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken["BID"]);
            Assert.IsNotNull(jToken["ID"]);
            Assert.IsNotNull(jToken["ClassTypeID"]);
            Assert.IsNotNull(jToken["ModifiedDT"]);
            Assert.IsNotNull(jToken["IsSystem"]);
            Assert.IsNotNull(jToken["IsActive"]);
            Assert.IsNotNull(jToken["Name"]);
            Assert.IsNotNull(jToken["Description"]);
            Assert.IsNotNull(jToken["ConditionFx"]);
            Assert.IsNull(jToken["ConditionSQL"]);
            Assert.IsNotNull(jToken["DataType"]);
            Assert.IsNotNull(jToken["IsAlwaysShown"]);
            Assert.IsNotNull(jToken["LimitToRoles"]);
            Assert.IsNull(jToken["AverageRunDuration"]);
            Assert.IsNull(jToken["LastRecordCount"]);
            Assert.IsNull(jToken["LastRunDT"]);
            Assert.IsNull(jToken["LastRunDurationSec"]);
        }

        [TestMethod]
        public void SerializeRightsGroupTest()
        {
            var entity = new RightsGroup()
            {
                ID = 99,
                Name = "Test RightsGroup",
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(2, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken[nameof(entity.ID)]);
        }

        [TestMethod]
        public void SerializeRightsGroupListTest()
        {
            var entity = new RightsGroupList()
            {
                BID = 1,
                ID = 99,
                ClassTypeID = (int)ClassType.Location,
                Name = "Test RightsGroupLink",
                RightsArray = "AAA",
                RightsGroups = null,
                UserLinks = new List<UserLink>()
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(7, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.RightsArray)]);
            Assert.IsNull(jToken[nameof(entity.RightsGroups)]);
            Assert.IsNotNull(jToken[nameof(entity.UserLinks)]);
        }

        [TestMethod]
        public void RightsGroupListRightsGroupLink()
        {
            var entity = new RightsGroupListRightsGroupLink()
            {
                BID = 1,
                GroupID = 100,
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(2, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNotNull(jToken[nameof(entity.GroupID)]);
        }

        [TestMethod]
        public void SerializeRightsGroupChildGroupLinkTest()
        {
            var entity = new RightsGroupChildGroupLink()
            {
                ParentID = 99,
                ChildID = 100,
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(2, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken[nameof(entity.ParentID)]);
            Assert.IsNotNull(jToken[nameof(entity.ChildID)]);
        }

        [TestMethod]
        public void SerializeRightsGroupRightLinkTest()
        {
            var entity = new RightsGroupRightLink()
            {
                GroupID = 99,
                RightID = 0,
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(2, jToken.Values().Count(), jToken.ToString());
            Assert.IsNotNull(jToken[nameof(entity.GroupID)]);
            Assert.IsNotNull(jToken[nameof(entity.RightID)]);
        }

        [TestMethod]
        public void SerializeUserLinkTest()
        {
            var entity = new UserLink()
            {
                BID = 1,
                ID = 99,
                ClassTypeID = (int)ClassType.Contact,
                ContactID = 99,
                EmployeeID = 99,
                UserAccessType = UserAccessType.Contact,
                RightsGroupListID = 99,
                AuthUserID = 99,
                TempUserID = Guid.Empty,
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(8, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.ContactID)]);
            Assert.IsNotNull(jToken[nameof(entity.EmployeeID)]);
            Assert.IsNotNull(jToken[nameof(entity.UserAccessType)]);
            Assert.IsNotNull(jToken[nameof(entity.RightsGroupListID)]);
            Assert.IsNotNull(jToken[nameof(entity.AuthUserID)]);
            Assert.IsNotNull(jToken[nameof(entity.TempUserID)]);
        }

        [TestMethod]
        public void SerializeBoardEmployeeLinkTest()
        {
            var entity = new BoardEmployeeLink()
            {
                BID = 1,
                BoardID = 2,
                EmployeeID = 3,
                SortIndex = 0,
                ModuleType = Module.Accounting
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(4, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNotNull(jToken[nameof(entity.BoardID)]);
            Assert.IsNotNull(jToken[nameof(entity.EmployeeID)]);
            Assert.IsNotNull(jToken[nameof(entity.SortIndex)]);
            Assert.IsNull(jToken[nameof(entity.IsFavorite)]);
            Assert.IsNotNull(jToken[nameof(entity.ModuleType)]);
        }

        [TestMethod]
        public void SerializeBoardModuleLinkTest()
        {
            var entity = new BoardModuleLink()
            {
                BID = 1,
                BoardID = 2,
                ModuleType = Module.Ecommerce,
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(2, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNotNull(jToken[nameof(entity.BoardID)]);
            Assert.IsNotNull(jToken[nameof(entity.ModuleType)]);
        }

        [TestMethod]
        public void SerializeBoardRoleLinkTest()
        {
            var entity = new BoardRoleLink()
            {
                BID = 1,
                BoardID = 2,
                RoleID = 3,
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(2, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNotNull(jToken[nameof(entity.BoardID)]);
            Assert.IsNotNull(jToken[nameof(entity.RoleID)]);
        }

        [TestMethod]
        public void SerializeDomainDataTest()
        {
            var entity = new DomainData()
            {
                BID = 1,
                ID = 2,
                ClassTypeID = (int)ClassType.Domain,
                ModifiedDT = DateTime.UtcNow,
                Domain = "TEST DOMAIN NAME",
                Status = DomainStatus.Pending,
                LocationID = 1,
                AccessType = DomainAccessType.Business
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(7, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNull(jToken[nameof(entity.Location)]);
            Assert.IsNull(jToken[nameof(entity.SSLCertificate)]);
            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.ModifiedDT)]);
            Assert.IsNotNull(jToken[nameof(entity.Domain)]);
            Assert.IsNotNull(jToken[nameof(entity.Status)]);
            Assert.IsNotNull(jToken[nameof(entity.LocationID)]);
            Assert.IsNotNull(jToken[nameof(entity.AccessType)]);

        }

        [TestMethod]
        public void SerializeSSLCertificateDataTest()
        {
            var entity = new SSLCertificateData()
            {
                BID = 1,
                ID = 2,
                ClassTypeID = (int)ClassType.SSLCertificate,
                ModifiedDT = DateTime.UtcNow,
                CommonName = "TEST SSLC CERT NAME",
                FileName = "TEST FILE NAME",
                Thumbprint = "TEST THUMBPRINT",
                CanHaveMultipleDomains = false,
                InstalledDT = DateTime.UtcNow,
                ValidFromDT = DateTime.UtcNow,
                ValidToDT = DateTime.UtcNow.AddMonths(3)
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(10, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.ModifiedDT)]);
            Assert.IsNotNull(jToken[nameof(entity.CommonName)]);
            Assert.IsNotNull(jToken[nameof(entity.Thumbprint)]);
            Assert.IsNotNull(jToken[nameof(entity.FileName)]);
            Assert.IsNotNull(jToken[nameof(entity.CanHaveMultipleDomains)]);
            Assert.IsNotNull(jToken[nameof(entity.InstalledDT)]);
            Assert.IsNotNull(jToken[nameof(entity.ValidFromDT)]);
            Assert.IsNotNull(jToken[nameof(entity.ValidToDT)]);

        }

        [TestMethod]
        public void TestCustomFieldLayoutDefinition()
        {
            var layout = new CustomFieldLayoutDefinition();
            layout.ID = -99;
            layout.BID = 1;
            layout.IsActive = true;
            layout.IsAllTab = true;
            layout.IsSubTab = true;
            layout.ModifiedDT = DateTime.UtcNow;
            layout.Name = "bar";
            layout.SortIndex = 1;
            this.SerializeAndTest(layout);
        }

        [TestMethod]
        public void TestCustomFieldLayoutElement()
        {
            var element = new CustomFieldLayoutElement();
            element.BID = 1;
            element.ID = 1;
            element.ClassTypeID = 1;
            element.Name = "bar";
            element.Definition = new CustomFieldDefinition();
            element.DefinitionID = 1;
            element.DataType = 1;
            element.ModifiedDT = DateTime.UtcNow;
            element.Column = 1;
            element.ColumnsWide = 1;
            element.ElementType = AssemblyElementType.Checkbox;
            element.IsDisabled = false;
            element.IsReadOnly = false;
            element.Row = 1;
            element.Tooltip = "";
            element.LayoutID = 0;

            this.SerializeAndTest(element, nameof(element.BID), nameof(element.ModifiedDT));
        }

        public void SerializeAndTest<T>(T entity, params string[] ignoredProperties)
        {
            if (ignoredProperties == null)
            {
                ignoredProperties = new string[] { };
            }

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(entity);
            Dictionary<string, object> propsDictionary = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

            foreach (var prop in typeof(T).GetProperties())
            {
                if (ignoredProperties.Contains(prop.Name))
                {
                    Assert.IsFalse(propsDictionary.ContainsKey(prop.Name), $"{prop.Name} should not be serialized");
                }
                else if (prop.GetValue(entity) == null)
                {
                    Assert.IsFalse(propsDictionary.ContainsKey(prop.Name), $"{prop.Name} should not be serialized");
                }
                else
                {
                    Assert.IsTrue(propsDictionary.ContainsKey(prop.Name), $"{prop.Name} should be serialized");
                }
            }
        }

        [TestMethod]
        public void BoardView()
        {
            var view = new BoardView
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.BoardView,
                Name = "Card",
                IsActive = true,
                IsSystem = true,
                DataType = 0,
                GroupBy = null,
                Columns = null,
                SortOptions = null,
                CustomLayout = null,
                IsTileView = true,
                Boards = null
            };

            var serialized = JsonConvert.SerializeObject(view);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }


        [TestMethod]
        public void BoardDefinitionWithLinksAndView()
        {
            var board = new BoardDefinitionData
            {
                BID = 1,
                ID = 999,
                IsActive = true,
                ClassTypeID = (int)ClassType.BoardDefinition,
                DataType = DataType.Order,
                ConditionFx = null,
            };
            var view1 = new BoardView
            {
                BID = 1,
                ID = 998,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.BoardView,
                Name = "Card",
                IsActive = true,
                IsSystem = true,
                DataType = 0,
                GroupBy = null,
                Columns = null,
                SortOptions = null,
                CustomLayout = null,
                IsTileView = true,
                Boards = null
            };

            var view2 = new BoardView
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.BoardView,
                Name = "List",
                IsActive = true,
                IsSystem = true,
                DataType = 0,
                GroupBy = null,
                Columns = null,
                SortOptions = null,
                CustomLayout = null,
                IsTileView = false,
                Boards = null
            };

            var link1 = new BoardViewLink
            {
                BID = 1,
                BoardID = 999,
                BoardView = view1,
                IsPrimary = null,
                SortIndex = 0,
                ViewID = view1.ID
            };

            var link2 = new BoardViewLink
            {
                BID = 1,
                BoardID = 999,
                BoardView = view2,
                IsPrimary = null,
                SortIndex = 0,
                ViewID = view2.ID
            };

            board.BoardViewLinks = new List<BoardViewLink> { link1, link2 };

            var serialized = JsonConvert.SerializeObject(board);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }

        [TestMethod]
        public void SerializeAssemblyModelsTest()
        {
            var assembly = new AssemblyData
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.Assembly.ID(),
                Name = "Test.Assembly",
                Description = "Test.Assembly description block",
                ModifiedDT = DateTime.UtcNow,
                SKU = "MySKU",
                EnableArriveByDate = true,
                EnableBlindShipping = true,
                EnableFromAddress = true,
                EnablePackagesTab = true,
                EnableShipByDate = true,
                EnableToAddress = true
            };

            List<string> nullProps = new List<string>()
            {
                  nameof(AssemblyData.BID)
                , nameof(AssemblyData.AssemblyCategoryLinks)
                , nameof(AssemblyData.AssemblyCategories)
                , nameof(AssemblyData.SimpleAssemblyCategories)
                , nameof(AssemblyData.Variables)
                , nameof(AssemblyData.TaxabilityCodeID)
                , nameof(AssemblyData.IncomeAccountID)
                , nameof(AssemblyData.Layouts)
                , nameof(AssemblyData.Tables)
                , nameof(AssemblyData.FixedPrice)
                , nameof(AssemblyData.FixedMargin)
                , nameof(AssemblyData.FixedMarkup)
                , nameof(AssemblyData.ShipByDateLabel)
                , nameof(AssemblyData.ArriveByDateLabel)
                , nameof(AssemblyData.FromAddressLabel)
                , nameof(AssemblyData.ToAddressLabel)
                , nameof(AssemblyData.BlindShippingLabel)
            };
            this.SerializeAndTest(assembly, nullProps.ToArray());
            nullProps.AddRange(new List<string>()
            {
                  nameof(AssemblyData.EnableArriveByDate)
                , nameof(AssemblyData.EnableBlindShipping)
                , nameof(AssemblyData.EnableFromAddress)
                , nameof(AssemblyData.EnablePackagesTab)
                , nameof(AssemblyData.EnableShipByDate)
                , nameof(AssemblyData.EnableToAddress)
            });
            assembly.EnableArriveByDate = false;
            assembly.EnableBlindShipping = false;
            assembly.EnableFromAddress = false;
            assembly.EnablePackagesTab = false;
            assembly.EnableShipByDate = false;
            assembly.EnableToAddress = false;
            this.SerializeAndTest(assembly, nullProps.ToArray());

            var assemblyCategory = new AssemblyCategory();
            assemblyCategory.BID = 1;
            assemblyCategory.ID = 1;
            assemblyCategory.ClassTypeID = ClassType.AssemblyCategory.ID();
            assemblyCategory.Name = "Test.Assembly";
            assemblyCategory.Description = "";
            assemblyCategory.ModifiedDT = DateTime.UtcNow;
            nullProps = new List<string>() { "BID", "ParentID", "ParentCategory", "ChildCategories", "AssemblyCategoryLinks", "Assemblies", "SimpleAssemblies", "Elements", "Variables" };
            this.SerializeAndTest(assemblyCategory, nullProps.ToArray());

            var assemblyCategoryLink = new AssemblyCategoryLink();
            assemblyCategoryLink.BID = 1;
            assemblyCategoryLink.PartID = 1;
            assemblyCategoryLink.CategoryID = 1;
            nullProps = new List<string>() { "BID", "Assembly", "AssemblyCategory", "Elements", "Variables" };
            this.SerializeAndTest(assemblyCategoryLink, nullProps.ToArray());
        }

        [TestMethod]
        public void MessageHeader()
        {
            var msgHeader = new MessageHeader
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageHeader,
                ReceivedDT = DateTime.Now,
                EmployeeID = 1000,
                ParticipantID = 1000,
                BodyID = 1000,
                IsRead = false,
                ReadDT = DateTime.Now,
                IsDeleted = false,
                DeletedOrExpiredDT = DateTime.Now,
                IsExpired = false,
                Channels = MessageChannelType.Message,
                InSentFolder = false
            };

            var serialized = JsonConvert.SerializeObject(msgHeader);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }

        [TestMethod]
        public void MessageDeliveryRecord()
        {
            var msgDeliveryRecord = new MessageDeliveryRecord
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageDeliveryRecord,
                ParticipantID = 500,
                AttemptNumber = 500,
                IsPending = false,
                ScheduledDT = DateTime.Now,
                AttemptedDT = DateTime.Now,
                WasSuccessful = false,
                FailureMessage = "FailureMessage",
                MetaData = "Metadata"
            };

            var serialized = JsonConvert.SerializeObject(msgDeliveryRecord);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }


        [TestMethod]
        public void MessageObjectLink()
        {
            var msgObjectLink = new MessageObjectLink
            {
                BID = 1,
                BodyID = 500,
                LinkedObjectClassTypeID = 500,
                LinkedObjectID = 1000,
                IsSourceOfMessage = false,
                DisplayText = "Display Text"
            };

            var serialized = JsonConvert.SerializeObject(msgObjectLink);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }

        [TestMethod]
        public void MessageParticipantInfo()
        {
            var messageParticipantInfo = new MessageParticipantInfo
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageParticipantInfo,
                BodyID = 1000,
                ParticipantRoleType = MessageParticipantRoleType.To,
                Channel = MessageChannelType.Email,
                UserName = "Test",
                IsMergeField = false,
                EmployeeID = 1000,
                TeamID = 1000,
                DeliveredDT = DateTime.Now,
                ContactID = 1000,
                IsDelivered = false
            };

            var serialized = JsonConvert.SerializeObject(messageParticipantInfo);
            var deserialized = JsonConvert.DeserializeObject<BoardView>(serialized);
            Assert.IsNotNull(deserialized);
        }

        [TestMethod]
        public void SerializeAssemblyElementModelsTest()
        {
            var element = new AssemblyElement()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.AssemblyElement.ID(),
                ModifiedDT = DateTime.UtcNow,
                IsDisabled = false,
                IsReadOnly = false,
                Column = 1,
                ColumnsWide = 2,
                Row = 1,
                ElementType = AssemblyElementType.Checkbox,
                DataType = DataType.Order,
                AssemblyID = 1,
                VariableName = "somevariable",
                Elements = new List<AssemblyElement>()
            };
            var childElement = new AssemblyElement()
            {
                BID = 1,
                ID = 2,
                ClassTypeID = ClassType.AssemblyElement.ID(),
                ModifiedDT = DateTime.UtcNow,
                IsDisabled = false,
                IsReadOnly = false,
                Column = 1,
                ColumnsWide = 2,
                Row = 1,
                ElementType = AssemblyElementType.Checkbox,
                DataType = DataType.Number,
                AssemblyID = 1,
                ParentID = 1,
                VariableName = "somevariable",
                Elements = new List<AssemblyElement>()
            };

            var nullProps = new string[] { "BID", "ParentID", "Children", "Tooltip", "Variable", "VariableID", "TempVariableID" };
            this.SerializeAndTest(element, nullProps);

            nullProps = new string[] { "BID", "Children", "Options", "Tooltip", "DefaultValueText", "Variable", "VariableID", "TempVariableID" };
            this.SerializeAndTest(childElement, nullProps);

            element.Elements = new List<AssemblyElement>();
            element.Elements.Add(childElement);
            nullProps = new string[] { "BID", "ParentID", "Options", "Tooltip", "DefaultValueText", "Variable", "VariableID", "TempVariableID" };
            this.SerializeAndTest(element, nullProps);


        }

        [TestMethod]
        public void TaxabilityCodeSerializationTest()
        {
            var tcLinks = new List<TaxabilityCodeItemExemptionLink>()
            {
                new TaxabilityCodeItemExemptionLink()
                {
                    BID = 1,
                    TaxCodeID = 1,
                    TaxItemID = 1
                }
            };

            var tiList = new List<TaxItem>()
            {
                new TaxItem()
                {
                    BID = 1,
                    Name = "Test.TaxItem"
                }
            };

            var entity = new TaxabilityCode()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.TaxabilityCode.ID(),
                IsActive = true,
                IsSystem = false,
                IsTaxExempt = false,
                Name = "Test.TaxCode.Serialization",
                TaxabilityCodeItemExemptionLinks = tcLinks,
                TaxItems = tiList,
                HasExcludedTaxItems = false,
                ModifiedDT = DateTime.UtcNow
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(9, jToken.Values().Count(), jToken.ToString());

            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.IsActive)]);
            Assert.IsNotNull(jToken[nameof(entity.IsSystem)]);
            Assert.IsNotNull(jToken[nameof(entity.IsTaxExempt)]);
            Assert.IsNotNull(jToken[nameof(entity.Name)]);
            Assert.IsNotNull(jToken[nameof(entity.TaxItems)]);
            Assert.IsNotNull(jToken[nameof(entity.HasExcludedTaxItems)]);
            Assert.IsNotNull(jToken[nameof(entity.ModifiedDT)]);

            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNull(jToken[nameof(entity.Description)]);
            Assert.IsNull(jToken[nameof(entity.TaxCode)]);
            Assert.IsNull(jToken[nameof(entity.TaxabilityCodeItemExemptionLinks)]);
        }

        [TestMethod]
        public void QuickItemSerializationTest()
        {
            var entity = new QuickItemData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.QuickItem.ID(),
                IsActive = true,
                Name = "Test.Quick.Item.Data"
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(9, jToken.Values().Count(), jToken.ToString());

            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.IsActive)]);
            Assert.IsNotNull(jToken[nameof(entity.Name)]);
            Assert.IsNotNull(jToken[nameof(entity.ModifiedDT)]);
            Assert.IsNotNull(jToken[nameof(entity.LineItemCount)]);
            Assert.IsNotNull(jToken[nameof(entity.IsShared)]);
            Assert.IsNotNull(jToken[nameof(entity.IsCompanyItem)]);
            Assert.IsNotNull(jToken[nameof(entity.IsGlobalItem)]);

            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNull(jToken[nameof(entity.CompanyID)]);
            Assert.IsNull(jToken[nameof(entity.Company)]);
            Assert.IsNull(jToken[nameof(entity.EmployeeID)]);
            Assert.IsNull(jToken[nameof(entity.Employee)]);
            Assert.IsNull(jToken[nameof(entity.CategoryLinks)]);
            Assert.IsNull(jToken[nameof(entity.Categories)]);
            Assert.IsNull(jToken[nameof(entity.DataJSON)]);
        }

        [TestMethod]
        public void SurchargeDefSerializationTest()
        {
            var entity = new SurchargeDef()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.SurchargeDef.ID(),
                IsActive = true,
                Name = "Test.SurchargeDef",
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(9, jToken.Values().Count(), jToken.ToString());

            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.IsActive)]);
            Assert.IsNotNull(jToken[nameof(entity.Name)]);
            Assert.IsNotNull(jToken[nameof(entity.ModifiedDT)]);
            Assert.IsNotNull(jToken[nameof(entity.TaxCodeID)]);
            Assert.IsNotNull(jToken[nameof(entity.IncomeAccountID)]);
            Assert.IsNotNull(jToken[nameof(entity.AppliedByDefault)]);
            Assert.IsNotNull(jToken[nameof(entity.SortIndex)]);

            Assert.IsNull(jToken[nameof(entity.BID)]);
            Assert.IsNull(jToken[nameof(entity.DefaultFixedFee)]);
            Assert.IsNull(jToken[nameof(entity.DefaultPerUnitFee)]);
            Assert.IsNull(jToken[nameof(entity.CompanyID)]);
        }

        [TestMethod]
        public void AssemblyTableSerializationTest()
        {
            var entity = new AssemblyTable()
            {
                BID = 1,
                ID = 1,
                Label = "Test"
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(15, jToken.Values().Count(), jToken.ToString());

            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.AssemblyID)]);
            Assert.IsNotNull(jToken[nameof(entity.TableType)]);
            Assert.IsNotNull(jToken[nameof(entity.IsTierTable)]);
            Assert.IsNotNull(jToken[nameof(entity.RowDataType)]);
            Assert.IsNotNull(jToken[nameof(entity.RowCount)]);
            Assert.IsNotNull(jToken[nameof(entity.RowIsSorted)]);
            Assert.IsNotNull(jToken[nameof(entity.RowMatchType)]);
            Assert.IsNotNull(jToken[nameof(entity.ColumnCount)]);
            Assert.IsNotNull(jToken[nameof(entity.ColumnIsSorted)]);
            Assert.IsNotNull(jToken[nameof(entity.ColumnMatchType)]);
            Assert.IsNotNull(jToken[nameof(entity.CellDataType)]);

            Assert.IsNull(jToken[nameof(entity.RowVariableID)]);
            Assert.IsNull(jToken[nameof(entity.RowVariableTempID)]);
            Assert.IsNull(jToken[nameof(entity.RowUnitID)]);
            Assert.IsNull(jToken[nameof(entity.ColumnVariableID)]);
            Assert.IsNull(jToken[nameof(entity.ColumnVariableTempID)]);
            Assert.IsNull(jToken[nameof(entity.ColumnDataType)]);
            Assert.IsNull(jToken[nameof(entity.ColumnUnitID)]);
        }

        [TestMethod]
        public void DMAccessTokenSerializationTest()
        {
            var entity = new DMAccessToken()
            {
                BID = 1,
                ID = new Guid(),
                ExpirationDate = DateTime.UtcNow
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(4, jToken.Values().Count(), jToken.ToString());

            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.ExpirationDate)]);
            Assert.IsNotNull(jToken[nameof(entity.ViewedCount)]);

        }

        [TestMethod]
        public void EmailAccountDataSerializationTest()
        {
            var entity = new EmailAccountData()
            {
                BID = 1
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken jToken = JToken.Parse(serialized);

            Assert.AreEqual(7, jToken.Values().Count(), jToken.ToString());

            Assert.IsNotNull(jToken[nameof(entity.ID)]);
            Assert.IsNotNull(jToken[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(jToken[nameof(entity.ModifiedDT)]);
            Assert.IsNotNull(jToken[nameof(entity.IsActive)]);
            Assert.IsNotNull(jToken[nameof(entity.StatusType)]);
            Assert.IsNotNull(jToken[nameof(entity.DomainEmailID)]);
            Assert.IsNotNull(jToken[nameof(entity.IsPrivate)]);
            Assert.IsNull(jToken[nameof(entity.EmailAccountTeamLinks)]);

            entity.EmailAccountTeamLinks = new List<EmailAccountTeamLink>()
            {
                new EmailAccountTeamLink()
                {
                    BID = 1,
                    TeamID = 1,
                    EmailAccountID = 1
                }
            };
            serialized = JsonConvert.SerializeObject(entity);
            jToken = JToken.Parse(serialized);
            Assert.IsNotNull(jToken[nameof(entity.EmailAccountTeamLinks)]);
        }

        [TestMethod]
        public void PaymentMasterSerialization()
        {
            var entity = new PaymentMaster()
            {
                BID = 1
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken myObj = JToken.Parse(serialized);

            Assert.AreEqual(9, myObj.Values().Count(), myObj.ToString());

            Assert.IsNotNull(myObj[nameof(entity.ID)]);
            Assert.IsNotNull(myObj[nameof(entity.ClassTypeID)]);
            Assert.IsNull(myObj[nameof(entity.ModifiedDT)]);
            Assert.IsNotNull(myObj[nameof(entity.AccountingDT)]);

            entity.PaymentApplications = new HashSet<PaymentApplication>();
            entity.PaymentApplications.Add(new PaymentApplication());

            serialized = JsonConvert.SerializeObject(entity);
            myObj = JToken.Parse(serialized);

            Assert.IsNotNull(myObj[nameof(entity.PaymentApplications)]);
        }

        [TestMethod]
        public void GLAccountSerialization()
        {
            var entity = new GLAccount()
            {
                BID = 1,
                Name = "test"
            };

            var serialized = JsonConvert.SerializeObject(entity);
            JToken myObj = JToken.Parse(serialized);

            Assert.AreEqual(7, myObj.Values().Count(), myObj.ToString());

            Assert.IsNotNull(myObj[nameof(entity.ID)]);
            Assert.IsNotNull(myObj[nameof(entity.ClassTypeID)]);
            Assert.IsNull(myObj[nameof(entity.BID)]);
            Assert.IsNull(myObj[nameof(entity.PaymentMethods)]);
            Assert.IsNull(myObj[nameof(entity.IncomeMachines)]);
            Assert.IsNull(myObj[nameof(entity.ExpenseMachines)]);
            Assert.IsNull(myObj[nameof(entity.IncomeLabors)]);
            Assert.IsNull(myObj[nameof(entity.ExpenseLabors)]);
            Assert.IsNotNull(myObj[nameof(entity.Name)]);

        }

        [TestMethod]
        public void TestRefreshMessageSerialization()
        {
            RefreshEntity refreshEntity = new RefreshEntity();
            List<RefreshEntity> refreshEntities = new List<RefreshEntity>() { refreshEntity };
            RefreshMessage refreshMessage = new RefreshMessage() { RefreshEntities = refreshEntities };

            var serialized = JsonConvert.SerializeObject(refreshMessage);
            Assert.IsNotNull(serialized);
            JToken myObj = JToken.Parse(serialized);
            Assert.IsNotNull(myObj);
        }

        [TestMethod]
        public void VariableValueDictionarySerialization()
        {
            string test = @"
			{
				""1"": {
					""Value"": ""Paper""
				}
			}";

            JSchema jsonSchema = new JSchemaGenerator().Generate(typeof(Dictionary<string, VariableValue>));
            JsonTextReader reader = new JsonTextReader(new StringReader(test));

            JSchemaValidatingReader validatingReader = new JSchemaValidatingReader(reader) { Schema = jsonSchema };
            JsonSerializer serializer = new JsonSerializer();
            Dictionary<string, VariableValue> varDictionary = serializer.Deserialize<Dictionary<string, VariableValue>>(validatingReader);
            Assert.IsNotNull(varDictionary);
        }

        class VariableValueParent
        {
            public Dictionary<string, VariableValue> Variables { get; set; }
        }

        [TestMethod]
        public void VariableValueChildSerialization()
        {
            string test = @"
			{
		        ""Variables"": {
			        ""1072"": {
				        ""Value"": ""1""
                    },
			        ""1073"": {
				        ""Value"": ""b""
			        },
			        ""1074"": {
				        ""Value"": ""true""
			        }
		        }
			}";

            JSchema jsonSchema = new JSchemaGenerator().Generate(typeof(VariableValueParent));
            JsonTextReader reader = new JsonTextReader(new StringReader(test));

            JSchemaValidatingReader validatingReader = new JSchemaValidatingReader(reader) { Schema = jsonSchema };
            JsonSerializer serializer = new JsonSerializer();
            VariableValueParent varParent = serializer.Deserialize<VariableValueParent>(validatingReader);
            Assert.IsNotNull(varParent);
        }

        [TestMethod]
        public void TestDomainEmailSerialization()
        {
            var entity = new DomainEmail()
            {
                BID = 1,
                DomainName = "TEST"
            };
            var serialized = JsonConvert.SerializeObject(entity);
            JToken myObj = JToken.Parse(serialized);

            Assert.AreEqual(8, myObj.Values().Count(), myObj.ToString());

            Assert.IsNull(myObj[nameof(entity.BID)]);
            Assert.IsNotNull(myObj[nameof(entity.ID)]);
            Assert.IsNotNull(myObj[nameof(entity.ClassTypeID)]);
            Assert.IsNotNull(myObj[nameof(entity.CreatedDate)]);
            Assert.IsNotNull(myObj[nameof(entity.ModifiedDT)]);
            Assert.IsNotNull(myObj[nameof(entity.IsActive)]);
            Assert.IsNotNull(myObj[nameof(entity.DomainName)]);
            Assert.IsNotNull(myObj[nameof(entity.ProviderType)]);
            Assert.IsNull(myObj[nameof(entity.SMTPConfigurationType)]);
            Assert.IsNull(myObj[nameof(entity.SMTPAddress)]);
            Assert.IsNull(myObj[nameof(entity.SMTPPort)]);
            Assert.IsNull(myObj[nameof(entity.SMTPSecurityType)]);
            Assert.IsNull(myObj[nameof(entity.SMTPAuthenticateFirst)]);
            Assert.IsNull(myObj[nameof(entity.LastVerificationAttemptDT)]);
            Assert.IsNull(myObj[nameof(entity.LastVerificationSuccess)]);
            Assert.IsNull(myObj[nameof(entity.LastVerificationResult)]);
            Assert.IsNull(myObj[nameof(entity.LastVerifiedDT)]);
            Assert.IsNotNull(myObj[nameof(entity.IsForAllLocations)]);
            Assert.IsTrue(entity.IsForAllLocations);

            Assert.IsNull(myObj[nameof(entity.SimpleLocations)]);
            Assert.IsNull(myObj[nameof(entity.LocationLinks)]);
        }
    }
}
