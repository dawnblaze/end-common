﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Endor.Models;
using Newtonsoft.Json.Linq;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class OrderItemStatusTests
    {
        [TestMethod]
        public void TestJSONProperties()
        {
            var newOrderItemStatus = new OrderItemStatus();
            var json = JsonConvert.SerializeObject(newOrderItemStatus);
            var properties = typeof(OrderItemStatus).GetProperties();
            foreach(var prop in properties)
            {
                //Ignored properties
                if (prop.Name.Equals("BID"))
                {
                    //BID is never serialized
                    Assert.IsFalse(json.Contains($"\"{prop.Name}\""), $"{prop.Name} should not be serialized");
                }
                else if (prop.GetValue(newOrderItemStatus) == null)
                {
                    //Property Value is NULL -- Conditionally Serialized
                    JsonPropertyAttribute jsonAttribute = prop.GetCustomAttributes(typeof(JsonPropertyAttribute), true).Cast<JsonPropertyAttribute>().FirstOrDefault();

                    if (jsonAttribute != null)
                    {
                        //Json Property Attribute Present
                        if (jsonAttribute.NullValueHandling == NullValueHandling.Ignore)
                        {
                            //Attribute set to ignore NULLs
                            Assert.IsFalse(json.Contains($"\"{prop.Name}\""), $"{prop.Name} has NullValueHandling set to Ignore and value is NULL. This should NOT be serialized");
                        }
                        else
                        {
                            //Attribute set to include NULLs
                            Assert.IsTrue(json.Contains($"\"{prop.Name}\""), $"{prop.Name} has NullValueHandling set to Include and value is NULL. This should be serialized");
                        }
                    }
                }
                else
                {//Property Value is NOT NULL, Always Serialized
                    Assert.IsTrue(json.Contains($"\"{prop.Name}\""), $"{prop.Name} should be serialized");
                }
            }
        }
    }
}
