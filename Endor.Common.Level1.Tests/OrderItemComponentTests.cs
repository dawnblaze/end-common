﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class OrderItemComponentTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        private static OrderItemComponent TestOrderItemComponent()
        {
            OrderItemComponent orderItemComponent = new OrderItemComponent()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.OrderNote, //10015
                ModifiedDT = DateTime.Now,
                OrderID = 1,
                OrderItemID = 1,
                Number = 123,
                Description = "Description",
                Name = "Name",
            };

            return orderItemComponent;
        }

        [TestMethod]
        public void OrderItemComponentSerializationTest()
        {
            OrderItemComponent orderItemComponent = TestOrderItemComponent();

            string orderItemComponentSerialized = JsonConvert.SerializeObject(orderItemComponent);
            JToken orderItemComponentJToken = JToken.Parse(orderItemComponentSerialized);

            //Token Count
            Assert.AreEqual(14, orderItemComponentJToken.Values().Count(), orderItemComponentJToken.ToString());

            //Serialized
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.ID)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.ClassTypeID)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.ModifiedDT)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.OrderID)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.OrderItemID)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.Number)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.Description)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.Name)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.CostOV)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.PriceUnitOV)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.AssemblyQuantityOV)]);
            Assert.IsNotNull(orderItemComponentJToken[nameof(orderItemComponent.TotalQuantityOV)]);

            //Not serialized
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.BID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.Assembly)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.AssemblyDataJSON)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.AssemblyID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.AssemblyVariable)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.AssemblyVariableID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.ChildComponents)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.CostNet)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.CostUnit)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.Destination)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.ExpenseAccount)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.ExpenseAccountID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.IncomeAccount)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.IncomeAccountID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.Labor)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.LaborID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.Machine)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.MachineID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.Material)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.MaterialID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.Order)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.OrderItem)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.ParentComponent)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.ParentComponentID)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.PricePreTax)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.PriceUnit)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.TotalQuantity)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.QuantityUnit)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.AssemblyQuantity)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.PricePerItem)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.CostPerItem)]);
            Assert.IsNull(orderItemComponentJToken[nameof(orderItemComponent.PriceComputed)]);
        }
    }
}