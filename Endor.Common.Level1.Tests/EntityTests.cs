﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Endor.Common.Tests
{
    [TestClass]
    public class EntityTests
    {
        [TestMethod]
        public void EmployeeLocatorXMLRoundtripTest()
        {
            PhoneMetaData originalMetaData = new PhoneMetaData()
            {
                AreaCode = "123",
                CountryCode = "1",
                Extension = "3",
                PhoneNumber = "123-1234"
            };
            string soloXML = XMLUtilities.SerializeMetaDataToXML(originalMetaData, LocatorType.Phone);

            EmployeeLocator originalLocator = new EmployeeLocator()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.EmployeeLocator,
                LocatorType = (int)LocatorType.Phone,
                ModifiedDT = DateTime.UtcNow,
                ParentID = 1,
                MetaDataObject = originalMetaData
            };

            //the constructor should have initialized the metadata field
            string testXML = originalLocator.MetaDataXML;
            Assert.IsNotNull(testXML);

            //serialization should be the same
            Assert.AreEqual(testXML, soloXML);

            //blanking metadata should blank metaDataObject
            originalLocator.MetaDataXML = null;
            Assert.IsNull(originalLocator.MetaDataObject);

            //setting the metadata should create a metaDataObject on access
            originalLocator.MetaDataXML = testXML;
            Assert.IsNotNull(originalLocator.MetaDataObject);

            EmployeeLocator xmlFirstLocator = new EmployeeLocator()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.EmployeeLocator,
                LocatorType = (int)LocatorType.Phone,
                ModifiedDT = DateTime.UtcNow,
                ParentID = 1,
                MetaDataXML = soloXML
            };
            Assert.IsNotNull(xmlFirstLocator.MetaDataObject);

            string JSON = JsonConvert.SerializeObject(xmlFirstLocator);
            //the JSON should have our metadata as JSON
            Assert.IsTrue(JSON.Contains(JsonConvert.SerializeObject(originalMetaData)));
            //and should not have a MetaDataObject
            Assert.IsFalse(JSON.Contains("MetaDataObject"));

            EmployeeLocator fromJSON = JsonConvert.DeserializeObject<EmployeeLocator>(JSON);
            //the JSON deserializer should be able to materialize our JSON
            Assert.IsNotNull(fromJSON);
            //and should have MetaData filled in
            Assert.IsFalse(String.IsNullOrWhiteSpace(fromJSON.MetaDataXML));

            //the metadata should have the correct data
            object metaDataObjectFromJSON = fromJSON.MetaDataObject;
            PhoneMetaData phoneMetaDataFromJSON = metaDataObjectFromJSON as PhoneMetaData;
            Assert.IsNotNull(phoneMetaDataFromJSON);
            Assert.AreEqual(originalMetaData.AreaCode, phoneMetaDataFromJSON.AreaCode);
            Assert.AreEqual(originalMetaData.PhoneNumber, phoneMetaDataFromJSON.PhoneNumber);
        }

        [TestMethod]
        public void ListFilterXMLRoundtripTest()
        {
            ListFilterItem item = new ListFilterItem()
            {
                IsHidden = true,
                CategoryID = 1,
                DisplayText = "blah",
                Field = "IsActive",
                Label = "blah",
                SearchValue = "True"
            };
            var items = new ListFilterItem[] { item };
            string soloXML = XMLUtilities.SerializeMetaDataToXML(items, typeof(ListFilterItem[]));

            var listFilter = new SystemListFilter()
            {
                CriteriaObject = items
            };

            string testXML = listFilter.CriteriaXML;
            Assert.IsNotNull(testXML);

            //serialization should be the same
            Assert.AreEqual(testXML, soloXML);

            //blanking metadata should blank metaDataObject
            listFilter.CriteriaXML = null;
            Assert.IsNull(listFilter.CriteriaObject);

            //setting the Criteria should create a CriteriaObject on access
            listFilter.CriteriaXML = testXML;
            Assert.IsNotNull(listFilter.CriteriaObject);


            string JSON = JsonConvert.SerializeObject(listFilter);
            //the JSON should have our Criteria as JSON
            Assert.IsTrue(JSON.Contains(JsonConvert.SerializeObject(items)));
            //and should not have a CriteriaObject
            Assert.IsFalse(JSON.Contains("CriteriaObject"));

            SystemListFilter fromJSON = JsonConvert.DeserializeObject<SystemListFilter>(JSON);
            //the JSON deserializer should be able to materialize our JSON
            Assert.IsNotNull(fromJSON);
            //and should have Criteria filled in
            Assert.IsFalse(String.IsNullOrWhiteSpace(fromJSON.CriteriaXML));

            //the Criteria should have the correct data
            object CriteriaObjectFromJSON = fromJSON.CriteriaObject;
            ListFilterItem[] criteriaFromJSON = CriteriaObjectFromJSON as ListFilterItem[];
            Assert.IsNotNull(criteriaFromJSON);
            Assert.AreEqual(items[0].SearchValue, criteriaFromJSON[0].SearchValue);
        }


        [TestMethod]
        public void SystemAutomationTest()
        {
            Assert.IsTrue(HasAllProperties(typeof(SystemAutomationActionDataTypeLink), new string[]{
                "ActionID", "DataType"
            }));

            Assert.IsTrue(HasAllProperties(typeof(SystemAutomationActionDefinition), new string[]{
                "ID", "Name", "AppliesToAlerts", "AppliesToAutomations", "AppliesToAllDataTypes"
            }));

            Assert.IsTrue(OneToManyMapping(typeof(SystemAutomationActionDataTypeLink), "ActionDefinition", typeof(SystemAutomationActionDefinition), "DataTypeLinks"));

            Assert.IsTrue(HasAllProperties(typeof(EnumAutomationTriggerCategoryType), new string[]{
                "ID", "Name"
            }));

            Assert.IsTrue(HasAllProperties(typeof(SystemAutomationTriggerDefinition), new string[]{
                "ID", "Name", "AppliesToAlerts", "AppliesToAutomations", "TriggerCategoryType", "HasCondition", "ConditionDataType"
            }));
        }

        [TestMethod]
        public void AlertDefinitionTest()
        {
            Assert.IsTrue(HasAllProperties(typeof(AlertDefinition), new string[]{
                "BID","ID", "Name", "ClassTypeID", "ModifiedDT", "IsSystem", "IsActive", "HasImage",
                "EmployeeID", "Description", "DataType", "TriggerID", "TriggerConditionFx", "TriggerReadable",
                "ConditionFx", "ConditionReadable", "ConditionSQL", "LastRunDT", "CummRunCount"
            }));

            Assert.IsTrue(HasAllProperties(typeof(AlertDefinitionAction), new string[]{
                "BID","ID","ClassTypeID", "ModifiedDT",
                "AlertDefinitionID", "ActionDefinitionID", "SortIndex", "DataType", "Subject", "SubjectHasMergeFields",
                "Body", "BodyHasMergeFields", "MetaData"
            }));
            Assert.IsTrue(OneToManyMapping(typeof(AlertDefinitionAction), "AlertDefinition", typeof(AlertDefinition), "Actions"));
        }

        [TestMethod]
        public void TimeCardDetailTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("TimeCardDetail", ((5031))));

            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "BID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "ID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "AdjustedByEmployeeID", typeof(short?), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "AdjustedDT", typeof(DateTime?), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "EmployeeID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "EndDT", typeof(DateTime?), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "IsPaid", typeof(bool), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "LatEnd", typeof(decimal?), "decimal"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "LatStart", typeof(decimal?), "decimal"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "LongEnd", typeof(decimal?), "decimal"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "LongStart", typeof(decimal?), "decimal"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "MetaData", typeof(string), "xml"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "OrderItemID", typeof(int?), "int"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "OrderItemStatusID", typeof(short?), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "SimultaneousDetailCards", typeof(byte) , "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "StartDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "TimeCardID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "TimeClockActivityID", typeof(short?), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "TimeClockBreakID", typeof(short?), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "IsClosed", typeof(bool?), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "PaidTimeInMin", typeof(decimal?), "numeric"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "TimeInMin", typeof(decimal?), "numeric"));
            Assert.IsTrue(PropertyCheck(typeof(TimeCardDetail), "IsAdjusted", typeof(bool), "bit"));
        }

        [TestMethod]
        public void MessageBodyTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("MessageBody", ((14110))));

            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "BID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "ID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "Subject", typeof(string), "varchar"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "BodyFirstLine", typeof(string), "nvarchar"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "HasBody", typeof(bool), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "AttachedFileCount", typeof(byte), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "AttachedFileNames", typeof(string), "varchar"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "HasAttachment", typeof(bool), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "MetaData", typeof(string), "xml"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "WasModified", typeof(bool), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(MessageBody), "SizeInKB", typeof(int?), "smallint"));


            Assert.IsTrue(HasAllProperties(typeof(MessageBody), new string[]{
                "BID","ID", "ClassTypeID", "ModifiedDT", "Subject", "BodyFirstLine", "HasBody",
                "AttachedFileCount", "AttachedFileNames", "HasAttachment", "MetaData", "WasModified", "SizeInKB"
            }
            ));
        }

        [TestMethod]
        public void RightsGroupMenuTreeTest()
        {
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "ID"               , typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "ParentID"         , typeof(short?), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "Module"           , typeof(Models.Module?), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "Name"             , typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "Depth"            , typeof(byte), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "SortIndex"        , typeof(byte?), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "EnabledGroupID"   , typeof(short?), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "IsSharedGroup"    , typeof(bool), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "FullAccessGroupID", typeof(short?), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "Description"      , typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "SearchTerms"      , typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "HintText"         , typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "IsInternal"       , typeof(bool), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupMenuTree), "Children"         , typeof(ICollection<RightsGroupMenuTree>), ""));
        }

        [TestMethod]
        public void RightsGroupTest()
        {
            Assert.IsTrue(PropertyCheck(typeof(RightsGroup), "ID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroup), "Name", typeof(string), ""));
        }

        [TestMethod]
        public void RightsGroupChildLinkTest()
        {
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupChildGroupLink), "ParentID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupChildGroupLink), "ChildID", typeof(short), ""));
        }

        [TestMethod]
        public void RightsGroupRightsLinkTest()
        {
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupRightLink), "GroupID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupRightLink), "RightID", typeof(short), ""));
        }

        [TestMethod]
        public void RightsUserAccessTypeGroupLinkTest()
        {
            Assert.IsTrue(PropertyCheck(typeof(RightsUserAccessTypeGroupLink), "ID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsUserAccessTypeGroupLink), "UserAccessType", typeof(UserAccessType), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsUserAccessTypeGroupLink), "IncludedRightsGroupID", typeof(short), ""));
        }

        [TestMethod]
        public void RightsGroupListTest()
        {
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupList), "ID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupList), "BID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupList), "ClassTypeID", typeof(int), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupList), "Name", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupList), "IsUserSpecific", typeof(bool), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupList), "RightsArray", typeof(string), ""));
        }

        [TestMethod]
        public void ListRightsGroupLinkTest()
        {
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupListRightsGroupLink), "BID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupListRightsGroupLink), "ListID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupListRightsGroupLink), "GroupID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(RightsGroupListRightsGroupLink), "CreatedDT", typeof(DateTime), ""));
        }

        [TestMethod]
        public void ReconciliationTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("Reconciliation", ((8015))));

            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "BID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "ID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "CreatedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "LastAccountingDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "AccountingDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "IsAdjustmentEntry", typeof(bool), "bool"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "LocationID", typeof(byte), "byte"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "GLActivityID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "StartingGLID", typeof(int?), "int?"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "EndingGLID", typeof(int?), "int?"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "EnteredByID", typeof(short?), "int?"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "Description", typeof(string), "string"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "Notes", typeof(string), "string"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "ExportedDT", typeof(DateTime?), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "WasExported", typeof(bool), "bool"));
            // Navigation
            Assert.IsTrue(PropertyCheck(typeof(Reconciliation), "Items", typeof(ICollection<ReconciliationItem>), ""));
        }

        [TestMethod]
        public void ReconciliationItemTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("ReconciliationItem", ((8016))));

            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "BID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "ID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "ReconciliationID", typeof(int), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "GLAccountID", typeof(int?), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "PaymentMethodID", typeof(PaymentMethodType?), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "PaymentMasterCount", typeof(short?), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "PaymentApplicationCount", typeof(short?), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "Amount", typeof(decimal?), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "Balance", typeof(decimal?), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "CurrencyType", typeof(byte?), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "IsPaymentSummary", typeof(bool), ""));
            // Navigation
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "Reconciliation", typeof(Reconciliation), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "GLAccount", typeof(GLAccount), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "PaymentMethodTypeNavigation", typeof(EnumPaymentMethodType), ""));
            Assert.IsTrue(PropertyCheck(typeof(ReconciliationItem), "CurrencyTypeNavigation", typeof(EnumAccountingCurrencyType), ""));
        }


        public static bool OneToManyMapping(Type type1, string oneProperty, Type type2, string manyProperty)
        {
            Type entityType = type1.GetProperty(oneProperty).PropertyType;
            Type collectionType = type2.GetProperty(manyProperty).PropertyType;
            return entityType == type2 && collectionType.IsGenericType && collectionType.GetGenericArguments().Single() == type1;
        }

        public static bool HasAllProperties(Type type, string[] propertyNames)
        {
            var props = type.GetProperties();
            return propertyNames.All(x => props.FirstOrDefault(p => p.Name == x) != null);
        }

        public static bool PropertyCheck(Type t, string propertyName, Type clrPropertyType, string sqlColumnType)
        {
            // Verify that property exists on type
            PropertyInfo pi = t.GetProperty(propertyName);
            Assert.IsNotNull(pi, $"{propertyName} is not found in {t.Name}");

            // Verify that property type matches the expected type
            Assert.AreEqual(clrPropertyType, pi.PropertyType, $"{propertyName} is incorrect type");
                        
            // Verify that get and set methods are available 
            var getMethod = pi.GetGetMethod(false);
            var setMethod = pi.GetSetMethod(false);
            Assert.IsNotNull(getMethod);
            Assert.IsNotNull(setMethod);

            return true;
        }

        public static bool ClassTypeIDCheck(string modelClassName, int classTypeID)
        {
            ClassType? ct = Enum.Parse(typeof(ClassType), modelClassName) as ClassType?;
            Assert.IsNotNull(ct);
            Assert.AreEqual(classTypeID, ct.Value.ID());

            return true;
        }

        public static bool TestSerializationAttributes(string json, object entity, Type entityType)
        {
            var properties = entityType.GetProperties();
            foreach (var prop in properties)
            {
                //Ignored properties
                if (prop.Name.Equals("BID"))
                {
                    //BID is never serialized
                    Assert.IsFalse(json.Contains($"\"{prop.Name}\""), $"{prop.Name} should not be serialized");
                }
                else if (prop.GetValue(entity) == null)
                {
                    //Property Value is NULL -- Conditionally Serialized
                    JsonPropertyAttribute jsonAttribute = prop.GetCustomAttributes(typeof(JsonPropertyAttribute), true).Cast<JsonPropertyAttribute>().FirstOrDefault();

                    if (jsonAttribute != null)
                    {
                        //Json Property Attribute Present
                        if (jsonAttribute.NullValueHandling == NullValueHandling.Ignore)
                        {
                            //Attribute set to ignore NULLs
                            Assert.IsFalse(json.Contains($"\"{prop.Name}\""), $"{prop.Name} has NullValueHandling set to Ignore and value is NULL. This should NOT be serialized");
                        }
                        else
                        {
                            //Attribute set to include NULLs
                            Assert.IsTrue(json.Contains($"\"{prop.Name}\""), $"{prop.Name} has NullValueHandling set to Include and value is NULL. This should be serialized");
                        }
                    }
                    else
                    {
                        //NO Json Property Attribute Present
                        Assert.IsTrue(json.Contains($"\"{prop.Name}\""), $"{prop.Name} has no JsonPropertyAttribute set and value is NULL. This should be serialized");
                    }
                }
                else
                {//Property Value is NOT NULL, Always Serialized
                    Assert.IsTrue(json.Contains($"\"{prop.Name}\""), $"{prop.Name} should be serialized");
                }
            }
            return true;
        }
    }
}
