﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class OrderContactRoleLocatorTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        [TestMethod]
        public void OrderContactRoleLocatorSerializationTest()
        {
            OrderContactRoleLocator locator = new OrderContactRoleLocator()
            {
                Locator = "My Locator",
                RawInput = "Oh yes my locator",
            };

            string serialized = JsonConvert.SerializeObject(locator);
            JToken orderContactRoleLocatorToken = JToken.Parse(serialized);

            Assert.AreEqual(14, orderContactRoleLocatorToken.Values().Count(), orderContactRoleLocatorToken.ToString());
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.BID)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.ID)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.ClassTypeID)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.LocatorType)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.LocatorSubType)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.Locator)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.RawInput)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.SortIndex)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.IsValid)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.IsVerified)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.ParentID)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.eLocatorType)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.HasImage)]);
            Assert.IsNotNull(orderContactRoleLocatorToken[nameof(locator.ModifiedDT)]);

        }
    }
}
