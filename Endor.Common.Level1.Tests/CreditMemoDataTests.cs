﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class CreditMemoDataTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        [TestMethod]
        public void CreditMemoDataSerializationTest()
        {
            CreditMemoData order = new CreditMemoData()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.CreditMemo.ID(),
                ModifiedDT = DateTime.UtcNow,
                TransactionType = (byte)OrderTransactionType.Memo,
                Number = 1000,
                FormattedNumber = "LinkedFormattedNumber",
                Description = "Description",
                LocationID = 1,
                CompanyID = 1,
                OrderStatusID = OrderOrderStatus.CreditMemoPosted,
                OrderStatusStartDT = DateTime.UtcNow,
                PriceIsLocked = false,
                PriceTaxableOV = false,
                PriceTaxRate = 0.1m,
                IsTaxExempt = false,
                PaymentPaid = 100,
                PaymentTotal = 100m,
                CreditMemoHasBalance = false,
                TaxGroupID = 1,
                TaxGroupOV = false,
                CostTotal = 0,
                HasSingleDestination = true,
                DestinationType = OrderDestinationType.Installed,
                ProductionLocationID = 1,
                PickupLocationID = 1,
                HasOrderLinks = false,
                HasDocuments = false,
            };

            string serializedOrder = JsonConvert.SerializeObject(order);
            JToken orderToken = JToken.Parse(serializedOrder);

            Assert.AreEqual(27, orderToken.Values().Count(), orderToken.ToString());
            Assert.IsNull(orderToken["BID"]);
            Assert.IsNotNull(orderToken["ID"]);
            Assert.IsNotNull(orderToken["ClassTypeID"]);
            Assert.IsNotNull(orderToken["ModifiedDT"]);
            Assert.IsNotNull(orderToken["TransactionType"]);
            Assert.IsNotNull(orderToken["Number"]);
            Assert.IsNotNull(orderToken["FormattedNumber"]);
            Assert.IsNotNull(orderToken["Description"]);
            Assert.IsNotNull(orderToken["LocationID"]);
            Assert.IsNotNull(orderToken["CompanyID"]);
            Assert.IsNotNull(orderToken["OrderStatusID"]);
            Assert.IsNotNull(orderToken["OrderStatusStartDT"]);
            Assert.IsNotNull(orderToken["PriceIsLocked"]);
            Assert.IsNotNull(orderToken["PriceTaxableOV"]);
            Assert.IsNotNull(orderToken["PriceTaxRate"]);
            Assert.IsNotNull(orderToken["IsTaxExempt"]);
            Assert.IsNotNull(orderToken["PaymentPaid"]);
            Assert.IsNotNull(orderToken["PaymentTotal"]);
            Assert.IsNotNull(orderToken["CreditMemoHasBalance"]);
            Assert.IsNotNull(orderToken["TaxGroupID"]);
            Assert.IsNotNull(orderToken["TaxGroupOV"]);
            Assert.IsNotNull(orderToken["CostTotal"]);
            Assert.IsNotNull(orderToken["HasSingleDestination"]);
            Assert.IsNotNull(orderToken["DestinationType"]);
            Assert.IsNotNull(orderToken["ProductionLocationID"]);
            Assert.IsNotNull(orderToken["PickupLocationID"]);
            Assert.IsNotNull(orderToken["HasOrderLinks"]);
            Assert.IsNotNull(orderToken["HasDocuments"]);

            order.InvoiceNumber = 100;
            order.OpportunityID = 1;
            order.PriceProductTotal = 1;
            order.PriceDestinationTotal = 1;
            order.PriceFinanceCharge = 1;
            order.PriceNet = 1;
            order.PriceDiscountPercent = 1;
            order.PriceDiscount = 1;
            order.PricePreTax = 1;
            order.PriceTaxable = 1;
            order.PriceTax = 1;
            order.PriceTotal = 1;
            order.PaymentAuthorized = 1;
            order.PaymentWriteOff = 1;
            order.CreditMemoApplied = 1;
            order.PaymentBalanceDue = 1;
            order.CreditMemoCredit = 1;
            order.CreditMemoUsed = 1;
            order.CreditMemoBalance = 1;
            order.CostMaterial = 1;
            order.CostLabor = 1;
            order.CostMachine = 1;
            order.CostOther = 1;
            order.OrderPONumber = "PO1";
            order.OriginID = 1;

            serializedOrder = JsonConvert.SerializeObject(order);
            orderToken = JToken.Parse(serializedOrder);

            Assert.AreEqual(52, orderToken.Values().Count(), orderToken.ToString());
            Assert.IsNotNull(orderToken["PriceProductTotal"]);
            Assert.IsNotNull(orderToken["PriceDestinationTotal"]);
            Assert.IsNotNull(orderToken["PriceFinanceCharge"]);
            Assert.IsNotNull(orderToken["PriceNet"]);
            Assert.IsNotNull(orderToken["PriceDiscountPercent"]);
            Assert.IsNotNull(orderToken["PriceDiscount"]);
            Assert.IsNotNull(orderToken["PricePreTax"]);
            Assert.IsNotNull(orderToken["PriceTaxable"]);
            Assert.IsNotNull(orderToken["PriceTaxableOV"]);
            Assert.IsNotNull(orderToken["PriceTaxRate"]);
            Assert.IsNotNull(orderToken["IsTaxExempt"]);
            Assert.IsNotNull(orderToken["PriceTax"]);
            Assert.IsNotNull(orderToken["PriceTotal"]);
            Assert.IsNotNull(orderToken["PaymentPaid"]);
            Assert.IsNotNull(orderToken["PaymentAuthorized"]);
            Assert.IsNotNull(orderToken["PaymentWriteOff"]);
            Assert.IsNotNull(orderToken["CreditMemoApplied"]);
            Assert.IsNotNull(orderToken["PaymentTotal"]);
            Assert.IsNotNull(orderToken["PaymentBalanceDue"]);
            Assert.IsNotNull(orderToken["CreditMemoCredit"]);
            Assert.IsNotNull(orderToken["CreditMemoUsed"]);
            Assert.IsNotNull(orderToken["CreditMemoBalance"]);
            Assert.IsNotNull(orderToken["CreditMemoHasBalance"]);
            Assert.IsNotNull(orderToken["TaxGroupID"]);
            Assert.IsNotNull(orderToken["TaxGroupOV"]);
            Assert.IsNotNull(orderToken["CostMaterial"]);
            Assert.IsNotNull(orderToken["CostLabor"]);
            Assert.IsNotNull(orderToken["CostMachine"]);
            Assert.IsNotNull(orderToken["CostOther"]);
            Assert.IsNotNull(orderToken["CostTotal"]);
            Assert.IsNotNull(orderToken["OrderPONumber"]);
            Assert.IsNotNull(orderToken["OriginID"]);

            order.Dates = new HashSet<OrderKeyDate>();
            order.Notes = new HashSet<OrderNote>();
            order.Links = new HashSet<OrderOrderLink>();

            serializedOrder = JsonConvert.SerializeObject(order);
            orderToken = JToken.Parse(serializedOrder);

            Assert.AreEqual(55, orderToken.Values().Count(), orderToken.ToString());

            Assert.IsNotNull(orderToken["Dates"]);
            Assert.IsNotNull(orderToken["Notes"]);
            Assert.IsNotNull(orderToken["Links"]);
        }
    }
}
