﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class OrderEmployeeRoleTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        [TestMethod]
        public void OrderEmployeeRoleSerializationTest()
        {
            OrderEmployeeRole orderEmployeeRole = new OrderEmployeeRole()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = ClassType.Order.ID(),
                ModifiedDT = DateTime.UtcNow,
                OrderID = 1,
                EmployeeID = 1,
                IsDestinationRole = false,
                IsOrderItemRole = false,
                IsOrderRole = true,
                RoleID = 1
            };

            string serializedOrderEmployeeRole = JsonConvert.SerializeObject(orderEmployeeRole);
            JToken orderEmployeeRoleToken = JToken.Parse(serializedOrderEmployeeRole);

            Assert.AreEqual(6, orderEmployeeRoleToken.Values().Count(), orderEmployeeRoleToken.ToString());
            Assert.IsNull(orderEmployeeRoleToken["BID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ClassTypeID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ModifiedDT"]);
            Assert.IsNotNull(orderEmployeeRoleToken["OrderID"]);
            Assert.IsNull(orderEmployeeRoleToken["OrderItemID"]);
            Assert.IsNull(orderEmployeeRoleToken["DestinationID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["RoleID"]);
            Assert.IsNull(orderEmployeeRoleToken["EmployeeName"]);
            Assert.IsNull(orderEmployeeRoleToken["IsOrderRole"]);
            Assert.IsNull(orderEmployeeRoleToken["IsOrderItemRole"]);
            Assert.IsNull(orderEmployeeRoleToken["IsDestinationRole"]);
            Assert.IsNull(orderEmployeeRoleToken["Employee"]);

            orderEmployeeRole.EmployeeName = "Bob Jones";
            orderEmployeeRole.Employee = new EmployeeData() { LongName = "Bob Jones" };

            serializedOrderEmployeeRole = JsonConvert.SerializeObject(orderEmployeeRole);
            orderEmployeeRoleToken = JToken.Parse(serializedOrderEmployeeRole);

            Assert.AreEqual(8, orderEmployeeRoleToken.Values().Count(), orderEmployeeRoleToken.ToString());
            Assert.IsNull(orderEmployeeRoleToken["BID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ClassTypeID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["ModifiedDT"]);
            Assert.IsNotNull(orderEmployeeRoleToken["OrderID"]);
            Assert.IsNull(orderEmployeeRoleToken["OrderItemID"]);
            Assert.IsNull(orderEmployeeRoleToken["DestinationID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["RoleID"]);
            Assert.IsNotNull(orderEmployeeRoleToken["EmployeeName"]);
            Assert.IsNull(orderEmployeeRoleToken["IsOrderRole"]);
            Assert.IsNull(orderEmployeeRoleToken["IsOrderItemRole"]);
            Assert.IsNull(orderEmployeeRoleToken["IsDestinationRole"]);
            Assert.IsNotNull(orderEmployeeRoleToken["Employee"]);
        }

        [TestMethod]
        public void OrderEmployeeRoleIsIAtomOfShort()
        {
            Assert.IsTrue(typeof(IAtom<short>).IsAssignableFrom(typeof(EmployeeRole)));
        }
    }
}
