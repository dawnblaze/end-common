﻿using Endor.Models;
using Endor.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using static Endor.Common.Tests.EntityTests;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class SystemAssemblyVariableTests
    {
        [TestMethod]
        public void SystemAssemblyVariableElementVerificationTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("SystemAssemblyVariable", ((12048))));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "ID", typeof(short), "short"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "VariableName", typeof(string), "varchar"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "ElementType", typeof(AssemblyElementType), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "SystemVariableID", typeof(short?), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "DataType", typeof(DataType), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "DefaultValue", typeof(string), "nvarchar"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "IsFormula", typeof(bool), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "IsRequired", typeof(bool), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "UnitID", typeof(Unit?), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "ListDataType", typeof(DataType?), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "ListValuesJSON", typeof(string), "nvarchar"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "AllowCustomValue", typeof(bool?), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "AllowMultiSelect", typeof(bool?), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "GroupOptionsByCategory", typeof(bool?), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "Label", typeof(string), "nvarchar"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "LabelType", typeof(AssemblyLabelType?), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "AltText", typeof(string), "varchar"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "ElementUseCount", typeof(byte), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "Tooltip", typeof(string), "nvarchar"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "AllowDecimals", typeof(bool?), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "DisplayType", typeof(CustomFieldNumberDisplayType?), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(SystemAssemblyVariable), "DecimalPlaces", typeof(byte?), "tinyint"));
        }
    }
}
