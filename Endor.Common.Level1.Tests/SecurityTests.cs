﻿using Endor.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Security.Claims;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class SecurityTests
    {
        public readonly byte testAID = 0;
        public readonly short testBID = 1;

        [TestMethod]
        public void TestAIDUserExtensions()
        {
            var claim = new Claim(ClaimNameConstants.AID, testAID.ToString());
            var claimIdentity = new ClaimsIdentity();
            claimIdentity.AddClaim(claim);
            var principal = new ClaimsPrincipal(claimIdentity);

            Assert.IsNotNull(principal.AID());
            Assert.AreEqual(testAID, principal.AID());
        }

        [TestMethod]
        public void TestBIDUserExtensions()
        {
            var claim = new Claim(ClaimNameConstants.BID, testBID.ToString());
            var claimIdentity = new ClaimsIdentity();
            claimIdentity.AddClaim(claim);
            var principal = new ClaimsPrincipal(claimIdentity);

            Assert.IsNotNull(principal.BID());
            Assert.AreEqual(testBID, principal.BID());
        }

    }
}
