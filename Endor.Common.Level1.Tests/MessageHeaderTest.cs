﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class MessageHeaderTest
    {

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        private static MessageHeader TestMessageHeader()
        {
            MessageHeader msgHeader = new MessageHeader
            {
                BID = 1,
                ID = 999,
                ModifiedDT = DateTime.Now,
                ClassTypeID = (int)ClassType.MessageHeader,
                ReceivedDT = DateTime.Now,
                EmployeeID = 1000,
                ParticipantID = 1000,
                BodyID = 1000,
                IsRead = false,
                ReadDT = DateTime.Now,
                IsDeleted = false,
                DeletedOrExpiredDT = DateTime.Now,
                IsExpired = false,
                Channels = MessageChannelType.Message,
                InSentFolder = false
            };


            return msgHeader;
        }

        [TestMethod]
        public void MessageHeaderSerializationTest()
        {
            MessageHeader msgHeader = TestMessageHeader();

            string msgHeaderSerialized = JsonConvert.SerializeObject(msgHeader);
            JToken msgHeaderJToken = JToken.Parse(msgHeaderSerialized);

            //Token Count
            Assert.AreEqual(14, msgHeaderJToken.Values().Count(), msgHeaderJToken.ToString());

            //Serialized
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.ID)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.ClassTypeID)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.ModifiedDT)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.ReceivedDT)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.EmployeeID)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.ParticipantID)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.BodyID)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.IsRead)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.ReadDT)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.IsDeleted)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.DeletedOrExpiredDT)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.IsExpired)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.Channels)]);
            Assert.IsNotNull(msgHeaderJToken[nameof(msgHeader.InSentFolder)]);

            //Not serialized
            Assert.IsNull(msgHeaderJToken[nameof(msgHeader.BID)]);
        }

    }
}
