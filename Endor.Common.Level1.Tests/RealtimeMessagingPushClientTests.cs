﻿using Endor.RTM;
using Endor.RTM.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class RealtimeMessagingPushClientTests
    {
        [TestMethod]
        public async Task PushRefreshMessageTest()
        {
            var url = GetLocalhostAddress();
            string excludeConnectionId = Guid.NewGuid().ToString();
            RefreshEntity refreshEntity = new RefreshEntity();
            List<RefreshEntity> refreshEntities = new List<RefreshEntity>() { refreshEntity };
            RefreshMessage refreshMessage = new RefreshMessage() { RefreshEntities = refreshEntities };
            HttpClientHandler httpClientHandler = new TestHttpClientHandler(JsonConvert.SerializeObject(refreshMessage));

            IRTMPushClient client = new RealtimeMessagingPushClient(url);
            ((RealtimeMessagingPushClient)client).RTC = new HttpClient(httpClientHandler);
            await client.SendRefreshMessage(refreshMessage);

            httpClientHandler = new TestHttpClientHandler(JsonConvert.SerializeObject(refreshMessage), excludeConnectionId);
            ((RealtimeMessagingPushClient)client).RTC = new HttpClient(httpClientHandler);
            await client.SendRefreshMessage(refreshMessage, excludeConnectionId);
        }

        private string GetLocalhostAddress()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            int port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();

            return $"http://localhost:{port}/";
        }

        public class TestHttpClientHandler : HttpClientHandler
        {
            private string serializedRequestBody;
            private string excludeConnectionId;

            public TestHttpClientHandler(string serializedRequestBody, string excludeConnectionId = null)
            {
                this.serializedRequestBody = serializedRequestBody;
                this.excludeConnectionId = excludeConnectionId;
            }

            protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                string serializedEntity = await request.Content.ReadAsStringAsync();
                Assert.AreEqual(serializedRequestBody, serializedEntity);

                if (!String.IsNullOrWhiteSpace(this.excludeConnectionId))
                {   
                    Assert.AreEqual(excludeConnectionId, HttpUtility.ParseQueryString(request.RequestUri.Query)["excludeConnectionId"]);
                }
                HttpResponseMessage response = new HttpResponseMessage() { StatusCode = HttpStatusCode.OK, Content = new StringContent("") };
                return response;
            }
        }
    }
}