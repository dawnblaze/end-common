﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Serilog.Events;
using Serilog.Parsing;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;
using Endor.Logging.Models;

namespace Endor.Common.Tests
{
    [TestClass]
    public class SerilogHTTPSinkDeserializeTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore};
        }

        [TestMethod]
        public void Roundtrip_HTTP()
        {
            string rawHTTPBody = @"
{
    'Timestamp': '2016-11-03T00:09:11.4899425+01:00',
    'Level': 'Information',
    'MessageTemplate': 'From {MachineName} on {BID}',
    'Properties': {
        'MachineName': 'Workstation',
        'BID': 1
    }
}";
            
            LogEvent logEvent = JsonConvert.DeserializeObject<LogEvent>(rawHTTPBody, new LogEventJsonConverter(), new LogEventPropertyValueConverter());

            Assert.AreEqual(DateTimeOffset.Parse("2016-11-03T00:09:11.4899425+01:00"), logEvent.Timestamp);

            LogEventPropertyValue value;
            logEvent.Properties.TryGetValue("MachineName", out value);
            Assert.AreEqual("Workstation", (value as ScalarValue).Value);
        }

        [TestMethod]
        public void Roundtrip_HTTP_MS()
        {
            string rawHTTPBody = @"
    {
      'Timestamp': '2017-05-23T11:05:50.1541613-05:00',
      'Level': 'Debug',
      'MessageTemplate': 'Executed action method {ActionName}, returned result {ActionResult}.',
      'RenderedMessage': 'Executed action method \""Endor.Logging.Client.LogLevelController.SetFrameworkLogLevel(Endor.Logging.Client)\"", returned result \""Microsoft.AspNetCore.Mvc.OkResult\"".',
      'Properties': {
                'ActionName': 'Endor.Logging.Client.LogLevelController.SetFrameworkLogLevel (Endor.Logging.Client)',
        'ActionResult': 'Microsoft.AspNetCore.Mvc.OkResult',
        'EventID': {
                    'ID': 2
        },
        'SourceContext': 'Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker',
        'ActionID': 'de8664ab-2540-46aa-8956-535b95f83b2f',
        'RequestID': '0HL51UMACQ6JM',
        'RequestPath': '/api/loglevel/framework'
      }
    }
";
            LogEvent logEvent = JsonConvert.DeserializeObject<LogEvent>(rawHTTPBody, new LogEventJsonConverter(), new LogEventPropertyValueConverter());

            Assert.AreEqual(DateTimeOffset.Parse("2017-05-23T11:05:50.1541613-05:00"), logEvent.Timestamp);

            LogEventPropertyValue value;
            logEvent.Properties.TryGetValue("SourceContext", out value);
            Assert.AreEqual("Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker", (value as ScalarValue).Value);
        }

        [TestMethod]
        public void Roundtrip_HTTP_MS_Extended()
        {
            string rawHTTPBody = @"
    [{
      'Timestamp': '2017-05-23T11:56:17.8353874-05:00',
      'Level': 'Information',
      'MessageTemplate': '{HostingRequestStarting:l}',
      'RenderedMessage': 'Request starting HTTP/1.1 OPTIONS https://endorauth.localcyriousdevelopment.com:5001/token  ',
      'Properties': {
                'Protocol': 'HTTP/1.1',
        'Method': 'OPTIONS',
        'ContentType': null,
        'ContentLength': null,
        'Scheme': 'https',
        'Host': 'endorauth.localcyriousdevelopment.com:5001',
        'PathBase': '',
        'Path': '/token',
        'QueryString': '',
        'HostingRequestStarting': 'Request starting HTTP/1.1 OPTIONS https://endorauth.localcyriousdevelopment.com:5001/token  ',
        'EventID': {
                    'ID': 1
        },
        'SourceContext': 'Microsoft.AspNetCore.Hosting.Internal.WebHost',
        'RequestID': '0HL51VE1OPH8A',
        'RequestPath': '/token'
      },
      'Renderings': {
                'HostingRequestStarting': [
                  {
            'Format': 'l',
                    'Rendering': 'Request starting HTTP/1.1 OPTIONS https://endorauth.localcyriousdevelopment.com:5001/token  '
          }
        ]
      }
    },
	{
		'Timestamp': '2017-05-23T12:43:38.6588397-05:00',
		'Level': 'Information',
		'MessageTemplate': 'Executingactionmethod{
			ActionName
		}witharguments({
			Arguments
		})-ModelStateis{
			ValidationState
		}',
		'RenderedMessage': 'Executingactionmethod\""Endor.Logging.Client.LogLevelController.SetFrameworkLogLevel(Endor.Logging.Client)\""witharguments([\""Information\""])-ModelStateisValid',
		'Properties': {
			'ActionName': 'Endor.Logging.Client.LogLevelController.SetFrameworkLogLevel(Endor.Logging.Client)',
			'Arguments': ['Information'],
			'ValidationState': 'Valid',
			'EventID': {
				'ID': 1
			},
			'SourceContext': 'Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker',
			'ActionID': '60efb054-1038-477c-9ff2-0505e74cf845',
			'RequestID': '0HL51VE1OPH8N',
			'RequestPath': '/api/loglevel/framework'
		}
	}]";
            LogEvent[] logEvent = JsonConvert.DeserializeObject<LogEvent[]>(rawHTTPBody, new LogEventJsonConverter(), new LogEventPropertyValueConverter());

            Assert.AreEqual(DateTimeOffset.Parse("2017-05-23T11:56:17.8353874-05:00"), logEvent[0].Timestamp);

            LogEventPropertyValue value;
            logEvent[0].Properties.TryGetValue("SourceContext", out value);
            Assert.AreEqual("Microsoft.AspNetCore.Hosting.Internal.WebHost", (value as ScalarValue).Value);
        }

        [TestMethod]
        public void Roundtrip_Timestamp()
        {
            DateTimeOffset timestamp = DateTimeOffset.Now;

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(timestamp, LogEventLevel.Fatal, null, new MessageTemplateParser().Parse("Test"), new LogEventProperty[0]));

            Assert.AreEqual(timestamp, logEvent.Timestamp);
        }

        [TestMethod]
        public void Roundtrip_Level()
        {
            var level = LogEventLevel.Fatal;

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(DateTimeOffset.Now, level, null, new MessageTemplateParser().Parse("Test"), new LogEventProperty[0]));

            Assert.AreEqual(level, logEvent.Level);
        }

        [TestMethod]
        public void Roundtrip_MessageTemplate_Simple()
        {
            MessageTemplate messageTemplate = new MessageTemplateParser().Parse("Test");

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(DateTimeOffset.Now, LogEventLevel.Fatal, null, messageTemplate, new LogEventProperty[0]));

            Assert.AreEqual(messageTemplate.Text, logEvent.MessageTemplate.Text);
        }

        [TestMethod]
        public void Roundtrip_MessageTemplate_Properties()
        {
            MessageTemplate messageTemplate = new MessageTemplateParser().Parse("Test {key} and {value}");

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(DateTimeOffset.Now, LogEventLevel.Fatal, null, messageTemplate, new LogEventProperty[0]));

            Assert.AreEqual(messageTemplate.Text, logEvent.MessageTemplate.Text);
            Assert.AreEqual(2, messageTemplate.Tokens.OfType<TextToken>().Count());
            Assert.AreEqual(2, messageTemplate.Tokens.OfType<PropertyToken>().Count());
        }

        [TestMethod]
        public void Roundtrip_LogEventProperties_Scalar()
        {
            var logEventProperties = new List<LogEventProperty>();
            logEventProperties.Add(new LogEventProperty("scalar", new ScalarValue(42)));
            logEventProperties.Add(new LogEventProperty("dict", new DictionaryValue(new List<KeyValuePair<ScalarValue, LogEventPropertyValue>> { new KeyValuePair<ScalarValue, LogEventPropertyValue>(new ScalarValue(42), new ScalarValue(43)) })));
            logEventProperties.Add(new LogEventProperty("struct", new StructureValue(new[] { new LogEventProperty("struct-1", new ScalarValue(5)) })));
            logEventProperties.Add(new LogEventProperty("seq", new SequenceValue(new[] { new ScalarValue(37) })));

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(DateTimeOffset.Now, LogEventLevel.Debug, null, new MessageTemplateParser().Parse("Test"), logEventProperties));

            LogEventPropertyValue value;
            Assert.IsTrue(logEvent.Properties.TryGetValue("scalar", out value));

            //Assert.IsAssignableFrom<ScalarValue>(value);
            Assert.AreEqual(42L, ((ScalarValue)value).Value);
        }

        [TestMethod]
        public void Roundtrip_LogEventProperties_Dictionary()
        {
            var logEventProperties = new List<LogEventProperty>();
            logEventProperties.Add(new LogEventProperty("dict", new DictionaryValue(new List<KeyValuePair<ScalarValue, LogEventPropertyValue>> { new KeyValuePair<ScalarValue, LogEventPropertyValue>(new ScalarValue(92), new ScalarValue("value")) })));

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(DateTimeOffset.Now, LogEventLevel.Debug, null, new MessageTemplateParser().Parse("Test"), logEventProperties));

            LogEventPropertyValue value;
            Assert.IsTrue(logEvent.Properties.TryGetValue("dict", out value));

            //Assert.IsAssignableFrom<DictionaryValue>(value);
            Assert.AreEqual("92", ((DictionaryValue)value).Elements.Keys.First().Value);
            Assert.AreEqual("value", ((DictionaryValue)value).Elements.Values.OfType<ScalarValue>().First().Value);
        }

        [TestMethod]
        public void Roundtrip_LogEventProperties_Structure()
        {
            var logEventProperties = new List<LogEventProperty>();
            logEventProperties.Add(new LogEventProperty("struct", new StructureValue(new[] { new LogEventProperty("struct-1", new ScalarValue(5)) }, "typeTag")));

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(DateTimeOffset.Now, LogEventLevel.Debug, null, new MessageTemplateParser().Parse("Test"), logEventProperties));

            LogEventPropertyValue value;
            Assert.IsTrue(logEvent.Properties.TryGetValue("struct", out value));

            //Assert.IsAssignableFrom<StructureValue>(value);

            var structureValue = (StructureValue)value;
            Assert.AreEqual("typeTag", structureValue.TypeTag);

            LogEventProperty logEventProperty = structureValue.Properties.First();
            Assert.AreEqual("struct-1", logEventProperty.Name);
            Assert.AreEqual(5L, ((ScalarValue)logEventProperty.Value).Value);
        }

        [TestMethod]
        public void Roundtrip_LogEventProperties_Structure_NoTypeTag()
        {
            var logEventProperties = new List<LogEventProperty>();
            logEventProperties.Add(new LogEventProperty("struct", new StructureValue(new[] { new LogEventProperty("struct-1", new ScalarValue(5)) })));

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(DateTimeOffset.Now, LogEventLevel.Debug, null, new MessageTemplateParser().Parse("Test"), logEventProperties));

            LogEventPropertyValue value;
            Assert.IsTrue(logEvent.Properties.TryGetValue("struct", out value));

            //Assert.IsAssignableFrom<StructureValue>(value);

            var structureValue = (StructureValue)value;

            LogEventProperty logEventProperty = structureValue.Properties.First();
            Assert.AreEqual("struct-1", logEventProperty.Name);
            Assert.AreEqual(5L, ((ScalarValue)logEventProperty.Value).Value);
        }

        [TestMethod]
        public void Roundtrip_LogEventProperties_Sequence()
        {
            var logEventProperties = new List<LogEventProperty>();
            logEventProperties.Add(new LogEventProperty("seq", new SequenceValue(new[] { new ScalarValue(37) })));

            LogEvent logEvent = RoundtripLogEvent(new LogEvent(DateTimeOffset.Now, LogEventLevel.Debug, null, new MessageTemplateParser().Parse("Test"), logEventProperties));

            LogEventPropertyValue value;
            Assert.IsTrue(logEvent.Properties.TryGetValue("seq", out value));

            //Assert.IsAssignableFrom<SequenceValue>(value);
            Assert.AreEqual(37L, ((SequenceValue)value).Elements.OfType<ScalarValue>().First().Value);
        }

        private LogEvent RoundtripLogEvent(LogEvent logEvent)
        {
            string json = JsonConvert.SerializeObject(logEvent);
            return JsonConvert.DeserializeObject<LogEvent>(json, new LogEventJsonConverter(), new LogEventPropertyValueConverter());
        }
    }
}
