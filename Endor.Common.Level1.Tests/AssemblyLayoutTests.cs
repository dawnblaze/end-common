﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using static Endor.Common.Tests.EntityTests;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class AssemblyLayoutTests
    {
        [TestMethod]
        public void AssemblyLayoutElementVerificationTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("AssemblyLayout", ((12043))));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "BID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "ID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "AssemblyID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "LayoutType", typeof(AssemblyLayoutType), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "Name", typeof(string), "varchar"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "TempLayoutID", typeof(Guid?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyLayout), "Elements", typeof(ICollection<AssemblyElement>), ""));
        }
    }
}
