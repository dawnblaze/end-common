﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class BusinessTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, NullValueHandling = NullValueHandling.Ignore };
        }

        [TestMethod]
        public void BusinessTimeZoneLinkSerializationTest()
        {
            BusinessTimeZoneLink businessTimeZoneLink = new BusinessTimeZoneLink
            {
                BID = 1,
                TimeZoneID = 1
            };

            string serializedBusinessTimeZoneLink = JsonConvert.SerializeObject(businessTimeZoneLink);
            JToken businessTimeZoneLinkToken = JToken.Parse(serializedBusinessTimeZoneLink);
            Assert.AreEqual(1, businessTimeZoneLinkToken.Values().Count(), businessTimeZoneLinkToken.ToString());
            Assert.IsNull(businessTimeZoneLinkToken["BID"]);
            Assert.IsNotNull(businessTimeZoneLinkToken["TimeZoneID"]);
        }
    }
}
