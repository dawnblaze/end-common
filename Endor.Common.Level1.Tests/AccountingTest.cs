﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Endor.Common.Tests.EntityTests;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class AccountingTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void GLActivitySerializationTest()
        {
            var variable = new ActivityGlactivity()
            {
            };

            variable.BID = 1;
            variable.Name = "a";
            variable.Subject = "subject";

            var serialized = JsonConvert.SerializeObject(variable);
            var jToken = JToken.Parse(serialized);
            Assert.AreEqual(12, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken["BID"]);
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["CreatedDT"], default(DateTime));
            Assert.AreEqual(jToken["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken["IsActive"], default(bool));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["Name"], "a");
            Assert.AreEqual((ActivityType)((byte)jToken["ActivityType"]), ActivityType.System);
            Assert.AreEqual(jToken["Subject"], "subject");
            Assert.AreEqual(jToken["StartDT"], default(DateTime));
            Assert.AreEqual(jToken["EndDT"], default(DateTime));
            
            Assert.AreEqual((GLEntryType)((byte)jToken["GLEntryType"]), GLEntryType.Manual_GL_Entry);
        }

        [TestMethod]
        public void GLDataSerializationTest()
        {
            var variable = new GLData()
            {
            };

            variable.BID = 1;
            
            var serialized = JsonConvert.SerializeObject(variable);
            var jToken = JToken.Parse(serialized);
            Assert.AreEqual(9, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken["BID"]);
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.IsNull(jToken["ModifiedDT"]);
            Assert.AreEqual(jToken["AccountingDT"], default(DateTime));
            Assert.AreEqual(jToken["RecordedDT"], default(DateTime));
            Assert.AreEqual(jToken["LocationID"], default(int));
            Assert.AreEqual(jToken["GLAccountID"], default(int));
            Assert.AreEqual((AccountingCurrencyType)((byte)jToken["CurrencyType"]), AccountingCurrencyType.USDollar);
            Assert.AreEqual(jToken["Amount"], default(decimal));
            Assert.AreEqual(jToken["ActivityID"], default(int));

            // Test Nullable Properties with default values
            Assert.IsNull(jToken["Debit"]);
            Assert.IsNull(jToken["Credit"]);
            Assert.IsNull(jToken["IsOffBS"]);
            Assert.IsNull(jToken["IsTaxable"]);
            Assert.IsNull(jToken["TaxGroupID"]);
            Assert.IsNull(jToken["ReconciliationID"]);
            Assert.IsNull(jToken["CompanyID"]);
            Assert.IsNull(jToken["OrderID"]);
            Assert.IsNull(jToken["OrderItemID"]);
            Assert.IsNull(jToken["DestinationID"]);
            Assert.IsNull(jToken["ItemComponentID"]);
            Assert.IsNull(jToken["MaterialID"]);
            Assert.IsNull(jToken["LaborID"]);
            Assert.IsNull(jToken["MachineID"]);
            Assert.IsNull(jToken["AssemblyID"]);
            Assert.IsNull(jToken["PlaceID"]);
            Assert.IsNull(jToken["AssetID"]);
            Assert.IsNull(jToken["TaxItemID"]);
            Assert.IsNull(jToken["PaymentMethodID"]);
            Assert.IsNull(jToken["PaymentID"]);

            // Test Nullable Properties with values
            variable = new GLData()
            {
                Debit = 99,
                Credit = 99,
                IsOffBS = false,
                IsTaxable = false,
                TaxGroupID = 99,
                ActivityID = 99,
                ReconciliationID = 99,
                CompanyID = 99,
                OrderID = 99,
                OrderItemID = 99,
                DestinationID = 99,
                ItemComponentID = 99,
                MaterialID = 99,
                LaborID = 99,
                MachineID = 99,
                AssemblyID = 99,
                PlaceID = 99,
                AssetID = 99,
                TaxItemID = 99,
                PaymentMethodID = 99,
                PaymentID = 99,
            };
            serialized = JsonConvert.SerializeObject(variable);
            jToken = JToken.Parse(serialized);
            Assert.AreEqual(jToken["Debit"], variable.Debit);
            Assert.AreEqual(jToken["Credit"], variable.Credit);
            Assert.IsNull(jToken["IsOffBS"]);
            Assert.IsNull(jToken["IsTaxable"]);
            Assert.AreEqual(jToken["TaxGroupID"], variable.TaxGroupID);
            Assert.AreEqual(jToken["ReconciliationID"], variable.ReconciliationID);
            Assert.AreEqual(jToken["CompanyID"], variable.CompanyID);
            Assert.AreEqual(jToken["OrderID"], variable.OrderID);
            Assert.AreEqual(jToken["OrderItemID"], variable.OrderItemID);
            Assert.AreEqual(jToken["DestinationID"], variable.DestinationID);
            Assert.AreEqual(jToken["ItemComponentID"], variable.ItemComponentID);
            Assert.AreEqual(jToken["MaterialID"], variable.MaterialID);
            Assert.AreEqual(jToken["LaborID"], variable.LaborID);
            Assert.AreEqual(jToken["MachineID"], variable.MachineID);
            Assert.AreEqual(jToken["AssemblyID"], variable.AssemblyID);
            Assert.AreEqual(jToken["PlaceID"], variable.PlaceID);
            Assert.AreEqual(jToken["AssetID"], variable.AssetID);
            Assert.AreEqual(jToken["TaxItemID"], variable.TaxItemID);
            Assert.AreEqual(jToken["PaymentMethodID"], variable.PaymentMethodID);
            Assert.AreEqual(jToken["PaymentID"], variable.PaymentID);

            // test serialization of Properties with non-default values
            variable = new GLData()
            {
                IsOffBS = true,
                IsTaxable = true,
            };
            serialized = JsonConvert.SerializeObject(variable);
            jToken = JToken.Parse(serialized);
            Assert.AreEqual(jToken["IsOffBS"], variable.IsOffBS);
            Assert.AreEqual(jToken["IsTaxable"], variable.IsTaxable);
        }

        [TestMethod]
        public void PaymentSerializationTest()
        {
            var variable = new PaymentMaster()
            {
            };

            variable.BID = 1;

            var serialized = JsonConvert.SerializeObject(variable);
            var jToken = JToken.Parse(serialized);
            Assert.AreEqual(9, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken["BID"]);
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["AccountingDT"], default(DateTime));
            Assert.AreEqual(jToken["Amount"], default(decimal));
            Assert.AreEqual(jToken["RefBalance"], default(decimal));
            Assert.AreEqual(jToken["LocationID"], default(int));
            Assert.AreEqual(jToken["ReceivedLocationID"], default(int));
            Assert.AreEqual(jToken["CompanyID"], default(int));

        }
    }
}
