﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Endor.Common.Tests.EntityTests;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class URLRegistrationTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void URLRegistrationDataSerializationTest()
        {
            var variable = new URLRegistrationData()
            {
            };

            var serialized = JsonConvert.SerializeObject(variable);
            JToken jToken = JToken.Parse(serialized);
            Assert.AreEqual(9, jToken.Values().Count(), jToken.ToString());
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken["CreatedDT"], default(DateTime));
            Assert.AreEqual(jToken["IsActive"], default(bool));
            //Assert.AreEqual(jToken["IsRevoked"], default(bool));
            Assert.AreEqual(jToken["RegistrationType"], default(byte));
            Assert.AreEqual(jToken["UseCount"], default(int));
            Assert.AreEqual(jToken["MaxUseCount"], default(int));
            
            serialized = JsonConvert.SerializeObject(variable);
            jToken = JToken.Parse(serialized);
            Assert.AreEqual(9, jToken.Values().Count(), jToken.ToString());
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken["CreatedDT"], default(DateTime));
            Assert.AreEqual(jToken["IsActive"], default(bool));
            //Assert.AreEqual(jToken["IsRevoked"], default(bool));
            Assert.AreEqual(jToken["RegistrationType"], default(byte));
            Assert.AreEqual(jToken["UseCount"], default(int));
            Assert.AreEqual(jToken["MaxUseCount"], default(int));

        }

        [TestMethod]
        public void URLRegistrationLookupHistorySerializaitonTest()
        {
            var variable = new URLRegistrationLookupHistory()
            {
            };

            var serialized = JsonConvert.SerializeObject(variable);
            JToken jToken = JToken.Parse(serialized);
            Assert.AreEqual(5, jToken.Values().Count(), jToken.ToString());
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken["RegistrationID"], default(int));
            Assert.AreEqual(jToken["WasSuccessful"], default(bool));
            
            serialized = JsonConvert.SerializeObject(variable);
            jToken = JToken.Parse(serialized);
            Assert.AreEqual(5, jToken.Values().Count(), jToken.ToString());
            Assert.AreEqual(jToken["ID"], default(int));
            Assert.AreEqual(jToken["ClassTypeID"], default(int));
            Assert.AreEqual(jToken["ModifiedDT"], default(DateTime));
            Assert.AreEqual(jToken["RegistrationID"], default(int));
            Assert.AreEqual(jToken["WasSuccessful"], default(bool));

        }
    }
}
