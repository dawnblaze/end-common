﻿using Endor.Models;
using Endor.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class UnitsTests
    {
        [TestMethod]
        public void AllInUnits_Test()
        {
            List<string> missingMethods = new List<string>();

            void CheckInUnitExists(string unitName)
            {
                string methodName = $"In{unitName.Replace(" ", "").Replace("/", "Per")}";

                if (typeof(MeasurementExtension).GetMethod(methodName) == null)
                    missingMethods.Add(methodName);
            }

            foreach (UnitInfo unitInfo in UnitInfo.InfoList)
            {
                if (unitInfo.Unit != Unit.None)
                {
                    CheckInUnitExists(unitInfo.NamePluralUS);

                    if (unitInfo.NamePluralUK != unitInfo.NamePluralUS)
                        CheckInUnitExists(unitInfo.NamePluralUK);
                }
            }

            if (missingMethods.Count > 0)
                Assert.Fail($"Missing properties:\r\n{string.Join("\r\n", missingMethods)}");
        }

        [TestMethod]
        public void TestMeasurementProperties()
        {
            static void ValuesAreEqual(decimal? value1, decimal? value2)
            {
                Assert.AreEqual(Round1(value1), Round1(value2));
            }

            static decimal? Round1(decimal? value)
            {
                if (value.HasValue)
                    return decimal.Round(value.Value, 4);

                return null;
            }

            decimal oneMeter = 1;
            decimal oneMeterToInch = 39.3701M;
            decimal oneMeterToFeet = 3.2808M;
            decimal oneMeterToYard = 1.0936M;

            Measurement meterMeasurement = new Measurement(oneMeter, Unit.Meter);

            ValuesAreEqual(oneMeter, meterMeasurement.Value);
            ValuesAreEqual(oneMeter * 100M, meterMeasurement.InCM());
            ValuesAreEqual(oneMeter * 1000M, meterMeasurement.InMM());
            ValuesAreEqual(oneMeterToInch, meterMeasurement.InInches());
            ValuesAreEqual(oneMeterToFeet, meterMeasurement.InFeet());
            ValuesAreEqual(oneMeterToYard, meterMeasurement.InYards());

            ValuesAreEqual(meterMeasurement.ConvertTo(Unit.Centimeter), oneMeter * 100);

            decimal oneInch = 1;
            decimal oneInchToFeet = 0.0833M;
            decimal oneInchToYard = 0.0278M;
            decimal oneInchToCentimeter = 2.54M;
            decimal oneInchToMeter = 0.0254M;
            decimal oneInchToMillimeter = 25.4M;

            Measurement inchMeasurement = new Measurement(oneInch, Unit.Inch);

            ValuesAreEqual(inchMeasurement.Value, oneInch);
            ValuesAreEqual(oneInchToFeet, inchMeasurement.InFeet());
            ValuesAreEqual(oneInchToYard, inchMeasurement.InYards());
            ValuesAreEqual(oneInchToCentimeter, inchMeasurement.InCM());
            ValuesAreEqual(oneInchToMeter, inchMeasurement.InMeters());
            ValuesAreEqual(oneInchToMillimeter, inchMeasurement.InMM());

            ValuesAreEqual(oneInchToFeet, inchMeasurement.ConvertTo(Unit.Foot));

            decimal oneSquareMeter = 1;
            decimal oneSquareMeterToCM = 10000M;
            decimal oneSquareMeterToMM = 1000000M;
            decimal oneSquareMeterToInches = 1550.0031M;
            decimal oneSquareMeterToFeet = 10.7639M;
            decimal oneSquareMeterToYards = 1.1960M;

            Measurement sqmMeasurement = new Measurement(oneSquareMeter, Unit.SquareMeter);

            ValuesAreEqual(oneSquareMeter, sqmMeasurement.Value);
            ValuesAreEqual(oneSquareMeterToCM, sqmMeasurement.InSqCM());
            ValuesAreEqual(oneSquareMeterToMM, sqmMeasurement.InSqMM());
            ValuesAreEqual(oneSquareMeterToInches, sqmMeasurement.InSqInches());
            ValuesAreEqual(oneSquareMeterToFeet, sqmMeasurement.InSqFeet());
            ValuesAreEqual(oneSquareMeterToYards, sqmMeasurement.InSqYards());

            ValuesAreEqual(oneSquareMeterToCM, sqmMeasurement.ConvertTo(Unit.SquareCentimeter));

            decimal oneCubicMeter = 1;
            decimal oneCubicMeterToCM = 1000000M;
            decimal oneCubicMeterToMM = 1000000000M;
            decimal oneCubicMeterToInches = 61023.7441M;
            decimal oneCubicMeterToFeet = 35.3147M;
            decimal oneCubicMeterToYards = 1.3080M;

            Measurement cubicMeterMeasurement = new Measurement(oneCubicMeter, Unit.CubicMeter);

            ValuesAreEqual(oneCubicMeter, cubicMeterMeasurement.Value);
            ValuesAreEqual(oneCubicMeterToCM, cubicMeterMeasurement.InCuCM());
            ValuesAreEqual(oneCubicMeterToMM, cubicMeterMeasurement.InCuMM());
            ValuesAreEqual(oneCubicMeterToInches, cubicMeterMeasurement.InCuInches());
            ValuesAreEqual(oneCubicMeterToFeet, cubicMeterMeasurement.InCuFeet());
            ValuesAreEqual(oneCubicMeterToYards, cubicMeterMeasurement.InCuYards());

            ValuesAreEqual(oneCubicMeterToCM, cubicMeterMeasurement.ConvertTo(Unit.CubicCentimeter));

            decimal oneCubicInch = 1;
            decimal oneCubicInchToFeet = 0.0006M;

            Measurement cubicInchMeasurement = new Measurement(oneCubicInch, Unit.CubicInch);

            ValuesAreEqual(oneCubicInch, cubicInchMeasurement.Value);
            ValuesAreEqual(oneCubicInchToFeet, cubicInchMeasurement.InCuFeet());
            ValuesAreEqual(oneCubicMeterToInches, cubicMeterMeasurement.InCuInches());
            ValuesAreEqual(oneCubicMeterToFeet, cubicMeterMeasurement.InCuFeet());
            ValuesAreEqual(oneCubicMeterToYards, cubicMeterMeasurement.InCuYards());

            ValuesAreEqual(oneCubicMeterToCM, cubicMeterMeasurement.ConvertTo(Unit.CubicCentimeter));

            //test hours
            decimal hours = 2.0M;
            Measurement myHourMeasurement = new Measurement(hours, Unit.Hour);
            ValuesAreEqual(120, myHourMeasurement.InMinutes());
            ValuesAreEqual(7200, myHourMeasurement.InSeconds());

            //test minutes
            decimal minutes = 60.0M;
            Measurement myMinutesMeasurement = new Measurement(minutes, Unit.Minute);
            ValuesAreEqual(1, myMinutesMeasurement.InHours());
            ValuesAreEqual(3600, myMinutesMeasurement.InSeconds());

            //test seconds
            decimal seconds = 7200.0M;
            Measurement mySecondsMeasurement = new Measurement(seconds, Unit.Second);
            ValuesAreEqual(2, mySecondsMeasurement.InHours());
            ValuesAreEqual(120, mySecondsMeasurement.InMinutes());
        }

        [TestMethod]
        public void MeasurementComparisonTest()
        {
            Measurement feetMeasurement = new Measurement(2, Unit.Foot);
            Measurement inchMeasurement = new Measurement(20, Unit.Inch);
            Measurement poundMeasurement = new Measurement(5, Unit.Pound);

            Assert.AreEqual(1, feetMeasurement.CompareTo(inchMeasurement));
            Assert.ThrowsException<UnitConversionException>(() => feetMeasurement.CompareTo(poundMeasurement));
        }
    }
}
