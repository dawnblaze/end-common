﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class OrderNoteTests
    {

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        private static OrderNote TestOrderNote()
        {
            OrderNote orderNote = new OrderNote()
            {
                BID = 1,
                ID = 1,
                ClassTypeID = (int)ClassType.OrderNote, //10015
                ModifiedDT = DateTime.Now,
                OrderID = 1,
                OrderItemID = null,
                DestinationID = null,
                NoteType = OrderNoteType.Sales,
                Note = "My Note",
                EmployeeID = 1,
                IsOrderNote = true,
                IsOrderItemNote = false,
                IsDestinationNote = false,
            };

            return orderNote;
        }

        [TestMethod]
        public void OrderNoteSerializationTest()
        {
            OrderNote orderNote = TestOrderNote();

            string orderNoteSerialized = JsonConvert.SerializeObject(orderNote);
            JToken orderNoteJToken = JToken.Parse(orderNoteSerialized);

            //Token Count
            Assert.AreEqual(7, orderNoteJToken.Values().Count(), orderNoteJToken.ToString());

            //Serialized
            Assert.IsNotNull(orderNoteJToken[nameof(orderNote.ID)]);
            Assert.IsNotNull(orderNoteJToken[nameof(orderNote.OrderID)]);
            Assert.IsNotNull(orderNoteJToken[nameof(orderNote.NoteType)]);
            Assert.IsNotNull(orderNoteJToken[nameof(orderNote.Note)]);
            Assert.IsNotNull(orderNoteJToken[nameof(orderNote.EmployeeID)]);

            //Not serialized
            Assert.IsNull(orderNoteJToken[nameof(orderNote.BID)]);
            Assert.IsNull(orderNoteJToken[nameof(orderNote.ClassTypeID)]);
            Assert.IsNull(orderNoteJToken[nameof(orderNote.ModifiedDT)]);
            Assert.IsNull(orderNoteJToken[nameof(orderNote.OrderItemID)]);
            Assert.IsNull(orderNoteJToken[nameof(orderNote.DestinationID)]);
            Assert.IsNull(orderNoteJToken[nameof(orderNote.IsOrderNote)]);
            Assert.IsNull(orderNoteJToken[nameof(orderNote.IsOrderItemNote)]);
            Assert.IsNull(orderNoteJToken[nameof(orderNote.IsDestinationNote)]);
        }
    }
}