﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using static Endor.Common.Tests.EntityTests;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class UserDraftTest
    {

        [TestMethod]
        public void UserDraftElementVerificationTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("UserDraft", ((1050))));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "BID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "Description", typeof(string), "nvarchar"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "ExpirationDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "ID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "IsExpired", typeof(bool?), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "MetaData", typeof(string), "xml"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "ObjectCTID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "ObjectID", typeof(int?), "int"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "UserLinkID", typeof(short), "short"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "VersionBasedOn", typeof(string), "varchar"));
            Assert.IsTrue(PropertyCheck(typeof(UserDraft), "ObjectJSON", typeof(string), "varchar"));
        }
    }
}
