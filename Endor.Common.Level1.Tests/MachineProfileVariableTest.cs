﻿using Endor.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;


namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class MachineProfileVariableTest
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void BasicMachineProfileVariableSerializationTest()
        {
            MachineProfileVariable machineProfileVariable = new MachineProfileVariable();


            var serializedMachineProfileVariable = JsonConvert.SerializeObject(machineProfileVariable);
            JToken jToken = JToken.Parse(serializedMachineProfileVariable);

            Assert.IsNull(jToken["BID"]);
            Assert.AreEqual(default(short), jToken["ID"]);
            Assert.AreEqual(default(int), jToken["ClassTypeID"]);
            Assert.AreEqual(default(DateTime), jToken["ModifiedDT"]);
            Assert.AreEqual(default(int), jToken["ProfileID"]);
            Assert.AreEqual(default(int), jToken["VariableID"]);
            Assert.AreEqual(default(bool), jToken["OverrideDefault"]);
            Assert.IsNull(jToken["Name"]);
            Assert.IsNull(jToken["Label"]);
            Assert.IsNull(jToken["Tooltip"]);
            Assert.IsNull(jToken["DefaultValue"]);
            Assert.IsNull(jToken["IsRequired"]);
            Assert.IsNull(jToken["IsDisabled"]);
            Assert.AreEqual(default(int), jToken["AssemblyID"]);
            Assert.IsNull(jToken["ListValuesJSON"]);
            Assert.IsNull(jToken["UnitID"]);
            Assert.IsNull(jToken["GroupOptionByCategory"]);
        }
    }
}
