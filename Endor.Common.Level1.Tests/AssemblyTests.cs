﻿using Endor.Models;
using Endor.Units;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Endor.Common.Tests.EntityTests;

namespace Endor.Common.Level1.Tests
{
    [TestClass]
    public class AssemblyTests
    {
        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        [TestMethod]
        public void AssemblyVariableSerializationTest()
        {
            var variable = new AssemblyVariable()
            {
            };

            var serialized = JsonConvert.SerializeObject(variable);
            JToken jToken = JToken.Parse(serialized);
            Assert.AreEqual(14, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(variable.BID)]);
            Assert.AreEqual(jToken[nameof(variable.ID)], default(int));
            Assert.IsNull(jToken[nameof(variable.Name)]);
            Assert.AreEqual(jToken[nameof(variable.IsFormula)],default(bool));
            Assert.AreEqual(jToken[nameof(variable.IsRequired)], default(bool));
            Assert.AreEqual(jToken[nameof(variable.IsDisabled)], default(bool));
            Assert.AreEqual((DataType)((byte)jToken[nameof(variable.DataType)]), DataType.None);
            Assert.AreEqual(jToken[nameof(variable.ModifiedDT)], default(DateTime));
            Assert.AreEqual(jToken[nameof(variable.ClassTypeID)], default(int));
            Assert.AreEqual(jToken[nameof(variable.AssemblyID)], default(int));
            Assert.AreEqual(jToken[nameof(variable.ElementUseCount)], default(int));
            Assert.AreEqual(jToken[nameof(variable.ElementType)], default(int));
            Assert.AreEqual(jToken[nameof(variable.IsConsumptionFormula)], default(bool));
            Assert.AreEqual(jToken[nameof(variable.IsAltTextFormula)], default(bool));
            Assert.AreEqual(jToken[nameof(variable.EnableProfileOV)], default(bool));

            variable.BID = 1;
            variable.Name = "a";

            serialized = JsonConvert.SerializeObject(variable);
            jToken = JToken.Parse(serialized);
            Assert.AreEqual(15, jToken.Values().Count(), jToken.ToString());
            Assert.IsNull(jToken[nameof(variable.BID)]);
            Assert.AreEqual(jToken[nameof(variable.ID)], default(int));
            Assert.AreEqual(jToken[nameof(variable.Name)], "a");
            Assert.AreEqual(jToken[nameof(variable.IsFormula)], default(bool));
            Assert.AreEqual(jToken[nameof(variable.IsRequired)], default(bool));
            Assert.AreEqual(jToken[nameof(variable.IsDisabled)], default(bool));
            Assert.AreEqual((DataType)((byte)jToken[nameof(variable.DataType)]), DataType.None);
            Assert.AreEqual(jToken[nameof(variable.ModifiedDT)], default(DateTime));
            Assert.AreEqual(jToken[nameof(variable.ClassTypeID)], default(int));
            Assert.AreEqual(jToken[nameof(variable.AssemblyID)], default(int));
            Assert.AreEqual(jToken[nameof(variable.ElementUseCount)], default(int));
            Assert.AreEqual(jToken[nameof(variable.ElementType)], default(int));
            Assert.AreEqual(jToken[nameof(variable.IsConsumptionFormula)], default(bool));
            Assert.AreEqual(jToken[nameof(variable.IsAltTextFormula)], default(bool));
            Assert.AreEqual(jToken[nameof(variable.EnableProfileOV)], default(bool));
        }

        [TestMethod]
        public void AssemblyVariableVerificationTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("AssemblyVariable", ((12046))));

            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "BID", typeof(short), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "ClassTypeID", typeof(int), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "ID", typeof(int), ""));

            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "ModifiedDT", typeof(DateTime), ""));

            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "Name", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "AssemblyID", typeof(int), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "DataType", typeof(DataType), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "ElementType", typeof(AssemblyElementType), ""));

            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "IsRequired", typeof(bool), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "IsDisabled", typeof(bool), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "SystemVariableID", typeof(short?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "DefaultValue", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "IsFormula", typeof(bool), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "UnitID", typeof(Unit?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "ListDataType", typeof(DataType?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "ListValuesJSON", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "AllowCustomValue", typeof(bool?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "AllowMultiSelect", typeof(bool?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "GroupOptionsByCategory", typeof(bool?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "Label", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "LabelType", typeof(AssemblyLabelType?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "AltText", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "ElementUseCount", typeof(byte), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "Tooltip", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "AllowDecimals", typeof(bool?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "DisplayType", typeof(CustomFieldNumberDisplayType?), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariable), "DecimalPlaces", typeof(byte?), ""));
        }

        [TestMethod]
        public void AssemblyElementVerificationTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("AssemblyElement", ((12044))));

            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "BID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "ID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "LayoutID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "ParentID", typeof(int?), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "ElementType", typeof(AssemblyElementType), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "VariableID", typeof(int?), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "VariableName", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "Column", typeof(byte), "byte"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "Row", typeof(byte), "byte"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "ColumnsWide", typeof(byte), "byte"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "DataType", typeof(DataType), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "IsReadOnly", typeof(bool), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "IsDisabled", typeof(bool), "bit"));                     
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "Tooltip", typeof(string), "nvarchar"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "AssemblyID", typeof(int), "int"));                       
            Assert.IsTrue(PropertyCheck(typeof(AssemblyElement), "Elements", typeof(ICollection<AssemblyElement>), ""));
        }

        [TestMethod]
        public void AssemblyVariableFormulaTest()
        {
            Assert.IsTrue(ClassTypeIDCheck("AssemblyVariableFormula", ((12052))));

            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "BID", typeof(short), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "ID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "ClassTypeID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "ModifiedDT", typeof(DateTime), "datetime2"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "VariableID", typeof(int), "int"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "FormulaUseType", typeof(AssemblyFormulaUseType), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "FormulaText", typeof(string), ""));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "DataType", typeof(DataType), "smallint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "FormulaEvalType", typeof(AssemblyFormulaEvalType), "tinyint"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "IsFormula", typeof(bool), "bit"));
            Assert.IsTrue(PropertyCheck(typeof(AssemblyVariableFormula), "ChildVariableName", typeof(string), ""));
        }
    }
}
