﻿using Endor.Tenant;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using System.IO;

namespace Endor.EF.StartupEmulator
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public class ApiContextFactory : IDesignTimeDbContextFactory<ApiContext>
    {
        private const string migrationOptionsFileName = "MigrationOptions.json";
        public ApiContext CreateDbContext(string[] args)
        {
            if (args == null || args.Length < 1)
            {
                while (!File.Exists(migrationOptionsFileName))
                {
                    Console.WriteLine("Please type in a connection string for your BusinessDB");
                    string inputString = Console.ReadLine();
                    System.IO.File.WriteAllText(migrationOptionsFileName,
$@"{{
  ""ConnectionStrings"": {{
    ""Business"": ""{inputString.Replace("\\","\\\\").Replace("\"","\\\"")}""
  }}
}}");
                }
                    IConfigurationBuilder configurationBuilder = new ConfigurationBuilder()
                        .SetBasePath(Environment.CurrentDirectory)
                        .AddJsonFile(migrationOptionsFileName);
                    IConfigurationRoot config = configurationBuilder.Build();
                    args = new string[] { config.GetConnectionString("Business") };
                
            }

            if (args == null || args.Length < 1)
            {
                throw new ArgumentNullException(nameof(args), "No arguments passed, the first argument must be the connectionString for the database");
            }

            string connString = args[0];
            MockTenantDataCache.ConnString = connString;
            var optionsBuilder = new DbContextOptionsBuilder<ApiContext>();
            Console.WriteLine($"using connection string of: {connString}");
            optionsBuilder.UseSqlServer<ApiContext>(connString);

            return new ApiContext(optionsBuilder.Options, new MockTenantDataCache(), 1);
        }
    }

    public class MockTenantDataCache : ITenantDataCache
    {
        public static string ConnString;
        public MockTenantDataCache()
        {
        }

        private static TenantData Mock = new TenantData()
        {
            APIURL = "https://endorapi.localcyriousdevelopment.com:5002/",
            ReportingURL = "",
            MessagingURL = "https://endormessaging.localcyriousdevelopment.com:5004/",
            LoggingURL = "https://endorlog.localcyriousdevelopment.com:5003",
            BackgroundEngineURL = "https://endorbgengine.localcyriousdevelopment.com:5006/",
            StorageURL = "http://127.0.0.1:10000/devstoreaccount1/",
            StorageConnectionString = "UseDevelopmentStorage=true",
            IndexStorageConnectionString = "UseDevelopmentStorage=true",
            BusinessDBConnectionString = ConnString,
            LoggingDBConnectionString = "",
            MessagingDBConnectionString = "",
            SystemReportBConnectionString = "",
            SystemDataDBConnectionString = "",
        };

        public Task<TenantData> Get(short bid)
        {
            if (bid == 1)
            {

                TenantData td = Mock;
                td.BusinessDBConnectionString = ConnString;
                return Task.FromResult(td);
            }
            else
                return null;
        }

        public void InvalidateCache()
        {
        }

        public void InvalidateCache(short bid)
        {
        }
    }

}
