using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Endor.Tenant.Cache.Controller.Net462")]
[assembly: AssemblyDescription("Endor.Tenant.Cache.Controller.Net462")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Atomology")]
[assembly: AssemblyProduct("Endor.Tenant.Cache.Controller.Net462")]
[assembly: AssemblyCopyright("Copyright �  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7a026bf5-8374-437a-90f2-d956f2662013")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.9.57.0")]
[assembly: AssemblyFileVersion("0.9.57.0")]