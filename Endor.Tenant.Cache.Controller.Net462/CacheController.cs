﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Endor.Tenant.Cache
{
    public class CacheController : ApiController
    {
        private static ITenantDataCache _cacheObject;
        private static Func<ITenantDataCache> _cacheGetter;

        private static ITenantDataCache Cache
        {
            get
            {
                if (_cacheObject == null)
                {
                    if (_cacheGetter != null)
                        _cacheObject = _cacheGetter();
                }

                return _cacheObject;
            }
        }
        public static void SetCache(ITenantDataCache cache)
        {
            _cacheObject = cache;
        }
        public static void SetCacheEvent(Func<ITenantDataCache> cacheGetter)
        {
            _cacheGetter = cacheGetter;
        }

        [HttpPost]
        [ActionName("Invalidate")]
        public HttpResponseMessage InvalidateCache([FromUri] short? bid = null)
        {
            if (bid.HasValue)
                Cache.InvalidateCache(bid.Value);
            else
                Cache.InvalidateCache();
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
