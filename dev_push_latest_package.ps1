﻿param (
    [Parameter(Mandatory=$true)][string]$project,
    [Parameter(Mandatory=$true)][string]$mygetkey,
    [string]$build = "Debug",
    [Parameter()][string]$exactMatchPackageVersion = ''
)

$projectFull = (Get-Item -Path ".\" -Verbose).FullName + '\' + $project + '\bin\' + $build

Write-Host "Searching for packages in $projectFull"

if ([IO.Directory]::Exists($projectFull))
{

}
else
{
	Write-Host "No folder named $project found" -BackgroundColor Red -ForegroundColor White
	exit	
}

$files = @(Get-ChildItem -Path $projectFull -Exclude *.symbols.nupkg | Where-Object { 
!$_.PsIsContainer -and ($_.Extension -eq ".nupkg")
} | Sort-Object { $_.CreationTime })

if ($files.length -eq 0) {
	Write-Host "Found no packages" -BackgroundColor Red -ForegroundColor White
	exit
}

$candidate = $files[-1].Name
$candidatePath = $files[-1].FullName

Write-Host "The most recently created package is $candidate"

$yn = '&no'

if ($exactMatchPackageVersion -eq '') {
  $yn = & .\_scripts\_readChoice 'Publish Confirmation' '&yes','&no' '&no' ('Do you want to publish '+$candidate+'?')
} else {
	if ($candidate -eq $exactMatchPackageVersion) {
		$yn = '&yes'
	} else {
		Write-Host "Aborting publish because latest package $candidate does not match $exactMatchPackageVersion" -BackgroundColor Red -ForegroundColor White
		exit
	}
}


switch($yn) 
{
	'&yes' 
	{
		& .\dev_push_package $candidatePath $mygetkey
	}
	'&no'
	{
		Write-Host "Aborting publish" -BackgroundColor Red -ForegroundColor White
		exit
	}
}
