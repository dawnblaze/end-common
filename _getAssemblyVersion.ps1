param (
    [Parameter(Mandatory=$true)][string]$csprojPath
)

#Read csproj (XML)
$xml = [xml](Get-Content $csprojPath)

$propertyGroup = $xml.Project.PropertyGroup
if ($propertyGroup -is [array]) {
  $propertyGroup = $xml.Project.PropertyGroup[0]
} 

return $propertyGroup.Version