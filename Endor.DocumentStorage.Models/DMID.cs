﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.DocumentStorage.Models
{
    public class DMID : IDMID
    {
        public int? id { get; set; }
        public int? ctid { get; set; }
        public Guid? guid { get; set; }
        public string classFolder { get; set; }
        public bool IsTemporary
        {
            get { return guid.HasValue; }
        }
        public bool IsEntityLevel
        {
            get { return this.ctid.HasValue && String.IsNullOrWhiteSpace(this.classFolder) && this.id.HasValue; }
        }
        public bool IsClassTypeLevel
        {
            get { return this.ctid.HasValue && !IsEntityLevel; }
        }
        public override string ToString()
        {
            if (IsTemporary)
                return $"{this.guid.Value}";
            else if (this.IsClassTypeLevel)
            {
                return $"{this.ctid.Value}/{classFolder}";
            }
            else if (this.IsEntityLevel)
            {
                return $"{ctid.Value}/{id.Value}";
            }
            else
            {
                return "-1"; //if you see this it's invalid
            }
        }
    }
}
