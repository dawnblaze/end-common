﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.DocumentStorage.Models
{
    public enum StorageBin { Permanent, Temp, Trash }
    public enum Bucket { Documents, Data, Reports }
}
