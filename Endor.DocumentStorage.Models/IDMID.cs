﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.DocumentStorage.Models
{
    public interface IDMID
    {
        int? id { get; set; }
        int? ctid { get; set; }
        Guid? guid { get; set; }
        string classFolder { get; set; }
    }    
}
