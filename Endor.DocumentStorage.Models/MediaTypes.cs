﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.DocumentStorage.Models
{
    public class MediaType
    {
        public string MimeType { get; private set; }
        public int ID { get; private set; }
        public AssetType AssetType { get; private set; }
        public int SortIndex { get; private set; }
        public bool IsPartialMatchOK { get; private set; }
        public string DefaultExtensions { get; private set; }
        public string FontClass { get; private set; }

        public MediaType(string mimeType, int id, AssetType assetType, int sortIndex, bool isPartialMatchOK, string defaultExtensions, string fontClass)
        {
            this.MimeType = mimeType;
            this.ID = id;
            this.AssetType = assetType;
            this.SortIndex = sortIndex;
            this.IsPartialMatchOK = isPartialMatchOK;
            this.DefaultExtensions = defaultExtensions;
            this.FontClass = fontClass;
        }
    }
    public static class MediaTypes
    {
        public static MediaType text_plain = new MediaType(Constants.text_plain, 1, AssetTypes.Text, 104, false, "txt;log", "");
        public static MediaType text_csv = new MediaType(Constants.text_csv, 11, AssetTypes.Text, 102, false, "csv", "");
        public static MediaType application_pdf = new MediaType(Constants.application_pdf, 2, AssetTypes.Document, 208, false, "pdf", "fa fa-file-pdf-o");
        public static MediaType application_vnd_ms_excel = new MediaType(Constants.application_vnd_ms_excel, 3, AssetTypes.Document, 204, false, "xls", "fa fa-file-excel-o");
        public static MediaType application_vnd_ms_powerpoint = new MediaType(Constants.application_vnd_ms_powerpoint, 4, AssetTypes.Document, 212, false, "ppt", "fa fa-file-powerpoint-o");
        public static MediaType application_vnd_ms_word = new MediaType(Constants.application_vnd_ms_word, 5, AssetTypes.Document, 220, false, "doc", "fa fa-file-word-o");
        public static MediaType application_vnd_opemxmlformats_officedocument_spreadsheetml_sheet = new MediaType(Constants.application_vnd_opemxmlformats_officedocument_spreadsheetml_sheet, 6, AssetTypes.Document, 202, false, "xlsx", "fa fa-file-excel-o");
        public static MediaType application_vnd_opemxmlformats_officedocument_wordprocessingml_document = new MediaType(Constants.application_vnd_opemxmlformats_officedocument_wordprocessingml_document, 7, AssetTypes.Document, 218, false, "docx", "fa fa-file-word-o");
        public static MediaType application_vnd_opemxmlformats_officedocument_spreadsheetml_presentation = new MediaType(Constants.application_vnd_opemxmlformats_officedocument_spreadsheetml_presentation, 8, AssetTypes.Document, 210, false, "pptx", "fa fa-file-powerpoint-o");
        public static MediaType application_vnd_ = new MediaType(Constants.application_vnd_, 9, AssetTypes.Document, 250, true, "", "");
        public static MediaType application_x_latext = new MediaType(Constants.application_x_latext, 10, AssetTypes.Document, 206, false, "", "");
        public static MediaType text_vcard = new MediaType(Constants.text_vcard, 12, AssetTypes.Document, 216, false, "vcard", "fa fa-vcard-o");
        public static MediaType application_x_cyr_unidentified_document = new MediaType(Constants.application_x_cyr_unidentified_document, 53, AssetTypes.Document, 214, false, "", "");
        public static MediaType image_gif = new MediaType(Constants.image_gif, 17, AssetTypes.Image, 308, false, "gif", "");
        public static MediaType image_jpeg = new MediaType(Constants.image_jpeg, 18, AssetTypes.Image, 310, false, "jpg;jpeg", "");
        public static MediaType image_png = new MediaType(Constants.image_png, 20, AssetTypes.Image, 312, false, "png", "");
        public static MediaType image_svg_xml = new MediaType(Constants.image_svg_xml, 21, AssetTypes.Image, 316, false, "svg", "");
        public static MediaType image_tiff = new MediaType(Constants.image_tiff, 22, AssetTypes.Image, 318, false, "tif;tiff", "");
        public static MediaType image_ = new MediaType(Constants.image_, 23, AssetTypes.Image, 352, true, "bmp;cdr;ai;", "");
        public static MediaType application_postscript = new MediaType(Constants.application_postscript, 24, AssetTypes.Image, 306, false, "eps;ps", "");
        public static MediaType image_x_cyr_unidentified_image = new MediaType(Constants.image_x_cyr_unidentified_image, 54, AssetTypes.Image, 320, false, "", "");
        public static MediaType application_x_cyr_artwork = new MediaType(Constants.application_x_cyr_artwork, 13, AssetTypes.Artwork, 362, false, "", "");
        public static MediaType application_x_cyr_artwork_template = new MediaType(Constants.application_x_cyr_artwork_template, 14, AssetTypes.Artwork, 364, false, "", "");
        public static MediaType application_x_cyr_stockartwork = new MediaType(Constants.application_x_cyr_stockartwork, 15, AssetTypes.Artwork, 366, false, "", "");
        public static MediaType image_x_cyr_artwork = new MediaType(Constants.image_x_cyr_artwork, 16, AssetTypes.Artwork, 368, true, "", "");
        public static MediaType application_x_cyr_unidentified_artwork = new MediaType(Constants.application_x_cyr_unidentified_artwork, 62, AssetTypes.Artwork, 369, false, "", "");
        public static MediaType video_ = new MediaType(Constants.video_, 25, AssetTypes.Video, 450, true, "", "");
        public static MediaType video_x_cyr_unidentified_video = new MediaType(Constants.video_x_cyr_unidentified_video, 55, AssetTypes.Video, 402, false, "", "");
        public static MediaType audio_ = new MediaType(Constants.audio_, 26, AssetTypes.Audio, 550, true, "", "");
        public static MediaType audio_x_cyr_unidentified_audio = new MediaType(Constants.audio_x_cyr_unidentified_audio, 56, AssetTypes.Audio, 502, false, "", "");
        public static MediaType application_ecmascript = new MediaType(Constants.application_ecmascript, 27, AssetTypes.Web, 614, false, "js", "");
        public static MediaType application_json = new MediaType(Constants.application_json, 29, AssetTypes.Web, 620, false, "jsn;json", "");
        public static MediaType application_javascript = new MediaType(Constants.application_javascript, 30, AssetTypes.Web, 616, false, "", "");
        public static MediaType text_css = new MediaType(Constants.text_css, 32, AssetTypes.Web, 602, false, "css", "");
        public static MediaType text_html = new MediaType(Constants.text_html, 33, AssetTypes.Web, 612, false, "htm;html", "");
        public static MediaType text_javascript = new MediaType(Constants.text_javascript, 34, AssetTypes.Web, 618, false, "", "");
        public static MediaType application_x_www_form_urlencoded = new MediaType(Constants.application_x_www_form_urlencoded, 36, AssetTypes.Web, 604, false, "", "");
        public static MediaType text_x_cyr_unidentified_web = new MediaType(Constants.text_x_cyr_unidentified_web, 57, AssetTypes.Web, 622, false, "", "");
        public static MediaType text_less = new MediaType(Constants.text_less, 63, AssetTypes.Web, 603, false, "less", "");
        public static MediaType application_zip = new MediaType(Constants.application_zip, 37, AssetTypes.Binary, 712, false, "zip", "");
        public static MediaType application_gzip = new MediaType(Constants.application_gzip, 38, AssetTypes.Binary, 704, false, "gz;gzip", "");
        public static MediaType application_x_stuffit = new MediaType(Constants.application_x_stuffit, 39, AssetTypes.Binary, 708, false, "stuff", "");
        public static MediaType application_x_rar_compressed = new MediaType(Constants.application_x_rar_compressed, 40, AssetTypes.Binary, 706, false, "rar", "");
        public static MediaType application_tar = new MediaType(Constants.application_tar, 41, AssetTypes.Binary, 710, false, "tar", "");
        public static MediaType application_octet_stream = new MediaType(Constants.application_octet_stream, 42, AssetTypes.Binary, 702, false, "", "");
        public static MediaType application_x_cyr_unidentified_binary = new MediaType(Constants.application_x_cyr_unidentified_binary, 58, AssetTypes.Binary, 714, false, "", "");
        public static MediaType application_xml = new MediaType(Constants.application_xml, 43, AssetTypes.XML, 806, false, "xml", "");
        public static MediaType application_soap_xml = new MediaType(Constants.application_soap_xml, 45, AssetTypes.XML, 802, false, "", "");
        public static MediaType text_xml = new MediaType(Constants.text_xml, 59, AssetTypes.XML, 804, false, "", "");
        public static MediaType message_ = new MediaType(Constants.message_, 47, AssetTypes.Other, 954, true, "eml", "");
        public static MediaType model_ = new MediaType(Constants.model_, 48, AssetTypes.Other, 950, true, "", "");
        public static MediaType multi_part_ = new MediaType(Constants.multi_part_, 49, AssetTypes.Other, 956, true, "mime", "");
        public static MediaType application_x_ = new MediaType(Constants.application_x_, 50, AssetTypes.Other, 952, true, "", "");
        public static MediaType application_x_cyr_unidentified_other = new MediaType(Constants.application_x_cyr_unidentified_other, 60, AssetTypes.Other, 902, false, "", "");
        public static MediaType unknown = new MediaType(Constants.unknown, 51, AssetTypes.Unknown, 1050, true, "", "");
        public static MediaType application_x_cyr_unidentified_unknown = new MediaType(Constants.application_x_cyr_unidentified_unknown, 61, AssetTypes.Unknown, 1002, false, "", "");
        public static MediaType application_font_woff = new MediaType(Constants.application_font_woff, 28, AssetTypes.Font, 610, false, "woff", "");
        public static MediaType application_ogg = new MediaType(Constants.application_ogg, 31, AssetTypes.Font, 606, false, "ogg", "");
        public static MediaType application_x_font_ttf = new MediaType(Constants.application_x_font_ttf, 35, AssetTypes.Font, 608, false, "ttf;tt", "");
        public static class Constants
        {
            public const string text_plain = "text/plain";
            public const string text_csv = "text/csv";
            public const string application_pdf = "application/pdf";
            public const string application_vnd_ms_excel = "application/vnd.ms-excel";
            public const string application_vnd_ms_powerpoint = "application/vnd.ms-powerpoint";
            public const string application_vnd_ms_word = "application/vnd.ms-word";
            public const string application_vnd_opemxmlformats_officedocument_spreadsheetml_sheet = "application/vnd.opemxmlformats-officedocument.spreadsheetml.sheet";
            public const string application_vnd_opemxmlformats_officedocument_wordprocessingml_document = "application/vnd.opemxmlformats-officedocument.wordprocessingml.document";
            public const string application_vnd_opemxmlformats_officedocument_spreadsheetml_presentation = "application/vnd.opemxmlformats-officedocument.spreadsheetml.presentation";
            public const string application_vnd_ = "application/vnd.";
            public const string application_x_latext = "application/x-latext";
            public const string text_vcard = "text/vcard";
            public const string application_x_cyr_unidentified_document = "application/x-cyr.unidentified.document";
            public const string image_gif = "image/gif";
            public const string image_jpeg = "image/jpeg";
            public const string image_png = "image/png";
            public const string image_svg_xml = "image/svg+xml";
            public const string image_tiff = "image/tiff";
            public const string image_ = "image/";
            public const string application_postscript = "application/postscript";
            public const string image_x_cyr_unidentified_image = "image/x-cyr.unidentified.image";
            public const string application_x_cyr_artwork = "application/x-cyr.artwork";
            public const string application_x_cyr_artwork_template = "application/x-cyr.artwork.template";
            public const string application_x_cyr_stockartwork = "application/x-cyr.stockartwork";
            public const string image_x_cyr_artwork = "image/x-cyr.artwork";
            public const string application_x_cyr_unidentified_artwork = "application/x-cyr.unidentified.artwork";
            public const string video_ = "video/";
            public const string video_x_cyr_unidentified_video = "video/x-cyr.unidentified.video";
            public const string audio_ = "audio/";
            public const string audio_x_cyr_unidentified_audio = "audio/x-cyr.unidentified.audio";
            public const string application_ecmascript = "application/ecmascript";
            public const string application_json = "application/json";
            public const string application_javascript = "application/javascript";
            public const string text_css = "text/css";
            public const string text_html = "text/html";
            public const string text_javascript = "text/javascript";
            public const string application_x_www_form_urlencoded = "application/x-www-form-urlencoded";
            public const string text_x_cyr_unidentified_web = "text/x-cyr-unidentified-web";
            public const string text_less = "text/less";
            public const string application_zip = "application/zip";
            public const string application_gzip = "application/gzip";
            public const string application_x_stuffit = "application/x-stuffit";
            public const string application_x_rar_compressed = "application/x-rar-compressed";
            public const string application_tar = "application/tar";
            public const string application_octet_stream = "application/octet-stream";
            public const string application_x_cyr_unidentified_binary = "application/x-cyr.unidentified.binary";
            public const string application_xml = "application/xml";
            public const string application_soap_xml = "application/soap+xml";
            public const string text_xml = "text/xml";
            public const string message_ = "message/";
            public const string model_ = "model/";
            public const string multi_part_ = "multi-part/";
            public const string application_x_ = "application/x-";
            public const string application_x_cyr_unidentified_other = "application/x-cyr.unidentified.other";
            public const string unknown = "";
            public const string application_x_cyr_unidentified_unknown = "application/x-cyr.unidentified.unknown";
            public const string application_font_woff = "application/font-woff";
            public const string application_ogg = "application/ogg";
            public const string application_x_font_ttf = "application/x-font-ttf";
        }

        public static Dictionary<string, MediaType> Lookup = new Dictionary<string, MediaType>()
        {
            { Constants.text_plain, text_plain },
            { Constants.text_csv, text_csv },
            { Constants.application_pdf, application_pdf },
            { Constants.application_vnd_ms_excel, application_vnd_ms_excel },
            { Constants.application_vnd_ms_powerpoint, application_vnd_ms_powerpoint },
            { Constants.application_vnd_ms_word, application_vnd_ms_word },
            { Constants.application_vnd_opemxmlformats_officedocument_spreadsheetml_sheet, application_vnd_opemxmlformats_officedocument_spreadsheetml_sheet },
            { Constants.application_vnd_opemxmlformats_officedocument_wordprocessingml_document, application_vnd_opemxmlformats_officedocument_wordprocessingml_document },
            { Constants.application_vnd_opemxmlformats_officedocument_spreadsheetml_presentation, application_vnd_opemxmlformats_officedocument_spreadsheetml_presentation },
            { Constants.application_vnd_, application_vnd_ },
            { Constants.application_x_latext, application_x_latext },
            { Constants.text_vcard, text_vcard },
            { Constants.application_x_cyr_unidentified_document, application_x_cyr_unidentified_document },
            { Constants.image_gif, image_gif },
            { Constants.image_jpeg, image_jpeg },
            { Constants.image_png, image_png },
            { Constants.image_svg_xml, image_svg_xml },
            { Constants.image_tiff, image_tiff },
            { Constants.image_, image_ },
            { Constants.application_postscript, application_postscript },
            { Constants.image_x_cyr_unidentified_image, image_x_cyr_unidentified_image },
            { Constants.application_x_cyr_artwork, application_x_cyr_artwork },
            { Constants.application_x_cyr_artwork_template, application_x_cyr_artwork_template },
            { Constants.application_x_cyr_stockartwork, application_x_cyr_stockartwork },
            { Constants.image_x_cyr_artwork, image_x_cyr_artwork },
            { Constants.application_x_cyr_unidentified_artwork, application_x_cyr_unidentified_artwork },
            { Constants.video_, video_ },
            { Constants.video_x_cyr_unidentified_video, video_x_cyr_unidentified_video },
            { Constants.audio_, audio_ },
            { Constants.audio_x_cyr_unidentified_audio, audio_x_cyr_unidentified_audio },
            { Constants.application_ecmascript, application_ecmascript },
            { Constants.application_json, application_json },
            { Constants.application_javascript, application_javascript },
            { Constants.text_css, text_css },
            { Constants.text_html, text_html },
            { Constants.text_javascript, text_javascript },
            { Constants.application_x_www_form_urlencoded, application_x_www_form_urlencoded },
            { Constants.text_x_cyr_unidentified_web, text_x_cyr_unidentified_web },
            { Constants.text_less, text_less },
            { Constants.application_zip, application_zip },
            { Constants.application_gzip, application_gzip },
            { Constants.application_x_stuffit, application_x_stuffit },
            { Constants.application_x_rar_compressed, application_x_rar_compressed },
            { Constants.application_tar, application_tar },
            { Constants.application_octet_stream, application_octet_stream },
            { Constants.application_x_cyr_unidentified_binary, application_x_cyr_unidentified_binary },
            { Constants.application_xml, application_xml },
            { Constants.application_soap_xml, application_soap_xml },
            { Constants.text_xml, text_xml },
            { Constants.message_, message_ },
            { Constants.model_, model_ },
            { Constants.multi_part_, multi_part_ },
            { Constants.application_x_, application_x_ },
            { Constants.application_x_cyr_unidentified_other, application_x_cyr_unidentified_other },
            { Constants.unknown, unknown },
            { Constants.application_x_cyr_unidentified_unknown, application_x_cyr_unidentified_unknown },
            { Constants.application_font_woff, application_font_woff },
            { Constants.application_ogg, application_ogg },
            { Constants.application_x_font_ttf, application_x_font_ttf },
        };
    }
}
