﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.DocumentStorage.Models
{
    public class AssetType
    {
        public AssetType(int id, string name, string displayName, int sortIndex, MediaType defaultMediaType, string fontClass)
        {
            this.ID = id;
            this.Name = name;
            this.DisplayName = displayName;
            this.SortIndex = SortIndex;
            this.DefaultMediaType = defaultMediaType;
            this.FontClass = fontClass;
        }
        public int ID { get; protected set; }
        public string Name { get; protected set; }
        public string DisplayName { get; protected set; }
        public int SortIndex { get; protected set; }
        public MediaType DefaultMediaType { get; protected set; }
        public string FontClass { get; protected set; }
    }
    public static class AssetTypes
    {
        public static AssetType Text = new AssetType(1, "Text", "Text Files", 1, MediaTypes.text_plain, "fa fa-file-text-o");
        public static AssetType Document = new AssetType(2, "Document", "Documents", 2, MediaTypes.application_x_cyr_unidentified_document, "fa fa-file-o");
        public static AssetType Image = new AssetType(3, "Image", "Image Files", 3, MediaTypes.image_x_cyr_unidentified_image, "fa fa-file-image-o");
        public static AssetType Artwork = new AssetType(4, "Artwork", "Artwork Files", 4, MediaTypes.application_x_cyr_unidentified_artwork, "fa fa-file-picture-o");
        public static AssetType Video = new AssetType(5, "Video", "Video Files", 5, MediaTypes.video_x_cyr_unidentified_video, "fa fa-file-video-o");
        public static AssetType Audio = new AssetType(6, "Audio", "Audio Files", 6, MediaTypes.audio_x_cyr_unidentified_audio, "fa fa-file-audio-o");
        public static AssetType Web = new AssetType(7, "Web", "Web Files", 7, MediaTypes.text_x_cyr_unidentified_web, "fa fa-file-code-o");
        public static AssetType Binary = new AssetType(8, "Binary", "Binary Data", 8, MediaTypes.application_x_cyr_unidentified_binary, "fa fa-file-zip-o");
        public static AssetType XML = new AssetType(9, "XML", "XML Data", 9, MediaTypes.text_xml, "fa fa-file-code-o");
        public static AssetType Other = new AssetType(10, "Other", "Other Data", 10, MediaTypes.application_x_cyr_unidentified_other, "fa fa-file-file-o");
        public static AssetType Unknown = new AssetType(11, "Unknown", "Unknown File Type", 11, MediaTypes.application_x_cyr_unidentified_unknown, "fa fa-file-file");
        public static AssetType Font = new AssetType(12, "Font", "Font", 12, MediaTypes.application_font_woff, "fa fa-file-file");

        public static class Constants
        {
            public const int Text = 1;
            public const int Document = 2;
            public const int Image = 3;
            public const int Artwork = 4;
            public const int Video = 5;
            public const int Audio = 6;
            public const int Web = 7;
            public const int Binary = 8;
            public const int XML = 9;
            public const int Other = 10;
            public const int Unknown = 11;
            public const int Font = 12;
        }

        public static Dictionary<int, AssetType> Lookup = new Dictionary<int, AssetType>()
        {
            {Constants.Text    , Text     },
            {Constants.Document, Document },
            {Constants.Image   , Image    },
            {Constants.Artwork , Artwork  },
            {Constants.Video   , Video    },
            {Constants.Audio   , Audio    },
            {Constants.Web     , Web      },
            {Constants.Binary  , Binary   },
            {Constants.XML     , XML      },
            {Constants.Other   , Other    },
            {Constants.Unknown , Unknown  },
            {Constants.Font    , Font     },
        };
    }
}
