﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Authentication
{
    public class InternalAuthenticationSchemeOptions : AuthenticationSchemeOptions
    {
        public string TenantSecretKey { get; set; }
    }
}
