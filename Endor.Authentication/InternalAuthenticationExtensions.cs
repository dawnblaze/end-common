﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Authentication
{
    public static class InternalAuthenticationExtensions
    {
        private const string InternalScheme = "Internal";

        public static AuthenticationBuilder AddInternal(this AuthenticationBuilder builder, Action<InternalAuthenticationSchemeOptions> configOptions)
        {
            return builder.AddScheme<InternalAuthenticationSchemeOptions, InternalAuthenticationHandler>(InternalScheme, configOptions);
        }
        
    }
}
