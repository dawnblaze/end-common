﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Endor.Authentication
{
    public class InternalAuthenticationHandler : AuthenticationHandler<InternalAuthenticationSchemeOptions>
    {

        private const string InternalSchemeName = "Internal";
        private const string AuthorizationHeaderName = "Authorization";

        public InternalAuthenticationHandler(IOptionsMonitor<InternalAuthenticationSchemeOptions> options,
            ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            return await Task.FromResult(InternalAuthenticate());
        }

        protected override Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            return base.HandleChallengeAsync(properties);
        }

        protected override Task HandleForbiddenAsync(AuthenticationProperties properties)
        {
            return base.HandleForbiddenAsync(properties);
        }

        public AuthenticateResult InternalAuthenticate()
        {
            if (!Request.Headers.ContainsKey(AuthorizationHeaderName))
                return AuthenticateResult.NoResult();

            if (!AuthenticationHeaderValue.TryParse(Request.Headers[AuthorizationHeaderName], out AuthenticationHeaderValue authenticationHeaderValue))
            {
                return AuthenticateResult.NoResult();
            }

            if (!InternalSchemeName.Equals(authenticationHeaderValue.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                return AuthenticateResult.NoResult();
            }

            string[] values = authenticationHeaderValue.Parameter.Split(':');
            string tenantKey = values[0];
            
            if (String.IsNullOrWhiteSpace(tenantKey) || tenantKey != Options.TenantSecretKey)
                return AuthenticateResult.Fail("Invalid Internal Authentication header");


            ClaimsIdentity identity = new ClaimsIdentity(Scheme.Name) { };

            if (values.Length > 1)
            {
                short.TryParse(values[1], out short myBID);
                identity.AddClaim(new Claim("BID", myBID.ToString(), ClaimValueTypes.Integer));
            }

            if (values.Length > 2)
            {
                byte.TryParse(values[1], out byte myAID);
                identity.AddClaim(new Claim("AID", myAID.ToString(), ClaimValueTypes.Integer));
            }

            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }
    }
}
