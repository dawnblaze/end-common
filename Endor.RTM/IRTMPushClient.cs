﻿using Endor.RTM.Models;
using System.Threading.Tasks;

namespace Endor.RTM
{
    public interface IRTMPushClient
    {
        Task SendRefreshMessage(RefreshMessage refreshMsg, string excludedConnectionId = null);

        Task SendSystemMessage(SystemMessage systemMsg);

        Task SendUserMessage(UserMessage userMsg);

        Task SendEmployeeMessage(EmployeeMessage userMsg);

        Task SendReportTemplateRefreshMessage(ReportTemplateRefreshMessage refreshMsg);
    }
}