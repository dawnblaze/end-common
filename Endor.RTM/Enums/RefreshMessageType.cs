﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.RTM.Enums
{
    public enum RefreshMessageType
    {
        Change, //= Existing Record Changed
        New, //= New Record of Specified Type
        Delete, // = Existing Record Deleted
        CollectionChange, // = Container contents changes, but container unaffected
        Command, // = message to client to take some action (e.g., Logout)
        Information, // = Non - refresh related message (e.g., Toast Message)
    }
}
