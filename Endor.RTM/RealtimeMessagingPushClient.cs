﻿using Endor.RTM.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Endor.RTM
{
    public class RealtimeMessagingPushClient: IRTMPushClient
    {
        private readonly string _messagingURL;
        private const string userEndpoint = "user";
        private const string employeeEndpoint = "employee";
        private const string refreshEndpoint = "refresh";
        private const string systemEndpoint = "system";
        private const string refreshReportTemplateEndpoint = "refreshReportTemplate";

        public RealtimeMessagingPushClient(string messagingURL)
        {
            this.RTC = new HttpClient();
            this._messagingURL = messagingURL;
        }

        public HttpClient RTC { get; set; }

        public async Task SendRefreshMessage(RefreshMessage refreshMsg, string excludedConnectionId = null)
        {
            await SendMsg(refreshEndpoint, JsonConvert.SerializeObject(refreshMsg), excludedConnectionId);
        }

        public async Task SendSystemMessage(SystemMessage systemMsg)
        {
            await SendMsg(systemEndpoint, JsonConvert.SerializeObject(systemMsg));
        }

        public async Task SendUserMessage(UserMessage userMsg)
        {
            await SendMsg(userEndpoint, JsonConvert.SerializeObject(userMsg));
        }

        public async Task SendEmployeeMessage(EmployeeMessage userMsg)
        {
            await SendMsg(employeeEndpoint, JsonConvert.SerializeObject(userMsg));
        }

        public async Task SendReportTemplateRefreshMessage(ReportTemplateRefreshMessage refreshMsg)
        {
            await SendMsg(refreshReportTemplateEndpoint, JsonConvert.SerializeObject(refreshMsg));
        }

        private async Task SendMsg(string endpoint, string jsonContent, string excludeConnectionId=null)
        {
            HttpResponseMessage responseMsg = await RTC.PostAsync($"{_messagingURL}api/external/{endpoint}{ (String.IsNullOrWhiteSpace(excludeConnectionId) ? String.Empty :"?excludeConnectionId="+ excludeConnectionId)}", new StringContent(jsonContent, Encoding.UTF8, "application/json"));
            Console.WriteLine(responseMsg.IsSuccessStatusCode ? await responseMsg.Content.ReadAsStringAsync() : "Failed response");
        }
    }
}
