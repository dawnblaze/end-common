﻿using Endor.RTM.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.RTM.Models
{
    public class ReportTemplateRefreshEntity
    {
        [JsonProperty("bid")]
        public short BID { get; set; }
        [JsonProperty("id")]
        public string ID { get; set; }
        [JsonProperty("datetime")]
        public DateTime DateTime { get; set; }
        [JsonProperty("data")]
        public string Data { get; set; }
        [JsonProperty("type")]
        public RefreshMessageType RefreshMessageType { get; set; }
        [JsonProperty("headingHash", NullValueHandling = NullValueHandling.Ignore)]
        public string HeadingHash { get; set; }
    }
}
