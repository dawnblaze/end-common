﻿using Newtonsoft.Json;

namespace Endor.RTM.Models
{
    public class EmployeeMessage
    {
        [JsonProperty("bid")]
        public short BID { get; set; }
        [JsonProperty("employeeID")]
        public short EmployeeID { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("data")]
        public string Data { get; set; }
    }
}
