﻿using Endor.RTM.Enums;
using Newtonsoft.Json;
using System;

namespace Endor.RTM.Models
{
    public class RefreshEntity
    {
        [JsonProperty("bid")]
        public short BID { get; set; }
        [JsonProperty("id")]
        public int ID { get; set; }
        [JsonProperty("ctid")]
        public int ClasstypeID { get; set; }
        [JsonProperty("datetime")]
        public DateTime DateTime { get; set; }
        [JsonProperty("data")]
        public string Data { get; set; }
        [JsonProperty("type")]
        public RefreshMessageType RefreshMessageType { get; set; }
        [JsonProperty("headingHash", NullValueHandling = NullValueHandling.Ignore)]
        public string HeadingHash { get; set; }
    }
}
