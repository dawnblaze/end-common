﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.RTM.Models
{
    public class SystemMessage
    {
        public string Message { get; set; }
        public string Data { get; set; }
    }
}
