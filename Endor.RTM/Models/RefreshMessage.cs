﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Endor.RTM.Models
{
    public class RefreshMessage
    {
        [JsonProperty("refreshEntities")]
        public List<RefreshEntity> RefreshEntities { get; set; }
    }
}
