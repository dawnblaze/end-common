﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.RTM.Models
{
    public class UserMessage
    {
        public string Message { get; set; }
        public string Username { get; set; }
        public string Data { get; set; }
    }
}
