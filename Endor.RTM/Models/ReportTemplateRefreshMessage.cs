﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.RTM.Models
{
    public class ReportTemplateRefreshMessage
    {
        public List<ReportTemplateRefreshEntity> RefreshEntities { get; set; }
    }
}
