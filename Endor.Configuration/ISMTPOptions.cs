﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Configuration
{

    public interface ISMTPOptions
    {
        string Host { get; set; }
        int Port { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string MailFromAddress { get; set; }
    }
}
