﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Endor.Configuration
{
    /// <summary>
    /// Encompasses settings for SMTP
    /// </summary>
    public class SMTPOptions : ISMTPOptions
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string MailFromAddress { get; set; }
    }

}
