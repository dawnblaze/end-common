
ALTER PROCEDURE [dbo].[Accounting.Tax.Group.Recalc]
                                @BID SMALLINT
AS
BEGIN
    DECLARE @DT DateTime2(2) = GetDate();

    DECLARE @T TABLE(
        BID SMALLINT
      , ID SMALLINT
      , ClassTypeID SMALLINT
      , OldRate DECIMAL(12,4)
      , NewRate DECIMAL(12,4)
    );

    -- Insert into the Temp Table
    --   All records where the Group.TaxRate != Sum(Item.TaxRate)
    INSERT INTO @T
        SELECT G.BID, G.ID, G.ClassTypeID, COALESCE(G.TaxRate,0.0000), SUM(A.TaxRate)
        FROM [Accounting.Tax.Group] G
        JOIN [Accounting.Tax.Group.ItemLink] GAL ON (GAL.BID = G.BID AND GAL.GroupID = G.ID)
        JOIN [Accounting.Tax.Item] A             ON (A.BID = GAL.BID AND A.ID = GAL.ItemID)
        WHERE G.BID = @BID
          AND A.IsActive = 1
        GROUP BY G.BID, G.ID, G.ClassTypeID, COALESCE(G.TaxRate,0.0000)
        HAVING COALESCE(G.TaxRate,0.0000) != SUM(A.TaxRate)
        ORDER BY G.BID, G.ID
    ;

    -- Update the Tax Groups
    UPDATE G
    SET ModifiedDT = @DT
      , TaxRate = T.NewRate
    FROM [Accounting.Tax.Group] G
    JOIN @T T ON (T.BID = G.BID AND T.ID = G.ID)
    ;

    -- Output the Result
    SELECT * FROM @T
    ;
END