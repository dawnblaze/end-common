IF EXISTS(select 1 from sys.views where name='Accounting.Payment.Term.SimpleList' and type='v')
DROP VIEW [dbo].[Accounting.Payment.Term.SimpleList];
GO
CREATE VIEW[dbo].[Accounting.Payment.Term.SimpleList]
    AS
SELECT[BID]
    , [ID]
    , [ClassTypeID]
    , [Name] as DisplayName
    , [IsActive]
    , CONVERT(BIT, 0) AS [IsDefault]
FROM[Accounting.Payment.Term]