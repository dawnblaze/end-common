USE [Dev.Endor.Business.DB1]
GO

/****** Object:  View [dbo].[Accounting.Tax.Item.SimpleList]    Script Date: 02/14/2018 12:30:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[Accounting.Tax.Item.SimpleList] AS
    SELECT [BID]
         , [ID]
         , [ClassTypeID]
         , [Name] as DisplayName
         , [IsActive]
         , CONVERT(BIT, 0) AS [HasImage]
         , CONVERT(BIT, 0) AS [IsDefault]
    FROM [Accounting.Tax.Item]
GO


