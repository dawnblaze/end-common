IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.CanDelete')
BEGIN
    DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.CanDelete]
END

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.SetActive')
BEGIN
    DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetActive]
END

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'Accounting.Payment.Term.Action.SetDefault')
BEGIN
    DROP PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetDefault]
END

