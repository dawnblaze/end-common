/* 
========================================================
    Name: [Accounting.Payment.Term.Action.CanDelete]

    Description: This procedure checks if the PaymentTerm is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Payment.Term.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Payment.Term.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the PaymentTerm can be deleted. The boolean response is returned in the body.  A PaymentTerm can be deleted if
	It is used by no companies. 
	It is used by no order
	It is used by no estimates
	It is used by no purchase orders
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Payment.Term] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term Specified Does not Exist. PaymentTermID=', @ID)

/* --FUTURE
	ELSE IF  EXISTS( SELECT * FROM [Company.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Company Data. PaymentTermID=', @ID)

	ELSE IF EXISTS( SELECT * FROM [Order.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Order Data. PaymentTermID=', @ID)
	ELSE IF EXISTS( SELECT * FROM [Estimate.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Estimate Data. PaymentTermID=', @ID)
	ELSE IF EXISTS( SELECT * FROM [PO.Data] WHERE BID = @BID AND PaymentTermsID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Payment Term is used in the Purchase Order Data. PaymentTermID=', @ID)
*/
    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END