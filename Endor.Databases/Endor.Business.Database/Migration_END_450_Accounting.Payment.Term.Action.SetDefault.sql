-- ========================================================
-- Name: [Accounting.Payment.Term.Action.SetDefault]
--
-- Description: This procedure sets the default PaymentTerm in the options definition table.
--
-- Sample Use:   EXEC dbo.[Accounting.Payment.Term.Action.SetDefault] @BID=1, @PaymentTermID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetDefault]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @PaymentTermID     SMALLINT     -- = 2

        , @IsDefault       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the option specified is valid
    IF NOT EXISTS(SELECT * FROM [System.Option.Definition] WHERE Name = 'Accounting.PaymentTerm.DefaultID')
    BEGIN
        SELECT @Result = 0
             , @Message = 'The Accounting.PaymentTerm.DefaultID OPTION is not found.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET DefaultValue = @PaymentTermID
    FROM [System.Option.Definition] L
    WHERE Name = 'Accounting.PaymentTerm.DefaultID'

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END