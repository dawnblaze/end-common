USE [Dev.Endor.Business.DB1]

GO

DROP TABLE IF EXISTS [CRM.Setup.Industry]
DROP TABLE IF EXISTS [CRM.Setup.Source]

GO

CREATE TABLE [Accounting.Payment.Term](
    [BID] [smallint] NOT NULL,
    [ID] [smallint] NOT NULL,
    [ClassTypeID] AS ((11105)),
    [CreatedDate] [date] NOT NULL,
    [ModifiedDT] [datetime2](2) NOT NULL,
    [IsActive] [bit] NOT NULL,
    [Name] [varchar](255) NULL,
    [GracePeriod] [smallint] NULL,
    [InterestRate] [decimal](12, 4) NULL,
    [InterestBasedOnSaleDate] [bit] NOT NULL,
    [CompoundInterest] [bit] NOT NULL,
    [AllowEarlyPaymentDiscount] [bit] NOT NULL,
    [EarlyPaymentDays] [smallint] SPARSE NULL,
    [EarlyPaymentDiscount] [decimal](12, 4) SPARSE NULL,
 CONSTRAINT [PK_Accounting.Payment.Term] PRIMARY KEY CLUSTERED (BID, ID)
)
GO

CREATE TABLE [Accounting.Payment.Term.LocationLink](
    [BID] [smallint] NOT NULL,
    [TermID] [smallint] NOT NULL,
    [LocationID] [tinyint] NOT NULL,
 CONSTRAINT [PK_Accounting.Payment.Term.LocationLink] PRIMARY KEY CLUSTERED (BID, TermID, LocationID)
)
GO

CREATE TABLE [Accounting.Tax.Assessment](
    [BID] [smallint] NOT NULL,
    [ID] [smallint] NOT NULL,
    [ClassTypeID] AS ((11102)),
    [CreatedDate] [date] NOT NULL,
    [ModifiedDT] [datetime2](2) NOT NULL,
    [IsActive] [bit] NOT NULL,
    [Name] [varchar](255) NOT NULL,
    [InvoiceText] [varchar](7) NOT NULL,
    [TaxRate] [decimal](12, 4) NOT NULL,
    [LookupCode] [varchar](255) NULL,
    [AgencyName] [varchar](255) NULL,
    [AccountNumber] [varchar](255) NULL,
    [VendorID] [int] NULL,
 CONSTRAINT [PK_Accounting.Tax.Assessment_1] PRIMARY KEY CLUSTERED (BID, ID)
)
GO

CREATE TABLE [Accounting.Tax.Group](
    [BID] [smallint] NOT NULL,
    [ID] [smallint] NOT NULL,
    [ClassTypeID] AS ((11101)),
    [CreatedDate] [date] NOT NULL,
    [ModifiedDT] [datetime2](2) NOT NULL,
    [IsActive] [bit] NOT NULL,
    [Name] [varchar](255) NOT NULL,
    [IsDefault] [bit] NOT NULL,
    [TaxRate] [decimal](12, 4) NULL,
    [IsTaxExempt] AS (case when [TaxRate]=(0.0) then CONVERT([bit],(1)) else CONVERT([bit],(0)) end),
 CONSTRAINT [PK_Accounting.Tax.Group] PRIMARY KEY CLUSTERED (BID, ID)
)
GO

CREATE TABLE [Accounting.Tax.Group.AssessmentLink](
    [BID] [smallint] NOT NULL,
    [GroupID] [smallint] NOT NULL,
    [AssessmentID] [smallint] NOT NULL,
 CONSTRAINT [PK_Accounting.Tax.Group.AssessmentLink] PRIMARY KEY CLUSTERED (BID, GroupID, AssessmentID)
)
GO

CREATE TABLE [Accounting.Tax.Group.LocationLink](
    [BID] [smallint] NOT NULL,
    [GroupID] [smallint] NOT NULL,
    [LocationID] [tinyint] NOT NULL,
 CONSTRAINT [PK_Accounting.Tax.Group.LocationLink] PRIMARY KEY CLUSTERED (BID, GroupID, LocationID)
)
GO

CREATE TABLE [CRM.Industry](
    [BID] [smallint] NOT NULL,
    [ID] [smallint] NOT NULL,
    [ClassTypeID] AS ((2011)),
    [ModifiedDT] [datetime2](2) NOT NULL,
    [IsActive] [bit] NOT NULL,
    [Name] [varchar](255) NOT NULL,
    [IsLocked] [bit] NOT NULL,
    [ParentID] [smallint] NULL,
    [IsTopLevel] AS (case when [ParentID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end),
 CONSTRAINT [PK_CRM.Industry] PRIMARY KEY CLUSTERED (BID, ID)
)
GO

CREATE TABLE [CRM.Source](
    [BID] [smallint] NOT NULL,
    [ID] [smallint] NOT NULL,
    [ClassTypeID] AS ((2012)),
    [ModifiedDT] [datetime2](2) NOT NULL,
    [IsActive] [bit] NOT NULL,
    [Name] [varchar](100) NOT NULL,
    [IsLocked] [bit] NOT NULL,
    [ParentID] [smallint] NULL,
    [IsTopLevel] AS (case when [ParentID] IS NULL then CONVERT([bit],(1)) else CONVERT([bit],(0)) end),
 CONSTRAINT [PK_CRM.Setup.Source] PRIMARY KEY CLUSTERED (BID, ID)
)
GO

CREATE NONCLUSTERED INDEX [IX_Accounting.Payment.Term)Name] ON [Accounting.Payment.Term]
(
    [BID] ASC,
    [Name] ASC,
    [IsActive] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_Accounting.Payment.Term.LocationLink] ON [Accounting.Payment.Term.LocationLink]
(
    [BID] ASC,
    [LocationID] ASC,
    [TermID] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_Accounting.Tax.Assessment_Name] ON [Accounting.Tax.Assessment]
(
    [BID] ASC,
    [Name] ASC,
    [IsActive] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_Accounting.Tax.Group_Name] ON [Accounting.Tax.Group]
(
    [BID] ASC,
    [Name] ASC,
    [IsActive] ASC,
    [IsDefault] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_Accounting.Tax.Group.AssessmentLink_Assessment] ON [Accounting.Tax.Group.AssessmentLink]
(
    [BID] ASC,
    [AssessmentID] ASC,
    [GroupID] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_Accounting.Tax.Group.LocationLink_Location] ON [Accounting.Tax.Group.LocationLink]
(
    [BID] ASC,
    [LocationID] ASC,
    [GroupID] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_CRM.Industry_Name] ON [CRM.Industry]
(
    [BID] ASC,
    [Name] ASC,
    [IsActive] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_CRM.Industry_Parent] ON [CRM.Industry]
(
    [BID] ASC,
    [ParentID] ASC,
    [Name] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_CRM.Source_Name] ON [CRM.Source]
(
    [BID] ASC,
    [Name] ASC,
    [IsActive] ASC
)
GO

CREATE NONCLUSTERED INDEX [IX_CRM.Source_Parent] ON [CRM.Source]
(
    [BID] ASC,
    [ParentID] ASC,
    [Name] ASC
)
GO

ALTER TABLE [Accounting.Payment.Term] ADD CONSTRAINT [DF_Accounting.Payment.Term_CreatedDate] DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [Accounting.Payment.Term] ADD CONSTRAINT [DF_Accounting.Payment.Term_ModifiedDT] DEFAULT (getutcdate()) FOR [ModifiedDT]
GO
ALTER TABLE [Accounting.Payment.Term] ADD CONSTRAINT [DF_Accounting.Payment.Term_IsActive] DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [Accounting.Payment.Term] ADD CONSTRAINT [DF_Accounting.Payment.Term_InterestBasedOnSaleDate] DEFAULT ((0)) FOR [InterestBasedOnSaleDate]
GO
ALTER TABLE [Accounting.Payment.Term] ADD CONSTRAINT [DF_Accounting.Payment.Term_CompoundInterest] DEFAULT ((1)) FOR [CompoundInterest]
GO
ALTER TABLE [Accounting.Payment.Term] ADD CONSTRAINT [DF_Accounting.Payment.Term_AllowEarlyPaymentDiscount] DEFAULT ((0)) FOR [AllowEarlyPaymentDiscount]
GO
ALTER TABLE [Accounting.Tax.Assessment] ADD CONSTRAINT [DF_Settings.Tax.Assessment.Data_CreatedDate] DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [Accounting.Tax.Assessment] ADD CONSTRAINT [DF_Settings.Tax.Assessment.Data_ModifiedDT] DEFAULT (getutcdate()) FOR [ModifiedDT]
GO
ALTER TABLE [Accounting.Tax.Assessment] ADD CONSTRAINT [DF_Settings.Tax.Assessment.Data_IsActive] DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [Accounting.Tax.Group] ADD CONSTRAINT [DF_Accounting.Tax.Group.Data_CreatedDate] DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [Accounting.Tax.Group] ADD CONSTRAINT [DF_Accounting.Tax.Group.Data_ModifiedDT] DEFAULT (getutcdate()) FOR [ModifiedDT]
GO
ALTER TABLE [Accounting.Tax.Group] ADD CONSTRAINT [DF_Accounting.Tax.Group.Data_IsActive] DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [Accounting.Tax.Group] ADD CONSTRAINT [DF_Accounting.Tax.Group.Data_IsDefault] DEFAULT ((0)) FOR [IsDefault]
GO
ALTER TABLE [CRM.Industry] ADD CONSTRAINT [DF_CRM.Industry_ModifiedDT] DEFAULT (getdate()) FOR [ModifiedDT]
GO
ALTER TABLE [CRM.Industry] ADD CONSTRAINT [DF_CRM.Industry_IsActive] DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [CRM.Industry] ADD CONSTRAINT [DF_CRM.Industry_IsLocked] DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [CRM.Source] ADD CONSTRAINT [DF_CRM.Setup.Source_ModifiedDT] DEFAULT (getdate()) FOR [ModifiedDT]
GO
ALTER TABLE [CRM.Source] ADD CONSTRAINT [DF_CRM.Setup.Source_IsActive] DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [CRM.Source] ADD CONSTRAINT [DF_CRM.Source_IsLocked] DEFAULT ((0)) FOR [IsLocked]
GO
ALTER TABLE [Accounting.Payment.Term.LocationLink] WITH CHECK ADD CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term] FOREIGN KEY([BID], [TermID])
REFERENCES [Accounting.Payment.Term] ([BID], [ID])
GO
ALTER TABLE [Accounting.Payment.Term.LocationLink] CHECK CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Accounting.Payment.Term]
GO
ALTER TABLE [Accounting.Payment.Term.LocationLink] WITH CHECK ADD CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Location.Data] FOREIGN KEY([BID], [LocationID])
REFERENCES [Location.Data] ([BID], [ID])
GO
ALTER TABLE [Accounting.Payment.Term.LocationLink] CHECK CONSTRAINT [FK_Accounting.Payment.Term.LocationLink_Location.Data]
GO
ALTER TABLE [Accounting.Tax.Group.AssessmentLink] WITH CHECK ADD CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment] FOREIGN KEY([BID], [AssessmentID])
REFERENCES [Accounting.Tax.Assessment] ([BID], [ID])
GO
ALTER TABLE [Accounting.Tax.Group.AssessmentLink] CHECK CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Assessment]
GO
ALTER TABLE [Accounting.Tax.Group.AssessmentLink] WITH CHECK ADD CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group] FOREIGN KEY([BID], [GroupID])
REFERENCES [Accounting.Tax.Group] ([BID], [ID])
GO
ALTER TABLE [Accounting.Tax.Group.AssessmentLink] CHECK CONSTRAINT [FK_Accounting.Tax.Group.AssessmentLink_Accounting.Tax.Group]
GO
ALTER TABLE [Accounting.Tax.Group.LocationLink] WITH CHECK ADD CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group] FOREIGN KEY([BID], [GroupID])
REFERENCES [Accounting.Tax.Group] ([BID], [ID])
GO
ALTER TABLE [Accounting.Tax.Group.LocationLink] CHECK CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Accounting.Tax.Group]
GO
ALTER TABLE [Accounting.Tax.Group.LocationLink] WITH CHECK ADD CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Location.Data] FOREIGN KEY([BID], [LocationID])
REFERENCES [Location.Data] ([BID], [ID])
GO
ALTER TABLE [Accounting.Tax.Group.LocationLink] CHECK CONSTRAINT [FK_Accounting.Tax.Group.LocationLink_Location.Data]
GO
ALTER TABLE [CRM.Industry] WITH CHECK ADD CONSTRAINT [FK_CRM.Industry_CRM.Industry] FOREIGN KEY([BID], [ParentID])
REFERENCES [CRM.Industry] ([BID], [ID])
GO
ALTER TABLE [CRM.Industry] CHECK CONSTRAINT [FK_CRM.Industry_CRM.Industry]
GO
ALTER TABLE [CRM.Source] WITH CHECK ADD CONSTRAINT [FK_CRM.Source_CRM.Source] FOREIGN KEY([BID], [ParentID])
REFERENCES [CRM.Source] ([BID], [ID])
GO
ALTER TABLE [CRM.Source] CHECK CONSTRAINT [FK_CRM.Source_CRM.Source]
GO