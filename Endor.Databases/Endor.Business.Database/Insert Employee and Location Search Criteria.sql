USE [Dev.Endor.Business.DB1]
GO

SELECT * FROM [dbo].[System.List.Filter.Criteria]
GO

SET IDENTITY_INSERT [dbo].[System.List.Filter.Criteria] ON 

INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (1, 5000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 0, 0, 0, NULL, NULL, 0, NULL)
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (2, 5000, N'Name', N'Name', N'FullName', 0, 0, 0, 0, NULL, NULL, 0, 0)
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (3, 5000, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, NULL, NULL, 1, NULL)
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (4, 5000, N'Email', N'Email', N'EmailAddress', 0, 0, 0, 0, NULL, NULL, 0, NULL)
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (5, 5000, N'Location', N'Location', N'LocationID', 0, 1, 4, 0, NULL, NULL, 0, NULL)
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (6, 1005, N'Name', N'Name', N'Name', 0, 0, 0, 0, NULL, NULL, 0, 0)
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (7, 1005, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, NULL, NULL, 1, NULL)

SET IDENTITY_INSERT [dbo].[System.List.Filter.Criteria] OFF
