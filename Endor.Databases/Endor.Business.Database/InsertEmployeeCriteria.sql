declare @BID as smallint = 1;
declare @CT as varchar(128) = 'employee';
declare @CTID as int = (SELECT TOP 1 ID as lName FROM dbo.[enum.ClassType] where lower(Name) = lower(@CT));
print('Seeding '+@CT+' List.Criteria for BID: '+CAST(@BID as varchar(10)))

INSERT INTO [dbo].[System.List.Filter.Criteria]
(--[ID] Identity
      --[ClassTypeID] computed
      [TargetClassTypeID]
      ,[Name]
      ,[Label]
      ,[Field]
      ,[IsHidden]
      ,[DataType]
      ,[InputType]
      ,[AllowMultiple]
      ,[ListValues]
      ,[ListValuesEndpoint]
      ,[IsLimitToList]
      --,[SortIndex]
      --[IsVisible] --computed
	  )
  values (
	@CTID,
	'Name', --name
	'Name', --label
	'FullName', --field
	0, --ishidden
	0, --datatype
	0, --inputtype
	0, --allowmultiple
	null, --listvalues
	null, --listvalues endpoint
	0 --islimittolist
	--50 --sortindex
  )
  
INSERT INTO [dbo].[System.List.Filter.Criteria]
(--[ID] Identity
      --[ClassTypeID] computed
      [TargetClassTypeID]
      ,[Name]
      ,[Label]
      ,[Field]
      ,[IsHidden]
      ,[DataType]
      ,[InputType]
      ,[AllowMultiple]
      ,[ListValues]
      ,[ListValuesEndpoint]
      ,[IsLimitToList]
      --,[SortIndex]
      --[IsVisible] --computed
	  )
  values (
	@CTID,
	'PhoneNumber', --name
	'Phone Number', --label
	'PhoneNumber', --field
	0, --ishidden
	0, --datatype
	0, --inputtype
	0, --allowmultiple
	null, --listvalues
	null, --listvalues endpoint
	0 --islimittolist
	--50 --sortindex
  )