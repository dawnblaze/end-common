IF EXISTS(select * from sys.objects where name = 'Accounting.Payment.Method.Action.SetActive' and type = 'P')
  DROP PROCEDURE [dbo].[Accounting.Payment.Method.Action.SetActive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Name: [Accounting.Payment.Method.Action.SetActive]
--
-- Description: This procedure sets the PaymentMethod to active or inactive
-- and sets the corresponding GLAccount to active or inactive as well
--
-- Sample Use:   EXEC dbo.[Accounting.Payment.Method.Action.SetActive] @BID=1, @PaymentMethodID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Payment.Method.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @PaymentMethodID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the PaymentMethod specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Method] WHERE BID = @BID and ID = @PaymentMethodID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid PaymentMethod Specified. PaymentMethodID='+CONVERT(VARCHAR(12),@PaymentMethodID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.Payment.Method] L
    WHERE BID = @BID and ID = @PaymentMethodID
      AND COALESCE(IsActive,~@IsActive) != @IsActive
	  
    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END
GO


