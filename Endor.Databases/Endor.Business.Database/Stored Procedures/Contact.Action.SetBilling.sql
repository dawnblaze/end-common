IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetBilling')
  DROP PROCEDURE [Contact.Action.SetBilling];
GO

-- ========================================================
-- Name: [Contact.Action.SetBilling]( @BID tinyint, @ContactID int )
--
-- Description: This function sets any existing Billing Contact
-- to false then sets the Contact (@ContactID) to Billing and active
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetBilling] @BID=1, @ContactID=1
-- ========================================================
CREATE PROCEDURE [Contact.Action.SetBilling]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ContactID      INT     -- = 2

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Contact specified is valid
    IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID AND ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR(12),@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @CompanyID INT, @IsCompanyAdHoc BIT;
    SET @CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID=@ContactID);
    SET @IsCompanyAdHoc = (SELECT IsAdHoc FROM [Company.Data] WHERE ID=@CompanyID);

    IF (@IsCompanyAdHoc = 0)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Cannot change Billing contact for AdHoc company.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Contact.Data] WHERE CompanyID=@CompanyID);

	IF (@Count > 1)
	BEGIN
		-- Remove IsBilling from any other Billing Contact(s) for the Company
		UPDATE C
		SET
              IsBilling = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Contact.Data] C
		WHERE BID = @BID AND IsBilling = 1 AND CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID = @ContactID)
	END

    -- Now update it
    UPDATE C
    SET
          IsActive = 1
		, IsBilling = 1
        , ModifiedDT = GetUTCDate()
    FROM [Contact.Data] C
    WHERE BID = @BID AND ID = @ContactID
		AND COALESCE(IsBilling,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END