CREATE TABLE [List.Filter](
    [BID] [smallint] NOT NULL,
    [ID] [int] NOT NULL,
    [ClassTypeID] AS ((1700)),
    [CreatedDate] [date] NOT NULL,
    [ModifiedDT] [datetime2](2) NOT NULL,
    [IsActive] [bit] NOT NULL,
    [Name] [varchar](255) NOT NULL,
    [TargetClassTypeID] [int] NOT NULL,
    [IDs] [xml] NULL,
    [Criteria] [xml] NULL,
    [OwnerID] [smallint] NULL,
    [IsDynamic] AS (isnull(CONVERT([bit],case when [IDs] IS NULL then (1) else (0) end),(1))),
    [IsPublic] [bit] NOT NULL,
    [IsSystem] [bit] NOT NULL,
    [IsDefault] [bit] NOT NULL,
    [SortIndex] [tinyint] NOT NULL,
    [Hint] [varchar](max) SPARSE NULL,
    CONSTRAINT [PK_List.Filter] PRIMARY KEY CLUSTERED (BID, ID)
)
;
CREATE NONCLUSTERED INDEX [IX_List.Filter_TargetCTID] 
ON [List.Filter] ([TargetClassTypeID] ASC, [IsSystem] ASC, [BID] ASC )
;
ALTER TABLE [List.Filter] 
ADD CONSTRAINT [DF_List.Filter_CreatedDate] DEFAULT (getutcdate()) FOR [CreatedDate]
;
ALTER TABLE [List.Filter] 
ADD CONSTRAINT [DF_List.Filter_ModifiedDT] DEFAULT (getutcdate()) FOR [ModifiedDT]
;
ALTER TABLE [List.Filter] 
ADD CONSTRAINT [DF_List.Filter_IsActive] DEFAULT ((1)) FOR [IsActive]
;
ALTER TABLE [List.Filter] 
ADD CONSTRAINT [DF_List.Filter_IsDefault] DEFAULT ((0)) FOR [IsDefault]
;
ALTER TABLE [List.Filter] 
ADD CONSTRAINT [DF_List.Filter_SortIndex] DEFAULT ((50)) FOR [SortIndex]
;
ALTER TABLE [List.Filter] 
ADD CONSTRAINT [DF_List.Filter_IsPublic] DEFAULT ((0)) FOR [IsPublic]
;
ALTER TABLE [List.Filter] 
ADD CONSTRAINT [DF_List.Filter_IsSystem] DEFAULT ((0)) FOR [IsSystem]
;
ALTER TABLE [List.Filter] WITH CHECK 
ADD CONSTRAINT [FK_List.Filter_Employee.Data] FOREIGN KEY([BID], [OwnerID])
REFERENCES [Employee.Data] ([BID], [ID])
;
ALTER TABLE [List.Filter] CHECK CONSTRAINT [FK_List.Filter_Employee.Data]

--#######################################################################--

CREATE TABLE [List.Filter.EmployeeSubscription](
    [BID] [smallint] NOT NULL,
    [FilterID] [int] NOT NULL,
    [EmployeeID] [smallint] NOT NULL,
    [IsFavorite] [bit] NOT NULL,
    [IsShownAsTab] [bit] NOT NULL,
    [SortIndex] [tinyint] NOT NULL,
    CONSTRAINT [PK_List.Filter.Subscription] PRIMARY KEY CLUSTERED (BID, FilterID, EmployeeID)
)
;
CREATE NONCLUSTERED INDEX [IX_List.Filter.Subscription_Employee] 
ON [List.Filter.EmployeeSubscription] ([BID] ASC, [EmployeeID] ASC, [FilterID] ASC )
;
ALTER TABLE [List.Filter.EmployeeSubscription] WITH CHECK 
ADD CONSTRAINT [FK_List.Filter.EmployeeSubscription_Employee.Data] FOREIGN KEY([BID], [EmployeeID])
REFERENCES [Employee.Data] ([BID], [ID])
;
ALTER TABLE [List.Filter.EmployeeSubscription] CHECK CONSTRAINT [FK_List.Filter.EmployeeSubscription_Employee.Data]
;
ALTER TABLE [List.Filter.EmployeeSubscription] WITH CHECK 
ADD CONSTRAINT [FK_List.Filter.EmployeeSubscription_List.Filter] FOREIGN KEY([BID], [FilterID])
REFERENCES [List.Filter] ([BID], [ID])
;
ALTER TABLE [List.Filter.EmployeeSubscription] CHECK CONSTRAINT [FK_List.Filter.EmployeeSubscription_List.Filter]
;
ALTER TABLE [List.Filter.EmployeeSubscription] 
ADD CONSTRAINT [DF_List.Filter.Subscription_IsFavorite] DEFAULT ((0)) FOR [IsFavorite]
;
ALTER TABLE [List.Filter.EmployeeSubscription] 
ADD CONSTRAINT [DF_List.Filter.EmployeeSubscription_IsShownAsTab] DEFAULT ((0)) FOR [IsShownAsTab]
;
ALTER TABLE [List.Filter.EmployeeSubscription] 
ADD CONSTRAINT [DF_List.Filter.EmployeeSubscription_SortIndex] DEFAULT ((50)) FOR [SortIndex]
;

--#######################################################################--

CREATE TABLE [dbo].[System.List.Column](
    [ID] [smallint] IDENTITY(1,1) NOT NULL,
    [ClassTypeID] AS ((1711)),
    [TargetClassTypeID] [int] NULL,
    [Name] [varchar](255) NOT NULL,
    [HeaderText] [varchar](255) NULL,
    [Field] [varchar](255) NULL,
    [IsSortable] [bit] NOT NULL,
    [SortField] [varchar](255) NULL,
    [IsFrozen] [bit] NOT NULL,
    [IsExpander] [bit] NOT NULL,
    [StyleClass] [varchar](255) NULL,
    [SortIndex] [tinyint] NULL,
    [IsVisible] AS (isnull(case when [SortIndex] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0))),
    CONSTRAINT [PK_System.List.Column] PRIMARY KEY CLUSTERED (ID)
)
;
ALTER TABLE [dbo].[System.List.Column] ADD CONSTRAINT [DF_System.List.Column_IsSortable] DEFAULT ((0)) FOR [IsSortable]
;
ALTER TABLE [dbo].[System.List.Column] ADD CONSTRAINT [DF_System.List.Column_IsFrozen] DEFAULT ((0)) FOR [IsFrozen]
;
ALTER TABLE [dbo].[System.List.Column] ADD CONSTRAINT [DF_System.List.Column_IsExpander] DEFAULT ((0)) FOR [IsExpander]
;
ALTER TABLE [dbo].[System.List.Column] ADD CONSTRAINT [DF_Table_1_Index] DEFAULT ((50)) FOR [SortIndex]
;
CREATE NONCLUSTERED INDEX [IX_System.List.Column_TargetCTID] 
ON [dbo].[System.List.Column] ( [TargetClassTypeID], [SortIndex], [Name] )
;

GO

--#######################################################################--

CREATE TABLE [dbo].[System.List.Filter.Criteria](
    [ID] [smallint] IDENTITY(1,1) NOT NULL,
    [ClassTypeID] AS ((1710)),
    [TargetClassTypeID] [int] NULL,
    [Name] [varchar](255) NOT NULL,
    [Label] [varchar](255) NULL,
    [Field] [varchar](255) NULL,
    [IsHidden] [bit] NOT NULL,
    [DataType] [tinyint] NOT NULL,
    [InputType] [tinyint] NOT NULL,
    [AllowMultiple] [bit] NOT NULL,
    [ListValues] [varchar](max) NULL,
    [ListValuesEndpoint] [varchar](255) NULL,
    [IsLimitToList] [bit] NOT NULL,
    [SortIndex] [tinyint] NULL,
    [IsVisible] AS (isnull(case when [SortIndex] IS NULL then CONVERT([bit],(0)) else CONVERT([bit],(1)) end,(0))),
    CONSTRAINT [PK_System.List.Filter.Criteria] PRIMARY KEY CLUSTERED (ID)
)
;
ALTER TABLE [dbo].[System.List.Filter.Criteria] ADD CONSTRAINT [DF_System.List.Filter.Criteria_IsFrozen] DEFAULT ((0)) FOR [IsHidden]
;
ALTER TABLE [dbo].[System.List.Filter.Criteria] ADD CONSTRAINT [DF_System.List.Filter.Criteria_AllowMultiple] DEFAULT ((0)) FOR [AllowMultiple]
;
ALTER TABLE [dbo].[System.List.Filter.Criteria] ADD CONSTRAINT [DF_System.List.Filter.Criteria_IsLimitToList] DEFAULT ((1)) FOR [IsLimitToList]
;
CREATE NONCLUSTERED INDEX [IX_System.List.Filter.Criteria_TargetCTID] 
ON [dbo].[System.List.Filter.Criteria] ( [TargetClassTypeID], [SortIndex], [Name] )
;

--#######################################################################--

IF EXISTS(SELECT * FROM sys.objects WHERE type = 'IF' AND name = 'List.Filter.MyLists')
   DROP FUNCTION [List.Filter.MyLists]
GO

-- =============================================
-- This function returns a users Lists for a particular TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.MyLists](7, 10001, 2000)
--
-- =============================================
-- =============================================
-- This function returns a users Lists for a particular TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.MyLists](7, 10001, 2000)
--
-- =============================================
CREATE FUNCTION [List.Filter.MyLists] (@BID SMALLINT, @EmployeeID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT L.* 
    FROM [List.Filter] L
    WHERE BID = @BID 
      AND ID IN (
                    SELECT L.ID
                    FROM [List.Filter] L
                    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
                      AND (L.IsSystem=1 OR (L.BID = @BID AND L.OwnerID = @EmployeeID)) 
    
                    UNION

                    SELECT L.ID
                    FROM [List.Filter] L
                    JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = L.BID AND LS.FilterID = L.ID
                    WHERE (L.BID = @BID) 
                        AND (L.TargetClassTypeID = @TargetClassTypeID)
                        AND (LS.EmployeeID = @EmployeeID)
                )
)
;

GO 

--#######################################################################--

-- =============================================
-- This function returns a SimpleList of Lists for a particular Employee for a TargetClassTypeID.
--
-- Sample Usage:   SELECT * FROM [List.Filter.SimpleList](7, 10001, 2000)
--
-- =============================================
CREATE FUNCTION [dbo].[List.Filter.SimpleList] (@BID SMALLINT, @EmployeeID INT, @TargetClassTypeID INT)
RETURNS TABLE 
AS
RETURN 
(
    SELECT TOP 100 PERCENT 
           @BID as BID
         , L.[ID]
         , L.[ClassTypeID]
         , L.[Name] as DisplayName
         , L.[IsActive]
         , CONVERT(BIT,0) AS [HasImage]
         , L.TargetClassTypeID
         , (CASE WHEN LS.FilterID IS NOT NULL THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END) AS IsSubscribed
         , LS.IsShownAsTab
         , LS.IsFavorite
         , L.IsSystem
    FROM [List.Filter] L
    LEFT JOIN [List.Filter.EmployeeSubscription] LS ON LS.BID = @BID AND LS.FilterID = L.ID
    WHERE (L.TargetClassTypeID = @TargetClassTypeID)
      AND (L.IsSystem=1 
           OR (L.BID = @BID 
               AND (L.IsPublic=1 OR L.OwnerID = @EmployeeID)
               )
          )
    ORDER BY [Name]
)
