IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Location.Action.SetActiveMultiple')
  DROP PROCEDURE [Location.Action.SetActiveMultiple];
GO

-- ========================================================
-- Name: [Location.Action.SetActiveMultiple]
--
-- Description: This procedure sets the reference location
--
-- Sample Use:   EXEC dbo.[Location.Action.SetActiveMultiple] @BID=1, @LocationIDs='1,2', @IsActive=1
-- ========================================================
CREATE PROCEDURE [Location.Action.SetActiveMultiple]
-- DECLARE 
          @BID            TINYINT		-- = 1
        , @LocationIDs    VARCHAR(1024) -- = '1,2'
        , @IsActive       BIT			-- = 1
        , @Result         INT     = NULL	OUTPUT
AS
BEGIN
	DECLARE @Message VARCHAR(MAX) = '';

	DECLARE @TempTable TABLE (
			  BID SMALLINT NOT NULL
			, LocationId SMALLINT  NULL
			, IsActive BIT  NULL
			, IsSuccess BIT NOT NULL
			, ErrorMessage VARCHAR(1024)
			);

	INSERT INTO @TempTable( BID, LocationID, IsActive, IsSuccess, ErrorMessage)
		SELECT @BID
		     , LData.ID
			 , LData.IsActive
			 , (CASE WHEN LData.ID IS NULL THEN 0 
			         WHEN @IsActive=0 AND IsDefault=1 THEN 0 
					 ELSE  1 END )
			 , (CASE WHEN LData.ID IS NULL THEN 'Location Not Found; ' 
			         WHEN @IsActive=0 AND IsDefault=1 THEN 'Attempting to set the Default Location to Inactive. Set another Location as the Default first before setting this one to Inactive.; '
			         ELSE '' END )
		FROM string_split(@LocationIDs,',') LInput 
		LEFT JOIN [Location.Data] LData ON LData.BID = @BID AND LData.ID = CONVERT(INT, LInput.[value])
	;

	-- Check that we don't have any errors
	SELECT @Message += ErrorMessage
	FROM @TempTable
	WHERE IsSuccess = 0
	;

 	-- Check that some other Location is Active
	IF (@IsActive = 0) 
		AND NOT EXISTS( SELECT * 
						FROM [Location.Data] LData 
						WHERE BID = @BID AND IsActive=1 and ID NOT IN (SELECT LocationID FROM @TempTable) )
		SET @Message = @Message + ' Attempting to set the only Active Location(s) to Inactive. Set another Location to active first before setting this one to Inactive.';


	IF LEN(@Message) > 1 
		THROW 50000, @Message, 1
	ELSE
		-- Now update it
		UPDATE L
		SET   IsActive   = @IsActive
			, ModifiedDT = GetUTCDate()
		FROM [Location.Data] L
		JOIN @TempTable T on L.BID = @BID AND L.ID = T.LocationId
		WHERE COALESCE(L.IsActive,~@IsActive) != @IsActive

	SET @Result = @@ROWCOUNT;

	SELECT @Result AS Result;
END