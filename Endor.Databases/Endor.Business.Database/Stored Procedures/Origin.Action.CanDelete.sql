IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.CanDelete')
  DROP PROCEDURE [Origin.Action.CanDelete];
GO

-- ========================================================
-- Name: [Origin.Action.CanDelete]
--
-- Description: This procedure checks if the origin is in use and can be deleted
--
-- Sample Use:   EXEC dbo.[Origin.Action.CanDelete] @BID=1, @OriginID=1
-- ========================================================
CREATE PROCEDURE [Origin.Action.CanDelete]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @OriginID       INT     -- = 2

        , @Result         BIT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @OriginID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Origin Specified. OriginID='+CONVERT(VARCHAR(12),@OriginID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    SET @Result = 
    CASE WHEN 
        NOT EXISTS(SELECT * FROM [Company.Data] WHERE OriginID = @OriginID)
            --AND NOT EXISTS(SELECT * FROM [Order.Data] WHERE OriginID = @OriginID)
        THEN 1 
        ELSE 0 
    END;

    SELECT @Result as Result;
END