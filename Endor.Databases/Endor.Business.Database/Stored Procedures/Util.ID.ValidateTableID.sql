IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Util.ID.ValidateTableID')
	DROP PROCEDURE [Util.ID.ValidateTableID];
GO
-- ========================================================
-- 
-- Name: Util.ID.ValidateTableID( @BID smallint, @ClassTypeID int, @TableName varchar(155) )
--
-- Description: This Function Gets a New ID for a ClassType (Table).
--  If requesting multiple IDs, the first ID in the series is returned.
--
-- Sample Use:   
--      exec dbo.[Util.ID.ValidateTableID] @BID=100, @ClassTypeID=3500, @TableName='Employee.Data'
--
-- ========================================================
CREATE PROCEDURE [Util.ID.ValidateTableID] 
        @BID smallint
      , @ClassTypeID int
      , @TableName  varchar(155) = '' -- The corresponding tablename.  Only used when @ValidateTable=1
AS
BEGIN
    DECLARE @NextTrackingID INT =  (
                    SELECT NextID
                    FROM [Util.NextID]
                    WHERE BID = @BID AND ClassTypeID = @ClassTypeID
                );

    DECLARE @NextTableID int
            , @cmd Nvarchar(max) = 'SELECT @outvar = MAX(ID)+1 FROM ['+@TableName+'] WHERE BID='+CONVERT(VARCHAR(12), @BID);

    EXEC SP_ExecuteSQL @Query = @cmd
                    , @Params = N'@outvar INT OUTPUT'
                    , @outvar = @NextTableID OUTPUT
    ;

    IF (@NextTrackingID IS NULL)
    BEGIN
        DECLARE @IDDataType VARCHAR(32)
              , @StartingID INT
              ;

        -- The the NextTableID is not high, we need to check if it is below the max
        IF (@NextTableID > 1000)
            SET @StartingID = 1000

        -- Look up the Data Type for the ID field
        ELSE
        BEGIN
            SET @IDDataType = (
                        SELECT DATA_TYPE 
                        FROM INFORMATION_SCHEMA.COLUMNS
                        WHERE TABLE_NAME = @TableName
                        AND COLUMN_NAME = 'ID'
                    );

            -- Set the starting value based on it
            SET @StartingID = (
                        CASE @IDDataType
                            WHEN 'tinyint' THEN 11
                            WHEN 'smallint' THEN 100
                            ELSE 1000
                        END
                    );
        END;

        INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) 
        VALUES( @BID
              , @ClassTypeID
              , IIF( @NextTableID > @StartingID, @NextTableID, @StartingID )
        );

        SELECT @TableName + ' NextID Updated to ' + CONVERT(Varchar(12), IIF( @NextTableID > 1000, @NextTableID, @StartingID ) )
        ;
    END

    ELSE IF (@NextTableID > @NextTrackingID)
    BEGIN
        UPDATE [Util.NextID]
        SET NextID = @NextTableID
        WHERE BID = @BID AND ClassTypeID = @ClassTypeID
        ;
        SELECT @TableName + ' NextID Updated to ' + CONVERT(Varchar(12), @NextTableID )
        ;
    END
    ELSE
        SELECT @TableName + ' Ok ';
END;