IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Location.Action.SetDefault')
  DROP PROCEDURE [Location.Action.SetDefault];
GO

-- ========================================================
-- Name: [Location.Action.SetDefault]( @BID tinyint, @LocationID int )
--
-- Description: This function sets any existing default location
-- to false then sets the location (@LocationID) to default and active
--
-- Sample Use:   EXEC dbo.[Location.Action.SetDefault] @BID=1, @LocationID=1
-- ========================================================
CREATE PROCEDURE [Location.Action.SetDefault]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @LocationID     INT     -- = 2

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	DECLARE @Count INT = (SELECT COUNT(ID) FROM dbo.[Location.Data]);

	IF (@Count > 1)
	BEGIN
		-- Remove IsDefault from any other Default location(s) for the business
		UPDATE L
		SET IsDefault = 0
			, ModifiedDT = GETUTCDATE()
		FROM [Location.Data] L
		WHERE BID = @BID AND IsDefault = 1
	END

    -- Now update it
    UPDATE L
    SET IsActive   = 1
		, IsDefault = 1
        , ModifiedDT = GetUTCDate()
    FROM [Location.Data] L
    WHERE BID = @BID and ID = @LocationID
		AND COALESCE(IsDefault,~1) != 1

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END