IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Company.Action.SetActive')
  DROP PROCEDURE [Company.Action.SetActive];
GO

-- ========================================================
-- Name: [Company.Action.SetActive]
--
-- Description: This procedure sets the referenced Company's active status
--
-- Sample Use:   EXEC dbo.[Company.Action.SetActive] @BID=1, @CompanyID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [Company.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @CompanyID      INT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Company specified is valid
    IF NOT EXISTS(SELECT * FROM [Company.Data] WHERE BID = @BID and ID = @CompanyID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Company Specified. CompanyID='+CONVERT(VARCHAR(12),@CompanyID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update it
    UPDATE C
    SET   
        IsActive = @IsActive
      , ModifiedDT = GetUTCDate()
    FROM [Company.Data] C
    WHERE BID = @BID and ID = @CompanyID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END