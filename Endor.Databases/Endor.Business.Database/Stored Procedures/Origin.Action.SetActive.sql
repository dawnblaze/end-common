IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Origin.Action.SetActive')
  DROP PROCEDURE [Origin.Action.SetActive];
GO

-- ========================================================
-- Name: [Origin.Action.SetActive]
--
-- Description: This procedure sets the referenced Origin's active status
--
-- Sample Use:   EXEC dbo.[Origin.Action.SetActive] @BID=1, @OriginID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [Origin.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @OriginID       INT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Origin specified is valid
    IF NOT EXISTS(SELECT * FROM [CRM.Origin] WHERE BID = @BID and ID = @OriginID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Origin Specified. OriginID='+CONVERT(VARCHAR(12),@OriginID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Update it
    UPDATE O
    SET   
        IsActive = @IsActive
      , ModifiedDT = GetUTCDate()
    FROM [CRM.Origin] O
    WHERE BID = @BID and ID = @OriginID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END