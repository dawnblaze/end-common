IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Util.ID.GetID')
	DROP PROCEDURE [Util.ID.GetID];
GO
-- ========================================================
-- 
-- Name: Util.ID.GetID( @BID smallint, @ClassTypeID int, @Count int = 1 )
--
-- Description: This Function Gets a New ID for a ClassType (Table).
--  If requesting multiple IDs, the first ID in the series is returned.
--
-- Sample Use:   
--      declare @NewID int;
--      exec @NewID = dbo.[Util.ID.GetID] @BID=100, @ClassTypeID=3500, @Count=1
--
-- for Byte IDs, use
--      exec @NewID = dbo.[Util.ID.GetID] @BID=100, @ClassTypeID=3500, @StartingID=10
-- ========================================================
CREATE PROCEDURE [Util.ID.GetID] 
        @BID smallint
      , @ClassTypeID int

      -- Optional fields
      , @Count      int = 1   -- how many IDs are requested.  The first ID in a sequential block is returned.
      , @StartingID int = 1000
AS
BEGIN
    DECLARE @NextID INT;
    IF (@BID IS NULL) SET @BID = -1; -- Use -1 for System

    -- Assume the record exists ... 
    UPDATE [Util.NextID]
    SET @NextID = NextID = COALESCE(NextID, @StartingID-1) + @Count
    WHERE BID = @BID AND ClassTypeID = @ClassTypeID
    ;

    -- Check if no rows found, in which case use an add
    IF (@@RowCount=0)
    BEGIN
        SET @NextID = @StartingID + @Count;
        INSERT INTO [Util.NextID] (BID, ClassTypeID, NextID) VALUES(@BID, @ClassTypeID, @NextID);
    END;

    SELECT (@NextID-@Count) as FirstID;
    RETURN (@NextID-@Count);
END;