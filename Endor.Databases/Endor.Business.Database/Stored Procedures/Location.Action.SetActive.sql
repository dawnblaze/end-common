IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Location.Action.SetActive')
  DROP PROCEDURE [Location.Action.SetActive];
GO

-- ========================================================
-- Name: [Location.Action.SetActive]
--
-- Description: This procedure sets the reference location
--
-- Sample Use:   EXEC dbo.[Location.Action.SetActive] @BID=1, @LocationID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [Location.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @LocationID     INT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that some other Location is Active
    IF (@IsActive = 0) AND NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID != @LocationID and IsActive=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to set the only Active Location to Inactive. Set another Location to active first before setting this one to Inactive.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that this is not the default location
    IF (@IsActive = 0) AND EXISTS(SELECT * FROM [Location.Data] WHERE BID=@BID and ID=@LocationID and IsDefault=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to set the Default Location to Inactive. Set another Location as the Default first before setting this one to Inactive.'
             ;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Location.Data] L
    WHERE BID = @BID and ID = @LocationID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END