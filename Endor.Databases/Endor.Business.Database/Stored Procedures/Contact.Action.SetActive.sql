IF EXISTS(SELECT * FROM sys.objects WHERE type = 'P' AND name = 'Contact.Action.SetActive')
  DROP PROCEDURE [Contact.Action.SetActive];
GO

-- ========================================================
-- Name: [Contact.Action.SetActive]
--
-- Description: This procedure sets the referenced Contact's active status
--
-- Sample Use:   EXEC dbo.[Contact.Action.SetActive] @BID=1, @ContactID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [Contact.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @ContactID      INT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the Contact specified is valid
    IF NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID and ID = @ContactID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Contact Specified. ContactID='+CONVERT(VARCHAR(12),@ContactID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    DECLARE @CompanyID INT, @IsCompanyAdHoc BIT;
    SET @CompanyID = (SELECT CompanyID FROM [Contact.Data] WHERE ID=@ContactID);
    SET @IsCompanyAdHoc = (SELECT IsAdHoc FROM [Company.Data] WHERE ID=@CompanyID);

    -- Check that some other Contact is Active if Company is NOT AdHoc
    IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND NOT EXISTS(SELECT * FROM [Contact.Data] WHERE BID = @BID
        AND ID != @ContactID AND IsActive = 1 AND CompanyID != (SELECT CompanyID FROM [Contact.Data] WHERE ID = @ContactID))
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to set the only Active Contact to Inactive. Set another Contact to active first before setting this one to Inactive.'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that this is not the default Contact if Company is NOT AdHoc
    IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND EXISTS(SELECT * FROM [Contact.Data] WHERE BID=@BID AND ID=@ContactID AND IsDefault=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to set the Default Contact to Inactive. Set another Contact as the Default first before setting this one to Inactive.'
             ;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check that this is not the billing Contact if Company is NOT AdHoc
    IF (@IsActive = 0 AND @IsCompanyAdHoc = 0) AND EXISTS(SELECT * FROM [Contact.Data] WHERE BID=@BID AND ID=@ContactID AND IsBilling=1)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Attempting to set the Billing Contact to Inactive. Set another Contact as the Billing first before setting this one to Inactive.'
             ;
        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE C
    SET   
          IsActive = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Contact.Data] C
    WHERE BID = @BID and ID = @ContactID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    -- If associated company is AdHoc, update it as well
    IF (@IsCompanyAdHoc = 1)
    BEGIN
        UPDATE Co
        SET
              IsActive = @IsActive
            , ModifiedDT = GetUTCDate()
        FROM [Company.Data] Co
        WHERE BID = @BID AND ID = @CompanyID
            AND COALESCE(IsActive,~1) != @IsActive
    END

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END