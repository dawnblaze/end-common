declare @BID as smallint = 1;
declare @CT as varchar(128) = 'location';
declare @CTID as int = (SELECT TOP 1 ID as lName FROM dbo.[enum.ClassType] where lower(Name) = lower(@CT));
print('Seeding '+@CT+' List.Filter for BID: '+CAST(@BID as varchar(10)))

declare @newID as int;
EXEC @newID = [dbo].[Util.ID.GetID] @BID, @CTID, 1;

print('adding ALL filter')
insert into [List.Filter]
(
BID, ID, CreatedDate, ModifiedDT, IsActive, [Name], TargetClassTypeID, IsPublic, IsSystem, IsDefault, SortIndex
)
VALUES
(
@BID,
@newID,
GETUTCDATE(),
GETUTCDATE(),
1,
'All',
@CTID,
1,1,1,50
)

--select * from [List.Filter] where BID = @bid and ID = @newID;


print('adding Active filter')
EXEC @newID = [dbo].[Util.ID.GetID] @BID, @CTID, 1;
DECLARE @xml as xml = (SELECT 'Active' as Label, 'True' as SearchValue, 'IsActive' as Field, 'Active' as DisplayText, 'false' as IsHidden FOR XML Path('ListFilterItem'), root ('ArrayOfListFilterItem'))
select @xml;

insert into [List.Filter]
(
BID, ID, CreatedDate, ModifiedDT, IsActive, [Name], TargetClassTypeID, IsPublic, IsSystem, IsDefault, SortIndex
,Criteria
)
VALUES
(
@BID,
@newID,
GETUTCDATE(),
GETUTCDATE(),
1,
'Active',
@CTID,
1,1,1,50
,@xml
)

--select * from [List.Filter] where BID = @bid and ID = @newID