CREATE TABLE [enum.Accounting.PaymentDueBasedOnType](
    [ID] [tinyint] NOT NULL,
    [Name] [varchar](100) NOT NULL,
    CONSTRAINT [PK_enum.Accounting.PaymentDueBasedOnType] PRIMARY KEY CLUSTERED (ID)
);
 
INSERT [enum.Accounting.PaymentDueBasedOnType] ([ID], [Name])
VALUES (0, N'Days After Invoice')
    , (1, N'Days After Built')
    , (2, N'Days After Ordered')
    , (3, N'Days After End of Invoice Month')
    , (4, N'Day of the Month')
    , (5, N'Day of the Following Month')
;