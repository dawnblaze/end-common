/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [Dev.Endor.Business.DB1]
GO
DELETE FROM [dbo].[System.List.Filter.Criteria] WHERE TargetClassTypeID = 5000
GO
SET IDENTITY_INSERT [dbo].[System.List.Filter.Criteria] ON 
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (1, 5000, N'PhoneNumber', N'Phone Number', N'PhoneNumber', 0, 0, 0, 0, NULL, NULL, 0, NULL)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (2, 5000, N'Name', N'Name', N'FullName', 0, 0, 0, 0, NULL, NULL, 0, 0)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (3, 5000, N'Is Active', N'Is Active', N'IsActive', 0, 3, 2, 0, NULL, NULL, 1, NULL)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (4, 5000, N'Email', N'Email', N'EmailAddress', 0, 0, 0, 0, NULL, NULL, 0, NULL)
GO
INSERT [dbo].[System.List.Filter.Criteria] ([ID], [TargetClassTypeID], [Name], [Label], [Field], [IsHidden], [DataType], [InputType], [AllowMultiple], [ListValues], [ListValuesEndpoint], [IsLimitToList], [SortIndex]) VALUES (5, 5000, N'Location', N'Location', N'LocationID', 0, 1, 4, 0, NULL, NULL, 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[System.List.Filter.Criteria] OFF
GO
