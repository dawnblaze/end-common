-- ========================================================
-- Name: [Accounting.Tax.Group.Action.LinkLocation]
--
-- Description: This procedure links/unlinks the Location to the TaxGroup
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.LinkLocation] @BID=1, @TaxGroupID=1, @LocationID=1, @IsLinked=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.LinkLocation]
--DECLARE 
          @BID            TINYINT  --= 1
        , @TaxGroupID     SMALLINT --= 2
		, @LocationID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the Location specified is valid
    IF NOT EXISTS(SELECT * FROM [Location.Data] WHERE BID = @BID and ID = @LocationID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid Location Specified. LocationID='+CONVERT(VARCHAR(12),@LocationID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.LocationLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID and GroupID = @TaxGroupID and LocationID = @LocationID)
		BEGIN
			INSERT INTO [Accounting.Tax.Group.LocationLink] (BID, GroupID, LocationID)
			VALUES (@BID, @TaxGroupID, @LocationID)
		END;

		-- Check location default taxgroup and add taxgroup ID if null
		IF EXISTS (SELECT * FROM [Location.Data] WHERE ID = @LocationID and DefaultTaxGroupID IS NULL)
		BEGIN
			UPDATE [Location.Data]
			SET DefaultTaxGroupID = @TaxGroupID
			WHERE ID = @LocationID and DefaultTaxGroupID IS NULL
		END
	END
	ELSE
	BEGIN
		-- Remove entry from Tax.Group.LocationLink
		DELETE FROM [Accounting.Tax.Group.LocationLink] WHERE BID = @BID and GroupID = @TaxGroupID and LocationID = @LocationID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END