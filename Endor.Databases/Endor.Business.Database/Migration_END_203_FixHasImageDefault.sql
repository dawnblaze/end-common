-- END-203 - Fix HasImage always defaulting to true on saving.
ALTER TABLE [dbo].[Employee.Data] DROP CONSTRAINT [DF_Employee.Data_HasImage]
ALTER TABLE [dbo].[Location.Data] DROP CONSTRAINT [DF__Business.__HasIm__160F4887]
ALTER TABLE [dbo].[Employee.Data] ADD CONSTRAINT [DF_Employee.Data_HasImage] DEFAULT ((0)) FOR [HasImage]
ALTER TABLE [dbo].[Location.Data] ADD CONSTRAINT [DF__Business.__HasIm__160F4887] DEFAULT ((0)) FOR [HasImage]
