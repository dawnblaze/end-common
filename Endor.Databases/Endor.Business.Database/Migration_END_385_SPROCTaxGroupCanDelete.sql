/* 
========================================================
    Name: [Accounting.Tax.Group.Action.CanDelete]

    Description: This procedure checks if the TaxGroup is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Tax.Group.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the TaxGroup can be deleted. The boolean response is returned in the body.  A TaxGroup can be deleted if
	It is not the default for a Location. Location.Data.DefaultTaxGroupID
	It is not the default for a Company. Company.Data.TaxGroupID
	Is it not used by any order. (?)
	It is not used by the GL.Data.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group Specified Does not Exist. TaxGroupID=', @ID)

    -- It is not the default for a Location. Location.Data.DefaultTaxGroupID
	-- It is not the default for a Company. Company.Data.TaxGroupID
	ELSE IF  EXISTS( SELECT * FROM [Location.Data] WHERE BID = @BID AND DefaultTaxGroupID = @ID )
             OR EXISTS( SELECT * FROM [Company.Data] WHERE BID = @BID AND TaxGroupID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group is used in the Location and Company Data. TaxGroupID=', @ID)

	-- Is it not used by any order. (?)
	-- FUTURE

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END