INSERT INTO [System.List.Filter.Criteria]
(TargetClassTypeID, Name, Label, Field, IsHidden, DataType, InputType, AllowMultiple, ListValues, IsLimitToList, SortIndex)
VALUES (11105, 'Name','Name','Name',0,0,0,0,NULL,0,0);

INSERT INTO [System.List.Filter.Criteria]
(TargetClassTypeID, Name, Label, Field, IsHidden, DataType, InputType, AllowMultiple, ListValues, IsLimitToList, SortIndex)
VALUES (11105, 'Is Active','Is Active','IsActive',0,3,2,0,'Is Not Active,Is Active',1,1);

INSERT INTO [dbo].[List.Filter]
([BID],[ID],[CreatedDate],[ModifiedDT],[IsActive],[Name],[TargetClassTypeID],[IDs],[Criteria],[OwnerID],[IsPublic],[IsSystem],[Hint],[IsDefault],[SortIndex])
VALUES
(1,1018,'2018-2-27','2018-2-27',1,'Active',11105,null,'<ArrayOfListFilterItem>
	<ListFilterItem>
		<SearchValue>true</SearchValue>
		<Field>IsActive</Field>
		<IsHidden>true</IsHidden>
		<IsSystem>true</IsSystem>
		<DisplayText>Is Active</DisplayText>
	</ListFilterItem>
</ArrayOfListFilterItem>',null,0,1,null,1,0);