
-- ========================================================
-- Name: [Accounting.Tax.Group.Action.SetActive]
--
-- Description: This procedure sets the TaxGroup to active or inactive
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.SetActive] @BID=1, @TaxGroupID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @TaxGroupID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.Tax.Group] L
    WHERE BID = @BID and ID = @TaxGroupID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END