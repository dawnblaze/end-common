-- ========================================================
-- Name: [Accounting.Payment.Term.Action.SetActive]
--
-- Description: This procedure sets the PaymentTerm to active or inactive
--
-- Sample Use:   EXEC dbo.[Accounting.Payment.Term.Action.SetActive] @BID=1, @PaymentTermID=1, @IsActive=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Payment.Term.Action.SetActive]
-- DECLARE 
          @BID            TINYINT -- = 1
        , @PaymentTermID     SMALLINT     -- = 2

        , @IsActive       BIT     = 1

        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the PaymentTerm specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Payment.Term] WHERE BID = @BID and ID = @PaymentTermID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid PaymentTerm Specified. PaymentTermID='+CONVERT(VARCHAR(12),@PaymentTermID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Now update it
    UPDATE L
    SET IsActive   = @IsActive
        , ModifiedDT = GetUTCDate()
    FROM [Accounting.Payment.Term] L
    WHERE BID = @BID and ID = @PaymentTermID
      AND COALESCE(IsActive,~@IsActive) != @IsActive

    SET @Result = @@ROWCOUNT;

    SELECT @Result as Result;
END