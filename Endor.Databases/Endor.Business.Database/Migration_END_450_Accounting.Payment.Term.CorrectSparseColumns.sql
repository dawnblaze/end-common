
IF COL_LENGTH('[dbo].[Accounting.Payment.Term]', 'EarlyPaymentPercent') IS NOT NULL
BEGIN
	ALTER TABLE [Accounting.Payment.Term] ALTER COLUMN
		[EarlyPaymentPercent]    DECIMAL (12, 4) SPARSE NULL
END

IF COL_LENGTH('[dbo].[Accounting.Payment.Term]', 'EarlyPaymentDays') IS NOT NULL
BEGIN
	ALTER TABLE [Accounting.Payment.Term] ALTER COLUMN
		[EarlyPaymentDays]        SMALLINT SPARSE        NULL
END
