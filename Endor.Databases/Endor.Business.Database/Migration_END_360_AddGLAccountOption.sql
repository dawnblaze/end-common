  INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
  VALUES (9, 'Chart Of Accounts', 1, 'GLAccount Options', 4, 0)

  INSERT INTO [System.Option.Definition] (ID, Name, Label, Description, DataType, CategoryID, ListValues, DefaultValue, IsHidden)
  VALUES (39, 'Accounting.GLAccount.ShowNumberedNames', 'Use Numbered Accounts', NULL, 3, 9, NULL, 0, 0)