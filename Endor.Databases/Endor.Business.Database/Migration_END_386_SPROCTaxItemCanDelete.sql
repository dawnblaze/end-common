/* 
========================================================
    Name: [Accounting.Tax.Item.Action.CanDelete]

    Description: This procedure checks if the TaxItem is in use and can be deleted

    Sample Use:   
        EXEC dbo.[Accounting.Tax.Item.Action.CanDelete] @BID=1, @ID=1200, @ShowCantDeleteReason=1
======================================================== 
*/

CREATE PROCEDURE [dbo].[Accounting.Tax.Item.Action.CanDelete]
          @BID                  TINYINT 
        , @ID                   INT    

        , @Result               BIT     = NULL  OUTPUT
        , @CantDeleteReason     VARCHAR(1024) = NULL OUTPUT
        , @ShowCantDeleteReason BIT = 0
AS
BEGIN

/* 
	Checks if the TaxItem can be deleted. The boolean response is returned in the body.  A TaxItem can be deleted if
	It is not linked to a TaxGroup.
*/

    -- Check if the record exists
    IF NOT EXISTS( SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID AND ID = @ID ) 

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Group Specified Does not Exist. TaxItemID=', @ID)

    -- It is not linked to a TaxGroup.
	ELSE IF  EXISTS( SELECT * FROM [Accounting.Tax.Group.AssessmentLink] WHERE BID = @BID AND AssessmentID = @ID )

        SELECT @Result = 0
             , @CantDeleteReason = CONCAT('Tax Item is used in the Tax Group Data. TaxItemID=', @ID)

    ELSE
        SET @Result = 1;

    IF (@Result = 0) AND (@ShowCantDeleteReason=1) 
        SELECT @Result as Result, @CantDeleteReason as [Can't Delete Reason]

    ELSE
        SELECT @Result as Result;
END