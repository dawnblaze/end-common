-- ========================================================
-- Name: [Accounting.Tax.Group.Action.LinkTaxItem]
--
-- Description: This procedure links/unlinks the TaxItem to the TaxGroup
--
-- Sample Use:   EXEC dbo.[Accounting.Tax.Group.Action.LinkTaxItem] @BID=1, @TaxGroupID=1, @TaxItemID=1, @IsLinked=1
-- ========================================================
CREATE PROCEDURE [dbo].[Accounting.Tax.Group.Action.LinkTaxItem]
--DECLARE 
          @BID            TINYINT  --= 1
        , @TaxGroupID     SMALLINT --= 2
		, @TaxItemID      SMALLINT --= 1
        , @IsLinked       BIT     = 1
        , @Result         INT     = NULL  OUTPUT
AS
BEGIN
    DECLARE @Message VARCHAR(1024);

    -- Check if the TaxGroup specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group] WHERE BID = @BID and ID = @TaxGroupID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxGroup Specified. TaxGroupID='+CONVERT(VARCHAR(12),@TaxGroupID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

    -- Check if the TaxItem specified is valid
    IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Item] WHERE BID = @BID and ID = @TaxItemID)
    BEGIN
        SELECT @Result = 0
             , @Message = 'Invalid TaxItem Specified. TaxItemID='+CONVERT(VARCHAR(12),@TaxItemID)+' not found'
             ;

        THROW 50000, @Message, 1;
        RETURN @Result;
    END;

	IF @IsLinked = 1
	BEGIN
		-- Add new entry to Tax.Group.AssessmentLink if link is not yet found
		IF NOT EXISTS(SELECT * FROM [Accounting.Tax.Group.AssessmentLink] WHERE BID = @BID and GroupID = @TaxGroupID and AssessmentID = @TaxItemID)
		BEGIN
			INSERT INTO [Accounting.Tax.Group.AssessmentLink] (BID, GroupID, AssessmentID)
			VALUES (@BID, @TaxGroupID, @TaxItemID)
		END;
	END
	ELSE
	BEGIN
		-- Remove entry from Tax.Group.AssessmentLink
		DELETE FROM [Accounting.Tax.Group.AssessmentLink] WHERE BID = @BID and GroupID = @TaxGroupID and AssessmentID = @TaxItemID
	END

    SET @Result = 1;

    SELECT @Result as Result;
END