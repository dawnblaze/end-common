
  --Payment Section
  INSERT INTO [System.Option.Section] (ID, Name, ParentID, IsHidden, Depth, SearchTerms)
  VALUES (11, 'Payment', 1, 0, 2, 'Payment')

  --Payment Type Category
  INSERT INTO [System.Option.Section] (ID, Name, ParentID, IsHidden, Depth, SearchTerms)
  VALUES (12, 'Payment Type', 11, 0, 3, 'Payment Type, Type, Deposit, Credit Card, Mastercard, Visa, AmEx, Check, Cash, Method, Payment Method')

  INSERT INTO [System.Option.Category] (ID, Name, SectionID, Description, OptionLevels, IsHidden)
  VALUES (10, 'Accounting.Defaults', 11, '', 4, 0)

  INSERT INTO [System.Option.Definition] (ID, Name, Label, Description, DataType, CategoryID, ListValues, DefaultValue, IsHidden)
  VALUES (40, 'Accounting.PaymentTerm.DefaultID', 'Payment Term Default ID', 'The PaymentTermID default to use for this business.', 1, 10, NULL, '', 0)