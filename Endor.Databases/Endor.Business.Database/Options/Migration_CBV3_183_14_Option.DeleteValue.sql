USE [Dev.Endor.Business.DB1]
GO
/****** Object:  StoredProcedure [dbo].[Option.DeleteValue]    Script Date: 12/15/2017 3:58:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- 
-- Name: [Option.DeleteValue]
--
-- Description: This procedure deletes the value for an option/setting.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.DeleteValue] 
        -- Require Fields
          @OptionName       = 'GLAccount.TaxName2'
        , @OptionID         = NULL

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @EmployeeID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

    SELECT * FROM [Option.Data]
*/
-- ========================================================

ALTER PROCEDURE [dbo].[Option.DeleteValue]
-- DECLARE 
            @OptionID       INT          = NULL
          , @OptionName     VARCHAR(255) = NULL

          , @AssociationID  TINYINT      = NULL
          , @BID            SMALLINT     = NULL
          , @LocationID     SMALLINT     = NULL
          , @EmployeeID     SMALLINT     = NULL
          , @CompanyID      INT          = NULL
          , @ContactID      INT          = NULL
          , @StorefrontID   SMALLINT	 = NULL

AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
          , @Level  TINYINT     
          ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition] 
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
             , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

	DELETE FROM [Option.Data] WHERE Value = @Result AND OptionLevel = @Level AND OptionID = @OptionID

	IF ((SELECT COUNT(*) FROM [Option.Data] WHERE OptionID = @OptionID) = 0)
	BEGIN
		-- Clear out definition if none in data
		DELETE FROM [System.Option.Definition] WHERE ID = @OptionID
	END

	
END