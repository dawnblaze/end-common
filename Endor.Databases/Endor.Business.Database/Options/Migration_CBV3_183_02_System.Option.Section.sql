CREATE TABLE [System.Option.Section](
    [ID] [smallint] NOT NULL,
    [Name] [varchar](255) NOT NULL,
    [ParentID] [smallint] NULL,
    [IsHidden] [bit] NOT NULL,
    [Depth] [tinyint] NOT NULL,
    CONSTRAINT [PK_System.Option.Section] PRIMARY KEY CLUSTERED (ID)
);


CREATE NONCLUSTERED INDEX [IX_System.Option.Section_ParentID] ON [System.Option.Section]
( [ParentID] ASC, [Name] ASC )
;

ALTER TABLE [System.Option.Section] ADD  CONSTRAINT [DF_System.Option.Section_IsHidden]  DEFAULT ((0)) FOR [IsHidden];
ALTER TABLE [System.Option.Section] ADD  CONSTRAINT [DF_System.Option.Section_Depth]  DEFAULT ((0)) FOR [Depth];
ALTER TABLE [System.Option.Section]  WITH CHECK ADD  CONSTRAINT [FK_System.Option.Section_System.Option.Section] FOREIGN KEY([ParentID])
REFERENCES [System.Option.Section] ([ID]);
ALTER TABLE [System.Option.Section] CHECK CONSTRAINT [FK_System.Option.Section_System.Option.Section];

-- SAMPLE DATA
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth]) VALUES (1, N'Accounting', NULL, 0, 0);
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth]) VALUES (2, N'Tax Accounting', 1, 0, 1);
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth]) VALUES (3, N'Income Accounting', 1, 0, 1);
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth]) VALUES (4, N'Expense Accounting', 1, 0, 1);
INSERT [System.Option.Section] ([ID], [Name], [ParentID], [IsHidden], [Depth]) VALUES (5, N'Domain Settings', NULL, 0, 0);

INSERT INTO [System.Option.Section]
([ID], [Name], [ParentID], [IsHidden], [Depth])
Values
(6,	'Office & Facilities',	NULL,	0,	0),
(7,	'Ordering',	NULL,	0,	0),
(8,	'Pricing',	NULL,	0,	0),
(9,	'Invoice',	NULL,	0,	0)