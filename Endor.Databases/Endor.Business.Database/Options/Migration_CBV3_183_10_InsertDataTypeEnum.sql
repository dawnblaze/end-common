IF NOT EXISTS (SELECT TOP 1 * FROM [dbo].[enum.CustomField.DataType] WHERE [Name] LIKE 'Date' AND ID = 7)
	INSERT INTO [dbo].[enum.CustomField.DataType] VALUES (7, 'Date')

IF NOT EXISTS (SELECT TOP 1 * FROM [dbo].[enum.CustomField.DataType] WHERE [Name] LIKE 'Time' AND ID = 8)
	INSERT INTO [dbo].[enum.CustomField.DataType] VALUES (8, 'Time')

IF NOT EXISTS (SELECT TOP 1 * FROM [dbo].[enum.CustomField.DataType] WHERE [Name] LIKE 'DateTime' AND ID = 9)
	INSERT INTO [dbo].[enum.CustomField.DataType] VALUES (9, 'DateTime')