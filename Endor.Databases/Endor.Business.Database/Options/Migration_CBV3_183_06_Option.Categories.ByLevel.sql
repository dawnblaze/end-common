CREATE FUNCTION [dbo].[Option.Categories.ByLevel] ( @Level tinyint )
		RETURNS TABLE
AS

RETURN 

SELECT
TOP 1000
	a.[ID]
	,a.[Name]
	,a.[SectionID]
	,a.[Description]
	,a.[OptionLevels]
	,a.[IsHidden]
	,a.[IsSystemOption]
	,a.[IsAssociationOption]
	,a.[IsBusinessOption]
	,a.[IsLocationOption]
	,a.[IsStorefrontOption]
	,a.[IsEmployeeOption]
	,a.[IsCompanyOption]
	,a.[IsContactOption]
FROM [dbo].[System.Option.Category] a
WHERE OptionLevels = @Level
ORDER BY a.[Name] ASC