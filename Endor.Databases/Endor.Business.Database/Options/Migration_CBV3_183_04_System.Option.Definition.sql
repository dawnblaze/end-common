CREATE TABLE [System.Option.Definition](
    [ID] [smallint] NOT NULL,
    [Name] [varchar](255) NOT NULL,
    [Label] [varchar](255) NOT NULL,
    [Description] [varchar](max) NULL,
    [DataType] [tinyint] NOT NULL,
    [CategoryID] [smallint] NOT NULL,
    [ListValues] [varchar](max) NULL,
    [DefaultValue] [varchar](max) NULL,
    [IsHidden] [bit] NOT NULL,
    CONSTRAINT [PK_System.Option.Definition] PRIMARY KEY CLUSTERED (ID)
)
;
ALTER TABLE [System.Option.Definition] ADD  CONSTRAINT [DF_System.Option.Definition_DataType]  DEFAULT ((0)) FOR [DataType]
;
ALTER TABLE [System.Option.Definition] ADD  CONSTRAINT [DF_System.Option.Definition_IsHidden]  DEFAULT ((0)) FOR [IsHidden]
;
CREATE NONCLUSTERED INDEX [IX_System.Option.Definition_CategoryID] ON [System.Option.Definition]
( [CategoryID] ASC, [Name] ASC, [IsHidden] ASC )
;

CREATE NONCLUSTERED INDEX [IX_System.Option.Definition_Name] ON [System.Option.Definition]
( [Name] ASC, [CategoryID] ASC, [IsHidden] ASC )
;

--
-- SAMPLE DATA FOR TESTING
--
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES (1, N'GLAccount.Discount', N'Discount Account', N'Select the GL', 0, 1, NULL, N'1080', 0)
;
INSERT [System.Option.Definition] ([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden]) VALUES (2, N'GLAccount.TaxName1', N'Sales Tax #1 Name', N'Enter the name used for the highest category of sales tax', 0, 1, NULL, N'GST', 0)
;


insert into [System.Option.Definition] 
([ID], [Name], [Label], [Description], [DataType], [CategoryID], [ListValues], [DefaultValue], [IsHidden])
Values 
(3,	    'InvoicePrefix',	            'Invoice Prefix',	    NULL,	0,	6,	NULL,	NULL,	0),
(4,	    'InvoiceNextNumber',	        'Next Invoice Number',	NULL,	1,	6,	NULL,	NULL,	0),
(5,	    'InvoiceVersionDisplayFormat',	'Version Numbering',	NULL,	1,	6,	NULL,	0,	    0),

(10,	'Hours.Sales.Day0.Open',	'Hours Sales Day 0 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(11,	'Hours.Sales.Day1.Open',	'Hours Sales Day 1 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(12,	'Hours.Sales.Day2.Open',	'Hours Sales Day 2 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(13,	'Hours.Sales.Day3.Open',	'Hours Sales Day 3 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(14,	'Hours.Sales.Day4.Open',	'Hours Sales Day 4 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(15,	'Hours.Sales.Day5.Open',	'Hours Sales Day 5 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(16,	'Hours.Sales.Day6.Open',	'Hours Sales Day 6 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(17,	'Hours.Sales.Day0.Close',	'Hours Sales Day 0 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(18,	'Hours.Sales.Day1.Close',	'Hours Sales Day 1 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(19,	'Hours.Sales.Day2.Close',	'Hours Sales Day 2 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(20,	'Hours.Sales.Day3.Close',	'Hours Sales Day 3 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(21,	'Hours.Sales.Day4.Close',	'Hours Sales Day 4 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(22,	'Hours.Sales.Day5.Close',	'Hours Sales Day 5 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(23,	'Hours.Sales.Day6.Close',	'Hours Sales Day 6 Close',	NULL,	0,	6,	NULL,	'08:00',1),

(24,	'Hours.Production.Day0.Open',	'Hours Production Day 0 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(25,	'Hours.Production.Day1.Open',	'Hours Production Day 1 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(26,	'Hours.Production.Day2.Open',	'Hours Production Day 2 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(27,	'Hours.Production.Day3.Open',	'Hours Production Day 3 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(28,	'Hours.Production.Day4.Open',	'Hours Production Day 4 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(29,	'Hours.Production.Day5.Open',	'Hours Production Day 5 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(30,	'Hours.Production.Day6.Open',	'Hours Production Day 6 Open',	NULL,	0,	6,	NULL,	'08:00',1),
(31,	'Hours.Production.Day0.Close',	'Hours Production Day 0 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(32,	'Hours.Production.Day1.Close',	'Hours Production Day 1 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(33,	'Hours.Production.Day2.Close',	'Hours Production Day 2 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(34,	'Hours.Production.Day3.Close',	'Hours Production Day 3 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(35,	'Hours.Production.Day4.Close',	'Hours Production Day 4 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(36,	'Hours.Production.Day5.Close',	'Hours Production Day 5 Close',	NULL,	0,	6,	NULL,	'08:00',1),
(37,	'Hours.Production.Day6.Close',	'Hours Production Day 6 Close',	NULL,	0,	6,	NULL,	'08:00',1)