DROP PROCEDURE IF EXISTS [Option.GetSection];
GO
-- ========================================================
-- 
-- Name: [Option.GetSections]
--
-- Description: This procedure gets the heirarchy for sections and categories.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.GetSections] 
        -- Require Fields
          @SectionID       = 1
		  @SectionName     = 'Accounting'
*/
-- ========================================================

CREATE PROCEDURE [Option.GetSection]
-- DECLARE 
            @SectionID        INT          = NULL
			,@SectionName     VARCHAR(255) = NULL
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@SectionID IS NULL) AND (@SectionName IS NULL)) OR ((@SectionID IS NOT NULL) AND (@SectionName IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @SectionID or the @SectionName.', 1;

    -- ======================================
    -- Lookup SectionID
    -- ======================================
    IF (@SectionID IS NULL)
    BEGIN
        SELECT @SectionID = ID 
        FROM [System.Option.Section] 
        WHERE Name = @SectionName;

        IF (@SectionID IS NULL)
			THROW 180000, '@SectionName not found.', 1;
    END;

	WITH Section (LevelType, ID, Level, Name, Description, ParentID, ImageName)
	AS
	(
		SELECT CONVERT(VARCHAR(12),'Section'), S.ID, CONVERT(TINYINT, 0), S.Name, CONVERT(VARCHAR(max), NULL), CONVERT(SMALLINT, NULL) AS ParentID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Section] S
		WHERE S.ID = @SectionID

		UNION ALL

		SELECT CONVERT(VARCHAR(12),'Section'), S.ID, CONVERT(TINYINT, P.Level+1), S.Name, CONVERT(VARCHAR(max), NULL), P.ID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Section] S
		JOIN Section P on P.ID = S.ParentID AND P.LevelType = 'Section'

		UNION ALL

		SELECT CONVERT(VARCHAR(12),'Category'), C.ID, CONVERT(TINYINT, P.Level+1), C.Name, C.Description, P.ID, CONVERT(VARCHAR(255), NULL) AS ImageName
		FROM [System.Option.Category] C
		JOIN Section P on P.ID = C.SectionID AND P.LevelType = 'Section'
	)
	SELECT *
	FROM Section S
END