USE [Dev.Endor.Business.DB1];
GO

-- ========================================================
-- 
-- Name: [Option.GetValue]
--
-- Description: This procedure looks up a option/setting based on a hierarchy of possibilities.
-- 
--
-- Sample Use:   EXEC dbo.[Option.GetValue] @OptionName = 'GLAccount.TaxName1', @BID=2, @LocationID = 1
-- Returns:  Value varchar(MAX), OptionLevel TINYINT
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128
-- ========================================================

CREATE PROCEDURE [Option.GetValue]
 --DECLARE 
        @OptionID       INT          = NULL -- = 2
      , @OptionName     VARCHAR(255) = NULL -- 'GLAccount.TaxName1'

      , @AssociationID  tinyint      = NULL
	  , @BID            smallint     = NULL
	  , @LocationID     smallint     = NULL
	  , @StoreFrontID   smallint     = NULL
	  , @EmployeeID     smallint     = NULL
	  , @CompanyID      int          = NULL
	  , @ContactID      int          = NULL
AS
BEGIN
    DECLARE @Result VARCHAR(MAX)
          , @Level  TINYINT     
          ;

    IF (@OptionID IS NULL) 
    BEGIN
        SELECT @OptionID = ID 
        FROM [System.Option.Definition]
        WHERE Name = @OptionName
        ;
        IF (@OptionID IS NULL)
            RETURN NULL;
    END;

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
	    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

		IF (@AnswerCount > 1)
			THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID = @BID AND OptionID = @OptionID
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF (@Result IS NULL) 
        SELECT TOP 1 @Result = Value, @Level = OptionLevel
        FROM [Option.Data]
        WHERE BID IS NULL AND OptionID = @OptionID
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF (@Result IS NULL) 
        SELECT @Result = DefaultValue 
             , @Level  = 0
        FROM [System.Option.Definition]
        WHERE ID = @OptionID
    ;

    SELECT 
	    CAST (@OptionID AS SMALLINT) AS ID -- needed in the EF
		,@Result as [Value]
        , @Level as [OptionLevel];
END
