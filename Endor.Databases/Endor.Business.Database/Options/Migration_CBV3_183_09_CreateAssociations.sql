-- Option.Data
ALTER TABLE [dbo].[Option.Data]  WITH CHECK ADD  CONSTRAINT [FK_Option.Data_enum.Option.Level] FOREIGN KEY([OptionLevel])
REFERENCES [dbo].[enum.Option.Level] ([ID])
;

ALTER TABLE [dbo].[Option.Data] CHECK CONSTRAINT [FK_Option.Data_enum.Option.Level]
;

ALTER TABLE [dbo].[Option.Data]  WITH CHECK ADD  CONSTRAINT [FK_Option.Data_System.Option.Definition] FOREIGN KEY([OptionID])
REFERENCES [dbo].[System.Option.Definition] ([ID])
;

ALTER TABLE [dbo].[Option.Data] CHECK CONSTRAINT [FK_Option.Data_System.Option.Definition]
;

-- System.Option.Definitions
ALTER TABLE [dbo].[System.Option.Definition]  WITH CHECK ADD  CONSTRAINT [FK_System.Option.Definition_enum.CustomField.DataType] FOREIGN KEY([DataType])
REFERENCES [dbo].[enum.CustomField.DataType] ([ID])
;

ALTER TABLE [dbo].[System.Option.Definition] CHECK CONSTRAINT [FK_System.Option.Definition_enum.CustomField.DataType]
;

ALTER TABLE [dbo].[System.Option.Definition]  WITH CHECK ADD  CONSTRAINT [FK_System.Option.Definition_System.Option.Category] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[System.Option.Category] ([ID])
;

ALTER TABLE [dbo].[System.Option.Definition] CHECK CONSTRAINT [FK_System.Option.Definition_System.Option.Category]
;

