
CREATE TABLE [dbo].[enum.Option.Level](
	[ID] [tinyint] NOT NULL,
	[Name] [varchar](255) NOT NULL,
 CONSTRAINT [PK_enum.Option.Level] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (0, N'Default')
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (1, N'System')
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (2, N'Association')
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (4, N'Business')
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (8, N'Location')
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (16, N'Storefront')
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (32, N'Employee')
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (64, N'Company')
;
INSERT [dbo].[enum.Option.Level] ([ID], [Name]) VALUES (128, N'Contact')
;
