drop procedure if exists [Option.GetValues]
go

-- ========================================================
-- 
-- Name: [Option.GetValues]
--
-- Description: This procedure looks up a option/setting based on a hierarchy of possibilities.
-- 
--
-- Sample Use:   EXEC dbo.[Option.GetValue] @OptionName = 'GLAccount.TaxName1', @BID=2, @LocationID = 1
-- Returns: Table with columns
	-- ID INT
	-- [Name] VARCHAR(255),
	-- [Value] VARCHAR(255),
	-- [Level] INT,
	-- CategoryID INT,
	-- CategoryName VARCHAR(255),
	-- SectionName VARCHAR(255)

    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128
-- ========================================================

CREATE PROCEDURE [dbo].[Option.GetValues]
-- DECLARE 
		@AssociationID  tinyint      = NULL
		,@BID            smallint     = NULL
		,@LocationID     smallint     = NULL
		,@StoreFrontID   smallint     = NULL
		,@EmployeeID     smallint     = NULL
		,@CompanyID      int          = NULL
		,@ContactID      int          = NULL
AS

BEGIN
	DECLARE @Result TABLE (
		ID INT
		,[Name] VARCHAR(255)
		,[Value] VARCHAR(255)
		,[Level] TINYINT
		,CategoryID SMALLINT
		,CategoryName VARCHAR(255)
		,SectionName VARCHAR(255)
	)

    -- Lookup values in the option hiearchy not supplied
    IF (@BID IS NOT NULL)
    BEGIN
        IF (@ContactID IS NOT NULL) AND (@CompanyID IS NULL) 
            SELECT @CompanyID = Company.ID
                 , @LocationID = Company.LocationID
            FROM [Company.Data] Company
            JOIN [Contact.Data] Contact ON Contact.CompanyID = @CompanyID
            WHERE Contact.ID = @ContactID
            ;

        IF (@CompanyID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = Company.LocationID
            FROM [Company.Data] Company
            WHERE Company.ID = @CompanyID
            ;

        IF (@EmployeeID IS NOT NULL) AND (@LocationID IS NULL) 
            SELECT @LocationID = E.LocationID
            FROM [Employee.Data] E
            WHERE E.ID = @EmployeeID
            ;

            -- no logic for storefront yet so far
    END
    ELSE IF COALESCE(@ContactID, @CompanyID, @EmployeeID, @StorefrontID, @LocationID) IS NOT NULL
    BEGIN;
        THROW 180000, 'You must pass a BID if passing @ContactID, @CompanyID, @EmployeeID, @StorefrontID, or @LocationID', 1;
        RETURN;
    END;


    -- The value of an option is the value that is set that is closest to the customer
    -- For efficiency, we are going to run different queries based on the level we are seeking
    -- OPTION LEVELS
    --    Default=0
    --    System=1
    --    Association=2
    --    Business=4
    --    Location=8
    --    Storefront=16
    --    Employee=32
    --    Company=64
    --    Contact=128

    -- For efficiency, we have 2 indexes on the table.  
    --      The first is by BID, OptionID, OptionLevel
    --      The second if by OptionID, OptionLevel but only indexes WHERE BID IS NULL
    -- To utilize this, we separate the two cases and only use the latter when we have to.

    IF (@ContactID IS NOT NULL)
		INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (EmployeeID   IS NULL)
        AND (ContactID    = @ContactID
                OR CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC
                          

    ELSE IF (@CompanyID IS NOT NULL)
        INSERT INTO @Result 
		SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN  [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (ContactID    IS NULL)
        AND (EmployeeID   IS NULL)
        AND (CompanyID    = @CompanyID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@EmployeeID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID
        AND (CompanyID    IS NULL)
        AND (EmployeeID   = @EmployeeID
                OR StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@StoreFrontID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID = @StoreFrontID
                OR LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@LocationID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   = @LocationID)
        ORDER BY OptionLevel DESC

    ELSE IF (@BID IS NOT NULL)
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID = @BID 
        AND (CompanyID    IS NULL )
        AND (EmployeeID   IS NULL )
        AND (StoreFrontID IS NULL )
        AND (LocationID   IS NULL )
        ORDER BY OptionLevel DESC
    ;

    -- Now check the association(franchise) and system level if we don't have values
    IF NOT EXISTS (SELECT COUNT(*) FROM @Result) 
        INSERT INTO @Result 
        SELECT 
		b.[ID]
		,b.[Name]
		,a.[Value]
		,a.[OptionLevel]
		,b.[CategoryID]
		,c.[Name] AS CategoryName
		,d.[Name] AS SectionName
        FROM [Option.Data] a
		FULL JOIN [System.Option.Definition] b ON a.OptionID = b.ID
		LEFT JOIN [System.Option.Category] c ON c.ID = b.CategoryID
		LEFT JOIN [System.Option.Section] d ON d.ID = c.SectionID
        WHERE BID IS NULL
        AND (AssociationID IS NULL OR AssociationID = @AssociationID)
        ORDER BY OptionLevel DESC
    ;

    -- If still NULL, pull the default value from the [System.Option.Definition] Table
    IF NOT EXISTS (SELECT COUNT(*) FROM @Result) 
        INSERT INTO @Result
		SELECT 
		0,
		'Default',
		DefaultValue, 
		0,
		0,
		'Default Category',
		'Default Section'
        FROM [System.Option.Definition]
        --WHERE ID = @OptionID
    ;

    SELECT 
	ID
	,[Name]
	,[Value]
	,[Level]
	,CategoryID
	,CategoryName
	,SectionName
	FROM @Result;
END
