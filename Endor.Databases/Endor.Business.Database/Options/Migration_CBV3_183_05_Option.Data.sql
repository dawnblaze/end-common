CREATE TABLE [Option.Data](
    [ID] [int] IDENTITY(1,1) NOT NULL,
    [ClassTypeID]  AS ((1800)),
    [CreatedDate] [date] NOT NULL,
    [ModifiedDT] [datetime2](2) NOT NULL,
    [IsActive] [bit] NOT NULL,
    [OptionID] [smallint] NOT NULL,
    [Value] [varchar](max) NOT NULL,
    [AssociationID] [tinyint] SPARSE NULL,
    [BID] [smallint] NULL,
    [LocationID] [smallint] NULL,
    [StoreFrontID] [smallint] SPARSE  NULL,
    [EmployeeID] [smallint] SPARSE  NULL,
    [CompanyID] [int] SPARSE  NULL,
    [ContactID] [int] SPARSE  NULL,
    [OptionLevel]  AS (isnull(CONVERT([tinyint],case when [ContactID] IS NOT NULL then (128) when [CompanyID] IS NOT NULL then (64) when [EmployeeID] IS NOT NULL then (32) when [StoreFrontID] IS NOT NULL then (16) when [LocationID] IS NOT NULL then (8) when [BID] IS NOT NULL then (4) when [AssociationID] IS NOT NULL then (2) else (1) end),(1))) PERSISTED NOT NULL,
    CONSTRAINT [PK_Option.Data] PRIMARY KEY CLUSTERED (ID)
)
;
ALTER TABLE [Option.Data] ADD  CONSTRAINT [DF_Option.Data2_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
;
ALTER TABLE [Option.Data] ADD  CONSTRAINT [DF_Option.Data2_ModifiedDT]  DEFAULT (getutcdate()) FOR [ModifiedDT]
;
ALTER TABLE [Option.Data] ADD  CONSTRAINT [DF_Option.Data2_IsActive]  DEFAULT ((1)) FOR [IsActive]
;

CREATE NONCLUSTERED INDEX [IX_Option.Data_BID_OptionID_Level] ON [Option.Data]
( [BID] ASC, [OptionID] ASC, [OptionLevel] DESC )
;
CREATE NONCLUSTERED INDEX [IX_Option.Data_OptionID_Level_BIDNull] ON [Option.Data]
( [OptionID] ASC, [OptionLevel] DESC )
WHERE ([BID] IS NULL)
;

--
-- SAMPLE DATA FOR TESTING
--
SET IDENTITY_INSERT [Option.Data] ON
;
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES (1, CAST(N'2017-08-20' AS Date), CAST(N'2017-08-20T15:18:37.4200000' AS DateTime2), 1, 2, N'HST', NULL, 1, NULL, NULL, NULL, NULL, NULL)
;
INSERT [Option.Data] ([ID], [CreatedDate], [ModifiedDT], [IsActive], [OptionID], [Value], [AssociationID], [BID], [LocationID], [StoreFrontID], [EmployeeID], [CompanyID], [ContactID]) VALUES (2, CAST(N'2017-08-20' AS Date), CAST(N'2017-08-20T15:21:15.8900000' AS DateTime2), 1, 2, N'State', 4, NULL, NULL, NULL, NULL, NULL, NULL)
;
SET IDENTITY_INSERT [Option.Data] OFF
;