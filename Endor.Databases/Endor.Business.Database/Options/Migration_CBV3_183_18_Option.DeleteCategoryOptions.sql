DROP PROCEDURE IF EXISTS [Option.DeleteCategoryOptions];
GO
-- ========================================================
-- 
-- Name: [Option.DeleteCategoryOptions]
--
-- Description: This procedure deletes the options list for the selected category.
-- 
--
-- Sample Use:   
/*
    EXEC dbo.[Option.DeleteCategoryOptions] 
        -- Require Fields
          @CategoryID       = 1
		  @CategoryName     = 'COGS Accounts'
*/
-- ========================================================

CREATE PROCEDURE [Option.DeleteCategoryOptions]
-- DECLARE 
            @CategoryID        INT          = NULL
			,@CategoryName     VARCHAR(255) = NULL
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF ((@CategoryID IS NULL) AND (@CategoryName IS NULL)) OR ((@CategoryID IS NOT NULL) AND (@CategoryName IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @CategoryID or the @CategoryName.', 1;

    -- ======================================
    -- Lookup CategoryID
    -- ======================================
    IF (@CategoryID IS NULL)
    BEGIN
        SELECT @CategoryID = ID 
        FROM [System.Option.Category] 
        WHERE Name = @CategoryName;

        IF (@CategoryID IS NULL)
			THROW 180000, '@CategoryName not found.', 1;
    END;

	WITH Category (ID, Name, Value, Level, CategoryID, CategoryName, SectionName)
	AS
	(
		SELECT D.ID, D.Name, O.Value, O.OptionLevel, C.ID, C.Name, S.Name
		FROM [System.Option.Definition] D
		INNER JOIN [Option.Data] O ON O.OptionID = D.ID
		INNER JOIN [System.Option.Category] C ON C.ID = D.CategoryID
		INNER JOIN [System.Option.Section] S ON S.ID = C.SectionID
	
		WHERE CategoryID = @CategoryID
	)
	DELETE FROM [Option.Data] WHERE OptionID IN (SELECT ID FROM Category)
END