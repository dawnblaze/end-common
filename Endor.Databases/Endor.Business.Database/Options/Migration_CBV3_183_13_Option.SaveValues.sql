USE [Dev.Endor.Business.DB1]
GO
/****** Object:  StoredProcedure [dbo].[Option.SaveValues]    Script Date: 12/15/2017 4:37:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
/*
    Name: [Option.SaveValues]

    Description: 
        Create a procedure that saves multiple options values
        at once.  The values are all given the same access
        level when saved.

    Sample Use:   

    DECLARE @T OptionsArray;
    INSERT INTO @T
      VALUES  (NULL, 'GLAccount.TaxName1', 'State')
            , (NULL, 'GLAccount.TaxName2', 'City')
            , (NULL, 'Employee.Collection.SortOrder', '4;5;1;5')
            , (NULL, 'Employee.OldSort', NULL)
            ;

    EXEC dbo.[Option.SaveValues] 
        -- Require Fields
          @Options_Array    = @T

        -- One (and only one) of these is required
        , @AssociationID    = NULL
        , @BID              = 2          -- Required (except for dev or franchise use)

        -- One (ad only one) of the following can be supplied when BID is also supplied
        -- BID must be supplied if these are used.
        , @LocationID       = NULL
        , @EmployeeID       = 1
        , @CompanyID        = NULL
        , @ContactID        = NULL
        , @StorefrontID     = NULL

        , @Debug            = 1

*/
-- ===============================================
ALTER PROCEDURE [dbo].[Option.SaveValues]
      @Options_Array  OptionsArray  READONLY

    , @AssociationID  TINYINT       = NULL
    , @BID            SMALLINT      = NULL
    , @LocationID     SMALLINT      = NULL
    , @EmployeeID     SMALLINT      = NULL
    , @CompanyID      INT           = NULL
    , @ContactID      INT           = NULL
    , @StorefrontID   SMALLINT      = NULL

    , @Debug          BIT           = 0
AS
BEGIN
    -- ======================================
    -- Run some checks
    -- ======================================
    IF EXISTS(SELECT * FROM @Options_Array WHERE (OptionID IS NULL) AND (OptionName IS NULL) )
        THROW 180000, 'You must specify the @OptionID or the @OptionName on every row.', 1;

    IF ((@BID IS NULL) AND (@AssociationID IS NULL)) OR ((@BID IS NOT NULL) AND (@AssociationID IS NOT NULL))
        THROW 180000, 'You must specify one (and only one) of the @BID or the @AssociationID.', 1;

    DECLARE @AnswerCount TINYINT =    IIF(@LocationID   IS NULL, 0, 1) 
                                    + IIF(@EmployeeID   IS NULL, 0, 1)
                                    + IIF(@CompanyID    IS NULL, 0, 1)
                                    + IIF(@ContactID    IS NULL, 0, 1)
                                    + IIF(@StoreFrontID IS NULL, 0, 1)
                                    ;

    IF (@BID IS NULL AND @AnswerCount > 0)
        THROW 180000, 'You must specify the @BID with @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    IF (@AnswerCount > 1)
        THROW 180000, 'You can only specify one of the following: @LocationID, @CompanyID, @EmployeeID, @ContactID, or @StorefrontID.', 1;

    -- ======================================
    -- Create a Working Data Table
    -- ======================================
    DECLARE @Options TABLE (
              RowID         INT NOT NULL PRIMARY KEY IDENTITY(1,1)
            , OptionID      INT
            , OptionName    VARCHAR(255)
            , Value         VARCHAR(MAX)
            , InstanceID    INT
            , IsNewAdHoc    BIT
    );

    INSERT INTO @Options(OptionID, OptionName, Value, IsNewAdHoc)
        SELECT *, 0
        FROM @Options_Array;

    -- ======================================
    -- Lookup OptionID and Save Option Definition if Needed
    -- ======================================

    -- Lookup any missing  IDs from the Definitions Table
    -- -------------------------------------------------
    UPDATE Opt
    SET OptionID = Def.ID
    FROM @Options Opt
    JOIN [System.Option.Definition] Def ON Def.Name = Opt.OptionName
    WHERE OptionID IS NULL
    ;

    -- If not defined, and NULL, there will be nothing to delete
    -- -------------------------------------------------
    DELETE FROM @Options
    WHERE (OptionID IS NULL)
        AND (Value IS NULL)
    ;

    -- Any missing IDs must be AdHoc so create them
    -- -------------------------------------------------
    DECLARE @NewIDs INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);

    IF (@NewIDs > 0)
    BEGIN
		DECLARE @NewID INT
		DECLARE @NewIDCount INT = (SELECT COUNT(*) FROM @Options WHERE InstanceID IS NULL);
		EXEC @NewID = [dbo].[Util.ID.GetID] @BID, 1810, @NewIDCount;


        -- Any missing IDs must be AdHoc so create them
        -- -------------------------------------------------
        UPDATE @Options
        SET   IsNewAdHoc = 1
            , OptionID = @NewID, @NewID = @NewID + 1
        WHERE OptionID IS NULL
        ;

        INSERT INTO [System.Option.Definition] (ID, Name, Label, DefaultValue, DataType, CategoryID, IsHidden)
        SELECT OptionID AS ID
                , OptionName AS Name
                , 'Custom: '+OptionName AS Label
                , NULL AS DefaultValue
                , 0 AS DataType -- Always string
                , -1 AS CategoryID  -- AdHoc Options
                , 1 AS IsHidden
        FROM @Options
        WHERE IsNewAdHoc = 1
        ;
    END;

    -- ======================================
    -- Lookup Current Instance if it Exists
    -- ======================================
    UPDATE Opt
    SET InstanceID = 
           CASE WHEN @LocationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.LocationID = @LocationID)

                WHEN @EmployeeID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.EmployeeID = @EmployeeID)

                WHEN @CompanyID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.CompanyID  = @CompanyID )

                WHEN @ContactID  IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.ContactID  = @ContactID )

                WHEN @StorefrontID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID AND D.StorefrontID = @StorefrontID)

                WHEN @BID        IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.BID = @BID)

                WHEN @AssociationID IS NOT NULL 
                THEN (SELECT ID FROM [Option.Data] D WHERE D.OptionID = Opt.OptionID AND D.AssociationID = @AssociationID)

           ELSE NULL
           END
    FROM @Options Opt
    ;

    IF (@Debug=1)
        SELECT * FROM @Options;

    -- ======================================
    -- Delete any Options with NULL Values Passed in
    -- ======================================
    DELETE D
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NULL
    ;

    -- ======================================
    -- Update any Existing Values
    -- ======================================
    UPDATE D
    SET ModifiedDT = GetUTCDate()
      , Value = Opt.Value
    FROM [Option.Data] D
    JOIN @Options Opt on Opt.InstanceID = D.ID
    WHERE Opt.Value IS NOT NULL
    ;

    -- ======================================
    -- Create New Options if not Found Values
    -- ======================================
    INSERT INTO [Option.Data] 
        ( CreatedDate, ModifiedDT, IsActive, OptionID
        , [Value], AssociationID, BID, LocationID
        , StoreFrontID, EmployeeID, CompanyID, ContactID
        )
        SELECT
            GetUTCDate(), GetUTCDate(), 1, OptionID
            , Value, @AssociationID, @BID, @LocationID
            , @StoreFrontID, @EmployeeID, @CompanyID, @ContactID
        FROM @Options
        WHERE InstanceID IS NULL
          AND Value IS NOT NULL
    ;
END