CREATE TABLE [System.Option.Category](
    [ID] [smallint] NOT NULL,
    [Name] [varchar](255) NOT NULL,
    [SectionID] [smallint] NULL,
    [Description] [varchar](max) NULL,
    [OptionLevels] [tinyint] NOT NULL,
    [IsHidden] [bit] NOT NULL,
    [IsSystemOption]  AS (isnull(CONVERT([bit],case when ([OptionLevels]&(1))<>(0) then (1) else (0) end),(0))) PERSISTED NOT NULL,
    [IsAssociationOption]  AS (isnull(CONVERT([bit],case when ([OptionLevels]&(2))<>(0) then (1) else (0) end),(0))) PERSISTED NOT NULL,
    [IsBusinessOption]  AS (isnull(CONVERT([bit],case when ([OptionLevels]&(4))<>(0) then (1) else (0) end),(0))) PERSISTED NOT NULL,
    [IsLocationOption]  AS (isnull(CONVERT([bit],case when ([OptionLevels]&(8))<>(0) then (1) else (0) end),(0))) PERSISTED NOT NULL,
    [IsStorefrontOption]  AS (isnull(CONVERT([bit],case when ([OptionLevels]&(16))<>(0) then (1) else (0) end),(0))) PERSISTED NOT NULL,
    [IsEmployeeOption]  AS (isnull(CONVERT([bit],case when ([OptionLevels]&(32))<>(0) then (1) else (0) end),(0))) PERSISTED NOT NULL,
    [IsCompanyOption]  AS (isnull(CONVERT([bit],case when ([OptionLevels]&(64))<>(0) then (1) else (0) end),(0))) PERSISTED NOT NULL,
    [IsContactOption]  AS (isnull(CONVERT([bit],case when ([OptionLevels]&(128))<>(0) then (1) else (0) end),(0))) PERSISTED NOT NULL,
    CONSTRAINT [PK_System.Option.Category] PRIMARY KEY CLUSTERED (ID)
);

CREATE NONCLUSTERED INDEX [[IX_System.Option.Category_SectionID] ON [System.Option.Category]
( [SectionID] ASC, [Name] ASC, [IsHidden] ASC, [OptionLevels] ASC );

ALTER TABLE [System.Option.Category] ADD  CONSTRAINT [DF_System.Option.Category_IsHidden_1]  DEFAULT ((0)) FOR [IsHidden];
ALTER TABLE [System.Option.Category]  WITH CHECK ADD  CONSTRAINT [FK_System.Option.Category_System.Option.Section] FOREIGN KEY([SectionID])
REFERENCES [System.Option.Section] ([ID]);
ALTER TABLE [System.Option.Category] CHECK CONSTRAINT [FK_System.Option.Category_System.Option.Section];

-- SAMPLE DATA

INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden]) VALUES (1, N'COGS Accounts', 4, N'Morbi arcu odio, condimentum vitae sapien scelerisque...', 8, 0)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden]) VALUES (2, N'Income Accounts', 3, N'Morbi arcu odio, condimentum vitae sapien scelerisque...', 8, 0)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden]) VALUES (3, N'Domain', 5, N'Morbi arcu odio, condimentum vitae sapien scelerisque...', 8, 0)
INSERT [System.Option.Category] ([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden]) VALUES (4, N'Email', 5, N'Morbi arcu odio, condimentum vitae sapien scelerisque...', 8, 0)

INSERT INTO [SYSTEM.OPTION.Category]
([ID], [Name], [SectionID], [Description], [OptionLevels], [IsHidden])
Values
(5,	'Hours of Operation',	6,	'Change this location''s hours of operation',	8),
(6,	'Invoice Numbering',	9,	'Invoice numbering options',	4)